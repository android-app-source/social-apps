.class public LX/E52;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E52;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E53;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E52",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E53;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077772
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2077773
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E52;->b:LX/0Zi;

    .line 2077774
    iput-object p1, p0, LX/E52;->a:LX/0Ot;

    .line 2077775
    return-void
.end method

.method public static a(LX/0QB;)LX/E52;
    .locals 4

    .prologue
    .line 2077776
    sget-object v0, LX/E52;->c:LX/E52;

    if-nez v0, :cond_1

    .line 2077777
    const-class v1, LX/E52;

    monitor-enter v1

    .line 2077778
    :try_start_0
    sget-object v0, LX/E52;->c:LX/E52;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077779
    if-eqz v2, :cond_0

    .line 2077780
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077781
    new-instance v3, LX/E52;

    const/16 p0, 0x311b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E52;-><init>(LX/0Ot;)V

    .line 2077782
    move-object v0, v3

    .line 2077783
    sput-object v0, LX/E52;->c:LX/E52;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077784
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077785
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077786
    :cond_1
    sget-object v0, LX/E52;->c:LX/E52;

    return-object v0

    .line 2077787
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077788
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2077789
    check-cast p2, LX/E51;

    .line 2077790
    iget-object v0, p0, LX/E52;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E53;

    iget-object v2, p2, LX/E51;->a:Ljava/lang/String;

    iget-object v3, p2, LX/E51;->b:LX/0Px;

    iget-object v4, p2, LX/E51;->c:LX/2km;

    iget-object v5, p2, LX/E51;->d:Ljava/lang/String;

    iget-object v6, p2, LX/E51;->e:Ljava/lang/String;

    move-object v1, p1

    .line 2077791
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const v8, 0x7f01072b

    invoke-interface {v7, v8}, LX/1Dh;->U(I)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const/4 p0, 0x0

    invoke-interface {v8, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    const/4 p0, 0x2

    invoke-interface {v8, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    const/4 p0, 0x6

    const p1, 0x7f010717

    invoke-interface {v8, p0, p1}, LX/1Dh;->p(II)LX/1Dh;

    move-result-object v8

    const/16 p0, 0x8

    const p1, 0x7f0b163a

    invoke-interface {v8, p0, p1}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    const p1, 0x7f0b0056

    invoke-virtual {p0, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    sget-object p1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {p0, p1}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    iget-object p0, v0, LX/E53;->a:LX/E4m;

    const/4 p1, 0x0

    .line 2077792
    new-instance p2, LX/E4l;

    invoke-direct {p2, p0}, LX/E4l;-><init>(LX/E4m;)V

    .line 2077793
    iget-object v0, p0, LX/E4m;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E4k;

    .line 2077794
    if-nez v0, :cond_0

    .line 2077795
    new-instance v0, LX/E4k;

    invoke-direct {v0, p0}, LX/E4k;-><init>(LX/E4m;)V

    .line 2077796
    :cond_0
    invoke-static {v0, v1, p1, p1, p2}, LX/E4k;->a$redex0(LX/E4k;LX/1De;IILX/E4l;)V

    .line 2077797
    move-object p2, v0

    .line 2077798
    move-object p1, p2

    .line 2077799
    move-object p0, p1

    .line 2077800
    iget-object p1, p0, LX/E4k;->a:LX/E4l;

    iput-object v3, p1, LX/E4l;->a:LX/0Px;

    .line 2077801
    iget-object p1, p0, LX/E4k;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/BitSet;->set(I)V

    .line 2077802
    move-object p0, p0

    .line 2077803
    iget-object p1, p0, LX/E4k;->a:LX/E4l;

    iput-object v4, p1, LX/E4l;->b:LX/2km;

    .line 2077804
    move-object p0, p0

    .line 2077805
    iget-object p1, p0, LX/E4k;->a:LX/E4l;

    iput-object v5, p1, LX/E4l;->c:Ljava/lang/String;

    .line 2077806
    move-object p0, p0

    .line 2077807
    iget-object p1, p0, LX/E4k;->a:LX/E4l;

    iput-object v6, p1, LX/E4l;->d:Ljava/lang/String;

    .line 2077808
    move-object p0, p0

    .line 2077809
    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/4 p1, 0x1

    const p2, 0x7f0b163e

    invoke-interface {p0, p1, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 2077810
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2077811
    invoke-static {}, LX/1dS;->b()V

    .line 2077812
    const/4 v0, 0x0

    return-object v0
.end method
