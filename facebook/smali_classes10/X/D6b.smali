.class public final LX/D6b;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ov;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D6Y;


# direct methods
.method public constructor <init>(LX/D6Y;)V
    .locals 0

    .prologue
    .line 1965991
    iput-object p1, p0, LX/D6b;->a:LX/D6Y;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ov;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1965992
    const-class v0, LX/2ov;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1965993
    check-cast p1, LX/2ov;

    .line 1965994
    iget-boolean v0, p1, LX/2ov;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D6b;->a:LX/D6Y;

    iget-object v0, v0, LX/7N3;->d:LX/7MP;

    sget-object v1, LX/7MP;->ALWAYS_HIDDEN:LX/7MP;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/D6b;->a:LX/D6Y;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D6b;->a:LX/D6Y;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 1965995
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1965996
    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/D6b;->a:LX/D6Y;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 1965997
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1965998
    sget-object v1, LX/2qV;->ATTEMPT_TO_PAUSE:LX/2qV;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/D6b;->a:LX/D6Y;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 1965999
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1966000
    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/D6b;->a:LX/D6Y;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 1966001
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1966002
    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/D6b;->a:LX/D6Y;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 1966003
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1966004
    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_1

    .line 1966005
    :cond_0
    iget-object v0, p0, LX/D6b;->a:LX/D6Y;

    iget-object v0, v0, LX/D6Y;->r:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;->f()V

    .line 1966006
    :cond_1
    return-void
.end method
