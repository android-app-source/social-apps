.class public final LX/E1w;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/E1y;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/E1x;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2071950
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2071951
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "content"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/E1w;->b:[Ljava/lang/String;

    .line 2071952
    iput v3, p0, LX/E1w;->c:I

    .line 2071953
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/E1w;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/E1w;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/E1w;LX/1De;IILX/E1x;)V
    .locals 1

    .prologue
    .line 2071946
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2071947
    iput-object p4, p0, LX/E1w;->a:LX/E1x;

    .line 2071948
    iget-object v0, p0, LX/E1w;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2071949
    return-void
.end method


# virtual methods
.method public final a(LX/1X1;)LX/E1w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)",
            "LX/E1w;"
        }
    .end annotation

    .prologue
    .line 2071921
    iget-object v0, p0, LX/E1w;->a:LX/E1x;

    iput-object p1, v0, LX/E1x;->a:LX/1X1;

    .line 2071922
    iget-object v0, p0, LX/E1w;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2071923
    return-object p0
.end method

.method public final a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;)LX/E1w;
    .locals 1

    .prologue
    .line 2071942
    iget-object v0, p0, LX/E1w;->a:LX/E1x;

    iput-object p1, v0, LX/E1x;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    .line 2071943
    return-object p0
.end method

.method public final a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;)LX/E1w;
    .locals 1

    .prologue
    .line 2071944
    iget-object v0, p0, LX/E1w;->a:LX/E1x;

    iput-object p1, v0, LX/E1x;->d:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    .line 2071945
    return-object p0
.end method

.method public final a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;)LX/E1w;
    .locals 1

    .prologue
    .line 2071940
    iget-object v0, p0, LX/E1w;->a:LX/E1x;

    iput-object p1, v0, LX/E1x;->e:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    .line 2071941
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2071936
    invoke-super {p0}, LX/1X5;->a()V

    .line 2071937
    const/4 v0, 0x0

    iput-object v0, p0, LX/E1w;->a:LX/E1x;

    .line 2071938
    sget-object v0, LX/E1y;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2071939
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/E1w;
    .locals 1

    .prologue
    .line 2071934
    iget-object v0, p0, LX/E1w;->a:LX/E1x;

    iput-object p1, v0, LX/E1x;->b:Ljava/lang/String;

    .line 2071935
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/E1y;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2071924
    iget-object v1, p0, LX/E1w;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/E1w;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/E1w;->c:I

    if-ge v1, v2, :cond_2

    .line 2071925
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2071926
    :goto_0
    iget v2, p0, LX/E1w;->c:I

    if-ge v0, v2, :cond_1

    .line 2071927
    iget-object v2, p0, LX/E1w;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2071928
    iget-object v2, p0, LX/E1w;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2071929
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2071930
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2071931
    :cond_2
    iget-object v0, p0, LX/E1w;->a:LX/E1x;

    .line 2071932
    invoke-virtual {p0}, LX/E1w;->a()V

    .line 2071933
    return-object v0
.end method
