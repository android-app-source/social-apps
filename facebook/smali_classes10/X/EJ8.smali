.class public final LX/EJ8;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EJ9;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<+",
            "LX/8cw;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/EJ9;


# direct methods
.method public constructor <init>(LX/EJ9;)V
    .locals 1

    .prologue
    .line 2103309
    iput-object p1, p0, LX/EJ8;->b:LX/EJ9;

    .line 2103310
    move-object v0, p1

    .line 2103311
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2103312
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2103313
    const-string v0, "SearchResultsSimpleCoverPhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2103314
    if-ne p0, p1, :cond_1

    .line 2103315
    :cond_0
    :goto_0
    return v0

    .line 2103316
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2103317
    goto :goto_0

    .line 2103318
    :cond_3
    check-cast p1, LX/EJ8;

    .line 2103319
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2103320
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2103321
    if-eq v2, v3, :cond_0

    .line 2103322
    iget-object v2, p0, LX/EJ8;->a:LX/CzL;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/EJ8;->a:LX/CzL;

    iget-object v3, p1, LX/EJ8;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2103323
    goto :goto_0

    .line 2103324
    :cond_4
    iget-object v2, p1, LX/EJ8;->a:LX/CzL;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
