.class public final enum LX/Cyd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cyd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cyd;

.field public static final enum LOAD:LX/Cyd;

.field public static final enum LOAD_MORE:LX/Cyd;

.field public static final enum STREAMING:LX/Cyd;

.field public static final enum UNKNOWN:LX/Cyd;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1953364
    new-instance v0, LX/Cyd;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/Cyd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cyd;->UNKNOWN:LX/Cyd;

    .line 1953365
    new-instance v0, LX/Cyd;

    const-string v1, "LOAD"

    invoke-direct {v0, v1, v3}, LX/Cyd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cyd;->LOAD:LX/Cyd;

    .line 1953366
    new-instance v0, LX/Cyd;

    const-string v1, "LOAD_MORE"

    invoke-direct {v0, v1, v4}, LX/Cyd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cyd;->LOAD_MORE:LX/Cyd;

    .line 1953367
    new-instance v0, LX/Cyd;

    const-string v1, "STREAMING"

    invoke-direct {v0, v1, v5}, LX/Cyd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cyd;->STREAMING:LX/Cyd;

    .line 1953368
    const/4 v0, 0x4

    new-array v0, v0, [LX/Cyd;

    sget-object v1, LX/Cyd;->UNKNOWN:LX/Cyd;

    aput-object v1, v0, v2

    sget-object v1, LX/Cyd;->LOAD:LX/Cyd;

    aput-object v1, v0, v3

    sget-object v1, LX/Cyd;->LOAD_MORE:LX/Cyd;

    aput-object v1, v0, v4

    sget-object v1, LX/Cyd;->STREAMING:LX/Cyd;

    aput-object v1, v0, v5

    sput-object v0, LX/Cyd;->$VALUES:[LX/Cyd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1953361
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cyd;
    .locals 1

    .prologue
    .line 1953363
    const-class v0, LX/Cyd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cyd;

    return-object v0
.end method

.method public static values()[LX/Cyd;
    .locals 1

    .prologue
    .line 1953362
    sget-object v0, LX/Cyd;->$VALUES:[LX/Cyd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cyd;

    return-object v0
.end method
