.class public LX/DpU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final id:Ljava/lang/Integer;

.field public final public_key:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2045665
    new-instance v0, LX/1sv;

    const-string v1, "PublicKeyWithID"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpU;->b:LX/1sv;

    .line 2045666
    new-instance v0, LX/1sw;

    const-string v1, "public_key"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpU;->c:LX/1sw;

    .line 2045667
    new-instance v0, LX/1sw;

    const-string v1, "id"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpU;->d:LX/1sw;

    .line 2045668
    const/4 v0, 0x1

    sput-boolean v0, LX/DpU;->a:Z

    return-void
.end method

.method public constructor <init>([BLjava/lang/Integer;)V
    .locals 0

    .prologue
    .line 2045661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2045662
    iput-object p1, p0, LX/DpU;->public_key:[B

    .line 2045663
    iput-object p2, p0, LX/DpU;->id:Ljava/lang/Integer;

    .line 2045664
    return-void
.end method

.method public static b(LX/1su;)LX/DpU;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2045647
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    .line 2045648
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2045649
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_2

    .line 2045650
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 2045651
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2045652
    :pswitch_0
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xb

    if-ne v3, v4, :cond_0

    .line 2045653
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v1

    goto :goto_0

    .line 2045654
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2045655
    :pswitch_1
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 2045656
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2045657
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2045658
    :cond_2
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2045659
    new-instance v2, LX/DpU;

    invoke-direct {v2, v1, v0}, LX/DpU;-><init>([BLjava/lang/Integer;)V

    .line 2045660
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2045619
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2045620
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2045621
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2045622
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PublicKeyWithID"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045623
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045624
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045625
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045626
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045627
    const-string v4, "public_key"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045628
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045629
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045630
    iget-object v4, p0, LX/DpU;->public_key:[B

    if-nez v4, :cond_3

    .line 2045631
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045632
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045633
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045634
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045635
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045636
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045637
    iget-object v0, p0, LX/DpU;->id:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 2045638
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045639
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045640
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045641
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2045642
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2045643
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2045644
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2045645
    :cond_3
    iget-object v4, p0, LX/DpU;->public_key:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2045646
    :cond_4
    iget-object v0, p0, LX/DpU;->id:Ljava/lang/Integer;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2045584
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2045585
    iget-object v0, p0, LX/DpU;->public_key:[B

    if-eqz v0, :cond_0

    .line 2045586
    sget-object v0, LX/DpU;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045587
    iget-object v0, p0, LX/DpU;->public_key:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2045588
    :cond_0
    iget-object v0, p0, LX/DpU;->id:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2045589
    sget-object v0, LX/DpU;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045590
    iget-object v0, p0, LX/DpU;->id:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2045591
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2045592
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2045593
    return-void
.end method

.method public final a(LX/DpU;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2045602
    if-nez p1, :cond_1

    .line 2045603
    :cond_0
    :goto_0
    return v2

    .line 2045604
    :cond_1
    iget-object v0, p0, LX/DpU;->public_key:[B

    if-eqz v0, :cond_6

    move v0, v1

    .line 2045605
    :goto_1
    iget-object v3, p1, LX/DpU;->public_key:[B

    if-eqz v3, :cond_7

    move v3, v1

    .line 2045606
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 2045607
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2045608
    iget-object v0, p0, LX/DpU;->public_key:[B

    iget-object v3, p1, LX/DpU;->public_key:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2045609
    :cond_3
    iget-object v0, p0, LX/DpU;->id:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2045610
    :goto_3
    iget-object v3, p1, LX/DpU;->id:Ljava/lang/Integer;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2045611
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2045612
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2045613
    iget-object v0, p0, LX/DpU;->id:Ljava/lang/Integer;

    iget-object v3, p1, LX/DpU;->id:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_5
    move v2, v1

    .line 2045614
    goto :goto_0

    :cond_6
    move v0, v2

    .line 2045615
    goto :goto_1

    :cond_7
    move v3, v2

    .line 2045616
    goto :goto_2

    :cond_8
    move v0, v2

    .line 2045617
    goto :goto_3

    :cond_9
    move v3, v2

    .line 2045618
    goto :goto_4
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2045598
    if-nez p1, :cond_1

    .line 2045599
    :cond_0
    :goto_0
    return v0

    .line 2045600
    :cond_1
    instance-of v1, p1, LX/DpU;

    if-eqz v1, :cond_0

    .line 2045601
    check-cast p1, LX/DpU;

    invoke-virtual {p0, p1}, LX/DpU;->a(LX/DpU;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2045597
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2045594
    sget-boolean v0, LX/DpU;->a:Z

    .line 2045595
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpU;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2045596
    return-object v0
.end method
