.class public final enum LX/DAH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DAH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DAH;

.field public static final enum OFF:LX/DAH;

.field public static final enum ON:LX/DAH;

.field public static final enum UNKNOWN:LX/DAH;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1971000
    new-instance v0, LX/DAH;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/DAH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAH;->UNKNOWN:LX/DAH;

    .line 1971001
    new-instance v0, LX/DAH;

    const-string v1, "ON"

    invoke-direct {v0, v1, v3}, LX/DAH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAH;->ON:LX/DAH;

    .line 1971002
    new-instance v0, LX/DAH;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v4}, LX/DAH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAH;->OFF:LX/DAH;

    .line 1971003
    const/4 v0, 0x3

    new-array v0, v0, [LX/DAH;

    sget-object v1, LX/DAH;->UNKNOWN:LX/DAH;

    aput-object v1, v0, v2

    sget-object v1, LX/DAH;->ON:LX/DAH;

    aput-object v1, v0, v3

    sget-object v1, LX/DAH;->OFF:LX/DAH;

    aput-object v1, v0, v4

    sput-object v0, LX/DAH;->$VALUES:[LX/DAH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1971004
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DAH;
    .locals 1

    .prologue
    .line 1971005
    const-class v0, LX/DAH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DAH;

    return-object v0
.end method

.method public static values()[LX/DAH;
    .locals 1

    .prologue
    .line 1971006
    sget-object v0, LX/DAH;->$VALUES:[LX/DAH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DAH;

    return-object v0
.end method
