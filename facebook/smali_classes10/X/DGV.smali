.class public LX/DGV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DGV",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979940
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1979941
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DGV;->b:LX/0Zi;

    .line 1979942
    iput-object p1, p0, LX/DGV;->a:LX/0Ot;

    .line 1979943
    return-void
.end method

.method public static a(LX/0QB;)LX/DGV;
    .locals 4

    .prologue
    .line 1979899
    const-class v1, LX/DGV;

    monitor-enter v1

    .line 1979900
    :try_start_0
    sget-object v0, LX/DGV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979901
    sput-object v2, LX/DGV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979902
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979903
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979904
    new-instance v3, LX/DGV;

    const/16 p0, 0x21a8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DGV;-><init>(LX/0Ot;)V

    .line 1979905
    move-object v0, v3

    .line 1979906
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979907
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DGV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979908
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979909
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1979939
    const v0, 0x5dfd276b

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1979927
    check-cast p2, LX/DGU;

    .line 1979928
    iget-object v0, p0, LX/DGV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;

    iget-object v1, p2, LX/DGU;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/DGU;->b:LX/1Pd;

    const/4 p0, 0x1

    const/4 p2, 0x2

    .line 1979929
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    iget-object v3, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->d:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    .line 1979930
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1979931
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1979932
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1979933
    const/4 v0, 0x0

    .line 1979934
    :goto_0
    move-object v3, v0

    .line 1979935
    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    const v5, 0x3ff745d1

    invoke-virtual {v3, v5}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    check-cast v2, LX/1Pp;

    invoke-virtual {v3, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    const/4 p0, 0x0

    invoke-interface {v4, v5, p0}, LX/1Dh;->y(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    .line 1979936
    const v5, 0x5dfd276b

    const/4 p0, 0x0

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1979937
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    const p0, 0x7f0214b3

    invoke-virtual {v5, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v5, p0}, LX/1Di;->b(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1979938
    return-object v0

    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1979910
    invoke-static {}, LX/1dS;->b()V

    .line 1979911
    iget v0, p1, LX/1dQ;->b:I

    .line 1979912
    packed-switch v0, :pswitch_data_0

    .line 1979913
    :goto_0
    return-object v2

    .line 1979914
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1979915
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1979916
    check-cast v1, LX/DGU;

    .line 1979917
    iget-object v3, p0, LX/DGV;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;

    iget-object v4, v1, LX/DGU;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/DGU;->b:LX/1Pd;

    .line 1979918
    iget-object p2, v3, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->b:LX/2mY;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p0

    sget-object v1, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p2, v4, p0, v1}, LX/2mY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0am;Lcom/facebook/common/callercontext/CallerContext;)LX/C2F;

    move-result-object p2

    .line 1979919
    iget-object p0, v3, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->c:LX/2mZ;

    .line 1979920
    iget-object v1, p2, LX/C2F;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v1, v1

    .line 1979921
    invoke-virtual {p0, v4, v1}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object p0

    .line 1979922
    new-instance v1, LX/C2G;

    invoke-direct {v1, p2, p0}, LX/C2G;-><init>(LX/C2F;LX/3Im;)V

    .line 1979923
    iget-object p0, v1, LX/C2G;->a:LX/C2F;

    move-object p2, p1

    check-cast p2, LX/1Po;

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object p2

    invoke-static {p2}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object p2

    check-cast p1, LX/1Pt;

    invoke-virtual {p0, p2, p1}, LX/C2F;->a(LX/04D;LX/1Pt;)V

    .line 1979924
    iget-object p2, v3, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->e:LX/2mX;

    iget-object p0, v1, LX/C2G;->a:LX/C2F;

    invoke-virtual {p2, p0}, LX/2mX;->a(LX/C2F;)LX/CD3;

    move-result-object p2

    .line 1979925
    invoke-virtual {p2, v0}, LX/CD3;->a(Landroid/view/View;)V

    .line 1979926
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5dfd276b
        :pswitch_0
    .end packed-switch
.end method
