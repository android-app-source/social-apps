.class public LX/DKo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ResultType:",
        "Ljava/lang/Object;",
        "NodeType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/DKg;

.field public final b:LX/DKg;

.field private final c:LX/0Sh;

.field private final d:LX/0Sg;

.field public e:LX/DKm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DKm",
            "<TResultType;TNodeType;>;"
        }
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TNodeType;>;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Z

.field private j:J

.field private k:I


# direct methods
.method public constructor <init>(LX/0Sh;LX/0So;LX/0ad;LX/0Sg;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1987546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1987547
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1987548
    iput-object v0, p0, LX/DKo;->f:LX/0Px;

    .line 1987549
    iput-object v5, p0, LX/DKo;->g:Ljava/lang/String;

    .line 1987550
    iput-boolean v2, p0, LX/DKo;->h:Z

    .line 1987551
    iput-boolean v2, p0, LX/DKo;->i:Z

    .line 1987552
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, LX/DKo;->j:J

    .line 1987553
    iput v2, p0, LX/DKo;->k:I

    .line 1987554
    iput-object p1, p0, LX/DKo;->c:LX/0Sh;

    .line 1987555
    iput-object p4, p0, LX/DKo;->d:LX/0Sg;

    .line 1987556
    sget-short v0, LX/88j;->e:S

    invoke-interface {p3, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v5, p4

    .line 1987557
    :cond_0
    new-instance v0, LX/DKg;

    new-instance v1, LX/DKh;

    invoke-direct {v1, p0}, LX/DKh;-><init>(LX/DKo;)V

    new-instance v2, LX/DKi;

    invoke-direct {v2, p0}, LX/DKi;-><init>(LX/DKo;)V

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/DKg;-><init>(Ljava/util/concurrent/Callable;LX/0TF;LX/0So;LX/0Sh;LX/0Sg;)V

    iput-object v0, p0, LX/DKo;->a:LX/DKg;

    .line 1987558
    new-instance v0, LX/DKg;

    new-instance v1, LX/DKj;

    invoke-direct {v1, p0}, LX/DKj;-><init>(LX/DKo;)V

    new-instance v2, LX/DKk;

    invoke-direct {v2, p0}, LX/DKk;-><init>(LX/DKo;)V

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/DKg;-><init>(Ljava/util/concurrent/Callable;LX/0TF;LX/0So;LX/0Sh;LX/0Sg;)V

    iput-object v0, p0, LX/DKo;->b:LX/DKg;

    .line 1987559
    return-void
.end method

.method public static a$redex0(LX/DKo;LX/DKg;)V
    .locals 4

    .prologue
    .line 1987543
    iget v0, p0, LX/DKo;->k:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/DKo;->k:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 1987544
    iget-wide v0, p0, LX/DKo;->j:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/DKo;->j:J

    invoke-virtual {p1, v0, v1}, LX/DKg;->a(J)V

    .line 1987545
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/DKo;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 1987537
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1987538
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 1987539
    instance-of v1, v0, Lcom/facebook/graphql/error/GraphQLError;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/graphql/error/GraphQLError;

    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    const v1, 0x198f03

    if-ne v0, v1, :cond_0

    .line 1987540
    invoke-static {p0}, LX/DKo;->e(LX/DKo;)V

    .line 1987541
    invoke-virtual {p0}, LX/DKo;->a()V

    .line 1987542
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/DKo;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TResultType;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1987497
    if-nez p1, :cond_1

    .line 1987498
    iput-object v1, p0, LX/DKo;->g:Ljava/lang/String;

    .line 1987499
    :cond_0
    :goto_0
    return-void

    .line 1987500
    :cond_1
    iget-object v0, p0, LX/DKo;->e:LX/DKm;

    invoke-interface {v0, p1}, LX/DKm;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/DKn;

    move-result-object v0

    .line 1987501
    if-nez v0, :cond_2

    .line 1987502
    iput-object v1, p0, LX/DKo;->g:Ljava/lang/String;

    goto :goto_0

    .line 1987503
    :cond_2
    iget-object v1, v0, LX/DKn;->c:Ljava/lang/String;

    iput-object v1, p0, LX/DKo;->g:Ljava/lang/String;

    .line 1987504
    iget-boolean v1, v0, LX/DKn;->b:Z

    iput-boolean v1, p0, LX/DKo;->h:Z

    .line 1987505
    iget-object v1, v0, LX/DKn;->a:LX/0Px;

    if-eqz v1, :cond_0

    .line 1987506
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    iget-object v2, p0, LX/DKo;->f:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    iget-object v0, v0, LX/DKn;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/DKo;->f:LX/0Px;

    .line 1987507
    iget-object v0, p0, LX/DKo;->e:LX/DKm;

    iget-object v1, p0, LX/DKo;->f:LX/0Px;

    iget-boolean v2, p0, LX/DKo;->h:Z

    invoke-interface {v0, v1, v2}, LX/DKm;->a(LX/0Px;Z)V

    .line 1987508
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/DKo;
    .locals 5

    .prologue
    .line 1987535
    new-instance v4, LX/DKo;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v3

    check-cast v3, LX/0Sg;

    invoke-direct {v4, v0, v1, v2, v3}, LX/DKo;-><init>(LX/0Sh;LX/0So;LX/0ad;LX/0Sg;)V

    .line 1987536
    return-object v4
.end method

.method public static d$redex0(LX/DKo;)V
    .locals 2

    .prologue
    .line 1987532
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, LX/DKo;->j:J

    .line 1987533
    const/4 v0, 0x0

    iput v0, p0, LX/DKo;->k:I

    .line 1987534
    return-void
.end method

.method private static e(LX/DKo;)V
    .locals 1

    .prologue
    .line 1987524
    iget-object v0, p0, LX/DKo;->a:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->b()V

    .line 1987525
    iget-object v0, p0, LX/DKo;->b:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->b()V

    .line 1987526
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DKo;->h:Z

    .line 1987527
    const/4 v0, 0x0

    iput-object v0, p0, LX/DKo;->g:Ljava/lang/String;

    .line 1987528
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1987529
    iput-object v0, p0, LX/DKo;->f:LX/0Px;

    .line 1987530
    invoke-static {p0}, LX/DKo;->d$redex0(LX/DKo;)V

    .line 1987531
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1987521
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/DKo;->a:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1987522
    monitor-exit p0

    return-void

    .line 1987523
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/DKm;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DKm",
            "<TResultType;TNodeType;>;)V"
        }
    .end annotation

    .prologue
    .line 1987515
    iput-object p1, p0, LX/DKo;->e:LX/DKm;

    .line 1987516
    iget-boolean v0, p0, LX/DKo;->h:Z

    if-nez v0, :cond_0

    .line 1987517
    iget-object v0, p0, LX/DKo;->e:LX/DKm;

    const/4 v1, 0x0

    sget-object v2, LX/0zS;->b:LX/0zS;

    invoke-interface {v0, v1, v2}, LX/DKm;->a(Ljava/lang/String;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1987518
    iget-object v1, p0, LX/DKo;->c:LX/0Sh;

    new-instance v2, LX/DKl;

    invoke-direct {v2, p0}, LX/DKl;-><init>(LX/DKo;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1987519
    iget-object v1, p0, LX/DKo;->d:LX/0Sg;

    invoke-virtual {v1, v0}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1987520
    :cond_0
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 1987512
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/DKo;->b:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1987513
    monitor-exit p0

    return-void

    .line 1987514
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 1987509
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/DKo;->e(LX/DKo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1987510
    monitor-exit p0

    return-void

    .line 1987511
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
