.class public LX/Dxl;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2063216
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2063217
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2063215
    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2063212
    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2063213
    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2063214
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2063218
    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2063219
    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2063220
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2063211
    sget-object v0, LX/Dxk;->CONTRIBUTOR:LX/Dxk;

    invoke-virtual {v0}, LX/Dxk;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2063195
    if-nez p2, :cond_1

    .line 2063196
    new-instance p2, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorView;-><init>(Landroid/content/Context;)V

    .line 2063197
    :goto_0
    iget-object v0, p0, LX/Dxl;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2063198
    if-eqz v0, :cond_2

    const/4 p0, 0x1

    :goto_1
    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 2063199
    iput-object v0, p2, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorView;->b:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2063200
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 2063201
    const p0, 0x7f0d0b65

    invoke-virtual {p2, p0}, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorView;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2063202
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p1

    sget-object p3, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2063203
    :cond_0
    const p0, 0x7f0d0b66

    invoke-virtual {p2, p0}, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorView;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/resources/ui/FbTextView;

    .line 2063204
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 2063205
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2063206
    :goto_2
    new-instance p0, LX/Dxj;

    invoke-direct {p0, p2}, LX/Dxj;-><init>(Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorView;)V

    invoke-virtual {p2, p0}, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2063207
    return-object p2

    .line 2063208
    :cond_1
    check-cast p2, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorView;

    goto :goto_0

    .line 2063209
    :cond_2
    const/4 p0, 0x0

    goto :goto_1

    .line 2063210
    :cond_3
    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2063194
    invoke-static {}, LX/Dxk;->values()[LX/Dxk;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
