.class public LX/Ckj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/03V;

.field public final c:LX/0tX;

.field public final d:LX/1Ck;

.field public final e:LX/CkM;

.field public final f:LX/0hB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1931263
    const-class v0, LX/Ckj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ckj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0tX;LX/1Ck;LX/CkM;LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1931264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931265
    iput-object p1, p0, LX/Ckj;->b:LX/03V;

    .line 1931266
    iput-object p2, p0, LX/Ckj;->c:LX/0tX;

    .line 1931267
    iput-object p3, p0, LX/Ckj;->d:LX/1Ck;

    .line 1931268
    iput-object p4, p0, LX/Ckj;->e:LX/CkM;

    .line 1931269
    iput-object p5, p0, LX/Ckj;->f:LX/0hB;

    .line 1931270
    return-void
.end method

.method public static a(LX/0QB;)LX/Ckj;
    .locals 9

    .prologue
    .line 1931271
    const-class v1, LX/Ckj;

    monitor-enter v1

    .line 1931272
    :try_start_0
    sget-object v0, LX/Ckj;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1931273
    sput-object v2, LX/Ckj;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1931274
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931275
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1931276
    new-instance v3, LX/Ckj;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/CkM;->a(LX/0QB;)LX/CkM;

    move-result-object v7

    check-cast v7, LX/CkM;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v8

    check-cast v8, LX/0hB;

    invoke-direct/range {v3 .. v8}, LX/Ckj;-><init>(LX/03V;LX/0tX;LX/1Ck;LX/CkM;LX/0hB;)V

    .line 1931277
    move-object v0, v3

    .line 1931278
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1931279
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ckj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1931280
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1931281
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
