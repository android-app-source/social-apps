.class public LX/Dyx;
.super LX/Dyu;
.source ""


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field public final b:LX/96B;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/content/Context;LX/96B;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066076
    invoke-direct {p0}, LX/Dyu;-><init>()V

    .line 2066077
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dyx;->c:Z

    .line 2066078
    const/4 v0, 0x0

    iput-object v0, p0, LX/Dyx;->d:Ljava/lang/String;

    .line 2066079
    iput-object p1, p0, LX/Dyx;->a:Landroid/view/LayoutInflater;

    .line 2066080
    iput-object p2, p0, LX/Dyx;->e:Landroid/content/Context;

    .line 2066081
    iput-object p3, p0, LX/Dyx;->b:LX/96B;

    .line 2066082
    return-void
.end method


# virtual methods
.method public final a()LX/9jL;
    .locals 1

    .prologue
    .line 2066113
    sget-object v0, LX/9jL;->AddPlace:LX/9jL;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2066088
    if-nez p1, :cond_0

    .line 2066089
    iget-object v0, p0, LX/Dyx;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030099

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 2066090
    new-instance v1, LX/Dyw;

    invoke-direct {v1}, LX/Dyw;-><init>()V

    .line 2066091
    const v0, 0x7f0d04a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LX/Dyw;->a:Landroid/widget/TextView;

    .line 2066092
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2066093
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dyw;

    .line 2066094
    iget-object v0, v0, LX/Dyw;->a:Landroid/widget/TextView;

    .line 2066095
    iget-object v1, p0, LX/Dyx;->b:LX/96B;

    const-string v2, "android_place_picker_add_button"

    .line 2066096
    iget-object v3, v1, LX/96B;->a:LX/0Zb;

    const-string v4, "entry_point_impression"

    invoke-static {v1, v2, v4}, LX/96B;->a(LX/96B;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2066097
    iget-object v1, p0, LX/Dyx;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0816bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    .line 2066098
    array-length v3, v2

    new-array v5, v3, [Ljava/lang/String;

    move v3, v4

    .line 2066099
    :goto_0
    array-length p2, v2

    if-ge v3, p2, :cond_1

    .line 2066100
    const-string p2, "###magic%d###"

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, p3, v4

    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v5, v3

    .line 2066101
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2066102
    :cond_1
    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2066103
    new-instance p2, Landroid/text/SpannableStringBuilder;

    invoke-direct {p2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2066104
    :goto_1
    array-length v3, v2

    if-ge v4, v3, :cond_2

    .line 2066105
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aget-object p3, v5, v4

    invoke-virtual {v3, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 2066106
    aget-object p3, v5, v4

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p3

    add-int/2addr p3, v3

    aget-object p0, v2, v4

    invoke-virtual {p2, v3, p3, p0}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2066107
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2066108
    :cond_2
    move-object v1, p2

    .line 2066109
    move-object v1, v1

    .line 2066110
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2066111
    return-object p1
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 2066112
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2066087
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2066083
    iget-object v0, p0, LX/Dyx;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/Dyx;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2066084
    if-eqz v0, :cond_0

    .line 2066085
    new-instance v0, Landroid/util/Pair;

    sget-object v1, LX/9jL;->AddPlace:LX/9jL;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2066086
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
