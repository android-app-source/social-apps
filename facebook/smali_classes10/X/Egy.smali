.class public LX/Egy;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Egx;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2157468
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Egy;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2157469
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2157470
    iput-object p1, p0, LX/Egy;->b:LX/0Ot;

    .line 2157471
    return-void
.end method

.method public static a(LX/0QB;)LX/Egy;
    .locals 4

    .prologue
    .line 2157472
    const-class v1, LX/Egy;

    monitor-enter v1

    .line 2157473
    :try_start_0
    sget-object v0, LX/Egy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2157474
    sput-object v2, LX/Egy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2157475
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2157476
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2157477
    new-instance v3, LX/Egy;

    const/16 p0, 0x1811

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Egy;-><init>(LX/0Ot;)V

    .line 2157478
    move-object v0, v3

    .line 2157479
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2157480
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Egy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2157481
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2157482
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 12

    .prologue
    .line 2157483
    check-cast p2, LX/Egw;

    .line 2157484
    iget-object v0, p0, LX/Egy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;

    iget-object v1, p2, LX/Egw;->a:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    .line 2157485
    invoke-virtual {v1}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;->b()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2157486
    invoke-virtual {v1}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;->lv_()Ljava/lang/String;

    move-result-object v11

    .line 2157487
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Landroid/app/Activity;

    invoke-static {v4, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    iget-object v5, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0ad;

    iget-object v6, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/ComponentName;

    iget-object v7, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->e:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/ComponentName;

    iget-object v8, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->g:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0Uh;

    iget-object v9, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->i:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/17X;

    iget-object v10, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->h:LX/0Ot;

    invoke-interface {v10}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/2lA;

    invoke-static/range {v2 .. v10}, LX/FBl;->a(JLandroid/app/Activity;LX/0ad;Landroid/content/ComponentName;Landroid/content/ComponentName;LX/0Uh;LX/17X;LX/2lA;)Landroid/content/Intent;

    move-result-object v4

    .line 2157488
    if-nez v4, :cond_3

    .line 2157489
    iget-object v4, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/17X;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v2, v3, v11}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2157490
    :goto_0
    if-nez v2, :cond_2

    .line 2157491
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object v3, v2

    .line 2157492
    :goto_1
    invoke-static {v11}, LX/2l9;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2157493
    if-eqz v11, :cond_0

    .line 2157494
    const-string v2, "extra_launch_uri"

    invoke-virtual {v3, v2, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2157495
    :cond_0
    iget-object v2, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2157496
    :goto_2
    return-void

    .line 2157497
    :cond_1
    iget-object v2, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2

    :cond_2
    move-object v3, v2

    goto :goto_1

    :cond_3
    move-object v2, v4

    goto :goto_0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2157498
    const v0, 0x1deb06fd

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2157499
    check-cast p2, LX/Egw;

    .line 2157500
    iget-object v0, p0, LX/Egy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;

    iget-object v1, p2, LX/Egw;->a:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    .line 2157501
    invoke-virtual {v1}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;->b()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;

    move-result-object v3

    .line 2157502
    invoke-virtual {v3}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 2157503
    invoke-virtual {v3}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;->lv_()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2157504
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [No URL]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2157505
    :cond_0
    iget-object v3, v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->c:LX/EtW;

    invoke-virtual {v3, p1}, LX/EtW;->c(LX/1De;)LX/EtV;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/EtV;->b(Ljava/lang/String;)LX/EtV;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;->d()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$ImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/EtV;->a(Landroid/net/Uri;)LX/EtV;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    .line 2157506
    :goto_0
    iget-object v4, v3, LX/EtV;->a:Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;

    iput-object v2, v4, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->n:LX/1X1;

    .line 2157507
    move-object v2, v3

    .line 2157508
    sget-object v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/EtV;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/EtV;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 2157509
    const v3, 0x1deb06fd

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2157510
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const v3, 0x7f020201

    invoke-interface {v2, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b088b

    invoke-interface {v2, v3}, LX/1Di;->t(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    const v4, 0x7f0b0082

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b0082

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2157511
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 2157512
    const/4 v4, 0x0

    const/16 v5, 0xb

    invoke-static {v5}, LX/6WK;->b(I)I

    move-result v5

    invoke-static {p1, v4, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    move-object v2, v4

    .line 2157513
    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2157514
    invoke-static {}, LX/1dS;->b()V

    .line 2157515
    iget v0, p1, LX/1dQ;->b:I

    .line 2157516
    packed-switch v0, :pswitch_data_0

    .line 2157517
    :goto_0
    return-object v2

    .line 2157518
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2157519
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/Egy;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1deb06fd
        :pswitch_0
    .end packed-switch
.end method
