.class public LX/E4R;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E4R;


# instance fields
.field private final a:LX/3mF;

.field private final b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/3mF;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2076655
    iput-object p1, p0, LX/E4R;->a:LX/3mF;

    .line 2076656
    iput-object p2, p0, LX/E4R;->b:Ljava/util/concurrent/ExecutorService;

    .line 2076657
    return-void
.end method

.method public static a(LX/0QB;)LX/E4R;
    .locals 5

    .prologue
    .line 2076658
    sget-object v0, LX/E4R;->c:LX/E4R;

    if-nez v0, :cond_1

    .line 2076659
    const-class v1, LX/E4R;

    monitor-enter v1

    .line 2076660
    :try_start_0
    sget-object v0, LX/E4R;->c:LX/E4R;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2076661
    if-eqz v2, :cond_0

    .line 2076662
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2076663
    new-instance p0, LX/E4R;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v3

    check-cast v3, LX/3mF;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4}, LX/E4R;-><init>(LX/3mF;Ljava/util/concurrent/ExecutorService;)V

    .line 2076664
    move-object v0, p0

    .line 2076665
    sput-object v0, LX/E4R;->c:LX/E4R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076666
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2076667
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2076668
    :cond_1
    sget-object v0, LX/E4R;->c:LX/E4R;

    return-object v0

    .line 2076669
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2076670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1Pq;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/E2S;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 9
    .param p1    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/E2S;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "LX/E2S;",
            "Lcom/facebook/feed/rows/core/props/FeedProps;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2076671
    iget-object v0, p3, LX/E2S;->a:LX/03R;

    move-object v0, v0

    .line 2076672
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_1

    move v3, v6

    .line 2076673
    :goto_0
    if-nez v3, :cond_2

    move v0, v6

    .line 2076674
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2076675
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v3, v7

    .line 2076676
    goto :goto_0

    :cond_2
    move v0, v7

    .line 2076677
    goto :goto_1

    .line 2076678
    :cond_3
    if-eqz v0, :cond_4

    iget-object v0, p0, LX/E4R;->a:LX/3mF;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "pages_profile_groups_tab"

    invoke-virtual {v0, v1, v2}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v8, v0

    .line 2076679
    :goto_3
    if-eqz v3, :cond_5

    sget-object v0, LX/03R;->NO:LX/03R;

    :goto_4
    invoke-virtual {p3, v0}, LX/E2S;->a(LX/03R;)V

    .line 2076680
    new-instance v0, LX/E4Q;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/E4Q;-><init>(LX/E4R;LX/E2S;ZLX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iget-object v1, p0, LX/E4R;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v8, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2076681
    new-array v0, v6, [Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object p4, v0, v7

    invoke-interface {p1, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_2

    .line 2076682
    :cond_4
    iget-object v0, p0, LX/E4R;->a:LX/3mF;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "pages_profile_groups_tab"

    const-string v4, "ALLOW_READD"

    invoke-virtual {v0, v1, v2, v4}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v8, v0

    goto :goto_3

    .line 2076683
    :cond_5
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_4
.end method
