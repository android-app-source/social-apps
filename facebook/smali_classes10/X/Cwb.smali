.class public final enum LX/Cwb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cwb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cwb;

.field public static final enum BLENDED:LX/Cwb;

.field public static final enum DEFAULT:LX/Cwb;

.field public static final enum ENTITY:LX/Cwb;

.field public static final enum KEYWORD:LX/Cwb;

.field public static final enum NEARBY:LX/Cwb;

.field public static final enum NO_GROUP:LX/Cwb;

.field public static final enum NS_INTERESTED:LX/Cwb;

.field public static final enum NS_PULSE:LX/Cwb;

.field public static final enum NS_SEARCH_SPOTLIGHT:LX/Cwb;

.field public static final enum NS_SOCIAL:LX/Cwb;

.field public static final enum NS_SUGGESTED:LX/Cwb;

.field public static final enum NS_TOP:LX/Cwb;

.field public static final enum PLACE_TIP:LX/Cwb;

.field public static final enum PYMK:LX/Cwb;

.field public static final enum RECENT:LX/Cwb;

.field public static final enum RECENT_VIDEOS:LX/Cwb;

.field public static final enum TRENDING:LX/Cwb;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1950885
    new-instance v0, LX/Cwb;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->DEFAULT:LX/Cwb;

    .line 1950886
    new-instance v0, LX/Cwb;

    const-string v1, "NO_GROUP"

    invoke-direct {v0, v1, v4}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->NO_GROUP:LX/Cwb;

    .line 1950887
    new-instance v0, LX/Cwb;

    const-string v1, "RECENT"

    invoke-direct {v0, v1, v5}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->RECENT:LX/Cwb;

    .line 1950888
    new-instance v0, LX/Cwb;

    const-string v1, "RECENT_VIDEOS"

    invoke-direct {v0, v1, v6}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->RECENT_VIDEOS:LX/Cwb;

    .line 1950889
    new-instance v0, LX/Cwb;

    const-string v1, "TRENDING"

    invoke-direct {v0, v1, v7}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->TRENDING:LX/Cwb;

    .line 1950890
    new-instance v0, LX/Cwb;

    const-string v1, "PYMK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->PYMK:LX/Cwb;

    .line 1950891
    new-instance v0, LX/Cwb;

    const-string v1, "KEYWORD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->KEYWORD:LX/Cwb;

    .line 1950892
    new-instance v0, LX/Cwb;

    const-string v1, "ENTITY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->ENTITY:LX/Cwb;

    .line 1950893
    new-instance v0, LX/Cwb;

    const-string v1, "BLENDED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->BLENDED:LX/Cwb;

    .line 1950894
    new-instance v0, LX/Cwb;

    const-string v1, "PLACE_TIP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->PLACE_TIP:LX/Cwb;

    .line 1950895
    new-instance v0, LX/Cwb;

    const-string v1, "NEARBY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->NEARBY:LX/Cwb;

    .line 1950896
    new-instance v0, LX/Cwb;

    const-string v1, "NS_PULSE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->NS_PULSE:LX/Cwb;

    .line 1950897
    new-instance v0, LX/Cwb;

    const-string v1, "NS_INTERESTED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->NS_INTERESTED:LX/Cwb;

    .line 1950898
    new-instance v0, LX/Cwb;

    const-string v1, "NS_SOCIAL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->NS_SOCIAL:LX/Cwb;

    .line 1950899
    new-instance v0, LX/Cwb;

    const-string v1, "NS_SUGGESTED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->NS_SUGGESTED:LX/Cwb;

    .line 1950900
    new-instance v0, LX/Cwb;

    const-string v1, "NS_TOP"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->NS_TOP:LX/Cwb;

    .line 1950901
    new-instance v0, LX/Cwb;

    const-string v1, "NS_SEARCH_SPOTLIGHT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/Cwb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwb;->NS_SEARCH_SPOTLIGHT:LX/Cwb;

    .line 1950902
    const/16 v0, 0x11

    new-array v0, v0, [LX/Cwb;

    sget-object v1, LX/Cwb;->DEFAULT:LX/Cwb;

    aput-object v1, v0, v3

    sget-object v1, LX/Cwb;->NO_GROUP:LX/Cwb;

    aput-object v1, v0, v4

    sget-object v1, LX/Cwb;->RECENT:LX/Cwb;

    aput-object v1, v0, v5

    sget-object v1, LX/Cwb;->RECENT_VIDEOS:LX/Cwb;

    aput-object v1, v0, v6

    sget-object v1, LX/Cwb;->TRENDING:LX/Cwb;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Cwb;->PYMK:LX/Cwb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Cwb;->KEYWORD:LX/Cwb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Cwb;->ENTITY:LX/Cwb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Cwb;->BLENDED:LX/Cwb;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Cwb;->PLACE_TIP:LX/Cwb;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Cwb;->NEARBY:LX/Cwb;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Cwb;->NS_PULSE:LX/Cwb;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Cwb;->NS_INTERESTED:LX/Cwb;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Cwb;->NS_SOCIAL:LX/Cwb;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Cwb;->NS_SUGGESTED:LX/Cwb;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Cwb;->NS_TOP:LX/Cwb;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Cwb;->NS_SEARCH_SPOTLIGHT:LX/Cwb;

    aput-object v2, v0, v1

    sput-object v0, LX/Cwb;->$VALUES:[LX/Cwb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1950903
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cwb;
    .locals 1

    .prologue
    .line 1950904
    const-class v0, LX/Cwb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cwb;

    return-object v0
.end method

.method public static values()[LX/Cwb;
    .locals 1

    .prologue
    .line 1950905
    sget-object v0, LX/Cwb;->$VALUES:[LX/Cwb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cwb;

    return-object v0
.end method
