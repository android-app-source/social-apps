.class public final LX/DTB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/DTZ;


# direct methods
.method public constructor <init>(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2000250
    iput-object p1, p0, LX/DTB;->d:LX/DTZ;

    iput-object p2, p0, LX/DTB;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DTB;->b:Landroid/content/Context;

    iput-object p4, p0, LX/DTB;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    .line 2000251
    new-instance v0, LX/DTA;

    invoke-direct {v0, p0}, LX/DTA;-><init>(LX/DTB;)V

    .line 2000252
    iget-object v1, p0, LX/DTB;->d:LX/DTZ;

    iget-object v2, p0, LX/DTB;->b:Landroid/content/Context;

    iget-object v3, p0, LX/DTB;->a:Ljava/lang/String;

    iget-object v4, p0, LX/DTB;->c:Ljava/lang/String;

    const/4 p1, 0x1

    .line 2000253
    iget-object v5, v1, LX/DTZ;->b:Landroid/content/res/Resources;

    const v6, 0x7f082f98

    new-array v7, p1, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v4, v7, p0

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2000254
    iget-object v6, v1, LX/DTZ;->a:Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2000255
    iget-object v5, v1, LX/DTZ;->i:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    if-ne v5, p1, :cond_1

    iget-object v5, v1, LX/DTZ;->b:Landroid/content/res/Resources;

    const v6, 0x7f082f9a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2000256
    :cond_0
    :goto_0
    new-instance v6, LX/0ju;

    invoke-direct {v6, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2000257
    iget-object v7, v1, LX/DTZ;->b:Landroid/content/res/Resources;

    const p0, 0x7f082f96

    invoke-virtual {v7, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000258
    invoke-virtual {v6, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000259
    iget-object v5, v1, LX/DTZ;->b:Landroid/content/res/Resources;

    const v7, 0x7f082f97

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5, v0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000260
    iget-object v5, v1, LX/DTZ;->b:Landroid/content/res/Resources;

    const v7, 0x7f082f93

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v7, LX/DTE;

    invoke-direct {v7, v1}, LX/DTE;-><init>(LX/DTZ;)V

    invoke-virtual {v6, v5, v7}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000261
    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v5

    invoke-virtual {v5}, LX/2EJ;->show()V

    .line 2000262
    const/4 v0, 0x1

    return v0

    .line 2000263
    :cond_1
    iget-object v5, v1, LX/DTZ;->b:Landroid/content/res/Resources;

    const v6, 0x7f082f99

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method
