.class public LX/D5D;
.super LX/9Bh;
.source ""


# instance fields
.field public final a:LX/1Aa;

.field private final b:LX/7zh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7zh",
            "<",
            "LX/D5z;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/D5J;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:LX/D5z;

.field private h:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

.field public i:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/0zw;LX/D5J;LX/7za;LX/0wW;LX/4mV;)V
    .locals 2
    .param p1    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/D5J;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;",
            "Lcom/facebook/video/channelfeed/ChannelFeedMoreVideosPillController$ViewCallback;",
            "LX/7za;",
            "LX/0wW;",
            "Lcom/facebook/ui/animations/ViewAnimatorFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963427
    invoke-direct {p0, p4, p5}, LX/9Bh;-><init>(LX/0wW;LX/4mV;)V

    .line 1963428
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zw;

    iput-object v0, p0, LX/D5D;->c:LX/0zw;

    .line 1963429
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D5J;

    iput-object v0, p0, LX/D5D;->d:LX/D5J;

    .line 1963430
    iget-object v0, p3, LX/7za;->a:LX/1Aa;

    move-object v0, v0

    .line 1963431
    iput-object v0, p0, LX/D5D;->a:LX/1Aa;

    .line 1963432
    new-instance v0, LX/D5B;

    invoke-direct {v0, p0}, LX/D5B;-><init>(LX/D5D;)V

    iput-object v0, p0, LX/D5D;->b:LX/7zh;

    .line 1963433
    iget-object v0, p0, LX/D5D;->a:LX/1Aa;

    iget-object v1, p0, LX/D5D;->b:LX/7zh;

    invoke-virtual {v0, v1}, LX/1Aa;->a(LX/7zh;)V

    .line 1963434
    return-void
.end method

.method public static h(LX/D5D;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1963435
    iget-object v0, p0, LX/D5D;->a:LX/1Aa;

    iget-object v1, p0, LX/D5D;->b:LX/7zh;

    invoke-virtual {v0, v1}, LX/1Aa;->b(LX/7zh;)V

    .line 1963436
    iget-object v0, p0, LX/D5D;->g:LX/D5z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D5D;->h:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    if-eqz v0, :cond_0

    .line 1963437
    iget-object v0, p0, LX/D5D;->g:LX/D5z;

    invoke-virtual {v0}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iget-object v1, p0, LX/D5D;->h:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;)V

    .line 1963438
    iput-object v2, p0, LX/D5D;->h:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    .line 1963439
    iput-object v2, p0, LX/D5D;->g:LX/D5z;

    .line 1963440
    :cond_0
    invoke-virtual {p0}, LX/9Bh;->b()V

    .line 1963441
    return-void
.end method

.method public static i(LX/D5D;)V
    .locals 3

    .prologue
    .line 1963442
    iget-boolean v0, p0, LX/D5D;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D5D;->g:LX/D5z;

    if-nez v0, :cond_1

    .line 1963443
    :cond_0
    :goto_0
    return-void

    .line 1963444
    :cond_1
    new-instance v0, Lcom/facebook/video/channelfeed/ChannelFeedMoreVideosPillController$2;

    const v1, 0x3f333333    # 0.7f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/video/channelfeed/ChannelFeedMoreVideosPillController$2;-><init>(LX/D5D;FF)V

    iput-object v0, p0, LX/D5D;->h:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    .line 1963445
    iget-object v0, p0, LX/D5D;->g:LX/D5z;

    invoke-virtual {v0}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iget-object v1, p0, LX/D5D;->h:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;)V

    goto :goto_0
.end method


# virtual methods
.method public final f()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1963446
    iget-object v0, p0, LX/D5D;->c:LX/0zw;

    return-object v0
.end method
