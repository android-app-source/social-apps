.class public final enum LX/ECN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ECN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ECN;

.field public static final enum CONFERENCE_JOIN:LX/ECN;

.field public static final enum CONFERENCE_LEAVE:LX/ECN;

.field public static final enum CONNECT:LX/ECN;

.field public static final enum DISCONNECT:LX/ECN;

.field public static final enum DROPPED_CALL:LX/ECN;

.field public static final enum END_CALL:LX/ECN;

.field public static final enum LOBBY_JOIN:LX/ECN;

.field public static final enum LOW_BATTERY:LX/ECN;

.field public static final enum RINGBACK:LX/ECN;

.field public static final enum SEARCHING:LX/ECN;

.field public static final enum VIDEO_ON:LX/ECN;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2089613
    new-instance v0, LX/ECN;

    const-string v1, "RINGBACK"

    invoke-direct {v0, v1, v3}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->RINGBACK:LX/ECN;

    .line 2089614
    new-instance v0, LX/ECN;

    const-string v1, "END_CALL"

    invoke-direct {v0, v1, v4}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->END_CALL:LX/ECN;

    .line 2089615
    new-instance v0, LX/ECN;

    const-string v1, "LOW_BATTERY"

    invoke-direct {v0, v1, v5}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->LOW_BATTERY:LX/ECN;

    .line 2089616
    new-instance v0, LX/ECN;

    const-string v1, "VIDEO_ON"

    invoke-direct {v0, v1, v6}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->VIDEO_ON:LX/ECN;

    .line 2089617
    new-instance v0, LX/ECN;

    const-string v1, "CONNECT"

    invoke-direct {v0, v1, v7}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->CONNECT:LX/ECN;

    .line 2089618
    new-instance v0, LX/ECN;

    const-string v1, "DISCONNECT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->DISCONNECT:LX/ECN;

    .line 2089619
    new-instance v0, LX/ECN;

    const-string v1, "SEARCHING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->SEARCHING:LX/ECN;

    .line 2089620
    new-instance v0, LX/ECN;

    const-string v1, "DROPPED_CALL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->DROPPED_CALL:LX/ECN;

    .line 2089621
    new-instance v0, LX/ECN;

    const-string v1, "CONFERENCE_JOIN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->CONFERENCE_JOIN:LX/ECN;

    .line 2089622
    new-instance v0, LX/ECN;

    const-string v1, "CONFERENCE_LEAVE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->CONFERENCE_LEAVE:LX/ECN;

    .line 2089623
    new-instance v0, LX/ECN;

    const-string v1, "LOBBY_JOIN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/ECN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECN;->LOBBY_JOIN:LX/ECN;

    .line 2089624
    const/16 v0, 0xb

    new-array v0, v0, [LX/ECN;

    sget-object v1, LX/ECN;->RINGBACK:LX/ECN;

    aput-object v1, v0, v3

    sget-object v1, LX/ECN;->END_CALL:LX/ECN;

    aput-object v1, v0, v4

    sget-object v1, LX/ECN;->LOW_BATTERY:LX/ECN;

    aput-object v1, v0, v5

    sget-object v1, LX/ECN;->VIDEO_ON:LX/ECN;

    aput-object v1, v0, v6

    sget-object v1, LX/ECN;->CONNECT:LX/ECN;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/ECN;->DISCONNECT:LX/ECN;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/ECN;->SEARCHING:LX/ECN;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/ECN;->DROPPED_CALL:LX/ECN;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/ECN;->CONFERENCE_JOIN:LX/ECN;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/ECN;->CONFERENCE_LEAVE:LX/ECN;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/ECN;->LOBBY_JOIN:LX/ECN;

    aput-object v2, v0, v1

    sput-object v0, LX/ECN;->$VALUES:[LX/ECN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2089612
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ECN;
    .locals 1

    .prologue
    .line 2089610
    const-class v0, LX/ECN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ECN;

    return-object v0
.end method

.method public static values()[LX/ECN;
    .locals 1

    .prologue
    .line 2089611
    sget-object v0, LX/ECN;->$VALUES:[LX/ECN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ECN;

    return-object v0
.end method
