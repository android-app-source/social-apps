.class public LX/CqA;
.super LX/Cod;
.source ""

# interfaces
.implements LX/02k;
.implements LX/CnG;
.implements LX/CnI;
.implements LX/CnJ;
.implements LX/Cq9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/CoB;",
        ">;",
        "LX/02k;",
        "Lcom/facebook/richdocument/view/block/WebViewBlockView;",
        "LX/Cq9;"
    }
.end annotation


# static fields
.field public static final C:Ljava/lang/String;

.field private static final D:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final E:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final F:Z


# instance fields
.field public A:LX/8bZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/Cig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final G:F

.field private final H:Z

.field private final I:Z

.field private final J:Z

.field public final K:Z

.field private final L:[I

.field public final M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final N:LX/ChN;

.field private final O:LX/Ci3;

.field private final P:LX/CoO;

.field private final Q:LX/CoO;

.field private R:Z

.field public S:I

.field public T:J

.field private U:J

.field public V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field private W:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

.field public X:Z

.field public Y:Ljava/lang/String;

.field public Z:Z

.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:LX/CsX;

.field private ab:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public ac:Lcom/facebook/fbui/glyph/GlyphView;

.field public ad:LX/CpO;

.field private ae:Lcom/facebook/widget/CustomLinearLayout;

.field private af:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

.field private ag:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

.field private ah:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

.field public ai:Landroid/os/Bundle;

.field public aj:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ak:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public al:Z

.field public am:Z

.field public an:Z

.field public ao:Z

.field public ap:Z

.field public final aq:Z

.field public b:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/17T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Cta;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/ClK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/ClH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Ckv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/Cs3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/CjD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Ciy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/1m0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/CoM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/CoR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/CoV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/8bO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/CqH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1939347
    const-class v0, LX/CqA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CqA;->C:Ljava/lang/String;

    .line 1939348
    new-instance v0, LX/Cpt;

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-direct {v0, v1}, LX/Cpt;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/CqA;->D:Ljava/util/EnumMap;

    .line 1939349
    new-instance v0, LX/Cpu;

    const-class v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-direct {v0, v1}, LX/Cpu;-><init>(Ljava/lang/Class;)V

    sput-object v0, LX/CqA;->E:Ljava/util/EnumMap;

    .line 1939350
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1939351
    sput-boolean v0, LX/CqA;->F:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/widget/FrameLayout;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1939352
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1939353
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/CqA;->L:[I

    .line 1939354
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CqA;->M:Ljava/util/List;

    .line 1939355
    new-instance v0, LX/Cpv;

    invoke-direct {v0, p0}, LX/Cpv;-><init>(LX/CqA;)V

    iput-object v0, p0, LX/CqA;->N:LX/ChN;

    .line 1939356
    new-instance v0, LX/Cpw;

    invoke-direct {v0, p0}, LX/Cpw;-><init>(LX/CqA;)V

    iput-object v0, p0, LX/CqA;->O:LX/Ci3;

    .line 1939357
    new-instance v0, LX/Cpx;

    invoke-direct {v0, p0}, LX/Cpx;-><init>(LX/CqA;)V

    iput-object v0, p0, LX/CqA;->P:LX/CoO;

    .line 1939358
    new-instance v0, LX/Cpy;

    invoke-direct {v0, p0}, LX/Cpy;-><init>(LX/CqA;)V

    iput-object v0, p0, LX/CqA;->Q:LX/CoO;

    .line 1939359
    iput v2, p0, LX/CqA;->S:I

    .line 1939360
    iput-wide v4, p0, LX/CqA;->T:J

    .line 1939361
    iput-wide v4, p0, LX/CqA;->U:J

    .line 1939362
    iput-boolean v2, p0, LX/CqA;->Z:Z

    .line 1939363
    iput-boolean v2, p0, LX/CqA;->al:Z

    .line 1939364
    iput-boolean v2, p0, LX/CqA;->am:Z

    .line 1939365
    iput-boolean v2, p0, LX/CqA;->ao:Z

    .line 1939366
    iput-boolean v2, p0, LX/CqA;->ap:Z

    .line 1939367
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/CqA;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1939368
    iget-object v0, p0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0x3d7

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1939369
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqA;->H:Z

    .line 1939370
    :goto_0
    iget-object v0, p0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0x3d6

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CqA;->al:Z

    .line 1939371
    iget-object v0, p0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0xae

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CqA;->I:Z

    .line 1939372
    iget-object v0, p0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0xc4

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CqA;->J:Z

    .line 1939373
    iget-object v0, p0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0xa7

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CqA;->K:Z

    .line 1939374
    iget-boolean v0, p0, LX/CqA;->H:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CqA;->q:LX/Ciy;

    invoke-virtual {v0}, LX/Ciy;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1939375
    :cond_0
    invoke-static {p0}, LX/CqA;->k(LX/CqA;)V

    .line 1939376
    :goto_1
    sget-boolean v0, LX/CqA;->F:Z

    if-eqz v0, :cond_1

    .line 1939377
    iget-object v0, p0, LX/CqA;->p:LX/Chv;

    iget-object v1, p0, LX/CqA;->O:LX/Ci3;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1939378
    :cond_1
    iget-object v0, p0, LX/CqA;->x:LX/8bO;

    .line 1939379
    iget-object v1, v0, LX/8bO;->a:LX/0ad;

    sget v4, LX/2yD;->w:F

    const v5, 0x3f2aaaab

    invoke-interface {v1, v4, v5}, LX/0ad;->a(FF)F

    move-result v1

    move v0, v1

    .line 1939380
    iput v0, p0, LX/CqA;->G:F

    .line 1939381
    iget-object v0, p0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0xc6

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CqA;->aq:Z

    .line 1939382
    new-instance v0, LX/Cmz;

    new-instance v1, LX/Cn4;

    iget-object v2, p0, LX/CqA;->l:LX/Cju;

    invoke-direct {v1, v2}, LX/Cn4;-><init>(LX/Cju;)V

    invoke-direct {v0, v1, v3, v3, v3}, LX/Cmz;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V

    .line 1939383
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1939384
    return-void

    .line 1939385
    :cond_2
    iget-object v0, p0, LX/CqA;->r:LX/0ad;

    sget-short v1, LX/2yD;->H:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CqA;->H:Z

    goto :goto_0

    .line 1939386
    :cond_3
    iget-object v0, p0, LX/CqA;->p:LX/Chv;

    iget-object v1, p0, LX/CqA;->N:LX/ChN;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1939387
    iget-object v0, p0, LX/CqA;->M:Ljava/util/List;

    new-instance v1, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$7;

    invoke-direct {v1, p0}, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$7;-><init>(LX/CqA;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static a(Landroid/view/View;)LX/CqA;
    .locals 1

    .prologue
    .line 1939388
    check-cast p0, Landroid/widget/FrameLayout;

    .line 1939389
    new-instance v0, LX/CqA;

    invoke-direct {v0, p0}, LX/CqA;-><init>(Landroid/widget/FrameLayout;)V

    return-object v0
.end method

.method private static a(LX/CqA;LX/CsX;)V
    .locals 3

    .prologue
    .line 1939390
    iget-object v0, p0, LX/CqA;->m:LX/Cs3;

    invoke-virtual {v0, p1}, LX/Cs3;->a(Landroid/webkit/WebView;)V

    .line 1939391
    new-instance v0, LX/Cq4;

    invoke-direct {v0, p0}, LX/Cq4;-><init>(LX/CqA;)V

    .line 1939392
    invoke-virtual {p1, v0}, LX/CsX;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1939393
    new-instance v1, LX/Cq1;

    invoke-direct {v1, p0}, LX/Cq1;-><init>(LX/CqA;)V

    invoke-virtual {p1, v1}, LX/CsX;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 1939394
    invoke-virtual {p1, v0}, LX/CsX;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1939395
    return-void
.end method

.method private static a(LX/CqA;Lcom/facebook/content/SecureContextHelper;LX/0So;LX/03V;LX/17W;LX/17T;LX/Ckw;LX/Ck0;LX/Cta;LX/ClK;LX/ClH;LX/Ckv;LX/Cju;LX/Cs3;LX/0Uh;LX/CjD;LX/Chv;LX/Ciy;LX/0ad;LX/Crz;LX/1m0;LX/CoM;LX/CoR;LX/CoV;LX/8bO;LX/CqH;LX/0hB;LX/8bZ;LX/Cig;)V
    .locals 1

    .prologue
    .line 1939396
    iput-object p1, p0, LX/CqA;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, LX/CqA;->b:LX/0So;

    iput-object p3, p0, LX/CqA;->c:LX/03V;

    iput-object p4, p0, LX/CqA;->d:LX/17W;

    iput-object p5, p0, LX/CqA;->e:LX/17T;

    iput-object p6, p0, LX/CqA;->f:LX/Ckw;

    iput-object p7, p0, LX/CqA;->g:LX/Ck0;

    iput-object p8, p0, LX/CqA;->h:LX/Cta;

    iput-object p9, p0, LX/CqA;->i:LX/ClK;

    iput-object p10, p0, LX/CqA;->j:LX/ClH;

    iput-object p11, p0, LX/CqA;->k:LX/Ckv;

    iput-object p12, p0, LX/CqA;->l:LX/Cju;

    iput-object p13, p0, LX/CqA;->m:LX/Cs3;

    iput-object p14, p0, LX/CqA;->n:LX/0Uh;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/CqA;->o:LX/CjD;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/CqA;->p:LX/Chv;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/CqA;->q:LX/Ciy;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/CqA;->r:LX/0ad;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/CqA;->s:LX/Crz;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/CqA;->t:LX/1m0;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/CqA;->u:LX/CoM;

    move-object/from16 v0, p22

    iput-object v0, p0, LX/CqA;->v:LX/CoR;

    move-object/from16 v0, p23

    iput-object v0, p0, LX/CqA;->w:LX/CoV;

    move-object/from16 v0, p24

    iput-object v0, p0, LX/CqA;->x:LX/8bO;

    move-object/from16 v0, p25

    iput-object v0, p0, LX/CqA;->y:LX/CqH;

    move-object/from16 v0, p26

    iput-object v0, p0, LX/CqA;->z:LX/0hB;

    move-object/from16 v0, p27

    iput-object v0, p0, LX/CqA;->A:LX/8bZ;

    move-object/from16 v0, p28

    iput-object v0, p0, LX/CqA;->B:LX/Cig;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 29

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v28

    move-object/from16 v0, p0

    check-cast v0, LX/CqA;

    invoke-static/range {v28 .. v28}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v28 .. v28}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-static/range {v28 .. v28}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static/range {v28 .. v28}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static/range {v28 .. v28}, LX/17T;->a(LX/0QB;)LX/17T;

    move-result-object v5

    check-cast v5, LX/17T;

    invoke-static/range {v28 .. v28}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v6

    check-cast v6, LX/Ckw;

    invoke-static/range {v28 .. v28}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v7

    check-cast v7, LX/Ck0;

    invoke-static/range {v28 .. v28}, LX/Cta;->a(LX/0QB;)LX/Cta;

    move-result-object v8

    check-cast v8, LX/Cta;

    invoke-static/range {v28 .. v28}, LX/ClK;->a(LX/0QB;)LX/ClK;

    move-result-object v9

    check-cast v9, LX/ClK;

    invoke-static/range {v28 .. v28}, LX/ClH;->a(LX/0QB;)LX/ClH;

    move-result-object v10

    check-cast v10, LX/ClH;

    invoke-static/range {v28 .. v28}, LX/Ckv;->a(LX/0QB;)LX/Ckv;

    move-result-object v11

    check-cast v11, LX/Ckv;

    invoke-static/range {v28 .. v28}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v12

    check-cast v12, LX/Cju;

    invoke-static/range {v28 .. v28}, LX/Cs3;->a(LX/0QB;)LX/Cs3;

    move-result-object v13

    check-cast v13, LX/Cs3;

    invoke-static/range {v28 .. v28}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v14

    check-cast v14, LX/0Uh;

    invoke-static/range {v28 .. v28}, LX/CjD;->a(LX/0QB;)LX/CjD;

    move-result-object v15

    check-cast v15, LX/CjD;

    invoke-static/range {v28 .. v28}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v16

    check-cast v16, LX/Chv;

    invoke-static/range {v28 .. v28}, LX/Ciy;->a(LX/0QB;)LX/Ciy;

    move-result-object v17

    check-cast v17, LX/Ciy;

    invoke-static/range {v28 .. v28}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v18

    check-cast v18, LX/0ad;

    invoke-static/range {v28 .. v28}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v19

    check-cast v19, LX/Crz;

    invoke-static/range {v28 .. v28}, LX/1m0;->a(LX/0QB;)LX/1m0;

    move-result-object v20

    check-cast v20, LX/1m0;

    invoke-static/range {v28 .. v28}, LX/CoM;->a(LX/0QB;)LX/CoM;

    move-result-object v21

    check-cast v21, LX/CoM;

    invoke-static/range {v28 .. v28}, LX/CoR;->a(LX/0QB;)LX/CoR;

    move-result-object v22

    check-cast v22, LX/CoR;

    invoke-static/range {v28 .. v28}, LX/CoV;->a(LX/0QB;)LX/CoV;

    move-result-object v23

    check-cast v23, LX/CoV;

    invoke-static/range {v28 .. v28}, LX/8bO;->a(LX/0QB;)LX/8bO;

    move-result-object v24

    check-cast v24, LX/8bO;

    invoke-static/range {v28 .. v28}, LX/CqH;->a(LX/0QB;)LX/CqH;

    move-result-object v25

    check-cast v25, LX/CqH;

    invoke-static/range {v28 .. v28}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v26

    check-cast v26, LX/0hB;

    invoke-static/range {v28 .. v28}, LX/8bZ;->a(LX/0QB;)LX/8bZ;

    move-result-object v27

    check-cast v27, LX/8bZ;

    invoke-static/range {v28 .. v28}, LX/Cig;->a(LX/0QB;)LX/Cig;

    move-result-object v28

    check-cast v28, LX/Cig;

    invoke-static/range {v0 .. v28}, LX/CqA;->a(LX/CqA;Lcom/facebook/content/SecureContextHelper;LX/0So;LX/03V;LX/17W;LX/17T;LX/Ckw;LX/Ck0;LX/Cta;LX/ClK;LX/ClH;LX/Ckv;LX/Cju;LX/Cs3;LX/0Uh;LX/CjD;LX/Chv;LX/Ciy;LX/0ad;LX/Crz;LX/1m0;LX/CoM;LX/CoR;LX/CoV;LX/8bO;LX/CqH;LX/0hB;LX/8bZ;LX/Cig;)V

    return-void
.end method

.method public static a$redex0(LX/CqA;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1939149
    if-nez p1, :cond_1

    .line 1939150
    :cond_0
    :goto_0
    return-void

    .line 1939151
    :cond_1
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1939152
    invoke-static {v0}, LX/1H1;->f(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1939153
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1939154
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1939155
    const-string v0, "com.android.browser.headers"

    invoke-static {}, LX/Cs3;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1939156
    const-string v0, "android.intent.category.BROWSABLE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1939157
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1939158
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setSelector(Landroid/content/Intent;)V

    .line 1939159
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1939160
    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 1939161
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1939162
    iget-object v1, p0, LX/CqA;->e:LX/17T;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/17T;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1939163
    :catch_0
    move-exception v0

    .line 1939164
    iget-object v1, p0, LX/CqA;->c:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/CqA;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_startActivityForUrl"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error trying to launch url:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 1939165
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1939166
    move-object v0, v2

    .line 1939167
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto/16 :goto_0

    .line 1939168
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/CqA;->B:LX/Cig;

    new-instance v2, LX/Cin;

    invoke-direct {v2}, LX/Cin;-><init>()V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1939169
    iget-object v0, p0, LX/CqA;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1939170
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1939171
    const-string v1, "webview_type"

    iget-object v2, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939172
    iget-object v1, p0, LX/CqA;->f:LX/Ckw;

    invoke-virtual {v1, p1, v0}, LX/Ckw;->b(Ljava/lang/String;Ljava/util/Map;)V

    .line 1939173
    iget-object v0, p0, LX/CqA;->f:LX/Ckw;

    const-string v1, "WEBVIEW"

    invoke-virtual {v0, p1, v1}, LX/Ckw;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1939174
    iget-object v0, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v1, :cond_3

    .line 1939175
    iget-object v0, p0, LX/CqA;->m:LX/Cs3;

    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    .line 1939176
    if-nez v1, :cond_4

    .line 1939177
    :cond_3
    :goto_1
    invoke-static {p0}, LX/CqA;->q(LX/CqA;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1939178
    :cond_4
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1939179
    if-eqz v1, :cond_5

    instance-of v2, v1, LX/CsX;

    if-eqz v2, :cond_5

    move-object v2, v1

    .line 1939180
    check-cast v2, LX/CsX;

    invoke-virtual {v2}, LX/CsX;->getWebViewHorizontalScrollRange()I

    move-result v2

    .line 1939181
    check-cast v1, LX/CsX;

    invoke-virtual {v1}, LX/CsX;->getWebViewVerticalScrollRange()I

    move-result v4

    .line 1939182
    if-lez v2, :cond_5

    if-lez v4, :cond_5

    .line 1939183
    const-string v5, "ad_width"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939184
    const-string v5, "ad_height"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939185
    const-string v5, "ad_aspect_ratio"

    int-to-float v2, v2

    int-to-float v4, v4

    div-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v3, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939186
    :cond_5
    iget-object v2, v0, LX/Cs3;->i:LX/Ckw;

    const-string v4, "WEBVIEW"

    .line 1939187
    if-nez v3, :cond_6

    .line 1939188
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1939189
    :cond_6
    const-string v5, "interaction"

    const-string v6, "ad_tap"

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939190
    const-string v5, "webview_URL"

    invoke-interface {v3, v5, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939191
    const-string v5, "ia_source"

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939192
    const-string v5, "android_native_article_block_interaction"

    invoke-virtual {v2, v5, v3}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 1939193
    goto :goto_1
.end method

.method public static b(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1939397
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 1939398
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 1939399
    :goto_0
    return-void

    .line 1939400
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "javascript: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static k(LX/CqA;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1939401
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d16d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    .line 1939402
    iget-object v0, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16d6

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/CqA;->ab:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1939403
    iget-object v0, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16d7

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/CqA;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1939404
    iget-object v0, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16d8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    iput-object v0, p0, LX/CqA;->af:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    .line 1939405
    iget-object v0, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16da

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    iput-object v0, p0, LX/CqA;->ag:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    .line 1939406
    iget-object v0, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16db

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    iput-object v0, p0, LX/CqA;->ah:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    .line 1939407
    iget-object v0, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16d9

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/FrameLayout;

    .line 1939408
    iget-object v0, p0, LX/CqA;->s:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1939409
    iget-object v0, p0, LX/CqA;->ab:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1939410
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1939411
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    .line 1939412
    :goto_0
    iget-object v0, p0, LX/CqA;->g:LX/Ck0;

    iget-object v1, p0, LX/CqA;->ab:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v5, 0x7f0d011b

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1939413
    iget-object v0, p0, LX/CqA;->y:LX/CqH;

    invoke-virtual {v0}, LX/CqH;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1939414
    iget-object v0, p0, LX/CqA;->y:LX/CqH;

    .line 1939415
    invoke-virtual {v0}, LX/CqH;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1939416
    const/4 v1, 0x0

    .line 1939417
    :goto_1
    move-object v0, v1

    .line 1939418
    iput-object v0, p0, LX/CqA;->aa:LX/CsX;

    .line 1939419
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->onPause()V

    .line 1939420
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->onResume()V

    .line 1939421
    :goto_2
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    iget-object v1, p0, LX/CqA;->f:LX/Ckw;

    .line 1939422
    iput-object v1, v0, LX/CsW;->c:LX/Ckw;

    .line 1939423
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0, v2}, LX/CsX;->setBackgroundColor(I)V

    .line 1939424
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 1939425
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 1939426
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    .line 1939427
    :cond_0
    iput-boolean v2, p0, LX/CqA;->am:Z

    .line 1939428
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v1, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1939429
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v6, v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1939430
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqA;->Z:Z

    .line 1939431
    return-void

    .line 1939432
    :cond_1
    iget-object v0, p0, LX/CqA;->ab:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1939433
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1939434
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    goto :goto_0

    .line 1939435
    :cond_2
    new-instance v0, LX/CsX;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CsX;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/CqA;->aa:LX/CsX;

    goto :goto_2

    .line 1939436
    :cond_3
    iget-object v1, v0, LX/CqH;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1939437
    new-instance v1, LX/CsX;

    iget-object v3, v0, LX/CqH;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, LX/CsX;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 1939438
    :cond_4
    iget-object v1, v0, LX/CqH;->a:Ljava/util/List;

    iget-object v3, v0, LX/CqH;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CsX;

    goto :goto_1
.end method

.method private static l(LX/CqA;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1939439
    iget-object v0, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v1, :cond_1

    .line 1939440
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a061f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1939441
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-static {v1, v0}, LX/8ba;->a(Landroid/view/View;I)V

    .line 1939442
    iget-object v0, p0, LX/CqA;->ab:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1939443
    iget-object v0, p0, LX/CqA;->ab:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1939444
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1939445
    const v1, 0x7f081c59

    invoke-virtual {v0, v1}, LX/CtG;->setText(I)V

    .line 1939446
    iget-object v0, p0, LX/CqA;->n:LX/0Uh;

    const/16 v1, 0x50a

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939447
    iget-object v0, p0, LX/CqA;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1939448
    iget-object v0, p0, LX/CqA;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Cpz;

    invoke-direct {v1, p0}, LX/Cpz;-><init>(LX/CqA;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1939449
    iget-object v0, p0, LX/CqA;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-static {v0, v3}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v1

    .line 1939450
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d16d5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 1939451
    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 1939452
    :cond_0
    :goto_0
    return-void

    .line 1939453
    :cond_1
    iget-object v0, p0, LX/CqA;->ab:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1939454
    iget-object v0, p0, LX/CqA;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 1939455
    sget-boolean v0, LX/CqA;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/CqA;->R:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/CqA;->Z:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/CqA;->am:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, LX/CqA;->J:Z

    if-eqz v0, :cond_1

    .line 1939456
    :cond_0
    const/4 v0, 0x0

    .line 1939457
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static q(LX/CqA;)V
    .locals 1

    .prologue
    .line 1939458
    invoke-direct {p0}, LX/CqA;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1939459
    :goto_0
    return-void

    .line 1939460
    :cond_0
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->onPause()V

    goto :goto_0
.end method

.method public static r(LX/CqA;)V
    .locals 1

    .prologue
    .line 1939344
    invoke-direct {p0}, LX/CqA;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1939345
    :goto_0
    return-void

    .line 1939346
    :cond_0
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->onResume()V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 1939461
    invoke-super {p0, p1}, LX/Cod;->a(LX/1a1;)V

    .line 1939462
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/1a1;->a(Z)V

    .line 1939463
    return-void
.end method

.method public final a(LX/CnQ;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1939137
    invoke-interface {p1}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v0

    .line 1939138
    iget-object v1, v0, LX/ClU;->e:LX/ClR;

    move-object v0, v1

    .line 1939139
    sget-object v1, LX/ClR;->TOP:LX/ClR;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/ClR;->ABOVE:LX/ClR;

    if-ne v0, v1, :cond_2

    .line 1939140
    :cond_0
    iget-object v0, p0, LX/CqA;->af:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->setVisibility(I)V

    .line 1939141
    iget-object v0, p0, LX/CqA;->af:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a(LX/CnQ;)V

    .line 1939142
    :cond_1
    :goto_0
    return-void

    .line 1939143
    :cond_2
    sget-object v1, LX/ClR;->CENTER:LX/ClR;

    if-ne v0, v1, :cond_3

    .line 1939144
    iget-object v0, p0, LX/CqA;->ag:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->setVisibility(I)V

    .line 1939145
    iget-object v0, p0, LX/CqA;->ag:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a(LX/CnQ;)V

    goto :goto_0

    .line 1939146
    :cond_3
    sget-object v1, LX/ClR;->BELOW:LX/ClR;

    if-eq v0, v1, :cond_4

    sget-object v1, LX/ClR;->BOTTOM:LX/ClR;

    if-ne v0, v1, :cond_1

    .line 1939147
    :cond_4
    iget-object v0, p0, LX/CqA;->ah:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->setVisibility(I)V

    .line 1939148
    iget-object v0, p0, LX/CqA;->ah:Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a(LX/CnQ;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1939194
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1939195
    iget-object v0, p0, LX/CqA;->ab:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a()V

    .line 1939196
    iput-boolean v1, p0, LX/CqA;->am:Z

    .line 1939197
    iput-boolean v1, p0, LX/CqA;->ap:Z

    .line 1939198
    return-void
.end method

.method public final a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;)V
    .locals 0

    .prologue
    .line 1939199
    return-void
.end method

.method public final a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)V
    .locals 1

    .prologue
    .line 1939200
    iget-object v0, p0, LX/CqA;->w:LX/CoV;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    invoke-virtual {v0, p0, p1, p2, p3}, LX/CoV;->a(LX/Cq9;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)V

    .line 1939201
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILX/CtY;Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 1939202
    iget-boolean v0, p0, LX/CqA;->R:Z

    if-eqz v0, :cond_0

    .line 1939203
    :goto_0
    return-void

    .line 1939204
    :cond_0
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-static {p0, v0}, LX/CqA;->a(LX/CqA;LX/CsX;)V

    .line 1939205
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    .line 1939206
    iput-object p1, v0, LX/CsW;->a:Ljava/lang/String;

    .line 1939207
    iput-object p1, p0, LX/CqA;->Y:Ljava/lang/String;

    .line 1939208
    iput-object p5, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 1939209
    iput-object p6, p0, LX/CqA;->W:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 1939210
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqA;->R:Z

    .line 1939211
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1939212
    if-nez v0, :cond_1

    .line 1939213
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1939214
    :cond_1
    if-lez p3, :cond_2

    .line 1939215
    iput p3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1939216
    :goto_1
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v1, v0}, LX/CsX;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1939217
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {p0}, LX/CqA;->g()F

    move-result v1

    invoke-virtual {v0, v1}, LX/CsX;->setFallbackAspectRatio(F)V

    .line 1939218
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->clearHistory()V

    .line 1939219
    iget-object v0, p0, LX/CqA;->h:LX/Cta;

    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    .line 1939220
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1939221
    :goto_2
    invoke-static {p0}, LX/CqA;->l(LX/CqA;)V

    goto :goto_0

    .line 1939222
    :cond_2
    iget v1, p0, LX/CqA;->S:I

    if-lez v1, :cond_3

    .line 1939223
    iget v1, p0, LX/CqA;->S:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1

    .line 1939224
    :cond_3
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1

    .line 1939225
    :cond_4
    iget-object v2, v0, LX/Cta;->c:LX/ClK;

    .line 1939226
    iget-object p3, v1, LX/CsW;->c:LX/Ckw;

    move-object p3, p3

    .line 1939227
    invoke-virtual {v2, p3, p1}, LX/ClK;->a(LX/Ckw;Ljava/lang/String;)V

    .line 1939228
    new-instance v2, LX/CtX;

    invoke-direct {v2, v1, p1, p2, p4}, LX/CtX;-><init>(LX/CsX;Ljava/lang/String;Ljava/lang/String;LX/CtY;)V

    .line 1939229
    invoke-static {v0, v2}, LX/Cta;->a$redex0(LX/Cta;LX/CtX;)V

    .line 1939230
    iget-object p3, v0, LX/Cta;->a:Ljava/util/Queue;

    invoke-interface {p3, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1939231
    invoke-static {v0}, LX/Cta;->d(LX/Cta;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/CtY;Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V
    .locals 6

    .prologue
    const/4 v3, -0x2

    const/4 v0, 0x0

    .line 1939232
    iget-boolean v1, p0, LX/CqA;->R:Z

    if-eqz v1, :cond_0

    .line 1939233
    :goto_0
    return-void

    .line 1939234
    :cond_0
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-static {p0, v1}, LX/CqA;->a(LX/CqA;LX/CsX;)V

    .line 1939235
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    .line 1939236
    iput-object p1, v1, LX/CsW;->a:Ljava/lang/String;

    .line 1939237
    iput-object p1, p0, LX/CqA;->Y:Ljava/lang/String;

    .line 1939238
    iput-object p6, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 1939239
    iput-object p7, p0, LX/CqA;->W:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 1939240
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/CqA;->R:Z

    .line 1939241
    iget-boolean v1, p0, LX/CqA;->al:Z

    if-eqz v1, :cond_1

    .line 1939242
    invoke-virtual {p0}, LX/CqA;->g()F

    move-result v1

    .line 1939243
    cmpg-float v2, v1, v0

    if-gez v2, :cond_5

    .line 1939244
    :cond_1
    :goto_1
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v1, v0}, LX/CsX;->setFallbackAspectRatio(F)V

    .line 1939245
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1939246
    if-nez v0, :cond_2

    .line 1939247
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1939248
    :cond_2
    if-lez p4, :cond_3

    .line 1939249
    iput p4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1939250
    :goto_2
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v1, v0}, LX/CsX;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1939251
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->clearHistory()V

    .line 1939252
    iget-object v0, p0, LX/CqA;->h:LX/Cta;

    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/Cta;->a(LX/CsX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CtY;)V

    .line 1939253
    invoke-static {p0}, LX/CqA;->l(LX/CqA;)V

    goto :goto_0

    .line 1939254
    :cond_3
    iget v1, p0, LX/CqA;->S:I

    if-lez v1, :cond_4

    .line 1939255
    iget v1, p0, LX/CqA;->S:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_2

    .line 1939256
    :cond_4
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1939257
    iput-boolean p1, p0, LX/CqA;->X:Z

    .line 1939258
    return-void
.end method

.method public final a(II)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1939259
    iget-boolean v2, p0, LX/CqA;->ap:Z

    if-eqz v2, :cond_1

    .line 1939260
    :cond_0
    :goto_0
    return v0

    .line 1939261
    :cond_1
    iget-boolean v2, p0, LX/CqA;->I:Z

    if-eqz v2, :cond_2

    .line 1939262
    iget-object v2, p0, LX/CqA;->aa:LX/CsX;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, LX/CsX;->canScrollHorizontally(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v2, v0}, LX/CsX;->canScrollHorizontally(I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1939263
    :cond_2
    iget-object v2, p0, LX/CqA;->aa:LX/CsX;

    iget-object v3, p0, LX/CqA;->L:[I

    invoke-virtual {v2, v3}, LX/CsX;->getLocationOnScreen([I)V

    .line 1939264
    iget-object v2, p0, LX/CqA;->L:[I

    aget v2, v2, v1

    if-lt p1, v2, :cond_3

    iget-object v2, p0, LX/CqA;->L:[I

    aget v2, v2, v1

    iget-object v3, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v3}, LX/CsX;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    if-gt p1, v2, :cond_3

    iget-object v2, p0, LX/CqA;->L:[I

    aget v2, v2, v0

    if-lt p2, v2, :cond_3

    iget-object v2, p0, LX/CqA;->L:[I

    aget v2, v2, v0

    iget-object v3, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v3}, LX/CsX;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    if-le p2, v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b(II)V
    .locals 5

    .prologue
    .line 1939265
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1939266
    :cond_0
    :goto_0
    return-void

    .line 1939267
    :cond_1
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1939268
    if-nez v0, :cond_2

    .line 1939269
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1939270
    :cond_2
    iget-object v1, p0, LX/CqA;->A:LX/8bZ;

    invoke-virtual {v1}, LX/8bZ;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/CqA;->A:LX/8bZ;

    invoke-virtual {v1}, LX/8bZ;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1939271
    iget-object v1, p0, LX/CqA;->A:LX/8bZ;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v3, LX/8bY;->PIXEL:LX/8bY;

    invoke-virtual {v1, v2, v3}, LX/8bZ;->a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;LX/8bY;)I

    move-result v1

    .line 1939272
    :goto_1
    mul-int v2, v1, p1

    div-int/2addr v2, p2

    .line 1939273
    mul-int/lit8 v3, v2, 0x64

    div-int/2addr v3, p1

    .line 1939274
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1939275
    iget-boolean v1, p0, LX/CqA;->aq:Z

    if-nez v1, :cond_3

    .line 1939276
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1939277
    :cond_3
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v1, v0}, LX/CsX;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1939278
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0, v3}, LX/CsX;->setInitialScale(I)V

    goto :goto_0

    .line 1939279
    :cond_4
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 1939280
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1939281
    iget-object v2, p0, LX/CqA;->l:LX/Cju;

    const v3, 0x7f0d0121

    invoke-interface {v2, v3}, LX/Cju;->c(I)I

    move-result v2

    .line 1939282
    iget-object v3, p0, LX/CqA;->l:LX/Cju;

    const v4, 0x7f0d0122

    invoke-interface {v3, v4}, LX/Cju;->c(I)I

    move-result v3

    .line 1939283
    sub-int/2addr v1, v2

    sub-int/2addr v1, v3

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    .line 1939284
    invoke-super {p0, p1}, LX/Cod;->b(Landroid/os/Bundle;)V

    .line 1939285
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqA;->an:Z

    .line 1939286
    iput-object p1, p0, LX/CqA;->ai:Landroid/os/Bundle;

    .line 1939287
    iget-object v0, p0, LX/CqA;->i:LX/ClK;

    iget-object v1, p0, LX/CqA;->f:LX/Ckw;

    iget-object v2, p0, LX/CqA;->Y:Ljava/lang/String;

    .line 1939288
    iget-object v8, v0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map;

    .line 1939289
    if-eqz v8, :cond_0

    invoke-interface {v8, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 1939290
    :cond_0
    :goto_0
    iget-object v0, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v1, :cond_1

    .line 1939291
    iget-object v0, p0, LX/CqA;->ad:LX/CpO;

    if-eqz v0, :cond_4

    .line 1939292
    iget-wide v0, p0, LX/CqA;->T:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3

    .line 1939293
    iget-object v0, p0, LX/CqA;->ad:LX/CpO;

    iget-wide v2, p0, LX/CqA;->T:J

    invoke-virtual {v0, v2, v3}, LX/CpO;->a(J)V

    .line 1939294
    :goto_1
    iget-object v0, p0, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v0, p1}, LX/Cod;->b(Landroid/os/Bundle;)V

    .line 1939295
    :cond_1
    :goto_2
    invoke-static {p0}, LX/CqA;->r(LX/CqA;)V

    .line 1939296
    iget-boolean v0, p0, LX/CqA;->aq:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v1, :cond_2

    .line 1939297
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->getWebViewHorizontalScrollRange()I

    move-result v0

    .line 1939298
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v1}, LX/CsX;->getWebViewVerticalScrollRange()I

    move-result v1

    .line 1939299
    if-lez v1, :cond_2

    div-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/CqA;->G:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1939300
    iget-object v0, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v0}, LX/CsX;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1939301
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v1}, LX/CsX;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, LX/CqA;->G:F

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1939302
    iget-object v1, p0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v1, v0}, LX/CsX;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1939303
    :cond_2
    return-void

    .line 1939304
    :cond_3
    iget-object v0, p0, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->p()V

    goto :goto_1

    .line 1939305
    :cond_4
    iget-object v0, p0, LX/CqA;->v:LX/CoR;

    iget-object v1, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v2, LX/CoQ;

    sget-object v3, LX/CoP;->PERCENTAGE:LX/CoP;

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4}, LX/CoQ;-><init>(LX/CoP;I)V

    iget-object v3, p0, LX/CqA;->P:LX/CoO;

    invoke-virtual {v0, v1, v2, v3}, LX/CoR;->a(Landroid/view/View;LX/CoQ;LX/CoO;)V

    .line 1939306
    iget-object v0, p0, LX/CqA;->v:LX/CoR;

    iget-object v1, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v2, LX/CoQ;

    sget-object v3, LX/CoP;->PERCENTAGE:LX/CoP;

    const/16 v4, 0x14

    invoke-direct {v2, v3, v4}, LX/CoQ;-><init>(LX/CoP;I)V

    iget-object v3, p0, LX/CqA;->Q:LX/CoO;

    invoke-virtual {v0, v1, v2, v3}, LX/CoR;->a(Landroid/view/View;LX/CoQ;LX/CoO;)V

    .line 1939307
    iget-wide v0, p0, LX/CqA;->T:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_1

    .line 1939308
    iget-object v0, p0, LX/CqA;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/CqA;->T:J

    goto :goto_2

    .line 1939309
    :cond_5
    invoke-interface {v8, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/ClJ;

    .line 1939310
    iget-wide v10, v8, LX/ClJ;->h:J

    long-to-float v9, v10

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_0

    .line 1939311
    iget-object v9, v0, LX/ClK;->b:LX/0So;

    invoke-interface {v9}, LX/0So;->now()J

    move-result-wide v10

    iput-wide v10, v8, LX/ClJ;->h:J

    goto/16 :goto_0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    .line 1939312
    invoke-super {p0, p1}, LX/Cod;->c(Landroid/os/Bundle;)V

    .line 1939313
    iget-object v0, p0, LX/CqA;->i:LX/ClK;

    iget-object v1, p0, LX/CqA;->f:LX/Ckw;

    iget-object v2, p0, LX/CqA;->Y:Ljava/lang/String;

    .line 1939314
    iget-object v6, v0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 1939315
    if-eqz v6, :cond_0

    invoke-interface {v6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1939316
    :cond_0
    :goto_0
    iget-object v0, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v1, :cond_2

    .line 1939317
    iget-object v0, p0, LX/CqA;->ad:LX/CpO;

    if-eqz v0, :cond_4

    .line 1939318
    iget-wide v0, p0, LX/CqA;->U:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 1939319
    iget-object v0, p0, LX/CqA;->ad:LX/CpO;

    iget-wide v2, p0, LX/CqA;->U:J

    .line 1939320
    iget-object v6, v0, LX/CpO;->i:LX/Ckv;

    iget-object v7, v0, LX/CpO;->y:Ljava/lang/String;

    .line 1939321
    invoke-static {v7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    iget-object v9, v6, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 1939322
    :cond_1
    :goto_1
    iget-object v0, p0, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v0, p1}, LX/Cod;->c(Landroid/os/Bundle;)V

    .line 1939323
    :cond_2
    :goto_2
    invoke-static {p0}, LX/CqA;->q(LX/CqA;)V

    .line 1939324
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CqA;->an:Z

    .line 1939325
    return-void

    .line 1939326
    :cond_3
    iget-object v0, p0, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->q()V

    goto :goto_1

    .line 1939327
    :cond_4
    iget-object v0, p0, LX/CqA;->v:LX/CoR;

    iget-object v1, p0, LX/CqA;->ae:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1}, LX/CoR;->a(Landroid/view/View;)V

    .line 1939328
    iget-wide v0, p0, LX/CqA;->U:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_2

    .line 1939329
    iget-object v0, p0, LX/CqA;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/CqA;->U:J

    goto :goto_2

    .line 1939330
    :cond_5
    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/ClJ;

    .line 1939331
    iget-wide v8, v6, LX/ClJ;->i:J

    long-to-float v7, v8

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-gtz v7, :cond_0

    .line 1939332
    iget-object v7, v0, LX/ClK;->b:LX/0So;

    invoke-interface {v7}, LX/0So;->now()J

    move-result-wide v8

    iput-wide v8, v6, LX/ClJ;->i:J

    goto :goto_0

    .line 1939333
    :cond_6
    iget-object v9, v6, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/Cku;

    .line 1939334
    iget-wide v11, v9, LX/Cku;->g:J

    long-to-float v10, v11

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-gtz v10, :cond_1

    .line 1939335
    iput-wide v2, v9, LX/Cku;->g:J

    goto :goto_1
.end method

.method public final g()F
    .locals 2

    .prologue
    .line 1939336
    sget-object v0, LX/CqA;->D:Ljava/util/EnumMap;

    iget-object v1, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939337
    sget-object v0, LX/CqA;->D:Ljava/util/EnumMap;

    iget-object v1, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1939338
    :goto_0
    return v0

    .line 1939339
    :cond_0
    iget-boolean v0, p0, LX/CqA;->al:Z

    if-eqz v0, :cond_1

    .line 1939340
    sget-object v0, LX/CqA;->E:Ljava/util/EnumMap;

    iget-object v1, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1939341
    sget-object v0, LX/CqA;->E:Ljava/util/EnumMap;

    iget-object v1, p0, LX/CqA;->V:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939342
    :cond_1
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public final getAnnotationViews()LX/Cs7;
    .locals 1

    .prologue
    .line 1939343
    const/4 v0, 0x0

    return-object v0
.end method
