.class public LX/E22;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E20;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E22;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2072127
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E22;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072124
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2072125
    iput-object p1, p0, LX/E22;->b:LX/0Ot;

    .line 2072126
    return-void
.end method

.method public static a(LX/0QB;)LX/E22;
    .locals 4

    .prologue
    .line 2072111
    sget-object v0, LX/E22;->c:LX/E22;

    if-nez v0, :cond_1

    .line 2072112
    const-class v1, LX/E22;

    monitor-enter v1

    .line 2072113
    :try_start_0
    sget-object v0, LX/E22;->c:LX/E22;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072114
    if-eqz v2, :cond_0

    .line 2072115
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072116
    new-instance v3, LX/E22;

    const/16 p0, 0x30b3

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E22;-><init>(LX/0Ot;)V

    .line 2072117
    move-object v0, v3

    .line 2072118
    sput-object v0, LX/E22;->c:LX/E22;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072119
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072120
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072121
    :cond_1
    sget-object v0, LX/E22;->c:LX/E22;

    return-object v0

    .line 2072122
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072123
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2072107
    check-cast p2, LX/E21;

    .line 2072108
    iget-object v0, p0, LX/E22;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;

    iget-object v1, p2, LX/E21;->a:LX/5sY;

    iget v2, p2, LX/E21;->b:F

    .line 2072109
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1up;->c(F)LX/1up;

    move-result-object v3

    sget-object p0, LX/1Up;->g:LX/1Up;

    invoke-virtual {v3, p0}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object p0

    iget-object v3, v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, p2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v3

    sget-object p2, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object p0

    const p2, 0x7f0a010a

    invoke-virtual {p0, p2}, LX/1nh;->i(I)LX/1nh;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2072110
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2072105
    invoke-static {}, LX/1dS;->b()V

    .line 2072106
    const/4 v0, 0x0

    return-object v0
.end method
