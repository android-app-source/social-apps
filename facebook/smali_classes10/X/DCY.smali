.class public LX/DCY;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DCX;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DCZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1974120
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DCY;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DCZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1974088
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1974089
    iput-object p1, p0, LX/DCY;->b:LX/0Ot;

    .line 1974090
    return-void
.end method

.method public static a(LX/0QB;)LX/DCY;
    .locals 4

    .prologue
    .line 1974109
    const-class v1, LX/DCY;

    monitor-enter v1

    .line 1974110
    :try_start_0
    sget-object v0, LX/DCY;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1974111
    sput-object v2, LX/DCY;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1974112
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974113
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1974114
    new-instance v3, LX/DCY;

    const/16 p0, 0x1e37

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DCY;-><init>(LX/0Ot;)V

    .line 1974115
    move-object v0, v3

    .line 1974116
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1974117
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DCY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1974118
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1974119
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 1974104
    check-cast p2, LX/DCW;

    .line 1974105
    iget-object v0, p0, LX/DCY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DCZ;

    iget-object v1, p2, LX/DCW;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1974106
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Landroid/app/Activity;

    invoke-static {v2, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    .line 1974107
    iget-object v9, v0, LX/DCZ;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, v0, LX/DCZ;->b:LX/DwF;

    sget-object v5, LX/8AB;->FEED:LX/8AB;

    sget-object v6, LX/21D;->NEWSFEED:LX/21D;

    const-string v7, "albumCallToAction"

    const/4 v8, 0x1

    move-object v4, v1

    invoke-virtual/range {v2 .. v8}, LX/DwF;->a(Landroid/app/Activity;Lcom/facebook/graphql/model/GraphQLAlbum;LX/8AB;LX/21D;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const/16 v4, 0x6dc

    invoke-interface {v9, v2, v4, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1974108
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1974103
    const v0, 0x7cf1115f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1974097
    check-cast p2, LX/DCW;

    .line 1974098
    iget-object v0, p0, LX/DCY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DCZ;

    iget-object v1, p2, LX/DCW;->a:Ljava/lang/String;

    const/4 p2, 0x1

    .line 1974099
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    .line 1974100
    const v3, 0x7cf1115f

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1974101
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    iget-object v4, v0, LX/DCZ;->c:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v4

    const p0, 0x7f020959

    invoke-virtual {v4, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v4

    const p0, 0x7f0a00d2

    invoke-virtual {v4, p0}, LX/2xv;->j(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    sget-object v4, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v3, v4}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00d2

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x0

    const p0, 0x7f0b0064

    invoke-interface {v3, v4, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b0064

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1974102
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1974091
    invoke-static {}, LX/1dS;->b()V

    .line 1974092
    iget v0, p1, LX/1dQ;->b:I

    .line 1974093
    packed-switch v0, :pswitch_data_0

    .line 1974094
    :goto_0
    return-object v2

    .line 1974095
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1974096
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/DCY;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7cf1115f
        :pswitch_0
    .end packed-switch
.end method
