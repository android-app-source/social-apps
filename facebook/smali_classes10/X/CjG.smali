.class public LX/CjG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929263
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CjG;->a:Ljava/util/Map;

    .line 1929264
    return-void
.end method

.method public static a(LX/0QB;)LX/CjG;
    .locals 3

    .prologue
    .line 1929265
    const-class v1, LX/CjG;

    monitor-enter v1

    .line 1929266
    :try_start_0
    sget-object v0, LX/CjG;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1929267
    sput-object v2, LX/CjG;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1929268
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1929269
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1929270
    new-instance v0, LX/CjG;

    invoke-direct {v0}, LX/CjG;-><init>()V

    .line 1929271
    move-object v0, v0

    .line 1929272
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1929273
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CjG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1929274
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1929275
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Landroid/content/Context;)Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;
    .locals 2

    .prologue
    .line 1929244
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CjG;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    .line 1929245
    if-nez v0, :cond_0

    .line 1929246
    new-instance v0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    invoke-direct {v0, p2}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;-><init>(Landroid/content/Context;)V

    .line 1929247
    iget-object v1, p0, LX/CjG;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929248
    :cond_0
    monitor-exit p0

    return-object v0

    .line 1929249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/CGy;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2

    .prologue
    .line 1929259
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p3, p1}, LX/CjG;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    move-result-object v0

    new-instance v1, LX/CjF;

    invoke-direct {v1, p4}, LX/CjF;-><init>(LX/CGy;)V

    invoke-virtual {v0, p2, v1, p5}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->a(Ljava/lang/String;LX/CGy;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929260
    monitor-exit p0

    return-void

    .line 1929261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1929256
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p3, p1}, LX/CjG;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    move-result-object v0

    invoke-virtual {v0, p2, p4}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929257
    monitor-exit p0

    return-void

    .line 1929258
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1929250
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CjG;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    .line 1929251
    if-eqz v0, :cond_0

    .line 1929252
    invoke-virtual {v0}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->a()V

    .line 1929253
    iget-object v0, p0, LX/CjG;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929254
    :cond_0
    monitor-exit p0

    return-void

    .line 1929255
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
