.class public LX/DmD;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2039068
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2039069
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;
    .locals 12

    .prologue
    .line 2039066
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    invoke-static {p0}, LX/DnT;->b(LX/0QB;)LX/DnT;

    move-result-object v2

    check-cast v2, LX/DnT;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/6Zi;->a(LX/0QB;)LX/6Zi;

    move-result-object v4

    check-cast v4, LX/6Zi;

    invoke-static {p0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object v5

    check-cast v5, LX/Dih;

    invoke-static {p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v6

    check-cast v6, LX/1nG;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v7

    check-cast v7, LX/01T;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-static {p0}, LX/CK5;->a(LX/0QB;)LX/CK5;

    move-result-object v9

    check-cast v9, LX/CK5;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v11

    check-cast v11, LX/0wM;

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;-><init>(Landroid/content/Context;LX/DnT;Lcom/facebook/content/SecureContextHelper;LX/6Zi;LX/Dih;LX/1nG;LX/01T;LX/17Y;LX/CK5;LX/0Uh;LX/0wM;)V

    .line 2039067
    return-object v0
.end method
