.class public LX/CxQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CxP;


# instance fields
.field private final a:LX/CzE;


# direct methods
.method public constructor <init>(LX/CzE;)V
    .locals 0
    .param p1    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951708
    iput-object p1, p0, LX/CxQ;->a:LX/CzE;

    .line 1951709
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I
    .locals 3

    .prologue
    .line 1951699
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/CxQ;->a:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1951700
    iget-object v0, p0, LX/CxQ;->a:LX/CzE;

    invoke-virtual {v0, v1}, LX/CzE;->b(I)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v0, :cond_0

    .line 1951701
    iget-object v0, p0, LX/CxQ;->a:LX/CzE;

    invoke-virtual {v0, v1}, LX/CzE;->b(I)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 1951702
    iget-object v2, v0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    move-object v0, v2

    .line 1951703
    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1951704
    :goto_1
    return v1

    .line 1951705
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1951706
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final b(LX/CzL;)I
    .locals 1

    .prologue
    .line 1951697
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 1951698
    invoke-virtual {p0, v0}, LX/CxQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v0

    return v0
.end method
