.class public abstract LX/EWS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<BuilderType:",
        "LX/EWS;",
        ">",
        "Ljava/lang/Object;",
        "LX/EWR;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2129974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129975
    return-void
.end method

.method private static a(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2129976
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 2129977
    if-nez v1, :cond_0

    .line 2129978
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2129979
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/Iterable;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<TT;>;",
            "Ljava/util/Collection",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2129980
    instance-of v0, p0, LX/EYv;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 2129981
    check-cast v0, LX/EYv;

    invoke-interface {v0}, LX/EYv;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/EWS;->a(Ljava/lang/Iterable;)V

    .line 2129982
    :goto_0
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_2

    .line 2129983
    check-cast p0, Ljava/util/Collection;

    .line 2129984
    invoke-interface {p1, p0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 2129985
    :cond_0
    return-void

    .line 2129986
    :cond_1
    invoke-static {p0}, LX/EWS;->a(Ljava/lang/Iterable;)V

    goto :goto_0

    .line 2129987
    :cond_2
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 2129988
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public a(LX/EWc;LX/EYZ;)LX/EWS;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWc;",
            "LX/EYZ;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2129989
    :try_start_0
    invoke-virtual {p1}, LX/EWc;->h()LX/EWd;

    move-result-object v0

    .line 2129990
    invoke-virtual {p0, v0, p2}, LX/EWS;->b(LX/EWd;LX/EYZ;)LX/EWS;

    .line 2129991
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EWd;->a(I)V
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2129992
    return-object p0

    .line 2129993
    :catch_0
    move-exception v0

    .line 2129994
    throw v0

    .line 2129995
    :catch_1
    move-exception v0

    .line 2129996
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(LX/EWd;)LX/EWS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWd;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2129997
    sget-object v0, LX/EYZ;->c:LX/EYZ;

    move-object v0, v0

    .line 2129998
    invoke-virtual {p0, p1, v0}, LX/EWS;->b(LX/EWd;LX/EYZ;)LX/EWS;

    move-result-object v0

    return-object v0
.end method

.method public a([B)LX/EWS;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2129999
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LX/EWS;->a([BII)LX/EWS;

    move-result-object v0

    return-object v0
.end method

.method public a([BII)LX/EWS;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII)TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130000
    :try_start_0
    invoke-static {p1, p2, p3}, LX/EWd;->a([BII)LX/EWd;

    move-result-object v0

    .line 2130001
    invoke-virtual {p0, v0}, LX/EWS;->a(LX/EWd;)LX/EWS;

    .line 2130002
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EWd;->a(I)V
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2130003
    return-object p0

    .line 2130004
    :catch_0
    move-exception v0

    .line 2130005
    throw v0

    .line 2130006
    :catch_1
    move-exception v0

    .line 2130007
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public synthetic b([B)LX/EWR;
    .locals 1

    .prologue
    .line 2130008
    invoke-virtual {p0, p1}, LX/EWS;->a([B)LX/EWS;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(LX/EWd;LX/EYZ;)LX/EWS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWd;",
            "LX/EYZ;",
            ")TBuilderType;"
        }
    .end annotation
.end method

.method public synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2130009
    invoke-virtual {p0, p1, p2}, LX/EWS;->b(LX/EWd;LX/EYZ;)LX/EWS;

    move-result-object v0

    return-object v0
.end method

.method public abstract c()LX/EWS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2130010
    invoke-virtual {p0}, LX/EWS;->c()LX/EWS;

    move-result-object v0

    return-object v0
.end method
