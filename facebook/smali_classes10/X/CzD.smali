.class public final LX/CzD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/facebook/search/results/model/SearchResultsBridge;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CzE;

.field private b:I

.field private c:Lcom/facebook/search/results/model/SearchResultsBridge;


# direct methods
.method public constructor <init>(LX/CzE;)V
    .locals 1

    .prologue
    .line 1954241
    iput-object p1, p0, LX/CzD;->a:LX/CzE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954242
    const/4 v0, -0x1

    iput v0, p0, LX/CzD;->b:I

    .line 1954243
    const/4 v0, 0x0

    iput-object v0, p0, LX/CzD;->c:Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 1954244
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/CzD;->a(I)V

    .line 1954245
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 1954246
    :goto_0
    iget-object v0, p0, LX/CzD;->a:LX/CzE;

    iget-object v0, v0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 1954247
    iget-object v0, p0, LX/CzD;->a:LX/CzE;

    iget-object v0, v0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1954248
    instance-of v1, v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v1, :cond_0

    .line 1954249
    iput p1, p0, LX/CzD;->b:I

    .line 1954250
    check-cast v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    iput-object v0, p0, LX/CzD;->c:Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 1954251
    :goto_1
    return-void

    .line 1954252
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1954253
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/CzD;->c:Lcom/facebook/search/results/model/SearchResultsBridge;

    goto :goto_1
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 1954254
    iget-object v0, p0, LX/CzD;->c:Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1954255
    iget-object v0, p0, LX/CzD;->c:Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 1954256
    iget v1, p0, LX/CzD;->b:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, LX/CzD;->a(I)V

    .line 1954257
    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 1954258
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Removing is not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
