.class public final LX/EQz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2120192
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 2120193
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2120194
    :goto_0
    return v1

    .line 2120195
    :cond_0
    const-string v7, "viewer_can_enable"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2120196
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 2120197
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 2120198
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2120199
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2120200
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 2120201
    const-string v7, "images"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2120202
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2120203
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_b

    .line 2120204
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2120205
    :goto_2
    move v5, v6

    .line 2120206
    goto :goto_1

    .line 2120207
    :cond_2
    const-string v7, "moments_app_promotion"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2120208
    invoke-static {p0, p1}, LX/EQy;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2120209
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2120210
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2120211
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2120212
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2120213
    if-eqz v0, :cond_5

    .line 2120214
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 2120215
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1

    .line 2120216
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_9

    .line 2120217
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2120218
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2120219
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_7

    if-eqz v9, :cond_7

    .line 2120220
    const-string v10, "count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 2120221
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v8, v5

    move v5, v7

    goto :goto_3

    .line 2120222
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2120223
    :cond_9
    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2120224
    if-eqz v5, :cond_a

    .line 2120225
    invoke-virtual {p1, v6, v8, v6}, LX/186;->a(III)V

    .line 2120226
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_2

    :cond_b
    move v5, v6

    move v8, v6

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2120227
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2120228
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2120229
    if-eqz v0, :cond_1

    .line 2120230
    const-string v1, "images"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120231
    const/4 v1, 0x0

    .line 2120232
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2120233
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2120234
    if-eqz v1, :cond_0

    .line 2120235
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120236
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2120237
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2120238
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2120239
    if-eqz v0, :cond_2

    .line 2120240
    const-string v1, "moments_app_promotion"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120241
    invoke-static {p0, v0, p2, p3}, LX/EQy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2120242
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2120243
    if-eqz v0, :cond_3

    .line 2120244
    const-string v1, "viewer_can_enable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120245
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2120246
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2120247
    return-void
.end method
