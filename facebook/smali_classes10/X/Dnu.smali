.class public LX/Dnu;
.super LX/6lf;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6lf",
        "<",
        "LX/Dnt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/6lN;

.field private final d:LX/3A0;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0ad;

.field private final g:LX/0Xl;

.field private final h:LX/DiY;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/3A0;LX/6lN;LX/0Or;LX/0ad;LX/0Xl;LX/DiY;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/rtcpresence/annotations/IsVoipEnabledForUser;
        .end annotation
    .end param
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/3A0;",
            "LX/6lN;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ad;",
            "LX/0Xl;",
            "LX/DiY;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2041215
    invoke-direct {p0}, LX/6lf;-><init>()V

    .line 2041216
    iput-object p1, p0, LX/Dnu;->a:Landroid/content/Context;

    .line 2041217
    iput-object p2, p0, LX/Dnu;->b:LX/0Or;

    .line 2041218
    iput-object p3, p0, LX/Dnu;->d:LX/3A0;

    .line 2041219
    iput-object p4, p0, LX/Dnu;->c:LX/6lN;

    .line 2041220
    iput-object p5, p0, LX/Dnu;->e:LX/0Or;

    .line 2041221
    iput-object p6, p0, LX/Dnu;->f:LX/0ad;

    .line 2041222
    iput-object p7, p0, LX/Dnu;->g:LX/0Xl;

    .line 2041223
    iput-object p8, p0, LX/Dnu;->h:LX/DiY;

    .line 2041224
    return-void
.end method

.method public static b(LX/0QB;)LX/Dnu;
    .locals 13

    .prologue
    .line 2041225
    new-instance v0, LX/Dnu;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x12cd

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/3A0;->a(LX/0QB;)LX/3A0;

    move-result-object v3

    check-cast v3, LX/3A0;

    invoke-static {p0}, LX/6lN;->a(LX/0QB;)LX/6lN;

    move-result-object v4

    check-cast v4, LX/6lN;

    const/16 v5, 0x1568

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    .line 2041226
    new-instance v12, LX/DiY;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p0}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->b(LX/0QB;)Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    move-result-object v10

    check-cast v10, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    invoke-static {p0}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object v11

    check-cast v11, LX/30I;

    invoke-direct {v12, v8, v9, v10, v11}, LX/DiY;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;LX/30I;)V

    .line 2041227
    move-object v8, v12

    .line 2041228
    check-cast v8, LX/DiY;

    invoke-direct/range {v0 .. v8}, LX/Dnu;-><init>(Landroid/content/Context;LX/0Or;LX/3A0;LX/6lN;LX/0Or;LX/0ad;LX/0Xl;LX/DiY;)V

    .line 2041229
    return-object v0
.end method


# virtual methods
.method public final b(Landroid/view/ViewGroup;)LX/6le;
    .locals 3

    .prologue
    .line 2041230
    iget-object v0, p0, LX/Dnu;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2041231
    const v1, 0x7f031232

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2041232
    new-instance v1, LX/Dnt;

    invoke-direct {v1, v0}, LX/Dnt;-><init>(Landroid/view/View;)V

    return-object v1
.end method
