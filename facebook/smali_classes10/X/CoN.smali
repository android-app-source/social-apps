.class public final LX/CoN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field public final b:LX/CoO;

.field public final c:LX/CoQ;

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Landroid/view/View;LX/CoO;LX/CoQ;)V
    .locals 0

    .prologue
    .line 1935588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1935589
    iput-object p1, p0, LX/CoN;->a:Landroid/view/View;

    .line 1935590
    iput-object p2, p0, LX/CoN;->b:LX/CoO;

    .line 1935591
    iput-object p3, p0, LX/CoN;->c:LX/CoQ;

    .line 1935592
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1935593
    iget-object v0, p0, LX/CoN;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CoN;->b:LX/CoO;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1935594
    iget v0, p0, LX/CoN;->d:I

    if-lt p1, v0, :cond_0

    iget v0, p0, LX/CoN;->e:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1935595
    invoke-virtual {p0}, LX/CoN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935596
    iget-object v0, p0, LX/CoN;->b:LX/CoO;

    invoke-interface {v0}, LX/CoO;->a()V

    .line 1935597
    :cond_0
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1935598
    instance-of v0, p1, LX/CoN;

    if-eqz v0, :cond_1

    .line 1935599
    check-cast p1, LX/CoN;

    .line 1935600
    iget-object v0, p0, LX/CoN;->b:LX/CoO;

    iget-object v1, p1, LX/CoN;->b:LX/CoO;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 1935601
    :goto_0
    return v0

    .line 1935602
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1935603
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
