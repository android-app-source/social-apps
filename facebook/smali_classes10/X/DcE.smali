.class public LX/DcE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018097
    iput-object p1, p0, LX/DcE;->a:LX/0tX;

    .line 2018098
    iput-object p2, p0, LX/DcE;->b:LX/1Ck;

    .line 2018099
    iput-object p3, p0, LX/DcE;->c:LX/0Ot;

    .line 2018100
    return-void
.end method

.method public static a(LX/DcE;Ljava/lang/String;Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Z)V
    .locals 4

    .prologue
    .line 2018101
    new-instance v0, LX/8Al;

    invoke-direct {v0}, LX/8Al;-><init>()V

    move-object v0, v0

    .line 2018102
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2018103
    iget-object v1, p0, LX/DcE;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2018104
    if-eqz p3, :cond_0

    iget-object v0, p0, LX/DcE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iH;

    invoke-virtual {v0, p1}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2018105
    :goto_0
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    invoke-static {v2}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2018106
    iget-object v1, p0, LX/DcE;->b:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "task_key_load_menu_info"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/DcD;

    invoke-direct {v3, p0, p3, p2}, LX/DcD;-><init>(LX/DcE;ZLcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2018107
    return-void

    .line 2018108
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
