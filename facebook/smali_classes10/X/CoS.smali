.class public final enum LX/CoS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CoS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CoS;

.field public static final enum ENFORCED:LX/CoS;

.field public static final enum EXPAND_COLLAPSE_TRIGGERED:LX/CoS;

.field public static final enum SCROLL_TRIGGERED:LX/CoS;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1935706
    new-instance v0, LX/CoS;

    const-string v1, "EXPAND_COLLAPSE_TRIGGERED"

    invoke-direct {v0, v1, v2}, LX/CoS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CoS;->EXPAND_COLLAPSE_TRIGGERED:LX/CoS;

    .line 1935707
    new-instance v0, LX/CoS;

    const-string v1, "SCROLL_TRIGGERED"

    invoke-direct {v0, v1, v3}, LX/CoS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CoS;->SCROLL_TRIGGERED:LX/CoS;

    .line 1935708
    new-instance v0, LX/CoS;

    const-string v1, "ENFORCED"

    invoke-direct {v0, v1, v4}, LX/CoS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CoS;->ENFORCED:LX/CoS;

    .line 1935709
    const/4 v0, 0x3

    new-array v0, v0, [LX/CoS;

    sget-object v1, LX/CoS;->EXPAND_COLLAPSE_TRIGGERED:LX/CoS;

    aput-object v1, v0, v2

    sget-object v1, LX/CoS;->SCROLL_TRIGGERED:LX/CoS;

    aput-object v1, v0, v3

    sget-object v1, LX/CoS;->ENFORCED:LX/CoS;

    aput-object v1, v0, v4

    sput-object v0, LX/CoS;->$VALUES:[LX/CoS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1935703
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CoS;
    .locals 1

    .prologue
    .line 1935705
    const-class v0, LX/CoS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CoS;

    return-object v0
.end method

.method public static values()[LX/CoS;
    .locals 1

    .prologue
    .line 1935704
    sget-object v0, LX/CoS;->$VALUES:[LX/CoS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CoS;

    return-object v0
.end method
