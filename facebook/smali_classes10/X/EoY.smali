.class public LX/EoY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Enm;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/EoY;


# instance fields
.field public final a:LX/EoJ;

.field public final b:LX/Eok;

.field private final c:LX/EpA;

.field private final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/En6",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/EoJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Eok;LX/EpA;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2168229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2168230
    iput-object p1, p0, LX/EoY;->b:LX/Eok;

    .line 2168231
    iput-object p2, p0, LX/EoY;->c:LX/EpA;

    .line 2168232
    new-instance v0, LX/EoW;

    invoke-direct {v0, p0}, LX/EoW;-><init>(LX/EoY;)V

    iput-object v0, p0, LX/EoY;->a:LX/EoJ;

    .line 2168233
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-class v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    new-instance v2, LX/EoX;

    invoke-direct {v2, p0}, LX/EoX;-><init>(LX/EoY;)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/EoY;->d:LX/0P1;

    .line 2168234
    iget-object v0, p0, LX/EoY;->a:LX/EoJ;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/EoY;->e:LX/0Rf;

    .line 2168235
    return-void
.end method

.method public static a(LX/0QB;)LX/EoY;
    .locals 5

    .prologue
    .line 2168236
    sget-object v0, LX/EoY;->f:LX/EoY;

    if-nez v0, :cond_1

    .line 2168237
    const-class v1, LX/EoY;

    monitor-enter v1

    .line 2168238
    :try_start_0
    sget-object v0, LX/EoY;->f:LX/EoY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2168239
    if-eqz v2, :cond_0

    .line 2168240
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2168241
    new-instance p0, LX/EoY;

    const-class v3, LX/Eok;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Eok;

    invoke-static {v0}, LX/EpA;->a(LX/0QB;)LX/EpA;

    move-result-object v4

    check-cast v4, LX/EpA;

    invoke-direct {p0, v3, v4}, LX/EoY;-><init>(LX/Eok;LX/EpA;)V

    .line 2168242
    move-object v0, p0

    .line 2168243
    sput-object v0, LX/EoY;->f:LX/EoY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2168244
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2168245
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2168246
    :cond_1
    sget-object v0, LX/EoY;->f:LX/EoY;

    return-object v0

    .line 2168247
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2168248
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/EoJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2168249
    iget-object v0, p0, LX/EoY;->e:LX/0Rf;

    return-object v0
.end method

.method public final b()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/En6",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 2168250
    iget-object v0, p0, LX/EoY;->d:LX/0P1;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2168251
    iget-object v0, p0, LX/EoY;->c:LX/EpA;

    invoke-virtual {v0}, LX/EpA;->d()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
