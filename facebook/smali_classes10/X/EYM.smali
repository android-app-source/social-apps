.class public final LX/EYM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EYE;
.implements LX/EXG;


# instance fields
.field public final a:I

.field public b:LX/EX6;

.field private final c:Ljava/lang/String;

.field private final d:LX/EYQ;

.field public final e:LX/EYL;


# direct methods
.method public constructor <init>(LX/EX6;LX/EYQ;LX/EYL;I)V
    .locals 2

    .prologue
    .line 2137026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137027
    iput p4, p0, LX/EYM;->a:I

    .line 2137028
    iput-object p1, p0, LX/EYM;->b:LX/EX6;

    .line 2137029
    iput-object p2, p0, LX/EYM;->d:LX/EYQ;

    .line 2137030
    iput-object p3, p0, LX/EYM;->e:LX/EYL;

    .line 2137031
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, LX/EYL;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LX/EX6;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYM;->c:Ljava/lang/String;

    .line 2137032
    iget-object v0, p2, LX/EYQ;->h:LX/EYJ;

    invoke-virtual {v0, p0}, LX/EYJ;->a(LX/EYE;)V

    .line 2137033
    iget-object v0, p2, LX/EYQ;->h:LX/EYJ;

    .line 2137034
    new-instance p1, LX/EYG;

    .line 2137035
    iget-object v1, p0, LX/EYM;->e:LX/EYL;

    move-object v1, v1

    .line 2137036
    invoke-virtual {p0}, LX/EYM;->getNumber()I

    move-result p2

    invoke-direct {p1, v1, p2}, LX/EYG;-><init>(LX/EYE;I)V

    .line 2137037
    iget-object v1, v0, LX/EYJ;->e:Ljava/util/Map;

    invoke-interface {v1, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYM;

    .line 2137038
    if-eqz v1, :cond_0

    .line 2137039
    iget-object p2, v0, LX/EYJ;->e:Ljava/util/Map;

    invoke-interface {p2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2137040
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137025
    iget-object v0, p0, LX/EYM;->b:LX/EX6;

    invoke-virtual {v0}, LX/EX6;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137024
    iget-object v0, p0, LX/EYM;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/EYQ;
    .locals 1

    .prologue
    .line 2137023
    iget-object v0, p0, LX/EYM;->d:LX/EYQ;

    return-object v0
.end method

.method public final getNumber()I
    .locals 1

    .prologue
    .line 2137019
    iget-object v0, p0, LX/EYM;->b:LX/EX6;

    .line 2137020
    iget p0, v0, LX/EX6;->number_:I

    move v0, p0

    .line 2137021
    return v0
.end method

.method public final h()LX/EWY;
    .locals 1

    .prologue
    .line 2137022
    iget-object v0, p0, LX/EYM;->b:LX/EX6;

    return-object v0
.end method
