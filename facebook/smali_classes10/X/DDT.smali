.class public LX/DDT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1LV;

.field public final b:LX/3mL;

.field public final c:LX/DDO;


# direct methods
.method public constructor <init>(LX/3mL;LX/1LV;LX/DDO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1975631
    iput-object p1, p0, LX/DDT;->b:LX/3mL;

    .line 1975632
    iput-object p2, p0, LX/DDT;->a:LX/1LV;

    .line 1975633
    iput-object p3, p0, LX/DDT;->c:LX/DDO;

    .line 1975634
    return-void
.end method

.method public static a(LX/0QB;)LX/DDT;
    .locals 6

    .prologue
    .line 1975635
    const-class v1, LX/DDT;

    monitor-enter v1

    .line 1975636
    :try_start_0
    sget-object v0, LX/DDT;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975637
    sput-object v2, LX/DDT;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975638
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975639
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975640
    new-instance p0, LX/DDT;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v4

    check-cast v4, LX/1LV;

    const-class v5, LX/DDO;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/DDO;

    invoke-direct {p0, v3, v4, v5}, LX/DDT;-><init>(LX/3mL;LX/1LV;LX/DDO;)V

    .line 1975641
    move-object v0, p0

    .line 1975642
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975643
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DDT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975644
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975645
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
