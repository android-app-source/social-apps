.class public LX/E5A;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E58;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E5A;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5B;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2077970
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5A;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5B;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077971
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2077972
    iput-object p1, p0, LX/E5A;->b:LX/0Ot;

    .line 2077973
    return-void
.end method

.method public static a(LX/0QB;)LX/E5A;
    .locals 4

    .prologue
    .line 2077974
    sget-object v0, LX/E5A;->c:LX/E5A;

    if-nez v0, :cond_1

    .line 2077975
    const-class v1, LX/E5A;

    monitor-enter v1

    .line 2077976
    :try_start_0
    sget-object v0, LX/E5A;->c:LX/E5A;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077977
    if-eqz v2, :cond_0

    .line 2077978
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077979
    new-instance v3, LX/E5A;

    const/16 p0, 0x311f

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E5A;-><init>(LX/0Ot;)V

    .line 2077980
    move-object v0, v3

    .line 2077981
    sput-object v0, LX/E5A;->c:LX/E5A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077982
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077983
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077984
    :cond_1
    sget-object v0, LX/E5A;->c:LX/E5A;

    return-object v0

    .line 2077985
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077986
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2077987
    check-cast p2, LX/E59;

    .line 2077988
    iget-object v0, p0, LX/E5A;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E5B;

    iget-object v2, p2, LX/E59;->a:Ljava/lang/String;

    iget-object v3, p2, LX/E59;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    iget-object v4, p2, LX/E59;->c:LX/174;

    iget-object v5, p2, LX/E59;->d:LX/174;

    iget-boolean v6, p2, LX/E59;->e:Z

    move-object v1, p1

    .line 2077989
    iget-object p1, v0, LX/E5B;->b:LX/0SG;

    if-nez v4, :cond_0

    const/4 p0, 0x0

    :goto_0
    invoke-static {p1, v1, p0, v2, v3}, LX/E6H;->a(LX/0SG;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;)Landroid/text/SpannableStringBuilder;

    move-result-object p0

    .line 2077990
    if-eqz v6, :cond_1

    .line 2077991
    iget-object p1, v0, LX/E5B;->c:LX/E5T;

    invoke-virtual {p1, v1}, LX/E5T;->c(LX/1De;)LX/E5R;

    move-result-object p1

    invoke-virtual {p1, p0}, LX/E5R;->a(Landroid/text/SpannableStringBuilder;)LX/E5R;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/E5R;->a(LX/174;)LX/E5R;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    .line 2077992
    :goto_1
    move-object v0, p0

    .line 2077993
    return-object v0

    .line 2077994
    :cond_0
    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 2077995
    :cond_1
    iget-object p1, v0, LX/E5B;->a:LX/E4q;

    invoke-virtual {p1, v1}, LX/E4q;->c(LX/1De;)LX/E4o;

    move-result-object p1

    invoke-virtual {p1, p0}, LX/E4o;->a(Landroid/text/SpannableStringBuilder;)LX/E4o;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2077996
    invoke-static {}, LX/1dS;->b()V

    .line 2077997
    const/4 v0, 0x0

    return-object v0
.end method
