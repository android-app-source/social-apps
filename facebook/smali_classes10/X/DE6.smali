.class public LX/DE6;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mX",
        "<",
        "LX/DEI;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/DEF;

.field private final d:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Po;LX/25M;LX/DEF;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Po;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/DEI;",
            ">;TE;",
            "LX/25M;",
            "LX/DEF;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976352
    move-object v0, p3

    check-cast v0, LX/1Pq;

    invoke-direct {p0, p1, p2, v0, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1976353
    iput-object p5, p0, LX/DE6;->c:LX/DEF;

    .line 1976354
    iput-object p3, p0, LX/DE6;->d:LX/1Po;

    .line 1976355
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1976356
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 1976357
    check-cast p2, LX/DEI;

    .line 1976358
    iget-object v0, p0, LX/DE6;->c:LX/DEF;

    const/4 v1, 0x0

    .line 1976359
    new-instance v2, LX/DEE;

    invoke-direct {v2, v0}, LX/DEE;-><init>(LX/DEF;)V

    .line 1976360
    iget-object v3, v0, LX/DEF;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DED;

    .line 1976361
    if-nez v3, :cond_0

    .line 1976362
    new-instance v3, LX/DED;

    invoke-direct {v3, v0}, LX/DED;-><init>(LX/DEF;)V

    .line 1976363
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/DED;->a$redex0(LX/DED;LX/1De;IILX/DEE;)V

    .line 1976364
    move-object v2, v3

    .line 1976365
    move-object v1, v2

    .line 1976366
    move-object v0, v1

    .line 1976367
    iget-object v1, v0, LX/DED;->a:LX/DEE;

    iput-object p2, v1, LX/DEE;->a:LX/DEI;

    .line 1976368
    iget-object v1, v0, LX/DED;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1976369
    move-object v0, v0

    .line 1976370
    iget-object v1, p0, LX/DE6;->d:LX/1Po;

    .line 1976371
    iget-object v2, v0, LX/DED;->a:LX/DEE;

    iput-object v1, v2, LX/DEE;->b:LX/1Po;

    .line 1976372
    iget-object v2, v0, LX/DED;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1976373
    move-object v0, v0

    .line 1976374
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1976375
    const/4 v0, 0x0

    return v0
.end method
