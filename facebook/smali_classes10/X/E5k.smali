.class public LX/E5k;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078860
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5k;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078857
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078858
    iput-object p1, p0, LX/E5k;->b:LX/0Ot;

    .line 2078859
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2078836
    check-cast p2, LX/E5j;

    .line 2078837
    iget-object v0, p0, LX/E5k;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;

    iget-object v1, p2, LX/E5j;->a:Ljava/lang/String;

    iget-object v2, p2, LX/E5j;->b:Landroid/net/Uri;

    iget-object v3, p2, LX/E5j;->c:Landroid/view/View$OnClickListener;

    const/4 v5, 0x0

    const/4 v6, 0x2

    .line 2078838
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    .line 2078839
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v6, 0x7f01072b

    invoke-interface {v4, v6}, LX/1Dh;->U(I)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x6

    const p0, 0x7f010718

    invoke-interface {v4, v6, p0}, LX/1Dh;->t(II)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x1

    const p0, 0x7f0b163a

    invoke-interface {v4, v6, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x3

    const p0, 0x7f0b163d

    invoke-interface {v4, v6, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    if-nez v2, :cond_0

    move-object v4, v5

    :goto_0
    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    const p0, 0x7f0b0050

    invoke-virtual {v6, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const p0, 0x7f010714

    invoke-virtual {v6, p0}, LX/1ne;->o(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v6, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    if-nez v3, :cond_1

    :goto_1
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2078840
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object p0

    const p2, 0x7f0a010a

    invoke-virtual {p0, p2}, LX/1nh;->i(I)LX/1nh;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object p0

    iget-object v4, v0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v4

    sget-object p2, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const p0, 0x7f0b1631

    invoke-interface {v4, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const p0, 0x7f0b1631

    invoke-interface {v4, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const/4 p0, 0x5

    const p2, 0x7f0b1607

    invoke-interface {v4, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    goto :goto_0

    :cond_1
    iget-object v5, v0, Lcom/facebook/reaction/feed/unitcomponents/spec/header/ReactionIconHeaderComponentSpec;->c:LX/E56;

    const/4 v6, 0x0

    .line 2078841
    new-instance p0, LX/E55;

    invoke-direct {p0, v5}, LX/E55;-><init>(LX/E56;)V

    .line 2078842
    sget-object p2, LX/E56;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/E54;

    .line 2078843
    if-nez p2, :cond_2

    .line 2078844
    new-instance p2, LX/E54;

    invoke-direct {p2}, LX/E54;-><init>()V

    .line 2078845
    :cond_2
    invoke-static {p2, p1, v6, v6, p0}, LX/E54;->a$redex0(LX/E54;LX/1De;IILX/E55;)V

    .line 2078846
    move-object p0, p2

    .line 2078847
    move-object v6, p0

    .line 2078848
    move-object v5, v6

    .line 2078849
    iget-object v6, v5, LX/E54;->a:LX/E55;

    iput-object v3, v6, LX/E55;->b:Landroid/view/View$OnClickListener;

    .line 2078850
    iget-object v6, v5, LX/E54;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2078851
    move-object v5, v5

    .line 2078852
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    const p0, 0x7f020aea

    invoke-virtual {v6, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v6

    .line 2078853
    iget-object p0, v5, LX/E54;->a:LX/E55;

    invoke-virtual {v6}, LX/1X5;->d()LX/1X1;

    move-result-object p2

    iput-object p2, p0, LX/E55;->a:LX/1X1;

    .line 2078854
    iget-object p0, v5, LX/E54;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2078855
    move-object v5, v5

    .line 2078856
    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2078834
    invoke-static {}, LX/1dS;->b()V

    .line 2078835
    const/4 v0, 0x0

    return-object v0
.end method
