.class public LX/DpY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final body:LX/DpZ;

.field public final ephemeral_lifetime_micros:Ljava/lang/Long;

.field public final sender_hmac_key:[B

.field public final type:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2046001
    new-instance v0, LX/1sv;

    const-string v1, "Salamander"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpY;->b:LX/1sv;

    .line 2046002
    new-instance v0, LX/1sw;

    const-string v1, "type"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpY;->c:LX/1sw;

    .line 2046003
    new-instance v0, LX/1sw;

    const-string v1, "body"

    const/16 v2, 0xc

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpY;->d:LX/1sw;

    .line 2046004
    new-instance v0, LX/1sw;

    const-string v1, "sender_hmac_key"

    const/16 v2, 0xb

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpY;->e:LX/1sw;

    .line 2046005
    new-instance v0, LX/1sw;

    const-string v1, "ephemeral_lifetime_micros"

    const/16 v2, 0xa

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpY;->f:LX/1sw;

    .line 2046006
    const/4 v0, 0x1

    sput-boolean v0, LX/DpY;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;LX/DpZ;[BLjava/lang/Long;)V
    .locals 0

    .prologue
    .line 2045995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2045996
    iput-object p1, p0, LX/DpY;->type:Ljava/lang/Integer;

    .line 2045997
    iput-object p2, p0, LX/DpY;->body:LX/DpZ;

    .line 2045998
    iput-object p3, p0, LX/DpY;->sender_hmac_key:[B

    .line 2045999
    iput-object p4, p0, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    .line 2046000
    return-void
.end method

.method public static a(LX/DpY;)V
    .locals 3

    .prologue
    .line 2045885
    iget-object v0, p0, LX/DpY;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/Dpc;->a:LX/1sn;

    iget-object v1, p0, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2045886
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'type\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2045887
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2045945
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2045946
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v2, v0

    .line 2045947
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    move-object v1, v0

    .line 2045948
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "Salamander"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045949
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045950
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045951
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045952
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045953
    const-string v0, "type"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045954
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045955
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045956
    iget-object v0, p0, LX/DpY;->type:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 2045957
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045958
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045959
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045960
    const-string v0, "body"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045961
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045962
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045963
    iget-object v0, p0, LX/DpY;->body:LX/DpZ;

    if-nez v0, :cond_6

    .line 2045964
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045965
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045966
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045967
    const-string v0, "sender_hmac_key"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045968
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045969
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045970
    iget-object v0, p0, LX/DpY;->sender_hmac_key:[B

    if-nez v0, :cond_7

    .line 2045971
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045972
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045973
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045974
    const-string v0, "ephemeral_lifetime_micros"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045975
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045976
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045977
    iget-object v0, p0, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    if-nez v0, :cond_8

    .line 2045978
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045979
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045980
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045981
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2045982
    :cond_1
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2045983
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2045984
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 2045985
    :cond_4
    sget-object v0, LX/Dpc;->b:Ljava/util/Map;

    iget-object v5, p0, LX/DpY;->type:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2045986
    if-eqz v0, :cond_5

    .line 2045987
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045988
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045989
    :cond_5
    iget-object v5, p0, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2045990
    if-eqz v0, :cond_0

    .line 2045991
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2045992
    :cond_6
    iget-object v0, p0, LX/DpY;->body:LX/DpZ;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2045993
    :cond_7
    iget-object v0, p0, LX/DpY;->sender_hmac_key:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2045994
    :cond_8
    iget-object v0, p0, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2045928
    invoke-static {p0}, LX/DpY;->a(LX/DpY;)V

    .line 2045929
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2045930
    iget-object v0, p0, LX/DpY;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2045931
    sget-object v0, LX/DpY;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045932
    iget-object v0, p0, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2045933
    :cond_0
    iget-object v0, p0, LX/DpY;->body:LX/DpZ;

    if-eqz v0, :cond_1

    .line 2045934
    sget-object v0, LX/DpY;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045935
    iget-object v0, p0, LX/DpY;->body:LX/DpZ;

    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    .line 2045936
    :cond_1
    iget-object v0, p0, LX/DpY;->sender_hmac_key:[B

    if-eqz v0, :cond_2

    .line 2045937
    sget-object v0, LX/DpY;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045938
    iget-object v0, p0, LX/DpY;->sender_hmac_key:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2045939
    :cond_2
    iget-object v0, p0, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2045940
    sget-object v0, LX/DpY;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045941
    iget-object v0, p0, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2045942
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2045943
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2045944
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2045892
    if-nez p1, :cond_1

    .line 2045893
    :cond_0
    :goto_0
    return v0

    .line 2045894
    :cond_1
    instance-of v1, p1, LX/DpY;

    if-eqz v1, :cond_0

    .line 2045895
    check-cast p1, LX/DpY;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2045896
    if-nez p1, :cond_3

    .line 2045897
    :cond_2
    :goto_1
    move v0, v2

    .line 2045898
    goto :goto_0

    .line 2045899
    :cond_3
    iget-object v0, p0, LX/DpY;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    move v0, v1

    .line 2045900
    :goto_2
    iget-object v3, p1, LX/DpY;->type:Ljava/lang/Integer;

    if-eqz v3, :cond_d

    move v3, v1

    .line 2045901
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2045902
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045903
    iget-object v0, p0, LX/DpY;->type:Ljava/lang/Integer;

    iget-object v3, p1, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2045904
    :cond_5
    iget-object v0, p0, LX/DpY;->body:LX/DpZ;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2045905
    :goto_4
    iget-object v3, p1, LX/DpY;->body:LX/DpZ;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2045906
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2045907
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045908
    iget-object v0, p0, LX/DpY;->body:LX/DpZ;

    iget-object v3, p1, LX/DpY;->body:LX/DpZ;

    invoke-virtual {v0, v3}, LX/DpZ;->a(LX/DpZ;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2045909
    :cond_7
    iget-object v0, p0, LX/DpY;->sender_hmac_key:[B

    if-eqz v0, :cond_10

    move v0, v1

    .line 2045910
    :goto_6
    iget-object v3, p1, LX/DpY;->sender_hmac_key:[B

    if-eqz v3, :cond_11

    move v3, v1

    .line 2045911
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2045912
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045913
    iget-object v0, p0, LX/DpY;->sender_hmac_key:[B

    iget-object v3, p1, LX/DpY;->sender_hmac_key:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2045914
    :cond_9
    iget-object v0, p0, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 2045915
    :goto_8
    iget-object v3, p1, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 2045916
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2045917
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045918
    iget-object v0, p0, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    iget-object v3, p1, LX/DpY;->ephemeral_lifetime_micros:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 2045919
    goto :goto_1

    :cond_c
    move v0, v2

    .line 2045920
    goto :goto_2

    :cond_d
    move v3, v2

    .line 2045921
    goto :goto_3

    :cond_e
    move v0, v2

    .line 2045922
    goto :goto_4

    :cond_f
    move v3, v2

    .line 2045923
    goto :goto_5

    :cond_10
    move v0, v2

    .line 2045924
    goto :goto_6

    :cond_11
    move v3, v2

    .line 2045925
    goto :goto_7

    :cond_12
    move v0, v2

    .line 2045926
    goto :goto_8

    :cond_13
    move v3, v2

    .line 2045927
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2045891
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2045888
    sget-boolean v0, LX/DpY;->a:Z

    .line 2045889
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpY;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2045890
    return-object v0
.end method
