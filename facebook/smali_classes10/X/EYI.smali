.class public final enum LX/EYI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EYI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EYI;

.field public static final enum AGGREGATES_ONLY:LX/EYI;

.field public static final enum ALL_SYMBOLS:LX/EYI;

.field public static final enum TYPES_ONLY:LX/EYI;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2136879
    new-instance v0, LX/EYI;

    const-string v1, "TYPES_ONLY"

    invoke-direct {v0, v1, v2}, LX/EYI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EYI;->TYPES_ONLY:LX/EYI;

    new-instance v0, LX/EYI;

    const-string v1, "AGGREGATES_ONLY"

    invoke-direct {v0, v1, v3}, LX/EYI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EYI;->AGGREGATES_ONLY:LX/EYI;

    new-instance v0, LX/EYI;

    const-string v1, "ALL_SYMBOLS"

    invoke-direct {v0, v1, v4}, LX/EYI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EYI;->ALL_SYMBOLS:LX/EYI;

    .line 2136880
    const/4 v0, 0x3

    new-array v0, v0, [LX/EYI;

    sget-object v1, LX/EYI;->TYPES_ONLY:LX/EYI;

    aput-object v1, v0, v2

    sget-object v1, LX/EYI;->AGGREGATES_ONLY:LX/EYI;

    aput-object v1, v0, v3

    sget-object v1, LX/EYI;->ALL_SYMBOLS:LX/EYI;

    aput-object v1, v0, v4

    sput-object v0, LX/EYI;->$VALUES:[LX/EYI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2136881
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EYI;
    .locals 1

    .prologue
    .line 2136882
    const-class v0, LX/EYI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EYI;

    return-object v0
.end method

.method public static values()[LX/EYI;
    .locals 1

    .prologue
    .line 2136883
    sget-object v0, LX/EYI;->$VALUES:[LX/EYI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EYI;

    return-object v0
.end method
