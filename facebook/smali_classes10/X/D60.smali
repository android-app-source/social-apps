.class public LX/D60;
.super LX/2oV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oV",
        "<",
        "LX/D5z;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/04B;

.field private c:Z

.field private d:LX/D5r;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/D5r;LX/04B;LX/9Da;LX/1My;LX/03V;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/D5r;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1965092
    invoke-direct {p0, p1}, LX/2oV;-><init>(Ljava/lang/String;)V

    .line 1965093
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/D60;->a:Ljava/util/Set;

    .line 1965094
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D60;->c:Z

    .line 1965095
    iput-object p2, p0, LX/D60;->d:LX/D5r;

    .line 1965096
    iput-object p3, p0, LX/D60;->b:LX/04B;

    .line 1965097
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1965032
    check-cast p1, LX/D5z;

    const/4 v0, 0x0

    .line 1965033
    iget-boolean v1, p0, LX/D60;->c:Z

    if-nez v1, :cond_0

    .line 1965034
    :goto_0
    return-void

    .line 1965035
    :cond_0
    iput-boolean v0, p0, LX/D60;->c:Z

    .line 1965036
    iget-object v1, p0, LX/D60;->d:LX/D5r;

    invoke-virtual {p1}, LX/D5z;->getSeekPosition()I

    move-result v2

    invoke-virtual {v1, v2}, LX/D5r;->a(I)V

    .line 1965037
    iget-object v1, p0, LX/D60;->d:LX/D5r;

    invoke-virtual {p1}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 1965038
    :cond_1
    iput-boolean v0, v1, LX/D5r;->f:Z

    .line 1965039
    iget-object v0, p0, LX/D60;->d:LX/D5r;

    invoke-virtual {p1}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->s()Z

    move-result v1

    .line 1965040
    iput-boolean v1, v0, LX/D5r;->g:Z

    .line 1965041
    invoke-virtual {p1}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 1965042
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1965043
    if-eqz v0, :cond_3

    invoke-virtual {p1}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 1965044
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1965045
    invoke-virtual {v0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    .line 1965046
    :goto_1
    iget-object v1, p0, LX/D60;->d:LX/D5r;

    .line 1965047
    iput-object v0, v1, LX/D5r;->h:LX/2oi;

    .line 1965048
    iget-object v0, p0, LX/D60;->d:LX/D5r;

    invoke-virtual {v0}, LX/D5r;->b()LX/2oO;

    move-result-object v0

    .line 1965049
    invoke-virtual {p1}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v1

    sget-object v2, LX/2qV;->PAUSED:LX/2qV;

    if-ne v1, v2, :cond_4

    .line 1965050
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/2oO;->u:Z

    .line 1965051
    :cond_2
    :goto_2
    invoke-virtual {p1}, LX/D5z;->y()V

    goto :goto_0

    .line 1965052
    :cond_3
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    goto :goto_1

    .line 1965053
    :cond_4
    invoke-virtual {p1}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1965054
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/2oO;->u:Z

    .line 1965055
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p1, v0}, LX/D5z;->b(LX/04g;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1965056
    check-cast p1, LX/D5z;

    const/4 v2, 0x1

    .line 1965057
    iget-boolean v0, p0, LX/D60;->c:Z

    if-eqz v0, :cond_0

    .line 1965058
    :goto_0
    return-void

    .line 1965059
    :cond_0
    iput-boolean v2, p0, LX/D60;->c:Z

    .line 1965060
    iget-object v0, p0, LX/D60;->d:LX/D5r;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D60;->d:LX/D5r;

    .line 1965061
    iget-boolean v1, v0, LX/D5r;->e:Z

    move v0, v1

    .line 1965062
    if-nez v0, :cond_1

    .line 1965063
    iget-object v0, p1, LX/D5z;->r:Ljava/lang/String;

    move-object v0, v0

    .line 1965064
    iget-object v1, p0, LX/D60;->b:LX/04B;

    .line 1965065
    iget-object v3, v1, LX/04B;->i:Ljava/lang/String;

    move-object v1, v3

    .line 1965066
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1965067
    iget-object v3, p1, LX/D5z;->a:LX/1C2;

    iget-object v4, p1, LX/D5z;->q:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v5, p1, LX/D5z;->r:Ljava/lang/String;

    iget-object v6, p1, LX/D5z;->s:LX/D4U;

    check-cast v6, LX/D4U;

    .line 1965068
    iget-object v7, v6, LX/D4U;->q:LX/04D;

    move-object v6, v7

    .line 1965069
    sget-object v7, LX/04G;->CHANNEL_PLAYER:LX/04G;

    iget-object v8, p1, LX/D5z;->q:LX/2pa;

    iget-object v8, v8, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v8, v8, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v3 .. v8}, LX/1C2;->a(LX/0lF;Ljava/lang/String;LX/04D;LX/04G;Z)LX/1C2;

    .line 1965070
    iget-object v0, p0, LX/D60;->d:LX/D5r;

    .line 1965071
    iput-boolean v2, v0, LX/D5r;->e:Z

    .line 1965072
    :cond_1
    iget-object v0, p0, LX/D60;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1965073
    iget-object v0, p0, LX/D60;->d:LX/D5r;

    invoke-virtual {v0}, LX/D5r;->b()LX/2oO;

    move-result-object v0

    .line 1965074
    iget-object v1, p0, LX/D60;->a:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/2oO;->a(Ljava/util/Set;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1965075
    const/4 v1, 0x0

    .line 1965076
    iput-boolean v1, v0, LX/2oO;->y:Z

    .line 1965077
    iget-object v0, p0, LX/D60;->d:LX/D5r;

    invoke-virtual {v0}, LX/D5r;->a()I

    move-result v0

    iget-object v1, p0, LX/D60;->d:LX/D5r;

    invoke-virtual {v1}, LX/D5r;->c()LX/04g;

    move-result-object v1

    iget-object v2, p0, LX/D60;->d:LX/D5r;

    .line 1965078
    iget-object v3, v2, LX/D5r;->h:LX/2oi;

    move-object v2, v3

    .line 1965079
    iget-object v3, p1, LX/D5z;->s:LX/D4U;

    .line 1965080
    iget-object v4, v3, LX/D4U;->s:LX/D4m;

    if-eqz v4, :cond_5

    iget-object v4, v3, LX/D4U;->s:LX/D4m;

    .line 1965081
    iget-boolean v3, v4, LX/D4m;->g:Z

    move v4, v3

    .line 1965082
    if-eqz v4, :cond_5

    const/4 v4, 0x1

    :goto_1
    move v3, v4

    .line 1965083
    if-eqz v3, :cond_3

    .line 1965084
    :goto_2
    invoke-virtual {p1}, LX/D5z;->x()V

    goto :goto_0

    .line 1965085
    :cond_2
    invoke-virtual {p1}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 1965086
    iget-object v1, p0, LX/D60;->d:LX/D5r;

    invoke-virtual {v1}, LX/D5r;->a()I

    move-result v1

    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    goto :goto_2

    .line 1965087
    :cond_3
    invoke-static {p1}, LX/D5z;->k(LX/D5z;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1965088
    iget-object v3, p1, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v4, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v3, v0, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1965089
    :cond_4
    iget-object v3, p1, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 1965090
    iget-object v3, p1, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v4, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oi;LX/04g;)V

    .line 1965091
    iget-object v3, p1, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v4, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method
