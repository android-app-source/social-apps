.class public final LX/EMS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8d1;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:LX/CzL;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/8d1;LX/1Pn;LX/CzL;)V
    .locals 0

    .prologue
    .line 2110726
    iput-object p1, p0, LX/EMS;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iput-object p2, p0, LX/EMS;->a:LX/8d1;

    iput-object p3, p0, LX/EMS;->b:LX/1Pn;

    iput-object p4, p0, LX/EMS;->c:LX/CzL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v0, 0x1

    const v1, -0x701ea8ac

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2110727
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    iget-object v1, p0, LX/EMS;->a:LX/8d1;

    invoke-interface {v1}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2110728
    iget-object v0, p0, LX/EMS;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v2, p0, LX/EMS;->b:LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2110729
    iget-object v0, p0, LX/EMS;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v1, p0, LX/EMS;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    iget-object v2, p0, LX/EMS;->c:LX/CzL;

    invoke-static {v2}, LX/CvY;->a(LX/CzL;)LX/CvJ;

    move-result-object v2

    iget-object v3, p0, LX/EMS;->b:LX/1Pn;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/EMS;->c:LX/CzL;

    invoke-interface {v3, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, LX/EMS;->c:LX/CzL;

    iget-object v5, p0, LX/EMS;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v5, v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->i:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CvY;

    iget-object v6, p0, LX/EMS;->b:LX/1Pn;

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    iget-object v8, p0, LX/EMS;->c:LX/CzL;

    invoke-static {v6, v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzL;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CvJ;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2110730
    const v0, 0x426a8af9

    invoke-static {v9, v9, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
