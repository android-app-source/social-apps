.class public LX/EFg;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:LX/EFf;

.field private final b:Landroid/graphics/drawable/ShapeDrawable;


# direct methods
.method public constructor <init>(IIII)V
    .locals 1

    .prologue
    .line 2096157
    new-instance v0, LX/EFf;

    invoke-direct {v0, p1, p2, p3, p4}, LX/EFf;-><init>(IIII)V

    invoke-direct {p0, v0}, LX/EFg;-><init>(LX/EFf;)V

    .line 2096158
    return-void
.end method

.method public constructor <init>(LX/EFf;)V
    .locals 2

    .prologue
    .line 2096150
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2096151
    iput-object p1, p0, LX/EFg;->a:LX/EFf;

    .line 2096152
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object v0, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    .line 2096153
    iget-object v0, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    iget v1, p1, LX/EFf;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicWidth(I)V

    .line 2096154
    iget-object v0, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    iget v1, p1, LX/EFf;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicHeight(I)V

    .line 2096155
    iget-object v0, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    iget v1, p1, LX/EFf;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2096156
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2096132
    iget-object v0, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 2096133
    return-void
.end method

.method public final getAlpha()I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 2096149
    iget-object v0, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getAlpha()I

    move-result v0

    return v0
.end method

.method public final getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    .prologue
    .line 2096148
    iget-object v0, p0, LX/EFg;->a:LX/EFf;

    return-object v0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 2096159
    iget-object v0, p0, LX/EFg;->a:LX/EFf;

    iget v0, v0, LX/EFf;->b:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 2096147
    iget-object v0, p0, LX/EFg;->a:LX/EFf;

    iget v0, v0, LX/EFf;->a:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2096146
    iget-object v0, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 2096144
    iget-object v0, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->setAlpha(I)V

    .line 2096145
    return-void
.end method

.method public final setBounds(IIII)V
    .locals 5

    .prologue
    .line 2096136
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2096137
    invoke-virtual {p0}, LX/EFg;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 2096138
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, LX/EFg;->a:LX/EFf;

    iget v2, v2, LX/EFf;->c:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 2096139
    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, LX/EFg;->a:LX/EFf;

    iget v2, v2, LX/EFf;->c:I

    sub-int/2addr v0, v2

    iget-object v2, p0, LX/EFg;->a:LX/EFf;

    iget v2, v2, LX/EFf;->c:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    .line 2096140
    iget-object v2, p0, LX/EFg;->a:LX/EFf;

    iget v2, v2, LX/EFf;->c:I

    add-int/2addr v2, v1

    .line 2096141
    iget-object v3, p0, LX/EFg;->a:LX/EFf;

    iget v3, v3, LX/EFf;->c:I

    add-int/2addr v3, v0

    .line 2096142
    iget-object v4, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v4, v1, v0, v2, v3}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 2096143
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 2096134
    iget-object v0, p0, LX/EFg;->b:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2096135
    return-void
.end method
