.class public LX/DIv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DataProvider::",
        "LX/0j7;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0tX;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TDataProvider;>;"
        }
    .end annotation
.end field

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/groupcommerce/composer/ComposerSellController$Delegate;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/concurrent/ExecutorService;

.field public final i:LX/03V;

.field public final j:LX/34x;

.field public k:Lcom/facebook/ipc/composer/model/ProductItemPlace;

.field public l:J

.field public m:Z

.field public n:J


# direct methods
.method public constructor <init>(LX/DJT;LX/0j7;ZZZZJLjava/util/concurrent/ExecutorService;LX/0tX;LX/03V;LX/34x;)V
    .locals 3
    .param p1    # LX/DJT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0j7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groupcommerce/composer/ComposerSellController$Delegate;",
            "TDataProvider;ZZZZJ",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/34x;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1984596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1984597
    const/4 v0, 0x0

    iput-object v0, p0, LX/DIv;->k:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    .line 1984598
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/DIv;->l:J

    .line 1984599
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DIv;->m:Z

    .line 1984600
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/DIv;->n:J

    .line 1984601
    iput-object p10, p0, LX/DIv;->a:LX/0tX;

    .line 1984602
    iput-object p9, p0, LX/DIv;->h:Ljava/util/concurrent/ExecutorService;

    .line 1984603
    iput-boolean p3, p0, LX/DIv;->c:Z

    .line 1984604
    iput-boolean p4, p0, LX/DIv;->d:Z

    .line 1984605
    iput-boolean p5, p0, LX/DIv;->e:Z

    .line 1984606
    iput-boolean p6, p0, LX/DIv;->f:Z

    .line 1984607
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/DIv;->g:Ljava/lang/ref/WeakReference;

    .line 1984608
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/DIv;->b:Ljava/lang/ref/WeakReference;

    .line 1984609
    iput-object p11, p0, LX/DIv;->i:LX/03V;

    .line 1984610
    iput-wide p7, p0, LX/DIv;->n:J

    .line 1984611
    iput-object p12, p0, LX/DIv;->j:LX/34x;

    .line 1984612
    return-void
.end method

.method public static f(LX/DIv;)LX/0w7;
    .locals 6

    .prologue
    .line 1984560
    iget-object v0, p0, LX/DIv;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j7;

    .line 1984561
    invoke-interface {v0}, LX/0j7;->getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v0

    .line 1984562
    if-eqz v0, :cond_0

    .line 1984563
    new-instance v1, LX/0w7;

    invoke-direct {v1}, LX/0w7;-><init>()V

    const-string v2, "latitude"

    iget-wide v4, v0, Lcom/facebook/ipc/composer/model/ComposerLocation;->latitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0w7;

    move-result-object v1

    const-string v2, "longitude"

    iget-wide v4, v0, Lcom/facebook/ipc/composer/model/ComposerLocation;->longitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0w7;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0w7;

    move-result-object v0

    .line 1984564
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/0w7;

    invoke-direct {v0}, LX/0w7;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1984565
    iget-boolean v0, p0, LX/DIv;->c:Z

    if-nez v0, :cond_0

    .line 1984566
    :goto_0
    iget-boolean v0, p0, LX/DIv;->d:Z

    if-nez v0, :cond_1

    .line 1984567
    :goto_1
    iget-boolean v0, p0, LX/DIv;->e:Z

    if-nez v0, :cond_2

    .line 1984568
    :goto_2
    iget-boolean v0, p0, LX/DIv;->f:Z

    if-nez v0, :cond_3

    .line 1984569
    :goto_3
    return-void

    .line 1984570
    :cond_0
    invoke-static {}, LX/9Jh;->a()LX/9Jg;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-static {p0}, LX/DIv;->f(LX/DIv;)LX/0w7;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 1984571
    iget-object v1, p0, LX/DIv;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1984572
    new-instance v1, LX/DIr;

    invoke-direct {v1, p0}, LX/DIr;-><init>(LX/DIv;)V

    iget-object v2, p0, LX/DIv;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 1984573
    :cond_1
    new-instance v0, LX/9Js;

    invoke-direct {v0}, LX/9Js;-><init>()V

    move-object v0, v0

    .line 1984574
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-static {p0}, LX/DIv;->f(LX/DIv;)LX/0w7;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 1984575
    iget-object v1, p0, LX/DIv;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1984576
    new-instance v1, LX/DIq;

    invoke-direct {v1, p0}, LX/DIq;-><init>(LX/DIv;)V

    iget-object v2, p0, LX/DIv;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 1984577
    :cond_2
    invoke-static {}, LX/9Jo;->a()LX/9Jn;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1984578
    new-instance v1, LX/9K4;

    invoke-direct {v1}, LX/9K4;-><init>()V

    move-object v1, v1

    .line 1984579
    const-string v2, "group_id"

    iget-wide v4, p0, LX/DIv;->n:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1984580
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 1984581
    iget-object v2, p0, LX/DIv;->a:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1984582
    iget-object v2, p0, LX/DIv;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 1984583
    new-instance v2, LX/DIs;

    invoke-direct {v2, p0}, LX/DIs;-><init>(LX/DIv;)V

    iget-object v3, p0, LX/DIv;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1984584
    new-instance v0, LX/DIt;

    invoke-direct {v0, p0}, LX/DIt;-><init>(LX/DIv;)V

    iget-object v2, p0, LX/DIv;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_2

    .line 1984585
    :cond_3
    new-instance v0, LX/9K9;

    invoke-direct {v0}, LX/9K9;-><init>()V

    move-object v0, v0

    .line 1984586
    const-string v1, "image_size"

    const/16 v2, 0xa0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "can_post"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v2, "limit"

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "order"

    .line 1984587
    iget-object v4, p0, LX/DIv;->j:LX/34x;

    .line 1984588
    iget-object v5, v4, LX/34x;->c:LX/0W3;

    sget-wide v7, LX/0X5;->dJ:J

    invoke-interface {v5, v7, v8}, LX/0W4;->a(J)Z

    move-result v5

    move v4, v5

    .line 1984589
    if-eqz v4, :cond_4

    .line 1984590
    const-string v4, "city_forum_group"

    .line 1984591
    :goto_4
    move-object v3, v4

    .line 1984592
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1984593
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1984594
    iget-object v1, p0, LX/DIv;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1984595
    new-instance v1, LX/DIu;

    invoke-direct {v1, p0}, LX/DIu;-><init>(LX/DIv;)V

    iget-object v2, p0, LX/DIv;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_3

    :cond_4
    const-string v4, "viewer_visitation"

    goto :goto_4
.end method
