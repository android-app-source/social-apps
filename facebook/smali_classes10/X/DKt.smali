.class public LX/DKt;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/DLH;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field private c:Ljava/lang/String;

.field public d:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/DLH;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/DLK;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/DLK;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1987611
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1987612
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/DKt;->a:Ljava/util/List;

    .line 1987613
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/DKt;->e:Ljava/util/Map;

    .line 1987614
    iput-object p1, p0, LX/DKt;->c:Ljava/lang/String;

    .line 1987615
    iput-object p2, p0, LX/DKt;->f:LX/DLK;

    .line 1987616
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 13

    .prologue
    .line 1987600
    sget-object v0, LX/DKr;->FILE_OR_DOC_ROW:LX/DKr;

    invoke-virtual {v0}, LX/DKr;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 1987601
    new-instance v0, LX/DKq;

    iget-object v1, p0, LX/DKt;->f:LX/DLK;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/DKt;->d:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    .line 1987602
    new-instance v4, LX/DLJ;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v1}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v8

    check-cast v8, LX/6RZ;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    .line 1987603
    new-instance v6, LX/DLV;

    invoke-static {v1}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v5

    check-cast v5, LX/0hy;

    invoke-direct {v6, v5}, LX/DLV;-><init>(LX/0hy;)V

    .line 1987604
    move-object v10, v6

    .line 1987605
    check-cast v10, LX/DLU;

    const/16 v5, 0x1399

    invoke-static {v1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v5, 0xac0

    invoke-static {v1, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    move-object v5, v2

    move-object v6, v3

    invoke-direct/range {v4 .. v12}, LX/DLJ;-><init>(Landroid/content/Context;Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;Landroid/content/res/Resources;LX/6RZ;Lcom/facebook/content/SecureContextHelper;LX/DLU;LX/0Or;LX/0Ot;)V

    .line 1987606
    move-object v1, v4

    .line 1987607
    invoke-direct {v0, v1}, LX/DKq;-><init>(LX/DLJ;)V

    .line 1987608
    :goto_0
    return-object v0

    .line 1987609
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1987610
    new-instance v0, LX/DKs;

    const v2, 0x7f03080b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DKs;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(ILX/DLI;)V
    .locals 1

    .prologue
    .line 1987595
    iget-object v0, p0, LX/DKt;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DLH;

    .line 1987596
    if-nez v0, :cond_0

    .line 1987597
    :goto_0
    return-void

    .line 1987598
    :cond_0
    iput-object p2, v0, LX/DLH;->b:LX/DLI;

    .line 1987599
    invoke-virtual {p0, p1}, LX/1OM;->i_(I)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1987591
    instance-of v0, p1, LX/DKq;

    if-eqz v0, :cond_0

    .line 1987592
    check-cast p1, LX/DKq;

    iget-object v1, p1, LX/DKq;->l:LX/DLJ;

    .line 1987593
    iget-object v0, p0, LX/DKt;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DLH;

    iget-object v2, p0, LX/DKt;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, p2}, LX/DLJ;->a(LX/DLH;Ljava/lang/String;I)V

    .line 1987594
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/DLI;)V
    .locals 1

    .prologue
    .line 1987586
    iget-object v0, p0, LX/DKt;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DLH;

    .line 1987587
    if-nez v0, :cond_0

    .line 1987588
    :goto_0
    return-void

    .line 1987589
    :cond_0
    iput-object p2, v0, LX/DLH;->b:LX/DLI;

    .line 1987590
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1987580
    iget-object v0, p0, LX/DKt;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DLH;

    .line 1987581
    if-nez v0, :cond_1

    .line 1987582
    :cond_0
    :goto_0
    return-void

    .line 1987583
    :cond_1
    if-eqz p2, :cond_0

    .line 1987584
    iget-object v1, p0, LX/DKt;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1987585
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1987577
    iget-object v0, p0, LX/DKt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1987578
    sget-object v0, LX/DKr;->FILE_OR_DOC_ROW:LX/DKr;

    invoke-virtual {v0}, LX/DKr;->ordinal()I

    move-result v0

    .line 1987579
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/DKr;->LOADING_BAR:LX/DKr;

    invoke-virtual {v0}, LX/DKr;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1987576
    iget-object v0, p0, LX/DKt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-boolean v0, p0, LX/DKt;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
