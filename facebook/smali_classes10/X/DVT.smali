.class public final LX/DVT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Lcom/facebook/base/fragment/FbFragment;

.field public final synthetic d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;LX/0Px;LX/0Px;Lcom/facebook/base/fragment/FbFragment;)V
    .locals 0

    .prologue
    .line 2005027
    iput-object p1, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iput-object p2, p0, LX/DVT;->a:LX/0Px;

    iput-object p3, p0, LX/DVT;->b:LX/0Px;

    iput-object p4, p0, LX/DVT;->c:Lcom/facebook/base/fragment/FbFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2005081
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    .line 2005082
    iput-boolean v7, v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->V:Z

    .line 2005083
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    .line 2005084
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2005085
    if-eqz v0, :cond_1

    .line 2005086
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005087
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2005088
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->w:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->x:Landroid/content/res/Resources;

    const v3, 0x7f0f0156

    iget-object v4, p0, LX/DVT;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, LX/DVT;->a:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2005089
    :cond_0
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->W:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_1

    .line 2005090
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->W:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2005091
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2005028
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2005029
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    .line 2005030
    iput-boolean v1, v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->V:Z

    .line 2005031
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2005032
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005033
    if-eqz v0, :cond_5

    .line 2005034
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005035
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->a()LX/0Px;

    move-result-object v4

    .line 2005036
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;

    .line 2005037
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005038
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2005039
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005040
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->o()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$InvitedUsersModel;

    .line 2005041
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$InvitedUsersModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005042
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2005043
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005044
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AlreadyAddedUsersModel;

    .line 2005045
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AlreadyAddedUsersModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005046
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2005047
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005048
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->k()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AlreadyInvitedUsersModel;

    .line 2005049
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AlreadyInvitedUsersModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005050
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2005051
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005052
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->p()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$RequestedUsersModel;

    .line 2005053
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$RequestedUsersModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005054
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2005055
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005056
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->n()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_5
    if-ge v2, v5, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2005057
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005058
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 2005059
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2005060
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    .line 2005061
    iget-boolean v3, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v3

    .line 2005062
    if-eqz v0, :cond_7

    .line 2005063
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005064
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->W:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_6

    .line 2005065
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->W:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2005066
    :cond_6
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2005067
    iget-object v0, p0, LX/DVT;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v3, p0, LX/DVT;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/2addr v0, v3

    .line 2005068
    iget-object v3, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v3, v3, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->w:LX/0kL;

    new-instance v4, LX/27k;

    iget-object v5, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v5, v5, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->H:LX/DVb;

    invoke-virtual {v5, p1, v0}, LX/DVb;->a(Lcom/facebook/graphql/executor/GraphQLResult;I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v4}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2005069
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    if-ne v3, v0, :cond_8

    .line 2005070
    iget-object v1, p0, LX/DVT;->c:Lcom/facebook/base/fragment/FbFragment;

    .line 2005071
    instance-of v2, v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    if-eqz v2, :cond_a

    .line 2005072
    check-cast v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-virtual {v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->z()V

    .line 2005073
    :cond_7
    :goto_6
    return-void

    .line 2005074
    :cond_8
    iget-object v0, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    .line 2005075
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_7
    if-ge v1, v4, :cond_7

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2005076
    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2005077
    iget-object v5, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v6, p0, LX/DVT;->d:Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    iget-object v6, v6, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 2005078
    invoke-virtual {v5, v0, v6}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 2005079
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 2005080
    :cond_a
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_6
.end method
