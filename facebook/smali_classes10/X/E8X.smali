.class public final LX/E8X;
.super LX/4o9;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V
    .locals 0

    .prologue
    .line 2082791
    iput-object p1, p0, LX/E8X;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    invoke-direct {p0}, LX/4o9;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2082792
    iget-object v0, p0, LX/E8X;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    const/4 v1, 0x1

    .line 2082793
    iput-boolean v1, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->C:Z

    .line 2082794
    iget-object v0, p0, LX/E8X;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    .line 2082795
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2082796
    if-eqz v0, :cond_0

    .line 2082797
    iget-object v0, p0, LX/E8X;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    invoke-static {v0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->z(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    .line 2082798
    :cond_0
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 5

    .prologue
    .line 2082799
    iget-object v0, p0, LX/E8X;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-object v1, p0, LX/E8X;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-object v1, v1, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 2082800
    iput-wide v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->H:J

    .line 2082801
    iget-object v0, p0, LX/E8X;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-object v0, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->P:LX/2jY;

    .line 2082802
    iget-object v1, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2082803
    invoke-static {v0}, LX/2s8;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2082804
    iget-object v0, p0, LX/E8X;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    invoke-static {v0}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->u(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V

    .line 2082805
    :cond_0
    return-void
.end method
