.class public final LX/DYE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DML;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/DML",
        "<",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V
    .locals 0

    .prologue
    .line 2011347
    iput-object p1, p0, LX/DYE;->a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2011310
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2011311
    const v1, 0x7f03085a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2011312
    const v1, 0x7f0d15c3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2011313
    const v3, 0x7f03085b

    move v2, v3

    .line 2011314
    invoke-virtual {v1, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2011315
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2011316
    iget-object v1, p0, LX/DYE;->a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v1, v1, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    .line 2011317
    const v3, 0x7f0b207b

    move v2, v3

    .line 2011318
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2011319
    iget-object v1, p0, LX/DYE;->a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v1, v1, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    .line 2011320
    const v4, 0x7f0b207a

    move v3, v4

    .line 2011321
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2011322
    const v1, 0x7f0d15cd

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbButton;

    .line 2011323
    iget-object v4, p0, LX/DYE;->a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v4, v4, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    .line 2011324
    const p1, 0x106000b

    move v5, p1

    .line 2011325
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 2011326
    iget-object v4, p0, LX/DYE;->a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v4, v4, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    .line 2011327
    const p1, 0x7f020c83

    move v5, p1

    .line 2011328
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2011329
    invoke-virtual {v1, v3, v2, v3, v2}, Lcom/facebook/resources/ui/FbButton;->setPadding(IIII)V

    .line 2011330
    const v1, 0x7f0d15ce

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbButton;

    .line 2011331
    iget-object v4, p0, LX/DYE;->a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v4, v4, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    .line 2011332
    const p1, 0x106000c

    move v5, p1

    .line 2011333
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 2011334
    iget-object v4, p0, LX/DYE;->a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v4, v4, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    .line 2011335
    const p1, 0x7f020c86

    move v5, p1

    .line 2011336
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2011337
    invoke-virtual {v1, v3, v2, v3, v2}, Lcom/facebook/resources/ui/FbButton;->setPadding(IIII)V

    .line 2011338
    const v1, 0x7f0d15cf

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbButton;

    .line 2011339
    iget-object v4, p0, LX/DYE;->a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v4, v4, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    .line 2011340
    const p1, 0x106000c

    move v5, p1

    .line 2011341
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 2011342
    iget-object v4, p0, LX/DYE;->a:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v4, v4, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    .line 2011343
    const p0, 0x7f020c86

    move v5, p0

    .line 2011344
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2011345
    invoke-virtual {v1, v3, v2, v3, v2}, Lcom/facebook/resources/ui/FbButton;->setPadding(IIII)V

    .line 2011346
    return-object v0
.end method
