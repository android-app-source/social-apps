.class public final LX/DEs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2e2;


# direct methods
.method public constructor <init>(LX/2e2;)V
    .locals 0

    .prologue
    .line 1977387
    iput-object p1, p0, LX/DEs;->a:LX/2e2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1977388
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1977389
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1977390
    check-cast v0, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;

    .line 1977391
    if-nez v0, :cond_0

    .line 1977392
    const/4 v1, 0x0

    .line 1977393
    :goto_0
    move-object v0, v1

    .line 1977394
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->K()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v0

    return-object v0

    .line 1977395
    :cond_0
    new-instance v1, LX/4XR;

    invoke-direct {v1}, LX/4XR;-><init>()V

    .line 1977396
    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 1977397
    iput-object v2, v1, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1977398
    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->c()Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;

    move-result-object v2

    .line 1977399
    if-nez v2, :cond_1

    .line 1977400
    const/4 v3, 0x0

    .line 1977401
    :goto_1
    move-object v2, v3

    .line 1977402
    iput-object v2, v1, LX/4XR;->L:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    .line 1977403
    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowQueryModel;->b()I

    move-result v2

    .line 1977404
    iput v2, v1, LX/4XR;->ff:I

    .line 1977405
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    goto :goto_0

    .line 1977406
    :cond_1
    new-instance v5, LX/4Xr;

    invoke-direct {v5}, LX/4Xr;-><init>()V

    .line 1977407
    invoke-virtual {v2}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;->b()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1977408
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1977409
    const/4 v3, 0x0

    move v4, v3

    :goto_2
    invoke-virtual {v2}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_2

    .line 1977410
    invoke-virtual {v2}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel;

    .line 1977411
    if-nez v3, :cond_4

    .line 1977412
    const/4 v7, 0x0

    .line 1977413
    :goto_3
    move-object v3, v7

    .line 1977414
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1977415
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 1977416
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1977417
    iput-object v3, v5, LX/4Xr;->b:LX/0Px;

    .line 1977418
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel;->a()LX/0us;

    move-result-object v3

    .line 1977419
    if-nez v3, :cond_8

    .line 1977420
    const/4 v4, 0x0

    .line 1977421
    :goto_4
    move-object v3, v4

    .line 1977422
    iput-object v3, v5, LX/4Xr;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1977423
    invoke-virtual {v5}, LX/4Xr;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    move-result-object v3

    goto :goto_1

    .line 1977424
    :cond_4
    new-instance v7, LX/4Xs;

    invoke-direct {v7}, LX/4Xs;-><init>()V

    .line 1977425
    invoke-virtual {v3}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel;->b()Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    move-result-object v8

    .line 1977426
    if-nez v8, :cond_5

    .line 1977427
    const/4 v9, 0x0

    .line 1977428
    :goto_5
    move-object v8, v9

    .line 1977429
    iput-object v8, v7, LX/4Xs;->b:Lcom/facebook/graphql/model/GraphQLUser;

    .line 1977430
    invoke-virtual {v3}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel;->a()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 1977431
    if-nez v8, :cond_7

    .line 1977432
    const/4 p0, 0x0

    .line 1977433
    :goto_6
    move-object v8, p0

    .line 1977434
    iput-object v8, v7, LX/4Xs;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1977435
    invoke-virtual {v3}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel;->c()Ljava/lang/String;

    move-result-object v8

    .line 1977436
    iput-object v8, v7, LX/4Xs;->d:Ljava/lang/String;

    .line 1977437
    new-instance v8, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-direct {v8, v7}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;-><init>(LX/4Xs;)V

    .line 1977438
    move-object v7, v8

    .line 1977439
    goto :goto_3

    .line 1977440
    :cond_5
    new-instance v9, LX/33O;

    invoke-direct {v9}, LX/33O;-><init>()V

    .line 1977441
    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object p0

    .line 1977442
    iput-object p0, v9, LX/33O;->P:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1977443
    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object p0

    .line 1977444
    iput-object p0, v9, LX/33O;->U:Ljava/lang/String;

    .line 1977445
    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object p0

    .line 1977446
    iput-object p0, v9, LX/33O;->aI:Ljava/lang/String;

    .line 1977447
    invoke-virtual {v8}, Lcom/facebook/friends/protocol/FetchPaginatedPeopleYouMayKnowGraphQLModels$PaginatedPeopleYouMayKnowFeedUnitUsersConnectionModel$EdgesModel$NodeModel;->e()LX/1Fb;

    move-result-object p0

    .line 1977448
    if-nez p0, :cond_6

    .line 1977449
    const/4 p1, 0x0

    .line 1977450
    :goto_7
    move-object p0, p1

    .line 1977451
    iput-object p0, v9, LX/33O;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1977452
    invoke-virtual {v9}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v9

    goto :goto_5

    .line 1977453
    :cond_6
    new-instance p1, LX/2dc;

    invoke-direct {p1}, LX/2dc;-><init>()V

    .line 1977454
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v8

    .line 1977455
    iput v8, p1, LX/2dc;->c:I

    .line 1977456
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v8

    .line 1977457
    iput-object v8, p1, LX/2dc;->h:Ljava/lang/String;

    .line 1977458
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v8

    .line 1977459
    iput v8, p1, LX/2dc;->i:I

    .line 1977460
    invoke-virtual {p1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    goto :goto_7

    .line 1977461
    :cond_7
    new-instance p0, LX/173;

    invoke-direct {p0}, LX/173;-><init>()V

    .line 1977462
    const/4 p1, 0x0

    invoke-virtual {v9, v8, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object p1

    .line 1977463
    iput-object p1, p0, LX/173;->f:Ljava/lang/String;

    .line 1977464
    invoke-virtual {p0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    goto :goto_6

    .line 1977465
    :cond_8
    new-instance v4, LX/17L;

    invoke-direct {v4}, LX/17L;-><init>()V

    .line 1977466
    invoke-interface {v3}, LX/0us;->a()Ljava/lang/String;

    move-result-object v6

    .line 1977467
    iput-object v6, v4, LX/17L;->c:Ljava/lang/String;

    .line 1977468
    invoke-interface {v3}, LX/0us;->b()Z

    move-result v6

    .line 1977469
    iput-boolean v6, v4, LX/17L;->d:Z

    .line 1977470
    invoke-interface {v3}, LX/0us;->c()Z

    move-result v6

    .line 1977471
    iput-boolean v6, v4, LX/17L;->e:Z

    .line 1977472
    invoke-interface {v3}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v6

    .line 1977473
    iput-object v6, v4, LX/17L;->f:Ljava/lang/String;

    .line 1977474
    invoke-virtual {v4}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    goto/16 :goto_4
.end method
