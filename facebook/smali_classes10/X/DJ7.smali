.class public final LX/DJ7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

.field private final b:LX/DJ8;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;LX/DJ8;)V
    .locals 0

    .prologue
    .line 1984877
    iput-object p1, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1984878
    iput-object p2, p0, LX/DJ7;->b:LX/DJ8;

    .line 1984879
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x2

    const v1, -0x270e07c7

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1984880
    iget-object v1, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    iget-object v2, p0, LX/DJ7;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1984881
    iget-object v1, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    iget-object v2, p0, LX/DJ7;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1984882
    iget-object v1, p0, LX/DJ7;->b:LX/DJ8;

    iget-object v1, v1, LX/DJ8;->m:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1984883
    :goto_0
    iget-object v1, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->d:LX/DJH;

    if-eqz v1, :cond_0

    .line 1984884
    iget-object v1, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->d:LX/DJH;

    invoke-interface {v1}, LX/DJH;->a()V

    .line 1984885
    :cond_0
    const v1, -0x951ef87

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1984886
    :cond_1
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 1984887
    iget-object v1, p0, LX/DJ7;->c:Ljava/lang/String;

    const-string v6, "0"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/DJ7;->c:Ljava/lang/String;

    iget-object v6, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    iget-object v6, v6, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->g:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1984888
    :cond_2
    :goto_1
    move v1, v2

    .line 1984889
    if-eqz v1, :cond_3

    .line 1984890
    iget-object v1, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    iget-object v2, p0, LX/DJ7;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1984891
    iget-object v1, p0, LX/DJ7;->b:LX/DJ8;

    iget-object v1, v1, LX/DJ8;->m:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1984892
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1984893
    const v2, 0x7f082f2a

    new-array v3, v3, [Ljava/lang/Object;

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1984894
    :cond_4
    iget-object v1, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    invoke-virtual {v1}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    .line 1984895
    :goto_2
    iget-object v6, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    iget-object v6, v6, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    iget-object v7, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    iget-object v7, v7, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->g:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1984896
    add-int/lit8 v1, v1, 0x1

    .line 1984897
    :cond_5
    iget-object v6, p0, LX/DJ7;->a:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    iget-object v6, v6, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    add-int/lit8 v1, v1, 0xa

    if-lt v6, v1, :cond_2

    move v2, v4

    goto :goto_1

    :cond_6
    move v1, v4

    goto :goto_2
.end method
