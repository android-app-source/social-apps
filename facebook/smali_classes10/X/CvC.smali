.class public LX/CvC;
.super LX/16T;
.source ""

# interfaces
.implements LX/Cv0;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static d:LX/0hs;

.field private static volatile e:LX/CvC;


# instance fields
.field public final a:LX/0tQ;

.field public final b:LX/0iA;

.field public c:Z


# direct methods
.method public constructor <init>(LX/0tQ;LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1947825
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1947826
    iput-object p1, p0, LX/CvC;->a:LX/0tQ;

    .line 1947827
    iput-object p2, p0, LX/CvC;->b:LX/0iA;

    .line 1947828
    return-void
.end method

.method public static a(LX/0QB;)LX/CvC;
    .locals 5

    .prologue
    .line 1947829
    sget-object v0, LX/CvC;->e:LX/CvC;

    if-nez v0, :cond_1

    .line 1947830
    const-class v1, LX/CvC;

    monitor-enter v1

    .line 1947831
    :try_start_0
    sget-object v0, LX/CvC;->e:LX/CvC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1947832
    if-eqz v2, :cond_0

    .line 1947833
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1947834
    new-instance p0, LX/CvC;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v3

    check-cast v3, LX/0tQ;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v4

    check-cast v4, LX/0iA;

    invoke-direct {p0, v3, v4}, LX/CvC;-><init>(LX/0tQ;LX/0iA;)V

    .line 1947835
    move-object v0, p0

    .line 1947836
    sput-object v0, LX/CvC;->e:LX/CvC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1947837
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1947838
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1947839
    :cond_1
    sget-object v0, LX/CvC;->e:LX/CvC;

    return-object v0

    .line 1947840
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1947841
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1947842
    iget-object v0, p0, LX/CvC;->a:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CvC;->a:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/CvC;->c:Z

    if-nez v0, :cond_0

    .line 1947843
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 1947844
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(LX/1A0;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1947845
    sget-object v0, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne p1, v0, :cond_0

    sget-object v0, LX/CvC;->d:LX/0hs;

    if-eqz v0, :cond_0

    sget-object v0, LX/CvC;->d:LX/0hs;

    .line 1947846
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 1947847
    if-nez v0, :cond_1

    :cond_0
    sget-object v0, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne p1, v0, :cond_2

    iget-boolean v0, p0, LX/CvC;->c:Z

    if-eqz v0, :cond_2

    .line 1947848
    :cond_1
    :goto_0
    return-void

    .line 1947849
    :cond_2
    new-instance v0, LX/0hs;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1947850
    sput-object v0, LX/CvC;->d:LX/0hs;

    const/4 v1, -0x1

    .line 1947851
    iput v1, v0, LX/0hs;->t:I

    .line 1947852
    sget-object v0, LX/CvC;->d:LX/0hs;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1947853
    sget-object v2, LX/CvB;->a:[I

    iget-object p1, p0, LX/CvC;->a:LX/0tQ;

    invoke-virtual {p1}, LX/0tQ;->m()LX/2qY;

    move-result-object p1

    invoke-virtual {p1}, LX/2qY;->ordinal()I

    move-result p1

    aget v2, v2, p1

    packed-switch v2, :pswitch_data_0

    .line 1947854
    const v2, 0x7f080d3f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_1
    move-object v1, v2

    .line 1947855
    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1947856
    sget-object v0, LX/CvC;->d:LX/0hs;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1947857
    sget-object v2, LX/CvB;->a:[I

    iget-object p1, p0, LX/CvC;->a:LX/0tQ;

    invoke-virtual {p1}, LX/0tQ;->m()LX/2qY;

    move-result-object p1

    invoke-virtual {p1}, LX/2qY;->ordinal()I

    move-result p1

    aget v2, v2, p1

    packed-switch v2, :pswitch_data_1

    .line 1947858
    const v2, 0x7f080d41

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_2
    move-object v1, v2

    .line 1947859
    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1947860
    sget-object v0, LX/CvC;->d:LX/0hs;

    invoke-virtual {v0, p2}, LX/0ht;->f(Landroid/view/View;)V

    .line 1947861
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CvC;->c:Z

    .line 1947862
    iget-object v0, p0, LX/CvC;->b:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4345"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1947863
    goto :goto_0

    .line 1947864
    :pswitch_0
    const v2, 0x7f080d3e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_1

    .line 1947865
    :pswitch_1
    const v2, 0x7f080d40

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_1

    .line 1947866
    :pswitch_2
    const v2, 0x7f080d42

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1947867
    const-string v0, "4345"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1947868
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_COMPLETE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
