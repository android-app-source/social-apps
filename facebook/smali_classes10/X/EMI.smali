.class public LX/EMI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/CxV;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EMK;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EMI",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EMK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110511
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2110512
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/EMI;->b:LX/0Zi;

    .line 2110513
    iput-object p1, p0, LX/EMI;->a:LX/0Ot;

    .line 2110514
    return-void
.end method

.method public static a(LX/0QB;)LX/EMI;
    .locals 4

    .prologue
    .line 2110500
    const-class v1, LX/EMI;

    monitor-enter v1

    .line 2110501
    :try_start_0
    sget-object v0, LX/EMI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110502
    sput-object v2, LX/EMI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110503
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110504
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110505
    new-instance v3, LX/EMI;

    const/16 p0, 0x3413

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EMI;-><init>(LX/0Ot;)V

    .line 2110506
    move-object v0, v3

    .line 2110507
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110508
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EMI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110509
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110510
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2110515
    check-cast p2, LX/EMH;

    .line 2110516
    iget-object v0, p0, LX/EMI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EMK;

    iget-boolean v1, p2, LX/EMH;->a:Z

    .line 2110517
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    iget-object v2, v0, LX/EMK;->a:LX/1vg;

    invoke-virtual {v2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v2

    const p0, 0x7f020781

    invoke-virtual {v2, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object p0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    if-eqz v1, :cond_0

    const v2, 0x7f0a008a

    :goto_0
    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p0, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    const p0, 0x7f0b1713

    invoke-interface {v2, v3, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b1714

    invoke-interface {v2, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b1714

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    .line 2110518
    const v3, -0x60672871

    const/4 p0, 0x0

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2110519
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2110520
    return-object v0

    :cond_0
    const v2, 0x7f0a00a3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2110491
    invoke-static {}, LX/1dS;->b()V

    .line 2110492
    iget v0, p1, LX/1dQ;->b:I

    .line 2110493
    packed-switch v0, :pswitch_data_0

    .line 2110494
    :goto_0
    return-object v2

    .line 2110495
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2110496
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2110497
    check-cast v1, LX/EMH;

    .line 2110498
    iget-object v3, p0, LX/EMI;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EMK;

    iget-object v4, v1, LX/EMH;->b:LX/CzL;

    iget-object v5, v1, LX/EMH;->c:LX/CxA;

    iget-object p1, v1, LX/EMH;->d:Ljava/lang/String;

    iget-boolean p2, v1, LX/EMH;->a:Z

    invoke-virtual {v3, v4, v5, p1, p2}, LX/EMK;->a(LX/CzL;LX/CxA;Ljava/lang/String;Z)V

    .line 2110499
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x60672871
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/EMG;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/EMI",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2110483
    new-instance v1, LX/EMH;

    invoke-direct {v1, p0}, LX/EMH;-><init>(LX/EMI;)V

    .line 2110484
    iget-object v2, p0, LX/EMI;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EMG;

    .line 2110485
    if-nez v2, :cond_0

    .line 2110486
    new-instance v2, LX/EMG;

    invoke-direct {v2, p0}, LX/EMG;-><init>(LX/EMI;)V

    .line 2110487
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EMG;->a$redex0(LX/EMG;LX/1De;IILX/EMH;)V

    .line 2110488
    move-object v1, v2

    .line 2110489
    move-object v0, v1

    .line 2110490
    return-object v0
.end method
