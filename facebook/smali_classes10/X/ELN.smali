.class public final LX/ELN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8d9;

.field public final synthetic b:LX/ELP;


# direct methods
.method public constructor <init>(LX/ELP;LX/8d9;)V
    .locals 0

    .prologue
    .line 2108783
    iput-object p1, p0, LX/ELN;->b:LX/ELP;

    iput-object p2, p0, LX/ELN;->a:LX/8d9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2108784
    iget-object v0, p0, LX/ELN;->b:LX/ELP;

    iget-object v0, v0, LX/ELP;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyD;

    iget-object v1, p0, LX/ELN;->a:LX/8d9;

    invoke-interface {v1}, LX/8d9;->dW_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/ELN;->a:LX/8d9;

    invoke-interface {v2}, LX/8d9;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    iget-object v3, p0, LX/ELN;->a:LX/8d9;

    invoke-interface {v3}, LX/8d9;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    iget-object v4, p0, LX/ELN;->a:LX/8d9;

    invoke-interface {v4}, LX/8d9;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/CyD;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
