.class public final LX/Dli;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;)V
    .locals 0

    .prologue
    .line 2038550
    iput-object p1, p0, LX/Dli;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x2fc871e6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2038543
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dlk;

    .line 2038544
    iget-object v2, p0, LX/Dli;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->d:LX/Dll;

    iget-object v2, v2, LX/Dll;->a:LX/Dlk;

    if-eq v0, v2, :cond_1

    .line 2038545
    iget-object v2, p0, LX/Dli;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->c:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    if-eqz v2, :cond_0

    .line 2038546
    iget-object v2, p0, LX/Dli;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->c:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v0}, LX/Dlk;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2038547
    :cond_0
    iget-object v2, p0, LX/Dli;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;

    .line 2038548
    invoke-static {v2, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCalendarTabsView;LX/Dlk;)V

    .line 2038549
    :cond_1
    const v0, -0x45601849

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
