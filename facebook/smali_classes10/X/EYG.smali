.class public final LX/EYG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/EYE;

.field private final b:I


# direct methods
.method public constructor <init>(LX/EYE;I)V
    .locals 0

    .prologue
    .line 2136863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2136864
    iput-object p1, p0, LX/EYG;->a:LX/EYE;

    .line 2136865
    iput p2, p0, LX/EYG;->b:I

    .line 2136866
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2136859
    instance-of v1, p1, LX/EYG;

    if-nez v1, :cond_1

    .line 2136860
    :cond_0
    :goto_0
    return v0

    .line 2136861
    :cond_1
    check-cast p1, LX/EYG;

    .line 2136862
    iget-object v1, p0, LX/EYG;->a:LX/EYE;

    iget-object v2, p1, LX/EYG;->a:LX/EYE;

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/EYG;->b:I

    iget v2, p1, LX/EYG;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2136867
    iget-object v0, p0, LX/EYG;->a:LX/EYE;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0xffff

    mul-int/2addr v0, v1

    iget v1, p0, LX/EYG;->b:I

    add-int/2addr v0, v1

    return v0
.end method
