.class public LX/Dof;
.super LX/Dod;
.source ""


# instance fields
.field private final a:LX/2Oy;


# direct methods
.method public constructor <init>(LX/0Or;LX/2Oy;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;",
            "LX/2Oy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042247
    const-string v0, "properties"

    sget-object v1, LX/3Rq;->a:LX/0U1;

    sget-object v2, LX/3Rq;->b:LX/0U1;

    invoke-direct {p0, p1, v0, v1, v2}, LX/Dod;-><init>(LX/0Or;Ljava/lang/String;LX/0U1;LX/0U1;)V

    .line 2042248
    iput-object p2, p0, LX/Dof;->a:LX/2Oy;

    .line 2042249
    return-void
.end method

.method public static b(LX/0QB;)LX/Dof;
    .locals 3

    .prologue
    .line 2042250
    new-instance v1, LX/Dof;

    const/16 v0, 0xdc6

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/2Oy;->a(LX/0QB;)LX/2Oy;

    move-result-object v0

    check-cast v0, LX/2Oy;

    invoke-direct {v1, v2, v0}, LX/Dof;-><init>(LX/0Or;LX/2Oy;)V

    .line 2042251
    return-object v1
.end method


# virtual methods
.method public final a(LX/2PC;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2042252
    invoke-virtual {p0, p1}, LX/Dod;->b(LX/2PC;)[B

    move-result-object v1

    .line 2042253
    if-nez v1, :cond_0

    .line 2042254
    const/4 v0, 0x0

    .line 2042255
    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, LX/Dof;->a:LX/2Oy;

    invoke-virtual {v2, v1}, LX/2Oy;->b([B)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2042256
    :catch_0
    move-exception v0

    .line 2042257
    :goto_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to decrypt db property"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2042258
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final a(LX/2PC;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2042259
    const/4 v0, 0x0

    .line 2042260
    if-eqz p2, :cond_0

    .line 2042261
    :try_start_0
    iget-object v0, p0, LX/Dof;->a:LX/2Oy;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Oy;->a([B)[B
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 2042262
    :cond_0
    invoke-virtual {p0, p1, v0}, LX/Dod;->a(LX/2PC;[B)V

    .line 2042263
    return-void

    .line 2042264
    :catch_0
    move-exception v0

    .line 2042265
    :goto_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to encrypt db property"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2042266
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method
