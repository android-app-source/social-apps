.class public LX/CyV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CyQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/CyQ",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final b:Landroid/content/res/Resources;

.field private final c:LX/Cvk;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;Landroid/content/res/Resources;LX/Cvk;)V
    .locals 0

    .prologue
    .line 1953019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1953020
    iput-object p1, p0, LX/CyV;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1953021
    iput-object p2, p0, LX/CyV;->b:Landroid/content/res/Resources;

    .line 1953022
    iput-object p3, p0, LX/CyV;->c:LX/Cvk;

    .line 1953023
    return-void
.end method

.method public static a(LX/8cK;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 1

    .prologue
    .line 1952803
    iget-object v0, p0, LX/8cI;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1952804
    invoke-static {v0}, LX/38I;->a(Ljava/lang/String;)I

    move-result v0

    .line 1952805
    sparse-switch v0, :sswitch_data_0

    .line 1952806
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1952807
    :sswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 1952808
    :sswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 1952809
    :sswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 1952810
    :sswitch_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 1952811
    :sswitch_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 1952812
    :sswitch_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3ff252d0 -> :sswitch_5
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
        0x499e8e7 -> :sswitch_4
    .end sparse-switch
.end method

.method public static b(LX/8cK;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1953009
    iget-object v0, p0, LX/8cI;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1953010
    invoke-static {v0}, LX/38I;->a(Ljava/lang/String;)I

    move-result v0

    .line 1953011
    sparse-switch v0, :sswitch_data_0

    .line 1953012
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1953013
    :sswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_USER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1953014
    :sswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1953015
    :sswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1953016
    :sswitch_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1953017
    :sswitch_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1953018
    :sswitch_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3ff252d0 -> :sswitch_5
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
        0x499e8e7 -> :sswitch_4
    .end sparse-switch
.end method

.method private c(Ljava/util/List;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8cK;",
            ">;)",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;"
        }
    .end annotation

    .prologue
    .line 1952839
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 1952840
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, LX/8cK;

    .line 1952841
    iget-object v0, v9, LX/8cI;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1952842
    invoke-static {v0}, LX/38I;->a(Ljava/lang/String;)I

    move-result v11

    .line 1952843
    new-instance v7, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1952844
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 1952845
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cK;

    .line 1952846
    iget-object v2, v0, LX/8cI;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1952847
    invoke-static {v2}, LX/38I;->a(Ljava/lang/String;)I

    move-result v2

    if-ne v11, v2, :cond_0

    .line 1952848
    iget-object v2, v0, LX/8cI;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1952849
    invoke-virtual {v7, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1952850
    iget-object v2, v0, LX/8cI;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1952851
    invoke-virtual {v12, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1952852
    :cond_1
    invoke-static {v9}, LX/CyV;->b(LX/8cK;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 1952853
    invoke-static {v9}, LX/CyV;->a(LX/8cK;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    .line 1952854
    iget-object v0, p0, LX/CyV;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/7BG;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1952855
    new-instance v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1952856
    iget-object v0, v9, LX/8cI;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1952857
    invoke-direct {v5, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    .line 1952858
    new-instance v0, LX/Cw2;

    invoke-virtual {v12}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3}, LX/7CJ;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Cw4;

    iget-object v8, p0, LX/CyV;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {v4, v8}, LX/Cw4;-><init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    invoke-static {v5}, LX/7CN;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "bootstrap"

    invoke-direct/range {v0 .. v8}, LX/Cw2;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/Integer;Ljava/lang/String;LX/Cw4;Ljava/lang/String;Ljava/lang/String;LX/162;Ljava/lang/String;)V

    .line 1952859
    invoke-virtual {v12}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1952860
    invoke-virtual {v12, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8cK;

    .line 1952861
    iget-object v5, v3, LX/8cI;->d:Ljava/lang/String;

    move-object v5, v5

    .line 1952862
    invoke-static {v5}, LX/38I;->a(Ljava/lang/String;)I

    move-result v5

    if-ne v11, v5, :cond_2

    .line 1952863
    iget-object v5, p0, LX/CyV;->c:LX/Cvk;

    new-instance v6, LX/Cw0;

    invoke-direct {v6, v2, v0}, LX/Cw0;-><init>(Ljava/lang/String;LX/Cw2;)V

    invoke-virtual {v5, v6}, LX/Cvk;->a(LX/Cw0;)Ljava/lang/String;

    move-result-object v2

    .line 1952864
    new-instance v5, LX/8dV;

    invoke-direct {v5}, LX/8dV;-><init>()V

    .line 1952865
    iput-object v2, v5, LX/8dV;->a:Ljava/lang/String;

    .line 1952866
    move-object v2, v5

    .line 1952867
    new-instance v5, LX/173;

    invoke-direct {v5}, LX/173;-><init>()V

    .line 1952868
    iget-object v6, v3, LX/8cI;->c:Ljava/lang/String;

    move-object v6, v6

    .line 1952869
    iput-object v6, v5, LX/173;->f:Ljava/lang/String;

    .line 1952870
    move-object v5, v5

    .line 1952871
    invoke-virtual {v5}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 1952872
    iget-object v6, v3, LX/8cI;->d:Ljava/lang/String;

    move-object v6, v6

    .line 1952873
    invoke-static {v6}, LX/38I;->a(Ljava/lang/String;)I

    move-result v6

    .line 1952874
    new-instance v8, LX/8dX;

    invoke-direct {v8}, LX/8dX;-><init>()V

    .line 1952875
    iget-object v13, v3, LX/8cI;->a:Ljava/lang/String;

    move-object v13, v13

    .line 1952876
    iput-object v13, v8, LX/8dX;->L:Ljava/lang/String;

    .line 1952877
    move-object v8, v8

    .line 1952878
    new-instance v13, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;

    invoke-direct {v13}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;-><init>()V

    .line 1952879
    iput v6, v13, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a:I

    .line 1952880
    move-object v13, v13

    .line 1952881
    invoke-virtual {v13}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    .line 1952882
    iput-object v13, v8, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1952883
    move-object v8, v8

    .line 1952884
    iget-object v13, v3, LX/8cI;->b:Ljava/lang/String;

    move-object v13, v13

    .line 1952885
    iput-object v13, v8, LX/8dX;->ad:Ljava/lang/String;

    .line 1952886
    move-object v8, v8

    .line 1952887
    new-instance v13, LX/4aO;

    invoke-direct {v13}, LX/4aO;-><init>()V

    .line 1952888
    iget-object p1, v3, LX/8cK;->c:Ljava/lang/String;

    move-object p1, p1

    .line 1952889
    iput-object p1, v13, LX/4aO;->c:Ljava/lang/String;

    .line 1952890
    move-object v13, v13

    .line 1952891
    invoke-virtual {v13}, LX/4aO;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v13

    .line 1952892
    iput-object v13, v8, LX/8dX;->aw:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1952893
    move-object v8, v8

    .line 1952894
    iget-boolean v13, v3, LX/8cK;->d:Z

    move v13, v13

    .line 1952895
    iput-boolean v13, v8, LX/8dX;->W:Z

    .line 1952896
    move-object v8, v8

    .line 1952897
    sparse-switch v6, :sswitch_data_0

    .line 1952898
    :goto_2
    invoke-virtual {v8}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v5

    move-object v3, v5

    .line 1952899
    iput-object v3, v2, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1952900
    move-object v2, v2

    .line 1952901
    invoke-virtual {v2}, LX/8dV;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    move-result-object v2

    invoke-virtual {v10, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 1952902
    :cond_3
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1952903
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 1952904
    const-string v4, "bootstrap_item_count"

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v4

    const-string v5, "bootstrap_item_ids"

    invoke-virtual {v4, v5, v7}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1952905
    iget-object v4, p0, LX/CyV;->c:LX/Cvk;

    invoke-virtual {v4, v0}, LX/Cvk;->a(LX/Cw2;)Ljava/lang/String;

    move-result-object v0

    .line 1952906
    new-instance v4, LX/8dO;

    invoke-direct {v4}, LX/8dO;-><init>()V

    .line 1952907
    iput-object v0, v4, LX/8dO;->c:Ljava/lang/String;

    .line 1952908
    move-object v0, v4

    .line 1952909
    new-instance v4, LX/8dQ;

    invoke-direct {v4}, LX/8dQ;-><init>()V

    invoke-static {v9}, LX/CyV;->a(LX/8cK;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v5

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1952910
    iput-object v5, v4, LX/8dQ;->q:LX/0Px;

    .line 1952911
    move-object v4, v4

    .line 1952912
    iput-object v1, v4, LX/8dQ;->Z:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1952913
    move-object v1, v4

    .line 1952914
    new-instance v4, LX/8dU;

    invoke-direct {v4}, LX/8dU;-><init>()V

    .line 1952915
    iput-object v2, v4, LX/8dU;->a:LX/0Px;

    .line 1952916
    move-object v2, v4

    .line 1952917
    invoke-virtual {v2}, LX/8dU;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v2

    .line 1952918
    iput-object v2, v1, LX/8dQ;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    .line 1952919
    move-object v1, v1

    .line 1952920
    iget-object v2, v9, LX/8cI;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1952921
    invoke-static {v2}, LX/38I;->a(Ljava/lang/String;)I

    move-result v2

    .line 1952922
    sparse-switch v2, :sswitch_data_1

    .line 1952923
    const/4 v2, 0x0

    :goto_3
    move-object v2, v2

    .line 1952924
    iput-object v2, v1, LX/8dQ;->ab:Ljava/lang/String;

    .line 1952925
    move-object v1, v1

    .line 1952926
    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1952927
    iput-object v2, v1, LX/8dQ;->A:Ljava/lang/String;

    .line 1952928
    move-object v1, v1

    .line 1952929
    new-instance v2, LX/8dq;

    invoke-direct {v2}, LX/8dq;-><init>()V

    invoke-static {v9}, LX/CyV;->a(LX/8cK;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    iget-object v4, p0, LX/CyV;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/7BG;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1952930
    iput-object v3, v2, LX/8dq;->c:Ljava/lang/String;

    .line 1952931
    move-object v2, v2

    .line 1952932
    new-instance v3, LX/8dr;

    invoke-direct {v3}, LX/8dr;-><init>()V

    iget-object v4, p0, LX/CyV;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v4

    .line 1952933
    iput-object v4, v3, LX/8dr;->a:Ljava/lang/String;

    .line 1952934
    move-object v3, v3

    .line 1952935
    invoke-virtual {v3}, LX/8dr;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel$QueryTitleModel;

    move-result-object v3

    .line 1952936
    iput-object v3, v2, LX/8dq;->e:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel$QueryTitleModel;

    .line 1952937
    move-object v2, v2

    .line 1952938
    invoke-static {v9}, LX/CyV;->a(LX/8cK;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 1952939
    iput-object v3, v2, LX/8dq;->f:LX/0Px;

    .line 1952940
    move-object v2, v2

    .line 1952941
    invoke-virtual {v2}, LX/8dq;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-result-object v2

    .line 1952942
    iput-object v2, v1, LX/8dQ;->av:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    .line 1952943
    move-object v1, v1

    .line 1952944
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1952945
    sparse-switch v11, :sswitch_data_2

    .line 1952946
    const/4 v3, 0x0

    :goto_4
    move v3, v3

    .line 1952947
    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1952948
    iput-object v2, v1, LX/8dQ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1952949
    move-object v1, v1

    .line 1952950
    invoke-virtual {v1}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    .line 1952951
    iput-object v1, v0, LX/8dO;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 1952952
    move-object v0, v0

    .line 1952953
    invoke-virtual {v0}, LX/8dO;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    return-object v0

    .line 1952954
    :sswitch_0
    iget-object v6, v3, LX/8cK;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v6, v6

    .line 1952955
    iput-object v6, v8, LX/8dX;->F:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1952956
    move-object v6, v8

    .line 1952957
    new-instance v13, LX/8hU;

    invoke-direct {v13}, LX/8hU;-><init>()V

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    .line 1952958
    iput-object v5, v13, LX/8hU;->a:Ljava/lang/String;

    .line 1952959
    move-object v5, v13

    .line 1952960
    invoke-virtual {v5}, LX/8hU;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v5

    .line 1952961
    iput-object v5, v6, LX/8dX;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1952962
    goto/16 :goto_2

    .line 1952963
    :sswitch_1
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1952964
    iget-object v6, v3, LX/8cK;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1952965
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 1952966
    iget-object v6, v3, LX/8cK;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1952967
    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1952968
    :cond_4
    :goto_5
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1952969
    iput-object v5, v8, LX/8dX;->aD:LX/0Px;

    .line 1952970
    move-object v5, v8

    .line 1952971
    const/4 v6, 0x1

    .line 1952972
    iput-boolean v6, v5, LX/8dX;->l:Z

    .line 1952973
    move-object v5, v5

    .line 1952974
    iget-boolean v6, v3, LX/8cK;->g:Z

    move v6, v6

    .line 1952975
    iput-boolean v6, v5, LX/8dX;->u:Z

    .line 1952976
    goto/16 :goto_2

    .line 1952977
    :cond_5
    iget-object v6, v3, LX/8cI;->c:Ljava/lang/String;

    move-object v6, v6

    .line 1952978
    if-eqz v6, :cond_4

    .line 1952979
    iget-object v6, v3, LX/8cI;->c:Ljava/lang/String;

    move-object v6, v6

    .line 1952980
    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 1952981
    :sswitch_2
    new-instance v6, LX/8gh;

    invoke-direct {v6}, LX/8gh;-><init>()V

    .line 1952982
    iget-object v13, v3, LX/8cK;->h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object v13, v13

    .line 1952983
    iput-object v13, v8, LX/8dX;->ba:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1952984
    move-object v13, v8

    .line 1952985
    invoke-virtual {v6}, LX/8gh;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v6

    .line 1952986
    iput-object v6, v13, LX/8dX;->G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1952987
    move-object v6, v13

    .line 1952988
    new-instance v13, LX/8gl;

    invoke-direct {v13}, LX/8gl;-><init>()V

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    .line 1952989
    iput-object v5, v13, LX/8gl;->a:Ljava/lang/String;

    .line 1952990
    move-object v5, v13

    .line 1952991
    invoke-virtual {v5}, LX/8gl;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v5

    .line 1952992
    iput-object v5, v6, LX/8dX;->aF:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1952993
    goto/16 :goto_2

    .line 1952994
    :sswitch_3
    iget-object v5, v3, LX/8cI;->c:Ljava/lang/String;

    move-object v5, v5

    .line 1952995
    iput-object v5, v8, LX/8dX;->aP:Ljava/lang/String;

    .line 1952996
    goto/16 :goto_2

    .line 1952997
    :sswitch_4
    iget-object v2, p0, LX/CyV;->b:Landroid/content/res/Resources;

    const v4, 0x7f0822bf

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1952998
    :sswitch_5
    iget-object v2, p0, LX/CyV;->b:Landroid/content/res/Resources;

    const v4, 0x7f0822c0

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1952999
    :sswitch_6
    iget-object v2, p0, LX/CyV;->b:Landroid/content/res/Resources;

    const v4, 0x7f0822c1

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1953000
    :sswitch_7
    iget-object v2, p0, LX/CyV;->b:Landroid/content/res/Resources;

    const v4, 0x7f0822c4

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1953001
    :sswitch_8
    iget-object v2, p0, LX/CyV;->b:Landroid/content/res/Resources;

    const v4, 0x7f0822c3

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1953002
    :sswitch_9
    iget-object v2, p0, LX/CyV;->b:Landroid/content/res/Resources;

    const v4, 0x7f0822c2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1953003
    :sswitch_a
    const v3, -0xa123fbe

    goto/16 :goto_4

    .line 1953004
    :sswitch_b
    const v3, 0x34fdde85

    goto/16 :goto_4

    .line 1953005
    :sswitch_c
    const v3, 0x585964cb

    goto/16 :goto_4

    .line 1953006
    :sswitch_d
    const v3, -0x5cafdbb0

    goto/16 :goto_4

    .line 1953007
    :sswitch_e
    const v3, -0x1f44e6bd

    goto/16 :goto_4

    .line 1953008
    :sswitch_f
    const v3, -0x31093a57

    goto/16 :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
        0x499e8e7 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x3ff252d0 -> :sswitch_9
        0x25d6af -> :sswitch_5
        0x285feb -> :sswitch_4
        0x403827a -> :sswitch_7
        0x41e065f -> :sswitch_6
        0x499e8e7 -> :sswitch_8
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        -0x3ff252d0 -> :sswitch_f
        0x25d6af -> :sswitch_b
        0x285feb -> :sswitch_a
        0x403827a -> :sswitch_d
        0x41e065f -> :sswitch_c
        0x499e8e7 -> :sswitch_e
    .end sparse-switch
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1952813
    iget-object v0, p0, LX/CyV;->c:LX/Cvk;

    new-instance v1, LX/Cw4;

    iget-object v2, p0, LX/CyV;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {v1, v2}, LX/Cw4;-><init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    invoke-virtual {v0, v1}, LX/Cvk;->a(LX/Cw4;)Ljava/lang/String;

    move-result-object v1

    .line 1952814
    new-instance v2, LX/8ei;

    invoke-direct {v2}, LX/8ei;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1952815
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1952816
    :goto_0
    iput-object v0, v2, LX/8ei;->a:LX/0Px;

    .line 1952817
    move-object v0, v2

    .line 1952818
    iget-object v2, p0, LX/CyV;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1952819
    iget-object v3, v2, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v2, v3

    .line 1952820
    iput-object v2, v0, LX/8ei;->f:Ljava/lang/String;

    .line 1952821
    move-object v0, v0

    .line 1952822
    iget-object v2, p0, LX/CyV;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v2

    .line 1952823
    iput-object v2, v0, LX/8ei;->d:Ljava/lang/String;

    .line 1952824
    move-object v0, v0

    .line 1952825
    new-instance v2, LX/1lO;

    invoke-direct {v2}, LX/1lO;-><init>()V

    new-instance v3, LX/8eh;

    invoke-direct {v3}, LX/8eh;-><init>()V

    .line 1952826
    iput-object v1, v3, LX/8eh;->c:Ljava/lang/String;

    .line 1952827
    move-object v1, v3

    .line 1952828
    invoke-virtual {v0}, LX/8ei;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v0

    .line 1952829
    iput-object v0, v1, LX/8eh;->a:Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1952830
    move-object v0, v1

    .line 1952831
    invoke-virtual {v0}, LX/8eh;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    move-result-object v0

    .line 1952832
    iput-object v0, v2, LX/1lO;->k:Ljava/lang/Object;

    .line 1952833
    move-object v0, v2

    .line 1952834
    const-string v1, "bootstrap_entities"

    .line 1952835
    iput-object v1, v0, LX/1lO;->i:Ljava/lang/String;

    .line 1952836
    move-object v0, v0

    .line 1952837
    invoke-virtual {v0}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0

    .line 1952838
    :cond_0
    invoke-direct {p0, p1}, LX/CyV;->c(Ljava/util/List;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
