.class public LX/EJf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2104887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/CzL;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/A2T;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;"
        }
    .end annotation

    .prologue
    .line 2104888
    invoke-virtual {p0}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 2104889
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_0

    .line 2104890
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2104891
    check-cast v0, LX/A2T;

    invoke-interface {v0}, LX/A2T;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2104892
    :cond_0
    :goto_0
    return-object v0

    .line 2104893
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0
.end method
