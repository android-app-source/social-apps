.class public abstract LX/Csu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/support/v7/widget/RecyclerView;

.field public final b:F

.field public final c:F


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;FF)V
    .locals 0

    .prologue
    .line 1943576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1943577
    iput-object p1, p0, LX/Csu;->a:Landroid/support/v7/widget/RecyclerView;

    .line 1943578
    iput p2, p0, LX/Csu;->b:F

    .line 1943579
    iput p3, p0, LX/Csu;->c:F

    .line 1943580
    return-void
.end method

.method public static a(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1943581
    if-eqz p0, :cond_0

    .line 1943582
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1943583
    invoke-virtual {p0, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1943584
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1943585
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1943586
    iget-object v1, p0, LX/Csu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1943587
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1943588
    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, p0, LX/Csu;->b:F

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1943589
    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, p0, LX/Csu;->c:F

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1943590
    move-object v1, v1

    .line 1943591
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1943592
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1943593
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 1943594
    invoke-static {v0}, LX/Csu;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    .line 1943595
    if-eqz v4, :cond_5

    invoke-virtual {v4, v1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 1943596
    if-eqz v4, :cond_0

    .line 1943597
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1943598
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_2

    .line 1943599
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1943600
    :goto_2
    return-object v0

    .line 1943601
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_4

    .line 1943602
    iget v0, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int v6, v0, v3

    .line 1943603
    const/4 v5, 0x0

    .line 1943604
    const v4, 0x7fffffff

    .line 1943605
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1943606
    invoke-static {v0}, LX/Csu;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    .line 1943607
    iget p1, v3, Landroid/graphics/Rect;->top:I

    .line 1943608
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 1943609
    if-ge v2, v6, :cond_7

    .line 1943610
    sub-int p1, v6, v2

    .line 1943611
    :goto_4
    move v3, p1

    .line 1943612
    if-ge v3, v4, :cond_6

    move p1, v3

    move-object v3, v0

    move v0, p1

    :goto_5
    move v4, v0

    move-object v5, v3

    .line 1943613
    goto :goto_3

    .line 1943614
    :cond_3
    move-object v0, v5

    .line 1943615
    goto :goto_2

    .line 1943616
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    :cond_6
    move v0, v4

    move-object v3, v5

    goto :goto_5

    .line 1943617
    :cond_7
    if-le p1, v6, :cond_8

    .line 1943618
    sub-int/2addr p1, v6

    goto :goto_4

    .line 1943619
    :cond_8
    const/4 p1, 0x0

    goto :goto_4
.end method
