.class public final LX/E9I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V
    .locals 0

    .prologue
    .line 2084029
    iput-object p1, p0, LX/E9I;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2084030
    iget-object v0, p0, LX/E9I;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-object v0, v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->c:LX/E9A;

    iget-object v1, p0, LX/E9I;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-object v1, v1, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->k:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v1

    .line 2084031
    iget-object v3, v0, LX/E9A;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2084032
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    :goto_0
    iput-object v2, v0, LX/E9A;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2084033
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v4, v0, LX/E9A;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v2, v4}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2084034
    if-eqz v2, :cond_2

    .line 2084035
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2084036
    :goto_1
    iget-object v0, p0, LX/E9I;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-object v0, v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b:LX/1vi;

    new-instance v1, LX/Cg6;

    iget-object v2, p0, LX/E9I;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-object v2, v2, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->c:LX/E9A;

    .line 2084037
    iget-object v3, v2, LX/E9A;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v2, v3

    .line 2084038
    iget-object v3, p0, LX/E9I;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-object v3, v3, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->i:LX/2ja;

    invoke-virtual {v3}, LX/2ja;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/Cg6;-><init>(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2084039
    const/4 v0, 0x1

    return v0

    .line 2084040
    :cond_0
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0

    .line 2084041
    :cond_1
    iget-object v2, v0, LX/E9A;->b:LX/2dj;

    const-string v4, "PROFILE"

    invoke-virtual {v2, v1, v4}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2084042
    :goto_2
    iget-object v4, v0, LX/E9A;->a:LX/0Sh;

    new-instance p1, LX/E99;

    invoke-direct {p1, v0, v3}, LX/E99;-><init>(LX/E9A;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    invoke-virtual {v4, v2, p1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1

    .line 2084043
    :cond_2
    iget-object v2, v0, LX/E9A;->b:LX/2dj;

    const-string v4, "PROFILE"

    invoke-virtual {v2, v1, v4}, LX/2dj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_2
.end method
