.class public final LX/EyY;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friends/model/FriendRequest;

.field public final synthetic b:LX/2iU;


# direct methods
.method public constructor <init>(LX/2iU;Lcom/facebook/friends/model/FriendRequest;)V
    .locals 0

    .prologue
    .line 2186385
    iput-object p1, p0, LX/EyY;->b:LX/2iU;

    iput-object p2, p0, LX/EyY;->a:Lcom/facebook/friends/model/FriendRequest;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2186386
    iget-object v0, p0, LX/EyY;->b:LX/2iU;

    iget-object v1, p0, LX/EyY;->a:Lcom/facebook/friends/model/FriendRequest;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v0, v1, v2, p1}, LX/2iU;->a$redex0(LX/2iU;Lcom/facebook/friends/model/FriendRequest;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Ljava/lang/Throwable;)V

    .line 2186387
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186388
    return-void
.end method
