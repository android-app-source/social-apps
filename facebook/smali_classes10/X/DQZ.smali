.class public abstract LX/DQZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DLO;


# instance fields
.field public a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

.field public b:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1994754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1994755
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1994756
    iput-object p1, p0, LX/DQZ;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1994757
    iput-object p2, p0, LX/DQZ;->b:Landroid/content/Context;

    .line 1994758
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1994759
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 1994760
    return-void
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 1994761
    iget-object v0, p0, LX/DQZ;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DQZ;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DQZ;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/DQZ;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->SECRET:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
