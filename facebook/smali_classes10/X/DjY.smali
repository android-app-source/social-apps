.class public final LX/DjY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 2033321
    iput-object p1, p0, LX/DjY;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iput-object p2, p0, LX/DjY;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x1d0b5c18

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2033322
    new-instance v6, LX/DjX;

    invoke-direct {v6, p0}, LX/DjX;-><init>(LX/DjY;)V

    .line 2033323
    iget-object v0, p0, LX/DjY;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->b:LX/Dka;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DjY;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2033324
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2033325
    iget-object v3, p0, LX/DjY;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->h:Ljava/lang/String;

    iget-object v4, p0, LX/DjY;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->g:Ljava/lang/String;

    iget-object v5, p0, LX/DjY;->a:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, LX/Dka;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/DjX;)V

    .line 2033326
    const v0, 0x6311e077

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
