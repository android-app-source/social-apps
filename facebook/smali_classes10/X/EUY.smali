.class public LX/EUY;
.super LX/ESt;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/fragment/VideoHomeSectionController;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126217
    invoke-direct {p0}, LX/ESt;-><init>()V

    .line 2126218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EUY;->a:Ljava/util/Map;

    .line 2126219
    return-void
.end method

.method public static a(LX/0QB;)LX/EUY;
    .locals 1

    .prologue
    .line 2126228
    new-instance v0, LX/EUY;

    invoke-direct {v0}, LX/EUY;-><init>()V

    .line 2126229
    move-object v0, v0

    .line 2126230
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;)V
    .locals 3

    .prologue
    .line 2126220
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2126221
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2126222
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EUY;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2126223
    iget-object v1, p0, LX/EUY;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2126224
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EUD;

    .line 2126225
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/ETQ;->a(LX/ESs;)V

    .line 2126226
    invoke-interface {v0, p1}, LX/ESs;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)V

    goto :goto_0

    .line 2126227
    :cond_0
    return-void
.end method
