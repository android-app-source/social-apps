.class public LX/ELu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ELs;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ELv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2109989
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ELu;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ELv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109990
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2109991
    iput-object p1, p0, LX/ELu;->b:LX/0Ot;

    .line 2109992
    return-void
.end method

.method public static a(LX/0QB;)LX/ELu;
    .locals 4

    .prologue
    .line 2109978
    const-class v1, LX/ELu;

    monitor-enter v1

    .line 2109979
    :try_start_0
    sget-object v0, LX/ELu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109980
    sput-object v2, LX/ELu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109981
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109982
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109983
    new-instance v3, LX/ELu;

    const/16 p0, 0x3409

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ELu;-><init>(LX/0Ot;)V

    .line 2109984
    move-object v0, v3

    .line 2109985
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109986
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ELu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109987
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109988
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2109948
    check-cast p2, LX/ELt;

    .line 2109949
    iget-object v0, p0, LX/ELu;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ELv;

    iget-object v1, p2, LX/ELt;->a:LX/CzL;

    iget-object v2, p2, LX/ELt;->b:Ljava/lang/String;

    .line 2109950
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b1735

    invoke-interface {v3, v4}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b173a

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/ELv;->a:LX/EM2;

    const/4 p0, 0x0

    .line 2109951
    new-instance p2, LX/EM1;

    invoke-direct {p2, v4}, LX/EM1;-><init>(LX/EM2;)V

    .line 2109952
    sget-object v0, LX/EM2;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EM0;

    .line 2109953
    if-nez v0, :cond_0

    .line 2109954
    new-instance v0, LX/EM0;

    invoke-direct {v0}, LX/EM0;-><init>()V

    .line 2109955
    :cond_0
    invoke-static {v0, p1, p0, p0, p2}, LX/EM0;->a$redex0(LX/EM0;LX/1De;IILX/EM1;)V

    .line 2109956
    move-object p2, v0

    .line 2109957
    move-object p0, p2

    .line 2109958
    move-object v4, p0

    .line 2109959
    iget-object p0, v4, LX/EM0;->a:LX/EM1;

    iput-object v1, p0, LX/EM1;->a:LX/CzL;

    .line 2109960
    iget-object p0, v4, LX/EM0;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2109961
    move-object v4, v4

    .line 2109962
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    .line 2109963
    new-instance p0, LX/ELx;

    invoke-direct {p0}, LX/ELx;-><init>()V

    .line 2109964
    sget-object p2, LX/ELy;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/ELw;

    .line 2109965
    if-nez p2, :cond_1

    .line 2109966
    new-instance p2, LX/ELw;

    invoke-direct {p2}, LX/ELw;-><init>()V

    .line 2109967
    :cond_1
    invoke-static {p2, p1, v4, v4, p0}, LX/ELw;->a$redex0(LX/ELw;LX/1De;IILX/ELx;)V

    .line 2109968
    move-object p0, p2

    .line 2109969
    move-object v4, p0

    .line 2109970
    move-object v4, v4

    .line 2109971
    iget-object p0, v4, LX/ELw;->a:LX/ELx;

    iput-object v2, p0, LX/ELx;->a:Ljava/lang/String;

    .line 2109972
    iget-object p0, v4, LX/ELw;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2109973
    move-object v4, v4

    .line 2109974
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2109975
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2109976
    invoke-static {}, LX/1dS;->b()V

    .line 2109977
    const/4 v0, 0x0

    return-object v0
.end method
