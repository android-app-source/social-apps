.class public LX/CwC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1950115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/CwB;)Z
    .locals 5
    .param p0    # LX/CwB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1950108
    if-eqz p0, :cond_2

    invoke-interface {p0}, LX/CwB;->k()LX/103;

    move-result-object v0

    sget-object v3, LX/103;->VIDEO:LX/103;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 1950109
    :goto_0
    if-eqz p0, :cond_4

    invoke-interface {p0}, LX/CwB;->jC_()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-interface {p0}, LX/CwB;->jC_()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_1
    move-object v3, v3

    .line 1950110
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v3, v4, :cond_3

    move v3, v1

    .line 1950111
    :goto_2
    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 1950112
    goto :goto_0

    :cond_3
    move v3, v2

    .line 1950113
    goto :goto_2

    :cond_4
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_1
.end method

.method public static b(LX/CwB;)Z
    .locals 2
    .param p0    # LX/CwB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1950114
    if-eqz p0, :cond_1

    invoke-interface {p0}, LX/CwB;->k()LX/103;

    move-result-object v0

    sget-object v1, LX/103;->MARKETPLACE:LX/103;

    if-eq v0, v1, :cond_0

    invoke-interface {p0}, LX/CwB;->k()LX/103;

    move-result-object v0

    sget-object v1, LX/103;->COMMERCE:LX/103;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
