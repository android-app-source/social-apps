.class public LX/Cot;
.super LX/Cos;
.source ""

# interfaces
.implements LX/Cob;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cos",
        "<",
        "LX/Co8;",
        "Lcom/facebook/richdocument/view/widget/SlideshowView;",
        ">;",
        "LX/Cob;"
    }
.end annotation


# instance fields
.field public a:LX/Cqw;

.field public b:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Ljava/lang/String;

.field public k:I

.field private final l:LX/1B1;

.field private final m:LX/Chm;


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;Landroid/widget/ImageView;)V
    .locals 3

    .prologue
    .line 1936329
    invoke-direct {p0, p1, p2}, LX/Cos;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 1936330
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, LX/Cot;->l:LX/1B1;

    .line 1936331
    new-instance v0, LX/Cpg;

    invoke-direct {v0, p0}, LX/Cpg;-><init>(LX/Cot;)V

    iput-object v0, p0, LX/Cot;->m:LX/Chm;

    .line 1936332
    const-class v0, LX/Cot;

    invoke-static {v0, p0}, LX/Cot;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936333
    invoke-virtual {p0, p1}, LX/Cot;->a(LX/Ctg;)V

    .line 1936334
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    .line 1936335
    new-instance v1, LX/Cph;

    invoke-direct {v1, p0, v0}, LX/Cph;-><init>(LX/Cot;Lcom/facebook/richdocument/view/widget/SlideshowView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1936336
    iget-object v1, p0, LX/Cot;->l:LX/1B1;

    new-instance v2, LX/Cht;

    invoke-direct {v2, v0}, LX/Cht;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 1936337
    iget-object v0, p0, LX/Cot;->l:LX/1B1;

    iget-object v1, p0, LX/Cot;->b:LX/Chv;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 1936338
    iget-object v0, p0, LX/Cot;->c:LX/Ck0;

    const v1, 0x7f0d0163

    const v2, 0x7f0d0164

    invoke-virtual {v0, p3, v1, v2}, LX/Ck0;->a(Landroid/view/View;II)V

    .line 1936339
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cot;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v1

    check-cast v1, LX/Chv;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v2

    check-cast v2, LX/Ck0;

    invoke-static {p0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object p0

    check-cast p0, LX/Ckw;

    iput-object v1, p1, LX/Cot;->b:LX/Chv;

    iput-object v2, p1, LX/Cot;->c:LX/Ck0;

    iput-object p0, p1, LX/Cot;->d:LX/Ckw;

    return-void
.end method


# virtual methods
.method public a(LX/Ctg;)V
    .locals 1

    .prologue
    .line 1936326
    new-instance v0, LX/Cu2;

    invoke-direct {v0, p1}, LX/Cu2;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936327
    new-instance v0, LX/CuU;

    invoke-direct {v0, p1}, LX/CuU;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936328
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1936323
    invoke-super {p0, p1}, LX/Cos;->a(Landroid/os/Bundle;)V

    .line 1936324
    invoke-virtual {p0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->o()V

    .line 1936325
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;LX/Clo;)V
    .locals 2

    .prologue
    .line 1936299
    invoke-virtual {p2}, LX/Clo;->d()I

    move-result v0

    iput v0, p0, LX/Cot;->k:I

    .line 1936300
    invoke-static {p1}, LX/CoV;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Cqw;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Cqw;)V

    .line 1936301
    invoke-virtual {p0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/richdocument/view/widget/SlideshowView;->setSlides$6708424b(LX/Clo;)V

    .line 1936302
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/Clo;->d()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 1936303
    const-class v0, LX/Cu2;

    invoke-virtual {p0, v0}, LX/Cos;->b(Ljava/lang/Class;)V

    .line 1936304
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1936321
    iput-object p1, p0, LX/Cot;->e:Ljava/lang/String;

    .line 1936322
    return-void
.end method

.method public final a(II)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1936317
    invoke-virtual {p0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v2

    .line 1936318
    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 1936319
    invoke-virtual {v2, v3}, Lcom/facebook/richdocument/view/widget/SlideshowView;->getLocationOnScreen([I)V

    .line 1936320
    aget v4, v3, v1

    if-lt p1, v4, :cond_0

    aget v4, v3, v1

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/widget/SlideshowView;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    if-gt p1, v4, :cond_0

    aget v4, v3, v0

    if-lt p2, v4, :cond_0

    aget v3, v3, v0

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/widget/SlideshowView;->getHeight()I

    move-result v2

    add-int/2addr v2, v3

    if-gt p2, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1936310
    iget-object v0, p0, LX/Cot;->b:LX/Chv;

    iget-object v1, p0, LX/Cot;->m:LX/Chm;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1936311
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1936312
    const-string v1, "block_media_type"

    const-string v2, "slideshow"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1936313
    const-string v1, "current_slide"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1936314
    const-string v1, "total_slides"

    iget v2, p0, LX/Cot;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1936315
    iget-object v1, p0, LX/Cot;->d:LX/Ckw;

    iget-object v2, p0, LX/Cot;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/Ckw;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1936316
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1936306
    invoke-super {p0, p1}, LX/Cos;->c(Landroid/os/Bundle;)V

    .line 1936307
    iget-object v0, p0, LX/Cot;->b:LX/Chv;

    iget-object v1, p0, LX/Cot;->m:LX/Chm;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1936308
    invoke-virtual {p0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->onDetachedFromWindow()V

    .line 1936309
    return-void
.end method

.method public final f()Lcom/facebook/richdocument/view/widget/SlideshowView;
    .locals 1

    .prologue
    .line 1936305
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    return-object v0
.end method
