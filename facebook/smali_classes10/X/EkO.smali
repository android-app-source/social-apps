.class public abstract LX/EkO;
.super LX/EkN;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/AU0;",
        "V:",
        "LX/1a1;",
        ">",
        "LX/EkN",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private b:LX/AU0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2163396
    invoke-direct {p0, p1}, LX/EkN;-><init>(Landroid/content/Context;)V

    .line 2163397
    return-void
.end method


# virtual methods
.method public a(LX/1a1;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;I)V"
        }
    .end annotation

    .prologue
    .line 2163405
    invoke-super {p0, p1, p2}, LX/EkN;->a(LX/1a1;I)V

    .line 2163406
    iget-object v0, p0, LX/EkO;->b:LX/AU0;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AU0;

    invoke-virtual {p0, p1, v0}, LX/EkO;->a(LX/1a1;LX/AU0;)V

    .line 2163407
    return-void
.end method

.method public abstract a(LX/1a1;LX/AU0;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TT;)V"
        }
    .end annotation
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 2163404
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot call changeCursor(cursor), must call changeCursor(cursor, daoItem)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/database/Cursor;LX/AU0;)V
    .locals 0
    .param p2    # LX/AU0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 2163401
    iput-object p2, p0, LX/EkO;->b:LX/AU0;

    .line 2163402
    invoke-super {p0, p1}, LX/EkN;->a(Landroid/database/Cursor;)V

    .line 2163403
    return-void
.end method

.method public final d()LX/AU0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2163398
    iget-object v0, p0, LX/EkO;->b:LX/AU0;

    if-nez v0, :cond_0

    .line 2163399
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getDAOItem called from an illegal context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163400
    :cond_0
    iget-object v0, p0, LX/EkO;->b:LX/AU0;

    return-object v0
.end method
