.class public final LX/EqV;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;)V
    .locals 0

    .prologue
    .line 2171628
    iput-object p1, p0, LX/EqV;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 2171622
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2171623
    iget-object v1, p0, LX/EqV;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;

    iget-object v1, v1, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->s:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2171624
    const-string v2, "extra_events_note_text"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2171625
    iget-object v1, p0, LX/EqV;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->setResult(ILandroid/content/Intent;)V

    .line 2171626
    iget-object v0, p0, LX/EqV;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;

    invoke-virtual {v0}, Lcom/facebook/events/invite/EventsExtendedInviteAddNoteActivity;->finish()V

    .line 2171627
    return-void
.end method
