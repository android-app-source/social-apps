.class public LX/ELB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        "T::",
        "LX/8cx;",
        ":",
        "LX/8cy;",
        ":",
        "LX/8cz;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2SY;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EQ6;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/2SY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EQ6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2108120
    iput-object p1, p0, LX/ELB;->a:LX/0ad;

    .line 2108121
    iput-object p2, p0, LX/ELB;->b:LX/0Ot;

    .line 2108122
    iput-object p3, p0, LX/ELB;->c:LX/0Ot;

    .line 2108123
    iput-object p4, p0, LX/ELB;->d:LX/0Ot;

    .line 2108124
    iput-object p5, p0, LX/ELB;->e:LX/0Ot;

    .line 2108125
    iput-object p6, p0, LX/ELB;->f:LX/0Ot;

    .line 2108126
    return-void
.end method

.method public static a(LX/0QB;)LX/ELB;
    .locals 10

    .prologue
    .line 2108127
    const-class v1, LX/ELB;

    monitor-enter v1

    .line 2108128
    :try_start_0
    sget-object v0, LX/ELB;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108129
    sput-object v2, LX/ELB;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2108130
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108131
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2108132
    new-instance v3, LX/ELB;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const/16 v5, 0x11b8

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x34d5

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xb19

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2eb

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x32d4

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/ELB;-><init>(LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2108133
    move-object v0, v3

    .line 2108134
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2108135
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ELB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2108136
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2108137
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/8cx;LX/CxP;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TE;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 2108138
    iget-object v0, p0, LX/ELB;->a:LX/0ad;

    sget-short v1, LX/100;->bw:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2108139
    :goto_0
    return-void

    .line 2108140
    :cond_0
    new-instance v0, LX/CwR;

    invoke-direct {v0}, LX/CwR;-><init>()V

    invoke-interface {p1}, LX/8cx;->dW_()Ljava/lang/String;

    move-result-object v1

    .line 2108141
    iput-object v1, v0, LX/CwR;->b:Ljava/lang/String;

    .line 2108142
    move-object v1, v0

    .line 2108143
    move-object v0, p1

    check-cast v0, LX/8cz;

    invoke-interface {v0}, LX/8cz;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 2108144
    iput-object v0, v1, LX/CwR;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2108145
    move-object v1, v1

    .line 2108146
    move-object v0, p1

    check-cast v0, LX/8cz;

    invoke-interface {v0}, LX/8cz;->d()Ljava/lang/String;

    move-result-object v0

    .line 2108147
    iput-object v0, v1, LX/CwR;->a:Ljava/lang/String;

    .line 2108148
    move-object v0, v1

    .line 2108149
    check-cast p1, LX/8cy;

    .line 2108150
    invoke-interface {p1}, LX/8cy;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, LX/8cy;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    .line 2108151
    :goto_1
    if-eqz v1, :cond_4

    const-string v1, ""

    :goto_2
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v1, v1

    .line 2108152
    iput-object v1, v0, LX/CwR;->e:Landroid/net/Uri;

    .line 2108153
    move-object v0, v0

    .line 2108154
    const/4 v1, 0x1

    .line 2108155
    iput-boolean v1, v0, LX/CwR;->d:Z

    .line 2108156
    move-object v0, v0

    .line 2108157
    invoke-virtual {v0}, LX/CwR;->j()Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v4

    .line 2108158
    check-cast p2, LX/CxV;

    invoke-interface {p2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v2

    .line 2108159
    iget-object v0, p0, LX/ELB;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SY;

    iget-object v1, p0, LX/ELB;->a:LX/0ad;

    sget-short v5, LX/100;->cb:S

    invoke-interface {v1, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    .line 2108160
    :goto_3
    invoke-static {v0, v4, v1}, LX/2SY;->b(LX/2SY;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/String;)V

    .line 2108161
    iget-object v0, p0, LX/ELB;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQ6;

    invoke-virtual {v0, v3, v4, v2}, LX/EQ6;->a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v3

    .line 2108162
    goto :goto_3

    .line 2108163
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 2108164
    :cond_4
    invoke-interface {p1}, LX/8cy;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/CzL;LX/CxP;Landroid/content/Context;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+TT;>;TE;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2108165
    invoke-virtual/range {p1 .. p1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8cx;

    .line 2108166
    invoke-interface {v2}, LX/8cx;->dW_()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    move-object v3, v2

    check-cast v3, LX/8cz;

    invoke-interface {v3}, LX/8cz;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2108167
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v3, v2

    .line 2108168
    check-cast v3, LX/8cz;

    invoke-interface {v3}, LX/8cz;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 2108169
    invoke-virtual/range {p1 .. p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 2108170
    invoke-virtual/range {p1 .. p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v4

    invoke-static {v4}, LX/8eM;->e(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v4

    invoke-static {v3, v4}, LX/8eM;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object v4, v3

    .line 2108171
    :goto_1
    invoke-interface {v2}, LX/8cx;->dW_()Ljava/lang/String;

    move-result-object v10

    .line 2108172
    move-object/from16 v0, p0

    iget-object v3, v0, LX/ELB;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1nG;

    invoke-virtual {v3, v4, v10}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2108173
    if-eqz v3, :cond_0

    .line 2108174
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, LX/ELB;->a(LX/8cx;LX/CxP;)V

    .line 2108175
    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELB;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/17W;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2108176
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    .line 2108177
    :goto_2
    invoke-virtual/range {p1 .. p1}, LX/CzL;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2108178
    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELB;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, LX/CvY;

    move-object/from16 v2, p2

    check-cast v2, LX/CxV;

    invoke-interface {v2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v13

    sget-object v14, LX/8ch;->CLICK:LX/8ch;

    invoke-virtual/range {p1 .. p1}, LX/CzL;->d()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELB;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-object/from16 v2, p2

    check-cast v2, LX/CxV;

    invoke-interface {v2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, LX/CzL;->l()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, LX/CxP;->b(LX/CzL;)I

    move-result v5

    invoke-virtual/range {p1 .. p1}, LX/CzL;->d()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, LX/CzL;->k()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v8

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v11

    invoke-static/range {v2 .. v11}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    move-object v2, v12

    move-object v3, v13

    move-object v4, v14

    move v5, v15

    move-object/from16 v6, p1

    invoke-virtual/range {v2 .. v7}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_0

    .line 2108179
    :cond_2
    const/4 v9, 0x0

    goto :goto_2

    .line 2108180
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELB;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CvY;

    move-object/from16 v3, p2

    check-cast v3, LX/CxV;

    invoke-interface {v3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v3

    sget-object v4, LX/8ch;->CLICK:LX/8ch;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, LX/CxP;->b(LX/CzL;)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/ELB;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-object/from16 v6, p2

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, LX/CxP;->b(LX/CzL;)I

    move-result v7

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v8

    invoke-static {v6, v7, v10, v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    move-object/from16 v6, p1

    invoke-virtual/range {v2 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_0

    :cond_4
    move-object v4, v3

    goto/16 :goto_1
.end method
