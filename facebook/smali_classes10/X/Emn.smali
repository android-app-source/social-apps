.class public final enum LX/Emn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Emn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Emn;

.field public static final enum ENTITY_CARDS_DISMISSED:LX/Emn;

.field public static final enum ENTITY_CARDS_DISPLAYED:LX/Emn;

.field public static final enum ENTITY_CARDS_IMPRESSION:LX/Emn;

.field public static final enum ENTITY_CARDS_NAVIGATED_TO_ENTITY:LX/Emn;

.field public static final enum ENTITY_CARDS_PAGE_DOWNLOAD:LX/Emn;

.field public static final enum TAP:LX/Emn;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2166168
    new-instance v0, LX/Emn;

    const-string v1, "ENTITY_CARDS_IMPRESSION"

    const-string v2, "ec_impression"

    invoke-direct {v0, v1, v4, v2}, LX/Emn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emn;->ENTITY_CARDS_IMPRESSION:LX/Emn;

    .line 2166169
    new-instance v0, LX/Emn;

    const-string v1, "TAP"

    const-string v2, "tap"

    invoke-direct {v0, v1, v5, v2}, LX/Emn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emn;->TAP:LX/Emn;

    .line 2166170
    new-instance v0, LX/Emn;

    const-string v1, "ENTITY_CARDS_DISPLAYED"

    const-string v2, "ec_entity_cards_displayed"

    invoke-direct {v0, v1, v6, v2}, LX/Emn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emn;->ENTITY_CARDS_DISPLAYED:LX/Emn;

    .line 2166171
    new-instance v0, LX/Emn;

    const-string v1, "ENTITY_CARDS_DISMISSED"

    const-string v2, "ec_entity_cards_dismissed"

    invoke-direct {v0, v1, v7, v2}, LX/Emn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emn;->ENTITY_CARDS_DISMISSED:LX/Emn;

    .line 2166172
    new-instance v0, LX/Emn;

    const-string v1, "ENTITY_CARDS_PAGE_DOWNLOAD"

    const-string v2, "ec_cards_page_download"

    invoke-direct {v0, v1, v8, v2}, LX/Emn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emn;->ENTITY_CARDS_PAGE_DOWNLOAD:LX/Emn;

    .line 2166173
    new-instance v0, LX/Emn;

    const-string v1, "ENTITY_CARDS_NAVIGATED_TO_ENTITY"

    const/4 v2, 0x5

    const-string v3, "ec_navigated_to_entity"

    invoke-direct {v0, v1, v2, v3}, LX/Emn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emn;->ENTITY_CARDS_NAVIGATED_TO_ENTITY:LX/Emn;

    .line 2166174
    const/4 v0, 0x6

    new-array v0, v0, [LX/Emn;

    sget-object v1, LX/Emn;->ENTITY_CARDS_IMPRESSION:LX/Emn;

    aput-object v1, v0, v4

    sget-object v1, LX/Emn;->TAP:LX/Emn;

    aput-object v1, v0, v5

    sget-object v1, LX/Emn;->ENTITY_CARDS_DISPLAYED:LX/Emn;

    aput-object v1, v0, v6

    sget-object v1, LX/Emn;->ENTITY_CARDS_DISMISSED:LX/Emn;

    aput-object v1, v0, v7

    sget-object v1, LX/Emn;->ENTITY_CARDS_PAGE_DOWNLOAD:LX/Emn;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Emn;->ENTITY_CARDS_NAVIGATED_TO_ENTITY:LX/Emn;

    aput-object v2, v0, v1

    sput-object v0, LX/Emn;->$VALUES:[LX/Emn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2166175
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2166176
    iput-object p3, p0, LX/Emn;->name:Ljava/lang/String;

    .line 2166177
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Emn;
    .locals 1

    .prologue
    .line 2166167
    const-class v0, LX/Emn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Emn;

    return-object v0
.end method

.method public static values()[LX/Emn;
    .locals 1

    .prologue
    .line 2166166
    sget-object v0, LX/Emn;->$VALUES:[LX/Emn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Emn;

    return-object v0
.end method
