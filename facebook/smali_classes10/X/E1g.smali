.class public LX/E1g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E1g;


# instance fields
.field private final a:LX/0wM;

.field private final b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0wM;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2070401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2070402
    iput-object p1, p0, LX/E1g;->a:LX/0wM;

    .line 2070403
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    .line 2070404
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->OPEN_VERTICAL_ACTION_SHEET:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const v2, 0x7f020722

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2070405
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->PROMOTE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const v2, 0x7f020926

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2070406
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->PROMOTE_WEBSITE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const v2, 0x7f020926

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2070407
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->PUBLISH_PAGE_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const v2, 0x7f0207b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2070408
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->PUBLISH_PAGE_POST:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const v2, 0x7f02080f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2070409
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SHARE_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const v2, 0x7f0209c5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2070410
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SHARE_OG_OBJECT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const v2, 0x7f0209c5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2070411
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_PAGE_MORE_MENU:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const v2, 0x7f020722

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2070412
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_PAGE_SETTINGS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    const v2, 0x7f0208ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2070413
    return-void
.end method

.method public static a(LX/0QB;)LX/E1g;
    .locals 4

    .prologue
    .line 2070414
    sget-object v0, LX/E1g;->c:LX/E1g;

    if-nez v0, :cond_1

    .line 2070415
    const-class v1, LX/E1g;

    monitor-enter v1

    .line 2070416
    :try_start_0
    sget-object v0, LX/E1g;->c:LX/E1g;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2070417
    if-eqz v2, :cond_0

    .line 2070418
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2070419
    new-instance p0, LX/E1g;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-direct {p0, v3}, LX/E1g;-><init>(LX/0wM;)V

    .line 2070420
    move-object v0, p0

    .line 2070421
    sput-object v0, LX/E1g;->c:LX/E1g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2070422
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2070423
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2070424
    :cond_1
    sget-object v0, LX/E1g;->c:LX/E1g;

    return-object v0

    .line 2070425
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2070426
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2070427
    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/E1g;->a:LX/0wM;

    iget-object v0, p0, LX/E1g;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
