.class public final LX/EDc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field public final synthetic a:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;)V
    .locals 0

    .prologue
    .line 2091569
    iput-object p1, p0, LX/EDc;->a:LX/EDx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2091535
    check-cast p2, LX/EGa;

    .line 2091536
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    .line 2091537
    iget-object v1, p2, LX/EGa;->a:LX/EGe;

    move-object v1, v1

    .line 2091538
    iput-object v1, v0, LX/EDx;->bq:LX/EGe;

    .line 2091539
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    .line 2091540
    iput-boolean v7, v0, LX/EDx;->bp:Z

    .line 2091541
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    invoke-static {v0}, LX/EDx;->bA(LX/EDx;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2091542
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->bC:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short v5, LX/3Dx;->eg:S

    invoke-interface {v0, v1, v4, v5, v6}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2091543
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    invoke-static {v0}, LX/EDx;->ci(LX/EDx;)V

    .line 2091544
    :cond_0
    :goto_0
    return-void

    .line 2091545
    :cond_1
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    .line 2091546
    iget-boolean v1, v0, LX/EDx;->aB:Z

    move v0, v1

    .line 2091547
    if-eqz v0, :cond_2

    .line 2091548
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    sget-object v1, LX/EDv;->STARTED:LX/EDv;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EDv;)V

    .line 2091549
    :cond_2
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    invoke-virtual {v0, v7}, LX/EDx;->o(Z)Z

    .line 2091550
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    iget-boolean v0, v0, LX/EDx;->aY:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    iget-wide v0, v0, LX/EDx;->ac:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 2091551
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    iget-object v1, p0, LX/EDc;->a:LX/EDx;

    iget-object v1, v1, LX/EDx;->bl:LX/7TQ;

    iget-object v4, p0, LX/EDc;->a:LX/EDx;

    iget-boolean v4, v4, LX/EDx;->bm:Z

    const-string v5, ""

    .line 2091552
    invoke-static/range {v0 .. v5}, LX/EDx;->a$redex0(LX/EDx;LX/7TQ;JZLjava/lang/String;)V

    .line 2091553
    goto :goto_0

    .line 2091554
    :cond_3
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->bD:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->bD:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Window;

    .line 2091555
    :goto_1
    if-eqz v0, :cond_5

    .line 2091556
    iget-object v1, p0, LX/EDc;->a:LX/EDx;

    iget-object v1, v1, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v1, v0}, LX/EGe;->a(Landroid/view/Window;)V

    goto :goto_0

    .line 2091557
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2091558
    :cond_5
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->bq:LX/EGe;

    .line 2091559
    iget-object v1, v0, LX/EGe;->s:LX/EFs;

    if-eqz v1, :cond_6

    iget-object v1, v0, LX/EGe;->s:LX/EFs;

    .line 2091560
    iget-boolean v0, v1, LX/EFs;->d:Z

    move v1, v0

    .line 2091561
    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 2091562
    if-nez v0, :cond_0

    .line 2091563
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    invoke-static {v0, v6}, LX/EDx;->z(LX/EDx;Z)V

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 2091564
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    const/4 v1, 0x0

    .line 2091565
    iput-object v1, v0, LX/EDx;->bq:LX/EGe;

    .line 2091566
    iget-object v0, p0, LX/EDc;->a:LX/EDx;

    const/4 v1, 0x0

    .line 2091567
    iput-boolean v1, v0, LX/EDx;->bp:Z

    .line 2091568
    return-void
.end method
