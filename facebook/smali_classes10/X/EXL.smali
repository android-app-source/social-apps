.class public final LX/EXL;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EXC;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXL;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EXL;


# instance fields
.field public bitField0_:I

.field public defaultValue_:Ljava/lang/Object;

.field public extendee_:Ljava/lang/Object;

.field public label_:LX/EXI;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public name_:Ljava/lang/Object;

.field public number_:I

.field public options_:LX/EXR;

.field public typeName_:Ljava/lang/Object;

.field public type_:LX/EXK;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2133119
    new-instance v0, LX/EXB;

    invoke-direct {v0}, LX/EXB;-><init>()V

    sput-object v0, LX/EXL;->a:LX/EWZ;

    .line 2133120
    new-instance v0, LX/EXL;

    invoke-direct {v0}, LX/EXL;-><init>()V

    .line 2133121
    sput-object v0, LX/EXL;->c:LX/EXL;

    invoke-direct {v0}, LX/EXL;->K()V

    .line 2133122
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2133216
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2133217
    iput-byte v0, p0, LX/EXL;->memoizedIsInitialized:B

    .line 2133218
    iput v0, p0, LX/EXL;->memoizedSerializedSize:I

    .line 2133219
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2133220
    iput-object v0, p0, LX/EXL;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x1

    .line 2133160
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2133161
    iput-byte v0, p0, LX/EXL;->memoizedIsInitialized:B

    .line 2133162
    iput v0, p0, LX/EXL;->memoizedSerializedSize:I

    .line 2133163
    invoke-direct {p0}, LX/EXL;->K()V

    .line 2133164
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v4

    .line 2133165
    const/4 v0, 0x0

    move v2, v0

    .line 2133166
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 2133167
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2133168
    sparse-switch v0, :sswitch_data_0

    .line 2133169
    invoke-virtual {p0, p1, v4, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 2133170
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 2133171
    goto :goto_0

    .line 2133172
    :sswitch_1
    iget v0, p0, LX/EXL;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EXL;->bitField0_:I

    .line 2133173
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXL;->name_:Ljava/lang/Object;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2133174
    :catch_0
    move-exception v0

    .line 2133175
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2133176
    move-object v0, v0

    .line 2133177
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2133178
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EXL;->unknownFields:LX/EZQ;

    .line 2133179
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2133180
    :sswitch_2
    :try_start_2
    iget v0, p0, LX/EXL;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, LX/EXL;->bitField0_:I

    .line 2133181
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXL;->extendee_:Ljava/lang/Object;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2133182
    :catch_1
    move-exception v0

    .line 2133183
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2133184
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2133185
    move-object v0, v1

    .line 2133186
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2133187
    :sswitch_3
    :try_start_4
    iget v0, p0, LX/EXL;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EXL;->bitField0_:I

    .line 2133188
    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v0

    iput v0, p0, LX/EXL;->number_:I

    goto :goto_0

    .line 2133189
    :sswitch_4
    invoke-virtual {p1}, LX/EWd;->m()I

    move-result v0

    .line 2133190
    invoke-static {v0}, LX/EXI;->valueOf(I)LX/EXI;

    move-result-object v1

    .line 2133191
    if-nez v1, :cond_1

    .line 2133192
    const/4 v1, 0x4

    invoke-virtual {v4, v1, v0}, LX/EZM;->a(II)LX/EZM;

    goto :goto_0

    .line 2133193
    :cond_1
    iget v0, p0, LX/EXL;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EXL;->bitField0_:I

    .line 2133194
    iput-object v1, p0, LX/EXL;->label_:LX/EXI;

    goto :goto_0

    .line 2133195
    :sswitch_5
    invoke-virtual {p1}, LX/EWd;->m()I

    move-result v0

    .line 2133196
    invoke-static {v0}, LX/EXK;->valueOf(I)LX/EXK;

    move-result-object v1

    .line 2133197
    if-nez v1, :cond_2

    .line 2133198
    const/4 v1, 0x5

    invoke-virtual {v4, v1, v0}, LX/EZM;->a(II)LX/EZM;

    goto :goto_0

    .line 2133199
    :cond_2
    iget v0, p0, LX/EXL;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EXL;->bitField0_:I

    .line 2133200
    iput-object v1, p0, LX/EXL;->type_:LX/EXK;

    goto/16 :goto_0

    .line 2133201
    :sswitch_6
    iget v0, p0, LX/EXL;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/EXL;->bitField0_:I

    .line 2133202
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXL;->typeName_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 2133203
    :sswitch_7
    iget v0, p0, LX/EXL;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, LX/EXL;->bitField0_:I

    .line 2133204
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXL;->defaultValue_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 2133205
    :sswitch_8
    const/4 v0, 0x0

    .line 2133206
    iget v1, p0, LX/EXL;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v5, 0x80

    if-ne v1, v5, :cond_5

    .line 2133207
    iget-object v0, p0, LX/EXL;->options_:LX/EXR;

    invoke-virtual {v0}, LX/EXR;->z()LX/EXO;

    move-result-object v0

    move-object v1, v0

    .line 2133208
    :goto_1
    sget-object v0, LX/EXR;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EXR;

    iput-object v0, p0, LX/EXL;->options_:LX/EXR;

    .line 2133209
    if-eqz v1, :cond_3

    .line 2133210
    iget-object v0, p0, LX/EXL;->options_:LX/EXR;

    invoke-virtual {v1, v0}, LX/EXO;->a(LX/EXR;)LX/EXO;

    .line 2133211
    invoke-virtual {v1}, LX/EXO;->l()LX/EXR;

    move-result-object v0

    iput-object v0, p0, LX/EXL;->options_:LX/EXR;

    .line 2133212
    :cond_3
    iget v0, p0, LX/EXL;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, LX/EXL;->bitField0_:I
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2133213
    :cond_4
    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXL;->unknownFields:LX/EZQ;

    .line 2133214
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2133215
    return-void

    :cond_5
    move-object v1, v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2133155
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2133156
    iput-byte v1, p0, LX/EXL;->memoizedIsInitialized:B

    .line 2133157
    iput v1, p0, LX/EXL;->memoizedSerializedSize:I

    .line 2133158
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXL;->unknownFields:LX/EZQ;

    .line 2133159
    return-void
.end method

.method private G()LX/EWc;
    .locals 2

    .prologue
    .line 2133150
    iget-object v0, p0, LX/EXL;->name_:Ljava/lang/Object;

    .line 2133151
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133152
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2133153
    iput-object v0, p0, LX/EXL;->name_:Ljava/lang/Object;

    .line 2133154
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private H()LX/EWc;
    .locals 2

    .prologue
    .line 2133145
    iget-object v0, p0, LX/EXL;->typeName_:Ljava/lang/Object;

    .line 2133146
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133147
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2133148
    iput-object v0, p0, LX/EXL;->typeName_:Ljava/lang/Object;

    .line 2133149
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private I()LX/EWc;
    .locals 2

    .prologue
    .line 2133140
    iget-object v0, p0, LX/EXL;->extendee_:Ljava/lang/Object;

    .line 2133141
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133142
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2133143
    iput-object v0, p0, LX/EXL;->extendee_:Ljava/lang/Object;

    .line 2133144
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private J()LX/EWc;
    .locals 2

    .prologue
    .line 2133135
    iget-object v0, p0, LX/EXL;->defaultValue_:Ljava/lang/Object;

    .line 2133136
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133137
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2133138
    iput-object v0, p0, LX/EXL;->defaultValue_:Ljava/lang/Object;

    .line 2133139
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private K()V
    .locals 1

    .prologue
    .line 2133125
    const-string v0, ""

    iput-object v0, p0, LX/EXL;->name_:Ljava/lang/Object;

    .line 2133126
    const/4 v0, 0x0

    iput v0, p0, LX/EXL;->number_:I

    .line 2133127
    sget-object v0, LX/EXI;->LABEL_OPTIONAL:LX/EXI;

    iput-object v0, p0, LX/EXL;->label_:LX/EXI;

    .line 2133128
    sget-object v0, LX/EXK;->TYPE_DOUBLE:LX/EXK;

    iput-object v0, p0, LX/EXL;->type_:LX/EXK;

    .line 2133129
    const-string v0, ""

    iput-object v0, p0, LX/EXL;->typeName_:Ljava/lang/Object;

    .line 2133130
    const-string v0, ""

    iput-object v0, p0, LX/EXL;->extendee_:Ljava/lang/Object;

    .line 2133131
    const-string v0, ""

    iput-object v0, p0, LX/EXL;->defaultValue_:Ljava/lang/Object;

    .line 2133132
    sget-object v0, LX/EXR;->c:LX/EXR;

    move-object v0, v0

    .line 2133133
    iput-object v0, p0, LX/EXL;->options_:LX/EXR;

    .line 2133134
    return-void
.end method

.method private static e(LX/EXL;)LX/EXD;
    .locals 1

    .prologue
    .line 2133124
    invoke-static {}, LX/EXD;->n()LX/EXD;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EXD;->a(LX/EXL;)LX/EXD;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Z
    .locals 2

    .prologue
    .line 2133123
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2133088
    iget-object v0, p0, LX/EXL;->defaultValue_:Ljava/lang/Object;

    .line 2133089
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133090
    check-cast v0, Ljava/lang/String;

    .line 2133091
    :goto_0
    return-object v0

    .line 2133092
    :cond_0
    check-cast v0, LX/EWc;

    .line 2133093
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2133094
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2133095
    iput-object v1, p0, LX/EXL;->defaultValue_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2133096
    goto :goto_0
.end method

.method public final C()Z
    .locals 2

    .prologue
    .line 2133118
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2133116
    new-instance v0, LX/EXD;

    invoke-direct {v0, p1}, LX/EXD;-><init>(LX/EYd;)V

    .line 2133117
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2133097
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2133098
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2133099
    invoke-direct {p0}, LX/EXL;->G()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2133100
    :cond_0
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 2133101
    invoke-direct {p0}, LX/EXL;->I()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2133102
    :cond_1
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 2133103
    const/4 v0, 0x3

    iget v1, p0, LX/EXL;->number_:I

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(II)V

    .line 2133104
    :cond_2
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 2133105
    iget-object v0, p0, LX/EXL;->label_:LX/EXI;

    invoke-virtual {v0}, LX/EXI;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, LX/EWf;->d(II)V

    .line 2133106
    :cond_3
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 2133107
    const/4 v0, 0x5

    iget-object v1, p0, LX/EXL;->type_:LX/EXK;

    invoke-virtual {v1}, LX/EXK;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->d(II)V

    .line 2133108
    :cond_4
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 2133109
    const/4 v0, 0x6

    invoke-direct {p0}, LX/EXL;->H()LX/EWc;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2133110
    :cond_5
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 2133111
    const/4 v0, 0x7

    invoke-direct {p0}, LX/EXL;->J()LX/EWc;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2133112
    :cond_6
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 2133113
    iget-object v0, p0, LX/EXL;->options_:LX/EXR;

    invoke-virtual {p1, v4, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2133114
    :cond_7
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2133115
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2133221
    iget-byte v2, p0, LX/EXL;->memoizedIsInitialized:B

    .line 2133222
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 2133223
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 2133224
    goto :goto_0

    .line 2133225
    :cond_1
    invoke-virtual {p0}, LX/EXL;->C()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2133226
    iget-object v2, p0, LX/EXL;->options_:LX/EXR;

    move-object v2, v2

    .line 2133227
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2133228
    iput-byte v1, p0, LX/EXL;->memoizedIsInitialized:B

    move v0, v1

    .line 2133229
    goto :goto_0

    .line 2133230
    :cond_2
    iput-byte v0, p0, LX/EXL;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2133029
    iget v0, p0, LX/EXL;->memoizedSerializedSize:I

    .line 2133030
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2133031
    :goto_0
    return v0

    .line 2133032
    :cond_0
    const/4 v0, 0x0

    .line 2133033
    iget v1, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2133034
    invoke-direct {p0}, LX/EXL;->G()LX/EWc;

    move-result-object v0

    invoke-static {v2, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2133035
    :cond_1
    iget v1, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_2

    .line 2133036
    invoke-direct {p0}, LX/EXL;->I()LX/EWc;

    move-result-object v1

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2133037
    :cond_2
    iget v1, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 2133038
    const/4 v1, 0x3

    iget v2, p0, LX/EXL;->number_:I

    invoke-static {v1, v2}, LX/EWf;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2133039
    :cond_3
    iget v1, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 2133040
    iget-object v1, p0, LX/EXL;->label_:LX/EXI;

    invoke-virtual {v1}, LX/EXI;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, LX/EWf;->h(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2133041
    :cond_4
    iget v1, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_5

    .line 2133042
    const/4 v1, 0x5

    iget-object v2, p0, LX/EXL;->type_:LX/EXK;

    invoke-virtual {v2}, LX/EXK;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, LX/EWf;->h(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2133043
    :cond_5
    iget v1, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 2133044
    const/4 v1, 0x6

    invoke-direct {p0}, LX/EXL;->H()LX/EWc;

    move-result-object v2

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2133045
    :cond_6
    iget v1, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 2133046
    const/4 v1, 0x7

    invoke-direct {p0}, LX/EXL;->J()LX/EWc;

    move-result-object v2

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2133047
    :cond_7
    iget v1, p0, LX/EXL;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 2133048
    iget-object v1, p0, LX/EXL;->options_:LX/EXR;

    invoke-static {v5, v1}, LX/EWf;->e(ILX/EWW;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2133049
    :cond_8
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2133050
    iput v0, p0, LX/EXL;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2133051
    iget-object v0, p0, LX/EXL;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2133052
    sget-object v0, LX/EYC;->j:LX/EYn;

    const-class v1, LX/EXL;

    const-class v2, LX/EXD;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2133053
    sget-object v0, LX/EXL;->a:LX/EWZ;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2133054
    iget-object v0, p0, LX/EXL;->name_:Ljava/lang/Object;

    .line 2133055
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133056
    check-cast v0, Ljava/lang/String;

    .line 2133057
    :goto_0
    return-object v0

    .line 2133058
    :cond_0
    check-cast v0, LX/EWc;

    .line 2133059
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2133060
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2133061
    iput-object v1, p0, LX/EXL;->name_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2133062
    goto :goto_0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 2133063
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2133064
    invoke-static {p0}, LX/EXL;->e(LX/EXL;)LX/EXD;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2133065
    invoke-static {}, LX/EXD;->n()LX/EXD;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2133066
    invoke-static {p0}, LX/EXL;->e(LX/EXL;)LX/EXD;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2133067
    sget-object v0, LX/EXL;->c:LX/EXL;

    return-object v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 2133068
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2133069
    iget-object v0, p0, LX/EXL;->typeName_:Ljava/lang/Object;

    .line 2133070
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133071
    check-cast v0, Ljava/lang/String;

    .line 2133072
    :goto_0
    return-object v0

    .line 2133073
    :cond_0
    check-cast v0, LX/EWc;

    .line 2133074
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2133075
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2133076
    iput-object v1, p0, LX/EXL;->typeName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2133077
    goto :goto_0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 2133078
    iget v0, p0, LX/EXL;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2133079
    iget-object v0, p0, LX/EXL;->extendee_:Ljava/lang/Object;

    .line 2133080
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133081
    check-cast v0, Ljava/lang/String;

    .line 2133082
    :goto_0
    return-object v0

    .line 2133083
    :cond_0
    check-cast v0, LX/EWc;

    .line 2133084
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2133085
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2133086
    iput-object v1, p0, LX/EXL;->extendee_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2133087
    goto :goto_0
.end method
