.class public final LX/DZO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DZN;


# instance fields
.field public final synthetic a:LX/DZW;


# direct methods
.method public constructor <init>(LX/DZW;)V
    .locals 0

    .prologue
    .line 2013297
    iput-object p1, p0, LX/DZO;->a:LX/DZW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2013298
    iget-object v0, p0, LX/DZO;->a:LX/DZW;

    iget-object v0, v0, LX/DZW;->a:Landroid/content/res/Resources;

    const v1, 0x7f082fdc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 2013299
    iget-object v1, p0, LX/DZO;->a:LX/DZW;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 2013300
    :goto_0
    iput-object v0, v1, LX/DZW;->j:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 2013301
    iget-object v0, p0, LX/DZO;->a:LX/DZW;

    invoke-static {v0}, LX/DZW;->c(LX/DZW;)V

    .line 2013302
    iget-object v0, p0, LX/DZO;->a:LX/DZW;

    iget-object v0, v0, LX/DZW;->b:LX/DZH;

    iget-object v1, p0, LX/DZO;->a:LX/DZW;

    iget-object v1, v1, LX/DZW;->o:Ljava/lang/String;

    iget-object v2, p0, LX/DZO;->a:LX/DZW;

    iget-object v2, v2, LX/DZW;->n:LX/DQn;

    invoke-interface {v2}, LX/DQn;->ky_()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    move-result-object v2

    iget-object v3, p0, LX/DZO;->a:LX/DZW;

    iget-object v3, v3, LX/DZW;->j:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 2013303
    iget-object v4, v0, LX/DZH;->h:LX/DQQ;

    if-eqz v4, :cond_0

    .line 2013304
    iget-object v4, v0, LX/DZH;->h:LX/DQQ;

    invoke-interface {v4, v3}, LX/DQQ;->a(Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;)V

    .line 2013305
    :cond_0
    new-instance v4, LX/4GF;

    invoke-direct {v4}, LX/4GF;-><init>()V

    .line 2013306
    const-string p0, "group_id"

    invoke-virtual {v4, p0, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013307
    move-object p0, v4

    .line 2013308
    sget-object v4, LX/DZH;->c:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2013309
    const-string p1, "setting"

    invoke-virtual {p0, p1, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013310
    move-object v4, p0

    .line 2013311
    iget-object p0, v0, LX/DZH;->g:Ljava/lang/String;

    .line 2013312
    const-string p1, "actor_id"

    invoke-virtual {v4, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013313
    move-object v4, v4

    .line 2013314
    new-instance p0, LX/DZk;

    invoke-direct {p0}, LX/DZk;-><init>()V

    move-object p0, p0

    .line 2013315
    const-string p1, "input"

    invoke-virtual {p0, p1, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/DZk;

    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2013316
    iget-object p0, v0, LX/DZH;->f:LX/0tX;

    invoke-virtual {p0, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2013317
    new-instance p0, LX/DZG;

    invoke-direct {p0, v0, v2}, LX/DZG;-><init>(LX/DZH;Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;)V

    iget-object p1, v0, LX/DZH;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2013318
    return-void

    .line 2013319
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2013320
    iget-object v0, p0, LX/DZO;->a:LX/DZW;

    iget-object v0, v0, LX/DZW;->j:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
