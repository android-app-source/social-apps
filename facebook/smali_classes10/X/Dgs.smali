.class public final enum LX/Dgs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dgs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dgs;

.field public static final enum GET_LOCATION:LX/Dgs;

.field public static final enum GET_PLACES:LX/Dgs;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2029922
    new-instance v0, LX/Dgs;

    const-string v1, "GET_LOCATION"

    invoke-direct {v0, v1, v2}, LX/Dgs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dgs;->GET_LOCATION:LX/Dgs;

    .line 2029923
    new-instance v0, LX/Dgs;

    const-string v1, "GET_PLACES"

    invoke-direct {v0, v1, v3}, LX/Dgs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dgs;->GET_PLACES:LX/Dgs;

    .line 2029924
    const/4 v0, 0x2

    new-array v0, v0, [LX/Dgs;

    sget-object v1, LX/Dgs;->GET_LOCATION:LX/Dgs;

    aput-object v1, v0, v2

    sget-object v1, LX/Dgs;->GET_PLACES:LX/Dgs;

    aput-object v1, v0, v3

    sput-object v0, LX/Dgs;->$VALUES:[LX/Dgs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2029925
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dgs;
    .locals 1

    .prologue
    .line 2029921
    const-class v0, LX/Dgs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dgs;

    return-object v0
.end method

.method public static values()[LX/Dgs;
    .locals 1

    .prologue
    .line 2029920
    sget-object v0, LX/Dgs;->$VALUES:[LX/Dgs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dgs;

    return-object v0
.end method
