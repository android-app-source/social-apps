.class public LX/DVp;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/String;Ljava/lang/String;LX/0tX;LX/B1b;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2005807
    invoke-direct {p0, p1, p4, p5}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;)V

    .line 2005808
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005809
    iput-object v0, p0, LX/DVp;->e:LX/0Px;

    .line 2005810
    iput-object p2, p0, LX/DVp;->f:Ljava/lang/String;

    .line 2005811
    iput-object p3, p0, LX/DVp;->g:Ljava/lang/String;

    .line 2005812
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005813
    new-instance v0, LX/DX7;

    invoke-direct {v0}, LX/DX7;-><init>()V

    move-object v0, v0

    .line 2005814
    const-string v1, "user_id"

    iget-object v2, p0, LX/DVp;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005815
    const-string v1, "searchTerm"

    iget-object v2, p0, LX/DVp;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005816
    const-string v1, "afterCursor"

    iget-object v2, p0, LX/B1d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005817
    const-string v1, "member_count_to_fetch"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2005818
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2005819
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005820
    if-eqz v0, :cond_1

    .line 2005821
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005822
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2005823
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2005824
    iget-object v0, p0, LX/DVp;->e:LX/0Px;

    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2005825
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005826
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$FriendsModel;

    move-result-object v6

    .line 2005827
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v4, v2

    :goto_0
    if-ge v4, v8, :cond_0

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$FriendsModel$EdgesModel;

    .line 2005828
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$FriendsModel$EdgesModel;->a()Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$FriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0, v1}, LX/DVf;->a(LX/DWH;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005829
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 2005830
    :cond_0
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$FriendsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    sget-object v7, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2005831
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/DVp;->e:LX/0Px;

    .line 2005832
    if-eqz v6, :cond_2

    invoke-virtual {v4, v6, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, LX/DVp;->a:Ljava/lang/String;

    .line 2005833
    if-eqz v6, :cond_3

    invoke-virtual {v4, v6, v3}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    iput-boolean v0, p0, LX/DVp;->b:Z

    .line 2005834
    :cond_1
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 2005835
    return-void

    .line 2005836
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    .line 2005837
    goto :goto_1

    :cond_3
    move v0, v3

    .line 2005838
    goto :goto_2
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2005839
    const-string v0, "User friends search fetch failed"

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005840
    iget-object v0, p0, LX/DVp;->e:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 2005841
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005842
    iput-object v0, p0, LX/DVp;->e:LX/0Px;

    .line 2005843
    return-void
.end method
