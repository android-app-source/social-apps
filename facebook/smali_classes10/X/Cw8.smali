.class public final enum LX/Cw8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cw8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cw8;

.field public static final enum App:LX/Cw8;

.field private static final CORE_SEARCH_FILTERS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Cw8;",
            ">;"
        }
    .end annotation
.end field

.field private static final DISPLAY_STYLE_TO_FILTER_TYPE:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "LX/Cw8;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum Event:LX/Cw8;

.field public static final enum Group:LX/Cw8;

.field public static final enum Page:LX/Cw8;

.field public static final enum People:LX/Cw8;

.field public static final enum Photos:LX/Cw8;

.field public static final enum Places:LX/Cw8;

.field public static final enum Search:LX/Cw8;

.field public static final enum SearchDark:LX/Cw8;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1950081
    new-instance v0, LX/Cw8;

    const-string v1, "Search"

    invoke-direct {v0, v1, v3}, LX/Cw8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cw8;->Search:LX/Cw8;

    .line 1950082
    new-instance v0, LX/Cw8;

    const-string v1, "SearchDark"

    invoke-direct {v0, v1, v4}, LX/Cw8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cw8;->SearchDark:LX/Cw8;

    .line 1950083
    new-instance v0, LX/Cw8;

    const-string v1, "People"

    invoke-direct {v0, v1, v5}, LX/Cw8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cw8;->People:LX/Cw8;

    .line 1950084
    new-instance v0, LX/Cw8;

    const-string v1, "Page"

    invoke-direct {v0, v1, v6}, LX/Cw8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cw8;->Page:LX/Cw8;

    .line 1950085
    new-instance v0, LX/Cw8;

    const-string v1, "Group"

    invoke-direct {v0, v1, v7}, LX/Cw8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cw8;->Group:LX/Cw8;

    .line 1950086
    new-instance v0, LX/Cw8;

    const-string v1, "Event"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Cw8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cw8;->Event:LX/Cw8;

    .line 1950087
    new-instance v0, LX/Cw8;

    const-string v1, "Photos"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Cw8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cw8;->Photos:LX/Cw8;

    .line 1950088
    new-instance v0, LX/Cw8;

    const-string v1, "Places"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Cw8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cw8;->Places:LX/Cw8;

    .line 1950089
    new-instance v0, LX/Cw8;

    const-string v1, "App"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Cw8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cw8;->App:LX/Cw8;

    .line 1950090
    const/16 v0, 0x9

    new-array v0, v0, [LX/Cw8;

    sget-object v1, LX/Cw8;->Search:LX/Cw8;

    aput-object v1, v0, v3

    sget-object v1, LX/Cw8;->SearchDark:LX/Cw8;

    aput-object v1, v0, v4

    sget-object v1, LX/Cw8;->People:LX/Cw8;

    aput-object v1, v0, v5

    sget-object v1, LX/Cw8;->Page:LX/Cw8;

    aput-object v1, v0, v6

    sget-object v1, LX/Cw8;->Group:LX/Cw8;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Cw8;->Event:LX/Cw8;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Cw8;->Photos:LX/Cw8;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Cw8;->Places:LX/Cw8;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Cw8;->App:LX/Cw8;

    aput-object v2, v0, v1

    sput-object v0, LX/Cw8;->$VALUES:[LX/Cw8;

    .line 1950091
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->People:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Search:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Search:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Search:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Page:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Group:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Event:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Photos:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Places:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->App:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Search:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Search:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/Cw8;->Photos:LX/Cw8;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/Cw8;->DISPLAY_STYLE_TO_FILTER_TYPE:LX/0P1;

    .line 1950092
    sget-object v0, LX/Cw8;->Search:LX/Cw8;

    sget-object v1, LX/Cw8;->People:LX/Cw8;

    sget-object v2, LX/Cw8;->Page:LX/Cw8;

    sget-object v3, LX/Cw8;->Group:LX/Cw8;

    sget-object v4, LX/Cw8;->Event:LX/Cw8;

    sget-object v5, LX/Cw8;->App:LX/Cw8;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Cw8;->CORE_SEARCH_FILTERS:LX/0Px;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1950080
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static from(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/Cw8;
    .locals 1

    .prologue
    .line 1950078
    sget-object v0, LX/Cw8;->DISPLAY_STYLE_TO_FILTER_TYPE:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cw8;

    .line 1950079
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Cw8;->Search:LX/Cw8;

    goto :goto_0
.end method

.method public static getCoreFilterTypeAt(I)LX/Cw8;
    .locals 1

    .prologue
    .line 1950075
    if-ltz p0, :cond_0

    invoke-static {}, LX/Cw8;->getCoreFilterTypeLength()I

    move-result v0

    if-lt p0, v0, :cond_1

    .line 1950076
    :cond_0
    const/4 v0, 0x0

    .line 1950077
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/Cw8;->CORE_SEARCH_FILTERS:LX/0Px;

    invoke-virtual {v0, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cw8;

    goto :goto_0
.end method

.method public static getCoreFilterTypeLength()I
    .locals 1

    .prologue
    .line 1950074
    sget-object v0, LX/Cw8;->CORE_SEARCH_FILTERS:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public static getPositionOfCoreDisplayStyle(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)I
    .locals 1

    .prologue
    .line 1950073
    sget-object v0, LX/Cw8;->DISPLAY_STYLE_TO_FILTER_TYPE:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cw8;

    invoke-static {v0}, LX/Cw8;->getPositionOfCoreFilterType(LX/Cw8;)I

    move-result v0

    return v0
.end method

.method public static getPositionOfCoreFilterType(LX/Cw8;)I
    .locals 1

    .prologue
    .line 1950072
    sget-object v0, LX/Cw8;->CORE_SEARCH_FILTERS:LX/0Px;

    invoke-virtual {v0, p0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cw8;
    .locals 1

    .prologue
    .line 1950071
    const-class v0, LX/Cw8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cw8;

    return-object v0
.end method

.method public static values()[LX/Cw8;
    .locals 1

    .prologue
    .line 1950070
    sget-object v0, LX/Cw8;->$VALUES:[LX/Cw8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cw8;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950069
    invoke-virtual {p0}, LX/Cw8;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
