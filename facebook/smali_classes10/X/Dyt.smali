.class public LX/Dyt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/92D;

.field public b:Lcom/facebook/places/checkin/PlacePickerFragment;

.field public c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

.field private d:LX/1Kf;

.field public e:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

.field public f:LX/9j5;

.field public g:Ljava/lang/String;

.field public h:LX/9j9;

.field private i:LX/9jA;

.field private j:LX/APL;

.field private k:LX/01T;

.field private l:LX/92C;

.field public m:LX/9jG;

.field private n:Lcom/facebook/common/perftest/PerfTestConfig;

.field public o:LX/926;

.field public p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public q:Landroid/content/Context;

.field public r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Kf;LX/9j5;LX/9j9;LX/9jA;LX/APL;LX/01T;LX/92C;Lcom/facebook/common/perftest/PerfTestConfig;LX/926;LX/92D;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2065960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2065961
    iput-object p1, p0, LX/Dyt;->q:Landroid/content/Context;

    .line 2065962
    iput-object p2, p0, LX/Dyt;->d:LX/1Kf;

    .line 2065963
    iput-object p3, p0, LX/Dyt;->f:LX/9j5;

    .line 2065964
    iput-object p4, p0, LX/Dyt;->h:LX/9j9;

    .line 2065965
    iput-object p5, p0, LX/Dyt;->i:LX/9jA;

    .line 2065966
    iput-object p6, p0, LX/Dyt;->j:LX/APL;

    .line 2065967
    iput-object p7, p0, LX/Dyt;->k:LX/01T;

    .line 2065968
    iput-object p8, p0, LX/Dyt;->l:LX/92C;

    .line 2065969
    iput-object p9, p0, LX/Dyt;->n:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 2065970
    iput-object p10, p0, LX/Dyt;->o:LX/926;

    .line 2065971
    iput-object p11, p0, LX/Dyt;->a:LX/92D;

    .line 2065972
    return-void
.end method

.method public static a(LX/0QB;)LX/Dyt;
    .locals 13

    .prologue
    .line 2066048
    new-instance v1, LX/Dyt;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v3

    check-cast v3, LX/1Kf;

    invoke-static {p0}, LX/9j5;->a(LX/0QB;)LX/9j5;

    move-result-object v4

    check-cast v4, LX/9j5;

    invoke-static {p0}, LX/9j9;->b(LX/0QB;)LX/9j9;

    move-result-object v5

    check-cast v5, LX/9j9;

    invoke-static {p0}, LX/9jA;->b(LX/0QB;)LX/9jA;

    move-result-object v6

    check-cast v6, LX/9jA;

    invoke-static {p0}, LX/APL;->a(LX/0QB;)LX/APL;

    move-result-object v7

    check-cast v7, LX/APL;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v8

    check-cast v8, LX/01T;

    invoke-static {p0}, LX/92C;->a(LX/0QB;)LX/92C;

    move-result-object v9

    check-cast v9, LX/92C;

    invoke-static {p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v10

    check-cast v10, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {p0}, LX/926;->b(LX/0QB;)LX/926;

    move-result-object v11

    check-cast v11, LX/926;

    invoke-static {p0}, LX/92D;->a(LX/0QB;)LX/92D;

    move-result-object v12

    check-cast v12, LX/92D;

    invoke-direct/range {v1 .. v12}, LX/Dyt;-><init>(Landroid/content/Context;LX/1Kf;LX/9j5;LX/9j9;LX/9jA;LX/APL;LX/01T;LX/92C;Lcom/facebook/common/perftest/PerfTestConfig;LX/926;LX/92D;)V

    .line 2066049
    move-object v0, v1

    .line 2066050
    return-object v0
.end method

.method public static a(LX/Dyt;Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 2065994
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065995
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    move-object v0, v1

    .line 2065996
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2065997
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2065998
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065999
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    move-object v9, v1

    .line 2066000
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v7, v6

    :goto_0
    if-ge v7, v10, :cond_0

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 2066001
    new-instance v1, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v1 .. v6}, Lcom/facebook/ipc/model/FacebookProfile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2066002
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 2066003
    :cond_0
    const-string v0, "full_profiles"

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2066004
    :cond_1
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066005
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    move-object v0, v1

    .line 2066006
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2066007
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066008
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    move-object v1, v2

    .line 2066009
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2066010
    const-string v1, "profiles"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2066011
    :cond_2
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066012
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v1

    .line 2066013
    if-eqz v0, :cond_3

    .line 2066014
    const-string v0, "minutiae_object"

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066015
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 2066016
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2066017
    :cond_3
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066018
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    move-object v0, v1

    .line 2066019
    if-eqz v0, :cond_4

    .line 2066020
    const-string v0, "media_id"

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066021
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    move-object v1, v2

    .line 2066022
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2066023
    :cond_4
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066024
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2066025
    if-eqz v0, :cond_5

    .line 2066026
    const-string v0, "comment_id"

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066027
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2066028
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2066029
    :cond_5
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066030
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v1

    .line 2066031
    if-eqz v0, :cond_6

    .line 2066032
    const-string v0, "feedback_for_social_search"

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066033
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v1, v2

    .line 2066034
    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2066035
    :cond_6
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066036
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    move-object v0, v1

    .line 2066037
    if-eqz v0, :cond_7

    .line 2066038
    const-string v0, "story_id_for_social_search"

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066039
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    move-object v1, v2

    .line 2066040
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2066041
    :cond_7
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066042
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    move-object v0, v1

    .line 2066043
    if-eqz v0, :cond_8

    .line 2066044
    const-string v0, "launcher_type"

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066045
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    move-object v1, v2

    .line 2066046
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2066047
    :cond_8
    return-void
.end method

.method public static a(LX/Dyt;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;LX/0am;LX/0am;)V
    .locals 5
    .param p0    # LX/Dyt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            ">;",
            "LX/0am",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2065973
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065974
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v1

    .line 2065975
    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 2065976
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v0

    .line 2065977
    const/4 v2, 0x1

    .line 2065978
    iput-boolean v2, v0, LX/5RO;->d:Z

    .line 2065979
    if-eqz p1, :cond_0

    .line 2065980
    invoke-virtual {v0, p1}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    .line 2065981
    :cond_0
    if-eqz p2, :cond_1

    .line 2065982
    invoke-virtual {v0, p2}, LX/5RO;->a(Ljava/lang/String;)LX/5RO;

    .line 2065983
    :cond_1
    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2065984
    invoke-virtual {p3}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2065985
    invoke-virtual {p3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2065986
    :cond_2
    invoke-virtual {p4}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2065987
    invoke-virtual {p4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2065988
    :cond_3
    iget-object v0, p0, LX/Dyt;->d:LX/1Kf;

    iget-object v2, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065989
    iget-object v3, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v2, v3

    .line 2065990
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    const/4 v3, 0x4

    iget-object v4, p0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2065991
    move-object v4, v4

    .line 2065992
    invoke-interface {v0, v2, v1, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2065993
    return-void
.end method

.method public static d(LX/Dyt;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2065795
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2065796
    const-string v1, "extra_place"

    invoke-static {v0, v1, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2065797
    iget-object v1, p0, LX/Dyt;->e:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    if-eqz v1, :cond_0

    .line 2065798
    const-string v1, "extra_implicit_location"

    iget-object v2, p0, LX/Dyt;->e:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2065799
    :cond_0
    invoke-static {p0, v0}, LX/Dyt;->a(LX/Dyt;Landroid/content/Intent;)V

    .line 2065800
    return-object v0
.end method

.method public static e(LX/Dyt;)Z
    .locals 1

    .prologue
    .line 2065957
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065958
    iget-boolean p0, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    move v0, p0

    .line 2065959
    return v0
.end method

.method public static g(LX/Dyt;)Z
    .locals 1

    .prologue
    .line 2066051
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2066052
    iget-boolean p0, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    move v0, p0

    .line 2066053
    return v0
.end method

.method public static h(LX/Dyt;)Z
    .locals 1

    .prologue
    .line 2065954
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065955
    iget-boolean p0, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    move v0, p0

    .line 2065956
    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2065871
    iget-object v0, p0, LX/Dyt;->f:LX/9j5;

    invoke-virtual {v0, p1}, LX/9j5;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2065872
    invoke-static {p0}, LX/Dyt;->h(LX/Dyt;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2065873
    iget-object v0, p0, LX/Dyt;->h:LX/9j9;

    .line 2065874
    iget-object v1, v0, LX/9j9;->c:LX/9jB;

    invoke-virtual {v1}, LX/9jB;->a()V

    .line 2065875
    iget-object v0, p0, LX/Dyt;->f:LX/9j5;

    .line 2065876
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_people_to_place_select"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065877
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 2065878
    sget-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->k:Z

    move v1, v1

    .line 2065879
    if-nez v1, :cond_8

    .line 2065880
    :cond_1
    :goto_1
    move v0, v0

    .line 2065881
    if-eqz v0, :cond_3

    .line 2065882
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Dyt;->g:Ljava/lang/String;

    .line 2065883
    iget-object v0, p0, LX/Dyt;->l:LX/92C;

    invoke-virtual {v0}, LX/92C;->a()V

    .line 2065884
    iget-object v0, p0, LX/Dyt;->a:LX/92D;

    invoke-virtual {v0}, LX/92D;->a()V

    .line 2065885
    iget-object v0, p0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2065886
    iget-object v1, p0, LX/Dyt;->o:LX/926;

    iget-object v2, p0, LX/Dyt;->r:Ljava/lang/String;

    .line 2065887
    invoke-static {}, Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;->newBuilder()LX/937;

    move-result-object v3

    invoke-static {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    .line 2065888
    iput-object v4, v3, LX/937;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065889
    move-object v3, v3

    .line 2065890
    sget-object v4, LX/93E;->OBJECT_PICKER:LX/93E;

    .line 2065891
    iput-object v4, v3, LX/937;->o:LX/93E;

    .line 2065892
    move-object v3, v3

    .line 2065893
    iput-object v2, v3, LX/937;->l:Ljava/lang/String;

    .line 2065894
    move-object v3, v3

    .line 2065895
    invoke-virtual {v3}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v3

    move-object v2, v3

    .line 2065896
    iget-object v3, p0, LX/Dyt;->q:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, LX/926;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    move-object v1, v1

    .line 2065897
    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2065898
    iget-object v0, p0, LX/Dyt;->f:LX/9j5;

    .line 2065899
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_minutiae_shown"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065900
    :goto_2
    return-void

    .line 2065901
    :cond_2
    invoke-static {p0}, LX/Dyt;->g(LX/Dyt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2065902
    iget-object v0, p0, LX/Dyt;->f:LX/9j5;

    .line 2065903
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_minutiae_to_place_select"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065904
    goto :goto_0

    .line 2065905
    :cond_3
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065906
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v4, v1

    .line 2065907
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v2

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    if-eqz v3, :cond_5

    move v3, v6

    :goto_3
    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableFriendTagging()Z

    move-result v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, LX/APL;->a(LX/2rw;ZZZZLX/ARN;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2065908
    :goto_4
    iget-object v0, p0, LX/Dyt;->k:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_7

    .line 2065909
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065910
    iget-boolean v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    move v0, v1

    .line 2065911
    if-nez v0, :cond_4

    invoke-static {p0}, LX/Dyt;->e(LX/Dyt;)Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz v6, :cond_7

    :cond_4
    iget-object v0, p0, LX/Dyt;->i:LX/9jA;

    .line 2065912
    sget-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->k:Z

    move v1, v1

    .line 2065913
    if-nez v1, :cond_9

    .line 2065914
    const/4 v1, 0x0

    .line 2065915
    :goto_5
    move v0, v1

    .line 2065916
    if-eqz v0, :cond_7

    .line 2065917
    invoke-static {}, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->newBuilder()LX/A5S;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    .line 2065918
    iput-object v1, v0, LX/A5S;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065919
    move-object v0, v0

    .line 2065920
    invoke-static {p0}, LX/Dyt;->e(LX/Dyt;)Z

    move-result v1

    .line 2065921
    iput-boolean v1, v0, LX/A5S;->e:Z

    .line 2065922
    move-object v0, v0

    .line 2065923
    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065924
    iget-object p1, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, p1

    .line 2065925
    iput-object v1, v0, LX/A5S;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2065926
    move-object v0, v0

    .line 2065927
    iget-object v1, p0, LX/Dyt;->e:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 2065928
    iput-object v1, v0, LX/A5S;->c:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 2065929
    move-object v0, v0

    .line 2065930
    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065931
    iget-object p1, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    move-object v1, p1

    .line 2065932
    iput-object v1, v0, LX/A5S;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2065933
    move-object v0, v0

    .line 2065934
    const/4 v1, 0x1

    .line 2065935
    iput-boolean v1, v0, LX/A5S;->f:Z

    .line 2065936
    move-object v0, v0

    .line 2065937
    invoke-virtual {v0}, LX/A5S;->a()Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    move-result-object v0

    .line 2065938
    iget-object v1, p0, LX/Dyt;->q:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->a(Landroid/content/Context;Lcom/facebook/tagging/conversion/FriendSelectorConfig;)Landroid/content/Intent;

    move-result-object v0

    .line 2065939
    move-object v0, v0

    .line 2065940
    iget-object v1, p0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_2

    :cond_5
    move v3, v7

    .line 2065941
    goto :goto_3

    :cond_6
    move v6, v7

    goto :goto_4

    .line 2065942
    :cond_7
    invoke-virtual {p0, p1}, LX/Dyt;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    goto/16 :goto_2

    .line 2065943
    :cond_8
    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065944
    iget-boolean v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    move v1, v2

    .line 2065945
    if-nez v1, :cond_1

    .line 2065946
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bw_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v1

    .line 2065947
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Dyt;->m:LX/9jG;

    invoke-static {p1, v1}, LX/DzN;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/9jG;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2065948
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Dyt;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065949
    iget-boolean v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->t:Z

    move v1, v2

    .line 2065950
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto/16 :goto_1

    .line 2065951
    :cond_9
    iget-object v1, v0, LX/9jA;->d:LX/1e2;

    invoke-virtual {v1}, LX/1e2;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2065952
    const/4 v1, 0x1

    goto :goto_5

    .line 2065953
    :cond_a
    iget-object v1, v0, LX/9jA;->c:LX/9jB;

    invoke-virtual {v1}, LX/9jB;->c()Z

    move-result v1

    goto/16 :goto_5
.end method

.method public final a(IILandroid/content/Intent;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 2065823
    if-ne p2, v2, :cond_2

    .line 2065824
    packed-switch p1, :pswitch_data_0

    .line 2065825
    :cond_0
    :goto_0
    return v0

    .line 2065826
    :pswitch_0
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065827
    invoke-static {p0}, LX/Dyt;->e(LX/Dyt;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2065828
    const-string v2, "minutiae_object"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2065829
    const/4 v3, 0x0

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p1

    invoke-static {p0, v0, v3, v2, p1}, LX/Dyt;->a(LX/Dyt;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;LX/0am;LX/0am;)V

    .line 2065830
    :goto_1
    move v0, v1

    .line 2065831
    goto :goto_0

    .line 2065832
    :pswitch_1
    const-string v4, "extra_place"

    invoke-static {p3, v4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065833
    invoke-static {p0}, LX/Dyt;->e(LX/Dyt;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2065834
    const-string v5, "extra_tagged_profiles"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 2065835
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2065836
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2065837
    iget-wide v8, v5, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v8, v9}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v8

    iget-object v9, v5, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 2065838
    iput-object v9, v8, LX/5Rc;->b:Ljava/lang/String;

    .line 2065839
    move-object v8, v8

    .line 2065840
    iget-object v5, v5, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 2065841
    iput-object v5, v8, LX/5Rc;->c:Ljava/lang/String;

    .line 2065842
    move-object v5, v8

    .line 2065843
    invoke-virtual {v5}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2065844
    :cond_1
    const/4 v5, 0x0

    iget-object v7, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065845
    iget-object v8, v7, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v7, v8

    .line 2065846
    invoke-static {v7}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v7

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-static {v6}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    invoke-static {p0, v4, v5, v7, v6}, LX/Dyt;->a(LX/Dyt;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;LX/0am;LX/0am;)V

    .line 2065847
    :goto_3
    move v0, v1

    .line 2065848
    goto :goto_0

    .line 2065849
    :pswitch_2
    iget-object v0, p0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-virtual {v0, v2, p3}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(ILandroid/content/Intent;)V

    move v0, v1

    .line 2065850
    goto/16 :goto_0

    .line 2065851
    :cond_2
    if-nez p2, :cond_0

    .line 2065852
    packed-switch p1, :pswitch_data_1

    goto/16 :goto_0

    .line 2065853
    :pswitch_3
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2065854
    const-string v3, "composer_cancelled"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2065855
    iget-object v3, p0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-virtual {v3, v0, v2}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(ILandroid/content/Intent;)V

    move v0, v1

    .line 2065856
    goto/16 :goto_0

    .line 2065857
    :pswitch_4
    if-eqz p3, :cond_3

    const-string v0, "extra_place"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2065858
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, LX/Dyt;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    :cond_3
    move v0, v1

    .line 2065859
    goto/16 :goto_0

    .line 2065860
    :cond_4
    invoke-static {p0, v0}, LX/Dyt;->d(LX/Dyt;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Landroid/content/Intent;

    move-result-object v0

    .line 2065861
    iget-object v2, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065862
    iget-object v3, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v2, v3

    .line 2065863
    if-eqz v2, :cond_5

    .line 2065864
    const-string v2, "minutiae_object"

    iget-object v3, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065865
    iget-object p1, v3, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v3, p1

    .line 2065866
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2065867
    :cond_5
    iget-object v0, p0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, p3}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 2065868
    :cond_6
    invoke-static {p0, p3}, LX/Dyt;->a(LX/Dyt;Landroid/content/Intent;)V

    .line 2065869
    const-string v4, "tag_people_after_tag_place"

    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2065870
    iget-object v4, p0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 v5, -0x1

    invoke-virtual {v4, v5, p3}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(ILandroid/content/Intent;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V
    .locals 3

    .prologue
    .line 2065817
    invoke-static {p0}, LX/Dyt;->e(LX/Dyt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2065818
    const/4 v0, 0x0

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065819
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 2065820
    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-static {p0, p1, v0, v1, v2}, LX/Dyt;->a(LX/Dyt;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;LX/0am;LX/0am;)V

    .line 2065821
    :goto_0
    return-void

    .line 2065822
    :cond_0
    iget-object v0, p0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 v1, -0x1

    invoke-static {p0, p1}, LX/Dyt;->d(LX/Dyt;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2065806
    invoke-static {p0}, LX/Dyt;->e(LX/Dyt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2065807
    const/4 v0, 0x0

    iget-object v1, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065808
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 2065809
    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-static {p0, v0, p1, v1, v2}, LX/Dyt;->a(LX/Dyt;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;LX/0am;LX/0am;)V

    .line 2065810
    :goto_0
    return-void

    .line 2065811
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2065812
    const-string v1, "text_only_place"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2065813
    iget-object v1, p0, LX/Dyt;->e:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    if-eqz v1, :cond_1

    .line 2065814
    const-string v1, "extra_implicit_location"

    iget-object v2, p0, LX/Dyt;->e:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2065815
    :cond_1
    invoke-static {p0, v0}, LX/Dyt;->a(LX/Dyt;Landroid/content/Intent;)V

    .line 2065816
    iget-object v1, p0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2065801
    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065802
    iget-boolean v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    move v0, v1

    .line 2065803
    if-nez v0, :cond_0

    iget-object v0, p0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065804
    iget-boolean v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    move v0, v1

    .line 2065805
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
