.class public final LX/DGt;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DGu;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/DGm;

.field public final synthetic c:LX/DGu;


# direct methods
.method public constructor <init>(LX/DGu;)V
    .locals 1

    .prologue
    .line 1980918
    iput-object p1, p0, LX/DGt;->c:LX/DGu;

    .line 1980919
    move-object v0, p1

    .line 1980920
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1980921
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1980922
    const-string v0, "LinkSetsPageItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1980923
    if-ne p0, p1, :cond_1

    .line 1980924
    :cond_0
    :goto_0
    return v0

    .line 1980925
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1980926
    goto :goto_0

    .line 1980927
    :cond_3
    check-cast p1, LX/DGt;

    .line 1980928
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1980929
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1980930
    if-eq v2, v3, :cond_0

    .line 1980931
    iget-object v2, p0, LX/DGt;->a:LX/1Pv;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DGt;->a:LX/1Pv;

    iget-object v3, p1, LX/DGt;->a:LX/1Pv;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1980932
    goto :goto_0

    .line 1980933
    :cond_5
    iget-object v2, p1, LX/DGt;->a:LX/1Pv;

    if-nez v2, :cond_4

    .line 1980934
    :cond_6
    iget-object v2, p0, LX/DGt;->b:LX/DGm;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/DGt;->b:LX/DGm;

    iget-object v3, p1, LX/DGt;->b:LX/DGm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1980935
    goto :goto_0

    .line 1980936
    :cond_7
    iget-object v2, p1, LX/DGt;->b:LX/DGm;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
