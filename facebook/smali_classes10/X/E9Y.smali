.class public LX/E9Y;
.super LX/E9M;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/E9M",
        "<",
        "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsInterfaces$UserReviews$AuthoredReviews$Edges;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/9Eb;

.field public final b:LX/E9R;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsInterfaces$UserReviews$AuthoredReviews$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Ea;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsInterfaces$UserReviews$AuthoredReviews$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsInterfaces$UserReviews$AuthoredReviews$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsInterfaces$UserReviews$AuthoredReviews$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:Z


# direct methods
.method public constructor <init>(LX/9Eb;LX/E9R;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2084528
    invoke-direct {p0}, LX/E9M;-><init>()V

    .line 2084529
    iput-object p2, p0, LX/E9Y;->b:LX/E9R;

    .line 2084530
    iput-object p1, p0, LX/E9Y;->a:LX/9Eb;

    .line 2084531
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/E9Y;->e:Ljava/util/Map;

    .line 2084532
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/E9Y;->f:Ljava/util/Map;

    .line 2084533
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/E9Y;->g:Ljava/util/Map;

    .line 2084534
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E9Y;->c:Ljava/util/List;

    .line 2084535
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E9Y;->d:Ljava/util/List;

    .line 2084536
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/E9Y;->j:Z

    .line 2084537
    return-void
.end method

.method public static a$redex0(LX/E9Y;ILcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 2

    .prologue
    .line 2084538
    if-eqz p2, :cond_0

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    move-object v1, v0

    .line 2084539
    :goto_0
    iget-object v0, p0, LX/E9Y;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ea;

    invoke-virtual {v0, v1}, LX/9Ea;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2084540
    return-void

    .line 2084541
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method public static a$redex0(LX/E9Y;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)V
    .locals 3

    .prologue
    .line 2084542
    iget-object v0, p0, LX/E9Y;->f:Ljava/util/Map;

    invoke-static {p1}, LX/BNJ;->b(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2084543
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v0

    .line 2084544
    invoke-static {v0}, LX/BNJ;->b(LX/5tj;)Ljava/lang/String;

    move-result-object v1

    .line 2084545
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2084546
    iget-object v2, p0, LX/E9Y;->g:Ljava/util/Map;

    invoke-interface {v2, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2084547
    :cond_0
    invoke-static {v0}, LX/BNJ;->c(LX/5tj;)Ljava/lang/String;

    move-result-object v0

    .line 2084548
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2084549
    iget-object v1, p0, LX/E9Y;->e:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2084550
    :cond_1
    return-void
.end method

.method public static e(LX/E9Y;I)LX/9Ea;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2084551
    iget-object v0, p0, LX/E9Y;->a:LX/9Eb;

    new-instance v1, LX/E9X;

    invoke-direct {v1, p0, p1}, LX/E9X;-><init>(LX/E9Y;I)V

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/9Eb;->a(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;)LX/9Ea;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/E9Y;I)V
    .locals 1

    .prologue
    .line 2084552
    :goto_0
    iget-object v0, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2084553
    iget-object v0, p0, LX/E9Y;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Ea;

    invoke-virtual {v0}, LX/9Ea;->a()V

    .line 2084554
    iget-object v0, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v0

    invoke-interface {v0}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    invoke-static {v0}, LX/9JZ;->a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/E9Y;->a$redex0(LX/E9Y;ILcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 2084555
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2084556
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)I
    .locals 3

    .prologue
    .line 2084557
    iget-object v0, p0, LX/E9Y;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E9Y;->c:Ljava/util/List;

    iget-object v1, p0, LX/E9Y;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2084558
    :goto_0
    iget-object v1, p0, LX/E9Y;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2084559
    iget-object v1, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2084560
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v1

    invoke-interface {v1}, LX/5tj;->bh_()LX/1VU;

    move-result-object v1

    invoke-static {v1}, LX/9JZ;->a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/E9Y;->a$redex0(LX/E9Y;ILcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 2084561
    :goto_1
    invoke-static {p0, p2}, LX/E9Y;->a$redex0(LX/E9Y;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)V

    .line 2084562
    return v0

    .line 2084563
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2084564
    :cond_1
    iget-object v1, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v1, v0, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2084565
    iget-object v1, p0, LX/E9Y;->d:Ljava/util/List;

    invoke-static {p0, v0}, LX/E9Y;->e(LX/E9Y;I)LX/9Ea;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2084566
    invoke-static {p0, v0}, LX/E9Y;->f(LX/E9Y;I)V

    goto :goto_1
.end method

.method public final a(I)LX/E9W;
    .locals 1

    .prologue
    .line 2084504
    sget-object v0, LX/E9W;->USER_REVIEW:LX/E9W;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2084527
    iget-object v0, p0, LX/E9Y;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel;)V
    .locals 7
    .param p1    # Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2084512
    if-nez p1, :cond_0

    .line 2084513
    :goto_0
    return-void

    .line 2084514
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    .line 2084515
    invoke-static {v0}, LX/BNJ;->b(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)Ljava/lang/String;

    move-result-object v5

    .line 2084516
    if-eqz v0, :cond_1

    if-eqz v5, :cond_1

    iget-object v6, p0, LX/E9Y;->f:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2084517
    invoke-static {p0, v0}, LX/E9Y;->a$redex0(LX/E9Y;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)V

    .line 2084518
    iget-object v5, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2084519
    iget-object v0, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2084520
    invoke-static {p0, v0}, LX/E9Y;->e(LX/E9Y;I)LX/9Ea;

    move-result-object v6

    .line 2084521
    iget-object v5, p0, LX/E9Y;->d:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2084522
    iget-object v5, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    invoke-virtual {v5}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v5

    invoke-interface {v5}, LX/5tj;->bh_()LX/1VU;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2084523
    iget-object v5, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    invoke-virtual {v5}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v5

    invoke-interface {v5}, LX/5tj;->bh_()LX/1VU;

    move-result-object v5

    invoke-static {v5}, LX/9JZ;->a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/9Ea;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2084524
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2084525
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/E9Y;->i:Ljava/lang/String;

    .line 2084526
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    iput-boolean v0, p0, LX/E9Y;->j:Z

    goto/16 :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2084511
    iget-object v0, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 2084510
    iget-object v0, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/E9W;
    .locals 1

    .prologue
    .line 2084509
    iget-object v0, p0, LX/E9Y;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, LX/E9W;->NO_HEADER:LX/E9W;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/E9W;->DEFAULT_HEADER:LX/E9W;

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2084508
    iget-boolean v0, p0, LX/E9Y;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/E9Y;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2084507
    iget-object v0, p0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic l()LX/E9Q;
    .locals 1

    .prologue
    .line 2084505
    iget-object v0, p0, LX/E9Y;->b:LX/E9R;

    move-object v0, v0

    .line 2084506
    return-object v0
.end method
