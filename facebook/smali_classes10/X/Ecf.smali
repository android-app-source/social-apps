.class public final LX/Ecf;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EcJ;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ecf;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/Ecf;


# instance fields
.field public aliceBaseKey_:LX/EWc;

.field public bitField0_:I

.field public localIdentityPublic_:LX/EWc;

.field public localRegistrationId_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public needsRefresh_:Z

.field public pendingKeyExchange_:LX/Eca;

.field public pendingPreKey_:LX/Ece;

.field public previousCounter_:I

.field public receiverChains_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EcW;",
            ">;"
        }
    .end annotation
.end field

.field public remoteIdentityPublic_:LX/EWc;

.field public remoteRegistrationId_:I

.field public rootKey_:LX/EWc;

.field public senderChain_:LX/EcW;

.field public sessionVersion_:I

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2147618
    new-instance v0, LX/EcI;

    invoke-direct {v0}, LX/EcI;-><init>()V

    sput-object v0, LX/Ecf;->a:LX/EWZ;

    .line 2147619
    new-instance v0, LX/Ecf;

    invoke-direct {v0}, LX/Ecf;-><init>()V

    .line 2147620
    sput-object v0, LX/Ecf;->c:LX/Ecf;

    invoke-direct {v0}, LX/Ecf;->S()V

    .line 2147621
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2147622
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147623
    iput-byte v0, p0, LX/Ecf;->memoizedIsInitialized:B

    .line 2147624
    iput v0, p0, LX/Ecf;->memoizedSerializedSize:I

    .line 2147625
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2147626
    iput-object v0, p0, LX/Ecf;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v3, 0x0

    const/16 v7, 0x40

    .line 2147627
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147628
    iput-byte v1, p0, LX/Ecf;->memoizedIsInitialized:B

    .line 2147629
    iput v1, p0, LX/Ecf;->memoizedSerializedSize:I

    .line 2147630
    invoke-direct {p0}, LX/Ecf;->S()V

    .line 2147631
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v6

    move v4, v0

    move v1, v0

    .line 2147632
    :cond_0
    :goto_0
    if-nez v4, :cond_6

    .line 2147633
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2147634
    sparse-switch v0, :sswitch_data_0

    .line 2147635
    invoke-virtual {p0, p1, v6, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v5

    .line 2147636
    goto :goto_0

    :sswitch_0
    move v4, v5

    .line 2147637
    goto :goto_0

    .line 2147638
    :sswitch_1
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ecf;->bitField0_:I

    .line 2147639
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v0

    iput v0, p0, LX/Ecf;->sessionVersion_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2147640
    :catch_0
    move-exception v0

    .line 2147641
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147642
    move-object v0, v0

    .line 2147643
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2147644
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x40

    if-ne v1, v7, :cond_1

    .line 2147645
    iget-object v1, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    .line 2147646
    :cond_1
    invoke-virtual {v6}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/Ecf;->unknownFields:LX/EZQ;

    .line 2147647
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2147648
    :sswitch_2
    :try_start_2
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/Ecf;->bitField0_:I

    .line 2147649
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->localIdentityPublic_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2147650
    :catch_1
    move-exception v0

    .line 2147651
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2147652
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147653
    move-object v0, v2

    .line 2147654
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2147655
    :sswitch_3
    :try_start_4
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/Ecf;->bitField0_:I

    .line 2147656
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->remoteIdentityPublic_:LX/EWc;

    goto :goto_0

    .line 2147657
    :sswitch_4
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/Ecf;->bitField0_:I

    .line 2147658
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->rootKey_:LX/EWc;

    goto :goto_0

    .line 2147659
    :sswitch_5
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/Ecf;->bitField0_:I

    .line 2147660
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v0

    iput v0, p0, LX/Ecf;->previousCounter_:I

    goto :goto_0

    .line 2147661
    :sswitch_6
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_a

    .line 2147662
    iget-object v0, p0, LX/Ecf;->senderChain_:LX/EcW;

    invoke-virtual {v0}, LX/EcW;->r()LX/EcN;

    move-result-object v0

    move-object v2, v0

    .line 2147663
    :goto_1
    sget-object v0, LX/EcW;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EcW;

    iput-object v0, p0, LX/Ecf;->senderChain_:LX/EcW;

    .line 2147664
    if-eqz v2, :cond_2

    .line 2147665
    iget-object v0, p0, LX/Ecf;->senderChain_:LX/EcW;

    invoke-virtual {v2, v0}, LX/EcN;->a(LX/EcW;)LX/EcN;

    .line 2147666
    invoke-virtual {v2}, LX/EcN;->m()LX/EcW;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->senderChain_:LX/EcW;

    .line 2147667
    :cond_2
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, LX/Ecf;->bitField0_:I

    goto/16 :goto_0

    .line 2147668
    :sswitch_7
    and-int/lit8 v0, v1, 0x40

    if-eq v0, v7, :cond_3

    .line 2147669
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    .line 2147670
    or-int/lit8 v1, v1, 0x40

    .line 2147671
    :cond_3
    iget-object v0, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    sget-object v2, LX/EcW;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2147672
    :sswitch_8
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-ne v0, v7, :cond_9

    .line 2147673
    iget-object v0, p0, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    invoke-virtual {v0}, LX/Eca;->C()LX/EcZ;

    move-result-object v0

    move-object v2, v0

    .line 2147674
    :goto_2
    sget-object v0, LX/Eca;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/Eca;

    iput-object v0, p0, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    .line 2147675
    if-eqz v2, :cond_4

    .line 2147676
    iget-object v0, p0, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    invoke-virtual {v2, v0}, LX/EcZ;->a(LX/Eca;)LX/EcZ;

    .line 2147677
    invoke-virtual {v2}, LX/EcZ;->l()LX/Eca;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    .line 2147678
    :cond_4
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, LX/Ecf;->bitField0_:I

    goto/16 :goto_0

    .line 2147679
    :sswitch_9
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_8

    .line 2147680
    iget-object v0, p0, LX/Ecf;->pendingPreKey_:LX/Ece;

    invoke-virtual {v0}, LX/Ece;->q()LX/Ecd;

    move-result-object v0

    move-object v2, v0

    .line 2147681
    :goto_3
    sget-object v0, LX/Ece;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/Ece;

    iput-object v0, p0, LX/Ecf;->pendingPreKey_:LX/Ece;

    .line 2147682
    if-eqz v2, :cond_5

    .line 2147683
    iget-object v0, p0, LX/Ecf;->pendingPreKey_:LX/Ece;

    invoke-virtual {v2, v0}, LX/Ecd;->a(LX/Ece;)LX/Ecd;

    .line 2147684
    invoke-virtual {v2}, LX/Ecd;->m()LX/Ece;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->pendingPreKey_:LX/Ece;

    .line 2147685
    :cond_5
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, LX/Ecf;->bitField0_:I

    goto/16 :goto_0

    .line 2147686
    :sswitch_a
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, LX/Ecf;->bitField0_:I

    .line 2147687
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v0

    iput v0, p0, LX/Ecf;->remoteRegistrationId_:I

    goto/16 :goto_0

    .line 2147688
    :sswitch_b
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, LX/Ecf;->bitField0_:I

    .line 2147689
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v0

    iput v0, p0, LX/Ecf;->localRegistrationId_:I

    goto/16 :goto_0

    .line 2147690
    :sswitch_c
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, LX/Ecf;->bitField0_:I

    .line 2147691
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v0

    iput-boolean v0, p0, LX/Ecf;->needsRefresh_:Z

    goto/16 :goto_0

    .line 2147692
    :sswitch_d
    iget v0, p0, LX/Ecf;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, LX/Ecf;->bitField0_:I

    .line 2147693
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->aliceBaseKey_:LX/EWc;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2147694
    :cond_6
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v7, :cond_7

    .line 2147695
    iget-object v0, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    .line 2147696
    :cond_7
    invoke-virtual {v6}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->unknownFields:LX/EZQ;

    .line 2147697
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2147698
    return-void

    :cond_8
    move-object v2, v3

    goto :goto_3

    :cond_9
    move-object v2, v3

    goto/16 :goto_2

    :cond_a
    move-object v2, v3

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2147699
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2147700
    iput-byte v1, p0, LX/Ecf;->memoizedIsInitialized:B

    .line 2147701
    iput v1, p0, LX/Ecf;->memoizedSerializedSize:I

    .line 2147702
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->unknownFields:LX/EZQ;

    .line 2147703
    return-void
.end method

.method private S()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2147704
    iput v1, p0, LX/Ecf;->sessionVersion_:I

    .line 2147705
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ecf;->localIdentityPublic_:LX/EWc;

    .line 2147706
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ecf;->remoteIdentityPublic_:LX/EWc;

    .line 2147707
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ecf;->rootKey_:LX/EWc;

    .line 2147708
    iput v1, p0, LX/Ecf;->previousCounter_:I

    .line 2147709
    sget-object v0, LX/EcW;->c:LX/EcW;

    move-object v0, v0

    .line 2147710
    iput-object v0, p0, LX/Ecf;->senderChain_:LX/EcW;

    .line 2147711
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    .line 2147712
    sget-object v0, LX/Eca;->c:LX/Eca;

    move-object v0, v0

    .line 2147713
    iput-object v0, p0, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    .line 2147714
    sget-object v0, LX/Ece;->c:LX/Ece;

    move-object v0, v0

    .line 2147715
    iput-object v0, p0, LX/Ecf;->pendingPreKey_:LX/Ece;

    .line 2147716
    iput v1, p0, LX/Ecf;->remoteRegistrationId_:I

    .line 2147717
    iput v1, p0, LX/Ecf;->localRegistrationId_:I

    .line 2147718
    iput-boolean v1, p0, LX/Ecf;->needsRefresh_:Z

    .line 2147719
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ecf;->aliceBaseKey_:LX/EWc;

    .line 2147720
    return-void
.end method

.method public static a(LX/Ecf;)LX/EcK;
    .locals 1

    .prologue
    .line 2147721
    invoke-static {}, LX/EcK;->x()LX/EcK;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EcK;->a(LX/Ecf;)LX/EcK;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final D()Z
    .locals 2

    .prologue
    .line 2147722
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final O()LX/EcK;
    .locals 1

    .prologue
    .line 2147723
    invoke-static {p0}, LX/Ecf;->a(LX/Ecf;)LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2147724
    new-instance v0, LX/EcK;

    invoke-direct {v0, p1}, LX/EcK;-><init>(LX/EYd;)V

    .line 2147725
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2147583
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2147584
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2147585
    iget v0, p0, LX/Ecf;->sessionVersion_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2147586
    :cond_0
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2147587
    iget-object v0, p0, LX/Ecf;->localIdentityPublic_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2147588
    :cond_1
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2147589
    const/4 v0, 0x3

    iget-object v1, p0, LX/Ecf;->remoteIdentityPublic_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2147590
    :cond_2
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 2147591
    iget-object v0, p0, LX/Ecf;->rootKey_:LX/EWc;

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2147592
    :cond_3
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2147593
    const/4 v0, 0x5

    iget v1, p0, LX/Ecf;->previousCounter_:I

    invoke-virtual {p1, v0, v1}, LX/EWf;->c(II)V

    .line 2147594
    :cond_4
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 2147595
    const/4 v0, 0x6

    iget-object v1, p0, LX/Ecf;->senderChain_:LX/EcW;

    invoke-virtual {p1, v0, v1}, LX/EWf;->b(ILX/EWW;)V

    .line 2147596
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 2147597
    const/4 v2, 0x7

    iget-object v0, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v2, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2147598
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2147599
    :cond_6
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 2147600
    iget-object v0, p0, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    invoke-virtual {p1, v4, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2147601
    :cond_7
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 2147602
    const/16 v0, 0x9

    iget-object v1, p0, LX/Ecf;->pendingPreKey_:LX/Ece;

    invoke-virtual {p1, v0, v1}, LX/EWf;->b(ILX/EWW;)V

    .line 2147603
    :cond_8
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 2147604
    const/16 v0, 0xa

    iget v1, p0, LX/Ecf;->remoteRegistrationId_:I

    invoke-virtual {p1, v0, v1}, LX/EWf;->c(II)V

    .line 2147605
    :cond_9
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 2147606
    const/16 v0, 0xb

    iget v1, p0, LX/Ecf;->localRegistrationId_:I

    invoke-virtual {p1, v0, v1}, LX/EWf;->c(II)V

    .line 2147607
    :cond_a
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_b

    .line 2147608
    const/16 v0, 0xc

    iget-boolean v1, p0, LX/Ecf;->needsRefresh_:Z

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(IZ)V

    .line 2147609
    :cond_b
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_c

    .line 2147610
    const/16 v0, 0xd

    iget-object v1, p0, LX/Ecf;->aliceBaseKey_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2147611
    :cond_c
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2147612
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2147613
    iget-byte v1, p0, LX/Ecf;->memoizedIsInitialized:B

    .line 2147614
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2147615
    :goto_0
    return v0

    .line 2147616
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2147617
    :cond_1
    iput-byte v0, p0, LX/Ecf;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2147542
    iget v0, p0, LX/Ecf;->memoizedSerializedSize:I

    .line 2147543
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2147544
    :goto_0
    return v0

    .line 2147545
    :cond_0
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_d

    .line 2147546
    iget v0, p0, LX/Ecf;->sessionVersion_:I

    invoke-static {v3, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2147547
    :goto_1
    iget v2, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2147548
    iget-object v2, p0, LX/Ecf;->localIdentityPublic_:LX/EWc;

    invoke-static {v4, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2147549
    :cond_1
    iget v2, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 2147550
    const/4 v2, 0x3

    iget-object v3, p0, LX/Ecf;->remoteIdentityPublic_:LX/EWc;

    invoke-static {v2, v3}, LX/EWf;->c(ILX/EWc;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2147551
    :cond_2
    iget v2, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_3

    .line 2147552
    iget-object v2, p0, LX/Ecf;->rootKey_:LX/EWc;

    invoke-static {v5, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2147553
    :cond_3
    iget v2, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 2147554
    const/4 v2, 0x5

    iget v3, p0, LX/Ecf;->previousCounter_:I

    invoke-static {v2, v3}, LX/EWf;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2147555
    :cond_4
    iget v2, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 2147556
    const/4 v2, 0x6

    iget-object v3, p0, LX/Ecf;->senderChain_:LX/EcW;

    invoke-static {v2, v3}, LX/EWf;->e(ILX/EWW;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    move v2, v0

    .line 2147557
    :goto_2
    iget-object v0, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 2147558
    const/4 v3, 0x7

    iget-object v0, p0, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2147559
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2147560
    :cond_6
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 2147561
    iget-object v0, p0, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    invoke-static {v6, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2147562
    :cond_7
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 2147563
    const/16 v0, 0x9

    iget-object v1, p0, LX/Ecf;->pendingPreKey_:LX/Ece;

    invoke-static {v0, v1}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2147564
    :cond_8
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 2147565
    const/16 v0, 0xa

    iget v1, p0, LX/Ecf;->remoteRegistrationId_:I

    invoke-static {v0, v1}, LX/EWf;->g(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 2147566
    :cond_9
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 2147567
    const/16 v0, 0xb

    iget v1, p0, LX/Ecf;->localRegistrationId_:I

    invoke-static {v0, v1}, LX/EWf;->g(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 2147568
    :cond_a
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_b

    .line 2147569
    const/16 v0, 0xc

    iget-boolean v1, p0, LX/Ecf;->needsRefresh_:Z

    invoke-static {v0, v1}, LX/EWf;->b(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 2147570
    :cond_b
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_c

    .line 2147571
    const/16 v0, 0xd

    iget-object v1, p0, LX/Ecf;->aliceBaseKey_:LX/EWc;

    invoke-static {v0, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2147572
    :cond_c
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2147573
    iput v0, p0, LX/Ecf;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_d
    move v0, v1

    goto/16 :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2147574
    iget-object v0, p0, LX/Ecf;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2147575
    sget-object v0, LX/Eck;->b:LX/EYn;

    const-class v1, LX/Ecf;

    const-class v2, LX/EcK;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ecf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2147576
    sget-object v0, LX/Ecf;->a:LX/EWZ;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 2147577
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2147578
    invoke-virtual {p0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2147579
    invoke-static {}, LX/EcK;->x()LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2147580
    invoke-virtual {p0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2147581
    sget-object v0, LX/Ecf;->c:LX/Ecf;

    return-object v0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 2147582
    iget v0, p0, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
