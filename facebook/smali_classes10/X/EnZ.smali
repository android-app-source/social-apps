.class public LX/EnZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EnY;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Landroid/app/Activity;

.field public e:LX/Emj;

.field private f:LX/0h5;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/Emj;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2167275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167276
    iput-object p1, p0, LX/EnZ;->d:Landroid/app/Activity;

    .line 2167277
    iput-object p2, p0, LX/EnZ;->e:LX/Emj;

    .line 2167278
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2167273
    iget-object v0, p0, LX/EnZ;->f:LX/0h5;

    instance-of v0, v0, Landroid/view/View;

    const-string v1, "Called getTitleHeight before initializing"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2167274
    iget-object v0, p0, LX/EnZ;->f:LX/0h5;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    return v0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 2167272
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2167253
    invoke-static {p1}, LX/63Z;->a(Landroid/view/View;)Z

    .line 2167254
    const v0, 0x7f0d00bc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, LX/EnZ;->f:LX/0h5;

    .line 2167255
    if-eqz p2, :cond_0

    .line 2167256
    invoke-virtual {p0, p2}, LX/EnZ;->a(Ljava/lang/String;)V

    .line 2167257
    :cond_0
    iget-object v0, p0, LX/EnZ;->f:LX/0h5;

    new-instance v1, LX/EnW;

    invoke-direct {v1, p0}, LX/EnW;-><init>(LX/EnZ;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2167258
    iget-object v0, p0, LX/EnZ;->c:LX/0Uh;

    const/16 v1, 0x24f

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_1

    .line 2167259
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f0208ac

    .line 2167260
    iput v1, v0, LX/108;->i:I

    .line 2167261
    move-object v0, v0

    .line 2167262
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2167263
    iget-object v1, p0, LX/EnZ;->f:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2167264
    iget-object v0, p0, LX/EnZ;->f:LX/0h5;

    new-instance v1, LX/EnX;

    invoke-direct {v1, p0}, LX/EnX;-><init>(LX/EnZ;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2167265
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2167268
    iget-object v0, p0, LX/EnZ;->f:LX/0h5;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Called setTitleString before initializing"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2167269
    iget-object v0, p0, LX/EnZ;->f:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2167270
    return-void

    .line 2167271
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2167266
    iget-object v0, p0, LX/EnZ;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 2167267
    const v1, 0x7f0b2210

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method
