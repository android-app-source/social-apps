.class public LX/D4Q;
.super LX/2oV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "LX/2oV",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/animation/ValueAnimator;

.field private final c:LX/D4i;

.field public d:LX/D4P;

.field private e:LX/D4P;

.field public f:F

.field public g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/D5z;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/D4i;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962116
    invoke-direct {p0, p1}, LX/2oV;-><init>(Ljava/lang/String;)V

    .line 1962117
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/D4Q;->a:Ljava/util/Set;

    .line 1962118
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    .line 1962119
    sget-object v0, LX/D4P;->DIMMED:LX/D4P;

    iput-object v0, p0, LX/D4Q;->d:LX/D4P;

    .line 1962120
    const/4 v0, 0x0

    iput-object v0, p0, LX/D4Q;->e:LX/D4P;

    .line 1962121
    iput-object p2, p0, LX/D4Q;->c:LX/D4i;

    .line 1962122
    iget-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1962123
    const v0, 0x3da3d70a    # 0.08f

    iput v0, p0, LX/D4Q;->f:F

    .line 1962124
    return-void
.end method

.method public static a(LX/D4Q;LX/D4P;)V
    .locals 1

    .prologue
    .line 1962125
    iget-object v0, p0, LX/D4Q;->d:LX/D4P;

    iput-object v0, p0, LX/D4Q;->e:LX/D4P;

    .line 1962126
    iput-object p1, p0, LX/D4Q;->d:LX/D4P;

    .line 1962127
    return-void
.end method

.method public static a$redex0(LX/D4Q;F)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1962128
    iget-object v0, p0, LX/D4Q;->d:LX/D4P;

    sget-object v3, LX/D4P;->DIMMED:LX/D4P;

    if-eq v0, v3, :cond_1

    iget-object v0, p0, LX/D4Q;->d:LX/D4P;

    sget-object v3, LX/D4P;->UNDIMMED:LX/D4P;

    if-ne v0, v3, :cond_0

    iget-object v0, p0, LX/D4Q;->e:LX/D4P;

    sget-object v3, LX/D4P;->IMMERSED:LX/D4P;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, LX/D4Q;->e:LX/D4P;

    sget-object v3, LX/D4P;->FOCUS_DIMMED:LX/D4P;

    if-ne v0, v3, :cond_1

    :cond_0
    iget-object v0, p0, LX/D4Q;->d:LX/D4P;

    sget-object v3, LX/D4P;->IMMERSED:LX/D4P;

    if-ne v0, v3, :cond_3

    :cond_1
    move v0, v2

    .line 1962129
    :goto_0
    iget-object v3, p0, LX/D4Q;->d:LX/D4P;

    sget-object v4, LX/D4P;->IMMERSED:LX/D4P;

    if-eq v3, v4, :cond_4

    .line 1962130
    :goto_1
    iget-object v1, p0, LX/D4Q;->h:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_6

    const/4 v1, 0x0

    :goto_2
    move-object v1, v1

    .line 1962131
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 1962132
    invoke-virtual {v1, p1}, LX/D5z;->setAlpha(F)V

    .line 1962133
    :cond_2
    if-eqz v2, :cond_5

    .line 1962134
    iget-object v0, p0, LX/D4Q;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1962135
    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_3

    :cond_3
    move v0, v1

    .line 1962136
    goto :goto_0

    :cond_4
    move v2, v1

    .line 1962137
    goto :goto_1

    .line 1962138
    :cond_5
    return-void

    :cond_6
    iget-object v1, p0, LX/D4Q;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/D5z;

    goto :goto_2
.end method

.method public static k(LX/D4Q;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v0, 0x12c

    const/high16 v2, 0x3f800000    # 1.0f

    const v1, 0x3da3d70a    # 0.08f

    .line 1962139
    sget-object v3, LX/D4O;->a:[I

    iget-object v4, p0, LX/D4Q;->d:LX/D4P;

    invoke-virtual {v4}, LX/D4P;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1962140
    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move v8, v2

    move v2, v1

    move v1, v8

    .line 1962141
    :goto_1
    :pswitch_1
    iget-object v3, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1962142
    iget-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, LX/D4Q;->f:F

    cmpl-float v0, v1, v0

    if-nez v0, :cond_1

    .line 1962143
    invoke-static {p0, v1}, LX/D4Q;->a$redex0(LX/D4Q;F)V

    goto :goto_0

    .line 1962144
    :pswitch_2
    const/16 v0, 0x7d0

    .line 1962145
    goto :goto_1

    :pswitch_3
    move v8, v2

    move v2, v1

    move v1, v8

    .line 1962146
    goto :goto_1

    .line 1962147
    :cond_1
    iget-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1962148
    iput v1, p0, LX/D4Q;->f:F

    .line 1962149
    iget-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [F

    aput v2, v3, v6

    aput v1, v3, v7

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 1962150
    iget-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1962151
    iget-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    new-instance v1, LX/D4N;

    invoke-direct {v1, p0}, LX/D4N;-><init>(LX/D4Q;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1962152
    iget-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 1962153
    :cond_2
    iget v0, p0, LX/D4Q;->f:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_0

    .line 1962154
    iput v1, p0, LX/D4Q;->f:F

    .line 1962155
    iget-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 1962156
    iget-object v2, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1962157
    iget-object v2, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [F

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v3, v6

    aput v1, v3, v7

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 1962158
    iget-object v0, p0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1962159
    instance-of v0, p1, LX/D5z;

    if-eqz v0, :cond_0

    .line 1962160
    new-instance v0, Ljava/lang/ref/WeakReference;

    check-cast p1, LX/D5z;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D4Q;->h:Ljava/lang/ref/WeakReference;

    .line 1962161
    :goto_0
    invoke-static {p0}, LX/D4Q;->k(LX/D4Q;)V

    .line 1962162
    return-void

    .line 1962163
    :cond_0
    iget-object v0, p0, LX/D4Q;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 1

    .prologue
    .line 1962164
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D4Q;->g:Ljava/lang/ref/WeakReference;

    .line 1962165
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1962166
    iget-object v0, p0, LX/D4Q;->c:LX/D4i;

    .line 1962167
    invoke-static {v0}, LX/D4i;->g(LX/D4i;)LX/D4Q;

    move-result-object v1

    .line 1962168
    if-ne v1, p0, :cond_0

    .line 1962169
    iget-object v1, v0, LX/D4i;->a:LX/D4g;

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, LX/D4g;->removeMessages(I)V

    .line 1962170
    iget-object v1, v0, LX/D4i;->f:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 1962171
    :goto_0
    iget-object v1, p0, LX/D4Q;->d:LX/D4P;

    sget-object p1, LX/D4P;->DIMMED:LX/D4P;

    if-ne v1, p1, :cond_3

    .line 1962172
    :goto_1
    const/4 v1, 0x0

    iput-object v1, v0, LX/D4i;->e:Ljava/lang/ref/WeakReference;

    .line 1962173
    :cond_0
    return-void

    .line 1962174
    :cond_1
    iget-object v1, v0, LX/D4i;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1962175
    if-eqz v1, :cond_2

    .line 1962176
    iget-object p1, v0, LX/D4i;->b:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    invoke-virtual {v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;)V

    .line 1962177
    iget-object p1, v0, LX/D4i;->c:LX/D4h;

    invoke-virtual {v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oa;)V

    .line 1962178
    iget-object v1, v0, LX/D4i;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->clear()V

    .line 1962179
    :cond_2
    const/4 v1, 0x0

    iput-object v1, v0, LX/D4i;->f:Ljava/lang/ref/WeakReference;

    goto :goto_0

    .line 1962180
    :cond_3
    sget-object v1, LX/D4P;->DIMMED:LX/D4P;

    invoke-static {p0, v1}, LX/D4Q;->a(LX/D4Q;LX/D4P;)V

    .line 1962181
    invoke-static {p0}, LX/D4Q;->k(LX/D4Q;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1962182
    iget-object v0, p0, LX/D4Q;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1962183
    iget-object v0, p0, LX/D4Q;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 1962184
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1962185
    instance-of v0, p1, LX/D5z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4Q;->h:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4Q;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962186
    iget-object v0, p0, LX/D4Q;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 1962187
    :goto_0
    return-void

    .line 1962188
    :cond_0
    iget-object v0, p0, LX/D4Q;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1962189
    iget-object v0, p0, LX/D4Q;->c:LX/D4i;

    .line 1962190
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/D4i;->e:Ljava/lang/ref/WeakReference;

    .line 1962191
    iget-object v1, p0, LX/D4Q;->g:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 1962192
    if-nez v1, :cond_3

    .line 1962193
    :goto_1
    iget-boolean v1, v0, LX/D4i;->i:Z

    if-eqz v1, :cond_1

    .line 1962194
    const/4 v2, 0x1

    .line 1962195
    invoke-static {v0}, LX/D4i;->g(LX/D4i;)LX/D4Q;

    move-result-object v1

    .line 1962196
    if-eqz v1, :cond_0

    .line 1962197
    iget-object p1, v1, LX/D4Q;->d:LX/D4P;

    sget-object p0, LX/D4P;->DIMMED:LX/D4P;

    if-eq p1, p0, :cond_5

    .line 1962198
    :cond_0
    :goto_2
    iput-boolean v2, v0, LX/D4i;->j:Z

    .line 1962199
    iget-boolean v1, v0, LX/D4i;->h:Z

    if-eqz v1, :cond_4

    .line 1962200
    :goto_3
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/D4i;->i:Z

    .line 1962201
    :goto_4
    return-void

    .line 1962202
    :cond_1
    iget-object v1, p0, LX/D4Q;->d:LX/D4P;

    sget-object v2, LX/D4P;->UNDIMMED:LX/D4P;

    if-ne v1, v2, :cond_6

    .line 1962203
    :goto_5
    invoke-static {v0}, LX/D4i;->c$redex0(LX/D4i;)V

    goto :goto_4

    :cond_2
    iget-object v1, p0, LX/D4Q;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/player/RichVideoPlayer;

    goto :goto_0

    .line 1962204
    :cond_3
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, LX/D4i;->f:Ljava/lang/ref/WeakReference;

    .line 1962205
    iget-object v2, v0, LX/D4i;->b:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;)V

    .line 1962206
    iget-object v2, v0, LX/D4i;->c:LX/D4h;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    goto :goto_1

    .line 1962207
    :cond_4
    iput-boolean v2, v0, LX/D4i;->h:Z

    .line 1962208
    iget-object v1, v0, LX/D4i;->d:Ljava/util/Set;

    iget-object v2, v0, LX/D4i;->g:Landroid/animation/ValueAnimator;

    const/16 p1, 0x12c

    invoke-static {v1, v2, p1}, LX/D4T;->a(Ljava/util/Collection;Landroid/animation/ValueAnimator;I)V

    goto :goto_3

    .line 1962209
    :cond_5
    sget-object p1, LX/D4P;->IMMERSED:LX/D4P;

    invoke-static {v1, p1}, LX/D4Q;->a(LX/D4Q;LX/D4P;)V

    .line 1962210
    invoke-static {v1}, LX/D4Q;->k(LX/D4Q;)V

    goto :goto_2

    .line 1962211
    :cond_6
    sget-object v1, LX/D4P;->UNDIMMED:LX/D4P;

    invoke-static {p0, v1}, LX/D4Q;->a(LX/D4Q;LX/D4P;)V

    .line 1962212
    invoke-static {p0}, LX/D4Q;->k(LX/D4Q;)V

    goto :goto_5
.end method
