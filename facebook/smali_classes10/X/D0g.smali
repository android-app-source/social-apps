.class public final LX/D0g;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/D0i;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/D0h;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 1955771
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1955772
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "leftTextRes"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "leftClickHandler"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "rightTextRes"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "rightClickHandler"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/D0g;->b:[Ljava/lang/String;

    .line 1955773
    iput v3, p0, LX/D0g;->c:I

    .line 1955774
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/D0g;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/D0g;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/D0g;LX/1De;IILX/D0h;)V
    .locals 1

    .prologue
    .line 1955775
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1955776
    iput-object p4, p0, LX/D0g;->a:LX/D0h;

    .line 1955777
    iget-object v0, p0, LX/D0g;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1955778
    return-void
.end method


# virtual methods
.method public final a(LX/1dQ;)LX/D0g;
    .locals 2

    .prologue
    .line 1955779
    iget-object v0, p0, LX/D0g;->a:LX/D0h;

    iput-object p1, v0, LX/D0h;->b:LX/1dQ;

    .line 1955780
    iget-object v0, p0, LX/D0g;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1955781
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1955782
    invoke-super {p0}, LX/1X5;->a()V

    .line 1955783
    const/4 v0, 0x0

    iput-object v0, p0, LX/D0g;->a:LX/D0h;

    .line 1955784
    sget-object v0, LX/D0i;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1955785
    return-void
.end method

.method public final b(LX/1dQ;)LX/D0g;
    .locals 2

    .prologue
    .line 1955786
    iget-object v0, p0, LX/D0g;->a:LX/D0h;

    iput-object p1, v0, LX/D0h;->d:LX/1dQ;

    .line 1955787
    iget-object v0, p0, LX/D0g;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1955788
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/D0i;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1955789
    iget-object v1, p0, LX/D0g;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/D0g;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/D0g;->c:I

    if-ge v1, v2, :cond_2

    .line 1955790
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1955791
    :goto_0
    iget v2, p0, LX/D0g;->c:I

    if-ge v0, v2, :cond_1

    .line 1955792
    iget-object v2, p0, LX/D0g;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1955793
    iget-object v2, p0, LX/D0g;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1955794
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1955795
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1955796
    :cond_2
    iget-object v0, p0, LX/D0g;->a:LX/D0h;

    .line 1955797
    invoke-virtual {p0}, LX/D0g;->a()V

    .line 1955798
    return-object v0
.end method

.method public final h(I)LX/D0g;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1955799
    iget-object v0, p0, LX/D0g;->a:LX/D0h;

    iput p1, v0, LX/D0h;->a:I

    .line 1955800
    iget-object v0, p0, LX/D0g;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1955801
    return-object p0
.end method

.method public final i(I)LX/D0g;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1955802
    iget-object v0, p0, LX/D0g;->a:LX/D0h;

    iput p1, v0, LX/D0h;->c:I

    .line 1955803
    iget-object v0, p0, LX/D0g;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1955804
    return-object p0
.end method
