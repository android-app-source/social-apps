.class public LX/ENd;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ENd",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113102
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2113103
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ENd;->b:LX/0Zi;

    .line 2113104
    iput-object p1, p0, LX/ENd;->a:LX/0Ot;

    .line 2113105
    return-void
.end method

.method public static a(LX/0QB;)LX/ENd;
    .locals 4

    .prologue
    .line 2113091
    const-class v1, LX/ENd;

    monitor-enter v1

    .line 2113092
    :try_start_0
    sget-object v0, LX/ENd;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113093
    sput-object v2, LX/ENd;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113094
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113095
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113096
    new-instance v3, LX/ENd;

    const/16 p0, 0x3448

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ENd;-><init>(LX/0Ot;)V

    .line 2113097
    move-object v0, v3

    .line 2113098
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113099
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113100
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113101
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2113067
    check-cast p2, LX/ENc;

    .line 2113068
    iget-object v0, p0, LX/ENd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;

    iget-object v1, p2, LX/ENc;->a:LX/CzL;

    .line 2113069
    iget-object v2, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2113070
    if-eqz v2, :cond_0

    .line 2113071
    iget-object v2, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2113072
    check-cast v2, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->b()LX/1Fb;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2113073
    :cond_0
    const/4 v2, 0x0

    .line 2113074
    :goto_0
    move-object v0, v2

    .line 2113075
    return-object v0

    .line 2113076
    :cond_1
    iget-object v2, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2113077
    check-cast v2, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->b()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2113078
    invoke-static {v2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    .line 2113079
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    iget-object v2, v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    sget-object p2, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    const v3, 0x400fae14    # 2.245f

    invoke-virtual {v2, v3}, LX/1up;->c(F)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 2113080
    const v3, -0x3106daee

    const/4 p0, 0x0

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2113081
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2113082
    invoke-static {}, LX/1dS;->b()V

    .line 2113083
    iget v0, p1, LX/1dQ;->b:I

    .line 2113084
    packed-switch v0, :pswitch_data_0

    .line 2113085
    :goto_0
    return-object v2

    .line 2113086
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2113087
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2113088
    check-cast v1, LX/ENc;

    .line 2113089
    iget-object v3, p0, LX/ENd;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;

    iget-object p1, v1, LX/ENc;->a:LX/CzL;

    iget-object p2, v1, LX/ENc;->b:LX/1Ps;

    invoke-virtual {v3, p1, p2}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->a(LX/CzL;LX/1Ps;)V

    .line 2113090
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3106daee
        :pswitch_0
    .end packed-switch
.end method
