.class public final enum LX/Cl4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cl4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cl4;

.field public static final enum IDLE:LX/Cl4;

.field public static final enum PAUSED:LX/Cl4;

.field public static final enum TRACKING:LX/Cl4;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1931906
    new-instance v0, LX/Cl4;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, LX/Cl4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cl4;->IDLE:LX/Cl4;

    .line 1931907
    new-instance v0, LX/Cl4;

    const-string v1, "TRACKING"

    invoke-direct {v0, v1, v3}, LX/Cl4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cl4;->TRACKING:LX/Cl4;

    .line 1931908
    new-instance v0, LX/Cl4;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, LX/Cl4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cl4;->PAUSED:LX/Cl4;

    .line 1931909
    const/4 v0, 0x3

    new-array v0, v0, [LX/Cl4;

    sget-object v1, LX/Cl4;->IDLE:LX/Cl4;

    aput-object v1, v0, v2

    sget-object v1, LX/Cl4;->TRACKING:LX/Cl4;

    aput-object v1, v0, v3

    sget-object v1, LX/Cl4;->PAUSED:LX/Cl4;

    aput-object v1, v0, v4

    sput-object v0, LX/Cl4;->$VALUES:[LX/Cl4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1931905
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cl4;
    .locals 1

    .prologue
    .line 1931904
    const-class v0, LX/Cl4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cl4;

    return-object v0
.end method

.method public static values()[LX/Cl4;
    .locals 1

    .prologue
    .line 1931903
    sget-object v0, LX/Cl4;->$VALUES:[LX/Cl4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cl4;

    return-object v0
.end method
