.class public LX/Cwu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/2Sc;


# direct methods
.method public constructor <init>(LX/2Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951338
    iput-object p1, p0, LX/Cwu;->a:LX/2Sc;

    .line 1951339
    return-void
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1951281
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1951282
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;

    .line 1951283
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_2

    move v1, v4

    :goto_1
    if-eqz v1, :cond_4

    move v1, v4

    :goto_2
    if-nez v1, :cond_0

    .line 1951284
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v8, v1, LX/1vs;->a:LX/15i;

    iget v9, v1, LX/1vs;->b:I

    .line 1951285
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v10, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v10, v1, v4}, LX/15i;->g(II)I

    move-result v11

    .line 1951286
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_6

    .line 1951287
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1951288
    invoke-virtual {v3, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1951289
    :goto_3
    new-instance v12, LX/CwP;

    invoke-direct {v12}, LX/CwP;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->o()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->o()Ljava/lang/String;

    move-result-object v3

    :goto_4
    invoke-virtual {v12, v3}, LX/CwP;->a(Ljava/lang/String;)LX/CwP;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->p()Ljava/lang/String;

    move-result-object v12

    .line 1951290
    iput-object v12, v3, LX/CwP;->b:Ljava/lang/String;

    .line 1951291
    move-object v3, v3

    .line 1951292
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->n()Ljava/lang/String;

    move-result-object v12

    .line 1951293
    iput-object v12, v3, LX/CwP;->c:Ljava/lang/String;

    .line 1951294
    move-object v3, v3

    .line 1951295
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->m()Ljava/lang/String;

    move-result-object v12

    .line 1951296
    iput-object v12, v3, LX/CwP;->d:Ljava/lang/String;

    .line 1951297
    move-object v3, v3

    .line 1951298
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->l()Ljava/lang/String;

    move-result-object v12

    .line 1951299
    iput-object v12, v3, LX/CwP;->e:Ljava/lang/String;

    .line 1951300
    move-object v3, v3

    .line 1951301
    invoke-virtual {v8, v9, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 1951302
    iput-object v8, v3, LX/CwP;->j:Ljava/lang/String;

    .line 1951303
    move-object v3, v3

    .line 1951304
    invoke-virtual {v10, v11, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 1951305
    iput-object v8, v3, LX/CwP;->k:Ljava/lang/String;

    .line 1951306
    move-object v3, v3

    .line 1951307
    iput-object v1, v3, LX/CwP;->l:Ljava/lang/String;

    .line 1951308
    move-object v1, v3

    .line 1951309
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->q()Ljava/lang/String;

    move-result-object v3

    .line 1951310
    iput-object v3, v1, LX/CwP;->m:Ljava/lang/String;

    .line 1951311
    move-object v1, v1

    .line 1951312
    invoke-virtual {v1, p1}, LX/CwP;->n(Ljava/lang/String;)LX/CwP;

    move-result-object v1

    .line 1951313
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1951314
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;

    move-result-object v3

    .line 1951315
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 1951316
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;->l()LX/1vs;

    move-result-object v0

    iget-object v8, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1951317
    invoke-virtual {v8, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1951318
    :goto_5
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;->k()Ljava/lang/String;

    move-result-object v8

    .line 1951319
    iput-object v8, v1, LX/CwP;->f:Ljava/lang/String;

    .line 1951320
    move-object v8, v1

    .line 1951321
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;->m()Ljava/lang/String;

    move-result-object v9

    .line 1951322
    iput-object v9, v8, LX/CwP;->i:Ljava/lang/String;

    .line 1951323
    move-object v8, v8

    .line 1951324
    iput-object v0, v8, LX/CwP;->g:Ljava/lang/String;

    .line 1951325
    move-object v0, v8

    .line 1951326
    invoke-virtual {v3}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 1951327
    iput-object v3, v0, LX/CwP;->h:Ljava/lang/String;

    .line 1951328
    :cond_1
    invoke-virtual {v1}, LX/CwP;->a()Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1951329
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1951330
    invoke-virtual {v3, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v4

    goto/16 :goto_1

    :cond_3
    move v1, v5

    goto/16 :goto_1

    .line 1951331
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1951332
    invoke-virtual {v3, v1, v4}, LX/15i;->g(II)I

    move-result v1

    if-nez v1, :cond_5

    move v1, v4

    goto/16 :goto_2

    :cond_5
    move v1, v5

    goto/16 :goto_2

    :cond_6
    move-object v1, v2

    .line 1951333
    goto/16 :goto_3

    :cond_7
    move-object v3, p1

    .line 1951334
    goto/16 :goto_4

    :cond_8
    move-object v0, v2

    .line 1951335
    goto :goto_5

    .line 1951336
    :cond_9
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;)LX/Cwv;
    .locals 5

    .prologue
    .line 1951253
    new-instance v1, LX/Cwv;

    invoke-direct {v1}, LX/Cwv;-><init>()V

    .line 1951254
    if-nez p1, :cond_1

    .line 1951255
    :try_start_0
    new-instance v0, LX/7C4;

    sget-object v2, LX/3Ql;->FETCH_NULL_STATE_MODULES_FAIL:LX/3Ql;

    const-string v3, "Missing provider"

    invoke-direct {v0, v2, v3}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 1951256
    :catch_0
    move-exception v0

    .line 1951257
    iget-object v2, p0, LX/Cwu;->a:LX/2Sc;

    invoke-virtual {v2, v0}, LX/2Sc;->a(LX/7C4;)V

    :cond_0
    move-object v0, v1

    .line 1951258
    :goto_0
    return-object v0

    .line 1951259
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel;->a()LX/0Px;

    move-result-object v0

    .line 1951260
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1951261
    new-instance v0, LX/Cwv;

    invoke-direct {v0}, LX/Cwv;-><init>()V

    goto :goto_0

    .line 1951262
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel;

    .line 1951263
    new-instance v3, LX/CwN;

    invoke-direct {v3}, LX/CwN;-><init>()V

    .line 1951264
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel;->m()LX/0Px;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel;->l()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, LX/Cwu;->a(Ljava/util/List;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 1951265
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, LX/CwN;->a(Ljava/lang/String;)LX/CwN;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel;->k()Ljava/lang/String;

    move-result-object p1

    .line 1951266
    iput-object p1, v3, LX/CwN;->b:Ljava/lang/String;

    .line 1951267
    move-object v3, v3

    .line 1951268
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel;->n()Ljava/lang/String;

    move-result-object p1

    .line 1951269
    iput-object p1, v3, LX/CwN;->c:Ljava/lang/String;

    .line 1951270
    move-object v3, v3

    .line 1951271
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel;->a()Ljava/lang/String;

    move-result-object p1

    .line 1951272
    iput-object p1, v3, LX/CwN;->d:Ljava/lang/String;

    .line 1951273
    move-object v3, v3

    .line 1951274
    iput-object v4, v3, LX/CwN;->e:LX/0Px;

    .line 1951275
    move-object v3, v3

    .line 1951276
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchSingleStateQueryModels$FB4AGraphSearchSingleStateQueryModel$ProviderModel$ModulesModel;->j()I

    move-result v4

    .line 1951277
    iput v4, v3, LX/CwN;->f:I

    .line 1951278
    move-object v3, v3

    .line 1951279
    invoke-virtual {v3}, LX/CwN;->a()Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    move-result-object v3

    move-object v0, v3

    .line 1951280
    invoke-virtual {v1, v0}, LX/Cwv;->a(Lcom/facebook/search/model/NullStateModuleCollectionUnit;)V
    :try_end_1
    .catch LX/7C4; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
