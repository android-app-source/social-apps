.class public LX/Cv8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Cv8;


# instance fields
.field private final a:LX/16F;


# direct methods
.method public constructor <init>(LX/16F;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1947777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1947778
    iput-object p1, p0, LX/Cv8;->a:LX/16F;

    .line 1947779
    return-void
.end method

.method public static a(LX/0QB;)LX/Cv8;
    .locals 4

    .prologue
    .line 1947764
    sget-object v0, LX/Cv8;->b:LX/Cv8;

    if-nez v0, :cond_1

    .line 1947765
    const-class v1, LX/Cv8;

    monitor-enter v1

    .line 1947766
    :try_start_0
    sget-object v0, LX/Cv8;->b:LX/Cv8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1947767
    if-eqz v2, :cond_0

    .line 1947768
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1947769
    new-instance p0, LX/Cv8;

    invoke-static {v0}, LX/16F;->a(LX/0QB;)LX/16F;

    move-result-object v3

    check-cast v3, LX/16F;

    invoke-direct {p0, v3}, LX/Cv8;-><init>(LX/16F;)V

    .line 1947770
    move-object v0, p0

    .line 1947771
    sput-object v0, LX/Cv8;->b:LX/Cv8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1947772
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1947773
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1947774
    :cond_1
    sget-object v0, LX/Cv8;->b:LX/Cv8;

    return-object v0

    .line 1947775
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1947776
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 1947762
    const-wide/32 v2, 0x240c8400

    move-wide v0, v2

    .line 1947763
    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1947761
    iget-object v0, p0, LX/Cv8;->a:LX/16F;

    invoke-virtual {v0, p1}, LX/16F;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 1947760
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1947755
    iget-object v0, p0, LX/Cv8;->a:LX/16F;

    const v1, 0x7f080d37

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080d36

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, LX/16F;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1947756
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1947759
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1947758
    const-string v0, "4436"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1947757
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->ITEM_SAVED_IN_NOTIFICATIONS_TAB:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
