.class public final LX/CsF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2dD;


# instance fields
.field public final synthetic a:LX/CsG;


# direct methods
.method public constructor <init>(LX/CsG;)V
    .locals 0

    .prologue
    .line 1942339
    iput-object p1, p0, LX/CsF;->a:LX/CsG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0ht;)Z
    .locals 2

    .prologue
    .line 1942340
    iget-object v0, p0, LX/CsF;->a:LX/CsG;

    iget-boolean v0, v0, LX/CsG;->n:Z

    if-eqz v0, :cond_1

    .line 1942341
    iget-object v0, p0, LX/CsF;->a:LX/CsG;

    iget-object v0, v0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    check-cast v0, LX/Csn;

    iget-object v1, p0, LX/CsF;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    invoke-virtual {v0, v1}, LX/Csn;->a(Landroid/text/Spannable;)V

    .line 1942342
    :goto_0
    iget-object v0, p0, LX/CsF;->a:LX/CsG;

    iget-boolean v0, v0, LX/CsG;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/CsF;->a:LX/CsG;

    iget-boolean v0, v0, LX/CsG;->r:Z

    if-nez v0, :cond_0

    .line 1942343
    iget-object v0, p0, LX/CsF;->a:LX/CsG;

    iget-object v0, v0, LX/CsG;->c:LX/Ckw;

    const-string v1, "text_selection_dismissed"

    invoke-virtual {v0, v1}, LX/Ckw;->a(Ljava/lang/String;)V

    .line 1942344
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 1942345
    :cond_1
    iget-object v0, p0, LX/CsF;->a:LX/CsG;

    .line 1942346
    iget-object v1, v0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Landroid/text/SpannableString;

    .line 1942347
    iget-object p1, v0, LX/CsG;->g:Landroid/text/style/BackgroundColorSpan;

    invoke-virtual {v1, p1}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    .line 1942348
    iget-object p1, v0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1942349
    goto :goto_0
.end method
