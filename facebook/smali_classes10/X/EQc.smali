.class public final LX/EQc;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 2118840
    sget-object v0, LX/DbD;->a:LX/0U1;

    sget-object v1, LX/DbD;->b:LX/0U1;

    sget-object v2, LX/DbD;->c:LX/0U1;

    sget-object v3, LX/DbD;->d:LX/0U1;

    sget-object v4, LX/DbD;->e:LX/0U1;

    sget-object v5, LX/DbD;->f:LX/0U1;

    sget-object v6, LX/DbD;->g:LX/0U1;

    sget-object v7, LX/DbD;->h:LX/0U1;

    sget-object v8, LX/DbD;->i:LX/0U1;

    invoke-static/range {v0 .. v8}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/EQc;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2118841
    const-string v0, "images"

    sget-object v1, LX/EQc;->a:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2118842
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 2118843
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2118844
    const-string v0, "images"

    const-string v1, "vault_upload_state_idx"

    sget-object v2, LX/DbD;->f:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x48d2768d

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x4a5e5196    # 3642469.5f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2118845
    const-string v0, "images"

    const-string v1, "vault_image_hash_idx"

    sget-object v2, LX/DbD;->a:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x6fb0ef45

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x457f06d4

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2118846
    return-void
.end method
