.class public LX/D0r;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0s;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/D0r",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/D0s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1956089
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1956090
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/D0r;->b:LX/0Zi;

    .line 1956091
    iput-object p1, p0, LX/D0r;->a:LX/0Ot;

    .line 1956092
    return-void
.end method

.method public static a(LX/0QB;)LX/D0r;
    .locals 4

    .prologue
    .line 1956078
    const-class v1, LX/D0r;

    monitor-enter v1

    .line 1956079
    :try_start_0
    sget-object v0, LX/D0r;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1956080
    sput-object v2, LX/D0r;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1956081
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1956082
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1956083
    new-instance v3, LX/D0r;

    const/16 p0, 0x356f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/D0r;-><init>(LX/0Ot;)V

    .line 1956084
    move-object v0, v3

    .line 1956085
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1956086
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D0r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1956087
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1956088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1956035
    iget-object v0, p0, LX/D0r;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0s;

    const/4 v3, 0x0

    const/4 p2, 0x6

    .line 1956036
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a009a

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, LX/D0s;->d:LX/D0m;

    invoke-virtual {v3, p1}, LX/D0m;->c(LX/1De;)LX/D0k;

    move-result-object v3

    const p0, 0x7f021823

    invoke-virtual {v3, p0}, LX/D0k;->j(I)LX/D0k;

    move-result-object v3

    const-string p0, "link_learn_more"

    invoke-virtual {v3, p0}, LX/D0k;->b(Ljava/lang/String;)LX/D0k;

    move-result-object v3

    const-string p0, "/help/www/217854714899185"

    invoke-virtual {v3, p0}, LX/D0k;->c(Ljava/lang/String;)LX/D0k;

    move-result-object v3

    const p0, 0x7f082a45

    invoke-virtual {v3, p0}, LX/D0k;->h(I)LX/D0k;

    move-result-object v3

    const p0, 0x7f082a46

    invoke-virtual {v3, p0}, LX/D0k;->i(I)LX/D0k;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v3

    const p0, 0x7f0b00d6

    invoke-virtual {v3, p0}, LX/3Ad;->j(I)LX/3Ad;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, LX/D0s;->c:LX/D0i;

    invoke-virtual {v3, p1}, LX/D0i;->c(LX/1De;)LX/D0g;

    move-result-object v3

    const p0, 0x7f082a40

    invoke-virtual {v3, p0}, LX/D0g;->h(I)LX/D0g;

    move-result-object v3

    .line 1956037
    const p0, -0x2cd20fee

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1956038
    invoke-virtual {v3, p0}, LX/D0g;->a(LX/1dQ;)LX/D0g;

    move-result-object v3

    const p0, 0x7f082a41

    invoke-virtual {v3, p0}, LX/D0g;->i(I)LX/D0g;

    move-result-object v3

    .line 1956039
    const p0, -0x2cd20bd0

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1956040
    invoke-virtual {v3, p0}, LX/D0g;->b(LX/1dQ;)LX/D0g;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/D0w;->c(LX/1De;)LX/D0u;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1956041
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1956042
    invoke-static {}, LX/1dS;->b()V

    .line 1956043
    iget v0, p1, LX/1dQ;->b:I

    .line 1956044
    sparse-switch v0, :sswitch_data_0

    .line 1956045
    :goto_0
    return-object v1

    .line 1956046
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1956047
    check-cast v0, LX/D0q;

    .line 1956048
    iget-object v2, p0, LX/D0r;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/D0s;

    iget-object v3, v0, LX/D0q;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, v0, LX/D0q;->b:LX/1Pq;

    .line 1956049
    iget-object p1, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1956050
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p2

    .line 1956051
    new-instance p1, LX/D0U;

    invoke-direct {p1}, LX/D0U;-><init>()V

    move-object p1, p1

    .line 1956052
    new-instance p0, LX/4EB;

    invoke-direct {p0}, LX/4EB;-><init>()V

    .line 1956053
    const-string v0, "ent_id"

    invoke-virtual {p0, v0, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956054
    move-object p0, p0

    .line 1956055
    const-string v0, "input"

    invoke-virtual {p1, v0, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1956056
    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    .line 1956057
    iget-object p1, v2, LX/D0s;->e:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1956058
    iget-object p1, v2, LX/D0s;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1Vv;

    const-string p0, "dti_user_clicked_appeal"

    invoke-virtual {p1, p0, p2}, LX/1Vv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956059
    iget-object p1, v2, LX/D0s;->a:LX/1Vu;

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED_AND_ALREADY_APPEALED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    invoke-virtual {p1, v3, p2}, LX/1Vu;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;)V

    .line 1956060
    const/4 p1, 0x1

    new-array p1, p1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x0

    aput-object v3, p1, p2

    invoke-interface {v4, p1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1956061
    goto :goto_0

    .line 1956062
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1956063
    check-cast v0, LX/D0q;

    .line 1956064
    iget-object v2, p0, LX/D0r;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/D0s;

    iget-object v3, v0, LX/D0q;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, v0, LX/D0q;->b:LX/1Pq;

    .line 1956065
    iget-object p1, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1956066
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p2

    .line 1956067
    new-instance p1, LX/D0Y;

    invoke-direct {p1}, LX/D0Y;-><init>()V

    move-object p1, p1

    .line 1956068
    new-instance p0, LX/4EC;

    invoke-direct {p0}, LX/4EC;-><init>()V

    .line 1956069
    const-string v0, "ent_id"

    invoke-virtual {p0, v0, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956070
    move-object p0, p0

    .line 1956071
    const-string v0, "input"

    invoke-virtual {p1, v0, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1956072
    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    .line 1956073
    iget-object p1, v2, LX/D0s;->e:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1956074
    iget-object p1, v2, LX/D0s;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1Vv;

    const-string p0, "dti_user_clicked_secure"

    invoke-virtual {p1, p0, p2}, LX/1Vv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956075
    iget-object p1, v2, LX/D0s;->a:LX/1Vu;

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;->BLOCKED_AND_ALREADY_CONFIRMED:Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;

    invoke-virtual {p1, v3, p2}, LX/1Vu;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLDisplayTimeBlockAppealState;)V

    .line 1956076
    const/4 p1, 0x1

    new-array p1, p1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x0

    aput-object v3, p1, p2

    invoke-interface {v4, p1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1956077
    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2cd20fee -> :sswitch_0
        -0x2cd20bd0 -> :sswitch_1
    .end sparse-switch
.end method
