.class public abstract LX/CsC;
.super Landroid/view/View;
.source ""


# instance fields
.field private final A:Landroid/graphics/Paint;

.field public B:Landroid/graphics/Bitmap;

.field private final C:Landroid/graphics/RectF;

.field public D:LX/1FZ;

.field public final E:Landroid/os/Handler;

.field private F:Z

.field public a:I

.field public b:F

.field public c:F

.field public d:F

.field public e:Landroid/graphics/Path;

.field public f:I

.field public final g:Landroid/graphics/Path;

.field public final h:Landroid/graphics/Path;

.field public final i:Landroid/graphics/PathMeasure;

.field public final j:Landroid/animation/ValueAnimator;

.field public final k:Landroid/animation/ValueAnimator;

.field public final l:Landroid/animation/ValueAnimator;

.field public final m:Landroid/animation/ValueAnimator;

.field public final n:Landroid/animation/ValueAnimator;

.field public final o:Ljava/lang/Runnable;

.field public final p:Ljava/lang/Runnable;

.field public q:F

.field public r:F

.field public s:F

.field public t:F

.field public u:F

.field public v:Z

.field private final w:Landroid/graphics/Paint;

.field public final x:Landroid/graphics/Paint;

.field public final y:Landroid/graphics/Paint;

.field public final z:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1942233
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/CsC;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1942234
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1942278
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/CsC;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942279
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v4, -0x1

    const/high16 v6, -0x1000000

    const/4 v1, 0x2

    .line 1942241
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942242
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/CsC;->g:Landroid/graphics/Path;

    .line 1942243
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/CsC;->h:Landroid/graphics/Path;

    .line 1942244
    new-instance v0, Landroid/graphics/PathMeasure;

    invoke-direct {v0}, Landroid/graphics/PathMeasure;-><init>()V

    iput-object v0, p0, LX/CsC;->i:Landroid/graphics/PathMeasure;

    .line 1942245
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    .line 1942246
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    .line 1942247
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->l:Landroid/animation/ValueAnimator;

    .line 1942248
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->m:Landroid/animation/ValueAnimator;

    .line 1942249
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->n:Landroid/animation/ValueAnimator;

    .line 1942250
    new-instance v0, Lcom/facebook/richdocument/view/widget/InchwormAnimatedView$1;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/view/widget/InchwormAnimatedView$1;-><init>(LX/CsC;)V

    iput-object v0, p0, LX/CsC;->o:Ljava/lang/Runnable;

    .line 1942251
    new-instance v0, Lcom/facebook/richdocument/view/widget/InchwormAnimatedView$2;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/view/widget/InchwormAnimatedView$2;-><init>(LX/CsC;)V

    iput-object v0, p0, LX/CsC;->p:Ljava/lang/Runnable;

    .line 1942252
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/CsC;->A:Landroid/graphics/Paint;

    .line 1942253
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/CsC;->C:Landroid/graphics/RectF;

    .line 1942254
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/CsC;->E:Landroid/os/Handler;

    .line 1942255
    const-class v0, LX/CsC;

    invoke-static {v0, p0}, LX/CsC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1942256
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b129d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 1942257
    const v1, 0x3f99999a    # 1.2f

    div-float v1, v0, v1

    .line 1942258
    sget-object v2, LX/03r;->InchwormAnimatedView:[I

    invoke-virtual {p1, p2, v2, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 1942259
    const/16 v3, 0x0

    float-to-int v0, v0

    invoke-virtual {v2, v3, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/CsC;->c:F

    .line 1942260
    const/16 v0, 0x1

    invoke-virtual {v2, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 1942261
    const/16 v3, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 1942262
    const/16 v4, 0x3

    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 1942263
    const/16 v5, 0x4

    float-to-int v1, v1

    invoke-virtual {v2, v5, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/CsC;->a:I

    .line 1942264
    iget v1, p0, LX/CsC;->c:F

    iput v1, p0, LX/CsC;->d:F

    .line 1942265
    iget v1, p0, LX/CsC;->c:F

    const/16 v5, 0xff

    invoke-static {v0, v1, v5}, LX/CsC;->a(IFI)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->w:Landroid/graphics/Paint;

    .line 1942266
    iget v0, p0, LX/CsC;->c:F

    const/16 v1, 0x99

    invoke-static {v3, v0, v1}, LX/CsC;->a(IFI)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->x:Landroid/graphics/Paint;

    .line 1942267
    iget v0, p0, LX/CsC;->c:F

    iget v1, p0, LX/CsC;->a:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    const/16 v1, 0x33

    invoke-static {v4, v0, v1}, LX/CsC;->a(IFI)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->y:Landroid/graphics/Paint;

    .line 1942268
    iget v0, p0, LX/CsC;->c:F

    iget v1, p0, LX/CsC;->a:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    iget v3, p0, LX/CsC;->d:F

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    const/16 v1, 0xc

    invoke-static {v6, v0, v1}, LX/CsC;->a(IFI)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->z:Landroid/graphics/Paint;

    .line 1942269
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 1942270
    iget v0, p0, LX/CsC;->c:F

    iget v1, p0, LX/CsC;->a:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, LX/CsC;->b:F

    .line 1942271
    invoke-direct {p0}, LX/CsC;->c()V

    .line 1942272
    iput-boolean v7, p0, LX/CsC;->F:Z

    .line 1942273
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1942274
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1942275
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1942276
    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1942277
    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static a(IFI)Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 1942235
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 1942236
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1942237
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1942238
    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1942239
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1942240
    return-object v0
.end method

.method public static a(Landroid/graphics/RectF;F)Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1942220
    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v0

    .line 1942221
    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1942222
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, p0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 1942223
    div-float v3, v0, v1

    .line 1942224
    cmpl-float v3, p1, v3

    if-lez v3, :cond_0

    .line 1942225
    div-float/2addr v0, p1

    .line 1942226
    iget v3, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v0

    div-float/2addr v1, v4

    add-float/2addr v1, v3

    iput v1, v2, Landroid/graphics/RectF;->top:F

    .line 1942227
    iget v1, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, v2, Landroid/graphics/RectF;->bottom:F

    .line 1942228
    :goto_0
    return-object v2

    .line 1942229
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1942230
    mul-float/2addr v1, p1

    .line 1942231
    iget v3, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    div-float/2addr v0, v4

    add-float/2addr v0, v3

    iput v0, v2, Landroid/graphics/RectF;->left:F

    .line 1942232
    iget v0, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iput v0, v2, Landroid/graphics/RectF;->right:F

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CsC;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object p0

    check-cast p0, LX/1FZ;

    iput-object p0, p1, LX/CsC;->D:LX/1FZ;

    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const-wide/16 v6, 0xbb8

    const-wide/16 v4, 0x5dc

    const/4 v2, -0x1

    .line 1942206
    iget-object v0, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1942207
    iget-object v0, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1942208
    iget-object v0, p0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1942209
    iget-object v0, p0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1942210
    iget-object v0, p0, LX/CsC;->l:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1942211
    iget-object v0, p0, LX/CsC;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1942212
    iget-object v0, p0, LX/CsC;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 1942213
    iget-object v0, p0, LX/CsC;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1942214
    iget-object v0, p0, LX/CsC;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1942215
    iget-object v0, p0, LX/CsC;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 1942216
    iget-object v0, p0, LX/CsC;->n:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1942217
    iget-object v0, p0, LX/CsC;->n:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1942218
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/CsC;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1942219
    return-void
.end method

.method public static f(LX/CsC;)V
    .locals 1

    .prologue
    .line 1942198
    iget-object v0, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1942199
    iget-object v0, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1942200
    iget-object v0, p0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1942201
    iget-object v0, p0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1942202
    iget-object v0, p0, LX/CsC;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1942203
    iget-object v0, p0, LX/CsC;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1942204
    iget-object v0, p0, LX/CsC;->n:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1942205
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/graphics/RectF;)Landroid/graphics/Path;
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1942190
    iget-boolean v2, p0, LX/CsC;->F:Z

    if-eqz v2, :cond_0

    .line 1942191
    :goto_0
    return v0

    .line 1942192
    :cond_0
    iput-boolean v1, p0, LX/CsC;->F:Z

    .line 1942193
    iget-object v2, p0, LX/CsC;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 1942194
    iget-object v2, p0, LX/CsC;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 1942195
    iget-object v2, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 1942196
    iput-boolean v0, p0, LX/CsC;->v:Z

    move v0, v1

    .line 1942197
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1942187
    iget-boolean v0, p0, LX/CsC;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CsC;->n:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1942188
    :cond_0
    :goto_0
    return-void

    .line 1942189
    :cond_1
    iget-object v0, p0, LX/CsC;->n:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1942171
    iget-boolean v0, p0, LX/CsC;->F:Z

    if-nez v0, :cond_0

    .line 1942172
    :goto_0
    return-void

    .line 1942173
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CsC;->F:Z

    .line 1942174
    iget-object v0, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 1942175
    iget-object v0, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1942176
    :cond_1
    iget-object v0, p0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 1942177
    iget-object v0, p0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1942178
    :cond_2
    iget-object v0, p0, LX/CsC;->l:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 1942179
    iget-object v0, p0, LX/CsC;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1942180
    :cond_3
    iget-object v0, p0, LX/CsC;->m:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    .line 1942181
    iget-object v0, p0, LX/CsC;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1942182
    :cond_4
    iget-object v0, p0, LX/CsC;->n:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    .line 1942183
    iget-object v0, p0, LX/CsC;->n:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1942184
    :cond_5
    iget-object v0, p0, LX/CsC;->E:Landroid/os/Handler;

    iget-object v1, p0, LX/CsC;->o:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1942185
    iget-object v0, p0, LX/CsC;->E:Landroid/os/Handler;

    iget-object v1, p0, LX/CsC;->p:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1942186
    invoke-virtual {p0}, LX/CsC;->invalidate()V

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5db83652

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1942161
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1942162
    invoke-static {p0}, LX/CsC;->f(LX/CsC;)V

    .line 1942163
    iget-object v1, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    new-instance v2, LX/Csa;

    invoke-direct {v2, p0}, LX/Csa;-><init>(LX/CsC;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1942164
    iget-object v1, p0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    new-instance v2, LX/Csb;

    invoke-direct {v2, p0}, LX/Csb;-><init>(LX/CsC;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1942165
    iget-object v1, p0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    new-instance v2, LX/Csc;

    invoke-direct {v2, p0}, LX/Csc;-><init>(LX/CsC;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1942166
    iget-object v1, p0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    new-instance v2, LX/Csd;

    invoke-direct {v2, p0}, LX/Csd;-><init>(LX/CsC;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1942167
    iget-object v1, p0, LX/CsC;->l:Landroid/animation/ValueAnimator;

    new-instance v2, LX/Cse;

    invoke-direct {v2, p0}, LX/Cse;-><init>(LX/CsC;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1942168
    iget-object v1, p0, LX/CsC;->m:Landroid/animation/ValueAnimator;

    new-instance v2, LX/Csf;

    invoke-direct {v2, p0}, LX/Csf;-><init>(LX/CsC;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1942169
    iget-object v1, p0, LX/CsC;->n:Landroid/animation/ValueAnimator;

    new-instance v2, LX/Csg;

    invoke-direct {v2, p0}, LX/Csg;-><init>(LX/CsC;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1942170
    const/16 v1, 0x2d

    const v2, -0x1d34184c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x264b4caa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1942112
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1942113
    invoke-static {p0}, LX/CsC;->f(LX/CsC;)V

    .line 1942114
    const/16 v1, 0x2d

    const v2, -0x712ddb59

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1942132
    iget-object v0, p0, LX/CsC;->e:Landroid/graphics/Path;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/CsC;->e:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1942133
    iget-object v0, p0, LX/CsC;->B:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CsC;->B:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/CsC;->B:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 1942134
    :cond_1
    iget-object v0, p0, LX/CsC;->D:LX/1FZ;

    invoke-virtual {p0}, LX/CsC;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/CsC;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v3, v4}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, LX/CsC;->B:Landroid/graphics/Bitmap;

    .line 1942135
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, LX/CsC;->B:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1942136
    iget-object v1, p0, LX/CsC;->e:Landroid/graphics/Path;

    iget-object v3, p0, LX/CsC;->z:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1942137
    iget-object v1, p0, LX/CsC;->e:Landroid/graphics/Path;

    iget-object v3, p0, LX/CsC;->y:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1942138
    iget-object v1, p0, LX/CsC;->e:Landroid/graphics/Path;

    iget-object v3, p0, LX/CsC;->x:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1942139
    :cond_2
    iget-object v0, p0, LX/CsC;->B:Landroid/graphics/Bitmap;

    iget-object v1, p0, LX/CsC;->A:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1942140
    iget-boolean v0, p0, LX/CsC;->F:Z

    if-eqz v0, :cond_5

    .line 1942141
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1942142
    iget v3, p0, LX/CsC;->r:F

    iget v4, p0, LX/CsC;->t:F

    add-float/2addr v3, v4

    iget v4, p0, LX/CsC;->u:F

    add-float/2addr v3, v4

    .line 1942143
    iget v4, p0, LX/CsC;->q:F

    iget v5, p0, LX/CsC;->s:F

    add-float/2addr v4, v5

    iget v5, p0, LX/CsC;->u:F

    add-float/2addr v4, v5

    .line 1942144
    float-to-double v5, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-float v5, v5

    sub-float/2addr v3, v5

    .line 1942145
    float-to-double v5, v4

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-float v5, v5

    sub-float/2addr v4, v5

    .line 1942146
    cmpg-float v5, v3, v4

    if-gez v5, :cond_6

    .line 1942147
    iget-object v5, p0, LX/CsC;->i:Landroid/graphics/PathMeasure;

    iget v6, p0, LX/CsC;->f:I

    int-to-float v6, v6

    mul-float/2addr v3, v6

    iget v6, p0, LX/CsC;->f:I

    int-to-float v6, v6

    mul-float/2addr v4, v6

    iget-object v6, p0, LX/CsC;->g:Landroid/graphics/Path;

    invoke-virtual {v5, v3, v4, v6, v9}, Landroid/graphics/PathMeasure;->getSegment(FFLandroid/graphics/Path;Z)Z

    .line 1942148
    iget-object v3, p0, LX/CsC;->g:Landroid/graphics/Path;

    invoke-virtual {v3, v8, v8}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 1942149
    :goto_0
    iget-object v0, p0, LX/CsC;->g:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1942150
    iget-object v0, p0, LX/CsC;->g:Landroid/graphics/Path;

    iget-object v1, p0, LX/CsC;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1942151
    iget-object v0, p0, LX/CsC;->g:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1942152
    :cond_3
    iget-object v0, p0, LX/CsC;->h:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1942153
    iget-object v0, p0, LX/CsC;->h:Landroid/graphics/Path;

    iget-object v1, p0, LX/CsC;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1942154
    iget-object v0, p0, LX/CsC;->h:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1942155
    :cond_4
    :goto_1
    return-void

    .line 1942156
    :cond_5
    iget-object v0, p0, LX/CsC;->e:Landroid/graphics/Path;

    iget-object v1, p0, LX/CsC;->w:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 1942157
    :cond_6
    iget-object v5, p0, LX/CsC;->i:Landroid/graphics/PathMeasure;

    iget v6, p0, LX/CsC;->f:I

    int-to-float v6, v6

    mul-float/2addr v3, v6

    const/high16 v6, 0x3f800000    # 1.0f

    iget v7, p0, LX/CsC;->f:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    iget-object v7, p0, LX/CsC;->h:Landroid/graphics/Path;

    invoke-virtual {v5, v3, v6, v7, v9}, Landroid/graphics/PathMeasure;->getSegment(FFLandroid/graphics/Path;Z)Z

    .line 1942158
    iget-object v3, p0, LX/CsC;->g:Landroid/graphics/Path;

    invoke-virtual {v3, v8, v8}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 1942159
    iget-object v3, p0, LX/CsC;->i:Landroid/graphics/PathMeasure;

    iget v5, p0, LX/CsC;->f:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, LX/CsC;->g:Landroid/graphics/Path;

    invoke-virtual {v3, v8, v4, v5, v9}, Landroid/graphics/PathMeasure;->getSegment(FFLandroid/graphics/Path;Z)Z

    .line 1942160
    iget-object v3, p0, LX/CsC;->h:Landroid/graphics/Path;

    invoke-virtual {v3, v8, v8}, Landroid/graphics/Path;->rLineTo(FF)V

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 1942119
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 1942120
    if-eqz p1, :cond_1

    .line 1942121
    invoke-virtual {p0}, LX/CsC;->getPaddingLeft()I

    move-result v0

    .line 1942122
    invoke-virtual {p0}, LX/CsC;->getPaddingTop()I

    move-result v1

    .line 1942123
    invoke-virtual {p0}, LX/CsC;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/CsC;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1942124
    invoke-virtual {p0}, LX/CsC;->getHeight()I

    move-result v3

    invoke-virtual {p0}, LX/CsC;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1942125
    new-instance v4, Landroid/graphics/RectF;

    int-to-float v0, v0

    int-to-float v1, v1

    int-to-float v2, v2

    int-to-float v3, v3

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1942126
    iget-object v0, p0, LX/CsC;->C:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CsC;->C:Landroid/graphics/RectF;

    invoke-virtual {v0, v4}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1942127
    :cond_0
    iget-object v0, p0, LX/CsC;->C:Landroid/graphics/RectF;

    invoke-virtual {v0, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 1942128
    invoke-virtual {p0, v4}, LX/CsC;->a(Landroid/graphics/RectF;)Landroid/graphics/Path;

    move-result-object v0

    iput-object v0, p0, LX/CsC;->e:Landroid/graphics/Path;

    .line 1942129
    iget-object v0, p0, LX/CsC;->i:Landroid/graphics/PathMeasure;

    iget-object v1, p0, LX/CsC;->e:Landroid/graphics/Path;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PathMeasure;->setPath(Landroid/graphics/Path;Z)V

    .line 1942130
    iget-object v0, p0, LX/CsC;->i:Landroid/graphics/PathMeasure;

    invoke-virtual {v0}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/CsC;->f:I

    .line 1942131
    :cond_1
    return-void
.end method

.method public final onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1942115
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 1942116
    if-ne p1, p0, :cond_1

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const/16 v0, 0x8

    if-ne p2, v0, :cond_1

    .line 1942117
    :cond_0
    invoke-virtual {p0}, LX/CsC;->e()V

    .line 1942118
    :cond_1
    return-void
.end method
