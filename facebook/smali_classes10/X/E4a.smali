.class public final LX/E4a;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E4b;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

.field public c:Ljava/lang/String;

.field public d:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public final synthetic g:LX/E4b;


# direct methods
.method public constructor <init>(LX/E4b;)V
    .locals 1

    .prologue
    .line 2076895
    iput-object p1, p0, LX/E4a;->g:LX/E4b;

    .line 2076896
    move-object v0, p1

    .line 2076897
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2076898
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2076899
    const-string v0, "ReactionActionDelegateComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2076900
    if-ne p0, p1, :cond_1

    .line 2076901
    :cond_0
    :goto_0
    return v0

    .line 2076902
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2076903
    goto :goto_0

    .line 2076904
    :cond_3
    check-cast p1, LX/E4a;

    .line 2076905
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2076906
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2076907
    if-eq v2, v3, :cond_0

    .line 2076908
    iget-object v2, p0, LX/E4a;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E4a;->a:LX/1X1;

    iget-object v3, p1, LX/E4a;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2076909
    goto :goto_0

    .line 2076910
    :cond_5
    iget-object v2, p1, LX/E4a;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 2076911
    :cond_6
    iget-object v2, p0, LX/E4a;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E4a;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p1, LX/E4a;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2076912
    goto :goto_0

    .line 2076913
    :cond_8
    iget-object v2, p1, LX/E4a;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-nez v2, :cond_7

    .line 2076914
    :cond_9
    iget-object v2, p0, LX/E4a;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E4a;->c:Ljava/lang/String;

    iget-object v3, p1, LX/E4a;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2076915
    goto :goto_0

    .line 2076916
    :cond_b
    iget-object v2, p1, LX/E4a;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2076917
    :cond_c
    iget-object v2, p0, LX/E4a;->d:LX/2km;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E4a;->d:LX/2km;

    iget-object v3, p1, LX/E4a;->d:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2076918
    goto :goto_0

    .line 2076919
    :cond_e
    iget-object v2, p1, LX/E4a;->d:LX/2km;

    if-nez v2, :cond_d

    .line 2076920
    :cond_f
    iget-object v2, p0, LX/E4a;->e:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/E4a;->e:Ljava/lang/String;

    iget-object v3, p1, LX/E4a;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 2076921
    goto :goto_0

    .line 2076922
    :cond_11
    iget-object v2, p1, LX/E4a;->e:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 2076923
    :cond_12
    iget-object v2, p0, LX/E4a;->f:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/E4a;->f:Ljava/lang/String;

    iget-object v3, p1, LX/E4a;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2076924
    goto/16 :goto_0

    .line 2076925
    :cond_13
    iget-object v2, p1, LX/E4a;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2076926
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/E4a;

    .line 2076927
    iget-object v1, v0, LX/E4a;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/E4a;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/E4a;->a:LX/1X1;

    .line 2076928
    return-object v0

    .line 2076929
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
