.class public LX/DoT;
.super LX/DoK;
.source ""

# interfaces
.implements LX/DoS;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/DoO;

.field private final c:LX/DoP;

.field private final d:LX/DoR;

.field private e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:LX/DoN;

.field private h:I

.field public i:LX/Ebg;

.field private j:I

.field public k:LX/Ebk;

.field private l:I

.field public m:LX/Eap;

.field public n:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2041778
    const-class v0, LX/DoT;

    sput-object v0, LX/DoT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/util/JsonReader;LX/DoO;LX/DoP;LX/DoR;)V
    .locals 4
    .param p3    # LX/DoP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/DoR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2041793
    invoke-direct {p0}, LX/DoK;-><init>()V

    .line 2041794
    iput-object p2, p0, LX/DoT;->b:LX/DoO;

    .line 2041795
    iput-object p3, p0, LX/DoT;->c:LX/DoP;

    .line 2041796
    iput-object p4, p0, LX/DoT;->d:LX/DoR;

    .line 2041797
    const/4 v1, 0x0

    .line 2041798
    const/4 v0, 0x0

    .line 2041799
    const-string v2, ""

    iput-object v2, p0, LX/DoT;->f:Ljava/lang/String;

    .line 2041800
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 2041801
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2041802
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 2041803
    const-string v3, "state"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2041804
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    invoke-static {v2}, LX/DoN;->from(I)LX/DoN;

    move-result-object v2

    iput-object v2, p0, LX/DoT;->g:LX/DoN;

    goto :goto_0

    .line 2041805
    :cond_0
    const-string v3, "counter"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2041806
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    iput v2, p0, LX/DoT;->h:I

    goto :goto_0

    .line 2041807
    :cond_1
    const-string v3, "id"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2041808
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/DoT;->e:Ljava/lang/String;

    goto :goto_0

    .line 2041809
    :cond_2
    const-string v3, "pre_key_id"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2041810
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    iput v2, p0, LX/DoT;->j:I

    goto :goto_0

    .line 2041811
    :cond_3
    const-string v3, "pre_key_record"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2041812
    new-instance v2, LX/Ebg;

    invoke-static {p1}, LX/DoT;->a(Landroid/util/JsonReader;)[B

    move-result-object v3

    invoke-direct {v2, v3}, LX/Ebg;-><init>([B)V

    iput-object v2, p0, LX/DoT;->i:LX/Ebg;

    goto :goto_0

    .line 2041813
    :cond_4
    const-string v3, "signed_pre_key_id"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2041814
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    iput v2, p0, LX/DoT;->l:I

    goto :goto_0

    .line 2041815
    :cond_5
    const-string v3, "signed_pre_key_record"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2041816
    new-instance v2, LX/Ebk;

    invoke-static {p1}, LX/DoT;->a(Landroid/util/JsonReader;)[B

    move-result-object v3

    invoke-direct {v2, v3}, LX/Ebk;-><init>([B)V

    iput-object v2, p0, LX/DoT;->k:LX/Ebk;

    goto :goto_0

    .line 2041817
    :cond_6
    const-string v3, "session_address_name"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2041818
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2041819
    :cond_7
    const-string v3, "session_address_device"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2041820
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    goto/16 :goto_0

    .line 2041821
    :cond_8
    const-string v3, "session_record"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2041822
    invoke-static {p1}, LX/DoT;->a(Landroid/util/JsonReader;)[B

    move-result-object v2

    iput-object v2, p0, LX/DoT;->n:[B

    goto/16 :goto_0

    .line 2041823
    :cond_9
    const-string v3, "device_id"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2041824
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/DoT;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 2041825
    :cond_a
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 2041826
    :cond_b
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 2041827
    if-nez v1, :cond_c

    if-eqz v0, :cond_e

    .line 2041828
    :cond_c
    new-instance v2, LX/Eap;

    if-nez v1, :cond_d

    const-string v1, ""

    :cond_d
    invoke-direct {v2, v1, v0}, LX/Eap;-><init>(Ljava/lang/String;I)V

    iput-object v2, p0, LX/DoT;->m:LX/Eap;

    .line 2041829
    :cond_e
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/DoO;LX/DoP;LX/DoR;)V
    .locals 1
    .param p3    # LX/DoP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/DoR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2041784
    invoke-direct {p0}, LX/DoK;-><init>()V

    .line 2041785
    iput-object p1, p0, LX/DoT;->e:Ljava/lang/String;

    .line 2041786
    iput-object p2, p0, LX/DoT;->b:LX/DoO;

    .line 2041787
    iput-object p3, p0, LX/DoT;->c:LX/DoP;

    .line 2041788
    iput-object p4, p0, LX/DoT;->d:LX/DoR;

    .line 2041789
    sget-object v0, LX/DoN;->NEW:LX/DoN;

    iput-object v0, p0, LX/DoT;->g:LX/DoN;

    .line 2041790
    const/4 v0, 0x0

    iput v0, p0, LX/DoT;->h:I

    .line 2041791
    const-string v0, ""

    iput-object v0, p0, LX/DoT;->f:Ljava/lang/String;

    .line 2041792
    return-void
.end method

.method private static a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2041783
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method private static a([B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2041782
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a([B[B)Z
    .locals 1

    .prologue
    .line 2041781
    invoke-static {p0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/util/JsonReader;)[B
    .locals 2

    .prologue
    .line 2041780
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/Eaf;
    .locals 1

    .prologue
    .line 2041779
    iget-object v0, p0, LX/DoT;->b:LX/DoO;

    invoke-interface {v0}, LX/DoO;->a()LX/Eaf;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/Ebg;
    .locals 3

    .prologue
    .line 2041772
    iget-object v0, p0, LX/DoT;->c:LX/DoP;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2041773
    iget-object v0, p0, LX/DoT;->c:LX/DoP;

    invoke-interface {v0, p1}, LX/DoP;->a(I)LX/Ebg;

    move-result-object v0

    .line 2041774
    :goto_0
    return-object v0

    .line 2041775
    :cond_0
    iget v0, p0, LX/DoT;->j:I

    if-ne p1, v0, :cond_1

    iget-object v0, p0, LX/DoT;->i:LX/Ebg;

    if-nez v0, :cond_2

    .line 2041776
    :cond_1
    new-instance v0, LX/Eah;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid key id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eah;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2041777
    :cond_2
    iget-object v0, p0, LX/DoT;->i:LX/Ebg;

    goto :goto_0
.end method

.method public final a(LX/Eap;)LX/Ebh;
    .locals 5

    .prologue
    .line 2041761
    iget-object v0, p0, LX/DoT;->n:[B

    if-nez v0, :cond_0

    .line 2041762
    iput-object p1, p0, LX/DoT;->m:LX/Eap;

    .line 2041763
    new-instance v0, LX/Ebh;

    invoke-direct {v0}, LX/Ebh;-><init>()V

    .line 2041764
    invoke-virtual {v0}, LX/Ebh;->e()[B

    move-result-object v1

    iput-object v1, p0, LX/DoT;->n:[B

    .line 2041765
    :goto_0
    return-object v0

    .line 2041766
    :cond_0
    iget-object v0, p0, LX/DoT;->m:LX/Eap;

    invoke-virtual {p1, v0}, LX/Eap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2041767
    :try_start_0
    new-instance v0, LX/Ebh;

    iget-object v1, p0, LX/DoT;->n:[B

    invoke-direct {v0, v1}, LX/Ebh;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2041768
    :catch_0
    move-exception v0

    .line 2041769
    sget-object v1, LX/DoT;->a:Ljava/lang/Class;

    const-string v2, "Error deserialising crypto session record"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2041770
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2041771
    :cond_1
    sget-object v0, LX/DoT;->a:Ljava/lang/Class;

    const-string v1, "Could not find session for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, LX/Eap;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(LX/DoN;)V
    .locals 0

    .prologue
    .line 2041759
    iput-object p1, p0, LX/DoT;->g:LX/DoN;

    .line 2041760
    return-void
.end method

.method public final a(LX/Eap;LX/Eae;)V
    .locals 1

    .prologue
    .line 2041757
    iget-object v0, p0, LX/DoT;->b:LX/DoO;

    invoke-interface {v0, p1, p2}, LX/DoO;->a(LX/Eap;LX/Eae;)V

    .line 2041758
    return-void
.end method

.method public final a(LX/Eap;LX/Ebh;)V
    .locals 1

    .prologue
    .line 2041830
    iget-object v0, p0, LX/DoT;->m:LX/Eap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DoT;->m:LX/Eap;

    invoke-virtual {p1, v0}, LX/Eap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2041831
    :cond_0
    iput-object p1, p0, LX/DoT;->m:LX/Eap;

    .line 2041832
    :cond_1
    invoke-virtual {p2}, LX/Ebh;->e()[B

    move-result-object v0

    iput-object v0, p0, LX/DoT;->n:[B

    .line 2041833
    return-void
.end method

.method public final a(Landroid/util/JsonWriter;)V
    .locals 4

    .prologue
    .line 2041694
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 2041695
    const-string v0, "state"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, LX/DoT;->g:LX/DoN;

    invoke-virtual {v1}, LX/DoN;->getValue()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 2041696
    const-string v0, "counter"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget v1, p0, LX/DoT;->h:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 2041697
    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, LX/DoT;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2041698
    const-string v0, "device_id"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, LX/DoT;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2041699
    iget-object v0, p0, LX/DoT;->i:LX/Ebg;

    if-eqz v0, :cond_0

    .line 2041700
    const-string v0, "pre_key_id"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget v1, p0, LX/DoT;->j:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 2041701
    const-string v0, "pre_key_record"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, LX/DoT;->i:LX/Ebg;

    invoke-virtual {v1}, LX/Ebg;->c()[B

    move-result-object v1

    invoke-static {v1}, LX/DoT;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2041702
    :cond_0
    iget-object v0, p0, LX/DoT;->k:LX/Ebk;

    if-eqz v0, :cond_1

    .line 2041703
    const-string v0, "signed_pre_key_id"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget v1, p0, LX/DoT;->l:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 2041704
    const-string v0, "signed_pre_key_record"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, LX/DoT;->k:LX/Ebk;

    invoke-virtual {v1}, LX/Ebk;->d()[B

    move-result-object v1

    invoke-static {v1}, LX/DoT;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2041705
    :cond_1
    iget-object v0, p0, LX/DoT;->m:LX/Eap;

    if-eqz v0, :cond_2

    .line 2041706
    const-string v0, "session_address_name"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, LX/DoT;->m:LX/Eap;

    .line 2041707
    iget-object v2, v1, LX/Eap;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2041708
    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2041709
    const-string v0, "session_address_device"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, LX/DoT;->m:LX/Eap;

    .line 2041710
    iget v2, v1, LX/Eap;->b:I

    move v1, v2

    .line 2041711
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 2041712
    :cond_2
    iget-object v0, p0, LX/DoT;->n:[B

    if-eqz v0, :cond_3

    .line 2041713
    const-string v0, "session_record"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, LX/DoT;->n:[B

    invoke-static {v1}, LX/DoT;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 2041714
    :cond_3
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 2041715
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2041717
    iput-object p1, p0, LX/DoT;->f:Ljava/lang/String;

    .line 2041718
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2041719
    iget-object v0, p0, LX/DoT;->b:LX/DoO;

    invoke-interface {v0}, LX/DoO;->b()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2041720
    iget-object v0, p0, LX/DoT;->c:LX/DoP;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 2041721
    iget-object v0, p0, LX/DoT;->c:LX/DoP;

    invoke-interface {v0, p1}, LX/DoP;->b(I)V

    .line 2041722
    :cond_0
    :goto_0
    return-void

    .line 2041723
    :cond_1
    iget v0, p0, LX/DoT;->j:I

    if-ne v0, p1, :cond_0

    .line 2041724
    const/4 v0, 0x0

    iput-object v0, p0, LX/DoT;->i:LX/Ebg;

    goto :goto_0
.end method

.method public final b(LX/Eap;)Z
    .locals 1

    .prologue
    .line 2041716
    iget-object v0, p0, LX/DoT;->m:LX/Eap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/DoT;->m:LX/Eap;

    invoke-virtual {p1, v0}, LX/Eap;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/Eap;LX/Eae;)Z
    .locals 1

    .prologue
    .line 2041725
    iget-object v0, p0, LX/DoT;->b:LX/DoO;

    invoke-interface {v0, p1, p2}, LX/DoO;->b(LX/Eap;LX/Eae;)Z

    move-result v0

    return v0
.end method

.method public final c(I)LX/Ebk;
    .locals 3

    .prologue
    .line 2041726
    iget-object v0, p0, LX/DoT;->d:LX/DoR;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2041727
    iget-object v0, p0, LX/DoT;->d:LX/DoR;

    invoke-interface {v0, p1}, LX/DoR;->c(I)LX/Ebk;

    move-result-object v0

    .line 2041728
    :goto_0
    return-object v0

    .line 2041729
    :cond_0
    iget v0, p0, LX/DoT;->l:I

    if-ne p1, v0, :cond_1

    iget-object v0, p0, LX/DoT;->k:LX/Ebk;

    if-nez v0, :cond_2

    .line 2041730
    :cond_1
    new-instance v0, LX/Eah;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid key id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eah;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2041731
    :cond_2
    iget-object v0, p0, LX/DoT;->k:LX/Ebk;

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2041732
    iget-object v0, p0, LX/DoT;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2041733
    iget-object v0, p0, LX/DoT;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2041734
    iget v0, p0, LX/DoT;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/DoT;->h:I

    .line 2041735
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2041736
    if-nez p1, :cond_1

    .line 2041737
    :cond_0
    :goto_0
    return v0

    .line 2041738
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 2041739
    goto :goto_0

    .line 2041740
    :cond_2
    instance-of v2, p1, LX/DoT;

    if-eqz v2, :cond_0

    .line 2041741
    check-cast p1, LX/DoT;

    .line 2041742
    iget-object v2, p1, LX/DoT;->g:LX/DoN;

    iget-object v3, p0, LX/DoT;->g:LX/DoN;

    if-ne v2, v3, :cond_0

    iget v2, p1, LX/DoT;->h:I

    iget v3, p0, LX/DoT;->h:I

    if-ne v2, v3, :cond_0

    iget-object v2, p1, LX/DoT;->e:Ljava/lang/String;

    iget-object v3, p0, LX/DoT;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p1, LX/DoT;->j:I

    iget v3, p0, LX/DoT;->j:I

    if-ne v2, v3, :cond_0

    iget v2, p1, LX/DoT;->l:I

    iget v3, p0, LX/DoT;->l:I

    if-ne v2, v3, :cond_0

    iget-object v2, p1, LX/DoT;->m:LX/Eap;

    iget-object v3, p0, LX/DoT;->m:LX/Eap;

    .line 2041743
    if-nez v2, :cond_5

    if-nez v3, :cond_4

    const/4 v4, 0x1

    :goto_1
    move v2, v4

    .line 2041744
    if-eqz v2, :cond_0

    .line 2041745
    iget-object v2, p1, LX/DoT;->i:LX/Ebg;

    if-nez v2, :cond_6

    iget-object v2, p0, LX/DoT;->i:LX/Ebg;

    if-nez v2, :cond_7

    :cond_3
    iget-object v2, p1, LX/DoT;->k:LX/Ebk;

    if-nez v2, :cond_8

    iget-object v2, p0, LX/DoT;->k:LX/Ebk;

    if-nez v2, :cond_7

    :goto_2
    iget-object v2, p1, LX/DoT;->n:[B

    if-nez v2, :cond_9

    iget-object v2, p0, LX/DoT;->n:[B

    if-nez v2, :cond_7

    :goto_3
    const/4 v2, 0x1

    :goto_4
    move v2, v2

    .line 2041746
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    :cond_5
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_1

    :cond_6
    iget-object v2, p0, LX/DoT;->i:LX/Ebg;

    if-eqz v2, :cond_7

    iget-object v2, p1, LX/DoT;->i:LX/Ebg;

    invoke-virtual {v2}, LX/Ebg;->c()[B

    move-result-object v2

    iget-object v3, p0, LX/DoT;->i:LX/Ebg;

    invoke-virtual {v3}, LX/Ebg;->c()[B

    move-result-object v3

    invoke-static {v2, v3}, LX/DoT;->a([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_7
    const/4 v2, 0x0

    goto :goto_4

    :cond_8
    iget-object v2, p0, LX/DoT;->k:LX/Ebk;

    if-eqz v2, :cond_7

    iget-object v2, p1, LX/DoT;->k:LX/Ebk;

    invoke-virtual {v2}, LX/Ebk;->d()[B

    move-result-object v2

    iget-object v3, p0, LX/DoT;->k:LX/Ebk;

    invoke-virtual {v3}, LX/Ebk;->d()[B

    move-result-object v3

    invoke-static {v2, v3}, LX/DoT;->a([B[B)Z

    move-result v2

    if-eqz v2, :cond_7

    goto :goto_2

    :cond_9
    iget-object v2, p0, LX/DoT;->n:[B

    if-eqz v2, :cond_7

    iget-object v2, p1, LX/DoT;->n:[B

    iget-object v3, p0, LX/DoT;->n:[B

    invoke-static {v2, v3}, LX/DoT;->a([B[B)Z

    move-result v2

    if-eqz v2, :cond_7

    goto :goto_3
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2041747
    iget-object v0, p0, LX/DoT;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2041748
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/DoT;->g:LX/DoN;

    invoke-virtual {v1}, LX/DoN;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2041749
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/DoT;->h:I

    add-int/2addr v0, v1

    .line 2041750
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/DoT;->j:I

    add-int/2addr v0, v1

    .line 2041751
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/DoT;->l:I

    add-int/2addr v0, v1

    .line 2041752
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/DoT;->i:LX/Ebg;

    invoke-static {v1}, LX/DoT;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2041753
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/DoT;->k:LX/Ebk;

    invoke-static {v1}, LX/DoT;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2041754
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/DoT;->m:LX/Eap;

    invoke-static {v1}, LX/DoT;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2041755
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/DoT;->n:[B

    invoke-static {v1}, LX/DoT;->a(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2041756
    return v0
.end method
