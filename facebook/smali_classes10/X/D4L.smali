.class public LX/D4L;
.super LX/3Vv;
.source ""


# instance fields
.field private final a:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1962044
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D4L;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1962045
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1962046
    invoke-direct {p0, p1, p2}, LX/3Vv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1962047
    const v0, 0x7f030263

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1962048
    const v0, 0x7f0d08fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/D4L;->c:Landroid/widget/TextView;

    .line 1962049
    const v0, 0x7f0d0900

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/D4L;->d:Landroid/widget/TextView;

    .line 1962050
    const v0, 0x7f0d0901

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/D4L;->e:Landroid/widget/TextView;

    .line 1962051
    const v0, 0x7f0d0902

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    iput-object v0, p0, LX/D4L;->a:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 1962052
    const v0, 0x7f0d08fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/D4L;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1962053
    const v0, 0x7f0d08ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, LX/D4L;->f:Landroid/widget/RatingBar;

    .line 1962054
    return-void
.end method


# virtual methods
.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 1

    .prologue
    .line 1962055
    iget-object v0, p0, LX/D4L;->a:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    return-object v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1962056
    iget-object v0, p0, LX/D4L;->a:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    invoke-virtual {v0, p1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1962057
    return-void
.end method
