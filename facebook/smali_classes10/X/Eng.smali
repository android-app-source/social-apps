.class public final LX/Eng;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "+",
        "Landroid/os/Parcelable;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Enq;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/En3;

.field public final synthetic d:LX/Eni;


# direct methods
.method public constructor <init>(LX/Eni;LX/Enq;Lcom/google/common/util/concurrent/SettableFuture;LX/En3;)V
    .locals 0

    .prologue
    .line 2167372
    iput-object p1, p0, LX/Eng;->d:LX/Eni;

    iput-object p2, p0, LX/Eng;->a:LX/Enq;

    iput-object p3, p0, LX/Eng;->b:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p4, p0, LX/Eng;->c:LX/En3;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2167360
    iget-object v0, p0, LX/Eng;->d:LX/Eni;

    iget-object v0, v0, LX/Eni;->d:LX/03V;

    const-string v1, "people_entity_cards_page"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2167361
    iget-object v0, p0, LX/Eng;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2167362
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2167363
    check-cast p1, LX/0P1;

    .line 2167364
    iget-object v0, p0, LX/Eng;->d:LX/Eni;

    iget-object v0, v0, LX/Eni;->f:Ljava/util/HashSet;

    iget-object v1, p0, LX/Eng;->a:LX/Enq;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2167365
    iget-object v0, p0, LX/Eng;->d:LX/Eni;

    iget-object v0, v0, LX/Eni;->g:Ljava/util/HashMap;

    iget-object v1, p0, LX/Eng;->a:LX/Enq;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167366
    iget-object v0, p0, LX/Eng;->b:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/Eng;->d:LX/Eni;

    iget-object v2, p0, LX/Eng;->c:LX/En3;

    const/4 p0, 0x0

    .line 2167367
    invoke-static {v2, p1}, LX/Enp;->a(LX/En3;LX/0P1;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2167368
    iget-object v3, v1, LX/Eni;->d:LX/03V;

    const-string v4, "entity_cards_page_loader_bad_ids_page"

    const-string v5, "Surface \'%s\' did an entities fetch that returned a bad set of results. Expected IDs [%s] but got entities for [%s]"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v1, LX/Eni;->c:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, ","

    invoke-static {v8}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v8

    invoke-virtual {v2}, LX/En2;->e()LX/0Px;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, ","

    invoke-static {v8}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v8

    invoke-virtual {p1}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2167369
    :cond_0
    new-instance v3, LX/Enp;

    invoke-direct {v3, v2, p1, p0, p0}, LX/Enp;-><init>(LX/En3;LX/0P1;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 2167370
    const v2, -0xd9f5271

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2167371
    return-void
.end method
