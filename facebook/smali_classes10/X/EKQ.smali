.class public LX/EKQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EKO;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106597
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EKQ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106598
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2106599
    iput-object p1, p0, LX/EKQ;->b:LX/0Ot;

    .line 2106600
    return-void
.end method

.method public static a(LX/0QB;)LX/EKQ;
    .locals 4

    .prologue
    .line 2106601
    const-class v1, LX/EKQ;

    monitor-enter v1

    .line 2106602
    :try_start_0
    sget-object v0, LX/EKQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106603
    sput-object v2, LX/EKQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106604
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106605
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106606
    new-instance v3, LX/EKQ;

    const/16 p0, 0x33c9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EKQ;-><init>(LX/0Ot;)V

    .line 2106607
    move-object v0, v3

    .line 2106608
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106609
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106610
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106611
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2106612
    check-cast p2, LX/EKP;

    .line 2106613
    iget-object v0, p0, LX/EKQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;

    iget-object v1, p2, LX/EKP;->a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    iget-object v2, p2, LX/EKP;->b:Ljava/lang/String;

    iget v3, p2, LX/EKP;->c:I

    const/4 v10, 0x2

    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 2106614
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082297

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->fu_()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    .line 2106615
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v10}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0058

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a00ab

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-interface {v6, v9, v10}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    const v7, 0x7f082296

    invoke-virtual {v6, v7}, LX/1ne;->h(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0052

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a00ab

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b010f

    invoke-interface {v6, v9, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->b:LX/EKr;

    const/4 v7, 0x0

    .line 2106616
    new-instance v10, LX/EKq;

    invoke-direct {v10, v6}, LX/EKq;-><init>(LX/EKr;)V

    .line 2106617
    sget-object p0, LX/EKr;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EKp;

    .line 2106618
    if-nez p0, :cond_0

    .line 2106619
    new-instance p0, LX/EKp;

    invoke-direct {p0}, LX/EKp;-><init>()V

    .line 2106620
    :cond_0
    invoke-static {p0, p1, v7, v7, v10}, LX/EKp;->a$redex0(LX/EKp;LX/1De;IILX/EKq;)V

    .line 2106621
    move-object v10, p0

    .line 2106622
    move-object v7, v10

    .line 2106623
    move-object v6, v7

    .line 2106624
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->a()I

    move-result v7

    .line 2106625
    iget-object v10, v6, LX/EKp;->a:LX/EKq;

    iput v7, v10, LX/EKq;->a:I

    .line 2106626
    iget-object v10, v6, LX/EKp;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 2106627
    move-object v6, v6

    .line 2106628
    iget-object v7, v6, LX/EKp;->a:LX/EKq;

    iput-object v2, v7, LX/EKq;->b:Ljava/lang/String;

    .line 2106629
    iget-object v7, v6, LX/EKp;->d:Ljava/util/BitSet;

    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Ljava/util/BitSet;->set(I)V

    .line 2106630
    move-object v6, v6

    .line 2106631
    iget-object v7, v6, LX/EKp;->a:LX/EKq;

    iput v3, v7, LX/EKq;->c:I

    .line 2106632
    iget-object v7, v6, LX/EKp;->d:Ljava/util/BitSet;

    const/4 v10, 0x2

    invoke-virtual {v7, v10}, Ljava/util/BitSet;->set(I)V

    .line 2106633
    move-object v6, v6

    .line 2106634
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->c()Z

    move-result v7

    .line 2106635
    iget-object v10, v6, LX/EKp;->a:LX/EKq;

    iput-boolean v7, v10, LX/EKq;->d:Z

    .line 2106636
    iget-object v10, v6, LX/EKp;->d:Ljava/util/BitSet;

    const/4 p0, 0x3

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 2106637
    move-object v6, v6

    .line 2106638
    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b010f

    invoke-interface {v6, v9, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->ft_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->e()Ljava/lang/String;

    move-result-object v7

    .line 2106639
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2106640
    const/4 v10, 0x0

    .line 2106641
    :goto_0
    move-object v6, v10

    .line 2106642
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0052

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a00ab

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/16 v7, 0x9

    invoke-interface {v6, v9, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v6, 0x7f0b0050

    invoke-virtual {v4, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v6, 0x7f0a00ab

    invoke-virtual {v4, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2106643
    return-object v0

    .line 2106644
    :cond_1
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const p0, 0x7f0822a0

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v7, p2, v2

    invoke-virtual {v10, p0, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2106645
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p2

    iget-object v10, v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->c:LX/0Or;

    invoke-interface {v10}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/1Ad;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v10, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v10

    sget-object v2, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v10, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v10

    invoke-virtual {v10}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v10

    invoke-virtual {p2, v10}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v10

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object p2

    const v2, 0x7f0a009e

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p2, v2, v3}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object p2

    invoke-virtual {v10, p2}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    .line 2106646
    const p2, 0x72d39a39

    const/4 v2, 0x0

    invoke-static {p1, p2, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 2106647
    invoke-interface {v10, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v10

    invoke-interface {v10, p0}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v10

    const p0, 0x7f0b172d

    invoke-interface {v10, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v10

    const p0, 0x7f0b172d

    invoke-interface {v10, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v10

    const/4 p0, 0x3

    const p2, 0x7f0b010f

    invoke-interface {v10, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v10

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2106648
    invoke-static {}, LX/1dS;->b()V

    .line 2106649
    iget v0, p1, LX/1dQ;->b:I

    .line 2106650
    packed-switch v0, :pswitch_data_0

    .line 2106651
    :goto_0
    return-object v2

    .line 2106652
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2106653
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2106654
    check-cast v1, LX/EKP;

    .line 2106655
    iget-object v3, p0, LX/EKQ;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;

    iget-object v4, v1, LX/EKP;->a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    .line 2106656
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2106657
    :goto_1
    goto :goto_0

    .line 2106658
    :cond_0
    iget-object p1, v3, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    const p1, 0x25d6af

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, p0

    invoke-static {p1, p2}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 2106659
    iget-object p1, v3, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->e:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p1, p0, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x72d39a39
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/EKO;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2106660
    new-instance v1, LX/EKP;

    invoke-direct {v1, p0}, LX/EKP;-><init>(LX/EKQ;)V

    .line 2106661
    sget-object v2, LX/EKQ;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EKO;

    .line 2106662
    if-nez v2, :cond_0

    .line 2106663
    new-instance v2, LX/EKO;

    invoke-direct {v2}, LX/EKO;-><init>()V

    .line 2106664
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EKO;->a$redex0(LX/EKO;LX/1De;IILX/EKP;)V

    .line 2106665
    move-object v1, v2

    .line 2106666
    move-object v0, v1

    .line 2106667
    return-object v0
.end method
