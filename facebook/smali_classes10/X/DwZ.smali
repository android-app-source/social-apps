.class public final LX/DwZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dwa;


# direct methods
.method public constructor <init>(LX/Dwa;)V
    .locals 0

    .prologue
    .line 2060595
    iput-object p1, p0, LX/DwZ;->a:LX/Dwa;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2060596
    iget-object v0, p0, LX/DwZ;->a:LX/Dwa;

    iget-object v0, v0, LX/Dvb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "refetchAlbumDetails"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2060597
    iget-object v0, p0, LX/DwZ;->a:LX/Dwa;

    const v1, 0x1ab7d012

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2060598
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2060599
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2060600
    if-nez p1, :cond_1

    .line 2060601
    :cond_0
    :goto_0
    return-void

    .line 2060602
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2060603
    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;

    .line 2060604
    if-nez v0, :cond_5

    .line 2060605
    const/4 v2, 0x0

    .line 2060606
    :goto_1
    move-object v0, v2

    .line 2060607
    if-eqz v0, :cond_0

    .line 2060608
    invoke-static {v0}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/4Vp;

    move-result-object v0

    .line 2060609
    iget-object v1, p0, LX/DwZ;->a:LX/Dwa;

    iget-object v1, v1, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v1, :cond_3

    .line 2060610
    iget-object v1, p0, LX/DwZ;->a:LX/Dwa;

    iget-object v1, v1, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2060611
    iget-object v1, p0, LX/DwZ;->a:LX/Dwa;

    iget-object v1, v1, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    .line 2060612
    iput-object v1, v0, LX/4Vp;->n:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 2060613
    :cond_2
    iget-object v1, p0, LX/DwZ;->a:LX/Dwa;

    iget-object v1, v1, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2060614
    iget-object v1, p0, LX/DwZ;->a:LX/Dwa;

    iget-object v1, v1, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    .line 2060615
    iput-object v1, v0, LX/4Vp;->u:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 2060616
    :cond_3
    iget-object v1, p0, LX/DwZ;->a:LX/Dwa;

    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 2060617
    iput-object v0, v1, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2060618
    iget-object v0, p0, LX/DwZ;->a:LX/Dwa;

    invoke-static {v0}, LX/Dwa;->s(LX/Dwa;)LX/Dv0;

    move-result-object v0

    .line 2060619
    iget-object v1, p0, LX/DwZ;->a:LX/Dwa;

    if-eqz v0, :cond_4

    .line 2060620
    iget-boolean v2, v0, LX/Dv0;->c:Z

    move v0, v2

    .line 2060621
    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 2060622
    :goto_2
    iput-boolean v0, v1, LX/Dwa;->m:Z

    .line 2060623
    iget-object v0, p0, LX/DwZ;->a:LX/Dwa;

    iget-object v1, p0, LX/DwZ;->a:LX/Dwa;

    invoke-static {v1}, LX/Dwa;->q(LX/Dwa;)Z

    move-result v1

    .line 2060624
    iput-boolean v1, v0, LX/Dwa;->z:Z

    .line 2060625
    iget-object v0, p0, LX/DwZ;->a:LX/Dwa;

    const v1, -0x53b91229

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2060626
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 2060627
    :cond_5
    new-instance v4, LX/4Vp;

    invoke-direct {v4}, LX/4Vp;-><init>()V

    .line 2060628
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->b()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    move-result-object v2

    .line 2060629
    if-nez v2, :cond_8

    .line 2060630
    const/4 v3, 0x0

    .line 2060631
    :goto_3
    move-object v2, v3

    .line 2060632
    iput-object v2, v4, LX/4Vp;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2060633
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->c()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    .line 2060634
    iput-object v2, v4, LX/4Vp;->c:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 2060635
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->d()Z

    move-result v2

    .line 2060636
    iput-boolean v2, v4, LX/4Vp;->d:Z

    .line 2060637
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->e()Z

    move-result v2

    .line 2060638
    iput-boolean v2, v4, LX/4Vp;->f:Z

    .line 2060639
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->bS_()Z

    move-result v2

    .line 2060640
    iput-boolean v2, v4, LX/4Vp;->g:Z

    .line 2060641
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->bT_()Z

    move-result v2

    .line 2060642
    iput-boolean v2, v4, LX/4Vp;->h:Z

    .line 2060643
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->u()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 2060644
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2060645
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->u()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_6

    .line 2060646
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->u()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;

    .line 2060647
    if-nez v2, :cond_a

    .line 2060648
    const/4 v6, 0x0

    .line 2060649
    :goto_5
    move-object v2, v6

    .line 2060650
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2060651
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 2060652
    :cond_6
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2060653
    iput-object v2, v4, LX/4Vp;->i:LX/0Px;

    .line 2060654
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->j()J

    move-result-wide v2

    .line 2060655
    iput-wide v2, v4, LX/4Vp;->j:J

    .line 2060656
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->k()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    move-result-object v2

    .line 2060657
    if-nez v2, :cond_c

    .line 2060658
    const/4 v3, 0x0

    .line 2060659
    :goto_6
    move-object v2, v3

    .line 2060660
    iput-object v2, v4, LX/4Vp;->k:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 2060661
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2060662
    iput-object v2, v4, LX/4Vp;->m:Ljava/lang/String;

    .line 2060663
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->m()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel;

    move-result-object v2

    .line 2060664
    if-nez v2, :cond_d

    .line 2060665
    const/4 v3, 0x0

    .line 2060666
    :goto_7
    move-object v2, v3

    .line 2060667
    iput-object v2, v4, LX/4Vp;->n:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 2060668
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->n()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v2

    .line 2060669
    if-nez v2, :cond_e

    .line 2060670
    const/4 v3, 0x0

    .line 2060671
    :goto_8
    move-object v2, v3

    .line 2060672
    iput-object v2, v4, LX/4Vp;->p:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 2060673
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->o()LX/174;

    move-result-object v2

    invoke-static {v2}, LX/Dy6;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 2060674
    iput-object v2, v4, LX/4Vp;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2060675
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->p()J

    move-result-wide v2

    .line 2060676
    iput-wide v2, v4, LX/4Vp;->r:J

    .line 2060677
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->q()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    move-result-object v2

    .line 2060678
    if-nez v2, :cond_f

    .line 2060679
    const/4 v3, 0x0

    .line 2060680
    :goto_9
    move-object v2, v3

    .line 2060681
    iput-object v2, v4, LX/4Vp;->t:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2060682
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->v()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2060683
    if-nez v2, :cond_10

    .line 2060684
    const/4 v5, 0x0

    .line 2060685
    :goto_a
    move-object v2, v5

    .line 2060686
    iput-object v2, v4, LX/4Vp;->u:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 2060687
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->r()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    move-result-object v2

    .line 2060688
    if-nez v2, :cond_11

    .line 2060689
    const/4 v3, 0x0

    .line 2060690
    :goto_b
    move-object v2, v3

    .line 2060691
    iput-object v2, v4, LX/4Vp;->v:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 2060692
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->s()LX/174;

    move-result-object v2

    invoke-static {v2}, LX/Dy6;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 2060693
    iput-object v2, v4, LX/4Vp;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2060694
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel;->t()Ljava/lang/String;

    move-result-object v2

    .line 2060695
    iput-object v2, v4, LX/4Vp;->y:Ljava/lang/String;

    .line 2060696
    invoke-virtual {v4}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    goto/16 :goto_1

    .line 2060697
    :cond_8
    new-instance v3, LX/4Xy;

    invoke-direct {v3}, LX/4Xy;-><init>()V

    .line 2060698
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->a()LX/1Fb;

    move-result-object v5

    .line 2060699
    if-nez v5, :cond_9

    .line 2060700
    const/4 v6, 0x0

    .line 2060701
    :goto_c
    move-object v5, v6

    .line 2060702
    iput-object v5, v3, LX/4Xy;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2060703
    invoke-virtual {v3}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v3

    goto/16 :goto_3

    .line 2060704
    :cond_9
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 2060705
    invoke-interface {v5}, LX/1Fb;->a()I

    move-result v2

    .line 2060706
    iput v2, v6, LX/2dc;->c:I

    .line 2060707
    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 2060708
    iput-object v2, v6, LX/2dc;->h:Ljava/lang/String;

    .line 2060709
    invoke-interface {v5}, LX/1Fb;->c()I

    move-result v2

    .line 2060710
    iput v2, v6, LX/2dc;->i:I

    .line 2060711
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    goto :goto_c

    .line 2060712
    :cond_a
    new-instance v6, LX/3dL;

    invoke-direct {v6}, LX/3dL;-><init>()V

    .line 2060713
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 2060714
    iput-object v7, v6, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2060715
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 2060716
    iput-object v7, v6, LX/3dL;->E:Ljava/lang/String;

    .line 2060717
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;->d()Ljava/lang/String;

    move-result-object v7

    .line 2060718
    iput-object v7, v6, LX/3dL;->ag:Ljava/lang/String;

    .line 2060719
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;->e()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 2060720
    if-nez v7, :cond_b

    .line 2060721
    const/4 v9, 0x0

    .line 2060722
    :goto_d
    move-object v7, v9

    .line 2060723
    iput-object v7, v6, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2060724
    invoke-virtual {v6}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    goto/16 :goto_5

    .line 2060725
    :cond_b
    new-instance v9, LX/2dc;

    invoke-direct {v9}, LX/2dc;-><init>()V

    .line 2060726
    const/4 v2, 0x0

    invoke-virtual {v8, v7, v2}, LX/15i;->j(II)I

    move-result v2

    .line 2060727
    iput v2, v9, LX/2dc;->c:I

    .line 2060728
    const/4 v2, 0x1

    invoke-virtual {v8, v7, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2060729
    iput-object v2, v9, LX/2dc;->h:Ljava/lang/String;

    .line 2060730
    const/4 v2, 0x2

    invoke-virtual {v8, v7, v2}, LX/15i;->j(II)I

    move-result v2

    .line 2060731
    iput v2, v9, LX/2dc;->i:I

    .line 2060732
    invoke-virtual {v9}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    goto :goto_d

    .line 2060733
    :cond_c
    new-instance v3, LX/4Y6;

    invoke-direct {v3}, LX/4Y6;-><init>()V

    .line 2060734
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 2060735
    iput-object v5, v3, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2060736
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2060737
    iput-object v5, v3, LX/4Y6;->n:Ljava/lang/String;

    .line 2060738
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 2060739
    iput-object v5, v3, LX/4Y6;->r:Ljava/lang/String;

    .line 2060740
    invoke-virtual {v3}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v3

    goto/16 :goto_6

    .line 2060741
    :cond_d
    new-instance v3, LX/4XE;

    invoke-direct {v3}, LX/4XE;-><init>()V

    .line 2060742
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkMetaDataAlbumModel$MediaModel;->a()I

    move-result v5

    .line 2060743
    iput v5, v3, LX/4XE;->b:I

    .line 2060744
    invoke-virtual {v3}, LX/4XE;->a()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v3

    goto/16 :goto_7

    .line 2060745
    :cond_e
    new-instance v3, LX/25F;

    invoke-direct {v3}, LX/25F;-><init>()V

    .line 2060746
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 2060747
    iput-object v5, v3, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2060748
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2060749
    iput-object v5, v3, LX/25F;->C:Ljava/lang/String;

    .line 2060750
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 2060751
    iput-object v5, v3, LX/25F;->T:Ljava/lang/String;

    .line 2060752
    invoke-virtual {v3}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    goto/16 :goto_8

    .line 2060753
    :cond_f
    new-instance v3, LX/3dL;

    invoke-direct {v3}, LX/3dL;-><init>()V

    .line 2060754
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 2060755
    iput-object v5, v3, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2060756
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2060757
    iput-object v5, v3, LX/3dL;->E:Ljava/lang/String;

    .line 2060758
    invoke-virtual {v3}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    goto/16 :goto_9

    .line 2060759
    :cond_10
    new-instance v5, LX/4XE;

    invoke-direct {v5}, LX/4XE;-><init>()V

    .line 2060760
    const/4 v6, 0x0

    invoke-virtual {v3, v2, v6}, LX/15i;->j(II)I

    move-result v6

    .line 2060761
    iput v6, v5, LX/4XE;->b:I

    .line 2060762
    invoke-virtual {v5}, LX/4XE;->a()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v5

    goto/16 :goto_a

    .line 2060763
    :cond_11
    new-instance v3, LX/4YL;

    invoke-direct {v3}, LX/4YL;-><init>()V

    .line 2060764
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2060765
    iput-object v5, v3, LX/4YL;->c:Ljava/lang/String;

    .line 2060766
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->b()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    move-result-object v5

    .line 2060767
    if-nez v5, :cond_12

    .line 2060768
    const/4 v6, 0x0

    .line 2060769
    :goto_e
    move-object v5, v6

    .line 2060770
    iput-object v5, v3, LX/4YL;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2060771
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2060772
    iput-object v5, v3, LX/4YL;->h:Ljava/lang/String;

    .line 2060773
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 2060774
    iput-object v5, v3, LX/4YL;->i:Ljava/lang/String;

    .line 2060775
    invoke-virtual {v2}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->e()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v5

    .line 2060776
    if-nez v5, :cond_13

    .line 2060777
    const/4 v6, 0x0

    .line 2060778
    :goto_f
    move-object v5, v6

    .line 2060779
    iput-object v5, v3, LX/4YL;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 2060780
    invoke-virtual {v3}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    goto/16 :goto_b

    .line 2060781
    :cond_12
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 2060782
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 2060783
    iput-object v7, v6, LX/2dc;->f:Ljava/lang/String;

    .line 2060784
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 2060785
    iput-object v7, v6, LX/2dc;->h:Ljava/lang/String;

    .line 2060786
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    goto :goto_e

    .line 2060787
    :cond_13
    new-instance v6, LX/4YI;

    invoke-direct {v6}, LX/4YI;-><init>()V

    .line 2060788
    invoke-virtual {v5}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v7

    .line 2060789
    iput-object v7, v6, LX/4YI;->b:LX/0Px;

    .line 2060790
    invoke-virtual {v6}, LX/4YI;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v6

    goto :goto_f
.end method
