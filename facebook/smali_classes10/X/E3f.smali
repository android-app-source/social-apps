.class public final LX/E3f;
.super LX/1Cz;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Cz",
        "<",
        "LX/E4T;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/E3g;


# direct methods
.method public constructor <init>(LX/E3g;I)V
    .locals 0

    .prologue
    .line 2074973
    iput-object p1, p0, LX/E3f;->b:LX/E3g;

    iput p2, p0, LX/E3f;->a:I

    invoke-direct {p0}, LX/1Cz;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2074974
    new-instance v0, LX/E4T;

    invoke-direct {v0, p1}, LX/E4T;-><init>(Landroid/content/Context;)V

    .line 2074975
    iget-object v1, v0, LX/E4T;->b:Landroid/widget/LinearLayout;

    move-object v1, v1

    .line 2074976
    if-eqz v1, :cond_0

    .line 2074977
    iget-object v1, v0, LX/E4T;->b:Landroid/widget/LinearLayout;

    move-object v1, v1

    .line 2074978
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2074979
    iget-object v1, v0, LX/E4T;->b:Landroid/widget/LinearLayout;

    move-object v1, v1

    .line 2074980
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/4 v2, -0x1

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2074981
    :cond_0
    const/high16 v1, 0x40800000    # 4.0f

    const/4 v3, 0x0

    .line 2074982
    invoke-virtual {v0}, LX/E4T;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 2074983
    if-nez v2, :cond_1

    .line 2074984
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2074985
    :cond_1
    invoke-virtual {v0}, LX/E4T;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v3

    .line 2074986
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v2, v4, v5, p1, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2074987
    invoke-virtual {v0, v2}, LX/E4T;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2074988
    new-instance v1, LX/E3e;

    invoke-direct {v1, p0}, LX/E3e;-><init>(LX/E3f;)V

    .line 2074989
    iget-object v2, v0, LX/E4T;->c:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2074990
    return-object v0
.end method
