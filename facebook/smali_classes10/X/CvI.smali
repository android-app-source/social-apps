.class public LX/CvI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/CvJ;

.field private final b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final c:Lcom/facebook/analytics/logger/HoneyClientEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I

.field public e:LX/CvV;


# direct methods
.method public constructor <init>(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 0
    .param p3    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1948016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1948017
    iput-object p1, p0, LX/CvI;->a:LX/CvJ;

    .line 1948018
    iput-object p2, p0, LX/CvI;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1948019
    iput-object p3, p0, LX/CvI;->c:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1948020
    return-void
.end method

.method public static a(LX/CvI;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 3

    .prologue
    .line 1948021
    iget-object v0, p0, LX/CvI;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1948022
    iget-object v1, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v0, v1

    .line 1948023
    const-string v1, "typeahead_sid"

    iget-object v2, v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "candidate_result_sid"

    iget-object v0, v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "serp_sid"

    iget-object v2, p0, LX/CvI;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1948024
    iget-object p1, v2, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v2, p1

    .line 1948025
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "filter_type"

    iget-object v2, p0, LX/CvI;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    invoke-static {v2}, LX/7CN;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "session_id"

    iget-object v2, p0, LX/CvI;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v2

    invoke-interface {v2}, LX/8ef;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1948026
    return-void
.end method
