.class public LX/Ems;
.super LX/3sJ;
.source ""


# instance fields
.field private final a:LX/Emq;

.field private final b:LX/Enl;

.field private final c:LX/0So;

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:J

.field public f:Z


# direct methods
.method public constructor <init>(LX/Emq;LX/Enl;LX/0So;)V
    .locals 2
    .param p1    # LX/Emq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Enl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2166313
    invoke-direct {p0}, LX/3sJ;-><init>()V

    .line 2166314
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ems;->d:Ljava/lang/String;

    .line 2166315
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/Ems;->e:J

    .line 2166316
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ems;->f:Z

    .line 2166317
    iput-object p1, p0, LX/Ems;->a:LX/Emq;

    .line 2166318
    iput-object p2, p0, LX/Ems;->b:LX/Enl;

    .line 2166319
    iput-object p3, p0, LX/Ems;->c:LX/0So;

    .line 2166320
    return-void
.end method

.method public static a(LX/Ems;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2166339
    iput-object p1, p0, LX/Ems;->d:Ljava/lang/String;

    .line 2166340
    iget-object v0, p0, LX/Ems;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/Ems;->e:J

    .line 2166341
    return-void
.end method

.method public static c(LX/Ems;)V
    .locals 8

    .prologue
    .line 2166342
    iget-object v0, p0, LX/Ems;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2166343
    :goto_0
    return-void

    .line 2166344
    :cond_0
    iget-object v0, p0, LX/Ems;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/Ems;->e:J

    sub-long/2addr v0, v2

    .line 2166345
    iget-object v2, p0, LX/Ems;->a:LX/Emq;

    iget-object v3, p0, LX/Ems;->d:Ljava/lang/String;

    iget-object v4, p0, LX/Ems;->b:LX/Enl;

    iget-object v5, p0, LX/Ems;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/Enl;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    long-to-double v0, v0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v6

    invoke-virtual {v2, v3, v4, v0, v1}, LX/Emq;->a(Ljava/lang/String;Ljava/lang/Object;D)V

    .line 2166346
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/Ems;->e:J

    goto :goto_0
.end method


# virtual methods
.method public final B_(I)V
    .locals 8

    .prologue
    .line 2166328
    invoke-virtual {p0, p1}, LX/Ems;->c(I)V

    .line 2166329
    iget-object v0, p0, LX/Ems;->b:LX/Enl;

    invoke-virtual {v0}, LX/Enl;->g()LX/0Px;

    move-result-object v0

    .line 2166330
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 2166331
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2166332
    iget-object v1, p0, LX/Ems;->a:LX/Emq;

    const/4 v3, 0x0

    .line 2166333
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2166334
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 2166335
    const-string v2, "profile_id"

    invoke-interface {v7, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166336
    iget-object v2, v1, LX/Emq;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0gh;

    const-string v4, "entity_cards"

    const-string v5, "entity_cards"

    move-object v6, v3

    invoke-virtual/range {v2 .. v7}, LX/0gh;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2166337
    :cond_0
    return-void

    .line 2166338
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2166321
    iget-object v0, p0, LX/Ems;->b:LX/Enl;

    invoke-virtual {v0}, LX/Enl;->g()LX/0Px;

    move-result-object v0

    .line 2166322
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 2166323
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2166324
    iget-object v1, p0, LX/Ems;->d:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2166325
    invoke-static {p0}, LX/Ems;->c(LX/Ems;)V

    .line 2166326
    invoke-static {p0, v0}, LX/Ems;->a(LX/Ems;Ljava/lang/String;)V

    .line 2166327
    :cond_0
    return-void
.end method
