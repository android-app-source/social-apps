.class public LX/DPS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1993347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1993348
    iput-object p1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    .line 1993349
    return-void
.end method

.method public static a(LX/0QB;)LX/DPS;
    .locals 1

    .prologue
    .line 1993350
    invoke-static {p0}, LX/DPS;->b(LX/0QB;)LX/DPS;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0gc;I)Landroid/support/v4/app/DialogFragment;
    .locals 2

    .prologue
    .line 1993351
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 1993352
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1993353
    return-object v0
.end method

.method public static a(Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 1993354
    if-eqz p0, :cond_0

    .line 1993355
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1993356
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/DPS;Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3
    .param p2    # Landroid/content/DialogInterface$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1993357
    new-instance v0, LX/0ju;

    invoke-direct {v0, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1993358
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f083033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993359
    if-eqz p3, :cond_0

    .line 1993360
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f083037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993361
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f083041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993362
    :goto_0
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f083040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993363
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f08302c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993364
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1993365
    return-void

    .line 1993366
    :cond_0
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f083039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/DPS;
    .locals 2

    .prologue
    .line 1993367
    new-instance v1, LX/DPS;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/DPS;-><init>(Landroid/content/res/Resources;)V

    .line 1993368
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/0ju;
    .locals 3

    .prologue
    .line 1993369
    new-instance v0, LX/0ju;

    invoke-direct {v0, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1993370
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x104000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/DPM;

    invoke-direct {v2, p0}, LX/DPM;-><init>(LX/DPS;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993371
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f08302b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993372
    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;Ljava/lang/String;Z)V
    .locals 10

    .prologue
    .line 1993373
    new-instance v9, LX/0ju;

    invoke-direct {v9, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1993374
    if-eqz p7, :cond_0

    const v0, 0x7f083030

    .line 1993375
    :goto_0
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993376
    new-instance v0, LX/DPN;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, LX/DPN;-><init>(LX/DPS;Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;Ljava/lang/String;Z)V

    .line 1993377
    new-instance v2, LX/DPO;

    invoke-direct {v2, p0, p1, p3, v0}, LX/DPO;-><init>(LX/DPS;Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1993378
    if-eqz p7, :cond_1

    const v1, 0x7f08303e

    .line 1993379
    :goto_1
    sget-object v3, LX/DPR;->a:[I

    invoke-virtual {p5}, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1993380
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f083035

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p6, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993381
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, p2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993382
    :goto_2
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/DPQ;

    invoke-direct {v1, p0}, LX/DPQ;-><init>(LX/DPS;)V

    invoke-virtual {v9, v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993383
    invoke-virtual {v9}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1993384
    return-void

    .line 1993385
    :cond_0
    const v0, 0x7f08302f

    goto :goto_0

    .line 1993386
    :cond_1
    const v1, 0x7f08303d

    goto :goto_1

    .line 1993387
    :pswitch_0
    iget-object v2, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v3, 0x7f083035

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993388
    iget-object v2, p0, LX/DPS;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1, p2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993389
    iget-object v1, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f083032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v1, LX/DPP;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, LX/DPP;-><init>(LX/DPS;Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v9, v7, v1}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_2

    .line 1993390
    :pswitch_1
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v3, 0x7f083035

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p6, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993391
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, p2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993392
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v1, 0x7f083032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, v2}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_2

    .line 1993393
    :pswitch_2
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v3, 0x7f083038

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p6, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993394
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, p2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993395
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v1, 0x7f083032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, v2}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto/16 :goto_2

    .line 1993396
    :pswitch_3
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v3, 0x7f08303b

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p6, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993397
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, p2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993398
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v1, 0x7f083032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, v2}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto/16 :goto_2

    .line 1993399
    :pswitch_4
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v1, 0x7f083031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993400
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v1, 0x7f08303c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p6, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993401
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v1, 0x7f08303f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, p2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto/16 :goto_2

    .line 1993402
    :pswitch_5
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    const v2, 0x7f083036

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p6, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993403
    iget-object v0, p0, LX/DPS;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, p2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
