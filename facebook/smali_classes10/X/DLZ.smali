.class public final LX/DLZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DLb;


# direct methods
.method public constructor <init>(LX/DLb;)V
    .locals 0

    .prologue
    .line 1988369
    iput-object p1, p0, LX/DLZ;->a:LX/DLb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1988370
    iget-object v0, p0, LX/DLZ;->a:LX/DLb;

    iget-object v0, v0, LX/DLb;->a:LX/0tX;

    iget-object v1, p0, LX/DLZ;->a:LX/DLb;

    iget-object v1, v1, LX/DLb;->d:Ljava/lang/String;

    iget-object v2, p0, LX/DLZ;->a:LX/DLb;

    iget-object v2, v2, LX/DLb;->g:Ljava/lang/String;

    .line 1988371
    new-instance v3, LX/DLp;

    invoke-direct {v3}, LX/DLp;-><init>()V

    move-object v3, v3

    .line 1988372
    const-string v4, "id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1988373
    const-string v4, "count"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1988374
    if-eqz v2, :cond_0

    .line 1988375
    const-string v4, "offset"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1988376
    :cond_0
    move-object v1, v3

    .line 1988377
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
