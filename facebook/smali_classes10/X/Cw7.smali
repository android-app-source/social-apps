.class public final LX/Cw7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public e:Landroid/net/Uri;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

.field public j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public k:Z

.field public l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field public m:Z

.field public n:D

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:I

.field public w:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1949920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949921
    iput-boolean v2, p0, LX/Cw7;->h:Z

    .line 1949922
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->NOT_VERIFIED:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, LX/Cw7;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1949923
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/Cw7;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1949924
    iput-boolean v2, p0, LX/Cw7;->k:Z

    .line 1949925
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, LX/Cw7;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1949926
    iput-boolean v2, p0, LX/Cw7;->m:Z

    .line 1949927
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/Cw7;->n:D

    .line 1949928
    iput-boolean v2, p0, LX/Cw7;->s:Z

    return-void
.end method


# virtual methods
.method public final a(I)LX/Cw7;
    .locals 1

    .prologue
    .line 1949929
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v0, p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1949930
    iput-object v0, p0, LX/Cw7;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1949931
    move-object v0, p0

    .line 1949932
    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/EntityTypeaheadUnit;)LX/Cw7;
    .locals 6

    .prologue
    .line 1949933
    iget-object v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1949934
    iput-object v0, p0, LX/Cw7;->a:Ljava/lang/String;

    .line 1949935
    move-object v0, p0

    .line 1949936
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1949937
    iput-object v1, v0, LX/Cw7;->b:Ljava/lang/String;

    .line 1949938
    move-object v0, v0

    .line 1949939
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1949940
    iput-object v1, v0, LX/Cw7;->c:Ljava/lang/String;

    .line 1949941
    move-object v0, v0

    .line 1949942
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1949943
    iput-object v1, v0, LX/Cw7;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1949944
    move-object v0, v0

    .line 1949945
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v1, v1

    .line 1949946
    iput-object v1, v0, LX/Cw7;->e:Landroid/net/Uri;

    .line 1949947
    move-object v0, v0

    .line 1949948
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1949949
    iput-object v1, v0, LX/Cw7;->f:Ljava/lang/String;

    .line 1949950
    move-object v0, v0

    .line 1949951
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1949952
    iput-object v1, v0, LX/Cw7;->g:Ljava/lang/String;

    .line 1949953
    move-object v0, v0

    .line 1949954
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->h:Z

    move v1, v1

    .line 1949955
    iput-boolean v1, v0, LX/Cw7;->h:Z

    .line 1949956
    move-object v0, v0

    .line 1949957
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v1, v1

    .line 1949958
    iput-object v1, v0, LX/Cw7;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1949959
    move-object v0, v0

    .line 1949960
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v1, v1

    .line 1949961
    iput-object v1, v0, LX/Cw7;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1949962
    move-object v0, v0

    .line 1949963
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->k:Z

    move v1, v1

    .line 1949964
    iput-boolean v1, v0, LX/Cw7;->k:Z

    .line 1949965
    move-object v0, v0

    .line 1949966
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object v1, v1

    .line 1949967
    iput-object v1, v0, LX/Cw7;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1949968
    move-object v0, v0

    .line 1949969
    const/4 v1, 0x1

    .line 1949970
    iput-boolean v1, v0, LX/Cw7;->m:Z

    .line 1949971
    move-object v0, v0

    .line 1949972
    iget-wide v4, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->n:D

    move-wide v2, v4

    .line 1949973
    iput-wide v2, v0, LX/Cw7;->n:D

    .line 1949974
    move-object v0, v0

    .line 1949975
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->o:LX/0Px;

    move-object v1, v1

    .line 1949976
    iput-object v1, v0, LX/Cw7;->o:LX/0Px;

    .line 1949977
    move-object v0, v0

    .line 1949978
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->q:Z

    move v1, v1

    .line 1949979
    iput-boolean v1, v0, LX/Cw7;->p:Z

    .line 1949980
    move-object v0, v0

    .line 1949981
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->p:LX/0P1;

    move-object v1, v1

    .line 1949982
    iput-object v1, v0, LX/Cw7;->t:LX/0P1;

    .line 1949983
    move-object v0, v0

    .line 1949984
    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v1

    .line 1949985
    iput-boolean v1, v0, LX/Cw7;->r:Z

    .line 1949986
    move-object v0, v0

    .line 1949987
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->t:Z

    move v1, v1

    .line 1949988
    iput-boolean v1, v0, LX/Cw7;->s:Z

    .line 1949989
    move-object v0, v0

    .line 1949990
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->u:Ljava/lang/String;

    move-object v1, v1

    .line 1949991
    iput-object v1, v0, LX/Cw7;->u:Ljava/lang/String;

    .line 1949992
    move-object v0, v0

    .line 1949993
    iget v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->v:I

    move v1, v1

    .line 1949994
    iput v1, v0, LX/Cw7;->v:I

    .line 1949995
    move-object v0, v0

    .line 1949996
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->w:Z

    move v1, v1

    .line 1949997
    iput-boolean v1, v0, LX/Cw7;->w:Z

    .line 1949998
    move-object v0, v0

    .line 1949999
    return-object v0
.end method

.method public final x()Lcom/facebook/search/model/EntityTypeaheadUnit;
    .locals 2

    .prologue
    .line 1950000
    new-instance v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    invoke-direct {v0, p0}, Lcom/facebook/search/model/EntityTypeaheadUnit;-><init>(LX/Cw7;)V

    return-object v0
.end method
