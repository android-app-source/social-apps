.class public final LX/EuU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/FetchFriendRequestJewelCountGraphQLModels$FriendRequestJewelCountModel;",
        ">;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EuV;


# direct methods
.method public constructor <init>(LX/EuV;)V
    .locals 0

    .prologue
    .line 2179633
    iput-object p1, p0, LX/EuU;->a:LX/EuV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2179634
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2179635
    if-eqz p1, :cond_0

    .line 2179636
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179637
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2179638
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2179639
    :goto_1
    return-object v0

    .line 2179640
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179641
    check-cast v0, Lcom/facebook/friends/protocol/FetchFriendRequestJewelCountGraphQLModels$FriendRequestJewelCountModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchFriendRequestJewelCountGraphQLModels$FriendRequestJewelCountModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2179642
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2179643
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179644
    check-cast v0, Lcom/facebook/friends/protocol/FetchFriendRequestJewelCountGraphQLModels$FriendRequestJewelCountModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchFriendRequestJewelCountGraphQLModels$FriendRequestJewelCountModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1
.end method
