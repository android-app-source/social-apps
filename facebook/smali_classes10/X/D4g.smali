.class public final LX/D4g;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/D4i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/D4i;)V
    .locals 1

    .prologue
    .line 1962456
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1962457
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D4g;->a:Ljava/lang/ref/WeakReference;

    .line 1962458
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 1962459
    iget-object v0, p0, LX/D4g;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D4i;

    .line 1962460
    if-nez v0, :cond_0

    .line 1962461
    :goto_0
    return-void

    .line 1962462
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1962463
    :pswitch_0
    invoke-static {v0}, LX/D4i;->g(LX/D4i;)LX/D4Q;

    move-result-object v1

    .line 1962464
    if-eqz v1, :cond_1

    .line 1962465
    iget-object p0, v1, LX/D4Q;->d:LX/D4P;

    sget-object p1, LX/D4P;->UNDIMMED:LX/D4P;

    if-eq p0, p1, :cond_3

    .line 1962466
    :cond_1
    :goto_1
    iget-boolean v1, v0, LX/D4i;->h:Z

    if-eqz v1, :cond_2

    .line 1962467
    :goto_2
    goto :goto_0

    .line 1962468
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/D4i;->h:Z

    .line 1962469
    iget-object v1, v0, LX/D4i;->d:Ljava/util/Set;

    iget-object p0, v0, LX/D4i;->g:Landroid/animation/ValueAnimator;

    const/16 p1, 0x7d0

    invoke-static {v1, p0, p1}, LX/D4T;->a(Ljava/util/Collection;Landroid/animation/ValueAnimator;I)V

    goto :goto_2

    .line 1962470
    :cond_3
    sget-object p0, LX/D4P;->FOCUS_DIMMED:LX/D4P;

    invoke-static {v1, p0}, LX/D4Q;->a(LX/D4Q;LX/D4P;)V

    .line 1962471
    invoke-static {v1}, LX/D4Q;->k(LX/D4Q;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
