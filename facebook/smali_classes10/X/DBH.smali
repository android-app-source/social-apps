.class public LX/DBH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/events/model/Event;

.field public b:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final c:Landroid/content/Context;

.field private final d:LX/0tX;

.field private final e:LX/1Ck;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1972633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1972634
    iput-object p1, p0, LX/DBH;->c:Landroid/content/Context;

    .line 1972635
    iput-object p2, p0, LX/DBH;->d:LX/0tX;

    .line 1972636
    iput-object p3, p0, LX/DBH;->e:LX/1Ck;

    .line 1972637
    return-void
.end method

.method public static a(LX/0QB;)LX/DBH;
    .locals 1

    .prologue
    .line 1972632
    invoke-static {p0}, LX/DBH;->b(LX/0QB;)LX/DBH;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DBH;
    .locals 4

    .prologue
    .line 1972630
    new-instance v3, LX/DBH;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-direct {v3, v0, v1, v2}, LX/DBH;-><init>(Landroid/content/Context;LX/0tX;LX/1Ck;)V

    .line 1972631
    return-object v3
.end method


# virtual methods
.method public final a(JLcom/facebook/events/common/ActionMechanism;)V
    .locals 8

    .prologue
    .line 1972579
    iget-object v0, p0, LX/DBH;->a:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_0

    .line 1972580
    :goto_0
    return-void

    .line 1972581
    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v1, v0

    .line 1972582
    new-instance v0, LX/4EU;

    invoke-direct {v0}, LX/4EU;-><init>()V

    iget-object v2, p0, LX/DBH;->a:Lcom/facebook/events/model/Event;

    .line 1972583
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1972584
    const-string v3, "event_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972585
    move-object v0, v0

    .line 1972586
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1972587
    const-string v3, "schedule_publish_timestamp"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1972588
    move-object v0, v0

    .line 1972589
    new-instance v2, LX/4EG;

    invoke-direct {v2}, LX/4EG;-><init>()V

    iget-object v3, p0, LX/DBH;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v2

    .line 1972590
    iget-object v3, p0, LX/DBH;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 1972591
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->h:Lcom/facebook/events/common/ActionMechanism;

    move-object v3, v4

    .line 1972592
    if-eqz v3, :cond_1

    .line 1972593
    iget-object v3, p0, LX/DBH;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 1972594
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->h:Lcom/facebook/events/common/ActionMechanism;

    move-object v3, v4

    .line 1972595
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1972596
    :cond_1
    new-instance v3, LX/4EG;

    invoke-direct {v3}, LX/4EG;-><init>()V

    iget-object v4, p0, LX/DBH;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v3

    .line 1972597
    invoke-virtual {p3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1972598
    new-instance v4, LX/4EL;

    invoke-direct {v4}, LX/4EL;-><init>()V

    .line 1972599
    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1972600
    move-object v2, v4

    .line 1972601
    const-string v3, "context"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1972602
    move-object v0, v0

    .line 1972603
    new-instance v2, LX/7uK;

    invoke-direct {v2}, LX/7uK;-><init>()V

    move-object v2, v2

    .line 1972604
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7uK;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1972605
    new-instance v2, LX/7ub;

    invoke-direct {v2}, LX/7ub;-><init>()V

    .line 1972606
    iget-object v3, p0, LX/DBH;->a:Lcom/facebook/events/model/Event;

    .line 1972607
    iget-object v4, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1972608
    iput-object v3, v2, LX/7ub;->a:Ljava/lang/String;

    .line 1972609
    move-object v3, v2

    .line 1972610
    int-to-long v4, v1

    .line 1972611
    iput-wide v4, v3, LX/7ub;->b:J

    .line 1972612
    invoke-virtual {v2}, LX/7ub;->a()Lcom/facebook/events/graphql/EventsMutationsModels$EventSchedulePublishMutationModel$EventModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    .line 1972613
    iget-object v1, p0, LX/DBH;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1972614
    new-instance v1, LX/DBG;

    invoke-direct {v1, p0}, LX/DBG;-><init>(LX/DBH;)V

    .line 1972615
    iget-object v2, p0, LX/DBH;->e:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "schedule_event"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/DBH;->a:Lcom/facebook/events/model/Event;

    .line 1972616
    iget-object v5, v4, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1972617
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/events/common/ActionMechanism;)V
    .locals 6

    .prologue
    .line 1972621
    iget-object v0, p0, LX/DBH;->a:Lcom/facebook/events/model/Event;

    .line 1972622
    iget-wide v4, v0, Lcom/facebook/events/model/Event;->A:J

    move-wide v0, v4

    .line 1972623
    iget-object v2, p0, LX/DBH;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->a(JJ)Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    move-result-object v1

    .line 1972624
    new-instance v0, LX/DBF;

    invoke-direct {v0, p0, p1}, LX/DBF;-><init>(LX/DBH;Lcom/facebook/events/common/ActionMechanism;)V

    .line 1972625
    iput-object v0, v1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->r:LX/BjY;

    .line 1972626
    iget-object v0, p0, LX/DBH;->c:Landroid/content/Context;

    const-class v2, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 1972627
    if-eqz v0, :cond_0

    .line 1972628
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "schedule_publish_time"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1972629
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 1972618
    iput-object p1, p0, LX/DBH;->a:Lcom/facebook/events/model/Event;

    .line 1972619
    iput-object p2, p0, LX/DBH;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1972620
    return-void
.end method
