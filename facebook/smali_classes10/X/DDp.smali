.class public final LX/DDp;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/34w;


# direct methods
.method public constructor <init>(LX/34w;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1976021
    iput-object p1, p0, LX/DDp;->d:LX/34w;

    iput-object p2, p0, LX/DDp;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DDp;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p4, p0, LX/DDp;->c:Ljava/lang/String;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 1976022
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1976023
    iget-object v0, p0, LX/DDp;->d:LX/34w;

    const-string v2, "comcom_num_click"

    iget-object v3, p0, LX/DDp;->a:Ljava/lang/String;

    iget-object v4, p0, LX/DDp;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v2, v3, v4}, LX/34w;->a$redex0(LX/34w;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1976024
    new-instance v6, LX/3Af;

    invoke-direct {v6, v1}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 1976025
    new-instance v7, LX/34b;

    invoke-direct {v7, v1}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 1976026
    const v0, 0x7f082518

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/DDp;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1976027
    invoke-virtual {v7, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v8

    iget-object v0, p0, LX/DDp;->d:LX/34w;

    const-string v2, "tel"

    iget-object v3, p0, LX/DDp;->a:Ljava/lang/String;

    iget-object v4, p0, LX/DDp;->b:Lcom/facebook/graphql/model/GraphQLStory;

    const-string v5, "comcom_tel_click"

    invoke-static/range {v0 .. v5}, LX/34w;->a(LX/34w;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v2, p0, LX/DDp;->d:LX/34w;

    iget-object v2, v2, LX/34w;->f:LX/0wM;

    const v3, 0x7f020956

    const v4, 0x7f0a00e7

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1976028
    const v0, 0x7f082519

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1976029
    invoke-virtual {v7, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    iget-object v2, p0, LX/DDp;->d:LX/34w;

    iget-object v3, p0, LX/DDp;->a:Ljava/lang/String;

    iget-object v4, p0, LX/DDp;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1976030
    new-instance v5, LX/DDq;

    invoke-direct {v5, v2, v1, v3, v4}, LX/DDq;-><init>(LX/34w;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v2, v5

    .line 1976031
    invoke-virtual {v0, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v2, p0, LX/DDp;->d:LX/34w;

    iget-object v2, v2, LX/34w;->f:LX/0wM;

    const v3, 0x7f020813

    const v4, 0x7f0a00e7

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1976032
    iget-object v0, p0, LX/DDp;->d:LX/34w;

    .line 1976033
    iget-object v2, v0, LX/34w;->g:LX/0ad;

    sget-short v3, LX/DIp;->b:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 1976034
    if-eqz v0, :cond_0

    .line 1976035
    const v0, 0x7f08251a

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1976036
    invoke-virtual {v7, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    iget-object v2, p0, LX/DDp;->d:LX/34w;

    iget-object v3, p0, LX/DDp;->a:Ljava/lang/String;

    iget-object v4, p0, LX/DDp;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1976037
    new-instance v5, LX/DDs;

    invoke-direct {v5, v2, v3, v4, v1}, LX/DDs;-><init>(LX/34w;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    move-object v2, v5

    .line 1976038
    invoke-virtual {v0, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v2, p0, LX/DDp;->d:LX/34w;

    iget-object v2, v2, LX/34w;->f:LX/0wM;

    const v3, 0x7f020886

    const v4, 0x7f0a00e7

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1976039
    :cond_0
    iget-object v0, p0, LX/DDp;->d:LX/34w;

    .line 1976040
    iget-object v2, v0, LX/34w;->g:LX/0ad;

    sget-short v3, LX/DIp;->c:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 1976041
    if-eqz v0, :cond_1

    .line 1976042
    const v0, 0x7f08251b

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1976043
    invoke-virtual {v7, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v8

    iget-object v0, p0, LX/DDp;->d:LX/34w;

    const-string v2, "sms"

    iget-object v3, p0, LX/DDp;->a:Ljava/lang/String;

    iget-object v4, p0, LX/DDp;->b:Lcom/facebook/graphql/model/GraphQLStory;

    const-string v5, "comcom_send_sms_click"

    invoke-static/range {v0 .. v5}, LX/34w;->a(LX/34w;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, LX/DDp;->d:LX/34w;

    iget-object v1, v1, LX/34w;->f:LX/0wM;

    const v2, 0x7f02092c

    const v3, 0x7f0a00e7

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1976044
    :cond_1
    invoke-virtual {v6, v7}, LX/3Af;->a(LX/1OM;)V

    .line 1976045
    invoke-virtual {v6}, LX/3Af;->show()V

    .line 1976046
    return-void
.end method
