.class public LX/DDg;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public final a:Lcom/facebook/widget/text/BetterTextView;

.field public final b:Lcom/facebook/widget/text/BetterTextView;

.field public final c:Lcom/facebook/widget/text/BetterTextView;

.field public final d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public final e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public final f:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1975893
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DDg;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1975894
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1975895
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1975896
    const v0, 0x7f0307fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1975897
    const v0, 0x7f0d1502

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/DDg;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1975898
    const v0, 0x7f0d1503

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DDg;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1975899
    const v0, 0x7f0d0a20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DDg;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1975900
    const v0, 0x7f0d1505

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DDg;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1975901
    const v0, 0x7f0d1506

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/DDg;->e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1975902
    const v0, 0x7f0d1504

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/DDg;->f:Landroid/widget/ImageView;

    .line 1975903
    iget-object v0, p0, LX/DDg;->f:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1975904
    iget-object v0, p0, LX/DDg;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {p0}, LX/DDg;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020c36

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1975905
    return-void
.end method


# virtual methods
.method public getImageBlockView()Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1975906
    iget-object v0, p0, LX/DDg;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    return-object v0
.end method

.method public setOnImageBlockClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1975907
    iget-object v0, p0, LX/DDg;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1975908
    return-void
.end method
