.class public final LX/CtX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/CsX;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/CtY;


# direct methods
.method public constructor <init>(LX/CsX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1945025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1945026
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    .line 1945027
    iput-object v1, p0, LX/CtX;->a:Ljava/lang/String;

    .line 1945028
    iput-object v1, p0, LX/CtX;->b:Ljava/lang/String;

    .line 1945029
    iput-object v1, p0, LX/CtX;->c:Ljava/lang/String;

    .line 1945030
    iput-object v1, p0, LX/CtX;->d:Ljava/lang/String;

    .line 1945031
    sget-object v0, LX/CtY;->LOW:LX/CtY;

    iput-object v0, p0, LX/CtX;->f:LX/CtY;

    .line 1945032
    return-void
.end method

.method public constructor <init>(LX/CsX;Ljava/lang/String;Ljava/lang/String;LX/CtY;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1945033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1945034
    iput-object p2, p0, LX/CtX;->a:Ljava/lang/String;

    .line 1945035
    iput-object p3, p0, LX/CtX;->b:Ljava/lang/String;

    .line 1945036
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    .line 1945037
    iput-object v1, p0, LX/CtX;->c:Ljava/lang/String;

    .line 1945038
    iput-object v1, p0, LX/CtX;->d:Ljava/lang/String;

    .line 1945039
    iput-object p4, p0, LX/CtX;->f:LX/CtY;

    .line 1945040
    return-void
.end method

.method public constructor <init>(LX/CsX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CtY;)V
    .locals 1

    .prologue
    .line 1945041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1945042
    iput-object p2, p0, LX/CtX;->a:Ljava/lang/String;

    .line 1945043
    iput-object p3, p0, LX/CtX;->c:Ljava/lang/String;

    .line 1945044
    iput-object p4, p0, LX/CtX;->d:Ljava/lang/String;

    .line 1945045
    const/4 v0, 0x0

    iput-object v0, p0, LX/CtX;->b:Ljava/lang/String;

    .line 1945046
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    .line 1945047
    iput-object p5, p0, LX/CtX;->f:LX/CtY;

    .line 1945048
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1945049
    instance-of v0, p1, LX/CtX;

    if-eqz v0, :cond_4

    .line 1945050
    check-cast p1, LX/CtX;

    .line 1945051
    iget-object v0, p0, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p1, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_0
    iget-object v0, p1, LX/CtX;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CtX;->c:Ljava/lang/String;

    iget-object v1, p1, LX/CtX;->c:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p1, LX/CtX;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/CtX;->b:Ljava/lang/String;

    iget-object v1, p1, LX/CtX;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 1945052
    :goto_0
    return v0

    .line 1945053
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1945054
    :cond_4
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
