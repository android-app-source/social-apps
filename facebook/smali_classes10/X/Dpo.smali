.class public LX/Dpo;
.super LX/7H0;
.source ""


# instance fields
.field public final currentVersion:I

.field public final newVersion:I

.field public final packet:LX/DpN;


# direct methods
.method public constructor <init>(IILX/DpN;)V
    .locals 0

    .prologue
    .line 2047294
    invoke-direct {p0}, LX/7H0;-><init>()V

    .line 2047295
    iput p1, p0, LX/Dpo;->currentVersion:I

    .line 2047296
    iput p2, p0, LX/Dpo;->newVersion:I

    .line 2047297
    iput-object p3, p0, LX/Dpo;->packet:LX/DpN;

    .line 2047298
    return-void
.end method


# virtual methods
.method public final getMessage()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2047299
    const-string v0, "Expected packet version %d, got %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, LX/Dpo;->currentVersion:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LX/Dpo;->newVersion:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
