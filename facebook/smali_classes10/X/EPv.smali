.class public final LX/EPv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3d4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3d4",
        "<",
        "Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Sb;

.field public final synthetic b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

.field public final synthetic c:Ljava/lang/Integer;

.field public final synthetic d:LX/EPw;


# direct methods
.method public constructor <init>(LX/EPw;LX/2Sb;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 2118043
    iput-object p1, p0, LX/EPv;->d:LX/EPw;

    iput-object p2, p0, LX/EPv;->a:LX/2Sb;

    iput-object p3, p0, LX/EPv;->b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    iput-object p4, p0, LX/EPv;->c:Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2118044
    check-cast p1, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;

    .line 2118045
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;->a()LX/0Px;

    move-result-object v0

    .line 2118046
    iget-object v1, p0, LX/EPv;->b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    invoke-static {v1}, LX/2Sb;->a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;

    move-result-object v1

    .line 2118047
    new-instance v2, LX/9zi;

    invoke-direct {v2}, LX/9zi;-><init>()V

    .line 2118048
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;->a()LX/0Px;

    move-result-object v3

    iput-object v3, v2, LX/9zi;->a:LX/0Px;

    .line 2118049
    move-object v2, v2

    .line 2118050
    iget-object v3, p0, LX/EPv;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v0, v1, v3}, LX/EQ1;->a(LX/0Px;Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;I)LX/0Px;

    move-result-object v0

    .line 2118051
    iput-object v0, v2, LX/9zi;->a:LX/0Px;

    .line 2118052
    move-object v0, v2

    .line 2118053
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2118054
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2118055
    iget-object v5, v0, LX/9zi;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2118056
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 2118057
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 2118058
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2118059
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2118060
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2118061
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2118062
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2118063
    new-instance v5, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;

    invoke-direct {v5, v4}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel;-><init>(LX/15i;)V

    .line 2118064
    move-object v0, v5

    .line 2118065
    return-object v0
.end method
