.class public final LX/Ec4;
.super LX/EWj;
.source ""

# interfaces
.implements LX/Ec3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/Ec4;",
        ">;",
        "LX/Ec3;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:I

.field public c:LX/Ec8;

.field public d:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/Ec8;",
            "LX/Ec7;",
            "LX/Ec6;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/EcG;

.field public f:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/EcG;",
            "LX/EcF;",
            "LX/EcE;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EcC;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EcC;",
            "LX/EcB;",
            "LX/EcA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2145384
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2145385
    sget-object v0, LX/Ec8;->c:LX/Ec8;

    move-object v0, v0

    .line 2145386
    iput-object v0, p0, LX/Ec4;->c:LX/Ec8;

    .line 2145387
    sget-object v0, LX/EcG;->c:LX/EcG;

    move-object v0, v0

    .line 2145388
    iput-object v0, p0, LX/Ec4;->e:LX/EcG;

    .line 2145389
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    .line 2145390
    invoke-direct {p0}, LX/Ec4;->w()V

    .line 2145391
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2145392
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2145393
    sget-object v0, LX/Ec8;->c:LX/Ec8;

    move-object v0, v0

    .line 2145394
    iput-object v0, p0, LX/Ec4;->c:LX/Ec8;

    .line 2145395
    sget-object v0, LX/EcG;->c:LX/EcG;

    move-object v0, v0

    .line 2145396
    iput-object v0, p0, LX/Ec4;->e:LX/EcG;

    .line 2145397
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    .line 2145398
    invoke-direct {p0}, LX/Ec4;->w()V

    .line 2145399
    return-void
.end method

.method private A()LX/EcH;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2145400
    new-instance v2, LX/EcH;

    invoke-direct {v2, p0}, LX/EcH;-><init>(LX/EWj;)V

    .line 2145401
    iget v3, p0, LX/Ec4;->a:I

    .line 2145402
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    .line 2145403
    :goto_0
    iget v1, p0, LX/Ec4;->b:I

    .line 2145404
    iput v1, v2, LX/EcH;->senderKeyId_:I

    .line 2145405
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_5

    .line 2145406
    or-int/lit8 v0, v0, 0x2

    move v1, v0

    .line 2145407
    :goto_1
    iget-object v0, p0, LX/Ec4;->d:LX/EZ7;

    if-nez v0, :cond_2

    .line 2145408
    iget-object v0, p0, LX/Ec4;->c:LX/Ec8;

    .line 2145409
    iput-object v0, v2, LX/EcH;->senderChainKey_:LX/Ec8;

    .line 2145410
    :goto_2
    and-int/lit8 v0, v3, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    .line 2145411
    or-int/lit8 v1, v1, 0x4

    .line 2145412
    :cond_0
    iget-object v0, p0, LX/Ec4;->f:LX/EZ7;

    if-nez v0, :cond_3

    .line 2145413
    iget-object v0, p0, LX/Ec4;->e:LX/EcG;

    .line 2145414
    iput-object v0, v2, LX/EcH;->senderSigningKey_:LX/EcG;

    .line 2145415
    :goto_3
    iget-object v0, p0, LX/Ec4;->h:LX/EZ2;

    if-nez v0, :cond_4

    .line 2145416
    iget v0, p0, LX/Ec4;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    .line 2145417
    iget-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    .line 2145418
    iget v0, p0, LX/Ec4;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, LX/Ec4;->a:I

    .line 2145419
    :cond_1
    iget-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    .line 2145420
    iput-object v0, v2, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    .line 2145421
    :goto_4
    iput v1, v2, LX/EcH;->bitField0_:I

    .line 2145422
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2145423
    return-object v2

    .line 2145424
    :cond_2
    iget-object v0, p0, LX/Ec4;->d:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/Ec8;

    .line 2145425
    iput-object v0, v2, LX/EcH;->senderChainKey_:LX/Ec8;

    .line 2145426
    goto :goto_2

    .line 2145427
    :cond_3
    iget-object v0, p0, LX/Ec4;->f:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EcG;

    .line 2145428
    iput-object v0, v2, LX/EcH;->senderSigningKey_:LX/EcG;

    .line 2145429
    goto :goto_3

    .line 2145430
    :cond_4
    iget-object v0, p0, LX/Ec4;->h:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v0

    .line 2145431
    iput-object v0, v2, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    .line 2145432
    goto :goto_4

    :cond_5
    move v1, v0

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public static D(LX/Ec4;)V
    .locals 2

    .prologue
    .line 2145433
    iget v0, p0, LX/Ec4;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 2145434
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/Ec4;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    .line 2145435
    iget v0, p0, LX/Ec4;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/Ec4;->a:I

    .line 2145436
    :cond_0
    return-void
.end method

.method private E()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EcC;",
            "LX/EcB;",
            "LX/EcA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2145437
    iget-object v0, p0, LX/Ec4;->h:LX/EZ2;

    if-nez v0, :cond_0

    .line 2145438
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/Ec4;->g:Ljava/util/List;

    iget v0, p0, LX/Ec4;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2145439
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2145440
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/Ec4;->h:LX/EZ2;

    .line 2145441
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    .line 2145442
    :cond_0
    iget-object v0, p0, LX/Ec4;->h:LX/EZ2;

    return-object v0

    .line 2145443
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/Ec4;
    .locals 1

    .prologue
    .line 2145444
    instance-of v0, p1, LX/EcH;

    if-eqz v0, :cond_0

    .line 2145445
    check-cast p1, LX/EcH;

    invoke-virtual {p0, p1}, LX/Ec4;->a(LX/EcH;)LX/Ec4;

    move-result-object p0

    .line 2145446
    :goto_0
    return-object p0

    .line 2145447
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/Ec4;
    .locals 4

    .prologue
    .line 2145448
    const/4 v2, 0x0

    .line 2145449
    :try_start_0
    sget-object v0, LX/EcH;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcH;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2145450
    if-eqz v0, :cond_0

    .line 2145451
    invoke-virtual {p0, v0}, LX/Ec4;->a(LX/EcH;)LX/Ec4;

    .line 2145452
    :cond_0
    return-object p0

    .line 2145453
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2145454
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2145455
    check-cast v0, LX/EcH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2145456
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2145457
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2145458
    invoke-virtual {p0, v1}, LX/Ec4;->a(LX/EcH;)LX/Ec4;

    :cond_1
    throw v0

    .line 2145459
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private w()V
    .locals 4

    .prologue
    .line 2145322
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_2

    .line 2145323
    iget-object v0, p0, LX/Ec4;->d:LX/EZ7;

    if-nez v0, :cond_0

    .line 2145324
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/Ec4;->c:LX/Ec8;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2145325
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2145326
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/Ec4;->d:LX/EZ7;

    .line 2145327
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ec4;->c:LX/Ec8;

    .line 2145328
    :cond_0
    iget-object v0, p0, LX/Ec4;->f:LX/EZ7;

    if-nez v0, :cond_1

    .line 2145329
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/Ec4;->e:LX/EcG;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2145330
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2145331
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/Ec4;->f:LX/EZ7;

    .line 2145332
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ec4;->e:LX/EcG;

    .line 2145333
    :cond_1
    invoke-direct {p0}, LX/Ec4;->E()LX/EZ2;

    .line 2145334
    :cond_2
    return-void
.end method

.method public static x()LX/Ec4;
    .locals 1

    .prologue
    .line 2145460
    new-instance v0, LX/Ec4;

    invoke-direct {v0}, LX/Ec4;-><init>()V

    return-object v0
.end method

.method private y()LX/Ec4;
    .locals 2

    .prologue
    .line 2145461
    invoke-static {}, LX/Ec4;->x()LX/Ec4;

    move-result-object v0

    invoke-direct {p0}, LX/Ec4;->A()LX/EcH;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ec4;->a(LX/EcH;)LX/Ec4;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2145462
    invoke-direct {p0, p1}, LX/Ec4;->d(LX/EWY;)LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2145463
    invoke-direct {p0, p1, p2}, LX/Ec4;->d(LX/EWd;LX/EYZ;)LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/Ec4;
    .locals 1

    .prologue
    .line 2145464
    iget v0, p0, LX/Ec4;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ec4;->a:I

    .line 2145465
    iput p1, p0, LX/Ec4;->b:I

    .line 2145466
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145467
    return-object p0
.end method

.method public final a(LX/Ec8;)LX/Ec4;
    .locals 1

    .prologue
    .line 2145468
    iget-object v0, p0, LX/Ec4;->d:LX/EZ7;

    if-nez v0, :cond_1

    .line 2145469
    if-nez p1, :cond_0

    .line 2145470
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2145471
    :cond_0
    iput-object p1, p0, LX/Ec4;->c:LX/Ec8;

    .line 2145472
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145473
    :goto_0
    iget v0, p0, LX/Ec4;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/Ec4;->a:I

    .line 2145474
    return-object p0

    .line 2145475
    :cond_1
    iget-object v0, p0, LX/Ec4;->d:LX/EZ7;

    invoke-virtual {v0, p1}, LX/EZ7;->a(LX/EWp;)LX/EZ7;

    goto :goto_0
.end method

.method public final a(LX/EcC;)LX/Ec4;
    .locals 1

    .prologue
    .line 2145476
    iget-object v0, p0, LX/Ec4;->h:LX/EZ2;

    if-nez v0, :cond_1

    .line 2145477
    if-nez p1, :cond_0

    .line 2145478
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2145479
    :cond_0
    invoke-static {p0}, LX/Ec4;->D(LX/Ec4;)V

    .line 2145480
    iget-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2145481
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145482
    :goto_0
    return-object p0

    .line 2145483
    :cond_1
    iget-object v0, p0, LX/Ec4;->h:LX/EZ2;

    invoke-virtual {v0, p1}, LX/EZ2;->a(LX/EWp;)LX/EZ2;

    goto :goto_0
.end method

.method public final a(LX/EcH;)LX/Ec4;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2145335
    sget-object v1, LX/EcH;->c:LX/EcH;

    move-object v1, v1

    .line 2145336
    if-ne p1, v1, :cond_0

    .line 2145337
    :goto_0
    return-object p0

    .line 2145338
    :cond_0
    const/4 v1, 0x1

    .line 2145339
    iget v2, p1, LX/EcH;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_9

    :goto_1
    move v1, v1

    .line 2145340
    if-eqz v1, :cond_1

    .line 2145341
    iget v1, p1, LX/EcH;->senderKeyId_:I

    move v1, v1

    .line 2145342
    invoke-virtual {p0, v1}, LX/Ec4;->a(I)LX/Ec4;

    .line 2145343
    :cond_1
    iget v1, p1, LX/EcH;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_a

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2145344
    if-eqz v1, :cond_2

    .line 2145345
    iget-object v1, p1, LX/EcH;->senderChainKey_:LX/Ec8;

    move-object v1, v1

    .line 2145346
    iget-object v2, p0, LX/Ec4;->d:LX/EZ7;

    if-nez v2, :cond_c

    .line 2145347
    iget v2, p0, LX/Ec4;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    iget-object v2, p0, LX/Ec4;->c:LX/Ec8;

    .line 2145348
    sget-object v3, LX/Ec8;->c:LX/Ec8;

    move-object v3, v3

    .line 2145349
    if-eq v2, v3, :cond_b

    .line 2145350
    iget-object v2, p0, LX/Ec4;->c:LX/Ec8;

    invoke-static {v2}, LX/Ec8;->a(LX/Ec8;)LX/Ec7;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/Ec7;->a(LX/Ec8;)LX/Ec7;

    move-result-object v2

    invoke-virtual {v2}, LX/Ec7;->m()LX/Ec8;

    move-result-object v2

    iput-object v2, p0, LX/Ec4;->c:LX/Ec8;

    .line 2145351
    :goto_3
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145352
    :goto_4
    iget v2, p0, LX/Ec4;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, LX/Ec4;->a:I

    .line 2145353
    :cond_2
    iget v1, p1, LX/EcH;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_d

    const/4 v1, 0x1

    :goto_5
    move v1, v1

    .line 2145354
    if-eqz v1, :cond_3

    .line 2145355
    iget-object v1, p1, LX/EcH;->senderSigningKey_:LX/EcG;

    move-object v1, v1

    .line 2145356
    iget-object v2, p0, LX/Ec4;->f:LX/EZ7;

    if-nez v2, :cond_f

    .line 2145357
    iget v2, p0, LX/Ec4;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    iget-object v2, p0, LX/Ec4;->e:LX/EcG;

    .line 2145358
    sget-object v3, LX/EcG;->c:LX/EcG;

    move-object v3, v3

    .line 2145359
    if-eq v2, v3, :cond_e

    .line 2145360
    iget-object v2, p0, LX/Ec4;->e:LX/EcG;

    invoke-static {v2}, LX/EcG;->a(LX/EcG;)LX/EcF;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/EcF;->a(LX/EcG;)LX/EcF;

    move-result-object v2

    invoke-virtual {v2}, LX/EcF;->m()LX/EcG;

    move-result-object v2

    iput-object v2, p0, LX/Ec4;->e:LX/EcG;

    .line 2145361
    :goto_6
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145362
    :goto_7
    iget v2, p0, LX/Ec4;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, LX/Ec4;->a:I

    .line 2145363
    :cond_3
    iget-object v1, p0, LX/Ec4;->h:LX/EZ2;

    if-nez v1, :cond_6

    .line 2145364
    iget-object v0, p1, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2145365
    iget-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2145366
    iget-object v0, p1, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    iput-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    .line 2145367
    iget v0, p0, LX/Ec4;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, LX/Ec4;->a:I

    .line 2145368
    :goto_8
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145369
    :cond_4
    :goto_9
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto/16 :goto_0

    .line 2145370
    :cond_5
    invoke-static {p0}, LX/Ec4;->D(LX/Ec4;)V

    .line 2145371
    iget-object v0, p0, LX/Ec4;->g:Ljava/util/List;

    iget-object v1, p1, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    .line 2145372
    :cond_6
    iget-object v1, p1, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2145373
    iget-object v1, p0, LX/Ec4;->h:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2145374
    iget-object v1, p0, LX/Ec4;->h:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2145375
    iput-object v0, p0, LX/Ec4;->h:LX/EZ2;

    .line 2145376
    iget-object v1, p1, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    iput-object v1, p0, LX/Ec4;->g:Ljava/util/List;

    .line 2145377
    iget v1, p0, LX/Ec4;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, LX/Ec4;->a:I

    .line 2145378
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_7

    invoke-direct {p0}, LX/Ec4;->E()LX/EZ2;

    move-result-object v0

    :cond_7
    iput-object v0, p0, LX/Ec4;->h:LX/EZ2;

    goto :goto_9

    .line 2145379
    :cond_8
    iget-object v0, p0, LX/Ec4;->h:LX/EZ2;

    iget-object v1, p1, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 2145380
    :cond_b
    iput-object v1, p0, LX/Ec4;->c:LX/Ec8;

    goto/16 :goto_3

    .line 2145381
    :cond_c
    iget-object v2, p0, LX/Ec4;->d:LX/EZ7;

    invoke-virtual {v2, v1}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto/16 :goto_4

    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 2145382
    :cond_e
    iput-object v1, p0, LX/Ec4;->e:LX/EcG;

    goto/16 :goto_6

    .line 2145383
    :cond_f
    iget-object v2, p0, LX/Ec4;->f:LX/EZ7;

    invoke-virtual {v2, v1}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto/16 :goto_7
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2145303
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2145302
    invoke-direct {p0, p1, p2}, LX/Ec4;->d(LX/EWd;LX/EYZ;)LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2145321
    invoke-direct {p0}, LX/Ec4;->y()LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2145304
    invoke-direct {p0, p1, p2}, LX/Ec4;->d(LX/EWd;LX/EYZ;)LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2145305
    invoke-direct {p0}, LX/Ec4;->y()LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2145306
    invoke-direct {p0, p1}, LX/Ec4;->d(LX/EWY;)LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2145307
    invoke-direct {p0}, LX/Ec4;->y()LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2145308
    sget-object v0, LX/Eck;->v:LX/EYn;

    const-class v1, LX/EcH;

    const-class v2, LX/Ec4;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2145309
    sget-object v0, LX/Eck;->u:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2145310
    invoke-direct {p0}, LX/Ec4;->y()LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2145311
    invoke-direct {p0}, LX/Ec4;->A()LX/EcH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2145312
    invoke-virtual {p0}, LX/Ec4;->l()LX/EcH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2145313
    invoke-direct {p0}, LX/Ec4;->A()LX/EcH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2145314
    invoke-virtual {p0}, LX/Ec4;->l()LX/EcH;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EcH;
    .locals 2

    .prologue
    .line 2145315
    invoke-direct {p0}, LX/Ec4;->A()LX/EcH;

    move-result-object v0

    .line 2145316
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2145317
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2145318
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145319
    sget-object v0, LX/EcH;->c:LX/EcH;

    move-object v0, v0

    .line 2145320
    return-object v0
.end method
