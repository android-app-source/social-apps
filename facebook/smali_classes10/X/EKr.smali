.class public LX/EKr;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EKp;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EKs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2107352
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EKr;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EKs;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107353
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2107354
    iput-object p1, p0, LX/EKr;->b:LX/0Ot;

    .line 2107355
    return-void
.end method

.method public static a(LX/0QB;)LX/EKr;
    .locals 4

    .prologue
    .line 2107356
    const-class v1, LX/EKr;

    monitor-enter v1

    .line 2107357
    :try_start_0
    sget-object v0, LX/EKr;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107358
    sput-object v2, LX/EKr;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107359
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107360
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107361
    new-instance v3, LX/EKr;

    const/16 p0, 0x33d7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EKr;-><init>(LX/0Ot;)V

    .line 2107362
    move-object v0, v3

    .line 2107363
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107364
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107365
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2107367
    check-cast p2, LX/EKq;

    .line 2107368
    iget-object v0, p0, LX/EKr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EKs;

    iget v2, p2, LX/EKq;->a:I

    iget-object v3, p2, LX/EKq;->b:Ljava/lang/String;

    iget v4, p2, LX/EKq;->c:I

    iget-boolean v5, p2, LX/EKq;->d:Z

    move-object v1, p1

    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 2107369
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v6

    const v7, -0x161412

    invoke-interface {v6, v7}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b172e

    invoke-interface {v6, v7}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v6

    const/16 v7, 0x96

    invoke-interface {v6, v7}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v6

    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v7

    .line 2107370
    sget-object v8, LX/EKU;->a:LX/0P1;

    invoke-virtual {v8, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 2107371
    if-eqz v8, :cond_0

    :goto_0
    move v8, v8

    .line 2107372
    invoke-virtual {v7, v8}, LX/25Q;->h(I)LX/25Q;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v8, 0x7f0b172e

    invoke-interface {v7, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v7

    mul-int/lit16 v8, v2, 0x96

    const/16 p0, 0x10f

    invoke-static {v4, p0}, Ljava/lang/Math;->max(II)I

    move-result p0

    div-int/2addr v8, p0

    invoke-interface {v7, v8}, LX/1Di;->r(I)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p1, p1}, LX/1Di;->k(II)LX/1Di;

    move-result-object v7

    const/4 v8, 0x3

    invoke-interface {v7, v8, p1}, LX/1Di;->k(II)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    .line 2107373
    if-nez v5, :cond_1

    .line 2107374
    const/4 v7, 0x0

    .line 2107375
    :goto_1
    move-object v7, v7

    .line 2107376
    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2107377
    return-object v0

    :cond_0
    const v8, -0x5c318f

    goto :goto_0

    .line 2107378
    :cond_1
    iget-object v7, v0, LX/EKs;->a:LX/1vg;

    invoke-virtual {v7, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v7

    const v8, 0x7f0207db

    invoke-virtual {v7, v8}, LX/2xv;->h(I)LX/2xv;

    move-result-object v7

    const v8, 0x7f0a0097

    invoke-virtual {v7, v8}, LX/2xv;->j(I)LX/2xv;

    move-result-object v7

    invoke-virtual {v7}, LX/1n6;->b()LX/1dc;

    move-result-object v7

    .line 2107379
    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v8, 0x7f0b172f

    invoke-interface {v7, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b172f

    invoke-interface {v7, v8}, LX/1Di;->q(I)LX/1Di;

    move-result-object v7

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2107380
    invoke-static {}, LX/1dS;->b()V

    .line 2107381
    const/4 v0, 0x0

    return-object v0
.end method
