.class public final enum LX/EDv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDv;

.field public static final enum PAUSED:LX/EDv;

.field public static final enum STARTED:LX/EDv;

.field public static final enum STOPPED:LX/EDv;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2091996
    new-instance v0, LX/EDv;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v2}, LX/EDv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDv;->STARTED:LX/EDv;

    .line 2091997
    new-instance v0, LX/EDv;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v3}, LX/EDv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDv;->STOPPED:LX/EDv;

    .line 2091998
    new-instance v0, LX/EDv;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, LX/EDv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDv;->PAUSED:LX/EDv;

    .line 2091999
    const/4 v0, 0x3

    new-array v0, v0, [LX/EDv;

    sget-object v1, LX/EDv;->STARTED:LX/EDv;

    aput-object v1, v0, v2

    sget-object v1, LX/EDv;->STOPPED:LX/EDv;

    aput-object v1, v0, v3

    sget-object v1, LX/EDv;->PAUSED:LX/EDv;

    aput-object v1, v0, v4

    sput-object v0, LX/EDv;->$VALUES:[LX/EDv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2091995
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDv;
    .locals 1

    .prologue
    .line 2091993
    const-class v0, LX/EDv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDv;

    return-object v0
.end method

.method public static values()[LX/EDv;
    .locals 1

    .prologue
    .line 2091994
    sget-object v0, LX/EDv;->$VALUES:[LX/EDv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDv;

    return-object v0
.end method
