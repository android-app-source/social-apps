.class public LX/ESu;
.super LX/ESt;
.source ""


# instance fields
.field private final a:LX/ETT;


# direct methods
.method public constructor <init>(LX/ETT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2123703
    invoke-direct {p0}, LX/ESt;-><init>()V

    .line 2123704
    iput-object p1, p0, LX/ESu;->a:LX/ETT;

    .line 2123705
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 2123706
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2123707
    if-nez v0, :cond_1

    .line 2123708
    :cond_0
    :goto_0
    return-void

    .line 2123709
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2123710
    iget-object v1, p0, LX/ESu;->a:LX/ETT;

    .line 2123711
    iget-object v2, v1, LX/ETT;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123712
    iget-object v2, v1, LX/ETT;->b:LX/14v;

    iget-object p0, v1, LX/ETT;->a:LX/157;

    invoke-virtual {v2, p1, p0}, LX/14v;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/157;)V

    .line 2123713
    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 2123714
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2123715
    if-nez v0, :cond_0

    .line 2123716
    :goto_0
    return-void

    .line 2123717
    :cond_0
    iget-object v1, p0, LX/ESu;->a:LX/ETT;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    .line 2123718
    iget-object p0, v1, LX/ETT;->c:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123719
    iget-object p0, v1, LX/ETT;->b:LX/14v;

    iget-object p1, v1, LX/ETT;->a:LX/157;

    invoke-virtual {p0, v0, p1}, LX/14v;->a(Ljava/lang/String;LX/157;)V

    .line 2123720
    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 2123721
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2123722
    if-nez v0, :cond_0

    .line 2123723
    :goto_0
    return-void

    .line 2123724
    :cond_0
    iget-object v1, p0, LX/ESu;->a:LX/ETT;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    .line 2123725
    iget-object p0, v1, LX/ETT;->c:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2123726
    iget-object p0, v1, LX/ETT;->c:Ljava/util/HashMap;

    invoke-virtual {p0, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123727
    :cond_1
    goto :goto_0
.end method
