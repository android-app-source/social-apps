.class public LX/D0N;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Z

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;LX/0Or;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1955420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1955421
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/D0N;->a:Z

    .line 1955422
    iput-object p2, p0, LX/D0N;->b:LX/0Or;

    .line 1955423
    return-void
.end method

.method public static a(LX/0QB;)LX/D0N;
    .locals 1

    .prologue
    .line 1955419
    invoke-static {p0}, LX/D0N;->b(LX/0QB;)LX/D0N;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1955410
    iget-boolean v0, p0, LX/D0N;->a:Z

    if-eqz v0, :cond_1

    .line 1955411
    iget-object v0, p0, LX/D0N;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1955412
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 1955413
    goto :goto_0

    .line 1955414
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p2, v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/D0N;
    .locals 3

    .prologue
    .line 1955417
    new-instance v1, LX/D0N;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const/16 v2, 0x15e7

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/D0N;-><init>(Ljava/lang/Boolean;LX/0Or;)V

    .line 1955418
    return-object v1
.end method


# virtual methods
.method public final a(LX/8d1;)Z
    .locals 2

    .prologue
    .line 1955416
    invoke-interface {p1}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LX/8d1;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/D0N;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLNode;)Z
    .locals 2

    .prologue
    .line 1955415
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x285feb

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/D0N;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
