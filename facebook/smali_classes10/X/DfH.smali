.class public final LX/DfH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2026386
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 2026387
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2026388
    :goto_0
    return v1

    .line 2026389
    :cond_0
    const-string v11, "is_messenger_user"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2026390
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    .line 2026391
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 2026392
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2026393
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2026394
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2026395
    const-string v11, "best_description"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2026396
    invoke-static {p0, p1}, LX/Df9;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2026397
    :cond_2
    const-string v11, "commerce_page_settings"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2026398
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2026399
    :cond_3
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2026400
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2026401
    :cond_4
    const-string v11, "messenger_content_subscription_description"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2026402
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2026403
    :cond_5
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2026404
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2026405
    :cond_6
    const-string v11, "profile_picture"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 2026406
    invoke-static {p0, p1}, LX/CKy;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2026407
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2026408
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2026409
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2026410
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 2026411
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2026412
    if-eqz v0, :cond_9

    .line 2026413
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 2026414
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2026415
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2026416
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2026417
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2026418
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2026419
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026420
    if-eqz v0, :cond_0

    .line 2026421
    const-string v1, "best_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026422
    invoke-static {p0, v0, p2}, LX/Df9;->a(LX/15i;ILX/0nX;)V

    .line 2026423
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2026424
    if-eqz v0, :cond_1

    .line 2026425
    const-string v0, "commerce_page_settings"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026426
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2026427
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026428
    if-eqz v0, :cond_2

    .line 2026429
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026430
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026431
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2026432
    if-eqz v0, :cond_3

    .line 2026433
    const-string v1, "is_messenger_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026434
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2026435
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026436
    if-eqz v0, :cond_4

    .line 2026437
    const-string v1, "messenger_content_subscription_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026438
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026439
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026440
    if-eqz v0, :cond_5

    .line 2026441
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026442
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026443
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026444
    if-eqz v0, :cond_6

    .line 2026445
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026446
    invoke-static {p0, v0, p2}, LX/CKy;->a(LX/15i;ILX/0nX;)V

    .line 2026447
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2026448
    return-void
.end method
