.class public final LX/Dk7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Diz;

.field public final synthetic b:LX/DkN;


# direct methods
.method public constructor <init>(LX/DkN;LX/Diz;)V
    .locals 0

    .prologue
    .line 2033949
    iput-object p1, p0, LX/Dk7;->b:LX/DkN;

    iput-object p2, p0, LX/Dk7;->a:LX/Diz;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2033950
    iget-object v0, p0, LX/Dk7;->a:LX/Diz;

    invoke-virtual {v0, p1}, LX/Diz;->a(Ljava/lang/Throwable;)V

    .line 2033951
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033952
    check-cast p1, LX/0Px;

    .line 2033953
    if-nez p1, :cond_0

    .line 2033954
    iget-object v0, p0, LX/Dk7;->a:LX/Diz;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "result is NULL"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/Diz;->a(Ljava/lang/Throwable;)V

    .line 2033955
    :goto_0
    return-void

    .line 2033956
    :cond_0
    iget-object v0, p0, LX/Dk7;->a:LX/Diz;

    .line 2033957
    iget-object v1, v0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->h:LX/DmQ;

    .line 2033958
    iget-object v2, v1, LX/DmQ;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2033959
    iget-object v2, v1, LX/DmQ;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2033960
    iget-object v1, v0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->h:LX/DmQ;

    iget-object v2, v0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->j:LX/Did;

    .line 2033961
    iput-object v2, v1, LX/DmQ;->i:LX/Did;

    .line 2033962
    iget-object v1, v0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->h:LX/DmQ;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2033963
    iget-object v1, v0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;Z)V

    .line 2033964
    iget-object v1, v0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v2, v0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->h:LX/DmQ;

    .line 2033965
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENT_REQUESTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    iget-object p1, v2, LX/DmQ;->c:LX/DkQ;

    invoke-virtual {p1}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2033966
    if-eqz v0, :cond_1

    .line 2033967
    const v0, 0x7f082b9f

    .line 2033968
    :goto_1
    move v2, v0

    .line 2033969
    invoke-static {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;I)V

    .line 2033970
    goto :goto_0

    :cond_1
    const v0, 0x7f082b9e

    goto :goto_1
.end method
