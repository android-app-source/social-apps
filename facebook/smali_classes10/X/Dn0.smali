.class public final enum LX/Dn0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dn0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dn0;

.field public static final enum DATE_PICKER:LX/Dn0;

.field public static final enum HEADER_TEXT:LX/Dn0;

.field public static final enum OPTIONAL_ADDITIONAL_NOTES:LX/Dn0;

.field public static final enum OPTIONAL_DECLINE_REQUEST_BUTTON:LX/Dn0;

.field public static final enum OPTIONAL_DIVIDER:LX/Dn0;

.field public static final enum OPTIONAL_REQUEST_ITEM_TIME:LX/Dn0;

.field public static final enum OPTIONAL_SERVICE_GENERAL_INFO:LX/Dn0;

.field public static final enum OPTIONAL_USER_AVAILABILITY:LX/Dn0;

.field public static final enum SERVICE_SUMMARY:LX/Dn0;

.field public static final enum TIME_SPAN_PICKER:LX/Dn0;


# instance fields
.field public final layoutResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2039821
    new-instance v0, LX/Dn0;

    const-string v1, "OPTIONAL_REQUEST_ITEM_TIME"

    const v2, 0x7f0303a6

    invoke-direct {v0, v1, v4, v2}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->OPTIONAL_REQUEST_ITEM_TIME:LX/Dn0;

    .line 2039822
    new-instance v0, LX/Dn0;

    const-string v1, "OPTIONAL_SERVICE_GENERAL_INFO"

    const v2, 0x7f0303a4

    invoke-direct {v0, v1, v5, v2}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->OPTIONAL_SERVICE_GENERAL_INFO:LX/Dn0;

    .line 2039823
    new-instance v0, LX/Dn0;

    const-string v1, "OPTIONAL_USER_AVAILABILITY"

    const v2, 0x7f0301b5

    invoke-direct {v0, v1, v6, v2}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->OPTIONAL_USER_AVAILABILITY:LX/Dn0;

    .line 2039824
    new-instance v0, LX/Dn0;

    const-string v1, "OPTIONAL_ADDITIONAL_NOTES"

    const v2, 0x7f0301b5

    invoke-direct {v0, v1, v7, v2}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->OPTIONAL_ADDITIONAL_NOTES:LX/Dn0;

    .line 2039825
    new-instance v0, LX/Dn0;

    const-string v1, "OPTIONAL_DECLINE_REQUEST_BUTTON"

    const v2, 0x7f0303a2

    invoke-direct {v0, v1, v8, v2}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->OPTIONAL_DECLINE_REQUEST_BUTTON:LX/Dn0;

    .line 2039826
    new-instance v0, LX/Dn0;

    const-string v1, "OPTIONAL_DIVIDER"

    const/4 v2, 0x5

    const v3, 0x7f0303a3

    invoke-direct {v0, v1, v2, v3}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->OPTIONAL_DIVIDER:LX/Dn0;

    .line 2039827
    new-instance v0, LX/Dn0;

    const-string v1, "HEADER_TEXT"

    const/4 v2, 0x6

    const v3, 0x7f0303a5

    invoke-direct {v0, v1, v2, v3}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->HEADER_TEXT:LX/Dn0;

    .line 2039828
    new-instance v0, LX/Dn0;

    const-string v1, "SERVICE_SUMMARY"

    const/4 v2, 0x7

    const v3, 0x7f0303a7

    invoke-direct {v0, v1, v2, v3}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->SERVICE_SUMMARY:LX/Dn0;

    .line 2039829
    new-instance v0, LX/Dn0;

    const-string v1, "DATE_PICKER"

    const/16 v2, 0x8

    const v3, 0x7f0303a1

    invoke-direct {v0, v1, v2, v3}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->DATE_PICKER:LX/Dn0;

    .line 2039830
    new-instance v0, LX/Dn0;

    const-string v1, "TIME_SPAN_PICKER"

    const/16 v2, 0x9

    const v3, 0x7f0303a8

    invoke-direct {v0, v1, v2, v3}, LX/Dn0;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dn0;->TIME_SPAN_PICKER:LX/Dn0;

    .line 2039831
    const/16 v0, 0xa

    new-array v0, v0, [LX/Dn0;

    sget-object v1, LX/Dn0;->OPTIONAL_REQUEST_ITEM_TIME:LX/Dn0;

    aput-object v1, v0, v4

    sget-object v1, LX/Dn0;->OPTIONAL_SERVICE_GENERAL_INFO:LX/Dn0;

    aput-object v1, v0, v5

    sget-object v1, LX/Dn0;->OPTIONAL_USER_AVAILABILITY:LX/Dn0;

    aput-object v1, v0, v6

    sget-object v1, LX/Dn0;->OPTIONAL_ADDITIONAL_NOTES:LX/Dn0;

    aput-object v1, v0, v7

    sget-object v1, LX/Dn0;->OPTIONAL_DECLINE_REQUEST_BUTTON:LX/Dn0;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Dn0;->OPTIONAL_DIVIDER:LX/Dn0;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Dn0;->HEADER_TEXT:LX/Dn0;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Dn0;->SERVICE_SUMMARY:LX/Dn0;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Dn0;->DATE_PICKER:LX/Dn0;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Dn0;->TIME_SPAN_PICKER:LX/Dn0;

    aput-object v2, v0, v1

    sput-object v0, LX/Dn0;->$VALUES:[LX/Dn0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2039832
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2039833
    iput p3, p0, LX/Dn0;->layoutResId:I

    .line 2039834
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dn0;
    .locals 1

    .prologue
    .line 2039835
    const-class v0, LX/Dn0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dn0;

    return-object v0
.end method

.method public static values()[LX/Dn0;
    .locals 1

    .prologue
    .line 2039836
    sget-object v0, LX/Dn0;->$VALUES:[LX/Dn0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dn0;

    return-object v0
.end method
