.class public LX/Crt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1941541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941542
    return-void
.end method

.method public static a(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)LX/Cn6;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1941556
    invoke-static {p0, p1}, LX/Crt;->c(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)I

    move-result v0

    .line 1941557
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, LX/Cmv;->a(FI)LX/Cmv;

    move-result-object v3

    .line 1941558
    new-instance v0, LX/Cn6;

    new-instance v1, LX/Cmw;

    sget-object v4, LX/Cmv;->a:LX/Cmv;

    sget-object v5, LX/Cmv;->a:LX/Cmv;

    invoke-direct {v1, v3, v4, v3, v5}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    const/4 v4, 0x0

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, LX/Cn6;-><init>(LX/Cmw;LX/Cmr;LX/Cmp;ILX/Cmo;LX/Cmw;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1941553
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;->b()LX/8Z4;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1941554
    :cond_0
    const/4 v0, 0x0

    .line 1941555
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;->b()LX/8Z4;

    move-result-object v0

    invoke-interface {v0}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentRelatedArticleSocialContext$SocialContextProfiles;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1941543
    invoke-static {p0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941544
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 1941545
    :goto_0
    return-object v0

    .line 1941546
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1941547
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextProfilesModel;

    .line 1941548
    if-eqz v0, :cond_1

    .line 1941549
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextProfilesModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextProfilesModel$ProfilePictureModel;

    move-result-object v0

    .line 1941550
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextProfilesModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1941551
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleSocialContextModel$SocialContextProfilesModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1941552
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 6

    .prologue
    .line 1941436
    if-eqz p1, :cond_0

    invoke-static {p3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1941437
    :cond_0
    return-void

    .line 1941438
    :cond_1
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 1941439
    if-eqz p2, :cond_2

    .line 1941440
    invoke-virtual {p1, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1941441
    :cond_2
    if-eqz p3, :cond_3

    .line 1941442
    invoke-virtual {p1, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1941443
    :cond_3
    if-eqz p4, :cond_4

    .line 1941444
    invoke-virtual {p1, p4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1941445
    :cond_4
    array-length v2, p5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p5, v0

    .line 1941446
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v4, p0, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v5, 0x21

    invoke-virtual {p1, v4, v1, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1941447
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;LX/Cm2;LX/Ck0;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1941537
    sget-object v0, LX/Cm2;->INLINE:LX/Cm2;

    if-ne p1, v0, :cond_0

    .line 1941538
    const v3, 0x7f0d0177

    const v5, 0x7f0d0177

    move-object v0, p2

    move-object v1, p0

    move v4, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1941539
    :goto_0
    return-void

    :cond_0
    move-object v0, p2

    move-object v1, p0

    move v3, v2

    move v4, v2

    move v5, v2

    .line 1941540
    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)Z
    .locals 1

    .prologue
    .line 1941536
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_COMPRESSED:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_FULL_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)I
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 1941531
    if-nez p0, :cond_0

    .line 1941532
    :goto_0
    return v0

    .line 1941533
    :cond_0
    sget-object v1, LX/Crs;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1941534
    :pswitch_0
    const/16 v0, 0x1c

    goto :goto_0

    .line 1941535
    :pswitch_1
    const/16 v0, 0x1d

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)LX/Cml;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1941559
    invoke-static {p0, p1}, LX/Crt;->c(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)I

    move-result v0

    .line 1941560
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, LX/Cmv;->a(FI)LX/Cmv;

    move-result-object v0

    .line 1941561
    new-instance v1, LX/Cmy;

    new-instance v2, LX/Cmw;

    sget-object v3, LX/Cmv;->a:LX/Cmv;

    sget-object v4, LX/Cmv;->a:LX/Cmv;

    invoke-direct {v2, v0, v3, v0, v4}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    const/4 v0, 0x0

    invoke-direct {v1, v2, v5, v5, v0}, LX/Cmy;-><init>(LX/Cmw;LX/Cmr;LX/Cmp;I)V

    return-object v1
.end method

.method public static b(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1941528
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$LinkMediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$LinkMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$LinkMediaModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$LinkMediaModel$ImageModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1941529
    :cond_0
    const/4 v0, 0x0

    .line 1941530
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$LinkMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$LinkMediaModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$LinkMediaModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$LinkMediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)I
    .locals 1

    .prologue
    .line 1941523
    invoke-virtual {p0}, LX/8bZ;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1941524
    invoke-static {p1}, LX/Crt;->a(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0177

    .line 1941525
    :goto_0
    return v0

    .line 1941526
    :cond_0
    const v0, 0x7f0d0120

    goto :goto_0

    .line 1941527
    :cond_1
    invoke-virtual {p0}, LX/8bZ;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, LX/Crt;->a(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d0177

    goto :goto_0

    :cond_2
    const v0, 0x7f0d0127

    goto :goto_0
.end method

.method public static c(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1941520
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->eh_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$TitleModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1941521
    :cond_0
    const/4 v0, 0x0

    .line 1941522
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->eh_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 9

    .prologue
    .line 1941460
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1941461
    :cond_0
    const/4 v0, 0x0

    .line 1941462
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;

    move-result-object v0

    .line 1941463
    if-nez v0, :cond_2

    .line 1941464
    const/4 v1, 0x0

    .line 1941465
    :goto_1
    move-object v0, v1

    .line 1941466
    goto :goto_0

    .line 1941467
    :cond_2
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    .line 1941468
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1941469
    iput-object v2, v1, LX/3dM;->y:Ljava/lang/String;

    .line 1941470
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    move-result-object v2

    .line 1941471
    if-nez v2, :cond_3

    .line 1941472
    const/4 v3, 0x0

    .line 1941473
    :goto_2
    move-object v2, v3

    .line 1941474
    invoke-virtual {v1, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 1941475
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    move-result-object v2

    .line 1941476
    if-nez v2, :cond_4

    .line 1941477
    const/4 v3, 0x0

    .line 1941478
    :goto_3
    move-object v2, v3

    .line 1941479
    invoke-virtual {v1, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 1941480
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    move-result-object v2

    .line 1941481
    if-nez v2, :cond_5

    .line 1941482
    const/4 v3, 0x0

    .line 1941483
    :goto_4
    move-object v2, v3

    .line 1941484
    invoke-virtual {v1, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 1941485
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    goto :goto_1

    .line 1941486
    :cond_3
    new-instance v3, LX/3dO;

    invoke-direct {v3}, LX/3dO;-><init>()V

    .line 1941487
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;->a()I

    move-result v4

    .line 1941488
    iput v4, v3, LX/3dO;->b:I

    .line 1941489
    invoke-virtual {v3}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v3

    goto :goto_2

    .line 1941490
    :cond_4
    new-instance v3, LX/4ZH;

    invoke-direct {v3}, LX/4ZH;-><init>()V

    .line 1941491
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;->a()I

    move-result v4

    .line 1941492
    iput v4, v3, LX/4ZH;->b:I

    .line 1941493
    invoke-virtual {v3}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v3

    goto :goto_3

    .line 1941494
    :cond_5
    new-instance v5, LX/3dQ;

    invoke-direct {v5}, LX/3dQ;-><init>()V

    .line 1941495
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 1941496
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1941497
    const/4 v3, 0x0

    move v4, v3

    :goto_5
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_6

    .line 1941498
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel$EdgesModel;

    .line 1941499
    if-nez v3, :cond_8

    .line 1941500
    const/4 v7, 0x0

    .line 1941501
    :goto_6
    move-object v3, v7

    .line 1941502
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1941503
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5

    .line 1941504
    :cond_6
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1941505
    iput-object v3, v5, LX/3dQ;->b:LX/0Px;

    .line 1941506
    :cond_7
    invoke-virtual {v5}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v3

    goto :goto_4

    .line 1941507
    :cond_8
    new-instance v7, LX/4ZJ;

    invoke-direct {v7}, LX/4ZJ;-><init>()V

    .line 1941508
    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel$EdgesModel$NodeModel;

    move-result-object v8

    .line 1941509
    if-nez v8, :cond_9

    .line 1941510
    const/4 p0, 0x0

    .line 1941511
    :goto_7
    move-object v8, p0

    .line 1941512
    iput-object v8, v7, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 1941513
    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel$EdgesModel;->b()I

    move-result v8

    .line 1941514
    iput v8, v7, LX/4ZJ;->c:I

    .line 1941515
    invoke-virtual {v7}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v7

    goto :goto_6

    .line 1941516
    :cond_9
    new-instance p0, LX/4WM;

    invoke-direct {p0}, LX/4WM;-><init>()V

    .line 1941517
    invoke-virtual {v8}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel$EdgesModel$NodeModel;->a()I

    move-result v0

    .line 1941518
    iput v0, p0, LX/4WM;->f:I

    .line 1941519
    invoke-virtual {p0}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object p0

    goto :goto_7
.end method

.method public static e(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1941457
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1941458
    :cond_0
    const/4 v0, 0x0

    .line 1941459
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1941454
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleDocumentOwnerModel$DocumentOwnerModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1941455
    :cond_0
    const/4 v0, 0x0

    .line 1941456
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleDocumentOwnerModel$DocumentOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleDocumentOwnerModel$DocumentOwnerModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1941451
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->ei_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$SummaryModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1941452
    :cond_0
    const/4 v0, 0x0

    .line 1941453
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->ei_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$SummaryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$SummaryModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1941448
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleDocumentOwnerModel$DocumentOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleDocumentOwnerModel$DocumentOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleDocumentOwnerModel$DocumentOwnerModel;->b()LX/1Fb;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1941449
    :cond_0
    const/4 v0, 0x0

    .line 1941450
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleGlobalShareModel$RelatedArticleInstantArticleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleVersionModel$RelatedArticleVersionRelatedArticleVersionModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleDocumentOwnerModel$DocumentOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleDocumentOwnerModel$DocumentOwnerModel;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
