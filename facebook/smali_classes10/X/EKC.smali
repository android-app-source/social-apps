.class public final LX/EKC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;

.field public final synthetic b:Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;)V
    .locals 0

    .prologue
    .line 2106384
    iput-object p1, p0, LX/EKC;->b:Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;

    iput-object p2, p0, LX/EKC;->a:Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, 0x46afbdcc

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2106385
    iget-object v1, p0, LX/EKC;->b:Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->m:LX/929;

    const-string v2, "fetch_composer_in_tnv2_search_results"

    .line 2106386
    iget-object p1, v1, LX/929;->b:LX/1Ck;

    invoke-virtual {p1, v2}, LX/1Ck;->b(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p1, v1, LX/929;->b:LX/1Ck;

    invoke-virtual {p1, v2}, LX/1Ck;->b(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    invoke-interface {p1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    :goto_0
    move v1, p1

    .line 2106387
    if-eqz v1, :cond_0

    .line 2106388
    iget-object v1, p0, LX/EKC;->b:Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->n:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2106389
    :goto_1
    const v1, -0x90e5246

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2106390
    :cond_0
    iget-object v1, p0, LX/EKC;->b:Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;

    iget-object v2, p0, LX/EKC;->a:Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;

    invoke-static {v1, v2}, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->c(Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;)V

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method
