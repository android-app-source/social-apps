.class public final LX/ESN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSimpleOptInFragment;)V
    .locals 0

    .prologue
    .line 2122918
    iput-object p1, p0, LX/ESN;->a:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1a0a63ac

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2122919
    iget-object v1, p0, LX/ESN;->a:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->h:LX/17Y;

    if-nez v1, :cond_0

    .line 2122920
    const v1, 0x3c7c17fe

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2122921
    :goto_0
    return-void

    .line 2122922
    :cond_0
    iget-object v1, p0, LX/ESN;->a:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->h:LX/17Y;

    iget-object v2, p0, LX/ESN;->a:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "photosync_help"

    invoke-static {v3}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2122923
    if-nez v1, :cond_1

    .line 2122924
    const v1, 0x3c78f8b6

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2122925
    :cond_1
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2122926
    iget-object v2, p0, LX/ESN;->a:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultSimpleOptInFragment;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/ESN;->a:Lcom/facebook/vault/ui/VaultSimpleOptInFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2122927
    const v1, 0x7bf4b18c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
