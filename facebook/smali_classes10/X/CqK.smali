.class public LX/CqK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/CqE;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/Ciy;

.field public final c:LX/Chv;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:LX/ChN;

.field public f:LX/CqE;

.field public g:LX/CqJ;

.field public h:Z


# direct methods
.method public constructor <init>(LX/Ciy;LX/Chv;LX/0Wd;)V
    .locals 2
    .param p3    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1939590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1939591
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/CqK;->a:Ljava/util/Queue;

    .line 1939592
    new-instance v0, LX/CqI;

    invoke-direct {v0, p0}, LX/CqI;-><init>(LX/CqK;)V

    iput-object v0, p0, LX/CqK;->e:LX/ChN;

    .line 1939593
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CqK;->h:Z

    .line 1939594
    iput-object p1, p0, LX/CqK;->b:LX/Ciy;

    .line 1939595
    iput-object p2, p0, LX/CqK;->c:LX/Chv;

    .line 1939596
    iput-object p3, p0, LX/CqK;->d:Ljava/util/concurrent/ExecutorService;

    .line 1939597
    iget-object v0, p0, LX/CqK;->b:LX/Ciy;

    invoke-virtual {v0}, LX/Ciy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939598
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqK;->h:Z

    .line 1939599
    :goto_0
    return-void

    .line 1939600
    :cond_0
    iget-object v0, p0, LX/CqK;->c:LX/Chv;

    iget-object v1, p0, LX/CqK;->e:LX/ChN;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/CqK;
    .locals 6

    .prologue
    .line 1939579
    const-class v1, LX/CqK;

    monitor-enter v1

    .line 1939580
    :try_start_0
    sget-object v0, LX/CqK;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1939581
    sput-object v2, LX/CqK;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1939582
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939583
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1939584
    new-instance p0, LX/CqK;

    invoke-static {v0}, LX/Ciy;->a(LX/0QB;)LX/Ciy;

    move-result-object v3

    check-cast v3, LX/Ciy;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v4

    check-cast v4, LX/Chv;

    invoke-static {v0}, LX/0kl;->b(LX/0QB;)LX/0Wd;

    move-result-object v5

    check-cast v5, LX/0Wd;

    invoke-direct {p0, v3, v4, v5}, LX/CqK;-><init>(LX/Ciy;LX/Chv;LX/0Wd;)V

    .line 1939585
    move-object v0, p0

    .line 1939586
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1939587
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CqK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1939588
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1939589
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static declared-synchronized b$redex0(LX/CqK;)V
    .locals 2

    .prologue
    .line 1939565
    monitor-enter p0

    .line 1939566
    :try_start_0
    iget-object v0, p0, LX/CqK;->f:LX/CqE;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/CqK;->f:LX/CqE;

    invoke-interface {v0}, LX/CqE;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1939567
    iget-object v0, p0, LX/CqK;->f:LX/CqE;

    .line 1939568
    :cond_0
    :goto_0
    move-object v0, v0

    .line 1939569
    iput-object v0, p0, LX/CqK;->f:LX/CqE;

    .line 1939570
    iget-object v0, p0, LX/CqK;->f:LX/CqE;

    if-eqz v0, :cond_1

    .line 1939571
    iget-object v0, p0, LX/CqK;->g:LX/CqJ;

    if-eqz v0, :cond_2

    .line 1939572
    iget-object v0, p0, LX/CqK;->f:LX/CqE;

    invoke-interface {v0}, LX/CqE;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1939573
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 1939574
    :cond_2
    :try_start_1
    new-instance v0, LX/CqJ;

    iget-object v1, p0, LX/CqK;->d:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, p0, v1}, LX/CqJ;-><init>(LX/CqK;Ljava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, LX/CqK;->g:LX/CqJ;

    .line 1939575
    iget-object v0, p0, LX/CqK;->g:LX/CqJ;

    invoke-virtual {v0}, LX/1Rm;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1939576
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1939577
    :cond_3
    iget-object v0, p0, LX/CqK;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqE;

    .line 1939578
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/CqE;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(LX/CqE;)V
    .locals 1

    .prologue
    .line 1939559
    monitor-enter p0

    if-nez p1, :cond_1

    .line 1939560
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1939561
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/CqK;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1939562
    iget-object v0, p0, LX/CqK;->f:LX/CqE;

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/CqK;->h:Z

    if-eqz v0, :cond_0

    .line 1939563
    invoke-static {p0}, LX/CqK;->b$redex0(LX/CqK;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1939564
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
