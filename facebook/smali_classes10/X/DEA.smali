.class public LX/DEA;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DEC;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DEA",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DEC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976419
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1976420
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DEA;->b:LX/0Zi;

    .line 1976421
    iput-object p1, p0, LX/DEA;->a:LX/0Ot;

    .line 1976422
    return-void
.end method

.method public static a(LX/0QB;)LX/DEA;
    .locals 4

    .prologue
    .line 1976423
    const-class v1, LX/DEA;

    monitor-enter v1

    .line 1976424
    :try_start_0
    sget-object v0, LX/DEA;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976425
    sput-object v2, LX/DEA;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976426
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976427
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976428
    new-instance v3, LX/DEA;

    const/16 p0, 0x1fb3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DEA;-><init>(LX/0Ot;)V

    .line 1976429
    move-object v0, v3

    .line 1976430
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976431
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DEA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976432
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976433
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1976434
    check-cast p2, LX/DE9;

    .line 1976435
    iget-object v0, p0, LX/DEA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DEC;

    iget-object v1, p2, LX/DE9;->a:LX/1Po;

    iget-object v2, p2, LX/DE9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976436
    new-instance v4, LX/DEB;

    invoke-direct {v4, v0, v2}, LX/DEB;-><init>(LX/DEC;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1976437
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    .line 1976438
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1976439
    check-cast v3, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 1976440
    iput-object v3, v5, LX/3mP;->d:LX/25L;

    .line 1976441
    move-object v5, v5

    .line 1976442
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1976443
    check-cast v3, LX/0jW;

    .line 1976444
    iput-object v3, v5, LX/3mP;->e:LX/0jW;

    .line 1976445
    move-object v3, v5

    .line 1976446
    const/16 v5, 0x8

    .line 1976447
    iput v5, v3, LX/3mP;->b:I

    .line 1976448
    move-object v3, v3

    .line 1976449
    iput-object v4, v3, LX/3mP;->g:LX/25K;

    .line 1976450
    move-object v3, v3

    .line 1976451
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v3

    .line 1976452
    iget-object v4, v0, LX/DEC;->c:LX/DE7;

    .line 1976453
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1976454
    check-cast v5, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    .line 1976455
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k()LX/0Px;

    move-result-object v6

    .line 1976456
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1976457
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 1976458
    new-instance v9, LX/DEI;

    invoke-direct {v9, v5, v6}, LX/DEI;-><init>(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;)V

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1976459
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v5, v5

    .line 1976460
    new-instance v6, LX/DE6;

    move-object v9, v1

    check-cast v9, LX/1Po;

    invoke-static {v4}, LX/DEF;->a(LX/0QB;)LX/DEF;

    move-result-object v11

    check-cast v11, LX/DEF;

    move-object v7, p1

    move-object v8, v5

    move-object v10, v3

    invoke-direct/range {v6 .. v11}, LX/DE6;-><init>(Landroid/content/Context;LX/0Px;LX/1Po;LX/25M;LX/DEF;)V

    .line 1976461
    move-object v4, v6

    .line 1976462
    iget-object v3, v0, LX/DEC;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3mL;

    invoke-virtual {v3, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1976463
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1976464
    invoke-static {}, LX/1dS;->b()V

    .line 1976465
    const/4 v0, 0x0

    return-object v0
.end method
