.class public LX/ElN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Ekt;

.field private final b:LX/ElF;

.field private final c:LX/0n9;

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z


# direct methods
.method public constructor <init>(LX/0Zh;LX/Ekt;LX/ElF;)V
    .locals 1

    .prologue
    .line 2164280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2164281
    iput-object p2, p0, LX/ElN;->a:LX/Ekt;

    .line 2164282
    iput-object p3, p0, LX/ElN;->b:LX/ElF;

    .line 2164283
    invoke-virtual {p1}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    iput-object v0, p0, LX/ElN;->c:LX/0n9;

    .line 2164284
    return-void
.end method


# virtual methods
.method public final a()LX/Ekk;
    .locals 4

    .prologue
    .line 2164285
    iget-object v0, p0, LX/ElN;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2164286
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must set method"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2164287
    :cond_0
    iget-object v0, p0, LX/ElN;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ElN;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2164288
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must set tier and uriPath (maybe you should be using a helper like FbGraphQLRequestBuilder?)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2164289
    :cond_2
    iget-object v0, p0, LX/ElN;->f:Ljava/lang/String;

    const-string v1, "method/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2164290
    const-string v0, "method"

    iget-object v1, p0, LX/ElN;->f:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/ElN;->a(Ljava/lang/String;Ljava/lang/String;)LX/ElN;

    .line 2164291
    :goto_0
    iget-object v0, p0, LX/ElN;->a:LX/Ekt;

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    iget-object v2, p0, LX/ElN;->b:LX/ElF;

    invoke-interface {v2}, LX/ElF;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, p0, LX/ElN;->b:LX/ElF;

    iget-object v3, p0, LX/ElN;->e:Ljava/lang/String;

    invoke-interface {v2, v3}, LX/ElF;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, p0, LX/ElN;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2164292
    iget-object v2, v0, LX/Ekt;->d:LX/Ekz;

    invoke-interface {v2, v1}, LX/Ekz;->b(Ljava/lang/String;)LX/Ekz;

    .line 2164293
    iget-object v0, p0, LX/ElN;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2164294
    iget-object v0, p0, LX/ElN;->a:LX/Ekt;

    const-string v1, "If-None-Match"

    iget-object v2, p0, LX/ElN;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/Ekt;->a(Ljava/lang/String;Ljava/lang/String;)LX/Ekt;

    .line 2164295
    :cond_3
    iget-object v0, p0, LX/ElN;->c:LX/0n9;

    invoke-virtual {v0}, LX/0nA;->f()V

    .line 2164296
    new-instance v1, LX/ElW;

    iget-object v0, p0, LX/ElN;->c:LX/0n9;

    invoke-direct {v1, v0}, LX/ElW;-><init>(LX/0nA;)V

    .line 2164297
    iget-boolean v0, p0, LX/ElN;->h:Z

    if-nez v0, :cond_5

    .line 2164298
    iget-object v0, p0, LX/ElN;->a:LX/Ekt;

    const-string v2, "Content-Encoding"

    const-string v3, "gzip"

    invoke-virtual {v0, v2, v3}, LX/Ekt;->a(Ljava/lang/String;Ljava/lang/String;)LX/Ekt;

    .line 2164299
    new-instance v0, LX/ElM;

    invoke-direct {v0, v1}, LX/ElM;-><init>(LX/El1;)V

    .line 2164300
    :goto_1
    iget-object v1, p0, LX/ElN;->a:LX/Ekt;

    const-string v2, "POST"

    .line 2164301
    iget-object v3, v1, LX/Ekt;->d:LX/Ekz;

    invoke-interface {v3, v2, v0}, LX/Ekz;->a(Ljava/lang/String;LX/El1;)LX/Ekz;

    .line 2164302
    iget-object v0, p0, LX/ElN;->a:LX/Ekt;

    .line 2164303
    iget-object v1, v0, LX/Ekt;->a:LX/Ekr;

    iget-object v2, v0, LX/Ekt;->c:LX/Eki;

    .line 2164304
    new-instance v0, LX/Ekj;

    invoke-direct {v0, v2}, LX/Ekj;-><init>(LX/Eki;)V

    move-object v2, v0

    .line 2164305
    invoke-virtual {v1, v2}, LX/Ekr;->a(LX/Ekj;)LX/Ekk;

    move-result-object v1

    move-object v0, v1

    .line 2164306
    return-object v0

    .line 2164307
    :cond_4
    const-string v0, "method"

    iget-object v1, p0, LX/ElN;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/ElN;->a(Ljava/lang/String;Ljava/lang/String;)LX/ElN;

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 2164308
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/0nA;)LX/ElN;
    .locals 2

    .prologue
    .line 2164276
    if-nez p2, :cond_0

    .line 2164277
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "valueSubBody cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2164278
    :cond_0
    iget-object v0, p0, LX/ElN;->c:LX/0n9;

    invoke-virtual {v0, p1, p2}, LX/0n9;->a(Ljava/lang/String;LX/0nA;)V

    .line 2164279
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/ElN;
    .locals 2

    .prologue
    .line 2164271
    if-nez p2, :cond_0

    .line 2164272
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "value cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2164273
    :cond_0
    iget-object v0, p0, LX/ElN;->c:LX/0n9;

    .line 2164274
    invoke-static {v0, p1, p2}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2164275
    return-object p0
.end method
