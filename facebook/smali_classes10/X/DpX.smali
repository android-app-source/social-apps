.class public LX/DpX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final is_primary_device:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2045882
    new-instance v0, LX/1sv;

    const-string v1, "RegisterResponsePayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpX;->b:LX/1sv;

    .line 2045883
    new-instance v0, LX/1sw;

    const-string v1, "is_primary_device"

    const/4 v2, 0x2

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpX;->c:LX/1sw;

    .line 2045884
    const/4 v0, 0x1

    sput-boolean v0, LX/DpX;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 2045833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2045834
    iput-object p1, p0, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    .line 2045835
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2045862
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2045863
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2045864
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2045865
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RegisterResponsePayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045866
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045867
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045868
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045869
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045870
    const-string v4, "is_primary_device"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045871
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045872
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045873
    iget-object v0, p0, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    .line 2045874
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045875
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045876
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045877
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2045878
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 2045879
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 2045880
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 2045881
    :cond_3
    iget-object v0, p0, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2045855
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2045856
    iget-object v0, p0, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 2045857
    sget-object v0, LX/DpX;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045858
    iget-object v0, p0, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2045859
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2045860
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2045861
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2045840
    if-nez p1, :cond_1

    .line 2045841
    :cond_0
    :goto_0
    return v0

    .line 2045842
    :cond_1
    instance-of v1, p1, LX/DpX;

    if-eqz v1, :cond_0

    .line 2045843
    check-cast p1, LX/DpX;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2045844
    if-nez p1, :cond_3

    .line 2045845
    :cond_2
    :goto_1
    move v0, v2

    .line 2045846
    goto :goto_0

    .line 2045847
    :cond_3
    iget-object v0, p0, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    move v0, v1

    .line 2045848
    :goto_2
    iget-object v3, p1, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    if-eqz v3, :cond_7

    move v3, v1

    .line 2045849
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2045850
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045851
    iget-object v0, p0, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    iget-object v3, p1, LX/DpX;->is_primary_device:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 2045852
    goto :goto_1

    :cond_6
    move v0, v2

    .line 2045853
    goto :goto_2

    :cond_7
    move v3, v2

    .line 2045854
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2045839
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2045836
    sget-boolean v0, LX/DpX;->a:Z

    .line 2045837
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpX;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2045838
    return-object v0
.end method
