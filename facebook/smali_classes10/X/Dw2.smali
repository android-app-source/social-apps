.class public LX/Dw2;
.super LX/556;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/556",
        "<",
        "Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;",
        ">;"
    }
.end annotation


# instance fields
.field public final b:Landroid/graphics/Rect;

.field public final c:[I

.field private final d:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;)V
    .locals 1

    .prologue
    .line 2059845
    invoke-direct {p0, p1}, LX/556;-><init>(Landroid/view/View;)V

    .line 2059846
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/Dw2;->b:Landroid/graphics/Rect;

    .line 2059847
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/Dw2;->c:[I

    .line 2059848
    iput-object p1, p0, LX/Dw2;->d:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    .line 2059849
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 2059874
    iget-object v0, p0, LX/Dw2;->d:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    float-to-int v2, p1

    float-to-int v3, p2

    const/4 p0, 0x0

    .line 2059875
    if-gez v3, :cond_0

    move v3, p0

    .line 2059876
    :cond_0
    if-gez v2, :cond_1

    move v2, p0

    .line 2059877
    :cond_1
    :goto_0
    iget-object p1, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    array-length p1, p1

    if-ge p0, p1, :cond_4

    .line 2059878
    iget-object p1, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    aget-object p1, p1, p0

    .line 2059879
    iget-object p2, p1, LX/DwE;->b:Landroid/graphics/Rect;

    if-eqz p2, :cond_3

    iget-object p1, p1, LX/DwE;->b:Landroid/graphics/Rect;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2059880
    :goto_1
    move v0, p0

    .line 2059881
    if-eq v0, v1, :cond_2

    .line 2059882
    :goto_2
    return v0

    :cond_2
    move v0, v1

    goto :goto_2

    .line 2059883
    :cond_3
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 2059884
    :cond_4
    const/4 p0, -0x1

    goto :goto_1
.end method

.method public final a(ILX/3sp;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 2059855
    iget-object v0, p0, LX/Dw2;->d:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    .line 2059856
    if-ltz p1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getNumOfItems()I

    move-result v1

    if-ge p1, v1, :cond_2

    iget-object v1, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    aget-object v1, v1, p1

    :goto_0
    move-object v0, v1

    .line 2059857
    if-eqz v0, :cond_1

    .line 2059858
    iget-object v1, p0, LX/Dw2;->d:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    invoke-virtual {p2, v1}, LX/3sp;->d(Landroid/view/View;)V

    .line 2059859
    invoke-virtual {p2, v2}, LX/3sp;->a(I)V

    .line 2059860
    const/16 v1, 0x10

    invoke-virtual {p2, v1}, LX/3sp;->a(I)V

    .line 2059861
    iget-object v1, p0, LX/Dw2;->d:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    .line 2059862
    iget-object v3, v0, LX/DwE;->b:Landroid/graphics/Rect;

    if-eqz v3, :cond_0

    .line 2059863
    iget-object v3, p0, LX/Dw2;->c:[I

    invoke-virtual {v1, v3}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getLocationOnScreen([I)V

    .line 2059864
    iget-object v3, p0, LX/Dw2;->c:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    .line 2059865
    iget-object v4, p0, LX/Dw2;->c:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    .line 2059866
    iget-object v5, p0, LX/Dw2;->b:Landroid/graphics/Rect;

    iget-object v6, v0, LX/DwE;->b:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v3

    iget-object v7, v0, LX/DwE;->b:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v4

    iget-object p1, v0, LX/DwE;->b:Landroid/graphics/Rect;

    iget p1, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, p1

    iget-object p1, v0, LX/DwE;->b:Landroid/graphics/Rect;

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, p1

    invoke-virtual {v5, v6, v7, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2059867
    iget-object v3, p0, LX/Dw2;->b:Landroid/graphics/Rect;

    invoke-virtual {p2, v3}, LX/3sp;->d(Landroid/graphics/Rect;)V

    .line 2059868
    :cond_0
    iget-object v0, v0, LX/DwE;->h:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 2059869
    invoke-virtual {p2, v2}, LX/3sp;->c(Z)V

    .line 2059870
    invoke-virtual {p2, v2}, LX/3sp;->a(Z)V

    .line 2059871
    invoke-virtual {p2, v2}, LX/3sp;->f(Z)V

    .line 2059872
    invoke-virtual {p2, v2}, LX/3sp;->h(Z)V

    .line 2059873
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(LX/3sp;)V
    .locals 2

    .prologue
    .line 2059850
    iget-object v0, p0, LX/Dw2;->d:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    .line 2059851
    const/4 v1, 0x0

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getNumOfItems()I

    move-result p0

    :goto_0
    if-ge v1, p0, :cond_0

    .line 2059852
    invoke-virtual {p1, v0, v1}, LX/3sp;->c(Landroid/view/View;I)V

    .line 2059853
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2059854
    :cond_0
    return-void
.end method
