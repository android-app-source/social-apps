.class public final LX/D3u;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Ve;

.field public final synthetic b:LX/3Q3;


# direct methods
.method public constructor <init>(LX/3Q3;LX/0Ve;)V
    .locals 0

    .prologue
    .line 1961402
    iput-object p1, p0, LX/D3u;->b:LX/3Q3;

    iput-object p2, p0, LX/D3u;->a:LX/0Ve;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1961400
    iget-object v0, p0, LX/D3u;->a:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1961401
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1961390
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1961391
    if-eqz p1, :cond_0

    .line 1961392
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1961393
    if-nez v0, :cond_1

    .line 1961394
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Fetched story was non-existent"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/D3u;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1961395
    :goto_0
    return-void

    .line 1961396
    :cond_1
    iget-object v1, p0, LX/D3u;->b:LX/3Q3;

    .line 1961397
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1961398
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1, v0}, LX/3Q3;->a(LX/3Q3;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1961399
    iget-object v1, p0, LX/D3u;->a:LX/0Ve;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
