.class public final LX/ESq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/videohome/data/VideoHomeItem;

.field public final synthetic b:LX/ESr;


# direct methods
.method public constructor <init>(LX/ESr;Lcom/facebook/video/videohome/data/VideoHomeItem;)V
    .locals 0

    .prologue
    .line 2123598
    iput-object p1, p0, LX/ESq;->b:LX/ESr;

    iput-object p2, p0, LX/ESq;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2123599
    sget-object v0, LX/ESr;->a:Ljava/lang/String;

    const-string v1, "Video Home channel creator info query failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2123600
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2123601
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2123602
    if-eqz p1, :cond_0

    .line 2123603
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2123604
    if-nez v0, :cond_2

    .line 2123605
    :cond_0
    sget-object v0, LX/ESr;->a:Ljava/lang/String;

    const-string v1, "Video Home channel creator info query succeeded but result was null."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2123606
    :cond_1
    return-void

    .line 2123607
    :cond_2
    iget-object v0, p0, LX/ESq;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-virtual {v0}, LX/ETQ;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2123608
    invoke-static {v0}, LX/ESx;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2123609
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2123610
    invoke-interface {v1}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v1

    .line 2123611
    invoke-static {v1}, LX/ESz;->fromString(Ljava/lang/String;)LX/ESz;

    move-result-object v3

    .line 2123612
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2123613
    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel;

    .line 2123614
    invoke-virtual {v1}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel;->j()Ljava/lang/String;

    move-result-object v5

    .line 2123615
    invoke-virtual {v1}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeChannelCreatorInfoModel;->k()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2123616
    const/4 v7, 0x0

    const-class v8, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    invoke-virtual {v6, v1, v7, v8, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionVideoCreatorStatus;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/ESz;->fromString(Ljava/lang/String;)LX/ESz;

    move-result-object v1

    .line 2123617
    if-eqz v1, :cond_4

    .line 2123618
    iget-object v6, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v6

    .line 2123619
    invoke-interface {v6}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2123620
    if-eq v3, v1, :cond_5

    .line 2123621
    iget-object v5, p0, LX/ESq;->b:LX/ESr;

    invoke-static {v5, v1, v0}, LX/ESr;->a$redex0(LX/ESr;LX/ESz;Lcom/facebook/video/videohome/data/VideoHomeItem;)V

    goto :goto_0

    .line 2123622
    :cond_5
    iget-object v1, p0, LX/ESq;->b:LX/ESr;

    invoke-virtual {v1, v0}, LX/ESr;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    goto :goto_0
.end method
