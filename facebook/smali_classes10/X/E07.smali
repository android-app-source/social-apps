.class public LX/E07;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0kL;

.field public final b:Landroid/content/Context;

.field public final c:Landroid/content/res/Resources;

.field private final d:LX/03V;


# direct methods
.method public constructor <init>(LX/0kL;Landroid/content/Context;Landroid/content/res/Resources;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2067847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067848
    iput-object p1, p0, LX/E07;->a:LX/0kL;

    .line 2067849
    iput-object p2, p0, LX/E07;->b:Landroid/content/Context;

    .line 2067850
    iput-object p3, p0, LX/E07;->c:Landroid/content/res/Resources;

    .line 2067851
    iput-object p4, p0, LX/E07;->d:LX/03V;

    .line 2067852
    return-void
.end method

.method public static a(LX/0QB;)LX/E07;
    .locals 7

    .prologue
    .line 2067829
    const-class v1, LX/E07;

    monitor-enter v1

    .line 2067830
    :try_start_0
    sget-object v0, LX/E07;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2067831
    sput-object v2, LX/E07;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2067832
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067833
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2067834
    new-instance p0, LX/E07;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/E07;-><init>(LX/0kL;Landroid/content/Context;Landroid/content/res/Resources;LX/03V;)V

    .line 2067835
    move-object v0, p0

    .line 2067836
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2067837
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/E07;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2067838
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2067839
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 2067845
    new-instance v0, LX/0ju;

    iget-object v1, p0, LX/E07;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080036

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2067846
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 2067853
    iget-object v0, p0, LX/E07;->d:LX/03V;

    const-string v1, "PlaceCreationErrorHandler"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2067854
    iget-object v0, p0, LX/E07;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0816cd

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    const-class v2, LX/E08;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2067855
    iput-object v2, v1, LX/27k;->g:Ljava/lang/String;

    .line 2067856
    move-object v1, v1

    .line 2067857
    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2067858
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2067840
    iget-object v0, p0, LX/E07;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081724

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    const-class v2, LX/E08;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2067841
    iput-object v2, v1, LX/27k;->g:Ljava/lang/String;

    .line 2067842
    move-object v1, v1

    .line 2067843
    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2067844
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;LX/Dzm;)V
    .locals 8

    .prologue
    .line 2067780
    :try_start_0
    throw p1
    :try_end_0
    .catch Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/E0y; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/E0x; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException; {:try_start_0 .. :try_end_0} :catch_3
    .catch LX/E0w; {:try_start_0 .. :try_end_0} :catch_4
    .catch LX/E0u; {:try_start_0 .. :try_end_0} :catch_5
    .catch LX/E0v; {:try_start_0 .. :try_end_0} :catch_6
    .catch LX/E0z; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_9

    .line 2067781
    :catch_0
    move-exception v0

    .line 2067782
    const/4 p1, 0x1

    const/4 v7, 0x0

    .line 2067783
    iget-object v1, p2, LX/Dzm;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v1, v1, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2067784
    new-instance v2, LX/0ju;

    iget-object v3, p0, LX/E07;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2067785
    invoke-virtual {v2, v7}, LX/0ju;->a(Z)LX/0ju;

    .line 2067786
    const v3, 0x7f0816e5

    new-instance v4, LX/E03;

    invoke-direct {v4, p0, p2, v0}, LX/E03;-><init>(LX/E07;LX/Dzm;Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2067787
    iget-object v3, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 2067788
    iget-object v1, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v3, 0x7f0816d1

    new-array v4, p1, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;->name:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2067789
    :goto_0
    invoke-virtual {v2}, LX/0ju;->b()LX/2EJ;

    .line 2067790
    :goto_1
    return-void

    .line 2067791
    :catch_1
    invoke-direct {p0}, LX/E07;->b()V

    goto :goto_1

    .line 2067792
    :catch_2
    invoke-direct {p0}, LX/E07;->b()V

    goto :goto_1

    .line 2067793
    :catch_3
    move-exception v0

    .line 2067794
    const/4 v5, 0x0

    .line 2067795
    new-instance v2, LX/0ju;

    iget-object v1, p0, LX/E07;->b:Landroid/content/Context;

    invoke-direct {v2, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2067796
    iget-object v1, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;->suggestion:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2067797
    sget-object v1, LX/E06;->a:[I

    iget-object v3, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;->reason:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    invoke-virtual {v3}, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 2067798
    iget-object v1, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v3, 0x7f0816d3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    move-object v1, v1

    .line 2067799
    const v3, 0x7f080036

    invoke-virtual {v2, v3, v5}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2067800
    :goto_3
    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2067801
    invoke-virtual {v2}, LX/0ju;->b()LX/2EJ;

    .line 2067802
    goto :goto_1

    .line 2067803
    :catch_4
    iget-object v0, p0, LX/E07;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0816ce

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    const-class v2, LX/E08;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2067804
    iput-object v2, v1, LX/27k;->g:Ljava/lang/String;

    .line 2067805
    move-object v1, v1

    .line 2067806
    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2067807
    goto :goto_1

    .line 2067808
    :catch_5
    const v0, 0x7f0816aa

    invoke-direct {p0, v0}, LX/E07;->a(I)V

    goto :goto_1

    .line 2067809
    :catch_6
    const v0, 0x7f0816ab

    invoke-direct {p0, v0}, LX/E07;->a(I)V

    goto :goto_1

    .line 2067810
    :catch_7
    move-exception v0

    .line 2067811
    invoke-direct {p0, v0}, LX/E07;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 2067812
    :catch_8
    move-exception v0

    .line 2067813
    invoke-direct {p0, v0}, LX/E07;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 2067814
    :catch_9
    move-exception v0

    .line 2067815
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2067816
    :cond_0
    iget-object v3, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v4, 0x7f0816d2

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;->name:Ljava/lang/String;

    aput-object v6, v5, v7

    aput-object v1, v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2067817
    const v1, 0x7f0816e6

    new-instance v3, LX/E04;

    invoke-direct {v3, p0, p2, v0}, LX/E04;-><init>(LX/E07;LX/Dzm;Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;)V

    invoke-virtual {v2, v1, v3}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto/16 :goto_0

    .line 2067818
    :cond_1
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 2067819
    sget-object v1, LX/E06;->a:[I

    iget-object v3, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;->reason:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    invoke-virtual {v3}, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_1

    .line 2067820
    iget-object v1, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v3, 0x7f0816d4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;->suggestion:Ljava/lang/String;

    aput-object v6, v4, v7

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_4
    move-object v1, v1

    .line 2067821
    const v3, 0x7f080036

    new-instance v4, LX/E05;

    invoke-direct {v4, p0, p2, v0}, LX/E05;-><init>(LX/E07;LX/Dzm;Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2067822
    const v3, 0x7f080017

    invoke-virtual {v2, v3, v5}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto/16 :goto_3

    .line 2067823
    :pswitch_0
    iget-object v1, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v3, 0x7f0816d5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 2067824
    :pswitch_1
    iget-object v1, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v3, 0x7f0816d7

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 2067825
    :pswitch_2
    iget-object v1, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v3, 0x7f0816d9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 2067826
    :pswitch_3
    iget-object v1, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v3, 0x7f0816d6

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;->suggestion:Ljava/lang/String;

    aput-object v6, v4, v7

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 2067827
    :pswitch_4
    iget-object v1, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v3, 0x7f0816d8

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;->suggestion:Ljava/lang/String;

    aput-object v6, v4, v7

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 2067828
    :pswitch_5
    iget-object v1, p0, LX/E07;->c:Landroid/content/res/Resources;

    const v3, 0x7f0816da

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;->suggestion:Ljava/lang/String;

    aput-object v6, v4, v7

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
