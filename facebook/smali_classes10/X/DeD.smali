.class public LX/DeD;
.super LX/2Vx;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Vx",
        "<",
        "LX/DeE;",
        "[B>;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DeD;


# direct methods
.method public constructor <init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/0rb;LX/1Ha;LX/1GQ;)V
    .locals 10
    .param p5    # LX/0rb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1Ha;
        .annotation build Lcom/facebook/messaging/font/FontFileCache;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2021195
    invoke-static {}, LX/DeD;->b()LX/2W2;

    move-result-object v5

    invoke-static {}, LX/DeD;->a()LX/2W4;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, LX/2Vx;-><init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/2W2;LX/0rb;LX/1Ha;LX/2W4;LX/1GQ;)V

    .line 2021196
    return-void
.end method

.method private static a()LX/2W4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2W4",
            "<",
            "LX/DeE;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 2021197
    new-instance v0, LX/DeC;

    invoke-direct {v0}, LX/DeC;-><init>()V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/DeD;
    .locals 11

    .prologue
    .line 2021198
    sget-object v0, LX/DeD;->a:LX/DeD;

    if-nez v0, :cond_1

    .line 2021199
    const-class v1, LX/DeD;

    monitor-enter v1

    .line 2021200
    :try_start_0
    sget-object v0, LX/DeD;->a:LX/DeD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2021201
    if-eqz v2, :cond_0

    .line 2021202
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2021203
    new-instance v3, LX/DeD;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v5

    check-cast v5, LX/0pi;

    invoke-static {v0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v6

    check-cast v6, LX/1Gm;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v8

    check-cast v8, LX/0rb;

    invoke-static {v0}, LX/DeA;->a(LX/0QB;)LX/1Ha;

    move-result-object v9

    check-cast v9, LX/1Ha;

    invoke-static {v0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v10

    check-cast v10, LX/1GQ;

    invoke-direct/range {v3 .. v10}, LX/DeD;-><init>(LX/0SG;LX/0pi;LX/1Gm;LX/03V;LX/0rb;LX/1Ha;LX/1GQ;)V

    .line 2021204
    move-object v0, v3

    .line 2021205
    sput-object v0, LX/DeD;->a:LX/DeD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2021206
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2021207
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2021208
    :cond_1
    sget-object v0, LX/DeD;->a:LX/DeD;

    return-object v0

    .line 2021209
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2021210
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b()LX/2W2;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2021211
    new-instance v0, LX/2W2;

    invoke-direct {v0}, LX/2W2;-><init>()V

    const-string v1, "custom_fonts"

    .line 2021212
    iput-object v1, v0, LX/2W2;->a:Ljava/lang/String;

    .line 2021213
    move-object v0, v0

    .line 2021214
    const-string v1, "custom_fonts"

    .line 2021215
    iput-object v1, v0, LX/2W2;->b:Ljava/lang/String;

    .line 2021216
    move-object v0, v0

    .line 2021217
    const/16 v1, 0x32

    .line 2021218
    iput v1, v0, LX/2W2;->f:I

    .line 2021219
    move-object v0, v0

    .line 2021220
    iput v2, v0, LX/2W2;->d:I

    .line 2021221
    move-object v0, v0

    .line 2021222
    iput v2, v0, LX/2W2;->e:I

    .line 2021223
    move-object v0, v0

    .line 2021224
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2021225
    check-cast p1, [B

    .line 2021226
    array-length v0, p1

    return v0
.end method
