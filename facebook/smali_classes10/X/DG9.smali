.class public LX/DG9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/DGN;

.field private final b:LX/DGV;


# direct methods
.method public constructor <init>(LX/DGN;LX/DGV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1979262
    iput-object p1, p0, LX/DG9;->a:LX/DGN;

    .line 1979263
    iput-object p2, p0, LX/DG9;->b:LX/DGV;

    .line 1979264
    return-void
.end method

.method public static a(LX/0QB;)LX/DG9;
    .locals 5

    .prologue
    .line 1979265
    const-class v1, LX/DG9;

    monitor-enter v1

    .line 1979266
    :try_start_0
    sget-object v0, LX/DG9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979267
    sput-object v2, LX/DG9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979268
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979269
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979270
    new-instance p0, LX/DG9;

    invoke-static {v0}, LX/DGN;->a(LX/0QB;)LX/DGN;

    move-result-object v3

    check-cast v3, LX/DGN;

    invoke-static {v0}, LX/DGV;->a(LX/0QB;)LX/DGV;

    move-result-object v4

    check-cast v4, LX/DGV;

    invoke-direct {p0, v3, v4}, LX/DG9;-><init>(LX/DGN;LX/DGV;)V

    .line 1979271
    move-object v0, p0

    .line 1979272
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979273
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DG9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979274
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979275
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/DGK;LX/DG8;LX/1Pd;)LX/1Dg;
    .locals 3
    .param p2    # LX/DGK;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/DG8;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pd;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/DGK;",
            "LX/DG8;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1979276
    sget-object v0, LX/DG7;->a:[I

    invoke-virtual {p3}, LX/DG8;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1979277
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No case to handle PageType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1979278
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No case to handle PageType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1979279
    :pswitch_1
    iget-object v0, p0, LX/DG9;->a:LX/DGN;

    const/4 v1, 0x0

    .line 1979280
    new-instance v2, LX/DGM;

    invoke-direct {v2, v0}, LX/DGM;-><init>(LX/DGN;)V

    .line 1979281
    iget-object p0, v0, LX/DGN;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DGL;

    .line 1979282
    if-nez p0, :cond_0

    .line 1979283
    new-instance p0, LX/DGL;

    invoke-direct {p0, v0}, LX/DGL;-><init>(LX/DGN;)V

    .line 1979284
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/DGL;->a$redex0(LX/DGL;LX/1De;IILX/DGM;)V

    .line 1979285
    move-object v2, p0

    .line 1979286
    move-object v1, v2

    .line 1979287
    move-object v0, v1

    .line 1979288
    iget-object v1, p2, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 1979289
    invoke-static {v1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1979290
    iget-object v2, v0, LX/DGL;->a:LX/DGM;

    iput-object v1, v2, LX/DGM;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979291
    iget-object v2, v0, LX/DGL;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1979292
    move-object v0, v0

    .line 1979293
    iget-object v1, v0, LX/DGL;->a:LX/DGM;

    iput-object p2, v1, LX/DGM;->a:LX/DGK;

    .line 1979294
    iget-object v1, v0, LX/DGL;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1979295
    move-object v0, v0

    .line 1979296
    iget-object v1, v0, LX/DGL;->a:LX/DGM;

    iput-object p4, v1, LX/DGM;->b:LX/1Pd;

    .line 1979297
    iget-object v1, v0, LX/DGL;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1979298
    move-object v0, v0

    .line 1979299
    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    .line 1979300
    :goto_0
    return-object v0

    :pswitch_2
    iget-object v0, p0, LX/DG9;->b:LX/DGV;

    const/4 v1, 0x0

    .line 1979301
    new-instance v2, LX/DGU;

    invoke-direct {v2, v0}, LX/DGU;-><init>(LX/DGV;)V

    .line 1979302
    iget-object p0, v0, LX/DGV;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DGT;

    .line 1979303
    if-nez p0, :cond_1

    .line 1979304
    new-instance p0, LX/DGT;

    invoke-direct {p0, v0}, LX/DGT;-><init>(LX/DGV;)V

    .line 1979305
    :cond_1
    invoke-static {p0, p1, v1, v1, v2}, LX/DGT;->a$redex0(LX/DGT;LX/1De;IILX/DGU;)V

    .line 1979306
    move-object v2, p0

    .line 1979307
    move-object v1, v2

    .line 1979308
    move-object v0, v1

    .line 1979309
    iget-object v1, p2, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 1979310
    invoke-static {v1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1979311
    iget-object v2, v0, LX/DGT;->a:LX/DGU;

    iput-object v1, v2, LX/DGU;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979312
    iget-object v2, v0, LX/DGT;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1979313
    move-object v0, v0

    .line 1979314
    iget-object v1, v0, LX/DGT;->a:LX/DGU;

    iput-object p4, v1, LX/DGU;->b:LX/1Pd;

    .line 1979315
    iget-object v1, v0, LX/DGT;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1979316
    move-object v0, v0

    .line 1979317
    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
