.class public LX/Er9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:LX/ErU;

.field public B:LX/ErW;

.field private C:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public D:LX/0jo;

.field public E:LX/3kp;

.field private final a:LX/0Tn;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public e:Z

.field private f:Z

.field public g:Landroid/content/Context;

.field public h:LX/Blb;

.field public i:LX/Eqe;

.field public j:LX/Eqw;

.field public k:LX/Eqx;

.field public l:LX/Eqx;

.field public m:LX/Eqx;

.field private n:LX/EqG;

.field public o:LX/0gc;

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Eqx;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/1OX;

.field public u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public v:Landroid/widget/TextView;

.field public w:Landroid/view/View;

.field public x:Landroid/view/ViewGroup;

.field private y:LX/Eqy;

.field public z:LX/0iA;


# direct methods
.method public constructor <init>(LX/Eqy;LX/ErU;LX/ErW;LX/0iA;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0jo;LX/0Or;LX/3kp;Landroid/view/ViewGroup;LX/Blb;Ljava/util/Set;IZLX/0Px;LX/0Px;ZLX/0Px;ZLX/Eqe;LX/EqG;LX/1OX;LX/1OX;LX/0gc;)V
    .locals 7
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p9    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/Blb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Ljava/util/Set;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p18    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p19    # LX/Eqe;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p20    # LX/EqG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p21    # LX/1OX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p22    # LX/1OX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p23    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Eqy;",
            "LX/ErU;",
            "LX/ErW;",
            "LX/0iA;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0jo;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/3kp;",
            "Landroid/view/ViewGroup;",
            "LX/Blb;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;IZ",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;Z",
            "Lcom/facebook/events/invite/EventsExtendedInviteFragment$AddContactsButtonClickListener;",
            "Lcom/facebook/events/invite/EventsExtendedInviteFriendSelectionChangedListener;",
            "LX/1OX;",
            "LX/1OX;",
            "LX/0gc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2172563
    iput-object p1, p0, LX/Er9;->y:LX/Eqy;

    .line 2172564
    iput-object p2, p0, LX/Er9;->A:LX/ErU;

    .line 2172565
    iput-object p3, p0, LX/Er9;->B:LX/ErW;

    .line 2172566
    iput-object p4, p0, LX/Er9;->z:LX/0iA;

    .line 2172567
    iput-object p5, p0, LX/Er9;->C:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2172568
    iput-object p6, p0, LX/Er9;->D:LX/0jo;

    .line 2172569
    iput-object p8, p0, LX/Er9;->E:LX/3kp;

    .line 2172570
    invoke-interface {p7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    iput-object v1, p0, LX/Er9;->a:LX/0Tn;

    .line 2172571
    invoke-virtual/range {p9 .. p9}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, LX/Er9;->g:Landroid/content/Context;

    .line 2172572
    move-object/from16 v0, p10

    iput-object v0, p0, LX/Er9;->h:LX/Blb;

    .line 2172573
    move-object/from16 v0, p11

    iput-object v0, p0, LX/Er9;->b:Ljava/util/Set;

    .line 2172574
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Er9;->r:LX/0Px;

    .line 2172575
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Er9;->s:LX/0Px;

    .line 2172576
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Er9;->q:LX/0Px;

    .line 2172577
    move/from16 v0, p18

    iput-boolean v0, p0, LX/Er9;->f:Z

    .line 2172578
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Er9;->i:LX/Eqe;

    .line 2172579
    move-object/from16 v0, p20

    iput-object v0, p0, LX/Er9;->n:LX/EqG;

    .line 2172580
    move-object/from16 v0, p22

    iput-object v0, p0, LX/Er9;->t:LX/1OX;

    .line 2172581
    move-object/from16 v0, p23

    iput-object v0, p0, LX/Er9;->o:LX/0gc;

    move-object v1, p0

    move-object/from16 v2, p9

    move/from16 v3, p12

    move/from16 v4, p13

    move/from16 v5, p16

    move-object/from16 v6, p21

    .line 2172582
    invoke-direct/range {v1 .. v6}, LX/Er9;->a(Landroid/view/ViewGroup;IZZLX/1OX;)V

    .line 2172583
    return-void
.end method

.method public static a(LX/Er9;Z)V
    .locals 2

    .prologue
    .line 2172555
    if-eqz p1, :cond_1

    .line 2172556
    iget-object v0, p0, LX/Er9;->v:Landroid/widget/TextView;

    const v1, 0x7f080039

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2172557
    :goto_0
    invoke-static {p0}, LX/Er9;->d(LX/Er9;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2172558
    invoke-static {p0}, LX/Er9;->e(LX/Er9;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {p0, v0}, LX/Er9;->b(LX/Er9;Z)V

    .line 2172559
    :cond_0
    return-void

    .line 2172560
    :cond_1
    iget-object v0, p0, LX/Er9;->v:Landroid/widget/TextView;

    const v1, 0x7f080bd7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 2172561
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/view/ViewGroup;IZZLX/1OX;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2172530
    iget-object v0, p0, LX/Er9;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f03057d

    invoke-virtual {v0, v3, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    .line 2172531
    invoke-virtual {p0}, LX/Er9;->c()V

    .line 2172532
    iget-object v0, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    const v3, 0x7f0d0f31

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2172533
    iget-object v0, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/1P0;

    iget-object v4, p0, LX/Er9;->g:Landroid/content/Context;

    invoke-direct {v3, v4, v1, v2}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2172534
    invoke-static {p0, p2}, LX/Er9;->b(LX/Er9;I)LX/Eqw;

    move-result-object v0

    iput-object v0, p0, LX/Er9;->j:LX/Eqw;

    .line 2172535
    iget-object v0, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, LX/Er9;->j:LX/Eqw;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2172536
    iget-object v0, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/Er4;

    invoke-direct {v3, p0}, LX/Er4;-><init>(LX/Er9;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemClickListener(LX/1OZ;)V

    .line 2172537
    if-eqz p5, :cond_0

    .line 2172538
    iget-object v0, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p5}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2172539
    :cond_0
    iget-object v0, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/62g;

    iget-object v4, p0, LX/Er9;->j:LX/Eqw;

    invoke-direct {v3, v4, v2}, LX/62g;-><init>(LX/Eqw;Z)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2172540
    iget-object v0, p0, LX/Er9;->h:LX/Blb;

    sget-object v3, LX/Blb;->FACEBOOK:LX/Blb;

    if-ne v0, v3, :cond_1

    .line 2172541
    iget-object v0, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/Er5;

    invoke-direct {v3, p0}, LX/Er5;-><init>(LX/Er9;)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(LX/0fu;)V

    .line 2172542
    :cond_1
    iget-object v0, p0, LX/Er9;->h:LX/Blb;

    sget-object v3, LX/Blb;->CONTACTS:LX/Blb;

    if-ne v0, v3, :cond_2

    .line 2172543
    iget-object v0, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, LX/Er9;->t:LX/1OX;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2172544
    :cond_2
    iget-object v0, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    const v3, 0x7f0d0f2d

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Er9;->v:Landroid/widget/TextView;

    .line 2172545
    iget-object v0, p0, LX/Er9;->h:LX/Blb;

    sget-object v3, LX/Blb;->FACEBOOK:LX/Blb;

    if-ne v0, v3, :cond_3

    if-nez p3, :cond_4

    :cond_3
    iget-object v0, p0, LX/Er9;->h:LX/Blb;

    sget-object v3, LX/Blb;->CONTACTS:LX/Blb;

    if-ne v0, v3, :cond_5

    if-eqz p4, :cond_5

    .line 2172546
    :cond_4
    iget-object v0, p0, LX/Er9;->v:Landroid/widget/TextView;

    const v3, 0x7f080039

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 2172547
    :cond_5
    iget-object v0, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    const v3, 0x7f0d0f33

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Er9;->w:Landroid/view/View;

    .line 2172548
    invoke-static {p0}, LX/Er9;->d(LX/Er9;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2172549
    const/16 v1, 0x8

    .line 2172550
    iget-object v0, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2172551
    iget-object v0, p0, LX/Er9;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2172552
    iget-object v0, p0, LX/Er9;->w:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2172553
    :goto_0
    return-void

    .line 2172554
    :cond_6
    invoke-static {p0}, LX/Er9;->e(LX/Er9;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    invoke-static {p0, v0}, LX/Er9;->b(LX/Er9;Z)V

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method private static b(LX/Er9;I)LX/Eqw;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 2172512
    sget-object v0, LX/Er8;->a:[I

    iget-object v1, p0, LX/Er9;->h:LX/Blb;

    invoke-virtual {v1}, LX/Blb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2172513
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2172514
    iput-object v0, p0, LX/Er9;->p:LX/0Px;

    .line 2172515
    :goto_0
    new-instance v0, LX/Eqw;

    iget-object v1, p0, LX/Er9;->p:LX/0Px;

    invoke-direct {v0, v1}, LX/Eqw;-><init>(LX/0Px;)V

    return-object v0

    .line 2172516
    :pswitch_0
    iget-object v0, p0, LX/Er9;->y:LX/Eqy;

    iget-object v1, p0, LX/Er9;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081ecd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, LX/Er9;->s:LX/0Px;

    if-nez v3, :cond_0

    .line 2172517
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v4, v3

    .line 2172518
    :goto_1
    iget-object v5, p0, LX/Er9;->b:Ljava/util/Set;

    iget-object v6, p0, LX/Er9;->n:LX/EqG;

    sget-object v7, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    move v3, p1

    invoke-virtual/range {v0 .. v7}, LX/Eqy;->a(Ljava/lang/String;ZILX/0Px;Ljava/util/Set;LX/EqG;LX/ErX;)LX/Eqx;

    move-result-object v0

    iput-object v0, p0, LX/Er9;->m:LX/Eqx;

    .line 2172519
    iget-object v0, p0, LX/Er9;->y:LX/Eqy;

    iget-object v1, p0, LX/Er9;->r:LX/0Px;

    if-nez v1, :cond_1

    .line 2172520
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v4, v1

    .line 2172521
    :goto_2
    iget-object v5, p0, LX/Er9;->b:Ljava/util/Set;

    iget-object v6, p0, LX/Er9;->n:LX/EqG;

    sget-object v7, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    move-object v1, v9

    move v2, v8

    move v3, p1

    invoke-virtual/range {v0 .. v7}, LX/Eqy;->a(Ljava/lang/String;ZILX/0Px;Ljava/util/Set;LX/EqG;LX/ErX;)LX/Eqx;

    move-result-object v0

    iput-object v0, p0, LX/Er9;->l:LX/Eqx;

    .line 2172522
    iget-object v0, p0, LX/Er9;->m:LX/Eqx;

    iget-object v1, p0, LX/Er9;->l:LX/Eqx;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Er9;->p:LX/0Px;

    goto :goto_0

    .line 2172523
    :cond_0
    iget-object v4, p0, LX/Er9;->s:LX/0Px;

    goto :goto_1

    .line 2172524
    :cond_1
    iget-object v4, p0, LX/Er9;->r:LX/0Px;

    goto :goto_2

    .line 2172525
    :pswitch_1
    iget-object v0, p0, LX/Er9;->y:LX/Eqy;

    iget-object v1, p0, LX/Er9;->q:LX/0Px;

    if-nez v1, :cond_2

    .line 2172526
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v4, v1

    .line 2172527
    :goto_3
    iget-object v5, p0, LX/Er9;->b:Ljava/util/Set;

    iget-object v6, p0, LX/Er9;->n:LX/EqG;

    sget-object v7, LX/ErX;->CONTACTS:LX/ErX;

    move-object v1, v9

    move v2, v8

    move v3, p1

    invoke-virtual/range {v0 .. v7}, LX/Eqy;->a(Ljava/lang/String;ZILX/0Px;Ljava/util/Set;LX/EqG;LX/ErX;)LX/Eqx;

    move-result-object v0

    iput-object v0, p0, LX/Er9;->k:LX/Eqx;

    .line 2172528
    iget-object v0, p0, LX/Er9;->k:LX/Eqx;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Er9;->p:LX/0Px;

    goto :goto_0

    .line 2172529
    :cond_2
    iget-object v4, p0, LX/Er9;->q:LX/0Px;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(LX/Er9;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 2172507
    iget-object v3, p0, LX/Er9;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2172508
    iget-object v0, p0, LX/Er9;->v:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2172509
    iget-object v0, p0, LX/Er9;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2172510
    return-void

    :cond_1
    move v0, v2

    .line 2172511
    goto :goto_0
.end method

.method public static d(LX/Er9;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2172468
    sget-object v2, LX/Er8;->a:[I

    iget-object v3, p0, LX/Er9;->h:LX/Blb;

    invoke-virtual {v3}, LX/Blb;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 2172469
    :cond_0
    :goto_0
    return v0

    .line 2172470
    :pswitch_0
    iget-object v2, p0, LX/Er9;->r:LX/0Px;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2172471
    :pswitch_1
    iget-object v2, p0, LX/Er9;->q:LX/0Px;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static e(LX/Er9;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2172503
    sget-object v2, LX/Er8;->a:[I

    iget-object v3, p0, LX/Er9;->h:LX/Blb;

    invoke-virtual {v3}, LX/Blb;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 2172504
    :cond_0
    :goto_0
    return v0

    .line 2172505
    :pswitch_0
    iget-object v2, p0, LX/Er9;->r:LX/0Px;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/Er9;->r:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 2172506
    :pswitch_1
    iget-object v2, p0, LX/Er9;->q:LX/0Px;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/Er9;->q:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final c()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2172472
    iget-boolean v1, p0, LX/Er9;->f:Z

    if-nez v1, :cond_0

    .line 2172473
    :goto_0
    return-void

    .line 2172474
    :cond_0
    iget-object v1, p0, LX/Er9;->C:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/Er9;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 2172475
    :cond_1
    sget-object v1, LX/Er8;->a:[I

    iget-object v2, p0, LX/Er9;->h:LX/Blb;

    invoke-virtual {v2}, LX/Blb;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2172476
    if-nez v0, :cond_5

    .line 2172477
    iget-object v1, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    const v2, 0x7f0d0f37

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2172478
    :cond_2
    :goto_1
    goto :goto_0

    .line 2172479
    :pswitch_0
    const v1, 0x7f0d0192

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 2172480
    iget-object v2, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2172481
    if-nez v2, :cond_3

    .line 2172482
    iget-object v2, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    const v3, 0x7f0d0f39

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2172483
    invoke-virtual {v2, v1}, Landroid/view/View;->setId(I)V

    .line 2172484
    :cond_3
    if-nez v0, :cond_6

    .line 2172485
    iget-object v3, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    const v4, 0x7f0d0f3a

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2172486
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2172487
    :cond_4
    :goto_2
    goto :goto_0

    .line 2172488
    :cond_5
    iget-object v1, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    const v2, 0x7f0d0f37

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2172489
    invoke-virtual {v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_2

    .line 2172490
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2172491
    iget-object v2, p0, LX/Er9;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2172492
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f081eb2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f081eb3

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2172493
    new-instance v2, LX/Er6;

    invoke-direct {v2, p0}, LX/Er6;-><init>(LX/Er9;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2172494
    new-instance v2, LX/Er7;

    invoke-direct {v2, p0}, LX/Er7;-><init>(LX/Er9;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/16 :goto_1

    .line 2172495
    :cond_6
    iget-object v3, p0, LX/Er9;->x:Landroid/view/ViewGroup;

    const v4, 0x7f0d0f3a

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2172496
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2172497
    iget-boolean v2, p0, LX/Er9;->d:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, LX/Er9;->e:Z

    if-nez v2, :cond_4

    .line 2172498
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2172499
    const-string v3, "ci_flow"

    sget-object v4, LX/89v;->EVENTS_EXTENDED_INVITE:LX/89v;

    iget-object v4, v4, LX/89v;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2172500
    iget-object v3, p0, LX/Er9;->D:LX/0jo;

    sget-object v4, LX/0cQ;->FRIEND_FINDER_INTRO_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-interface {v3, v4}, LX/0jo;->a(I)LX/0jq;

    move-result-object v3

    invoke-interface {v3, v2}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2172501
    iget-object v3, p0, LX/Er9;->o:LX/0gc;

    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    const-string v4, "EVENTS_CONTACTS_FINDER_FRAGMENT_TAG"

    invoke-virtual {v3, v1, v2, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->b()I

    .line 2172502
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/Er9;->e:Z

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
