.class public LX/E89;
.super LX/Cfm;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/reaction/feed/rows/attachments/ReactionSinglePhotoAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/reaction/ui/attachment/handler/ReactionSinglePhotoHandler;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2082057
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->SINGLE_LARGE_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-direct {p0, v0}, LX/Cfm;-><init>(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V

    .line 2082058
    iput-object p2, p0, LX/E89;->a:LX/0Or;

    .line 2082059
    iput-object p1, p0, LX/E89;->b:LX/0Or;

    .line 2082060
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<",
            "Lcom/facebook/reaction/common/ReactionAttachmentNode;",
            "+",
            "LX/1PW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2082061
    iget-object v0, p0, LX/E89;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    return-object v0
.end method

.method public final c()LX/Cfk;
    .locals 1

    .prologue
    .line 2082062
    iget-object v0, p0, LX/E89;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfk;

    return-object v0
.end method
