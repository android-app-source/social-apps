.class public final LX/EPH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/CvV;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public j:I


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;LX/0Px;LX/CvV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;I)V
    .locals 0
    .param p2    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;",
            "LX/CvV;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2116373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2116374
    iput-object p1, p0, LX/EPH;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2116375
    iput-object p2, p0, LX/EPH;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2116376
    iput-object p3, p0, LX/EPH;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2116377
    iput-object p4, p0, LX/EPH;->d:LX/0Px;

    .line 2116378
    iput-object p5, p0, LX/EPH;->e:LX/CvV;

    .line 2116379
    iput-object p6, p0, LX/EPH;->f:Ljava/lang/String;

    .line 2116380
    iput-object p7, p0, LX/EPH;->g:Ljava/lang/String;

    .line 2116381
    iput-object p8, p0, LX/EPH;->h:Ljava/lang/String;

    .line 2116382
    iput-object p9, p0, LX/EPH;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2116383
    iput p10, p0, LX/EPH;->j:I

    .line 2116384
    return-void
.end method
