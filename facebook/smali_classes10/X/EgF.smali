.class public LX/EgF;
.super LX/3Ag;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public c:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

.field public d:LX/Efv;

.field public final e:Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2155975
    invoke-direct {p0, p1}, LX/3Ag;-><init>(Landroid/content/Context;)V

    .line 2155976
    new-instance v0, LX/EgD;

    invoke-direct {v0, p0}, LX/EgD;-><init>(LX/EgF;)V

    iput-object v0, p0, LX/EgF;->e:Landroid/content/DialogInterface$OnDismissListener;

    .line 2155977
    const/4 p1, 0x1

    const/4 v3, 0x0

    .line 2155978
    invoke-virtual {p0, p1}, LX/EgF;->requestWindowFeature(I)Z

    .line 2155979
    const v0, 0x7f03136f

    invoke-virtual {p0, v0}, LX/EgF;->setContentView(I)V

    .line 2155980
    invoke-virtual {p0}, LX/EgF;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2155981
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2155982
    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 2155983
    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 2155984
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2155985
    const v0, 0x7f0d2ceb

    invoke-virtual {p0, v0}, LX/EgF;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    iput-object v0, p0, LX/EgF;->c:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    .line 2155986
    invoke-virtual {p0}, LX/EgF;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08315c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2155987
    new-array v1, p1, [Ljava/lang/Object;

    invoke-static {p2}, LX/7hK;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2155988
    iget-object v1, p0, LX/EgF;->c:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-virtual {v1, v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->setReplyEditTextHint(Ljava/lang/String;)V

    .line 2155989
    iget-object v0, p0, LX/EgF;->e:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {p0, v0}, LX/EgF;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2155990
    iget-object v0, p0, LX/EgF;->c:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    new-instance v1, LX/EgE;

    invoke-direct {v1, p0}, LX/EgE;-><init>(LX/EgF;)V

    .line 2155991
    iput-object v1, v0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->g:LX/AL3;

    .line 2155992
    iget-object v0, p0, LX/EgF;->c:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-virtual {v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->a()V

    .line 2155993
    return-void
.end method
