.class public final LX/DMQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DMP;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/events/GroupEventsBaseFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V
    .locals 0

    .prologue
    .line 1989829
    iput-object p1, p0, LX/DMQ;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)V
    .locals 2

    .prologue
    .line 1989811
    invoke-static {p1, p2}, LX/7vF;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)LX/7vF;

    move-result-object v0

    .line 1989812
    iget-object v1, p0, LX/DMQ;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v1, v1, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    invoke-virtual {v1, v0, p3}, LX/DMe;->a(LX/7vF;Z)V

    .line 1989813
    iget-object v1, p0, LX/DMQ;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v1, v1, Lcom/facebook/groups/events/GroupEventsBaseFragment;->c:LX/DMZ;

    invoke-virtual {v1, v0, p2}, LX/DMZ;->a(LX/7vF;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    .line 1989814
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1989827
    iget-object v0, p0, LX/DMQ;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->j:LX/DMJ;

    const v1, 0x588d213b

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1989828
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 1

    .prologue
    .line 1989825
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/DMQ;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)V

    .line 1989826
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 3

    .prologue
    .line 1989821
    invoke-static {p1, p2}, LX/7vF;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/7vF;

    move-result-object v0

    .line 1989822
    iget-object v1, p0, LX/DMQ;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v1, v1, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/DMe;->a(LX/7vF;Z)V

    .line 1989823
    iget-object v1, p0, LX/DMQ;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v1, v1, Lcom/facebook/groups/events/GroupEventsBaseFragment;->c:LX/DMZ;

    invoke-virtual {v1, v0, p2}, LX/DMZ;->a(LX/7vF;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 1989824
    return-void
.end method

.method public final b(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 1

    .prologue
    .line 1989819
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/DMQ;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)V

    .line 1989820
    return-void
.end method

.method public final b(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 3

    .prologue
    .line 1989815
    invoke-static {p1, p2}, LX/7vF;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/7vF;

    move-result-object v0

    .line 1989816
    iget-object v1, p0, LX/DMQ;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v1, v1, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/DMe;->a(LX/7vF;Z)V

    .line 1989817
    iget-object v1, p0, LX/DMQ;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v1, v1, Lcom/facebook/groups/events/GroupEventsBaseFragment;->c:LX/DMZ;

    invoke-virtual {v1, v0, p2}, LX/DMZ;->a(LX/7vF;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 1989818
    return-void
.end method
