.class public abstract LX/EWY;
.super LX/EWX;
.source ""

# interfaces
.implements LX/EWW;
.implements LX/EWT;


# instance fields
.field private memoizedSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2130316
    invoke-direct {p0}, LX/EWX;-><init>()V

    .line 2130317
    const/4 v0, -0x1

    iput v0, p0, LX/EWY;->memoizedSize:I

    .line 2130318
    return-void
.end method

.method public static a(LX/EXG;)I
    .locals 1

    .prologue
    .line 2130315
    invoke-interface {p0}, LX/EXG;->getNumber()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(LX/EWf;)V
    .locals 6

    .prologue
    .line 2130300
    invoke-virtual {p0}, LX/EWY;->e()LX/EYF;

    move-result-object v0

    invoke-virtual {v0}, LX/EYF;->d()LX/EXf;

    move-result-object v0

    .line 2130301
    iget-boolean v1, v0, LX/EXf;->messageSetWireFormat_:Z

    move v2, v1

    .line 2130302
    invoke-virtual {p0}, LX/EWY;->kb_()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2130303
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    .line 2130304
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 2130305
    if-eqz v2, :cond_0

    invoke-virtual {v1}, LX/EYP;->q()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2130306
    iget-object v4, v1, LX/EYP;->g:LX/EYO;

    move-object v4, v4

    .line 2130307
    sget-object v5, LX/EYO;->MESSAGE:LX/EYO;

    if-ne v4, v5, :cond_0

    invoke-virtual {v1}, LX/EYP;->m()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2130308
    invoke-virtual {v1}, LX/EYP;->e()I

    move-result v1

    check-cast v0, LX/EWY;

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(ILX/EWW;)V

    goto :goto_0

    .line 2130309
    :cond_0
    invoke-static {v1, v0, p1}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;LX/EWf;)V

    goto :goto_0

    .line 2130310
    :cond_1
    invoke-virtual {p0}, LX/EWY;->g()LX/EZQ;

    move-result-object v0

    .line 2130311
    if-eqz v2, :cond_2

    .line 2130312
    invoke-virtual {v0, p1}, LX/EZQ;->b(LX/EWf;)V

    .line 2130313
    :goto_1
    return-void

    .line 2130314
    :cond_2
    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    goto :goto_1
.end method

.method public a()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2130286
    invoke-virtual {p0}, LX/EWY;->e()LX/EYF;

    move-result-object v0

    invoke-virtual {v0}, LX/EYF;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2130287
    invoke-virtual {v0}, LX/EYP;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2130288
    invoke-virtual {p0, v0}, LX/EWY;->a(LX/EYP;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 2130289
    :goto_0
    return v0

    .line 2130290
    :cond_1
    invoke-virtual {p0}, LX/EWY;->kb_()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2130291
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    .line 2130292
    invoke-virtual {v1}, LX/EYP;->f()LX/EYN;

    move-result-object v4

    sget-object v5, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v4, v5, :cond_2

    .line 2130293
    invoke-virtual {v1}, LX/EYP;->m()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2130294
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    .line 2130295
    invoke-interface {v0}, LX/EWQ;->a()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 2130296
    goto :goto_0

    .line 2130297
    :cond_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    invoke-interface {v0}, LX/EWQ;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 2130298
    goto :goto_0

    .line 2130299
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()I
    .locals 7

    .prologue
    .line 2130266
    iget v0, p0, LX/EWY;->memoizedSize:I

    .line 2130267
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2130268
    :goto_0
    return v0

    .line 2130269
    :cond_0
    const/4 v0, 0x0

    .line 2130270
    invoke-virtual {p0}, LX/EWY;->e()LX/EYF;

    move-result-object v1

    invoke-virtual {v1}, LX/EYF;->d()LX/EXf;

    move-result-object v1

    .line 2130271
    iget-boolean v2, v1, LX/EXf;->messageSetWireFormat_:Z

    move v3, v2

    .line 2130272
    invoke-virtual {p0}, LX/EWY;->kb_()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2130273
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    .line 2130274
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 2130275
    if-eqz v3, :cond_1

    invoke-virtual {v1}, LX/EYP;->q()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2130276
    iget-object v5, v1, LX/EYP;->g:LX/EYO;

    move-object v5, v5

    .line 2130277
    sget-object v6, LX/EYO;->MESSAGE:LX/EYO;

    if-ne v5, v6, :cond_1

    invoke-virtual {v1}, LX/EYP;->m()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2130278
    invoke-virtual {v1}, LX/EYP;->e()I

    move-result v1

    check-cast v0, LX/EWY;

    invoke-static {v1, v0}, LX/EWf;->f(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    goto :goto_1

    .line 2130279
    :cond_1
    invoke-static {v1, v0}, LX/EYc;->c(LX/EYP;Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    .line 2130280
    goto :goto_1

    .line 2130281
    :cond_2
    invoke-virtual {p0}, LX/EWY;->g()LX/EZQ;

    move-result-object v0

    .line 2130282
    if-eqz v3, :cond_3

    .line 2130283
    invoke-virtual {v0}, LX/EZQ;->g()I

    move-result v0

    add-int/2addr v0, v2

    .line 2130284
    :goto_2
    iput v0, p0, LX/EWY;->memoizedSize:I

    goto :goto_0

    .line 2130285
    :cond_3
    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    goto :goto_2
.end method

.method public final c()LX/EZL;
    .locals 1

    .prologue
    .line 2130319
    invoke-static {p0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2130258
    if-ne p1, p0, :cond_1

    .line 2130259
    :cond_0
    :goto_0
    return v0

    .line 2130260
    :cond_1
    instance-of v2, p1, LX/EWY;

    if-nez v2, :cond_2

    move v0, v1

    .line 2130261
    goto :goto_0

    .line 2130262
    :cond_2
    check-cast p1, LX/EWY;

    .line 2130263
    invoke-virtual {p0}, LX/EWY;->e()LX/EYF;

    move-result-object v2

    invoke-interface {p1}, LX/EWT;->e()LX/EYF;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2130264
    goto :goto_0

    .line 2130265
    :cond_3
    invoke-virtual {p0}, LX/EWY;->kb_()Ljava/util/Map;

    move-result-object v2

    invoke-interface {p1}, LX/EWT;->kb_()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, LX/EWY;->g()LX/EZQ;

    move-result-object v2

    invoke-interface {p1}, LX/EWT;->g()LX/EZQ;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/EZQ;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    .line 2130234
    invoke-virtual {p0}, LX/EWY;->e()LX/EYF;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x30b

    .line 2130235
    invoke-virtual {p0}, LX/EWY;->kb_()Ljava/util/Map;

    move-result-object v1

    .line 2130236
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 2130237
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EYP;

    .line 2130238
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    .line 2130239
    mul-int/lit8 v5, v0, 0x25

    invoke-virtual {v3}, LX/EYP;->e()I

    move-result v6

    add-int/2addr v5, v6

    .line 2130240
    iget-object v6, v3, LX/EYP;->g:LX/EYO;

    move-object v6, v6

    .line 2130241
    sget-object v7, LX/EYO;->ENUM:LX/EYO;

    if-eq v6, v7, :cond_0

    .line 2130242
    mul-int/lit8 v3, v5, 0x35

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int v0, v3, v2

    goto :goto_0

    .line 2130243
    :cond_0
    invoke-virtual {v3}, LX/EYP;->m()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2130244
    check-cast v2, Ljava/util/List;

    .line 2130245
    mul-int/lit8 v3, v5, 0x35

    .line 2130246
    const/4 v5, 0x1

    .line 2130247
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v6, v5

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/EXG;

    .line 2130248
    mul-int/lit8 v6, v6, 0x1f

    invoke-static {v5}, LX/EWY;->a(LX/EXG;)I

    move-result v5

    add-int/2addr v5, v6

    move v6, v5

    .line 2130249
    goto :goto_1

    .line 2130250
    :cond_1
    move v2, v6

    .line 2130251
    add-int v0, v3, v2

    .line 2130252
    goto :goto_0

    .line 2130253
    :cond_2
    mul-int/lit8 v3, v5, 0x35

    check-cast v2, LX/EXG;

    invoke-static {v2}, LX/EWY;->a(LX/EXG;)I

    move-result v2

    add-int v0, v3, v2

    .line 2130254
    goto :goto_0

    .line 2130255
    :cond_3
    move v0, v0

    .line 2130256
    mul-int/lit8 v0, v0, 0x1d

    invoke-virtual {p0}, LX/EWY;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2130257
    return v0
.end method

.method public abstract i()LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<+",
            "Lcom/google/protobuf/Message;",
            ">;"
        }
    .end annotation
.end method

.method public abstract s()LX/EWU;
.end method

.method public abstract t()LX/EWU;
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2130233
    invoke-static {p0}, LX/EZK;->a(LX/EWT;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
