.class public final enum LX/Emo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Emo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Emo;

.field public static final enum ACTION_BAR:LX/Emo;

.field public static final enum CONTEXT_ITEM:LX/Emo;

.field public static final enum COVER_PHOTO:LX/Emo;

.field public static final enum DEFAULT_ACTION:LX/Emo;

.field public static final enum EDIT_BIO:LX/Emo;

.field public static final enum EDIT_TAGS:LX/Emo;

.field public static final enum FEATURED_PHOTO:LX/Emo;

.field public static final enum HEADER:LX/Emo;

.field public static final enum PHOTO_PROTILE_FOOTER:LX/Emo;

.field public static final enum PHOTO_PROTILE_HEADER:LX/Emo;

.field public static final enum PHOTO_PROTILE_PHOTO:LX/Emo;

.field public static final enum PROFILE_PICTURE:LX/Emo;

.field public static final enum SEE_MORE:LX/Emo;

.field public static final enum TAG:LX/Emo;

.field public static final enum TYPEAHEAD:LX/Emo;

.field public static final enum UNKNOWN:LX/Emo;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2166178
    new-instance v0, LX/Emo;

    const-string v1, "HEADER"

    const-string v2, "header"

    invoke-direct {v0, v1, v4, v2}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->HEADER:LX/Emo;

    .line 2166179
    new-instance v0, LX/Emo;

    const-string v1, "PROFILE_PICTURE"

    const-string v2, "profile_picture"

    invoke-direct {v0, v1, v5, v2}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->PROFILE_PICTURE:LX/Emo;

    .line 2166180
    new-instance v0, LX/Emo;

    const-string v1, "COVER_PHOTO"

    const-string v2, "cover_photo"

    invoke-direct {v0, v1, v6, v2}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->COVER_PHOTO:LX/Emo;

    .line 2166181
    new-instance v0, LX/Emo;

    const-string v1, "ACTION_BAR"

    const-string v2, "action_bar"

    invoke-direct {v0, v1, v7, v2}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->ACTION_BAR:LX/Emo;

    .line 2166182
    new-instance v0, LX/Emo;

    const-string v1, "CONTEXT_ITEM"

    const-string v2, "context_item"

    invoke-direct {v0, v1, v8, v2}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->CONTEXT_ITEM:LX/Emo;

    .line 2166183
    new-instance v0, LX/Emo;

    const-string v1, "FEATURED_PHOTO"

    const/4 v2, 0x5

    const-string v3, "featured_photo"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->FEATURED_PHOTO:LX/Emo;

    .line 2166184
    new-instance v0, LX/Emo;

    const-string v1, "PHOTO_PROTILE_HEADER"

    const/4 v2, 0x6

    const-string v3, "photo_protile_header"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->PHOTO_PROTILE_HEADER:LX/Emo;

    .line 2166185
    new-instance v0, LX/Emo;

    const-string v1, "PHOTO_PROTILE_PHOTO"

    const/4 v2, 0x7

    const-string v3, "photo_protile_photo"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->PHOTO_PROTILE_PHOTO:LX/Emo;

    .line 2166186
    new-instance v0, LX/Emo;

    const-string v1, "PHOTO_PROTILE_FOOTER"

    const/16 v2, 0x8

    const-string v3, "photo_protile_footer"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->PHOTO_PROTILE_FOOTER:LX/Emo;

    .line 2166187
    new-instance v0, LX/Emo;

    const-string v1, "DEFAULT_ACTION"

    const/16 v2, 0x9

    const-string v3, "default_action"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->DEFAULT_ACTION:LX/Emo;

    .line 2166188
    new-instance v0, LX/Emo;

    const-string v1, "TYPEAHEAD"

    const/16 v2, 0xa

    const-string v3, "curation_tags_typeahead"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->TYPEAHEAD:LX/Emo;

    .line 2166189
    new-instance v0, LX/Emo;

    const-string v1, "TAG"

    const/16 v2, 0xb

    const-string v3, "curation_tag"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->TAG:LX/Emo;

    .line 2166190
    new-instance v0, LX/Emo;

    const-string v1, "SEE_MORE"

    const/16 v2, 0xc

    const-string v3, "curation_tags_more"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->SEE_MORE:LX/Emo;

    .line 2166191
    new-instance v0, LX/Emo;

    const-string v1, "EDIT_TAGS"

    const/16 v2, 0xd

    const-string v3, "curation_tags_edit"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->EDIT_TAGS:LX/Emo;

    .line 2166192
    new-instance v0, LX/Emo;

    const-string v1, "EDIT_BIO"

    const/16 v2, 0xe

    const-string v3, "edit_intro_card_bio"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->EDIT_BIO:LX/Emo;

    .line 2166193
    new-instance v0, LX/Emo;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xf

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/Emo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emo;->UNKNOWN:LX/Emo;

    .line 2166194
    const/16 v0, 0x10

    new-array v0, v0, [LX/Emo;

    sget-object v1, LX/Emo;->HEADER:LX/Emo;

    aput-object v1, v0, v4

    sget-object v1, LX/Emo;->PROFILE_PICTURE:LX/Emo;

    aput-object v1, v0, v5

    sget-object v1, LX/Emo;->COVER_PHOTO:LX/Emo;

    aput-object v1, v0, v6

    sget-object v1, LX/Emo;->ACTION_BAR:LX/Emo;

    aput-object v1, v0, v7

    sget-object v1, LX/Emo;->CONTEXT_ITEM:LX/Emo;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Emo;->FEATURED_PHOTO:LX/Emo;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Emo;->PHOTO_PROTILE_HEADER:LX/Emo;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Emo;->PHOTO_PROTILE_PHOTO:LX/Emo;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Emo;->PHOTO_PROTILE_FOOTER:LX/Emo;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Emo;->DEFAULT_ACTION:LX/Emo;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Emo;->TYPEAHEAD:LX/Emo;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Emo;->TAG:LX/Emo;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Emo;->SEE_MORE:LX/Emo;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Emo;->EDIT_TAGS:LX/Emo;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Emo;->EDIT_BIO:LX/Emo;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Emo;->UNKNOWN:LX/Emo;

    aput-object v2, v0, v1

    sput-object v0, LX/Emo;->$VALUES:[LX/Emo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2166195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2166196
    iput-object p3, p0, LX/Emo;->name:Ljava/lang/String;

    .line 2166197
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Emo;
    .locals 1

    .prologue
    .line 2166198
    const-class v0, LX/Emo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Emo;

    return-object v0
.end method

.method public static values()[LX/Emo;
    .locals 1

    .prologue
    .line 2166199
    sget-object v0, LX/Emo;->$VALUES:[LX/Emo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Emo;

    return-object v0
.end method
