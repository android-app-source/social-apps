.class public LX/DAU;
.super LX/3OP;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/model/threads/ThreadSummary;

.field public final b:LX/3OI;

.field private final c:Ljava/lang/String;

.field public final d:LX/DAS;

.field public final e:LX/Ddk;

.field private f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public s:J

.field private t:J

.field public u:I


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/3OI;Ljava/lang/String;LX/DAS;LX/Ddk;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1971297
    invoke-direct {p0}, LX/3OP;-><init>()V

    .line 1971298
    iput-boolean v0, p0, LX/DAU;->h:Z

    .line 1971299
    iput-boolean v0, p0, LX/DAU;->j:Z

    .line 1971300
    iput-boolean v0, p0, LX/DAU;->k:Z

    .line 1971301
    iput-boolean v0, p0, LX/DAU;->l:Z

    .line 1971302
    iput-boolean v0, p0, LX/DAU;->m:Z

    .line 1971303
    const/4 v0, 0x1

    iput v0, p0, LX/DAU;->u:I

    .line 1971304
    iput-object p1, p0, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 1971305
    iput-object p2, p0, LX/DAU;->b:LX/3OI;

    .line 1971306
    iput-object p3, p0, LX/DAU;->c:Ljava/lang/String;

    .line 1971307
    iput-object p4, p0, LX/DAU;->d:LX/DAS;

    .line 1971308
    iput-object p5, p0, LX/DAU;->e:LX/Ddk;

    .line 1971309
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/DAU;->q:Ljava/util/List;

    .line 1971310
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/DAU;->r:Ljava/util/List;

    .line 1971311
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/DAU;->t:J

    .line 1971312
    return-void
.end method


# virtual methods
.method public final a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "ARG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3L9",
            "<TT;TARG;>;TARG;)TT;"
        }
    .end annotation

    .prologue
    .line 1971296
    invoke-interface {p1, p0, p2}, LX/3L9;->a(LX/DAU;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1971294
    iput-boolean p1, p0, LX/DAU;->f:Z

    .line 1971295
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1971293
    iget-boolean v0, p0, LX/DAU;->f:Z

    return v0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1971291
    iput-boolean p1, p0, LX/DAU;->g:Z

    .line 1971292
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1971283
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 1971284
    :cond_0
    const/4 v0, 0x0

    .line 1971285
    :goto_0
    return v0

    .line 1971286
    :cond_1
    check-cast p1, LX/DAU;

    .line 1971287
    iget-object v0, p1, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v0, v0

    .line 1971288
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1971289
    iget-object v1, p0, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-object v1, v1

    .line 1971290
    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1971279
    iget-object v0, p0, LX/DAU;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->hashCode()I

    move-result v0

    return v0
.end method

.method public final p()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1971282
    iget-object v0, p0, LX/DAU;->q:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final r()J
    .locals 2

    .prologue
    .line 1971281
    iget-wide v0, p0, LX/DAU;->t:J

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1971280
    iget-object v0, p0, LX/DAU;->c:Ljava/lang/String;

    return-object v0
.end method
