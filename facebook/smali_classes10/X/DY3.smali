.class public final LX/DY3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

.field public final synthetic b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;)V
    .locals 0

    .prologue
    .line 2011141
    iput-object p1, p0, LX/DY3;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iput-object p2, p0, LX/DY3;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x6dce173a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2011131
    iget-object v1, p0, LX/DY3;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v1, v1, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->k:LX/DYU;

    iget-object v2, p0, LX/DY3;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

    .line 2011132
    iget-object v4, v1, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v4, v4, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->c:LX/DYT;

    iget-object v5, v1, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v5, v5, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->k:Ljava/lang/String;

    .line 2011133
    iget-object v6, v4, LX/DYT;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->a()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v7

    sget-object v8, LX/DYS;->MEMBER_REQUEST_IGNORED:LX/DYS;

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2011134
    new-instance v6, LX/4G0;

    invoke-direct {v6}, LX/4G0;-><init>()V

    iget-object v7, v4, LX/DYT;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/4G0;->a(Ljava/lang/String;)LX/4G0;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/4G0;->b(Ljava/lang/String;)LX/4G0;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->a()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4G0;->c(Ljava/lang/String;)LX/4G0;

    move-result-object v6

    const-string v7, "unknown"

    invoke-virtual {v6, v7}, LX/4G0;->d(Ljava/lang/String;)LX/4G0;

    move-result-object v6

    .line 2011135
    invoke-static {}, LX/DZ1;->b()LX/DZ0;

    move-result-object v7

    .line 2011136
    const-string v8, "input"

    invoke-virtual {v7, v8, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2011137
    iget-object v6, v4, LX/DYT;->d:LX/0tX;

    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2011138
    new-instance v7, LX/DYP;

    invoke-direct {v7, v4, v5, v2}, LX/DYP;-><init>(LX/DYT;Ljava/lang/String;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;)V

    iget-object v8, v4, LX/DYT;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2011139
    iget-object v4, v1, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v4, v4, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->g:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v5, v1, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v5, v5, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->j:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    iget-object v6, v1, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v6, v6, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->i:LX/0Px;

    iget-object v7, v1, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v7, v7, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->c:LX/DYT;

    const/4 v8, 0x0

    iget-object v9, v1, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-boolean v9, v9, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->h:Z

    invoke-virtual/range {v4 .. v9}, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->a(Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;LX/0Px;LX/DYT;ZZ)V

    .line 2011140
    const v1, -0x564a28cd

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
