.class public final LX/DmW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2039288
    iput-object p1, p0, LX/DmW;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iput-object p2, p0, LX/DmW;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x314dc95c

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2039289
    iget-object v0, p0, LX/DmW;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2039290
    iget-object v0, p0, LX/DmW;->a:Ljava/lang/String;

    invoke-static {v0}, LX/DkQ;->f(Ljava/lang/String;)LX/DkQ;

    move-result-object v3

    .line 2039291
    iget-object v0, p0, LX/DmW;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v4, p0, LX/DmW;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v0, v4}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2039292
    iget-object v3, p0, LX/DmW;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2039293
    const v0, -0x5ff7c6c1

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
