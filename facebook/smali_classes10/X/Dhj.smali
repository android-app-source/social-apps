.class public LX/Dhj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/Set;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Tn;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 1

    .prologue
    .line 2031322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2031323
    iput-object p1, p0, LX/Dhj;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2031324
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/Dhj;->c:LX/0Tn;

    .line 2031325
    return-void
.end method

.method private static declared-synchronized a(LX/Dhj;)V
    .locals 4

    .prologue
    .line 2031326
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Dhj;->a:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 2031327
    new-instance v0, Ljava/util/LinkedHashSet;

    .line 2031328
    iget-object v1, p0, LX/Dhj;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/Dhj;->c:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2031329
    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    move-object v1, v1

    .line 2031330
    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/Dhj;->a:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2031331
    :cond_0
    monitor-exit p0

    return-void

    .line 2031332
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/16 v2, 0x2c

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2031333
    monitor-enter p0

    if-nez p1, :cond_0

    .line 2031334
    const/4 v0, 0x0

    .line 2031335
    :goto_0
    monitor-exit p0

    return v0

    .line 2031336
    :cond_0
    :try_start_0
    invoke-static {p0}, LX/Dhj;->a(LX/Dhj;)V

    .line 2031337
    iget-object v0, p0, LX/Dhj;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    .line 2031338
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
