.class public final LX/Coy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/view/animation/Animation;

.field public final synthetic b:Landroid/view/animation/Animation;

.field public final synthetic c:Landroid/view/animation/Animation;

.field public final synthetic d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1936446
    iput-object p1, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iput-object p2, p0, LX/Coy;->a:Landroid/view/animation/Animation;

    iput-object p3, p0, LX/Coy;->b:Landroid/view/animation/Animation;

    iput-object p4, p0, LX/Coy;->c:Landroid/view/animation/Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x25d31a61

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1936447
    iget-object v1, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->g:LX/Cj9;

    iget-object v2, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v2, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->D:Ljava/lang/String;

    iget-object v3, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v3, v3, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->f:LX/Chi;

    .line 1936448
    iget-object v4, v3, LX/Chi;->c:Ljava/lang/String;

    move-object v3, v4

    .line 1936449
    iget-object v4, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v4, v4, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->F:Ljava/lang/String;

    iget-object v5, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v5, v5, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->E:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/Cj9;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1936450
    new-instance v2, LX/Cox;

    invoke-direct {v2, p0}, LX/Cox;-><init>(LX/Coy;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1936451
    iget-object v1, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->i:Landroid/widget/LinearLayout;

    iget-object v2, p0, LX/Coy;->a:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1936452
    iget-object v1, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->i:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1936453
    iget-object v1, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->j:Landroid/widget/LinearLayout;

    iget-object v2, p0, LX/Coy;->b:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1936454
    iget-object v1, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->x:Landroid/widget/LinearLayout;

    iget-object v2, p0, LX/Coy;->c:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1936455
    iget-object v1, p0, LX/Coy;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->j:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1936456
    const v1, -0x24885675

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
