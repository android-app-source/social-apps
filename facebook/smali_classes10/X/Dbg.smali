.class public final LX/Dbg;
.super LX/1L3;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;)V
    .locals 0

    .prologue
    .line 2017364
    iput-object p1, p0, LX/Dbg;->a:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    invoke-direct {p0}, LX/1L3;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2017365
    check-cast p1, LX/1ZY;

    .line 2017366
    iget-object v0, p0, LX/Dbg;->a:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    iget-object v0, v0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->n:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    iget-wide v2, p1, LX/1ZY;->c:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2017367
    iget-object v2, v0, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel;

    .line 2017368
    invoke-virtual {v2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2017369
    iget-object v3, v0, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->d:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2017370
    const v2, -0x173b8d7b

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2017371
    :cond_1
    return-void
.end method
