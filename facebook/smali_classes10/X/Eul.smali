.class public final enum LX/Eul;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Eul;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Eul;

.field public static final enum FRIENDS_CENTER_FRIENDS_TAB_IMPRESSION:LX/Eul;

.field public static final enum FRIENDS_CENTER_OPENED:LX/Eul;

.field public static final enum FRIENDS_CENTER_REQUESTS_TAB_IMPRESSION:LX/Eul;

.field public static final enum FRIENDS_CENTER_SEARCH_IMPRESSION:LX/Eul;

.field public static final enum FRIENDS_CENTER_SUGGESTIONS_TAB_IMPRESSION:LX/Eul;

.field public static final enum FRIENDS_CENTER_TAB_SELECTED:LX/Eul;

.field public static final enum FRIENDS_CENTER_TOTAL_SEARCHES:LX/Eul;


# instance fields
.field public final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2179962
    new-instance v0, LX/Eul;

    const-string v1, "FRIENDS_CENTER_OPENED"

    const-string v2, "friends_center_opened"

    invoke-direct {v0, v1, v4, v2}, LX/Eul;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eul;->FRIENDS_CENTER_OPENED:LX/Eul;

    .line 2179963
    new-instance v0, LX/Eul;

    const-string v1, "FRIENDS_CENTER_TAB_SELECTED"

    const-string v2, "friends_center_tab_selected"

    invoke-direct {v0, v1, v5, v2}, LX/Eul;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eul;->FRIENDS_CENTER_TAB_SELECTED:LX/Eul;

    .line 2179964
    new-instance v0, LX/Eul;

    const-string v1, "FRIENDS_CENTER_FRIENDS_TAB_IMPRESSION"

    const-string v2, "friends_center_friends_tab_impression"

    invoke-direct {v0, v1, v6, v2}, LX/Eul;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eul;->FRIENDS_CENTER_FRIENDS_TAB_IMPRESSION:LX/Eul;

    .line 2179965
    new-instance v0, LX/Eul;

    const-string v1, "FRIENDS_CENTER_SEARCH_IMPRESSION"

    const-string v2, "friends_center_search_impression"

    invoke-direct {v0, v1, v7, v2}, LX/Eul;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eul;->FRIENDS_CENTER_SEARCH_IMPRESSION:LX/Eul;

    .line 2179966
    new-instance v0, LX/Eul;

    const-string v1, "FRIENDS_CENTER_TOTAL_SEARCHES"

    const-string v2, "friends_center_total_searches"

    invoke-direct {v0, v1, v8, v2}, LX/Eul;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eul;->FRIENDS_CENTER_TOTAL_SEARCHES:LX/Eul;

    .line 2179967
    new-instance v0, LX/Eul;

    const-string v1, "FRIENDS_CENTER_SUGGESTIONS_TAB_IMPRESSION"

    const/4 v2, 0x5

    const-string v3, "friends_center_suggestions_tab_impression"

    invoke-direct {v0, v1, v2, v3}, LX/Eul;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eul;->FRIENDS_CENTER_SUGGESTIONS_TAB_IMPRESSION:LX/Eul;

    .line 2179968
    new-instance v0, LX/Eul;

    const-string v1, "FRIENDS_CENTER_REQUESTS_TAB_IMPRESSION"

    const/4 v2, 0x6

    const-string v3, "friends_center_requests_tab_impression"

    invoke-direct {v0, v1, v2, v3}, LX/Eul;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eul;->FRIENDS_CENTER_REQUESTS_TAB_IMPRESSION:LX/Eul;

    .line 2179969
    const/4 v0, 0x7

    new-array v0, v0, [LX/Eul;

    sget-object v1, LX/Eul;->FRIENDS_CENTER_OPENED:LX/Eul;

    aput-object v1, v0, v4

    sget-object v1, LX/Eul;->FRIENDS_CENTER_TAB_SELECTED:LX/Eul;

    aput-object v1, v0, v5

    sget-object v1, LX/Eul;->FRIENDS_CENTER_FRIENDS_TAB_IMPRESSION:LX/Eul;

    aput-object v1, v0, v6

    sget-object v1, LX/Eul;->FRIENDS_CENTER_SEARCH_IMPRESSION:LX/Eul;

    aput-object v1, v0, v7

    sget-object v1, LX/Eul;->FRIENDS_CENTER_TOTAL_SEARCHES:LX/Eul;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Eul;->FRIENDS_CENTER_SUGGESTIONS_TAB_IMPRESSION:LX/Eul;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Eul;->FRIENDS_CENTER_REQUESTS_TAB_IMPRESSION:LX/Eul;

    aput-object v2, v0, v1

    sput-object v0, LX/Eul;->$VALUES:[LX/Eul;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2179957
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2179958
    iput-object p3, p0, LX/Eul;->analyticsName:Ljava/lang/String;

    .line 2179959
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Eul;
    .locals 1

    .prologue
    .line 2179961
    const-class v0, LX/Eul;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Eul;

    return-object v0
.end method

.method public static values()[LX/Eul;
    .locals 1

    .prologue
    .line 2179960
    sget-object v0, LX/Eul;->$VALUES:[LX/Eul;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Eul;

    return-object v0
.end method
