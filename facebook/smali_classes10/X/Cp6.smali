.class public final LX/Cp6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/view/animation/Animation;

.field public final synthetic b:Landroid/view/animation/Animation;

.field public final synthetic c:Landroid/view/animation/Animation;

.field public final synthetic d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1936583
    iput-object p1, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iput-object p2, p0, LX/Cp6;->a:Landroid/view/animation/Animation;

    iput-object p3, p0, LX/Cp6;->b:Landroid/view/animation/Animation;

    iput-object p4, p0, LX/Cp6;->c:Landroid/view/animation/Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x1c6f8c3c

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1936584
    iget-object v1, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->g:LX/Cj9;

    iget-object v2, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v2, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->A:Ljava/lang/String;

    iget-object v3, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v3, v3, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->f:LX/Chi;

    .line 1936585
    iget-object v4, v3, LX/Chi;->c:Ljava/lang/String;

    move-object v3, v4

    .line 1936586
    iget-object v4, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v4, v4, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->C:Ljava/lang/String;

    iget-object v5, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v5, v5, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->B:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/Cj9;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1936587
    new-instance v2, LX/Cp5;

    invoke-direct {v2, p0}, LX/Cp5;-><init>(LX/Cp6;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1936588
    iget-object v1, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->i:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iget-object v2, p0, LX/Cp6;->a:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1936589
    iget-object v1, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->i:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 1936590
    iget-object v1, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->j:Landroid/widget/LinearLayout;

    iget-object v2, p0, LX/Cp6;->b:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1936591
    iget-object v1, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->u:Landroid/widget/LinearLayout;

    iget-object v2, p0, LX/Cp6;->c:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1936592
    iget-object v1, p0, LX/Cp6;->d:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->j:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1936593
    const v1, 0x6b0367e6

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
