.class public final LX/Eg4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Eg6;


# direct methods
.method public constructor <init>(LX/Eg6;)V
    .locals 0

    .prologue
    .line 2155732
    iput-object p1, p0, LX/Eg4;->a:LX/Eg6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x78df7a7f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2155716
    iget-object v0, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v0, v0, LX/Eg6;->r:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2155717
    const v0, 0x1eb4e939    # 1.91547E-20f

    invoke-static {v2, v2, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2155718
    :goto_0
    return-void

    .line 2155719
    :cond_0
    iget-object v0, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v0, v0, LX/Eg6;->o:LX/EgG;

    iget-object v1, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v1, v1, LX/Eg6;->r:LX/0Px;

    .line 2155720
    iput-object v1, v0, LX/EgG;->a:LX/0Px;

    .line 2155721
    iget-object v0, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v0, v0, LX/Eg6;->o:LX/EgG;

    const v1, -0x187a90d5

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2155722
    iget-object v0, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v0, v0, LX/Eg6;->p:LX/5ON;

    iget-object v1, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v1, v1, LX/Eg6;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2155723
    iget-object v0, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v0, v0, LX/Eg6;->m:LX/7ga;

    iget-object v1, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v1, v1, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155724
    iget-object v2, v1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v2

    .line 2155725
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Eg4;->a:LX/Eg6;

    invoke-virtual {v2}, LX/Eg6;->getCurrentThreadId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v3, v3, LX/Efo;->h:LX/Efj;

    .line 2155726
    iget v4, v3, LX/Efj;->b:I

    move v3, v4

    .line 2155727
    iget-object v4, p0, LX/Eg4;->a:LX/Eg6;

    invoke-virtual {v4}, LX/Eg6;->getCurrentThreadMediaType()LX/7gk;

    move-result-object v4

    sget-object v5, LX/7gZ;->SELF:LX/7gZ;

    iget-object v6, p0, LX/Eg4;->a:LX/Eg6;

    iget-object v6, v6, LX/Eg6;->r:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    .line 2155728
    invoke-static/range {v0 .. v5}, LX/7ga;->b(LX/7ga;Ljava/lang/String;Ljava/lang/String;ILX/7gk;LX/7gZ;)Landroid/os/Bundle;

    move-result-object v8

    .line 2155729
    sget-object v9, LX/7gY;->VIEWER_COUNT:LX/7gY;

    invoke-virtual {v9}, LX/7gY;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2155730
    sget-object v9, LX/7gX;->OPEN_SEEN_SUMMARY:LX/7gX;

    invoke-static {v0, v9, v8}, LX/7ga;->a(LX/7ga;LX/7gX;Landroid/os/Bundle;)V

    .line 2155731
    const v0, -0x46cb4b32

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_0
.end method
