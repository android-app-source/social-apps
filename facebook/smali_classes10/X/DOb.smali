.class public LX/DOb;
.super LX/1SX;
.source ""


# static fields
.field public static final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:LX/0Zb;

.field public final f:LX/DOl;

.field public final g:LX/B0n;

.field public final h:LX/1e0;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/00H;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/DOK;

.field private final k:Ljava/lang/String;

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/03V;

.field private final n:LX/1Kf;

.field public final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0ad;

.field private final q:LX/0Uh;

.field private final r:LX/0pf;

.field private final s:LX/88k;

.field private final t:LX/1Sp;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1992580
    const-class v0, LX/DOb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DOb;->b:Ljava/lang/String;

    .line 1992581
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/DOb;->c:LX/0Rf;

    .line 1992582
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_AD:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/DOb;->d:LX/0Rf;

    .line 1992583
    new-instance v0, LX/DOM;

    invoke-direct {v0}, LX/DOM;-><init>()V

    sput-object v0, LX/DOb;->a:LX/0Or;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0Or;LX/0SG;LX/DOl;LX/B0n;LX/1e0;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/DOK;LX/14w;LX/0ad;LX/0Or;LX/03V;LX/0Or;LX/1Qj;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;LX/0pf;LX/0Uh;LX/88k;)V
    .locals 42
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsfeedSpamReportingEnabled;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsGroupCommerceNewDeleteInterceptEnabled;
        .end annotation
    .end param
    .param p18    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p19    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p27    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .param p32    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .param p35    # LX/1Qj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/DOl;",
            "LX/B0n;",
            "LX/1e0;",
            "LX/0Ot",
            "<",
            "LX/00H;",
            ">;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/DOK;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/1Qj;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0wL;",
            "LX/0pf;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/88k;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992559
    sget-object v17, LX/DOb;->a:LX/0Or;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move-object/from16 v18, p15

    move-object/from16 v19, p16

    move-object/from16 v20, p17

    move-object/from16 v21, p18

    move-object/from16 v22, p20

    move-object/from16 v23, p21

    move-object/from16 v24, p26

    move-object/from16 v25, p27

    move-object/from16 v26, p28

    move-object/from16 v27, p30

    move-object/from16 v28, p31

    move-object/from16 v29, p34

    move-object/from16 v30, p35

    move-object/from16 v31, p36

    move-object/from16 v32, p37

    move-object/from16 v33, p38

    move-object/from16 v34, p39

    move-object/from16 v35, p40

    move-object/from16 v36, p41

    move-object/from16 v37, p42

    move-object/from16 v38, p43

    move-object/from16 v39, p44

    move-object/from16 v40, p45

    move-object/from16 v41, p46

    invoke-direct/range {v2 .. v41}, LX/1SX;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V

    .line 1992560
    new-instance v2, LX/DON;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/DON;-><init>(LX/DOb;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/DOb;->t:LX/1Sp;

    .line 1992561
    move-object/from16 v0, p47

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->r:LX/0pf;

    .line 1992562
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->o:LX/0Or;

    .line 1992563
    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->e:LX/0Zb;

    .line 1992564
    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->f:LX/DOl;

    .line 1992565
    move-object/from16 v0, p23

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->g:LX/B0n;

    .line 1992566
    move-object/from16 v0, p24

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->h:LX/1e0;

    .line 1992567
    move-object/from16 v0, p25

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->i:LX/0Ot;

    .line 1992568
    move-object/from16 v0, p29

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->j:LX/DOK;

    .line 1992569
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->n:LX/1Kf;

    .line 1992570
    invoke-interface/range {p19 .. p19}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, LX/DOb;->k:Ljava/lang/String;

    .line 1992571
    move-object/from16 v0, p32

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->l:LX/0Or;

    .line 1992572
    move-object/from16 v0, p33

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->m:LX/03V;

    .line 1992573
    move-object/from16 v0, p48

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->q:LX/0Uh;

    .line 1992574
    move-object/from16 v0, p49

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->s:LX/88k;

    .line 1992575
    const-string v2, "group_feed"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->b(Ljava/lang/String;)V

    .line 1992576
    const-string v2, "native_story_group"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(Ljava/lang/String;)V

    .line 1992577
    move-object/from16 v0, p0

    iget-object v2, v0, LX/DOb;->t:LX/1Sp;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(LX/1Sp;)V

    .line 1992578
    move-object/from16 v0, p31

    move-object/from16 v1, p0

    iput-object v0, v1, LX/DOb;->p:LX/0ad;

    .line 1992579
    return-void
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 8

    .prologue
    const/4 v3, 0x0

    const v1, -0x589999e3

    const v2, -0x7aa0fc74

    .line 1992549
    invoke-static {p0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v5

    .line 1992550
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1992551
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 1992552
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 1992553
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1992554
    :goto_1
    return v0

    .line 1992555
    :cond_0
    if-ne v0, v2, :cond_1

    move v0, v2

    .line 1992556
    goto :goto_1

    .line 1992557
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    move v0, v3

    .line 1992558
    goto :goto_1
.end method

.method public static e(LX/DOb;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 1992545
    const/4 v0, 0x0

    .line 1992546
    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1992547
    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/DOb;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1992548
    :cond_0
    return v0
.end method

.method public static g(LX/DOb;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 1992544
    invoke-static {p0, p1}, LX/DOb;->e(LX/DOb;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, LX/DOb;->o(LX/DOb;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1992542
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    const v0, 0x57490f63

    invoke-static {p0, v0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1992543
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(LX/DOb;Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1992538
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 1992539
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, LX/DOl;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    .line 1992540
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/DOb;->j:LX/DOK;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/DOb;->j:LX/DOK;

    invoke-virtual {v2, v1}, LX/DOK;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 1992541
    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 1992531
    invoke-virtual {p0, p1}, LX/1SX;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 1992532
    const-class v0, Landroid/app/Activity;

    invoke-static {p2, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1992533
    instance-of v1, v0, Lcom/facebook/base/activity/FbFragmentActivity;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 1992534
    check-cast v1, Lcom/facebook/base/activity/FbFragmentActivity;

    const-class v3, LX/CGW;

    invoke-virtual {v1, v3}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CGW;

    .line 1992535
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/CGW;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1992536
    :goto_0
    return-void

    .line 1992537
    :cond_0
    iget-object v1, p0, LX/DOb;->n:LX/1Kf;

    const/4 v3, 0x0

    const/16 v4, 0x6de

    invoke-interface {v1, v3, v2, v4, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public final d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 2

    .prologue
    .line 1992499
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1992500
    new-instance v0, LX/DOa;

    invoke-direct {v0, p0}, LX/DOa;-><init>(LX/DOb;)V

    .line 1992501
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, LX/1SX;->d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1992530
    const/4 v0, 0x1

    return v0
.end method

.method public final f()LX/6Vy;
    .locals 1

    .prologue
    .line 1992529
    sget-object v0, LX/6Vy;->GROUP_POST_CHEVRON:LX/6Vy;

    return-object v0
.end method

.method public j(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1992528
    invoke-virtual {p0, p1}, LX/DOb;->n(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    return v0
.end method

.method public l(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1992527
    const/4 v0, 0x0

    return v0
.end method

.method public final m(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1992506
    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1992507
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/DOb;->s:LX/88k;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->ac()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v0

    const/4 v5, 0x0

    .line 1992508
    if-nez v0, :cond_5

    move v4, v5

    .line 1992509
    :goto_0
    move v0, v4

    .line 1992510
    if-eqz v0, :cond_1

    .line 1992511
    :cond_0
    :goto_1
    return v2

    :cond_1
    move-object v0, p1

    .line 1992512
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/DOl;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1992513
    iget-object v3, p0, LX/DOb;->r:LX/0pf;

    invoke-virtual {v3, p1}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v3

    .line 1992514
    iget-object v4, v3, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v3, v4

    .line 1992515
    if-eqz v0, :cond_3

    iget-object v4, p0, LX/DOb;->j:LX/DOK;

    if-eqz v4, :cond_3

    iget-object v4, p0, LX/DOb;->j:LX/DOK;

    invoke-virtual {v4, v0}, LX/DOK;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v3, :cond_2

    .line 1992516
    iget-boolean v0, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v0

    .line 1992517
    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 1992518
    :goto_2
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    const v3, 0x51d811e3

    invoke-static {p1, v3}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    if-eqz v3, :cond_4

    move v3, v1

    .line 1992519
    :goto_3
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v2

    .line 1992520
    goto :goto_2

    :cond_4
    move v3, v2

    .line 1992521
    goto :goto_3

    .line 1992522
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_4
    if-ge v6, v8, :cond_8

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLConfiguration;

    .line 1992523
    if-eqz v4, :cond_7

    const-string v9, "gk_joinable_group_chats"

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLConfiguration;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1992524
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLConfiguration;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, v3, LX/88k;->a:LX/0Uh;

    const/16 v6, 0x3c3

    invoke-virtual {v4, v6, v5}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    goto :goto_0

    :cond_6
    move v4, v5

    goto :goto_0

    .line 1992525
    :cond_7
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_4

    :cond_8
    move v4, v5

    .line 1992526
    goto :goto_0
.end method

.method public final n(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1992502
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 1992503
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, LX/DOl;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    .line 1992504
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/DOb;->j:LX/DOK;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/DOb;->j:LX/DOK;

    invoke-virtual {v2, v1}, LX/DOK;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 1992505
    :cond_0
    return v0
.end method
