.class public final LX/DOy;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/DOy;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DOw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/DOz;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1992886
    const/4 v0, 0x0

    sput-object v0, LX/DOy;->a:LX/DOy;

    .line 1992887
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DOy;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1992888
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1992889
    new-instance v0, LX/DOz;

    invoke-direct {v0}, LX/DOz;-><init>()V

    iput-object v0, p0, LX/DOy;->c:LX/DOz;

    .line 1992890
    return-void
.end method

.method public static c(LX/1De;)LX/DOw;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1992891
    new-instance v1, LX/DOx;

    invoke-direct {v1}, LX/DOx;-><init>()V

    .line 1992892
    sget-object v2, LX/DOy;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DOw;

    .line 1992893
    if-nez v2, :cond_0

    .line 1992894
    new-instance v2, LX/DOw;

    invoke-direct {v2}, LX/DOw;-><init>()V

    .line 1992895
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/DOw;->a$redex0(LX/DOw;LX/1De;IILX/DOx;)V

    .line 1992896
    move-object v1, v2

    .line 1992897
    move-object v0, v1

    .line 1992898
    return-object v0
.end method

.method public static declared-synchronized q()LX/DOy;
    .locals 2

    .prologue
    .line 1992901
    const-class v1, LX/DOy;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/DOy;->a:LX/DOy;

    if-nez v0, :cond_0

    .line 1992902
    new-instance v0, LX/DOy;

    invoke-direct {v0}, LX/DOy;-><init>()V

    sput-object v0, LX/DOy;->a:LX/DOy;

    .line 1992903
    :cond_0
    sget-object v0, LX/DOy;->a:LX/DOy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1992904
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1992899
    invoke-static {}, LX/1dS;->b()V

    .line 1992900
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x637fb613

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1992883
    const/4 v1, 0x0

    iput v1, p5, LX/1no;->a:I

    .line 1992884
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0dc4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p5, LX/1no;->b:I

    .line 1992885
    const/16 v1, 0x1f

    const v2, -0x116b905a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1992881
    new-instance v0, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    invoke-direct {v0, p1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1992882
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1992874
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1992875
    check-cast p3, LX/DOx;

    .line 1992876
    check-cast p2, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    iget-object v0, p3, LX/DOx;->a:Ljava/lang/String;

    .line 1992877
    invoke-virtual {p2, v0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1992878
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1992879
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1992880
    const/16 v0, 0xf

    return v0
.end method
