.class public LX/Cys;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/D0P;",
            "LX/0Ot",
            "<+",
            "LX/Cyq;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Cyr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cyu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1953836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1953837
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Cys;->a:Ljava/util/Map;

    .line 1953838
    iget-object v0, p0, LX/Cys;->a:Ljava/util/Map;

    sget-object v1, LX/D0P;->ELECTIONS:LX/D0P;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953839
    iget-object v0, p0, LX/Cys;->a:Ljava/util/Map;

    sget-object v1, LX/D0P;->PHOTO_SNAPSHOT:LX/D0P;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953840
    return-void
.end method

.method public static a(LX/0QB;)LX/Cys;
    .locals 5

    .prologue
    .line 1953841
    const-class v1, LX/Cys;

    monitor-enter v1

    .line 1953842
    :try_start_0
    sget-object v0, LX/Cys;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1953843
    sput-object v2, LX/Cys;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1953844
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1953845
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1953846
    new-instance v3, LX/Cys;

    const/16 v4, 0x3365

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x3367

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/Cys;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1953847
    move-object v0, v3

    .line 1953848
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1953849
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Cys;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1953850
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1953851
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/D0P;LX/0gW;LX/Cwy;LX/Cx0;)V
    .locals 1
    .param p4    # LX/Cx0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/D0P;",
            "LX/0gW",
            "<TS;>;",
            "LX/Cwy;",
            "LX/Cx0;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1953852
    iget-object v0, p0, LX/Cys;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1953853
    :goto_0
    return-void

    .line 1953854
    :cond_0
    iget-object v0, p0, LX/Cys;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyq;

    .line 1953855
    invoke-interface {v0, p3}, LX/Cyq;->a(LX/Cwy;)V

    .line 1953856
    if-eqz p4, :cond_1

    .line 1953857
    invoke-interface {v0, p4}, LX/Cyq;->a(LX/Cx0;)V

    .line 1953858
    :cond_1
    invoke-interface {v0, p2}, LX/Cyq;->a(LX/0gW;)V

    goto :goto_0
.end method
