.class public LX/DJu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DJs;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/DJu;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1985955
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DJu;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1985956
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1985957
    iput-object p1, p0, LX/DJu;->b:LX/0Ot;

    .line 1985958
    return-void
.end method

.method public static a(LX/0QB;)LX/DJu;
    .locals 4

    .prologue
    .line 1985959
    sget-object v0, LX/DJu;->c:LX/DJu;

    if-nez v0, :cond_1

    .line 1985960
    const-class v1, LX/DJu;

    monitor-enter v1

    .line 1985961
    :try_start_0
    sget-object v0, LX/DJu;->c:LX/DJu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1985962
    if-eqz v2, :cond_0

    .line 1985963
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1985964
    new-instance v3, LX/DJu;

    const/16 p0, 0x236d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DJu;-><init>(LX/0Ot;)V

    .line 1985965
    move-object v0, v3

    .line 1985966
    sput-object v0, LX/DJu;->c:LX/DJu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1985967
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1985968
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1985969
    :cond_1
    sget-object v0, LX/DJu;->c:LX/DJu;

    return-object v0

    .line 1985970
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1985971
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1985972
    check-cast p2, LX/DJt;

    .line 1985973
    iget-object v0, p0, LX/DJu;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;

    iget-object v2, p2, LX/DJt;->a:Ljava/lang/String;

    iget-object v3, p2, LX/DJt;->b:Ljava/lang/Long;

    iget-object v4, p2, LX/DJt;->c:Ljava/lang/String;

    iget-object v5, p2, LX/DJt;->d:Ljava/lang/String;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->a(LX/1De;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)LX/1Dg;

    move-result-object v0

    .line 1985974
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1985975
    invoke-static {}, LX/1dS;->b()V

    .line 1985976
    const/4 v0, 0x0

    return-object v0
.end method
