.class public LX/EuY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/0TD;

.field private final c:LX/0ad;

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dl;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field public f:I


# direct methods
.method public constructor <init>(LX/0TD;LX/0ad;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2179681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179682
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179683
    iput-object v0, p0, LX/EuY;->d:LX/0Ot;

    .line 2179684
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179685
    iput-object v0, p0, LX/EuY;->e:LX/0Ot;

    .line 2179686
    const/4 v0, 0x0

    iput v0, p0, LX/EuY;->f:I

    .line 2179687
    iput-object p1, p0, LX/EuY;->b:LX/0TD;

    .line 2179688
    iput-object p2, p0, LX/EuY;->c:LX/0ad;

    .line 2179689
    invoke-static {}, LX/EuY;->d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/EuY;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179690
    return-void
.end method

.method public static a(LX/0QB;)LX/EuY;
    .locals 3

    .prologue
    .line 2179715
    new-instance v2, LX/EuY;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, LX/0TD;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/EuY;-><init>(LX/0TD;LX/0ad;)V

    .line 2179716
    const/16 v0, 0xa73

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    const/16 v1, 0xafc

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2179717
    iput-object v0, v2, LX/EuY;->d:LX/0Ot;

    iput-object v1, v2, LX/EuY;->e:LX/0Ot;

    .line 2179718
    move-object v0, v2

    .line 2179719
    return-object v0
.end method

.method public static a(LX/EuY;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2179701
    new-instance v0, LX/EuX;

    invoke-direct {v0, p0}, LX/EuX;-><init>(LX/EuY;)V

    iget-object v1, p0, LX/EuY;->b:LX/0TD;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/EuY;Lcom/facebook/common/callercontext/CallerContext;I)LX/0zO;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I)",
            "LX/0zO",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179702
    const-string v0, "You must provide a caller context"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2179703
    iget-object v0, p0, LX/EuY;->c:LX/0ad;

    sget-short v1, LX/2hr;->w:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2179704
    new-instance v1, LX/EvT;

    invoke-direct {v1}, LX/EvT;-><init>()V

    move-object v1, v1

    .line 2179705
    const-string v2, "after_param"

    iget-object v3, p0, LX/EuY;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "first_param"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "should_fetch_is_seen"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2179706
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2179707
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    const-string v2, "FC_REQUESTS_QUERY"

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    .line 2179708
    iput-object v2, v1, LX/0zO;->d:Ljava/util/Set;

    .line 2179709
    move-object v1, v1

    .line 2179710
    iput-object p1, v1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2179711
    move-object v1, v1

    .line 2179712
    const/4 v2, 0x1

    .line 2179713
    iput-boolean v2, v1, LX/0zO;->p:Z

    .line 2179714
    return-object v0
.end method

.method private static d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 2

    .prologue
    .line 2179697
    new-instance v0, LX/4a7;

    invoke-direct {v0}, LX/4a7;-><init>()V

    const/4 v1, 0x1

    .line 2179698
    iput-boolean v1, v0, LX/4a7;->b:Z

    .line 2179699
    move-object v0, v0

    .line 2179700
    invoke-virtual {v0}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2179696
    iget-object v0, p0, LX/EuY;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2179691
    invoke-static {}, LX/EuY;->d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/EuY;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179692
    iget v0, p0, LX/EuY;->f:I

    if-lez v0, :cond_0

    .line 2179693
    iget-object v0, p0, LX/EuY;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->b()V

    .line 2179694
    const/4 v0, 0x0

    iput v0, p0, LX/EuY;->f:I

    .line 2179695
    :cond_0
    return-void
.end method
