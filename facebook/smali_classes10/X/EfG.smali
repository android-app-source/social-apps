.class public final LX/EfG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AL3;


# instance fields
.field public final synthetic a:LX/EfL;


# direct methods
.method public constructor <init>(LX/EfL;)V
    .locals 0

    .prologue
    .line 2154090
    iput-object p1, p0, LX/EfG;->a:LX/EfL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2154091
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/audience/direct/ui/BackstageReplyThreadView$6$1;

    invoke-direct {v1, p0}, Lcom/facebook/audience/direct/ui/BackstageReplyThreadView$6$1;-><init>(LX/EfG;)V

    const v2, -0x7359a9b3

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2154092
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 2154093
    iget-object v0, p0, LX/EfG;->a:LX/EfL;

    iget-object v1, p0, LX/EfG;->a:LX/EfL;

    iget-object v1, v1, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    .line 2154094
    iget-object v2, v0, LX/EfL;->k:LX/0he;

    .line 2154095
    iget-object v3, v1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v3, v3

    .line 2154096
    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v3

    .line 2154097
    iget-object v4, v1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2154098
    iget-object v5, v1, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v5, v5

    .line 2154099
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, LX/0he;->b(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2154100
    iget-object v2, v0, LX/EfL;->h:LX/1EX;

    invoke-virtual {v0}, LX/EfL;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2154101
    iget-object v6, v2, LX/1EX;->e:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    .line 2154102
    iget-object v8, v1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v8, v8

    .line 2154103
    iget-object v9, v8, Lcom/facebook/audience/model/Reply;->g:Ljava/lang/String;

    move-object v8, v9

    .line 2154104
    iget-object v9, v1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v9, v9

    .line 2154105
    invoke-static {}, Lcom/facebook/audience/model/UploadShot;->newBuilder()Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v10

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/facebook/audience/model/UploadShot$Builder;->setPath(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/facebook/audience/model/UploadShot$Builder;->setCaption(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v10

    sget-object v11, LX/7gi;->PHOTO:LX/7gi;

    invoke-virtual {v10, v11}, Lcom/facebook/audience/model/UploadShot$Builder;->setMediaType(LX/7gi;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Lcom/facebook/audience/model/UploadShot$Builder;->setCreatedAtTime(J)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v10

    invoke-virtual {v10, v8}, Lcom/facebook/audience/model/UploadShot$Builder;->setReactionId(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v10

    invoke-virtual {v10, v9}, Lcom/facebook/audience/model/UploadShot$Builder;->setReplyThreadId(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/facebook/audience/model/UploadShot$Builder;->setMessage(Ljava/lang/String;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v10

    sget-object v11, LX/7h9;->TEXT:LX/7h9;

    invoke-virtual {v10, v11}, Lcom/facebook/audience/model/UploadShot$Builder;->setBackstagePostType(LX/7h9;)Lcom/facebook/audience/model/UploadShot$Builder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/audience/model/UploadShot$Builder;->a()Lcom/facebook/audience/model/UploadShot;

    move-result-object v10

    move-object v6, v10

    .line 2154106
    iget-object v7, v2, LX/1EX;->j:LX/0gK;

    invoke-virtual {v7}, LX/0gK;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2154107
    const/4 v7, 0x1

    new-array v7, v7, [Lcom/facebook/audience/model/UploadShot;

    const/4 v8, 0x0

    aput-object v6, v7, v8

    invoke-static {v2, v3, v7}, LX/1EX;->b(LX/1EX;Landroid/content/Context;[Lcom/facebook/audience/model/UploadShot;)V

    .line 2154108
    :goto_0
    iget-object v0, p0, LX/EfG;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->a:LX/1Eo;

    sget-object v1, LX/7gQ;->SUBMITTED_REPLY:LX/7gQ;

    invoke-virtual {v0, v1}, LX/1Eo;->a(LX/7gQ;)V

    .line 2154109
    return-void

    .line 2154110
    :cond_0
    iget-object v7, v2, LX/1EX;->f:LX/1Em;

    const v8, -0xffff01

    const/4 v9, 0x2

    invoke-virtual {v7, p1, v8, v9}, LX/1Em;->a(Ljava/lang/String;II)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    new-instance v8, LX/BaF;

    invoke-direct {v8, v2, v6, v3}, LX/BaF;-><init>(LX/1EX;Lcom/facebook/audience/model/UploadShot;Landroid/content/Context;)V

    iget-object v6, v2, LX/1EX;->g:Ljava/util/concurrent/Executor;

    invoke-static {v7, v8, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
