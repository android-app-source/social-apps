.class public LX/DL8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Ck;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7zS;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Boolean;

.field private final f:LX/0SI;

.field public final g:LX/0tX;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/lang/String;

.field private final j:Landroid/content/Context;

.field public final k:LX/DLN;

.field public final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/DL6;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/7yp;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/1Ck;LX/0Ot;LX/0SI;Ljava/lang/Boolean;LX/0tX;LX/0Ot;Ljava/lang/String;Landroid/content/Context;LX/DLN;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/DLN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/7zS;",
            ">;",
            "LX/0SI;",
            "Ljava/lang/Boolean;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$GroupsDocsAndFilesUploadControllerListener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1987938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1987939
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/DL8;->l:Ljava/util/Map;

    .line 1987940
    iput-object p1, p0, LX/DL8;->a:Ljava/util/concurrent/ExecutorService;

    .line 1987941
    iput-object p2, p0, LX/DL8;->b:LX/0Ot;

    .line 1987942
    iput-object p3, p0, LX/DL8;->c:LX/1Ck;

    .line 1987943
    iput-object p4, p0, LX/DL8;->d:LX/0Ot;

    .line 1987944
    iput-object p6, p0, LX/DL8;->e:Ljava/lang/Boolean;

    .line 1987945
    iput-object p5, p0, LX/DL8;->f:LX/0SI;

    .line 1987946
    iput-object p7, p0, LX/DL8;->g:LX/0tX;

    .line 1987947
    iput-object p8, p0, LX/DL8;->h:LX/0Ot;

    .line 1987948
    iput-object p9, p0, LX/DL8;->i:Ljava/lang/String;

    .line 1987949
    iput-object p10, p0, LX/DL8;->j:Landroid/content/Context;

    .line 1987950
    iput-object p11, p0, LX/DL8;->k:LX/DLN;

    .line 1987951
    return-void
.end method

.method private a(LX/4JX;Ljava/io/File;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1987906
    iget-object v0, p0, LX/DL8;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "TABLET"

    .line 1987907
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1987908
    new-instance v2, LX/4D3;

    invoke-direct {v2}, LX/4D3;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1987909
    const-string v4, "name"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1987910
    move-object v2, v2

    .line 1987911
    const-string v3, "url"

    invoke-virtual {v2, v3, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1987912
    move-object v2, v2

    .line 1987913
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1987914
    new-instance v2, LX/4D2;

    invoke-direct {v2}, LX/4D2;-><init>()V

    .line 1987915
    const-string v3, "files"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1987916
    move-object v1, v2

    .line 1987917
    const-string v2, "source"

    invoke-virtual {p1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1987918
    move-object v0, p1

    .line 1987919
    iget-object v2, p0, LX/DL8;->f:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 1987920
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1987921
    const-string v3, "actor_id"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1987922
    move-object v0, v0

    .line 1987923
    new-instance v2, LX/4JW;

    invoke-direct {v2}, LX/4JW;-><init>()V

    iget-object v3, p0, LX/DL8;->i:Ljava/lang/String;

    .line 1987924
    const-string v4, "to_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1987925
    move-object v2, v2

    .line 1987926
    const-string v3, "audience"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1987927
    move-object v0, v0

    .line 1987928
    new-instance v2, LX/4Jk;

    invoke-direct {v2}, LX/4Jk;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, LX/4Jk;->a(Ljava/lang/String;)LX/4Jk;

    move-result-object v2

    .line 1987929
    const-string v3, "message"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1987930
    move-object v0, v0

    .line 1987931
    const/4 v2, 0x1

    new-array v2, v2, [LX/4JV;

    const/4 v3, 0x0

    new-instance v4, LX/4JV;

    invoke-direct {v4}, LX/4JV;-><init>()V

    .line 1987932
    const-string p0, "file"

    invoke-virtual {v4, p0, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1987933
    move-object v1, v4

    .line 1987934
    aput-object v1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 1987935
    const-string v2, "attachments"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1987936
    return-void

    .line 1987937
    :cond_0
    const-string v0, "MOBILE"

    goto :goto_0
.end method

.method public static a(LX/DL8;III)V
    .locals 3

    .prologue
    .line 1987900
    new-instance v1, LX/0ju;

    iget-object v0, p0, LX/DL8;->j:Landroid/content/Context;

    invoke-direct {v1, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1987901
    iget-object v0, p0, LX/DL8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1987902
    iget-object v0, p0, LX/DL8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1987903
    iget-object v0, p0, LX/DL8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1987904
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1987905
    return-void
.end method

.method private static a(LX/DL8;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/DL7;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1987898
    iget-object v0, p0, LX/DL8;->c:LX/1Ck;

    sget-object v1, LX/DL4;->UPLOAD_GROUP_FILE_BODY:LX/DL4;

    new-instance v2, LX/DL0;

    invoke-direct {v2, p0}, LX/DL0;-><init>(LX/DL8;)V

    invoke-virtual {v0, v1, p1, v2}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1987899
    return-void
.end method

.method public static a$redex0(LX/DL8;Ljava/io/File;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/DL7;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1987888
    invoke-static {p1}, LX/DL8;->d(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 1987889
    iget-object v1, p0, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1987890
    iget-object v1, p0, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DL6;

    .line 1987891
    iput-object p2, v1, LX/DL6;->c:Ljava/lang/String;

    .line 1987892
    :cond_0
    new-instance v0, LX/4JX;

    invoke-direct {v0}, LX/4JX;-><init>()V

    .line 1987893
    invoke-direct {p0, v0, p1, p2}, LX/DL8;->a(LX/4JX;Ljava/io/File;Ljava/lang/String;)V

    .line 1987894
    new-instance v1, LX/DLd;

    invoke-direct {v1}, LX/DLd;-><init>()V

    move-object v1, v1

    .line 1987895
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/DLd;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1987896
    iget-object v2, p0, LX/DL8;->c:LX/1Ck;

    sget-object v3, LX/DL4;->UPLOAD_GROUP_FILE_HANDLE:LX/DL4;

    iget-object p2, p0, LX/DL8;->g:LX/0tX;

    invoke-virtual {p2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance p2, LX/DL1;

    invoke-direct {p2, p0, p3, p1}, LX/DL1;-><init>(LX/DL8;Lcom/google/common/util/concurrent/SettableFuture;Ljava/io/File;)V

    invoke-virtual {v2, v3, v1, p2}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1987897
    return-void
.end method

.method public static a$redex0(LX/DL8;Ljava/lang/String;LX/7z0;)V
    .locals 1

    .prologue
    .line 1987952
    iget-object v0, p0, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1987953
    iget-object v0, p0, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DL6;

    .line 1987954
    iput-object p2, v0, LX/DL6;->d:LX/7z0;

    .line 1987955
    :cond_0
    return-void
.end method

.method public static b(Ljava/io/File;Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;)LX/DL7;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 1987856
    new-instance v1, LX/DLr;

    invoke-direct {v1}, LX/DLr;-><init>()V

    .line 1987857
    invoke-virtual {p1}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/DLr;->c:Ljava/lang/String;

    .line 1987858
    invoke-virtual {p1}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1987859
    invoke-virtual {p1}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;

    .line 1987860
    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/DLr;->d:Ljava/lang/String;

    .line 1987861
    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1987862
    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/DLr;->b:Ljava/lang/String;

    .line 1987863
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1987864
    new-instance v2, LX/DLu;

    invoke-direct {v2}, LX/DLu;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$ActorsModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1987865
    iput-object v0, v2, LX/DLu;->c:Ljava/lang/String;

    .line 1987866
    move-object v0, v2

    .line 1987867
    invoke-virtual {v0}, LX/DLu;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OwnerModel;

    move-result-object v0

    iput-object v0, v1, LX/DLr;->f:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OwnerModel;

    .line 1987868
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;->l()J

    move-result-wide v2

    iput-wide v2, v1, LX/DLr;->a:J

    .line 1987869
    new-instance v0, LX/DLs;

    invoke-direct {v0}, LX/DLs;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 1987870
    iput-object v2, v0, LX/DLs;->a:Ljava/lang/String;

    .line 1987871
    move-object v0, v0

    .line 1987872
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 1987873
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1987874
    iget-object v5, v0, LX/DLs;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1987875
    iget-object v7, v0, LX/DLs;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1987876
    const/4 v9, 0x2

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 1987877
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 1987878
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1987879
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1987880
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1987881
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1987882
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1987883
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1987884
    new-instance v5, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OriginalPostModel;

    invoke-direct {v5, v4}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OriginalPostModel;-><init>(LX/15i;)V

    .line 1987885
    move-object v0, v5

    .line 1987886
    iput-object v0, v1, LX/DLr;->e:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OriginalPostModel;

    .line 1987887
    new-instance v0, LX/DL7;

    invoke-virtual {v1}, LX/DLr;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    move-result-object v1

    invoke-static {p0}, LX/DL8;->d(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/DL7;-><init>(Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Ljava/io/File;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1987855
    invoke-virtual {p0}, Ljava/io/File;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static f(LX/DL8;Ljava/io/File;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/DL7;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1987852
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 1987853
    iget-object v1, p0, LX/DL8;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;-><init>(LX/DL8;Ljava/io/File;Lcom/google/common/util/concurrent/SettableFuture;)V

    const v3, -0x2d5f82a9

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1987854
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/io/File;)V
    .locals 8

    .prologue
    .line 1987817
    const v7, 0x104000a

    const/4 v1, 0x1

    .line 1987818
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/32 v5, 0x1900000

    cmp-long v2, v3, v5

    if-lez v2, :cond_2

    .line 1987819
    const v2, 0x7f0830a5

    const v3, 0x7f0830a4

    invoke-static {p0, v2, v3, v7}, LX/DL8;->a(LX/DL8;III)V

    .line 1987820
    :goto_0
    move v0, v1

    .line 1987821
    if-eqz v0, :cond_0

    .line 1987822
    :goto_1
    return-void

    .line 1987823
    :cond_0
    const/4 v3, 0x0

    .line 1987824
    invoke-static {p1}, LX/DL8;->d(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 1987825
    iget-object v1, p0, LX/DL8;->k:LX/DLN;

    if-eqz v1, :cond_1

    .line 1987826
    iget-object v1, p0, LX/DL8;->k:LX/DLN;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1987827
    new-instance v5, LX/DLr;

    invoke-direct {v5}, LX/DLr;-><init>()V

    .line 1987828
    iput-object v0, v5, LX/DLr;->c:Ljava/lang/String;

    .line 1987829
    iput-object v2, v5, LX/DLr;->d:Ljava/lang/String;

    .line 1987830
    new-instance v6, LX/DLu;

    invoke-direct {v6}, LX/DLu;-><init>()V

    iget-object v4, p0, LX/DL8;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-virtual {v4}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v4

    .line 1987831
    iput-object v4, v6, LX/DLu;->c:Ljava/lang/String;

    .line 1987832
    move-object v4, v6

    .line 1987833
    invoke-virtual {v4}, LX/DLu;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OwnerModel;

    move-result-object v4

    iput-object v4, v5, LX/DLr;->f:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OwnerModel;

    .line 1987834
    if-eqz v3, :cond_7

    sget-object v4, LX/DLI;->UPLOADING_FAILED_SHOW_RETRY:LX/DLI;

    .line 1987835
    :goto_2
    new-instance v6, LX/DLH;

    invoke-virtual {v5}, LX/DLr;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    move-result-object v5

    invoke-direct {v6, v5, v4}, LX/DLH;-><init>(Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;LX/DLI;)V

    move-object v2, v6

    .line 1987836
    iget-object v3, v1, LX/DLN;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object v3, v3, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    .line 1987837
    iget-object v4, v3, LX/DKt;->e:Ljava/util/Map;

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1987838
    iget-object v4, v3, LX/DKt;->a:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1987839
    invoke-virtual {v3}, LX/1OM;->notifyDataSetChanged()V

    .line 1987840
    iget-object v3, v1, LX/DLN;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->a(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;Z)V

    .line 1987841
    :cond_1
    iget-object v1, p0, LX/DL8;->l:Ljava/util/Map;

    new-instance v2, LX/DL6;

    invoke-direct {v2, p1}, LX/DL6;-><init>(Ljava/io/File;)V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1987842
    invoke-static {p0, p1}, LX/DL8;->f(LX/DL8;Ljava/io/File;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {p0, v0}, LX/DL8;->a(LX/DL8;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_1

    .line 1987843
    :cond_2
    iget-object v2, p0, LX/DL8;->l:Ljava/util/Map;

    invoke-static {p1}, LX/DL8;->d(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1987844
    const v2, 0x7f0830a7

    const v3, 0x7f0830a6

    invoke-static {p0, v2, v3, v7}, LX/DL8;->a(LX/DL8;III)V

    goto/16 :goto_0

    .line 1987845
    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1987846
    invoke-static {v2}, LX/DM3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1987847
    const-string v4, ".exe"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, ".mp3"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_4
    const/4 v3, 0x1

    :goto_3
    move v2, v3

    .line 1987848
    if-eqz v2, :cond_5

    .line 1987849
    const v2, 0x7f0830a9

    const v3, 0x7f0830a8

    invoke-static {p0, v2, v3, v7}, LX/DL8;->a(LX/DL8;III)V

    goto/16 :goto_0

    .line 1987850
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_3

    .line 1987851
    :cond_7
    sget-object v4, LX/DLI;->UPLOADING_IS_IN_PROGRESS:LX/DLI;

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1987807
    iget-object v0, p0, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DL6;

    .line 1987808
    if-nez v0, :cond_0

    .line 1987809
    :goto_0
    return-void

    .line 1987810
    :cond_0
    iget-object v1, v0, LX/DL6;->b:LX/DL5;

    sget-object v2, LX/DL5;->SEGMENT_UPLOADING_FAILURE:LX/DL5;

    if-eq v1, v2, :cond_1

    iget-object v1, v0, LX/DL6;->c:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 1987811
    :cond_1
    iget-object v0, v0, LX/DL6;->a:Ljava/io/File;

    invoke-static {p0, v0}, LX/DL8;->f(LX/DL8;Ljava/io/File;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {p0, v0}, LX/DL8;->a(LX/DL8;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_0

    .line 1987812
    :cond_2
    iget-object v1, v0, LX/DL6;->a:Ljava/io/File;

    iget-object v0, v0, LX/DL6;->c:Ljava/lang/String;

    .line 1987813
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 1987814
    iget-object v3, p0, LX/DL8;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$3;

    invoke-direct {v4, p0, v1, v0, v2}, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$3;-><init>(LX/DL8;Ljava/io/File;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V

    const p1, -0x57f3c008

    invoke-static {v3, v4, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1987815
    move-object v0, v2

    .line 1987816
    invoke-static {p0, v0}, LX/DL8;->a(LX/DL8;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_0
.end method
