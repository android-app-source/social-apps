.class public LX/Esi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2176352
    return-void
.end method

.method public static a(LX/0QB;)LX/Esi;
    .locals 3

    .prologue
    .line 2176340
    const-class v1, LX/Esi;

    monitor-enter v1

    .line 2176341
    :try_start_0
    sget-object v0, LX/Esi;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176342
    sput-object v2, LX/Esi;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176343
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176344
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2176345
    new-instance v0, LX/Esi;

    invoke-direct {v0}, LX/Esi;-><init>()V

    .line 2176346
    move-object v0, v0

    .line 2176347
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176348
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Esi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176349
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176350
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
