.class public final LX/Dco;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;)V
    .locals 0

    .prologue
    .line 2018949
    iput-object p1, p0, LX/Dco;->a:Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 2018950
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2018951
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    .line 2018952
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 2018953
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v0, v1

    .line 2018954
    sget-object v1, LX/8LS;->PLACE_PHOTO:LX/8LS;

    if-ne v0, v1, :cond_0

    .line 2018955
    iget-object v0, p0, LX/Dco;->a:Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;

    iget-object v0, v0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->d:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/Dco;->a:Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0122

    .line 2018956
    iget-object v4, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v4, v4

    .line 2018957
    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2018958
    :cond_0
    return-void
.end method
