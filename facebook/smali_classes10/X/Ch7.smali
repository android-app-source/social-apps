.class public final LX/Ch7;
.super LX/Ch4;
.source ""


# instance fields
.field public final a:D

.field public final b:I

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsInterfaces$PageOverallStarRating$Histogram;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;


# direct methods
.method private constructor <init>(DILX/0am;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DI",
            "LX/0am",
            "<",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsInterfaces$PageOverallStarRating$Histogram;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1927785
    invoke-direct {p0}, LX/Ch4;-><init>()V

    .line 1927786
    iput-wide p1, p0, LX/Ch7;->a:D

    .line 1927787
    iput p3, p0, LX/Ch7;->b:I

    .line 1927788
    iput-object p4, p0, LX/Ch7;->c:LX/0am;

    .line 1927789
    iput-object p5, p0, LX/Ch7;->d:Ljava/lang/String;

    .line 1927790
    return-void
.end method

.method public constructor <init>(Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1927783
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->d()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->b()I

    move-result v4

    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    move-object v1, p0

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, LX/Ch7;-><init>(DILX/0am;Ljava/lang/String;)V

    .line 1927784
    return-void
.end method
