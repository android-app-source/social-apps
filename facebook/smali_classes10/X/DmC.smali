.class public final enum LX/DmC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DmC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DmC;

.field private static final LOOKUP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/DmC;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PAGE_IDENTITY:LX/DmC;

.field public static final enum SEND_MESSAGE:LX/DmC;

.field public static final enum SERVICE_BLUR_HEADER:LX/DmC;

.field public static final enum SERVICE_DATE_ONLY:LX/DmC;

.field public static final enum SERVICE_HEADER:LX/DmC;

.field public static final enum SERVICE_INFO:LX/DmC;

.field public static final enum SERVICE_LOCATION:LX/DmC;

.field public static final enum SERVICE_PHONE_NUMBER:LX/DmC;

.field public static final enum SERVICE_PHOTO:LX/DmC;

.field public static final enum SERVICE_TIME:LX/DmC;

.field public static final enum SERVICE_TIME_ONLY:LX/DmC;


# instance fields
.field private final viewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2038911
    new-instance v1, LX/DmC;

    const-string v2, "SERVICE_BLUR_HEADER"

    const v3, 0x7f0d01df

    invoke-direct {v1, v2, v0, v3}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SERVICE_BLUR_HEADER:LX/DmC;

    .line 2038912
    new-instance v1, LX/DmC;

    const-string v2, "SERVICE_HEADER"

    const v3, 0x7f0d01e0

    invoke-direct {v1, v2, v5, v3}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SERVICE_HEADER:LX/DmC;

    .line 2038913
    new-instance v1, LX/DmC;

    const-string v2, "PAGE_IDENTITY"

    const v3, 0x7f0d01dd

    invoke-direct {v1, v2, v6, v3}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->PAGE_IDENTITY:LX/DmC;

    .line 2038914
    new-instance v1, LX/DmC;

    const-string v2, "SERVICE_PHOTO"

    const v3, 0x7f0d01de

    invoke-direct {v1, v2, v7, v3}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SERVICE_PHOTO:LX/DmC;

    .line 2038915
    new-instance v1, LX/DmC;

    const-string v2, "SERVICE_DATE_ONLY"

    const v3, 0x7f0d01e2

    invoke-direct {v1, v2, v8, v3}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SERVICE_DATE_ONLY:LX/DmC;

    .line 2038916
    new-instance v1, LX/DmC;

    const-string v2, "SERVICE_TIME_ONLY"

    const/4 v3, 0x5

    const v4, 0x7f0d01e3

    invoke-direct {v1, v2, v3, v4}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SERVICE_TIME_ONLY:LX/DmC;

    .line 2038917
    new-instance v1, LX/DmC;

    const-string v2, "SERVICE_TIME"

    const/4 v3, 0x6

    const v4, 0x7f0d01e1

    invoke-direct {v1, v2, v3, v4}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SERVICE_TIME:LX/DmC;

    .line 2038918
    new-instance v1, LX/DmC;

    const-string v2, "SERVICE_INFO"

    const/4 v3, 0x7

    const v4, 0x7f0d01e4

    invoke-direct {v1, v2, v3, v4}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SERVICE_INFO:LX/DmC;

    .line 2038919
    new-instance v1, LX/DmC;

    const-string v2, "SERVICE_LOCATION"

    const/16 v3, 0x8

    const v4, 0x7f0d01e5

    invoke-direct {v1, v2, v3, v4}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SERVICE_LOCATION:LX/DmC;

    .line 2038920
    new-instance v1, LX/DmC;

    const-string v2, "SERVICE_PHONE_NUMBER"

    const/16 v3, 0x9

    const v4, 0x7f0d01e6

    invoke-direct {v1, v2, v3, v4}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SERVICE_PHONE_NUMBER:LX/DmC;

    .line 2038921
    new-instance v1, LX/DmC;

    const-string v2, "SEND_MESSAGE"

    const/16 v3, 0xa

    const v4, 0x7f0d01e7

    invoke-direct {v1, v2, v3, v4}, LX/DmC;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DmC;->SEND_MESSAGE:LX/DmC;

    .line 2038922
    const/16 v1, 0xb

    new-array v1, v1, [LX/DmC;

    sget-object v2, LX/DmC;->SERVICE_BLUR_HEADER:LX/DmC;

    aput-object v2, v1, v0

    sget-object v2, LX/DmC;->SERVICE_HEADER:LX/DmC;

    aput-object v2, v1, v5

    sget-object v2, LX/DmC;->PAGE_IDENTITY:LX/DmC;

    aput-object v2, v1, v6

    sget-object v2, LX/DmC;->SERVICE_PHOTO:LX/DmC;

    aput-object v2, v1, v7

    sget-object v2, LX/DmC;->SERVICE_DATE_ONLY:LX/DmC;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, LX/DmC;->SERVICE_TIME_ONLY:LX/DmC;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, LX/DmC;->SERVICE_TIME:LX/DmC;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, LX/DmC;->SERVICE_INFO:LX/DmC;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, LX/DmC;->SERVICE_LOCATION:LX/DmC;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, LX/DmC;->SERVICE_PHONE_NUMBER:LX/DmC;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, LX/DmC;->SEND_MESSAGE:LX/DmC;

    aput-object v3, v1, v2

    sput-object v1, LX/DmC;->$VALUES:[LX/DmC;

    .line 2038923
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, LX/DmC;->LOOKUP:Ljava/util/Map;

    .line 2038924
    invoke-static {}, LX/DmC;->values()[LX/DmC;

    move-result-object v1

    .line 2038925
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2038926
    sget-object v4, LX/DmC;->LOOKUP:Ljava/util/Map;

    invoke-virtual {v3}, LX/DmC;->toInt()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2038927
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2038928
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2038904
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2038905
    iput p3, p0, LX/DmC;->viewType:I

    .line 2038906
    return-void
.end method

.method public static fromInt(I)LX/DmC;
    .locals 3

    .prologue
    .line 2038907
    sget-object v0, LX/DmC;->LOOKUP:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DmC;

    .line 2038908
    if-eqz v0, :cond_0

    .line 2038909
    return-object v0

    .line 2038910
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType int "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/DmC;
    .locals 1

    .prologue
    .line 2038903
    const-class v0, LX/DmC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DmC;

    return-object v0
.end method

.method public static values()[LX/DmC;
    .locals 1

    .prologue
    .line 2038902
    sget-object v0, LX/DmC;->$VALUES:[LX/DmC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DmC;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 2038901
    iget v0, p0, LX/DmC;->viewType:I

    return v0
.end method
