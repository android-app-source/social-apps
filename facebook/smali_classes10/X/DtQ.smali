.class public final enum LX/DtQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DtQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DtQ;

.field public static final enum R_CANCELED:LX/DtQ;

.field public static final enum R_CANCELED_CHARGEBACK:LX/DtQ;

.field public static final enum R_CANCELED_CUSTOMER_SERVICE:LX/DtQ;

.field public static final enum R_CANCELED_DECLINED:LX/DtQ;

.field public static final enum R_CANCELED_EXPIRED:LX/DtQ;

.field public static final enum R_CANCELED_RECIPIENT_RISK:LX/DtQ;

.field public static final enum R_CANCELED_SAME_CARD:LX/DtQ;

.field public static final enum R_CANCELED_SENDER_RISK:LX/DtQ;

.field public static final enum R_CANCELED_SYSTEM_FAIL:LX/DtQ;

.field public static final enum R_COMPLETED:LX/DtQ;

.field public static final enum R_PENDING:LX/DtQ;

.field public static final enum R_PENDING_MANUAL_REVIEW:LX/DtQ;

.field public static final enum R_PENDING_NUX:LX/DtQ;

.field public static final enum R_PENDING_PROCESSING:LX/DtQ;

.field public static final enum R_PENDING_PUSH_FAIL:LX/DtQ;

.field public static final enum R_PENDING_PUSH_FAIL_CARD_EXPIRED:LX/DtQ;

.field public static final enum R_PENDING_VERIFICATION:LX/DtQ;

.field public static final enum R_PENDING_VERIFICATION_PROCESSING:LX/DtQ;

.field public static final enum S_CANCELED:LX/DtQ;

.field public static final enum S_CANCELED_CHARGEBACK:LX/DtQ;

.field public static final enum S_CANCELED_CUSTOMER_SERVICE:LX/DtQ;

.field public static final enum S_CANCELED_DECLINED:LX/DtQ;

.field public static final enum S_CANCELED_EXPIRED:LX/DtQ;

.field public static final enum S_CANCELED_RECIPIENT_RISK:LX/DtQ;

.field public static final enum S_CANCELED_SAME_CARD:LX/DtQ;

.field public static final enum S_CANCELED_SENDER_RISK:LX/DtQ;

.field public static final enum S_CANCELED_SYSTEM_FAIL:LX/DtQ;

.field public static final enum S_COMPLETED:LX/DtQ;

.field public static final enum S_PENDING:LX/DtQ;

.field public static final enum S_PENDING_MANUAL_REVIEW:LX/DtQ;

.field public static final enum S_PENDING_VERIFICATION:LX/DtQ;

.field public static final enum S_PENDING_VERIFICATION_PROCESSING:LX/DtQ;

.field public static final enum S_SENT:LX/DtQ;

.field public static final enum UNKNOWN_STATUS:LX/DtQ;


# instance fields
.field public final isTerminalStatus:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2052095
    new-instance v0, LX/DtQ;

    const-string v1, "R_PENDING"

    invoke-direct {v0, v1, v4, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_PENDING:LX/DtQ;

    .line 2052096
    new-instance v0, LX/DtQ;

    const-string v1, "R_PENDING_VERIFICATION"

    invoke-direct {v0, v1, v3, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_PENDING_VERIFICATION:LX/DtQ;

    .line 2052097
    new-instance v0, LX/DtQ;

    const-string v1, "R_PENDING_VERIFICATION_PROCESSING"

    invoke-direct {v0, v1, v5, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_PENDING_VERIFICATION_PROCESSING:LX/DtQ;

    .line 2052098
    new-instance v0, LX/DtQ;

    const-string v1, "R_PENDING_MANUAL_REVIEW"

    invoke-direct {v0, v1, v6, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_PENDING_MANUAL_REVIEW:LX/DtQ;

    .line 2052099
    new-instance v0, LX/DtQ;

    const-string v1, "R_CANCELED"

    invoke-direct {v0, v1, v7, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_CANCELED:LX/DtQ;

    .line 2052100
    new-instance v0, LX/DtQ;

    const-string v1, "R_CANCELED_SENDER_RISK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_CANCELED_SENDER_RISK:LX/DtQ;

    .line 2052101
    new-instance v0, LX/DtQ;

    const-string v1, "R_CANCELED_RECIPIENT_RISK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_CANCELED_RECIPIENT_RISK:LX/DtQ;

    .line 2052102
    new-instance v0, LX/DtQ;

    const-string v1, "R_CANCELED_DECLINED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_CANCELED_DECLINED:LX/DtQ;

    .line 2052103
    new-instance v0, LX/DtQ;

    const-string v1, "R_CANCELED_EXPIRED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_CANCELED_EXPIRED:LX/DtQ;

    .line 2052104
    new-instance v0, LX/DtQ;

    const-string v1, "R_CANCELED_SAME_CARD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_CANCELED_SAME_CARD:LX/DtQ;

    .line 2052105
    new-instance v0, LX/DtQ;

    const-string v1, "R_CANCELED_CUSTOMER_SERVICE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_CANCELED_CUSTOMER_SERVICE:LX/DtQ;

    .line 2052106
    new-instance v0, LX/DtQ;

    const-string v1, "R_CANCELED_CHARGEBACK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_CANCELED_CHARGEBACK:LX/DtQ;

    .line 2052107
    new-instance v0, LX/DtQ;

    const-string v1, "R_CANCELED_SYSTEM_FAIL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_CANCELED_SYSTEM_FAIL:LX/DtQ;

    .line 2052108
    new-instance v0, LX/DtQ;

    const-string v1, "R_COMPLETED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_COMPLETED:LX/DtQ;

    .line 2052109
    new-instance v0, LX/DtQ;

    const-string v1, "R_PENDING_NUX"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_PENDING_NUX:LX/DtQ;

    .line 2052110
    new-instance v0, LX/DtQ;

    const-string v1, "R_PENDING_PROCESSING"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_PENDING_PROCESSING:LX/DtQ;

    .line 2052111
    new-instance v0, LX/DtQ;

    const-string v1, "R_PENDING_PUSH_FAIL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_PENDING_PUSH_FAIL:LX/DtQ;

    .line 2052112
    new-instance v0, LX/DtQ;

    const-string v1, "R_PENDING_PUSH_FAIL_CARD_EXPIRED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->R_PENDING_PUSH_FAIL_CARD_EXPIRED:LX/DtQ;

    .line 2052113
    new-instance v0, LX/DtQ;

    const-string v1, "S_PENDING"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_PENDING:LX/DtQ;

    .line 2052114
    new-instance v0, LX/DtQ;

    const-string v1, "S_PENDING_VERIFICATION"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_PENDING_VERIFICATION:LX/DtQ;

    .line 2052115
    new-instance v0, LX/DtQ;

    const-string v1, "S_PENDING_VERIFICATION_PROCESSING"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_PENDING_VERIFICATION_PROCESSING:LX/DtQ;

    .line 2052116
    new-instance v0, LX/DtQ;

    const-string v1, "S_PENDING_MANUAL_REVIEW"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_PENDING_MANUAL_REVIEW:LX/DtQ;

    .line 2052117
    new-instance v0, LX/DtQ;

    const-string v1, "S_CANCELED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_CANCELED:LX/DtQ;

    .line 2052118
    new-instance v0, LX/DtQ;

    const-string v1, "S_CANCELED_SENDER_RISK"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_CANCELED_SENDER_RISK:LX/DtQ;

    .line 2052119
    new-instance v0, LX/DtQ;

    const-string v1, "S_CANCELED_RECIPIENT_RISK"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_CANCELED_RECIPIENT_RISK:LX/DtQ;

    .line 2052120
    new-instance v0, LX/DtQ;

    const-string v1, "S_CANCELED_DECLINED"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_CANCELED_DECLINED:LX/DtQ;

    .line 2052121
    new-instance v0, LX/DtQ;

    const-string v1, "S_CANCELED_EXPIRED"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_CANCELED_EXPIRED:LX/DtQ;

    .line 2052122
    new-instance v0, LX/DtQ;

    const-string v1, "S_CANCELED_SAME_CARD"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_CANCELED_SAME_CARD:LX/DtQ;

    .line 2052123
    new-instance v0, LX/DtQ;

    const-string v1, "S_CANCELED_CUSTOMER_SERVICE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_CANCELED_CUSTOMER_SERVICE:LX/DtQ;

    .line 2052124
    new-instance v0, LX/DtQ;

    const-string v1, "S_CANCELED_CHARGEBACK"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_CANCELED_CHARGEBACK:LX/DtQ;

    .line 2052125
    new-instance v0, LX/DtQ;

    const-string v1, "S_CANCELED_SYSTEM_FAIL"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_CANCELED_SYSTEM_FAIL:LX/DtQ;

    .line 2052126
    new-instance v0, LX/DtQ;

    const-string v1, "S_COMPLETED"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2, v3}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_COMPLETED:LX/DtQ;

    .line 2052127
    new-instance v0, LX/DtQ;

    const-string v1, "S_SENT"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->S_SENT:LX/DtQ;

    .line 2052128
    new-instance v0, LX/DtQ;

    const-string v1, "UNKNOWN_STATUS"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2, v4}, LX/DtQ;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/DtQ;->UNKNOWN_STATUS:LX/DtQ;

    .line 2052129
    const/16 v0, 0x22

    new-array v0, v0, [LX/DtQ;

    sget-object v1, LX/DtQ;->R_PENDING:LX/DtQ;

    aput-object v1, v0, v4

    sget-object v1, LX/DtQ;->R_PENDING_VERIFICATION:LX/DtQ;

    aput-object v1, v0, v3

    sget-object v1, LX/DtQ;->R_PENDING_VERIFICATION_PROCESSING:LX/DtQ;

    aput-object v1, v0, v5

    sget-object v1, LX/DtQ;->R_PENDING_MANUAL_REVIEW:LX/DtQ;

    aput-object v1, v0, v6

    sget-object v1, LX/DtQ;->R_CANCELED:LX/DtQ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DtQ;->R_CANCELED_SENDER_RISK:LX/DtQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DtQ;->R_CANCELED_RECIPIENT_RISK:LX/DtQ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DtQ;->R_CANCELED_DECLINED:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DtQ;->R_CANCELED_EXPIRED:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/DtQ;->R_CANCELED_SAME_CARD:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/DtQ;->R_CANCELED_CUSTOMER_SERVICE:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/DtQ;->R_CANCELED_CHARGEBACK:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/DtQ;->R_CANCELED_SYSTEM_FAIL:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/DtQ;->R_COMPLETED:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/DtQ;->R_PENDING_NUX:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/DtQ;->R_PENDING_PROCESSING:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/DtQ;->R_PENDING_PUSH_FAIL:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/DtQ;->R_PENDING_PUSH_FAIL_CARD_EXPIRED:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/DtQ;->S_PENDING:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/DtQ;->S_PENDING_VERIFICATION:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/DtQ;->S_PENDING_VERIFICATION_PROCESSING:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/DtQ;->S_PENDING_MANUAL_REVIEW:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/DtQ;->S_CANCELED:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/DtQ;->S_CANCELED_SENDER_RISK:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/DtQ;->S_CANCELED_RECIPIENT_RISK:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/DtQ;->S_CANCELED_DECLINED:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/DtQ;->S_CANCELED_EXPIRED:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/DtQ;->S_CANCELED_SAME_CARD:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/DtQ;->S_CANCELED_CUSTOMER_SERVICE:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/DtQ;->S_CANCELED_CHARGEBACK:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/DtQ;->S_CANCELED_SYSTEM_FAIL:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/DtQ;->S_COMPLETED:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/DtQ;->S_SENT:LX/DtQ;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/DtQ;->UNKNOWN_STATUS:LX/DtQ;

    aput-object v2, v0, v1

    sput-object v0, LX/DtQ;->$VALUES:[LX/DtQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 2052138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2052139
    iput-boolean p3, p0, LX/DtQ;->isTerminalStatus:Z

    .line 2052140
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/DtQ;
    .locals 5

    .prologue
    .line 2052133
    invoke-static {}, LX/DtQ;->values()[LX/DtQ;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2052134
    invoke-virtual {v0}, LX/DtQ;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2052135
    :goto_1
    return-object v0

    .line 2052136
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2052137
    :cond_1
    sget-object v0, LX/DtQ;->UNKNOWN_STATUS:LX/DtQ;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/DtQ;
    .locals 1

    .prologue
    .line 2052132
    const-class v0, LX/DtQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DtQ;

    return-object v0
.end method

.method public static values()[LX/DtQ;
    .locals 1

    .prologue
    .line 2052131
    sget-object v0, LX/DtQ;->$VALUES:[LX/DtQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DtQ;

    return-object v0
.end method


# virtual methods
.method public final getMessengerPayEntityType()LX/DtA;
    .locals 1

    .prologue
    .line 2052130
    sget-object v0, LX/DtA;->PAYMENT_TRANSACTION:LX/DtA;

    return-object v0
.end method
