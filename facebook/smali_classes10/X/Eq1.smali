.class public final LX/Eq1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V
    .locals 0

    .prologue
    .line 2170863
    iput-object p1, p0, LX/Eq1;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2170864
    iget-object v0, p0, LX/Eq1;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->x:LX/03V;

    sget-object v1, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->G:Ljava/lang/String;

    const-string v2, "Failed to fetch invitees restrictions"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2170865
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2170866
    move-object v1, v1

    .line 2170867
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2170868
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170869
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;

    const/4 v1, 0x0

    .line 2170870
    iget-object v0, p0, LX/Eq1;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170871
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->n()Z

    move-result v2

    move v0, v2

    .line 2170872
    if-eqz v0, :cond_0

    .line 2170873
    :goto_0
    return-void

    .line 2170874
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2170875
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2170876
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2170877
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    .line 2170878
    :goto_1
    if-ge v2, v6, :cond_3

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;

    .line 2170879
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->c(Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 2170880
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2170881
    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2170882
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->d(Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 2170883
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2170884
    :cond_3
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_4

    .line 2170885
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v5, p0, LX/Eq1;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    .line 2170886
    iput v0, v5, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->Q:I

    .line 2170887
    :cond_4
    iget-object v0, p0, LX/Eq1;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170888
    iput-object v3, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->K:Ljava/util/List;

    .line 2170889
    iget-object v0, p0, LX/Eq1;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170890
    iput-object v4, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->M:Ljava/util/List;

    .line 2170891
    iget-object v0, p0, LX/Eq1;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->R:LX/8tE;

    invoke-virtual {v0, v3}, LX/8tE;->b(Ljava/util/Collection;)V

    .line 2170892
    iget-object v0, p0, LX/Eq1;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->R:LX/8tE;

    invoke-virtual {v0, v4}, LX/8tE;->a(Ljava/util/Collection;)V

    .line 2170893
    iget-object v0, p0, LX/Eq1;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-static {v0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->P(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 2170894
    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_3
.end method
