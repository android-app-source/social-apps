.class public LX/Dps;
.super LX/6Ll;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field public b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Rect;

.field private d:Landroid/animation/ValueAnimator;

.field private e:Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private final i:Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2047455
    invoke-direct {p0, p1}, LX/6Ll;-><init>(Landroid/content/Context;)V

    .line 2047456
    new-instance v0, LX/Dpr;

    invoke-direct {v0, p0}, LX/Dpr;-><init>(LX/Dps;)V

    iput-object v0, p0, LX/Dps;->i:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 2047457
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/Dps;->a(Landroid/util/AttributeSet;)V

    .line 2047458
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2047481
    invoke-direct {p0, p1, p2}, LX/6Ll;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2047482
    new-instance v0, LX/Dpr;

    invoke-direct {v0, p0}, LX/Dpr;-><init>(LX/Dps;)V

    iput-object v0, p0, LX/Dps;->i:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 2047483
    invoke-direct {p0, p2}, LX/Dps;->a(Landroid/util/AttributeSet;)V

    .line 2047484
    return-void
.end method

.method private a(IJ)V
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2047494
    invoke-direct {p0}, LX/Dps;->getStatusBarColor()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 2047495
    :goto_0
    return-void

    .line 2047496
    :cond_0
    iput p1, p0, LX/Dps;->h:I

    .line 2047497
    iget-object v0, p0, LX/Dps;->d:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 2047498
    iget-object v0, p0, LX/Dps;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2047499
    iget-object v0, p0, LX/Dps;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 2047500
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Dps;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    const/16 v1, 0xff

    if-eq v0, v1, :cond_3

    .line 2047501
    :cond_2
    iget-object v0, p0, LX/Dps;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2047502
    invoke-virtual {p0}, LX/Dps;->invalidate()V

    goto :goto_0

    .line 2047503
    :cond_3
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-direct {p0}, LX/Dps;->getStatusBarColor()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    aput p1, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofArgb([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/Dps;->d:Landroid/animation/ValueAnimator;

    .line 2047504
    iget-object v0, p0, LX/Dps;->d:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/Dps;->i:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2047505
    iget-object v0, p0, LX/Dps;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p2, p3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2047506
    iget-object v0, p0, LX/Dps;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2047485
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/Dps;->b:Landroid/graphics/Paint;

    .line 2047486
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/Dps;->c:Landroid/graphics/Rect;

    .line 2047487
    invoke-virtual {p0}, LX/Dps;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->SystemBarConsumingLayout:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2047488
    :try_start_0
    const/16 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Dps;->f:Z

    .line 2047489
    const/16 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Dps;->g:Z

    .line 2047490
    const/16 v0, 0x2

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/Dps;->setStatusBarColor(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2047491
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2047492
    return-void

    .line 2047493
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private getStatusBarColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 2047476
    iget-object v0, p0, LX/Dps;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2047477
    invoke-super {p0, p1}, LX/6Ll;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2047478
    iget-object v0, p0, LX/Dps;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2047479
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v0, p0, LX/Dps;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v0

    iget-object v5, p0, LX/Dps;->b:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2047480
    :cond_0
    return-void
.end method

.method public final fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2047467
    iget-object v0, p0, LX/Dps;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2047468
    iget-boolean v0, p0, LX/Dps;->f:Z

    if-eqz v0, :cond_0

    .line 2047469
    iput v2, p1, Landroid/graphics/Rect;->top:I

    .line 2047470
    :cond_0
    iget-boolean v0, p0, LX/Dps;->g:Z

    if-eqz v0, :cond_1

    .line 2047471
    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    .line 2047472
    :cond_1
    invoke-super {p0, p1}, LX/6Ll;->fitSystemWindows(Landroid/graphics/Rect;)Z

    .line 2047473
    iget-object v0, p0, LX/Dps;->e:Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;

    if-eqz v0, :cond_2

    .line 2047474
    iget-object v0, p0, LX/Dps;->e:Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;

    invoke-virtual {p0}, LX/Dps;->getSystemInsets()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->a(Landroid/graphics/Rect;)V

    .line 2047475
    :cond_2
    return v2
.end method

.method public getSystemInsets()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 2047466
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, LX/Dps;->c:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getTargetStatusBarColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 2047463
    iget-object v0, p0, LX/Dps;->d:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dps;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2047464
    iget v0, p0, LX/Dps;->h:I

    .line 2047465
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LX/Dps;->getStatusBarColor()I

    move-result v0

    goto :goto_0
.end method

.method public setOnSystemInsetsChangedListener(Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;)V
    .locals 0

    .prologue
    .line 2047461
    iput-object p1, p0, LX/Dps;->e:Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;

    .line 2047462
    return-void
.end method

.method public setStatusBarColor(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2047459
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/Dps;->a(IJ)V

    .line 2047460
    return-void
.end method
