.class public final LX/ELV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;LX/CzL;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2109197
    iput-object p1, p0, LX/ELV;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;

    iput-object p2, p0, LX/ELV;->a:LX/CzL;

    iput-object p3, p0, LX/ELV;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v0, 0x1

    const v1, -0x720b8e08

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2109198
    iget-object v0, p0, LX/ELV;->a:LX/CzL;

    .line 2109199
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2109200
    move-object v5, v0

    check-cast v5, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2109201
    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bi()Ljava/lang/String;

    move-result-object v0

    .line 2109202
    if-eqz v0, :cond_0

    .line 2109203
    iget-object v1, p0, LX/ELV;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->f:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2109204
    :cond_0
    iget-object v0, p0, LX/ELV;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->g:LX/CvY;

    iget-object v1, p0, LX/ELV;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->OPEN_LINK:LX/8ch;

    iget-object v3, p0, LX/ELV;->b:LX/1Pn;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/ELV;->a:LX/CzL;

    invoke-interface {v3, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, LX/ELV;->a:LX/CzL;

    .line 2109205
    iget-object v6, p0, LX/ELV;->b:LX/1Pn;

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    iget-object v6, p0, LX/ELV;->b:LX/1Pn;

    check-cast v6, LX/CxP;

    iget-object v9, p0, LX/ELV;->a:LX/CzL;

    invoke-interface {v6, v9}, LX/CxP;->b(LX/CzL;)I

    move-result v6

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v5

    .line 2109206
    sget-object v9, LX/0Rg;->a:LX/0Rg;

    move-object v9, v9

    .line 2109207
    invoke-static {v8, v6, v5, v9}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2109208
    const v0, 0x109702b7

    invoke-static {v10, v10, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
