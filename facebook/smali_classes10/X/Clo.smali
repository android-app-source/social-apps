.class public LX/Clo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/os/Bundle;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Clr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1933134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933135
    iput-object p1, p0, LX/Clo;->a:Ljava/lang/String;

    .line 1933136
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/Clo;->b:Landroid/os/Bundle;

    .line 1933137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Clo;->c:Ljava/util/List;

    .line 1933138
    return-void
.end method


# virtual methods
.method public final a(I)LX/Clr;
    .locals 1

    .prologue
    .line 1933124
    iget-object v0, p0, LX/Clo;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Clr;

    return-object v0
.end method

.method public final a(LX/Clr;)V
    .locals 1

    .prologue
    .line 1933132
    iget-object v0, p0, LX/Clo;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1933133
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/Clr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1933129
    if-eqz p1, :cond_0

    .line 1933130
    iget-object v0, p0, LX/Clo;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1933131
    :cond_0
    return-void
.end method

.method public final b(ILX/Clr;)V
    .locals 1

    .prologue
    .line 1933126
    if-eqz p2, :cond_0

    .line 1933127
    iget-object v0, p0, LX/Clo;->c:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1933128
    :cond_0
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1933125
    iget-object v0, p0, LX/Clo;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
