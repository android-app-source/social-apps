.class public final LX/Dj6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V
    .locals 0

    .prologue
    .line 2032858
    iput-object p1, p0, LX/Dj6;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x69fe872c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2032859
    iget-object v1, p0, LX/Dj6;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->e:LX/0Uh;

    const/16 v2, 0x61e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    .line 2032860
    iget-object v1, p0, LX/Dj6;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    .line 2032861
    sget-object v2, LX/Diw;->ADMIN_DECLINE:LX/Diw;

    invoke-static {v2}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;->a(LX/Diw;)Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;

    move-result-object v2

    .line 2032862
    new-instance v3, LX/Dj1;

    invoke-direct {v3, v1}, LX/Dj1;-><init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V

    .line 2032863
    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;->m:LX/Dix;

    .line 2032864
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    const-string p0, "appointment_confirmation_dialog_fragment_tag"

    invoke-virtual {v2, v3, p0}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2032865
    const v1, -0x4a15cac9

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void
.end method
