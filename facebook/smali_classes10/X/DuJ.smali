.class public final LX/DuJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 2056561
    const/16 v22, 0x0

    .line 2056562
    const/16 v21, 0x0

    .line 2056563
    const/16 v20, 0x0

    .line 2056564
    const/4 v15, 0x0

    .line 2056565
    const-wide/16 v18, 0x0

    .line 2056566
    const-wide/16 v16, 0x0

    .line 2056567
    const/4 v14, 0x0

    .line 2056568
    const/4 v13, 0x0

    .line 2056569
    const/4 v12, 0x0

    .line 2056570
    const/4 v11, 0x0

    .line 2056571
    const/4 v10, 0x0

    .line 2056572
    const/4 v9, 0x0

    .line 2056573
    const/4 v8, 0x0

    .line 2056574
    const/4 v5, 0x0

    .line 2056575
    const-wide/16 v6, 0x0

    .line 2056576
    const/4 v4, 0x0

    .line 2056577
    const/4 v3, 0x0

    .line 2056578
    const/4 v2, 0x0

    .line 2056579
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_15

    .line 2056580
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2056581
    const/4 v2, 0x0

    .line 2056582
    :goto_0
    return v2

    .line 2056583
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_11

    .line 2056584
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2056585
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2056586
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 2056587
    const-string v6, "__type__"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "__typename"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2056588
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 2056589
    :cond_2
    const-string v6, "amount"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2056590
    invoke-static/range {p0 .. p1}, LX/DuB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto :goto_1

    .line 2056591
    :cond_3
    const-string v6, "amount_fb_discount"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2056592
    invoke-static/range {p0 .. p1}, LX/DuB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto :goto_1

    .line 2056593
    :cond_4
    const-string v6, "commerce_order"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2056594
    invoke-static/range {p0 .. p1}, LX/Dtz;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto :goto_1

    .line 2056595
    :cond_5
    const-string v6, "completed_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2056596
    const/4 v2, 0x1

    .line 2056597
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2056598
    :cond_6
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2056599
    const/4 v2, 0x1

    .line 2056600
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v9, v2

    move-wide/from16 v20, v6

    goto/16 :goto_1

    .line 2056601
    :cond_7
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2056602
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 2056603
    :cond_8
    const-string v6, "order_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2056604
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2056605
    :cond_9
    const-string v6, "platform_item"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2056606
    invoke-static/range {p0 .. p1}, LX/DuN;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 2056607
    :cond_a
    const-string v6, "receiver_profile"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2056608
    invoke-static/range {p0 .. p1}, LX/DuK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2056609
    :cond_b
    const-string v6, "receiver_status"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2056610
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferReceiverStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 2056611
    :cond_c
    const-string v6, "sender"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2056612
    invoke-static/range {p0 .. p1}, LX/DuK;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 2056613
    :cond_d
    const-string v6, "sender_status"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2056614
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerTransferSenderStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2056615
    :cond_e
    const-string v6, "transfer_context"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 2056616
    invoke-static/range {p0 .. p1}, LX/DuT;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2056617
    :cond_f
    const-string v6, "updated_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 2056618
    const/4 v2, 0x1

    .line 2056619
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide v10, v6

    goto/16 :goto_1

    .line 2056620
    :cond_10
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2056621
    :cond_11
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2056622
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056623
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056624
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056625
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056626
    if-eqz v3, :cond_12

    .line 2056627
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2056628
    :cond_12
    if-eqz v9, :cond_13

    .line 2056629
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2056630
    :cond_13
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056631
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056632
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056633
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056634
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2056635
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2056636
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2056637
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2056638
    if-eqz v8, :cond_14

    .line 2056639
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2056640
    :cond_14
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_15
    move/from16 v23, v20

    move/from16 v24, v21

    move/from16 v25, v22

    move-wide/from16 v20, v16

    move/from16 v22, v15

    move v15, v10

    move/from16 v16, v11

    move/from16 v17, v12

    move-wide v10, v6

    move v12, v5

    move/from16 v26, v13

    move v13, v8

    move v8, v2

    move/from16 v27, v9

    move v9, v3

    move v3, v4

    move-wide/from16 v4, v18

    move/from16 v19, v14

    move/from16 v18, v26

    move/from16 v14, v27

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2056555
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2056556
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2056557
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/DuJ;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2056558
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2056559
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2056560
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2056486
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2056487
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2056488
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2056489
    invoke-static {p0, p1}, LX/DuJ;->a(LX/15w;LX/186;)I

    move-result v1

    .line 2056490
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2056491
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0xc

    const/16 v3, 0xa

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 2056492
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2056493
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2056494
    if-eqz v0, :cond_0

    .line 2056495
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056496
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2056497
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056498
    if-eqz v0, :cond_1

    .line 2056499
    const-string v1, "amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056500
    invoke-static {p0, v0, p2}, LX/DuB;->a(LX/15i;ILX/0nX;)V

    .line 2056501
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056502
    if-eqz v0, :cond_2

    .line 2056503
    const-string v1, "amount_fb_discount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056504
    invoke-static {p0, v0, p2}, LX/DuB;->a(LX/15i;ILX/0nX;)V

    .line 2056505
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056506
    if-eqz v0, :cond_3

    .line 2056507
    const-string v1, "commerce_order"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056508
    invoke-static {p0, v0, p2}, LX/Dtz;->a(LX/15i;ILX/0nX;)V

    .line 2056509
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2056510
    cmp-long v2, v0, v4

    if-eqz v2, :cond_4

    .line 2056511
    const-string v2, "completed_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056512
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2056513
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2056514
    cmp-long v2, v0, v4

    if-eqz v2, :cond_5

    .line 2056515
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056516
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2056517
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056518
    if-eqz v0, :cond_6

    .line 2056519
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056520
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056521
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056522
    if-eqz v0, :cond_7

    .line 2056523
    const-string v1, "order_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056524
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056525
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056526
    if-eqz v0, :cond_8

    .line 2056527
    const-string v1, "platform_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056528
    invoke-static {p0, v0, p2, p3}, LX/DuN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2056529
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056530
    if-eqz v0, :cond_9

    .line 2056531
    const-string v1, "receiver_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056532
    invoke-static {p0, v0, p2}, LX/DuK;->a(LX/15i;ILX/0nX;)V

    .line 2056533
    :cond_9
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2056534
    if-eqz v0, :cond_a

    .line 2056535
    const-string v0, "receiver_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056536
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056537
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056538
    if-eqz v0, :cond_b

    .line 2056539
    const-string v1, "sender"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056540
    invoke-static {p0, v0, p2}, LX/DuK;->a(LX/15i;ILX/0nX;)V

    .line 2056541
    :cond_b
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 2056542
    if-eqz v0, :cond_c

    .line 2056543
    const-string v0, "sender_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056544
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056545
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056546
    if-eqz v0, :cond_d

    .line 2056547
    const-string v1, "transfer_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056548
    invoke-static {p0, v0, p2, p3}, LX/DuT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2056549
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2056550
    cmp-long v2, v0, v4

    if-eqz v2, :cond_e

    .line 2056551
    const-string v2, "updated_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056552
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2056553
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2056554
    return-void
.end method
