.class public LX/DbZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/DbZ;


# instance fields
.field private final a:LX/0rq;

.field private final b:LX/0tX;

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0sX;


# direct methods
.method public constructor <init>(LX/0rq;LX/0tX;Landroid/content/res/Resources;LX/1Ck;LX/0sX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2017223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2017224
    iput-object p1, p0, LX/DbZ;->a:LX/0rq;

    .line 2017225
    iput-object p2, p0, LX/DbZ;->b:LX/0tX;

    .line 2017226
    iput-object p3, p0, LX/DbZ;->c:Landroid/content/res/Resources;

    .line 2017227
    iput-object p4, p0, LX/DbZ;->d:LX/1Ck;

    .line 2017228
    iput-object p5, p0, LX/DbZ;->e:LX/0sX;

    .line 2017229
    return-void
.end method

.method public static a(LX/0QB;)LX/DbZ;
    .locals 9

    .prologue
    .line 2017210
    sget-object v0, LX/DbZ;->f:LX/DbZ;

    if-nez v0, :cond_1

    .line 2017211
    const-class v1, LX/DbZ;

    monitor-enter v1

    .line 2017212
    :try_start_0
    sget-object v0, LX/DbZ;->f:LX/DbZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2017213
    if-eqz v2, :cond_0

    .line 2017214
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2017215
    new-instance v3, LX/DbZ;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v4

    check-cast v4, LX/0rq;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v8

    check-cast v8, LX/0sX;

    invoke-direct/range {v3 .. v8}, LX/DbZ;-><init>(LX/0rq;LX/0tX;Landroid/content/res/Resources;LX/1Ck;LX/0sX;)V

    .line 2017216
    move-object v0, v3

    .line 2017217
    sput-object v0, LX/DbZ;->f:LX/DbZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2017218
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2017219
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2017220
    :cond_1
    sget-object v0, LX/DbZ;->f:LX/DbZ;

    return-object v0

    .line 2017221
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2017222
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/DbX;)V
    .locals 4

    .prologue
    .line 2017200
    new-instance v0, LX/8B5;

    invoke-direct {v0}, LX/8B5;-><init>()V

    move-object v0, v0

    .line 2017201
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2017202
    const-string v1, "afterCursor"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2017203
    const-string v1, "category"

    const-string v2, "FOOD"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2017204
    const-string v1, "automatic_photo_captioning_enabled"

    iget-object v2, p0, LX/DbZ;->e:LX/0sX;

    invoke-virtual {v2}, LX/0sX;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2017205
    const-string v1, "image_size"

    iget-object v2, p0, LX/DbZ;->c:Landroid/content/res/Resources;

    const v3, 0x7f0b1644

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2017206
    const-string v1, "count"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2017207
    iget-object v1, p0, LX/DbZ;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2017208
    iget-object v1, p0, LX/DbZ;->d:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "task_key_load_photos"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/DbY;

    invoke-direct {v3, p0, p3}, LX/DbY;-><init>(LX/DbZ;LX/DbX;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2017209
    return-void
.end method
