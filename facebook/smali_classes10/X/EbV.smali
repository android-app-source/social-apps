.class public final LX/EbV;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/EYF;

.field public static b:LX/EYn;

.field public static c:LX/EYF;

.field public static d:LX/EYn;

.field public static e:LX/EYF;

.field public static f:LX/EYn;

.field public static g:LX/EYF;

.field public static h:LX/EYn;

.field public static i:LX/EYF;

.field public static j:LX/EYn;

.field public static k:LX/EYF;

.field public static l:LX/EYn;

.field public static m:LX/EYQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2144030
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\n\u0019WhisperTextProtocol.proto\u0012\ntextsecure\"a\n\rSignalMessage\u0012\u0012\n\nratchetKey\u0018\u0001 \u0001(\u000c\u0012\u000f\n\u0007counter\u0018\u0002 \u0001(\r\u0012\u0017\n\u000fpreviousCounter\u0018\u0003 \u0001(\r\u0012\u0012\n\nciphertext\u0018\u0004 \u0001(\u000c\"\u008e\u0001\n\u0013PreKeySignalMessage\u0012\u0016\n\u000eregistrationId\u0018\u0005 \u0001(\r\u0012\u0010\n\u0008preKeyId\u0018\u0001 \u0001(\r\u0012\u0016\n\u000esignedPreKeyId\u0018\u0006 \u0001(\r\u0012\u000f\n\u0007baseKey\u0018\u0002 \u0001(\u000c\u0012\u0013\n\u000bidentityKey\u0018\u0003 \u0001(\u000c\u0012\u000f\n\u0007message\u0018\u0004 \u0001(\u000c\"t\n\u0012KeyExchangeMessage\u0012\n\n\u0002id\u0018\u0001 \u0001(\r\u0012\u000f\n\u0007baseKey\u0018\u0002 \u0001(\u000c\u0012\u0012\n\nratchetKey\u0018\u0003 \u0001(\u000c\u0012\u0013\n\u000bidentityKey\u0018\u0004 \u0001(\u000c\u0012\u0018\n\u0010baseKeySignature\u0018\u0005 \u0001("

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "\u000c\"E\n\u0010SenderKeyMessage\u0012\n\n\u0002id\u0018\u0001 \u0001(\r\u0012\u0011\n\titeration\u0018\u0002 \u0001(\r\u0012\u0012\n\nciphertext\u0018\u0003 \u0001(\u000c\"c\n\u001cSenderKeyDistributionMessage\u0012\n\n\u0002id\u0018\u0001 \u0001(\r\u0012\u0011\n\titeration\u0018\u0002 \u0001(\r\u0012\u0010\n\u0008chainKey\u0018\u0003 \u0001(\u000c\u0012\u0012\n\nsigningKey\u0018\u0004 \u0001(\u000c\"E\n\u001cDeviceConsistencyCodeMessage\u0012\u0012\n\ngeneration\u0018\u0001 \u0001(\r\u0012\u0011\n\tsignature\u0018\u0002 \u0001(\u000cB5\n%org.whispersystems.libsignal.protocolB\u000cSignalProtos"

    aput-object v2, v0, v1

    .line 2144031
    new-instance v1, LX/EbE;

    invoke-direct {v1}, LX/EbE;-><init>()V

    .line 2144032
    new-array v2, v3, [LX/EYQ;

    invoke-static {v0, v2, v1}, LX/EYQ;->a([Ljava/lang/String;[LX/EYQ;LX/EWg;)V

    .line 2144033
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2144034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
