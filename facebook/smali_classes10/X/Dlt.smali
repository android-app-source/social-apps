.class public final LX/Dlt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2038690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;
    .locals 1

    .prologue
    .line 2038691
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->valueOf(Ljava/lang/String;)Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2038692
    invoke-static {p1}, LX/Dlt;->a(Landroid/os/Parcel;)Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2038693
    new-array v0, p1, [Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    move-object v0, v0

    .line 2038694
    return-object v0
.end method
