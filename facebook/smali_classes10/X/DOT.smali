.class public final LX/DOT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/DOa;


# direct methods
.method public constructor <init>(LX/DOa;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1992352
    iput-object p1, p0, LX/DOT;->c:LX/DOa;

    iput-object p2, p0, LX/DOT;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/DOT;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 1992353
    iget-object v0, p0, LX/DOT;->c:LX/DOa;

    iget-object v0, v0, LX/DOa;->b:LX/DOb;

    iget-object v1, p0, LX/DOT;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/DOT;->b:Landroid/content/Context;

    .line 1992354
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    iget-object v3, v0, LX/DOb;->l:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-virtual {p0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object p0

    .line 1992355
    const-string v3, "group_feed_id"

    invoke-static {v1}, LX/DOl;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1992356
    const-string v3, "target_fragment"

    sget-object p1, LX/0cQ;->GROUPS_DISCUSSION_TOPICS_ASSIGN_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {p0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1992357
    const-string v3, "groups_discussion_topics_story_id"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1992358
    iget-object v3, v0, LX/DOb;->o:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, p0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1992359
    const/4 v0, 0x1

    return v0
.end method
