.class public final LX/Cok;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cmj;


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;)V
    .locals 0

    .prologue
    .line 1936014
    iput-object p1, p0, LX/Cok;->a:Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/Cml;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1936015
    check-cast p1, Landroid/view/ViewGroup;

    .line 1936016
    if-eqz p2, :cond_0

    invoke-interface {p2}, LX/Cml;->b()LX/Cmr;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1936017
    :cond_0
    :goto_0
    return-void

    .line 1936018
    :cond_1
    invoke-interface {p2}, LX/Cml;->b()LX/Cmr;

    move-result-object v2

    .line 1936019
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1936020
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1936021
    invoke-virtual {v2}, LX/Cmr;->getGravity()I

    move-result v3

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1936022
    const v1, 0x7f0d2a36

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1936023
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1936024
    invoke-virtual {v2}, LX/Cmr;->getGravity()I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1936025
    sget-object v0, LX/Cmr;->RIGHT:LX/Cmr;

    if-ne v2, v0, :cond_2

    .line 1936026
    iget-object v0, p0, LX/Cok;->a:Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1936027
    :goto_1
    iget-object v0, p0, LX/Cok;->a:Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936028
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936029
    invoke-virtual {v2}, LX/Cmr;->getGravity()I

    move-result v1

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    .line 1936030
    iget-object v0, p0, LX/Cok;->a:Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-static {v0}, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;)V

    goto :goto_0

    .line 1936031
    :cond_2
    iget-object v0, p0, LX/Cok;->a:Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setLayoutDirection(I)V

    goto :goto_1
.end method
