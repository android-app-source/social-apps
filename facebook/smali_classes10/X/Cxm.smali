.class public LX/Cxm;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Cxl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951959
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1951960
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;LX/CzA;LX/CyE;Ljava/lang/String;Landroid/content/Context;LX/CzA;LX/1PT;LX/CxF;Ljava/lang/Runnable;LX/1PY;LX/CzA;LX/CzA;Lcom/facebook/search/results/model/SearchResultsMutableContext;)LX/Cxl;
    .locals 46

    .prologue
    .line 1951961
    new-instance v1, LX/Cxl;

    const-class v2, LX/Cx4;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/Cx4;

    invoke-static/range {p0 .. p0}, LX/1Pz;->a(LX/0QB;)LX/1Pz;

    move-result-object v17

    check-cast v17, LX/1Pz;

    invoke-static/range {p0 .. p0}, LX/1Q0;->a(LX/0QB;)LX/1Q0;

    move-result-object v18

    check-cast v18, LX/1Q0;

    const-class v2, LX/1Q1;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/1Q1;

    const-class v2, LX/Cxx;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/Cxx;

    const-class v2, LX/CxE;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/CxE;

    invoke-static/range {p0 .. p0}, LX/1Q2;->a(LX/0QB;)LX/1Q2;

    move-result-object v22

    check-cast v22, LX/1Q2;

    const-class v2, LX/CyG;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/CyG;

    invoke-static/range {p0 .. p0}, LX/1Q3;->a(LX/0QB;)LX/1Q3;

    move-result-object v24

    check-cast v24, LX/1Q3;

    invoke-static/range {p0 .. p0}, LX/1Q4;->a(LX/0QB;)LX/1Q4;

    move-result-object v25

    check-cast v25, LX/1Q4;

    const-class v2, LX/1Q5;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/1Q5;

    const-class v2, LX/1Q6;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/1Q6;

    invoke-static/range {p0 .. p0}, LX/1Q7;->a(LX/0QB;)LX/1Q7;

    move-result-object v28

    check-cast v28, LX/1Q7;

    const-class v2, LX/CxM;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/CxM;

    const-class v2, LX/1QA;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v30

    check-cast v30, LX/1QA;

    const-class v2, LX/CxO;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v31

    check-cast v31, LX/CxO;

    invoke-static/range {p0 .. p0}, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a(LX/0QB;)Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    move-result-object v32

    check-cast v32, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    const-class v2, LX/1QC;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v33

    check-cast v33, LX/1QC;

    invoke-static/range {p0 .. p0}, LX/1QD;->a(LX/0QB;)LX/1QD;

    move-result-object v34

    check-cast v34, LX/1QD;

    const-class v2, LX/1QE;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v35

    check-cast v35, LX/1QE;

    invoke-static/range {p0 .. p0}, LX/1QF;->a(LX/0QB;)LX/1QF;

    move-result-object v36

    check-cast v36, LX/1QF;

    invoke-static/range {p0 .. p0}, LX/1QG;->a(LX/0QB;)LX/1QG;

    move-result-object v37

    check-cast v37, LX/1QG;

    invoke-static/range {p0 .. p0}, LX/99Q;->a(LX/0QB;)LX/99Q;

    move-result-object v38

    check-cast v38, LX/99Q;

    const-class v2, LX/1QI;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v39

    check-cast v39, LX/1QI;

    const-class v2, LX/CxZ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v40

    check-cast v40, LX/CxZ;

    const-class v2, LX/CxT;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v41

    check-cast v41, LX/CxT;

    const-class v2, LX/CxX;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v42

    check-cast v42, LX/CxX;

    invoke-static/range {p0 .. p0}, LX/Cxb;->a(LX/0QB;)LX/Cxb;

    move-result-object v43

    check-cast v43, LX/Cxb;

    const-class v2, LX/CyK;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v44

    check-cast v44, LX/CyK;

    invoke-static/range {p0 .. p0}, LX/1QK;->a(LX/0QB;)LX/1QK;

    move-result-object v45

    check-cast v45, LX/1QK;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v1 .. v45}, LX/Cxl;-><init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;LX/CzA;LX/CyE;Ljava/lang/String;Landroid/content/Context;LX/CzA;LX/1PT;LX/CxF;Ljava/lang/Runnable;LX/1PY;LX/CzA;LX/CzA;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Cx4;LX/1Pz;LX/1Q0;LX/1Q1;LX/Cxx;LX/CxE;LX/1Q2;LX/CyG;LX/1Q3;LX/1Q4;LX/1Q5;LX/1Q6;LX/1Q7;LX/CxM;LX/1QA;LX/CxO;Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;LX/1QC;LX/1QD;LX/1QE;LX/1QF;LX/1QG;LX/99Q;LX/1QI;LX/CxZ;LX/CxT;LX/CxX;LX/Cxb;LX/CyK;LX/1QK;)V

    .line 1951962
    return-object v1
.end method
