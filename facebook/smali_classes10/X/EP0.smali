.class public LX/EP0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EOy;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EP1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2115624
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EP0;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EP1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115625
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2115626
    iput-object p1, p0, LX/EP0;->b:LX/0Ot;

    .line 2115627
    return-void
.end method

.method public static a(LX/0QB;)LX/EP0;
    .locals 4

    .prologue
    .line 2115628
    const-class v1, LX/EP0;

    monitor-enter v1

    .line 2115629
    :try_start_0
    sget-object v0, LX/EP0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115630
    sput-object v2, LX/EP0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115631
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115632
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115633
    new-instance v3, LX/EP0;

    const/16 p0, 0x347a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EP0;-><init>(LX/0Ot;)V

    .line 2115634
    move-object v0, v3

    .line 2115635
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115636
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EP0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115637
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115638
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2115639
    check-cast p2, LX/EOz;

    .line 2115640
    iget-object v0, p0, LX/EP0;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EP1;

    iget-object v1, p2, LX/EOz;->a:LX/A3T;

    .line 2115641
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    .line 2115642
    iget-object v5, v0, LX/EP1;->a:LX/11S;

    sget-object v6, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-interface {v1}, LX/A3T;->W()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    invoke-interface {v5, v6, v7, v8}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v5

    .line 2115643
    const v6, 0x7f08114e

    invoke-virtual {p1, v6}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2115644
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, LX/A3T;->cs()LX/A4G;

    move-result-object v6

    invoke-interface {v6}, LX/A4G;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v3, v5

    .line 2115645
    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a010e

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    sget-object v3, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, v3}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x7

    const/4 v4, 0x6

    invoke-interface {v2, v3, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2115646
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2115647
    invoke-static {}, LX/1dS;->b()V

    .line 2115648
    const/4 v0, 0x0

    return-object v0
.end method
