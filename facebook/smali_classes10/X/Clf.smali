.class public final LX/Clf;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Clb;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/CharSequence;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentElementStyle;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentElementStyle;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1932724
    new-instance v0, LX/Clc;

    invoke-direct {v0}, LX/Clc;-><init>()V

    sput-object v0, LX/Clf;->e:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;LX/0Px;LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "LX/0Px",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentElementStyle;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentElementStyle;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1932718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932719
    iput-object p1, p0, LX/Clf;->a:Ljava/lang/CharSequence;

    .line 1932720
    iput-object p2, p0, LX/Clf;->b:LX/0Px;

    .line 1932721
    iput-object p4, p0, LX/Clf;->d:LX/0Px;

    .line 1932722
    iput-object p3, p0, LX/Clf;->c:LX/0Px;

    .line 1932723
    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 1932717
    iget-object v0, p0, LX/Clf;->c:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Clf;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
