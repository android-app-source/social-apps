.class public final LX/D2O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/1FJ",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/D2Q;


# direct methods
.method public constructor <init>(LX/D2Q;LX/0TF;)V
    .locals 0

    .prologue
    .line 1958620
    iput-object p1, p0, LX/D2O;->b:LX/D2Q;

    iput-object p2, p0, LX/D2O;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1958621
    :try_start_0
    iget-object v0, p0, LX/D2O;->b:LX/D2Q;

    const/4 v1, 0x0

    .line 1958622
    iput-boolean v1, v0, LX/D2Q;->f:Z

    .line 1958623
    iget-object v0, p0, LX/D2O;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1958624
    iget-object v0, p0, LX/D2O;->b:LX/D2Q;

    invoke-static {v0}, LX/D2Q;->b(LX/D2Q;)V

    .line 1958625
    return-void

    .line 1958626
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/D2O;->b:LX/D2Q;

    invoke-static {v1}, LX/D2Q;->b(LX/D2Q;)V

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1958627
    check-cast p1, LX/1FJ;

    .line 1958628
    iget-object v0, p0, LX/D2O;->b:LX/D2Q;

    const/4 v1, 0x0

    .line 1958629
    iput-boolean v1, v0, LX/D2Q;->f:Z

    .line 1958630
    iget-object v0, p0, LX/D2O;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1958631
    iget-object v0, p0, LX/D2O;->b:LX/D2Q;

    invoke-static {v0}, LX/D2Q;->b(LX/D2Q;)V

    .line 1958632
    return-void
.end method
