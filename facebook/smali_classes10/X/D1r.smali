.class public final LX/D1r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;)V
    .locals 0

    .prologue
    .line 1957443
    iput-object p1, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1957430
    iget-object v0, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    const v1, 0x7f03108c

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->setContentView(I)V

    .line 1957431
    iget-object v0, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1957432
    sget-object v1, LX/4gI;->VIDEO_ONLY:LX/4gI;

    invoke-static {v0, v1}, LX/D2E;->a(Landroid/content/ContentResolver;LX/4gI;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1957433
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1957434
    :goto_0
    iget-object v1, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    invoke-static {v1, v0}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->a(Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;Landroid/net/Uri;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1957435
    iget-object v1, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    invoke-static {v1, v0}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->b(Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;Landroid/support/v4/app/Fragment;)V

    .line 1957436
    iget-object v0, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->y:LX/BQj;

    .line 1957437
    iget-boolean v1, v0, LX/BQj;->d:Z

    move v0, v1

    .line 1957438
    if-nez v0, :cond_0

    .line 1957439
    iget-object v0, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->y:LX/BQj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/BQj;->a(I)V

    .line 1957440
    :cond_0
    iget-object v0, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->y:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->e()V

    .line 1957441
    return-void

    .line 1957442
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1957426
    iget-object v0, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->finish()V

    .line 1957427
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1957428
    iget-object v0, p0, LX/D1r;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->finish()V

    .line 1957429
    return-void
.end method
