.class public LX/EAV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3iK;

.field public final b:LX/0bH;

.field public final c:LX/1My;

.field public final d:LX/0SG;

.field public final e:LX/Ch5;

.field public final f:LX/1B1;

.field public g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/1B1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;LX/3iG;LX/0bH;LX/1My;LX/Ch5;LX/1B1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2085596
    iput-object p1, p0, LX/EAV;->d:LX/0SG;

    .line 2085597
    sget-object v0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {p2, v0}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v0

    iput-object v0, p0, LX/EAV;->a:LX/3iK;

    .line 2085598
    iput-object p3, p0, LX/EAV;->b:LX/0bH;

    .line 2085599
    iput-object p4, p0, LX/EAV;->c:LX/1My;

    .line 2085600
    iput-object p5, p0, LX/EAV;->e:LX/Ch5;

    .line 2085601
    iput-object p6, p0, LX/EAV;->f:LX/1B1;

    .line 2085602
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EAV;->g:LX/0am;

    .line 2085603
    return-void
.end method

.method public static a(LX/EAV;Lcom/facebook/graphql/model/GraphQLStory;LX/EA5;LX/EAW;)V
    .locals 9

    .prologue
    .line 2085604
    iget-object v0, p0, LX/EAV;->c:LX/1My;

    new-instance v7, LX/EAQ;

    invoke-direct {v7, p0, p3, p2}, LX/EAQ;-><init>(LX/EAV;LX/EAW;LX/EA5;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v8

    iget-object v1, p0, LX/EAV;->c:LX/1My;

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v2, p0, LX/EAV;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {p1}, LX/2vM;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0Rf;

    move-result-object v6

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    invoke-virtual {v0, v7, v8, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2085605
    return-void
.end method

.method public static b(LX/0QB;)LX/EAV;
    .locals 7

    .prologue
    .line 2085606
    new-instance v0, LX/EAV;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const-class v2, LX/3iG;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/3iG;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v4

    check-cast v4, LX/1My;

    invoke-static {p0}, LX/Ch5;->a(LX/0QB;)LX/Ch5;

    move-result-object v5

    check-cast v5, LX/Ch5;

    invoke-static {p0}, LX/1B1;->a(LX/0QB;)LX/1B1;

    move-result-object v6

    check-cast v6, LX/1B1;

    invoke-direct/range {v0 .. v6}, LX/EAV;-><init>(LX/0SG;LX/3iG;LX/0bH;LX/1My;LX/Ch5;LX/1B1;)V

    .line 2085607
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/EA5;LX/EAW;LX/EA0;)V
    .locals 8

    .prologue
    .line 2085608
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EAV;->g:LX/0am;

    .line 2085609
    iget-object v0, p0, LX/EAV;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B1;

    .line 2085610
    new-instance v1, LX/EAT;

    invoke-direct {v1, p0, p3}, LX/EAT;-><init>(LX/EAV;LX/EAW;)V

    move-object v1, v1

    .line 2085611
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2085612
    new-instance v1, LX/EAU;

    invoke-direct {v1, p0, p3, p2}, LX/EAU;-><init>(LX/EAV;LX/EAW;LX/EA5;)V

    move-object v1, v1

    .line 2085613
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2085614
    iget-object v1, p0, LX/EAV;->b:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2085615
    iget-object v0, p0, LX/EAV;->f:LX/1B1;

    .line 2085616
    new-instance v2, LX/EAR;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v2 .. v7}, LX/EAR;-><init>(LX/EAV;Ljava/lang/String;LX/EA5;LX/EAW;LX/EA0;)V

    move-object v1, v2

    .line 2085617
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2085618
    iget-object v0, p0, LX/EAV;->f:LX/1B1;

    .line 2085619
    new-instance v1, LX/EAS;

    invoke-direct {v1, p0, p1, p4}, LX/EAS;-><init>(LX/EAV;Ljava/lang/String;LX/EA0;)V

    move-object v1, v1

    .line 2085620
    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2085621
    iget-object v0, p0, LX/EAV;->f:LX/1B1;

    iget-object v1, p0, LX/EAV;->e:LX/Ch5;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2085622
    return-void
.end method
