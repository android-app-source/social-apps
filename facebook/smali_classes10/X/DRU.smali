.class public LX/DRU;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/Context;

.field public c:Ljava/lang/String;

.field public d:Z

.field public final e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1998289
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 1998290
    new-instance v0, LX/DRT;

    invoke-direct {v0, p0}, LX/DRT;-><init>(LX/DRU;)V

    iput-object v0, p0, LX/DRU;->e:Landroid/view/View$OnClickListener;

    .line 1998291
    iput-object p1, p0, LX/DRU;->b:Landroid/content/Context;

    .line 1998292
    const/4 v2, -0x2

    const/4 p1, 0x0

    .line 1998293
    const-class v0, LX/DRU;

    invoke-static {v0, p0}, LX/DRU;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1998294
    invoke-virtual {p0}, LX/DRU;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/DRU;->setTextColor(I)V

    .line 1998295
    const v0, 0x7f020c48

    invoke-virtual {p0, v0}, LX/DRU;->setBackgroundResource(I)V

    .line 1998296
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1998297
    invoke-virtual {p0}, LX/DRU;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1998298
    invoke-virtual {v0, p1, p1, v1, p1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1998299
    invoke-virtual {p0, v0}, LX/DRU;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1998300
    iget-object v0, p0, LX/DRU;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, LX/DRU;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1998301
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/DRU;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p0

    check-cast p0, LX/17W;

    iput-object p0, p1, LX/DRU;->a:LX/17W;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1998302
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1998303
    :goto_0
    return-void

    .line 1998304
    :cond_0
    invoke-virtual {p0, p1}, LX/DRU;->setText(Ljava/lang/CharSequence;)V

    .line 1998305
    iput-object p2, p0, LX/DRU;->c:Ljava/lang/String;

    .line 1998306
    iput-boolean p3, p0, LX/DRU;->d:Z

    goto :goto_0
.end method
