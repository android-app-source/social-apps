.class public LX/EeZ;
.super Landroid/app/DownloadManager$Request;
.source ""


# static fields
.field public static final a:Landroid/net/Uri;

.field public static b:Ljava/lang/reflect/Field;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2152907
    const-string v0, "http://facebook.com"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/EeZ;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2152908
    invoke-direct {p0, p1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 2152909
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 2152910
    invoke-direct {p0, p1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 2152911
    if-nez p2, :cond_0

    .line 2152912
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2152913
    :cond_0
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 2152914
    if-eqz v0, :cond_1

    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2152915
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can only download HTTP/HTTPS URIs: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2152916
    :cond_2
    :try_start_0
    sget-object v0, LX/EeZ;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2152917
    return-void

    .line 2152918
    :catch_0
    move-exception v0

    .line 2152919
    new-instance v1, LX/Eez;

    const-string v2, "dmrequestcompat_muri_set"

    const-string v3, "Unable to set the value of DownloadManager.Request.mUri via reflection."

    invoke-direct {v1, v2, v0, v3}, LX/Eez;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1
.end method

.method public static declared-synchronized a()V
    .locals 5

    .prologue
    .line 2152920
    const-class v1, LX/EeZ;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/EeZ;->b:Ljava/lang/reflect/Field;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 2152921
    :goto_0
    monitor-exit v1

    return-void

    .line 2152922
    :cond_0
    :try_start_1
    const-class v0, Landroid/app/DownloadManager$Request;

    const-string v2, "mUri"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 2152923
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2152924
    sput-object v0, LX/EeZ;->b:Ljava/lang/reflect/Field;
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2152925
    :catch_0
    move-exception v0

    .line 2152926
    :try_start_2
    new-instance v2, LX/Eez;

    const-string v3, "dmrequestcompat_muri_get"

    const-string v4, "Unable to find the DownloadManager.Request.mUri field via reflection."

    invoke-direct {v2, v3, v0, v4}, LX/Eez;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2152927
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final setNotificationVisibility(I)Landroid/app/DownloadManager$Request;
    .locals 2

    .prologue
    .line 2152928
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 2152929
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 2152930
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/EeZ;->setShowRunningNotification(Z)Landroid/app/DownloadManager$Request;

    move-result-object v0

    .line 2152931
    :goto_0
    return-object v0

    .line 2152932
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/EeZ;->setShowRunningNotification(Z)Landroid/app/DownloadManager$Request;

    move-result-object v0

    goto :goto_0

    .line 2152933
    :cond_1
    invoke-super {p0, p1}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    move-result-object v0

    goto :goto_0
.end method
