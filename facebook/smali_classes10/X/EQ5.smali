.class public final LX/EQ5;
.super LX/Cwh;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cwh",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2118200
    invoke-direct {p0}, LX/Cwh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118198
    iget-object v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 2118199
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2118192
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v0

    sget-object v1, LX/CwF;->local:LX/CwF;

    if-ne v0, v1, :cond_0

    .line 2118193
    const-string v0, "local"

    .line 2118194
    :goto_0
    return-object v0

    .line 2118195
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v0

    sget-object v1, LX/CwF;->local_category:LX/CwF;

    if-ne v0, v1, :cond_1

    .line 2118196
    const-string v0, "local_category"

    goto :goto_0

    .line 2118197
    :cond_1
    const-string v0, "keyword"

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/model/NullStateModuleSuggestionUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118191
    const-string v0, "null_state_module_suggestion"

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2118190
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[See More] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118187
    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "keyword"

    :goto_0
    return-object v0

    .line 2118188
    :cond_0
    iget-object v0, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 2118189
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118186
    const-string v0, "place"

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118181
    invoke-virtual {p1}, Lcom/facebook/search/model/SeeMoreResultPageUnit;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2118185
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[See More] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118183
    iget-object v0, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 2118184
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/TrendingTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118182
    const-string v0, "trending_topic"

    return-object v0
.end method
