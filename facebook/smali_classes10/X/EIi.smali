.class public final LX/EIi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EIk;


# direct methods
.method public constructor <init>(LX/EIk;)V
    .locals 0

    .prologue
    .line 2102283
    iput-object p1, p0, LX/EIi;->a:LX/EIk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x5ef21977

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2102269
    iget-object v1, p0, LX/EIi;->a:LX/EIk;

    iget-object v1, v1, LX/EIk;->c:LX/EBs;

    .line 2102270
    iget-object v3, v1, LX/EBs;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v3, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->R:Z

    if-eqz v3, :cond_0

    .line 2102271
    :goto_0
    const v1, -0x523f7cca

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2102272
    :cond_0
    iget-object v3, v1, LX/EBs;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    const/4 v6, 0x1

    .line 2102273
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x17

    if-lt v4, v5, :cond_1

    .line 2102274
    iget-object v4, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bg:LX/0i5;

    sget-object v5, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->N:[Ljava/lang/String;

    invoke-static {v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Ljava/lang/String;

    move-result-object v6

    .line 2102275
    const v7, 0x7f080011

    invoke-virtual {v3, v7}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2102276
    const p0, 0x7f0807de

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v7, p1, v1

    invoke-virtual {v3, p0, p1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    move-object v7, v7

    .line 2102277
    new-instance p0, LX/EBr;

    invoke-direct {p0, v3}, LX/EBr;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    invoke-virtual {v4, v5, v6, v7, p0}, LX/0i5;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Zx;)V

    .line 2102278
    :goto_1
    goto :goto_0

    .line 2102279
    :cond_1
    iput-boolean v6, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->R:Z

    .line 2102280
    invoke-static {v3, v6}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2102281
    iget-object v4, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    invoke-virtual {v4, v6}, LX/EDx;->k(Z)V

    .line 2102282
    invoke-static {v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_1
.end method
