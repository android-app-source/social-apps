.class public LX/EW0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EVz;


# instance fields
.field public a:Landroid/graphics/drawable/BitmapDrawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Landroid/graphics/Point;

.field public final e:LX/EW4;


# direct methods
.method public constructor <init>(LX/EW4;)V
    .locals 1

    .prologue
    .line 2129265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129266
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/EW0;->d:Landroid/graphics/Point;

    .line 2129267
    iput-object p1, p0, LX/EW0;->e:LX/EW4;

    .line 2129268
    return-void
.end method


# virtual methods
.method public final a(D)Landroid/graphics/Point;
    .locals 9

    .prologue
    .line 2129254
    iget-object v0, p0, LX/EW0;->c:Landroid/graphics/Rect;

    .line 2129255
    iget-object v1, p0, LX/EW0;->b:Landroid/graphics/Rect;

    .line 2129256
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, p1

    double-to-int v2, v2

    .line 2129257
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-double v4, v0

    mul-double/2addr v4, p1

    double-to-int v0, v4

    .line 2129258
    iget v3, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v4, v2, v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    .line 2129259
    iget v4, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int v5, v0, v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    .line 2129260
    iget-object v5, p0, LX/EW0;->b:Landroid/graphics/Rect;

    add-int v6, v3, v2

    add-int v7, v4, v0

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 2129261
    iget-object v3, p0, LX/EW0;->a:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, LX/EW0;->b:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2129262
    iget-object v3, p0, LX/EW0;->d:Landroid/graphics/Point;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    iput v2, v3, Landroid/graphics/Point;->x:I

    .line 2129263
    iget-object v2, p0, LX/EW0;->d:Landroid/graphics/Point;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, v2, Landroid/graphics/Point;->y:I

    .line 2129264
    iget-object v0, p0, LX/EW0;->d:Landroid/graphics/Point;

    return-object v0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 2129251
    iget-object v0, p0, LX/EW0;->b:Landroid/graphics/Rect;

    iget-object v1, p0, LX/EW0;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p1

    iget-object v2, p0, LX/EW0;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 2129252
    iget-object v0, p0, LX/EW0;->a:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, LX/EW0;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2129253
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2129248
    iget-object v0, p0, LX/EW0;->a:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 2129249
    iget-object v0, p0, LX/EW0;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 2129250
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 2129221
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2129222
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2129223
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2129224
    iget-object v3, p0, LX/EW0;->e:LX/EW4;

    invoke-virtual {v3}, LX/EW4;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2129225
    iget-object v3, p0, LX/EW0;->e:LX/EW4;

    invoke-virtual {v3}, LX/EW4;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-static {p1, v3}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2129226
    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 2129227
    invoke-static {p1, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2129228
    move-object v0, v0

    .line 2129229
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 2129230
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/EW0;->c:Landroid/graphics/Rect;

    .line 2129231
    new-instance v0, Landroid/graphics/Rect;

    iget-object v2, p0, LX/EW0;->c:Landroid/graphics/Rect;

    invoke-direct {v0, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, LX/EW0;->b:Landroid/graphics/Rect;

    .line 2129232
    iget-object v0, p0, LX/EW0;->b:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2129233
    iput-object v1, p0, LX/EW0;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 2129234
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2129247
    iget-object v0, p0, LX/EW0;->a:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2129244
    iget-object v0, p0, LX/EW0;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2129245
    const/4 v0, 0x0

    iput-object v0, p0, LX/EW0;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 2129246
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2129237
    iget-object v0, p0, LX/EW0;->b:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2129238
    new-instance v0, LX/EVw;

    invoke-direct {v0, p0}, LX/EVw;-><init>(LX/EW0;)V

    .line 2129239
    iget-object v1, p0, LX/EW0;->a:Landroid/graphics/drawable/BitmapDrawable;

    const-string v2, "bounds"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/EW0;->b:Landroid/graphics/Rect;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v0, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2129240
    new-instance v1, LX/EVx;

    invoke-direct {v1, p0}, LX/EVx;-><init>(LX/EW0;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2129241
    new-instance v1, LX/EVy;

    invoke-direct {v1, p0, p1}, LX/EVy;-><init>(LX/EW0;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2129242
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2129243
    return-void
.end method

.method public final c()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2129236
    iget-object v0, p0, LX/EW0;->c:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final d()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2129235
    iget-object v0, p0, LX/EW0;->b:Landroid/graphics/Rect;

    return-object v0
.end method
