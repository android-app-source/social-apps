.class public LX/Crk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/K2B;

.field public final c:LX/K29;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/richdocument/view/util/CompositeRecyclableViewFactory$RecyclableViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/K2B;LX/K29;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1941341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941342
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Crk;->d:Ljava/util/Map;

    .line 1941343
    iput-object p1, p0, LX/Crk;->a:Landroid/content/Context;

    .line 1941344
    iput-object p2, p0, LX/Crk;->b:LX/K2B;

    .line 1941345
    iput-object p3, p0, LX/Crk;->c:LX/K29;

    .line 1941346
    iget-object v0, p0, LX/Crk;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1941347
    iget-object v1, p0, LX/Crk;->d:Ljava/util/Map;

    const v2, 0x7f0d017b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, LX/Crj;

    new-instance v4, Lcom/facebook/widget/CustomViewGroup;

    iget-object p1, p0, LX/Crk;->a:Landroid/content/Context;

    invoke-direct {v4, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    sget p1, LX/CoL;->C:I

    iget-object p2, p0, LX/Crk;->a:Landroid/content/Context;

    iget-object p3, p0, LX/Crk;->b:LX/K2B;

    invoke-direct {v3, v4, p1, p2, p3}, LX/Crj;-><init>(Landroid/view/ViewGroup;ILandroid/content/Context;LX/K2B;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1941348
    iget-object v1, p0, LX/Crk;->d:Ljava/util/Map;

    const v2, 0x7f0311fc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, LX/Cri;

    new-instance v4, Lcom/facebook/widget/CustomViewGroup;

    iget-object p1, p0, LX/Crk;->a:Landroid/content/Context;

    invoke-direct {v4, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    sget p1, LX/CoL;->D:I

    const p2, 0x7f0311fc

    invoke-direct {v3, v4, p1, p2, v0}, LX/Cri;-><init>(Landroid/view/ViewGroup;IILandroid/view/LayoutInflater;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1941349
    iget-object v1, p0, LX/Crk;->d:Ljava/util/Map;

    const v2, 0x7f03120c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, LX/Cri;

    new-instance v4, Lcom/facebook/widget/CustomViewGroup;

    iget-object p1, p0, LX/Crk;->a:Landroid/content/Context;

    invoke-direct {v4, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    sget p1, LX/CoL;->E:I

    const p2, 0x7f03120c

    invoke-direct {v3, v4, p1, p2, v0}, LX/Cri;-><init>(Landroid/view/ViewGroup;IILandroid/view/LayoutInflater;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1941350
    iget-object v1, p0, LX/Crk;->c:LX/K29;

    invoke-virtual {v1}, LX/K29;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1941351
    iget-object v1, p0, LX/Crk;->d:Ljava/util/Map;

    .line 1941352
    const v3, 0x7f0311fa

    move v2, v3

    .line 1941353
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, LX/Cri;

    new-instance v4, Lcom/facebook/widget/CustomViewGroup;

    iget-object p1, p0, LX/Crk;->a:Landroid/content/Context;

    invoke-direct {v4, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    sget p1, LX/CoL;->E:I

    .line 1941354
    const p3, 0x7f0311fa

    move p2, p3

    .line 1941355
    invoke-direct {v3, v4, p1, p2, v0}, LX/Cri;-><init>(Landroid/view/ViewGroup;IILandroid/view/LayoutInflater;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1941356
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/Crk;
    .locals 6

    .prologue
    .line 1941357
    const-class v1, LX/Crk;

    monitor-enter v1

    .line 1941358
    :try_start_0
    sget-object v0, LX/Crk;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1941359
    sput-object v2, LX/Crk;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1941360
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941361
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1941362
    new-instance p0, LX/Crk;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/K2C;->b(LX/0QB;)LX/K2B;

    move-result-object v4

    check-cast v4, LX/K2B;

    invoke-static {v0}, LX/K2A;->b(LX/0QB;)LX/K29;

    move-result-object v5

    check-cast v5, LX/K29;

    invoke-direct {p0, v3, v4, v5}, LX/Crk;-><init>(Landroid/content/Context;LX/K2B;LX/K29;)V

    .line 1941363
    move-object v0, p0

    .line 1941364
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1941365
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Crk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1941366
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1941367
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 1941368
    iget-object v0, p0, LX/Crk;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941369
    iget-object v0, p0, LX/Crk;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Crh;

    .line 1941370
    iget-object v1, v0, LX/Crh;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1941371
    iget-object v1, v0, LX/Crh;->a:Ljava/util/List;

    iget-object p0, v0, LX/Crh;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-interface {v1, p0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CnP;

    .line 1941372
    :goto_0
    move-object v0, v1

    .line 1941373
    check-cast v0, Landroid/view/View;

    .line 1941374
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, LX/Crh;->b()LX/CnP;

    move-result-object v1

    goto :goto_0
.end method
