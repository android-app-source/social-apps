.class public final LX/ELP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/CxA;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;LX/CzL;LX/CxA;)V
    .locals 0

    .prologue
    .line 2108805
    iput-object p1, p0, LX/ELP;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    iput-object p2, p0, LX/ELP;->a:LX/CzL;

    iput-object p3, p0, LX/ELP;->b:LX/CxA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const v0, 0x160b98a

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2108806
    iget-object v0, p0, LX/ELP;->a:LX/CzL;

    .line 2108807
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2108808
    check-cast v0, LX/8d9;

    .line 2108809
    iget-object v1, p0, LX/ELP;->a:LX/CzL;

    invoke-static {v1}, LX/CzN;->c(LX/CzL;)LX/CzL;

    move-result-object v3

    .line 2108810
    iget-object v1, p0, LX/ELP;->a:LX/CzL;

    if-ne v1, v3, :cond_0

    .line 2108811
    const v0, 0x52d9de44

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2108812
    :goto_0
    return-void

    .line 2108813
    :cond_0
    new-instance v4, LX/ELU;

    invoke-interface {v0}, LX/8d9;->dW_()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v5}, LX/ELU;-><init>(Ljava/lang/String;Z)V

    .line 2108814
    iget-object v1, p0, LX/ELP;->b:LX/CxA;

    check-cast v1, LX/1Pr;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v1, v4, v5}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2108815
    iget-object v1, p0, LX/ELP;->b:LX/CxA;

    iget-object v4, p0, LX/ELP;->a:LX/CzL;

    invoke-interface {v1, v4, v3}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2108816
    iget-object v1, p0, LX/ELP;->b:LX/CxA;

    check-cast v1, LX/1Pq;

    invoke-interface {v1}, LX/1Pq;->iN_()V

    .line 2108817
    iget-object v1, p0, LX/ELP;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "apply_mutation_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, LX/8d9;->dW_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/ELN;

    invoke-direct {v5, p0, v0}, LX/ELN;-><init>(LX/ELP;LX/8d9;)V

    new-instance v0, LX/ELO;

    invoke-direct {v0, p0, v3}, LX/ELO;-><init>(LX/ELP;LX/CzL;)V

    invoke-virtual {v1, v4, v5, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2108818
    const v0, -0x6c7785fd

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method
