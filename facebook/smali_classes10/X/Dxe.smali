.class public final LX/Dxe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLAlbumsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Long;

.field public final synthetic b:LX/Dxg;


# direct methods
.method public constructor <init>(LX/Dxg;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2063102
    iput-object p1, p0, LX/Dxe;->b:LX/Dxg;

    iput-object p2, p0, LX/Dxe;->a:Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLAlbumsConnection;
    .locals 19
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLAlbumsConnection;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2063103
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2063104
    :cond_0
    const/4 v1, 0x0

    .line 2063105
    :goto_0
    return-object v1

    .line 2063106
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    .line 2063107
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel;->a()LX/0Px;

    move-result-object v4

    .line 2063108
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2063109
    const/4 v1, 0x0

    goto :goto_0

    .line 2063110
    :cond_2
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2063111
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_6

    .line 2063112
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel;

    .line 2063113
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v6

    .line 2063114
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel;->j()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;

    move-result-object v7

    .line 2063115
    const/4 v1, 0x0

    .line 2063116
    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;->j()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-lez v8, :cond_3

    .line 2063117
    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;->j()LX/0Px;

    move-result-object v1

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel$MediaEdgesModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel$MediaEdgesModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel$MediaEdgesModel$MediaEdgesNodeModel;

    move-result-object v1

    .line 2063118
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel$MediaEdgesModel$MediaEdgesNodeModel;->k()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 2063119
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel$MediaEdgesModel$MediaEdgesNodeModel;->k()LX/1vs;

    move-result-object v10

    iget-object v11, v10, LX/1vs;->a:LX/15i;

    iget v10, v10, LX/1vs;->b:I

    .line 2063120
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel$MediaEdgesModel$MediaEdgesNodeModel;->k()LX/1vs;

    move-result-object v12

    iget-object v13, v12, LX/1vs;->a:LX/15i;

    iget v12, v12, LX/1vs;->b:I

    .line 2063121
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel$MediaEdgesModel$MediaEdgesNodeModel;->k()LX/1vs;

    move-result-object v14

    iget-object v15, v14, LX/1vs;->a:LX/15i;

    iget v14, v14, LX/1vs;->b:I

    .line 2063122
    new-instance v16, LX/4Xy;

    invoke-direct/range {v16 .. v16}, LX/4Xy;-><init>()V

    new-instance v17, LX/2dc;

    invoke-direct/range {v17 .. v17}, LX/2dc;-><init>()V

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v9, v8, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, LX/2dc;->b(Ljava/lang/String;)LX/2dc;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v11, v10, v9}, LX/15i;->j(II)I

    move-result v9

    invoke-virtual {v8, v9}, LX/2dc;->b(I)LX/2dc;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v13, v12, v9}, LX/15i;->j(II)I

    move-result v9

    invoke-virtual {v8, v9}, LX/2dc;->a(I)LX/2dc;

    move-result-object v8

    invoke-virtual {v8}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, LX/4Xy;->m(Lcom/facebook/graphql/model/GraphQLImage;)LX/4Xy;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v15, v14, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/4Xy;->m(Ljava/lang/String;)LX/4Xy;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel$MediaEdgesModel$MediaEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, LX/4Xy;->d(Ljava/lang/String;)LX/4Xy;

    move-result-object v1

    invoke-virtual {v1}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    .line 2063123
    :cond_3
    if-nez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v8, v0, LX/Dxe;->a:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, LX/Dxe;->b:LX/Dxg;

    iget-object v9, v9, LX/Dxg;->c:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2063124
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->j()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 2063125
    new-instance v10, LX/4Vp;

    invoke-direct {v10}, LX/4Vp;-><init>()V

    invoke-virtual {v10, v1}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)LX/4Vp;

    move-result-object v1

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->OTHER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-virtual {v1, v10}, LX/4Vp;->a(Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;)LX/4Vp;

    move-result-object v1

    new-instance v10, LX/4XE;

    invoke-direct {v10}, LX/4XE;-><init>()V

    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel;->a()Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/photos/albums/protocols/AlbumListQueryModels$FBFamilyNonUserMemberAlbumsQueryModel$FamilyNonUserMembersModel$EdgesModel$NodeModel$FamilyTaggedMediasetModel$MediaModel;->a()I

    move-result v7

    invoke-virtual {v10, v7}, LX/4XE;->a(I)LX/4XE;

    move-result-object v7

    invoke-virtual {v7}, LX/4XE;->a()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;)LX/4Vp;

    move-result-object v1

    const/4 v7, 0x0

    invoke-virtual {v9, v8, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/4Vp;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/4Vp;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/4Vp;->a(Ljava/lang/String;)LX/4Vp;

    move-result-object v1

    const-string v6, "FamilyAlbum"

    invoke-virtual {v1, v6}, LX/4Vp;->b(Ljava/lang/String;)LX/4Vp;

    move-result-object v1

    invoke-virtual {v1}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 2063126
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2063127
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_1

    .line 2063128
    :cond_6
    new-instance v1, LX/4Vq;

    invoke-direct {v1}, LX/4Vq;-><init>()V

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Vq;->a(LX/0Px;)LX/4Vq;

    move-result-object v1

    invoke-virtual {v1}, LX/4Vq;->a()Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v1

    goto/16 :goto_0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2063129
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Dxe;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    move-result-object v0

    return-object v0
.end method
