.class public final LX/Ec0;
.super LX/EWj;
.source ""

# interfaces
.implements LX/Ebz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/Ec0;",
        ">;",
        "LX/Ebz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EcH;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EcH;",
            "LX/Ec4;",
            "LX/Ec3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2145189
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2145190
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ec0;->b:Ljava/util/List;

    .line 2145191
    invoke-direct {p0}, LX/Ec0;->n()V

    .line 2145192
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2145140
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2145141
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ec0;->b:Ljava/util/List;

    .line 2145142
    invoke-direct {p0}, LX/Ec0;->n()V

    .line 2145143
    return-void
.end method

.method private A()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EcH;",
            "LX/Ec4;",
            "LX/Ec3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2145144
    iget-object v1, p0, LX/Ec0;->c:LX/EZ2;

    if-nez v1, :cond_0

    .line 2145145
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/Ec0;->b:Ljava/util/List;

    iget v3, p0, LX/Ec0;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2145146
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2145147
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/Ec0;->c:LX/EZ2;

    .line 2145148
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ec0;->b:Ljava/util/List;

    .line 2145149
    :cond_0
    iget-object v0, p0, LX/Ec0;->c:LX/EZ2;

    return-object v0

    .line 2145150
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/Ec0;
    .locals 1

    .prologue
    .line 2145151
    instance-of v0, p1, LX/Ec1;

    if-eqz v0, :cond_0

    .line 2145152
    check-cast p1, LX/Ec1;

    invoke-virtual {p0, p1}, LX/Ec0;->a(LX/Ec1;)LX/Ec0;

    move-result-object p0

    .line 2145153
    :goto_0
    return-object p0

    .line 2145154
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/Ec0;
    .locals 4

    .prologue
    .line 2145155
    const/4 v2, 0x0

    .line 2145156
    :try_start_0
    sget-object v0, LX/Ec1;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ec1;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2145157
    if-eqz v0, :cond_0

    .line 2145158
    invoke-virtual {p0, v0}, LX/Ec0;->a(LX/Ec1;)LX/Ec0;

    .line 2145159
    :cond_0
    return-object p0

    .line 2145160
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2145161
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2145162
    check-cast v0, LX/Ec1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2145163
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2145164
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2145165
    invoke-virtual {p0, v1}, LX/Ec0;->a(LX/Ec1;)LX/Ec0;

    :cond_1
    throw v0

    .line 2145166
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 2145167
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2145168
    invoke-direct {p0}, LX/Ec0;->A()LX/EZ2;

    .line 2145169
    :cond_0
    return-void
.end method

.method public static u()LX/Ec0;
    .locals 1

    .prologue
    .line 2145170
    new-instance v0, LX/Ec0;

    invoke-direct {v0}, LX/Ec0;-><init>()V

    return-object v0
.end method

.method private w()LX/Ec0;
    .locals 2

    .prologue
    .line 2145171
    invoke-static {}, LX/Ec0;->u()LX/Ec0;

    move-result-object v0

    invoke-direct {p0}, LX/Ec0;->y()LX/Ec1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ec0;->a(LX/Ec1;)LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/Ec1;
    .locals 3

    .prologue
    .line 2145172
    new-instance v0, LX/Ec1;

    invoke-direct {v0, p0}, LX/Ec1;-><init>(LX/EWj;)V

    .line 2145173
    iget-object v1, p0, LX/Ec0;->c:LX/EZ2;

    if-nez v1, :cond_1

    .line 2145174
    iget v1, p0, LX/Ec0;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2145175
    iget-object v1, p0, LX/Ec0;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/Ec0;->b:Ljava/util/List;

    .line 2145176
    iget v1, p0, LX/Ec0;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/Ec0;->a:I

    .line 2145177
    :cond_0
    iget-object v1, p0, LX/Ec0;->b:Ljava/util/List;

    .line 2145178
    iput-object v1, v0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    .line 2145179
    :goto_0
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2145180
    return-object v0

    .line 2145181
    :cond_1
    iget-object v1, p0, LX/Ec0;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2145182
    iput-object v1, v0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    .line 2145183
    goto :goto_0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 2145184
    iget v0, p0, LX/Ec0;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 2145185
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/Ec0;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/Ec0;->b:Ljava/util/List;

    .line 2145186
    iget v0, p0, LX/Ec0;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ec0;->a:I

    .line 2145187
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2145188
    invoke-direct {p0, p1}, LX/Ec0;->d(LX/EWY;)LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2145137
    invoke-direct {p0, p1, p2}, LX/Ec0;->d(LX/EWd;LX/EYZ;)LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Ec1;)LX/Ec0;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2145193
    sget-object v1, LX/Ec1;->c:LX/Ec1;

    move-object v1, v1

    .line 2145194
    if-ne p1, v1, :cond_0

    .line 2145195
    :goto_0
    return-object p0

    .line 2145196
    :cond_0
    iget-object v1, p0, LX/Ec0;->c:LX/EZ2;

    if-nez v1, :cond_3

    .line 2145197
    iget-object v0, p1, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2145198
    iget-object v0, p0, LX/Ec0;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2145199
    iget-object v0, p1, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    iput-object v0, p0, LX/Ec0;->b:Ljava/util/List;

    .line 2145200
    iget v0, p0, LX/Ec0;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, LX/Ec0;->a:I

    .line 2145201
    :goto_1
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145202
    :cond_1
    :goto_2
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2145203
    :cond_2
    invoke-direct {p0}, LX/Ec0;->z()V

    .line 2145204
    iget-object v0, p0, LX/Ec0;->b:Ljava/util/List;

    iget-object v1, p1, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2145205
    :cond_3
    iget-object v1, p1, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2145206
    iget-object v1, p0, LX/Ec0;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2145207
    iget-object v1, p0, LX/Ec0;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2145208
    iput-object v0, p0, LX/Ec0;->c:LX/EZ2;

    .line 2145209
    iget-object v1, p1, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    iput-object v1, p0, LX/Ec0;->b:Ljava/util/List;

    .line 2145210
    iget v1, p0, LX/Ec0;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/Ec0;->a:I

    .line 2145211
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_4

    invoke-direct {p0}, LX/Ec0;->A()LX/EZ2;

    move-result-object v0

    :cond_4
    iput-object v0, p0, LX/Ec0;->c:LX/EZ2;

    goto :goto_2

    .line 2145212
    :cond_5
    iget-object v0, p0, LX/Ec0;->c:LX/EZ2;

    iget-object v1, p1, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_2
.end method

.method public final a(LX/EcH;)LX/Ec0;
    .locals 1

    .prologue
    .line 2145213
    iget-object v0, p0, LX/Ec0;->c:LX/EZ2;

    if-nez v0, :cond_1

    .line 2145214
    if-nez p1, :cond_0

    .line 2145215
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2145216
    :cond_0
    invoke-direct {p0}, LX/Ec0;->z()V

    .line 2145217
    iget-object v0, p0, LX/Ec0;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2145218
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145219
    :goto_0
    return-object p0

    .line 2145220
    :cond_1
    iget-object v0, p0, LX/Ec0;->c:LX/EZ2;

    invoke-virtual {v0, p1}, LX/EZ2;->a(LX/EWp;)LX/EZ2;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2145138
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2145139
    invoke-direct {p0, p1, p2}, LX/Ec0;->d(LX/EWd;LX/EYZ;)LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2145129
    invoke-direct {p0}, LX/Ec0;->w()LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2145128
    invoke-direct {p0, p1, p2}, LX/Ec0;->d(LX/EWd;LX/EYZ;)LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2145127
    invoke-direct {p0}, LX/Ec0;->w()LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2145126
    invoke-direct {p0, p1}, LX/Ec0;->d(LX/EWY;)LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2145125
    invoke-direct {p0}, LX/Ec0;->w()LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2145124
    sget-object v0, LX/Eck;->D:LX/EYn;

    const-class v1, LX/Ec1;

    const-class v2, LX/Ec0;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2145119
    sget-object v0, LX/Eck;->C:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2145130
    invoke-direct {p0}, LX/Ec0;->w()LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2145131
    invoke-direct {p0}, LX/Ec0;->y()LX/Ec1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2145132
    invoke-virtual {p0}, LX/Ec0;->l()LX/Ec1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2145133
    invoke-direct {p0}, LX/Ec0;->y()LX/Ec1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2145134
    invoke-virtual {p0}, LX/Ec0;->l()LX/Ec1;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Ec1;
    .locals 2

    .prologue
    .line 2145120
    invoke-direct {p0}, LX/Ec0;->y()LX/Ec1;

    move-result-object v0

    .line 2145121
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2145122
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2145123
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145135
    sget-object v0, LX/Ec1;->c:LX/Ec1;

    move-object v0, v0

    .line 2145136
    return-object v0
.end method
