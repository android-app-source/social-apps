.class public final LX/DKk;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TResultType;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DKo;


# direct methods
.method public constructor <init>(LX/DKo;)V
    .locals 0

    .prologue
    .line 1987467
    iput-object p1, p0, LX/DKk;->a:LX/DKo;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1987473
    iget-object v1, p0, LX/DKk;->a:LX/DKo;

    monitor-enter v1

    .line 1987474
    :try_start_0
    iget-object v0, p0, LX/DKk;->a:LX/DKo;

    iget-object v2, p0, LX/DKk;->a:LX/DKo;

    iget-object v2, v2, LX/DKo;->b:LX/DKg;

    invoke-static {v0, v2}, LX/DKo;->a$redex0(LX/DKo;LX/DKg;)V

    .line 1987475
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 1987476
    iget-object v0, p0, LX/DKk;->a:LX/DKo;

    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    invoke-static {v0, p1}, LX/DKo;->a$redex0(LX/DKo;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1987477
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1987468
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1987469
    iget-object v1, p0, LX/DKk;->a:LX/DKo;

    monitor-enter v1

    .line 1987470
    :try_start_0
    iget-object v0, p0, LX/DKk;->a:LX/DKo;

    invoke-static {v0}, LX/DKo;->d$redex0(LX/DKo;)V

    .line 1987471
    iget-object v0, p0, LX/DKk;->a:LX/DKo;

    invoke-static {v0, p1}, LX/DKo;->a$redex0(LX/DKo;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1987472
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
