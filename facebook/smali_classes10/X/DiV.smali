.class public LX/DiV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DiR;


# instance fields
.field private a:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 2032167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2032168
    iput-wide p1, p0, LX/DiV;->a:J

    .line 2032169
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2032170
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2032171
    const-string v1, "timestamp"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p0, LX/DiV;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2032172
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/DiR;)Z
    .locals 1

    .prologue
    .line 2032173
    instance-of v0, p1, LX/DiV;

    if-nez v0, :cond_0

    .line 2032174
    const/4 v0, 0x0

    .line 2032175
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
