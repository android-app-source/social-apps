.class public LX/EKf;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EKg;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EKf",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EKg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107035
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2107036
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/EKf;->b:LX/0Zi;

    .line 2107037
    iput-object p1, p0, LX/EKf;->a:LX/0Ot;

    .line 2107038
    return-void
.end method

.method public static a(LX/0QB;)LX/EKf;
    .locals 4

    .prologue
    .line 2107039
    const-class v1, LX/EKf;

    monitor-enter v1

    .line 2107040
    :try_start_0
    sget-object v0, LX/EKf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107041
    sput-object v2, LX/EKf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107044
    new-instance v3, LX/EKf;

    const/16 p0, 0x33d0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EKf;-><init>(LX/0Ot;)V

    .line 2107045
    move-object v0, v3

    .line 2107046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2107050
    check-cast p2, LX/EKe;

    .line 2107051
    iget-object v0, p0, LX/EKf;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/EKe;->a:LX/CzL;

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 2107052
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2107053
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aR()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v1

    .line 2107054
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2107055
    const/4 v1, 0x0

    .line 2107056
    :goto_0
    move-object v0, v1

    .line 2107057
    return-object v0

    .line 2107058
    :cond_0
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0822a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2107059
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0822a1

    new-array v4, p2, [Ljava/lang/Object;

    aput-object v1, v4, p0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2107060
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b0050

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0a008b

    invoke-virtual {v1, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    .line 2107061
    const v3, -0x7ee45b25

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2107062
    invoke-interface {v1, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    const v3, 0x7f0b010f

    invoke-interface {v1, p2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2107063
    invoke-static {}, LX/1dS;->b()V

    .line 2107064
    iget v0, p1, LX/1dQ;->b:I

    .line 2107065
    packed-switch v0, :pswitch_data_0

    .line 2107066
    :goto_0
    return-object v2

    .line 2107067
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2107068
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2107069
    check-cast v1, LX/EKe;

    .line 2107070
    iget-object v3, p0, LX/EKf;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EKg;

    iget-object p1, v1, LX/EKe;->a:LX/CzL;

    iget-object p2, v1, LX/EKe;->b:LX/CxP;

    invoke-virtual {v3, v0, p1, p2}, LX/EKg;->a(Landroid/view/View;LX/CzL;LX/CxP;)V

    .line 2107071
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x7ee45b25
        :pswitch_0
    .end packed-switch
.end method
