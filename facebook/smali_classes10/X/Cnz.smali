.class public LX/Cnz;
.super LX/CnT;
.source ""

# interfaces
.implements LX/Cny;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "LX/CpO;",
        "Lcom/facebook/richdocument/model/data/NativeAdBlockData;",
        ">;",
        "LX/Cny;"
    }
.end annotation


# instance fields
.field public d:LX/CjD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1m0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/CoM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/8bO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/CpO;


# direct methods
.method public constructor <init>(LX/CpO;)V
    .locals 7

    .prologue
    .line 1934704
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934705
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Cnz;

    invoke-static {v0}, LX/CjD;->a(LX/0QB;)LX/CjD;

    move-result-object v3

    check-cast v3, LX/CjD;

    invoke-static {v0}, LX/1m0;->a(LX/0QB;)LX/1m0;

    move-result-object v4

    check-cast v4, LX/1m0;

    invoke-static {v0}, LX/CoM;->a(LX/0QB;)LX/CoM;

    move-result-object v5

    check-cast v5, LX/CoM;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p1

    check-cast p1, LX/0Uh;

    invoke-static {v0}, LX/8bO;->b(LX/0QB;)LX/8bO;

    move-result-object v0

    check-cast v0, LX/8bO;

    iput-object v3, v2, LX/Cnz;->d:LX/CjD;

    iput-object v4, v2, LX/Cnz;->e:LX/1m0;

    iput-object v5, v2, LX/Cnz;->f:LX/CoM;

    iput-object v6, v2, LX/Cnz;->g:LX/03V;

    iput-object p1, v2, LX/Cnz;->h:LX/0Uh;

    iput-object v0, v2, LX/Cnz;->i:LX/8bO;

    .line 1934706
    return-void
.end method

.method public static a$redex0(LX/Cnz;Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1934604
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 1934605
    iget-object v0, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->r()V

    .line 1934606
    :goto_0
    return-void

    .line 1934607
    :cond_0
    if-eqz p1, :cond_4

    .line 1934608
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->t()Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    move-result-object v0

    invoke-static {v0}, LX/8bO;->a(Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;)LX/8bN;

    move-result-object v0

    .line 1934609
    :goto_1
    iget-object v2, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v2, v0}, LX/CpO;->a(LX/8bN;)V

    .line 1934610
    iget-object v2, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v2, v1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1934611
    iget-object v1, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v1}, LX/CpO;->o()V

    .line 1934612
    new-instance v11, LX/CmV;

    invoke-direct {v11}, LX/CmV;-><init>()V

    .line 1934613
    iput-object v0, v11, LX/CmV;->p:LX/8bN;

    .line 1934614
    if-eqz p1, :cond_5

    .line 1934615
    iget-object v1, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->s()Ljava/lang/String;

    move-result-object v2

    .line 1934616
    iput-object v2, v1, LX/CpO;->v:Ljava/lang/String;

    .line 1934617
    iget-object v1, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v1}, LX/CpO;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1934618
    iget-object v1, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v1}, LX/CpO;->f()V

    .line 1934619
    :cond_1
    sget-object v1, LX/Cnw;->a:[I

    invoke-virtual {v0}, LX/8bN;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1934620
    :cond_2
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->v()Ljava/lang/String;

    move-result-object v10

    .line 1934621
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->v()Ljava/lang/String;

    move-result-object v9

    .line 1934622
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->j()Ljava/lang/String;

    move-result-object v8

    .line 1934623
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->k()Ljava/lang/String;

    move-result-object v7

    .line 1934624
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->n()Ljava/lang/String;

    move-result-object v6

    .line 1934625
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->m()Ljava/lang/String;

    move-result-object v5

    .line 1934626
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 1934627
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->u()Ljava/lang/String;

    move-result-object v3

    .line 1934628
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1934629
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->p()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v1

    .line 1934630
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->w()Ljava/lang/String;

    move-result-object v0

    .line 1934631
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v12

    .line 1934632
    if-eqz v12, :cond_3

    .line 1934633
    invoke-interface {v12}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v12

    .line 1934634
    iput-object v12, v11, LX/CmV;->l:Ljava/lang/String;

    .line 1934635
    :cond_3
    :goto_3
    iput-object v10, v11, LX/CmV;->d:Ljava/lang/String;

    .line 1934636
    iput-object v9, v11, LX/CmV;->e:Ljava/lang/String;

    .line 1934637
    iput-object v8, v11, LX/CmV;->f:Ljava/lang/String;

    .line 1934638
    iput-object v7, v11, LX/CmV;->g:Ljava/lang/String;

    .line 1934639
    iput-object v6, v11, LX/CmV;->i:Ljava/lang/String;

    .line 1934640
    iput-object v5, v11, LX/CmV;->j:Ljava/lang/String;

    .line 1934641
    iput-object v4, v11, LX/CmV;->h:Ljava/lang/String;

    .line 1934642
    iput-object v3, v11, LX/CmV;->m:Ljava/lang/String;

    .line 1934643
    iput-object v2, v11, LX/CmV;->s:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1934644
    iput-object v1, v11, LX/CmV;->t:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 1934645
    iput-object v0, v11, LX/CmV;->n:Ljava/lang/String;

    .line 1934646
    iget-object v0, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v0, v11}, LX/CpO;->a(LX/CmV;)V

    .line 1934647
    iget-object v0, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->d()V

    goto/16 :goto_0

    .line 1934648
    :cond_4
    iget-object v0, p0, LX/Cnz;->j:LX/CpO;

    const/4 v2, 0x1

    .line 1934649
    iput-boolean v2, v0, LX/CpO;->A:Z

    .line 1934650
    move-object v0, v1

    goto/16 :goto_1

    .line 1934651
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBVideoModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->y()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->z()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->A()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v4

    iget-object v5, p0, LX/Cnz;->e:LX/1m0;

    iget-object v6, p0, LX/Cnz;->f:LX/CoM;

    iget-object v7, p0, LX/Cnz;->g:LX/03V;

    iget-object v8, p0, LX/Cnz;->h:LX/0Uh;

    const/16 v9, 0xa5

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, LX/0Uh;->a(IZ)Z

    move-result v8

    invoke-static/range {v0 .. v8}, LX/Cli;->a(LX/8Ys;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;LX/1m0;LX/CoM;LX/03V;Z)LX/Cli;

    move-result-object v0

    .line 1934652
    iput-object v0, v11, LX/CmV;->q:LX/Cli;

    .line 1934653
    goto/16 :goto_2

    .line 1934654
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 1934655
    if-eqz v0, :cond_2

    .line 1934656
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 1934657
    iput-object v0, v11, LX/CmV;->k:Ljava/lang/String;

    .line 1934658
    goto/16 :goto_2

    .line 1934659
    :pswitch_2
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdMultiShareObjectFragmentModel$ChildAdObjectsModel;

    move-result-object v0

    .line 1934660
    iput-object v0, v11, LX/CmV;->o:Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdMultiShareObjectFragmentModel$ChildAdObjectsModel;

    .line 1934661
    goto/16 :goto_2

    .line 1934662
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 1934663
    if-eqz v0, :cond_6

    .line 1934664
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 1934665
    iput-object v0, v11, LX/CmV;->k:Ljava/lang/String;

    .line 1934666
    :cond_6
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->s()Ljava/lang/String;

    move-result-object v10

    .line 1934667
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->r()Ljava/lang/String;

    move-result-object v9

    .line 1934668
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v8

    .line 1934669
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v7

    .line 1934670
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v6

    .line 1934671
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v5

    .line 1934672
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 1934673
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v3

    .line 1934674
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1934675
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v1

    .line 1934676
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;->t()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 9

    .prologue
    .line 1934677
    check-cast p1, LX/CmU;

    .line 1934678
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934679
    check-cast v0, LX/CpO;

    iput-object v0, p0, LX/Cnz;->j:LX/CpO;

    .line 1934680
    iget-object v0, p0, LX/Cnz;->j:LX/CpO;

    if-nez v0, :cond_0

    .line 1934681
    :goto_0
    return-void

    .line 1934682
    :cond_0
    iget-object v0, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->a()V

    .line 1934683
    iget-object v0, p0, LX/Cnz;->j:LX/CpO;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v1

    .line 1934684
    iput-object v1, v0, LX/CpO;->y:Ljava/lang/String;

    .line 1934685
    iget-object v0, p0, LX/Cnz;->j:LX/CpO;

    invoke-interface {p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Cod;->a(LX/Cml;)V

    .line 1934686
    iget-object v0, p0, LX/Cnz;->j:LX/CpO;

    .line 1934687
    iget-object v1, v0, LX/CpO;->i:LX/Ckv;

    iget-object v2, v0, LX/CpO;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/Ckv;->a(Ljava/lang/String;)V

    .line 1934688
    iget-boolean v0, p1, LX/CmU;->b:Z

    move v0, v0

    .line 1934689
    if-eqz v0, :cond_1

    .line 1934690
    const/4 v0, 0x0

    .line 1934691
    iget-object v1, p1, LX/CmU;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;

    move-object v1, v1

    .line 1934692
    invoke-static {p0, v0, v1}, LX/Cnz;->a$redex0(LX/Cnz;Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdFragmentModel$NativeTypedAdObjectModel;Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;)V

    goto :goto_0

    .line 1934693
    :cond_1
    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v0

    .line 1934694
    iget-object v1, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v1}, LX/CpO;->k()V

    .line 1934695
    iget-object v1, p0, LX/Cnz;->d:LX/CjD;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v3}, LX/CpO;->h()I

    move-result v3

    iget-object v4, p0, LX/Cnz;->j:LX/CpO;

    invoke-virtual {v4}, LX/CpO;->i()I

    move-result v4

    const/high16 p1, 0x41000000    # 8.0f

    .line 1934696
    iget-object v6, v1, LX/CjD;->a:LX/0tX;

    .line 1934697
    new-instance v5, LX/8az;

    invoke-direct {v5}, LX/8az;-><init>()V

    move-object v5, v5

    .line 1934698
    const-string v7, "adID"

    invoke-virtual {v5, v7, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v7, "imageWidth"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v7, "imageHeight"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v7, "adChoicesIconWidth"

    invoke-static {v2, p1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v7, "adChoicesIconHeight"

    invoke-static {v2, p1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v7, "scale"

    sget-object v8, LX/0wC;->NUMBER_1:LX/0wC;

    invoke-virtual {v5, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/8az;

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 1934699
    new-instance v6, LX/CjB;

    invoke-direct {v6, v1}, LX/CjB;-><init>(LX/CjD;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v7

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v1, v5

    .line 1934700
    new-instance v2, LX/Cnv;

    invoke-direct {v2, p0}, LX/Cnv;-><init>(LX/Cnz;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1934701
    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1934702
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1934703
    const/4 v0, 0x1

    return v0
.end method
