.class public LX/DHK;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DHL;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DHK",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DHL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981730
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1981731
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DHK;->b:LX/0Zi;

    .line 1981732
    iput-object p1, p0, LX/DHK;->a:LX/0Ot;

    .line 1981733
    return-void
.end method

.method public static a(LX/0QB;)LX/DHK;
    .locals 4

    .prologue
    .line 1981734
    const-class v1, LX/DHK;

    monitor-enter v1

    .line 1981735
    :try_start_0
    sget-object v0, LX/DHK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981736
    sput-object v2, LX/DHK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981737
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981738
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981739
    new-instance v3, LX/DHK;

    const/16 p0, 0x21cc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DHK;-><init>(LX/0Ot;)V

    .line 1981740
    move-object v0, v3

    .line 1981741
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981742
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DHK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981743
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981744
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1981745
    check-cast p2, LX/DHJ;

    .line 1981746
    iget-object v0, p0, LX/DHK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DHL;

    iget-object v2, p2, LX/DHJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/DHJ;->b:LX/1Pn;

    iget v4, p2, LX/DHJ;->c:I

    iget v5, p2, LX/DHJ;->d:I

    iget-boolean v6, p2, LX/DHJ;->e:Z

    iget v7, p2, LX/DHJ;->f:I

    move-object v1, p1

    const/4 p2, 0x0

    const/4 p1, 0x1

    .line 1981747
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-interface {v8, v9}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    iget-object v9, v0, LX/DHL;->b:LX/1xp;

    invoke-virtual {v9, v1, p2, v4}, LX/1xp;->a(LX/1De;II)LX/1xt;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/1xt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1xt;

    move-result-object v9

    check-cast v3, LX/1Po;

    invoke-virtual {v9, v3}, LX/1xt;->a(LX/1Po;)LX/1xt;

    move-result-object v9

    invoke-virtual {v9, v5}, LX/1xt;->i(I)LX/1xt;

    move-result-object v9

    invoke-virtual {v9, v6}, LX/1xt;->b(Z)LX/1xt;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    .line 1981748
    iget-object v8, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 1981749
    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v3, 0x0

    .line 1981750
    const-string v0, ""

    .line 1981751
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    .line 1981752
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1981753
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1981754
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->w()LX/0Px;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    .line 1981755
    :goto_0
    move-object v8, p0

    .line 1981756
    invoke-virtual {v10, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v10, 0x7f0b0904

    invoke-virtual {v8, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const v10, 0x1010212

    const p0, -0x6e685d

    invoke-static {v1, v10, p0}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v10

    invoke-virtual {v8, v10}, LX/1ne;->m(I)LX/1ne;

    move-result-object v8

    const v10, 0x7f0b091e

    invoke-virtual {v8, v10}, LX/1ne;->s(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, v10}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p1}, LX/1ne;->c(Z)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    invoke-interface {v8, p1, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    invoke-interface {v9, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    move-object v0, v8

    .line 1981757
    return-object v0

    .line 1981758
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1981759
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1981760
    invoke-static {}, LX/1dS;->b()V

    .line 1981761
    const/4 v0, 0x0

    return-object v0
.end method
