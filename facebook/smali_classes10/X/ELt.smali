.class public final LX/ELt;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ELu;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public final synthetic c:LX/ELu;


# direct methods
.method public constructor <init>(LX/ELu;)V
    .locals 1

    .prologue
    .line 2109929
    iput-object p1, p0, LX/ELt;->c:LX/ELu;

    .line 2109930
    move-object v0, p1

    .line 2109931
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2109932
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2109933
    const-string v0, "SearchResultsRelatedPagesHScrollCardFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2109934
    if-ne p0, p1, :cond_1

    .line 2109935
    :cond_0
    :goto_0
    return v0

    .line 2109936
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2109937
    goto :goto_0

    .line 2109938
    :cond_3
    check-cast p1, LX/ELt;

    .line 2109939
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2109940
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2109941
    if-eq v2, v3, :cond_0

    .line 2109942
    iget-object v2, p0, LX/ELt;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ELt;->a:LX/CzL;

    iget-object v3, p1, LX/ELt;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2109943
    goto :goto_0

    .line 2109944
    :cond_5
    iget-object v2, p1, LX/ELt;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2109945
    :cond_6
    iget-object v2, p0, LX/ELt;->b:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/ELt;->b:Ljava/lang/String;

    iget-object v3, p1, LX/ELt;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2109946
    goto :goto_0

    .line 2109947
    :cond_7
    iget-object v2, p1, LX/ELt;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
