.class public LX/Cvj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gT;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/CvL;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/CvL;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1949207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949208
    iput-object p1, p0, LX/Cvj;->a:Ljava/util/Map;

    .line 1949209
    return-void
.end method


# virtual methods
.method public final serialize(LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1949210
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1949211
    iget-object v0, p0, LX/Cvj;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1949212
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1949213
    const-string v2, "state"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1949214
    const-string v2, "time"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v2, v4, v5}, LX/0nX;->a(Ljava/lang/String;J)V

    .line 1949215
    invoke-virtual {p1}, LX/0nX;->g()V

    goto :goto_0

    .line 1949216
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1949217
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 2

    .prologue
    .line 1949218
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Serialization infrastructure does not support type serialization."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
