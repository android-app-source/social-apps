.class public LX/DpB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final lookup_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/DpM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2043621
    new-instance v0, LX/1sv;

    const-string v1, "BatchLookupPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpB;->b:LX/1sv;

    .line 2043622
    new-instance v0, LX/1sw;

    const-string v1, "lookup_ids"

    const/16 v2, 0xf

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpB;->c:LX/1sw;

    .line 2043623
    const/4 v0, 0x1

    sput-boolean v0, LX/DpB;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/DpM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2043624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2043625
    iput-object p1, p0, LX/DpB;->lookup_ids:Ljava/util/List;

    .line 2043626
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2043627
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2043628
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2043629
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2043630
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BatchLookupPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2043631
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043632
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043633
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043634
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043635
    const-string v4, "lookup_ids"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043636
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043637
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043638
    iget-object v0, p0, LX/DpB;->lookup_ids:Ljava/util/List;

    if-nez v0, :cond_3

    .line 2043639
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043640
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043641
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043642
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2043643
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 2043644
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 2043645
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 2043646
    :cond_3
    iget-object v0, p0, LX/DpB;->lookup_ids:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 2043647
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2043648
    iget-object v0, p0, LX/DpB;->lookup_ids:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2043649
    sget-object v0, LX/DpB;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043650
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/DpB;->lookup_ids:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2043651
    iget-object v0, p0, LX/DpB;->lookup_ids:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DpM;

    .line 2043652
    invoke-virtual {v0, p1}, LX/DpM;->a(LX/1su;)V

    goto :goto_0

    .line 2043653
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2043654
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2043655
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2043656
    if-nez p1, :cond_1

    .line 2043657
    :cond_0
    :goto_0
    return v0

    .line 2043658
    :cond_1
    instance-of v1, p1, LX/DpB;

    if-eqz v1, :cond_0

    .line 2043659
    check-cast p1, LX/DpB;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2043660
    if-nez p1, :cond_3

    .line 2043661
    :cond_2
    :goto_1
    move v0, v2

    .line 2043662
    goto :goto_0

    .line 2043663
    :cond_3
    iget-object v0, p0, LX/DpB;->lookup_ids:Ljava/util/List;

    if-eqz v0, :cond_6

    move v0, v1

    .line 2043664
    :goto_2
    iget-object v3, p1, LX/DpB;->lookup_ids:Ljava/util/List;

    if-eqz v3, :cond_7

    move v3, v1

    .line 2043665
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2043666
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043667
    iget-object v0, p0, LX/DpB;->lookup_ids:Ljava/util/List;

    iget-object v3, p1, LX/DpB;->lookup_ids:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 2043668
    goto :goto_1

    :cond_6
    move v0, v2

    .line 2043669
    goto :goto_2

    :cond_7
    move v3, v2

    .line 2043670
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2043671
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2043672
    sget-boolean v0, LX/DpB;->a:Z

    .line 2043673
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpB;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2043674
    return-object v0
.end method
