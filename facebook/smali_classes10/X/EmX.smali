.class public LX/EmX;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/EmY;",
        "LX/EmS;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/EmX;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2165937
    invoke-direct {p0}, LX/0b4;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/EmX;
    .locals 3

    .prologue
    .line 2165938
    sget-object v0, LX/EmX;->a:LX/EmX;

    if-nez v0, :cond_1

    .line 2165939
    const-class v1, LX/EmX;

    monitor-enter v1

    .line 2165940
    :try_start_0
    sget-object v0, LX/EmX;->a:LX/EmX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2165941
    if-eqz v2, :cond_0

    .line 2165942
    :try_start_1
    new-instance v0, LX/EmX;

    invoke-direct {v0}, LX/EmX;-><init>()V

    .line 2165943
    move-object v0, v0

    .line 2165944
    sput-object v0, LX/EmX;->a:LX/EmX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2165945
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2165946
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2165947
    :cond_1
    sget-object v0, LX/EmX;->a:LX/EmX;

    return-object v0

    .line 2165948
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2165949
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
