.class public LX/Dt3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLInterfaces$DefaultTimeRangeFields;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/TimeZone;

.field public final d:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2051035
    const-class v0, LX/Dt3;

    sput-object v0, LX/Dt3;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/TimeZone;LX/0SG;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLInterfaces$DefaultTimeRangeFields;",
            ">;",
            "Ljava/util/TimeZone;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2051036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051037
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2051038
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2051039
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2051040
    new-instance v1, LX/Dt1;

    invoke-direct {v1, p0}, LX/Dt1;-><init>(LX/Dt3;)V

    invoke-static {v5, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2051041
    const-wide/16 v1, -0x1

    .line 2051042
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v3, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    .line 2051043
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v7

    cmp-long v2, v7, v3

    if-lez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v3

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v7

    cmp-long v2, v3, v7

    if-gtz v2, :cond_1

    .line 2051044
    :cond_0
    sget-object v2, LX/Dt3;->a:Ljava/lang/Class;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got invalid hours struct! ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2051045
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v1

    move-wide v3, v1

    .line 2051046
    goto :goto_1

    .line 2051047
    :cond_2
    move-object v0, v5

    .line 2051048
    iput-object v0, p0, LX/Dt3;->b:Ljava/util/List;

    .line 2051049
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    iput-object v0, p0, LX/Dt3;->c:Ljava/util/TimeZone;

    .line 2051050
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v0, p0, LX/Dt3;->d:LX/0SG;

    .line 2051051
    return-void

    .line 2051052
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(JJJ)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2051053
    cmp-long v2, p0, p2

    if-gez v2, :cond_2

    .line 2051054
    cmp-long v2, p4, p0

    if-ltz v2, :cond_1

    cmp-long v2, p4, p2

    if-gtz v2, :cond_1

    .line 2051055
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2051056
    goto :goto_0

    .line 2051057
    :cond_2
    cmp-long v2, p4, p0

    if-gez v2, :cond_0

    cmp-long v2, p4, p2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/Dt3;J)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v8, 0x1

    .line 2051058
    iget-object v0, p0, LX/Dt3;->b:Ljava/util/List;

    iget-object v1, p0, LX/Dt3;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v0

    add-long/2addr v0, v8

    .line 2051059
    iget-object v2, p0, LX/Dt3;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    .line 2051060
    invoke-virtual {v6}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v2

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, LX/Dt3;->a(JJJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2051061
    :goto_1
    return-object v6

    .line 2051062
    :cond_0
    invoke-virtual {v6}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v0

    add-long/2addr v0, v8

    .line 2051063
    goto :goto_0

    .line 2051064
    :cond_1
    sget-object v0, LX/Dt3;->a:Ljava/lang/Class;

    const-string v1, "Timestamp didn\'t belong to any timerange!  This shouldn\'t be possible!"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2051065
    const/4 v6, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/Dt2;
    .locals 15

    .prologue
    .line 2051066
    iget-object v2, p0, LX/Dt3;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 2051067
    iget-object v6, p0, LX/Dt3;->c:Ljava/util/TimeZone;

    .line 2051068
    const-wide/16 v9, 0x3e8

    mul-long/2addr v9, v2

    invoke-virtual {v6, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v9

    div-int/lit16 v9, v9, 0x3e8

    int-to-long v9, v9

    .line 2051069
    const-wide/16 v11, 0x7080

    add-long/2addr v9, v2

    const-wide/32 v13, 0x93a80

    rem-long/2addr v9, v13

    add-long/2addr v9, v11

    move-wide v6, v9

    .line 2051070
    move-wide v2, v6

    .line 2051071
    move-wide v0, v2

    .line 2051072
    invoke-static {p0, v0, v1}, LX/Dt3;->b(LX/Dt3;J)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    move-result-object v4

    .line 2051073
    if-nez v4, :cond_0

    .line 2051074
    sget-object v2, LX/Dt2;->UNKNOWN:LX/Dt2;

    .line 2051075
    :goto_0
    move-object v0, v2

    .line 2051076
    return-object v0

    .line 2051077
    :cond_0
    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v2

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v4

    move-wide v6, v0

    invoke-static/range {v2 .. v7}, LX/Dt3;->a(JJJ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2051078
    sget-object v2, LX/Dt2;->OPEN:LX/Dt2;

    goto :goto_0

    .line 2051079
    :cond_1
    sget-object v2, LX/Dt2;->CLOSED:LX/Dt2;

    goto :goto_0
.end method

.method public final a(I)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLInterfaces$DefaultTimeRangeFields;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2051080
    add-int/lit8 v0, p1, -0x5

    add-int/lit8 v0, v0, 0x7

    rem-int/lit8 v0, v0, 0x7

    const v1, 0x15180

    mul-int/2addr v0, v1

    add-int/lit16 v1, v0, 0x7080

    .line 2051081
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2051082
    iget-object v0, p0, LX/Dt3;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    .line 2051083
    int-to-long v4, v1

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v6

    const-wide/32 v12, 0x15180

    const-wide/16 v10, 0x7080

    .line 2051084
    sub-long v8, v4, v10

    div-long/2addr v8, v12

    sub-long v10, v6, v10

    div-long/2addr v10, v12

    cmp-long v8, v8, v10

    if-nez v8, :cond_2

    const/4 v8, 0x1

    :goto_1
    move v4, v8

    .line 2051085
    if-eqz v4, :cond_0

    .line 2051086
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2051087
    :cond_1
    return-object v2

    :cond_2
    const/4 v8, 0x0

    goto :goto_1
.end method
