.class public final LX/E6P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E6t;

.field public final synthetic b:LX/E6Q;


# direct methods
.method public constructor <init>(LX/E6Q;LX/E6t;)V
    .locals 0

    .prologue
    .line 2080091
    iput-object p1, p0, LX/E6P;->b:LX/E6Q;

    iput-object p2, p0, LX/E6P;->a:LX/E6t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x115217c9

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2080092
    iget-object v1, p0, LX/E6P;->a:LX/E6t;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2080093
    iget-object v3, v1, LX/E6t;->c:LX/E6w;

    iget-object v4, v1, LX/E6t;->a:LX/E6Q;

    iget-object v5, v1, LX/E6t;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    .line 2080094
    invoke-virtual {v4}, LX/E6Q;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 2080095
    iget-object v7, v3, LX/Cfk;->d:Landroid/content/Context;

    move-object v7, v7

    .line 2080096
    const v8, 0x7f0400aa

    invoke-static {v7, v8}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v7

    .line 2080097
    const-wide/16 v8, 0x12c

    invoke-virtual {v7, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2080098
    new-instance v8, LX/E6v;

    invoke-direct {v8, v3, v6, v5}, LX/E6v;-><init>(LX/E6w;Landroid/view/ViewGroup;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)V

    invoke-virtual {v7, v8}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2080099
    invoke-virtual {v4, v7}, LX/E6Q;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2080100
    iget-object v3, v1, LX/E6t;->c:LX/E6w;

    iget-object v4, v1, LX/E6t;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    .line 2080101
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->t()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionPageInviteFriendToLikeAttachmentFieldsModel$InviteeModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->B()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$PageModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$PageModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 2080102
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2080103
    new-instance v7, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;

    invoke-direct {v7, v6, v5}, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2080104
    const-string v9, "sendPageLikeInviteParams"

    invoke-virtual {v8, v9, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2080105
    iget-object v7, v3, LX/E6w;->c:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0aG;

    const-string v9, "send_page_like_invite"

    const v4, 0x4b490a4d    # 1.3175373E7f

    invoke-static {v7, v9, v8, v4}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v7

    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    move-object v6, v7

    .line 2080106
    iget-object v5, v3, LX/E6w;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Sh;

    new-instance v7, LX/E6u;

    invoke-direct {v7, v3}, LX/E6u;-><init>(LX/E6w;)V

    invoke-virtual {v5, v6, v7}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2080107
    const v1, -0x5fb2ff98

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
