.class public LX/ERQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/ERR;",
        "Lcom/facebook/vault/model/FacebookVaultDevice;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0lp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2120858
    const-class v0, LX/ERQ;

    sput-object v0, LX/ERQ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2120859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120860
    iput-object p1, p0, LX/ERQ;->b:LX/0lp;

    .line 2120861
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2120862
    check-cast p1, LX/ERR;

    .line 2120863
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2120864
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "identifier_value"

    .line 2120865
    iget-object v2, p1, LX/ERR;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2120866
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120867
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "vault device GET: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2120868
    new-instance v0, LX/14N;

    const-string v1, "vaultDeviceGet"

    const-string v2, "GET"

    const-string v3, "me/vaultdevices"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2120869
    iget-object v0, p0, LX/ERQ;->b:LX/0lp;

    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 2120870
    const-class v1, Lcom/facebook/vault/protocol/VaultDeviceGetResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/protocol/VaultDeviceGetResult;

    .line 2120871
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/vault/protocol/VaultDeviceGetResult;->data:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/vault/protocol/VaultDeviceGetResult;->data:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2120872
    iget-object v0, v0, Lcom/facebook/vault/protocol/VaultDeviceGetResult;->data:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/model/FacebookVaultDevice;

    .line 2120873
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
