.class public final LX/DmS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;)V
    .locals 0

    .prologue
    .line 2039255
    iput-object p1, p0, LX/DmS;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x2

    const v1, -0x6cc3fc5

    invoke-static {v0, v5, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2039256
    iget-object v1, p0, LX/DmS;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->f:LX/Dih;

    iget-object v2, p0, LX/DmS;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    iget-object v3, p0, LX/DmS;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    iget-object v4, p0, LX/DmS;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-virtual {v1, v2, v3, v4}, LX/Dih;->d(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)V

    .line 2039257
    iget-object v1, p0, LX/DmS;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-ne v1, v5, :cond_0

    .line 2039258
    iget-object v1, p0, LX/DmS;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    invoke-static {v1}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->m(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;)V

    .line 2039259
    :goto_0
    const v1, -0x5eeb2423

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2039260
    :cond_0
    iget-object v1, p0, LX/DmS;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v2, p0, LX/DmS;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->c(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;Ljava/lang/String;)V

    goto :goto_0
.end method
