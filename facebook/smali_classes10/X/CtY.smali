.class public final enum LX/CtY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CtY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CtY;

.field public static final enum BYPASS_LIMITS:LX/CtY;

.field public static final enum HIGH:LX/CtY;

.field public static final enum LOW:LX/CtY;

.field public static final enum NORMAL:LX/CtY;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1945055
    new-instance v0, LX/CtY;

    const-string v1, "BYPASS_LIMITS"

    invoke-direct {v0, v1, v2}, LX/CtY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CtY;->BYPASS_LIMITS:LX/CtY;

    .line 1945056
    new-instance v0, LX/CtY;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v3}, LX/CtY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CtY;->HIGH:LX/CtY;

    .line 1945057
    new-instance v0, LX/CtY;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, LX/CtY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CtY;->NORMAL:LX/CtY;

    .line 1945058
    new-instance v0, LX/CtY;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v5}, LX/CtY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CtY;->LOW:LX/CtY;

    .line 1945059
    const/4 v0, 0x4

    new-array v0, v0, [LX/CtY;

    sget-object v1, LX/CtY;->BYPASS_LIMITS:LX/CtY;

    aput-object v1, v0, v2

    sget-object v1, LX/CtY;->HIGH:LX/CtY;

    aput-object v1, v0, v3

    sget-object v1, LX/CtY;->NORMAL:LX/CtY;

    aput-object v1, v0, v4

    sget-object v1, LX/CtY;->LOW:LX/CtY;

    aput-object v1, v0, v5

    sput-object v0, LX/CtY;->$VALUES:[LX/CtY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1945060
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CtY;
    .locals 1

    .prologue
    .line 1945061
    const-class v0, LX/CtY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CtY;

    return-object v0
.end method

.method public static values()[LX/CtY;
    .locals 1

    .prologue
    .line 1945062
    sget-object v0, LX/CtY;->$VALUES:[LX/CtY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CtY;

    return-object v0
.end method
