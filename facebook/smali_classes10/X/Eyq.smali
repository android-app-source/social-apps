.class public final LX/Eyq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 0

    .prologue
    .line 2186585
    iput-object p1, p0, LX/Eyq;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2186586
    iget-object v0, p0, LX/Eyq;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    const/4 v1, 0x0

    .line 2186587
    iput-object v1, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->az:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2186588
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186589
    const/4 v2, 0x0

    .line 2186590
    iget-object v0, p0, LX/Eyq;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    .line 2186591
    iput v2, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aA:I

    .line 2186592
    iget-object v0, p0, LX/Eyq;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->H:LX/0xB;

    sget-object v1, LX/12j;->FRIEND_REQUESTS:LX/12j;

    invoke-virtual {v0, v1, v2}, LX/0xB;->a(LX/12j;I)V

    .line 2186593
    iget-object v0, p0, LX/Eyq;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    const/4 v1, 0x0

    .line 2186594
    iput-object v1, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->az:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2186595
    return-void
.end method
