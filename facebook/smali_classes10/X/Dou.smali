.class public LX/Dou;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;

.field private static volatile f:LX/Dou;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2P3;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dok;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042717
    const-class v0, LX/Dou;

    sput-object v0, LX/Dou;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2Oy;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2P3;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Dok;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042719
    iput-object p1, p0, LX/Dou;->b:LX/0Ot;

    .line 2042720
    iput-object p2, p0, LX/Dou;->c:LX/0Ot;

    .line 2042721
    iput-object p3, p0, LX/Dou;->d:LX/0Ot;

    .line 2042722
    iput-object p4, p0, LX/Dou;->e:LX/0Or;

    .line 2042723
    return-void
.end method

.method public static a(LX/0QB;)LX/Dou;
    .locals 7

    .prologue
    .line 2042724
    sget-object v0, LX/Dou;->f:LX/Dou;

    if-nez v0, :cond_1

    .line 2042725
    const-class v1, LX/Dou;

    monitor-enter v1

    .line 2042726
    :try_start_0
    sget-object v0, LX/Dou;->f:LX/Dou;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2042727
    if-eqz v2, :cond_0

    .line 2042728
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2042729
    new-instance v3, LX/Dou;

    const/16 v4, 0xdc2

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xdc5

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x2a16

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, LX/Dou;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V

    .line 2042730
    move-object v0, v3

    .line 2042731
    sput-object v0, LX/Dou;->f:LX/Dou;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042732
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2042733
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2042734
    :cond_1
    sget-object v0, LX/Dou;->f:LX/Dou;

    return-object v0

    .line 2042735
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2042736
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/Dou;Ljava/lang/String;[B[B)V
    .locals 12
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedMethod"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2042737
    :try_start_0
    iget-object v0, p0, LX/Dou;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oy;

    invoke-virtual {v0, p2, p3}, LX/2Oy;->b([B[B)[B

    move-result-object v0

    .line 2042738
    if-nez v0, :cond_1

    .line 2042739
    sget-object v0, LX/Dou;->a:Ljava/lang/Class;

    const-string v1, "Decrypted salamander is null."

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2042740
    :cond_0
    :goto_0
    return-void

    .line 2042741
    :cond_1
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2042742
    iget-object v2, p0, LX/Dou;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/Dpn;->b([B)LX/DpY;

    move-result-object v0

    .line 2042743
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, v0, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 2042744
    iget-object v0, v0, LX/DpY;->body:LX/DpZ;

    invoke-virtual {v0}, LX/DpZ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DpA;

    .line 2042745
    iget-object v0, v0, LX/DpA;->download_fbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 2042746
    iget-object v0, p0, LX/Dou;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 2042747
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2042748
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2042749
    invoke-static {v0}, LX/6bn;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    .line 2042750
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v4, v5

    .line 2042751
    iget-object v0, p0, LX/Dou;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0, v1, v3}, LX/6bn;->a(Landroid/content/Context;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 2042752
    :try_start_1
    invoke-static {v4, v0}, LX/1t3;->c(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/48g; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/48f; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 2042753
    :catch_0
    move-exception v5

    .line 2042754
    :try_start_2
    sget-object v6, LX/Dou;->a:Ljava/lang/Class;

    const-string v7, "Failed to move encrypted attachment with FBID %s from %s to %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v3, 0x1

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v3

    const/4 v3, 0x2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v3

    invoke-static {v6, v5, v7, v8}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch LX/48g; {:try_start_2 .. :try_end_2} :catch_1
    .catch LX/48f; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 2042755
    :catch_1
    move-exception v0

    .line 2042756
    :goto_2
    sget-object v1, LX/Dou;->a:Ljava/lang/Class;

    const-string v2, "Failed to move encrypted message attachments for thead %s"

    new-array v3, v11, [Ljava/lang/Object;

    aput-object p1, v3, v10

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2042757
    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2042758
    iget-object v0, p0, LX/Dou;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oy;

    iget-object v1, p0, LX/Dou;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dok;

    invoke-static {p1, v0, v1}, LX/Dow;->a(Landroid/database/sqlite/SQLiteDatabase;LX/2Oy;LX/Dok;)Ljava/util/Map;

    move-result-object v1

    .line 2042759
    if-eqz v1, :cond_3

    .line 2042760
    const-string v0, "SELECT thread_key, encrypted_content FROM messages"

    invoke-virtual {p1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 2042761
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2042762
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2042763
    invoke-static {v3, v4}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 2042764
    const-string v0, "thread_key"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2042765
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 2042766
    const-string v6, "encrypted_content"

    invoke-virtual {v4, v6}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v4

    .line 2042767
    if-eqz v0, :cond_0

    if-eqz v4, :cond_0

    .line 2042768
    invoke-static {p0, v5, v0, v4}, LX/Dou;->a(LX/Dou;Ljava/lang/String;[B[B)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2042769
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042770
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v3, :cond_1

    if-eqz v1, :cond_5

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_2
    throw v0

    :cond_2
    if-eqz v3, :cond_3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2042771
    :cond_3
    const/4 v1, 0x0

    .line 2042772
    :try_start_3
    iget-object v0, p0, LX/Dou;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/6bn;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    .line 2042773
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_6
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 2042774
    :cond_4
    :goto_3
    const/4 v0, 0x1

    return v0

    .line 2042775
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 2042776
    :cond_6
    :try_start_4
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_4
    if-ge v0, v4, :cond_4

    aget-object v5, v3, v0

    .line 2042777
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_7

    .line 2042778
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_8

    .line 2042779
    sget-object v6, LX/Dou;->a:Ljava/lang/Class;

    const-string v7, "Tried to delete file %s from root encrypted attachments directory %s but it does not exist"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, p1

    const/4 v5, 0x1

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v8, v5

    invoke-static {v6, v7, v8}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2042780
    :cond_7
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2042781
    :cond_8
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_7

    .line 2042782
    sget-object v6, LX/Dou;->a:Ljava/lang/Class;

    const-string v7, "Failed to delete file %s from root encrypted attachments directory %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, p1

    const/4 v5, 0x1

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v8, v5

    invoke-static {v6, v7, v8}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_5

    .line 2042783
    :catch_2
    move-exception v0

    .line 2042784
    sget-object v2, LX/Dou;->a:Ljava/lang/Class;

    const-string v3, "Failed to delete remaining attachment files in root encrypted attachments directory"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method
