.class public LX/EVX;
.super LX/4or;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 2128350
    invoke-direct {p0, p1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2128351
    const-class v0, LX/EVX;

    invoke-static {v0, p0, p1}, LX/EVX;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 2128352
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/CharSequence;

    const-string v1, "server value"

    aput-object v1, v0, v3

    const-string v1, "20 seconds"

    aput-object v1, v0, v4

    const-string v1, "30 seconds"

    aput-object v1, v0, v5

    const-string v1, "60 seconds"

    aput-object v1, v0, v6

    const-string v1, "2 minutes"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "10 minutes"

    aput-object v2, v0, v1

    .line 2128353
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/CharSequence;

    const-string v2, "0"

    aput-object v2, v1, v3

    const-string v2, "20"

    aput-object v2, v1, v4

    const-string v2, "30"

    aput-object v2, v1, v5

    const-string v2, "60"

    aput-object v2, v1, v6

    const-string v2, "120"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "600"

    aput-object v3, v1, v2

    .line 2128354
    invoke-virtual {p0, v0}, LX/EVX;->setEntries([Ljava/lang/CharSequence;)V

    .line 2128355
    invoke-virtual {p0, v1}, LX/EVX;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2128356
    const-string v0, "0"

    invoke-virtual {p0, v0}, LX/EVX;->setDefaultValue(Ljava/lang/Object;)V

    .line 2128357
    sget-object v0, LX/EVV;->b:LX/0Tn;

    invoke-virtual {p0, v0}, LX/4or;->a(LX/0Tn;)V

    .line 2128358
    invoke-virtual {p0, v4}, LX/EVX;->setPersistent(Z)V

    .line 2128359
    const-string v0, "Videohome prefetching interval"

    invoke-virtual {p0, v0}, LX/EVX;->setTitle(Ljava/lang/CharSequence;)V

    .line 2128360
    const-string v0, "override the prefetch interval from server"

    invoke-virtual {p0, v0}, LX/EVX;->setSummary(Ljava/lang/CharSequence;)V

    .line 2128361
    new-instance v0, LX/EVW;

    invoke-direct {v0, p0}, LX/EVW;-><init>(LX/EVX;)V

    invoke-virtual {p0, v0}, LX/EVX;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2128362
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/EVX;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p0

    check-cast p0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p0, p1, LX/EVX;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method
