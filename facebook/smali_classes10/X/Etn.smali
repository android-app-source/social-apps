.class public final LX/Etn;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Eto;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Etm;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 2178621
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2178622
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "showGlyph"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "textSizeRes"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "rightMargin"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "friendModel"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "friendingLocation"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Etn;->b:[Ljava/lang/String;

    .line 2178623
    iput v3, p0, LX/Etn;->c:I

    .line 2178624
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Etn;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Etn;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Etn;LX/1De;IILX/Etm;)V
    .locals 1

    .prologue
    .line 2178617
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2178618
    iput-object p4, p0, LX/Etn;->a:LX/Etm;

    .line 2178619
    iget-object v0, p0, LX/Etn;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2178620
    return-void
.end method


# virtual methods
.method public final a(LX/2h7;)LX/Etn;
    .locals 2

    .prologue
    .line 2178614
    iget-object v0, p0, LX/Etn;->a:LX/Etm;

    iput-object p1, v0, LX/Etm;->e:LX/2h7;

    .line 2178615
    iget-object v0, p0, LX/Etn;->d:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2178616
    return-object p0
.end method

.method public final a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Etn;
    .locals 2

    .prologue
    .line 2178591
    iget-object v0, p0, LX/Etn;->a:LX/Etm;

    iput-object p1, v0, LX/Etm;->d:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2178592
    iget-object v0, p0, LX/Etn;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2178593
    return-object p0
.end method

.method public final a(Z)LX/Etn;
    .locals 2

    .prologue
    .line 2178625
    iget-object v0, p0, LX/Etn;->a:LX/Etm;

    iput-boolean p1, v0, LX/Etm;->a:Z

    .line 2178626
    iget-object v0, p0, LX/Etn;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2178627
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2178610
    invoke-super {p0}, LX/1X5;->a()V

    .line 2178611
    const/4 v0, 0x0

    iput-object v0, p0, LX/Etn;->a:LX/Etm;

    .line 2178612
    sget-object v0, LX/Eto;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2178613
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Eto;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2178600
    iget-object v1, p0, LX/Etn;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Etn;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Etn;->c:I

    if-ge v1, v2, :cond_2

    .line 2178601
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2178602
    :goto_0
    iget v2, p0, LX/Etn;->c:I

    if-ge v0, v2, :cond_1

    .line 2178603
    iget-object v2, p0, LX/Etn;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2178604
    iget-object v2, p0, LX/Etn;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2178605
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2178606
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2178607
    :cond_2
    iget-object v0, p0, LX/Etn;->a:LX/Etm;

    .line 2178608
    invoke-virtual {p0}, LX/Etn;->a()V

    .line 2178609
    return-object v0
.end method

.method public final h(I)LX/Etn;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 2178597
    iget-object v0, p0, LX/Etn;->a:LX/Etm;

    iput p1, v0, LX/Etm;->b:I

    .line 2178598
    iget-object v0, p0, LX/Etn;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2178599
    return-object p0
.end method

.method public final i(I)LX/Etn;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 2178594
    iget-object v0, p0, LX/Etn;->a:LX/Etm;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, LX/Etm;->c:I

    .line 2178595
    iget-object v0, p0, LX/Etn;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2178596
    return-object p0
.end method
