.class public LX/D2b;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0b3;

.field public final c:LX/D2T;

.field public final d:LX/BQP;

.field public final e:Landroid/os/Handler;

.field public final f:LX/D2X;

.field public final g:Ljava/lang/Runnable;

.field public final h:LX/D2a;

.field public final i:LX/D2Z;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0b3;LX/D2T;LX/BQP;Landroid/os/Handler;LX/D2X;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1958878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958879
    new-instance v0, Lcom/facebook/timeline/profilevideo/upload/ProfileVideoUploadListener$1;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/profilevideo/upload/ProfileVideoUploadListener$1;-><init>(LX/D2b;)V

    iput-object v0, p0, LX/D2b;->g:Ljava/lang/Runnable;

    .line 1958880
    new-instance v0, LX/D2a;

    invoke-direct {v0, p0}, LX/D2a;-><init>(LX/D2b;)V

    iput-object v0, p0, LX/D2b;->h:LX/D2a;

    .line 1958881
    new-instance v0, LX/D2Z;

    invoke-direct {v0, p0}, LX/D2Z;-><init>(LX/D2b;)V

    iput-object v0, p0, LX/D2b;->i:LX/D2Z;

    .line 1958882
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/D2b;->a:Ljava/lang/String;

    .line 1958883
    iput-object p2, p0, LX/D2b;->b:LX/0b3;

    .line 1958884
    iput-object p3, p0, LX/D2b;->c:LX/D2T;

    .line 1958885
    iput-object p4, p0, LX/D2b;->d:LX/BQP;

    .line 1958886
    iput-object p5, p0, LX/D2b;->e:Landroid/os/Handler;

    .line 1958887
    iput-object p6, p0, LX/D2b;->f:LX/D2X;

    .line 1958888
    return-void
.end method

.method public static b(LX/D2b;)V
    .locals 2

    .prologue
    .line 1958889
    iget-object v0, p0, LX/D2b;->b:LX/0b3;

    iget-object v1, p0, LX/D2b;->h:LX/D2a;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1958890
    iget-object v0, p0, LX/D2b;->b:LX/0b3;

    iget-object v1, p0, LX/D2b;->i:LX/D2Z;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1958891
    iget-object v0, p0, LX/D2b;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/D2b;->g:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1958892
    return-void
.end method
