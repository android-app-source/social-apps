.class public LX/E9U;
.super LX/3Tf;
.source ""

# interfaces
.implements LX/E9T;
.implements LX/2ht;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field private final d:LX/E9i;

.field private final e:LX/E9m;

.field private final f:LX/E9o;

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/E9M;",
            ">;"
        }
    .end annotation
.end field

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/E9i;LX/E9m;LX/E9o;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2084466
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2084467
    iput-object p1, p0, LX/E9U;->c:Landroid/content/res/Resources;

    .line 2084468
    iput-object p2, p0, LX/E9U;->d:LX/E9i;

    .line 2084469
    iput-object p3, p0, LX/E9U;->e:LX/E9m;

    .line 2084470
    iput-object p4, p0, LX/E9U;->f:LX/E9o;

    .line 2084471
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E9U;->g:Ljava/util/ArrayList;

    .line 2084472
    const/4 v0, 0x0

    iput v0, p0, LX/E9U;->h:I

    .line 2084473
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;LX/E9W;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2084456
    sget-object v0, LX/E9S;->a:[I

    invoke-virtual {p1}, LX/E9W;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2084457
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2084458
    :pswitch_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031560

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2084459
    :goto_0
    return-object v0

    .line 2084460
    :pswitch_1
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2084461
    :pswitch_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0311dc

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2084462
    :pswitch_3
    new-instance v0, Lcom/facebook/reviews/ui/UserPlacesToReviewView;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/reviews/ui/UserPlacesToReviewView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2084463
    :pswitch_4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030a36

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2084464
    :pswitch_5
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0311e6

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2084465
    :pswitch_6
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0311dd

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method

.method private d(II)LX/E9W;
    .locals 2

    .prologue
    .line 2084452
    iget-object v0, p0, LX/E9U;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9M;

    .line 2084453
    invoke-virtual {v0}, LX/E9M;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/E9M;->k()I

    move-result v1

    if-ne p2, v1, :cond_0

    .line 2084454
    invoke-virtual {v0}, LX/E9M;->l()LX/E9Q;

    move-result-object v0

    invoke-virtual {v0}, LX/E9Q;->a()LX/E9W;

    move-result-object v0

    .line 2084455
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p2}, LX/E9M;->a(I)LX/E9W;

    move-result-object v0

    goto :goto_0
.end method

.method private g(I)LX/E9W;
    .locals 1

    .prologue
    .line 2084451
    iget-object v0, p0, LX/E9U;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9M;

    invoke-virtual {v0}, LX/E9M;->i()LX/E9W;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2084450
    invoke-direct {p0, p1}, LX/E9U;->g(I)LX/E9W;

    move-result-object v0

    invoke-virtual {v0}, LX/E9W;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2084437
    invoke-direct {p0, p1, p2}, LX/E9U;->d(II)LX/E9W;

    move-result-object v0

    .line 2084438
    if-nez p4, :cond_0

    .line 2084439
    invoke-static {p5, v0}, LX/E9U;->a(Landroid/view/ViewGroup;LX/E9W;)Landroid/view/View;

    move-result-object v1

    .line 2084440
    :goto_0
    invoke-virtual {p0, p1, p2}, LX/E9U;->a(II)Ljava/lang/Object;

    move-result-object v2

    .line 2084441
    sget-object v3, LX/E9S;->a:[I

    invoke-virtual {v0}, LX/E9W;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2084442
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected child view type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2084443
    :pswitch_0
    iget-object v3, p0, LX/E9U;->e:LX/E9m;

    move-object v0, v1

    check-cast v0, Lcom/facebook/reviews/ui/ReviewFeedRowView;

    check-cast v2, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    invoke-virtual {v3, v0, v2}, LX/E9m;->a(Lcom/facebook/reviews/ui/ReviewFeedRowView;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)V

    .line 2084444
    :goto_1
    :pswitch_1
    return-object v1

    .line 2084445
    :pswitch_2
    iget-object v3, p0, LX/E9U;->e:LX/E9m;

    move-object v0, v1

    check-cast v0, Lcom/facebook/reviews/ui/ReviewFeedRowView;

    check-cast v2, Landroid/util/Pair;

    invoke-virtual {v3, v0, v2}, LX/E9m;->a(Lcom/facebook/reviews/ui/ReviewFeedRowView;Landroid/util/Pair;)V

    goto :goto_1

    .line 2084446
    :pswitch_3
    iget-object v3, p0, LX/E9U;->d:LX/E9i;

    move-object v0, v1

    check-cast v0, Lcom/facebook/reviews/ui/UserPlacesToReviewView;

    check-cast v2, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;

    invoke-virtual {v3, v0, v2}, LX/E9i;->a(Lcom/facebook/reviews/ui/UserPlacesToReviewView;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;)V

    goto :goto_1

    .line 2084447
    :pswitch_4
    iget-object v0, p0, LX/E9U;->f:LX/E9o;

    check-cast v2, LX/E9V;

    .line 2084448
    new-instance v3, LX/E9n;

    invoke-direct {v3, v0, v2}, LX/E9n;-><init>(LX/E9o;LX/E9V;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084449
    goto :goto_1

    :cond_0
    move-object v1, p4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2084410
    invoke-direct {p0, p1}, LX/E9U;->g(I)LX/E9W;

    move-result-object v0

    .line 2084411
    if-nez p2, :cond_0

    .line 2084412
    invoke-static {p3, v0}, LX/E9U;->a(Landroid/view/ViewGroup;LX/E9W;)Landroid/view/View;

    move-result-object v1

    .line 2084413
    :goto_0
    sget-object v2, LX/E9S;->a:[I

    invoke-virtual {v0}, LX/E9W;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2084414
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected header view type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    move-object v0, v1

    .line 2084415
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, LX/E9U;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E9M;

    invoke-virtual {v2}, LX/E9M;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2084416
    :pswitch_1
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2084433
    iget-object v0, p0, LX/E9U;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9M;

    .line 2084434
    invoke-virtual {v0}, LX/E9M;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/E9M;->k()I

    move-result v1

    if-ne p2, v1, :cond_0

    .line 2084435
    invoke-virtual {v0}, LX/E9M;->l()LX/E9Q;

    move-result-object v0

    .line 2084436
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p2}, LX/E9M;->b(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/E9M;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2084430
    iget-object v0, p0, LX/E9U;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2084431
    const v0, -0x30bf005c

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2084432
    return-void
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2084422
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 2084423
    invoke-virtual {p0, v0, p2, p3}, LX/E9U;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2084424
    const/4 p3, 0x0

    .line 2084425
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 p1, -0x1

    const/4 p2, -0x2

    invoke-direct {v1, p1, p2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2084426
    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->measure(II)V

    .line 2084427
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v1, v1

    .line 2084428
    iput v1, p0, LX/E9U;->h:I

    .line 2084429
    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2084474
    iget-object v0, p0, LX/E9U;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2084421
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2084420
    iget-object v0, p0, LX/E9U;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 2084418
    iget-object v0, p0, LX/E9U;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9M;

    .line 2084419
    invoke-virtual {v0}, LX/E9M;->k()I

    move-result v1

    invoke-virtual {v0}, LX/E9M;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(II)I
    .locals 1

    .prologue
    .line 2084417
    invoke-direct {p0, p1, p2}, LX/E9U;->d(II)LX/E9W;

    move-result-object v0

    invoke-virtual {v0}, LX/E9W;->ordinal()I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2084409
    iget-object v0, p0, LX/E9U;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2084403
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v1

    aget v1, v1, v0

    .line 2084404
    invoke-direct {p0, v1}, LX/E9U;->g(I)LX/E9W;

    move-result-object v1

    .line 2084405
    sget-object v2, LX/E9S;->a:[I

    invoke-virtual {v1}, LX/E9W;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2084406
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to get height for unhandled section type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2084407
    :pswitch_0
    iget v0, p0, LX/E9U;->h:I

    .line 2084408
    :pswitch_1
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2084401
    const v0, -0x3e5054af

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2084402
    return-void
.end method

.method public final f(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2084396
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/3Tf;->getCount()I

    move-result v2

    if-lt p1, v2, :cond_2

    :cond_0
    move v0, v1

    .line 2084397
    :cond_1
    :goto_0
    return v0

    .line 2084398
    :cond_2
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v2

    .line 2084399
    aget v2, v2, v0

    .line 2084400
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2084395
    invoke-static {}, LX/E9W;->values()[LX/E9W;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final o_(I)I
    .locals 2

    .prologue
    .line 2084393
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 2084394
    invoke-direct {p0, v0}, LX/E9U;->g(I)LX/E9W;

    move-result-object v0

    invoke-virtual {v0}, LX/E9W;->ordinal()I

    move-result v0

    return v0
.end method
