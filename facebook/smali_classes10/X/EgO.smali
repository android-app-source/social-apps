.class public final LX/EgO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/bookmark/model/Bookmark;

.field public final synthetic c:Lcom/facebook/bookmark/client/BookmarkClient;


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 0

    .prologue
    .line 2156064
    iput-object p1, p0, LX/EgO;->c:Lcom/facebook/bookmark/client/BookmarkClient;

    iput-object p2, p0, LX/EgO;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/EgO;->b:Lcom/facebook/bookmark/model/Bookmark;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2156056
    iget-object v0, p0, LX/EgO;->c:Lcom/facebook/bookmark/client/BookmarkClient;

    iget-object v1, p0, LX/EgO;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    .line 2156057
    iget-object v1, v0, Lcom/facebook/bookmark/client/BookmarkClient;->i:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2156058
    const-string v1, "handleAddToFavoritesFailure"

    invoke-static {v0, p1, v1}, Lcom/facebook/bookmark/client/BookmarkClient;->a$redex0(Lcom/facebook/bookmark/client/BookmarkClient;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2156059
    iget-object v0, p0, LX/EgO;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2156060
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2156061
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2156062
    iget-object v0, p0, LX/EgO;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0xdd74abc

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2156063
    return-void
.end method
