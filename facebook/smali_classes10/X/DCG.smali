.class public final LX/DCG;
.super LX/1Yy;
.source ""


# instance fields
.field public final synthetic a:LX/DCI;


# direct methods
.method public constructor <init>(LX/DCI;)V
    .locals 0

    .prologue
    .line 1973863
    iput-object p1, p0, LX/DCG;->a:LX/DCI;

    invoke-direct {p0}, LX/1Yy;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 10

    .prologue
    .line 1973864
    check-cast p1, LX/1Zj;

    const/4 v7, 0x0

    .line 1973865
    iget-object v0, p1, LX/1Zj;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1973866
    iget-object v0, p0, LX/DCG;->a:LX/DCI;

    iget-object v0, v0, LX/DCI;->e:LX/0qq;

    iget-object v1, p1, LX/1Zj;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qq;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1973867
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1973868
    iget-object v0, p0, LX/DCG;->a:LX/DCI;

    iget-object v1, v0, LX/DCI;->b:LX/967;

    iget-boolean v3, p1, LX/1Zj;->e:Z

    iget-wide v4, p1, LX/1Zj;->g:J

    iget-object v6, p1, LX/1Zj;->h:Ljava/lang/String;

    iget-object v0, p0, LX/DCG;->a:LX/DCI;

    iget-object v8, v0, LX/DCI;->g:LX/1L9;

    invoke-virtual/range {v1 .. v8}, LX/967;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ZJLjava/lang/String;Ljava/lang/String;LX/1L9;)V

    .line 1973869
    :cond_0
    return-void

    .line 1973870
    :cond_1
    iget-object v0, p0, LX/DCG;->a:LX/DCI;

    iget-object v1, v0, LX/DCI;->e:LX/0qq;

    iget-object v0, p1, LX/1Zj;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p1, LX/1Zj;->a:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, LX/0qq;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1973871
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1973872
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    .line 1973873
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1973874
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iget-object v1, p1, LX/1Zj;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1973875
    if-eqz v2, :cond_2

    .line 1973876
    iget-object v0, p0, LX/DCG;->a:LX/DCI;

    iget-object v1, v0, LX/DCI;->b:LX/967;

    iget-boolean v3, p1, LX/1Zj;->e:Z

    iget-wide v4, p1, LX/1Zj;->g:J

    iget-object v6, p1, LX/1Zj;->h:Ljava/lang/String;

    iget-object v0, p0, LX/DCG;->a:LX/DCI;

    iget-object v8, v0, LX/DCI;->g:LX/1L9;

    invoke-virtual/range {v1 .. v8}, LX/967;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ZJLjava/lang/String;Ljava/lang/String;LX/1L9;)V

    goto :goto_1

    .line 1973877
    :cond_3
    iget-object v0, p1, LX/1Zj;->d:Ljava/lang/String;

    goto :goto_0
.end method
