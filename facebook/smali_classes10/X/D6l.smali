.class public final LX/D6l;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3H6;


# direct methods
.method public constructor <init>(LX/3H6;)V
    .locals 0

    .prologue
    .line 1966128
    iput-object p1, p0, LX/D6l;->a:LX/3H6;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1966127
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1966116
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1966117
    if-eqz p1, :cond_0

    .line 1966118
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1966119
    if-nez v0, :cond_1

    .line 1966120
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Fetched story was non-existent"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/D6l;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1966121
    :goto_0
    return-void

    .line 1966122
    :cond_1
    iget-object v1, p0, LX/D6l;->a:LX/3H6;

    iget-object v0, p0, LX/D6l;->a:LX/3H6;

    iget-object v2, v0, LX/3H6;->c:LX/189;

    .line 1966123
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1966124
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v0}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1966125
    iput-object v0, v1, LX/3H6;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1966126
    goto :goto_0
.end method
