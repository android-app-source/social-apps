.class public LX/D6v;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final h:Ljava/lang/String;


# instance fields
.field private A:LX/BSg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Z

.field public C:Z

.field private D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:LX/BST;

.field public F:LX/BSR;

.field public G:LX/04D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:LX/2oN;

.field public b:LX/2oN;

.field public c:J

.field public d:I

.field public e:Z

.field public f:J

.field public g:I

.field public final i:Ljava/lang/String;

.field public final j:LX/2ml;

.field private final k:LX/3I1;

.field public final l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

.field public final m:LX/D6u;

.field public final n:LX/0So;

.field public final o:LX/3H4;

.field private final p:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

.field private final q:LX/1C2;

.field public r:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/3GF;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/D6t;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field public u:J

.field public v:J

.field public w:LX/3H0;

.field public x:Z

.field private y:Z

.field public z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1966365
    const-class v0, LX/D6v;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/D6v;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/2ml;LX/3I1;Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;LX/0So;LX/3H4;Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;LX/1C2;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    .line 1966366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1966367
    sget-object v0, LX/2oN;->NONE:LX/2oN;

    iput-object v0, p0, LX/D6v;->a:LX/2oN;

    .line 1966368
    sget-object v0, LX/2oN;->NONE:LX/2oN;

    iput-object v0, p0, LX/D6v;->b:LX/2oN;

    .line 1966369
    iput-wide v2, p0, LX/D6v;->c:J

    .line 1966370
    const/4 v0, -0x1

    iput v0, p0, LX/D6v;->d:I

    .line 1966371
    iput-wide v2, p0, LX/D6v;->f:J

    .line 1966372
    iput-wide v2, p0, LX/D6v;->u:J

    .line 1966373
    iput-wide v2, p0, LX/D6v;->v:J

    .line 1966374
    iput-wide v2, p0, LX/D6v;->z:J

    .line 1966375
    sget-object v0, LX/BST;->NONE:LX/BST;

    iput-object v0, p0, LX/D6v;->E:LX/BST;

    .line 1966376
    sget-object v0, LX/BSR;->NONE:LX/BSR;

    iput-object v0, p0, LX/D6v;->F:LX/BSR;

    .line 1966377
    iput-object p1, p0, LX/D6v;->i:Ljava/lang/String;

    .line 1966378
    iput-object p2, p0, LX/D6v;->j:LX/2ml;

    .line 1966379
    iput-object p3, p0, LX/D6v;->k:LX/3I1;

    .line 1966380
    iput-object p4, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    .line 1966381
    iput-object p5, p0, LX/D6v;->n:LX/0So;

    .line 1966382
    iput-object p6, p0, LX/D6v;->o:LX/3H4;

    .line 1966383
    iput-object p7, p0, LX/D6v;->p:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    .line 1966384
    iput-object p8, p0, LX/D6v;->q:LX/1C2;

    .line 1966385
    new-instance v0, LX/D6u;

    invoke-direct {v0, p0}, LX/D6u;-><init>(LX/D6v;)V

    iput-object v0, p0, LX/D6v;->m:LX/D6u;

    .line 1966386
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D6v;->r:Ljava/lang/ref/WeakReference;

    .line 1966387
    return-void
.end method

.method public static a(LX/D6v;LX/2oN;)V
    .locals 1

    .prologue
    .line 1966388
    new-instance v0, LX/7M6;

    invoke-direct {v0}, LX/7M6;-><init>()V

    invoke-static {p0, p1, v0}, LX/D6v;->a(LX/D6v;LX/2oN;LX/7M6;)V

    .line 1966389
    return-void
.end method

.method private static a(LX/D6v;LX/2oN;LX/7M6;)V
    .locals 5

    .prologue
    .line 1966390
    iget-object v0, p0, LX/D6v;->a:LX/2oN;

    iput-object v0, p0, LX/D6v;->b:LX/2oN;

    .line 1966391
    iput-object p1, p0, LX/D6v;->a:LX/2oN;

    .line 1966392
    iget-object v0, p0, LX/D6v;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3GF;

    .line 1966393
    if-eqz v0, :cond_0

    .line 1966394
    iget-object v1, p0, LX/D6v;->a:LX/2oN;

    iget-object v2, p0, LX/D6v;->b:LX/2oN;

    invoke-interface {v0, v1, v2, p2}, LX/3GF;->a(LX/2oN;LX/2oN;LX/7M6;)V

    .line 1966395
    :cond_0
    new-instance v0, LX/BSP;

    invoke-direct {v0}, LX/BSP;-><init>()V

    .line 1966396
    iget-object v1, p0, LX/D6v;->E:LX/BST;

    iput-object v1, v0, LX/BSP;->e:LX/BST;

    .line 1966397
    iget-boolean v1, p0, LX/D6v;->C:Z

    iput-boolean v1, v0, LX/BSP;->g:Z

    .line 1966398
    iget-object v1, p0, LX/D6v;->D:Ljava/lang/String;

    iput-object v1, v0, LX/BSP;->i:Ljava/lang/String;

    .line 1966399
    invoke-virtual {p0}, LX/D6v;->d()LX/04D;

    move-result-object v1

    iput-object v1, v0, LX/BSP;->j:LX/04D;

    .line 1966400
    iget v1, p0, LX/D6v;->g:I

    iput v1, v0, LX/BSP;->k:I

    .line 1966401
    iget-object v1, p0, LX/D6v;->a:LX/2oN;

    iget-object v2, p0, LX/D6v;->b:LX/2oN;

    if-eq v1, v2, :cond_1

    .line 1966402
    iget-object v1, p0, LX/D6v;->o:LX/3H4;

    iget-object v2, p0, LX/D6v;->i:Ljava/lang/String;

    iget-object v3, p0, LX/D6v;->a:LX/2oN;

    iget-object v4, p0, LX/D6v;->w:LX/3H0;

    .line 1966403
    sget-object p0, LX/BSO;->d:[I

    invoke-virtual {v3}, LX/2oN;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 1966404
    sget-object p0, LX/BSS;->NONE:LX/BSS;

    :goto_0
    move-object p0, p0

    .line 1966405
    sget-object p1, LX/BSS;->NONE:LX/BSS;

    if-eq p0, p1, :cond_1

    iget-object p1, v0, LX/BSP;->e:LX/BST;

    sget-object p2, LX/BST;->SHORT_AD:LX/BST;

    if-ne p1, p2, :cond_2

    .line 1966406
    :cond_1
    :goto_1
    return-void

    .line 1966407
    :cond_2
    invoke-virtual {v1, v2, p0, v0, v4}, LX/3H4;->a(Ljava/lang/String;LX/BSS;LX/BSP;LX/3H0;)V

    goto :goto_1

    .line 1966408
    :pswitch_0
    sget-object p0, LX/BSS;->START_AD:LX/BSS;

    goto :goto_0

    .line 1966409
    :pswitch_1
    sget-object p0, LX/BSS;->TRANSIT:LX/BSS;

    goto :goto_0

    .line 1966410
    :pswitch_2
    sget-object p0, LX/BSS;->WAIT_FOR:LX/BSS;

    goto :goto_0

    .line 1966411
    :pswitch_3
    sget-object p0, LX/BSS;->STATIC_COUNTDOWN:LX/BSS;

    goto :goto_0

    .line 1966412
    :pswitch_4
    sget-object p0, LX/BSS;->VOD_NO_VIDEO_AD:LX/BSS;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static c(LX/D6v;LX/BSU;)V
    .locals 13

    .prologue
    const/4 v5, 0x1

    const-wide/16 v6, 0x7d0

    .line 1966413
    iget-boolean v0, p0, LX/D6v;->x:Z

    if-eqz v0, :cond_0

    .line 1966414
    :goto_0
    return-void

    .line 1966415
    :cond_0
    iget-object v0, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/D6v;->i:Ljava/lang/String;

    iget v2, p0, LX/D6v;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Ljava/lang/String;I)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1966416
    iget-object v1, p0, LX/D6v;->k:LX/3I1;

    iget-boolean v1, v1, LX/3I1;->f:Z

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    iget-object v1, p0, LX/D6v;->w:LX/3H0;

    sget-object v2, LX/3H0;->VOD:LX/3H0;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, LX/D6v;->w:LX/3H0;

    sget-object v2, LX/3H0;->NONLIVE:LX/3H0;

    if-ne v1, v2, :cond_2

    .line 1966417
    :cond_1
    new-instance v0, LX/BSP;

    invoke-direct {v0}, LX/BSP;-><init>()V

    .line 1966418
    iget-boolean v1, p0, LX/D6v;->C:Z

    iput-boolean v1, v0, LX/BSP;->g:Z

    .line 1966419
    invoke-virtual {p0}, LX/D6v;->d()LX/04D;

    move-result-object v1

    iput-object v1, v0, LX/BSP;->j:LX/04D;

    .line 1966420
    iget v1, p0, LX/D6v;->g:I

    iput v1, v0, LX/BSP;->k:I

    .line 1966421
    iget-object v1, p0, LX/D6v;->o:LX/3H4;

    iget-object v2, p0, LX/D6v;->i:Ljava/lang/String;

    sget-object v3, LX/BSS;->START_SHOW_NOTHING:LX/BSS;

    iget-object v4, p0, LX/D6v;->w:LX/3H0;

    invoke-virtual {v1, v2, v3, v0, v4}, LX/3H4;->a(Ljava/lang/String;LX/BSS;LX/BSP;LX/3H0;)V

    .line 1966422
    invoke-static {p0}, LX/D6v;->u(LX/D6v;)V

    goto :goto_0

    .line 1966423
    :cond_2
    iput-boolean v5, p0, LX/D6v;->B:Z

    .line 1966424
    new-instance v2, LX/7M6;

    invoke-direct {v2}, LX/7M6;-><init>()V

    .line 1966425
    iget-object v1, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v3, p0, LX/D6v;->i:Ljava/lang/String;

    iget v4, p0, LX/D6v;->g:I

    invoke-virtual {v1, v3, v4}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->b(Ljava/lang/String;I)LX/D6r;

    move-result-object v1

    .line 1966426
    invoke-static {p0}, LX/D6v;->x(LX/D6v;)J

    move-result-wide v8

    .line 1966427
    new-instance v10, LX/BSP;

    invoke-direct {v10}, LX/BSP;-><init>()V

    .line 1966428
    iput-wide v8, v10, LX/BSP;->a:J

    .line 1966429
    iget-wide v8, p0, LX/D6v;->z:J

    iput-wide v8, v10, LX/BSP;->b:J

    .line 1966430
    iget-object v8, p0, LX/D6v;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/3GF;

    .line 1966431
    if-eqz v8, :cond_4

    .line 1966432
    invoke-interface {v8}, LX/3GF;->d()LX/BSW;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 1966433
    invoke-interface {v8}, LX/3GF;->d()LX/BSW;

    move-result-object v9

    iput-object v9, v10, LX/BSP;->f:LX/BSW;

    .line 1966434
    :cond_3
    invoke-interface {v8}, LX/3GF;->e()I

    move-result v8

    iput v8, v10, LX/BSP;->h:I

    .line 1966435
    :cond_4
    iput-object p1, v10, LX/BSP;->d:LX/BSU;

    .line 1966436
    iget-boolean v8, p0, LX/D6v;->C:Z

    iput-boolean v8, v10, LX/BSP;->g:Z

    .line 1966437
    invoke-virtual {p0}, LX/D6v;->d()LX/04D;

    move-result-object v8

    iput-object v8, v10, LX/BSP;->j:LX/04D;

    .line 1966438
    iget v8, p0, LX/D6v;->g:I

    iput v8, v10, LX/BSP;->k:I

    .line 1966439
    iget-object v8, p0, LX/D6v;->o:LX/3H4;

    iget-object v9, p0, LX/D6v;->i:Ljava/lang/String;

    sget-object v11, LX/BSS;->START:LX/BSS;

    iget-object v12, p0, LX/D6v;->w:LX/3H0;

    invoke-virtual {v8, v9, v11, v10, v12}, LX/3H4;->a(Ljava/lang/String;LX/BSS;LX/BSP;LX/3H0;)V

    .line 1966440
    sget-object v3, LX/D6s;->b:[I

    invoke-virtual {v1}, LX/D6r;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    .line 1966441
    sget-object v1, LX/BST;->NO_VIDEO_AD:LX/BST;

    .line 1966442
    iget-object v0, p0, LX/D6v;->w:LX/3H0;

    sget-object v3, LX/3H0;->VOD:LX/3H0;

    if-ne v0, v3, :cond_d

    .line 1966443
    iget-object v0, p0, LX/D6v;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3GF;

    .line 1966444
    if-eqz v0, :cond_d

    invoke-interface {v0}, LX/3GF;->b()J

    move-result-wide v4

    .line 1966445
    const-wide/16 v8, 0x3e8

    div-long v8, v4, v8

    long-to-int v8, v8

    move v0, v8

    .line 1966446
    iget-object v3, p0, LX/D6v;->k:LX/3I1;

    iget v3, v3, LX/3I1;->c:I

    if-ge v0, v3, :cond_d

    .line 1966447
    sget-object v0, LX/BST;->TIME_SPENT_INSUFFICIENT:LX/BST;

    .line 1966448
    :goto_1
    invoke-virtual {p0, v6, v7, v0}, LX/D6v;->a(JLX/BST;)V

    .line 1966449
    :cond_5
    :goto_2
    iget-object v0, p0, LX/D6v;->w:LX/3H0;

    sget-object v1, LX/3H0;->LIVE:LX/3H0;

    if-ne v0, v1, :cond_8

    .line 1966450
    iget-object v8, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v9, p0, LX/D6v;->i:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->c(Ljava/lang/String;)J

    move-result-wide v8

    .line 1966451
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-gtz v10, :cond_6

    .line 1966452
    const-wide/16 v8, 0x3a98

    .line 1966453
    :cond_6
    move-wide v0, v8

    .line 1966454
    iget-object v3, p0, LX/D6v;->s:LX/D6t;

    if-eqz v3, :cond_7

    .line 1966455
    iget-object v3, p0, LX/D6v;->s:LX/D6t;

    invoke-virtual {v3}, LX/D6t;->cancel()V

    .line 1966456
    :cond_7
    new-instance v3, LX/D6t;

    invoke-direct {v3, p0, v0, v1}, LX/D6t;-><init>(LX/D6v;J)V

    iput-object v3, p0, LX/D6v;->s:LX/D6t;

    .line 1966457
    iget-object v3, p0, LX/D6v;->s:LX/D6t;

    invoke-virtual {v3}, LX/D6t;->start()Landroid/os/CountDownTimer;

    .line 1966458
    :cond_8
    sget-object v0, LX/2oN;->TRANSITION:LX/2oN;

    invoke-static {p0, v0, v2}, LX/D6v;->a(LX/D6v;LX/2oN;LX/7M6;)V

    goto/16 :goto_0

    .line 1966459
    :pswitch_0
    sget-object v0, LX/BST;->NO_VIDEO_AD:LX/BST;

    invoke-virtual {p0, v6, v7, v0}, LX/D6v;->a(JLX/BST;)V

    goto :goto_2

    .line 1966460
    :pswitch_1
    iget-object v1, p0, LX/D6v;->w:LX/3H0;

    sget-object v3, LX/3H0;->NONLIVE_POST_ROLL:LX/3H0;

    if-ne v1, v3, :cond_9

    .line 1966461
    invoke-static {p0}, LX/D6v;->m(LX/D6v;)V

    goto/16 :goto_0

    .line 1966462
    :cond_9
    if-nez v0, :cond_a

    .line 1966463
    sget-object v0, LX/BST;->NO_VIDEO_AD:LX/BST;

    invoke-virtual {p0, v6, v7, v0}, LX/D6v;->a(JLX/BST;)V

    goto :goto_2

    .line 1966464
    :cond_a
    iget v0, p0, LX/D6v;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_b

    .line 1966465
    iget-object v0, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/D6v;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, LX/D6v;->d:I

    .line 1966466
    :cond_b
    iput-boolean v5, v2, LX/7M6;->b:Z

    .line 1966467
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6, v7}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 1966468
    :pswitch_2
    iget-object v0, p0, LX/D6v;->w:LX/3H0;

    sget-object v1, LX/3H0;->VOD:LX/3H0;

    if-ne v0, v1, :cond_c

    .line 1966469
    sget-object v0, LX/BST;->NO_VIDEO_AD:LX/BST;

    invoke-virtual {p0, v6, v7, v0}, LX/D6v;->a(JLX/BST;)V

    goto :goto_2

    .line 1966470
    :cond_c
    iget-object v0, p0, LX/D6v;->w:LX/3H0;

    sget-object v1, LX/3H0;->LIVE:LX/3H0;

    if-ne v0, v1, :cond_5

    .line 1966471
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6, v7}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    :cond_d
    move-object v0, v1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static m(LX/D6v;)V
    .locals 4

    .prologue
    .line 1966472
    iget-object v0, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/D6v;->i:Ljava/lang/String;

    iget v2, p0, LX/D6v;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Ljava/lang/String;I)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1966473
    if-nez v1, :cond_0

    .line 1966474
    :goto_0
    return-void

    .line 1966475
    :cond_0
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1966476
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1966477
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1966478
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/D6v;->D:Ljava/lang/String;

    .line 1966479
    :cond_1
    new-instance v0, LX/BSg;

    invoke-direct {v0, v1}, LX/BSg;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, v0

    .line 1966480
    iput-object v0, p0, LX/D6v;->A:LX/BSg;

    .line 1966481
    invoke-static {v1}, LX/3HR;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    int-to-long v2, v0

    .line 1966482
    new-instance v0, LX/7M6;

    invoke-direct {v0}, LX/7M6;-><init>()V

    .line 1966483
    iput-wide v2, v0, LX/7M6;->a:J

    .line 1966484
    iput-object v1, v0, LX/7M6;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1966485
    sget-object v1, LX/2oN;->VIDEO_AD:LX/2oN;

    invoke-static {p0, v1, v0}, LX/D6v;->a(LX/D6v;LX/2oN;LX/7M6;)V

    goto :goto_0
.end method

.method public static p(LX/D6v;)V
    .locals 4

    .prologue
    .line 1966486
    iget-wide v0, p0, LX/D6v;->v:J

    const-wide/16 v2, 0x7d0

    sub-long/2addr v0, v2

    .line 1966487
    new-instance v2, LX/7M6;

    invoke-direct {v2}, LX/7M6;-><init>()V

    .line 1966488
    iput-wide v0, v2, LX/7M6;->a:J

    .line 1966489
    sget-object v3, LX/2oN;->STATIC_COUNTDOWN:LX/2oN;

    invoke-static {p0, v3, v2}, LX/D6v;->a(LX/D6v;LX/2oN;LX/7M6;)V

    .line 1966490
    iget-object v2, p0, LX/D6v;->m:LX/D6u;

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v0, v1}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    .line 1966491
    return-void
.end method

.method public static u(LX/D6v;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 1966492
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    invoke-virtual {v0, v6}, LX/D6u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1966493
    iget-object v0, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/D6v;->i:Ljava/lang/String;

    iget v2, p0, LX/D6v;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->c(Ljava/lang/String;I)V

    .line 1966494
    iget-object v0, p0, LX/D6v;->s:LX/D6t;

    if-eqz v0, :cond_0

    .line 1966495
    iget-object v0, p0, LX/D6v;->s:LX/D6t;

    invoke-virtual {v0}, LX/D6t;->cancel()V

    .line 1966496
    :cond_0
    iput-wide v4, p0, LX/D6v;->c:J

    .line 1966497
    iput-boolean v3, p0, LX/D6v;->t:Z

    .line 1966498
    iput-wide v4, p0, LX/D6v;->u:J

    .line 1966499
    iput-wide v4, p0, LX/D6v;->v:J

    .line 1966500
    iput-boolean v3, p0, LX/D6v;->y:Z

    .line 1966501
    iput-object v6, p0, LX/D6v;->A:LX/BSg;

    .line 1966502
    iput-boolean v3, p0, LX/D6v;->B:Z

    .line 1966503
    sget-object v0, LX/BST;->NONE:LX/BST;

    iput-object v0, p0, LX/D6v;->E:LX/BST;

    .line 1966504
    sget-object v0, LX/BSR;->NONE:LX/BSR;

    iput-object v0, p0, LX/D6v;->F:LX/BSR;

    .line 1966505
    return-void
.end method

.method private w()J
    .locals 4

    .prologue
    .line 1966506
    iget-wide v0, p0, LX/D6v;->v:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/D6v;->v:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/D6v;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->c(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static x(LX/D6v;)J
    .locals 4

    .prologue
    .line 1966507
    iget-object v0, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, LX/D6v;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->d(Ljava/lang/String;)J

    move-result-wide v2

    .line 1966508
    const/4 v1, 0x0

    .line 1966509
    iget-object v0, p0, LX/D6v;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3GF;

    .line 1966510
    if-eqz v0, :cond_1

    .line 1966511
    invoke-interface {v0}, LX/3GF;->a()I

    move-result v0

    .line 1966512
    :goto_0
    if-eqz v0, :cond_0

    int-to-long v0, v0

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    :goto_1
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1966360
    iget-object v0, p0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-ne v0, v1, :cond_0

    .line 1966361
    invoke-static {p0}, LX/D6v;->x(LX/D6v;)J

    move-result-wide v0

    iput-wide v0, p0, LX/D6v;->z:J

    .line 1966362
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/D6u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1966363
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/D6u;->sendEmptyMessage(I)Z

    .line 1966364
    :cond_0
    return-void
.end method

.method public final a(JLX/BST;)V
    .locals 3

    .prologue
    .line 1966513
    iget-object v0, p0, LX/D6v;->w:LX/3H0;

    sget-object v1, LX/3H0;->VOD:LX/3H0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/D6v;->w:LX/3H0;

    sget-object v1, LX/3H0;->NONLIVE:LX/3H0;

    if-ne v0, v1, :cond_2

    .line 1966514
    :cond_0
    iput-object p3, p0, LX/D6v;->E:LX/BST;

    .line 1966515
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, LX/D6u;->sendEmptyMessage(I)Z

    .line 1966516
    :cond_1
    :goto_0
    return-void

    .line 1966517
    :cond_2
    iget-object v0, p0, LX/D6v;->w:LX/3H0;

    sget-object v1, LX/3H0;->LIVE:LX/3H0;

    if-ne v0, v1, :cond_1

    .line 1966518
    iput-object p3, p0, LX/D6v;->E:LX/BST;

    .line 1966519
    iget-object v0, p0, LX/D6v;->E:LX/BST;

    sget-object v1, LX/BST;->HIDE_AD:LX/BST;

    if-ne v0, v1, :cond_4

    .line 1966520
    sget-object v0, LX/BSR;->HIDE_AD:LX/BSR;

    iput-object v0, p0, LX/D6v;->F:LX/BSR;

    .line 1966521
    :cond_3
    :goto_1
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1, p2}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1966522
    :cond_4
    iget-object v0, p0, LX/D6v;->E:LX/BST;

    sget-object v1, LX/BST;->PLAYBACK_ERROR:LX/BST;

    if-ne v0, v1, :cond_3

    .line 1966523
    sget-object v0, LX/BSR;->ERROR:LX/BSR;

    iput-object v0, p0, LX/D6v;->F:LX/BSR;

    goto :goto_1
.end method

.method public final a(LX/0lF;)V
    .locals 10
    .param p1    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1966299
    iget-boolean v0, p0, LX/D6v;->B:Z

    if-nez v0, :cond_1

    .line 1966300
    iget-object v0, p0, LX/D6v;->w:LX/3H0;

    sget-object v1, LX/3H0;->LIVE:LX/3H0;

    if-ne v0, v1, :cond_0

    .line 1966301
    iget-object v0, p0, LX/D6v;->q:LX/1C2;

    iget-object v1, p0, LX/D6v;->i:Ljava/lang/String;

    const/4 v8, 0x0

    .line 1966302
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/0JF;->COMMERCIAL_BREAK_ONSCREEN:LX/0JF;

    iget-object v3, v3, LX/0JF;->value:Ljava/lang/String;

    invoke-direct {v4, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1966303
    const/4 v7, 0x0

    move-object v3, v0

    move-object v5, v1

    move-object v6, p1

    move-object v9, v8

    invoke-static/range {v3 .. v9}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 1966304
    :cond_0
    iget-object v0, p0, LX/D6v;->o:LX/3H4;

    sget-object v1, LX/BSX;->INLINE_SCROLLED_INTO:LX/BSX;

    invoke-virtual {p0}, LX/D6v;->j()LX/BSQ;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/3H4;->a(LX/BSX;LX/BSQ;)V

    .line 1966305
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D6v;->B:Z

    .line 1966306
    return-void
.end method

.method public final a(LX/3GF;)V
    .locals 1

    .prologue
    .line 1966307
    iget-object v0, p0, LX/D6v;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    .line 1966308
    iget-object v0, p0, LX/D6v;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1966309
    iget-object v0, p0, LX/D6v;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3GF;

    invoke-interface {v0}, LX/3GF;->c()V

    .line 1966310
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D6v;->r:Ljava/lang/ref/WeakReference;

    .line 1966311
    :cond_1
    return-void
.end method

.method public final a(LX/BSR;)V
    .locals 2

    .prologue
    .line 1966312
    iput-object p1, p0, LX/D6v;->F:LX/BSR;

    .line 1966313
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, LX/D6u;->sendEmptyMessage(I)Z

    .line 1966314
    return-void
.end method

.method public final a(LX/BSU;)V
    .locals 2

    .prologue
    .line 1966315
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/D6u;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1966316
    invoke-virtual {p0, p1}, LX/D6v;->b(LX/BSU;)V

    .line 1966317
    :cond_0
    return-void
.end method

.method public final a(LX/BSU;I)V
    .locals 3

    .prologue
    .line 1966318
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/D6u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1966319
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    iget-object v1, p0, LX/D6v;->m:LX/D6u;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, LX/D6u;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/D6u;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1966320
    iput p2, p0, LX/D6v;->d:I

    .line 1966321
    return-void
.end method

.method public final b(LX/0lF;)V
    .locals 10
    .param p1    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1966322
    iget-boolean v0, p0, LX/D6v;->B:Z

    if-eqz v0, :cond_1

    .line 1966323
    iget-object v0, p0, LX/D6v;->w:LX/3H0;

    sget-object v1, LX/3H0;->LIVE:LX/3H0;

    if-ne v0, v1, :cond_0

    .line 1966324
    iget-object v0, p0, LX/D6v;->q:LX/1C2;

    iget-object v1, p0, LX/D6v;->i:Ljava/lang/String;

    const/4 v8, 0x0

    .line 1966325
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/0JF;->COMMERCIAL_BREAK_OFFSCREEN:LX/0JF;

    iget-object v3, v3, LX/0JF;->value:Ljava/lang/String;

    invoke-direct {v4, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1966326
    const/4 v7, 0x0

    move-object v3, v0

    move-object v5, v1

    move-object v6, p1

    move-object v9, v8

    invoke-static/range {v3 .. v9}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 1966327
    :cond_0
    iget-object v0, p0, LX/D6v;->o:LX/3H4;

    sget-object v1, LX/BSX;->INLINE_SCROLLED_AWAY:LX/BSX;

    invoke-virtual {p0}, LX/D6v;->j()LX/BSQ;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/3H4;->a(LX/BSX;LX/BSQ;)V

    .line 1966328
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D6v;->B:Z

    .line 1966329
    return-void
.end method

.method public final b(LX/BSU;)V
    .locals 1

    .prologue
    .line 1966330
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, LX/D6v;->a(LX/BSU;I)V

    .line 1966331
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1966332
    iget-object v0, p0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->VIDEO_AD:LX/2oN;

    if-ne v0, v1, :cond_0

    .line 1966333
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/D6u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1966334
    iget-object v0, p0, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/D6u;->sendEmptyMessage(I)Z

    .line 1966335
    :cond_0
    return-void
.end method

.method public final d()LX/04D;
    .locals 1

    .prologue
    .line 1966336
    iget-object v0, p0, LX/D6v;->G:LX/04D;

    if-nez v0, :cond_0

    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/D6v;->G:LX/04D;

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1966337
    iget-object v0, p0, LX/D6v;->D:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "-1"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/D6v;->D:Ljava/lang/String;

    goto :goto_0
.end method

.method public final f()V
    .locals 6

    .prologue
    .line 1966338
    new-instance v0, LX/7M6;

    invoke-direct {v0}, LX/7M6;-><init>()V

    .line 1966339
    sget-object v1, LX/D6s;->a:[I

    iget-object v2, p0, LX/D6v;->a:LX/2oN;

    invoke-virtual {v2}, LX/2oN;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1966340
    invoke-direct {p0}, LX/D6v;->w()J

    move-result-wide v2

    iput-wide v2, v0, LX/7M6;->a:J

    .line 1966341
    :goto_0
    :pswitch_0
    iget-object v1, p0, LX/D6v;->a:LX/2oN;

    invoke-static {p0, v1, v0}, LX/D6v;->a(LX/D6v;LX/2oN;LX/7M6;)V

    .line 1966342
    return-void

    .line 1966343
    :pswitch_1
    invoke-direct {p0}, LX/D6v;->w()J

    move-result-wide v2

    const-wide/16 v4, 0x7d0

    sub-long/2addr v2, v4

    iput-wide v2, v0, LX/7M6;->a:J

    goto :goto_0

    .line 1966344
    :pswitch_2
    iget-object v1, p0, LX/D6v;->l:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v2, p0, LX/D6v;->i:Ljava/lang/String;

    iget v3, p0, LX/D6v;->g:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(Ljava/lang/String;I)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1966345
    invoke-static {v1}, LX/3HR;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v2

    int-to-long v2, v2

    iget-wide v4, p0, LX/D6v;->c:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, LX/7M6;->a:J

    .line 1966346
    iput-object v1, v0, LX/7M6;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1966347
    iget-boolean v0, p0, LX/D6v;->y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/D6v;->A:LX/BSg;

    if-nez v0, :cond_1

    .line 1966348
    :cond_0
    :goto_0
    return-void

    .line 1966349
    :cond_1
    iget-object v0, p0, LX/D6v;->p:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v1, p0, LX/D6v;->A:LX/BSg;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a(LX/1g1;)V

    .line 1966350
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D6v;->y:Z

    goto :goto_0
.end method

.method public final j()LX/BSQ;
    .locals 2

    .prologue
    .line 1966351
    new-instance v0, LX/BSQ;

    invoke-direct {v0}, LX/BSQ;-><init>()V

    .line 1966352
    iget-object v1, p0, LX/D6v;->i:Ljava/lang/String;

    iput-object v1, v0, LX/BSQ;->a:Ljava/lang/String;

    .line 1966353
    iget-object v1, p0, LX/D6v;->a:LX/2oN;

    iput-object v1, v0, LX/BSQ;->b:LX/2oN;

    .line 1966354
    iget-object v1, p0, LX/D6v;->b:LX/2oN;

    iput-object v1, v0, LX/BSQ;->c:LX/2oN;

    .line 1966355
    iget-object v1, p0, LX/D6v;->w:LX/3H0;

    iput-object v1, v0, LX/BSQ;->d:LX/3H0;

    .line 1966356
    iget-boolean v1, p0, LX/D6v;->C:Z

    iput-boolean v1, v0, LX/BSQ;->f:Z

    .line 1966357
    invoke-virtual {p0}, LX/D6v;->d()LX/04D;

    move-result-object v1

    iput-object v1, v0, LX/BSQ;->g:LX/04D;

    .line 1966358
    iget v1, p0, LX/D6v;->g:I

    iput v1, v0, LX/BSQ;->h:I

    .line 1966359
    return-object v0
.end method
