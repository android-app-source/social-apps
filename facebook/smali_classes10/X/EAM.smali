.class public final LX/EAM;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/EAO;


# direct methods
.method public constructor <init>(LX/EAO;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 2085472
    iput-object p1, p0, LX/EAM;->b:LX/EAO;

    iput-object p2, p0, LX/EAM;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 7

    .prologue
    .line 2085478
    iget-object v0, p0, LX/EAM;->b:LX/EAO;

    iget-object v0, v0, LX/EAO;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814eb

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2085479
    iget-object v0, p0, LX/EAM;->b:LX/EAO;

    iget-object v6, v0, LX/EAO;->d:LX/0bH;

    new-instance v0, LX/1Nd;

    iget-object v1, p0, LX/EAM;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/EAM;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v5, p0, LX/EAM;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->H_()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 2085480
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2085473
    iget-object v0, p0, LX/EAM;->b:LX/EAO;

    iget-object v0, v0, LX/EAO;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814ec

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2085474
    iget-object v0, p0, LX/EAM;->b:LX/EAO;

    .line 2085475
    iget-object v1, v0, LX/EAO;->b:LX/EA5;

    move-object v0, v1

    .line 2085476
    iget-object v1, v0, LX/EA5;->p:LX/EA0;

    invoke-virtual {v1}, LX/EA0;->f()V

    .line 2085477
    return-void
.end method
