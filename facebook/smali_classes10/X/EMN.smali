.class public final LX/EMN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/CxA;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;LX/CzL;LX/CxA;)V
    .locals 0

    .prologue
    .line 2110591
    iput-object p1, p0, LX/EMN;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iput-object p2, p0, LX/EMN;->a:LX/CzL;

    iput-object p3, p0, LX/EMN;->b:LX/CxA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x4eb55803

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2110592
    iget-object v0, p0, LX/EMN;->a:LX/CzL;

    .line 2110593
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2110594
    check-cast v0, LX/8d1;

    .line 2110595
    iget-object v1, p0, LX/EMN;->a:LX/CzL;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->b(LX/CzL;)LX/CzL;

    move-result-object v3

    .line 2110596
    iget-object v1, p0, LX/EMN;->a:LX/CzL;

    if-ne v1, v3, :cond_0

    .line 2110597
    const v0, 0x15d7843c

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2110598
    :goto_0
    return-void

    .line 2110599
    :cond_0
    iget-object v1, p0, LX/EMN;->b:LX/CxA;

    iget-object v4, p0, LX/EMN;->a:LX/CzL;

    invoke-interface {v1, v4, v3}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2110600
    iget-object v1, p0, LX/EMN;->b:LX/CxA;

    check-cast v1, LX/1Pq;

    invoke-interface {v1}, LX/1Pq;->iN_()V

    .line 2110601
    iget-object v1, p0, LX/EMN;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "apply_mutation_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/EML;

    invoke-direct {v5, p0, v0}, LX/EML;-><init>(LX/EMN;LX/8d1;)V

    new-instance v0, LX/EMM;

    invoke-direct {v0, p0, v3}, LX/EMM;-><init>(LX/EMN;LX/CzL;)V

    invoke-virtual {v1, v4, v5, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2110602
    const v0, -0x315cfad0

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method
