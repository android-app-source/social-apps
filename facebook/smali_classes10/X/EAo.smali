.class public final LX/EAo;
.super LX/1NP;
.source ""


# instance fields
.field public final synthetic a:LX/E9Y;

.field public final synthetic b:LX/E9T;

.field public final synthetic c:LX/EAs;


# direct methods
.method public constructor <init>(LX/EAs;LX/E9Y;LX/E9T;)V
    .locals 0

    .prologue
    .line 2085910
    iput-object p1, p0, LX/EAo;->c:LX/EAs;

    iput-object p2, p0, LX/EAo;->a:LX/E9Y;

    iput-object p3, p0, LX/EAo;->b:LX/E9T;

    invoke-direct {p0}, LX/1NP;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2085911
    check-cast p1, LX/1Ne;

    .line 2085912
    iget-object v0, p1, LX/1Ne;->a:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    .line 2085913
    iget-object v1, p0, LX/EAo;->a:LX/E9Y;

    iget-object v0, p1, LX/1Ne;->a:Lcom/facebook/graphql/model/FeedUnit;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2085914
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, LX/BNJ;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2085915
    iget-object v2, v1, LX/E9Y;->f:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2085916
    :goto_0
    iget-object v0, p0, LX/EAo;->b:LX/E9T;

    invoke-interface {v0}, LX/E9T;->f()V

    .line 2085917
    :cond_0
    return-void

    .line 2085918
    :cond_1
    iget-object v2, v1, LX/E9Y;->f:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    .line 2085919
    invoke-static {v2, v0}, LX/BNI;->a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    move-result-object v4

    .line 2085920
    iget-object v5, v1, LX/E9Y;->c:Ljava/util/List;

    iget-object p1, v1, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {v5, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2085921
    iget-object v2, v1, LX/E9Y;->f:Ljava/util/Map;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
