.class public LX/E9Z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2084567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;)V
    .locals 6

    .prologue
    .line 2084568
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->d()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->setTitle(D)V

    .line 2084569
    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V

    .line 2084570
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel$HistogramModel;

    .line 2084571
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel$HistogramModel;->b()I

    move-result v5

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel$HistogramModel;->a()I

    move-result v0

    invoke-virtual {v2, v5, v0}, Landroid/util/SparseIntArray;->append(II)V

    .line 2084572
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2084573
    :cond_0
    move-object v0, v2

    .line 2084574
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->c()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a(Landroid/util/SparseIntArray;I)V

    .line 2084575
    return-void
.end method
