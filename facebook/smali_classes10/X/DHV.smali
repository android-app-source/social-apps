.class public LX/DHV;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/3mX",
        "<",
        "LX/DHU;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/DGu;

.field private final d:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Pn;LX/25M;LX/DGu;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/DHU;",
            ">;TE;",
            "LX/25M;",
            "LX/DGu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1981985
    move-object v0, p3

    check-cast v0, LX/1Pq;

    invoke-direct {p0, p1, p2, v0, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1981986
    iput-object p5, p0, LX/DHV;->c:LX/DGu;

    .line 1981987
    iput-object p3, p0, LX/DHV;->d:LX/1Pn;

    .line 1981988
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1981989
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 1981990
    check-cast p2, LX/DHU;

    .line 1981991
    iget-object v0, p2, LX/DHU;->b:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->ordinal()I

    .line 1981992
    iget-object v0, p0, LX/DHV;->c:LX/DGu;

    const/4 v1, 0x0

    .line 1981993
    new-instance v2, LX/DGt;

    invoke-direct {v2, v0}, LX/DGt;-><init>(LX/DGu;)V

    .line 1981994
    iget-object v3, v0, LX/DGu;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DGs;

    .line 1981995
    if-nez v3, :cond_0

    .line 1981996
    new-instance v3, LX/DGs;

    invoke-direct {v3, v0}, LX/DGs;-><init>(LX/DGu;)V

    .line 1981997
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/DGs;->a$redex0(LX/DGs;LX/1De;IILX/DGt;)V

    .line 1981998
    move-object v2, v3

    .line 1981999
    move-object v1, v2

    .line 1982000
    move-object v1, v1

    .line 1982001
    iget-object v0, p0, LX/DHV;->d:LX/1Pn;

    check-cast v0, LX/1Pv;

    .line 1982002
    iget-object v2, v1, LX/DGs;->a:LX/DGt;

    iput-object v0, v2, LX/DGt;->a:LX/1Pv;

    .line 1982003
    iget-object v2, v1, LX/DGs;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1982004
    move-object v0, v1

    .line 1982005
    iget-object v1, p2, LX/DHU;->a:LX/DGm;

    .line 1982006
    iget-object v2, v0, LX/DGs;->a:LX/DGt;

    iput-object v1, v2, LX/DGt;->b:LX/DGm;

    .line 1982007
    iget-object v2, v0, LX/DGs;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1982008
    move-object v0, v0

    .line 1982009
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1982010
    const/4 v0, 0x0

    return v0
.end method
