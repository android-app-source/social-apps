.class public final enum LX/EFe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EFe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EFe;

.field public static final enum GROUP_CALL_JOIN:LX/EFe;

.field public static final enum GROUP_CALL_START:LX/EFe;

.field public static final enum INSTANT_VIDEO:LX/EFe;

.field public static final enum PSTN_UPSELL:LX/EFe;

.field public static final enum REGULAR:LX/EFe;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2096043
    new-instance v0, LX/EFe;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v2}, LX/EFe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFe;->REGULAR:LX/EFe;

    .line 2096044
    new-instance v0, LX/EFe;

    const-string v1, "INSTANT_VIDEO"

    invoke-direct {v0, v1, v3}, LX/EFe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFe;->INSTANT_VIDEO:LX/EFe;

    .line 2096045
    new-instance v0, LX/EFe;

    const-string v1, "PSTN_UPSELL"

    invoke-direct {v0, v1, v4}, LX/EFe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFe;->PSTN_UPSELL:LX/EFe;

    .line 2096046
    new-instance v0, LX/EFe;

    const-string v1, "GROUP_CALL_START"

    invoke-direct {v0, v1, v5}, LX/EFe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFe;->GROUP_CALL_START:LX/EFe;

    .line 2096047
    new-instance v0, LX/EFe;

    const-string v1, "GROUP_CALL_JOIN"

    invoke-direct {v0, v1, v6}, LX/EFe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFe;->GROUP_CALL_JOIN:LX/EFe;

    .line 2096048
    const/4 v0, 0x5

    new-array v0, v0, [LX/EFe;

    sget-object v1, LX/EFe;->REGULAR:LX/EFe;

    aput-object v1, v0, v2

    sget-object v1, LX/EFe;->INSTANT_VIDEO:LX/EFe;

    aput-object v1, v0, v3

    sget-object v1, LX/EFe;->PSTN_UPSELL:LX/EFe;

    aput-object v1, v0, v4

    sget-object v1, LX/EFe;->GROUP_CALL_START:LX/EFe;

    aput-object v1, v0, v5

    sget-object v1, LX/EFe;->GROUP_CALL_JOIN:LX/EFe;

    aput-object v1, v0, v6

    sput-object v0, LX/EFe;->$VALUES:[LX/EFe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2096050
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EFe;
    .locals 1

    .prologue
    .line 2096051
    const-class v0, LX/EFe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EFe;

    return-object v0
.end method

.method public static values()[LX/EFe;
    .locals 1

    .prologue
    .line 2096049
    sget-object v0, LX/EFe;->$VALUES:[LX/EFe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EFe;

    return-object v0
.end method
