.class public final LX/EYL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EYE;
.implements LX/EXE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/EYE;",
        "LX/EXE",
        "<",
        "LX/EYM;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private b:LX/EWv;

.field public final c:Ljava/lang/String;

.field public final d:LX/EYQ;

.field private final e:LX/EYF;

.field private f:[LX/EYM;


# direct methods
.method public constructor <init>(LX/EWv;LX/EYQ;LX/EYF;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2136993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2136994
    iput p4, p0, LX/EYL;->a:I

    .line 2136995
    iput-object p1, p0, LX/EYL;->b:LX/EWv;

    .line 2136996
    invoke-virtual {p1}, LX/EWv;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3, v0}, LX/EYT;->b(LX/EYQ;LX/EYF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYL;->c:Ljava/lang/String;

    .line 2136997
    iput-object p2, p0, LX/EYL;->d:LX/EYQ;

    .line 2136998
    iput-object p3, p0, LX/EYL;->e:LX/EYF;

    .line 2136999
    invoke-virtual {p1}, LX/EWv;->l()I

    move-result v0

    if-nez v0, :cond_0

    .line 2137000
    new-instance v0, LX/EYK;

    const-string v1, "Enums must contain at least one value."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137001
    :cond_0
    invoke-virtual {p1}, LX/EWv;->l()I

    move-result v0

    new-array v0, v0, [LX/EYM;

    iput-object v0, p0, LX/EYL;->f:[LX/EYM;

    move v4, v5

    .line 2137002
    :goto_0
    invoke-virtual {p1}, LX/EWv;->l()I

    move-result v0

    if-ge v4, v0, :cond_1

    .line 2137003
    iget-object v6, p0, LX/EYL;->f:[LX/EYM;

    new-instance v0, LX/EYM;

    invoke-virtual {p1, v4}, LX/EWv;->a(I)LX/EX6;

    move-result-object v1

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v4}, LX/EYM;-><init>(LX/EX6;LX/EYQ;LX/EYL;I)V

    aput-object v0, v6, v4

    .line 2137004
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2137005
    :cond_1
    iget-object v0, p2, LX/EYQ;->h:LX/EYJ;

    invoke-virtual {v0, p0}, LX/EYJ;->a(LX/EYE;)V

    .line 2137006
    return-void
.end method

.method public static a$redex0(LX/EYL;LX/EWv;)V
    .locals 3

    .prologue
    .line 2137012
    iput-object p1, p0, LX/EYL;->b:LX/EWv;

    .line 2137013
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/EYL;->f:[LX/EYM;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2137014
    iget-object v1, p0, LX/EYL;->f:[LX/EYM;

    aget-object v1, v1, v0

    invoke-virtual {p1, v0}, LX/EWv;->a(I)LX/EX6;

    move-result-object v2

    .line 2137015
    iput-object v2, v1, LX/EYM;->b:LX/EX6;

    .line 2137016
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2137017
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)LX/EYM;
    .locals 2

    .prologue
    .line 2137011
    iget-object v0, p0, LX/EYL;->d:LX/EYQ;

    iget-object v0, v0, LX/EYQ;->h:LX/EYJ;

    iget-object v0, v0, LX/EYJ;->e:Ljava/util/Map;

    new-instance v1, LX/EYG;

    invoke-direct {v1, p0, p1}, LX/EYG;-><init>(LX/EYE;I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYM;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137010
    iget-object v0, p0, LX/EYL;->b:LX/EWv;

    invoke-virtual {v0}, LX/EWv;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137018
    iget-object v0, p0, LX/EYL;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/EYQ;
    .locals 1

    .prologue
    .line 2137009
    iget-object v0, p0, LX/EYL;->d:LX/EYQ;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/EYM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2137008
    iget-object v0, p0, LX/EYL;->f:[LX/EYM;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/EWY;
    .locals 1

    .prologue
    .line 2137007
    iget-object v0, p0, LX/EYL;->b:LX/EWv;

    return-object v0
.end method
