.class public final LX/EMD;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EME;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/8d0;

.field public final synthetic b:LX/EME;


# direct methods
.method public constructor <init>(LX/EME;)V
    .locals 1

    .prologue
    .line 2110340
    iput-object p1, p0, LX/EMD;->b:LX/EME;

    .line 2110341
    move-object v0, p1

    .line 2110342
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2110343
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2110344
    const-string v0, "SearchResultsRelatedPagesProfilePhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2110345
    if-ne p0, p1, :cond_1

    .line 2110346
    :cond_0
    :goto_0
    return v0

    .line 2110347
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2110348
    goto :goto_0

    .line 2110349
    :cond_3
    check-cast p1, LX/EMD;

    .line 2110350
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2110351
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2110352
    if-eq v2, v3, :cond_0

    .line 2110353
    iget-object v2, p0, LX/EMD;->a:LX/8d0;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/EMD;->a:LX/8d0;

    iget-object v3, p1, LX/EMD;->a:LX/8d0;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2110354
    goto :goto_0

    .line 2110355
    :cond_4
    iget-object v2, p1, LX/EMD;->a:LX/8d0;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
