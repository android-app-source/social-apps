.class public final enum LX/Dhu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dhu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dhu;

.field public static final enum CAPTIVE_PORTAL:LX/Dhu;

.field public static final enum LONG_QUEUE_TIME:LX/Dhu;

.field public static final enum MEDIA_UPLOAD_FILE_NOT_FOUND_LOW_DISK_SPACE:LX/Dhu;

.field public static final enum RESTRICTED_BACKGROUND_MODE:LX/Dhu;

.field public static final enum SMS_MSS_ERROR:LX/Dhu;

.field public static final enum UNKNOWN:LX/Dhu;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2031431
    new-instance v0, LX/Dhu;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/Dhu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhu;->UNKNOWN:LX/Dhu;

    .line 2031432
    new-instance v0, LX/Dhu;

    const-string v1, "CAPTIVE_PORTAL"

    invoke-direct {v0, v1, v4}, LX/Dhu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhu;->CAPTIVE_PORTAL:LX/Dhu;

    .line 2031433
    new-instance v0, LX/Dhu;

    const-string v1, "RESTRICTED_BACKGROUND_MODE"

    invoke-direct {v0, v1, v5}, LX/Dhu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhu;->RESTRICTED_BACKGROUND_MODE:LX/Dhu;

    .line 2031434
    new-instance v0, LX/Dhu;

    const-string v1, "LONG_QUEUE_TIME"

    invoke-direct {v0, v1, v6}, LX/Dhu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhu;->LONG_QUEUE_TIME:LX/Dhu;

    .line 2031435
    new-instance v0, LX/Dhu;

    const-string v1, "MEDIA_UPLOAD_FILE_NOT_FOUND_LOW_DISK_SPACE"

    invoke-direct {v0, v1, v7}, LX/Dhu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhu;->MEDIA_UPLOAD_FILE_NOT_FOUND_LOW_DISK_SPACE:LX/Dhu;

    .line 2031436
    new-instance v0, LX/Dhu;

    const-string v1, "SMS_MSS_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Dhu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhu;->SMS_MSS_ERROR:LX/Dhu;

    .line 2031437
    const/4 v0, 0x6

    new-array v0, v0, [LX/Dhu;

    sget-object v1, LX/Dhu;->UNKNOWN:LX/Dhu;

    aput-object v1, v0, v3

    sget-object v1, LX/Dhu;->CAPTIVE_PORTAL:LX/Dhu;

    aput-object v1, v0, v4

    sget-object v1, LX/Dhu;->RESTRICTED_BACKGROUND_MODE:LX/Dhu;

    aput-object v1, v0, v5

    sget-object v1, LX/Dhu;->LONG_QUEUE_TIME:LX/Dhu;

    aput-object v1, v0, v6

    sget-object v1, LX/Dhu;->MEDIA_UPLOAD_FILE_NOT_FOUND_LOW_DISK_SPACE:LX/Dhu;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Dhu;->SMS_MSS_ERROR:LX/Dhu;

    aput-object v2, v0, v1

    sput-object v0, LX/Dhu;->$VALUES:[LX/Dhu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2031439
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dhu;
    .locals 1

    .prologue
    .line 2031440
    const-class v0, LX/Dhu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dhu;

    return-object v0
.end method

.method public static values()[LX/Dhu;
    .locals 1

    .prologue
    .line 2031438
    sget-object v0, LX/Dhu;->$VALUES:[LX/Dhu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dhu;

    return-object v0
.end method
