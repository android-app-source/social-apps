.class public LX/DiU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DiR;


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2032155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2032156
    iput-object p1, p0, LX/DiU;->a:Ljava/lang/String;

    .line 2032157
    iput-object p2, p0, LX/DiU;->b:LX/0Px;

    .line 2032158
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2032159
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2032160
    iget-object v1, p0, LX/DiU;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2032161
    const-string v1, "topic"

    iget-object v2, p0, LX/DiU;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2032162
    :cond_0
    const-string v1, "options"

    iget-object v2, p0, LX/DiU;->b:LX/0Px;

    invoke-static {v2}, LX/16N;->a(Ljava/lang/Object;)LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2032163
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/DiR;)Z
    .locals 1

    .prologue
    .line 2032164
    instance-of v0, p1, LX/DiU;

    if-nez v0, :cond_0

    .line 2032165
    const/4 v0, 0x0

    .line 2032166
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
