.class public final LX/EAG;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EA0;

.field public final synthetic b:LX/EAH;


# direct methods
.method public constructor <init>(LX/EAH;LX/EA0;)V
    .locals 0

    .prologue
    .line 2085430
    iput-object p1, p0, LX/EAG;->b:LX/EAH;

    iput-object p2, p0, LX/EAG;->a:LX/EA0;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2085423
    iget-object v0, p0, LX/EAG;->a:LX/EA0;

    .line 2085424
    iget-object v1, v0, LX/EA0;->i:LX/0kL;

    new-instance p0, LX/27k;

    const p1, 0x7f0814f8

    invoke-direct {p0, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v1, p0}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2085425
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/EA0;->k:Z

    .line 2085426
    iget-object v1, v0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q()V

    .line 2085427
    iget-object v1, v0, LX/EA0;->g:LX/79D;

    iget-object p0, v0, LX/EA0;->t:Ljava/lang/String;

    .line 2085428
    const-string p1, "reviews_feed_load_stories_failure"

    const-string v0, "reviews_feed"

    invoke-static {v1, p1, v0, p0}, LX/79D;->b(LX/79D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2085429
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085389
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2085390
    iget-object v1, p0, LX/EAG;->a:LX/EA0;

    if-eqz p1, :cond_0

    .line 2085391
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2085392
    if-nez v0, :cond_7

    :cond_0
    const/4 v0, 0x0

    .line 2085393
    :goto_0
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/EA0;->k:Z

    .line 2085394
    iget-object v2, v1, LX/EA0;->g:LX/79D;

    iget-object v3, v1, LX/EA0;->t:Ljava/lang/String;

    .line 2085395
    const-string v4, "reviews_feed_load_stories_success"

    const-string v5, "reviews_feed"

    invoke-static {v2, v4, v5, v3}, LX/79D;->b(LX/79D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2085396
    if-eqz v0, :cond_6

    .line 2085397
    iget-object v2, v1, LX/EA0;->h:LX/EAW;

    .line 2085398
    iget-object v5, v2, LX/EAW;->a:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v7

    .line 2085399
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v10, :cond_2

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel$EdgesModel;

    .line 2085400
    invoke-virtual {v5}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 2085401
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 2085402
    invoke-static {v5}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v5

    .line 2085403
    iput-wide v7, v5, LX/23u;->G:J

    .line 2085404
    move-object v5, v5

    .line 2085405
    invoke-virtual {v5}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 2085406
    iget-object v11, v2, LX/EAW;->b:Ljava/util/List;

    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2085407
    iget-object v11, v2, LX/EAW;->c:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    iget-object v12, v2, LX/EAW;->b:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v5, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2085408
    :cond_1
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 2085409
    :cond_2
    iget-object v2, v1, LX/EA0;->m:LX/1Qq;

    invoke-interface {v2}, LX/1Cw;->notifyDataSetChanged()V

    .line 2085410
    iget-object v2, v1, LX/EA0;->e:LX/EAV;

    iget-object v3, v1, LX/EA0;->j:LX/EA5;

    iget-object v4, v1, LX/EA0;->h:LX/EAW;

    .line 2085411
    const/4 v5, 0x0

    move v6, v5

    :goto_2
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_4

    .line 2085412
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel$EdgesModel;

    invoke-virtual {v5}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 2085413
    if-eqz v5, :cond_3

    .line 2085414
    invoke-static {v2, v5, v3, v4}, LX/EAV;->a(LX/EAV;Lcom/facebook/graphql/model/GraphQLStory;LX/EA5;LX/EAW;)V

    .line 2085415
    :cond_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_2

    .line 2085416
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;->b()LX/0ut;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;->b()LX/0ut;

    move-result-object v2

    invoke-interface {v2}, LX/0ut;->b()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2085417
    :cond_5
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    iput-object v2, v1, LX/EA0;->p:LX/0am;

    .line 2085418
    :cond_6
    :goto_3
    iget-object v2, v1, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v2}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q()V

    .line 2085419
    return-void

    .line 2085420
    :cond_7
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2085421
    check-cast v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel;->a()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;

    move-result-object v0

    goto/16 :goto_0

    .line 2085422
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedStoriesModel$ReviewFeedStoriesModel;->b()LX/0ut;

    move-result-object v2

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, v1, LX/EA0;->p:LX/0am;

    goto :goto_3
.end method
