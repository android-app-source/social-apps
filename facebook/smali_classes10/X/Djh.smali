.class public final LX/Djh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dj9;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;)V
    .locals 0

    .prologue
    .line 2033426
    iput-object p1, p0, LX/Djh;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 4

    .prologue
    .line 2033427
    iget-object v0, p0, LX/Djh;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    .line 2033428
    iput-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->o:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    .line 2033429
    iget-object v0, p0, LX/Djh;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    .line 2033430
    iput-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    .line 2033431
    iget-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    const/4 p1, 0x0

    .line 2033432
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->h:LX/01T;

    sget-object v3, LX/01T;->FB4A:LX/01T;

    if-ne v2, v3, :cond_4

    .line 2033433
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v3, 0x61e

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2033434
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/DnS;->k(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 2033435
    :cond_0
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->p:LX/Djf;

    if-eqz v2, :cond_1

    .line 2033436
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->p:LX/Djf;

    invoke-virtual {v2}, LX/Djf;->a()V

    .line 2033437
    :cond_1
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2033438
    :goto_0
    move-object v2, v2

    .line 2033439
    :goto_1
    move-object v1, v2

    .line 2033440
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->o:LX/0Px;

    .line 2033441
    iget-object v0, p0, LX/Djh;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2033442
    iget-object v0, p0, LX/Djh;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    const v1, 0x7f082b9d

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;I)V

    .line 2033443
    return-void

    .line 2033444
    :cond_2
    if-eqz v1, :cond_3

    invoke-static {v1}, LX/DnS;->k(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 2033445
    :cond_3
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2033446
    :goto_2
    move-object v2, v2

    .line 2033447
    goto :goto_1

    .line 2033448
    :cond_4
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->h:LX/01T;

    sget-object v3, LX/01T;->MESSENGER:LX/01T;

    if-ne v2, v3, :cond_a

    .line 2033449
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->k:LX/0Uh;

    const/16 v3, 0x61e

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2033450
    if-eqz v1, :cond_5

    invoke-static {v1}, LX/DnS;->k(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 2033451
    :cond_5
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->p:LX/Djf;

    if-eqz v2, :cond_6

    .line 2033452
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->p:LX/Djf;

    invoke-virtual {v2}, LX/Djf;->a()V

    .line 2033453
    :cond_6
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2033454
    :goto_3
    move-object v2, v2

    .line 2033455
    goto :goto_1

    .line 2033456
    :cond_7
    if-eqz v1, :cond_8

    invoke-static {v1}, LX/DnS;->k(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Z

    move-result v2

    if-nez v2, :cond_16

    .line 2033457
    :cond_8
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->p:LX/Djf;

    if-eqz v2, :cond_9

    .line 2033458
    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->p:LX/Djf;

    invoke-virtual {v2}, LX/Djf;->a()V

    .line 2033459
    :cond_9
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2033460
    :goto_4
    move-object v2, v2

    .line 2033461
    goto :goto_1

    .line 2033462
    :cond_a
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2033463
    goto :goto_1

    .line 2033464
    :cond_b
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2033465
    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v3}, LX/DnS;->d(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v3

    .line 2033466
    iget-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {p1}, LX/DnS;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object p1

    .line 2033467
    if-nez v3, :cond_c

    if-eqz p1, :cond_d

    .line 2033468
    :cond_c
    sget-object v3, LX/DmC;->SERVICE_BLUR_HEADER:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033469
    :cond_d
    sget-object v3, LX/DmC;->SERVICE_HEADER:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033470
    sget-object v3, LX/DmC;->SERVICE_DATE_ONLY:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033471
    sget-object v3, LX/DmC;->SERVICE_TIME_ONLY:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033472
    sget-object v3, LX/DmC;->SERVICE_LOCATION:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033473
    invoke-static {v1}, LX/DnS;->f(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    move-result-object v3

    if-eqz v3, :cond_e

    .line 2033474
    sget-object v3, LX/DmC;->SERVICE_PHONE_NUMBER:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033475
    :cond_e
    sget-object v3, LX/DmC;->SEND_MESSAGE:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033476
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_0

    .line 2033477
    :cond_f
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2033478
    invoke-static {v1}, LX/DnS;->d(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_10

    .line 2033479
    sget-object v3, LX/DmC;->SERVICE_PHOTO:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033480
    :cond_10
    sget-object v3, LX/DmC;->SERVICE_TIME:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033481
    sget-object v3, LX/DmC;->SERVICE_INFO:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033482
    sget-object v3, LX/DmC;->SERVICE_LOCATION:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033483
    invoke-static {v1}, LX/DnS;->f(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    move-result-object v3

    if-eqz v3, :cond_11

    .line 2033484
    sget-object v3, LX/DmC;->SERVICE_PHONE_NUMBER:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033485
    :cond_11
    sget-object v3, LX/DmC;->SEND_MESSAGE:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033486
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_2

    .line 2033487
    :cond_12
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2033488
    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v3}, LX/DnS;->d(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v3

    .line 2033489
    iget-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {p1}, LX/DnS;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object p1

    .line 2033490
    if-nez v3, :cond_13

    if-eqz p1, :cond_14

    .line 2033491
    :cond_13
    sget-object v3, LX/DmC;->SERVICE_BLUR_HEADER:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033492
    :cond_14
    sget-object v3, LX/DmC;->SERVICE_HEADER:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033493
    sget-object v3, LX/DmC;->SERVICE_DATE_ONLY:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033494
    sget-object v3, LX/DmC;->SERVICE_TIME_ONLY:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033495
    sget-object v3, LX/DmC;->SERVICE_LOCATION:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033496
    invoke-static {v1}, LX/DnS;->f(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    move-result-object v3

    if-eqz v3, :cond_15

    .line 2033497
    sget-object v3, LX/DmC;->SERVICE_PHONE_NUMBER:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033498
    :cond_15
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_3

    .line 2033499
    :cond_16
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2033500
    invoke-static {v1}, LX/DnS;->d(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_17

    .line 2033501
    sget-object v3, LX/DmC;->SERVICE_PHOTO:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033502
    :cond_17
    sget-object v3, LX/DmC;->SERVICE_TIME:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033503
    sget-object v3, LX/DmC;->SERVICE_INFO:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033504
    sget-object v3, LX/DmC;->SERVICE_LOCATION:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033505
    invoke-static {v1}, LX/DnS;->f(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    move-result-object v3

    if-eqz v3, :cond_18

    .line 2033506
    sget-object v3, LX/DmC;->SERVICE_PHONE_NUMBER:LX/DmC;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033507
    :cond_18
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_4
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2033508
    iget-object v0, p0, LX/Djh;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    const v1, 0x7f08003a

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;I)V

    .line 2033509
    iget-object v0, p0, LX/Djh;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    const-string v1, "load_user_appointment_detail"

    invoke-static {v0, v1, p1}, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2033510
    return-void
.end method
