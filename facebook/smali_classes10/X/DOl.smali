.class public LX/DOl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final l:Ljava/lang/String;

.field private static volatile m:LX/DOl;


# instance fields
.field private final a:LX/0Sh;

.field public final b:LX/0bH;

.field public final c:LX/0kL;

.field private final d:LX/1dv;

.field private final e:LX/3mF;

.field public final f:LX/0tX;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/util/concurrent/ExecutorService;

.field public final i:LX/03V;

.field public final j:LX/1dz;

.field public final k:LX/1Sl;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1992766
    const-class v0, LX/DOl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DOl;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0bH;LX/0kL;LX/1dv;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;LX/0tX;LX/3mF;LX/03V;LX/1dz;LX/1Sl;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1992768
    iput-object p1, p0, LX/DOl;->a:LX/0Sh;

    .line 1992769
    iput-object p6, p0, LX/DOl;->g:Ljava/lang/String;

    .line 1992770
    iput-object p5, p0, LX/DOl;->h:Ljava/util/concurrent/ExecutorService;

    .line 1992771
    iput-object p2, p0, LX/DOl;->b:LX/0bH;

    .line 1992772
    iput-object p3, p0, LX/DOl;->c:LX/0kL;

    .line 1992773
    iput-object p4, p0, LX/DOl;->d:LX/1dv;

    .line 1992774
    iput-object p8, p0, LX/DOl;->e:LX/3mF;

    .line 1992775
    iput-object p7, p0, LX/DOl;->f:LX/0tX;

    .line 1992776
    iput-object p9, p0, LX/DOl;->i:LX/03V;

    .line 1992777
    iput-object p10, p0, LX/DOl;->j:LX/1dz;

    .line 1992778
    iput-object p11, p0, LX/DOl;->k:LX/1Sl;

    .line 1992779
    return-void
.end method

.method public static a(LX/0QB;)LX/DOl;
    .locals 15

    .prologue
    .line 1992780
    sget-object v0, LX/DOl;->m:LX/DOl;

    if-nez v0, :cond_1

    .line 1992781
    const-class v1, LX/DOl;

    monitor-enter v1

    .line 1992782
    :try_start_0
    sget-object v0, LX/DOl;->m:LX/DOl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1992783
    if-eqz v2, :cond_0

    .line 1992784
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1992785
    new-instance v3, LX/DOl;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v0}, LX/1dv;->b(LX/0QB;)LX/1dv;

    move-result-object v7

    check-cast v7, LX/1dv;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v11

    check-cast v11, LX/3mF;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {v0}, LX/1dz;->a(LX/0QB;)LX/1dz;

    move-result-object v13

    check-cast v13, LX/1dz;

    invoke-static {v0}, LX/1Sl;->b(LX/0QB;)LX/1Sl;

    move-result-object v14

    check-cast v14, LX/1Sl;

    invoke-direct/range {v3 .. v14}, LX/DOl;-><init>(LX/0Sh;LX/0bH;LX/0kL;LX/1dv;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;LX/0tX;LX/3mF;LX/03V;LX/1dz;LX/1Sl;)V

    .line 1992786
    move-object v0, v3

    .line 1992787
    sput-object v0, LX/DOl;->m:LX/DOl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1992788
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1992789
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1992790
    :cond_1
    sget-object v0, LX/DOl;->m:LX/DOl;

    return-object v0

    .line 1992791
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1992792
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1992763
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1992764
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    .line 1992765
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/DOl;Lcom/facebook/feed/rows/core/props/FeedProps;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1992756
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1992757
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1992758
    iget-object v1, p0, LX/DOl;->b:LX/0bH;

    new-instance v2, LX/1ZX;

    invoke-direct {v2, p1}, LX/1ZX;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1992759
    iget-object v1, p0, LX/DOl;->b:LX/0bH;

    new-instance v2, LX/1YM;

    invoke-direct {v2}, LX/1YM;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1992760
    iget-object v1, p0, LX/DOl;->d:LX/1dv;

    invoke-virtual {v1, v0}, LX/1dv;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1992761
    iget-object v2, p0, LX/DOl;->a:LX/0Sh;

    new-instance v3, LX/DOk;

    invoke-direct {v3, p0, v0, p2}, LX/DOk;-><init>(LX/DOl;Lcom/facebook/graphql/model/GraphQLStory;I)V

    invoke-virtual {v2, v1, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1992762
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1992670
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1992671
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1992672
    new-instance v1, LX/0ju;

    invoke-direct {v1, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081053

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f081062

    new-instance v3, LX/DOf;

    invoke-direct {v3, p0, v0, p3, p2}, LX/DOf;-><init>(LX/DOl;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0810d9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1992673
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1992674
    iget-object v0, p0, LX/DOl;->k:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1992675
    iget-object v0, p0, LX/DOl;->b:LX/0bH;

    new-instance v1, LX/DNw;

    invoke-direct {v1, p1, v2, v2}, LX/DNw;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1992676
    :cond_0
    iget-object v1, p0, LX/DOl;->e:LX/3mF;

    .line 1992677
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1992678
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v5, 0x1

    .line 1992679
    new-instance v2, LX/4Fe;

    invoke-direct {v2}, LX/4Fe;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    .line 1992680
    const-string v4, "group_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992681
    move-object v2, v2

    .line 1992682
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    .line 1992683
    const-string v4, "story_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992684
    move-object v2, v2

    .line 1992685
    new-instance v3, LX/B2b;

    invoke-direct {v3}, LX/B2b;-><init>()V

    move-object v3, v3

    .line 1992686
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1992687
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 1992688
    iget-object v3, v1, LX/3mF;->b:LX/1Sl;

    invoke-virtual {v3}, LX/1Sl;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1992689
    new-instance v3, LX/B2i;

    invoke-direct {v3}, LX/B2i;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    .line 1992690
    iput-object v4, v3, LX/B2i;->a:Ljava/lang/String;

    .line 1992691
    move-object v3, v3

    .line 1992692
    iput-boolean v5, v3, LX/B2i;->b:Z

    .line 1992693
    move-object v3, v3

    .line 1992694
    invoke-virtual {v3}, LX/B2i;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/399;->a(LX/0jT;)LX/399;

    .line 1992695
    iput-boolean v5, v2, LX/399;->d:Z

    .line 1992696
    :cond_1
    iget-object v3, v1, LX/3mF;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1992697
    iget-object v1, p0, LX/DOl;->a:LX/0Sh;

    new-instance v2, LX/DOe;

    invoke-direct {v2, p0, p1}, LX/DOe;-><init>(LX/DOl;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1992698
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/DOJ;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/DOJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1992699
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1992700
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1992701
    iget-object v1, p0, LX/DOl;->k:LX/1Sl;

    invoke-virtual {v1}, LX/1Sl;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1992702
    iget-object v1, p0, LX/DOl;->b:LX/0bH;

    new-instance v2, LX/DNz;

    const/4 v3, 0x1

    invoke-direct {v2, p1, p2, v3}, LX/DNz;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/DOJ;Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1992703
    :cond_0
    sget-object v1, LX/DOJ;->Pin:LX/DOJ;

    if-ne p2, v1, :cond_2

    iget-object v1, p0, LX/DOl;->e:LX/3mF;

    const-string v2, "group_mall"

    const/4 v6, 0x1

    .line 1992704
    new-instance v3, LX/4Fr;

    invoke-direct {v3}, LX/4Fr;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v4

    .line 1992705
    const-string v5, "group_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992706
    move-object v3, v3

    .line 1992707
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    .line 1992708
    const-string v5, "story_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992709
    move-object v3, v3

    .line 1992710
    const-string v4, "source"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992711
    move-object v3, v3

    .line 1992712
    new-instance v4, LX/B2c;

    invoke-direct {v4}, LX/B2c;-><init>()V

    move-object v4, v4

    .line 1992713
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1992714
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1992715
    iget-object v4, v1, LX/3mF;->b:LX/1Sl;

    invoke-virtual {v4}, LX/1Sl;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1992716
    new-instance v4, LX/B2i;

    invoke-direct {v4}, LX/B2i;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    .line 1992717
    iput-object v5, v4, LX/B2i;->a:Ljava/lang/String;

    .line 1992718
    move-object v4, v4

    .line 1992719
    iput-boolean v6, v4, LX/B2i;->d:Z

    .line 1992720
    move-object v4, v4

    .line 1992721
    const/4 v5, 0x0

    .line 1992722
    iput-boolean v5, v4, LX/B2i;->e:Z

    .line 1992723
    move-object v4, v4

    .line 1992724
    invoke-virtual {v4}, LX/B2i;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/399;->a(LX/0jT;)LX/399;

    .line 1992725
    iput-boolean v6, v3, LX/399;->d:Z

    .line 1992726
    :cond_1
    iget-object v4, v1, LX/3mF;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 1992727
    :goto_0
    iget-object v1, p0, LX/DOl;->a:LX/0Sh;

    new-instance v2, LX/DOd;

    invoke-direct {v2, p0, p1, p2}, LX/DOd;-><init>(LX/DOl;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DOJ;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1992728
    return-void

    .line 1992729
    :cond_2
    iget-object v1, p0, LX/DOl;->e:LX/3mF;

    const-string v2, "group_mall"

    const/4 v6, 0x1

    .line 1992730
    new-instance v3, LX/4GC;

    invoke-direct {v3}, LX/4GC;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v4

    .line 1992731
    const-string v5, "group_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992732
    move-object v3, v3

    .line 1992733
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    .line 1992734
    const-string v5, "story_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992735
    move-object v3, v3

    .line 1992736
    const-string v4, "source"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992737
    move-object v3, v3

    .line 1992738
    new-instance v4, LX/B2d;

    invoke-direct {v4}, LX/B2d;-><init>()V

    move-object v4, v4

    .line 1992739
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1992740
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1992741
    iget-object v4, v1, LX/3mF;->b:LX/1Sl;

    invoke-virtual {v4}, LX/1Sl;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1992742
    new-instance v4, LX/B2i;

    invoke-direct {v4}, LX/B2i;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    .line 1992743
    iput-object v5, v4, LX/B2i;->a:Ljava/lang/String;

    .line 1992744
    move-object v4, v4

    .line 1992745
    const/4 v5, 0x0

    .line 1992746
    iput-boolean v5, v4, LX/B2i;->d:Z

    .line 1992747
    move-object v4, v4

    .line 1992748
    iput-boolean v6, v4, LX/B2i;->e:Z

    .line 1992749
    move-object v4, v4

    .line 1992750
    invoke-virtual {v4}, LX/B2i;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/399;->a(LX/0jT;)LX/399;

    .line 1992751
    iput-boolean v6, v3, LX/399;->d:Z

    .line 1992752
    :cond_3
    iget-object v4, v1, LX/3mF;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 1992753
    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1992754
    new-instance v0, LX/0ju;

    invoke-direct {v0, p2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08104e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08105c

    new-instance v2, LX/DOj;

    invoke-direct {v2, p0, p1}, LX/DOj;-><init>(LX/DOl;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0810d9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1992755
    return-void
.end method
