.class public final enum LX/CyW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CyW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CyW;

.field public static final enum APP_SEARCH:LX/CyW;

.field public static final enum BLENDED_SEARCH:LX/CyW;

.field public static final enum COMMERCE_FOR_SALE_GROUP_SCOPED_SEARCH:LX/CyW;

.field public static final enum COMMERCE_SEARCH:LX/CyW;

.field public static final enum EVENT_SEARCH:LX/CyW;

.field public static final enum GROUP_SEARCH:LX/CyW;

.field public static final enum PAGE_SEARCH:LX/CyW;

.field public static final enum PHOTO_SEARCH:LX/CyW;

.field public static final enum USER_SEARCH:LX/CyW;

.field public static final enum VIDEO_SEARCH:LX/CyW;

.field private static final sDisplayStyleToCallSiteMap:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "LX/CyW;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1953024
    new-instance v0, LX/CyW;

    const-string v1, "BLENDED_SEARCH"

    const-string v2, "android:blended_search"

    invoke-direct {v0, v1, v4, v2}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->BLENDED_SEARCH:LX/CyW;

    .line 1953025
    new-instance v0, LX/CyW;

    const-string v1, "USER_SEARCH"

    const-string v2, "android:user_search"

    invoke-direct {v0, v1, v5, v2}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->USER_SEARCH:LX/CyW;

    .line 1953026
    new-instance v0, LX/CyW;

    const-string v1, "EVENT_SEARCH"

    const-string v2, "android:event_search"

    invoke-direct {v0, v1, v6, v2}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->EVENT_SEARCH:LX/CyW;

    .line 1953027
    new-instance v0, LX/CyW;

    const-string v1, "PAGE_SEARCH"

    const-string v2, "android:page_search"

    invoke-direct {v0, v1, v7, v2}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->PAGE_SEARCH:LX/CyW;

    .line 1953028
    new-instance v0, LX/CyW;

    const-string v1, "APP_SEARCH"

    const-string v2, "android:app_search"

    invoke-direct {v0, v1, v8, v2}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->APP_SEARCH:LX/CyW;

    .line 1953029
    new-instance v0, LX/CyW;

    const-string v1, "GROUP_SEARCH"

    const/4 v2, 0x5

    const-string v3, "android:group_search"

    invoke-direct {v0, v1, v2, v3}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->GROUP_SEARCH:LX/CyW;

    .line 1953030
    new-instance v0, LX/CyW;

    const-string v1, "PHOTO_SEARCH"

    const/4 v2, 0x6

    const-string v3, "android:photo_search"

    invoke-direct {v0, v1, v2, v3}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->PHOTO_SEARCH:LX/CyW;

    .line 1953031
    new-instance v0, LX/CyW;

    const-string v1, "VIDEO_SEARCH"

    const/4 v2, 0x7

    const-string v3, "android:video_search"

    invoke-direct {v0, v1, v2, v3}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->VIDEO_SEARCH:LX/CyW;

    .line 1953032
    new-instance v0, LX/CyW;

    const-string v1, "COMMERCE_SEARCH"

    const/16 v2, 0x8

    const-string v3, "browse:commerce:global_android"

    invoke-direct {v0, v1, v2, v3}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->COMMERCE_SEARCH:LX/CyW;

    .line 1953033
    new-instance v0, LX/CyW;

    const-string v1, "COMMERCE_FOR_SALE_GROUP_SCOPED_SEARCH"

    const/16 v2, 0x9

    const-string v3, "browse:commerce:groups_android"

    invoke-direct {v0, v1, v2, v3}, LX/CyW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CyW;->COMMERCE_FOR_SALE_GROUP_SCOPED_SEARCH:LX/CyW;

    .line 1953034
    const/16 v0, 0xa

    new-array v0, v0, [LX/CyW;

    sget-object v1, LX/CyW;->BLENDED_SEARCH:LX/CyW;

    aput-object v1, v0, v4

    sget-object v1, LX/CyW;->USER_SEARCH:LX/CyW;

    aput-object v1, v0, v5

    sget-object v1, LX/CyW;->EVENT_SEARCH:LX/CyW;

    aput-object v1, v0, v6

    sget-object v1, LX/CyW;->PAGE_SEARCH:LX/CyW;

    aput-object v1, v0, v7

    sget-object v1, LX/CyW;->APP_SEARCH:LX/CyW;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/CyW;->GROUP_SEARCH:LX/CyW;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CyW;->PHOTO_SEARCH:LX/CyW;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CyW;->VIDEO_SEARCH:LX/CyW;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/CyW;->COMMERCE_SEARCH:LX/CyW;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/CyW;->COMMERCE_FOR_SALE_GROUP_SCOPED_SEARCH:LX/CyW;

    aput-object v2, v0, v1

    sput-object v0, LX/CyW;->$VALUES:[LX/CyW;

    .line 1953035
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->BLENDED_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->PHOTO_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->PHOTO_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->VIDEO_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->EVENT_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->USER_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->PAGE_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->APP_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->GROUP_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->VIDEO_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->VIDEO_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->VIDEO_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->VIDEO_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->BLENDED_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->BLENDED_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->NEWS_LINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, LX/CyW;->BLENDED_SEARCH:LX/CyW;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/CyW;->sDisplayStyleToCallSiteMap:LX/0P1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1953036
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1953037
    iput-object p3, p0, LX/CyW;->name:Ljava/lang/String;

    .line 1953038
    return-void
.end method

.method public static fromDisplayStyle(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/CyW;
    .locals 3

    .prologue
    .line 1953039
    sget-object v0, LX/CyW;->sDisplayStyleToCallSiteMap:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyW;

    .line 1953040
    if-nez v0, :cond_0

    .line 1953041
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unregistered callsite for display style "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1953042
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CyW;
    .locals 1

    .prologue
    .line 1953043
    const-class v0, LX/CyW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CyW;

    return-object v0
.end method

.method public static values()[LX/CyW;
    .locals 1

    .prologue
    .line 1953044
    sget-object v0, LX/CyW;->$VALUES:[LX/CyW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CyW;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1953045
    iget-object v0, p0, LX/CyW;->name:Ljava/lang/String;

    return-object v0
.end method
