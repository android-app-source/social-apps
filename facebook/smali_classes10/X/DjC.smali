.class public final LX/DjC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DjB;


# instance fields
.field public final synthetic a:LX/DjE;


# direct methods
.method public constructor <init>(LX/DjE;)V
    .locals 0

    .prologue
    .line 2032916
    iput-object p1, p0, LX/DjC;->a:LX/DjE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2032917
    iget-object v0, p0, LX/DjC;->a:LX/DjE;

    iget-object v0, v0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->o:Ljava/lang/String;

    const-string v1, "list"

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032918
    iget-object v0, p0, LX/DjC;->a:LX/DjE;

    iget-object v0, v0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/DjC;->a:LX/DjE;

    iget-object v1, v1, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082bb6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2032919
    :cond_0
    iget-object v0, p0, LX/DjC;->a:LX/DjE;

    iget-object v0, v0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2032920
    iget-object v0, p0, LX/DjC;->a:LX/DjE;

    iget-object v0, v0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2032921
    iget-object v0, p0, LX/DjC;->a:LX/DjE;

    iget-object v0, v0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2032922
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2032923
    iget-object v0, p0, LX/DjC;->a:LX/DjE;

    iget-object v0, v0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->k:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08003c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2032924
    return-void
.end method
