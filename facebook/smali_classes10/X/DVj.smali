.class public LX/DVj;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public a:LX/B1b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B1b",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/B1b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B1b",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/DVd;

.field public e:LX/DVf;

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/DVU;

.field private final j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2005660
    const-class v0, LX/DVj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DVj;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;LX/DVU;LX/DVg;LX/DVe;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/DVU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2005702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2005703
    new-instance v0, LX/DVh;

    invoke-direct {v0, p0}, LX/DVh;-><init>(LX/DVj;)V

    iput-object v0, p0, LX/DVj;->a:LX/B1b;

    .line 2005704
    new-instance v0, LX/DVi;

    invoke-direct {v0, p0}, LX/DVi;-><init>(LX/DVj;)V

    iput-object v0, p0, LX/DVj;->b:LX/B1b;

    .line 2005705
    iget-object v0, p0, LX/DVj;->a:LX/B1b;

    .line 2005706
    new-instance v3, LX/DVd;

    invoke-static {p5}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p5}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-direct {v3, v1, p1, v2, v0}, LX/DVd;-><init>(LX/1Ck;Ljava/lang/String;LX/0tX;LX/B1b;)V

    .line 2005707
    move-object v0, v3

    .line 2005708
    iput-object v0, p0, LX/DVj;->d:LX/DVd;

    .line 2005709
    iget-object v0, p0, LX/DVj;->b:LX/B1b;

    .line 2005710
    new-instance v1, LX/DVf;

    invoke-static {p4}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p4}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    move-object v3, p1

    move-object v4, p2

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, LX/DVf;-><init>(LX/1Ck;Ljava/lang/String;Ljava/lang/Integer;LX/0tX;LX/B1b;)V

    .line 2005711
    move-object v0, v1

    .line 2005712
    iput-object v0, p0, LX/DVj;->e:LX/DVf;

    .line 2005713
    iput-object p3, p0, LX/DVj;->i:LX/DVU;

    .line 2005714
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/DVj;->j:I

    .line 2005715
    return-void
.end method

.method private a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005675
    iget-object v0, p0, LX/DVj;->h:Ljava/util/Set;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 2005676
    :cond_0
    :goto_0
    return-object p1

    .line 2005677
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2005678
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2005679
    iget-object v4, p0, LX/DVj;->h:Ljava/util/Set;

    .line 2005680
    iget-object v5, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2005681
    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2005682
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2005683
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2005684
    :cond_3
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    goto :goto_0
.end method

.method public static a$redex0(LX/DVj;Ljava/lang/String;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2005685
    const-string v0, "member_suggestions_section"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2005686
    iput-object p2, p0, LX/DVj;->f:LX/0Px;

    .line 2005687
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/DVj;->h:Ljava/util/Set;

    .line 2005688
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2005689
    iget-object v4, p0, LX/DVj;->h:Ljava/util/Set;

    .line 2005690
    iget-object p1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p1

    .line 2005691
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2005692
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2005693
    :cond_0
    iget-object v0, p0, LX/DVj;->g:LX/0Px;

    if-eqz v0, :cond_1

    .line 2005694
    iget-object v0, p0, LX/DVj;->g:LX/0Px;

    invoke-direct {p0, v0}, LX/DVj;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/DVj;->g:LX/0Px;

    .line 2005695
    :cond_1
    :goto_1
    invoke-virtual {p0, v2}, LX/DVj;->a(Z)V

    .line 2005696
    return-void

    .line 2005697
    :cond_2
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    iget v1, p0, LX/DVj;->j:I

    if-le v0, v1, :cond_3

    .line 2005698
    invoke-direct {p0, p2}, LX/DVj;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/DVj;->g:LX/0Px;

    goto :goto_1

    .line 2005699
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005700
    iput-object v0, p0, LX/DVj;->f:LX/0Px;

    .line 2005701
    iput-object p2, p0, LX/DVj;->g:LX/0Px;

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2005671
    iget-object v0, p0, LX/DVj;->f:LX/0Px;

    if-nez v0, :cond_0

    .line 2005672
    iget-object v0, p0, LX/DVj;->e:LX/DVf;

    invoke-virtual {v0}, LX/B1d;->f()V

    .line 2005673
    :cond_0
    iget-object v0, p0, LX/DVj;->d:LX/DVd;

    invoke-virtual {v0}, LX/B1d;->f()V

    .line 2005674
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2005661
    iget-object v0, p0, LX/DVj;->f:LX/0Px;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/DVj;->g:LX/0Px;

    if-eqz v0, :cond_2

    .line 2005662
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2005663
    iget-object v1, p0, LX/DVj;->f:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2005664
    const-string v1, "member_suggestions_section"

    iget-object v2, p0, LX/DVj;->f:LX/0Px;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2005665
    :cond_0
    iget-object v1, p0, LX/DVj;->g:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 2005666
    const-string v1, "member_picker_merged_section"

    iget-object v2, p0, LX/DVj;->g:LX/0Px;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2005667
    iget-object v1, p0, LX/DVj;->i:LX/DVU;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    invoke-interface {v1, v0}, LX/DVU;->a(LX/0P1;)V

    .line 2005668
    :cond_1
    :goto_0
    return-void

    .line 2005669
    :cond_2
    if-eqz p1, :cond_1

    .line 2005670
    invoke-virtual {p0}, LX/DVj;->a()V

    goto :goto_0
.end method
