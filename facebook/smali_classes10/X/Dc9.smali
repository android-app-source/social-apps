.class public interface abstract LX/Dc9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dc8;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "PageMenuTypeHandler"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract a(LX/15i;I)Z
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "isVisibleMenu"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end method

.method public abstract b(LX/15i;I)Z
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "shouldShowInManagementScreen"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end method
