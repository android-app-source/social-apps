.class public abstract LX/DqK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2048273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x4ac8992a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2048274
    instance-of v1, p1, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    if-nez v1, :cond_0

    .line 2048275
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    const v2, -0x5c82966d

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v1

    .line 2048276
    :cond_0
    check-cast p1, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2048277
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->toggle()V

    .line 2048278
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->isChecked()Z

    .line 2048279
    const v1, 0x2d043bf2

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void
.end method
