.class public final LX/DZK;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/Daz;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;

.field public final synthetic b:LX/DZW;


# direct methods
.method public constructor <init>(LX/DZW;LX/DML;Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;)V
    .locals 0

    .prologue
    .line 2013277
    iput-object p1, p0, LX/DZK;->b:LX/DZW;

    iput-object p3, p0, LX/DZK;->a:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2013278
    check-cast p1, LX/Daz;

    .line 2013279
    iget-object v0, p0, LX/DZK;->a:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, LX/DZK;->a:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/DZK;->b:LX/DZW;

    iget-object v3, v3, LX/DZW;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iget-object v3, p0, LX/DZK;->a:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, LX/Daz;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 2013280
    iget-object v0, p0, LX/DZK;->b:LX/DZW;

    iget-object v0, v0, LX/DZW;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, LX/Daz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2013281
    return-void
.end method
