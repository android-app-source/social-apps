.class public LX/DDM;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DDK;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1975390
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DDM;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975464
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1975465
    iput-object p1, p0, LX/DDM;->b:LX/0Ot;

    .line 1975466
    return-void
.end method

.method public static a(LX/0QB;)LX/DDM;
    .locals 4

    .prologue
    .line 1975453
    const-class v1, LX/DDM;

    monitor-enter v1

    .line 1975454
    :try_start_0
    sget-object v0, LX/DDM;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975455
    sput-object v2, LX/DDM;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975456
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975457
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975458
    new-instance v3, LX/DDM;

    const/16 p0, 0x1f5a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DDM;-><init>(LX/0Ot;)V

    .line 1975459
    move-object v0, v3

    .line 1975460
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975461
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DDM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975462
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975463
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1975452
    const v0, 0x54c552ff    # 6.780008E12f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1975422
    check-cast p2, LX/DDL;

    .line 1975423
    iget-object v0, p0, LX/DDM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;

    iget-object v1, p2, LX/DDL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975424
    const-string v6, ""

    .line 1975425
    const-string v3, ""

    .line 1975426
    const-string v4, ""

    .line 1975427
    const-string v5, ""

    .line 1975428
    if-eqz v1, :cond_4

    .line 1975429
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1975430
    if-eqz v2, :cond_4

    .line 1975431
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1975432
    check-cast v2, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1975433
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1975434
    check-cast v2, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    .line 1975435
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v6

    .line 1975436
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1975437
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->N()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 1975438
    :goto_0
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1975439
    iget-object v3, v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->c:Landroid/content/res/Resources;

    const v4, 0x7f0f00d1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->a()I

    move-result v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->f:LX/154;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->r()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->a()I

    move-result p0

    invoke-virtual {v11, p0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v3, v4, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1975440
    :goto_1
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1975441
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->m()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    move-object v5, v6

    move-object p2, v2

    move-object v2, v4

    move-object v4, p2

    .line 1975442
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f020a3c

    invoke-interface {v6, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v6

    if-nez v2, :cond_0

    const-string v2, ""

    :cond_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1975443
    iget-object v7, v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->b:LX/1nu;

    invoke-virtual {v7, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v7

    invoke-virtual {v7, v2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v7

    const v8, 0x7f020c6c

    invoke-virtual {v7, v8}, LX/1nw;->h(I)LX/1nw;

    move-result-object v7

    sget-object v8, LX/1Up;->g:LX/1Up;

    invoke-virtual {v7, v8}, LX/1nw;->b(LX/1Up;)LX/1nw;

    move-result-object v7

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1nw;->a(LX/4Ab;)LX/1nw;

    move-result-object v7

    sget-object v8, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, v8}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v8, 0x7f0b1255

    invoke-interface {v7, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b1255

    invoke-interface {v7, v8}, LX/1Di;->q(I)LX/1Di;

    move-result-object v7

    const/16 v8, 0x8

    const v9, 0x7f0b1256

    invoke-interface {v7, v8, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    move-object v2, v7

    .line 1975444
    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v8, 0x1

    .line 1975445
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v6

    const v7, 0x7f0a009e

    invoke-virtual {v6, v7}, LX/25Q;->i(I)LX/25Q;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-interface {v6, v8}, LX/1Di;->c(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b125a

    invoke-interface {v6, v8, v7}, LX/1Di;->l(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x0

    const v8, 0x7f0b1254

    invoke-interface {v6, v7, v8}, LX/1Di;->l(II)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1250

    invoke-interface {v6, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1258

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    move-object v6, v6

    .line 1975446
    invoke-interface {v2, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 1975447
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1ne;->t(I)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0050

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a0099

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v9}, LX/1ne;->j(I)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1250

    invoke-interface {v6, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1252

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1253

    invoke-interface {v6, v8, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x0

    const v8, 0x7f0b1254

    invoke-interface {v6, v7, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1254

    invoke-interface {v6, v9, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    move-object v5, v6

    .line 1975448
    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1, v4}, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->b(LX/1De;Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v4

    const/4 v5, 0x1

    const v6, 0x7f0b1253

    invoke-interface {v4, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0b1254

    invoke-interface {v4, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x2

    const v6, 0x7f0b1254

    invoke-interface {v4, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1, v3}, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->b(LX/1De;Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0x7f0b1254

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x2

    const v5, 0x7f0b1254

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x3

    const v5, 0x7f0b1257

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 1975449
    const v3, 0x54c552ff    # 6.780008E12f

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1975450
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1975451
    return-object v0

    :cond_1
    move-object v4, v2

    move-object v2, v5

    move-object v5, v6

    goto/16 :goto_2

    :cond_2
    move-object v3, v4

    goto/16 :goto_1

    :cond_3
    move-object v2, v3

    goto/16 :goto_0

    :cond_4
    move-object v2, v5

    move-object v5, v6

    move-object p2, v3

    move-object v3, v4

    move-object v4, p2

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1975391
    invoke-static {}, LX/1dS;->b()V

    .line 1975392
    iget v0, p1, LX/1dQ;->b:I

    .line 1975393
    packed-switch v0, :pswitch_data_0

    .line 1975394
    :goto_0
    return-object v2

    .line 1975395
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1975396
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1975397
    check-cast v1, LX/DDL;

    .line 1975398
    iget-object v3, p0, LX/DDM;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;

    iget-object v4, v1, LX/DDL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975399
    if-eqz v4, :cond_0

    .line 1975400
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1975401
    if-eqz v5, :cond_0

    .line 1975402
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1975403
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1975404
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1975405
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1975406
    :cond_0
    :goto_1
    goto :goto_0

    .line 1975407
    :cond_1
    new-instance p1, LX/5QQ;

    invoke-direct {p1}, LX/5QQ;-><init>()V

    .line 1975408
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1975409
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/5QQ;->a(Ljava/lang/String;)LX/5QQ;

    move-result-object p1

    .line 1975410
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1975411
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/5QQ;->b(Ljava/lang/String;)LX/5QQ;

    move-result-object v5

    .line 1975412
    iget-object p1, v5, LX/5QQ;->a:Landroid/os/Bundle;

    move-object p2, p1

    .line 1975413
    iget-object v5, v3, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->C:Ljava/lang/String;

    .line 1975414
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1975415
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p0, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1975416
    iget-object v5, v3, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1g8;

    .line 1975417
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1975418
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object p1

    .line 1975419
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object p2

    const-string p0, "group_id"

    invoke-virtual {p2, p0, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object p2

    .line 1975420
    iget-object p0, v5, LX/1g8;->d:LX/0if;

    sget-object v1, LX/0ig;->aK:LX/0ih;

    const-string v3, "gpymi_group_intro_click"

    const-string v0, "GROUP_FEED_PYMI"

    invoke-virtual {p0, v1, v3, v0, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1975421
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x54c552ff
        :pswitch_0
    .end packed-switch
.end method
