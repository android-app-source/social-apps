.class public LX/DHo;
.super LX/DHn;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field private final a:Lcom/facebook/drawee/view/GenericDraweeView;

.field private b:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1982513
    const v0, 0x7f0313f8

    invoke-direct {p0, p1, v0}, LX/DHo;-><init>(Landroid/content/Context;I)V

    .line 1982514
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 1982515
    invoke-direct {p0, p1, p2}, LX/DHn;-><init>(Landroid/content/Context;I)V

    .line 1982516
    const v0, 0x7f0d1507

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/DHo;->b:Landroid/widget/LinearLayout;

    .line 1982517
    const v0, 0x7f0d2de6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/GenericDraweeView;

    iput-object v0, p0, LX/DHo;->a:Lcom/facebook/drawee/view/GenericDraweeView;

    .line 1982518
    invoke-virtual {p0}, LX/DHo;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1982519
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a045d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1982520
    new-instance v2, LX/1Uo;

    invoke-direct {v2, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1982521
    iput-object v1, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1982522
    move-object v0, v2

    .line 1982523
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1982524
    iget-object v1, p0, LX/DHo;->a:Lcom/facebook/drawee/view/GenericDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1982525
    return-void
.end method


# virtual methods
.method public getPhoto()Lcom/facebook/drawee/view/GenericDraweeView;
    .locals 1

    .prologue
    .line 1982526
    iget-object v0, p0, LX/DHo;->a:Lcom/facebook/drawee/view/GenericDraweeView;

    return-object v0
.end method

.method public setWidth(I)V
    .locals 2

    .prologue
    .line 1982527
    iget-object v0, p0, LX/DHo;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1982528
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1982529
    iget-object v1, p0, LX/DHo;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1982530
    iget-object v0, p0, LX/DHo;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 1982531
    return-void
.end method
