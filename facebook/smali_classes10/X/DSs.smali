.class public LX/DSs;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1999955
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DSs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1999956
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1999944
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1999945
    const v0, 0x7f03081e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1999946
    const v0, 0x7f0d1548

    invoke-virtual {p0, v0}, LX/DSs;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DSs;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1999947
    return-void
.end method

.method public static setViewVisibility(LX/DSs;Z)V
    .locals 2

    .prologue
    .line 1999950
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 1999951
    :goto_0
    iget-object v1, p0, LX/DSs;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1999952
    invoke-virtual {p0, v0}, LX/DSs;->setVisibility(I)V

    .line 1999953
    return-void

    .line 1999954
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1999948
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/DSs;->setViewVisibility(LX/DSs;Z)V

    .line 1999949
    return-void
.end method
