.class public LX/EPO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EPM;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2116658
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EPO;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EPP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116659
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2116660
    iput-object p1, p0, LX/EPO;->b:LX/0Ot;

    .line 2116661
    return-void
.end method

.method public static a(LX/0QB;)LX/EPO;
    .locals 4

    .prologue
    .line 2116662
    const-class v1, LX/EPO;

    monitor-enter v1

    .line 2116663
    :try_start_0
    sget-object v0, LX/EPO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116664
    sput-object v2, LX/EPO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116665
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116666
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116667
    new-instance v3, LX/EPO;

    const/16 p0, 0x3491

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EPO;-><init>(LX/0Ot;)V

    .line 2116668
    move-object v0, v3

    .line 2116669
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116670
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EPO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116671
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116672
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2116673
    check-cast p2, LX/EPN;

    .line 2116674
    iget-object v0, p0, LX/EPO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPP;

    iget-object v1, p2, LX/EPN;->a:Ljava/lang/CharSequence;

    const/4 p2, 0x0

    .line 2116675
    const v2, 0x7f0e0123

    invoke-static {p1, p2, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    iget-object v3, v0, LX/EPP;->a:LX/23P;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 p0, 0x0

    invoke-virtual {v3, v4, p0}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    sget-object v3, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, v3}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-interface {v2, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    .line 2116676
    const v3, -0x4b89d0aa

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2116677
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2116678
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2116679
    invoke-static {}, LX/1dS;->b()V

    .line 2116680
    iget v0, p1, LX/1dQ;->b:I

    .line 2116681
    packed-switch v0, :pswitch_data_0

    .line 2116682
    :goto_0
    return-object v2

    .line 2116683
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2116684
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2116685
    check-cast v1, LX/EPN;

    .line 2116686
    iget-object p1, p0, LX/EPO;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/EPN;->b:Landroid/view/View$OnClickListener;

    .line 2116687
    if-eqz p1, :cond_0

    .line 2116688
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2116689
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x4b89d0aa
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/EPM;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2116690
    new-instance v1, LX/EPN;

    invoke-direct {v1, p0}, LX/EPN;-><init>(LX/EPO;)V

    .line 2116691
    sget-object v2, LX/EPO;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EPM;

    .line 2116692
    if-nez v2, :cond_0

    .line 2116693
    new-instance v2, LX/EPM;

    invoke-direct {v2}, LX/EPM;-><init>()V

    .line 2116694
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EPM;->a$redex0(LX/EPM;LX/1De;IILX/EPN;)V

    .line 2116695
    move-object v1, v2

    .line 2116696
    move-object v0, v1

    .line 2116697
    return-object v0
.end method
