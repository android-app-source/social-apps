.class public LX/Eab;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2142059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/EZh;[B[BJ[B[B[B)I
    .locals 7

    .prologue
    .line 2142060
    const/16 v0, 0x40

    new-array v1, v0, [B

    .line 2142061
    const/16 v0, 0x40

    new-array v2, v0, [B

    .line 2142062
    new-instance v3, LX/EaI;

    invoke-direct {v3}, LX/EaI;-><init>()V

    .line 2142063
    const/4 v0, 0x0

    const/16 v4, 0x40

    long-to-int v5, p3

    invoke-static {p2, v0, p1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2142064
    const/4 v0, 0x0

    const/16 v4, 0x20

    const/16 v5, 0x20

    invoke-static {p5, v0, p1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2142065
    const/4 v0, 0x0

    const/4 v4, -0x2

    aput-byte v4, p1, v0

    .line 2142066
    const/4 v0, 0x1

    :goto_0
    const/16 v4, 0x20

    if-ge v0, v4, :cond_0

    .line 2142067
    const/4 v4, -0x1

    aput-byte v4, p1, v0

    .line 2142068
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2142069
    :cond_0
    const/4 v0, 0x0

    const-wide/16 v4, 0x40

    add-long/2addr v4, p3

    long-to-int v4, v4

    const/16 v5, 0x40

    invoke-static {p7, v0, p1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2142070
    const-wide/16 v4, 0x80

    add-long/2addr v4, p3

    invoke-virtual {p0, v1, p1, v4, v5}, LX/EZh;->a([B[BJ)V

    .line 2142071
    const/4 v0, 0x0

    const/16 v4, 0x20

    const/16 v5, 0x20

    invoke-static {p6, v0, p1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2142072
    invoke-static {v1}, LX/EaZ;->a([B)V

    .line 2142073
    invoke-static {v3, v1}, LX/EaU;->a(LX/EaI;[B)V

    .line 2142074
    invoke-static {p1, v3}, LX/EaN;->a([BLX/EaI;)V

    .line 2142075
    const-wide/16 v4, 0x40

    add-long/2addr v4, p3

    invoke-virtual {p0, v2, p1, v4, v5}, LX/EZh;->a([B[BJ)V

    .line 2142076
    invoke-static {v2}, LX/EaZ;->a([B)V

    .line 2142077
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 2142078
    invoke-static {v0, v2, p5, v1}, LX/EaY;->a([B[B[B[B)V

    .line 2142079
    const/4 v1, 0x0

    const/16 v2, 0x20

    const/16 v3, 0x20

    invoke-static {v0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2142080
    const/4 v0, 0x0

    return v0
.end method
