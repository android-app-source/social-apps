.class public LX/Cve;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/Cvd;

.field public static final b:LX/Cvc;

.field public static final c:LX/Cvb;

.field private static final d:Ljava/lang/String;

.field private static final g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile s:LX/Cve;


# instance fields
.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/CwW;",
            "Ljava/util/List",
            "<",
            "LX/CvT;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/CvT;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/11i;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final i:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final j:LX/0So;

.field private final k:LX/0oz;

.field private final l:LX/0ad;

.field private final m:LX/0SG;

.field private final n:LX/0Uh;

.field private final o:LX/0vV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vV",
            "<",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private r:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1948932
    const-string v0, "0"

    sput-object v0, LX/Cve;->d:Ljava/lang/String;

    .line 1948933
    new-instance v0, LX/Cvd;

    invoke-direct {v0}, LX/Cvd;-><init>()V

    sput-object v0, LX/Cve;->a:LX/Cvd;

    .line 1948934
    new-instance v0, LX/Cvc;

    invoke-direct {v0}, LX/Cvc;-><init>()V

    sput-object v0, LX/Cve;->b:LX/Cvc;

    .line 1948935
    new-instance v0, LX/Cvb;

    invoke-direct {v0}, LX/Cvb;-><init>()V

    sput-object v0, LX/Cve;->c:LX/Cvb;

    .line 1948936
    const-string v0, "version"

    const-string v1, "2"

    const-string v2, "see_more"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/Cve;->g:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/11i;LX/0So;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0oz;LX/0ad;LX/0SG;LX/0Uh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1948937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1948938
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Cve;->e:Ljava/util/Map;

    .line 1948939
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Cve;->f:Ljava/util/Map;

    .line 1948940
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v0

    iput-object v0, p0, LX/Cve;->o:LX/0vV;

    .line 1948941
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Cve;->p:Ljava/util/Map;

    .line 1948942
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Cve;->q:Ljava/util/Set;

    .line 1948943
    iput-object p1, p0, LX/Cve;->h:LX/11i;

    .line 1948944
    iput-object p2, p0, LX/Cve;->j:LX/0So;

    .line 1948945
    iput-object p3, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1948946
    iput-object p4, p0, LX/Cve;->k:LX/0oz;

    .line 1948947
    iput-object p5, p0, LX/Cve;->l:LX/0ad;

    .line 1948948
    iput-object p6, p0, LX/Cve;->m:LX/0SG;

    .line 1948949
    iput-object p7, p0, LX/Cve;->n:LX/0Uh;

    .line 1948950
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1948951
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1948952
    :cond_0
    :goto_0
    return v0

    .line 1948953
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1948954
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1948955
    invoke-static {v1}, Ljava/lang/Character;->getNumericValue(C)I

    move-result v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;J)LX/0P1;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
            ">;",
            "Ljava/lang/String;",
            "J)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1948956
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    .line 1948957
    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1948958
    const-string v2, "response_freshness"

    .line 1948959
    iget-object v1, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 1948960
    if-eqz v1, :cond_0

    .line 1948961
    iget-object v1, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 1948962
    invoke-virtual {v1}, LX/0ta;->name()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1948963
    const-string v1, "response_age_in_ms"

    invoke-virtual {p0}, Lcom/facebook/fbservice/results/BaseResult;->getClientTimeMs()J

    move-result-wide v2

    sub-long v2, p2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1948964
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1948965
    if-eqz v1, :cond_1

    .line 1948966
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1948967
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v1

    .line 1948968
    :goto_1
    if-nez v1, :cond_2

    .line 1948969
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1948970
    :goto_2
    return-object v0

    .line 1948971
    :cond_0
    const-string v1, ""

    goto :goto_0

    .line 1948972
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 1948973
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->a()LX/0Px;

    move-result-object v7

    .line 1948974
    if-eqz v7, :cond_3

    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1948975
    :cond_3
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_2

    .line 1948976
    :cond_4
    const/4 v2, 0x0

    .line 1948977
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v1, 0x0

    move v6, v1

    move v1, v2

    :goto_3
    if-ge v6, v8, :cond_c

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    .line 1948978
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->v()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v9

    .line 1948979
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->u()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v2

    .line 1948980
    if-nez v9, :cond_5

    if-eqz v2, :cond_6

    .line 1948981
    :cond_5
    if-eqz v2, :cond_7

    .line 1948982
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->k()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->p()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, LX/Cve;->a(LX/0P2;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/util/List;ILcom/facebook/graphql/enums/GraphQLObjectType;)V

    .line 1948983
    :goto_4
    add-int/lit8 v1, v1, 0x1

    .line 1948984
    :cond_6
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_3

    .line 1948985
    :cond_7
    const/4 v2, 0x0

    .line 1948986
    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ax()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v5

    .line 1948987
    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bp()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bp()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 1948988
    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bp()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 1948989
    :cond_8
    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->X()LX/0Px;

    move-result-object v4

    .line 1948990
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v5, v10, :cond_e

    .line 1948991
    if-eqz v4, :cond_9

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1948992
    :cond_9
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->p()LX/0Px;

    move-result-object v4

    .line 1948993
    :cond_a
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->k()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v5

    if-eqz v5, :cond_b

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->k()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    .line 1948994
    :goto_5
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v3, v5, :cond_d

    .line 1948995
    const/4 v2, 0x1

    move-object v11, v4

    move v4, v2

    move-object v2, v3

    move-object v3, v11

    .line 1948996
    :goto_6
    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/Cve;->a(LX/0P2;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/util/List;ILcom/facebook/graphql/enums/GraphQLObjectType;)V

    goto :goto_4

    .line 1948997
    :cond_b
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    goto :goto_5

    .line 1948998
    :cond_c
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto/16 :goto_2

    :cond_d
    move-object v11, v4

    move v4, v2

    move-object v2, v3

    move-object v3, v11

    goto :goto_6

    :cond_e
    move-object v3, v4

    move v4, v2

    move-object v2, v5

    goto :goto_6
.end method

.method private static a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;Ljava/lang/String;)LX/0P1;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchQuery;",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1948899
    new-instance v8, LX/0P2;

    invoke-direct {v8}, LX/0P2;-><init>()V

    .line 1948900
    const-string v0, "id"

    invoke-virtual {v8, v0, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1948901
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;

    move-result-object v0

    .line 1948902
    if-nez v0, :cond_0

    .line 1948903
    invoke-virtual {v8}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1948904
    :goto_0
    return-object v0

    .line 1948905
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->d()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    move-result-object v0

    .line 1948906
    if-nez v0, :cond_1

    .line 1948907
    invoke-virtual {v8}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0

    .line 1948908
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->c()LX/0Px;

    move-result-object v10

    .line 1948909
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v9, v6

    move v7, v6

    :goto_1
    if-ge v9, v11, :cond_7

    invoke-virtual {v10, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;

    .line 1948910
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->e()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v1

    .line 1948911
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->fJ_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;

    move-result-object v4

    .line 1948912
    if-nez v1, :cond_2

    if-eqz v4, :cond_3

    .line 1948913
    :cond_2
    if-nez v4, :cond_4

    .line 1948914
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->d()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->c()LX/0Px;

    move-result-object v3

    move v4, v6

    move v1, v7

    move-object v0, v8

    .line 1948915
    :goto_2
    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, LX/Cve;->a(LX/0P2;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/util/List;ILcom/facebook/graphql/enums/GraphQLObjectType;)V

    .line 1948916
    add-int/lit8 v7, v7, 0x1

    .line 1948917
    :cond_3
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    .line 1948918
    :cond_4
    invoke-virtual {v4}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_5
    move v4, v6

    move v1, v7

    move-object v0, v8

    goto :goto_2

    :cond_6
    invoke-virtual {v4}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v1, v7

    move-object v0, v8

    goto :goto_2

    .line 1948919
    :cond_7
    invoke-virtual {v8}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Z)LX/0Pq;
    .locals 1

    .prologue
    .line 1948999
    if-eqz p0, :cond_0

    sget-object v0, LX/Cve;->a:LX/Cvd;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Cve;->b:LX/Cvc;

    goto :goto_0
.end method

.method private static declared-synchronized a(LX/Cve;LX/0Pq;Ljava/lang/String;)LX/11o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            "Ljava/lang/String;",
            ")",
            "LX/11o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1949000
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    invoke-interface {v0, p1, p2}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1949001
    if-eqz v0, :cond_0

    .line 1949002
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1949003
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Cve;->o:LX/0vV;

    invoke-virtual {v0, p1, p2}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1949004
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    const/4 v3, 0x0

    iget-object v1, p0, LX/Cve;->j:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    move-object v1, p1

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1949005
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/0QB;)LX/Cve;
    .locals 11

    .prologue
    .line 1949006
    sget-object v0, LX/Cve;->s:LX/Cve;

    if-nez v0, :cond_1

    .line 1949007
    const-class v1, LX/Cve;

    monitor-enter v1

    .line 1949008
    :try_start_0
    sget-object v0, LX/Cve;->s:LX/Cve;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1949009
    if-eqz v2, :cond_0

    .line 1949010
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1949011
    new-instance v3, LX/Cve;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v4

    check-cast v4, LX/11i;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v6

    check-cast v6, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v7

    check-cast v7, LX/0oz;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v3 .. v10}, LX/Cve;-><init>(LX/11i;LX/0So;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0oz;LX/0ad;LX/0SG;LX/0Uh;)V

    .line 1949012
    move-object v0, v3

    .line 1949013
    sput-object v0, LX/Cve;->s:LX/Cve;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1949014
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1949015
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1949016
    :cond_1
    sget-object v0, LX/Cve;->s:LX/Cve;

    return-object v0

    .line 1949017
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1949018
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1949019
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/0P2;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/util/List;ILcom/facebook/graphql/enums/GraphQLObjectType;)V
    .locals 5
    .param p5    # Lcom/facebook/graphql/enums/GraphQLObjectType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;I",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1949100
    const-string v0, "_result"

    invoke-static {p1, v0}, LX/Cve;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1949101
    const-string v0, "_role"

    invoke-static {p1, v0}, LX/Cve;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1949102
    if-eqz p5, :cond_0

    .line 1949103
    const-string v0, "_type"

    invoke-static {p1, v0}, LX/Cve;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1949104
    :cond_0
    if-eqz p3, :cond_3

    .line 1949105
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1949106
    const/4 v0, 0x1

    .line 1949107
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1949108
    if-eqz v1, :cond_1

    .line 1949109
    const/4 v1, 0x0

    .line 1949110
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1949111
    :cond_1
    const/16 v4, 0x2c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1949112
    :cond_2
    const-string v0, "_display"

    invoke-static {p1, v0}, LX/Cve;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1949113
    :cond_3
    return-void
.end method

.method private static declared-synchronized a(LX/Cve;LX/0Pq;Ljava/lang/String;LX/0P1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1949020
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    iget-object v1, p0, LX/Cve;->j:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v5}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    .line 1949021
    iget-object v0, p0, LX/Cve;->o:LX/0vV;

    invoke-virtual {v0, p1, p2}, LX/0Xt;->c(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1949022
    monitor-exit p0

    return-void

    .line 1949023
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/Cve;LX/11o;LX/CwB;LX/0P1;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11o",
            "<*>;",
            "LX/CwB;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1949024
    monitor-enter p0

    if-nez p5, :cond_4

    .line 1949025
    :try_start_0
    sget-object v2, LX/0Rg;->a:LX/0Rg;

    move-object v2, v2

    .line 1949026
    :goto_0
    iget-object v3, p0, LX/Cve;->p:Ljava/util/Map;

    invoke-interface {p2}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1949027
    if-eqz p3, :cond_0

    invoke-virtual {p3}, LX/0P1;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    const-string v4, "_result"

    invoke-static {v3, v4}, LX/Cve;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1949028
    :cond_0
    iget-object v3, p0, LX/Cve;->n:LX/0Uh;

    sget v4, LX/2SU;->x:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 1949029
    :cond_1
    if-eqz v0, :cond_2

    iget-object v3, p0, LX/Cve;->q:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-ne v3, v1, :cond_5

    .line 1949030
    :cond_2
    const-string v1, "network"

    const v3, -0x190b6fd7

    invoke-static {p1, v1, p4, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p1

    .line 1949031
    :goto_1
    if-eqz v0, :cond_6

    .line 1949032
    :cond_3
    :goto_2
    monitor-exit p0

    return-void

    .line 1949033
    :cond_4
    :try_start_1
    const-string v2, "session_id"

    invoke-static {v2, p5}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    goto :goto_0

    .line 1949034
    :cond_5
    const-string v1, "network"

    const v2, -0x1e0160e4

    invoke-static {p1, v1, p4, v2}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1949035
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1949036
    :cond_6
    :try_start_2
    const-string v0, "post_fetch"

    const v1, -0x15f92b57

    invoke-static {p1, v0, p4, p3, v1}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1949037
    if-eqz p6, :cond_3

    .line 1949038
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7002c

    invoke-static {p4}, LX/Cve;->a(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1949039
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7002b

    invoke-static {p4}, LX/Cve;->a(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private static declared-synchronized a(LX/Cve;LX/CwB;LX/0P1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1949040
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/Cve;->c:LX/Cvb;

    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/Cve;->a(LX/Cve;LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 1949041
    const-string v1, "tti"

    const/4 v2, 0x0

    const v3, -0x7ec40502

    invoke-static {v0, v1, v2, p2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1949042
    monitor-exit p0

    return-void

    .line 1949043
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0P2;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/contract/SearchResultsContext;",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1949044
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v1

    .line 1949045
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    sget-object v2, LX/Cve;->a:LX/Cvd;

    invoke-interface {v0, v2, v1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v2

    .line 1949046
    const-string v0, "serp_sid"

    .line 1949047
    iget-object v3, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1949048
    invoke-virtual {p2, v0, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v3, "current_connection_quality"

    invoke-static {p0}, LX/Cve;->g(LX/Cve;)LX/0p3;

    move-result-object v4

    invoke-virtual {v4}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v4, "endpoint_version"

    iget-object v0, p0, LX/Cve;->r:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_0
    invoke-virtual {v3, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v3, "KW_ONLY_TA_ENABLED"

    iget-object v4, p0, LX/Cve;->l:LX/0ad;

    sget-short v5, LX/100;->bN:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1949049
    if-eqz v2, :cond_0

    .line 1949050
    sget-object v2, LX/Cve;->a:LX/Cvd;

    invoke-static {p0, v2, v1, v0}, LX/Cve;->a(LX/Cve;LX/0Pq;Ljava/lang/String;LX/0P1;)V

    .line 1949051
    :cond_0
    iget-object v2, p0, LX/Cve;->h:LX/11i;

    sget-object v3, LX/Cve;->b:LX/Cvc;

    invoke-interface {v2, v3, v1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v2

    .line 1949052
    if-eqz v2, :cond_1

    .line 1949053
    sget-object v2, LX/Cve;->b:LX/Cvc;

    invoke-static {p0, v2, v1, v0}, LX/Cve;->a(LX/Cve;LX/0Pq;Ljava/lang/String;LX/0P1;)V

    .line 1949054
    :cond_1
    invoke-direct {p0}, LX/Cve;->d()V

    .line 1949055
    invoke-static {p0}, LX/Cve;->f(LX/Cve;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1949056
    monitor-exit p0

    return-void

    .line 1949057
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/Cve;->r:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1949058
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;ZZ)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1949064
    monitor-enter p0

    :try_start_0
    invoke-static {p3}, LX/Cve;->a(Z)LX/0Pq;

    move-result-object v2

    .line 1949065
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v3

    .line 1949066
    iget-object v4, p0, LX/Cve;->h:LX/11i;

    invoke-interface {v4, v2, v3}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 1949067
    if-nez v4, :cond_0

    .line 1949068
    :goto_0
    monitor-exit p0

    return v0

    .line 1949069
    :cond_0
    :try_start_1
    iget-object v5, p0, LX/Cve;->q:Ljava/util/Set;

    invoke-interface {v5, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1949070
    const-string v5, "post_fetch"

    invoke-interface {v4, v5, p2}, LX/11o;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, LX/Cve;->q:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    move v0, v1

    .line 1949071
    goto :goto_0

    .line 1949072
    :cond_1
    const-string v5, "post_fetch"

    const/4 v6, 0x0

    const v7, 0x3819b0b1

    invoke-static {v4, v5, p2, v6, v7}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1949073
    if-eqz p3, :cond_2

    .line 1949074
    iget-object v5, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x7002b

    invoke-static {p2}, LX/Cve;->a(Ljava/lang/String;)I

    move-result v7

    const/4 v8, 0x2

    invoke-interface {v5, v6, v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1949075
    :cond_2
    if-nez p4, :cond_3

    iget-object v5, p0, LX/Cve;->q:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1949076
    :cond_3
    const-string v5, "tti"

    const v6, 0x4a42fb32    # 3194572.5f

    invoke-static {v4, v5, v6}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949077
    if-eqz p3, :cond_4

    .line 1949078
    iget-object v4, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x7002d

    const/4 v6, 0x2

    invoke-interface {v4, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1949079
    :cond_4
    iget-object v4, p0, LX/Cve;->q:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_5

    move v0, v1

    .line 1949080
    goto :goto_0

    .line 1949081
    :cond_5
    :try_start_2
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    iget-object v4, p0, LX/Cve;->e:Ljava/util/Map;

    invoke-virtual {v1, v4}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catch LX/28F; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 1949082
    :goto_1
    :try_start_3
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    const-string v5, "module_size"

    invoke-virtual {v4, v5, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v4, "serp_sid"

    .line 1949083
    iget-object v5, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v5, v5

    .line 1949084
    invoke-virtual {v1, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v4, "current_connection_quality"

    invoke-static {p0}, LX/Cve;->g(LX/Cve;)LX/0p3;

    move-result-object v5

    invoke-virtual {v5}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v4

    const-string v5, "endpoint_version"

    iget-object v1, p0, LX/Cve;->r:Ljava/lang/String;

    if-nez v1, :cond_6

    const-string v1, ""

    :goto_2
    invoke-virtual {v4, v5, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v4, "KW_ONLY_TA_ENABLED"

    iget-object v5, p0, LX/Cve;->l:LX/0ad;

    sget-short v6, LX/100;->bN:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-static {p0, v2, v3, v1}, LX/Cve;->a(LX/Cve;LX/0Pq;Ljava/lang/String;LX/0P1;)V

    .line 1949085
    iget-object v1, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x70025

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1949086
    invoke-static {p0}, LX/Cve;->f(LX/Cve;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 1949087
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1949088
    :catch_0
    move-exception v1

    .line 1949089
    :try_start_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "can\'t serialize the module view size"

    invoke-static {v4, v5, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1949090
    invoke-virtual {v1}, LX/28F;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1949091
    :cond_6
    iget-object v1, p0, LX/Cve;->r:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method private static c(LX/CwB;)LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1949092
    invoke-interface {p0}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object v0

    .line 1949093
    const-string v1, "query_function"

    invoke-interface {p0}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "query_vertical"

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    invoke-static {v1, v2, v3, v0}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1949094
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7002a

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1949095
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7002c

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(I)V

    .line 1949096
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7002b

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(I)V

    .line 1949097
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7002d

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1949098
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x70025

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1949099
    return-void
.end method

.method private static declared-synchronized e(LX/Cve;)V
    .locals 4

    .prologue
    .line 1948920
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cve;->o:LX/0vV;

    sget-object v1, LX/Cve;->a:LX/Cvd;

    invoke-virtual {v0, v1}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1948921
    iget-object v2, p0, LX/Cve;->h:LX/11i;

    sget-object v3, LX/Cve;->a:LX/Cvd;

    invoke-interface {v2, v3, v0}, LX/11i;->a(LX/0Pq;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1948922
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1948923
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Cve;->o:LX/0vV;

    sget-object v1, LX/Cve;->b:LX/Cvc;

    invoke-virtual {v0, v1}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1948924
    iget-object v2, p0, LX/Cve;->h:LX/11i;

    sget-object v3, LX/Cve;->b:LX/Cvc;

    invoke-interface {v2, v3, v0}, LX/11i;->a(LX/0Pq;Ljava/lang/String;)V

    goto :goto_1

    .line 1948925
    :cond_1
    iget-object v0, p0, LX/Cve;->o:LX/0vV;

    sget-object v1, LX/Cve;->c:LX/Cvb;

    invoke-virtual {v0, v1}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1948926
    iget-object v2, p0, LX/Cve;->h:LX/11i;

    sget-object v3, LX/Cve;->c:LX/Cvb;

    invoke-interface {v2, v3, v0}, LX/11i;->a(LX/0Pq;Ljava/lang/String;)V

    goto :goto_2

    .line 1948927
    :cond_2
    invoke-static {p0}, LX/Cve;->f(LX/Cve;)V

    .line 1948928
    iget-object v0, p0, LX/Cve;->o:LX/0vV;

    invoke-virtual {v0}, LX/0Xs;->g()V

    .line 1948929
    iget-object v0, p0, LX/Cve;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1948930
    invoke-direct {p0}, LX/Cve;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1948931
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized f(LX/Cve;)V
    .locals 1

    .prologue
    .line 1949059
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cve;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1949060
    iget-object v0, p0, LX/Cve;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1949061
    iget-object v0, p0, LX/Cve;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1949062
    monitor-exit p0

    return-void

    .line 1949063
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static g(LX/Cve;)LX/0p3;
    .locals 1

    .prologue
    .line 1948771
    iget-object v0, p0, LX/Cve;->k:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->c()LX/0p3;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 5

    .prologue
    .line 1948772
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/Cve;->e(LX/Cve;)V

    .line 1948773
    const/4 v0, 0x1

    invoke-static {v0}, LX/Cve;->a(Z)LX/0Pq;

    move-result-object v0

    const-string v1, "SEE_MORE_SEQUENCE"

    invoke-static {p0, v0, v1}, LX/Cve;->a(LX/Cve;LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 1948774
    const-string v1, "pre_fetch"

    const/4 v2, 0x0

    sget-object v3, LX/Cve;->g:LX/0P1;

    const v4, -0x2d086287

    invoke-static {v0, v1, v2, v3, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948775
    monitor-exit p0

    return-void

    .line 1948776
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CwB;)V
    .locals 6

    .prologue
    .line 1948777
    monitor-enter p0

    :try_start_0
    sget-object v1, LX/Cve;->b:LX/Cvc;

    .line 1948778
    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v2

    .line 1948779
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    invoke-interface {v0, v1, v2}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 1948780
    if-nez v0, :cond_0

    .line 1948781
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Cve;->j:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    move-result-object v0

    .line 1948782
    iget-object v3, p0, LX/Cve;->o:LX/0vV;

    invoke-virtual {v3, v1, v2}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1948783
    const-string v1, "pre_fetch"

    const/4 v2, 0x0

    const-string v3, "version"

    const-string v4, "2"

    invoke-static {v3, v4}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    const v4, -0xa13a6b5

    invoke-static {v0, v1, v2, v3, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948784
    :cond_0
    monitor-exit p0

    return-void

    .line 1948785
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CwB;Ljava/lang/String;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1948786
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/Cve;->e(LX/Cve;)V

    .line 1948787
    const/4 v0, 0x1

    invoke-static {v0}, LX/Cve;->a(Z)LX/0Pq;

    move-result-object v0

    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/Cve;->a(LX/Cve;LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v1

    .line 1948788
    if-nez p2, :cond_0

    const-string v0, "version"

    const-string v2, "2"

    invoke-static {v0, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    .line 1948789
    :goto_0
    const-string v2, "tti"

    const v3, -0x3f0fb4ea

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1948790
    const-string v2, "pre_fetch"

    const/4 v3, 0x0

    const v4, 0x6ae1332a

    invoke-static {v1, v2, v3, v0, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1948791
    invoke-static {p0, p1, v0}, LX/Cve;->a(LX/Cve;LX/CwB;LX/0P1;)V

    .line 1948792
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x70025

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1948793
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7002a

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1948794
    iget-object v0, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7002d

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948795
    monitor-exit p0

    return-void

    .line 1948796
    :cond_0
    :try_start_1
    const-string v0, "version"

    const-string v2, "2"

    const-string v3, "typeahead_sid"

    invoke-static {v0, v2, v3, p2}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1948797
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CwB;Ljava/util/Map;Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/6VT;",
            ">;>;",
            "Ljava/util/Map",
            "<",
            "LX/CvL;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1948798
    monitor-enter p0

    :try_start_0
    sget-object v3, LX/Cve;->c:LX/Cvb;

    .line 1948799
    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v4

    .line 1948800
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    invoke-interface {v0, v3, v4}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1948801
    if-nez v0, :cond_0

    .line 1948802
    :goto_0
    monitor-exit p0

    return-void

    .line 1948803
    :cond_0
    :try_start_1
    new-instance v0, LX/Cvj;

    invoke-direct {v0, p3}, LX/Cvj;-><init>(Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1948804
    :try_start_2
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_2
    .catch LX/28F; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    move-object v2, v0

    .line 1948805
    :goto_1
    :try_start_3
    new-instance v0, LX/Cvi;

    invoke-direct {v0, p2}, LX/Cvi;-><init>(Ljava/util/Map;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1948806
    :try_start_4
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_4
    .catch LX/28F; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    move-object v1, v0

    .line 1948807
    :goto_2
    :try_start_5
    iget-object v0, p0, LX/Cve;->p:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    .line 1948808
    :goto_3
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    const-string v6, "serp_sid"

    check-cast p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1948809
    iget-object v7, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v7, v7

    .line 1948810
    invoke-virtual {v5, v6, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v5

    const-string v6, "session_id"

    invoke-virtual {v5, v6, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v5, "current_connection_quality"

    invoke-static {p0}, LX/Cve;->g(LX/Cve;)LX/0p3;

    move-result-object v6

    invoke-virtual {v6}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v5, "viewport_times"

    invoke-virtual {v0, v5, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v2, "image_load_times"

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "KW_ONLY_TA_ENABLED"

    iget-object v2, p0, LX/Cve;->l:LX/0ad;

    sget-short v5, LX/100;->bN:S

    const/4 v6, 0x0

    invoke-interface {v2, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-static {p0, v3, v4, v0}, LX/Cve;->a(LX/Cve;LX/0Pq;Ljava/lang/String;LX/0P1;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 1948811
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1948812
    :catch_0
    move-exception v0

    .line 1948813
    :try_start_6
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "can\'t serialize viewport times"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1948814
    invoke-virtual {v0}, LX/28F;->getMessage()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    .line 1948815
    :catch_1
    move-exception v0

    .line 1948816
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v5, "can\'t serialize viewport image load times"

    invoke-static {v1, v5, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1948817
    invoke-virtual {v0}, LX/28F;->getMessage()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 1948818
    :cond_1
    iget-object v0, p0, LX/Cve;->p:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3
.end method

.method public final declared-synchronized a(LX/CwB;ZLcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "Z",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1948819
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    invoke-static {p2}, LX/Cve;->a(Z)LX/0Pq;

    move-result-object v1

    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v1

    .line 1948820
    if-eqz v1, :cond_0

    const-string v0, "network"

    invoke-interface {v1, v0, p4}, LX/11o;->c(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 1948821
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1948822
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/Cve;->m:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p3, p4, v2, v3}, LX/Cve;->a(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;J)LX/0P1;

    move-result-object v3

    move-object v0, p0

    move-object v2, p1

    move-object v4, p4

    move-object v5, p5

    move v6, p2

    .line 1948823
    invoke-static/range {v0 .. v6}, LX/Cve;->a(LX/Cve;LX/11o;LX/CwB;LX/0P1;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1948824
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CwB;ZLcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1948825
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    invoke-static {p2}, LX/Cve;->a(Z)LX/0Pq;

    move-result-object v1

    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v1

    .line 1948826
    if-eqz v1, :cond_0

    const-string v0, "network"

    invoke-interface {v1, v0, p4}, LX/11o;->c(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 1948827
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1948828
    :cond_1
    :try_start_1
    invoke-static {p3, p4}, LX/Cve;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;Ljava/lang/String;)LX/0P1;

    move-result-object v3

    move-object v0, p0

    move-object v2, p1

    move-object v4, p4

    move-object v5, p5

    move v6, p2

    .line 1948829
    invoke-static/range {v0 .. v6}, LX/Cve;->a(LX/Cve;LX/11o;LX/CwB;LX/0P1;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1948830
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CwB;ZLjava/lang/Iterable;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "Z",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1948831
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cve;->h:LX/11i;

    invoke-static {p2}, LX/Cve;->a(Z)LX/0Pq;

    move-result-object v1

    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1948832
    if-nez v0, :cond_1

    .line 1948833
    :cond_0
    monitor-exit p0

    return-void

    .line 1948834
    :cond_1
    :try_start_1
    iput-object p4, p0, LX/Cve;->r:Ljava/lang/String;

    .line 1948835
    const-string v1, "pre_fetch"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1948836
    const-string v1, "pre_fetch"

    const/4 v2, 0x0

    invoke-static {p1}, LX/Cve;->c(LX/CwB;)LX/0P1;

    move-result-object v3

    const v4, -0x57b9cea2

    invoke-static {v0, v1, v2, v3, v4}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    move-result-object v0

    .line 1948837
    if-eqz p2, :cond_2

    .line 1948838
    iget-object v1, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x7002a

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    :cond_2
    move-object v1, v0

    .line 1948839
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1948840
    const-string v3, "network"

    const-string v4, "id"

    invoke-static {v4, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v4

    const v5, 0x791904ec

    invoke-static {v1, v3, v0, v4, v5}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1948841
    if-eqz p2, :cond_3

    .line 1948842
    iget-object v3, p0, LX/Cve;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x7002c

    invoke-static {v0}, LX/Cve;->a(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1948843
    iget-object v3, p0, LX/Cve;->q:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1948844
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CwB;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1948845
    monitor-enter p0

    :try_start_0
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p4}, LX/Cve;->a(LX/CwB;ZLjava/lang/Iterable;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948846
    monitor-exit p0

    return-void

    .line 1948847
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CwW;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;ILX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwW;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;I",
            "LX/0Px",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1948848
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cve;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1948849
    if-nez v0, :cond_0

    .line 1948850
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 1948851
    iget-object v1, p0, LX/Cve;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948852
    :cond_0
    new-instance v2, LX/CvT;

    invoke-direct {v2, p1, p3, p2, p4}, LX/CvT;-><init>(LX/CwW;LX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;I)V

    .line 1948853
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1948854
    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1948855
    iget-object v4, v2, LX/CvT;->d:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1948856
    iget-object v4, v2, LX/CvT;->d:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CvS;

    .line 1948857
    :goto_1
    iget-object v4, p0, LX/Cve;->f:Ljava/util/Map;

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948858
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1948859
    :cond_1
    monitor-exit p0

    return-void

    .line 1948860
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1948861
    :cond_2
    new-instance v4, LX/CvS;

    invoke-direct {v4}, LX/CvS;-><init>()V

    .line 1948862
    iget-object p1, v2, LX/CvT;->d:Ljava/util/Map;

    invoke-interface {p1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1948863
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cve;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvT;

    .line 1948864
    if-eqz v0, :cond_0

    .line 1948865
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1948866
    iget-object v2, v0, LX/CvT;->d:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CvS;

    .line 1948867
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 1948868
    iget-object v0, v2, LX/CvS;->a:Ljava/util/Map;

    invoke-interface {v0, p2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948869
    :cond_0
    monitor-exit p0

    return-void

    .line 1948870
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 3

    .prologue
    .line 1948871
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "did_not_finish"

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0P2;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948872
    monitor-exit p0

    return-void

    .line 1948873
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/search/results/model/SearchResultsMutableContext;I)V
    .locals 3

    .prologue
    .line 1948874
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "request_timeout"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0P2;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948875
    monitor-exit p0

    return-void

    .line 1948876
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/util/Map;Z)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/contract/SearchResultsContext;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1948877
    monitor-enter p0

    .line 1948878
    :try_start_0
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1948879
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, p1, v1, v0, p3}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;ZZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    move v3, v0

    .line 1948880
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1948881
    goto :goto_1

    .line 1948882
    :cond_2
    monitor-exit p0

    return v3

    .line 1948883
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 1948884
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/Cve;->e(LX/Cve;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948885
    monitor-exit p0

    return-void

    .line 1948886
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/CwB;)V
    .locals 5

    .prologue
    .line 1948887
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/Cve;->c:LX/Cvb;

    .line 1948888
    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v1

    .line 1948889
    iget-object v2, p0, LX/Cve;->h:LX/11i;

    invoke-interface {v2, v0, v1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 1948890
    if-eqz v0, :cond_0

    .line 1948891
    const-string v1, "tti"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1948892
    const-string v1, "tti"

    const/4 v2, 0x0

    invoke-static {p1}, LX/Cve;->c(LX/CwB;)LX/0P1;

    move-result-object v3

    const v4, 0x3650da61

    invoke-static {v0, v1, v2, v3, v4}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948893
    :cond_0
    monitor-exit p0

    return-void

    .line 1948894
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 1948895
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/Cve;->e(LX/Cve;)V

    .line 1948896
    invoke-static {p0}, LX/Cve;->f(LX/Cve;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1948897
    monitor-exit p0

    return-void

    .line 1948898
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
