.class public LX/DjF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2033048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2033049
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const-wide/16 v6, 0x0

    .line 2033027
    const-string v0, "extra_create_booking_appointment_model"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    .line 2033028
    const-string v2, "referrer"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2033029
    if-nez v0, :cond_0

    .line 2033030
    const-string v0, "booking_request_id"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2033031
    new-instance v0, LX/Djn;

    invoke-direct {v0}, LX/Djn;-><init>()V

    .line 2033032
    iput-boolean v1, v0, LX/Djn;->a:Z

    .line 2033033
    const-string v1, "composer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2033034
    const-string v1, "booking_request_owner_id"

    invoke-virtual {p1, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2033035
    iput-object v1, v0, LX/Djn;->d:Ljava/lang/String;

    .line 2033036
    :goto_1
    invoke-virtual {v0}, LX/Djn;->a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    move-result-object v0

    .line 2033037
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2033038
    const-string v3, "arg_create_booking_appointment_model"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2033039
    const-string v3, "referrer"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033040
    new-instance v3, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-direct {v3}, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;-><init>()V

    .line 2033041
    invoke-virtual {v3, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2033042
    move-object v0, v3

    .line 2033043
    return-object v0

    .line 2033044
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2033045
    :cond_2
    const-string v1, "booking_request_id"

    invoke-virtual {p1, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2033046
    iput-object v1, v0, LX/Djn;->c:Ljava/lang/String;

    .line 2033047
    goto :goto_1
.end method
