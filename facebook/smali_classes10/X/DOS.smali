.class public final LX/DOS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/DOa;


# direct methods
.method public constructor <init>(LX/DOa;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1992323
    iput-object p1, p0, LX/DOS;->c:LX/DOa;

    iput-object p2, p0, LX/DOS;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/DOS;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    .line 1992324
    iget-object v0, p0, LX/DOS;->c:LX/DOa;

    iget-object v0, v0, LX/DOa;->b:LX/DOb;

    iget-object v1, p0, LX/DOS;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/DOS;->b:Landroid/content/Context;

    .line 1992325
    invoke-static {v1}, LX/DOl;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v5

    .line 1992326
    const v3, 0x51d811e3

    invoke-static {v1, v3}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 1992327
    if-eqz v5, :cond_2

    if-eqz v3, :cond_2

    .line 1992328
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "group_side_conversation_displayed"

    invoke-direct {v4, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "group_feed"

    .line 1992329
    iput-object v6, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1992330
    move-object v4, v4

    .line 1992331
    const-string v6, "group_id"

    invoke-virtual {v4, v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1992332
    iget-object v6, v0, LX/DOb;->e:LX/0Zb;

    invoke-interface {v6, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1992333
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1992334
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->P()Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;

    move-result-object v3

    .line 1992335
    if-eqz v3, :cond_1

    .line 1992336
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersConnection;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v8, :cond_1

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersEdge;

    .line 1992337
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 1992338
    new-instance v9, LX/0XI;

    invoke-direct {v9}, LX/0XI;-><init>()V

    sget-object p0, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v9, p0, p1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v9

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupMessageChattableMembersEdge;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    .line 1992339
    iput-object v3, v9, LX/0XI;->h:Ljava/lang/String;

    .line 1992340
    move-object v3, v9

    .line 1992341
    invoke-virtual {v3}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1992342
    :cond_0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1992343
    :cond_1
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    iget-object v3, v0, LX/DOb;->l:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v4

    .line 1992344
    const-string v3, "group_feed_id"

    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1992345
    const-string v3, "target_fragment"

    sget-object v5, LX/0cQ;->GROUP_CREATE_SIDE_CONVERSATION_FRAGMENT:LX/0cQ;

    invoke-virtual {v5}, LX/0cQ;->ordinal()I

    move-result v5

    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1992346
    const-string v3, "PRE_SELECT_MEMBERS"

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1992347
    iget-object v3, v0, LX/DOb;->o:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v4, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1992348
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 1992349
    :cond_2
    if-nez v5, :cond_3

    .line 1992350
    iget-object v3, v0, LX/DOb;->m:LX/03V;

    sget-object v4, LX/DOb;->b:Ljava/lang/String;

    const-string v5, "Cannot start side conversation from group story. It is missing group id."

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1992351
    :cond_3
    iget-object v3, v0, LX/DOb;->m:LX/03V;

    sget-object v4, LX/DOb;->b:Ljava/lang/String;

    const-string v5, "Cannot start side conversation from group story. It is missing action link."

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
