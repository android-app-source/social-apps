.class public final LX/DKZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F4e;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic d:Z

.field public final synthetic e:LX/DKa;


# direct methods
.method public constructor <init>(LX/DKa;LX/F4e;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;Z)V
    .locals 0

    .prologue
    .line 1987226
    iput-object p1, p0, LX/DKZ;->e:LX/DKa;

    iput-object p2, p0, LX/DKZ;->a:LX/F4e;

    iput-object p3, p0, LX/DKZ;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DKZ;->c:Lcom/google/common/util/concurrent/SettableFuture;

    iput-boolean p5, p0, LX/DKZ;->d:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1987227
    iget-object v0, p0, LX/DKZ;->c:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/DKZ;->b:Ljava/lang/String;

    const v2, -0x5b536400

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1987228
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 1987229
    check-cast p1, Ljava/lang/String;

    .line 1987230
    iget-object v0, p0, LX/DKZ;->a:LX/F4e;

    if-eqz v0, :cond_0

    .line 1987231
    iget-object v0, p0, LX/DKZ;->e:LX/DKa;

    sget-object v1, LX/DKF;->SETTING_AS_COVER:LX/DKF;

    iget-object v2, p0, LX/DKZ;->a:LX/F4e;

    invoke-static {v0, v1, v2}, LX/DKa;->a$redex0(LX/DKa;LX/DKF;LX/F4e;)V

    .line 1987232
    :cond_0
    iget-object v0, p0, LX/DKZ;->e:LX/DKa;

    iget-object v2, p0, LX/DKZ;->b:Ljava/lang/String;

    iget-object v3, p0, LX/DKZ;->c:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v4, p0, LX/DKZ;->a:LX/F4e;

    iget-boolean v5, p0, LX/DKZ;->d:Z

    move-object v1, p1

    .line 1987233
    new-instance v6, LX/DKe;

    invoke-direct {v6}, LX/DKe;-><init>()V

    .line 1987234
    iput-object v2, v6, LX/DKe;->a:Ljava/lang/String;

    .line 1987235
    move-object v6, v6

    .line 1987236
    iput-object v1, v6, LX/DKe;->b:Ljava/lang/String;

    .line 1987237
    move-object v6, v6

    .line 1987238
    invoke-virtual {v6}, LX/DKe;->a()Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;

    move-result-object v8

    .line 1987239
    iget-object v13, v0, LX/DKa;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;

    move-object v7, v0

    move v9, v5

    move-object v10, v4

    move-object v11, v3

    move-object v12, v2

    invoke-direct/range {v6 .. v12}, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;-><init>(LX/DKa;Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;ZLX/F4e;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;)V

    const v7, -0x5a5d1e68

    invoke-static {v13, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1987240
    return-void
.end method
