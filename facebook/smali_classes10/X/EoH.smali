.class public LX/EoH;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;"
    }
.end annotation


# instance fields
.field public a:LX/EoF;

.field public final b:Lcom/facebook/widget/CustomFrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2168085
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2168086
    const v0, 0x7f030495

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2168087
    invoke-virtual {p0}, LX/EoH;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b220f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2168088
    invoke-virtual {p0, v0, v2, v0, v2}, LX/EoH;->setPadding(IIII)V

    .line 2168089
    const v0, 0x7f0d0d8f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v0, p0, LX/EoH;->b:Lcom/facebook/widget/CustomFrameLayout;

    .line 2168090
    return-void
.end method


# virtual methods
.method public getCardViews()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2168080
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2168081
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/EoH;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomFrameLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2168082
    iget-object v2, p0, LX/EoH;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/CustomFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2168083
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2168084
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5f96195f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2168075
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2168076
    iget-object v1, p0, LX/EoH;->a:LX/EoF;

    if-eqz v1, :cond_0

    .line 2168077
    iget-object v1, p0, LX/EoH;->a:LX/EoF;

    invoke-virtual {v1, p0}, LX/EoF;->a(LX/EoH;)V

    .line 2168078
    const/4 v1, 0x0

    iput-object v1, p0, LX/EoH;->a:LX/EoF;

    .line 2168079
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x773b93e2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public bridge synthetic setPresenter(LX/Emf;)V
    .locals 0

    .prologue
    .line 2168072
    check-cast p1, LX/EoF;

    .line 2168073
    iput-object p1, p0, LX/EoH;->a:LX/EoF;

    .line 2168074
    return-void
.end method
