.class public final enum LX/Dop;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dop;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dop;

.field public static final enum COMPLETED:LX/Dop;

.field public static final enum FAILED:LX/Dop;

.field public static final enum LOOKING_UP:LX/Dop;

.field public static final enum NOT_STARTED:LX/Dop;


# instance fields
.field private mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2042534
    new-instance v0, LX/Dop;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2, v2}, LX/Dop;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dop;->NOT_STARTED:LX/Dop;

    .line 2042535
    new-instance v0, LX/Dop;

    const-string v1, "LOOKING_UP"

    invoke-direct {v0, v1, v3, v3}, LX/Dop;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dop;->LOOKING_UP:LX/Dop;

    .line 2042536
    new-instance v0, LX/Dop;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v4, v4}, LX/Dop;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dop;->COMPLETED:LX/Dop;

    .line 2042537
    new-instance v0, LX/Dop;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5, v5}, LX/Dop;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Dop;->FAILED:LX/Dop;

    .line 2042538
    const/4 v0, 0x4

    new-array v0, v0, [LX/Dop;

    sget-object v1, LX/Dop;->NOT_STARTED:LX/Dop;

    aput-object v1, v0, v2

    sget-object v1, LX/Dop;->LOOKING_UP:LX/Dop;

    aput-object v1, v0, v3

    sget-object v1, LX/Dop;->COMPLETED:LX/Dop;

    aput-object v1, v0, v4

    sget-object v1, LX/Dop;->FAILED:LX/Dop;

    aput-object v1, v0, v5

    sput-object v0, LX/Dop;->$VALUES:[LX/Dop;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2042539
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2042540
    iput p3, p0, LX/Dop;->mValue:I

    .line 2042541
    return-void
.end method

.method public static from(I)LX/Dop;
    .locals 5

    .prologue
    .line 2042542
    invoke-static {}, LX/Dop;->values()[LX/Dop;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2042543
    invoke-virtual {v0}, LX/Dop;->getValue()I

    move-result v4

    if-ne p0, v4, :cond_0

    .line 2042544
    :goto_1
    return-object v0

    .line 2042545
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2042546
    :cond_1
    sget-object v0, LX/Dop;->NOT_STARTED:LX/Dop;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dop;
    .locals 1

    .prologue
    .line 2042547
    const-class v0, LX/Dop;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dop;

    return-object v0
.end method

.method public static values()[LX/Dop;
    .locals 1

    .prologue
    .line 2042548
    sget-object v0, LX/Dop;->$VALUES:[LX/Dop;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dop;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 2042549
    iget v0, p0, LX/Dop;->mValue:I

    return v0
.end method
