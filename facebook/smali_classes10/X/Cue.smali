.class public abstract LX/Cue;
.super LX/CsC;
.source ""


# instance fields
.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1947001
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Cue;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1947002
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1947003
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Cue;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947004
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1947005
    invoke-direct {p0, p1, p2, p3}, LX/CsC;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947006
    return-void
.end method


# virtual methods
.method public setLoading(Z)V
    .locals 1

    .prologue
    .line 1947007
    if-eqz p1, :cond_0

    .line 1947008
    invoke-virtual {p0}, LX/CsC;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/Cue;->e:Z

    .line 1947009
    :goto_0
    return-void

    .line 1947010
    :cond_0
    invoke-virtual {p0}, LX/CsC;->e()V

    .line 1947011
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cue;->e:Z

    goto :goto_0
.end method
