.class public LX/D9q;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/0f6;


# static fields
.field private static final g:LX/0wT;

.field private static final h:LX/0wT;


# instance fields
.field private A:Z

.field private B:LX/D9m;

.field private C:LX/D9p;

.field private final D:LX/D9l;

.field public a:LX/0wc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0wW;
    .annotation runtime Lcom/facebook/messaging/chatheads/annotations/ForChatHeads;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/chatheads/annotations/IsChatHeadsHardwareAccelerationDisabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0pu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/D9i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/D9J;",
            "LX/D9j;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/D9h;

.field private k:Landroid/view/ViewGroup;

.field private l:LX/3Nk;

.field private m:LX/3Nk;

.field private n:Landroid/widget/ImageView;

.field public o:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/0wd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:I

.field private r:LX/D9K;

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:LX/D9k;

.field private x:LX/D9J;

.field public y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1970551
    const-wide v0, 0x4062c00000000000L    # 150.0

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/D9q;->g:LX/0wT;

    .line 1970552
    const-wide v0, 0x4050400000000000L    # 65.0

    const-wide/high16 v2, 0x4021000000000000L    # 8.5

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/D9q;->h:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1970553
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D9q;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1970554
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1970555
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D9q;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1970556
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1970557
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1970558
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/D9q;->i:Ljava/util/Map;

    .line 1970559
    sget-object v0, LX/D9J;->UNSET:LX/D9J;

    iput-object v0, p0, LX/D9q;->x:LX/D9J;

    .line 1970560
    new-instance v0, LX/D9l;

    invoke-direct {v0, p0}, LX/D9l;-><init>(LX/D9q;)V

    iput-object v0, p0, LX/D9q;->D:LX/D9l;

    .line 1970561
    const-class v0, LX/D9q;

    invoke-static {v0, p0}, LX/D9q;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1970562
    iget-object v0, p0, LX/D9q;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/D9q;->A:Z

    .line 1970563
    invoke-direct {p0}, LX/D9q;->l()V

    .line 1970564
    iput-boolean v1, p0, LX/D9q;->y:Z

    .line 1970565
    new-instance v0, LX/D9m;

    invoke-direct {v0, p0}, LX/D9m;-><init>(LX/D9q;)V

    iput-object v0, p0, LX/D9q;->B:LX/D9m;

    .line 1970566
    return-void

    :cond_0
    move v0, v1

    .line 1970567
    goto :goto_0
.end method

.method private a(LX/D9J;)LX/D9j;
    .locals 1

    .prologue
    .line 1970568
    iget-object v0, p0, LX/D9q;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D9j;

    return-object v0
.end method

.method private a(I)Landroid/graphics/PointF;
    .locals 4

    .prologue
    .line 1970569
    invoke-virtual {p0}, LX/D9q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b016a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 1970570
    iget-object v1, p0, LX/D9q;->r:LX/D9K;

    invoke-interface {v1}, LX/D9K;->a()Landroid/graphics/PointF;

    move-result-object v1

    .line 1970571
    iget v2, v1, Landroid/graphics/PointF;->x:F

    int-to-float v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 1970572
    iget v2, v1, Landroid/graphics/PointF;->y:F

    int-to-float v0, v0

    add-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 1970573
    return-object v1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1970574
    invoke-direct {p0}, LX/D9q;->i()V

    .line 1970575
    invoke-virtual {p0}, LX/D9q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1970576
    const v1, 0x7f0b018b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/D9q;->u:I

    .line 1970577
    const v1, 0x7f0b018c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/D9q;->v:I

    .line 1970578
    const v1, 0x7f0b0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/D9q;->s:I

    .line 1970579
    const v1, 0x7f0b0187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9q;->t:I

    .line 1970580
    return-void
.end method

.method private a(FF)V
    .locals 4

    .prologue
    .line 1970581
    iget v0, p0, LX/D9q;->s:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    mul-float/2addr v0, p1

    .line 1970582
    iget v1, p0, LX/D9q;->t:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    mul-float/2addr v1, p2

    .line 1970583
    iget-object v2, p0, LX/D9q;->m:LX/3Nk;

    iget v3, p0, LX/D9q;->s:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v0, v3

    .line 1970584
    iget-object v3, v2, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 1970585
    iget-object v0, p0, LX/D9q;->m:LX/3Nk;

    iget v2, p0, LX/D9q;->t:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 1970586
    iget-object v2, v0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1970587
    iget-object v0, p0, LX/D9q;->l:LX/3Nk;

    iget v1, p0, LX/D9q;->u:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    .line 1970588
    iget-object v2, v0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setPivotX(F)V

    .line 1970589
    iget-object v0, p0, LX/D9q;->l:LX/3Nk;

    iget v1, p0, LX/D9q;->v:I

    int-to-float v1, v1

    add-float/2addr v1, p2

    .line 1970590
    iget-object v2, v0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setPivotY(F)V

    .line 1970591
    return-void
.end method

.method private static a(LX/D9q;LX/0wc;LX/0Uy;LX/0wW;LX/0Or;LX/0pu;LX/D9i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/D9q;",
            "LX/0wc;",
            "LX/0Uy;",
            "LX/0wW;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0pu;",
            "LX/D9i;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1970592
    iput-object p1, p0, LX/D9q;->a:LX/0wc;

    iput-object p2, p0, LX/D9q;->b:LX/0Uy;

    iput-object p3, p0, LX/D9q;->c:LX/0wW;

    iput-object p4, p0, LX/D9q;->d:LX/0Or;

    iput-object p5, p0, LX/D9q;->e:LX/0pu;

    iput-object p6, p0, LX/D9q;->f:LX/D9i;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/D9q;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, LX/D9q;

    invoke-static {v6}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object v1

    check-cast v1, LX/0wc;

    invoke-static {v6}, LX/43y;->a(LX/0QB;)LX/0Uy;

    move-result-object v2

    check-cast v2, LX/0Uy;

    invoke-static {v6}, LX/D9f;->a(LX/0QB;)LX/0wW;

    move-result-object v3

    check-cast v3, LX/0wW;

    const/16 v4, 0x1505

    invoke-static {v6, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v6}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v5

    check-cast v5, LX/0pu;

    const-class v7, LX/D9i;

    invoke-interface {v6, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/D9i;

    invoke-static/range {v0 .. v6}, LX/D9q;->a(LX/D9q;LX/0wc;LX/0Uy;LX/0wW;LX/0Or;LX/0pu;LX/D9i;)V

    return-void
.end method

.method public static b$redex0(LX/D9q;)V
    .locals 6

    .prologue
    .line 1970621
    invoke-virtual {p0}, LX/D9q;->getCurrentContent()LX/D9j;

    .line 1970622
    iget-object v0, p0, LX/D9q;->b:LX/0Uy;

    .line 1970623
    iget-object v1, v0, LX/0Uy;->a:LX/0Uo;

    invoke-static {v1}, LX/0Uo;->M(LX/0Uo;)V

    .line 1970624
    iget-object v1, v0, LX/0Uy;->a:LX/0Uo;

    const/4 v2, 0x1

    .line 1970625
    iput-boolean v2, v1, LX/0Uo;->T:Z

    .line 1970626
    iget-object v2, v0, LX/0Uy;->a:LX/0Uo;

    iget-object v1, v0, LX/0Uy;->a:LX/0Uo;

    iget-object v1, v1, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v3

    .line 1970627
    iput-wide v3, v2, LX/0Uo;->O:J

    .line 1970628
    iget-object v1, v0, LX/0Uy;->a:LX/0Uo;

    invoke-static {v1}, LX/0Uo;->w$redex0(LX/0Uo;)V

    .line 1970629
    return-void
.end method

.method public static e$redex0(LX/D9q;)V
    .locals 6

    .prologue
    .line 1970605
    invoke-virtual {p0}, LX/D9q;->getCurrentContent()LX/D9j;

    .line 1970606
    iget-object v0, p0, LX/D9q;->b:LX/0Uy;

    .line 1970607
    iget-object v1, v0, LX/0Uy;->a:LX/0Uo;

    invoke-static {v1}, LX/0Uo;->N(LX/0Uo;)V

    .line 1970608
    iget-object v1, v0, LX/0Uy;->a:LX/0Uo;

    const/4 v2, 0x0

    .line 1970609
    iput-boolean v2, v1, LX/0Uo;->T:Z

    .line 1970610
    iget-object v2, v0, LX/0Uy;->a:LX/0Uo;

    iget-object v1, v0, LX/0Uy;->a:LX/0Uo;

    iget-object v1, v1, LX/0Uo;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v3

    .line 1970611
    iput-wide v3, v2, LX/0Uo;->P:J

    .line 1970612
    iget-object v1, v0, LX/0Uy;->a:LX/0Uo;

    invoke-static {v1}, LX/0Uo;->w$redex0(LX/0Uo;)V

    .line 1970613
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1970614
    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    if-eqz v0, :cond_0

    .line 1970615
    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->a()V

    .line 1970616
    iput-object v2, p0, LX/D9q;->p:LX/0wd;

    .line 1970617
    :cond_0
    iget-object v0, p0, LX/D9q;->o:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_1

    .line 1970618
    iget-object v0, p0, LX/D9q;->o:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0SQ;->cancel(Z)Z

    .line 1970619
    iput-object v2, p0, LX/D9q;->o:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1970620
    :cond_1
    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    const-wide v2, 0x3f747ae140000000L    # 0.004999999888241291

    .line 1970597
    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    if-nez v0, :cond_0

    .line 1970598
    iget-object v0, p0, LX/D9q;->c:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/D9q;->g:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1970599
    iput-wide v2, v0, LX/0wd;->k:D

    .line 1970600
    move-object v0, v0

    .line 1970601
    iput-wide v2, v0, LX/0wd;->l:D

    .line 1970602
    move-object v0, v0

    .line 1970603
    new-instance v1, LX/D9o;

    invoke-direct {v1, p0}, LX/D9o;-><init>(LX/D9q;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/D9q;->p:LX/0wd;

    .line 1970604
    :cond_0
    return-void
.end method

.method private getBubbleContentElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/D9j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1970596
    iget-object v0, p0, LX/D9q;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private getNubTargetX()F
    .locals 2

    .prologue
    .line 1970593
    iget-object v0, p0, LX/D9q;->l:LX/3Nk;

    .line 1970594
    iget-object v1, v0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPivotX()F

    move-result v1

    move v0, v1

    .line 1970595
    iget v1, p0, LX/D9q;->u:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    return v0
.end method

.method private getNubTargetY()F
    .locals 2

    .prologue
    .line 1970533
    iget-object v0, p0, LX/D9q;->l:LX/3Nk;

    .line 1970534
    iget-object v1, v0, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPivotY()F

    move-result v1

    move v0, v1

    .line 1970535
    iget v1, p0, LX/D9q;->v:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    return v0
.end method

.method private h()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1970536
    new-instance v0, LX/3Nk;

    invoke-direct {v0, p0}, LX/3Nk;-><init>(Landroid/view/View;)V

    iput-object v0, p0, LX/D9q;->l:LX/3Nk;

    .line 1970537
    invoke-virtual {p0}, LX/D9q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1970538
    const v1, 0x7f0b018b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/D9q;->u:I

    .line 1970539
    const v1, 0x7f0b018c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9q;->v:I

    .line 1970540
    iget-object v0, p0, LX/D9q;->f:LX/D9i;

    iget-object v1, p0, LX/D9q;->l:LX/3Nk;

    .line 1970541
    new-instance v4, LX/D9h;

    invoke-static {v0}, LX/D9f;->a(LX/0QB;)LX/0wW;

    move-result-object v3

    check-cast v3, LX/0wW;

    invoke-direct {v4, v1, v3}, LX/D9h;-><init>(LX/3Nk;LX/0wW;)V

    .line 1970542
    move-object v0, v4

    .line 1970543
    iput-object v0, p0, LX/D9q;->j:LX/D9h;

    .line 1970544
    iget-boolean v0, p0, LX/D9q;->A:Z

    if-eqz v0, :cond_0

    .line 1970545
    invoke-virtual {p0, v2}, LX/D9q;->setScaleX(F)V

    .line 1970546
    invoke-virtual {p0, v2}, LX/D9q;->setScaleY(F)V

    .line 1970547
    invoke-virtual {p0, v2}, LX/D9q;->setAlpha(F)V

    .line 1970548
    iget-object v0, p0, LX/D9q;->j:LX/D9h;

    new-instance v1, LX/D9n;

    invoke-direct {v1, p0}, LX/D9n;-><init>(LX/D9q;)V

    .line 1970549
    iput-object v1, v0, LX/D9h;->g:LX/D9n;

    .line 1970550
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1970498
    invoke-virtual {p0}, LX/D9q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1970499
    const v1, 0x7f0b0186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/D9q;->s:I

    .line 1970500
    const v1, 0x7f0b0187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9q;->t:I

    .line 1970501
    const v0, 0x7f0d1eed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/D9q;->n:Landroid/widget/ImageView;

    .line 1970502
    new-instance v0, LX/3Nk;

    iget-object v1, p0, LX/D9q;->n:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, LX/3Nk;-><init>(Landroid/view/View;)V

    iput-object v0, p0, LX/D9q;->m:LX/3Nk;

    .line 1970503
    return-void
.end method

.method public static j(LX/D9q;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1970490
    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v0, v2

    .line 1970491
    :goto_0
    iget-boolean v2, p0, LX/D9q;->A:Z

    if-eqz v2, :cond_1

    .line 1970492
    invoke-virtual {p0, v0}, LX/D9q;->setScaleX(F)V

    .line 1970493
    invoke-virtual {p0, v0}, LX/D9q;->setScaleY(F)V

    .line 1970494
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p0, v0}, LX/D9q;->setAlpha(F)V

    .line 1970495
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 1970496
    goto :goto_0

    .line 1970497
    :cond_1
    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    :goto_2
    invoke-virtual {p0, v1}, LX/D9q;->setTranslationX(F)V

    goto :goto_1

    :cond_2
    const v1, 0x461c4000    # 10000.0f

    goto :goto_2
.end method

.method public static k(LX/D9q;)V
    .locals 2

    .prologue
    .line 1970484
    iget-object v0, p0, LX/D9q;->e:LX/0pu;

    invoke-virtual {v0}, LX/0pu;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/D9q;->j:LX/D9h;

    .line 1970485
    iget-object v1, v0, LX/D9h;->d:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/D9h;->e:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->i()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1970486
    if-nez v0, :cond_1

    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1970487
    :cond_1
    invoke-static {p0}, LX/0wc;->a(Landroid/view/View;)V

    .line 1970488
    :goto_1
    return-void

    .line 1970489
    :cond_2
    invoke-static {p0}, LX/0wc;->b(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 1970474
    invoke-virtual {p0}, LX/D9q;->removeAllViewsInLayout()V

    .line 1970475
    iget-object v0, p0, LX/D9q;->k:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1970476
    iget-object v0, p0, LX/D9q;->k:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1970477
    :cond_0
    const v0, 0x7f030c93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1970478
    const v0, 0x7f0d0807

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/D9q;->k:Landroid/view/ViewGroup;

    .line 1970479
    invoke-direct {p0}, LX/D9q;->g()V

    .line 1970480
    invoke-direct {p0}, LX/D9q;->h()V

    .line 1970481
    invoke-direct {p0}, LX/D9q;->i()V

    .line 1970482
    invoke-direct {p0}, LX/D9q;->a()V

    .line 1970483
    return-void
.end method


# virtual methods
.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 1970469
    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D9q;->p:LX/0wd;

    .line 1970470
    iget-wide v4, v0, LX/0wd;->i:D

    move-wide v0, v4

    .line 1970471
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1

    .line 1970472
    :cond_0
    const/4 v0, 0x1

    .line 1970473
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getCurrentAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1970465
    invoke-virtual {p0}, LX/D9q;->getCurrentContent()LX/D9j;

    move-result-object v0

    .line 1970466
    if-eqz v0, :cond_0

    .line 1970467
    invoke-interface {v0}, LX/D9j;->a()Ljava/lang/String;

    move-result-object v0

    .line 1970468
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentContent()LX/D9j;
    .locals 1

    .prologue
    .line 1970464
    iget-object v0, p0, LX/D9q;->x:LX/D9J;

    invoke-direct {p0, v0}, LX/D9q;->a(LX/D9J;)LX/D9j;

    move-result-object v0

    return-object v0
.end method

.method public getDebugInfo()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1970460
    invoke-virtual {p0}, LX/D9q;->getCurrentContent()LX/D9j;

    move-result-object v0

    .line 1970461
    instance-of v1, v0, LX/0f6;

    if-eqz v1, :cond_0

    .line 1970462
    check-cast v0, LX/0f6;

    invoke-interface {v0}, LX/0f6;->getDebugInfo()Ljava/util/Map;

    move-result-object v0

    .line 1970463
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShownContentType()LX/D9J;
    .locals 1

    .prologue
    .line 1970459
    iget-object v0, p0, LX/D9q;->x:LX/D9J;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5491123f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1970504
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1970505
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/D9q;->z:Z

    .line 1970506
    const/16 v1, 0x2d

    const v2, -0x35a1a79d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x73b94181

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1970507
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1970508
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/D9q;->z:Z

    .line 1970509
    invoke-direct {p0}, LX/D9q;->f()V

    .line 1970510
    iget-object v1, p0, LX/D9q;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1970511
    :cond_0
    iget-object v1, p0, LX/D9q;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1970512
    const v1, -0x11193b4

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1970513
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1970514
    if-eqz p1, :cond_0

    .line 1970515
    invoke-static {p0}, LX/D9q;->j(LX/D9q;)V

    .line 1970516
    :cond_0
    return-void
.end method

.method public setAdapter(LX/D9k;)V
    .locals 0

    .prologue
    .line 1970517
    iput-object p1, p0, LX/D9q;->w:LX/D9k;

    .line 1970518
    return-void
.end method

.method public setChatHeadsPositioningStrategy(LX/D9K;)V
    .locals 0

    .prologue
    .line 1970519
    iput-object p1, p0, LX/D9q;->r:LX/D9K;

    .line 1970520
    return-void
.end method

.method public setContentYOffset(I)V
    .locals 2

    .prologue
    .line 1970521
    iget-object v0, p0, LX/D9q;->k:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1970522
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1970523
    iget-object v1, p0, LX/D9q;->k:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1970524
    return-void
.end method

.method public setNubTarget(I)V
    .locals 2

    .prologue
    .line 1970525
    invoke-direct {p0, p1}, LX/D9q;->a(I)Landroid/graphics/PointF;

    move-result-object v0

    .line 1970526
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v1, v0}, LX/D9q;->a(FF)V

    .line 1970527
    iput p1, p0, LX/D9q;->q:I

    .line 1970528
    return-void
.end method

.method public setNubVisibility(I)V
    .locals 1

    .prologue
    .line 1970531
    iget-object v0, p0, LX/D9q;->m:LX/3Nk;

    invoke-virtual {v0, p1}, LX/3Nk;->setVisibility(I)V

    .line 1970532
    return-void
.end method

.method public setOnVisibilityChangeListener(LX/D9p;)V
    .locals 0

    .prologue
    .line 1970529
    iput-object p1, p0, LX/D9q;->C:LX/D9p;

    .line 1970530
    return-void
.end method
