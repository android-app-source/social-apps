.class public LX/DOF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0bH;

.field private final b:LX/DO0;

.field public final c:LX/189;

.field public final d:LX/1Sl;

.field public e:LX/0qq;

.field public f:LX/DOD;

.field public g:LX/DOJ;


# direct methods
.method public constructor <init>(LX/0bH;LX/189;LX/1Sl;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1992156
    iput-object p1, p0, LX/DOF;->a:LX/0bH;

    .line 1992157
    iput-object p2, p0, LX/DOF;->c:LX/189;

    .line 1992158
    new-instance v0, LX/DOE;

    invoke-direct {v0, p0}, LX/DOE;-><init>(LX/DOF;)V

    iput-object v0, p0, LX/DOF;->b:LX/DO0;

    .line 1992159
    iput-object p3, p0, LX/DOF;->d:LX/1Sl;

    .line 1992160
    return-void
.end method

.method public static a(LX/0QB;)LX/DOF;
    .locals 1

    .prologue
    .line 1992161
    invoke-static {p0}, LX/DOF;->b(LX/0QB;)LX/DOF;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DOF;
    .locals 4

    .prologue
    .line 1992162
    new-instance v3, LX/DOF;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v0

    check-cast v0, LX/0bH;

    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v1

    check-cast v1, LX/189;

    invoke-static {p0}, LX/1Sl;->b(LX/0QB;)LX/1Sl;

    move-result-object v2

    check-cast v2, LX/1Sl;

    invoke-direct {v3, v0, v1, v2}, LX/DOF;-><init>(LX/0bH;LX/189;LX/1Sl;)V

    .line 1992163
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1992164
    iget-object v0, p0, LX/DOF;->a:LX/0bH;

    iget-object v1, p0, LX/DOF;->b:LX/DO0;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1992165
    return-void
.end method

.method public final a(LX/0qq;LX/DOD;LX/DOJ;)V
    .locals 2

    .prologue
    .line 1992166
    iget-object v0, p0, LX/DOF;->a:LX/0bH;

    iget-object v1, p0, LX/DOF;->b:LX/DO0;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1992167
    iput-object p1, p0, LX/DOF;->e:LX/0qq;

    .line 1992168
    iput-object p2, p0, LX/DOF;->f:LX/DOD;

    .line 1992169
    iput-object p3, p0, LX/DOF;->g:LX/DOJ;

    .line 1992170
    return-void
.end method
