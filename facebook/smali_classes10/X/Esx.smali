.class public LX/Esx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2176718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/1Cn;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;LX/1Ps;)Landroid/view/View$OnClickListener;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Ps;",
            ":",
            "LX/1Po;",
            ">(",
            "LX/1Cn;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/lang/String;",
            "LX/1Nq;",
            "LX/1Kf;",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            "TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2176714
    invoke-static {p5}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    .line 2176715
    invoke-static {v2}, LX/Esx;->a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/39x;

    invoke-direct {v0}, LX/39x;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/39x;->e(Ljava/lang/String;)LX/39x;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/39x;

    move-result-object v0

    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v8

    .line 2176716
    :goto_0
    new-instance v0, LX/Esv;

    move-object v1, p0

    move-object v3, p2

    move-object/from16 v4, p6

    move-object v5, p1

    move-object v6, p3

    move-object v7, p5

    move-object v9, p4

    invoke-direct/range {v0 .. v9}, LX/Esv;-><init>(LX/1Cn;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Ljava/lang/String;LX/1Ps;LX/03V;LX/1Nq;Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Kf;)V

    return-object v0

    .line 2176717
    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Cn;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;LX/Es5;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Landroid/view/View$OnClickListener;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Ps;",
            ":",
            "LX/1Po;",
            ">(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/C51;",
            "LX/1Cn;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/lang/String;",
            "LX/1Nq;",
            "LX/1Kf;",
            "LX/Es5;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2176657
    invoke-virtual/range {p8 .. p8}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v3

    .line 2176658
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 2176659
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v11

    .line 2176660
    :goto_1
    new-instance v0, LX/Esu;

    move-object/from16 v1, p8

    move-object v2, p2

    move-object v4, p0

    move-object v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p9

    move-object/from16 v12, p7

    invoke-direct/range {v0 .. v12}, LX/Esu;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Cn;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Lcom/facebook/content/SecureContextHelper;LX/C51;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;LX/1Ps;Ljava/lang/String;LX/Es5;)V

    return-object v0

    .line 2176661
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2176662
    :cond_1
    const/4 v11, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Cn;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Landroid/view/View$OnClickListener;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Ps;",
            ":",
            "LX/1Po;",
            ">(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/C51;",
            "LX/1Cn;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/lang/String;",
            "LX/1Nq;",
            "LX/1Kf;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2176692
    iget-object v0, p7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 2176693
    check-cast v5, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2176694
    invoke-static {v5}, LX/23B;->f(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2176695
    const/4 v2, 0x0

    .line 2176696
    iget-object v0, p7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2176697
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2176698
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2176699
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2176700
    invoke-virtual {p7, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2176701
    check-cast p8, LX/1Po;

    invoke-interface {p8}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    const-string v2, "throwbackUnifiedShare"

    const-string v3, "goodwill_throwback_promotion_ufi"

    invoke-virtual {p1, v0, v1, v2, v3}, LX/C51;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Ljava/lang/String;Ljava/lang/String;)LX/C50;

    move-result-object v0

    .line 2176702
    :goto_0
    move-object v0, v0

    .line 2176703
    :goto_1
    return-object v0

    .line 2176704
    :cond_0
    const-string v0, "friendversary_polaroids"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "friendversary_polaroids_ipb"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "friendversary_card_data"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move-object v0, p2

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v6, p8

    .line 2176705
    invoke-static/range {v0 .. v6}, LX/Esx;->a(LX/1Cn;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v0

    goto :goto_1

    .line 2176706
    :cond_2
    const-string v0, "mle"

    .line 2176707
    const-string v1, "friendversary_card_collage"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "friendversary_card_collage_ipb"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2176708
    :cond_3
    const-string v0, "friendversary_collage"

    .line 2176709
    :cond_4
    const-string v1, "faceversary_card_collage"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2176710
    const-string v0, "faceversary_collage"

    move-object v1, v0

    .line 2176711
    :goto_2
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    .line 2176712
    new-instance v2, LX/Esw;

    invoke-direct {v2, v0, v1, p0}, LX/Esw;-><init>(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;)V

    move-object v0, v2

    .line 2176713
    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2176691
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 2176690
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2176689
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2176686
    const-string v0, "friendversary_card_collage"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "friendversary_card_collage_ipb"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "friendversary_polaroids"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "friendversary_polaroids_ipb"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2176687
    :cond_0
    const/4 v0, 0x1

    .line 2176688
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;)Lcom/facebook/goodwill/composer/GoodwillComposerEvent;
    .locals 10

    .prologue
    .line 2176678
    new-instance v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v2}, LX/Esx;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/Esx;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f082a1f

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f082a22

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f082a23

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->f()I

    move-result v7

    const-string v8, "throwback_promotion"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v9

    invoke-static {v9}, LX/CFE;->a(Lcom/facebook/graphql/model/GraphQLUser;)LX/0Px;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    .line 2176679
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2176680
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2176681
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2176682
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2176683
    new-instance v5, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;-><init>(Lcom/facebook/graphql/model/GraphQLMedia;)V

    invoke-virtual {v0, v5}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 2176684
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2176685
    :cond_1
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2176663
    if-nez p0, :cond_1

    .line 2176664
    :cond_0
    :goto_0
    return-object v0

    .line 2176665
    :cond_1
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    if-eqz v1, :cond_2

    .line 2176666
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 2176667
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v1

    .line 2176668
    if-eqz v1, :cond_0

    .line 2176669
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    goto :goto_0

    .line 2176670
    :cond_2
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    if-eqz v1, :cond_3

    .line 2176671
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-static {p0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 2176672
    if-eqz v1, :cond_0

    .line 2176673
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    goto :goto_0

    .line 2176674
    :cond_3
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    if-eqz v1, :cond_0

    .line 2176675
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 2176676
    if-eqz v1, :cond_0

    .line 2176677
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    goto :goto_0
.end method
