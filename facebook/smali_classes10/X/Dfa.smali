.class public final enum LX/Dfa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dfa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dfa;

.field public static final enum THREAD_MULTI:LX/Dfa;

.field public static final enum THREAD_SINGLE:LX/Dfa;

.field public static final enum THREAD_WITH_MONTAGE:LX/Dfa;

.field public static final enum V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL:LX/Dfa;

.field public static final enum V2_ACTIVE_NOW_TICKER:LX/Dfa;

.field public static final enum V2_ANNOUNCEMENT:LX/Dfa;

.field public static final enum V2_BYMM_PAGE:LX/Dfa;

.field public static final enum V2_BYMM_VERTICAL:LX/Dfa;

.field public static final enum V2_CONTACTS_YOU_MAY_KNOW:LX/Dfa;

.field public static final enum V2_CONTACTS_YOU_MAY_KNOW_ITEM:LX/Dfa;

.field public static final enum V2_CONVERSATION_STARTER:LX/Dfa;

.field public static final enum V2_DIRECT_M:LX/Dfa;

.field public static final enum V2_GAME_SUGGESTION:LX/Dfa;

.field public static final enum V2_HIDDEN_UNIT:LX/Dfa;

.field public static final enum V2_HORIZONTAL_TILES_PAGINATED_UNIT:LX/Dfa;

.field public static final enum V2_HORIZONTAL_TILES_UNIT_ITEM:LX/Dfa;

.field public static final enum V2_HORIZONTAL_TILE_ITEM:LX/Dfa;

.field public static final enum V2_INVITE_FB_FRIENDS:LX/Dfa;

.field public static final enum V2_INVITE_FB_FRIENDS_ITEM:LX/Dfa;

.field public static final enum V2_LOAD_MORE_THREADS_PLACEHOLDER:LX/Dfa;

.field public static final enum V2_MESSAGE_REQUEST_HEADER:LX/Dfa;

.field public static final enum V2_MESSAGE_REQUEST_THREADS:LX/Dfa;

.field public static final enum V2_MESSENGER_ADS_CLASSIC_UNIT:LX/Dfa;

.field public static final enum V2_MESSENGER_ADS_HSCROLL_UNIT:LX/Dfa;

.field public static final enum V2_MESSENGER_ADS_SINGLE_IMAGE_ITEM:LX/Dfa;

.field public static final enum V2_MONTAGE_COMPOSER_HEADER:LX/Dfa;

.field public static final enum V2_MONTAGE_COMPOSER_HEADER_ITEM:LX/Dfa;

.field public static final enum V2_MONTAGE_NUX_ITEM:LX/Dfa;

.field public static final enum V2_MORE_CONVERSATIONS_FOOTER:LX/Dfa;

.field public static final enum V2_MORE_FOOTER:LX/Dfa;

.field public static final enum V2_RANKED_USER:LX/Dfa;

.field public static final enum V2_RECENT_THREADS_PLACEHOLDER:LX/Dfa;

.field public static final enum V2_ROOM_SUGGESTION:LX/Dfa;

.field public static final enum V2_ROOM_SUGGESTION_CREATE_ROOM:LX/Dfa;

.field public static final enum V2_ROOM_SUGGESTION_ITEM:LX/Dfa;

.field public static final enum V2_ROOM_SUGGESTION_SEE_MORE:LX/Dfa;

.field public static final enum V2_RTC_RECOMMENDATION:LX/Dfa;

.field public static final enum V2_SECTION_HEADER:LX/Dfa;

.field public static final enum V2_SEE_ALL_FOOTER:LX/Dfa;

.field public static final enum V2_SUBSCRIPTION_CONTENT:LX/Dfa;

.field public static final enum V2_SUBSCRIPTION_NUX:LX/Dfa;

.field public static final enum V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER:LX/Dfa;

.field public static final enum V2_TRENDING_GIFS:LX/Dfa;

.field public static final enum V2_TRENDING_GIF_ITEM:LX/Dfa;

.field public static final enum V2_UNKNOWN_TYPE:LX/Dfa;

.field public static final enum V2_USER_WITH_STATUS:LX/Dfa;

.field private static final sValues:[LX/Dfa;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2027482
    new-instance v0, LX/Dfa;

    const-string v1, "THREAD_SINGLE"

    invoke-direct {v0, v1, v3}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->THREAD_SINGLE:LX/Dfa;

    .line 2027483
    new-instance v0, LX/Dfa;

    const-string v1, "THREAD_MULTI"

    invoke-direct {v0, v1, v4}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->THREAD_MULTI:LX/Dfa;

    .line 2027484
    new-instance v0, LX/Dfa;

    const-string v1, "THREAD_WITH_MONTAGE"

    invoke-direct {v0, v1, v5}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->THREAD_WITH_MONTAGE:LX/Dfa;

    .line 2027485
    new-instance v0, LX/Dfa;

    const-string v1, "V2_RECENT_THREADS_PLACEHOLDER"

    invoke-direct {v0, v1, v6}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_RECENT_THREADS_PLACEHOLDER:LX/Dfa;

    .line 2027486
    new-instance v0, LX/Dfa;

    const-string v1, "V2_CONVERSATION_STARTER"

    invoke-direct {v0, v1, v7}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_CONVERSATION_STARTER:LX/Dfa;

    .line 2027487
    new-instance v0, LX/Dfa;

    const-string v1, "V2_SECTION_HEADER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_SECTION_HEADER:LX/Dfa;

    .line 2027488
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MESSAGE_REQUEST_HEADER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MESSAGE_REQUEST_HEADER:LX/Dfa;

    .line 2027489
    new-instance v0, LX/Dfa;

    const-string v1, "V2_USER_WITH_STATUS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_USER_WITH_STATUS:LX/Dfa;

    .line 2027490
    new-instance v0, LX/Dfa;

    const-string v1, "V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL:LX/Dfa;

    .line 2027491
    new-instance v0, LX/Dfa;

    const-string v1, "V2_ACTIVE_NOW_TICKER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_ACTIVE_NOW_TICKER:LX/Dfa;

    .line 2027492
    new-instance v0, LX/Dfa;

    const-string v1, "V2_RTC_RECOMMENDATION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_RTC_RECOMMENDATION:LX/Dfa;

    .line 2027493
    new-instance v0, LX/Dfa;

    const-string v1, "V2_TRENDING_GIFS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_TRENDING_GIFS:LX/Dfa;

    .line 2027494
    new-instance v0, LX/Dfa;

    const-string v1, "V2_TRENDING_GIF_ITEM"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_TRENDING_GIF_ITEM:LX/Dfa;

    .line 2027495
    new-instance v0, LX/Dfa;

    const-string v1, "V2_LOAD_MORE_THREADS_PLACEHOLDER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_LOAD_MORE_THREADS_PLACEHOLDER:LX/Dfa;

    .line 2027496
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MONTAGE_COMPOSER_HEADER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MONTAGE_COMPOSER_HEADER:LX/Dfa;

    .line 2027497
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MONTAGE_COMPOSER_HEADER_ITEM"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MONTAGE_COMPOSER_HEADER_ITEM:LX/Dfa;

    .line 2027498
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MONTAGE_NUX_ITEM"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MONTAGE_NUX_ITEM:LX/Dfa;

    .line 2027499
    new-instance v0, LX/Dfa;

    const-string v1, "V2_BYMM_PAGE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_BYMM_PAGE:LX/Dfa;

    .line 2027500
    new-instance v0, LX/Dfa;

    const-string v1, "V2_BYMM_VERTICAL"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_BYMM_VERTICAL:LX/Dfa;

    .line 2027501
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MORE_FOOTER"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MORE_FOOTER:LX/Dfa;

    .line 2027502
    new-instance v0, LX/Dfa;

    const-string v1, "V2_SEE_ALL_FOOTER"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_SEE_ALL_FOOTER:LX/Dfa;

    .line 2027503
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MORE_CONVERSATIONS_FOOTER"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MORE_CONVERSATIONS_FOOTER:LX/Dfa;

    .line 2027504
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MESSAGE_REQUEST_THREADS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MESSAGE_REQUEST_THREADS:LX/Dfa;

    .line 2027505
    new-instance v0, LX/Dfa;

    const-string v1, "V2_ANNOUNCEMENT"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_ANNOUNCEMENT:LX/Dfa;

    .line 2027506
    new-instance v0, LX/Dfa;

    const-string v1, "V2_CONTACTS_YOU_MAY_KNOW"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_CONTACTS_YOU_MAY_KNOW:LX/Dfa;

    .line 2027507
    new-instance v0, LX/Dfa;

    const-string v1, "V2_CONTACTS_YOU_MAY_KNOW_ITEM"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_CONTACTS_YOU_MAY_KNOW_ITEM:LX/Dfa;

    .line 2027508
    new-instance v0, LX/Dfa;

    const-string v1, "V2_INVITE_FB_FRIENDS"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_INVITE_FB_FRIENDS:LX/Dfa;

    .line 2027509
    new-instance v0, LX/Dfa;

    const-string v1, "V2_INVITE_FB_FRIENDS_ITEM"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_INVITE_FB_FRIENDS_ITEM:LX/Dfa;

    .line 2027510
    new-instance v0, LX/Dfa;

    const-string v1, "V2_SUBSCRIPTION_CONTENT"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_SUBSCRIPTION_CONTENT:LX/Dfa;

    .line 2027511
    new-instance v0, LX/Dfa;

    const-string v1, "V2_SUBSCRIPTION_NUX"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_SUBSCRIPTION_NUX:LX/Dfa;

    .line 2027512
    new-instance v0, LX/Dfa;

    const-string v1, "V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER:LX/Dfa;

    .line 2027513
    new-instance v0, LX/Dfa;

    const-string v1, "V2_HORIZONTAL_TILE_ITEM"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_HORIZONTAL_TILE_ITEM:LX/Dfa;

    .line 2027514
    new-instance v0, LX/Dfa;

    const-string v1, "V2_HORIZONTAL_TILES_UNIT_ITEM"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_HORIZONTAL_TILES_UNIT_ITEM:LX/Dfa;

    .line 2027515
    new-instance v0, LX/Dfa;

    const-string v1, "V2_HORIZONTAL_TILES_PAGINATED_UNIT"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_HORIZONTAL_TILES_PAGINATED_UNIT:LX/Dfa;

    .line 2027516
    new-instance v0, LX/Dfa;

    const-string v1, "V2_RANKED_USER"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_RANKED_USER:LX/Dfa;

    .line 2027517
    new-instance v0, LX/Dfa;

    const-string v1, "V2_ROOM_SUGGESTION"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_ROOM_SUGGESTION:LX/Dfa;

    .line 2027518
    new-instance v0, LX/Dfa;

    const-string v1, "V2_ROOM_SUGGESTION_ITEM"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_ROOM_SUGGESTION_ITEM:LX/Dfa;

    .line 2027519
    new-instance v0, LX/Dfa;

    const-string v1, "V2_ROOM_SUGGESTION_CREATE_ROOM"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_ROOM_SUGGESTION_CREATE_ROOM:LX/Dfa;

    .line 2027520
    new-instance v0, LX/Dfa;

    const-string v1, "V2_ROOM_SUGGESTION_SEE_MORE"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_ROOM_SUGGESTION_SEE_MORE:LX/Dfa;

    .line 2027521
    new-instance v0, LX/Dfa;

    const-string v1, "V2_GAME_SUGGESTION"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_GAME_SUGGESTION:LX/Dfa;

    .line 2027522
    new-instance v0, LX/Dfa;

    const-string v1, "V2_HIDDEN_UNIT"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_HIDDEN_UNIT:LX/Dfa;

    .line 2027523
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MESSENGER_ADS_CLASSIC_UNIT"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MESSENGER_ADS_CLASSIC_UNIT:LX/Dfa;

    .line 2027524
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MESSENGER_ADS_HSCROLL_UNIT"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MESSENGER_ADS_HSCROLL_UNIT:LX/Dfa;

    .line 2027525
    new-instance v0, LX/Dfa;

    const-string v1, "V2_MESSENGER_ADS_SINGLE_IMAGE_ITEM"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_MESSENGER_ADS_SINGLE_IMAGE_ITEM:LX/Dfa;

    .line 2027526
    new-instance v0, LX/Dfa;

    const-string v1, "V2_DIRECT_M"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_DIRECT_M:LX/Dfa;

    .line 2027527
    new-instance v0, LX/Dfa;

    const-string v1, "V2_UNKNOWN_TYPE"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, LX/Dfa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfa;->V2_UNKNOWN_TYPE:LX/Dfa;

    .line 2027528
    const/16 v0, 0x2e

    new-array v0, v0, [LX/Dfa;

    sget-object v1, LX/Dfa;->THREAD_SINGLE:LX/Dfa;

    aput-object v1, v0, v3

    sget-object v1, LX/Dfa;->THREAD_MULTI:LX/Dfa;

    aput-object v1, v0, v4

    sget-object v1, LX/Dfa;->THREAD_WITH_MONTAGE:LX/Dfa;

    aput-object v1, v0, v5

    sget-object v1, LX/Dfa;->V2_RECENT_THREADS_PLACEHOLDER:LX/Dfa;

    aput-object v1, v0, v6

    sget-object v1, LX/Dfa;->V2_CONVERSATION_STARTER:LX/Dfa;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Dfa;->V2_SECTION_HEADER:LX/Dfa;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Dfa;->V2_MESSAGE_REQUEST_HEADER:LX/Dfa;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Dfa;->V2_USER_WITH_STATUS:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Dfa;->V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Dfa;->V2_ACTIVE_NOW_TICKER:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Dfa;->V2_RTC_RECOMMENDATION:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Dfa;->V2_TRENDING_GIFS:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Dfa;->V2_TRENDING_GIF_ITEM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Dfa;->V2_LOAD_MORE_THREADS_PLACEHOLDER:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Dfa;->V2_MONTAGE_COMPOSER_HEADER:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Dfa;->V2_MONTAGE_COMPOSER_HEADER_ITEM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Dfa;->V2_MONTAGE_NUX_ITEM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/Dfa;->V2_BYMM_PAGE:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/Dfa;->V2_BYMM_VERTICAL:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/Dfa;->V2_MORE_FOOTER:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/Dfa;->V2_SEE_ALL_FOOTER:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/Dfa;->V2_MORE_CONVERSATIONS_FOOTER:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/Dfa;->V2_MESSAGE_REQUEST_THREADS:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/Dfa;->V2_ANNOUNCEMENT:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/Dfa;->V2_CONTACTS_YOU_MAY_KNOW:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/Dfa;->V2_CONTACTS_YOU_MAY_KNOW_ITEM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/Dfa;->V2_INVITE_FB_FRIENDS:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/Dfa;->V2_INVITE_FB_FRIENDS_ITEM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/Dfa;->V2_SUBSCRIPTION_CONTENT:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/Dfa;->V2_SUBSCRIPTION_NUX:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/Dfa;->V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/Dfa;->V2_HORIZONTAL_TILE_ITEM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/Dfa;->V2_HORIZONTAL_TILES_UNIT_ITEM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/Dfa;->V2_HORIZONTAL_TILES_PAGINATED_UNIT:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/Dfa;->V2_RANKED_USER:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/Dfa;->V2_ROOM_SUGGESTION:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/Dfa;->V2_ROOM_SUGGESTION_ITEM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/Dfa;->V2_ROOM_SUGGESTION_CREATE_ROOM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/Dfa;->V2_ROOM_SUGGESTION_SEE_MORE:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/Dfa;->V2_GAME_SUGGESTION:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/Dfa;->V2_HIDDEN_UNIT:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/Dfa;->V2_MESSENGER_ADS_CLASSIC_UNIT:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/Dfa;->V2_MESSENGER_ADS_HSCROLL_UNIT:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/Dfa;->V2_MESSENGER_ADS_SINGLE_IMAGE_ITEM:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/Dfa;->V2_DIRECT_M:LX/Dfa;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/Dfa;->V2_UNKNOWN_TYPE:LX/Dfa;

    aput-object v2, v0, v1

    sput-object v0, LX/Dfa;->$VALUES:[LX/Dfa;

    .line 2027529
    invoke-static {}, LX/Dfa;->values()[LX/Dfa;

    move-result-object v0

    sput-object v0, LX/Dfa;->sValues:[LX/Dfa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2027480
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2027481
    return-void
.end method

.method public static valueOf(I)LX/Dfa;
    .locals 3

    .prologue
    .line 2027477
    if-ltz p0, :cond_0

    sget-object v0, LX/Dfa;->sValues:[LX/Dfa;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 2027478
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2027479
    :cond_1
    sget-object v0, LX/Dfa;->sValues:[LX/Dfa;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dfa;
    .locals 1

    .prologue
    .line 2027530
    const-class v0, LX/Dfa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dfa;

    return-object v0
.end method

.method public static values()[LX/Dfa;
    .locals 1

    .prologue
    .line 2027476
    sget-object v0, LX/Dfa;->$VALUES:[LX/Dfa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dfa;

    return-object v0
.end method


# virtual methods
.method public final shouldRecyclerViewProvidePressState()Z
    .locals 2

    .prologue
    .line 2027473
    sget-object v0, LX/DfZ;->a:[I

    invoke-virtual {p0}, LX/Dfa;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2027474
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2027475
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
