.class public final LX/Dec;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2022844
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 2022845
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2022846
    :goto_0
    return v1

    .line 2022847
    :cond_0
    const-string v11, "is_badged"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2022848
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    .line 2022849
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 2022850
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2022851
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2022852
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2022853
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2022854
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_1

    .line 2022855
    :cond_3
    const-string v11, "application"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2022856
    invoke-static {p0, p1}, LX/DeT;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2022857
    :cond_4
    const-string v11, "games"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2022858
    invoke-static {p0, p1}, LX/DeW;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2022859
    :cond_5
    const-string v11, "tags"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2022860
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2022861
    :cond_6
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 2022862
    invoke-static {p0, p1}, LX/DeX;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2022863
    :cond_7
    const-string v11, "update_lines"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2022864
    invoke-static {p0, p1}, LX/Deb;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2022865
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2022866
    :cond_9
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2022867
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2022868
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 2022869
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2022870
    if-eqz v0, :cond_a

    .line 2022871
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 2022872
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2022873
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2022874
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2022875
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 2022813
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2022814
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2022815
    if-eqz v0, :cond_0

    .line 2022816
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022817
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2022818
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2022819
    if-eqz v0, :cond_1

    .line 2022820
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022821
    invoke-static {p0, v0, p2, p3}, LX/DeT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2022822
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2022823
    if-eqz v0, :cond_2

    .line 2022824
    const-string v1, "games"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022825
    invoke-static {p0, v0, p2, p3}, LX/DeW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2022826
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2022827
    if-eqz v0, :cond_3

    .line 2022828
    const-string v1, "is_badged"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022829
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2022830
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2022831
    if-eqz v0, :cond_4

    .line 2022832
    const-string v0, "tags"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022833
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2022834
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2022835
    if-eqz v0, :cond_5

    .line 2022836
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022837
    invoke-static {p0, v0, p2}, LX/DeX;->a(LX/15i;ILX/0nX;)V

    .line 2022838
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2022839
    if-eqz v0, :cond_6

    .line 2022840
    const-string v1, "update_lines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022841
    invoke-static {p0, v0, p2, p3}, LX/Deb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2022842
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2022843
    return-void
.end method
