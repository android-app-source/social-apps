.class public final LX/D3W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Ljava/util/concurrent/ExecutorService;

.field public final synthetic b:LX/1Be;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/D3X;


# direct methods
.method public constructor <init>(LX/D3X;Ljava/util/concurrent/ExecutorService;LX/1Be;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1959914
    iput-object p1, p0, LX/D3W;->d:LX/D3X;

    iput-object p2, p0, LX/D3W;->a:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, LX/D3W;->b:LX/1Be;

    iput-object p4, p0, LX/D3W;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 1959915
    iget-object v0, p0, LX/D3W;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/ui/browser/prefs/BrowserClearPrefetchDataPreference$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/ui/browser/prefs/BrowserClearPrefetchDataPreference$1$1;-><init>(LX/D3W;)V

    const v2, -0x1eb87bdd

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1959916
    iget-object v0, p0, LX/D3W;->c:Landroid/content/Context;

    const-string v1, "Cleared"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1959917
    const/4 v0, 0x1

    return v0
.end method
