.class public final enum LX/CwI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CwI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CwI;

.field public static final enum BOOTSTRAP:LX/CwI;

.field public static final enum ECHO:LX/CwI;

.field public static final enum ENTITY_BOOTSTRAP:LX/CwI;

.field public static final enum ENTITY_REMOTE:LX/CwI;

.field public static final enum ESCAPE:LX/CwI;

.field public static final enum INJECTED_SUGGESTION:LX/CwI;

.field public static final enum NFL_SPORTS_TEAM:LX/CwI;

.field public static final enum NULL_STATE_MODULE:LX/CwI;

.field public static final enum RECENT_SEARCHES:LX/CwI;

.field public static final enum SEARCH_BUTTON:LX/CwI;

.field public static final enum SINGLE_STATE:LX/CwI;

.field public static final enum SPELL_CORRECTION_ESCAPE:LX/CwI;

.field public static final enum SS_SEE_MORE_BUTTON:LX/CwI;

.field public static final enum SS_SEE_MORE_LINK:LX/CwI;

.field public static final enum SUGGESTION:LX/CwI;

.field public static final enum TRENDING_ENTITY:LX/CwI;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1950325
    new-instance v0, LX/CwI;

    const-string v1, "SUGGESTION"

    invoke-direct {v0, v1, v3}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->SUGGESTION:LX/CwI;

    .line 1950326
    new-instance v0, LX/CwI;

    const-string v1, "ENTITY_REMOTE"

    invoke-direct {v0, v1, v4}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->ENTITY_REMOTE:LX/CwI;

    .line 1950327
    new-instance v0, LX/CwI;

    const-string v1, "ENTITY_BOOTSTRAP"

    invoke-direct {v0, v1, v5}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->ENTITY_BOOTSTRAP:LX/CwI;

    .line 1950328
    new-instance v0, LX/CwI;

    const-string v1, "BOOTSTRAP"

    invoke-direct {v0, v1, v6}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->BOOTSTRAP:LX/CwI;

    .line 1950329
    new-instance v0, LX/CwI;

    const-string v1, "SINGLE_STATE"

    invoke-direct {v0, v1, v7}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->SINGLE_STATE:LX/CwI;

    .line 1950330
    new-instance v0, LX/CwI;

    const-string v1, "RECENT_SEARCHES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->RECENT_SEARCHES:LX/CwI;

    .line 1950331
    new-instance v0, LX/CwI;

    const-string v1, "SEARCH_BUTTON"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    .line 1950332
    new-instance v0, LX/CwI;

    const-string v1, "ECHO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->ECHO:LX/CwI;

    .line 1950333
    new-instance v0, LX/CwI;

    const-string v1, "TRENDING_ENTITY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->TRENDING_ENTITY:LX/CwI;

    .line 1950334
    new-instance v0, LX/CwI;

    const-string v1, "NFL_SPORTS_TEAM"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->NFL_SPORTS_TEAM:LX/CwI;

    .line 1950335
    new-instance v0, LX/CwI;

    const-string v1, "SPELL_CORRECTION_ESCAPE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->SPELL_CORRECTION_ESCAPE:LX/CwI;

    .line 1950336
    new-instance v0, LX/CwI;

    const-string v1, "ESCAPE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->ESCAPE:LX/CwI;

    .line 1950337
    new-instance v0, LX/CwI;

    const-string v1, "NULL_STATE_MODULE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->NULL_STATE_MODULE:LX/CwI;

    .line 1950338
    new-instance v0, LX/CwI;

    const-string v1, "INJECTED_SUGGESTION"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->INJECTED_SUGGESTION:LX/CwI;

    .line 1950339
    new-instance v0, LX/CwI;

    const-string v1, "SS_SEE_MORE_LINK"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->SS_SEE_MORE_LINK:LX/CwI;

    .line 1950340
    new-instance v0, LX/CwI;

    const-string v1, "SS_SEE_MORE_BUTTON"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/CwI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwI;->SS_SEE_MORE_BUTTON:LX/CwI;

    .line 1950341
    const/16 v0, 0x10

    new-array v0, v0, [LX/CwI;

    sget-object v1, LX/CwI;->SUGGESTION:LX/CwI;

    aput-object v1, v0, v3

    sget-object v1, LX/CwI;->ENTITY_REMOTE:LX/CwI;

    aput-object v1, v0, v4

    sget-object v1, LX/CwI;->ENTITY_BOOTSTRAP:LX/CwI;

    aput-object v1, v0, v5

    sget-object v1, LX/CwI;->BOOTSTRAP:LX/CwI;

    aput-object v1, v0, v6

    sget-object v1, LX/CwI;->SINGLE_STATE:LX/CwI;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CwI;->RECENT_SEARCHES:LX/CwI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CwI;->ECHO:LX/CwI;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/CwI;->TRENDING_ENTITY:LX/CwI;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/CwI;->NFL_SPORTS_TEAM:LX/CwI;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/CwI;->SPELL_CORRECTION_ESCAPE:LX/CwI;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/CwI;->ESCAPE:LX/CwI;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/CwI;->NULL_STATE_MODULE:LX/CwI;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/CwI;->INJECTED_SUGGESTION:LX/CwI;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/CwI;->SS_SEE_MORE_LINK:LX/CwI;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/CwI;->SS_SEE_MORE_BUTTON:LX/CwI;

    aput-object v2, v0, v1

    sput-object v0, LX/CwI;->$VALUES:[LX/CwI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1950342
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CwI;
    .locals 1

    .prologue
    .line 1950343
    const-class v0, LX/CwI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CwI;

    return-object v0
.end method

.method public static values()[LX/CwI;
    .locals 1

    .prologue
    .line 1950344
    sget-object v0, LX/CwI;->$VALUES:[LX/CwI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CwI;

    return-object v0
.end method
