.class public final LX/EfR;
.super LX/3hi;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;)V
    .locals 0

    .prologue
    .line 2154543
    iput-object p1, p0, LX/EfR;->b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    invoke-direct {p0}, LX/3hi;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2154544
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 2154545
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 2154546
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v5, v0, 0x5

    .line 2154547
    mul-int/lit8 v6, v5, 0x5

    move v3, v4

    .line 2154548
    :goto_0
    if-ge v3, v6, :cond_3

    move v1, v4

    .line 2154549
    :goto_1
    if-ge v1, v6, :cond_2

    .line 2154550
    invoke-virtual {p1, v3, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v7

    move v2, v3

    .line 2154551
    :goto_2
    add-int v0, v3, v5

    if-ge v2, v0, :cond_1

    move v0, v1

    .line 2154552
    :goto_3
    add-int v8, v1, v5

    if-ge v0, v8, :cond_0

    .line 2154553
    invoke-virtual {p1, v2, v0, v7}, Landroid/graphics/Bitmap;->setPixel(III)V

    .line 2154554
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2154555
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2154556
    :cond_1
    add-int/2addr v1, v5

    goto :goto_1

    .line 2154557
    :cond_2
    add-int/2addr v3, v5

    goto :goto_0

    .line 2154558
    :cond_3
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2154559
    const-string v0, "snacks_inbox_media_preview"

    return-object v0
.end method
