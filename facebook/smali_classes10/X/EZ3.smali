.class public final LX/EZ3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "LX/EYy;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/EZ6;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/EYy;


# direct methods
.method public constructor <init>(LX/EWc;)V
    .locals 1

    .prologue
    .line 2138499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138500
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LX/EZ3;->a:Ljava/util/Stack;

    .line 2138501
    invoke-static {p0, p1}, LX/EZ3;->a(LX/EZ3;LX/EWc;)LX/EYy;

    move-result-object v0

    iput-object v0, p0, LX/EZ3;->b:LX/EYy;

    .line 2138502
    return-void
.end method

.method public static a(LX/EZ3;LX/EWc;)LX/EYy;
    .locals 2

    .prologue
    .line 2138493
    move-object v0, p1

    .line 2138494
    :goto_0
    instance-of v1, v0, LX/EZ6;

    if-eqz v1, :cond_0

    .line 2138495
    check-cast v0, LX/EZ6;

    .line 2138496
    iget-object v1, p0, LX/EZ3;->a:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2138497
    iget-object v0, v0, LX/EZ6;->e:LX/EWc;

    goto :goto_0

    .line 2138498
    :cond_0
    check-cast v0, LX/EYy;

    return-object v0
.end method


# virtual methods
.method public final a()LX/EYy;
    .locals 3

    .prologue
    .line 2138482
    iget-object v0, p0, LX/EZ3;->b:LX/EYy;

    if-nez v0, :cond_0

    .line 2138483
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 2138484
    :cond_0
    iget-object v0, p0, LX/EZ3;->b:LX/EYy;

    .line 2138485
    :cond_1
    iget-object v1, p0, LX/EZ3;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2138486
    const/4 v1, 0x0

    .line 2138487
    :goto_0
    move-object v1, v1

    .line 2138488
    iput-object v1, p0, LX/EZ3;->b:LX/EYy;

    .line 2138489
    return-object v0

    .line 2138490
    :cond_2
    iget-object v1, p0, LX/EZ3;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EZ6;

    iget-object v1, v1, LX/EZ6;->f:LX/EWc;

    invoke-static {p0, v1}, LX/EZ3;->a(LX/EZ3;LX/EWc;)LX/EYy;

    move-result-object v1

    .line 2138491
    invoke-virtual {v1}, LX/EWc;->b()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 2138492
    if-nez v2, :cond_1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 2138481
    iget-object v0, p0, LX/EZ3;->b:LX/EYy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2138479
    invoke-virtual {p0}, LX/EZ3;->a()LX/EYy;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 2138480
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
