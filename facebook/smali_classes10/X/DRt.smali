.class public final LX/DRt;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;",
        ">;"
    }
.end annotation


# instance fields
.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Integer;

.field public i:Z


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/0tX;LX/B1b;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1998877
    invoke-direct {p0, p1, p5, p6}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;)V

    .line 1998878
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1998879
    iput-object v0, p0, LX/DRt;->e:LX/0Px;

    .line 1998880
    iput-object p2, p0, LX/DRt;->f:Ljava/lang/String;

    .line 1998881
    iput-object p4, p0, LX/DRt;->h:Ljava/lang/Integer;

    .line 1998882
    iput-object p3, p0, LX/DRt;->g:Ljava/lang/String;

    .line 1998883
    return-void
.end method

.method private b(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1998848
    if-eqz p1, :cond_0

    .line 1998849
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998850
    if-eqz v0, :cond_0

    .line 1998851
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998852
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1998853
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998854
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1998855
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998856
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    .line 1998857
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1998858
    sget-object v0, LX/DSe;->ADMIN:LX/DSe;

    invoke-virtual {p0, v0}, LX/B1d;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1998859
    iget-object v7, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v8, LX/DSe;->ADMIN:LX/DSe;

    if-eqz v6, :cond_2

    invoke-virtual {v5, v6, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    :goto_0
    if-eqz v6, :cond_3

    invoke-virtual {v5, v6, v9}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v4, v0}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v0

    invoke-virtual {v7, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1998860
    :cond_0
    if-eqz p1, :cond_1

    .line 1998861
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998862
    if-eqz v0, :cond_1

    .line 1998863
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998864
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1998865
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998866
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1998867
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998868
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 1998869
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1998870
    sget-object v0, LX/DSe;->MODERATOR:LX/DSe;

    invoke-virtual {p0, v0}, LX/B1d;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1998871
    iget-object v6, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v7, LX/DSe;->MODERATOR:LX/DSe;

    if-eqz v5, :cond_4

    invoke-virtual {v4, v5, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    :goto_2
    if-eqz v5, :cond_5

    invoke-virtual {v4, v5, v9}, LX/15i;->h(II)Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1998872
    :cond_1
    return-void

    .line 1998873
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_2
    move-object v4, v3

    .line 1998874
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 1998875
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    move-object v0, v3

    .line 1998876
    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/Enum;)LX/0gW;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1998836
    invoke-static {}, LX/DU6;->a()LX/DU5;

    move-result-object v1

    .line 1998837
    const-string v0, "group_id"

    iget-object v2, p0, LX/DRt;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "search_term"

    iget-object v3, p0, LX/DRt;->g:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "profile_image_size"

    iget-object v3, p0, LX/DRt;->h:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "fetch_admin_type"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1998838
    sget-object v0, LX/DSe;->ADMIN:LX/DSe;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->ADMIN:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->ADMIN:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1998839
    const-string v2, "admin_end_cursor"

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->ADMIN:LX/DSe;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "admin_count"

    const-string v3, "12"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "moderator_count"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1998840
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->MODERATOR:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1998841
    const-string v2, "moderator_end_cursor"

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->MODERATOR:LX/DSe;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    :cond_0
    :goto_0
    move-object v0, v1

    .line 1998842
    :goto_1
    return-object v0

    .line 1998843
    :cond_1
    sget-object v0, LX/DSe;->MODERATOR:LX/DSe;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->MODERATOR:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->MODERATOR:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1998844
    const-string v2, "moderator_end_cursor"

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->MODERATOR:LX/DSe;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "moderator_count"

    const-string v3, "12"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "admin_count"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1998845
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->ADMIN:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1998846
    const-string v2, "admin_end_cursor"

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->ADMIN:LX/DSe;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_0

    .line 1998847
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1998833
    invoke-static {}, LX/DU6;->a()LX/DU5;

    move-result-object v0

    .line 1998834
    const-string v1, "admin_end_cursor"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "moderator_end_cursor"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "group_id"

    iget-object v3, p0, LX/DRt;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "search_term"

    iget-object v3, p0, LX/DRt;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_size"

    iget-object v3, p0, LX/DRt;->h:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "admin_count"

    const-string v3, "12"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "moderator_count"

    const-string v3, "12"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "fetch_admin_type"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1998835
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1998884
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1998885
    iget-object v0, p0, LX/DRt;->e:LX/0Px;

    invoke-virtual {v7, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1998886
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1998887
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998888
    if-eqz v0, :cond_0

    .line 1998889
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998890
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1998891
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998892
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1998893
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998894
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1998895
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998896
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1998897
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998898
    if-eqz v0, :cond_1

    .line 1998899
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998900
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1998901
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998902
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1998903
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998904
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1998905
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998906
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1998907
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1998908
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;

    .line 1998909
    if-eqz v5, :cond_2

    .line 1998910
    sget-object v2, LX/DSb;->ADMIN:LX/DSb;

    .line 1998911
    invoke-virtual {v5}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v5}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v0, v1, :cond_3

    .line 1998912
    sget-object v2, LX/DSb;->MODERATOR:LX/DSb;

    .line 1998913
    :cond_3
    new-instance v0, LX/DSf;

    invoke-virtual {v5}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->l()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v1

    sget-object v3, LX/DSc;->NOT_BLOCKED:LX/DSc;

    sget-object v4, LX/DSb;->ADMIN:LX/DSb;

    if-ne v2, v4, :cond_4

    sget-object v4, LX/DSe;->ADMIN:LX/DSe;

    :goto_1
    invoke-virtual {v5}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    move-result-object v6

    if-nez v6, :cond_5

    const/4 v5, 0x0

    :goto_2
    invoke-direct/range {v0 .. v5}, LX/DSf;-><init>(LX/DUV;LX/DSb;LX/DSc;LX/DSe;LX/DS2;)V

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    :cond_4
    sget-object v4, LX/DSe;->MODERATOR:LX/DSe;

    goto :goto_1

    :cond_5
    new-instance v6, LX/DS2;

    invoke-virtual {v5}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->k()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->j()J

    move-result-wide v12

    invoke-direct {v6, v9, v10, v12, v13}, LX/DS2;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    move-object v5, v6

    goto :goto_2

    .line 1998914
    :cond_6
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1998915
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998916
    if-eqz v0, :cond_a

    .line 1998917
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998918
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1998919
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998920
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1998921
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998922
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1998923
    :goto_3
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, p0, LX/DRt;->e:LX/0Px;

    .line 1998924
    if-eqz v0, :cond_8

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    :goto_4
    iput-object v2, p0, LX/DRt;->a:Ljava/lang/String;

    .line 1998925
    if-eqz v0, :cond_9

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    :goto_5
    iput-boolean v0, p0, LX/DRt;->b:Z

    .line 1998926
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998927
    if-eqz v0, :cond_7

    .line 1998928
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998929
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1998930
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1998931
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/DRt;->i:Z

    .line 1998932
    :cond_7
    invoke-direct {p0, p1}, LX/DRt;->b(Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1998933
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 1998934
    return-void

    .line 1998935
    :cond_8
    const/4 v2, 0x0

    goto :goto_4

    .line 1998936
    :cond_9
    const/4 v0, 0x1

    goto :goto_5

    :cond_a
    move v0, v1

    move-object v1, v2

    goto :goto_3
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1998832
    const/4 v0, 0x1

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1998827
    const-string v0, "Group admin members fetch failed"

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1998831
    iget-object v0, p0, LX/DRt;->e:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1998828
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1998829
    iput-object v0, p0, LX/DRt;->e:LX/0Px;

    .line 1998830
    return-void
.end method
