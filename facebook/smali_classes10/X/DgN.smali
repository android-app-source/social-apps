.class public final LX/DgN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3xK;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V
    .locals 0

    .prologue
    .line 2029347
    iput-object p1, p0, LX/DgN;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2029348
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 7

    .prologue
    .line 2029349
    iget-object v0, p0, LX/DgN;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-static {v0}, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->r(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2029350
    iget-object v0, p0, LX/DgN;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->w:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    .line 2029351
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    invoke-virtual {v1}, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->a()V

    .line 2029352
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->c:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    invoke-virtual {v1}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a()V

    .line 2029353
    invoke-static {v0}, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->d(Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;)V

    .line 2029354
    new-instance v2, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment$3;

    invoke-direct {v2, v0, p1}, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment$3;-><init>(Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;Ljava/lang/String;)V

    iput-object v2, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->e:Ljava/lang/Runnable;

    .line 2029355
    iget-object v2, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->b:Landroid/os/Handler;

    iget-object v3, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->e:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    const v6, -0x5a9b33f8

    invoke-static {v2, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2029356
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
