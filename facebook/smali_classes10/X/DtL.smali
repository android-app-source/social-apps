.class public final enum LX/DtL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DtL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DtL;

.field public static final enum MP:LX/DtL;

.field public static final enum ORION:LX/DtL;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2051929
    new-instance v0, LX/DtL;

    const-string v1, "ORION"

    invoke-direct {v0, v1, v2}, LX/DtL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DtL;->ORION:LX/DtL;

    .line 2051930
    new-instance v0, LX/DtL;

    const-string v1, "MP"

    invoke-direct {v0, v1, v3}, LX/DtL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DtL;->MP:LX/DtL;

    .line 2051931
    const/4 v0, 0x2

    new-array v0, v0, [LX/DtL;

    sget-object v1, LX/DtL;->ORION:LX/DtL;

    aput-object v1, v0, v2

    sget-object v1, LX/DtL;->MP:LX/DtL;

    aput-object v1, v0, v3

    sput-object v0, LX/DtL;->$VALUES:[LX/DtL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2051926
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DtL;
    .locals 1

    .prologue
    .line 2051927
    const-class v0, LX/DtL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DtL;

    return-object v0
.end method

.method public static values()[LX/DtL;
    .locals 1

    .prologue
    .line 2051928
    sget-object v0, LX/DtL;->$VALUES:[LX/DtL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DtL;

    return-object v0
.end method
