.class public LX/DFL;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFN;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DFL",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DFN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978074
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1978075
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DFL;->b:LX/0Zi;

    .line 1978076
    iput-object p1, p0, LX/DFL;->a:LX/0Ot;

    .line 1978077
    return-void
.end method

.method public static a(LX/0QB;)LX/DFL;
    .locals 4

    .prologue
    .line 1978063
    const-class v1, LX/DFL;

    monitor-enter v1

    .line 1978064
    :try_start_0
    sget-object v0, LX/DFL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978065
    sput-object v2, LX/DFL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978066
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978067
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978068
    new-instance v3, LX/DFL;

    const/16 p0, 0x20e1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DFL;-><init>(LX/0Ot;)V

    .line 1978069
    move-object v0, v3

    .line 1978070
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978071
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978072
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1978036
    check-cast p2, LX/DFK;

    .line 1978037
    iget-object v0, p0, LX/DFL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DFN;

    iget-object v1, p2, LX/DFK;->a:LX/1Pc;

    iget-object v2, p2, LX/DFK;->b:LX/2dy;

    .line 1978038
    new-instance v4, LX/DFM;

    invoke-direct {v4, v0, v2}, LX/DFM;-><init>(LX/DFN;LX/2dy;)V

    .line 1978039
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    iget-object v3, v2, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978040
    iget-object v6, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v6

    .line 1978041
    check-cast v3, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 1978042
    iput-object v3, v5, LX/3mP;->d:LX/25L;

    .line 1978043
    move-object v5, v5

    .line 1978044
    iget-object v3, v2, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978045
    iget-object v6, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v6

    .line 1978046
    check-cast v3, LX/0jW;

    .line 1978047
    iput-object v3, v5, LX/3mP;->e:LX/0jW;

    .line 1978048
    move-object v3, v5

    .line 1978049
    const/16 v5, 0x8

    .line 1978050
    iput v5, v3, LX/3mP;->b:I

    .line 1978051
    move-object v3, v3

    .line 1978052
    iput-object v4, v3, LX/3mP;->g:LX/25K;

    .line 1978053
    move-object v3, v3

    .line 1978054
    iget-object v4, v2, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978055
    iput-object v4, v3, LX/3mP;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978056
    move-object v3, v3

    .line 1978057
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 1978058
    iget-object v3, v0, LX/DFN;->a:LX/DFE;

    iget-object v4, v0, LX/DFN;->c:LX/0ad;

    iget-object v5, v0, LX/DFN;->d:LX/2e2;

    invoke-static {v2, v4, v5}, LX/DFD;->a(LX/2dy;LX/0ad;LX/2e2;)LX/0Px;

    move-result-object v6

    move-object v4, p1

    move-object v5, v2

    move-object v7, v1

    invoke-virtual/range {v3 .. v8}, LX/DFE;->a(Landroid/content/Context;LX/2dy;LX/0Px;Ljava/lang/Object;LX/25M;)LX/DFD;

    move-result-object v3

    .line 1978059
    iget-object v4, v0, LX/DFN;->e:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1978060
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1978061
    invoke-static {}, LX/1dS;->b()V

    .line 1978062
    const/4 v0, 0x0

    return-object v0
.end method
