.class public LX/EM2;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EM0;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EM4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2110097
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EM2;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EM4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110114
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2110115
    iput-object p1, p0, LX/EM2;->b:LX/0Ot;

    .line 2110116
    return-void
.end method

.method public static a(LX/0QB;)LX/EM2;
    .locals 4

    .prologue
    .line 2110117
    const-class v1, LX/EM2;

    monitor-enter v1

    .line 2110118
    :try_start_0
    sget-object v0, LX/EM2;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110119
    sput-object v2, LX/EM2;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110120
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110121
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110122
    new-instance v3, LX/EM2;

    const/16 p0, 0x340b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EM2;-><init>(LX/0Ot;)V

    .line 2110123
    move-object v0, v3

    .line 2110124
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110125
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EM2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110126
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110127
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2110100
    check-cast p2, LX/EM1;

    .line 2110101
    iget-object v0, p0, LX/EM2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EM4;

    iget-object v1, p2, LX/EM1;->a:LX/CzL;

    const/4 p0, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 2110102
    iget-object v2, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2110103
    check-cast v2, LX/8d0;

    .line 2110104
    invoke-interface {v2}, LX/8d0;->d()Ljava/lang/String;

    move-result-object v3

    .line 2110105
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b1733

    invoke-interface {v4, v5}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b1744

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b173d

    invoke-interface {v4, p0, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x5

    const v6, 0x7f0b173e

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b173f

    invoke-interface {v4, v7, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0e0120

    invoke-static {p1, v5, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v3, v5}, LX/1Di;->c(F)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2}, LX/8d0;->o()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2110106
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-interface {v2}, LX/8d0;->z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v2

    const/4 v6, 0x0

    .line 2110107
    sget-object v5, LX/EM3;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->ordinal()I

    move-result p2

    aget v5, v5, p2

    packed-switch v5, :pswitch_data_0

    .line 2110108
    iget-object v5, v0, LX/EM4;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Sc;

    sget-object p2, LX/3Ql;->UNKNOWN_VERIFIED_BADGE_STATUS:LX/3Ql;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string v1, "Not a valid verification status: "

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p2, p1}, LX/2Sc;->b(LX/3Ql;Ljava/lang/String;)V

    move v5, v6

    .line 2110109
    :goto_1
    move v2, v5

    .line 2110110
    invoke-virtual {v4, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v4, 0x7f0b1741

    invoke-interface {v2, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const v4, 0x7f0b1741

    invoke-interface {v2, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v8}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    const v4, 0x7f0b1742

    invoke-interface {v2, p0, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v4, 0x7f0b1743

    invoke-interface {v2, v7, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    goto :goto_0

    .line 2110111
    :pswitch_0
    const v5, 0x7f021a23

    goto :goto_1

    .line 2110112
    :pswitch_1
    const v5, 0x7f021a22

    goto :goto_1

    :pswitch_2
    move v5, v6

    .line 2110113
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2110098
    invoke-static {}, LX/1dS;->b()V

    .line 2110099
    const/4 v0, 0x0

    return-object v0
.end method
