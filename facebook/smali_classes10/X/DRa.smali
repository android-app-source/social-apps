.class public final enum LX/DRa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DRa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DRa;

.field public static final enum DISCUSSION:LX/DRa;

.field public static final enum LEARNING_UNITS:LX/DRa;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1998363
    new-instance v0, LX/DRa;

    const-string v1, "LEARNING_UNITS"

    invoke-direct {v0, v1, v2}, LX/DRa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DRa;->LEARNING_UNITS:LX/DRa;

    .line 1998364
    new-instance v0, LX/DRa;

    const-string v1, "DISCUSSION"

    invoke-direct {v0, v1, v3}, LX/DRa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DRa;->DISCUSSION:LX/DRa;

    .line 1998365
    const/4 v0, 0x2

    new-array v0, v0, [LX/DRa;

    sget-object v1, LX/DRa;->LEARNING_UNITS:LX/DRa;

    aput-object v1, v0, v2

    sget-object v1, LX/DRa;->DISCUSSION:LX/DRa;

    aput-object v1, v0, v3

    sput-object v0, LX/DRa;->$VALUES:[LX/DRa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1998366
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DRa;
    .locals 1

    .prologue
    .line 1998367
    const-class v0, LX/DRa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DRa;

    return-object v0
.end method

.method public static values()[LX/DRa;
    .locals 1

    .prologue
    .line 1998368
    sget-object v0, LX/DRa;->$VALUES:[LX/DRa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DRa;

    return-object v0
.end method
