.class public final LX/Doq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0SG;


# static fields
.field public static final a:LX/Doq;


# instance fields
.field private b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042561
    new-instance v0, LX/Doq;

    invoke-direct {v0}, LX/Doq;-><init>()V

    sput-object v0, LX/Doq;->a:LX/Doq;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2042550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042551
    return-void
.end method

.method public static b()LX/Doq;
    .locals 1

    .prologue
    .line 2042560
    sget-object v0, LX/Doq;->a:LX/Doq;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()J
    .locals 4

    .prologue
    .line 2042555
    monitor-enter p0

    .line 2042556
    :try_start_0
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 2042557
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    .line 2042558
    iget-wide v2, p0, LX/Doq;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-wide v0, p0, LX/Doq;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/Doq;->b:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2042559
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 2042552
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/Doq;->b:J

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/Doq;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2042553
    monitor-exit p0

    return-void

    .line 2042554
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
