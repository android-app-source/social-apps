.class public LX/EeE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/growth/contactimporter/UsersInviteParams;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/api/growth/contactimporter/UsersInviteResult;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:LX/266;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/266",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/api/growth/contactimporter/UsersInviteResult;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2152199
    new-instance v0, LX/EeD;

    invoke-direct {v0}, LX/EeD;-><init>()V

    sput-object v0, LX/EeE;->a:LX/266;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2152200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152201
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2152202
    check-cast p1, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;

    .line 2152203
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2152204
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2152205
    iget-object v0, p1, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->a:Ljava/util/List;

    move-object v1, v0

    .line 2152206
    const/4 v0, 0x1

    .line 2152207
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2152208
    if-eqz v1, :cond_0

    .line 2152209
    const/4 v1, 0x0

    .line 2152210
    :goto_1
    const-string v5, ","

    const-string v6, ""

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2152211
    :cond_0
    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2152212
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "emails"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152213
    iget-object v0, p1, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2152214
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2152215
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "country_code"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152216
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "ci_how_found"

    .line 2152217
    iget-object v2, p1, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2152218
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152219
    iget-object v0, p1, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->d:LX/89v;

    move-object v0, v0

    .line 2152220
    if-eqz v0, :cond_3

    sget-object v1, LX/89v;->UNKNOWN:LX/89v;

    invoke-virtual {v0, v1}, LX/89v;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2152221
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "ci_flow"

    iget-object v0, v0, LX/89v;->value:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152222
    :cond_3
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "is_invite_all"

    .line 2152223
    iget-boolean v2, p1, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->e:Z

    move v2, v2

    .line 2152224
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152225
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "is_invite_all_only"

    .line 2152226
    iget-boolean v2, p1, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->f:Z

    move v2, v2

    .line 2152227
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152228
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152229
    new-instance v0, LX/14N;

    const-string v1, "UsersInvite"

    const-string v2, "POST"

    const-string v3, "method/users.invite"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2152230
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 2152231
    sget-object v1, LX/EeE;->a:LX/266;

    invoke-virtual {v0, v1}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method
