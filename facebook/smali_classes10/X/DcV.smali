.class public final LX/DcV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

.field public final synthetic b:Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

.field public final synthetic c:LX/DcX;

.field public final synthetic d:LX/DcW;


# direct methods
.method public constructor <init>(LX/DcW;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;LX/DcX;)V
    .locals 0

    .prologue
    .line 2018496
    iput-object p1, p0, LX/DcV;->d:LX/DcW;

    iput-object p2, p0, LX/DcV;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iput-object p3, p0, LX/DcV;->b:Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    iput-object p4, p0, LX/DcV;->c:LX/DcX;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2018497
    iget-object v0, p0, LX/DcV;->d:LX/DcW;

    iget-object v1, p0, LX/DcV;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iget-object v2, p0, LX/DcV;->b:Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    iget-object v3, p0, LX/DcV;->c:LX/DcX;

    .line 2018498
    invoke-static {v2}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->a(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/DcX;->a(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)V

    .line 2018499
    invoke-static {v0, v1, v2, v3}, LX/DcW;->a(LX/DcW;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;LX/DcX;)V

    .line 2018500
    iget-object v5, v0, LX/DcW;->i:LX/0kL;

    new-instance p0, LX/27k;

    invoke-virtual {v2}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getContext()Landroid/content/Context;

    move-result-object v4

    const p1, 0x7f0828d6

    invoke-virtual {v4, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-direct {p0, v4}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v5, p0}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2018501
    return-void

    .line 2018502
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getContext()Landroid/content/Context;

    move-result-object v4

    const p1, 0x7f0828d5

    invoke-virtual {v4, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2018503
    return-void
.end method
