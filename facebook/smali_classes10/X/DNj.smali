.class public final LX/DNj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0pn;

.field public final synthetic b:Lcom/facebook/api/feedtype/FeedType;

.field public final synthetic c:LX/DNp;


# direct methods
.method public constructor <init>(LX/DNp;LX/0pn;Lcom/facebook/api/feedtype/FeedType;)V
    .locals 0

    .prologue
    .line 1991921
    iput-object p1, p0, LX/DNj;->c:LX/DNp;

    iput-object p2, p0, LX/DNj;->a:LX/0pn;

    iput-object p3, p0, LX/DNj;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1991922
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1991923
    iget-object v0, p0, LX/DNj;->a:LX/0pn;

    iget-object v1, p0, LX/DNj;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0, v1}, LX/0pn;->b(Lcom/facebook/api/feedtype/FeedType;)LX/0Px;

    move-result-object v3

    .line 1991924
    iget-object v0, p0, LX/DNj;->c:LX/DNp;

    iget-object v1, p0, LX/DNj;->a:LX/0pn;

    iget-object v4, p0, LX/DNj;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-static {v0, v1, v4, v3}, LX/DNp;->a(LX/DNp;LX/0pn;Lcom/facebook/api/feedtype/FeedType;LX/0Px;)LX/0Px;

    move-result-object v4

    .line 1991925
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    .line 1991926
    iget-object v6, v0, LX/44w;->a:Ljava/lang/Object;

    invoke-virtual {v4, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1991927
    iget-object v0, v0, LX/44w;->a:Ljava/lang/Object;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1991928
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1991929
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
