.class public LX/DOK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/DOK;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1992265
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/DOK;->a:Ljava/util/Set;

    .line 1992266
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/DOK;->b:Ljava/util/Set;

    .line 1992267
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/DOK;->c:Ljava/util/Map;

    .line 1992268
    return-void
.end method

.method public static a(LX/0QB;)LX/DOK;
    .locals 3

    .prologue
    .line 1992225
    sget-object v0, LX/DOK;->e:LX/DOK;

    if-nez v0, :cond_1

    .line 1992226
    const-class v1, LX/DOK;

    monitor-enter v1

    .line 1992227
    :try_start_0
    sget-object v0, LX/DOK;->e:LX/DOK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1992228
    if-eqz v2, :cond_0

    .line 1992229
    :try_start_1
    new-instance v0, LX/DOK;

    invoke-direct {v0}, LX/DOK;-><init>()V

    .line 1992230
    move-object v0, v0

    .line 1992231
    sput-object v0, LX/DOK;->e:LX/DOK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1992232
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1992233
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1992234
    :cond_1
    sget-object v0, LX/DOK;->e:LX/DOK;

    return-object v0

    .line 1992235
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1992236
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;LX/9Mz;)V
    .locals 2

    .prologue
    .line 1992237
    monitor-enter p0

    if-nez p1, :cond_1

    .line 1992238
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1992239
    :cond_1
    if-eqz p2, :cond_2

    :try_start_0
    invoke-interface {p2}, LX/9Mz;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v0, v1, :cond_4

    .line 1992240
    :cond_2
    iget-object v0, p0, LX/DOK;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1992241
    :goto_1
    if-eqz p2, :cond_5

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-interface {p2}, LX/9Mz;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v1

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-interface {p2}, LX/9Mz;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v1

    if-ne v0, v1, :cond_5

    .line 1992242
    :cond_3
    iget-object v0, p0, LX/DOK;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1992243
    :goto_2
    if-eqz p2, :cond_0

    .line 1992244
    iget-object v0, p0, LX/DOK;->c:Ljava/util/Map;

    invoke-interface {p2}, LX/9Mz;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1992245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1992246
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/DOK;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1992247
    :cond_5
    iget-object v0, p0, LX/DOK;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)V
    .locals 1

    .prologue
    .line 1992248
    monitor-enter p0

    if-nez p1, :cond_0

    .line 1992249
    :goto_0
    monitor-exit p0

    return-void

    .line 1992250
    :cond_0
    :try_start_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v0, p2, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v0, p2, :cond_2

    .line 1992251
    :cond_1
    iget-object v0, p0, LX/DOK;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1992252
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1992253
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/DOK;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 1992254
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/DOK;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1992255
    monitor-exit p0

    return-void

    .line 1992256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 1992257
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/DOK;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1992258
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/DOK;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1992259
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/DOK;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1992260
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/DOK;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1992261
    iget-object v0, p0, LX/DOK;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1992262
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1992263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
