.class public LX/Cqf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Ya;",
        "Lcom/facebook/richdocument/view/transition/ViewLayoutStrategy",
        "<",
        "LX/Ctg;",
        "LX/Cqw;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field public final a:LX/Cqt;

.field public final b:LX/Cqd;

.field public c:LX/Cqe;

.field public d:LX/CkJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final h:LX/Cqw;

.field public final i:LX/Ctg;

.field public final j:LX/Cqc;

.field private final k:Ljava/lang/Float;

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/CnQ;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/CrS;

.field private final n:Z

.field public o:Landroid/graphics/Rect;

.field private p:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1940217
    const-class v0, LX/Cqf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Cqf;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V
    .locals 8

    .prologue
    .line 1940213
    sget-object v7, LX/Cqt;->PORTRAIT:LX/Cqt;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;LX/Cqt;)V

    .line 1940214
    return-void
.end method

.method public constructor <init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;LX/Cqt;)V
    .locals 3

    .prologue
    .line 1940218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1940219
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Cqf;->l:Ljava/util/Map;

    .line 1940220
    iput-object p1, p0, LX/Cqf;->h:LX/Cqw;

    .line 1940221
    iput-object p2, p0, LX/Cqf;->i:LX/Ctg;

    .line 1940222
    iput-object p3, p0, LX/Cqf;->b:LX/Cqd;

    .line 1940223
    iput-object p4, p0, LX/Cqf;->c:LX/Cqe;

    .line 1940224
    iput-object p5, p0, LX/Cqf;->j:LX/Cqc;

    .line 1940225
    iput-object p6, p0, LX/Cqf;->k:Ljava/lang/Float;

    .line 1940226
    iput-object p7, p0, LX/Cqf;->a:LX/Cqt;

    .line 1940227
    new-instance v0, LX/CrT;

    invoke-direct {v0, p1}, LX/CrT;-><init>(LX/Cqv;)V

    iput-object v0, p0, LX/Cqf;->m:LX/CrS;

    .line 1940228
    const-class v0, LX/Cqf;

    invoke-interface {p2}, LX/Ctg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/Cqf;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 1940229
    iget-object v0, p0, LX/Cqf;->f:LX/0Uh;

    const/16 v1, 0xa6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Cqf;->n:Z

    .line 1940230
    return-void
.end method

.method public static a(LX/Cqf;IILX/CnQ;)I
    .locals 7

    .prologue
    .line 1940231
    invoke-interface {p3}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1940232
    invoke-static {p0, p3}, LX/Cqf;->a(LX/Cqf;LX/CnQ;)I

    move-result v2

    .line 1940233
    invoke-interface {p3}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 1940234
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, p1

    .line 1940235
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 1940236
    invoke-interface {p3}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v5

    .line 1940237
    iget-object v6, v5, LX/ClU;->d:LX/ClQ;

    move-object v5, v6

    .line 1940238
    sget-object v6, LX/Cqb;->a:[I

    invoke-virtual {v5}, LX/ClQ;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    move v0, v1

    .line 1940239
    :goto_0
    new-instance v1, Landroid/graphics/Rect;

    add-int/2addr v3, v0

    add-int v4, p2, v2

    invoke-direct {v1, v0, p2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1940240
    invoke-interface {p3}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v0

    new-instance v3, LX/CrW;

    invoke-direct {v3, v1}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v3}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940241
    return v2

    .line 1940242
    :pswitch_0
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v4

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v0, v1, v0

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 1940243
    :pswitch_1
    sub-int v1, v4, v3

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v0, v1, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/Cqf;LX/CnQ;)I
    .locals 1

    .prologue
    .line 1940244
    iget-object v0, p0, LX/Cqf;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1940245
    iget-object v0, p0, LX/Cqf;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1940246
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static varargs a(LX/Cqf;LX/Cs7;IIZZZ[LX/ClR;)I
    .locals 6

    .prologue
    .line 1940247
    invoke-virtual {p1, p7}, LX/Cs7;->a([LX/ClR;)Ljava/util/List;

    move-result-object v5

    .line 1940248
    if-eqz p6, :cond_0

    .line 1940249
    invoke-direct {p0, v5}, LX/Cqf;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    :cond_0
    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    .line 1940250
    const/4 p4, 0x0

    const/4 p1, -0x1

    const/4 p2, 0x0

    .line 1940251
    invoke-static {v5}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1940252
    :cond_1
    move v0, p2

    .line 1940253
    return v0

    .line 1940254
    :cond_2
    if-eqz v3, :cond_5

    move p0, p1

    .line 1940255
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result p7

    move p6, p0

    :goto_1
    if-ge p6, p7, :cond_1

    .line 1940256
    if-ne p6, p1, :cond_6

    move-object p5, p4

    .line 1940257
    :goto_2
    add-int/lit8 p0, p7, -0x1

    if-ne p6, p0, :cond_7

    move-object p3, p4

    .line 1940258
    :goto_3
    if-eqz p5, :cond_8

    .line 1940259
    add-int p0, v2, p2

    invoke-static {v0, v1, p0, p5}, LX/Cqf;->a(LX/Cqf;IILX/CnQ;)I

    move-result p0

    .line 1940260
    add-int/2addr p0, p2

    .line 1940261
    :goto_4
    if-nez p3, :cond_3

    if-eqz v4, :cond_4

    .line 1940262
    :cond_3
    iget-object p2, v0, LX/Cqf;->d:LX/CkJ;

    invoke-virtual {p2, p5, p3}, LX/CkJ;->a(LX/CnQ;LX/CnQ;)I

    move-result p2

    add-int/2addr p0, p2

    .line 1940263
    :cond_4
    add-int/lit8 p2, p6, 0x1

    move p6, p2

    move p2, p0

    goto :goto_1

    :cond_5
    move p0, p2

    .line 1940264
    goto :goto_0

    .line 1940265
    :cond_6
    invoke-interface {v5, p6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CnQ;

    move-object p5, p0

    goto :goto_2

    .line 1940266
    :cond_7
    add-int/lit8 p0, p6, 0x1

    invoke-interface {v5, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CnQ;

    move-object p3, p0

    goto :goto_3

    :cond_8
    move p0, p2

    goto :goto_4
.end method

.method private varargs a(LX/Cs7;ZZZ[LX/ClR;)I
    .locals 6

    .prologue
    .line 1940275
    invoke-virtual {p1, p5}, LX/Cs7;->a([LX/ClR;)Ljava/util/List;

    move-result-object v0

    .line 1940276
    if-eqz p4, :cond_0

    .line 1940277
    invoke-direct {p0, v0}, LX/Cqf;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1940278
    :cond_0
    const/4 v5, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 1940279
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1940280
    :cond_1
    move v0, v3

    .line 1940281
    return v0

    .line 1940282
    :cond_2
    if-eqz p2, :cond_5

    move v1, v2

    .line 1940283
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p5

    move p4, v1

    :goto_1
    if-ge p4, p5, :cond_1

    .line 1940284
    if-ne p4, v2, :cond_6

    move-object p1, v5

    .line 1940285
    :goto_2
    add-int/lit8 v1, p5, -0x1

    if-ne p4, v1, :cond_7

    move-object v4, v5

    .line 1940286
    :goto_3
    if-eqz p1, :cond_8

    .line 1940287
    invoke-static {p0, p1}, LX/Cqf;->a(LX/Cqf;LX/CnQ;)I

    move-result v1

    add-int/2addr v1, v3

    .line 1940288
    :goto_4
    if-nez v4, :cond_3

    if-eqz p3, :cond_4

    .line 1940289
    :cond_3
    iget-object v3, p0, LX/Cqf;->d:LX/CkJ;

    invoke-virtual {v3, p1, v4}, LX/CkJ;->a(LX/CnQ;LX/CnQ;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1940290
    :cond_4
    add-int/lit8 v3, p4, 0x1

    move p4, v3

    move v3, v1

    goto :goto_1

    :cond_5
    move v1, v3

    .line 1940291
    goto :goto_0

    .line 1940292
    :cond_6
    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CnQ;

    move-object p1, v1

    goto :goto_2

    .line 1940293
    :cond_7
    add-int/lit8 v1, p4, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CnQ;

    move-object v4, v1

    goto :goto_3

    :cond_8
    move v1, v3

    goto :goto_4
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/CnQ;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/CnQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1940267
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1940268
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1940269
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v3

    .line 1940270
    iget-object v4, p0, LX/Cqf;->m:LX/CrS;

    sget-object v5, LX/CrQ;->OPACITY:LX/CrQ;

    const-class p1, LX/CrV;

    invoke-interface {v4, v3, v5, p1}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v4

    check-cast v4, LX/CrV;

    .line 1940271
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/CrV;->b()Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    :cond_1
    const/4 v4, 0x1

    :goto_1
    move v3, v4

    .line 1940272
    if-eqz v3, :cond_0

    .line 1940273
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1940274
    :cond_2
    return-object v1

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static a(LX/Cqf;LX/Cs7;I)V
    .locals 5

    .prologue
    .line 1940303
    invoke-virtual {p1}, LX/Cs7;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1940304
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v2

    .line 1940305
    iget-object v3, p0, LX/Cqf;->i:LX/Ctg;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {p2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 p1, 0x0

    invoke-interface {v3, v2, v4, p1}, LX/Ctg;->a(Landroid/view/View;II)V

    .line 1940306
    iget-object v2, p0, LX/Cqf;->l:Ljava/util/Map;

    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1940307
    :cond_0
    return-void
.end method

.method public static a(LX/Cqf;Z)V
    .locals 9

    .prologue
    .line 1940308
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v8

    .line 1940309
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v1

    .line 1940310
    invoke-virtual {v8}, LX/CrW;->g()I

    move-result v3

    .line 1940311
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x2

    new-array v7, v0, [LX/ClR;

    const/4 v0, 0x0

    sget-object v6, LX/ClR;->ABOVE:LX/ClR;

    aput-object v6, v7, v0

    const/4 v0, 0x1

    sget-object v6, LX/ClR;->TOP:LX/ClR;

    aput-object v6, v7, v0

    move-object v0, p0

    move v6, p1

    invoke-static/range {v0 .. v7}, LX/Cqf;->a(LX/Cqf;LX/Cs7;IIZZZ[LX/ClR;)I

    .line 1940312
    invoke-virtual {v8}, LX/CrW;->f()I

    move-result v0

    div-int/lit8 v6, v0, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x1

    new-array v5, v0, [LX/ClR;

    const/4 v0, 0x0

    sget-object v4, LX/ClR;->CENTER:LX/ClR;

    aput-object v4, v5, v0

    move-object v0, p0

    move v4, p1

    invoke-direct/range {v0 .. v5}, LX/Cqf;->a(LX/Cs7;ZZZ[LX/ClR;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v3, v6, v0

    .line 1940313
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x1

    new-array v7, v0, [LX/ClR;

    const/4 v0, 0x0

    sget-object v6, LX/ClR;->CENTER:LX/ClR;

    aput-object v6, v7, v0

    move-object v0, p0

    move v6, p1

    invoke-static/range {v0 .. v7}, LX/Cqf;->a(LX/Cqf;LX/Cs7;IIZZZ[LX/ClR;)I

    .line 1940314
    invoke-virtual {v8}, LX/CrW;->h()I

    move-result v6

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x2

    new-array v5, v0, [LX/ClR;

    const/4 v0, 0x0

    sget-object v4, LX/ClR;->BOTTOM:LX/ClR;

    aput-object v4, v5, v0

    const/4 v0, 0x1

    sget-object v4, LX/ClR;->BELOW:LX/ClR;

    aput-object v4, v5, v0

    move-object v0, p0

    move v4, p1

    invoke-direct/range {v0 .. v5}, LX/Cqf;->a(LX/Cs7;ZZZ[LX/ClR;)I

    move-result v0

    sub-int v3, v6, v0

    .line 1940315
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x2

    new-array v7, v0, [LX/ClR;

    const/4 v0, 0x0

    sget-object v6, LX/ClR;->BOTTOM:LX/ClR;

    aput-object v6, v7, v0

    const/4 v0, 0x1

    sget-object v6, LX/ClR;->BELOW:LX/ClR;

    aput-object v6, v7, v0

    move-object v0, p0

    move v6, p1

    invoke-static/range {v0 .. v7}, LX/Cqf;->a(LX/Cqf;LX/Cs7;IIZZZ[LX/ClR;)I

    .line 1940316
    return-void
.end method

.method public static a(LX/CrW;Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 1940294
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 1940295
    iget-object v1, p0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1940296
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 1940297
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 1940298
    iget-object v2, p0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v2, v2

    .line 1940299
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1940300
    invoke-virtual {p0, v0}, LX/CrW;->b(I)V

    .line 1940301
    invoke-virtual {p0, v1}, LX/CrW;->a(I)V

    .line 1940302
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cqf;

    invoke-static {p0}, LX/CkJ;->a(LX/0QB;)LX/CkJ;

    move-result-object v0

    check-cast v0, LX/CkJ;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v0, p1, LX/Cqf;->d:LX/CkJ;

    iput-object v1, p1, LX/Cqf;->e:LX/03V;

    iput-object p0, p1, LX/Cqf;->f:LX/0Uh;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)LX/CrW;
    .locals 3

    .prologue
    .line 1940216
    iget-object v0, p0, LX/Cqf;->m:LX/CrS;

    sget-object v1, LX/CrQ;->RECT:LX/CrQ;

    const-class v2, LX/CrW;

    invoke-interface {v0, p1, v1, v2}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v0

    check-cast v0, LX/CrW;

    return-object v0
.end method

.method public final a(Landroid/view/View;LX/CqY;)V
    .locals 2

    .prologue
    .line 1940100
    iget-object v0, p0, LX/Cqf;->m:LX/CrS;

    invoke-interface {v0, p1}, LX/CrS;->a(Landroid/view/View;)LX/CrR;

    move-result-object v0

    .line 1940101
    if-nez v0, :cond_0

    .line 1940102
    new-instance v0, LX/CrR;

    invoke-direct {v0}, LX/CrR;-><init>()V

    .line 1940103
    iget-object v1, p0, LX/Cqf;->m:LX/CrS;

    invoke-interface {v1, p1, v0}, LX/CrS;->a(Landroid/view/View;LX/CrR;)V

    .line 1940104
    :cond_0
    invoke-virtual {v0, p2}, LX/CrR;->a(LX/CqY;)V

    .line 1940105
    return-void
.end method

.method public final e()Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1940106
    iget-object v0, p0, LX/Cqf;->p:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    .line 1940107
    iget-object v0, p0, LX/Cqf;->a:LX/Cqt;

    sget-object v1, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/Cqf;->a:LX/Cqt;

    sget-object v1, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    if-ne v0, v1, :cond_2

    .line 1940108
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v2, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/Cqf;->p:Landroid/graphics/Rect;

    .line 1940109
    :cond_1
    :goto_0
    iget-object v0, p0, LX/Cqf;->p:Landroid/graphics/Rect;

    return-object v0

    .line 1940110
    :cond_2
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, LX/Cqf;->p:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public final f()LX/CrS;
    .locals 15

    .prologue
    .line 1940111
    invoke-virtual {p0}, LX/Cqf;->g()V

    .line 1940112
    invoke-virtual {p0}, LX/Cqf;->h()V

    .line 1940113
    invoke-virtual {p0}, LX/Cqf;->i()V

    .line 1940114
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {p0, v0, v1}, LX/Cqf;->a(LX/Cqf;LX/Cs7;I)V

    .line 1940115
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/Cqf;->a(LX/Cqf;Z)V

    .line 1940116
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/Cqf;->a(LX/Cqf;Z)V

    .line 1940117
    iget-object v0, p0, LX/Cqf;->c:LX/Cqe;

    sget-object v1, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    if-ne v0, v1, :cond_2

    .line 1940118
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1940119
    iget-object v1, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v0, v1

    .line 1940120
    :goto_0
    iget-object v1, p0, LX/Cqf;->i:LX/Ctg;

    invoke-interface {v1}, LX/Ctg;->getOverlayView()Landroid/view/View;

    move-result-object v1

    new-instance v2, LX/CrW;

    invoke-direct {v2, v0}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v1, v2}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940121
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1940122
    iget-object v1, p0, LX/Cqf;->j:LX/Cqc;

    sget-object v2, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    if-ne v1, v2, :cond_0

    .line 1940123
    iget-object v1, p0, LX/Cqf;->i:LX/Ctg;

    invoke-interface {v1}, LX/Cq9;->getAnnotationViews()LX/Cs7;

    move-result-object v2

    .line 1940124
    iget-object v1, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1940125
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {p0, v2, v1}, LX/Cqf;->a(LX/Cqf;LX/Cs7;I)V

    .line 1940126
    new-array v8, v6, [LX/ClR;

    sget-object v1, LX/ClR;->ABOVE:LX/ClR;

    aput-object v1, v8, v3

    move-object v1, p0

    move v4, v3

    move v5, v3

    move v7, v3

    invoke-static/range {v1 .. v8}, LX/Cqf;->a(LX/Cqf;LX/Cs7;IIZZZ[LX/ClR;)I

    move-result v1

    .line 1940127
    iget-object v4, p0, LX/Cqf;->i:LX/Ctg;

    invoke-interface {v4}, LX/Ctg;->getOverlayView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v4

    .line 1940128
    invoke-virtual {v4}, LX/CrW;->g()I

    move-result v5

    add-int/2addr v1, v5

    invoke-virtual {v4, v1}, LX/CrW;->a(I)V

    .line 1940129
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 1940130
    iget-object v5, v4, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v5, v5

    .line 1940131
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v7

    .line 1940132
    iget-object v8, v1, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v8, v8

    .line 1940133
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    .line 1940134
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v8

    .line 1940135
    iget-object v9, v1, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v9, v9

    .line 1940136
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    .line 1940137
    iget v9, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v7, v9

    invoke-virtual {v1, v7}, LX/CrW;->b(I)V

    .line 1940138
    iget v7, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v8

    invoke-virtual {v1, v7}, LX/CrW;->a(I)V

    .line 1940139
    invoke-virtual {v4}, LX/CrW;->h()I

    move-result v10

    .line 1940140
    new-array v14, v6, [LX/ClR;

    sget-object v1, LX/ClR;->BELOW:LX/ClR;

    aput-object v1, v14, v3

    move-object v7, p0

    move-object v8, v2

    move v9, v3

    move v11, v6

    move v12, v3

    move v13, v3

    invoke-static/range {v7 .. v14}, LX/Cqf;->a(LX/Cqf;LX/Cs7;IIZZZ[LX/ClR;)I

    .line 1940141
    :cond_0
    invoke-virtual {p0}, LX/Cqf;->j()LX/CrW;

    move-result-object v0

    .line 1940142
    iget-object v1, p0, LX/Cqf;->j:LX/Cqc;

    sget-object v2, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    if-ne v1, v2, :cond_3

    .line 1940143
    iget-object v1, p0, LX/Cqf;->b:LX/Cqd;

    sget-object v2, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, LX/Cqf;->c:LX/Cqe;

    sget-object v2, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    if-eq v1, v2, :cond_3

    .line 1940144
    :cond_1
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 1940145
    iget-object v2, v1, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v1, v2

    .line 1940146
    new-instance v2, LX/CrW;

    invoke-direct {v2, v1}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    .line 1940147
    iget-object v3, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v3, v3

    .line 1940148
    invoke-static {v2, v3}, LX/Cqf;->a(LX/CrW;Landroid/graphics/Rect;)V

    .line 1940149
    iget-object v3, v2, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v3, v3

    .line 1940150
    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    invoke-virtual {v0, v3}, LX/CrW;->b(I)V

    .line 1940151
    iget-object v3, v2, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v2, v3

    .line 1940152
    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    invoke-virtual {v0, v1}, LX/CrW;->a(I)V

    .line 1940153
    :goto_1
    iget-object v1, p0, LX/Cqf;->i:LX/Ctg;

    invoke-interface {v1}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940154
    iget-object v0, p0, LX/Cqf;->m:LX/CrS;

    return-object v0

    .line 1940155
    :cond_2
    iget-object v0, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1940156
    goto/16 :goto_0

    .line 1940157
    :cond_3
    iget-object v1, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1940158
    invoke-static {v0, v1}, LX/Cqf;->a(LX/CrW;Landroid/graphics/Rect;)V

    goto :goto_1
.end method

.method public g()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 1940159
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 1940160
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1940161
    invoke-virtual {p0}, LX/Cqf;->k()F

    move-result v1

    .line 1940162
    invoke-virtual {p0}, LX/Cqf;->l()F

    move-result v4

    .line 1940163
    iget-object v5, p0, LX/Cqf;->b:LX/Cqd;

    sget-object v6, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    if-ne v5, v6, :cond_1

    .line 1940164
    cmpl-float v1, v4, v1

    if-ltz v1, :cond_0

    .line 1940165
    int-to-float v0, v2

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v1, v2

    move v2, v3

    .line 1940166
    :goto_0
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v1, v2

    add-int/2addr v0, v3

    invoke-direct {v4, v2, v3, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1940167
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/CrW;

    invoke-direct {v1, v4}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940168
    return-void

    .line 1940169
    :cond_0
    int-to-float v1, v0

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1940170
    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    goto :goto_0

    .line 1940171
    :cond_1
    iget-object v5, p0, LX/Cqf;->b:LX/Cqd;

    sget-object v6, LX/Cqd;->MEDIA_MATCH_FRAME:LX/Cqd;

    if-ne v5, v6, :cond_2

    move v1, v2

    move v2, v3

    .line 1940172
    goto :goto_0

    .line 1940173
    :cond_2
    sub-float v5, v4, v1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-double v6, v5

    sget-wide v8, LX/CoL;->m:D

    cmpg-double v5, v6, v8

    if-gtz v5, :cond_3

    cmpg-float v1, v4, v1

    if-gez v1, :cond_3

    .line 1940174
    int-to-float v1, v2

    div-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1940175
    sub-int v4, v0, v1

    div-int/lit8 v4, v4, 0x2

    .line 1940176
    if-le v1, v0, :cond_4

    iget-boolean v0, p0, LX/Cqf;->n:Z

    if-eqz v0, :cond_4

    .line 1940177
    sget-object v0, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    iput-object v0, p0, LX/Cqf;->c:LX/Cqe;

    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    goto :goto_0

    .line 1940178
    :cond_3
    int-to-float v1, v0

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1940179
    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    goto :goto_0

    :cond_4
    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    goto :goto_0
.end method

.method public h()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1940180
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 1940181
    iget-object v0, p0, LX/Cqf;->c:LX/Cqe;

    sget-object v2, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    if-ne v0, v2, :cond_0

    .line 1940182
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1}, LX/CrW;->f()I

    move-result v3

    invoke-direct {v0, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1940183
    :goto_0
    invoke-static {v1, v0}, LX/Cqf;->a(LX/CrW;Landroid/graphics/Rect;)V

    .line 1940184
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v1

    new-instance v2, LX/CrW;

    invoke-direct {v2, v0}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v1, v2}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940185
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    new-instance v1, LX/CrP;

    iget-object v2, p0, LX/Cqf;->a:LX/Cqt;

    invoke-virtual {v2}, LX/Cqt;->getDegree()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v2}, LX/CrP;-><init>(F)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940186
    return-void

    .line 1940187
    :cond_0
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method public i()V
    .locals 5

    .prologue
    .line 1940188
    iget-object v0, p0, LX/Cqf;->k:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 1940189
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    invoke-virtual {v0}, LX/Cs7;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1940190
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v2

    new-instance v3, LX/CrV;

    iget-object v4, p0, LX/Cqf;->k:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-direct {v3, v4}, LX/CrV;-><init>(F)V

    invoke-virtual {p0, v2, v3}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940191
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v0

    new-instance v2, LX/CqZ;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, LX/CqZ;-><init>(Z)V

    invoke-virtual {p0, v0, v2}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    goto :goto_0

    .line 1940192
    :cond_0
    return-void
.end method

.method public j()LX/CrW;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1940193
    new-instance v1, Landroid/graphics/Rect;

    iget-object v0, p0, LX/Cqf;->i:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getOverlayView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1940194
    iget-object v2, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v0, v2

    .line 1940195
    invoke-direct {v1, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1940196
    iput v5, v1, Landroid/graphics/Rect;->left:I

    .line 1940197
    iget-object v0, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1940198
    iget-object v0, p0, LX/Cqf;->i:LX/Ctg;

    invoke-interface {v0}, LX/Cq9;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    invoke-virtual {v0}, LX/Cs7;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1940199
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1940200
    iget-object v3, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v0, v3

    .line 1940201
    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v1, Landroid/graphics/Rect;->top:I

    .line 1940202
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 1940203
    :cond_0
    new-instance v0, LX/CrW;

    invoke-direct {v0, v1}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    .line 1940204
    invoke-virtual {v0, v5}, LX/CrW;->a(I)V

    .line 1940205
    return-object v0
.end method

.method public final k()F
    .locals 5

    .prologue
    .line 1940206
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    .line 1940207
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-gtz v1, :cond_0

    .line 1940208
    iget-object v1, p0, LX/Cqf;->e:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/Cqf;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".getBodyViewportAspectRatio"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bodyViewport\'s height is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 1940209
    const v0, 0x3fe38e39

    .line 1940210
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0
.end method

.method public final l()F
    .locals 1

    .prologue
    .line 1940211
    iget-object v0, p0, LX/Cqf;->i:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getMediaAspectRatio()F

    move-result v0

    return v0
.end method

.method public final m()Landroid/view/View;
    .locals 1

    .prologue
    .line 1940212
    iget-object v0, p0, LX/Cqf;->i:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;
    .locals 1

    .prologue
    .line 1940215
    iget-object v0, p0, LX/Cqf;->i:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    return-object v0
.end method
