.class public final LX/D3h;
.super LX/1cr;
.source ""


# instance fields
.field public final synthetic b:LX/D3i;


# direct methods
.method public constructor <init>(LX/D3i;LX/D3i;)V
    .locals 0

    .prologue
    .line 1960226
    iput-object p1, p0, LX/D3h;->b:LX/D3i;

    .line 1960227
    invoke-direct {p0, p2}, LX/1cr;-><init>(Landroid/view/View;)V

    .line 1960228
    return-void
.end method

.method private b(I)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 1960222
    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget v0, v0, LX/D3i;->m:I

    iget-object v1, p0, LX/D3h;->b:LX/D3i;

    iget v1, v1, LX/D3i;->v:I

    div-int/2addr v0, v1

    mul-int/2addr v0, p1

    .line 1960223
    iget-object v1, p0, LX/D3h;->b:LX/D3i;

    iget v1, v1, LX/D3i;->m:I

    iget-object v2, p0, LX/D3h;->b:LX/D3i;

    iget v2, v2, LX/D3i;->v:I

    div-int/2addr v1, v2

    add-int/2addr v1, v0

    .line 1960224
    iget-object v2, p0, LX/D3h;->b:LX/D3i;

    iget v2, v2, LX/D3i;->n:I

    .line 1960225
    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v3
.end method


# virtual methods
.method public final a(FF)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1960200
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    invoke-virtual {v0}, LX/D3i;->getHeight()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    .line 1960201
    :cond_0
    const/high16 v0, -0x80000000

    .line 1960202
    :goto_0
    return v0

    .line 1960203
    :cond_1
    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget-object v0, v0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->h(Lcom/facebook/uicontrib/calendar/CalendarView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1960204
    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget-object v0, v0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget v0, v0, LX/D3i;->m:I

    iget-object v2, p0, LX/D3h;->b:LX/D3i;

    iget v2, v2, LX/D3i;->m:I

    iget-object v3, p0, LX/D3h;->b:LX/D3i;

    iget v3, v3, LX/D3i;->v:I

    div-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 1960205
    :goto_1
    iget-object v2, p0, LX/D3h;->b:LX/D3i;

    invoke-static {v2, p1, v1, v0}, LX/D3i;->a(LX/D3i;FII)I

    move-result v0

    goto :goto_0

    .line 1960206
    :cond_2
    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget v0, v0, LX/D3i;->m:I

    goto :goto_1

    .line 1960207
    :cond_3
    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget-object v0, v0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget v0, v0, LX/D3i;->m:I

    iget-object v1, p0, LX/D3h;->b:LX/D3i;

    iget v1, v1, LX/D3i;->v:I

    div-int/2addr v0, v1

    .line 1960208
    :goto_2
    iget-object v1, p0, LX/D3h;->b:LX/D3i;

    iget v1, v1, LX/D3i;->m:I

    move v4, v1

    move v1, v0

    move v0, v4

    goto :goto_1

    :cond_4
    move v0, v1

    .line 1960209
    goto :goto_2
.end method

.method public final a(ILX/3sp;)V
    .locals 1

    .prologue
    .line 1960218
    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget-object v0, v0, LX/D3i;->e:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 1960219
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 1960220
    invoke-direct {p0, p1}, LX/D3h;->b(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 1960221
    return-void
.end method

.method public final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1960216
    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget-object v0, v0, LX/D3i;->e:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1960217
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1960211
    iget-object v0, p0, LX/D3h;->b:LX/D3i;

    iget v1, v0, LX/D3i;->v:I

    .line 1960212
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1960213
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1960214
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1960215
    :cond_0
    return-void
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 1960210
    const/4 v0, 0x0

    return v0
.end method
