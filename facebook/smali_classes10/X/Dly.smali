.class public final LX/Dly;
.super LX/Dlw;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V
    .locals 0

    .prologue
    .line 2038812
    iput-object p1, p0, LX/Dly;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    invoke-direct {p0, p2, p3, p4}, LX/Dlw;-><init>(Landroid/view/View;ZLX/0wM;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2038805
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->a()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    move v0, v1

    .line 2038806
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dly;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f082bf5

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/Dly;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v5, v5, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->l:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v2, p0, LX/Dly;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->l:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->a()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2038807
    :goto_1
    iget-object v1, p0, LX/Dlw;->n:Landroid/widget/TextView;

    iget-object v2, p0, LX/Dly;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082bf1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2038808
    const v1, 0x7f0207fd

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, LX/Dlw;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2038809
    return-void

    :cond_0
    move v0, v2

    .line 2038810
    goto :goto_0

    .line 2038811
    :cond_1
    iget-object v0, p0, LX/Dly;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->l:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->t()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
