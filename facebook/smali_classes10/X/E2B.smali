.class public LX/E2B;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E29;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E2B;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E2C;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2072364
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E2B;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E2C;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072365
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2072366
    iput-object p1, p0, LX/E2B;->b:LX/0Ot;

    .line 2072367
    return-void
.end method

.method public static a(LX/0QB;)LX/E2B;
    .locals 4

    .prologue
    .line 2072368
    sget-object v0, LX/E2B;->c:LX/E2B;

    if-nez v0, :cond_1

    .line 2072369
    const-class v1, LX/E2B;

    monitor-enter v1

    .line 2072370
    :try_start_0
    sget-object v0, LX/E2B;->c:LX/E2B;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072371
    if-eqz v2, :cond_0

    .line 2072372
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072373
    new-instance v3, LX/E2B;

    const/16 p0, 0x30b8

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E2B;-><init>(LX/0Ot;)V

    .line 2072374
    move-object v0, v3

    .line 2072375
    sput-object v0, LX/E2B;->c:LX/E2B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072376
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072377
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072378
    :cond_1
    sget-object v0, LX/E2B;->c:LX/E2B;

    return-object v0

    .line 2072379
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072380
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2072381
    check-cast p2, LX/E2A;

    .line 2072382
    iget-object v0, p0, LX/E2B;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2C;

    iget-object v1, p2, LX/E2A;->a:Ljava/lang/CharSequence;

    iget v2, p2, LX/E2A;->b:I

    iget-object v3, p2, LX/E2A;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    const/4 v5, 0x1

    .line 2072383
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->gL_()Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v5

    .line 2072384
    :goto_0
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->gM_()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;

    move-result-object v6

    .line 2072385
    if-nez v6, :cond_5

    .line 2072386
    const v7, 0x7f0b0050

    .line 2072387
    :goto_1
    move v6, v7

    .line 2072388
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->j()I

    move-result v7

    .line 2072389
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->m()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2072390
    iget-object p0, v0, LX/E2C;->a:LX/23P;

    const/4 p2, 0x0

    invoke-virtual {p0, v1, p2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2072391
    :cond_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    move-result-object p2

    .line 2072392
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->LEFT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    if-ne p2, v0, :cond_6

    .line 2072393
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 2072394
    :goto_2
    move-object p2, v0

    .line 2072395
    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1ne;->m(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    move-result-object p0

    const/4 v1, 0x0

    .line 2072396
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->BOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    if-ne p0, p2, :cond_9

    .line 2072397
    sget-object p2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v0, LX/0xr;->BOLD:LX/0xr;

    invoke-static {p1, p2, v0, v1}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p2

    .line 2072398
    :goto_3
    move-object p0, p2

    .line 2072399
    invoke-virtual {v6, p0}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v6

    .line 2072400
    if-eqz v4, :cond_3

    .line 2072401
    invoke-virtual {v6, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    .line 2072402
    :cond_1
    :goto_4
    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    move-result-object v5

    .line 2072403
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->LEFT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    if-ne v5, v6, :cond_c

    .line 2072404
    const/4 v6, 0x1

    .line 2072405
    :goto_5
    move v5, v6

    .line 2072406
    invoke-interface {v4, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2072407
    return-object v0

    .line 2072408
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 2072409
    :cond_3
    if-lez v7, :cond_1

    .line 2072410
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->k()I

    move-result v4

    .line 2072411
    if-ltz v4, :cond_4

    if-gt v4, v7, :cond_4

    .line 2072412
    invoke-virtual {v6, v4}, LX/1ne;->i(I)LX/1ne;

    .line 2072413
    :cond_4
    invoke-virtual {v6, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    goto :goto_4

    .line 2072414
    :cond_5
    sget-object v7, LX/CgV;->b:[I

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextSize;->ordinal()I

    move-result p0

    aget v7, v7, p0

    packed-switch v7, :pswitch_data_0

    .line 2072415
    const v7, 0x7f0b0050

    goto/16 :goto_1

    .line 2072416
    :pswitch_0
    const v7, 0x7f0b0058

    goto/16 :goto_1

    .line 2072417
    :pswitch_1
    const v7, 0x7f0b0052

    goto/16 :goto_1

    .line 2072418
    :pswitch_2
    const v7, 0x7f0b0050

    goto/16 :goto_1

    .line 2072419
    :pswitch_3
    const v7, 0x7f0b004e

    goto/16 :goto_1

    .line 2072420
    :pswitch_4
    const v7, 0x7f0b004c

    goto/16 :goto_1

    .line 2072421
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->CENTER:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    if-ne p2, v0, :cond_7

    .line 2072422
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto/16 :goto_2

    .line 2072423
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    if-ne p2, v0, :cond_8

    .line 2072424
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto/16 :goto_2

    .line 2072425
    :cond_8
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto/16 :goto_2

    .line 2072426
    :cond_9
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->SEMIBOLD:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    if-ne p0, p2, :cond_a

    .line 2072427
    sget-object p2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v0, LX/0xr;->MEDIUM:LX/0xr;

    invoke-static {p1, p2, v0, v1}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p2

    goto/16 :goto_3

    .line 2072428
    :cond_a
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;->LIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextBoldness;

    if-ne p0, p2, :cond_b

    .line 2072429
    sget-object p2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v0, LX/0xr;->LIGHT:LX/0xr;

    invoke-static {p1, p2, v0, v1}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p2

    goto/16 :goto_3

    .line 2072430
    :cond_b
    sget-object p2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v0, LX/0xr;->REGULAR:LX/0xr;

    invoke-static {p1, p2, v0, v1}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p2

    goto/16 :goto_3

    .line 2072431
    :cond_c
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->CENTER:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    if-ne v5, v6, :cond_d

    .line 2072432
    const/4 v6, 0x2

    goto/16 :goto_5

    .line 2072433
    :cond_d
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;->RIGHT:Lcom/facebook/graphql/enums/GraphQLReactionCoreTextAlignment;

    if-ne v5, v6, :cond_e

    .line 2072434
    const/4 v6, 0x3

    goto/16 :goto_5

    .line 2072435
    :cond_e
    const/4 v6, 0x0

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2072436
    invoke-static {}, LX/1dS;->b()V

    .line 2072437
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/E29;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2072438
    new-instance v1, LX/E2A;

    invoke-direct {v1, p0}, LX/E2A;-><init>(LX/E2B;)V

    .line 2072439
    sget-object v2, LX/E2B;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E29;

    .line 2072440
    if-nez v2, :cond_0

    .line 2072441
    new-instance v2, LX/E29;

    invoke-direct {v2}, LX/E29;-><init>()V

    .line 2072442
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/E29;->a$redex0(LX/E29;LX/1De;IILX/E2A;)V

    .line 2072443
    move-object v1, v2

    .line 2072444
    move-object v0, v1

    .line 2072445
    return-object v0
.end method
