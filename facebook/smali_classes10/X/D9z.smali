.class public final LX/D9z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;


# direct methods
.method public constructor <init>(Lcom/facebook/contactlogs/ContactLogsUploadRunner;)V
    .locals 0

    .prologue
    .line 1970718
    iput-object p1, p0, LX/D9z;->a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1970719
    iget-object v0, p0, LX/D9z;->a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    const/4 v1, 0x0

    .line 1970720
    iput-object v1, v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->m:LX/1ML;

    .line 1970721
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1970722
    invoke-direct {p0}, LX/D9z;->b()V

    .line 1970723
    iget-object v0, p0, LX/D9z;->a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    iget-object v0, v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->j:LX/2UL;

    sget-object v1, LX/DA8;->UPLOAD_FAILED:LX/DA8;

    invoke-virtual {v0, v1}, LX/2UL;->a(LX/DA8;)V

    .line 1970724
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1970725
    invoke-direct {p0}, LX/D9z;->b()V

    .line 1970726
    iget-object v0, p0, LX/D9z;->a:Lcom/facebook/contactlogs/ContactLogsUploadRunner;

    iget-object v0, v0, Lcom/facebook/contactlogs/ContactLogsUploadRunner;->j:LX/2UL;

    sget-object v1, LX/DA8;->UPLOAD_SUCCEEDED:LX/DA8;

    invoke-virtual {v0, v1}, LX/2UL;->a(LX/DA8;)V

    .line 1970727
    return-void
.end method
