.class public LX/ElT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2164427
    packed-switch p0, :pswitch_data_0

    .line 2164428
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to get the string value of: %d. This exceeds the max value we currently support. Please add more case statements here. We want to make sure that we are always using string literals rather than calling String.valueOf for params"

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2164429
    :pswitch_0
    const-string v0, "0"

    .line 2164430
    :goto_0
    return-object v0

    .line 2164431
    :pswitch_1
    const-string v0, "1"

    goto :goto_0

    .line 2164432
    :pswitch_2
    const-string v0, "2"

    goto :goto_0

    .line 2164433
    :pswitch_3
    const-string v0, "3"

    goto :goto_0

    .line 2164434
    :pswitch_4
    const-string v0, "4"

    goto :goto_0

    .line 2164435
    :pswitch_5
    const-string v0, "5"

    goto :goto_0

    .line 2164436
    :pswitch_6
    const-string v0, "6"

    goto :goto_0

    .line 2164437
    :pswitch_7
    const-string v0, "7"

    goto :goto_0

    .line 2164438
    :pswitch_8
    const-string v0, "8"

    goto :goto_0

    .line 2164439
    :pswitch_9
    const-string v0, "9"

    goto :goto_0

    .line 2164440
    :pswitch_a
    const-string v0, "10"

    goto :goto_0

    .line 2164441
    :pswitch_b
    const-string v0, "11"

    goto :goto_0

    .line 2164442
    :pswitch_c
    const-string v0, "12"

    goto :goto_0

    .line 2164443
    :pswitch_d
    const-string v0, "13"

    goto :goto_0

    .line 2164444
    :pswitch_e
    const-string v0, "14"

    goto :goto_0

    .line 2164445
    :pswitch_f
    const-string v0, "15"

    goto :goto_0

    .line 2164446
    :pswitch_10
    const-string v0, "16"

    goto :goto_0

    .line 2164447
    :pswitch_11
    const-string v0, "17"

    goto :goto_0

    .line 2164448
    :pswitch_12
    const-string v0, "18"

    goto :goto_0

    .line 2164449
    :pswitch_13
    const-string v0, "19"

    goto :goto_0

    .line 2164450
    :pswitch_14
    const-string v0, "20"

    goto :goto_0

    .line 2164451
    :pswitch_15
    const-string v0, "21"

    goto :goto_0

    .line 2164452
    :pswitch_16
    const-string v0, "22"

    goto :goto_0

    .line 2164453
    :pswitch_17
    const-string v0, "23"

    goto :goto_0

    .line 2164454
    :pswitch_18
    const-string v0, "24"

    goto :goto_0

    .line 2164455
    :pswitch_19
    const-string v0, "25"

    goto :goto_0

    .line 2164456
    :pswitch_1a
    const-string v0, "26"

    goto :goto_0

    .line 2164457
    :pswitch_1b
    const-string v0, "27"

    goto :goto_0

    .line 2164458
    :pswitch_1c
    const-string v0, "28"

    goto :goto_0

    .line 2164459
    :pswitch_1d
    const-string v0, "29"

    goto :goto_0

    .line 2164460
    :pswitch_1e
    const-string v0, "30"

    goto :goto_0

    .line 2164461
    :pswitch_1f
    const-string v0, "31"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch
.end method

.method public static a(IJ)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2164462
    invoke-static {p0, p1, p2}, LX/ElT;->b(IJ)V

    .line 2164463
    long-to-int v1, p1

    invoke-static {v1}, LX/ElT;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2164464
    return-object v0
.end method

.method private static b(IJ)V
    .locals 5

    .prologue
    .line 2164465
    const/16 v0, 0x20

    ushr-long v0, p1, v0

    long-to-int v0, v0

    .line 2164466
    if-eq v0, p0, :cond_0

    .line 2164467
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to pass a param that was generated from the graphql query with identity %d into request with identity %d"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2164468
    :cond_0
    return-void
.end method
