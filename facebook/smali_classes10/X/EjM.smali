.class public LX/EjM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/30Y;

.field public final b:LX/30Z;

.field private final c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/30Y;LX/30Z;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2161286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2161287
    iput-object p1, p0, LX/EjM;->a:LX/30Y;

    .line 2161288
    iput-object p3, p0, LX/EjM;->c:Ljava/util/concurrent/ExecutorService;

    .line 2161289
    iput-object p2, p0, LX/EjM;->b:LX/30Z;

    .line 2161290
    return-void
.end method

.method public static a(LX/0QB;)LX/EjM;
    .locals 4

    .prologue
    .line 2161291
    new-instance v3, LX/EjM;

    invoke-static {p0}, LX/30Y;->b(LX/0QB;)LX/30Y;

    move-result-object v0

    check-cast v0, LX/30Y;

    invoke-static {p0}, LX/30Z;->a(LX/0QB;)LX/30Z;

    move-result-object v1

    check-cast v1, LX/30Z;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v3, v0, v1, v2}, LX/EjM;-><init>(LX/30Y;LX/30Z;Ljava/util/concurrent/ExecutorService;)V

    .line 2161292
    move-object v0, v3

    .line 2161293
    return-object v0
.end method

.method public static a(LX/EjM;Z)V
    .locals 3

    .prologue
    .line 2161294
    iget-object v0, p0, LX/EjM;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/contacts/ccu/internsetting/ContactsUploadInternSettingHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/contacts/ccu/internsetting/ContactsUploadInternSettingHelper$1;-><init>(LX/EjM;Z)V

    const v2, -0x384d1642

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2161295
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2161296
    iget-object v0, p0, LX/EjM;->a:LX/30Y;

    invoke-virtual {v0}, LX/30Y;->a()Z

    move-result v0

    return v0
.end method
