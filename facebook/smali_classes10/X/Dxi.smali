.class public abstract LX/Dxi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final a:I

.field public b:I

.field public c:I

.field public d:I

.field private e:J

.field private f:I

.field public g:Z

.field private h:Z

.field private final i:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2063148
    const/16 v0, 0xc8

    invoke-direct {p0, v0}, LX/Dxi;-><init>(I)V

    .line 2063149
    return-void
.end method

.method private constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 2063150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2063151
    iput v0, p0, LX/Dxi;->b:I

    .line 2063152
    iput v0, p0, LX/Dxi;->c:I

    .line 2063153
    iput v0, p0, LX/Dxi;->d:I

    .line 2063154
    iput-boolean v1, p0, LX/Dxi;->g:Z

    .line 2063155
    iput-boolean v1, p0, LX/Dxi;->h:Z

    .line 2063156
    new-instance v0, LX/Dxh;

    invoke-direct {v0, p0}, LX/Dxh;-><init>(LX/Dxi;)V

    iput-object v0, p0, LX/Dxi;->i:Landroid/os/Handler;

    .line 2063157
    iput p1, p0, LX/Dxi;->a:I

    .line 2063158
    return-void
.end method


# virtual methods
.method public abstract a(III)V
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 4

    .prologue
    .line 2063159
    iput p2, p0, LX/Dxi;->b:I

    .line 2063160
    iput p3, p0, LX/Dxi;->c:I

    .line 2063161
    iput p4, p0, LX/Dxi;->d:I

    .line 2063162
    iget-boolean v0, p0, LX/Dxi;->h:Z

    if-nez v0, :cond_1

    .line 2063163
    iget v0, p0, LX/Dxi;->b:I

    iget v1, p0, LX/Dxi;->c:I

    iget v2, p0, LX/Dxi;->d:I

    invoke-virtual {p0, v0, v1, v2}, LX/Dxi;->a(III)V

    .line 2063164
    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/Dxi;->e:J

    .line 2063165
    return-void

    .line 2063166
    :cond_1
    iget v0, p0, LX/Dxi;->a:I

    if-lez v0, :cond_0

    iget v0, p0, LX/Dxi;->f:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2063167
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LX/Dxi;->e:J

    sub-long/2addr v0, v2

    iget v2, p0, LX/Dxi;->a:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 2063168
    iget v0, p0, LX/Dxi;->b:I

    iget v1, p0, LX/Dxi;->c:I

    iget v2, p0, LX/Dxi;->d:I

    invoke-virtual {p0, v0, v1, v2}, LX/Dxi;->a(III)V

    goto :goto_0

    .line 2063169
    :cond_2
    iget-boolean v0, p0, LX/Dxi;->g:Z

    if-nez v0, :cond_0

    .line 2063170
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Dxi;->g:Z

    .line 2063171
    iget-object v0, p0, LX/Dxi;->i:Landroid/os/Handler;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    iget v2, p0, LX/Dxi;->a:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    .line 2063172
    iget-boolean v0, p0, LX/Dxi;->h:Z

    if-nez v0, :cond_0

    .line 2063173
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Dxi;->h:Z

    .line 2063174
    :cond_0
    iput p2, p0, LX/Dxi;->f:I

    .line 2063175
    if-nez p2, :cond_1

    .line 2063176
    iget v0, p0, LX/Dxi;->b:I

    iget v1, p0, LX/Dxi;->c:I

    iget v2, p0, LX/Dxi;->d:I

    invoke-virtual {p0, v0, v1, v2}, LX/Dxi;->a(III)V

    .line 2063177
    :cond_1
    return-void
.end method
