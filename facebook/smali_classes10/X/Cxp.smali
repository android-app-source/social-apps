.class public LX/Cxp;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Cxo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952124
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1952125
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/content/Context;LX/1PT;LX/CxF;Ljava/lang/Runnable;LX/1Jg;LX/1PY;LX/CzE;LX/CzA;LX/CzE;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/CzE;LX/Cz2;LX/CyE;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/CzE;)LX/Cxo;
    .locals 56

    .prologue
    .line 1952126
    new-instance v1, LX/Cxo;

    invoke-static/range {p0 .. p0}, LX/1Pz;->a(LX/0QB;)LX/1Pz;

    move-result-object v22

    check-cast v22, LX/1Pz;

    invoke-static/range {p0 .. p0}, LX/1Q0;->a(LX/0QB;)LX/1Q0;

    move-result-object v23

    check-cast v23, LX/1Q0;

    const-class v2, LX/1Q1;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/1Q1;

    invoke-static/range {p0 .. p0}, LX/1Q2;->a(LX/0QB;)LX/1Q2;

    move-result-object v25

    check-cast v25, LX/1Q2;

    invoke-static/range {p0 .. p0}, LX/1Q3;->a(LX/0QB;)LX/1Q3;

    move-result-object v26

    check-cast v26, LX/1Q3;

    invoke-static/range {p0 .. p0}, LX/1Q4;->a(LX/0QB;)LX/1Q4;

    move-result-object v27

    check-cast v27, LX/1Q4;

    const-class v2, LX/1Q5;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v28

    check-cast v28, LX/1Q5;

    const-class v2, LX/1Q6;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/1Q6;

    invoke-static/range {p0 .. p0}, LX/1Q7;->a(LX/0QB;)LX/1Q7;

    move-result-object v30

    check-cast v30, LX/1Q7;

    const-class v2, LX/1QA;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v31

    check-cast v31, LX/1QA;

    const-class v2, LX/CxO;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v32

    check-cast v32, LX/CxO;

    invoke-static/range {p0 .. p0}, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a(LX/0QB;)Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    move-result-object v33

    check-cast v33, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    const-class v2, LX/1QC;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v34

    check-cast v34, LX/1QC;

    invoke-static/range {p0 .. p0}, LX/1QD;->a(LX/0QB;)LX/1QD;

    move-result-object v35

    check-cast v35, LX/1QD;

    const-class v2, LX/1QE;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v36

    check-cast v36, LX/1QE;

    invoke-static/range {p0 .. p0}, LX/1QF;->a(LX/0QB;)LX/1QF;

    move-result-object v37

    check-cast v37, LX/1QF;

    invoke-static/range {p0 .. p0}, LX/1QG;->a(LX/0QB;)LX/1QG;

    move-result-object v38

    check-cast v38, LX/1QG;

    const-class v2, LX/1QH;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v39

    check-cast v39, LX/1QH;

    const-class v2, LX/1QI;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v40

    check-cast v40, LX/1QI;

    const-class v2, LX/CxK;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v41

    check-cast v41, LX/CxK;

    const-class v2, LX/CxZ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v42

    check-cast v42, LX/CxZ;

    const-class v2, LX/CxR;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v43

    check-cast v43, LX/CxR;

    const-class v2, LX/CxX;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v44

    check-cast v44, LX/CxX;

    invoke-static/range {p0 .. p0}, LX/Cxb;->a(LX/0QB;)LX/Cxb;

    move-result-object v45

    check-cast v45, LX/Cxb;

    const-class v2, LX/CyK;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v46

    check-cast v46, LX/CyK;

    const-class v2, LX/Cx9;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v47

    check-cast v47, LX/Cx9;

    const-class v2, LX/Cy0;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v48

    check-cast v48, LX/Cy0;

    const-class v2, LX/Cy3;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v49

    check-cast v49, LX/Cy3;

    const-class v2, LX/CyA;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v50

    check-cast v50, LX/CyA;

    const-class v2, LX/CyG;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v51

    check-cast v51, LX/CyG;

    const-class v2, LX/Cxv;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v52

    check-cast v52, LX/Cxv;

    const-class v2, LX/CxC;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v53

    check-cast v53, LX/CxC;

    const-class v2, LX/Cx4;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v54

    check-cast v54, LX/Cx4;

    invoke-static/range {p0 .. p0}, LX/1QK;->a(LX/0QB;)LX/1QK;

    move-result-object v55

    check-cast v55, LX/1QK;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    invoke-direct/range {v1 .. v55}, LX/Cxo;-><init>(Ljava/lang/String;Landroid/content/Context;LX/1PT;LX/CxF;Ljava/lang/Runnable;LX/1Jg;LX/1PY;LX/CzE;LX/CzA;LX/CzE;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/CzE;LX/Cz2;LX/CyE;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/CzE;LX/1Pz;LX/1Q0;LX/1Q1;LX/1Q2;LX/1Q3;LX/1Q4;LX/1Q5;LX/1Q6;LX/1Q7;LX/1QA;LX/CxO;Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;LX/1QC;LX/1QD;LX/1QE;LX/1QF;LX/1QG;LX/1QH;LX/1QI;LX/CxK;LX/CxZ;LX/CxR;LX/CxX;LX/Cxb;LX/CyK;LX/Cx9;LX/Cy0;LX/Cy3;LX/CyA;LX/CyG;LX/Cxv;LX/CxC;LX/Cx4;LX/1QK;)V

    .line 1952127
    return-object v1
.end method
