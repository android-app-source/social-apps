.class public final LX/ExV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0zS;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2184767
    iput-object p1, p0, LX/ExV;->c:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iput-object p2, p0, LX/ExV;->a:LX/0zS;

    iput-object p3, p0, LX/ExV;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2184768
    const/16 v2, 0x14

    .line 2184769
    iget-object v0, p0, LX/ExV;->c:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->b:Z

    if-eqz v0, :cond_0

    .line 2184770
    iget-object v0, p0, LX/ExV;->c:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->f:LX/Eui;

    sget-object v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p0, LX/ExV;->a:LX/0zS;

    iget-object v4, p0, LX/ExV;->b:Ljava/lang/String;

    iget-object v5, p0, LX/ExV;->c:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    invoke-static {v5}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->d(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)LX/0TF;

    move-result-object v5

    .line 2184771
    invoke-static {v0, v1, v2, v3, v4}, LX/Eui;->b(LX/Eui;Lcom/facebook/common/callercontext/CallerContext;ILX/0zS;Ljava/lang/String;)LX/0zO;

    move-result-object v7

    .line 2184772
    iget-object v6, v0, LX/Eui;->c:LX/2dl;

    iget-object v8, v0, LX/Eui;->d:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1My;

    new-instance v9, LX/Eue;

    invoke-direct {v9, v0, v5}, LX/Eue;-><init>(LX/Eui;LX/0TF;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 2184773
    iget-object v11, v7, LX/0zO;->m:LX/0gW;

    move-object v11, v11

    .line 2184774
    iget-object v12, v11, LX/0gW;->f:Ljava/lang/String;

    move-object v11, v12

    .line 2184775
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v0, LX/Eui;->e:I

    add-int/lit8 v12, v11, 0x1

    iput v12, v0, LX/Eui;->e:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "FC_CONTEXTUAL_PYMK_ATTRIBUTES"

    invoke-virtual/range {v6 .. v11}, LX/2dl;->a(LX/0zO;LX/1My;LX/0TF;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2184776
    invoke-static {v0, v6}, LX/Eui;->b(LX/Eui;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2184777
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/ExV;->c:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->f:LX/Eui;

    sget-object v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p0, LX/ExV;->a:LX/0zS;

    iget-object v4, p0, LX/ExV;->b:Ljava/lang/String;

    .line 2184778
    invoke-static {v0, v1, v2, v3, v4}, LX/Eui;->b(LX/Eui;Lcom/facebook/common/callercontext/CallerContext;ILX/0zS;Ljava/lang/String;)LX/0zO;

    move-result-object v5

    .line 2184779
    iget-object v6, v0, LX/Eui;->c:LX/2dl;

    const-string v7, "FC_CONTEXTUAL_PYMK_ATTRIBUTES"

    invoke-virtual {v6, v5, v7}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    invoke-static {v0, v5}, LX/Eui;->b(LX/Eui;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 2184780
    goto :goto_0
.end method
