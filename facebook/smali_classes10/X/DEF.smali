.class public LX/DEF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DEF",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976531
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1976532
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DEF;->b:LX/0Zi;

    .line 1976533
    iput-object p1, p0, LX/DEF;->a:LX/0Ot;

    .line 1976534
    return-void
.end method

.method public static a(LX/0QB;)LX/DEF;
    .locals 4

    .prologue
    .line 1976535
    const-class v1, LX/DEF;

    monitor-enter v1

    .line 1976536
    :try_start_0
    sget-object v0, LX/DEF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976537
    sput-object v2, LX/DEF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976538
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976539
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976540
    new-instance v3, LX/DEF;

    const/16 p0, 0x1fb5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DEF;-><init>(LX/0Ot;)V

    .line 1976541
    move-object v0, v3

    .line 1976542
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976543
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DEF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976544
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976545
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 1976546
    check-cast p1, LX/DEE;

    .line 1976547
    iget-object v0, p0, LX/DEF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;

    iget-object v1, p1, LX/DEE;->a:LX/DEI;

    iget-object v2, p1, LX/DEE;->b:LX/1Po;

    .line 1976548
    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    sget-object v4, LX/1Qt;->GROUPS:LX/1Qt;

    if-ne v3, v4, :cond_0

    const-string v3, "GROUPS_YOU_SHOULD_CREATE_GROUPS_MALL"

    :goto_0
    iget-object v4, v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, v1, LX/DEI;->a:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    iget-object v6, v1, LX/DEI;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    iget-object v7, v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->g:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0tX;

    iget-object v8, v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->f:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v9, v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->h:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v3 .. v9}, LX/DES;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;LX/0tX;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/util/concurrent/ExecutorService;)V

    .line 1976549
    return-void

    .line 1976550
    :cond_0
    const-string v3, "GROUPS_YOU_SHOULD_CREATE_NEWS_FEED"

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1976551
    check-cast p2, LX/DEE;

    .line 1976552
    iget-object v0, p0, LX/DEF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;

    iget-object v1, p2, LX/DEE;->a:LX/DEI;

    iget-object v2, p2, LX/DEE;->b:LX/1Po;

    const/4 v11, 0x7

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 1976553
    move-object v3, v2

    check-cast v3, LX/1Pr;

    new-instance v4, LX/DEG;

    iget-object v5, v1, LX/DEI;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    invoke-direct {v4, v5}, LX/DEG;-><init>(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;)V

    iget-object v5, v1, LX/DEI;->a:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-interface {v3, v4, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    .line 1976554
    iget-object v3, v1, LX/DEI;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    const/4 v4, 0x0

    .line 1976555
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1976556
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1976557
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v12

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result p0

    move v6, v4

    move v5, v4

    :goto_0
    if-ge v6, p0, :cond_0

    invoke-virtual {v12, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLUser;

    .line 1976558
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 1976559
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1976560
    add-int/lit8 v4, v5, 0x1

    .line 1976561
    :goto_1
    const/4 v5, 0x5

    if-ge v4, v5, :cond_0

    .line 1976562
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    goto :goto_0

    .line 1976563
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 1976564
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3c

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-static {v0, p1}, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->b(Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;LX/1De;)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b0f4d

    invoke-interface {v5, v6}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b0f52

    invoke-interface {v5, v11, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->e:LX/8yS;

    const/4 v7, 0x0

    .line 1976565
    new-instance v12, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    invoke-direct {v12, v6}, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;-><init>(LX/8yS;)V

    .line 1976566
    sget-object p0, LX/8yS;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/8yR;

    .line 1976567
    if-nez p0, :cond_1

    .line 1976568
    new-instance p0, LX/8yR;

    invoke-direct {p0}, LX/8yR;-><init>()V

    .line 1976569
    :cond_1
    invoke-static {p0, p1, v7, v7, v12}, LX/8yR;->a$redex0(LX/8yR;LX/1De;IILcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;)V

    .line 1976570
    move-object v12, p0

    .line 1976571
    move-object v7, v12

    .line 1976572
    move-object v6, v7

    .line 1976573
    const v7, 0x7f0b0f59

    .line 1976574
    iget-object v12, v6, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    invoke-virtual {v6, v7}, LX/1Dp;->e(I)I

    move-result p0

    iput p0, v12, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->b:I

    .line 1976575
    move-object v6, v6

    .line 1976576
    const v7, 0x7f0b0f5a

    .line 1976577
    iget-object v12, v6, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    invoke-virtual {v6, v7}, LX/1Dp;->f(I)I

    move-result p0

    iput p0, v12, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->f:I

    .line 1976578
    move-object v6, v6

    .line 1976579
    iget-object v7, v6, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    iput-object v3, v7, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->h:Ljava/util/List;

    .line 1976580
    move-object v3, v6

    .line 1976581
    const v6, 0x7f0b0f5b

    .line 1976582
    iget-object v7, v3, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    invoke-virtual {v3, v6}, LX/1Dp;->e(I)I

    move-result v12

    iput v12, v7, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->c:I

    .line 1976583
    move-object v3, v3

    .line 1976584
    iget-object v6, v3, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    iput-boolean v10, v6, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->g:Z

    .line 1976585
    move-object v3, v3

    .line 1976586
    iget-object v6, v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->c:Landroid/content/res/Resources;

    const v7, 0x7f0c0041

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 1976587
    iget-object v7, v3, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    iput v6, v7, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->j:I

    .line 1976588
    move-object v3, v3

    .line 1976589
    iget-object v6, v1, LX/DEI;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 1976590
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 1976591
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    .line 1976592
    :goto_2
    move v6, v7

    .line 1976593
    iget-object v7, v3, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    iput v6, v7, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->i:I

    .line 1976594
    move-object v3, v3

    .line 1976595
    const v6, 0x7f0b0f5c

    .line 1976596
    iget-object v7, v3, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    invoke-virtual {v3, v6}, LX/1Dp;->e(I)I

    move-result v12

    iput v12, v7, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->e:I

    .line 1976597
    move-object v3, v3

    .line 1976598
    const v6, 0x7f0a00d5

    .line 1976599
    iget-object v7, v3, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    invoke-virtual {v3, v6}, LX/1Dp;->d(I)I

    move-result v12

    iput v12, v7, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->d:I

    .line 1976600
    move-object v3, v3

    .line 1976601
    sget-object v6, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1976602
    iget-object v7, v3, LX/8yR;->a:Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    iput-object v6, v7, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1976603
    move-object v3, v3

    .line 1976604
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v4

    const v5, 0x7f0a0046

    invoke-virtual {v4, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0033

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0f4e

    invoke-interface {v4, v5}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0f50

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0f52

    invoke-interface {v4, v11, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    invoke-interface {v4, v9, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v10}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    iget-object v6, v1, LX/DEI;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 1976605
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v7

    const v9, 0x7f0b0050

    invoke-virtual {v7, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v9, 0x7f0a010d

    invoke-virtual {v7, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    const/4 v9, 0x1

    invoke-virtual {v7, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v7

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v7, v9}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v7

    move-object v6, v7

    .line 1976606
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    .line 1976607
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f081863

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b004e

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a010e

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    move-object v6, v6

    .line 1976608
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    iget-object v6, v1, LX/DEI;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    const/4 v9, 0x0

    const/4 v11, 0x0

    .line 1976609
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v10

    .line 1976610
    if-lez v10, :cond_6

    .line 1976611
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 1976612
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v7

    .line 1976613
    :goto_3
    if-eqz v7, :cond_5

    iget-object v9, v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->c:Landroid/content/res/Resources;

    invoke-static {v10, v7, v9}, LX/DES;->a(ILjava/lang/String;Landroid/content/res/Resources;)Ljava/lang/StringBuffer;

    move-result-object v7

    .line 1976614
    :goto_4
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v11}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v7

    const v9, 0x7f0b004e

    invoke-virtual {v7, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v9, 0x7f0a010e

    invoke-virtual {v7, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    const/4 v9, 0x1

    invoke-virtual {v7, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v7

    move-object v6, v7

    .line 1976615
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a0046

    invoke-virtual {v5, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0033

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->b:LX/DE0;

    const/4 v6, 0x0

    .line 1976616
    new-instance v7, LX/DDy;

    invoke-direct {v7, v5}, LX/DDy;-><init>(LX/DE0;)V

    .line 1976617
    iget-object v9, v5, LX/DE0;->b:LX/0Zi;

    invoke-virtual {v9}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/DDz;

    .line 1976618
    if-nez v9, :cond_2

    .line 1976619
    new-instance v9, LX/DDz;

    invoke-direct {v9, v5}, LX/DDz;-><init>(LX/DE0;)V

    .line 1976620
    :cond_2
    invoke-static {v9, p1, v6, v6, v7}, LX/DDz;->a$redex0(LX/DDz;LX/1De;IILX/DDy;)V

    .line 1976621
    move-object v7, v9

    .line 1976622
    move-object v6, v7

    .line 1976623
    move-object v5, v6

    .line 1976624
    iget-object v6, v5, LX/DDz;->a:LX/DDy;

    iput-object v2, v6, LX/DDy;->b:LX/1Po;

    .line 1976625
    iget-object v6, v5, LX/DDz;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1976626
    move-object v5, v5

    .line 1976627
    iget-object v6, v5, LX/DDz;->a:LX/DDy;

    iput-object v1, v6, LX/DDy;->a:LX/DEI;

    .line 1976628
    iget-object v6, v5, LX/DDz;->e:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1976629
    move-object v5, v5

    .line 1976630
    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1976631
    return-object v0

    :cond_3
    move v4, v5

    goto/16 :goto_1

    :cond_4
    const/4 v7, 0x0

    goto/16 :goto_2

    :cond_5
    move-object v7, v9

    .line 1976632
    goto/16 :goto_4

    :cond_6
    move-object v7, v9

    goto/16 :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1976633
    invoke-static {}, LX/1dS;->b()V

    .line 1976634
    iget v0, p1, LX/1dQ;->b:I

    .line 1976635
    packed-switch v0, :pswitch_data_0

    .line 1976636
    :goto_0
    return-object v1

    .line 1976637
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/DEF;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x49b238bd
        :pswitch_0
    .end packed-switch
.end method
