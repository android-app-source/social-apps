.class public final LX/EpR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/2na;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EpL;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final synthetic d:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public final synthetic e:Z

.field public final synthetic f:Z

.field public final synthetic g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic h:LX/EpZ;


# direct methods
.method public constructor <init>(LX/EpZ;LX/EpL;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 1

    .prologue
    .line 2170286
    iput-object p1, p0, LX/EpR;->h:LX/EpZ;

    iput-object p2, p0, LX/EpR;->a:LX/EpL;

    iput-wide p3, p0, LX/EpR;->b:J

    iput-object p5, p0, LX/EpR;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object p6, p0, LX/EpR;->d:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-boolean p7, p0, LX/EpR;->e:Z

    iput-boolean p8, p0, LX/EpR;->f:Z

    iput-object p9, p0, LX/EpR;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11

    .prologue
    .line 2170287
    check-cast p1, LX/2na;

    .line 2170288
    sget-object v0, LX/2na;->CONFIRM:LX/2na;

    if-ne p1, v0, :cond_0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2170289
    :goto_0
    iget-object v1, p0, LX/EpR;->a:LX/EpL;

    iget-wide v2, p0, LX/EpR;->b:J

    iget-object v5, p0, LX/EpR;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v6, p0, LX/EpR;->d:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-interface/range {v1 .. v6}, LX/EpL;->a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2170290
    iget-object v0, p0, LX/EpR;->h:LX/EpZ;

    iget-object v0, v0, LX/EpZ;->c:LX/2dj;

    iget-wide v2, p0, LX/EpR;->b:J

    iget-object v1, p0, LX/EpR;->h:LX/EpZ;

    .line 2170291
    iget-object v4, v1, LX/EpZ;->a:LX/2h7;

    if-eqz v4, :cond_1

    iget-object v4, v1, LX/EpZ;->a:LX/2h7;

    iget-object v4, v4, LX/2h7;->friendRequestResponseRef:LX/2hA;

    if-eqz v4, :cond_1

    .line 2170292
    iget-object v4, v1, LX/EpZ;->a:LX/2h7;

    iget-object v4, v4, LX/2h7;->friendRequestResponseRef:LX/2hA;

    .line 2170293
    :goto_1
    move-object v1, v4

    .line 2170294
    invoke-virtual {v0, v2, v3, p1, v1}, LX/2dj;->a(JLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/EpR;->h:LX/EpZ;

    iget-object v2, v2, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 2170295
    iget-object v0, p0, LX/EpR;->h:LX/EpZ;

    iget-object v1, p0, LX/EpR;->h:LX/EpZ;

    iget-object v1, v1, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-wide v2, p0, LX/EpR;->b:J

    iget-object v4, p0, LX/EpR;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v5, p0, LX/EpR;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v6, p0, LX/EpR;->d:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iget-boolean v7, p0, LX/EpR;->e:Z

    iget-boolean v8, p0, LX/EpR;->f:Z

    iget-object v9, p0, LX/EpR;->a:LX/EpL;

    .line 2170296
    invoke-static/range {v0 .. v9}, LX/EpZ;->a$redex0(LX/EpZ;Lcom/google/common/util/concurrent/ListenableFuture;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V

    .line 2170297
    return-object v10

    .line 2170298
    :cond_0
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    :cond_1
    sget-object v4, LX/2hA;->ENTITY_CARDS:LX/2hA;

    goto :goto_1
.end method
