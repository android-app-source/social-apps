.class public final LX/EbP;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EbO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EbP;",
        ">;",
        "LX/EbO;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2143649
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2143650
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbP;->d:LX/EWc;

    .line 2143651
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2143674
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2143675
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbP;->d:LX/EWc;

    .line 2143676
    return-void
.end method

.method private d(LX/EWY;)LX/EbP;
    .locals 1

    .prologue
    .line 2143677
    instance-of v0, p1, LX/EbQ;

    if-eqz v0, :cond_0

    .line 2143678
    check-cast p1, LX/EbQ;

    invoke-virtual {p0, p1}, LX/EbP;->a(LX/EbQ;)LX/EbP;

    move-result-object p0

    .line 2143679
    :goto_0
    return-object p0

    .line 2143680
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EbP;
    .locals 4

    .prologue
    .line 2143681
    const/4 v2, 0x0

    .line 2143682
    :try_start_0
    sget-object v0, LX/EbQ;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EbQ;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2143683
    if-eqz v0, :cond_0

    .line 2143684
    invoke-virtual {p0, v0}, LX/EbP;->a(LX/EbQ;)LX/EbP;

    .line 2143685
    :cond_0
    return-object p0

    .line 2143686
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2143687
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2143688
    check-cast v0, LX/EbQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2143689
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2143690
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2143691
    invoke-virtual {p0, v1}, LX/EbP;->a(LX/EbQ;)LX/EbP;

    :cond_1
    throw v0

    .line 2143692
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static u()LX/EbP;
    .locals 1

    .prologue
    .line 2143693
    new-instance v0, LX/EbP;

    invoke-direct {v0}, LX/EbP;-><init>()V

    return-object v0
.end method

.method private w()LX/EbP;
    .locals 2

    .prologue
    .line 2143694
    invoke-static {}, LX/EbP;->u()LX/EbP;

    move-result-object v0

    invoke-direct {p0}, LX/EbP;->y()LX/EbQ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EbP;->a(LX/EbQ;)LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/EbQ;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2143695
    new-instance v2, LX/EbQ;

    invoke-direct {v2, p0}, LX/EbQ;-><init>(LX/EWj;)V

    .line 2143696
    iget v3, p0, LX/EbP;->a:I

    .line 2143697
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 2143698
    :goto_0
    iget v1, p0, LX/EbP;->b:I

    .line 2143699
    iput v1, v2, LX/EbQ;->id_:I

    .line 2143700
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2143701
    or-int/lit8 v0, v0, 0x2

    .line 2143702
    :cond_0
    iget v1, p0, LX/EbP;->c:I

    .line 2143703
    iput v1, v2, LX/EbQ;->iteration_:I

    .line 2143704
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 2143705
    or-int/lit8 v0, v0, 0x4

    .line 2143706
    :cond_1
    iget-object v1, p0, LX/EbP;->d:LX/EWc;

    .line 2143707
    iput-object v1, v2, LX/EbQ;->ciphertext_:LX/EWc;

    .line 2143708
    iput v0, v2, LX/EbQ;->bitField0_:I

    .line 2143709
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2143710
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2143737
    invoke-direct {p0, p1}, LX/EbP;->d(LX/EWY;)LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2143711
    invoke-direct {p0, p1, p2}, LX/EbP;->d(LX/EWd;LX/EYZ;)LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/EbP;
    .locals 1

    .prologue
    .line 2143712
    iget v0, p0, LX/EbP;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EbP;->a:I

    .line 2143713
    iput p1, p0, LX/EbP;->b:I

    .line 2143714
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143715
    return-object p0
.end method

.method public final a(LX/EWc;)LX/EbP;
    .locals 1

    .prologue
    .line 2143716
    if-nez p1, :cond_0

    .line 2143717
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2143718
    :cond_0
    iget v0, p0, LX/EbP;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EbP;->a:I

    .line 2143719
    iput-object p1, p0, LX/EbP;->d:LX/EWc;

    .line 2143720
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143721
    return-object p0
.end method

.method public final a(LX/EbQ;)LX/EbP;
    .locals 1

    .prologue
    .line 2143722
    sget-object v0, LX/EbQ;->c:LX/EbQ;

    move-object v0, v0

    .line 2143723
    if-ne p1, v0, :cond_0

    .line 2143724
    :goto_0
    return-object p0

    .line 2143725
    :cond_0
    invoke-virtual {p1}, LX/EbQ;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2143726
    iget v0, p1, LX/EbQ;->id_:I

    move v0, v0

    .line 2143727
    invoke-virtual {p0, v0}, LX/EbP;->a(I)LX/EbP;

    .line 2143728
    :cond_1
    invoke-virtual {p1}, LX/EbQ;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2143729
    iget v0, p1, LX/EbQ;->iteration_:I

    move v0, v0

    .line 2143730
    invoke-virtual {p0, v0}, LX/EbP;->b(I)LX/EbP;

    .line 2143731
    :cond_2
    invoke-virtual {p1}, LX/EbQ;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2143732
    iget-object v0, p1, LX/EbQ;->ciphertext_:LX/EWc;

    move-object v0, v0

    .line 2143733
    invoke-virtual {p0, v0}, LX/EbP;->a(LX/EWc;)LX/EbP;

    .line 2143734
    :cond_3
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2143735
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2143673
    invoke-direct {p0, p1, p2}, LX/EbP;->d(LX/EWd;LX/EYZ;)LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2143736
    invoke-direct {p0}, LX/EbP;->w()LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)LX/EbP;
    .locals 1

    .prologue
    .line 2143669
    iget v0, p0, LX/EbP;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EbP;->a:I

    .line 2143670
    iput p1, p0, LX/EbP;->c:I

    .line 2143671
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143672
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2143668
    invoke-direct {p0, p1, p2}, LX/EbP;->d(LX/EWd;LX/EYZ;)LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2143667
    invoke-direct {p0}, LX/EbP;->w()LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2143666
    invoke-direct {p0, p1}, LX/EbP;->d(LX/EWY;)LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2143665
    invoke-direct {p0}, LX/EbP;->w()LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2143664
    sget-object v0, LX/EbV;->h:LX/EYn;

    const-class v1, LX/EbQ;

    const-class v2, LX/EbP;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2143663
    sget-object v0, LX/EbV;->g:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2143662
    invoke-direct {p0}, LX/EbP;->w()LX/EbP;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2143661
    invoke-direct {p0}, LX/EbP;->y()LX/EbQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2143660
    invoke-virtual {p0}, LX/EbP;->l()LX/EbQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2143659
    invoke-direct {p0}, LX/EbP;->y()LX/EbQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2143658
    invoke-virtual {p0}, LX/EbP;->l()LX/EbQ;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EbQ;
    .locals 2

    .prologue
    .line 2143654
    invoke-direct {p0}, LX/EbP;->y()LX/EbQ;

    move-result-object v0

    .line 2143655
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2143656
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2143657
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2143652
    sget-object v0, LX/EbQ;->c:LX/EbQ;

    move-object v0, v0

    .line 2143653
    return-object v0
.end method
