.class public final LX/DkV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Dka;


# direct methods
.method public constructor <init>(LX/Dka;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2034283
    iput-object p1, p0, LX/DkV;->c:LX/Dka;

    iput-object p2, p0, LX/DkV;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DkV;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2034284
    iget-object v0, p0, LX/DkV;->c:LX/Dka;

    iget-object v0, v0, LX/Dka;->c:LX/Dih;

    const-string v1, "admin_schedule_appointment"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/DkV;->a:Ljava/lang/String;

    iget-object v4, p0, LX/DkV;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Dih;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2034285
    iget-object v0, p0, LX/DkV;->c:LX/Dka;

    iget-object v0, v0, LX/Dka;->d:LX/03V;

    const-class v1, LX/Dka;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "admin_schedule_appointment: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/DkV;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034286
    iget-object v0, p0, LX/DkV;->c:LX/Dka;

    iget-object v0, v0, LX/Dka;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08003c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2034287
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2034288
    return-void
.end method
