.class public LX/DfX;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2027422
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2027423
    invoke-direct {p0}, LX/DfX;->a()V

    .line 2027424
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2027419
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2027420
    invoke-direct {p0}, LX/DfX;->a()V

    .line 2027421
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2027416
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2027417
    invoke-direct {p0}, LX/DfX;->a()V

    .line 2027418
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2027412
    const v0, 0x7f0308f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2027413
    const v0, 0x7f0d1711

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DfX;->a:Landroid/widget/TextView;

    .line 2027414
    const v0, 0x7f0d1712

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DfX;->b:Landroid/widget/TextView;

    .line 2027415
    return-void
.end method

.method private getBadgeText()Ljava/lang/CharSequence;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2027391
    iget-object v1, p0, LX/DfX;->c:Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;

    iget-boolean v1, v1, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->b:Z

    if-nez v1, :cond_1

    .line 2027392
    :cond_0
    :goto_0
    return-object v0

    .line 2027393
    :cond_1
    iget-object v1, p0, LX/DfX;->c:Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;

    iget-object v1, v1, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2027394
    iget-object v0, p0, LX/DfX;->c:Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->c:Ljava/lang/String;

    goto :goto_0

    .line 2027395
    :cond_2
    iget-object v1, p0, LX/DfX;->c:Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;

    iget v1, v1, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->j:I

    if-lez v1, :cond_0

    .line 2027396
    invoke-direct {p0}, LX/DfX;->getBadgeTextResId()I

    move-result v1

    .line 2027397
    if-eqz v1, :cond_0

    .line 2027398
    invoke-virtual {p0}, LX/DfX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/DfX;->c:Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;

    iget v4, v4, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getBadgeTextResId()I
    .locals 2

    .prologue
    .line 2027407
    iget-object v0, p0, LX/DfX;->c:Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;

    iget-object v0, v0, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->q()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2027408
    sget-object v0, LX/DfW;->a:[I

    iget-object v1, p0, LX/DfX;->c:Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;

    iget-object v1, v1, Lcom/facebook/messaging/inbox2/items/InboxUnitItem;->h:Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/inbox2/graphql/InboxV2QueryModels$InboxV2QueryModel$MessengerInboxUnitsModel$NodesModel;->q()Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2027409
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2027410
    :pswitch_0
    const v0, 0x7f082ed5

    goto :goto_0

    .line 2027411
    :pswitch_1
    const v0, 0x7f082ed6

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public setUnit(Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2027399
    iput-object p1, p0, LX/DfX;->c:Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;

    .line 2027400
    iget-object v0, p0, LX/DfX;->a:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2027401
    iget-object v2, p0, LX/DfX;->a:Landroid/widget/TextView;

    iget-boolean v0, p1, Lcom/facebook/messaging/inbox2/hiding/HiddenUnitInboxItem;->k:Z

    if-eqz v0, :cond_1

    const v0, 0x7f020cc2

    :goto_0
    invoke-virtual {v2, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2027402
    invoke-direct {p0}, LX/DfX;->getBadgeText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2027403
    iget-object v2, p0, LX/DfX;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2027404
    iget-object v2, p0, LX/DfX;->b:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const/16 v1, 0x8

    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2027405
    return-void

    :cond_1
    move v0, v1

    .line 2027406
    goto :goto_0
.end method
