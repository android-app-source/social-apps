.class public final LX/EAZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5tj;

.field public final synthetic b:Z

.field public final synthetic c:LX/EAa;


# direct methods
.method public constructor <init>(LX/EAa;LX/5tj;Z)V
    .locals 0

    .prologue
    .line 2085739
    iput-object p1, p0, LX/EAZ;->c:LX/EAa;

    iput-object p2, p0, LX/EAZ;->a:LX/5tj;

    iput-boolean p3, p0, LX/EAZ;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2085697
    iget-object v1, p0, LX/EAZ;->a:LX/5tj;

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->a(LX/5tj;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    move-result-object v1

    iget-boolean v2, p0, LX/EAZ;->b:Z

    .line 2085698
    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v3

    .line 2085699
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;->a()I

    move-result v5

    if-eqz v2, :cond_0

    const/4 v4, 0x1

    :goto_0
    add-int/2addr v4, v5

    .line 2085700
    new-instance v5, LX/4aJ;

    invoke-direct {v5}, LX/4aJ;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;

    move-result-object v5

    .line 2085701
    new-instance p1, LX/4aJ;

    invoke-direct {p1}, LX/4aJ;-><init>()V

    .line 2085702
    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;->a()I

    move-result v0

    iput v0, p1, LX/4aJ;->a:I

    .line 2085703
    move-object v5, p1

    .line 2085704
    iput v4, v5, LX/4aJ;->a:I

    .line 2085705
    move-object v4, v5

    .line 2085706
    invoke-virtual {v4}, LX/4aJ;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;

    move-result-object v4

    .line 2085707
    new-instance v5, LX/4aI;

    invoke-direct {v5}, LX/4aI;-><init>()V

    .line 2085708
    new-instance v5, LX/4aI;

    invoke-direct {v5}, LX/4aI;-><init>()V

    .line 2085709
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->b()Z

    move-result p1

    iput-boolean p1, v5, LX/4aI;->a:Z

    .line 2085710
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->c()Z

    move-result p1

    iput-boolean p1, v5, LX/4aI;->b:Z

    .line 2085711
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->d()Z

    move-result p1

    iput-boolean p1, v5, LX/4aI;->c:Z

    .line 2085712
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->e()Z

    move-result p1

    iput-boolean p1, v5, LX/4aI;->d:Z

    .line 2085713
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->r_()Z

    move-result p1

    iput-boolean p1, v5, LX/4aI;->e:Z

    .line 2085714
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->s_()Z

    move-result p1

    iput-boolean p1, v5, LX/4aI;->f:Z

    .line 2085715
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->j()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v5, LX/4aI;->g:Ljava/lang/String;

    .line 2085716
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->k()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v5, LX/4aI;->h:Ljava/lang/String;

    .line 2085717
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;

    move-result-object p1

    iput-object p1, v5, LX/4aI;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;

    .line 2085718
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$TopLevelCommentsModel;

    move-result-object p1

    iput-object p1, v5, LX/4aI;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$TopLevelCommentsModel;

    .line 2085719
    move-object v5, v5

    .line 2085720
    iput-boolean v2, v5, LX/4aI;->f:Z

    .line 2085721
    move-object v5, v5

    .line 2085722
    iput-object v4, v5, LX/4aI;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;

    .line 2085723
    move-object v4, v5

    .line 2085724
    invoke-virtual {v4}, LX/4aI;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v4

    move-object v3, v4

    .line 2085725
    new-instance v4, LX/5tu;

    invoke-direct {v4}, LX/5tu;-><init>()V

    invoke-static {v1}, LX/5tu;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;)LX/5tu;

    move-result-object v4

    .line 2085726
    iput-object v3, v4, LX/5tu;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 2085727
    move-object v3, v4

    .line 2085728
    invoke-virtual {v3}, LX/5tu;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    move-result-object v3

    move-object v3, v3

    .line 2085729
    move-object v0, v3

    .line 2085730
    iget-object v1, p0, LX/EAZ;->c:LX/EAa;

    iget-object v1, v1, LX/EAa;->a:LX/E9b;

    .line 2085731
    iget-object v2, v1, LX/E9b;->a:LX/E9e;

    invoke-static {v2, v0}, LX/E9e;->d(LX/E9e;LX/5tj;)V

    .line 2085732
    return-void

    .line 2085733
    :cond_0
    const/4 v4, -0x1

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2085734
    iget-object v0, p0, LX/EAZ;->c:LX/EAa;

    iget-object v0, v0, LX/EAa;->a:LX/E9b;

    iget-object v1, p0, LX/EAZ;->a:LX/5tj;

    .line 2085735
    iget-object p0, v0, LX/E9b;->a:LX/E9e;

    invoke-static {p0, v1}, LX/E9e;->d(LX/E9e;LX/5tj;)V

    .line 2085736
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2085737
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2085738
    return-void
.end method
