.class public final LX/Did;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

.field public final synthetic b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V
    .locals 0

    .prologue
    .line 2032224
    iput-object p1, p0, LX/Did;->b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    iput-object p2, p0, LX/Did;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 2032225
    iget-object v0, p0, LX/Did;->b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->r:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Did;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2032226
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2032227
    invoke-static {p1}, LX/DkQ;->f(Ljava/lang/String;)LX/DkQ;

    move-result-object v1

    .line 2032228
    iget-object v0, p0, LX/Did;->b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2032229
    iget-boolean v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v2

    .line 2032230
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/Did;->b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    iget-object v0, p0, LX/Did;->b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v3, p0, LX/Did;->b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    invoke-virtual {v3}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "thread_booking_requests"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v0, v3}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2032231
    :goto_0
    const-string v1, "referrer"

    const-string v2, "list"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2032232
    iget-object v1, p0, LX/Did;->b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->r:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    iget-object v3, p0, LX/Did;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2032233
    return-void

    .line 2032234
    :cond_0
    iget-object v0, p0, LX/Did;->b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    iget-object v2, p0, LX/Did;->b:Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "thread_booking_requests"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
