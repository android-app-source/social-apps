.class public LX/CzO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/D0G;

.field public final d:LX/0RL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RL",
            "<TF;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Rf;LX/D0G;LX/0RL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ">;",
            "LX/D0G;",
            "LX/0RL",
            "<TF;>;)V"
        }
    .end annotation

    .prologue
    .line 1954886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954887
    iput-object p1, p0, LX/CzO;->b:LX/0Rf;

    .line 1954888
    iput-object p2, p0, LX/CzO;->c:LX/D0G;

    .line 1954889
    iput-object p3, p0, LX/CzO;->d:LX/0RL;

    .line 1954890
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/D0G;",
            "LX/0RL",
            "<TF;>;)V"
        }
    .end annotation

    .prologue
    .line 1954891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954892
    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/CzO;->b:LX/0Rf;

    .line 1954893
    iput-object p2, p0, LX/CzO;->c:LX/D0G;

    .line 1954894
    iput-object p3, p0, LX/CzO;->d:LX/0RL;

    .line 1954895
    return-void
.end method
