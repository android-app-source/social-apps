.class public LX/DAD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1970895
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "contactlogs/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1970896
    sput-object v0, LX/DAD;->a:LX/0Tn;

    const-string v1, "upload_enabled/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/DAD;->c:LX/0Tn;

    .line 1970897
    sget-object v0, LX/DAD;->a:LX/0Tn;

    const-string v1, "last_upload_attempt_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/DAD;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970894
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1970892
    sget-object v0, LX/DAD;->b:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
