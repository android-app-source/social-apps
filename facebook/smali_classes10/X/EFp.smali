.class public LX/EFp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/0wT;


# instance fields
.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0wW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Landroid/view/GestureDetector;

.field public final g:LX/EGb;

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:Z

.field public o:Z

.field public p:LX/0wd;

.field public q:LX/0wd;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2096462
    const-class v0, LX/EFp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EFp;->a:Ljava/lang/String;

    .line 2096463
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/EFp;->b:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/EGb;)V
    .locals 1
    .param p1    # LX/EGb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2096457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096458
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2096459
    iput-object v0, p0, LX/EFp;->c:LX/0Ot;

    .line 2096460
    iput-object p1, p0, LX/EFp;->g:LX/EGb;

    .line 2096461
    return-void
.end method

.method public static a$redex0(LX/EFp;I)I
    .locals 2

    .prologue
    .line 2096464
    iget-object v0, p0, LX/EFp;->g:LX/EGb;

    invoke-virtual {v0}, LX/EGb;->g()Landroid/graphics/Rect;

    move-result-object v0

    .line 2096465
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public static synthetic e(LX/EFp;I)I
    .locals 1

    .prologue
    .line 2096456
    invoke-static {p0, p1}, LX/EFp;->a$redex0(LX/EFp;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final g()V
    .locals 3

    .prologue
    .line 2096448
    iget-object v0, p0, LX/EFp;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2096449
    iget-object v1, v0, LX/EDx;->ck:LX/EDt;

    move-object v1, v1

    .line 2096450
    iget-object v0, p0, LX/EFp;->g:LX/EGb;

    invoke-virtual {v0}, LX/EGb;->g()Landroid/graphics/Rect;

    move-result-object v2

    .line 2096451
    invoke-virtual {v1}, LX/EDt;->isLeft()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, v2, Landroid/graphics/Rect;->left:I

    :goto_0
    iput v0, p0, LX/EFp;->h:I

    .line 2096452
    invoke-virtual {v1}, LX/EDt;->isTop()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, v2, Landroid/graphics/Rect;->top:I

    :goto_1
    iput v0, p0, LX/EFp;->i:I

    .line 2096453
    return-void

    .line 2096454
    :cond_0
    iget v0, v2, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 2096455
    :cond_1
    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_1
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 2096427
    iget-object v0, p0, LX/EFp;->g:LX/EGb;

    .line 2096428
    iget-object v1, v0, LX/EGb;->a:LX/EGe;

    iget-boolean v1, v1, LX/EGe;->S:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2096429
    if-nez v0, :cond_1

    .line 2096430
    const/4 v0, 0x0

    .line 2096431
    :cond_0
    :goto_1
    return v0

    .line 2096432
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float v1, v0, v1

    .line 2096433
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float v2, v0, v2

    .line 2096434
    invoke-virtual {p2, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2096435
    iget-object v0, p0, LX/EFp;->f:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2096436
    neg-float v1, v1

    neg-float v2, v2

    invoke-virtual {p2, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2096437
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 2096438
    :pswitch_1
    iget-object v1, p0, LX/EFp;->g:LX/EGb;

    .line 2096439
    iget-object v2, v1, LX/EGb;->a:LX/EGe;

    invoke-static {v2}, LX/EGe;->at(LX/EGe;)V

    .line 2096440
    goto :goto_1

    .line 2096441
    :pswitch_2
    if-nez v0, :cond_0

    .line 2096442
    iget-object v1, p0, LX/EFp;->g:LX/EGb;

    .line 2096443
    iget-object v2, v1, LX/EGb;->a:LX/EGe;

    invoke-static {v2}, LX/EGe;->av(LX/EGe;)V

    .line 2096444
    goto :goto_1

    .line 2096445
    :pswitch_3
    iget-object v1, p0, LX/EFp;->g:LX/EGb;

    .line 2096446
    iget-object v2, v1, LX/EGb;->a:LX/EGe;

    invoke-static {v2}, LX/EGe;->av(LX/EGe;)V

    .line 2096447
    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
