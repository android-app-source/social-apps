.class public LX/Day;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;

.field public b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2016691
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2016692
    const-class p1, LX/Day;

    invoke-static {p1, p0}, LX/Day;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2016693
    const p1, 0x7f030ffc

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2016694
    const p1, 0x7f0d2678

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/Day;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2016695
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Day;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object p0, p1, LX/Day;->a:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public setIcon(I)V
    .locals 5

    .prologue
    .line 2016696
    iget-object v0, p0, LX/Day;->c:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 2016697
    invoke-virtual {p0}, LX/Day;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030ffb

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2016698
    const v0, 0x7f0d2677

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Day;->c:Landroid/widget/ImageView;

    .line 2016699
    :cond_0
    iget-object v0, p0, LX/Day;->c:Landroid/widget/ImageView;

    iget-object v1, p0, LX/Day;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2016700
    iget-object v0, p0, LX/Day;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2016701
    iget-object v1, p0, LX/Day;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b2058

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2016702
    iget-object v1, p0, LX/Day;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b2058

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v0, v1}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 2016703
    iget-object v1, p0, LX/Day;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2016704
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2016705
    iget-object v0, p0, LX/Day;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2016706
    return-void
.end method
