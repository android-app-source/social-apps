.class public final enum LX/Dxk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dxk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dxk;

.field public static final enum CONTRIBUTOR:LX/Dxk;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2063189
    new-instance v0, LX/Dxk;

    const-string v1, "CONTRIBUTOR"

    invoke-direct {v0, v1, v2}, LX/Dxk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dxk;->CONTRIBUTOR:LX/Dxk;

    .line 2063190
    const/4 v0, 0x1

    new-array v0, v0, [LX/Dxk;

    sget-object v1, LX/Dxk;->CONTRIBUTOR:LX/Dxk;

    aput-object v1, v0, v2

    sput-object v0, LX/Dxk;->$VALUES:[LX/Dxk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2063191
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dxk;
    .locals 1

    .prologue
    .line 2063192
    const-class v0, LX/Dxk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dxk;

    return-object v0
.end method

.method public static values()[LX/Dxk;
    .locals 1

    .prologue
    .line 2063193
    sget-object v0, LX/Dxk;->$VALUES:[LX/Dxk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dxk;

    return-object v0
.end method
