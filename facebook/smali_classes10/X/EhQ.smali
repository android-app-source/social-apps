.class public final LX/EhQ;
.super LX/2lH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        ">",
        "LX/2lH",
        "<",
        "LX/EhS;",
        "TD;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

.field private f:I

.field private g:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Ljava/lang/Object;ILjava/lang/CharSequence;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2lb;",
            "TD;I",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2158409
    iput-object p1, p0, LX/EhQ;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    .line 2158410
    const v0, 0x7f03024d

    iget-object v1, p1, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b:Landroid/view/LayoutInflater;

    invoke-direct {p0, p2, v0, p3, v1}, LX/2lH;-><init>(LX/2lb;ILjava/lang/Object;Landroid/view/LayoutInflater;)V

    .line 2158411
    iput p4, p0, LX/EhQ;->f:I

    .line 2158412
    iput-object p5, p0, LX/EhQ;->g:Ljava/lang/CharSequence;

    .line 2158413
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2158422
    new-instance v0, LX/EhS;

    check-cast p1, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-direct {v0, p1}, LX/EhS;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2158414
    check-cast p1, LX/EhS;

    .line 2158415
    iget v0, p0, LX/EhQ;->f:I

    if-gez v0, :cond_0

    .line 2158416
    iget-object v0, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2158417
    :goto_0
    iget-object v0, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, LX/EhQ;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2158418
    iget-object v0, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, LX/EhQ;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2158419
    return-void

    .line 2158420
    :cond_0
    iget-object v0, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2158421
    iget-object v0, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget v1, p0, LX/EhQ;->f:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    goto :goto_0
.end method
