.class public LX/ERW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/ERX;",
        "Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0lC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121032
    const-class v0, LX/ERW;

    sput-object v0, LX/ERW;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2121029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121030
    iput-object p1, p0, LX/ERW;->b:LX/0lC;

    .line 2121031
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2121011
    check-cast p1, LX/ERX;

    .line 2121012
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2121013
    new-instance v0, Lorg/json/JSONArray;

    .line 2121014
    iget-object v1, p1, LX/ERX;->b:Ljava/util/Set;

    move-object v1, v1

    .line 2121015
    invoke-direct {v0, v1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 2121016
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "hashes"

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2121017
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "time_start"

    .line 2121018
    iget-wide v6, p1, LX/ERX;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    .line 2121019
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2121020
    new-instance v0, LX/14N;

    const-string v1, "vaultGetSyncedImageStatus"

    const-string v2, "GET"

    const-string v3, "%s/vaultlocalimages"

    .line 2121021
    iget-wide v6, p1, LX/ERX;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 2121022
    invoke-static {v3, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2121023
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2121024
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    .line 2121025
    iget-object v1, p0, LX/ERW;->b:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 2121026
    const-string v2, "data"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "data"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2121027
    new-instance v0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;

    invoke-direct {v0}, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;-><init>()V

    .line 2121028
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/ERW;->b:LX/0lC;

    const-class v2, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;

    goto :goto_0
.end method
