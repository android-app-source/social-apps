.class public LX/DPB;
.super LX/DPA;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/shortcuts/InstallShortcutHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;LX/3mF;LX/0Ot;LX/DPS;LX/0Ot;LX/0Ot;LX/3my;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/3mF;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/settings/GroupSubscriptionController;",
            ">;",
            "LX/DPS;",
            "LX/0Ot",
            "<",
            "LX/DPK;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/shortcuts/InstallShortcutHelper;",
            ">;",
            "LX/3my;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1993132
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/DPA;-><init>(LX/0Sh;LX/3mF;LX/0Ot;LX/DPS;LX/0Ot;LX/3my;)V

    .line 1993133
    iput-object p6, p0, LX/DPB;->a:LX/0Ot;

    .line 1993134
    return-void
.end method

.method public static a(LX/0QB;)LX/DPB;
    .locals 1

    .prologue
    .line 1993131
    invoke-static {p0}, LX/DPB;->b(LX/0QB;)LX/DPB;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DPB;
    .locals 8

    .prologue
    .line 1993129
    new-instance v0, LX/DPB;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v2

    check-cast v2, LX/3mF;

    const/16 v3, 0x24a8

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/DPS;->b(LX/0QB;)LX/DPS;

    move-result-object v4

    check-cast v4, LX/DPS;

    const/16 v5, 0x2450

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1926

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v7

    check-cast v7, LX/3my;

    invoke-direct/range {v0 .. v7}, LX/DPB;-><init>(LX/0Sh;LX/3mF;LX/0Ot;LX/DPS;LX/0Ot;LX/0Ot;LX/3my;)V

    .line 1993130
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1993126
    sget-object v0, LX/0ax;->C:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1993127
    iget-object v0, p0, LX/DPB;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    sget-object v2, LX/46b;->ROUNDED:LX/46b;

    invoke-virtual {v0, v1, p3, p1, v2}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/46b;)V

    .line 1993128
    return-void
.end method
