.class public final LX/DSO;
.super LX/DRp;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V
    .locals 0

    .prologue
    .line 1999361
    iput-object p1, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    invoke-direct {p0}, LX/DRp;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1999362
    check-cast p1, LX/DTl;

    const/4 v1, 0x1

    .line 1999363
    iget-object v0, p1, LX/DTl;->a:Ljava/lang/String;

    iget-object v2, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    .line 1999364
    iget-object v3, v2, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1999365
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1999366
    :goto_0
    return-void

    .line 1999367
    :cond_0
    const/4 v0, 0x0

    .line 1999368
    iget-object v2, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    iget-object v3, p1, LX/DTl;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1999369
    iget-object v0, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    iget-object v2, p1, LX/DTl;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move v0, v1

    .line 1999370
    :cond_1
    iget-object v2, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    iget-object v3, p1, LX/DTl;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1999371
    iget-object v0, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    iget-object v2, p1, LX/DTl;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move v0, v1

    .line 1999372
    :cond_2
    if-eqz v0, :cond_3

    .line 1999373
    iget-object v0, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->H(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    .line 1999374
    :cond_3
    iget-object v0, p0, LX/DSO;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v1, p1, LX/DTl;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->c(Lcom/facebook/groups/memberlist/GroupMemberListFragment;Ljava/lang/String;)V

    goto :goto_0
.end method
