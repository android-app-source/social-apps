.class public LX/CqV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/CqV;


# instance fields
.field public a:Z

.field public b:Z

.field public c:LX/CqU;

.field public d:Landroid/view/View;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/CqU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1939922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1939923
    iput-boolean v0, p0, LX/CqV;->a:Z

    .line 1939924
    iput-boolean v0, p0, LX/CqV;->b:Z

    .line 1939925
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CqV;->e:Ljava/util/Set;

    .line 1939926
    return-void
.end method

.method public static a(LX/0QB;)LX/CqV;
    .locals 3

    .prologue
    .line 1939910
    sget-object v0, LX/CqV;->f:LX/CqV;

    if-nez v0, :cond_1

    .line 1939911
    const-class v1, LX/CqV;

    monitor-enter v1

    .line 1939912
    :try_start_0
    sget-object v0, LX/CqV;->f:LX/CqV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1939913
    if-eqz v2, :cond_0

    .line 1939914
    :try_start_1
    new-instance v0, LX/CqV;

    invoke-direct {v0}, LX/CqV;-><init>()V

    .line 1939915
    move-object v0, v0

    .line 1939916
    sput-object v0, LX/CqV;->f:LX/CqV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1939917
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1939918
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1939919
    :cond_1
    sget-object v0, LX/CqV;->f:LX/CqV;

    return-object v0

    .line 1939920
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1939921
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ZLX/CqU;)V
    .locals 1

    .prologue
    .line 1939897
    if-eqz p1, :cond_0

    .line 1939898
    iget-object v0, p0, LX/CqV;->e:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1939899
    :goto_0
    iget-object v0, p0, LX/CqV;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    iput-boolean v0, p0, LX/CqV;->a:Z

    .line 1939900
    return-void

    .line 1939901
    :cond_0
    iget-object v0, p0, LX/CqV;->e:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(ZLX/CqU;Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1939902
    if-eqz p1, :cond_0

    .line 1939903
    iput-object v0, p0, LX/CqV;->c:LX/CqU;

    .line 1939904
    iput-object v0, p0, LX/CqV;->d:Landroid/view/View;

    .line 1939905
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqV;->b:Z

    .line 1939906
    :goto_0
    return-void

    .line 1939907
    :cond_0
    iput-object p2, p0, LX/CqV;->c:LX/CqU;

    .line 1939908
    iput-object p3, p0, LX/CqV;->d:Landroid/view/View;

    .line 1939909
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CqV;->b:Z

    goto :goto_0
.end method
