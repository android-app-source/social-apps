.class public LX/E3C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1DR;


# direct methods
.method public constructor <init>(LX/1DR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2073751
    iput-object p1, p0, LX/E3C;->a:LX/1DR;

    .line 2073752
    return-void
.end method

.method private static a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 2073753
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1625

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public static a(LX/0QB;)LX/E3C;
    .locals 4

    .prologue
    .line 2073739
    const-class v1, LX/E3C;

    monitor-enter v1

    .line 2073740
    :try_start_0
    sget-object v0, LX/E3C;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073741
    sput-object v2, LX/E3C;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073742
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073743
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073744
    new-instance p0, LX/E3C;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v3

    check-cast v3, LX/1DR;

    invoke-direct {p0, v3}, LX/E3C;-><init>(LX/1DR;)V

    .line 2073745
    move-object v0, p0

    .line 2073746
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073747
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/E3C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073748
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073749
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)I
    .locals 3

    .prologue
    .line 2073714
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_SQUARE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p2, v0, :cond_0

    .line 2073715
    iget-object v0, p0, LX/E3C;->a:LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    .line 2073716
    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1626

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2073717
    :goto_0
    return v0

    .line 2073718
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p2, v0, :cond_1

    .line 2073719
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1629

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    .line 2073720
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_EM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq p2, v0, :cond_2

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p2, v0, :cond_3

    .line 2073721
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1628

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0

    .line 2073722
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_CREATE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq p2, v0, :cond_4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGE_PHOTO_ALBUM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p2, v0, :cond_5

    .line 2073723
    :cond_4
    iget-object v0, p0, LX/E3C;->a:LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    .line 2073724
    div-int/lit8 v0, v0, 0x3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1626

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 2073725
    :cond_5
    invoke-static {p1}, LX/E3C;->a(Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)I
    .locals 3

    .prologue
    .line 2073726
    const/4 v1, 0x0

    .line 2073727
    invoke-static {p2}, LX/Cfu;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    .line 2073728
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    move-object v0, v1

    .line 2073729
    :goto_0
    move-object v0, v0

    .line 2073730
    if-nez v0, :cond_1

    .line 2073731
    invoke-static {p1}, LX/E3C;->a(Landroid/content/Context;)I

    move-result v0

    .line 2073732
    :goto_1
    return v0

    :cond_1
    invoke-virtual {p0, p1, v0}, LX/E3C;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)I

    move-result v0

    goto :goto_1

    .line 2073733
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v0

    .line 2073734
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2073735
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v0

    .line 2073736
    if-eqz v0, :cond_3

    .line 2073737
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 2073738
    goto :goto_0
.end method
