.class public final LX/Ec1;
.super LX/EWp;
.source ""

# interfaces
.implements LX/Ebz;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ec1;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/Ec1;


# instance fields
.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public senderKeyStates_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EcH;",
            ">;"
        }
    .end annotation
.end field

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2145291
    new-instance v0, LX/Eby;

    invoke-direct {v0}, LX/Eby;-><init>()V

    sput-object v0, LX/Ec1;->a:LX/EWZ;

    .line 2145292
    new-instance v0, LX/Ec1;

    invoke-direct {v0}, LX/Ec1;-><init>()V

    .line 2145293
    sput-object v0, LX/Ec1;->c:LX/Ec1;

    invoke-direct {v0}, LX/Ec1;->o()V

    .line 2145294
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2145286
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145287
    iput-byte v0, p0, LX/Ec1;->memoizedIsInitialized:B

    .line 2145288
    iput v0, p0, LX/Ec1;->memoizedSerializedSize:I

    .line 2145289
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2145290
    iput-object v0, p0, LX/Ec1;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 2145252
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145253
    iput-byte v1, p0, LX/Ec1;->memoizedIsInitialized:B

    .line 2145254
    iput v1, p0, LX/Ec1;->memoizedSerializedSize:I

    .line 2145255
    invoke-direct {p0}, LX/Ec1;->o()V

    .line 2145256
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v3

    move v1, v0

    .line 2145257
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 2145258
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v4

    .line 2145259
    sparse-switch v4, :sswitch_data_0

    .line 2145260
    invoke-virtual {p0, p1, v3, p2, v4}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 2145261
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 2145262
    goto :goto_0

    .line 2145263
    :sswitch_1
    and-int/lit8 v4, v0, 0x1

    if-eq v4, v2, :cond_1

    .line 2145264
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    .line 2145265
    or-int/lit8 v0, v0, 0x1

    .line 2145266
    :cond_1
    iget-object v4, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    sget-object v5, LX/EcH;->a:LX/EWZ;

    invoke-virtual {p1, v5, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2145267
    :catch_0
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 2145268
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145269
    move-object v0, v0

    .line 2145270
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2145271
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 2145272
    iget-object v1, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    .line 2145273
    :cond_2
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/Ec1;->unknownFields:LX/EZQ;

    .line 2145274
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2145275
    :cond_3
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 2145276
    iget-object v0, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    .line 2145277
    :cond_4
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ec1;->unknownFields:LX/EZQ;

    .line 2145278
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2145279
    return-void

    .line 2145280
    :catch_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 2145281
    :try_start_2
    new-instance v4, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2145282
    iput-object p0, v4, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145283
    move-object v0, v4

    .line 2145284
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2145285
    :catchall_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2145247
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2145248
    iput-byte v1, p0, LX/Ec1;->memoizedIsInitialized:B

    .line 2145249
    iput v1, p0, LX/Ec1;->memoizedSerializedSize:I

    .line 2145250
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ec1;->unknownFields:LX/EZQ;

    .line 2145251
    return-void
.end method

.method private static b(LX/Ec1;)LX/Ec0;
    .locals 1

    .prologue
    .line 2145246
    invoke-static {}, LX/Ec0;->u()LX/Ec0;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/Ec0;->a(LX/Ec1;)LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 2145226
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    .line 2145227
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2145244
    new-instance v0, LX/Ec0;

    invoke-direct {v0, p1}, LX/Ec0;-><init>(LX/EYd;)V

    .line 2145245
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    .line 2145238
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2145239
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2145240
    const/4 v2, 0x1

    iget-object v0, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v2, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2145241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2145242
    :cond_0
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2145243
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2145295
    iget-byte v1, p0, LX/Ec1;->memoizedIsInitialized:B

    .line 2145296
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2145297
    :goto_0
    return v0

    .line 2145298
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2145299
    :cond_1
    iput-byte v0, p0, LX/Ec1;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2145230
    iget v1, p0, LX/Ec1;->memoizedSerializedSize:I

    .line 2145231
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 2145232
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 2145233
    :goto_1
    iget-object v0, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2145234
    const/4 v3, 0x1

    iget-object v0, p0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2145235
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2145236
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2145237
    iput v0, p0, LX/Ec1;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2145229
    iget-object v0, p0, LX/Ec1;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2145228
    sget-object v0, LX/Eck;->D:LX/EYn;

    const-class v1, LX/Ec1;

    const-class v2, LX/Ec0;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ec1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2145225
    sget-object v0, LX/Ec1;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2145224
    invoke-static {p0}, LX/Ec1;->b(LX/Ec1;)LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2145223
    invoke-static {}, LX/Ec0;->u()LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2145222
    invoke-static {p0}, LX/Ec1;->b(LX/Ec1;)LX/Ec0;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145221
    sget-object v0, LX/Ec1;->c:LX/Ec1;

    return-object v0
.end method
