.class public LX/ERv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/6Wc;

.field public static final b:LX/6Wc;

.field private static volatile e:LX/ERv;


# instance fields
.field public final c:LX/6Wr;

.field public final d:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2121818
    new-instance v0, LX/6Wc;

    const-string v1, "vault_quota"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/ERv;->a:LX/6Wc;

    .line 2121819
    new-instance v0, LX/6Wc;

    const-string v1, "uid"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/ERv;->b:LX/6Wc;

    return-void
.end method

.method public constructor <init>(LX/6Wr;LX/0TD;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2121820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121821
    iput-object p1, p0, LX/ERv;->c:LX/6Wr;

    .line 2121822
    iput-object p2, p0, LX/ERv;->d:LX/0TD;

    .line 2121823
    return-void
.end method

.method public static a(LX/0QB;)LX/ERv;
    .locals 5

    .prologue
    .line 2121824
    sget-object v0, LX/ERv;->e:LX/ERv;

    if-nez v0, :cond_1

    .line 2121825
    const-class v1, LX/ERv;

    monitor-enter v1

    .line 2121826
    :try_start_0
    sget-object v0, LX/ERv;->e:LX/ERv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2121827
    if-eqz v2, :cond_0

    .line 2121828
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2121829
    new-instance p0, LX/ERv;

    invoke-static {v0}, LX/6Wr;->a(LX/0QB;)LX/6Wr;

    move-result-object v3

    check-cast v3, LX/6Wr;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-direct {p0, v3, v4}, LX/ERv;-><init>(LX/6Wr;LX/0TD;)V

    .line 2121830
    move-object v0, p0

    .line 2121831
    sput-object v0, LX/ERv;->e:LX/ERv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2121832
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2121833
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2121834
    :cond_1
    sget-object v0, LX/ERv;->e:LX/ERv;

    return-object v0

    .line 2121835
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2121836
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
