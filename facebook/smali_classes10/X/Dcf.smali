.class public final LX/Dcf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dce;


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/photos/PhotoCategoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/photos/PhotoCategoryFragment;)V
    .locals 0

    .prologue
    .line 2018660
    iput-object p1, p0, LX/Dcf;->a:Lcom/facebook/localcontent/photos/PhotoCategoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2018662
    iget-object v0, p0, LX/Dcf;->a:Lcom/facebook/localcontent/photos/PhotoCategoryFragment;

    .line 2018663
    iget-object v1, v0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->x:Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->setVisibility(I)V

    .line 2018664
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2018665
    iget-object v1, v0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->x:Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;

    iget-object v2, v0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->v:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->setPrimaryUploadMessage(Ljava/lang/String;)V

    .line 2018666
    iget-object v1, v0, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->x:Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0219f0

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/localcontent/photos/UploadPhotoByCategoryView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2018667
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2018661
    return-void
.end method
