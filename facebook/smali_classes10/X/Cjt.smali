.class public final enum LX/Cjt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cjt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cjt;

.field public static final enum AD_WITHOUT_CAPTION:LX/Cjt;

.field public static final enum AD_WITH_CAPTION:LX/Cjt;

.field public static final enum AUTHORS_CONTRIBUTORS_HEADER:LX/Cjt;

.field public static final enum HTML_WITHOUT_CAPTION:LX/Cjt;

.field public static final enum HTML_WITH_CAPTION:LX/Cjt;

.field public static final enum INLINE_RELATED_ARTICLES_FOOTER:LX/Cjt;

.field public static final enum INLINE_RELATED_ARTICLES_HEADER:LX/Cjt;

.field public static final enum MAP_WITHOUT_CAPTION:LX/Cjt;

.field public static final enum MAP_WITH_CAPTION:LX/Cjt;

.field public static final enum MEDIA_WITHOUT_ABOVE_OR_BELOW_CAPTION:LX/Cjt;

.field public static final enum MEDIA_WITH_ABOVE_AND_BELOW_CAPTION:LX/Cjt;

.field public static final enum MEDIA_WITH_ABOVE_CAPTION:LX/Cjt;

.field public static final enum MEDIA_WITH_BELOW_CAPTION:LX/Cjt;

.field public static final enum NONE:LX/Cjt;

.field public static final enum RELATED_ARTICLES_HEADER:LX/Cjt;

.field public static final enum RELATED_ARTICLE_CELL:LX/Cjt;

.field public static final enum SHARE_BUTTON:LX/Cjt;

.field public static final enum SOCIAL_EMBED_WITHOUT_CAPTION:LX/Cjt;

.field public static final enum SOCIAL_EMBED_WITH_CAPTION:LX/Cjt;

.field public static final enum TEXT_AUTHOR_PIC:LX/Cjt;

.field public static final enum TEXT_BLOCK_QUOTE:LX/Cjt;

.field public static final enum TEXT_BODY:LX/Cjt;

.field public static final enum TEXT_BULLETED_LIST:LX/Cjt;

.field public static final enum TEXT_BYLINE:LX/Cjt;

.field public static final enum TEXT_CAPTION_CREDIT:LX/Cjt;

.field public static final enum TEXT_CAPTION_LARGE:LX/Cjt;

.field public static final enum TEXT_CAPTION_MEDIUM:LX/Cjt;

.field public static final enum TEXT_CAPTION_SMALL:LX/Cjt;

.field public static final enum TEXT_CAPTION_XLARGE:LX/Cjt;

.field public static final enum TEXT_CODE:LX/Cjt;

.field public static final enum TEXT_ELEMENT_UFI:LX/Cjt;

.field public static final enum TEXT_END_CREDITS:LX/Cjt;

.field public static final enum TEXT_END_CREDITS_BAR:LX/Cjt;

.field public static final enum TEXT_H1:LX/Cjt;

.field public static final enum TEXT_H2:LX/Cjt;

.field public static final enum TEXT_KICKER:LX/Cjt;

.field public static final enum TEXT_NUMBERED_LIST:LX/Cjt;

.field public static final enum TEXT_PULL_QUOTE:LX/Cjt;

.field public static final enum TEXT_PULL_QUOTE_ATTRIBUTION:LX/Cjt;

.field public static final enum TEXT_SUBTITLE:LX/Cjt;

.field public static final enum TEXT_TITLE:LX/Cjt;

.field public static final enum UNKNOWN:LX/Cjt;

.field public static final enum VIDEO_SEEK_BAR:LX/Cjt;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1929894
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_KICKER"

    invoke-direct {v0, v1, v3}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_KICKER:LX/Cjt;

    .line 1929895
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_TITLE"

    invoke-direct {v0, v1, v4}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_TITLE:LX/Cjt;

    .line 1929896
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_SUBTITLE"

    invoke-direct {v0, v1, v5}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_SUBTITLE:LX/Cjt;

    .line 1929897
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_BYLINE"

    invoke-direct {v0, v1, v6}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_BYLINE:LX/Cjt;

    .line 1929898
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_AUTHOR_PIC"

    invoke-direct {v0, v1, v7}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_AUTHOR_PIC:LX/Cjt;

    .line 1929899
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_BODY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_BODY:LX/Cjt;

    .line 1929900
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_H1"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_H1:LX/Cjt;

    .line 1929901
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_H2"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_H2:LX/Cjt;

    .line 1929902
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_PULL_QUOTE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_PULL_QUOTE:LX/Cjt;

    .line 1929903
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_PULL_QUOTE_ATTRIBUTION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_PULL_QUOTE_ATTRIBUTION:LX/Cjt;

    .line 1929904
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_BLOCK_QUOTE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_BLOCK_QUOTE:LX/Cjt;

    .line 1929905
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_CODE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_CODE:LX/Cjt;

    .line 1929906
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_CAPTION_SMALL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_CAPTION_SMALL:LX/Cjt;

    .line 1929907
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_CAPTION_MEDIUM"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_CAPTION_MEDIUM:LX/Cjt;

    .line 1929908
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_CAPTION_LARGE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_CAPTION_LARGE:LX/Cjt;

    .line 1929909
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_CAPTION_XLARGE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_CAPTION_XLARGE:LX/Cjt;

    .line 1929910
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_CAPTION_CREDIT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_CAPTION_CREDIT:LX/Cjt;

    .line 1929911
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_ELEMENT_UFI"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_ELEMENT_UFI:LX/Cjt;

    .line 1929912
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_BULLETED_LIST"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_BULLETED_LIST:LX/Cjt;

    .line 1929913
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_NUMBERED_LIST"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_NUMBERED_LIST:LX/Cjt;

    .line 1929914
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_END_CREDITS_BAR"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_END_CREDITS_BAR:LX/Cjt;

    .line 1929915
    new-instance v0, LX/Cjt;

    const-string v1, "TEXT_END_CREDITS"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->TEXT_END_CREDITS:LX/Cjt;

    .line 1929916
    new-instance v0, LX/Cjt;

    const-string v1, "MEDIA_WITH_ABOVE_CAPTION"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->MEDIA_WITH_ABOVE_CAPTION:LX/Cjt;

    .line 1929917
    new-instance v0, LX/Cjt;

    const-string v1, "MEDIA_WITH_BELOW_CAPTION"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->MEDIA_WITH_BELOW_CAPTION:LX/Cjt;

    .line 1929918
    new-instance v0, LX/Cjt;

    const-string v1, "MEDIA_WITH_ABOVE_AND_BELOW_CAPTION"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->MEDIA_WITH_ABOVE_AND_BELOW_CAPTION:LX/Cjt;

    .line 1929919
    new-instance v0, LX/Cjt;

    const-string v1, "MEDIA_WITHOUT_ABOVE_OR_BELOW_CAPTION"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->MEDIA_WITHOUT_ABOVE_OR_BELOW_CAPTION:LX/Cjt;

    .line 1929920
    new-instance v0, LX/Cjt;

    const-string v1, "AD_WITH_CAPTION"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->AD_WITH_CAPTION:LX/Cjt;

    .line 1929921
    new-instance v0, LX/Cjt;

    const-string v1, "AD_WITHOUT_CAPTION"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->AD_WITHOUT_CAPTION:LX/Cjt;

    .line 1929922
    new-instance v0, LX/Cjt;

    const-string v1, "MAP_WITH_CAPTION"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->MAP_WITH_CAPTION:LX/Cjt;

    .line 1929923
    new-instance v0, LX/Cjt;

    const-string v1, "MAP_WITHOUT_CAPTION"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->MAP_WITHOUT_CAPTION:LX/Cjt;

    .line 1929924
    new-instance v0, LX/Cjt;

    const-string v1, "HTML_WITH_CAPTION"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->HTML_WITH_CAPTION:LX/Cjt;

    .line 1929925
    new-instance v0, LX/Cjt;

    const-string v1, "HTML_WITHOUT_CAPTION"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->HTML_WITHOUT_CAPTION:LX/Cjt;

    .line 1929926
    new-instance v0, LX/Cjt;

    const-string v1, "SOCIAL_EMBED_WITH_CAPTION"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->SOCIAL_EMBED_WITH_CAPTION:LX/Cjt;

    .line 1929927
    new-instance v0, LX/Cjt;

    const-string v1, "SOCIAL_EMBED_WITHOUT_CAPTION"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->SOCIAL_EMBED_WITHOUT_CAPTION:LX/Cjt;

    .line 1929928
    new-instance v0, LX/Cjt;

    const-string v1, "RELATED_ARTICLE_CELL"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->RELATED_ARTICLE_CELL:LX/Cjt;

    .line 1929929
    new-instance v0, LX/Cjt;

    const-string v1, "RELATED_ARTICLES_HEADER"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->RELATED_ARTICLES_HEADER:LX/Cjt;

    .line 1929930
    new-instance v0, LX/Cjt;

    const-string v1, "INLINE_RELATED_ARTICLES_HEADER"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->INLINE_RELATED_ARTICLES_HEADER:LX/Cjt;

    .line 1929931
    new-instance v0, LX/Cjt;

    const-string v1, "INLINE_RELATED_ARTICLES_FOOTER"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->INLINE_RELATED_ARTICLES_FOOTER:LX/Cjt;

    .line 1929932
    new-instance v0, LX/Cjt;

    const-string v1, "AUTHORS_CONTRIBUTORS_HEADER"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->AUTHORS_CONTRIBUTORS_HEADER:LX/Cjt;

    .line 1929933
    new-instance v0, LX/Cjt;

    const-string v1, "SHARE_BUTTON"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->SHARE_BUTTON:LX/Cjt;

    .line 1929934
    new-instance v0, LX/Cjt;

    const-string v1, "VIDEO_SEEK_BAR"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->VIDEO_SEEK_BAR:LX/Cjt;

    .line 1929935
    new-instance v0, LX/Cjt;

    const-string v1, "NONE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->NONE:LX/Cjt;

    .line 1929936
    new-instance v0, LX/Cjt;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, LX/Cjt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cjt;->UNKNOWN:LX/Cjt;

    .line 1929937
    const/16 v0, 0x2b

    new-array v0, v0, [LX/Cjt;

    sget-object v1, LX/Cjt;->TEXT_KICKER:LX/Cjt;

    aput-object v1, v0, v3

    sget-object v1, LX/Cjt;->TEXT_TITLE:LX/Cjt;

    aput-object v1, v0, v4

    sget-object v1, LX/Cjt;->TEXT_SUBTITLE:LX/Cjt;

    aput-object v1, v0, v5

    sget-object v1, LX/Cjt;->TEXT_BYLINE:LX/Cjt;

    aput-object v1, v0, v6

    sget-object v1, LX/Cjt;->TEXT_AUTHOR_PIC:LX/Cjt;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Cjt;->TEXT_BODY:LX/Cjt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Cjt;->TEXT_H1:LX/Cjt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Cjt;->TEXT_H2:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Cjt;->TEXT_PULL_QUOTE:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Cjt;->TEXT_PULL_QUOTE_ATTRIBUTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Cjt;->TEXT_BLOCK_QUOTE:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Cjt;->TEXT_CODE:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Cjt;->TEXT_CAPTION_SMALL:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Cjt;->TEXT_CAPTION_MEDIUM:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Cjt;->TEXT_CAPTION_LARGE:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Cjt;->TEXT_CAPTION_XLARGE:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Cjt;->TEXT_CAPTION_CREDIT:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/Cjt;->TEXT_ELEMENT_UFI:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/Cjt;->TEXT_BULLETED_LIST:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/Cjt;->TEXT_NUMBERED_LIST:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/Cjt;->TEXT_END_CREDITS_BAR:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/Cjt;->TEXT_END_CREDITS:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/Cjt;->MEDIA_WITH_ABOVE_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/Cjt;->MEDIA_WITH_BELOW_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/Cjt;->MEDIA_WITH_ABOVE_AND_BELOW_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/Cjt;->MEDIA_WITHOUT_ABOVE_OR_BELOW_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/Cjt;->AD_WITH_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/Cjt;->AD_WITHOUT_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/Cjt;->MAP_WITH_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/Cjt;->MAP_WITHOUT_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/Cjt;->HTML_WITH_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/Cjt;->HTML_WITHOUT_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/Cjt;->SOCIAL_EMBED_WITH_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/Cjt;->SOCIAL_EMBED_WITHOUT_CAPTION:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/Cjt;->RELATED_ARTICLE_CELL:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/Cjt;->RELATED_ARTICLES_HEADER:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/Cjt;->INLINE_RELATED_ARTICLES_HEADER:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/Cjt;->INLINE_RELATED_ARTICLES_FOOTER:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/Cjt;->AUTHORS_CONTRIBUTORS_HEADER:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/Cjt;->SHARE_BUTTON:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/Cjt;->VIDEO_SEEK_BAR:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/Cjt;->NONE:LX/Cjt;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/Cjt;->UNKNOWN:LX/Cjt;

    aput-object v2, v0, v1

    sput-object v0, LX/Cjt;->$VALUES:[LX/Cjt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1929892
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1929893
    return-void
.end method

.method public static from(LX/ClU;)LX/Cjt;
    .locals 2
    .param p0    # LX/ClU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1929868
    if-nez p0, :cond_0

    .line 1929869
    sget-object v0, LX/Cjt;->NONE:LX/Cjt;

    .line 1929870
    :goto_0
    return-object v0

    .line 1929871
    :cond_0
    iget-object v0, p0, LX/ClU;->a:LX/ClT;

    move-object v0, v0

    .line 1929872
    sget-object v1, LX/ClT;->COPYRIGHT:LX/ClT;

    if-ne v0, v1, :cond_1

    .line 1929873
    sget-object v0, LX/Cjt;->TEXT_CAPTION_CREDIT:LX/Cjt;

    goto :goto_0

    .line 1929874
    :cond_1
    iget-object v0, p0, LX/ClU;->a:LX/ClT;

    move-object v0, v0

    .line 1929875
    sget-object v1, LX/ClT;->UFI:LX/ClT;

    if-ne v0, v1, :cond_2

    .line 1929876
    sget-object v0, LX/Cjt;->TEXT_ELEMENT_UFI:LX/Cjt;

    goto :goto_0

    .line 1929877
    :cond_2
    iget-object v0, p0, LX/ClU;->a:LX/ClT;

    move-object v0, v0

    .line 1929878
    sget-object v1, LX/ClT;->VIDEO_SEEK_BAR:LX/ClT;

    if-ne v0, v1, :cond_3

    .line 1929879
    sget-object v0, LX/Cjt;->VIDEO_SEEK_BAR:LX/Cjt;

    goto :goto_0

    .line 1929880
    :cond_3
    invoke-static {p0}, LX/CIg;->a(LX/ClU;)I

    move-result v0

    .line 1929881
    if-nez v0, :cond_4

    .line 1929882
    sget-object v0, LX/Cjt;->NONE:LX/Cjt;

    goto :goto_0

    .line 1929883
    :cond_4
    const v1, 0x7f0e0795

    if-eq v0, v1, :cond_5

    const v1, 0x7f0e0793

    if-ne v0, v1, :cond_6

    .line 1929884
    :cond_5
    sget-object v0, LX/Cjt;->TEXT_CAPTION_SMALL:LX/Cjt;

    goto :goto_0

    .line 1929885
    :cond_6
    const v1, 0x7f0e0796

    if-ne v0, v1, :cond_7

    .line 1929886
    sget-object v0, LX/Cjt;->TEXT_CAPTION_MEDIUM:LX/Cjt;

    goto :goto_0

    .line 1929887
    :cond_7
    const v1, 0x7f0e0797

    if-ne v0, v1, :cond_8

    .line 1929888
    sget-object v0, LX/Cjt;->TEXT_CAPTION_LARGE:LX/Cjt;

    goto :goto_0

    .line 1929889
    :cond_8
    const v1, 0x7f0e0798

    if-ne v0, v1, :cond_9

    .line 1929890
    sget-object v0, LX/Cjt;->TEXT_CAPTION_XLARGE:LX/Cjt;

    goto :goto_0

    .line 1929891
    :cond_9
    sget-object v0, LX/Cjt;->NONE:LX/Cjt;

    goto :goto_0
.end method

.method public static from(LX/Clb;)LX/Cjt;
    .locals 2

    .prologue
    .line 1929847
    if-nez p0, :cond_0

    .line 1929848
    sget-object v0, LX/Cjt;->UNKNOWN:LX/Cjt;

    .line 1929849
    :goto_0
    return-object v0

    .line 1929850
    :cond_0
    sget-object v0, LX/Cjs;->a:[I

    invoke-virtual {p0}, LX/Clb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1929851
    sget-object v0, LX/Cjt;->UNKNOWN:LX/Cjt;

    goto :goto_0

    .line 1929852
    :pswitch_0
    sget-object v0, LX/Cjt;->TEXT_BODY:LX/Cjt;

    goto :goto_0

    .line 1929853
    :pswitch_1
    sget-object v0, LX/Cjt;->TEXT_BLOCK_QUOTE:LX/Cjt;

    goto :goto_0

    .line 1929854
    :pswitch_2
    sget-object v0, LX/Cjt;->TEXT_CAPTION_MEDIUM:LX/Cjt;

    goto :goto_0

    .line 1929855
    :pswitch_3
    sget-object v0, LX/Cjt;->TEXT_CAPTION_MEDIUM:LX/Cjt;

    goto :goto_0

    .line 1929856
    :pswitch_4
    sget-object v0, LX/Cjt;->TEXT_END_CREDITS:LX/Cjt;

    goto :goto_0

    .line 1929857
    :pswitch_5
    sget-object v0, LX/Cjt;->TEXT_H1:LX/Cjt;

    goto :goto_0

    .line 1929858
    :pswitch_6
    sget-object v0, LX/Cjt;->TEXT_H2:LX/Cjt;

    goto :goto_0

    .line 1929859
    :pswitch_7
    sget-object v0, LX/Cjt;->TEXT_PULL_QUOTE:LX/Cjt;

    goto :goto_0

    .line 1929860
    :pswitch_8
    sget-object v0, LX/Cjt;->TEXT_PULL_QUOTE_ATTRIBUTION:LX/Cjt;

    goto :goto_0

    .line 1929861
    :pswitch_9
    sget-object v0, LX/Cjt;->UNKNOWN:LX/Cjt;

    goto :goto_0

    .line 1929862
    :pswitch_a
    sget-object v0, LX/Cjt;->TEXT_SUBTITLE:LX/Cjt;

    goto :goto_0

    .line 1929863
    :pswitch_b
    sget-object v0, LX/Cjt;->TEXT_TITLE:LX/Cjt;

    goto :goto_0

    .line 1929864
    :pswitch_c
    sget-object v0, LX/Cjt;->TEXT_KICKER:LX/Cjt;

    goto :goto_0

    .line 1929865
    :pswitch_d
    sget-object v0, LX/Cjt;->AUTHORS_CONTRIBUTORS_HEADER:LX/Cjt;

    goto :goto_0

    .line 1929866
    :pswitch_e
    sget-object v0, LX/Cjt;->RELATED_ARTICLES_HEADER:LX/Cjt;

    goto :goto_0

    .line 1929867
    :pswitch_f
    sget-object v0, LX/Cjt;->INLINE_RELATED_ARTICLES_HEADER:LX/Cjt;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static from(LX/Clr;)LX/Cjt;
    .locals 4

    .prologue
    .line 1929811
    if-nez p0, :cond_0

    .line 1929812
    sget-object v0, LX/Cjt;->NONE:LX/Cjt;

    .line 1929813
    :goto_0
    return-object v0

    .line 1929814
    :cond_0
    invoke-interface {p0}, LX/Clr;->lx_()I

    move-result v0

    .line 1929815
    invoke-static {p0}, LX/Cjt;->hasAnyAnnotations(LX/Clr;)Z

    move-result v1

    .line 1929816
    invoke-static {p0}, LX/Cjt;->hasAboveAnnotation(LX/Clr;)Z

    move-result v2

    .line 1929817
    invoke-static {p0}, LX/Cjt;->hasBelowAnnotation(LX/Clr;)Z

    move-result v3

    .line 1929818
    packed-switch v0, :pswitch_data_0

    .line 1929819
    :pswitch_0
    sget-object v0, LX/Cjt;->UNKNOWN:LX/Cjt;

    goto :goto_0

    .line 1929820
    :pswitch_1
    check-cast p0, LX/Cmi;

    .line 1929821
    iget-object v0, p0, LX/Cmi;->g:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-object v0, v0

    .line 1929822
    invoke-static {v0, v1}, LX/Cjt;->getTypeFromWebViewBlock(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Z)LX/Cjt;

    move-result-object v0

    goto :goto_0

    .line 1929823
    :pswitch_2
    if-eqz v1, :cond_1

    sget-object v0, LX/Cjt;->MAP_WITH_CAPTION:LX/Cjt;

    goto :goto_0

    :cond_1
    sget-object v0, LX/Cjt;->MAP_WITHOUT_CAPTION:LX/Cjt;

    goto :goto_0

    .line 1929824
    :pswitch_3
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 1929825
    sget-object v0, LX/Cjt;->MEDIA_WITH_ABOVE_AND_BELOW_CAPTION:LX/Cjt;

    goto :goto_0

    .line 1929826
    :cond_2
    if-eqz v2, :cond_3

    .line 1929827
    sget-object v0, LX/Cjt;->MEDIA_WITH_ABOVE_CAPTION:LX/Cjt;

    goto :goto_0

    .line 1929828
    :cond_3
    if-eqz v3, :cond_4

    .line 1929829
    sget-object v0, LX/Cjt;->MEDIA_WITH_BELOW_CAPTION:LX/Cjt;

    goto :goto_0

    .line 1929830
    :cond_4
    sget-object v0, LX/Cjt;->MEDIA_WITHOUT_ABOVE_OR_BELOW_CAPTION:LX/Cjt;

    goto :goto_0

    .line 1929831
    :pswitch_4
    instance-of v0, p0, LX/CmO;

    if-eqz v0, :cond_6

    .line 1929832
    check-cast p0, LX/CmO;

    .line 1929833
    iget-boolean v0, p0, LX/CmO;->b:Z

    move v0, v0

    .line 1929834
    if-eqz v0, :cond_5

    sget-object v0, LX/Cjt;->TEXT_NUMBERED_LIST:LX/Cjt;

    goto :goto_0

    :cond_5
    sget-object v0, LX/Cjt;->TEXT_BULLETED_LIST:LX/Cjt;

    goto :goto_0

    .line 1929835
    :cond_6
    check-cast p0, LX/Cly;

    invoke-interface {p0}, LX/Cly;->f()LX/Clb;

    move-result-object v0

    invoke-static {v0}, LX/Cjt;->from(LX/Clb;)LX/Cjt;

    move-result-object v0

    goto :goto_0

    .line 1929836
    :pswitch_5
    sget-object v0, LX/Cjt;->TEXT_BLOCK_QUOTE:LX/Cjt;

    goto :goto_0

    .line 1929837
    :pswitch_6
    sget-object v0, LX/Cjt;->TEXT_PULL_QUOTE_ATTRIBUTION:LX/Cjt;

    goto :goto_0

    .line 1929838
    :pswitch_7
    sget-object v0, LX/Cjt;->TEXT_CODE:LX/Cjt;

    goto :goto_0

    .line 1929839
    :pswitch_8
    sget-object v0, LX/Cjt;->RELATED_ARTICLE_CELL:LX/Cjt;

    goto :goto_0

    .line 1929840
    :pswitch_9
    sget-object v0, LX/Cjt;->TEXT_BYLINE:LX/Cjt;

    goto :goto_0

    .line 1929841
    :pswitch_a
    sget-object v0, LX/Cjt;->NONE:LX/Cjt;

    goto :goto_0

    .line 1929842
    :pswitch_b
    sget-object v0, LX/Cjt;->SHARE_BUTTON:LX/Cjt;

    goto :goto_0

    .line 1929843
    :pswitch_c
    sget-object v0, LX/Cjt;->NONE:LX/Cjt;

    goto :goto_0

    .line 1929844
    :pswitch_d
    sget-object v0, LX/Cjt;->RELATED_ARTICLES_HEADER:LX/Cjt;

    goto :goto_0

    .line 1929845
    :pswitch_e
    sget-object v0, LX/Cjt;->INLINE_RELATED_ARTICLES_HEADER:LX/Cjt;

    goto :goto_0

    .line 1929846
    :pswitch_f
    sget-object v0, LX/Cjt;->INLINE_RELATED_ARTICLES_FOOTER:LX/Cjt;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_8
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_b
        :pswitch_0
        :pswitch_a
        :pswitch_c
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_d
        :pswitch_5
        :pswitch_7
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_0
        :pswitch_e
        :pswitch_d
    .end packed-switch
.end method

.method private static getTypeFromWebViewBlock(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Z)LX/Cjt;
    .locals 2

    .prologue
    .line 1929806
    sget-object v0, LX/Cjs;->b:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1929807
    if-eqz p1, :cond_2

    sget-object v0, LX/Cjt;->HTML_WITH_CAPTION:LX/Cjt;

    :goto_0
    return-object v0

    .line 1929808
    :pswitch_0
    if-eqz p1, :cond_0

    sget-object v0, LX/Cjt;->AD_WITH_CAPTION:LX/Cjt;

    goto :goto_0

    :cond_0
    sget-object v0, LX/Cjt;->AD_WITHOUT_CAPTION:LX/Cjt;

    goto :goto_0

    .line 1929809
    :pswitch_1
    if-eqz p1, :cond_1

    sget-object v0, LX/Cjt;->SOCIAL_EMBED_WITH_CAPTION:LX/Cjt;

    goto :goto_0

    :cond_1
    sget-object v0, LX/Cjt;->SOCIAL_EMBED_WITHOUT_CAPTION:LX/Cjt;

    goto :goto_0

    .line 1929810
    :cond_2
    sget-object v0, LX/Cjt;->HTML_WITHOUT_CAPTION:LX/Cjt;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static hasAboveAnnotation(LX/Clr;)Z
    .locals 3
    .param p0    # LX/Clr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1929801
    if-eqz p0, :cond_0

    instance-of v1, p0, LX/Clq;

    if-nez v1, :cond_1

    .line 1929802
    :cond_0
    :goto_0
    return v0

    .line 1929803
    :cond_1
    invoke-static {p0}, LX/Cjt;->isDefaultFullscreen(LX/Clr;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, LX/Cjt;->isSlideshow(LX/Clr;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1929804
    :cond_2
    check-cast p0, LX/Clq;

    .line 1929805
    invoke-interface {p0}, LX/Clq;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->ABOVE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    invoke-static {v1, v2}, LX/Cjt;->isAnnotationInSlot(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {p0}, LX/Clq;->iX_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->ABOVE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    invoke-static {v1, v2}, LX/Cjt;->isAnnotationInSlot(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {p0}, LX/Clq;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->ABOVE:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    invoke-static {v1, v2}, LX/Cjt;->isAnnotationInSlot(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static hasAnyAnnotations(LX/Clr;)Z
    .locals 3
    .param p0    # LX/Clr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1929797
    if-eqz p0, :cond_0

    instance-of v1, p0, LX/Clq;

    if-nez v1, :cond_1

    .line 1929798
    :cond_0
    :goto_0
    return v0

    .line 1929799
    :cond_1
    check-cast p0, LX/Clq;

    .line 1929800
    invoke-interface {p0}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-ne v1, v2, :cond_0

    invoke-interface {p0}, LX/Clq;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-interface {p0}, LX/Clq;->iX_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-interface {p0}, LX/Clq;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static hasBelowAnnotation(LX/Clr;)Z
    .locals 4
    .param p0    # LX/Clr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1929791
    if-eqz p0, :cond_0

    instance-of v0, p0, LX/Clq;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1929792
    :goto_0
    return v0

    .line 1929793
    :cond_1
    invoke-static {p0}, LX/Cjt;->isDefaultFullscreen(LX/Clr;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, LX/Cjt;->isSlideshow(LX/Clr;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1929794
    goto :goto_0

    :cond_2
    move-object v0, p0

    .line 1929795
    check-cast v0, LX/Clq;

    .line 1929796
    invoke-static {p0}, LX/Cjt;->hasBelowUfi(LX/Clr;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v0}, LX/Clq;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->BELOW:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    invoke-static {v2, v3}, LX/Cjt;->isAnnotationInSlot(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v0}, LX/Clq;->iX_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->BELOW:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    invoke-static {v2, v3}, LX/Cjt;->isAnnotationInSlot(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v0}, LX/Clq;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->BELOW:Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    invoke-static {v0, v2}, LX/Cjt;->isAnnotationInSlot(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private static hasBelowUfi(LX/Clr;)Z
    .locals 6
    .param p0    # LX/Clr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1929779
    instance-of v0, p0, LX/Clq;

    if-nez v0, :cond_1

    .line 1929780
    :cond_0
    :goto_0
    return v3

    :cond_1
    move-object v0, p0

    .line 1929781
    check-cast v0, LX/Clq;

    .line 1929782
    invoke-interface {v0}, LX/Clq;->j()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->NONE:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    if-eq v1, v4, :cond_2

    move v1, v2

    .line 1929783
    :goto_1
    invoke-static {p0}, LX/Cjt;->isSlideshow(LX/Clr;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v3, v1

    .line 1929784
    goto :goto_0

    :cond_2
    move v1, v3

    .line 1929785
    goto :goto_1

    .line 1929786
    :cond_3
    invoke-interface {v0}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-eq v4, v5, :cond_4

    invoke-interface {v0}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT_ONLY:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-ne v4, v5, :cond_6

    :cond_4
    move v4, v2

    .line 1929787
    :goto_2
    invoke-interface {v0}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->NON_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-ne v0, v5, :cond_7

    move v0, v2

    .line 1929788
    :goto_3
    if-nez v4, :cond_5

    if-eqz v0, :cond_0

    :cond_5
    if-eqz v1, :cond_0

    move v3, v2

    goto :goto_0

    :cond_6
    move v4, v3

    .line 1929789
    goto :goto_2

    :cond_7
    move v0, v3

    .line 1929790
    goto :goto_3
.end method

.method private static isAnnotationInSlot(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)Z
    .locals 1

    .prologue
    .line 1929772
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->e()Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isDefaultFullscreen(LX/Clr;)Z
    .locals 3
    .param p0    # LX/Clr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1929777
    if-eqz p0, :cond_0

    instance-of v1, p0, LX/Clp;

    if-nez v1, :cond_1

    .line 1929778
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p0, LX/Clp;

    invoke-interface {p0}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isSlideshow(LX/Clr;)Z
    .locals 3
    .param p0    # LX/Clr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1929775
    if-eqz p0, :cond_0

    instance-of v1, p0, LX/Clu;

    if-nez v1, :cond_1

    .line 1929776
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p0, LX/Clu;

    invoke-interface {p0}, LX/Clu;->d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cjt;
    .locals 1

    .prologue
    .line 1929774
    const-class v0, LX/Cjt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cjt;

    return-object v0
.end method

.method public static values()[LX/Cjt;
    .locals 1

    .prologue
    .line 1929773
    sget-object v0, LX/Cjt;->$VALUES:[LX/Cjt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cjt;

    return-object v0
.end method
