.class public LX/ETr;
.super LX/ETf;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/ETe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125529
    invoke-direct {p0}, LX/ETf;-><init>()V

    .line 2125530
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ETr;->a:Ljava/util/List;

    .line 2125531
    return-void
.end method

.method public static b(LX/0QB;)LX/ETr;
    .locals 1

    .prologue
    .line 2125526
    new-instance v0, LX/ETr;

    invoke-direct {v0}, LX/ETr;-><init>()V

    .line 2125527
    move-object v0, v0

    .line 2125528
    return-object v0
.end method


# virtual methods
.method public final a(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 2

    .prologue
    .line 2125523
    iget-object v0, p0, LX/ETr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETe;

    .line 2125524
    invoke-interface {v0, p1, p2, p3, p4}, LX/ETe;->a(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    goto :goto_0

    .line 2125525
    :cond_0
    return-void
.end method

.method public final a(LX/ETe;)V
    .locals 1

    .prologue
    .line 2125520
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2125521
    iget-object v0, p0, LX/ETr;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2125522
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 2

    .prologue
    .line 2125514
    iget-object v0, p0, LX/ETr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETe;

    .line 2125515
    invoke-interface {v0, p1}, LX/ETe;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    goto :goto_0

    .line 2125516
    :cond_0
    return-void
.end method

.method public final b(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 2

    .prologue
    .line 2125517
    iget-object v0, p0, LX/ETr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETe;

    .line 2125518
    invoke-interface {v0, p1, p2, p3, p4}, LX/ETe;->b(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    goto :goto_0

    .line 2125519
    :cond_0
    return-void
.end method
