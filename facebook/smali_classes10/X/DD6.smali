.class public final LX/DD6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final synthetic c:LX/DD9;


# direct methods
.method public constructor <init>(LX/DD9;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 0

    .prologue
    .line 1974856
    iput-object p1, p0, LX/DD6;->c:LX/DD9;

    iput-object p2, p0, LX/DD6;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    iput-object p3, p0, LX/DD6;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1974857
    iget-object v0, p0, LX/DD6;->c:LX/DD9;

    iget-object v0, v0, LX/DD9;->a:Ljava/util/Set;

    iget-object v1, p0, LX/DD6;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1974858
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 1974859
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    .line 1974860
    iget-object v0, p0, LX/DD6;->c:LX/DD9;

    iget-object v0, v0, LX/DD9;->a:Ljava/util/Set;

    iget-object v1, p0, LX/DD6;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1974861
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974862
    :goto_0
    return-void

    .line 1974863
    :cond_0
    iget-object v0, p0, LX/DD6;->c:LX/DD9;

    iget-object v0, v0, LX/DD9;->e:LX/189;

    iget-object v1, p0, LX/DD6;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    .line 1974864
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1974865
    invoke-static {v1}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)LX/0Px;

    move-result-object v6

    invoke-virtual {v8, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1974866
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1974867
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    .line 1974868
    const/4 v6, 0x0

    move v7, v6

    :goto_1
    if-ge v7, v10, :cond_2

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 1974869
    invoke-static {v6}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1974870
    invoke-virtual {v8, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1974871
    :cond_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 1974872
    :cond_2
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1974873
    invoke-static {v1}, LX/4Xg;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)LX/4Xg;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v8

    invoke-static {v8}, LX/4Xh;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;)LX/4Xh;

    move-result-object v8

    .line 1974874
    iput-object v6, v8, LX/4Xh;->b:LX/0Px;

    .line 1974875
    move-object v6, v8

    .line 1974876
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v8

    .line 1974877
    iput-object v8, v6, LX/4Xh;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1974878
    move-object v6, v6

    .line 1974879
    invoke-virtual {v6}, LX/4Xh;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v6

    .line 1974880
    iput-object v6, v7, LX/4Xg;->i:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    .line 1974881
    move-object v6, v7

    .line 1974882
    iget-object v7, v0, LX/189;->i:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    .line 1974883
    iput-wide v8, v6, LX/4Xg;->d:J

    .line 1974884
    move-object v6, v6

    .line 1974885
    invoke-virtual {v6}, LX/4Xg;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    move-result-object v6

    .line 1974886
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->I_()I

    move-result v7

    invoke-static {v6, v7}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 1974887
    const/4 v7, 0x0

    invoke-static {v6, v7}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;LX/0Rf;)V

    .line 1974888
    move-object v1, v6

    .line 1974889
    const/4 v0, 0x0

    .line 1974890
    if-eqz v1, :cond_3

    .line 1974891
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1974892
    :cond_3
    iget-object v2, p0, LX/DD6;->c:LX/DD9;

    iget-object v2, v2, LX/DD9;->d:LX/03V;

    const-class v3, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/DD6;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1974893
    iget-object v0, p0, LX/DD6;->c:LX/DD9;

    iget-object v0, v0, LX/DD9;->h:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v1}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1974894
    iget-object v0, p0, LX/DD6;->c:LX/DD9;

    iget-object v0, v0, LX/DD9;->h:LX/0bH;

    new-instance v2, LX/1Nf;

    invoke-direct {v2, v1}, LX/1Nf;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0
.end method
