.class public final enum LX/EDA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDA;

.field public static final enum VIDEO:LX/EDA;

.field public static final enum VOICE:LX/EDA;


# instance fields
.field private final mEventType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2091006
    new-instance v0, LX/EDA;

    const-string v1, "VOICE"

    invoke-direct {v0, v1, v3, v2}, LX/EDA;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EDA;->VOICE:LX/EDA;

    .line 2091007
    new-instance v0, LX/EDA;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v2, v4}, LX/EDA;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EDA;->VIDEO:LX/EDA;

    .line 2091008
    new-array v0, v4, [LX/EDA;

    sget-object v1, LX/EDA;->VOICE:LX/EDA;

    aput-object v1, v0, v3

    sget-object v1, LX/EDA;->VIDEO:LX/EDA;

    aput-object v1, v0, v2

    sput-object v0, LX/EDA;->$VALUES:[LX/EDA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2091002
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2091003
    iput p3, p0, LX/EDA;->mEventType:I

    .line 2091004
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDA;
    .locals 1

    .prologue
    .line 2091009
    const-class v0, LX/EDA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDA;

    return-object v0
.end method

.method public static values()[LX/EDA;
    .locals 1

    .prologue
    .line 2091005
    sget-object v0, LX/EDA;->$VALUES:[LX/EDA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDA;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 2091001
    iget v0, p0, LX/EDA;->mEventType:I

    return v0
.end method
