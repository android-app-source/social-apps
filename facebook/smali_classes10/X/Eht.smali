.class public LX/Eht;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/49e;


# instance fields
.field private final a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2158861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2158862
    iput-object p1, p0, LX/Eht;->a:LX/03V;

    .line 2158863
    return-void
.end method


# virtual methods
.method public final a(LX/0dI;LX/0dI;LX/49d;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2158864
    iget-object v0, p2, LX/0dI;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2158865
    if-eqz v0, :cond_0

    .line 2158866
    iget-object v1, p0, LX/Eht;->a:LX/03V;

    const-string v2, "marauder_device_id"

    invoke-virtual {v1, v2, v0}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2158867
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ErrorReporter DEVICE_ID_KEY set to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2158868
    :cond_0
    return-void
.end method
