.class public final LX/EO0;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/EO0;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ENy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/EO1;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2113810
    const/4 v0, 0x0

    sput-object v0, LX/EO0;->a:LX/EO0;

    .line 2113811
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EO0;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2113812
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2113813
    new-instance v0, LX/EO1;

    invoke-direct {v0}, LX/EO1;-><init>()V

    iput-object v0, p0, LX/EO0;->c:LX/EO1;

    .line 2113814
    return-void
.end method

.method public static declared-synchronized q()LX/EO0;
    .locals 2

    .prologue
    .line 2113815
    const-class v1, LX/EO0;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/EO0;->a:LX/EO0;

    if-nez v0, :cond_0

    .line 2113816
    new-instance v0, LX/EO0;

    invoke-direct {v0}, LX/EO0;-><init>()V

    sput-object v0, LX/EO0;->a:LX/EO0;

    .line 2113817
    :cond_0
    sget-object v0, LX/EO0;->a:LX/EO0;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2113818
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2113819
    check-cast p2, LX/ENz;

    .line 2113820
    iget-object v0, p2, LX/ENz;->a:Ljava/lang/String;

    iget-boolean v1, p2, LX/ENz;->b:Z

    .line 2113821
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    if-eqz v1, :cond_0

    const/4 v2, -0x1

    :goto_0
    invoke-virtual {p0, v2}, LX/1ne;->m(I)LX/1ne;

    move-result-object v2

    const/high16 p0, 0x41800000    # 16.0f

    .line 2113822
    iget-object p2, v2, LX/1ne;->a:LX/1nb;

    invoke-virtual {v2, p0}, LX/1Dp;->a(F)I

    move-result v0

    iput v0, p2, LX/1nb;->n:I

    .line 2113823
    move-object v2, v2

    .line 2113824
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x7

    const/4 p2, 0x4

    invoke-interface {v2, p0, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x6

    const/16 p2, 0x10

    invoke-interface {v2, p0, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x5

    const/16 p2, 0xa

    invoke-interface {v2, p0, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object p0

    if-eqz v1, :cond_1

    const v2, 0x7f02117a

    :goto_1
    invoke-interface {p0, v2}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    .line 2113825
    const p0, -0x1dd9367f

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2113826
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2113827
    return-object v0

    :cond_0
    const v2, -0xbbbbbc

    goto :goto_0

    :cond_1
    const v2, 0x7f021179

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2113828
    invoke-static {}, LX/1dS;->b()V

    .line 2113829
    iget v0, p1, LX/1dQ;->b:I

    .line 2113830
    packed-switch v0, :pswitch_data_0

    .line 2113831
    :goto_0
    return-object v2

    .line 2113832
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2113833
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2113834
    check-cast v1, LX/ENz;

    .line 2113835
    iget-object p0, v1, LX/ENz;->c:LX/ENv;

    .line 2113836
    invoke-virtual {p0}, LX/ENv;->onClick()V

    .line 2113837
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1dd9367f
        :pswitch_0
    .end packed-switch
.end method
