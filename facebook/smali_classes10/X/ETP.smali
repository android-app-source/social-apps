.class public final LX/ETP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/ETQ;


# direct methods
.method public constructor <init>(LX/ETQ;)V
    .locals 1

    .prologue
    .line 2124881
    iput-object p1, p0, LX/ETP;->d:LX/ETQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2124882
    const/4 v0, 0x0

    iput v0, p0, LX/ETP;->a:I

    .line 2124883
    const/4 v0, -0x1

    iput v0, p0, LX/ETP;->b:I

    .line 2124884
    iget-object v0, p1, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, LX/ETP;->c:Ljava/util/Iterator;

    .line 2124885
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 2124880
    iget-object v0, p0, LX/ETP;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2124876
    iget-object v0, p0, LX/ETP;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124877
    iget v1, p0, LX/ETP;->a:I

    iput v1, p0, LX/ETP;->b:I

    .line 2124878
    iget v1, p0, LX/ETP;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/ETP;->a:I

    .line 2124879
    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 2124867
    iget v0, p0, LX/ETP;->b:I

    if-gez v0, :cond_0

    .line 2124868
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2124869
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/ETP;->d:LX/ETQ;

    iget v1, p0, LX/ETP;->b:I

    invoke-virtual {v0, v1}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    .line 2124870
    iget-object v1, p0, LX/ETP;->c:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 2124871
    iget-object v1, p0, LX/ETP;->d:LX/ETQ;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, LX/ETQ;->c(LX/ETQ;Ljava/util/Collection;)V

    .line 2124872
    iget v0, p0, LX/ETP;->b:I

    iput v0, p0, LX/ETP;->a:I

    .line 2124873
    const/4 v0, -0x1

    iput v0, p0, LX/ETP;->b:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2124874
    return-void

    .line 2124875
    :catch_0
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0
.end method
