.class public LX/EEp;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/EEn;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field public b:Z

.field private c:Z

.field public d:LX/EEn;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EGE;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EGE;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:I

.field public i:I

.field private j:Landroid/view/View$OnClickListener;

.field private k:Landroid/view/LayoutInflater;

.field public final l:LX/0Uh;

.field public final m:LX/Dpx;

.field public final n:LX/0gc;

.field public final o:LX/6e1;

.field public final p:LX/3Mg;

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/Dd0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/2CH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/1tu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2094354
    const-class v0, LX/EEp;

    sput-object v0, LX/EEp;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Uh;LX/Dpx;LX/0Px;LX/0gc;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/Dpx;",
            "LX/0Px",
            "<",
            "LX/EGE;",
            ">;",
            "LX/0gc;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2094336
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2094337
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EEp;->b:Z

    .line 2094338
    iput-boolean v1, p0, LX/EEp;->c:Z

    .line 2094339
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EEp;->g:Ljava/util/Map;

    .line 2094340
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2094341
    iput-object v0, p0, LX/EEp;->q:LX/0Ot;

    .line 2094342
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/EEp;->k:Landroid/view/LayoutInflater;

    .line 2094343
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EEp;->e:Ljava/util/List;

    .line 2094344
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EEp;->f:Ljava/util/List;

    .line 2094345
    iput-object p2, p0, LX/EEp;->l:LX/0Uh;

    .line 2094346
    iput-object p3, p0, LX/EEp;->m:LX/Dpx;

    .line 2094347
    iput-object p5, p0, LX/EEp;->n:LX/0gc;

    .line 2094348
    new-instance v0, LX/EEd;

    invoke-direct {v0, p0}, LX/EEd;-><init>(LX/EEp;)V

    iput-object v0, p0, LX/EEp;->o:LX/6e1;

    .line 2094349
    invoke-direct {p0, p4}, LX/EEp;->b(LX/0Px;)V

    .line 2094350
    new-instance v0, LX/EEe;

    invoke-direct {v0, p0}, LX/EEe;-><init>(LX/EEp;)V

    iput-object v0, p0, LX/EEp;->p:LX/3Mg;

    .line 2094351
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/EEp;->h:I

    .line 2094352
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/EEp;->i:I

    .line 2094353
    return-void
.end method

.method private b(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/EGE;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2094314
    iget-object v0, p0, LX/EEp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2094315
    iget-object v0, p0, LX/EEp;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2094316
    invoke-virtual {p0}, LX/EEp;->clear()V

    .line 2094317
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2094318
    iget-object v3, v0, LX/EGE;->a:LX/EGD;

    sget-object v4, LX/EGD;->CONNECTED:LX/EGD;

    if-ne v3, v4, :cond_0

    .line 2094319
    iget-object v3, p0, LX/EEp;->e:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2094320
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2094321
    :cond_0
    iget-object v3, p0, LX/EEp;->f:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2094322
    :cond_1
    iget-object v0, p0, LX/EEp;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2094323
    iget-object v0, p0, LX/EEp;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2094324
    iget-object v0, p0, LX/EEp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2094325
    new-instance v0, LX/EEn;

    sget-object v1, LX/EEm;->InCallHeader:LX/EEm;

    invoke-direct {v0, p0, v1, v5}, LX/EEn;-><init>(LX/EEp;LX/EEm;LX/EGE;)V

    invoke-virtual {p0, v0}, LX/EEp;->add(Ljava/lang/Object;)V

    .line 2094326
    :cond_2
    iget-object v0, p0, LX/EEp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2094327
    new-instance v2, LX/EEn;

    sget-object v3, LX/EEm;->Participant:LX/EEm;

    invoke-direct {v2, p0, v3, v0}, LX/EEn;-><init>(LX/EEp;LX/EEm;LX/EGE;)V

    invoke-virtual {p0, v2}, LX/EEp;->add(Ljava/lang/Object;)V

    goto :goto_2

    .line 2094328
    :cond_3
    new-instance v0, LX/EEn;

    sget-object v1, LX/EEm;->NotInCallHeader:LX/EEm;

    invoke-direct {v0, p0, v1, v5}, LX/EEn;-><init>(LX/EEp;LX/EEm;LX/EGE;)V

    invoke-virtual {p0, v0}, LX/EEp;->add(Ljava/lang/Object;)V

    .line 2094329
    iget-boolean v0, p0, LX/EEp;->c:Z

    if-eqz v0, :cond_5

    .line 2094330
    iget-object v0, p0, LX/EEp;->d:LX/EEn;

    if-nez v0, :cond_4

    .line 2094331
    new-instance v0, LX/EEn;

    sget-object v1, LX/EEm;->Add_People:LX/EEm;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, LX/EEn;-><init>(LX/EEp;LX/EEm;LX/EGE;)V

    iput-object v0, p0, LX/EEp;->d:LX/EEn;

    .line 2094332
    :cond_4
    iget-object v0, p0, LX/EEp;->d:LX/EEn;

    invoke-virtual {p0, v0}, LX/EEp;->add(Ljava/lang/Object;)V

    .line 2094333
    :cond_5
    iget-object v0, p0, LX/EEp;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2094334
    new-instance v2, LX/EEn;

    sget-object v3, LX/EEm;->Participant:LX/EEm;

    invoke-direct {v2, p0, v3, v0}, LX/EEn;-><init>(LX/EEp;LX/EEm;LX/EGE;)V

    invoke-virtual {p0, v2}, LX/EEp;->add(Ljava/lang/Object;)V

    goto :goto_3

    .line 2094335
    :cond_6
    return-void
.end method

.method public static d(LX/EEp;)V
    .locals 3

    .prologue
    .line 2094309
    iget-object v0, p0, LX/EEp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2094310
    iget-object v2, p0, LX/EEp;->s:LX/2CH;

    iget-object v0, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2CH;->b(Lcom/facebook/user/model/UserKey;)V

    goto :goto_0

    .line 2094311
    :cond_0
    iget-object v0, p0, LX/EEp;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2094312
    iget-object v2, p0, LX/EEp;->s:LX/2CH;

    iget-object v0, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2CH;->b(Lcom/facebook/user/model/UserKey;)V

    goto :goto_1

    .line 2094313
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/EGE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2094299
    invoke-direct {p0, p1}, LX/EEp;->b(LX/0Px;)V

    .line 2094300
    invoke-static {p0}, LX/EEp;->d(LX/EEp;)V

    .line 2094301
    const/4 p1, 0x0

    .line 2094302
    iget-object v0, p0, LX/EEp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 2094303
    iput-boolean p1, p0, LX/EEp;->b:Z

    .line 2094304
    :cond_0
    const v0, 0x2942b449

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2094305
    return-void

    .line 2094306
    :cond_1
    iget-object v0, p0, LX/EEp;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2094307
    invoke-virtual {v0}, LX/EGE;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2094308
    iput-boolean p1, p0, LX/EEp;->b:Z

    goto :goto_0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2094355
    iput-object p1, p0, LX/EEp;->j:Landroid/view/View$OnClickListener;

    .line 2094356
    const v0, -0xadea549

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2094357
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2094295
    iget-boolean v0, p0, LX/EEp;->c:Z

    if-ne v0, p1, :cond_0

    .line 2094296
    :goto_0
    return-void

    .line 2094297
    :cond_0
    iput-boolean p1, p0, LX/EEp;->c:Z

    .line 2094298
    iget-object v0, p0, LX/EEp;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bj()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EEp;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2094294
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2094173
    invoke-virtual {p0, p1}, LX/EEp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EEn;

    iget-object v0, v0, LX/EEn;->a:LX/EEm;

    invoke-virtual {v0}, LX/EEm;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 2094175
    invoke-virtual {p0, p1}, LX/EEp;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EEn;

    .line 2094176
    sget-object v1, LX/EEj;->b:[I

    iget-object v2, v0, LX/EEn;->a:LX/EEm;

    invoke-virtual {v2}, LX/EEm;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2094177
    :goto_0
    return-object p2

    .line 2094178
    :pswitch_0
    if-nez p2, :cond_1

    .line 2094179
    iget-object v1, p0, LX/EEp;->k:Landroid/view/LayoutInflater;

    const v2, 0x7f0315f3

    invoke-virtual {v1, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2094180
    new-instance v2, LX/EEo;

    invoke-direct {v2}, LX/EEo;-><init>()V

    .line 2094181
    const v1, 0x7f0d3147    # 1.87677E38f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/tiles/UserTileView;

    iput-object v1, v2, LX/EEo;->a:Lcom/facebook/user/tiles/UserTileView;

    .line 2094182
    const v1, 0x7f0d3146

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/rtc/views/RtcLevelTileView;

    iput-object v1, v2, LX/EEo;->b:Lcom/facebook/rtc/views/RtcLevelTileView;

    .line 2094183
    const v1, 0x7f0d3148

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, v2, LX/EEo;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2094184
    const v1, 0x7f0d1e9a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, v2, LX/EEo;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2094185
    const v1, 0x7f0d314b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, LX/EEo;->e:Landroid/widget/ImageButton;

    .line 2094186
    const v1, 0x7f0d314a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterButton;

    iput-object v1, v2, LX/EEo;->f:Lcom/facebook/widget/text/BetterButton;

    .line 2094187
    const v1, 0x7f0d3149

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, LX/EEo;->h:Landroid/view/View;

    .line 2094188
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    .line 2094189
    :goto_1
    iget-object v2, v0, LX/EEn;->b:LX/EGE;

    iget-object v2, v2, LX/EGE;->b:Ljava/lang/String;

    .line 2094190
    new-instance v3, LX/6e5;

    invoke-direct {v3}, LX/6e5;-><init>()V

    .line 2094191
    const-string v4, ""

    .line 2094192
    iput-object v4, v3, LX/6e5;->b:Ljava/lang/String;

    .line 2094193
    new-instance v4, LX/6e3;

    invoke-direct {v4}, LX/6e3;-><init>()V

    const/4 v5, 0x1

    .line 2094194
    iput v5, v4, LX/6e3;->a:I

    .line 2094195
    move-object v4, v4

    .line 2094196
    const v5, 0x7f080728

    .line 2094197
    iput v5, v4, LX/6e3;->b:I

    .line 2094198
    move-object v4, v4

    .line 2094199
    const-string v5, "roster_view_profile"

    .line 2094200
    iput-object v5, v4, LX/6e3;->e:Ljava/lang/String;

    .line 2094201
    move-object v4, v4

    .line 2094202
    invoke-static {v2}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v5

    .line 2094203
    iput-object v5, v4, LX/6e3;->f:Landroid/os/Parcelable;

    .line 2094204
    move-object v4, v4

    .line 2094205
    invoke-virtual {v4}, LX/6e3;->g()Lcom/facebook/messaging/dialog/MenuDialogItem;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/6e5;->a(Lcom/facebook/messaging/dialog/MenuDialogItem;)LX/6e5;

    .line 2094206
    invoke-virtual {v3}, LX/6e5;->e()Lcom/facebook/messaging/dialog/MenuDialogParams;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/messaging/dialog/MenuDialogFragment;->a(Lcom/facebook/messaging/dialog/MenuDialogParams;)Lcom/facebook/messaging/dialog/MenuDialogFragment;

    move-result-object v3

    .line 2094207
    iget-object v4, p0, LX/EEp;->o:LX/6e1;

    .line 2094208
    iput-object v4, v3, Lcom/facebook/messaging/dialog/MenuDialogFragment;->n:LX/6e1;

    .line 2094209
    move-object v2, v3

    .line 2094210
    iput-object v2, v1, LX/EEo;->g:Lcom/facebook/messaging/dialog/MenuDialogFragment;

    .line 2094211
    iget-object v2, v1, LX/EEo;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, v0, LX/EEn;->b:LX/EGE;

    iget-object v3, v3, LX/EGE;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094212
    iget-object v2, v1, LX/EEo;->c:Lcom/facebook/resources/ui/FbTextView;

    iget v3, p0, LX/EEp;->h:I

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2094213
    iget-object v2, p0, LX/EEp;->r:LX/Dd0;

    invoke-virtual {v2}, LX/Dd0;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, LX/8ue;->NONE:LX/8ue;

    .line 2094214
    :goto_2
    iget-object v3, v1, LX/EEo;->a:Lcom/facebook/user/tiles/UserTileView;

    new-instance v4, Lcom/facebook/user/model/UserKey;

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v6, v0, LX/EEn;->b:LX/EGE;

    iget-object v6, v6, LX/EGE;->b:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-static {v4, v2}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2094215
    iget-object v2, v1, LX/EEo;->e:Landroid/widget/ImageButton;

    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2094216
    iget-object v2, v1, LX/EEo;->f:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v2, v8}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2094217
    iget-object v2, v0, LX/EEn;->b:LX/EGE;

    iget-object v2, v2, LX/EGE;->a:LX/EGD;

    sget-object v3, LX/EGD;->CONNECTED:LX/EGD;

    if-ne v2, v3, :cond_4

    .line 2094218
    iget-object v2, v1, LX/EEo;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2094219
    iget-object v2, v1, LX/EEo;->h:Landroid/view/View;

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2094220
    iget-object v2, p0, LX/EEp;->l:LX/0Uh;

    const/16 v3, 0x5f3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v2, v2

    .line 2094221
    if-eqz v2, :cond_0

    .line 2094222
    iget-object v2, v1, LX/EEo;->e:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2094223
    iget-object v2, v1, LX/EEo;->e:Landroid/widget/ImageButton;

    iget-object v3, v0, LX/EEn;->b:LX/EGE;

    iget-boolean v3, v3, LX/EGE;->j:Z

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2094224
    iget-object v3, v1, LX/EEo;->e:Landroid/widget/ImageButton;

    iget-object v2, v0, LX/EEn;->b:LX/EGE;

    iget-boolean v2, v2, LX/EGE;->j:Z

    if-eqz v2, :cond_3

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_3
    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 2094225
    iget-object v2, v1, LX/EEo;->e:Landroid/widget/ImageButton;

    iget-object v3, v0, LX/EEn;->b:LX/EGE;

    iget-object v3, v3, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 2094226
    iget-object v2, v1, LX/EEo;->e:Landroid/widget/ImageButton;

    new-instance v3, LX/EEf;

    invoke-direct {v3, p0}, LX/EEf;-><init>(LX/EEp;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2094227
    :cond_0
    :goto_4
    iget-object v2, v0, LX/EEn;->b:LX/EGE;

    iget-object v2, v2, LX/EGE;->a:LX/EGD;

    sget-object v3, LX/EGD;->CONNECTED:LX/EGD;

    if-ne v2, v3, :cond_8

    .line 2094228
    iget-object v2, v1, LX/EEo;->b:Lcom/facebook/rtc/views/RtcLevelTileView;

    invoke-virtual {v2, v7}, Lcom/facebook/rtc/views/RtcLevelTileView;->setVisibility(I)V

    .line 2094229
    iget-object v2, p0, LX/EEp;->g:Ljava/util/Map;

    iget-object v3, v0, LX/EEn;->b:LX/EGE;

    iget-object v3, v3, LX/EGE;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2094230
    iget-object v1, v1, LX/EEo;->b:Lcom/facebook/rtc/views/RtcLevelTileView;

    iget-object v2, p0, LX/EEp;->g:Ljava/util/Map;

    iget-object v0, v0, LX/EEn;->b:LX/EGE;

    iget-object v0, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/rtc/views/RtcLevelTileView;->a(I)V

    .line 2094231
    :goto_5
    new-instance v0, LX/EEh;

    invoke-direct {v0, p0}, LX/EEh;-><init>(LX/EEp;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto/16 :goto_0

    .line 2094232
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EEo;

    goto/16 :goto_1

    .line 2094233
    :cond_2
    sget-object v2, LX/8ue;->MESSENGER:LX/8ue;

    goto/16 :goto_2

    .line 2094234
    :cond_3
    const/high16 v2, 0x3f000000    # 0.5f

    goto :goto_3

    .line 2094235
    :cond_4
    iget-object v2, v0, LX/EEn;->b:LX/EGE;

    .line 2094236
    sget-object v3, LX/EEj;->a:[I

    iget-object v4, v2, LX/EGE;->a:LX/EGD;

    invoke-virtual {v4}, LX/EGD;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 2094237
    const/4 v3, 0x0

    :goto_6
    move-object v2, v3

    .line 2094238
    iget-object v3, v1, LX/EEo;->d:Lcom/facebook/resources/ui/FbTextView;

    iget v4, p0, LX/EEp;->i:I

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2094239
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2094240
    iget-object v2, v0, LX/EEn;->b:LX/EGE;

    iget-object v2, v2, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v2}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    .line 2094241
    iget-object v3, p0, LX/EEp;->t:LX/1tu;

    iget-object v4, p0, LX/EEp;->s:LX/2CH;

    invoke-virtual {v4, v2}, LX/2CH;->d(Lcom/facebook/user/model/UserKey;)LX/3Ox;

    move-result-object v4

    .line 2094242
    iget-wide v10, v4, LX/3Ox;->g:J

    move-wide v4, v10

    .line 2094243
    invoke-virtual {v3, v4, v5}, LX/1tu;->a(J)Z

    move-result v3

    .line 2094244
    iget-object v4, p0, LX/EEp;->s:LX/2CH;

    invoke-virtual {v4, v2}, LX/2CH;->c(Lcom/facebook/user/model/UserKey;)Z

    move-result v2

    .line 2094245
    if-eqz v3, :cond_5

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EEp;->u:LX/0ad;

    sget-short v3, LX/3Dx;->bx:S

    invoke-interface {v2, v3, v7}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2094246
    iget-object v2, v1, LX/EEo;->h:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2094247
    iget-object v2, v1, LX/EEo;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080720

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094248
    iget-object v2, v1, LX/EEo;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2094249
    :goto_7
    iget-object v2, v0, LX/EEn;->b:LX/EGE;

    invoke-virtual {v2}, LX/EGE;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2094250
    iget-object v2, v1, LX/EEo;->f:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v2, v7}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2094251
    iget-object v2, v1, LX/EEo;->f:Lcom/facebook/widget/text/BetterButton;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2094252
    iget-object v2, v1, LX/EEo;->f:Lcom/facebook/widget/text/BetterButton;

    iget-object v3, v0, LX/EEn;->b:LX/EGE;

    iget-object v3, v3, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterButton;->setTag(Ljava/lang/Object;)V

    .line 2094253
    iget-object v2, v1, LX/EEo;->f:Lcom/facebook/widget/text/BetterButton;

    new-instance v3, LX/EEg;

    invoke-direct {v3, p0}, LX/EEg;-><init>(LX/EEp;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 2094254
    :cond_5
    iget-object v2, v1, LX/EEo;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v9}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094255
    iget-object v2, v1, LX/EEo;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_7

    .line 2094256
    :cond_6
    iget-object v3, v1, LX/EEo;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094257
    iget-object v2, v1, LX/EEo;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_7

    .line 2094258
    :cond_7
    iget-object v0, v1, LX/EEo;->b:Lcom/facebook/rtc/views/RtcLevelTileView;

    invoke-virtual {v0, v7}, Lcom/facebook/rtc/views/RtcLevelTileView;->a(I)V

    goto/16 :goto_5

    .line 2094259
    :cond_8
    iget-object v0, v1, LX/EEo;->b:Lcom/facebook/rtc/views/RtcLevelTileView;

    invoke-virtual {v0, v8}, Lcom/facebook/rtc/views/RtcLevelTileView;->setVisibility(I)V

    goto/16 :goto_5

    .line 2094260
    :pswitch_1
    if-nez p2, :cond_9

    .line 2094261
    iget-object v1, p0, LX/EEp;->k:Landroid/view/LayoutInflater;

    const v2, 0x7f0315f2

    invoke-virtual {v1, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2094262
    new-instance v2, LX/EEl;

    invoke-direct {v2}, LX/EEl;-><init>()V

    .line 2094263
    const v1, 0x7f0d3143

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, v2, LX/EEl;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2094264
    const v1, 0x7f0d3144

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterButton;

    iput-object v1, v2, LX/EEl;->b:Lcom/facebook/widget/text/BetterButton;

    .line 2094265
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    .line 2094266
    :goto_8
    iget-object v0, v0, LX/EEn;->a:LX/EEm;

    sget-object v2, LX/EEm;->NotInCallHeader:LX/EEm;

    if-ne v0, v2, :cond_a

    iget-object v0, p0, LX/EEp;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bn()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2094267
    iget-object v0, v1, LX/EEl;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080726

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094268
    iget-object v0, v1, LX/EEl;->b:Lcom/facebook/widget/text/BetterButton;

    new-instance v2, LX/EEi;

    invoke-direct {v2, p0}, LX/EEi;-><init>(LX/EEp;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2094269
    iget-object v0, v1, LX/EEl;->b:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v7}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 2094270
    :cond_9
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EEl;

    goto :goto_8

    .line 2094271
    :cond_a
    iget-object v0, v1, LX/EEl;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080725

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094272
    iget-object v0, v1, LX/EEl;->b:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v9}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2094273
    iget-object v0, v1, LX/EEl;->b:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v8}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 2094274
    :pswitch_2
    if-nez p2, :cond_b

    .line 2094275
    iget-object v0, p0, LX/EEp;->k:Landroid/view/LayoutInflater;

    const v1, 0x7f0315f5

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2094276
    new-instance v1, LX/EEk;

    invoke-direct {v1}, LX/EEk;-><init>()V

    .line 2094277
    iput-object p2, v1, LX/EEk;->a:Landroid/view/View;

    .line 2094278
    const v0, 0x7f0d3159

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, v1, LX/EEk;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2094279
    const v0, 0x7f0d315a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, v1, LX/EEk;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2094280
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 2094281
    :goto_9
    iget-object v1, v0, LX/EEk;->b:Lcom/facebook/resources/ui/FbTextView;

    iget v2, p0, LX/EEp;->h:I

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2094282
    iget-object v1, v0, LX/EEk;->c:Lcom/facebook/resources/ui/FbTextView;

    iget v2, p0, LX/EEp;->h:I

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2094283
    iget-object v0, v0, LX/EEk;->a:Landroid/view/View;

    iget-object v1, p0, LX/EEp;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2094284
    :cond_b
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EEk;

    goto :goto_9

    .line 2094285
    :pswitch_3
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080735

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2094286
    :pswitch_4
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08072b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2094287
    :pswitch_5
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080737

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2094288
    :pswitch_6
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08072d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2094289
    :pswitch_7
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080738

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2094290
    :pswitch_8
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080739

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2094291
    :pswitch_9
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080736

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2094292
    :pswitch_a
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080742

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    .line 2094293
    :pswitch_b
    invoke-virtual {p0}, LX/EEp;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08073f

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object p1, v2, LX/EGE;->c:Ljava/lang/String;

    aput-object p1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2094174
    invoke-static {}, LX/EEm;->values()[LX/EEm;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
