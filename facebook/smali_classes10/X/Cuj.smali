.class public final enum LX/Cuj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cuj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cuj;

.field public static final enum APPLICATION_AUTOPLAY:LX/Cuj;

.field public static final enum APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

.field public static final enum APPLICATION_INIT_LISTENER:LX/Cuj;

.field public static final enum ATTEMPT_TO_PLAY:LX/Cuj;

.field public static final enum SYSTEM_AUTOHIDE_CONTROLS:LX/Cuj;

.field public static final enum SYSTEM_LOADING:LX/Cuj;

.field public static final enum SYSTEM_VIDEO_FINISHED:LX/Cuj;

.field public static final enum SYSTEM_VIDEO_PAUSE:LX/Cuj;

.field public static final enum SYSTEM_VIDEO_PLAY:LX/Cuj;

.field public static final enum USER_CLICK_MEDIA:LX/Cuj;

.field public static final enum USER_CONTROLLER_PAUSED:LX/Cuj;

.field public static final enum USER_PRESSED_BACK:LX/Cuj;

.field public static final enum USER_SCROLL_FINISHED:LX/Cuj;

.field public static final enum USER_UNFOCUSED_MEDIA:LX/Cuj;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1947105
    new-instance v0, LX/Cuj;

    const-string v1, "SYSTEM_VIDEO_PLAY"

    invoke-direct {v0, v1, v3}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    .line 1947106
    new-instance v0, LX/Cuj;

    const-string v1, "SYSTEM_VIDEO_PAUSE"

    invoke-direct {v0, v1, v4}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    .line 1947107
    new-instance v0, LX/Cuj;

    const-string v1, "SYSTEM_VIDEO_FINISHED"

    invoke-direct {v0, v1, v5}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    .line 1947108
    new-instance v0, LX/Cuj;

    const-string v1, "SYSTEM_AUTOHIDE_CONTROLS"

    invoke-direct {v0, v1, v6}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->SYSTEM_AUTOHIDE_CONTROLS:LX/Cuj;

    .line 1947109
    new-instance v0, LX/Cuj;

    const-string v1, "ATTEMPT_TO_PLAY"

    invoke-direct {v0, v1, v7}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->ATTEMPT_TO_PLAY:LX/Cuj;

    .line 1947110
    new-instance v0, LX/Cuj;

    const-string v1, "SYSTEM_LOADING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->SYSTEM_LOADING:LX/Cuj;

    .line 1947111
    new-instance v0, LX/Cuj;

    const-string v1, "USER_CLICK_MEDIA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    .line 1947112
    new-instance v0, LX/Cuj;

    const-string v1, "USER_SCROLL_FINISHED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->USER_SCROLL_FINISHED:LX/Cuj;

    .line 1947113
    new-instance v0, LX/Cuj;

    const-string v1, "USER_PRESSED_BACK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->USER_PRESSED_BACK:LX/Cuj;

    .line 1947114
    new-instance v0, LX/Cuj;

    const-string v1, "USER_UNFOCUSED_MEDIA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    .line 1947115
    new-instance v0, LX/Cuj;

    const-string v1, "USER_CONTROLLER_PAUSED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->USER_CONTROLLER_PAUSED:LX/Cuj;

    .line 1947116
    new-instance v0, LX/Cuj;

    const-string v1, "APPLICATION_AUTOPLAY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    .line 1947117
    new-instance v0, LX/Cuj;

    const-string v1, "APPLICATION_AUTOPLAY_PAUSE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    .line 1947118
    new-instance v0, LX/Cuj;

    const-string v1, "APPLICATION_INIT_LISTENER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/Cuj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cuj;->APPLICATION_INIT_LISTENER:LX/Cuj;

    .line 1947119
    const/16 v0, 0xe

    new-array v0, v0, [LX/Cuj;

    sget-object v1, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    aput-object v1, v0, v3

    sget-object v1, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    aput-object v1, v0, v4

    sget-object v1, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    aput-object v1, v0, v5

    sget-object v1, LX/Cuj;->SYSTEM_AUTOHIDE_CONTROLS:LX/Cuj;

    aput-object v1, v0, v6

    sget-object v1, LX/Cuj;->ATTEMPT_TO_PLAY:LX/Cuj;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Cuj;->SYSTEM_LOADING:LX/Cuj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Cuj;->USER_SCROLL_FINISHED:LX/Cuj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Cuj;->USER_PRESSED_BACK:LX/Cuj;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Cuj;->USER_CONTROLLER_PAUSED:LX/Cuj;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Cuj;->APPLICATION_INIT_LISTENER:LX/Cuj;

    aput-object v2, v0, v1

    sput-object v0, LX/Cuj;->$VALUES:[LX/Cuj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1947120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cuj;
    .locals 1

    .prologue
    .line 1947121
    const-class v0, LX/Cuj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cuj;

    return-object v0
.end method

.method public static values()[LX/Cuj;
    .locals 1

    .prologue
    .line 1947122
    sget-object v0, LX/Cuj;->$VALUES:[LX/Cuj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cuj;

    return-object v0
.end method
