.class public LX/DBx;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/0Vf;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/03V;

.field private final d:Lcom/facebook/api/feedtype/FeedType;

.field public final e:LX/1b1;

.field private f:LX/87o;

.field public g:LX/DBw;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/resources/ui/FbTextView;

.field public j:Lcom/facebook/fig/button/FigButton;

.field public k:Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;

.field public l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/goodfriends/data/FriendData;

.field public n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1973603
    const-class v0, LX/DBx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DBx;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1b1;LX/87o;LX/03V;LX/0Or;Landroid/content/Context;Lcom/facebook/api/feedtype/FeedType;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1b1;",
            "LX/87o;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/facebook/api/feedtype/FeedType;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1973594
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1973595
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DBx;->n:Z

    .line 1973596
    iput-object p5, p0, LX/DBx;->b:Landroid/content/Context;

    .line 1973597
    iput-object p2, p0, LX/DBx;->f:LX/87o;

    .line 1973598
    iput-object p3, p0, LX/DBx;->c:LX/03V;

    .line 1973599
    iput-object p4, p0, LX/DBx;->l:LX/0Or;

    .line 1973600
    iput-object p6, p0, LX/DBx;->d:Lcom/facebook/api/feedtype/FeedType;

    .line 1973601
    iput-object p1, p0, LX/DBx;->e:LX/1b1;

    .line 1973602
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1973572
    iget-object v0, p0, LX/DBx;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1973573
    const v0, 0x7f0d148b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DBx;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1973574
    const v0, 0x7f0d148c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DBx;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 1973575
    iget-object v0, p0, LX/DBx;->j:Lcom/facebook/fig/button/FigButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1973576
    iget-object v0, p0, LX/DBx;->j:Lcom/facebook/fig/button/FigButton;

    const v2, 0x7f082ee9

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1973577
    iget-object v0, p0, LX/DBx;->j:Lcom/facebook/fig/button/FigButton;

    const/16 v2, 0x12

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1973578
    iget-object v0, p0, LX/DBx;->j:Lcom/facebook/fig/button/FigButton;

    new-instance v2, LX/DBq;

    invoke-direct {v2, p0}, LX/DBq;-><init>(LX/DBx;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1973579
    iget-object v0, p0, LX/DBx;->h:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f082ee7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1973580
    const v0, 0x7f082ee8

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1973581
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1973582
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973583
    const v3, 0x7f082eea

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973584
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1973585
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1973586
    new-instance v4, LX/DBs;

    invoke-direct {v4, p0, v1}, LX/DBs;-><init>(LX/DBx;Landroid/content/res/Resources;)V

    .line 1973587
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 p1, 0x21

    invoke-virtual {v3, v4, v0, v2, p1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1973588
    move-object v0, v3

    .line 1973589
    iget-object v2, p0, LX/DBx;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1973590
    iget-object v0, p0, LX/DBx;->i:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f0a00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1973591
    iget-object v0, p0, LX/DBx;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1973592
    iget-object v0, p0, LX/DBx;->k:Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;

    new-instance v1, LX/DBr;

    invoke-direct {v1, p0}, LX/DBr;-><init>(LX/DBx;)V

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1973593
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1973556
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1973557
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307ba

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1973558
    const v0, 0x7f0d148d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/DBx;->j:Lcom/facebook/fig/button/FigButton;

    .line 1973559
    const v0, 0x7f0d148a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;

    iput-object v0, p0, LX/DBx;->k:Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;

    .line 1973560
    iget-object v0, p0, LX/DBx;->d:Lcom/facebook/api/feedtype/FeedType;

    sget-object v2, Lcom/facebook/api/feedtype/FeedType;->d:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v0, v2}, Lcom/facebook/api/feedtype/FeedType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1973561
    invoke-direct {p0, v1}, LX/DBx;->a(Landroid/view/View;)V

    .line 1973562
    :goto_0
    invoke-virtual {p0}, LX/DBx;->a()V

    .line 1973563
    return-object v1

    .line 1973564
    :cond_0
    iget-object v0, p0, LX/DBx;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1973565
    const v2, 0x7f020953

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1973566
    const p1, 0x7f0a00e6

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sget-object p1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1973567
    iget-object v0, p0, LX/DBx;->j:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1973568
    iget-object v0, p0, LX/DBx;->j:Lcom/facebook/fig/button/FigButton;

    const/16 v2, 0x102

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1973569
    iget-object v0, p0, LX/DBx;->j:Lcom/facebook/fig/button/FigButton;

    new-instance v2, LX/DBt;

    invoke-direct {v2, p0}, LX/DBt;-><init>(LX/DBx;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1973570
    iget-object v0, p0, LX/DBx;->k:Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;

    new-instance v2, LX/DBu;

    invoke-direct {v2, p0}, LX/DBu;-><init>(LX/DBx;)V

    invoke-virtual {v0, v2}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1973571
    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 1973552
    iget-object v0, p0, LX/DBx;->f:LX/87o;

    .line 1973553
    const/4 v1, 0x1

    new-instance v2, LX/DBv;

    invoke-direct {v2, p0}, LX/DBv;-><init>(LX/DBx;)V

    const/4 v3, 0x4

    .line 1973554
    const/4 p0, 0x1

    invoke-static {v0, v1, p0, v3, v2}, LX/87o;->a(LX/87o;ZZILX/87n;)V

    .line 1973555
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 1973545
    if-nez p2, :cond_0

    .line 1973546
    :goto_0
    return-void

    .line 1973547
    :cond_0
    check-cast p2, LX/DBw;

    .line 1973548
    iget-object v0, p2, LX/DBw;->b:LX/0Px;

    .line 1973549
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1973550
    iget-object v0, p0, LX/DBx;->k:Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;

    iget-object v1, p2, LX/DBw;->a:Lcom/facebook/goodfriends/data/FriendData;

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->setAvatarData(Lcom/facebook/goodfriends/data/FriendData;)V

    goto :goto_0

    .line 1973551
    :cond_1
    iget-object v0, p0, LX/DBx;->k:Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;

    iget-object v1, p2, LX/DBw;->b:LX/0Px;

    iget v2, p2, LX/DBw;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/goodfriends/ui/GoodFriendsHeaderAvatarsView;->a(LX/0Px;I)V

    goto :goto_0
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 1973543
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DBx;->n:Z

    .line 1973544
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1973539
    const/4 v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1973542
    iget-object v0, p0, LX/DBx;->g:LX/DBw;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1973541
    int-to-long v0, p1

    return-wide v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 1973540
    iget-boolean v0, p0, LX/DBx;->n:Z

    return v0
.end method
