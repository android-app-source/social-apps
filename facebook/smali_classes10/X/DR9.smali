.class public final LX/DR9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1997145
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 1997146
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1997147
    :goto_0
    return v1

    .line 1997148
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_4

    .line 1997149
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1997150
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1997151
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 1997152
    const-string v6, "count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1997153
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1997154
    :cond_1
    const-string v6, "edges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1997155
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1997156
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_2

    .line 1997157
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_2

    .line 1997158
    const/4 v6, 0x0

    .line 1997159
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_b

    .line 1997160
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1997161
    :goto_3
    move v5, v6

    .line 1997162
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1997163
    :cond_2
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1997164
    goto :goto_1

    .line 1997165
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1997166
    :cond_4
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1997167
    if-eqz v0, :cond_5

    .line 1997168
    invoke-virtual {p1, v1, v4, v1}, LX/186;->a(III)V

    .line 1997169
    :cond_5
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1997170
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1

    .line 1997171
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1997172
    :cond_8
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 1997173
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1997174
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1997175
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_8

    if-eqz v8, :cond_8

    .line 1997176
    const-string v9, "display_name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1997177
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 1997178
    :cond_9
    const-string v9, "node"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1997179
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_4

    .line 1997180
    :cond_a
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1997181
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 1997182
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1997183
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_3

    :cond_b
    move v5, v6

    move v7, v6

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1997184
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1997185
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1997186
    if-eqz v0, :cond_0

    .line 1997187
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1997188
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1997189
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1997190
    if-eqz v0, :cond_4

    .line 1997191
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1997192
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1997193
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1997194
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x1

    .line 1997195
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1997196
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1997197
    if-eqz v3, :cond_1

    .line 1997198
    const-string p1, "display_name"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1997199
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1997200
    :cond_1
    invoke-virtual {p0, v2, p3}, LX/15i;->g(II)I

    move-result v3

    .line 1997201
    if-eqz v3, :cond_2

    .line 1997202
    const-string v3, "node"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1997203
    invoke-virtual {p0, v2, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1997204
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1997205
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1997206
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1997207
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1997208
    return-void
.end method
