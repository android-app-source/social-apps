.class public final LX/DBQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DBX;


# direct methods
.method public constructor <init>(LX/DBX;)V
    .locals 0

    .prologue
    .line 1972801
    iput-object p1, p0, LX/DBQ;->a:LX/DBX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x4f56fe37

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1972802
    iget-object v1, p0, LX/DBQ;->a:LX/DBX;

    .line 1972803
    new-instance v4, LX/3Af;

    iget-object v3, v1, LX/DBX;->A:Landroid/content/Context;

    invoke-direct {v4, v3}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 1972804
    new-instance v5, LX/7TY;

    iget-object v3, v1, LX/DBX;->A:Landroid/content/Context;

    invoke-direct {v5, v3}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 1972805
    iget-object v3, v1, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972806
    iget-boolean v6, v3, Lcom/facebook/events/model/Event;->y:Z

    move v3, v6

    .line 1972807
    if-nez v3, :cond_1

    .line 1972808
    iget-object v3, v1, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972809
    iget-boolean v6, v3, Lcom/facebook/events/model/Event;->z:Z

    move v3, v6

    .line 1972810
    if-nez v3, :cond_0

    .line 1972811
    const v3, 0x7f082f37

    invoke-virtual {v5, v3}, LX/34c;->e(I)LX/3Ai;

    move-result-object v3

    .line 1972812
    iget-object v6, v1, LX/DBX;->i:LX/0wM;

    const v7, 0x7f0209c5

    const v8, -0x6e685d

    invoke-virtual {v6, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1972813
    new-instance v6, LX/DBR;

    invoke-direct {v6, v1}, LX/DBR;-><init>(LX/DBX;)V

    invoke-virtual {v3, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1972814
    :cond_0
    const v3, 0x7f082f35

    invoke-virtual {v5, v3}, LX/34c;->e(I)LX/3Ai;

    move-result-object v3

    .line 1972815
    const v6, 0x7f02064c

    invoke-virtual {v3, v6}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1972816
    new-instance v6, LX/DBS;

    invoke-direct {v6, v1}, LX/DBS;-><init>(LX/DBX;)V

    invoke-virtual {v3, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1972817
    :cond_1
    iget-object v3, v1, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972818
    iget-boolean v6, v3, Lcom/facebook/events/model/Event;->z:Z

    move v3, v6

    .line 1972819
    if-eqz v3, :cond_3

    .line 1972820
    const v3, 0x7f082f38

    invoke-virtual {v5, v3}, LX/34c;->e(I)LX/3Ai;

    move-result-object v3

    .line 1972821
    const v6, 0x7f0209c4

    invoke-virtual {v3, v6}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1972822
    new-instance v6, LX/DBT;

    invoke-direct {v6, v1}, LX/DBT;-><init>(LX/DBX;)V

    invoke-virtual {v3, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1972823
    iget-object v3, v1, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972824
    iget-wide v11, v3, Lcom/facebook/events/model/Event;->A:J

    move-wide v7, v11

    .line 1972825
    const-wide/16 v9, 0x0

    cmp-long v3, v7, v9

    if-eqz v3, :cond_2

    .line 1972826
    const v3, 0x7f082f3a

    .line 1972827
    :goto_0
    invoke-virtual {v5, v3}, LX/34c;->e(I)LX/3Ai;

    move-result-object v3

    .line 1972828
    const v6, 0x7f0207fd

    invoke-virtual {v3, v6}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1972829
    new-instance v6, LX/DBU;

    invoke-direct {v6, v1}, LX/DBU;-><init>(LX/DBX;)V

    invoke-virtual {v3, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1972830
    :goto_1
    invoke-virtual {v4, v5}, LX/3Af;->a(LX/1OM;)V

    .line 1972831
    invoke-virtual {v4}, LX/3Af;->show()V

    .line 1972832
    const v1, -0x3d0894c5

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1972833
    :cond_2
    const v3, 0x7f082f39

    goto :goto_0

    .line 1972834
    :cond_3
    const v3, 0x7f082f36

    invoke-virtual {v5, v3}, LX/34c;->e(I)LX/3Ai;

    move-result-object v3

    .line 1972835
    const v6, 0x7f02064b

    invoke-virtual {v3, v6}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1972836
    new-instance v6, LX/DBV;

    invoke-direct {v6, v1}, LX/DBV;-><init>(LX/DBX;)V

    invoke-virtual {v3, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1
.end method
