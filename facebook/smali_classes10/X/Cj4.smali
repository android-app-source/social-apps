.class public final LX/Cj4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cj;


# instance fields
.field private final a:Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

.field private final b:LX/Cj5;


# direct methods
.method public constructor <init>(LX/Cj5;Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;)V
    .locals 0

    .prologue
    .line 1929035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929036
    iput-object p1, p0, LX/Cj4;->b:LX/Cj5;

    .line 1929037
    iput-object p2, p0, LX/Cj4;->a:Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    .line 1929038
    return-void
.end method


# virtual methods
.method public final a(LX/1ca;)V
    .locals 2

    .prologue
    .line 1929039
    if-eqz p1, :cond_0

    .line 1929040
    invoke-interface {p1}, LX/1ca;->g()Z

    .line 1929041
    :cond_0
    iget-object v0, p0, LX/Cj4;->a:Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1929042
    :cond_1
    iget-object v0, p0, LX/Cj4;->a:Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    iget-object v1, p0, LX/Cj4;->b:LX/Cj5;

    iget-object v1, v1, LX/Cj5;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->a$redex0(Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;Ljava/lang/String;)V

    .line 1929043
    iget-object v0, p0, LX/Cj4;->b:LX/Cj5;

    .line 1929044
    iget-object v1, v0, LX/Cj5;->b:LX/CGy;

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1929045
    if-eqz v0, :cond_2

    .line 1929046
    iget-object v0, p0, LX/Cj4;->b:LX/Cj5;

    iget-object v0, v0, LX/Cj5;->b:LX/CGy;

    invoke-interface {v0}, LX/CGy;->a()V

    .line 1929047
    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(LX/1ca;)V
    .locals 2

    .prologue
    .line 1929048
    iget-object v0, p0, LX/Cj4;->a:Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    if-eqz v0, :cond_0

    .line 1929049
    iget-object v0, p0, LX/Cj4;->a:Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    iget-object v1, p0, LX/Cj4;->b:LX/Cj5;

    iget-object v1, v1, LX/Cj5;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->a$redex0(Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;Ljava/lang/String;)V

    .line 1929050
    invoke-interface {p1}, LX/1ca;->g()Z

    .line 1929051
    :cond_0
    return-void
.end method

.method public final c(LX/1ca;)V
    .locals 2

    .prologue
    .line 1929052
    iget-object v0, p0, LX/Cj4;->a:Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    if-eqz v0, :cond_0

    .line 1929053
    iget-object v0, p0, LX/Cj4;->a:Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    iget-object v1, p0, LX/Cj4;->b:LX/Cj5;

    iget-object v1, v1, LX/Cj5;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->a$redex0(Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;Ljava/lang/String;)V

    .line 1929054
    :cond_0
    return-void
.end method

.method public final d(LX/1ca;)V
    .locals 0

    .prologue
    .line 1929055
    return-void
.end method
