.class public LX/Ee8;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/0Tn;


# instance fields
.field public c:LX/0Zb;

.field public d:Landroid/os/Handler;

.field public e:Landroid/content/Context;

.field public f:LX/03V;

.field public g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public h:LX/0Xl;

.field public i:LX/0Yb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2152043
    const-class v0, LX/Ee8;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ee8;->a:Ljava/lang/String;

    .line 2152044
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "power/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "profile_upload_attempt"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Ee8;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Zb;Landroid/os/Handler;Landroid/content/Context;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;)V
    .locals 0
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2152045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152046
    iput-object p1, p0, LX/Ee8;->c:LX/0Zb;

    .line 2152047
    iput-object p2, p0, LX/Ee8;->d:Landroid/os/Handler;

    .line 2152048
    iput-object p3, p0, LX/Ee8;->e:Landroid/content/Context;

    .line 2152049
    iput-object p4, p0, LX/Ee8;->f:LX/03V;

    .line 2152050
    iput-object p5, p0, LX/Ee8;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2152051
    iput-object p6, p0, LX/Ee8;->h:LX/0Xl;

    .line 2152052
    return-void
.end method
