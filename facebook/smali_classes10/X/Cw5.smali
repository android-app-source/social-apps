.class public LX/Cw5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final b:J

.field public final c:LX/0ta;


# direct methods
.method public constructor <init>(LX/0Px;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 1949903
    sget-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    invoke-direct {p0, p1, p2, p3, v0}, LX/Cw5;-><init>(LX/0Px;JLX/0ta;)V

    .line 1949904
    return-void
.end method

.method public constructor <init>(LX/0Px;JLX/0ta;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;J",
            "LX/0ta;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1949898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949899
    iput-object p1, p0, LX/Cw5;->a:LX/0Px;

    .line 1949900
    iput-wide p2, p0, LX/Cw5;->b:J

    .line 1949901
    iput-object p4, p0, LX/Cw5;->c:LX/0ta;

    .line 1949902
    return-void
.end method


# virtual methods
.method public final d()Z
    .locals 1

    .prologue
    .line 1949897
    iget-object v0, p0, LX/Cw5;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    return v0
.end method
