.class public LX/EQ3;
.super LX/Cwh;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cwh",
        "<",
        "LX/EQ2;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2118159
    invoke-direct {p0}, LX/Cwh;-><init>()V

    .line 2118160
    return-void
.end method

.method public static a(LX/0QB;)LX/EQ3;
    .locals 1

    .prologue
    .line 2118156
    new-instance v0, LX/EQ3;

    invoke-direct {v0}, LX/EQ3;-><init>()V

    .line 2118157
    move-object v0, v0

    .line 2118158
    return-object v0
.end method

.method public static b(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/EQ2;
    .locals 2

    .prologue
    .line 2118151
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->k()LX/103;

    move-result-object v0

    sget-object v1, LX/103;->VIDEO:LX/103;

    if-ne v0, v1, :cond_1

    .line 2118152
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v0, v0

    .line 2118153
    sget-object v1, LX/CwI;->NULL_STATE_MODULE:LX/CwI;

    if-eq v0, v1, :cond_1

    .line 2118154
    sget-object v0, LX/EQ2;->USE_SERP_ENDPOINT:LX/EQ2;

    .line 2118155
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/EQ2;->DO_NOT_UPDATE:LX/EQ2;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118150
    sget-object v0, LX/EQ2;->USE_GRAPH_API:LX/EQ2;

    return-object v0
.end method

.method public final synthetic a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118149
    invoke-static {p1}, LX/EQ3;->b(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/EQ2;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateModuleSuggestionUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118148
    sget-object v0, LX/EQ2;->DO_NOT_UPDATE:LX/EQ2;

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118147
    sget-object v0, LX/EQ2;->DO_NOT_UPDATE:LX/EQ2;

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118140
    iget-boolean v0, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->d:Z

    move v0, v0

    .line 2118141
    if-nez v0, :cond_0

    .line 2118142
    sget-object v0, LX/EQ2;->DO_NOT_UPDATE:LX/EQ2;

    .line 2118143
    :goto_0
    return-object v0

    .line 2118144
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2118145
    sget-object v0, LX/EQ2;->USE_SERP_ENDPOINT:LX/EQ2;

    goto :goto_0

    .line 2118146
    :cond_1
    sget-object v0, LX/EQ2;->USE_GRAPH_API:LX/EQ2;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118135
    sget-object v0, LX/EQ2;->USE_GRAPH_API:LX/EQ2;

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118139
    sget-object v0, LX/EQ2;->USE_GRAPH_API:LX/EQ2;

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118138
    sget-object v0, LX/EQ2;->DO_NOT_UPDATE:LX/EQ2;

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118137
    sget-object v0, LX/EQ2;->USE_GRAPH_API:LX/EQ2;

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/TrendingTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118136
    sget-object v0, LX/EQ2;->DO_NOT_UPDATE:LX/EQ2;

    return-object v0
.end method
