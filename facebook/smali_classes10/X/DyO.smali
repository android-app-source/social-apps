.class public final LX/DyO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/photos/albums/protocols/MediasetQueryInterfaces$DefaultMediaSetMediaConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V
    .locals 0

    .prologue
    .line 2064415
    iput-object p1, p0, LX/DyO;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2064416
    iget-object v0, p0, LX/DyO;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->h:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2064417
    iget-object v0, p0, LX/DyO;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    const/16 p1, 0x8

    const/4 p0, 0x0

    .line 2064418
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2064419
    if-nez v1, :cond_1

    .line 2064420
    :goto_0
    return-void

    .line 2064421
    :cond_0
    iget-object v0, p0, LX/DyO;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    invoke-static {v0}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->d(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V

    goto :goto_0

    .line 2064422
    :cond_1
    iget-object v1, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/gridview/BetterGridView;->setVisibility(I)V

    .line 2064423
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2064424
    const v2, 0x7f0d0d65

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0811fd

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2064425
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2064426
    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2064427
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2064428
    const v2, 0x7f0d0d65

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2064429
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2064430
    const v2, 0x7f0d0d66

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2064431
    check-cast p1, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    .line 2064432
    if-nez p1, :cond_0

    .line 2064433
    :goto_0
    return-void

    .line 2064434
    :cond_0
    iget-object v0, p0, LX/DyO;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    .line 2064435
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->c()LX/0us;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2064436
    :cond_1
    :goto_1
    iget-object v0, p0, LX/DyO;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->h:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;

    const v1, 0x14aa11bb

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2064437
    iget-object v0, p0, LX/DyO;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->h:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;

    invoke-virtual {v0}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 2064438
    iget-object v0, p0, LX/DyO;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    invoke-static {v0}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->d(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V

    goto :goto_0

    .line 2064439
    :cond_2
    iget-object v0, p0, LX/DyO;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/gridview/BetterGridView;->setVisibility(I)V

    goto :goto_0

    .line 2064440
    :cond_3
    iget-object v1, v0, LX/9av;->e:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, LX/9av;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->c()LX/0us;

    move-result-object v2

    invoke-interface {v2}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2064441
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->c()LX/0us;

    move-result-object v1

    invoke-interface {v1}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9av;->d:Ljava/lang/String;

    .line 2064442
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->c()LX/0us;

    move-result-object v1

    invoke-interface {v1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9av;->e:Ljava/lang/String;

    .line 2064443
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->c()LX/0us;

    move-result-object v1

    invoke-interface {v1}, LX/0us;->b()Z

    move-result v1

    iput-boolean v1, v0, LX/9av;->b:Z

    .line 2064444
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->c()LX/0us;

    move-result-object v1

    invoke-interface {v1}, LX/0us;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/9av;->c:Z

    .line 2064445
    iget-object v1, v0, LX/9av;->a:LX/0Px;

    if-nez v1, :cond_5

    .line 2064446
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/9av;->a:LX/0Px;

    goto :goto_1

    .line 2064447
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2064448
    iget-object v2, v0, LX/9av;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/9av;->a:LX/0Px;

    goto/16 :goto_1
.end method
