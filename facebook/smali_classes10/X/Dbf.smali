.class public final LX/Dbf;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

.field public final synthetic b:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;)V
    .locals 0

    .prologue
    .line 2017355
    iput-object p1, p0, LX/Dbf;->b:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    iput-object p2, p0, LX/Dbf;->a:Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2017356
    iget-object v0, p0, LX/Dbf;->b:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    iget-object v0, v0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->n:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    iget-object v1, p0, LX/Dbf;->a:Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->a(Ljava/util/List;)V

    .line 2017357
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2017358
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2017359
    if-eqz p1, :cond_0

    .line 2017360
    iget-object v0, p0, LX/Dbf;->b:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    iput-object p1, v0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2017361
    iget-object v0, p0, LX/Dbf;->b:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    iget-object v0, v0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->i:LX/0SI;

    iget-object v1, p0, LX/Dbf;->b:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    iget-object v1, v1, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-interface {v0, v1}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2017362
    :cond_0
    iget-object v0, p0, LX/Dbf;->b:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    iget-object v0, v0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->n:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    iget-object v1, p0, LX/Dbf;->a:Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->a(Ljava/util/List;)V

    .line 2017363
    return-void
.end method
