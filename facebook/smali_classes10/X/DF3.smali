.class public LX/DF3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:LX/23P;

.field public final b:Landroid/graphics/Typeface;

.field public final c:Landroid/graphics/Typeface;

.field public final d:Landroid/content/Context;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:LX/17Y;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/2e1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/23P;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/2e1;LX/0Or;)V
    .locals 3
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/23P;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            "LX/2e1;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1977626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1977627
    iput-object p2, p0, LX/DF3;->a:LX/23P;

    .line 1977628
    iput-object p1, p0, LX/DF3;->d:Landroid/content/Context;

    .line 1977629
    iput-object p3, p0, LX/DF3;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1977630
    iput-object p4, p0, LX/DF3;->f:LX/17Y;

    .line 1977631
    iput-object p6, p0, LX/DF3;->g:LX/0Or;

    .line 1977632
    iput-object p5, p0, LX/DF3;->h:LX/2e1;

    .line 1977633
    sget-object v0, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v1, LX/0xr;->MEDIUM:LX/0xr;

    invoke-static {p1, v0, v1, v2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, LX/DF3;->b:Landroid/graphics/Typeface;

    .line 1977634
    sget-object v0, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v1, LX/0xr;->REGULAR:LX/0xr;

    invoke-static {p1, v0, v1, v2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, LX/DF3;->c:Landroid/graphics/Typeface;

    .line 1977635
    return-void
.end method

.method public static a(LX/0QB;)LX/DF3;
    .locals 10

    .prologue
    .line 1977636
    const-class v1, LX/DF3;

    monitor-enter v1

    .line 1977637
    :try_start_0
    sget-object v0, LX/DF3;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1977638
    sput-object v2, LX/DF3;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1977639
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977640
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1977641
    new-instance v3, LX/DF3;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v5

    check-cast v5, LX/23P;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v0}, LX/2e1;->a(LX/0QB;)LX/2e1;

    move-result-object v8

    check-cast v8, LX/2e1;

    const/16 v9, 0x12cb

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/DF3;-><init>(Landroid/content/Context;LX/23P;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/2e1;LX/0Or;)V

    .line 1977642
    move-object v0, v3

    .line 1977643
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1977644
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DF3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977645
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1977646
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
