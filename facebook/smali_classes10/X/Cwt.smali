.class public LX/Cwt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Cwt;


# instance fields
.field public final a:LX/2Sc;


# direct methods
.method public constructor <init>(LX/2Sc;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1951237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951238
    iput-object p1, p0, LX/Cwt;->a:LX/2Sc;

    .line 1951239
    return-void
.end method

.method public static a(LX/0QB;)LX/Cwt;
    .locals 4

    .prologue
    .line 1951240
    sget-object v0, LX/Cwt;->b:LX/Cwt;

    if-nez v0, :cond_1

    .line 1951241
    const-class v1, LX/Cwt;

    monitor-enter v1

    .line 1951242
    :try_start_0
    sget-object v0, LX/Cwt;->b:LX/Cwt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1951243
    if-eqz v2, :cond_0

    .line 1951244
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1951245
    new-instance p0, LX/Cwt;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v3

    check-cast v3, LX/2Sc;

    invoke-direct {p0, v3}, LX/Cwt;-><init>(LX/2Sc;)V

    .line 1951246
    move-object v0, p0

    .line 1951247
    sput-object v0, LX/Cwt;->b:LX/Cwt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1951248
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1951249
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1951250
    :cond_1
    sget-object v0, LX/Cwt;->b:LX/Cwt;

    return-object v0

    .line 1951251
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1951252
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
