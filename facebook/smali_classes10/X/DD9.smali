.class public LX/DD9;
.super LX/25J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25J",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/DD9;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/03V;

.field public final e:LX/189;

.field private final f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/DD8;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0sa;

.field public final h:LX/0bH;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;LX/189;LX/1Ck;LX/0sa;LX/0bH;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975022
    invoke-direct {p0}, LX/25J;-><init>()V

    .line 1975023
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/DD9;->a:Ljava/util/Set;

    .line 1975024
    iput-object p1, p0, LX/DD9;->b:LX/0tX;

    .line 1975025
    iput-object p2, p0, LX/DD9;->c:Ljava/util/concurrent/ExecutorService;

    .line 1975026
    iput-object p3, p0, LX/DD9;->d:LX/03V;

    .line 1975027
    iput-object p4, p0, LX/DD9;->e:LX/189;

    .line 1975028
    iput-object p5, p0, LX/DD9;->f:LX/1Ck;

    .line 1975029
    iput-object p6, p0, LX/DD9;->g:LX/0sa;

    .line 1975030
    iput-object p7, p0, LX/DD9;->h:LX/0bH;

    .line 1975031
    return-void
.end method

.method public static a(LX/0QB;)LX/DD9;
    .locals 11

    .prologue
    .line 1975009
    sget-object v0, LX/DD9;->i:LX/DD9;

    if-nez v0, :cond_1

    .line 1975010
    const-class v1, LX/DD9;

    monitor-enter v1

    .line 1975011
    :try_start_0
    sget-object v0, LX/DD9;->i:LX/DD9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1975012
    if-eqz v2, :cond_0

    .line 1975013
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1975014
    new-instance v3, LX/DD9;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v7

    check-cast v7, LX/189;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v9

    check-cast v9, LX/0sa;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v10

    check-cast v10, LX/0bH;

    invoke-direct/range {v3 .. v10}, LX/DD9;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;LX/189;LX/1Ck;LX/0sa;LX/0bH;)V

    .line 1975015
    move-object v0, v3

    .line 1975016
    sput-object v0, LX/DD9;->i:LX/DD9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975017
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1975018
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1975019
    :cond_1
    sget-object v0, LX/DD9;->i:LX/DD9;

    return-object v0

    .line 1975020
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1975021
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)Z
    .locals 2

    .prologue
    .line 1975007
    invoke-static {p0}, LX/DD9;->c(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 1975008
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1

    .prologue
    .line 1975006
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;I)Z
    .locals 4

    .prologue
    .line 1975001
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1975002
    const/4 v2, 0x2

    .line 1975003
    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 1975004
    :cond_0
    move v1, v2

    .line 1975005
    iget-object v2, p0, LX/DD9;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sub-int/2addr v0, v1

    if-lt p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Z
    .locals 1

    .prologue
    .line 1975000
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-static {p1}, LX/DD9;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Z
    .locals 1

    .prologue
    .line 1974991
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {p0, p1, p2}, LX/DD9;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;I)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)V
    .locals 5

    .prologue
    .line 1974995
    invoke-static {p1}, LX/DD9;->c(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 1974996
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1974997
    :cond_0
    :goto_0
    return-void

    .line 1974998
    :cond_1
    iget-object v1, p0, LX/DD9;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1974999
    iget-object v1, p0, LX/DD9;->f:LX/1Ck;

    sget-object v2, LX/DD8;->GPYMI_FETCH_MORE:LX/DD8;

    new-instance v3, LX/DD5;

    invoke-direct {v3, p0, p1, v0}, LX/DD5;-><init>(LX/DD9;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    new-instance v4, LX/DD6;

    invoke-direct {v4, p0, p1, v0}, LX/DD6;-><init>(LX/DD9;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final bridge synthetic b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 1974994
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {p0, p1}, LX/DD9;->b(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)V

    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/0Px;
    .locals 1

    .prologue
    .line 1974992
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    .line 1974993
    invoke-super {p0, p1}, LX/25J;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
