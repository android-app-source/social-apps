.class public LX/E72;
.super LX/Cfk;
.source ""


# direct methods
.method public constructor <init>(LX/3Tx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081029
    invoke-direct {p0, p1}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2081030
    return-void
.end method

.method private static a(Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;)V
    .locals 1

    .prologue
    .line 2081004
    const v0, 0x7f0e08fa

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setTitleTextAppearance(I)V

    .line 2081005
    const v0, 0x7f0e08fb

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setLabelTextAppearance(I)V

    .line 2081006
    return-void
.end method

.method private static b(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2081027
    const-string v0, "%,d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 1

    .prologue
    .line 2081028
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2081007
    const v0, 0x7f031116

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v1

    .line 2081008
    const v0, 0x7f0d2887

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;

    .line 2081009
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ad()I

    move-result v2

    invoke-static {v2}, LX/E72;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setTitle(Ljava/lang/String;)V

    .line 2081010
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ae()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setLabel(Ljava/lang/String;)V

    .line 2081011
    invoke-static {v0}, LX/E72;->a(Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;)V

    .line 2081012
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ab()I

    move-result v2

    .line 2081013
    const v0, 0x7f0d2888

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;

    .line 2081014
    if-lez v2, :cond_0

    .line 2081015
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ab()I

    move-result v2

    invoke-static {v2}, LX/E72;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setTitle(Ljava/lang/String;)V

    .line 2081016
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ac()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setLabel(Ljava/lang/String;)V

    .line 2081017
    invoke-static {v0}, LX/E72;->a(Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;)V

    .line 2081018
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->af()I

    move-result v2

    .line 2081019
    const v0, 0x7f0d2889

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;

    .line 2081020
    if-lez v2, :cond_1

    .line 2081021
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->af()I

    move-result v2

    invoke-static {v2}, LX/E72;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setTitle(Ljava/lang/String;)V

    .line 2081022
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ag()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setLabel(Ljava/lang/String;)V

    .line 2081023
    invoke-static {v0}, LX/E72;->a(Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;)V

    .line 2081024
    :goto_1
    return-object v1

    .line 2081025
    :cond_0
    invoke-virtual {v0, v3}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setVisibility(I)V

    goto :goto_0

    .line 2081026
    :cond_1
    invoke-virtual {v0, v3}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2081003
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ae()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ac()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ag()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ae()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ac()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->ag()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
