.class public LX/DPL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/DPL;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0tX;


# direct methods
.method public constructor <init>(LX/0Or;LX/0tX;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0tX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1993326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1993327
    iput-object p1, p0, LX/DPL;->a:LX/0Or;

    .line 1993328
    iput-object p2, p0, LX/DPL;->b:LX/0tX;

    .line 1993329
    return-void
.end method

.method public static a(LX/0QB;)LX/DPL;
    .locals 5

    .prologue
    .line 1993313
    sget-object v0, LX/DPL;->c:LX/DPL;

    if-nez v0, :cond_1

    .line 1993314
    const-class v1, LX/DPL;

    monitor-enter v1

    .line 1993315
    :try_start_0
    sget-object v0, LX/DPL;->c:LX/DPL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1993316
    if-eqz v2, :cond_0

    .line 1993317
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1993318
    new-instance v4, LX/DPL;

    const/16 v3, 0x15e7

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {v4, p0, v3}, LX/DPL;-><init>(LX/0Or;LX/0tX;)V

    .line 1993319
    move-object v0, v4

    .line 1993320
    sput-object v0, LX/DPL;->c:LX/DPL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1993321
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1993322
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1993323
    :cond_1
    sget-object v0, LX/DPL;->c:LX/DPL;

    return-object v0

    .line 1993324
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1993325
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupHideMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1993302
    new-instance v1, LX/4Fm;

    invoke-direct {v1}, LX/4Fm;-><init>()V

    .line 1993303
    iget-object v0, p0, LX/DPL;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1993304
    const-string v2, "actor_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993305
    move-object v0, v1

    .line 1993306
    const-string v2, "group_id"

    invoke-virtual {v0, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993307
    move-object v0, v0

    .line 1993308
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1993309
    const-string p1, "client_mutation_id"

    invoke-virtual {v0, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993310
    new-instance v0, LX/DPZ;

    invoke-direct {v0}, LX/DPZ;-><init>()V

    move-object v0, v0

    .line 1993311
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1993312
    iget-object v1, p0, LX/DPL;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1993291
    new-instance v1, LX/4GB;

    invoke-direct {v1}, LX/4GB;-><init>()V

    .line 1993292
    iget-object v0, p0, LX/DPL;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1993293
    const-string v2, "actor_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993294
    move-object v0, v1

    .line 1993295
    const-string v2, "group_id"

    invoke-virtual {v0, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993296
    move-object v0, v0

    .line 1993297
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1993298
    const-string p1, "client_mutation_id"

    invoke-virtual {v0, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993299
    new-instance v0, LX/DPa;

    invoke-direct {v0}, LX/DPa;-><init>()V

    move-object v0, v0

    .line 1993300
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1993301
    iget-object v1, p0, LX/DPL;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
