.class public LX/E5F;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/E5F;


# instance fields
.field private final a:LX/E1f;

.field private final b:LX/0SF;

.field private final c:LX/0W9;


# direct methods
.method public constructor <init>(LX/E1f;LX/0SF;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078102
    iput-object p1, p0, LX/E5F;->a:LX/E1f;

    .line 2078103
    iput-object p2, p0, LX/E5F;->b:LX/0SF;

    .line 2078104
    iput-object p3, p0, LX/E5F;->c:LX/0W9;

    .line 2078105
    return-void
.end method

.method public static a(LX/0QB;)LX/E5F;
    .locals 6

    .prologue
    .line 2078106
    sget-object v0, LX/E5F;->d:LX/E5F;

    if-nez v0, :cond_1

    .line 2078107
    const-class v1, LX/E5F;

    monitor-enter v1

    .line 2078108
    :try_start_0
    sget-object v0, LX/E5F;->d:LX/E5F;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078109
    if-eqz v2, :cond_0

    .line 2078110
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078111
    new-instance p0, LX/E5F;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v3

    check-cast v3, LX/E1f;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SF;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-direct {p0, v3, v4, v5}, LX/E5F;-><init>(LX/E1f;LX/0SF;LX/0W9;)V

    .line 2078112
    move-object v0, p0

    .line 2078113
    sput-object v0, LX/E5F;->d:LX/E5F;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078114
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078115
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078116
    :cond_1
    sget-object v0, LX/E5F;->d:LX/E5F;

    return-object v0

    .line 2078117
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078118
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/E5F;LX/0Px;Ljava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Landroid/text/SpannableString;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragment$PlaceInfoBlurbBreadcrumbs;",
            ">;",
            "Ljava/lang/String;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/res/Resources;",
            ")",
            "Landroid/text/SpannableString;"
        }
    .end annotation

    .prologue
    .line 2078119
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;

    .line 2078120
    const v1, 0x7f082231

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "{place_category}"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "{city}"

    aput-object v5, v3, v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2078121
    const/4 v1, 0x2

    new-array v8, v1, [LX/E6G;

    const/4 v1, 0x0

    new-instance v3, LX/E6G;

    const-string v4, "{place_category}"

    const/4 v5, 0x0

    invoke-direct {v3, v4, p2, v5}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v3, v8, v1

    const/4 v9, 0x1

    new-instance v10, LX/E6G;

    const-string v11, "{city}"

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-direct {v10, v11, v12, v1}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v10, v8, v9

    move-object/from16 v0, p6

    invoke-static {v7, v0, v8}, LX/E6H;->a(Ljava/lang/String;Landroid/content/res/Resources;[LX/E6G;)Landroid/text/SpannableString;

    move-result-object v1

    return-object v1

    :cond_0
    new-instance v1, LX/E69;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iget-object v3, p0, LX/E5F;->a:LX/E1f;

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, LX/E69;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/E1f;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(LX/0Px;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragment$PlaceInfoBlurbBreadcrumbs;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2078122
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2078123
    :goto_0
    return v0

    .line 2078124
    :cond_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;

    .line 2078125
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->b()LX/174;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->b()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 2078126
    goto :goto_0

    .line 2078127
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2078128
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/E5F;LX/0Px;Ljava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Landroid/text/SpannableString;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragment$PlaceInfoBlurbBreadcrumbs;",
            ">;",
            "Ljava/lang/String;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/res/Resources;",
            ")",
            "Landroid/text/SpannableString;"
        }
    .end annotation

    .prologue
    .line 2078129
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;

    .line 2078130
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;

    .line 2078131
    const v1, 0x7f082232

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "{place_category}"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "{neighborhood}"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "{city}"

    aput-object v5, v3, v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2078132
    const/4 v1, 0x3

    new-array v9, v1, [LX/E6G;

    const/4 v1, 0x0

    new-instance v3, LX/E6G;

    const-string v4, "{place_category}"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-direct {v3, v4, v0, v5}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v3, v9, v1

    const/4 v10, 0x1

    new-instance v11, LX/E6G;

    const-string v12, "{neighborhood}"

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-direct {v11, v12, v13, v1}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v11, v9, v10

    const/4 v10, 0x2

    new-instance v11, LX/E6G;

    const-string v12, "{city}"

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-direct {v11, v12, v13, v1}, LX/E6G;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    aput-object v11, v9, v10

    move-object/from16 v0, p6

    invoke-static {v8, v0, v9}, LX/E6H;->a(Ljava/lang/String;Landroid/content/res/Resources;[LX/E6G;)Landroid/text/SpannableString;

    move-result-object v1

    return-object v1

    :cond_0
    new-instance v1, LX/E69;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iget-object v3, p0, LX/E5F;->a:LX/E1f;

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, LX/E69;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/E1f;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v1, LX/E69;

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragmentModel$PlaceInfoBlurbBreadcrumbsModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    iget-object v3, p0, LX/E5F;->a:LX/E1f;

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, LX/E69;-><init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/E1f;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1De;LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;DDLjava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;)LX/1Dg;
    .locals 12
    .param p2    # LX/0Px;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # LX/0Px;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p6    # D
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # D
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p11    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragment$PlaceInfoBlurbBreadcrumbs;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLInterfaces$DefaultTimeRangeFields;",
            ">;",
            "Ljava/lang/String;",
            "DD",
            "Ljava/lang/String;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2078133
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 2078134
    new-instance v10, Landroid/text/SpannableStringBuilder;

    invoke-direct {v10}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2078135
    invoke-static {p2}, LX/E5F;->a(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2078136
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object/from16 v3, p11

    move-object/from16 v4, p12

    move-object/from16 v5, p13

    .line 2078137
    invoke-static/range {v0 .. v6}, LX/E5F;->b(LX/E5F;LX/0Px;Ljava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2078138
    :goto_0
    iget-object v0, p0, LX/E5F;->b:LX/0SF;

    iget-object v1, p0, LX/E5F;->c:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    move-object/from16 v8, p10

    invoke-static/range {v0 .. v9}, LX/E6H;->a(LX/0SF;Ljava/util/Locale;LX/0Px;Ljava/lang/String;DDLjava/lang/String;Landroid/content/res/Resources;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2078139
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f01072b

    invoke-interface {v1, v2}, LX/1Dh;->U(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b163a

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a00a3

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->b()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a00a3

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object/from16 v3, p11

    move-object/from16 v4, p12

    move-object/from16 v5, p13

    .line 2078140
    invoke-static/range {v0 .. v6}, LX/E5F;->a(LX/E5F;LX/0Px;Ljava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 2078141
    :cond_1
    invoke-virtual {v10, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0
.end method
