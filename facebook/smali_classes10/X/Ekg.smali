.class public LX/Ekg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Ekg;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2163695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163696
    iput-object p1, p0, LX/Ekg;->a:Landroid/content/Context;

    .line 2163697
    iput-object p2, p0, LX/Ekg;->b:Ljava/util/concurrent/ExecutorService;

    .line 2163698
    return-void
.end method

.method public static a(LX/0QB;)LX/Ekg;
    .locals 5

    .prologue
    .line 2163699
    sget-object v0, LX/Ekg;->c:LX/Ekg;

    if-nez v0, :cond_1

    .line 2163700
    const-class v1, LX/Ekg;

    monitor-enter v1

    .line 2163701
    :try_start_0
    sget-object v0, LX/Ekg;->c:LX/Ekg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2163702
    if-eqz v2, :cond_0

    .line 2163703
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2163704
    new-instance p0, LX/Ekg;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4}, LX/Ekg;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V

    .line 2163705
    move-object v0, p0

    .line 2163706
    sput-object v0, LX/Ekg;->c:LX/Ekg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2163707
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2163708
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2163709
    :cond_1
    sget-object v0, LX/Ekg;->c:LX/Ekg;

    return-object v0

    .line 2163710
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2163711
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
