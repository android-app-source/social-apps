.class public LX/E1d;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2iz;

.field public final b:Lcom/facebook/reaction/ReactionUtil;


# direct methods
.method public constructor <init>(LX/2iz;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2069952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2069953
    iput-object p1, p0, LX/E1d;->a:LX/2iz;

    .line 2069954
    iput-object p2, p0, LX/E1d;->b:Lcom/facebook/reaction/ReactionUtil;

    .line 2069955
    return-void
.end method

.method public static b(LX/0QB;)LX/E1d;
    .locals 3

    .prologue
    .line 2069956
    new-instance v2, LX/E1d;

    invoke-static {p0}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v0

    check-cast v0, LX/2iz;

    invoke-static {p0}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/ReactionUtil;

    invoke-direct {v2, v0, v1}, LX/E1d;-><init>(LX/2iz;Lcom/facebook/reaction/ReactionUtil;)V

    .line 2069957
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;JLjava/lang/String;)LX/2jY;
    .locals 2

    .prologue
    .line 2069958
    sget-object v0, LX/E1c;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2069959
    const-string v0, "ANDROID_PAGE_LIKES_CONTEXT_ITEM"

    :goto_0
    move-object v0, v0

    .line 2069960
    invoke-virtual {p0, v0, p2, p3, p4}, LX/E1d;->a(Ljava/lang/String;JLjava/lang/String;)LX/2jY;

    move-result-object v0

    return-object v0

    .line 2069961
    :pswitch_0
    const-string v0, "ANDROID_POPULAR_AT_PLACE_CONTEXT_ITEM"

    goto :goto_0

    .line 2069962
    :pswitch_1
    const-string v0, "ANDROID_PAGE_FRIENDS_CONTENT_CONTEXT_ITEM"

    goto :goto_0

    .line 2069963
    :pswitch_2
    const-string v0, "ANDROID_PAGE_SANDBOX"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;JLjava/lang/String;)LX/2jY;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2069964
    iget-object v0, p0, LX/E1d;->a:LX/2iz;

    invoke-virtual {v0, p4, p1}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v6

    .line 2069965
    iget-object v0, p0, LX/E1d;->a:LX/2iz;

    invoke-virtual {v0, p4}, LX/2iz;->c(Ljava/lang/String;)V

    .line 2069966
    new-instance v0, Lcom/facebook/reaction/action/PagesReactionManager$1;

    move-object v1, p0

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/action/PagesReactionManager$1;-><init>(LX/E1d;JLjava/lang/String;Ljava/lang/String;)V

    .line 2069967
    iput-object v0, v6, LX/2jY;->A:Ljava/lang/Runnable;

    .line 2069968
    return-object v6
.end method
