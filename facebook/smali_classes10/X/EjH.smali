.class public final LX/EjH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/30Z;


# direct methods
.method public constructor <init>(LX/30Z;Z)V
    .locals 0

    .prologue
    .line 2161071
    iput-object p1, p0, LX/EjH;->b:LX/30Z;

    iput-boolean p2, p0, LX/EjH;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 10

    .prologue
    .line 2161072
    iget-object v0, p0, LX/EjH;->b:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    const-string v1, "close_session_fail"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, LX/EjH;->a:Z

    iget-object v4, p0, LX/EjH;->b:LX/30Z;

    iget-wide v4, v4, LX/30Z;->M:J

    iget-object v6, p0, LX/EjH;->b:LX/30Z;

    iget-object v6, v6, LX/30Z;->h:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    iget-object v8, p0, LX/EjH;->b:LX/30Z;

    iget-wide v8, v8, LX/30Z;->O:J

    sub-long/2addr v6, v8

    iget-object v8, p0, LX/EjH;->b:LX/30Z;

    iget v8, v8, LX/30Z;->N:I

    iget-object v9, p0, LX/EjH;->b:LX/30Z;

    iget-object v9, v9, LX/30Z;->u:Ljava/lang/String;

    invoke-virtual/range {v0 .. v9}, LX/30d;->a(Ljava/lang/String;Ljava/lang/String;ZJJILjava/lang/String;)V

    .line 2161073
    iget-object v0, p0, LX/EjH;->b:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    sget-object v1, LX/Ejs;->CLOSE_SESSION_FAILURE:LX/Ejs;

    invoke-virtual {v1}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/30d;->c(Ljava/lang/String;)V

    .line 2161074
    iget-object v0, p0, LX/EjH;->b:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    invoke-virtual {v0}, LX/30d;->b()V

    .line 2161075
    iget-object v0, p0, LX/EjH;->b:LX/30Z;

    const/4 v1, 0x0

    .line 2161076
    iput-boolean v1, v0, LX/30Z;->C:Z

    .line 2161077
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 2161078
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 2161079
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161080
    if-eqz v0, :cond_0

    .line 2161081
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161082
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2161083
    iget-object v0, p0, LX/EjH;->b:LX/30Z;

    iget-object v0, v0, LX/30Z;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2vf;->a:LX/0Tn;

    iget-object v2, p0, LX/EjH;->b:LX/30Z;

    iget-object v2, v2, LX/30Z;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2161084
    iget-object v0, p0, LX/EjH;->b:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    iget-boolean v1, p0, LX/EjH;->a:Z

    iget-object v2, p0, LX/EjH;->b:LX/30Z;

    iget-wide v2, v2, LX/30Z;->M:J

    iget-object v4, p0, LX/EjH;->b:LX/30Z;

    iget-object v4, v4, LX/30Z;->h:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iget-object v6, p0, LX/EjH;->b:LX/30Z;

    iget-wide v6, v6, LX/30Z;->O:J

    sub-long/2addr v4, v6

    iget-object v6, p0, LX/EjH;->b:LX/30Z;

    iget v6, v6, LX/30Z;->N:I

    iget-object v7, p0, LX/EjH;->b:LX/30Z;

    iget-object v7, v7, LX/30Z;->u:Ljava/lang/String;

    .line 2161085
    iget-object v8, v0, LX/30d;->a:LX/0Zb;

    sget-object v9, LX/Ejs;->CCU_CONTACTS_UPLOAD_SUCCEEDED:LX/Ejs;

    invoke-virtual {v9}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v12, "full_upload"

    invoke-virtual {v9, v12, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v12, "last_upload_success_time"

    invoke-virtual {v9, v12, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v12, "time_spent"

    invoke-virtual {v9, v12, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v12, "num_of_retries"

    invoke-virtual {v9, v12, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v12, "ccu_session_id"

    invoke-virtual {v9, v12, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-interface {v8, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2161086
    move v1, v10

    .line 2161087
    :goto_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161088
    if-eqz v0, :cond_1

    .line 2161089
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161090
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2161091
    if-eqz v1, :cond_2

    const-string v0, "close_session_in_sync"

    .line 2161092
    :goto_2
    iget-object v1, p0, LX/EjH;->b:LX/30Z;

    iget-object v1, v1, LX/30Z;->g:LX/30d;

    invoke-virtual {v1, v0}, LX/30d;->b(Ljava/lang/String;)V

    .line 2161093
    iget-object v1, p0, LX/EjH;->b:LX/30Z;

    iget-object v1, v1, LX/30Z;->g:LX/30d;

    sget-object v2, LX/Ejs;->CLOSE_SESSION_SUCCESS:LX/Ejs;

    invoke-virtual {v2}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/30d;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161094
    iget-object v0, p0, LX/EjH;->b:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    invoke-virtual {v0}, LX/30d;->b()V

    .line 2161095
    iget-object v0, p0, LX/EjH;->b:LX/30Z;

    .line 2161096
    iput-boolean v11, v0, LX/30Z;->C:Z

    .line 2161097
    return-void

    .line 2161098
    :cond_0
    iget-object v0, p0, LX/EjH;->b:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    const-string v1, "close_session_out_of_sync"

    const/4 v2, 0x0

    iget-boolean v3, p0, LX/EjH;->a:Z

    iget-object v4, p0, LX/EjH;->b:LX/30Z;

    iget-wide v4, v4, LX/30Z;->M:J

    iget-object v6, p0, LX/EjH;->b:LX/30Z;

    iget-object v6, v6, LX/30Z;->h:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    iget-object v8, p0, LX/EjH;->b:LX/30Z;

    iget-wide v8, v8, LX/30Z;->O:J

    sub-long/2addr v6, v8

    iget-object v8, p0, LX/EjH;->b:LX/30Z;

    iget v8, v8, LX/30Z;->N:I

    iget-object v9, p0, LX/EjH;->b:LX/30Z;

    iget-object v9, v9, LX/30Z;->u:Ljava/lang/String;

    invoke-virtual/range {v0 .. v9}, LX/30d;->a(Ljava/lang/String;Ljava/lang/String;ZJJILjava/lang/String;)V

    move v1, v11

    goto :goto_0

    :cond_1
    move v10, v11

    .line 2161099
    goto :goto_1

    .line 2161100
    :cond_2
    const-string v0, "close_session_out_of_sync"

    goto :goto_2
.end method
