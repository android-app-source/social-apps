.class public final LX/D2x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/D2y;


# direct methods
.method public constructor <init>(LX/D2y;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1959377
    iput-object p1, p0, LX/D2x;->c:LX/D2y;

    iput-object p2, p0, LX/D2x;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/D2x;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    .line 1959378
    iget-object v0, p0, LX/D2x;->c:LX/D2y;

    iget-object v0, v0, LX/D2y;->b:LX/D2z;

    iget-object v1, p0, LX/D2x;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D2x;->c:LX/D2y;

    iget-object v0, v0, LX/D2y;->b:LX/D2z;

    iget-object v0, v0, LX/D2z;->h:LX/0ad;

    sget-short v1, LX/1RY;->s:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1959379
    iget-object v0, p0, LX/D2x;->c:LX/D2y;

    iget-object v0, v0, LX/D2y;->b:LX/D2z;

    iget-object v1, p0, LX/D2x;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/D2x;->b:Landroid/view/View;

    const/4 p1, -0x1

    .line 1959380
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1959381
    new-instance v4, LX/0ju;

    invoke-direct {v4, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f081064

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f08104f

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    const v5, 0x7f081064

    new-instance p0, LX/D2t;

    invoke-direct {p0, v0, v1, v2, v3}, LX/D2t;-><init>(LX/D2z;Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;Landroid/content/Context;)V

    invoke-virtual {v4, v5, p0}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    const v5, 0x7f08106b

    new-instance p0, LX/D2s;

    invoke-direct {p0, v0, v1, v3}, LX/D2s;-><init>(LX/D2z;Lcom/facebook/graphql/model/FeedUnit;Landroid/content/Context;)V

    invoke-virtual {v4, v5, p0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const v4, 0x7f0815b3

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    move-result-object v3

    .line 1959382
    const/4 v4, -0x3

    invoke-virtual {v3, v4}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v3, p1}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Button;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1959383
    invoke-virtual {v3, p1}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v4

    const/4 v5, -0x2

    invoke-virtual {v3, v5}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Button;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1959384
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1959385
    :cond_0
    iget-object v0, p0, LX/D2x;->c:LX/D2y;

    iget-object v0, v0, LX/D2y;->b:LX/D2z;

    iget-object v1, p0, LX/D2x;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/D2x;->b:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, LX/D2z;->c(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V

    goto :goto_0
.end method
