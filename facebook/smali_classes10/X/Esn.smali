.class public LX/Esn;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/24a;


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLImage;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:Lcom/facebook/graphql/model/GraphQLImage;

.field public final h:Lcom/facebook/fbui/glyph/GlyphView;

.field public final i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2176442
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2176443
    const v0, 0x7f0307de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2176444
    const v0, 0x7f0d0f66

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Esn;->b:Landroid/widget/TextView;

    .line 2176445
    const v0, 0x7f0d0626

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Esn;->c:Landroid/widget/TextView;

    .line 2176446
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Esn;->h:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2176447
    const v0, 0x7f0d0bbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Esn;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2176448
    const v0, 0x7f0d0bdf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Esn;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2176449
    const v0, 0x7f0d08f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Esn;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2176450
    const v0, 0x7f0d14c8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Esn;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2176451
    return-void
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 2176411
    iget-object v0, p0, LX/Esn;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2176412
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2176440
    iget-object v0, p0, LX/Esn;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2176441
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const-wide v6, 0x3fe999999999999aL    # 0.8

    .line 2176416
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 2176417
    iget-object v0, p0, LX/Esn;->a:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Esn;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-lez v0, :cond_1

    .line 2176418
    iget-object v0, p0, LX/Esn;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2176419
    int-to-double v2, v1

    iget-object v4, p0, LX/Esn;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v2, v4

    .line 2176420
    const/4 v4, -0x1

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2176421
    iget-object v4, p0, LX/Esn;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2176422
    invoke-virtual {p0}, LX/Esn;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, LX/Esn;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0, v8, v0, v8, v2}, LX/Esn;->setPadding(IIII)V

    .line 2176423
    iget-object v0, p0, LX/Esn;->g:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_1

    .line 2176424
    int-to-float v2, v1

    iget-boolean v0, p0, LX/Esn;->j:Z

    if-eqz v0, :cond_2

    const v0, 0x3ec16c17

    :goto_0
    mul-float/2addr v0, v2

    .line 2176425
    iget-object v2, p0, LX/Esn;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2176426
    float-to-int v3, v0

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2176427
    float-to-int v3, v0

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2176428
    iget-boolean v2, p0, LX/Esn;->j:Z

    if-eqz v2, :cond_0

    .line 2176429
    iget-object v2, p0, LX/Esn;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2176430
    float-to-int v3, v0

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2176431
    float-to-int v0, v0

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2176432
    :cond_0
    iget-object v0, p0, LX/Esn;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2176433
    const/4 v2, 0x3

    const v3, 0x7f0d08f1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2176434
    iget-object v2, p0, LX/Esn;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2176435
    :cond_1
    iget-object v0, p0, LX/Esn;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    int-to-double v2, v1

    mul-double/2addr v2, v6

    double-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2176436
    iget-object v0, p0, LX/Esn;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    int-to-double v2, v1

    mul-double/2addr v2, v6

    double-to-int v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2176437
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 2176438
    return-void

    .line 2176439
    :cond_2
    const v0, 0x3edb6db7

    goto :goto_0
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 2176413
    iget-object v1, p0, LX/Esn;->h:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2176414
    return-void

    .line 2176415
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
