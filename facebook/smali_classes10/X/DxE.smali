.class public LX/DxE;
.super LX/DvY;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dx6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Ot;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/Dx6;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2062579
    invoke-direct {p0}, LX/DvY;-><init>()V

    .line 2062580
    iput-object p1, p0, LX/DxE;->a:Ljava/lang/String;

    .line 2062581
    iput-object p2, p0, LX/DxE;->c:LX/0Ot;

    .line 2062582
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 6

    .prologue
    .line 2062583
    iget-object v0, p0, LX/DvY;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DvZ;

    .line 2062584
    iget-object v2, p0, LX/DvY;->c:LX/DvX;

    invoke-virtual {v0, v2}, LX/DvZ;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0

    .line 2062585
    :cond_0
    iget-object v0, p0, LX/DvY;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2062586
    const v0, 0x7d085f96

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2062587
    iput-object p1, p0, LX/DxE;->a:Ljava/lang/String;

    .line 2062588
    iput-object p2, p0, LX/DxE;->b:Ljava/lang/String;

    .line 2062589
    iget-object v0, p0, LX/DxE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DvZ;

    invoke-virtual {p0, v0}, LX/DvY;->a(LX/DvZ;)V

    .line 2062590
    iget-object v0, p0, LX/DxE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dx6;

    iget-object v1, p0, LX/DxE;->a:Ljava/lang/String;

    iget-object v2, p0, LX/DxE;->b:Ljava/lang/String;

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/Dvb;->a(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 2062591
    return-void
.end method

.method public final c()LX/Dvc;
    .locals 1

    .prologue
    .line 2062592
    iget-object v0, p0, LX/DxE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dx6;

    .line 2062593
    iget-object p0, v0, LX/Dvb;->i:LX/Dvc;

    move-object v0, p0

    .line 2062594
    return-object v0
.end method

.method public final d()LX/Dx6;
    .locals 1

    .prologue
    .line 2062595
    iget-object v0, p0, LX/DxE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dx6;

    return-object v0
.end method
