.class public final LX/DMb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/1Zp",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DMe;


# direct methods
.method public constructor <init>(LX/DMe;)V
    .locals 0

    .prologue
    .line 1990011
    iput-object p1, p0, LX/DMb;->a:LX/DMe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1990005
    iget-object v0, p0, LX/DMb;->a:LX/DMe;

    iget-object v0, v0, LX/DMe;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/groups/events/GroupEventsPagedListLoader$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/groups/events/GroupEventsPagedListLoader$2$1;-><init>(LX/DMb;)V

    const v2, -0x2d08c293

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1990006
    new-instance v0, LX/DMo;

    invoke-direct {v0}, LX/DMo;-><init>()V

    move-object v0, v0

    .line 1990007
    const-string v1, "group_events_page_size"

    iget-object v2, p0, LX/DMb;->a:LX/DMe;

    iget v2, v2, LX/DMe;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_size"

    iget-object v3, p0, LX/DMb;->a:LX/DMe;

    iget-object v3, v3, LX/DMe;->e:Landroid/content/res/Resources;

    const v4, 0x7f0b2032

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "timeframe"

    iget-object v3, p0, LX/DMb;->a:LX/DMe;

    iget-object v3, v3, LX/DMe;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "group_id"

    iget-object v3, p0, LX/DMb;->a:LX/DMe;

    iget-object v3, v3, LX/DMe;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1990008
    iget-object v1, p0, LX/DMb;->a:LX/DMe;

    iget-object v1, v1, LX/DMe;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1990009
    const-string v1, "end_cursor"

    iget-object v2, p0, LX/DMb;->a:LX/DMe;

    iget-object v2, v2, LX/DMe;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1990010
    :cond_0
    iget-object v1, p0, LX/DMb;->a:LX/DMe;

    iget-object v1, v1, LX/DMe;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
