.class public LX/Eb2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/EcH;


# direct methods
.method public constructor <init>(II[BLX/Eat;)V
    .locals 6

    .prologue
    .line 2142920
    sget-object v0, LX/Ect;->a:LX/Ect;

    move-object v5, v0

    .line 2142921
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/Eb2;-><init>(II[BLX/Eat;LX/Ecs;)V

    .line 2142922
    return-void
.end method

.method private constructor <init>(II[BLX/Eat;LX/Ecs;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II[B",
            "Lorg/whispersystems/libsignal/ecc/ECPublicKey;",
            "LX/Ecs",
            "<",
            "Lorg/whispersystems/libsignal/ecc/ECPrivateKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2142904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142905
    invoke-static {}, LX/Ec7;->w()LX/Ec7;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/Ec7;->a(I)LX/Ec7;

    move-result-object v0

    invoke-static {p3}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ec7;->a(LX/EWc;)LX/Ec7;

    move-result-object v0

    invoke-virtual {v0}, LX/Ec7;->l()LX/Ec8;

    move-result-object v1

    .line 2142906
    invoke-static {}, LX/EcF;->w()LX/EcF;

    move-result-object v0

    invoke-virtual {p4}, LX/Eat;->a()[B

    move-result-object v2

    invoke-static {v2}, LX/EWc;->a([B)LX/EWc;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/EcF;->a(LX/EWc;)LX/EcF;

    move-result-object v2

    .line 2142907
    invoke-virtual {p5}, LX/Ecs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2142908
    invoke-virtual {p5}, LX/Ecs;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eas;

    .line 2142909
    iget-object p2, v0, LX/Eas;->a:[B

    move-object v0, p2

    .line 2142910
    invoke-static {v0}, LX/EWc;->a([B)LX/EWc;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/EcF;->b(LX/EWc;)LX/EcF;

    .line 2142911
    :cond_0
    invoke-static {}, LX/Ec4;->x()LX/Ec4;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Ec4;->a(I)LX/Ec4;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/Ec4;->a(LX/Ec8;)LX/Ec4;

    move-result-object v0

    .line 2142912
    iget-object v1, v0, LX/Ec4;->f:LX/EZ7;

    if-nez v1, :cond_1

    .line 2142913
    invoke-virtual {v2}, LX/EcF;->l()LX/EcG;

    move-result-object v1

    iput-object v1, v0, LX/Ec4;->e:LX/EcG;

    .line 2142914
    invoke-virtual {v0}, LX/EWj;->t()V

    .line 2142915
    :goto_0
    iget v1, v0, LX/Ec4;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, LX/Ec4;->a:I

    .line 2142916
    move-object v0, v0

    .line 2142917
    invoke-virtual {v0}, LX/Ec4;->l()LX/EcH;

    move-result-object v0

    iput-object v0, p0, LX/Eb2;->a:LX/EcH;

    .line 2142918
    return-void

    .line 2142919
    :cond_1
    iget-object v1, v0, LX/Ec4;->f:LX/EZ7;

    invoke-virtual {v2}, LX/EcF;->l()LX/EcG;

    move-result-object p1

    invoke-virtual {v1, p1}, LX/EZ7;->a(LX/EWp;)LX/EZ7;

    goto :goto_0
.end method

.method public constructor <init>(II[BLX/Eau;)V
    .locals 6

    .prologue
    .line 2142900
    iget-object v0, p4, LX/Eau;->a:LX/Eat;

    move-object v4, v0

    .line 2142901
    iget-object v0, p4, LX/Eau;->b:LX/Eas;

    move-object v0, v0

    .line 2142902
    invoke-static {v0}, LX/Ecs;->a(Ljava/lang/Object;)LX/Ecs;

    move-result-object v5

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/Eb2;-><init>(II[BLX/Eat;LX/Ecs;)V

    .line 2142903
    return-void
.end method

.method public constructor <init>(LX/EcH;)V
    .locals 0

    .prologue
    .line 2142866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142867
    iput-object p1, p0, LX/Eb2;->a:LX/EcH;

    .line 2142868
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2142897
    iget-object v0, p0, LX/Eb2;->a:LX/EcH;

    .line 2142898
    iget p0, v0, LX/EcH;->senderKeyId_:I

    move v0, p0

    .line 2142899
    return v0
.end method

.method public final a(LX/Eaz;)V
    .locals 2

    .prologue
    .line 2142890
    invoke-static {}, LX/Ec7;->w()LX/Ec7;

    move-result-object v0

    .line 2142891
    iget v1, p1, LX/Eaz;->c:I

    move v1, v1

    .line 2142892
    invoke-virtual {v0, v1}, LX/Ec7;->a(I)LX/Ec7;

    move-result-object v0

    .line 2142893
    iget-object v1, p1, LX/Eaz;->d:[B

    move-object v1, v1

    .line 2142894
    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ec7;->a(LX/EWc;)LX/Ec7;

    move-result-object v0

    invoke-virtual {v0}, LX/Ec7;->l()LX/Ec8;

    move-result-object v0

    .line 2142895
    iget-object v1, p0, LX/Eb2;->a:LX/EcH;

    invoke-virtual {v1}, LX/EcH;->r()LX/Ec4;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Ec4;->a(LX/Ec8;)LX/Ec4;

    move-result-object v0

    invoke-virtual {v0}, LX/Ec4;->l()LX/EcH;

    move-result-object v0

    iput-object v0, p0, LX/Eb2;->a:LX/EcH;

    .line 2142896
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 2142884
    iget-object v0, p0, LX/Eb2;->a:LX/EcH;

    .line 2142885
    iget-object v1, v0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    move-object v0, v1

    .line 2142886
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcC;

    .line 2142887
    iget p0, v0, LX/EcC;->iteration_:I

    move v0, p0

    .line 2142888
    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    .line 2142889
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/Eaz;
    .locals 3

    .prologue
    .line 2142877
    new-instance v0, LX/Eaz;

    iget-object v1, p0, LX/Eb2;->a:LX/EcH;

    .line 2142878
    iget-object v2, v1, LX/EcH;->senderChainKey_:LX/Ec8;

    move-object v1, v2

    .line 2142879
    iget v2, v1, LX/Ec8;->iteration_:I

    move v1, v2

    .line 2142880
    iget-object v2, p0, LX/Eb2;->a:LX/EcH;

    .line 2142881
    iget-object p0, v2, LX/EcH;->senderChainKey_:LX/Ec8;

    move-object v2, p0

    .line 2142882
    iget-object p0, v2, LX/Ec8;->seed_:LX/EWc;

    move-object v2, p0

    .line 2142883
    invoke-virtual {v2}, LX/EWc;->d()[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Eaz;-><init>(I[B)V

    return-object v0
.end method

.method public final c()LX/Eat;
    .locals 2

    .prologue
    .line 2142873
    iget-object v0, p0, LX/Eb2;->a:LX/EcH;

    .line 2142874
    iget-object v1, v0, LX/EcH;->senderSigningKey_:LX/EcG;

    move-object v0, v1

    .line 2142875
    iget-object v1, v0, LX/EcG;->public_:LX/EWc;

    move-object v0, v1

    .line 2142876
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/Eas;
    .locals 1

    .prologue
    .line 2142869
    iget-object v0, p0, LX/Eb2;->a:LX/EcH;

    .line 2142870
    iget-object p0, v0, LX/EcH;->senderSigningKey_:LX/EcG;

    move-object v0, p0

    .line 2142871
    iget-object p0, v0, LX/EcG;->private_:LX/EWc;

    move-object v0, p0

    .line 2142872
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    invoke-static {v0}, LX/Ear;->a([B)LX/Eas;

    move-result-object v0

    return-object v0
.end method
