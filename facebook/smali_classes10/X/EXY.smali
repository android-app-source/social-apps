.class public final LX/EXY;
.super LX/EWy;
.source ""

# interfaces
.implements LX/EXX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWy",
        "<",
        "LX/EXb;",
        "LX/EXY;",
        ">;",
        "LX/EXX;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field public d:Z

.field public e:Z

.field private f:LX/EXa;

.field private g:Ljava/lang/Object;

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2134318
    invoke-direct {p0}, LX/EWy;-><init>()V

    .line 2134319
    const-string v0, ""

    iput-object v0, p0, LX/EXY;->b:Ljava/lang/Object;

    .line 2134320
    const-string v0, ""

    iput-object v0, p0, LX/EXY;->c:Ljava/lang/Object;

    .line 2134321
    sget-object v0, LX/EXa;->SPEED:LX/EXa;

    iput-object v0, p0, LX/EXY;->f:LX/EXa;

    .line 2134322
    const-string v0, ""

    iput-object v0, p0, LX/EXY;->g:Ljava/lang/Object;

    .line 2134323
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXY;->k:Ljava/util/List;

    .line 2134324
    invoke-direct {p0}, LX/EXY;->w()V

    .line 2134325
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2134326
    invoke-direct {p0, p1}, LX/EWy;-><init>(LX/EYd;)V

    .line 2134327
    const-string v0, ""

    iput-object v0, p0, LX/EXY;->b:Ljava/lang/Object;

    .line 2134328
    const-string v0, ""

    iput-object v0, p0, LX/EXY;->c:Ljava/lang/Object;

    .line 2134329
    sget-object v0, LX/EXa;->SPEED:LX/EXa;

    iput-object v0, p0, LX/EXY;->f:LX/EXa;

    .line 2134330
    const-string v0, ""

    iput-object v0, p0, LX/EXY;->g:Ljava/lang/Object;

    .line 2134331
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXY;->k:Ljava/util/List;

    .line 2134332
    invoke-direct {p0}, LX/EXY;->w()V

    .line 2134333
    return-void
.end method

.method private A()LX/EXb;
    .locals 2

    .prologue
    .line 2134334
    invoke-virtual {p0}, LX/EXY;->l()LX/EXb;

    move-result-object v0

    .line 2134335
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2134336
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2134337
    :cond_0
    return-object v0
.end method

.method private D()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2134338
    iget-object v0, p0, LX/EXY;->l:LX/EZ2;

    if-nez v0, :cond_0

    .line 2134339
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EXY;->k:Ljava/util/List;

    iget v0, p0, LX/EXY;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2134340
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2134341
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EXY;->l:LX/EZ2;

    .line 2134342
    const/4 v0, 0x0

    iput-object v0, p0, LX/EXY;->k:Ljava/util/List;

    .line 2134343
    :cond_0
    iget-object v0, p0, LX/EXY;->l:LX/EZ2;

    return-object v0

    .line 2134344
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/EXa;)LX/EXY;
    .locals 1

    .prologue
    .line 2134345
    if-nez p1, :cond_0

    .line 2134346
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2134347
    :cond_0
    iget v0, p0, LX/EXY;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/EXY;->a:I

    .line 2134348
    iput-object p1, p0, LX/EXY;->f:LX/EXa;

    .line 2134349
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134350
    return-object p0
.end method

.method private d(LX/EWY;)LX/EXY;
    .locals 1

    .prologue
    .line 2134351
    instance-of v0, p1, LX/EXb;

    if-eqz v0, :cond_0

    .line 2134352
    check-cast p1, LX/EXb;

    invoke-virtual {p0, p1}, LX/EXY;->a(LX/EXb;)LX/EXY;

    move-result-object p0

    .line 2134353
    :goto_0
    return-object p0

    .line 2134354
    :cond_0
    invoke-super {p0, p1}, LX/EWy;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EXY;
    .locals 4

    .prologue
    .line 2134355
    const/4 v2, 0x0

    .line 2134356
    :try_start_0
    sget-object v0, LX/EXb;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXb;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2134357
    if-eqz v0, :cond_0

    .line 2134358
    invoke-virtual {p0, v0}, LX/EXY;->a(LX/EXb;)LX/EXY;

    .line 2134359
    :cond_0
    return-object p0

    .line 2134360
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2134361
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2134362
    check-cast v0, LX/EXb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2134363
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2134364
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2134365
    invoke-virtual {p0, v1}, LX/EXY;->a(LX/EXb;)LX/EXY;

    :cond_1
    throw v0

    .line 2134366
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 2134367
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2134368
    invoke-direct {p0}, LX/EXY;->D()LX/EZ2;

    .line 2134369
    :cond_0
    return-void
.end method

.method public static x()LX/EXY;
    .locals 1

    .prologue
    .line 2134370
    new-instance v0, LX/EXY;

    invoke-direct {v0}, LX/EXY;-><init>()V

    return-object v0
.end method

.method private y()LX/EXY;
    .locals 2

    .prologue
    .line 2134311
    invoke-static {}, LX/EXY;->x()LX/EXY;

    move-result-object v0

    invoke-virtual {p0}, LX/EXY;->l()LX/EXb;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EXY;->a(LX/EXb;)LX/EXY;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2134371
    invoke-direct {p0, p1}, LX/EXY;->d(LX/EWY;)LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2134372
    invoke-direct {p0, p1, p2}, LX/EXY;->d(LX/EWd;LX/EYZ;)LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EXb;)LX/EXY;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2134373
    sget-object v1, LX/EXb;->c:LX/EXb;

    move-object v1, v1

    .line 2134374
    if-ne p1, v1, :cond_0

    .line 2134375
    :goto_0
    return-object p0

    .line 2134376
    :cond_0
    const/4 v1, 0x1

    .line 2134377
    iget v2, p1, LX/EXb;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_10

    :goto_1
    move v1, v1

    .line 2134378
    if-eqz v1, :cond_1

    .line 2134379
    iget v1, p0, LX/EXY;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/EXY;->a:I

    .line 2134380
    iget-object v1, p1, LX/EXb;->javaPackage_:Ljava/lang/Object;

    iput-object v1, p0, LX/EXY;->b:Ljava/lang/Object;

    .line 2134381
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134382
    :cond_1
    iget v1, p1, LX/EXb;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_11

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2134383
    if-eqz v1, :cond_2

    .line 2134384
    iget v1, p0, LX/EXY;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/EXY;->a:I

    .line 2134385
    iget-object v1, p1, LX/EXb;->javaOuterClassname_:Ljava/lang/Object;

    iput-object v1, p0, LX/EXY;->c:Ljava/lang/Object;

    .line 2134386
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134387
    :cond_2
    iget v1, p1, LX/EXb;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_12

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 2134388
    if-eqz v1, :cond_3

    .line 2134389
    iget-boolean v1, p1, LX/EXb;->javaMultipleFiles_:Z

    move v1, v1

    .line 2134390
    iget v2, p0, LX/EXY;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, LX/EXY;->a:I

    .line 2134391
    iput-boolean v1, p0, LX/EXY;->d:Z

    .line 2134392
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134393
    :cond_3
    iget v1, p1, LX/EXb;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_13

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 2134394
    if-eqz v1, :cond_4

    .line 2134395
    iget-boolean v1, p1, LX/EXb;->javaGenerateEqualsAndHash_:Z

    move v1, v1

    .line 2134396
    iget v2, p0, LX/EXY;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, LX/EXY;->a:I

    .line 2134397
    iput-boolean v1, p0, LX/EXY;->e:Z

    .line 2134398
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134399
    :cond_4
    iget v1, p1, LX/EXb;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_14

    const/4 v1, 0x1

    :goto_5
    move v1, v1

    .line 2134400
    if-eqz v1, :cond_5

    .line 2134401
    iget-object v1, p1, LX/EXb;->optimizeFor_:LX/EXa;

    move-object v1, v1

    .line 2134402
    invoke-direct {p0, v1}, LX/EXY;->a(LX/EXa;)LX/EXY;

    .line 2134403
    :cond_5
    iget v1, p1, LX/EXb;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_15

    const/4 v1, 0x1

    :goto_6
    move v1, v1

    .line 2134404
    if-eqz v1, :cond_6

    .line 2134405
    iget v1, p0, LX/EXY;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, LX/EXY;->a:I

    .line 2134406
    iget-object v1, p1, LX/EXb;->goPackage_:Ljava/lang/Object;

    iput-object v1, p0, LX/EXY;->g:Ljava/lang/Object;

    .line 2134407
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134408
    :cond_6
    iget v1, p1, LX/EXb;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_16

    const/4 v1, 0x1

    :goto_7
    move v1, v1

    .line 2134409
    if-eqz v1, :cond_7

    .line 2134410
    iget-boolean v1, p1, LX/EXb;->ccGenericServices_:Z

    move v1, v1

    .line 2134411
    iget v2, p0, LX/EXY;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, LX/EXY;->a:I

    .line 2134412
    iput-boolean v1, p0, LX/EXY;->h:Z

    .line 2134413
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134414
    :cond_7
    iget v1, p1, LX/EXb;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_17

    const/4 v1, 0x1

    :goto_8
    move v1, v1

    .line 2134415
    if-eqz v1, :cond_8

    .line 2134416
    iget-boolean v1, p1, LX/EXb;->javaGenericServices_:Z

    move v1, v1

    .line 2134417
    iget v2, p0, LX/EXY;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, LX/EXY;->a:I

    .line 2134418
    iput-boolean v1, p0, LX/EXY;->i:Z

    .line 2134419
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134420
    :cond_8
    iget v1, p1, LX/EXb;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_18

    const/4 v1, 0x1

    :goto_9
    move v1, v1

    .line 2134421
    if-eqz v1, :cond_9

    .line 2134422
    iget-boolean v1, p1, LX/EXb;->pyGenericServices_:Z

    move v1, v1

    .line 2134423
    iget v2, p0, LX/EXY;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, LX/EXY;->a:I

    .line 2134424
    iput-boolean v1, p0, LX/EXY;->j:Z

    .line 2134425
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134426
    :cond_9
    iget-object v1, p0, LX/EXY;->l:LX/EZ2;

    if-nez v1, :cond_d

    .line 2134427
    iget-object v0, p1, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2134428
    iget-object v0, p0, LX/EXY;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2134429
    iget-object v0, p1, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    iput-object v0, p0, LX/EXY;->k:Ljava/util/List;

    .line 2134430
    iget v0, p0, LX/EXY;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, LX/EXY;->a:I

    .line 2134431
    :goto_a
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134432
    :cond_a
    :goto_b
    invoke-virtual {p0, p1}, LX/EWy;->a(LX/EX1;)V

    .line 2134433
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto/16 :goto_0

    .line 2134434
    :cond_b
    iget v0, p0, LX/EXY;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_c

    .line 2134435
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EXY;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EXY;->k:Ljava/util/List;

    .line 2134436
    iget v0, p0, LX/EXY;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, LX/EXY;->a:I

    .line 2134437
    :cond_c
    iget-object v0, p0, LX/EXY;->k:Ljava/util/List;

    iget-object v1, p1, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_a

    .line 2134438
    :cond_d
    iget-object v1, p1, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 2134439
    iget-object v1, p0, LX/EXY;->l:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2134440
    iget-object v1, p0, LX/EXY;->l:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2134441
    iput-object v0, p0, LX/EXY;->l:LX/EZ2;

    .line 2134442
    iget-object v1, p1, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    iput-object v1, p0, LX/EXY;->k:Ljava/util/List;

    .line 2134443
    iget v1, p0, LX/EXY;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, LX/EXY;->a:I

    .line 2134444
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_e

    invoke-direct {p0}, LX/EXY;->D()LX/EZ2;

    move-result-object v0

    :cond_e
    iput-object v0, p0, LX/EXY;->l:LX/EZ2;

    goto :goto_b

    .line 2134445
    :cond_f
    iget-object v0, p0, LX/EXY;->l:LX/EZ2;

    iget-object v1, p1, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_b

    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_13
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_14
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_15
    const/4 v1, 0x0

    goto/16 :goto_6

    :cond_16
    const/4 v1, 0x0

    goto/16 :goto_7

    :cond_17
    const/4 v1, 0x0

    goto/16 :goto_8

    :cond_18
    const/4 v1, 0x0

    goto/16 :goto_9
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2134446
    move v0, v1

    .line 2134447
    :goto_0
    iget-object v2, p0, LX/EXY;->l:LX/EZ2;

    if-nez v2, :cond_3

    .line 2134448
    iget-object v2, p0, LX/EXY;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2134449
    :goto_1
    move v2, v2

    .line 2134450
    if-ge v0, v2, :cond_2

    .line 2134451
    iget-object v2, p0, LX/EXY;->l:LX/EZ2;

    if-nez v2, :cond_4

    .line 2134452
    iget-object v2, p0, LX/EXY;->k:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYB;

    .line 2134453
    :goto_2
    move-object v2, v2

    .line 2134454
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2134455
    :cond_0
    :goto_3
    return v1

    .line 2134456
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2134457
    :cond_2
    invoke-virtual {p0}, LX/EWy;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2134458
    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    iget-object v2, p0, LX/EXY;->l:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/EXY;->l:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EYB;

    goto :goto_2
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2134317
    invoke-direct {p0, p1, p2}, LX/EXY;->d(LX/EWd;LX/EYZ;)LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2134459
    invoke-direct {p0}, LX/EXY;->y()LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2134316
    invoke-direct {p0, p1, p2}, LX/EXY;->d(LX/EWd;LX/EYZ;)LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2134315
    invoke-direct {p0}, LX/EXY;->y()LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2134314
    invoke-direct {p0, p1}, LX/EXY;->d(LX/EWY;)LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2134313
    invoke-direct {p0}, LX/EXY;->y()LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2134312
    sget-object v0, LX/EYC;->t:LX/EYn;

    const-class v1, LX/EXb;

    const-class v2, LX/EXY;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2134310
    sget-object v0, LX/EYC;->s:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2134309
    invoke-direct {p0}, LX/EXY;->y()LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2134308
    invoke-virtual {p0}, LX/EXY;->l()LX/EXb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2134307
    invoke-direct {p0}, LX/EXY;->A()LX/EXb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2134306
    invoke-virtual {p0}, LX/EXY;->l()LX/EXb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2134305
    invoke-direct {p0}, LX/EXY;->A()LX/EXb;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EXb;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2134256
    new-instance v2, LX/EXb;

    invoke-direct {v2, p0}, LX/EXb;-><init>(LX/EWy;)V

    .line 2134257
    iget v3, p0, LX/EXY;->a:I

    .line 2134258
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    .line 2134259
    :goto_0
    iget-object v1, p0, LX/EXY;->b:Ljava/lang/Object;

    .line 2134260
    iput-object v1, v2, LX/EXb;->javaPackage_:Ljava/lang/Object;

    .line 2134261
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2134262
    or-int/lit8 v0, v0, 0x2

    .line 2134263
    :cond_0
    iget-object v1, p0, LX/EXY;->c:Ljava/lang/Object;

    .line 2134264
    iput-object v1, v2, LX/EXb;->javaOuterClassname_:Ljava/lang/Object;

    .line 2134265
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2134266
    or-int/lit8 v0, v0, 0x4

    .line 2134267
    :cond_1
    iget-boolean v1, p0, LX/EXY;->d:Z

    .line 2134268
    iput-boolean v1, v2, LX/EXb;->javaMultipleFiles_:Z

    .line 2134269
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2134270
    or-int/lit8 v0, v0, 0x8

    .line 2134271
    :cond_2
    iget-boolean v1, p0, LX/EXY;->e:Z

    .line 2134272
    iput-boolean v1, v2, LX/EXb;->javaGenerateEqualsAndHash_:Z

    .line 2134273
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2134274
    or-int/lit8 v0, v0, 0x10

    .line 2134275
    :cond_3
    iget-object v1, p0, LX/EXY;->f:LX/EXa;

    .line 2134276
    iput-object v1, v2, LX/EXb;->optimizeFor_:LX/EXa;

    .line 2134277
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 2134278
    or-int/lit8 v0, v0, 0x20

    .line 2134279
    :cond_4
    iget-object v1, p0, LX/EXY;->g:Ljava/lang/Object;

    .line 2134280
    iput-object v1, v2, LX/EXb;->goPackage_:Ljava/lang/Object;

    .line 2134281
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 2134282
    or-int/lit8 v0, v0, 0x40

    .line 2134283
    :cond_5
    iget-boolean v1, p0, LX/EXY;->h:Z

    .line 2134284
    iput-boolean v1, v2, LX/EXb;->ccGenericServices_:Z

    .line 2134285
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    .line 2134286
    or-int/lit16 v0, v0, 0x80

    .line 2134287
    :cond_6
    iget-boolean v1, p0, LX/EXY;->i:Z

    .line 2134288
    iput-boolean v1, v2, LX/EXb;->javaGenericServices_:Z

    .line 2134289
    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    .line 2134290
    or-int/lit16 v0, v0, 0x100

    .line 2134291
    :cond_7
    iget-boolean v1, p0, LX/EXY;->j:Z

    .line 2134292
    iput-boolean v1, v2, LX/EXb;->pyGenericServices_:Z

    .line 2134293
    iget-object v1, p0, LX/EXY;->l:LX/EZ2;

    if-nez v1, :cond_9

    .line 2134294
    iget v1, p0, LX/EXY;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    .line 2134295
    iget-object v1, p0, LX/EXY;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXY;->k:Ljava/util/List;

    .line 2134296
    iget v1, p0, LX/EXY;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, LX/EXY;->a:I

    .line 2134297
    :cond_8
    iget-object v1, p0, LX/EXY;->k:Ljava/util/List;

    .line 2134298
    iput-object v1, v2, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    .line 2134299
    :goto_1
    iput v0, v2, LX/EXb;->bitField0_:I

    .line 2134300
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2134301
    return-object v2

    .line 2134302
    :cond_9
    iget-object v1, p0, LX/EXY;->l:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2134303
    iput-object v1, v2, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    .line 2134304
    goto :goto_1

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic m()LX/EWy;
    .locals 1

    .prologue
    .line 2134255
    invoke-direct {p0}, LX/EXY;->y()LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2134253
    sget-object v0, LX/EXb;->c:LX/EXb;

    move-object v0, v0

    .line 2134254
    return-object v0
.end method
