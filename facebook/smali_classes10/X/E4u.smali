.class public LX/E4u;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E4s;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E4u;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E4v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2077505
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E4u;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E4v;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077502
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2077503
    iput-object p1, p0, LX/E4u;->b:LX/0Ot;

    .line 2077504
    return-void
.end method

.method public static a(LX/0QB;)LX/E4u;
    .locals 4

    .prologue
    .line 2077489
    sget-object v0, LX/E4u;->c:LX/E4u;

    if-nez v0, :cond_1

    .line 2077490
    const-class v1, LX/E4u;

    monitor-enter v1

    .line 2077491
    :try_start_0
    sget-object v0, LX/E4u;->c:LX/E4u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077492
    if-eqz v2, :cond_0

    .line 2077493
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077494
    new-instance v3, LX/E4u;

    const/16 p0, 0x3117

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E4u;-><init>(LX/0Ot;)V

    .line 2077495
    move-object v0, v3

    .line 2077496
    sput-object v0, LX/E4u;->c:LX/E4u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077497
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077498
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077499
    :cond_1
    sget-object v0, LX/E4u;->c:LX/E4u;

    return-object v0

    .line 2077500
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2077488
    const v0, 0x24af90d1

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 2077482
    check-cast p2, LX/E4t;

    .line 2077483
    iget-object v0, p0, LX/E4u;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/E4t;->a:LX/1X1;

    .line 2077484
    invoke-static {p1, v0}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object p0

    .line 2077485
    const p2, 0x24af90d1

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 2077486
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 2077487
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2077459
    invoke-static {}, LX/1dS;->b()V

    .line 2077460
    iget v0, p1, LX/1dQ;->b:I

    .line 2077461
    packed-switch v0, :pswitch_data_0

    .line 2077462
    :goto_0
    return-object v2

    .line 2077463
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2077464
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2077465
    check-cast v1, LX/E4t;

    .line 2077466
    iget-object v3, p0, LX/E4u;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/E4v;

    iget-object v5, v1, LX/E4t;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v6, v1, LX/E4t;->c:LX/2ja;

    iget-object v7, v1, LX/E4t;->d:LX/1Pq;

    iget-object v8, v1, LX/E4t;->e:LX/E2a;

    const/4 v1, 0x1

    .line 2077467
    iput-boolean v1, v8, LX/E2a;->a:Z

    .line 2077468
    iget-object p1, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object p1, p1

    .line 2077469
    iget-object p2, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object p2, p2

    .line 2077470
    iget-object p0, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object p0, p0

    .line 2077471
    invoke-interface {p0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object p0

    sget-object v0, LX/Cfc;->INLINE_EXPANSION_TAP:LX/Cfc;

    invoke-virtual {v6, p1, p2, p0, v0}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 2077472
    new-array p1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x0

    invoke-static {v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    aput-object p0, p1, p2

    invoke-interface {v7, p1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2077473
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x24af90d1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/E4s;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2077474
    new-instance v1, LX/E4t;

    invoke-direct {v1, p0}, LX/E4t;-><init>(LX/E4u;)V

    .line 2077475
    sget-object v2, LX/E4u;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E4s;

    .line 2077476
    if-nez v2, :cond_0

    .line 2077477
    new-instance v2, LX/E4s;

    invoke-direct {v2}, LX/E4s;-><init>()V

    .line 2077478
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/E4s;->a$redex0(LX/E4s;LX/1De;IILX/E4t;)V

    .line 2077479
    move-object v1, v2

    .line 2077480
    move-object v0, v1

    .line 2077481
    return-object v0
.end method
