.class public final LX/Dke;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2034378
    const-class v1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel;

    const v0, 0x6ee673c9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PageAppointmentsCalendarQuery"

    const-string v6, "93aec149251ac9ab819174aaabf4b2c0"

    const-string v7, "node"

    const-string v8, "10155156347471729"

    const-string v9, "10155259089481729"

    .line 2034379
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2034380
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2034381
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2034382
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2034383
    sparse-switch v0, :sswitch_data_0

    .line 2034384
    :goto_0
    return-object p1

    .line 2034385
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2034386
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2034387
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2034388
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2034389
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2034390
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x47f307db -> :sswitch_3
        -0x2fe52f35 -> :sswitch_4
        -0x23113c94 -> :sswitch_5
        0x58705dc -> :sswitch_0
        0x69940cd -> :sswitch_2
        0x711a52f3 -> :sswitch_1
    .end sparse-switch
.end method
