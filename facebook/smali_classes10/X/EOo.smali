.class public final LX/EOo;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;

.field private final b:LX/CxV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

.field private final d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;LX/CxV;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            "TE;Z)V"
        }
    .end annotation

    .prologue
    .line 2115295
    iput-object p1, p0, LX/EOo;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;

    invoke-direct {p0}, LX/2h0;-><init>()V

    .line 2115296
    iput-object p4, p0, LX/EOo;->b:LX/CxV;

    .line 2115297
    iput-object p2, p0, LX/EOo;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    .line 2115298
    iput-object p3, p0, LX/EOo;->d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    .line 2115299
    if-eqz p5, :cond_0

    .line 2115300
    const v0, 0x7f081cf6

    iput v0, p0, LX/EOo;->e:I

    .line 2115301
    const v0, 0x7f081cf8

    iput v0, p0, LX/EOo;->f:I

    .line 2115302
    :goto_0
    return-void

    .line 2115303
    :cond_0
    const v0, 0x7f082287

    iput v0, p0, LX/EOo;->e:I

    .line 2115304
    const v0, 0x7f082288

    iput v0, p0, LX/EOo;->f:I

    goto :goto_0
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2115305
    iget-object v0, p0, LX/EOo;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;->f:LX/0kL;

    new-instance v1, LX/27k;

    iget v2, p0, LX/EOo;->f:I

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2115306
    iget-object v1, p0, LX/EOo;->d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    iget-object v2, p0, LX/EOo;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    iget-object v3, p0, LX/EOo;->b:LX/CxV;

    .line 2115307
    invoke-static {v1, v2, v3}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;LX/CxV;)V

    .line 2115308
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2115309
    iget-object v0, p0, LX/EOo;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;->f:LX/0kL;

    new-instance v1, LX/27k;

    iget v2, p0, LX/EOo;->e:I

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2115310
    return-void
.end method
