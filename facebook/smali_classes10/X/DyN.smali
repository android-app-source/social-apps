.class public final LX/DyN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/photos/albums/protocols/MediasetQueryInterfaces$DefaultMediaSetMediaConnection;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V
    .locals 0

    .prologue
    .line 2064402
    iput-object p1, p0, LX/DyN;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2064403
    iget-object v0, p0, LX/DyN;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fx;

    iget-object v1, p0, LX/DyN;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, LX/DyN;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v3, v3, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    .line 2064404
    iget-object v4, v3, LX/9av;->e:Ljava/lang/String;

    move-object v3, v4

    .line 2064405
    const/16 v4, 0x28

    iget-object v5, p0, LX/DyN;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget v5, v5, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->p:I

    iget-object v6, p0, LX/DyN;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget v6, v6, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->p:I

    iget-object v7, p0, LX/DyN;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v7, v7, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->o:LX/5hJ;

    .line 2064406
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2064407
    sget-object v8, LX/9fw;->a:[I

    invoke-virtual {v7}, LX/5hJ;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 2064408
    invoke-static/range {v0 .. v6}, LX/9fx;->a(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    :goto_0
    move-object v0, v8

    .line 2064409
    return-object v0

    .line 2064410
    :pswitch_0
    invoke-static/range {v0 .. v6}, LX/9fx;->b(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    goto :goto_0

    .line 2064411
    :pswitch_1
    invoke-static/range {v0 .. v6}, LX/9fx;->c(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    goto :goto_0

    .line 2064412
    :pswitch_2
    invoke-static/range {v0 .. v6}, LX/9fx;->d(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    goto :goto_0

    .line 2064413
    :pswitch_3
    invoke-static/range {v0 .. v6}, LX/9fx;->e(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    goto :goto_0

    .line 2064414
    :pswitch_4
    invoke-static/range {v0 .. v6}, LX/9fx;->f(LX/9fx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
