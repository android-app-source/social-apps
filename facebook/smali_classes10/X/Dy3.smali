.class public LX/Dy3;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/Dy0;

.field public d:Landroid/view/View$OnClickListener;

.field public e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field private f:LX/Dy1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/Dy1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;",
            "LX/Dy1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2063934
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2063935
    iput-object p1, p0, LX/Dy3;->a:Landroid/content/Context;

    .line 2063936
    iput-object p2, p0, LX/Dy3;->b:LX/0Px;

    .line 2063937
    iput-object p3, p0, LX/Dy3;->f:LX/Dy1;

    .line 2063938
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2063939
    iget-object v0, p0, LX/Dy3;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2063940
    iget-object v0, p0, LX/Dy3;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2063941
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2063942
    invoke-virtual {p0, p1}, LX/Dy3;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063943
    iget-object v1, p0, LX/Dy3;->f:LX/Dy1;

    .line 2063944
    invoke-interface {v0}, LX/1oU;->b()LX/1Fd;

    move-result-object v2

    invoke-static {v1, v2}, LX/Dy1;->a(LX/Dy1;LX/1Fd;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 2063945
    if-eqz p2, :cond_0

    .line 2063946
    check-cast p2, LX/Dy4;

    .line 2063947
    :goto_0
    iget-object v2, p0, LX/Dy3;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p2, v0, v2, v1}, LX/Dy4;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;ZLjava/lang/String;)V

    .line 2063948
    iget-object v1, p0, LX/Dy3;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, LX/Dy4;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2063949
    invoke-virtual {p2, v0}, LX/Dy4;->setTag(Ljava/lang/Object;)V

    .line 2063950
    return-object p2

    .line 2063951
    :cond_0
    new-instance p2, LX/Dy4;

    iget-object v2, p0, LX/Dy3;->a:Landroid/content/Context;

    invoke-direct {p2, v2}, LX/Dy4;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method
