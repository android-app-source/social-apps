.class public LX/D4z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/1V8;

.field private final b:LX/1VF;

.field public final c:F

.field public final d:I

.field public final e:I

.field public final f:I


# direct methods
.method public constructor <init>(LX/1V8;LX/1VF;Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1963144
    iput-object p1, p0, LX/D4z;->a:LX/1V8;

    .line 1963145
    iput-object p2, p0, LX/D4z;->b:LX/1VF;

    .line 1963146
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1963147
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, LX/D4z;->c:F

    .line 1963148
    const v1, 0x7f0b00e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/D4z;->d:I

    .line 1963149
    const v1, 0x7f0b00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 1963150
    iput v1, p0, LX/D4z;->e:I

    .line 1963151
    const v1, 0x7f0b00e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D4z;->f:I

    .line 1963152
    return-void
.end method

.method public static a(LX/0QB;)LX/D4z;
    .locals 6

    .prologue
    .line 1963153
    const-class v1, LX/D4z;

    monitor-enter v1

    .line 1963154
    :try_start_0
    sget-object v0, LX/D4z;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1963155
    sput-object v2, LX/D4z;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1963156
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1963157
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1963158
    new-instance p0, LX/D4z;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v3

    check-cast v3, LX/1V8;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v4

    check-cast v4, LX/1VF;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, LX/D4z;-><init>(LX/1V8;LX/1VF;Landroid/content/Context;)V

    .line 1963159
    move-object v0, p0

    .line 1963160
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1963161
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D4z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1963162
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1963163
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
