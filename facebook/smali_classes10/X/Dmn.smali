.class public LX/Dmn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field private final c:LX/11S;

.field public final d:LX/Dih;

.field private final e:LX/DnP;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field public final g:LX/Dka;


# direct methods
.method private constructor <init>(LX/0Or;Landroid/content/Context;LX/11S;LX/Dih;LX/DnP;Lcom/facebook/content/SecureContextHelper;LX/Dka;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Landroid/content/Context;",
            "LX/11S;",
            "LX/Dih;",
            "LX/DnP;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/Dka;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2039647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2039648
    iput-object p1, p0, LX/Dmn;->a:LX/0Or;

    .line 2039649
    iput-object p2, p0, LX/Dmn;->b:Landroid/content/Context;

    .line 2039650
    iput-object p3, p0, LX/Dmn;->c:LX/11S;

    .line 2039651
    iput-object p4, p0, LX/Dmn;->d:LX/Dih;

    .line 2039652
    iput-object p5, p0, LX/Dmn;->e:LX/DnP;

    .line 2039653
    iput-object p6, p0, LX/Dmn;->f:Lcom/facebook/content/SecureContextHelper;

    .line 2039654
    iput-object p7, p0, LX/Dmn;->g:LX/Dka;

    .line 2039655
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 2039734
    invoke-direct {p0, p1, p2}, LX/Dmn;->b(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 2039735
    iget v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-ne v0, v3, :cond_0

    .line 2039736
    iget-object v0, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082bb8

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2039737
    iget-object v0, p0, LX/Dmn;->b:Landroid/content/Context;

    const v1, 0x7f082b90

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2039738
    iget-object v0, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f0a07fc

    .line 2039739
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2039740
    new-instance v6, LX/Dmp;

    new-instance v0, LX/Dmo;

    const v1, 0x7f02085b

    invoke-direct/range {v0 .. v5}, LX/Dmo;-><init>(ILjava/lang/String;ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x0

    invoke-direct {v6, v0, v1}, LX/Dmp;-><init>(LX/Dmo;LX/Dmo;)V

    return-object v6

    .line 2039741
    :cond_0
    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-wide v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->f:J

    invoke-direct {p0, v0, v1}, LX/Dmn;->a(J)Ljava/lang/String;

    move-result-object v2

    .line 2039742
    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v4, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->e:Ljava/lang/String;

    .line 2039743
    iget-object v0, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    if-ne v0, v3, :cond_1

    const v0, 0x7f0a07f1

    goto :goto_0

    :cond_1
    const v0, 0x7f0a07f0

    goto :goto_0
.end method

.method private a(J)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, 0x3e8

    .line 2039731
    iget-object v0, p0, LX/Dmn;->b:Landroid/content/Context;

    mul-long v2, p1, v4

    const v1, 0x1001a

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 2039732
    iget-object v1, p0, LX/Dmn;->c:LX/11S;

    sget-object v2, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    mul-long/2addr v4, p1

    invoke-interface {v1, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 2039733
    iget-object v2, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080086

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/Dmn;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V
    .locals 4

    .prologue
    .line 2039712
    iget-object v0, p0, LX/Dmn;->d:LX/Dih;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-virtual {v0, v1, v2, v3}, LX/Dih;->d(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)V

    .line 2039713
    iget-object v0, p0, LX/Dmn;->e:LX/DnP;

    iget-object v1, p0, LX/Dmn;->b:Landroid/content/Context;

    const-string v2, "booking_request/%s/create_appointment"

    invoke-static {v2}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2039714
    new-instance v1, LX/Djn;

    invoke-direct {v1}, LX/Djn;-><init>()V

    .line 2039715
    const/4 v2, 0x0

    .line 2039716
    iput-boolean v2, v1, LX/Djn;->a:Z

    .line 2039717
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    .line 2039718
    iput-object v2, v1, LX/Djn;->b:Ljava/lang/String;

    .line 2039719
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    .line 2039720
    iput-object v2, v1, LX/Djn;->e:Ljava/lang/String;

    .line 2039721
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->c:Ljava/lang/String;

    .line 2039722
    iput-object v2, v1, LX/Djn;->g:Ljava/lang/String;

    .line 2039723
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->i:Ljava/lang/String;

    .line 2039724
    iput-object v2, v1, LX/Djn;->k:Ljava/lang/String;

    .line 2039725
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->j:Ljava/lang/String;

    .line 2039726
    iput-object v2, v1, LX/Djn;->l:Ljava/lang/String;

    .line 2039727
    const-string v2, "extra_create_booking_appointment_model"

    invoke-virtual {v1}, LX/Djn;->a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2039728
    const-string v1, "referrer"

    const-string v2, "banner"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2039729
    iget-object v1, p0, LX/Dmn;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2039730
    return-void
.end method

.method public static b(LX/0QB;)LX/Dmn;
    .locals 8

    .prologue
    .line 2039710
    new-instance v0, LX/Dmn;

    const/16 v1, 0x19e

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v3

    check-cast v3, LX/11S;

    invoke-static {p0}, LX/Dih;->b(LX/0QB;)LX/Dih;

    move-result-object v4

    check-cast v4, LX/Dih;

    invoke-static {p0}, LX/DnP;->a(LX/0QB;)LX/DnP;

    move-result-object v5

    check-cast v5, LX/DnP;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/Dka;->b(LX/0QB;)LX/Dka;

    move-result-object v7

    check-cast v7, LX/Dka;

    invoke-direct/range {v0 .. v7}, LX/Dmn;-><init>(LX/0Or;Landroid/content/Context;LX/11S;LX/Dih;LX/DnP;Lcom/facebook/content/SecureContextHelper;LX/Dka;)V

    .line 2039711
    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039707
    iget v0, p3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    iget v1, p3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    add-int/2addr v0, v1

    iget v1, p3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    add-int/2addr v0, v1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2039708
    invoke-direct {p0, p2, p3}, LX/Dmn;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;

    move-result-object v0

    .line 2039709
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p3}, LX/Dmn;->e(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;
    .locals 2

    .prologue
    .line 2039704
    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    if-ne v0, v1, :cond_0

    .line 2039705
    invoke-direct {p0, p1, p2}, LX/Dmn;->c(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 2039706
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Dme;

    invoke-direct {v0, p0, p2}, LX/Dme;-><init>(LX/Dmn;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    goto :goto_0
.end method

.method private static b(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Z
    .locals 2

    .prologue
    .line 2039703
    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    iget v1, p0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    add-int/2addr v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039702
    new-instance v0, LX/Dmk;

    invoke-direct {v0, p0, p1}, LX/Dmk;-><init>(LX/Dmn;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    return-object v0
.end method

.method private c(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039701
    new-instance v0, LX/Dmf;

    invoke-direct {v0, p0, p1, p2}, LX/Dmf;-><init>(LX/Dmn;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    return-object v0
.end method

.method public static d(LX/Dmn;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/7TY;
    .locals 3

    .prologue
    .line 2039697
    new-instance v0, LX/7TY;

    iget-object v1, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2039698
    const v1, 0x7f082b95

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v1

    const v2, 0x7f02085b

    invoke-virtual {v1, v2}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v2, LX/Dmg;

    invoke-direct {v2, p0, p2}, LX/Dmg;-><init>(LX/Dmn;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2039699
    const v1, 0x7f082b96

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v1

    const v2, 0x7f020818

    invoke-virtual {v1, v2}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v2, LX/Dmh;

    invoke-direct {v2, p0, p1, p2}, LX/Dmh;-><init>(LX/Dmn;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2039700
    return-object v0
.end method

.method private d(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039696
    new-instance v0, LX/Dmd;

    invoke-direct {v0, p0, p1}, LX/Dmd;-><init>(LX/Dmn;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    return-object v0
.end method

.method private e(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2039688
    invoke-static {p2}, LX/Dmn;->b(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    if-lez v0, :cond_0

    .line 2039689
    new-instance v0, LX/Dmp;

    invoke-direct {p0, p1, p2}, LX/Dmn;->g(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmo;

    move-result-object v2

    invoke-direct {v0, v2, v1}, LX/Dmp;-><init>(LX/Dmo;LX/Dmo;)V

    .line 2039690
    :goto_0
    return-object v0

    .line 2039691
    :cond_0
    invoke-static {p2}, LX/Dmn;->b(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    if-lez v0, :cond_1

    .line 2039692
    new-instance v0, LX/Dmp;

    invoke-direct {p0, p1, p2}, LX/Dmn;->f(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmo;

    move-result-object v2

    invoke-direct {v0, v2, v1}, LX/Dmp;-><init>(LX/Dmo;LX/Dmo;)V

    goto :goto_0

    .line 2039693
    :cond_1
    invoke-static {p2}, LX/Dmn;->b(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2039694
    new-instance v0, LX/Dmp;

    invoke-direct {p0, p1, p2}, LX/Dmn;->f(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmo;

    move-result-object v1

    invoke-direct {p0, p1, p2}, LX/Dmn;->g(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmo;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Dmp;-><init>(LX/Dmo;LX/Dmo;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2039695
    goto :goto_0
.end method

.method private f(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmo;
    .locals 9

    .prologue
    .line 2039687
    new-instance v0, LX/Dmo;

    const v1, 0x7f02085b

    iget-object v2, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f013a

    iget v4, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    iget v5, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    add-int/2addr v4, v5

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    iget v8, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    add-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a07fc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iget-object v4, p0, LX/Dmn;->b:Landroid/content/Context;

    const v5, 0x7f082bab

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, p2}, LX/Dmn;->i(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Dmo;-><init>(ILjava/lang/String;ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private g(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmo;
    .locals 8

    .prologue
    .line 2039686
    new-instance v0, LX/Dmo;

    const v1, 0x7f02085b

    iget-object v2, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0139

    iget v4, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->c:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a07fc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iget-object v4, p0, LX/Dmn;->b:Landroid/content/Context;

    const v5, 0x7f082b90

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, p2}, LX/Dmn;->h(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Dmo;-><init>(ILjava/lang/String;ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private h(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039685
    new-instance v0, LX/Dmi;

    invoke-direct {v0, p0, p1, p2}, LX/Dmi;-><init>(LX/Dmn;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    return-object v0
.end method

.method private i(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039684
    new-instance v0, LX/Dmj;

    invoke-direct {v0, p0, p1, p2}, LX/Dmj;-><init>(LX/Dmn;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    return-object v0
.end method

.method private j(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2039669
    iget v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    if-le v0, v5, :cond_0

    .line 2039670
    new-instance v0, LX/Dmo;

    const v1, 0x7f02085b

    iget-object v2, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f013a

    iget v4, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a07fc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iget-object v4, p0, LX/Dmn;->b:Landroid/content/Context;

    const v5, 0x7f082bab

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p2}, LX/Dmn;->c(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Dmo;-><init>(ILjava/lang/String;ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2039671
    new-instance v1, LX/Dmp;

    invoke-direct {v1, v0, v6}, LX/Dmp;-><init>(LX/Dmo;LX/Dmo;)V

    move-object v0, v1

    .line 2039672
    :goto_0
    return-object v0

    .line 2039673
    :cond_0
    iget v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    if-eq v0, v5, :cond_1

    iget v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->b:I

    if-ne v0, v5, :cond_3

    .line 2039674
    :cond_1
    iget v0, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->d:I

    if-ne v0, v5, :cond_2

    .line 2039675
    invoke-direct {p0, p2}, LX/Dmn;->d(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 2039676
    const v0, 0x7f0a07f0

    .line 2039677
    iget-object v1, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v4, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->e:Ljava/lang/String;

    move v3, v0

    .line 2039678
    :goto_1
    new-instance v0, LX/Dmo;

    const v1, 0x7f02085b

    iget-object v2, p2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-wide v8, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->f:J

    invoke-direct {p0, v8, v9}, LX/Dmn;->a(J)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct/range {v0 .. v5}, LX/Dmo;-><init>(ILjava/lang/String;ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2039679
    new-instance v1, LX/Dmp;

    invoke-direct {v1, v0, v6}, LX/Dmp;-><init>(LX/Dmo;LX/Dmo;)V

    move-object v0, v1

    goto :goto_0

    .line 2039680
    :cond_2
    invoke-direct {p0, p1, p2}, LX/Dmn;->k(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 2039681
    const v0, 0x7f0a07fc

    .line 2039682
    iget-object v1, p0, LX/Dmn;->b:Landroid/content/Context;

    const v2, 0x7f082b90

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move v3, v0

    goto :goto_1

    :cond_3
    move-object v0, v6

    .line 2039683
    goto :goto_0
.end method

.method private k(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2039668
    new-instance v0, LX/Dml;

    invoke-direct {v0, p0, p1, p2}, LX/Dml;-><init>(LX/Dmn;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    return-object v0
.end method

.method public static l(LX/Dmn;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/7TY;
    .locals 3

    .prologue
    .line 2039664
    new-instance v0, LX/7TY;

    iget-object v1, p0, LX/Dmn;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2039665
    const v1, 0x7f082b93

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v1

    const v2, 0x7f0207d6

    invoke-virtual {v1, v2}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v2, LX/Dmm;

    invoke-direct {v2, p0, p2}, LX/Dmm;-><init>(LX/Dmn;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2039666
    const v1, 0x7f082b99

    invoke-virtual {v0, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v1

    const v2, 0x7f020818

    invoke-virtual {v1, v2}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v2, LX/Dmc;

    invoke-direct {v2, p0, p1, p2}, LX/Dmc;-><init>(LX/Dmn;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2039667
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2039656
    if-nez p3, :cond_0

    .line 2039657
    const/4 v0, 0x0

    .line 2039658
    :goto_0
    return-object v0

    .line 2039659
    :cond_0
    iget-object v0, p0, LX/Dmn;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2039660
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 2039661
    if-eqz v0, :cond_1

    .line 2039662
    invoke-direct {p0, p1, p2, p3}, LX/Dmn;->b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;

    move-result-object v0

    goto :goto_0

    .line 2039663
    :cond_1
    invoke-direct {p0, p2, p3}, LX/Dmn;->j(Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)LX/Dmp;

    move-result-object v0

    goto :goto_0
.end method
