.class public final enum LX/E1W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E1W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E1W;

.field public static final enum ERROR:LX/E1W;

.field public static final enum PROGRESS:LX/E1W;

.field public static final enum SETTINGS:LX/E1W;


# instance fields
.field public final errorVisibility:I

.field public final progressVisibility:I

.field public final settingsVisibility:I


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2069811
    new-instance v0, LX/E1W;

    const-string v1, "SETTINGS"

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/E1W;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v0, LX/E1W;->SETTINGS:LX/E1W;

    .line 2069812
    new-instance v4, LX/E1W;

    const-string v5, "PROGRESS"

    move v6, v3

    move v7, v2

    move v8, v2

    move v9, v3

    invoke-direct/range {v4 .. v9}, LX/E1W;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v4, LX/E1W;->PROGRESS:LX/E1W;

    .line 2069813
    new-instance v4, LX/E1W;

    const-string v5, "ERROR"

    move v6, v10

    move v7, v2

    move v8, v3

    move v9, v2

    invoke-direct/range {v4 .. v9}, LX/E1W;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v4, LX/E1W;->ERROR:LX/E1W;

    .line 2069814
    const/4 v0, 0x3

    new-array v0, v0, [LX/E1W;

    sget-object v1, LX/E1W;->SETTINGS:LX/E1W;

    aput-object v1, v0, v2

    sget-object v1, LX/E1W;->PROGRESS:LX/E1W;

    aput-object v1, v0, v3

    sget-object v1, LX/E1W;->ERROR:LX/E1W;

    aput-object v1, v0, v10

    sput-object v0, LX/E1W;->$VALUES:[LX/E1W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2069815
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2069816
    if-eqz p3, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, LX/E1W;->settingsVisibility:I

    .line 2069817
    if-eqz p4, :cond_1

    move v0, v1

    :goto_1
    iput v0, p0, LX/E1W;->errorVisibility:I

    .line 2069818
    if-eqz p5, :cond_2

    :goto_2
    iput v1, p0, LX/E1W;->progressVisibility:I

    .line 2069819
    return-void

    :cond_0
    move v0, v2

    .line 2069820
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2069821
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2069822
    goto :goto_2
.end method

.method public static valueOf(Ljava/lang/String;)LX/E1W;
    .locals 1

    .prologue
    .line 2069823
    const-class v0, LX/E1W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E1W;

    return-object v0
.end method

.method public static values()[LX/E1W;
    .locals 1

    .prologue
    .line 2069824
    sget-object v0, LX/E1W;->$VALUES:[LX/E1W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E1W;

    return-object v0
.end method
