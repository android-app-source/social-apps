.class public final enum LX/EEt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EEt;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EEt;

.field public static final enum MULTIWAY:LX/EEt;

.field public static final enum P2P:LX/EEt;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2094376
    new-instance v0, LX/EEt;

    const-string v1, "P2P"

    invoke-direct {v0, v1, v2}, LX/EEt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEt;->P2P:LX/EEt;

    .line 2094377
    new-instance v0, LX/EEt;

    const-string v1, "MULTIWAY"

    invoke-direct {v0, v1, v3}, LX/EEt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEt;->MULTIWAY:LX/EEt;

    .line 2094378
    const/4 v0, 0x2

    new-array v0, v0, [LX/EEt;

    sget-object v1, LX/EEt;->P2P:LX/EEt;

    aput-object v1, v0, v2

    sget-object v1, LX/EEt;->MULTIWAY:LX/EEt;

    aput-object v1, v0, v3

    sput-object v0, LX/EEt;->$VALUES:[LX/EEt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2094379
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EEt;
    .locals 1

    .prologue
    .line 2094380
    const-class v0, LX/EEt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EEt;

    return-object v0
.end method

.method public static values()[LX/EEt;
    .locals 1

    .prologue
    .line 2094381
    sget-object v0, LX/EEt;->$VALUES:[LX/EEt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EEt;

    return-object v0
.end method
