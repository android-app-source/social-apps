.class public final LX/DVf;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:I


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/String;Ljava/lang/Integer;LX/0tX;LX/B1b;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2005644
    invoke-direct {p0, p1, p4, p5}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;)V

    .line 2005645
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005646
    iput-object v0, p0, LX/DVf;->e:LX/0Px;

    .line 2005647
    iput-object p2, p0, LX/DVf;->f:Ljava/lang/String;

    .line 2005648
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/DVf;->g:I

    .line 2005649
    return-void
.end method

.method public static a(LX/DWH;Ljava/lang/String;)Lcom/facebook/user/model/User;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "createUserFromNodeInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 2005628
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    .line 2005629
    sget-object v0, LX/0XG;->FACEBOOK_CONTACT:LX/0XG;

    invoke-interface {p0}, LX/DWG;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 2005630
    invoke-interface {p0}, LX/DWH;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x3

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2005631
    iput-object v0, v2, LX/0XI;->n:Ljava/lang/String;

    .line 2005632
    invoke-interface {p0}, LX/DWG;->l()LX/0Px;

    move-result-object v3

    .line 2005633
    const-string v0, ""

    .line 2005634
    const-string v1, ""

    .line 2005635
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2005636
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2005637
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    if-le v4, v5, :cond_0

    .line 2005638
    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2005639
    :cond_0
    new-instance v3, Lcom/facebook/user/model/Name;

    invoke-interface {p0}, LX/DWG;->k()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v1, v4}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2005640
    iput-object v3, v2, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2005641
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2005642
    iput-object p1, v2, LX/0XI;->x:Ljava/lang/String;

    .line 2005643
    :cond_1
    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005621
    invoke-static {}, LX/DWY;->a()LX/DWX;

    move-result-object v0

    .line 2005622
    const-string v1, "group_id"

    iget-object v2, p0, LX/DVf;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005623
    const-string v1, "member_count_to_fetch"

    iget v2, p0, LX/DVf;->g:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005624
    const-string v1, "afterCursor"

    iget-object v2, p0, LX/B1d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005625
    const-string v1, "suggested_member_order_param"

    const-string v2, "importance"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005626
    const-string v1, "suggested_member_friend_sort_param"

    const-string v2, "is_viewer_friend"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005627
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2005603
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2005604
    iget-object v0, p0, LX/DVf;->e:LX/0Px;

    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2005605
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005606
    if-eqz v0, :cond_3

    .line 2005607
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005608
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2005609
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005610
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel;

    move-result-object v6

    .line 2005611
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_0

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;

    .line 2005612
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->a()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0, v3}, LX/DVf;->a(LX/DWH;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005613
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2005614
    :cond_0
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2005615
    :goto_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iput-object v5, p0, LX/DVf;->e:LX/0Px;

    .line 2005616
    if-eqz v0, :cond_1

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iput-object v3, p0, LX/DVf;->a:Ljava/lang/String;

    .line 2005617
    if-eqz v0, :cond_2

    invoke-virtual {v2, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, LX/DVf;->b:Z

    .line 2005618
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 2005619
    return-void

    :cond_2
    move v1, v4

    .line 2005620
    goto :goto_2

    :cond_3
    move v0, v1

    move-object v2, v3

    goto :goto_1
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2005598
    const-string v0, "Group suggested member fetch failed"

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005602
    iget-object v0, p0, LX/DVf;->e:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 2005599
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005600
    iput-object v0, p0, LX/DVf;->e:LX/0Px;

    .line 2005601
    return-void
.end method
