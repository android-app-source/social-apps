.class public final LX/EKP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EKQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

.field public b:Ljava/lang/String;

.field public c:I

.field public final synthetic d:LX/EKQ;


# direct methods
.method public constructor <init>(LX/EKQ;)V
    .locals 1

    .prologue
    .line 2106593
    iput-object p1, p0, LX/EKP;->d:LX/EKQ;

    .line 2106594
    move-object v0, p1

    .line 2106595
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2106596
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2106576
    const-string v0, "SearchResultsCandidateInfoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2106577
    if-ne p0, p1, :cond_1

    .line 2106578
    :cond_0
    :goto_0
    return v0

    .line 2106579
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2106580
    goto :goto_0

    .line 2106581
    :cond_3
    check-cast p1, LX/EKP;

    .line 2106582
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2106583
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2106584
    if-eq v2, v3, :cond_0

    .line 2106585
    iget-object v2, p0, LX/EKP;->a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EKP;->a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    iget-object v3, p1, LX/EKP;->a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2106586
    goto :goto_0

    .line 2106587
    :cond_5
    iget-object v2, p1, LX/EKP;->a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    if-nez v2, :cond_4

    .line 2106588
    :cond_6
    iget-object v2, p0, LX/EKP;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EKP;->b:Ljava/lang/String;

    iget-object v3, p1, LX/EKP;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2106589
    goto :goto_0

    .line 2106590
    :cond_8
    iget-object v2, p1, LX/EKP;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2106591
    :cond_9
    iget v2, p0, LX/EKP;->c:I

    iget v3, p1, LX/EKP;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2106592
    goto :goto_0
.end method
