.class public LX/CzE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;
.implements LX/Cz2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "LX/Cz2;"
    }
.end annotation


# static fields
.field public static final a:LX/0us;

.field public static final b:LX/0us;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cyz;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2Sc;

.field public final e:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;>;"
        }
    .end annotation
.end field

.field public l:I

.field public m:Z

.field public n:LX/0us;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1954338
    new-instance v0, LX/4a7;

    invoke-direct {v0}, LX/4a7;-><init>()V

    const/4 v1, 0x1

    .line 1954339
    iput-boolean v1, v0, LX/4a7;->b:Z

    .line 1954340
    move-object v0, v0

    .line 1954341
    iput-object v2, v0, LX/4a7;->a:Ljava/lang/String;

    .line 1954342
    move-object v0, v0

    .line 1954343
    invoke-virtual {v0}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    sput-object v0, LX/CzE;->a:LX/0us;

    .line 1954344
    new-instance v0, LX/4a7;

    invoke-direct {v0}, LX/4a7;-><init>()V

    const/4 v1, 0x0

    .line 1954345
    iput-boolean v1, v0, LX/4a7;->b:Z

    .line 1954346
    move-object v0, v0

    .line 1954347
    iput-object v2, v0, LX/4a7;->a:Ljava/lang/String;

    .line 1954348
    move-object v0, v0

    .line 1954349
    invoke-virtual {v0}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    sput-object v0, LX/CzE;->b:LX/0us;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Ot;LX/2Sc;)V
    .locals 2
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "LX/0Ot",
            "<",
            "LX/Cyz;",
            ">;",
            "LX/2Sc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1954350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954351
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/CzE;->f:Ljava/util/List;

    .line 1954352
    iput v1, p0, LX/CzE;->l:I

    .line 1954353
    iput-boolean v1, p0, LX/CzE;->m:Z

    .line 1954354
    sget-object v0, LX/CzE;->a:LX/0us;

    iput-object v0, p0, LX/CzE;->n:LX/0us;

    .line 1954355
    iput-object p1, p0, LX/CzE;->e:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1954356
    iput-object p2, p0, LX/CzE;->c:LX/0Ot;

    .line 1954357
    iput-object p3, p0, LX/CzE;->d:LX/2Sc;

    .line 1954358
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/CzE;->g:Ljava/util/WeakHashMap;

    .line 1954359
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CzE;->h:Ljava/util/Map;

    .line 1954360
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CzE;->i:Ljava/util/Map;

    .line 1954361
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CzE;->j:Ljava/util/Map;

    .line 1954362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CzE;->k:Ljava/util/ArrayList;

    .line 1954363
    return-void
.end method

.method public static c(LX/CzE;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 1

    .prologue
    .line 1954364
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 1954365
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0, p1}, LX/CzE;->h(LX/CzE;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1954366
    :cond_0
    :goto_0
    return-void

    .line 1954367
    :cond_1
    instance-of v0, p1, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    if-eqz v0, :cond_0

    .line 1954368
    check-cast p1, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    invoke-interface {p1}, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p0, v0}, LX/CzE;->h(LX/CzE;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method

.method private static h(LX/CzE;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 7

    .prologue
    .line 1954369
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1954370
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1954371
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1954372
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1954373
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1954374
    :cond_0
    iget-object v1, p0, LX/CzE;->g:Ljava/util/WeakHashMap;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1954375
    :goto_0
    return-void

    .line 1954376
    :cond_1
    iget-object v0, p0, LX/CzE;->d:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_NO_CACHEID_FOR_STORY:LX/3Ql;

    const-string v2, "Story cacheId was null"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    goto :goto_0

    .line 1954377
    :cond_2
    invoke-static {v1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v3

    .line 1954378
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1954379
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1954380
    iget-object v5, p0, LX/CzE;->g:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v5, v6, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1954381
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1954382
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/CzE;->l:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(LX/0Px;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1954383
    invoke-virtual {p1}, LX/0Px;->reverse()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1954384
    const/4 v5, 0x0

    .line 1954385
    iget-object v6, p0, LX/CzE;->g:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    move v6, v6

    .line 1954386
    if-eqz v6, :cond_2

    .line 1954387
    :goto_1
    move v0, v5

    .line 1954388
    if-eqz v0, :cond_1

    .line 1954389
    add-int/lit8 v0, v1, 0x1

    .line 1954390
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1954391
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_2

    .line 1954392
    :cond_2
    iget-object v6, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v6, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1954393
    invoke-static {p0, v0}, LX/CzE;->c(LX/CzE;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1954394
    const/4 v5, 0x1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I
    .locals 3

    .prologue
    .line 1954395
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1954396
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v0, :cond_0

    .line 1954397
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 1954398
    iget-object v2, v0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    move-object v0, v2

    .line 1954399
    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1954400
    :goto_1
    return v1

    .line 1954401
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1954402
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final a(LX/Cz4;Z)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cz4;",
            "Z)",
            "LX/0Px",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1954403
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1954404
    if-eqz p2, :cond_3

    .line 1954405
    invoke-virtual {p0}, LX/CzE;->size()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v6

    :goto_0
    const-string v1, "BEM results should always be first module"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1954406
    iget-object v0, p1, LX/Cz4;->a:LX/0Px;

    move-object v1, v0

    .line 1954407
    iget-object v0, p0, LX/CzE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyz;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1954408
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1954409
    :goto_1
    move-object v2, v1

    .line 1954410
    :goto_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    move v4, v7

    :goto_3
    if-ge v4, v8, :cond_8

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    .line 1954411
    iget-object v1, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/CzE;->f:Ljava/util/List;

    iget-object v3, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_FEATURED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v1, v3, :cond_0

    .line 1954412
    iget-object v1, p0, LX/CzE;->f:Ljava/util/List;

    iget-object v3, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 1954413
    iput-boolean v6, v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->p:Z

    .line 1954414
    :cond_0
    iget-object v1, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1954415
    invoke-static {v0, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1954416
    invoke-static {p0, v0}, LX/CzE;->c(LX/CzE;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1954417
    instance-of v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;

    if-eqz v1, :cond_6

    .line 1954418
    iget v0, p0, LX/CzE;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/CzE;->l:I

    .line 1954419
    :cond_1
    :goto_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_2
    move v0, v7

    .line 1954420
    goto :goto_0

    .line 1954421
    :cond_3
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1954422
    iget-object v0, p1, LX/Cz4;->a:LX/0Px;

    move-object v9, v0

    .line 1954423
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v8, v7

    :goto_5
    if-ge v8, v10, :cond_5

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    .line 1954424
    iget-object v0, p0, LX/CzE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyz;

    .line 1954425
    invoke-static {v1}, LX/Cyz;->b(Lcom/facebook/search/model/SearchResultsBaseFeedUnit;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    .line 1954426
    invoke-static {v0, v2}, LX/Cyz;->a(LX/Cyz;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v2

    move v0, v2

    .line 1954427
    if-eqz v0, :cond_4

    .line 1954428
    iget-object v0, p0, LX/CzE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyz;

    iget-object v2, p0, LX/CzE;->f:Ljava/util/List;

    iget-object v3, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual/range {v0 .. v5}, LX/Cyz;->a(Lcom/facebook/search/model/SearchResultsBaseFeedUnit;Ljava/util/List;Ljava/util/List;LX/0Pz;LX/0Pz;)V

    .line 1954429
    :goto_6
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_5

    .line 1954430
    :cond_4
    new-array v0, v6, [Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    aput-object v1, v0, v7

    invoke-virtual {v4, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    goto :goto_6

    .line 1954431
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1954432
    iget-object v0, p0, LX/CzE;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyz;

    invoke-virtual {v0, v1}, LX/Cyz;->c(LX/0Px;)V

    move-object v2, v1

    goto/16 :goto_2

    .line 1954433
    :cond_6
    instance-of v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    if-eqz v1, :cond_1

    .line 1954434
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 1954435
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v1

    const-class v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1954436
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v9

    .line 1954437
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v3, v7

    :goto_7
    if-ge v3, v10, :cond_7

    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1954438
    invoke-static {p0, v1}, LX/CzE;->h(LX/CzE;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1954439
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_7

    .line 1954440
    :cond_7
    iget-object v1, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 1954441
    :cond_8
    iget-object v0, p1, LX/Cz4;->b:LX/0us;

    move-object v0, v0

    .line 1954442
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    sget-object v1, LX/CzE;->a:LX/0us;

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0us;

    iput-object v0, p0, LX/CzE;->n:LX/0us;

    .line 1954443
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1954444
    :cond_9
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    if-ne v4, v2, :cond_a

    :goto_8
    const-string v4, "BEM results should only have one module"

    invoke-static {v2, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1954445
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    .line 1954446
    instance-of v4, v2, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v4, :cond_b

    .line 1954447
    check-cast v2, Lcom/facebook/search/results/model/SearchResultsBridge;

    invoke-static {v2}, LX/Cyz;->a(Lcom/facebook/search/results/model/SearchResultsBridge;)Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-result-object v2

    move-object v4, v2

    .line 1954448
    :goto_9
    new-instance v8, LX/0cA;

    invoke-direct {v8}, LX/0cA;-><init>()V

    .line 1954449
    invoke-virtual {v4}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    :goto_a
    if-ge v3, v10, :cond_c

    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLNode;

    .line 1954450
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1954451
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_a
    move v2, v3

    .line 1954452
    goto :goto_8

    .line 1954453
    :cond_b
    check-cast v2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-object v4, v2

    goto :goto_9

    .line 1954454
    :cond_c
    invoke-virtual {v4}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-static {v0, v8, v2}, LX/Cyz;->a(LX/Cyz;LX/0cA;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)V

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "LX/0am",
            "<",
            "Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954455
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1954456
    instance-of v2, v0, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    if-eqz v2, :cond_0

    .line 1954457
    check-cast v0, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    .line 1954458
    invoke-interface {v0}, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1954459
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1954460
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954461
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/CzE;->g:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954462
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CzE;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1954463
    invoke-virtual {p0, p1}, LX/CzE;->b(I)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954464
    invoke-virtual {p0, p1}, LX/CzE;->b(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1954465
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1954466
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0, p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1954467
    :goto_0
    return-object v0

    .line 1954468
    :cond_0
    invoke-virtual {p0, p1}, LX/CzE;->c(Lcom/facebook/graphql/model/GraphQLNode;)LX/0am;

    move-result-object v0

    .line 1954469
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1954470
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    invoke-virtual {v0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1954471
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 1954317
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1954318
    if-eq v0, v4, :cond_0

    .line 1954319
    iget-object v1, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1954320
    :cond_0
    instance-of v1, p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    if-eqz v1, :cond_1

    instance-of v1, p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    if-eqz v1, :cond_1

    .line 1954321
    iget-object v0, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1954322
    iget-object v2, p0, LX/CzE;->k:Ljava/util/ArrayList;

    move-object v0, p2

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 1954323
    :cond_1
    if-ne v0, v4, :cond_2

    .line 1954324
    iget-object v1, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1954325
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 1954326
    iget-object v0, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 1954327
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1954328
    if-eq v2, v4, :cond_3

    .line 1954329
    invoke-static {v0, p1, p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-result-object v2

    .line 1954330
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    iget-object v3, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1954331
    iget-object v3, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v3, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1954332
    iget-object v3, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v3, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1954333
    :cond_2
    if-ne v0, v4, :cond_4

    .line 1954334
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Item not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1954335
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 1954336
    :cond_4
    invoke-static {p0, p2}, LX/CzE;->c(LX/CzE;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1954337
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1954259
    iget-object v0, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)I
    .locals 1

    .prologue
    .line 1954472
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLNode;)I
    .locals 2

    .prologue
    .line 1954260
    invoke-virtual {p0, p1}, LX/CzE;->b(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1954261
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1954262
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p0, v0}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v0

    .line 1954263
    :goto_0
    return v0

    .line 1954264
    :cond_0
    invoke-virtual {p0, p1}, LX/CzE;->c(Lcom/facebook/graphql/model/GraphQLNode;)LX/0am;

    move-result-object v0

    .line 1954265
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1954266
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p0, v0}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v0

    goto :goto_0

    .line 1954267
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "LX/0am",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 1954268
    iget-object v0, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 1954269
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1954270
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1954271
    :goto_1
    return-object v0

    .line 1954272
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1954273
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(I)Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 1954274
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954275
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CzE;->i:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(ILcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1954276
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1954277
    :cond_0
    const/4 v0, 0x0

    .line 1954278
    :goto_0
    return v0

    .line 1954279
    :cond_1
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1954280
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1954281
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLNode;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ")",
            "LX/0am",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954282
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1954283
    instance-of v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    .line 1954284
    iget-object p0, v1, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v1, p0

    .line 1954285
    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1954286
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1954287
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954288
    new-instance v0, LX/CzC;

    invoke-direct {v0, p0}, LX/CzC;-><init>(LX/CzE;)V

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954289
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CzE;->j:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1954290
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1954291
    iget-object v0, p0, LX/CzE;->g:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->clear()V

    .line 1954292
    iget-object v0, p0, LX/CzE;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1954293
    iget-object v0, p0, LX/CzE;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1954294
    iget-object v0, p0, LX/CzE;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1954295
    iget-object v0, p0, LX/CzE;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1954296
    sget-object v0, LX/CzE;->a:LX/0us;

    iput-object v0, p0, LX/CzE;->n:LX/0us;

    .line 1954297
    iput v1, p0, LX/CzE;->l:I

    .line 1954298
    iput-boolean v1, p0, LX/CzE;->m:Z

    .line 1954299
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1954300
    iget-object v0, p0, LX/CzE;->n:LX/0us;

    invoke-interface {v0}, LX/0us;->b()Z

    move-result v0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1954301
    iget-object v0, p0, LX/CzE;->n:LX/0us;

    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1954302
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1954303
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1954304
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1954305
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1954306
    const-string v0, " FeedUnits:\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1954307
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1954308
    iget-object v0, p0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1954309
    const-string v3, "FeedUnit "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1954310
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1954311
    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1954312
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1954313
    invoke-interface {v0}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1954314
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1954315
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1954316
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchResultsFeedCollection{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
