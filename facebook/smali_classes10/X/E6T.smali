.class public LX/E6T;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0qn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/widget/ImageView;

.field public c:Landroid/view/View$OnClickListener;

.field private d:Landroid/widget/TextView;

.field public e:Lcom/facebook/graphql/model/GraphQLStory;

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9A1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2080241
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/E6T;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2080242
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2080243
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2080244
    const-class v0, LX/E6T;

    invoke-static {v0, p0}, LX/E6T;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2080245
    const v0, 0x7f03116a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2080246
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/E6T;->setOrientation(I)V

    .line 2080247
    const v0, 0x7f0d2910

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/E6T;->b:Landroid/widget/ImageView;

    .line 2080248
    const v0, 0x7f0d290d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/E6T;->d:Landroid/widget/TextView;

    .line 2080249
    return-void
.end method

.method public static a(LX/E6T;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 2080250
    const v0, 0x7f0215de

    const v1, 0x7f082225

    invoke-virtual {p0}, LX/E6T;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f040023

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, LX/E6T;->a$redex0(LX/E6T;IILandroid/view/animation/Animation;)V

    .line 2080251
    iput-object p1, p0, LX/E6T;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2080252
    invoke-static {p0}, LX/E6T;->c$redex0(LX/E6T;)V

    .line 2080253
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/E6T;

    const/16 p0, 0x1d69

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    iput-object p0, p1, LX/E6T;->f:LX/0Ot;

    invoke-static {v1}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v1

    check-cast v1, LX/0qn;

    iput-object v1, p1, LX/E6T;->a:LX/0qn;

    return-void
.end method

.method public static a$redex0(LX/E6T;IILandroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2080254
    iget-object v0, p0, LX/E6T;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/E6T;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2080255
    if-nez p3, :cond_0

    .line 2080256
    iget-object v0, p0, LX/E6T;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2080257
    :goto_0
    iget-object v0, p0, LX/E6T;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/E6T;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2080258
    return-void

    .line 2080259
    :cond_0
    iget-object v0, p0, LX/E6T;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static b(LX/E6T;)V
    .locals 3

    .prologue
    .line 2080260
    const v0, 0x7f0215c8

    const v1, 0x7f082226

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/E6T;->a$redex0(LX/E6T;IILandroid/view/animation/Animation;)V

    .line 2080261
    invoke-static {p0}, LX/E6T;->c$redex0(LX/E6T;)V

    .line 2080262
    return-void
.end method

.method public static c$redex0(LX/E6T;)V
    .locals 1

    .prologue
    .line 2080263
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/E6T;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080264
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/E6T;->setClickable(Z)V

    .line 2080265
    return-void
.end method
