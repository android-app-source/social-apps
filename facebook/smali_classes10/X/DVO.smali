.class public LX/DVO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Or;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2004994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2004995
    iput-object p1, p0, LX/DVO;->a:LX/0ad;

    .line 2004996
    iput-object p2, p0, LX/DVO;->b:LX/0Or;

    .line 2004997
    iput-object p3, p0, LX/DVO;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2004998
    iput-object p4, p0, LX/DVO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2004999
    iput-object p5, p0, LX/DVO;->e:Landroid/content/Context;

    .line 2005000
    return-void
.end method

.method public static a(LX/0QB;)LX/DVO;
    .locals 7

    .prologue
    .line 2005001
    new-instance v1, LX/DVO;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    const/16 v3, 0xc

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct/range {v1 .. v6}, LX/DVO;-><init>(LX/0ad;LX/0Or;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/Context;)V

    .line 2005002
    move-object v0, v1

    .line 2005003
    return-object v0
.end method
