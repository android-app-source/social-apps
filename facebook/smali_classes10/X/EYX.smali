.class public final LX/EYX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/EYF;

.field private final b:I


# direct methods
.method public constructor <init>(LX/EYF;I)V
    .locals 0

    .prologue
    .line 2137571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137572
    iput-object p1, p0, LX/EYX;->a:LX/EYF;

    .line 2137573
    iput p2, p0, LX/EYX;->b:I

    .line 2137574
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2137575
    instance-of v1, p1, LX/EYX;

    if-nez v1, :cond_1

    .line 2137576
    :cond_0
    :goto_0
    return v0

    .line 2137577
    :cond_1
    check-cast p1, LX/EYX;

    .line 2137578
    iget-object v1, p0, LX/EYX;->a:LX/EYF;

    iget-object v2, p1, LX/EYX;->a:LX/EYF;

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/EYX;->b:I

    iget v2, p1, LX/EYX;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2137579
    iget-object v0, p0, LX/EYX;->a:LX/EYF;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0xffff

    mul-int/2addr v0, v1

    iget v1, p0, LX/EYX;->b:I

    add-int/2addr v0, v1

    return v0
.end method
