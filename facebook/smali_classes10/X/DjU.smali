.class public final LX/DjU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DjW;


# direct methods
.method public constructor <init>(LX/DjW;)V
    .locals 0

    .prologue
    .line 2033301
    iput-object p1, p0, LX/DjU;->a:LX/DjW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2033302
    iget-object v0, p0, LX/DjU;->a:LX/DjW;

    iget-object v0, v0, LX/DjW;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2033303
    iget-object v0, p0, LX/DjU;->a:LX/DjW;

    iget-object v0, v0, LX/DjW;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2033304
    iget-boolean v0, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v0

    .line 2033305
    if-eqz v0, :cond_0

    .line 2033306
    iget-object v0, p0, LX/DjU;->a:LX/DjW;

    iget-object v0, v0, LX/DjW;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->b:LX/Dka;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2033307
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2033308
    iget-object v3, p0, LX/DjU;->a:LX/DjW;

    iget-object v3, v3, LX/DjW;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->h:Ljava/lang/String;

    iget-object v4, p0, LX/DjU;->a:LX/DjW;

    iget-object v4, v4, LX/DjW;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->g:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/Dka;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2033309
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DjU;->a:LX/DjW;

    iget-object v0, v0, LX/DjW;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->b:LX/Dka;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2033310
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2033311
    iget-object v3, p0, LX/DjU;->a:LX/DjW;

    iget-object v3, v3, LX/DjW;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->h:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/Dka;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
