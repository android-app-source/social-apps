.class public LX/Dz4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:J


# instance fields
.field public final b:LX/9jT;

.field public final c:LX/0y2;

.field private final d:Lcom/facebook/common/perftest/PerfTestConfig;

.field public e:Landroid/location/Location;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Landroid/location/Location;

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2066392
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/Dz4;->a:J

    return-void
.end method

.method public constructor <init>(LX/9jT;LX/0y2;Lcom/facebook/common/perftest/PerfTestConfig;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2066394
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dz4;->g:Z

    .line 2066395
    iput-object p1, p0, LX/Dz4;->b:LX/9jT;

    .line 2066396
    iput-object p2, p0, LX/Dz4;->c:LX/0y2;

    .line 2066397
    iput-object p3, p0, LX/Dz4;->d:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 2066398
    return-void
.end method
