.class public final LX/DGX;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/DGZ;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:I

.field public final synthetic e:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;LX/0Px;LX/DGZ;Lcom/facebook/feed/rows/core/props/FeedProps;I)V
    .locals 0

    .prologue
    .line 1980010
    iput-object p1, p0, LX/DGX;->e:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    iput-object p2, p0, LX/DGX;->a:LX/0Px;

    iput-object p3, p0, LX/DGX;->b:LX/DGZ;

    iput-object p4, p0, LX/DGX;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput p5, p0, LX/DGX;->d:I

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<",
            "LX/1Pf;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1980011
    move v0, v1

    :goto_0
    iget-object v2, p0, LX/DGX;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1980012
    iget-object v2, p0, LX/DGX;->e:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    iget-object v3, p0, LX/DGX;->b:LX/DGZ;

    .line 1980013
    sget-object v4, LX/DGY;->a:[I

    iget-object v5, v3, LX/DGZ;->b:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1980014
    iget-object v4, v2, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->i:Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;

    :goto_1
    move-object v2, v4

    .line 1980015
    new-instance v3, LX/DGm;

    iget-object v4, p0, LX/DGX;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v5, p0, LX/DGX;->d:I

    invoke-direct {v3, v4, v0, v5, v1}, LX/DGm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V

    invoke-virtual {p1, v2, v3}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1980016
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1980017
    :cond_0
    return-void

    .line 1980018
    :pswitch_0
    iget-object v4, v2, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->i:Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;

    move-object v4, v4

    .line 1980019
    goto :goto_1

    .line 1980020
    :pswitch_1
    iget-object v4, v2, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->j:Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;

    move-object v4, v4

    .line 1980021
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c(I)V
    .locals 6

    .prologue
    .line 1980022
    iget-object v0, p0, LX/DGX;->b:LX/DGZ;

    iget-object v0, v0, LX/DGZ;->a:LX/99i;

    iget-object v0, v0, LX/99i;->b:LX/8yy;

    iget-object v1, p0, LX/DGX;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-interface {v0, p1, v1}, LX/8yy;->a(II)V

    .line 1980023
    iget-object v0, p0, LX/DGX;->e:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->f:LX/2xr;

    iget-object v0, p0, LX/DGX;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980024
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1980025
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1, v0, p1}, LX/2xr;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1980026
    iget-object v0, p0, LX/DGX;->e:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->d:LX/1LV;

    iget-object v0, p0, LX/DGX;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980027
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1980028
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1, v0, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 1980029
    iget-object v0, p0, LX/DGX;->e:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    iget-object v2, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->c:LX/2yK;

    iget-object v1, p0, LX/DGX;->b:LX/DGZ;

    iget-object v1, v1, LX/DGZ;->b:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 1980030
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    if-ne v1, v3, :cond_2

    .line 1980031
    :cond_0
    sget-object v3, LX/0ig;->y:LX/0ih;

    .line 1980032
    :goto_0
    move-object v3, v3

    .line 1980033
    move-object v3, v3

    .line 1980034
    iget-object v0, p0, LX/DGX;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980035
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1980036
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    iget-object v1, p0, LX/DGX;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v1, p1, :cond_1

    const/4 v1, 0x1

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "position:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v1, v4}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;ILjava/lang/String;)V

    .line 1980037
    return-void

    .line 1980038
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    sget-object v3, LX/0ig;->z:LX/0ih;

    goto :goto_0
.end method
