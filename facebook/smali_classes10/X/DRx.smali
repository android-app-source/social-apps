.class public final LX/DRx;
.super LX/DRr;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/GroupBlockedListFragment;)V
    .locals 0

    .prologue
    .line 1998953
    iput-object p1, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    invoke-direct {p0}, LX/DRr;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1998954
    check-cast p1, LX/DTi;

    .line 1998955
    iget-object v0, p1, LX/DTi;->a:Ljava/lang/String;

    iget-object v1, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    .line 1998956
    iget-object v2, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1998957
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1998958
    :cond_0
    :goto_0
    return-void

    .line 1998959
    :cond_1
    iget-object v0, p1, LX/DTi;->b:Ljava/lang/String;

    iget-object v1, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1998960
    iget-object v0, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    iget-object v1, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1998961
    iput-object v1, v0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1998962
    iget-object v0, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e:LX/DTZ;

    iget-object v1, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    .line 1998963
    iget-object v2, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object v1, v2

    .line 1998964
    iput-object v1, v0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1998965
    iget-object v0, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    .line 1998966
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 1998967
    if-eqz v0, :cond_2

    .line 1998968
    const-string v1, "group_admin_type"

    iget-object v2, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1998969
    :cond_2
    iget-object v0, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->E(Lcom/facebook/groups/memberlist/GroupBlockedListFragment;)V

    .line 1998970
    iget-object v0, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    iget-boolean v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->p:Z

    if-eqz v0, :cond_0

    .line 1998971
    iget-object v0, p0, LX/DRx;->a:Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o()V

    goto :goto_0
.end method
