.class public LX/CyS;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cve;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8cT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CyY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cyx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/SearchRequestExecutor;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1952679
    const-string v0, "celebrity_suggestion"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/CyS;->a:Ljava/util/Set;

    .line 1952680
    const-class v0, LX/CyS;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CyS;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952682
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952683
    iput-object v0, p0, LX/CyS;->c:LX/0Ot;

    .line 1952684
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952685
    iput-object v0, p0, LX/CyS;->d:LX/0Ot;

    .line 1952686
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952687
    iput-object v0, p0, LX/CyS;->f:LX/0Ot;

    .line 1952688
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952689
    iput-object v0, p0, LX/CyS;->g:LX/0Ot;

    .line 1952690
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952691
    iput-object v0, p0, LX/CyS;->h:LX/0Ot;

    .line 1952692
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952693
    iput-object v0, p0, LX/CyS;->i:LX/0Ot;

    .line 1952694
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952695
    iput-object v0, p0, LX/CyS;->j:LX/0Ot;

    .line 1952696
    return-void
.end method

.method public static a$redex0(LX/CyS;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CyQ;Ljava/util/List;)Ljava/lang/Object;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "LX/CyQ",
            "<TR;>;",
            "Ljava/util/List",
            "<",
            "LX/8cK;",
            ">;)TR;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1952697
    :try_start_0
    const-string v0, "%s:process-bem-results"

    sget-object v2, LX/CyS;->b:Ljava/lang/String;

    const v3, 0x1432d85c

    invoke-static {v0, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1952698
    iget-object v0, p0, LX/CyS;->e:LX/0ad;

    sget-short v2, LX/100;->bH:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1952699
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->s:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    move-object v0, v0

    .line 1952700
    if-nez v0, :cond_5

    .line 1952701
    const/4 v0, 0x0

    .line 1952702
    :goto_0
    move-object v0, v0

    .line 1952703
    move-object v2, v0

    .line 1952704
    :goto_1
    if-eqz v2, :cond_1

    .line 1952705
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cK;

    .line 1952706
    iget-object v4, v2, LX/8cI;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1952707
    iget-object p1, v0, LX/8cI;->a:Ljava/lang/String;

    move-object v0, p1

    .line 1952708
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1952709
    const/4 v0, 0x1

    .line 1952710
    :goto_2
    if-nez v0, :cond_1

    .line 1952711
    const/4 v0, 0x0

    invoke-interface {p3, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1952712
    :cond_1
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 1952713
    :try_start_1
    iget-object v0, p0, LX/CyS;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyx;

    .line 1952714
    iget-object v5, v0, LX/Cyx;->c:Ljava/util/EnumMap;

    if-eqz v5, :cond_6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1952715
    :cond_2
    :goto_3
    :try_start_2
    invoke-interface {p2, p3}, LX/CyQ;->a(Ljava/util/List;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 1952716
    const v1, -0x1b1ea9bb

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 1952717
    :cond_3
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_1

    .line 1952718
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1952719
    :try_start_3
    iget-object v0, p0, LX/CyS;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    sget-object v2, LX/3Ql;->BAD_SERVER_CONFIG:LX/3Ql;

    invoke-virtual {v0, v2, v1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 1952720
    :catchall_0
    move-exception v0

    const v1, 0xf1902c2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    :try_start_4
    invoke-static {}, LX/8cK;->q()LX/8cJ;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->uid:Ljava/lang/String;

    .line 1952721
    iput-object v3, v2, LX/8cJ;->c:Ljava/lang/String;

    .line 1952722
    move-object v2, v2

    .line 1952723
    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->name:Ljava/lang/String;

    .line 1952724
    iput-object v3, v2, LX/8cJ;->d:Ljava/lang/String;

    .line 1952725
    move-object v2, v2

    .line 1952726
    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->type:Ljava/lang/String;

    .line 1952727
    iput-object v3, v2, LX/8cJ;->i:Ljava/lang/String;

    .line 1952728
    move-object v2, v2

    .line 1952729
    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->category:Ljava/lang/String;

    .line 1952730
    iput-object v3, v2, LX/8cJ;->f:Ljava/lang/String;

    .line 1952731
    move-object v2, v2

    .line 1952732
    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->subtext:Ljava/lang/String;

    .line 1952733
    iput-object v3, v2, LX/8cJ;->g:Ljava/lang/String;

    .line 1952734
    move-object v2, v2

    .line 1952735
    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->profilePic:Ljava/lang/String;

    .line 1952736
    iput-object v3, v2, LX/8cJ;->h:Ljava/lang/String;

    .line 1952737
    move-object v2, v2

    .line 1952738
    iget-boolean v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->isVerified:Z

    .line 1952739
    iput-boolean v3, v2, LX/8cJ;->a:Z

    .line 1952740
    move-object v2, v2

    .line 1952741
    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->friendshipStatus:Ljava/lang/String;

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    .line 1952742
    iput-object v3, v2, LX/8cJ;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1952743
    move-object v2, v2

    .line 1952744
    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->verificationStatus:Ljava/lang/String;

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v3

    .line 1952745
    iput-object v3, v2, LX/8cJ;->b:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1952746
    move-object v2, v2

    .line 1952747
    iget-object v3, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->groupJoinState:Ljava/lang/String;

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    .line 1952748
    iput-object v3, v2, LX/8cJ;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1952749
    move-object v2, v2

    .line 1952750
    iget-boolean v0, v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->doesViewerLike:Z

    .line 1952751
    iput-boolean v0, v2, LX/8cJ;->l:Z

    .line 1952752
    move-object v0, v2

    .line 1952753
    invoke-virtual {v0}, LX/8cJ;->q()LX/8cK;

    move-result-object v0

    goto/16 :goto_0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1952754
    :cond_6
    iget-object v5, v0, LX/Cyx;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0W3;

    sget-wide v7, LX/0X5;->lL:J

    const-string v6, "{}"

    invoke-interface {v5, v7, v8, v6}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1952755
    iget-object v5, v0, LX/Cyx;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0lB;

    .line 1952756
    iget-object v7, v5, LX/0lC;->_typeFactory:LX/0li;

    move-object v5, v7

    .line 1952757
    const-class v7, Ljava/util/EnumMap;

    const-class v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v5, v8}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v8

    const-class v9, Ljava/util/EnumSet;

    const-class v10, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v5, v9, v10}, LX/0li;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/267;

    move-result-object v5

    invoke-static {v7, v8, v5}, LX/1Xn;->b(Ljava/lang/Class;LX/0lJ;LX/0lJ;)LX/1Xn;

    move-result-object v7

    .line 1952758
    iget-object v5, v0, LX/Cyx;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0lB;

    invoke-virtual {v5, v6, v7}, LX/0lC;->a(Ljava/lang/String;LX/0lJ;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/EnumMap;

    iput-object v5, v0, LX/Cyx;->c:Ljava/util/EnumMap;

    goto/16 :goto_3
.end method

.method public static b(LX/0QB;)LX/CyS;
    .locals 9

    .prologue
    .line 1952759
    new-instance v0, LX/CyS;

    invoke-direct {v0}, LX/CyS;-><init>()V

    .line 1952760
    const/16 v1, 0x12b1

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x32d5

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    const/16 v4, 0x32c0

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x335f

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x336e

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x113f

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1421

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 1952761
    iput-object v1, v0, LX/CyS;->c:LX/0Ot;

    iput-object v2, v0, LX/CyS;->d:LX/0Ot;

    iput-object v3, v0, LX/CyS;->e:LX/0ad;

    iput-object v4, v0, LX/CyS;->f:LX/0Ot;

    iput-object v5, v0, LX/CyS;->g:LX/0Ot;

    iput-object v6, v0, LX/CyS;->h:LX/0Ot;

    iput-object v7, v0, LX/CyS;->i:LX/0Ot;

    iput-object v8, v0, LX/CyS;->j:LX/0Ot;

    .line 1952762
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/CyR;LX/CyQ;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "LX/CyR",
            "<TT;>;",
            "LX/CyQ",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1952763
    const/4 v0, 0x0

    .line 1952764
    iget-object v2, p0, LX/CyS;->e:LX/0ad;

    sget-short v3, LX/100;->bx:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-interface {p1}, LX/CwB;->jC_()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {p1}, LX/CwB;->jC_()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v2, v3, :cond_1

    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/7BG;->f(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1952765
    :cond_1
    :goto_0
    move v0, v0

    .line 1952766
    if-nez v0, :cond_2

    .line 1952767
    const/4 v0, 0x0

    .line 1952768
    :goto_1
    return v0

    .line 1952769
    :cond_2
    iget-object v0, p0, LX/CyS;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cve;

    const-string v2, "bootstrap_entities"

    const-string v3, "1"

    invoke-virtual {v0, p1, v1, v2, v3}, LX/Cve;->a(LX/CwB;ZLjava/lang/String;Ljava/lang/String;)V

    .line 1952770
    iget-object v0, p0, LX/CyS;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cT;

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/CyS;->e:LX/0ad;

    sget v4, LX/100;->by:I

    const/4 v5, 0x3

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    .line 1952771
    iget-object v4, v0, LX/8cT;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0TD;

    new-instance v5, LX/8cO;

    invoke-direct {v5, v0, v2, v3}, LX/8cO;-><init>(LX/8cT;Ljava/lang/String;I)V

    invoke-interface {v4, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 1952772
    new-instance v2, LX/CyO;

    invoke-direct {v2, p0, p1, p4}, LX/CyO;-><init>(LX/CyS;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CyQ;)V

    .line 1952773
    invoke-static {v0, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1952774
    new-instance v3, LX/CyP;

    invoke-direct {v3, p0, p3}, LX/CyP;-><init>(LX/CyS;LX/CyR;)V

    .line 1952775
    iget-object v0, p0, LX/CyS;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v4, "keyword_search_result_bootstrap_loader_key"

    invoke-virtual {v0, v4, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    move v0, v1

    .line 1952776
    goto :goto_1

    .line 1952777
    :cond_3
    invoke-interface {p1}, LX/CwB;->jB_()LX/CwF;

    move-result-object v2

    .line 1952778
    sget-object v3, LX/CwF;->trending:LX/CwF;

    if-eq v2, v3, :cond_1

    sget-object v3, LX/CwF;->celebrity:LX/CwF;

    if-eq v2, v3, :cond_1

    sget-object v3, LX/CwF;->local:LX/CwF;

    if-eq v2, v3, :cond_1

    .line 1952779
    const/4 v0, 0x1

    goto :goto_0
.end method
