.class public LX/Chi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field public final a:LX/0lC;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:LX/0lF;

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

.field public l:I

.field public m:Z

.field public n:LX/Clo;


# direct methods
.method public constructor <init>(LX/0lC;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lC;",
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1928467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1928468
    iput-object p1, p0, LX/Chi;->a:LX/0lC;

    .line 1928469
    iput-object p2, p0, LX/Chi;->b:LX/0Ot;

    .line 1928470
    return-void
.end method

.method public static a(LX/0QB;)LX/Chi;
    .locals 5

    .prologue
    .line 1928471
    const-class v1, LX/Chi;

    monitor-enter v1

    .line 1928472
    :try_start_0
    sget-object v0, LX/Chi;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1928473
    sput-object v2, LX/Chi;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1928474
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1928475
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1928476
    new-instance v4, LX/Chi;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    const/16 p0, 0x31e0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/Chi;-><init>(LX/0lC;LX/0Ot;)V

    .line 1928477
    move-object v0, v4

    .line 1928478
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1928479
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Chi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1928480
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1928481
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CH4;LX/CGs;LX/Chc;LX/Chc;ZLX/11o;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<REQUEST:",
            "Ljava/lang/Object;",
            "RESU",
            "LT:Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/richdocument/fetcher/RichDocumentFetcher",
            "<TREQUEST;TRESU",
            "LT;",
            ">;",
            "LX/CGs",
            "<TREQUEST;>;",
            "Lcom/facebook/richdocument/RichDocumentFetchResultParser",
            "<TRESU",
            "LT;",
            ">;",
            "Lcom/facebook/richdocument/RichDocumentFetchCallback;",
            "Z",
            "LX/11o;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1928482
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 1928483
    :cond_0
    :goto_0
    return-void

    .line 1928484
    :cond_1
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1928485
    invoke-virtual {p0}, LX/Chi;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p5, :cond_2

    .line 1928486
    new-instance v0, Lcom/facebook/richdocument/RichDocumentInfo$1;

    invoke-direct {v0, p0, p4}, Lcom/facebook/richdocument/RichDocumentInfo$1;-><init>(LX/Chi;LX/Chc;)V

    const v1, -0x152da13f

    invoke-static {v3, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 1928487
    :cond_2
    iget-object v0, p0, LX/Chi;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiR;

    invoke-direct {v1}, LX/CiR;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1928488
    if-eqz p6, :cond_3

    .line 1928489
    const-string v0, "rich_document_fetch"

    const v1, 0xd27b7e7

    invoke-static {p6, v0, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1928490
    :cond_3
    new-instance v0, LX/Chh;

    move-object v1, p0

    move-object v2, p6

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move v8, p5

    invoke-direct/range {v0 .. v8}, LX/Chh;-><init>(LX/Chi;LX/11o;Landroid/os/Handler;LX/CH4;LX/CGs;LX/Chc;LX/Chc;Z)V

    invoke-virtual {p1, p2, v0}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1928491
    iget-object v0, p0, LX/Chi;->g:LX/0lF;

    if-eqz v0, :cond_0

    .line 1928492
    iget-object v0, p0, LX/Chi;->g:LX/0lF;

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1928493
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;
    .locals 1

    .prologue
    .line 1928494
    iget-object v0, p0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1928495
    iget-object v0, p0, LX/Chi;->n:LX/Clo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
