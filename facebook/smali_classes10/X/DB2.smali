.class public final LX/DB2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 1971970
    const/16 v24, 0x0

    .line 1971971
    const/16 v23, 0x0

    .line 1971972
    const/16 v22, 0x0

    .line 1971973
    const/16 v21, 0x0

    .line 1971974
    const/16 v20, 0x0

    .line 1971975
    const/16 v19, 0x0

    .line 1971976
    const/16 v18, 0x0

    .line 1971977
    const/16 v17, 0x0

    .line 1971978
    const/16 v16, 0x0

    .line 1971979
    const/4 v15, 0x0

    .line 1971980
    const/4 v14, 0x0

    .line 1971981
    const/4 v13, 0x0

    .line 1971982
    const/4 v12, 0x0

    .line 1971983
    const/4 v11, 0x0

    .line 1971984
    const/4 v10, 0x0

    .line 1971985
    const/4 v9, 0x0

    .line 1971986
    const/4 v8, 0x0

    .line 1971987
    const/4 v7, 0x0

    .line 1971988
    const/4 v6, 0x0

    .line 1971989
    const/4 v5, 0x0

    .line 1971990
    const/4 v4, 0x0

    .line 1971991
    const/4 v3, 0x0

    .line 1971992
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    .line 1971993
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1971994
    const/4 v3, 0x0

    .line 1971995
    :goto_0
    return v3

    .line 1971996
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1971997
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_14

    .line 1971998
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v25

    .line 1971999
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1972000
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    if-eqz v25, :cond_1

    .line 1972001
    const-string v26, "action_properties"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2

    .line 1972002
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto :goto_1

    .line 1972003
    :cond_2
    const-string v26, "action_type"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 1972004
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto :goto_1

    .line 1972005
    :cond_3
    const-string v26, "auth_status"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 1972006
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto :goto_1

    .line 1972007
    :cond_4
    const-string v26, "client_id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 1972008
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto :goto_1

    .line 1972009
    :cond_5
    const-string v26, "code_type"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 1972010
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 1972011
    :cond_6
    const-string v26, "device_info"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 1972012
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 1972013
    :cond_7
    const-string v26, "device_model"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 1972014
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 1972015
    :cond_8
    const-string v26, "machine_id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 1972016
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 1972017
    :cond_9
    const-string v26, "nonce"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 1972018
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 1972019
    :cond_a
    const-string v26, "redirect_uri"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 1972020
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1972021
    :cond_b
    const-string v26, "result_id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_c

    .line 1972022
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 1972023
    :cond_c
    const-string v26, "scope"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_d

    .line 1972024
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 1972025
    :cond_d
    const-string v26, "session_key"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_e

    .line 1972026
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1972027
    :cond_e
    const-string v26, "timestamp_expire"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 1972028
    const/4 v5, 0x1

    .line 1972029
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto/16 :goto_1

    .line 1972030
    :cond_f
    const-string v26, "timestamp_init"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_10

    .line 1972031
    const/4 v4, 0x1

    .line 1972032
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto/16 :goto_1

    .line 1972033
    :cond_10
    const-string v26, "timestamp_token"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_11

    .line 1972034
    const/4 v3, 0x1

    .line 1972035
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 1972036
    :cond_11
    const-string v26, "url"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_12

    .line 1972037
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1972038
    :cond_12
    const-string v26, "user_code"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_13

    .line 1972039
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1972040
    :cond_13
    const-string v26, "user_id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 1972041
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1972042
    :cond_14
    const/16 v25, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1972043
    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1972044
    const/16 v24, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1972045
    const/16 v23, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1972046
    const/16 v22, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1972047
    const/16 v21, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1972048
    const/16 v20, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1972049
    const/16 v19, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1972050
    const/16 v18, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1972051
    const/16 v17, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1972052
    const/16 v16, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1972053
    const/16 v15, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1972054
    const/16 v14, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1972055
    const/16 v13, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1972056
    if-eqz v5, :cond_15

    .line 1972057
    const/16 v5, 0xd

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11, v12}, LX/186;->a(III)V

    .line 1972058
    :cond_15
    if-eqz v4, :cond_16

    .line 1972059
    const/16 v4, 0xe

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10, v5}, LX/186;->a(III)V

    .line 1972060
    :cond_16
    if-eqz v3, :cond_17

    .line 1972061
    const/16 v3, 0xf

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 1972062
    :cond_17
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1972063
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1972064
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1972065
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1972066
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1972067
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972068
    if-eqz v0, :cond_0

    .line 1972069
    const-string v1, "action_properties"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972070
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972071
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972072
    if-eqz v0, :cond_1

    .line 1972073
    const-string v1, "action_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972074
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972075
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972076
    if-eqz v0, :cond_2

    .line 1972077
    const-string v1, "auth_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972078
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972079
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972080
    if-eqz v0, :cond_3

    .line 1972081
    const-string v1, "client_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972082
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972083
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972084
    if-eqz v0, :cond_4

    .line 1972085
    const-string v1, "code_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972086
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972087
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972088
    if-eqz v0, :cond_5

    .line 1972089
    const-string v1, "device_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972090
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972091
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972092
    if-eqz v0, :cond_6

    .line 1972093
    const-string v1, "device_model"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972094
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972095
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972096
    if-eqz v0, :cond_7

    .line 1972097
    const-string v1, "machine_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972098
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972099
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972100
    if-eqz v0, :cond_8

    .line 1972101
    const-string v1, "nonce"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972102
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972103
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972104
    if-eqz v0, :cond_9

    .line 1972105
    const-string v1, "redirect_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972106
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972107
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972108
    if-eqz v0, :cond_a

    .line 1972109
    const-string v1, "result_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972110
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972111
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972112
    if-eqz v0, :cond_b

    .line 1972113
    const-string v1, "scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972114
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972115
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972116
    if-eqz v0, :cond_c

    .line 1972117
    const-string v1, "session_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972118
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972119
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1972120
    if-eqz v0, :cond_d

    .line 1972121
    const-string v1, "timestamp_expire"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972122
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1972123
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1972124
    if-eqz v0, :cond_e

    .line 1972125
    const-string v1, "timestamp_init"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972126
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1972127
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1972128
    if-eqz v0, :cond_f

    .line 1972129
    const-string v1, "timestamp_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972130
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1972131
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972132
    if-eqz v0, :cond_10

    .line 1972133
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972134
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972135
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972136
    if-eqz v0, :cond_11

    .line 1972137
    const-string v1, "user_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972138
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972139
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1972140
    if-eqz v0, :cond_12

    .line 1972141
    const-string v1, "user_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1972142
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1972143
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1972144
    return-void
.end method
