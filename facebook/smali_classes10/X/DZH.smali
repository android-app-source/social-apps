.class public abstract LX/DZH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public d:LX/0kL;

.field public e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/0tX;

.field public g:Ljava/lang/String;

.field public h:LX/DQQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2013251
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/DZH;->a:Ljava/util/HashMap;

    .line 2013252
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/DZH;->b:Ljava/util/HashMap;

    .line 2013253
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/DZH;->c:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(LX/0kL;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;LX/0tX;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param

    .prologue
    .line 2013254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2013255
    iput-object p1, p0, LX/DZH;->d:LX/0kL;

    .line 2013256
    iput-object p2, p0, LX/DZH;->e:Ljava/util/concurrent/ExecutorService;

    .line 2013257
    iput-object p3, p0, LX/DZH;->g:Ljava/lang/String;

    .line 2013258
    iput-object p4, p0, LX/DZH;->f:LX/0tX;

    .line 2013259
    sget-object p0, LX/DZH;->a:Ljava/util/HashMap;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->ALL_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    const-string p2, "ALL_POSTS"

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013260
    sget-object p0, LX/DZH;->a:Ljava/util/HashMap;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->FRIEND_POSTS:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    const-string p2, "FRIEND_POSTS"

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013261
    sget-object p0, LX/DZH;->a:Ljava/util/HashMap;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    const-string p2, "HIGHLIGHTS"

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013262
    sget-object p0, LX/DZH;->a:Ljava/util/HashMap;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    const-string p2, "OFF"

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013263
    sget-object p0, LX/DZH;->b:Ljava/util/HashMap;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const-string p2, "ON"

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013264
    sget-object p0, LX/DZH;->b:Ljava/util/HashMap;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const-string p2, "HIGHLIGHTS"

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013265
    sget-object p0, LX/DZH;->b:Ljava/util/HashMap;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const-string p2, "OFF"

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013266
    sget-object p0, LX/DZH;->c:Ljava/util/HashMap;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    const-string p2, "ON"

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013267
    sget-object p0, LX/DZH;->c:Ljava/util/HashMap;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    const-string p2, "OFF"

    invoke-virtual {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2013268
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V
    .locals 3

    .prologue
    .line 2013236
    iget-object v0, p0, LX/DZH;->h:LX/DQQ;

    if-eqz v0, :cond_0

    .line 2013237
    iget-object v0, p0, LX/DZH;->h:LX/DQQ;

    invoke-interface {v0, p3}, LX/DQQ;->a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V

    .line 2013238
    :cond_0
    new-instance v0, LX/4GG;

    invoke-direct {v0}, LX/4GG;-><init>()V

    iget-object v1, p0, LX/DZH;->g:Ljava/lang/String;

    .line 2013239
    const-string v2, "actor_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013240
    move-object v0, v0

    .line 2013241
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013242
    move-object v1, v0

    .line 2013243
    sget-object v0, LX/DZH;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2013244
    const-string v2, "setting"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013245
    move-object v0, v1

    .line 2013246
    new-instance v1, LX/DZl;

    invoke-direct {v1}, LX/DZl;-><init>()V

    move-object v1, v1

    .line 2013247
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/DZl;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2013248
    iget-object v1, p0, LX/DZH;->f:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2013249
    new-instance v1, LX/DZE;

    invoke-direct {v1, p0, p2}, LX/DZE;-><init>(LX/DZH;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V

    iget-object v2, p0, LX/DZH;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2013250
    return-void
.end method
