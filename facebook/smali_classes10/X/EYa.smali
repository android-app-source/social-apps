.class public final LX/EYa;
.super LX/EYZ;
.source ""


# static fields
.field public static final c:LX/EYa;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/EYY;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/EYX;",
            "LX/EYY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2137598
    new-instance v0, LX/EYa;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/EYa;-><init>(B)V

    sput-object v0, LX/EYa;->c:LX/EYa;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2137593
    invoke-direct {p0}, LX/EYZ;-><init>()V

    .line 2137594
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EYa;->a:Ljava/util/Map;

    .line 2137595
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EYa;->b:Ljava/util/Map;

    .line 2137596
    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    .prologue
    .line 2137599
    sget-object v0, LX/EYZ;->c:LX/EYZ;

    move-object v0, v0

    .line 2137600
    invoke-direct {p0, v0}, LX/EYZ;-><init>(LX/EYZ;)V

    .line 2137601
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/EYa;->a:Ljava/util/Map;

    .line 2137602
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/EYa;->b:Ljava/util/Map;

    .line 2137603
    return-void
.end method


# virtual methods
.method public final a(LX/EYF;I)LX/EYY;
    .locals 2

    .prologue
    .line 2137597
    iget-object v0, p0, LX/EYa;->b:Ljava/util/Map;

    new-instance v1, LX/EYX;

    invoke-direct {v1, p1, p2}, LX/EYX;-><init>(LX/EYF;I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYY;

    return-object v0
.end method
