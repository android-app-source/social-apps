.class public final LX/DnL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;I)V
    .locals 0

    .prologue
    .line 2040303
    iput-object p1, p0, LX/DnL;->b:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    iput p2, p0, LX/DnL;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x3a3eb997

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2040296
    iget-object v0, p0, LX/DnL;->b:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    iget-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->e:LX/DjO;

    iget-object v0, p0, LX/DnL;->b:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->b:LX/0Px;

    iget v3, p0, LX/DnL;->a:I

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    .line 2040297
    iget-object v3, v2, LX/DjO;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    .line 2040298
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    .line 2040299
    const-string p1, "extra_selected_service_item"

    invoke-static {p0, p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2040300
    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object p1

    const/4 v2, -0x1

    invoke-virtual {p1, v2, p0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2040301
    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 2040302
    const v0, 0x670ebff6

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
