.class public LX/DO3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0zS;


# instance fields
.field private final b:LX/0tX;

.field private final c:LX/0Sh;

.field public final d:LX/DOK;

.field private final e:LX/0hB;

.field private final f:LX/0sX;

.field private final g:Landroid/content/res/Resources;

.field private final h:LX/3my;

.field private final i:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1992089
    sget-object v0, LX/0zS;->d:LX/0zS;

    sput-object v0, LX/DO3;->a:LX/0zS;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0Sh;LX/DOK;LX/0hB;LX/0sX;Landroid/content/res/Resources;LX/3my;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1992091
    iput-object p1, p0, LX/DO3;->b:LX/0tX;

    .line 1992092
    iput-object p2, p0, LX/DO3;->c:LX/0Sh;

    .line 1992093
    iput-object p3, p0, LX/DO3;->d:LX/DOK;

    .line 1992094
    iput-object p4, p0, LX/DO3;->e:LX/0hB;

    .line 1992095
    iput-object p5, p0, LX/DO3;->f:LX/0sX;

    .line 1992096
    iput-object p6, p0, LX/DO3;->g:Landroid/content/res/Resources;

    .line 1992097
    iput-object p7, p0, LX/DO3;->h:LX/3my;

    .line 1992098
    iput-object p8, p0, LX/DO3;->i:LX/0Uh;

    .line 1992099
    return-void
.end method

.method public static a(LX/0QB;)LX/DO3;
    .locals 1

    .prologue
    .line 1992088
    invoke-static {p0}, LX/DO3;->b(LX/0QB;)LX/DO3;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DO3;
    .locals 9

    .prologue
    .line 1992086
    new-instance v0, LX/DO3;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/DOK;->a(LX/0QB;)LX/DOK;

    move-result-object v3

    check-cast v3, LX/DOK;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v5

    check-cast v5, LX/0sX;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {p0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v7

    check-cast v7, LX/3my;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v0 .. v8}, LX/DO3;-><init>(LX/0tX;LX/0Sh;LX/DOK;LX/0hB;LX/0sX;Landroid/content/res/Resources;LX/3my;LX/0Uh;)V

    .line 1992087
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0zS;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0zS;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1992078
    iget-object v0, p0, LX/DO3;->e:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    invoke-static {v0, p4}, LX/Baq;->b(II)I

    move-result v0

    .line 1992079
    div-int/lit8 v1, v0, 0x2

    .line 1992080
    invoke-static {}, LX/9Mb;->a()LX/9Ma;

    move-result-object v2

    .line 1992081
    const-string v3, "group_id"

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "first_members"

    const-string v5, "8"

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "profile_picture_size"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v3, "cover_photo_width"

    iget-object v4, p0, LX/DO3;->e:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v3, "cover_photo_height"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "multi_company_name_limit"

    const-string v3, "2"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/DO3;->f:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "card_image_width"

    iget-object v3, p0, LX/DO3;->g:Landroid/content/res/Resources;

    const v4, 0x7f0b1222

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "inviter_profile_width"

    const-string v3, "80"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "inviter_profile_height"

    const-string v3, "80"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "first_groups_rows_in_community"

    const-string v3, "3"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "first_groups_pogs_in_community"

    const-string v3, "20"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "first_community_events"

    const-string v3, "3"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "first_community_forsale_stories"

    const-string v3, "20"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "first_community_trending_stories"

    const-string v3, "3"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "number_of_tips"

    const-string v3, "10"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "first_learning_courses"

    const-string v3, "20"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "first_community_sections"

    const-string v3, "20"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "first_invite_friends"

    const-string v3, "3"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "invite_friends_profile_picture_size"

    const-string v3, "85"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "invite_friends_profile_picture_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "enable_page_voice_switcher"

    iget-object v3, p0, LX/DO3;->i:LX/0Uh;

    const/16 v4, 0x231

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "first_news"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "news_photo_height"

    iget-object v3, p0, LX/DO3;->g:Landroid/content/res/Resources;

    const v4, 0x7f0b2019

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "news_photo_width"

    iget-object v3, p0, LX/DO3;->g:Landroid/content/res/Resources;

    const v4, 0x7f0b2017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "cross_post_suggestion_image_size"

    const-string v3, "160"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "creation_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "viewer_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const/4 v1, 0x1

    .line 1992082
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 1992083
    iget-object v0, p0, LX/DO3;->b:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/32 v2, 0x93a80

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1992084
    iget-object v1, p0, LX/DO3;->c:LX/0Sh;

    new-instance v2, LX/DO2;

    invoke-direct {v2, p0, p1}, LX/DO2;-><init>(LX/DO3;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1992085
    return-object v0
.end method
