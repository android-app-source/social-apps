.class public final LX/Ct0;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V
    .locals 0

    .prologue
    .line 1943667
    iput-object p1, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method

.method private a(LX/1ln;)V
    .locals 2

    .prologue
    .line 1943668
    iget-object v1, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 1943669
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->f:Z

    .line 1943670
    iget-object v0, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    iget-boolean v0, v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->h:LX/Cov;

    if-eqz v0, :cond_0

    .line 1943671
    iget-object v0, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->h:LX/Cov;

    iget-object v1, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-interface {v0, v1}, LX/Cov;->a(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V

    .line 1943672
    :cond_0
    return-void

    .line 1943673
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/1ln;Landroid/graphics/drawable/Animatable;)V
    .locals 2
    .param p1    # LX/1ln;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1943674
    iget-object v1, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    .line 1943675
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->f:Z

    .line 1943676
    if-eqz p2, :cond_0

    .line 1943677
    invoke-interface {p2}, Landroid/graphics/drawable/Animatable;->start()V

    .line 1943678
    :cond_0
    iget-object v0, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    iget-boolean v0, v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->h:LX/Cov;

    if-eqz v0, :cond_1

    .line 1943679
    iget-object v0, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->h:LX/Cov;

    iget-object v1, p0, LX/Ct0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-interface {v0, v1}, LX/Cov;->b(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V

    .line 1943680
    :cond_1
    return-void

    .line 1943681
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1943682
    check-cast p2, LX/1ln;

    invoke-direct {p0, p2, p3}, LX/Ct0;->a(LX/1ln;Landroid/graphics/drawable/Animatable;)V

    return-void
.end method

.method public final synthetic b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1943683
    check-cast p2, LX/1ln;

    invoke-direct {p0, p2}, LX/Ct0;->a(LX/1ln;)V

    return-void
.end method
