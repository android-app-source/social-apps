.class public final LX/ENO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:LX/Cwx;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Ljava/util/List;

.field public final synthetic d:LX/697;

.field public final synthetic e:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;LX/Cwx;LX/0Px;Ljava/util/List;LX/697;)V
    .locals 0

    .prologue
    .line 2112517
    iput-object p1, p0, LX/ENO;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;

    iput-object p2, p0, LX/ENO;->a:LX/Cwx;

    iput-object p3, p0, LX/ENO;->b:LX/0Px;

    iput-object p4, p0, LX/ENO;->c:Ljava/util/List;

    iput-object p5, p0, LX/ENO;->d:LX/697;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    .line 2112518
    const v0, 0x7f020e90

    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v1

    .line 2112519
    invoke-virtual {p1}, LX/6al;->a()V

    .line 2112520
    invoke-virtual {p1, v4}, LX/6al;->a(Z)V

    .line 2112521
    new-instance v0, LX/ENM;

    invoke-direct {v0, p0}, LX/ENM;-><init>(LX/ENO;)V

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6ak;)V

    .line 2112522
    new-instance v0, LX/ENN;

    invoke-direct {v0, p0}, LX/ENN;-><init>(LX/ENO;)V

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6Zu;)V

    .line 2112523
    invoke-virtual {p1}, LX/6al;->c()LX/6az;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/6az;->c(Z)V

    .line 2112524
    invoke-virtual {p1}, LX/6al;->c()LX/6az;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/6az;->b(Z)V

    .line 2112525
    invoke-virtual {p1}, LX/6al;->c()LX/6az;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/6az;->a(Z)V

    .line 2112526
    iget-object v0, p0, LX/ENO;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    .line 2112527
    new-instance v3, LX/699;

    invoke-direct {v3}, LX/699;-><init>()V

    .line 2112528
    iput-object v0, v3, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2112529
    move-object v0, v3

    .line 2112530
    iput-object v1, v0, LX/699;->c:LX/68w;

    .line 2112531
    move-object v0, v0

    .line 2112532
    invoke-virtual {v0, v5, v5}, LX/699;->a(FF)LX/699;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/6al;->a(LX/699;)LX/6ax;

    goto :goto_0

    .line 2112533
    :cond_0
    iget-object v0, p0, LX/ENO;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2112534
    iget-object v0, p0, LX/ENO;->c:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    const/high16 v1, 0x41300000    # 11.0f

    invoke-static {v0, v1}, LX/692;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/692;

    move-result-object v0

    invoke-static {v0}, LX/6aN;->a(LX/692;)LX/6aM;

    move-result-object v0

    .line 2112535
    :goto_1
    invoke-virtual {p1, v0}, LX/6al;->a(LX/6aM;)V

    .line 2112536
    return-void

    .line 2112537
    :cond_1
    iget-object v0, p0, LX/ENO;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f0b1718

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget-object v0, p0, LX/ENO;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v2, 0x7f0b1717

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 2112538
    iget-object v1, p0, LX/ENO;->d:LX/697;

    invoke-static {v1, v0}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v0

    goto :goto_1
.end method
