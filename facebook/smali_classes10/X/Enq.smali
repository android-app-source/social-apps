.class public final enum LX/Enq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Enq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Enq;

.field public static final enum LEFT:LX/Enq;

.field public static final enum RIGHT:LX/Enq;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2167493
    new-instance v0, LX/Enq;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LX/Enq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Enq;->LEFT:LX/Enq;

    .line 2167494
    new-instance v0, LX/Enq;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, LX/Enq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Enq;->RIGHT:LX/Enq;

    .line 2167495
    const/4 v0, 0x2

    new-array v0, v0, [LX/Enq;

    sget-object v1, LX/Enq;->LEFT:LX/Enq;

    aput-object v1, v0, v2

    sget-object v1, LX/Enq;->RIGHT:LX/Enq;

    aput-object v1, v0, v3

    sput-object v0, LX/Enq;->$VALUES:[LX/Enq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2167496
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Enq;
    .locals 1

    .prologue
    .line 2167497
    const-class v0, LX/Enq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Enq;

    return-object v0
.end method

.method public static values()[LX/Enq;
    .locals 1

    .prologue
    .line 2167498
    sget-object v0, LX/Enq;->$VALUES:[LX/Enq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Enq;

    return-object v0
.end method
