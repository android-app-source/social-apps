.class public LX/CnB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cms;


# instance fields
.field private final a:LX/Cju;


# direct methods
.method public constructor <init>(LX/Cju;)V
    .locals 0

    .prologue
    .line 1933792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933793
    iput-object p1, p0, LX/CnB;->a:LX/Cju;

    .line 1933794
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;Landroid/graphics/Rect;LX/Cmr;)V
    .locals 5

    .prologue
    .line 1933795
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 1933796
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 1933797
    sget-object v2, LX/Cmr;->CENTER:LX/Cmr;

    if-ne p2, v2, :cond_0

    .line 1933798
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 1933799
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v2, v1, v3, v0, v4}, LX/CoV;->a(Landroid/view/View;IIII)V

    .line 1933800
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/Cml;)V
    .locals 2

    .prologue
    .line 1933801
    invoke-interface {p2}, LX/Cml;->a()LX/Cmw;

    move-result-object v0

    iget-object v1, p0, LX/CnB;->a:LX/Cju;

    invoke-static {v0, v1}, LX/Cn1;->a(LX/Cmw;LX/Cju;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1933802
    invoke-interface {p2}, LX/Cml;->b()LX/Cmr;

    move-result-object v1

    .line 1933803
    check-cast p1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, LX/CnB;->a(Landroid/view/ViewGroup;Landroid/graphics/Rect;LX/Cmr;)V

    .line 1933804
    return-void
.end method
