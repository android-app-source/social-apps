.class public final enum LX/DS9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DS9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DS9;

.field public static final enum LOADING_BAR:LX/DS9;

.field public static final enum MEMBER_ROW:LX/DS9;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1999227
    new-instance v0, LX/DS9;

    const-string v1, "MEMBER_ROW"

    invoke-direct {v0, v1, v2}, LX/DS9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DS9;->MEMBER_ROW:LX/DS9;

    .line 1999228
    new-instance v0, LX/DS9;

    const-string v1, "LOADING_BAR"

    invoke-direct {v0, v1, v3}, LX/DS9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DS9;->LOADING_BAR:LX/DS9;

    .line 1999229
    const/4 v0, 0x2

    new-array v0, v0, [LX/DS9;

    sget-object v1, LX/DS9;->MEMBER_ROW:LX/DS9;

    aput-object v1, v0, v2

    sget-object v1, LX/DS9;->LOADING_BAR:LX/DS9;

    aput-object v1, v0, v3

    sput-object v0, LX/DS9;->$VALUES:[LX/DS9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1999230
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DS9;
    .locals 1

    .prologue
    .line 1999231
    const-class v0, LX/DS9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DS9;

    return-object v0
.end method

.method public static values()[LX/DS9;
    .locals 1

    .prologue
    .line 1999232
    sget-object v0, LX/DS9;->$VALUES:[LX/DS9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DS9;

    return-object v0
.end method
