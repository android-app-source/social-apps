.class public LX/EhY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Lcom/facebook/bookmark/model/Bookmark;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Landroid/os/Parcelable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/facebook/bookmark/model/Bookmark;Ljava/lang/String;ZLandroid/os/Parcelable;)V
    .locals 2
    .param p5    # Landroid/os/Parcelable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2158535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2158536
    const/4 v0, 0x0

    iput-object v0, p0, LX/EhY;->f:Landroid/os/Bundle;

    .line 2158537
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, LX/EhY;->a:Landroid/app/Activity;

    .line 2158538
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 2158539
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bookmark and url are both null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2158540
    :cond_0
    iput-object p2, p0, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    .line 2158541
    iput-object p3, p0, LX/EhY;->c:Ljava/lang/String;

    .line 2158542
    iput-boolean p4, p0, LX/EhY;->d:Z

    .line 2158543
    iput-object p5, p0, LX/EhY;->e:Landroid/os/Parcelable;

    .line 2158544
    return-void
.end method

.method public static newBuilder()LX/EhZ;
    .locals 1

    .prologue
    .line 2158545
    new-instance v0, LX/EhZ;

    invoke-direct {v0}, LX/EhZ;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2158546
    iget-object v0, p0, LX/EhY;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EhY;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EhY;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 2158547
    iget-object v0, p0, LX/EhY;->f:Landroid/os/Bundle;

    if-nez v0, :cond_1

    .line 2158548
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/EhY;->f:Landroid/os/Bundle;

    .line 2158549
    iget-object v0, p0, LX/EhY;->f:Landroid/os/Bundle;

    const-string v1, "url"

    invoke-virtual {p0}, LX/EhY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2158550
    iget-object v0, p0, LX/EhY;->e:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 2158551
    iget-object v0, p0, LX/EhY;->f:Landroid/os/Bundle;

    const-string v1, "data"

    iget-object v2, p0, LX/EhY;->e:Landroid/os/Parcelable;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2158552
    :cond_0
    iget-object v0, p0, LX/EhY;->f:Landroid/os/Bundle;

    const-string v1, "long_click"

    iget-boolean v2, p0, LX/EhY;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2158553
    :cond_1
    iget-object v0, p0, LX/EhY;->f:Landroid/os/Bundle;

    return-object v0
.end method
