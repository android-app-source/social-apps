.class public abstract LX/Efq;
.super LX/0gG;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/Efs;

.field public c:LX/AKu;

.field private d:I

.field public final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/Efp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Efs;LX/AKu;)V
    .locals 1

    .prologue
    .line 2155418
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2155419
    const/4 v0, -0x1

    iput v0, p0, LX/Efq;->d:I

    .line 2155420
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Efq;->e:Landroid/util/SparseArray;

    .line 2155421
    iput-object p1, p0, LX/Efq;->a:Landroid/content/Context;

    .line 2155422
    iput-object p2, p0, LX/Efq;->b:LX/Efs;

    .line 2155423
    iput-object p3, p0, LX/Efq;->c:LX/AKu;

    .line 2155424
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 2155412
    invoke-virtual {p0, p2}, LX/Efq;->e(I)LX/Efo;

    move-result-object v0

    .line 2155413
    if-eqz v0, :cond_0

    .line 2155414
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, LX/Efo;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2155415
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2155416
    iget-object v1, p0, LX/Efq;->e:Landroid/util/SparseArray;

    new-instance v2, LX/Efp;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v0, v3}, LX/Efp;-><init>(LX/Efq;LX/Efo;Z)V

    invoke-virtual {v1, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2155417
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2155399
    check-cast p3, LX/Efo;

    .line 2155400
    const/4 v0, 0x0

    .line 2155401
    iput-object v0, p3, LX/Efo;->m:LX/Efs;

    .line 2155402
    invoke-virtual {p3}, LX/Efo;->f()V

    .line 2155403
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2155404
    iget-object v0, p0, LX/Efq;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 2155405
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2155425
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2155406
    iget v0, p0, LX/Efq;->d:I

    if-ne v0, p2, :cond_1

    .line 2155407
    :cond_0
    :goto_0
    return-void

    .line 2155408
    :cond_1
    iget-object v0, p0, LX/Efq;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Efp;

    .line 2155409
    if-eqz v0, :cond_0

    .line 2155410
    invoke-virtual {v0}, LX/Efp;->a()V

    .line 2155411
    iput p2, p0, LX/Efq;->d:I

    goto :goto_0
.end method

.method public abstract e(I)LX/Efo;
.end method
