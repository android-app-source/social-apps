.class public LX/D8o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2mz;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/17V;

.field private final c:LX/1nA;

.field private final d:LX/2mk;

.field private final e:LX/D8m;

.field private final f:LX/D8i;

.field private g:Landroid/content/Context;

.field private h:Landroid/view/ViewGroup;

.field private i:Landroid/view/ViewGroup;

.field private j:Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

.field public k:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/D8b;

.field public m:LX/D8V;

.field private n:I


# direct methods
.method public constructor <init>(LX/0Zb;LX/17V;LX/1nA;LX/2mk;LX/D8m;LX/D8i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1969500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1969501
    iput-object p1, p0, LX/D8o;->a:LX/0Zb;

    .line 1969502
    iput-object p2, p0, LX/D8o;->b:LX/17V;

    .line 1969503
    iput-object p3, p0, LX/D8o;->c:LX/1nA;

    .line 1969504
    iput-object p4, p0, LX/D8o;->d:LX/2mk;

    .line 1969505
    iput-object p5, p0, LX/D8o;->e:LX/D8m;

    .line 1969506
    iput-object p6, p0, LX/D8o;->f:LX/D8i;

    .line 1969507
    return-void
.end method

.method public static a(LX/0QB;)LX/D8o;
    .locals 10

    .prologue
    .line 1969489
    const-class v1, LX/D8o;

    monitor-enter v1

    .line 1969490
    :try_start_0
    sget-object v0, LX/D8o;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1969491
    sput-object v2, LX/D8o;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1969492
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1969493
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1969494
    new-instance v3, LX/D8o;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v5

    check-cast v5, LX/17V;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v6

    check-cast v6, LX/1nA;

    invoke-static {v0}, LX/2mk;->a(LX/0QB;)LX/2mk;

    move-result-object v7

    check-cast v7, LX/2mk;

    invoke-static {v0}, LX/D8m;->a(LX/0QB;)LX/D8m;

    move-result-object v8

    check-cast v8, LX/D8m;

    invoke-static {v0}, LX/D8i;->a(LX/0QB;)LX/D8i;

    move-result-object v9

    check-cast v9, LX/D8i;

    invoke-direct/range {v3 .. v9}, LX/D8o;-><init>(LX/0Zb;LX/17V;LX/1nA;LX/2mk;LX/D8m;LX/D8i;)V

    .line 1969495
    move-object v0, v3

    .line 1969496
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1969497
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D8o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1969498
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1969499
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private f()V
    .locals 13

    .prologue
    const/4 v8, 0x0

    .line 1969411
    iget-object v0, p0, LX/D8o;->h:Landroid/view/ViewGroup;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1969412
    iget-object v0, p0, LX/D8o;->g:Landroid/content/Context;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1969413
    iget-object v0, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1969414
    iget-object v0, p0, LX/D8o;->l:LX/D8b;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1969415
    iget-object v0, p0, LX/D8o;->m:LX/D8V;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1969416
    iget-object v0, p0, LX/D8o;->g:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031612

    iget-object v2, p0, LX/D8o;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/D8o;->i:Landroid/view/ViewGroup;

    .line 1969417
    iget-object v0, p0, LX/D8o;->h:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/D8o;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1969418
    iget-object v0, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969419
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1969420
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VS;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 1969421
    if-nez v2, :cond_1

    .line 1969422
    :cond_0
    :goto_0
    return-void

    .line 1969423
    :cond_1
    invoke-static {v2}, LX/2mk;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v3

    .line 1969424
    if-eqz v3, :cond_5

    .line 1969425
    new-instance v0, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    iget-object v1, p0, LX/D8o;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/D8o;->j:Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    .line 1969426
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1969427
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 1969428
    iget-object v1, p0, LX/D8o;->j:Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    .line 1969429
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/3qk;->a(I)D

    move-result-wide v9

    const-wide v11, 0x3fe6666666666666L    # 0.7

    cmpl-double v9, v9, v11

    if-ltz v9, :cond_9

    :cond_2
    const/4 v9, 0x1

    :goto_1
    move v4, v9

    .line 1969430
    if-eqz v4, :cond_a

    .line 1969431
    iget-object v5, v1, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    const/high16 v6, -0x1000000

    invoke-virtual {v5, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1969432
    :goto_2
    iget-object v1, p0, LX/D8o;->j:Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    invoke-virtual {v1, v0}, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->setBackgroundColor(I)V

    .line 1969433
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1969434
    iget-object v0, p0, LX/D8o;->j:Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aj()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1969435
    iget-object v4, v0, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v5, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v1, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1969436
    :cond_4
    iget-object v0, p0, LX/D8o;->j:Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    new-instance v1, LX/D8n;

    invoke-direct {v1, p0}, LX/D8n;-><init>(LX/D8o;)V

    .line 1969437
    iget-object v4, v0, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1969438
    iget-object v0, p0, LX/D8o;->h:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/D8o;->j:Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    invoke-virtual {v0, v1, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 1969439
    :cond_5
    iget-object v0, p0, LX/D8o;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1969440
    iget v1, p0, LX/D8o;->n:I

    invoke-virtual {v0, v8, v1, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1969441
    iget-object v0, p0, LX/D8o;->i:Landroid/view/ViewGroup;

    const v1, 0x7f0d319f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1969442
    iget-object v4, p0, LX/D8o;->c:LX/1nA;

    iget-object v1, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969443
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v5

    .line 1969444
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v4, v1, v2}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1969445
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1969446
    iget-object v0, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969447
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1969448
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1969449
    iget-object v1, p0, LX/D8o;->e:LX/D8m;

    iget-object v4, p0, LX/D8o;->g:Landroid/content/Context;

    .line 1969450
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 1969451
    invoke-static {v1, v5, v0, v3}, LX/D8m;->a(LX/D8m;Landroid/content/res/Resources;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Z)F

    move-result v6

    .line 1969452
    invoke-static {v5}, LX/D8m;->a(Landroid/content/res/Resources;)F

    move-result v7

    sub-float/2addr v6, v7

    .line 1969453
    const v7, 0x7f0b1c8e

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 1969454
    sub-float/2addr v6, v7

    .line 1969455
    invoke-static {v5}, LX/D8m;->d(Landroid/content/res/Resources;)F

    move-result v5

    invoke-static {v6, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    move v1, v5

    .line 1969456
    iget-object v4, p0, LX/D8o;->f:LX/D8i;

    iget-object v5, p0, LX/D8o;->i:Landroid/view/ViewGroup;

    .line 1969457
    iput-object v5, v4, LX/D8i;->d:Landroid/view/ViewGroup;

    .line 1969458
    iget-object v4, p0, LX/D8o;->f:LX/D8i;

    iget-object v5, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v6, p0, LX/D8o;->l:LX/D8b;

    iget-object v7, p0, LX/D8o;->g:Landroid/content/Context;

    invoke-virtual {v4, v1, v5, v6, v7}, LX/D8i;->a(FLcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;Landroid/content/Context;)V

    .line 1969459
    iget-object v1, p0, LX/D8o;->e:LX/D8m;

    iget-object v4, p0, LX/D8o;->g:Landroid/content/Context;

    .line 1969460
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 1969461
    invoke-static {v1, v5, v0, v3}, LX/D8m;->a(LX/D8m;Landroid/content/res/Resources;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Z)F

    move-result v6

    invoke-static {v5}, LX/D8m;->a(Landroid/content/res/Resources;)F

    move-result v7

    sub-float/2addr v6, v7

    invoke-static {v5}, LX/D8m;->d(Landroid/content/res/Resources;)F

    move-result v7

    sub-float/2addr v6, v7

    .line 1969462
    const v7, 0x7f0b1c8f

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 1969463
    const v1, 0x7f0b02b4

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v7, v4

    add-float/2addr v7, v1

    move v5, v7

    .line 1969464
    cmpl-float v5, v6, v5

    if-ltz v5, :cond_b

    const/4 v5, 0x1

    :goto_3
    move v0, v5

    .line 1969465
    if-eqz v0, :cond_6

    .line 1969466
    iget-object v0, p0, LX/D8o;->i:Landroid/view/ViewGroup;

    const v1, 0x7f0d31a1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 1969467
    invoke-virtual {v0, v8}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1969468
    iget-object v1, p0, LX/D8o;->d:LX/2mk;

    .line 1969469
    iget-object v3, v1, LX/2mk;->a:LX/0ad;

    sget-short v4, LX/38t;->e:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v1, v3

    .line 1969470
    if-eqz v1, :cond_8

    .line 1969471
    const/16 v1, 0x88

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1969472
    :goto_4
    iget-object v3, p0, LX/D8o;->c:LX/1nA;

    iget-object v1, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969473
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v4

    .line 1969474
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v3, v1, v2}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1969475
    :cond_6
    iget-object v0, p0, LX/D8o;->d:LX/2mk;

    .line 1969476
    iget-object v1, v0, LX/2mk;->a:LX/0ad;

    sget-short v3, LX/38t;->d:S

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1969477
    if-eqz v0, :cond_0

    .line 1969478
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v1

    .line 1969479
    if-nez v1, :cond_c

    .line 1969480
    :cond_7
    :goto_5
    goto/16 :goto_0

    .line 1969481
    :cond_8
    const/16 v1, 0x108

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    goto :goto_4

    :cond_9
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 1969482
    :cond_a
    iget-object v5, v1, Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    goto/16 :goto_2

    :cond_b
    const/4 v5, 0x0

    goto :goto_3

    .line 1969483
    :cond_c
    iget-object v0, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    .line 1969484
    iget-object v4, p0, LX/D8o;->b:LX/17V;

    iget-object v0, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969485
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 1969486
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    const-string v5, "video"

    invoke-virtual {v4, v1, v0, v3, v5}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1969487
    if-eqz v0, :cond_7

    .line 1969488
    iget-object v1, p0, LX/D8o;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_5
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1969401
    iget-object v0, p0, LX/D8o;->f:LX/D8i;

    if-eqz v0, :cond_1

    .line 1969402
    iget-object v0, p0, LX/D8o;->f:LX/D8i;

    const/4 v2, 0x0

    .line 1969403
    iget-object v1, v0, LX/D8i;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    if-eqz v1, :cond_0

    .line 1969404
    iget-object v1, v0, LX/D8i;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-static {v1}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 1969405
    :cond_0
    iput-object v2, v0, LX/D8i;->d:Landroid/view/ViewGroup;

    .line 1969406
    iput-object v2, v0, LX/D8i;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1969407
    :cond_1
    iget-object v0, p0, LX/D8o;->h:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 1969408
    iget-object v0, p0, LX/D8o;->h:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/D8o;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1969409
    iget-object v0, p0, LX/D8o;->h:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/D8o;->j:Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1969410
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1969393
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1969394
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VS;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1969395
    if-nez v0, :cond_0

    move v0, v1

    .line 1969396
    :goto_0
    return v0

    .line 1969397
    :cond_0
    invoke-static {v0}, LX/2mk;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v0

    .line 1969398
    if-nez v0, :cond_1

    move v0, v1

    .line 1969399
    goto :goto_0

    .line 1969400
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c90

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1969386
    invoke-direct {p0}, LX/D8o;->g()V

    .line 1969387
    iput-object v0, p0, LX/D8o;->h:Landroid/view/ViewGroup;

    .line 1969388
    iput-object v0, p0, LX/D8o;->i:Landroid/view/ViewGroup;

    .line 1969389
    iput-object v0, p0, LX/D8o;->j:Lcom/facebook/video/watchandshop/WatchAndShopHeaderView;

    .line 1969390
    iput-object v0, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969391
    iput-object v0, p0, LX/D8o;->l:LX/D8b;

    .line 1969392
    return-void
.end method

.method public final a(ILandroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;LX/D8V;ILX/D8S;)V
    .locals 0
    .param p9    # LX/D8S;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/D8b;",
            "Lcom/facebook/video/watchandmore/core/OnExitWatchAndMoreListener;",
            "I",
            "Lcom/facebook/video/watchandmore/core/WatchAndMoreContentAnimationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1969378
    iput-object p2, p0, LX/D8o;->h:Landroid/view/ViewGroup;

    .line 1969379
    iput-object p4, p0, LX/D8o;->g:Landroid/content/Context;

    .line 1969380
    iput-object p5, p0, LX/D8o;->k:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969381
    iput-object p6, p0, LX/D8o;->l:LX/D8b;

    .line 1969382
    iput-object p7, p0, LX/D8o;->m:LX/D8V;

    .line 1969383
    iput p8, p0, LX/D8o;->n:I

    .line 1969384
    invoke-direct {p0}, LX/D8o;->f()V

    .line 1969385
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;I)V
    .locals 2

    .prologue
    .line 1969373
    invoke-direct {p0}, LX/D8o;->g()V

    .line 1969374
    iput p2, p0, LX/D8o;->n:I

    .line 1969375
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1969376
    invoke-direct {p0}, LX/D8o;->f()V

    .line 1969377
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1969372
    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1969371
    const/4 v0, 0x1

    return v0
.end method

.method public final c()LX/D8g;
    .locals 1

    .prologue
    .line 1969368
    sget-object v0, LX/D8g;->WATCH_AND_SHOP:LX/D8g;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1969370
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/D7g;
    .locals 1

    .prologue
    .line 1969369
    const/4 v0, 0x0

    return-object v0
.end method
