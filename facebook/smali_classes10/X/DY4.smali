.class public final LX/DY4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/DY6;


# direct methods
.method public constructor <init>(LX/DY6;)V
    .locals 0

    .prologue
    .line 2011142
    iput-object p1, p0, LX/DY4;->a:LX/DY6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    .line 2011143
    iget-object v0, p0, LX/DY4;->a:LX/DY6;

    iget-object v0, v0, LX/DY6;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v0, v0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->k:LX/DYU;

    iget-object v1, p0, LX/DY4;->a:LX/DY6;

    iget-object v1, v1, LX/DY6;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

    .line 2011144
    iget-object v2, v0, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->c:LX/DYT;

    iget-object v3, v0, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v3, v3, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->k:Ljava/lang/String;

    .line 2011145
    iget-object v4, v2, LX/DYT;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->a()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/DYS;->MEMBER_REQUEST_BLOCKED:LX/DYS;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2011146
    new-instance v4, LX/4G0;

    invoke-direct {v4}, LX/4G0;-><init>()V

    iget-object v5, v2, LX/DYT;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/4G0;->a(Ljava/lang/String;)LX/4G0;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/4G0;->b(Ljava/lang/String;)LX/4G0;

    move-result-object v4

    const-string v5, "BLOCK"

    .line 2011147
    const-string v6, "consequence"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2011148
    move-object v4, v4

    .line 2011149
    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->a()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4G0;->c(Ljava/lang/String;)LX/4G0;

    move-result-object v4

    const-string v5, "unknown"

    invoke-virtual {v4, v5}, LX/4G0;->d(Ljava/lang/String;)LX/4G0;

    move-result-object v4

    .line 2011150
    invoke-static {}, LX/DZ1;->b()LX/DZ0;

    move-result-object v5

    .line 2011151
    const-string v6, "input"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2011152
    iget-object v4, v2, LX/DYT;->d:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2011153
    new-instance v5, LX/DYO;

    invoke-direct {v5, v2, v3, v1}, LX/DYO;-><init>(LX/DYT;Ljava/lang/String;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;)V

    iget-object v6, v2, LX/DYT;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2011154
    iget-object v2, v0, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->g:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v3, v0, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v3, v3, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->j:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    iget-object v4, v0, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v4, v4, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->i:LX/0Px;

    iget-object v5, v0, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v5, v5, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->c:LX/DYT;

    const/4 v6, 0x0

    iget-object v7, v0, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-boolean v7, v7, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->h:Z

    invoke-virtual/range {v2 .. v7}, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->a(Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;LX/0Px;LX/DYT;ZZ)V

    .line 2011155
    return-void
.end method
