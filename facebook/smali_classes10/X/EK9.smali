.class public final LX/EK9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;

.field private final b:LX/D0L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/3ag;

.field public final d:I

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/3ag;I)V
    .locals 6

    .prologue
    .line 2106243
    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, LX/EK9;-><init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/D0L;LX/3ag;IZ)V

    .line 2106244
    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/3ag;IZ)V
    .locals 6

    .prologue
    .line 2106257
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/EK9;-><init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/D0L;LX/3ag;IZ)V

    .line 2106258
    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/D0L;LX/3ag;I)V
    .locals 6

    .prologue
    .line 2106255
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LX/EK9;-><init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/D0L;LX/3ag;IZ)V

    .line 2106256
    return-void
.end method

.method private constructor <init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/D0L;LX/3ag;IZ)V
    .locals 0

    .prologue
    .line 2106248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2106249
    iput-object p1, p0, LX/EK9;->a:Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;

    .line 2106250
    iput-object p2, p0, LX/EK9;->b:LX/D0L;

    .line 2106251
    iput-object p3, p0, LX/EK9;->c:LX/3ag;

    .line 2106252
    iput p4, p0, LX/EK9;->d:I

    .line 2106253
    iput-boolean p5, p0, LX/EK9;->e:Z

    .line 2106254
    return-void
.end method

.method public static synthetic a(LX/EK9;)Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;
    .locals 1

    .prologue
    .line 2106247
    iget-object v0, p0, LX/EK9;->a:Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;

    return-object v0
.end method

.method public static synthetic d(LX/EK9;)LX/D0L;
    .locals 1

    .prologue
    .line 2106246
    iget-object v0, p0, LX/EK9;->b:LX/D0L;

    return-object v0
.end method

.method public static synthetic e(LX/EK9;)Z
    .locals 1

    .prologue
    .line 2106245
    iget-boolean v0, p0, LX/EK9;->e:Z

    return v0
.end method
