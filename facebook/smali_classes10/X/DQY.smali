.class public LX/DQY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/1g8;


# direct methods
.method public constructor <init>(LX/0ad;LX/1g8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1994719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1994720
    iput-object p1, p0, LX/DQY;->a:LX/0ad;

    .line 1994721
    iput-object p2, p0, LX/DQY;->b:LX/1g8;

    .line 1994722
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1994723
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1994724
    const-string v2, "group_feed_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1994725
    const-string v3, "source"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1994726
    const-string v4, "enableFlaggedPostGroups"

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1994727
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1994728
    const-string v5, "propertyToUpdate"

    const-string v6, "reported"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1994729
    const-string v5, "group"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1994730
    const-string v5, "source"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1994731
    const-string v3, "enableFlaggedPost"

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1994732
    iget-object v0, p0, LX/DQY;->b:LX/1g8;

    .line 1994733
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "admin_panel_reported_posts_view"

    invoke-direct {v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "reported_posts_admin"

    .line 1994734
    iput-object v3, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1994735
    move-object v1, v1

    .line 1994736
    const-string v3, "group_id"

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1994737
    iget-object v3, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v3, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1994738
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v1, "/group_reported_posts"

    .line 1994739
    iput-object v1, v0, LX/98r;->a:Ljava/lang/String;

    .line 1994740
    move-object v0, v0

    .line 1994741
    iput-object v4, v0, LX/98r;->f:Landroid/os/Bundle;

    .line 1994742
    move-object v0, v0

    .line 1994743
    const-string v1, "GroupsReportedPostsRoute"

    .line 1994744
    iput-object v1, v0, LX/98r;->b:Ljava/lang/String;

    .line 1994745
    move-object v0, v0

    .line 1994746
    const-string v1, "reported_posts_admin"

    .line 1994747
    iput-object v1, v0, LX/98r;->g:Ljava/lang/String;

    .line 1994748
    move-object v0, v0

    .line 1994749
    const v1, 0x7f083061

    .line 1994750
    iput v1, v0, LX/98r;->d:I

    .line 1994751
    move-object v0, v0

    .line 1994752
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 1994753
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    return-object v0
.end method
