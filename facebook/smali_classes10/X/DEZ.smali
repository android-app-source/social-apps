.class public final LX/DEZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;

.field public final synthetic b:LX/2cr;


# direct methods
.method public constructor <init>(LX/2cr;Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;)V
    .locals 0

    .prologue
    .line 1977158
    iput-object p1, p0, LX/DEZ;->b:LX/2cr;

    iput-object p2, p0, LX/DEZ;->a:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x66e77d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1977159
    iget-object v1, p0, LX/DEZ;->b:LX/2cr;

    iget-object v2, p0, LX/DEZ;->a:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;

    .line 1977160
    invoke-virtual {v2}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->a()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    .line 1977161
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 1977162
    sget-object v6, LX/0ax;->B:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1977163
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1977164
    const-string v4, "event_ref_mechanism"

    sget-object p0, Lcom/facebook/events/common/ActionMechanism;->EVENT_TIPS:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v7, v4, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1977165
    iget-object v4, v1, LX/2cr;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-virtual {v4, v5, v6, v7}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 1977166
    iget-object v4, v1, LX/2cr;->a:LX/2cw;

    sget-object v5, LX/3FC;->EVENT_TIP_CLICK:LX/3FC;

    sget-object v6, LX/2cx;->GPS:LX/2cx;

    invoke-virtual {v2}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->a()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->b()Ljava/lang/String;

    move-result-object v7

    const/4 p0, 0x0

    invoke-virtual {v4, v5, v6, v7, p0}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/String;Z)V

    .line 1977167
    const v1, 0x6720488f

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
