.class public final LX/Dwm;
.super LX/DvC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 0

    .prologue
    .line 2061316
    iput-object p1, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {p0}, LX/DvC;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 2061317
    check-cast p1, LX/DvB;

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2061318
    new-instance v6, Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    sget-object v0, LX/3WA;->USER_INITIATED:LX/3WA;

    iget-object v1, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v1, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->aa:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v6, v0, v1}, Lcom/facebook/photos/base/photos/PhotoFetchInfo;-><init>(LX/3WA;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2061319
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2061320
    iget-boolean v1, v0, LX/Dxa;->a:Z

    move v0, v1

    .line 2061321
    if-eqz v0, :cond_1

    .line 2061322
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p1, LX/DvB;->b:Landroid/net/Uri;

    iget-object v5, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;Z)V

    .line 2061323
    :cond_0
    :goto_0
    return-void

    .line 2061324
    :cond_1
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2061325
    iget-boolean v1, v0, LX/Dxa;->b:Z

    move v0, v1

    .line 2061326
    if-eqz v0, :cond_2

    .line 2061327
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p1, LX/DvB;->b:Landroid/net/Uri;

    iget-object v5, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    move v7, v8

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;Z)V

    goto :goto_0

    .line 2061328
    :cond_2
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Y:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Y:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_3

    .line 2061329
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v1, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    iget-object v2, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v2, v2, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Y:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2061330
    iget-object v3, v2, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-object v2, v3

    .line 2061331
    iget-object v3, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V

    goto :goto_0

    .line 2061332
    :cond_3
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Y:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Y:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_4

    .line 2061333
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v1, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    iget-object v2, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v3, v3, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061334
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2061335
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Landroid/support/v4/app/FragmentActivity;J)V

    goto/16 :goto_0

    .line 2061336
    :cond_4
    iget-object v0, p1, LX/DvB;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_5

    move v7, v8

    .line 2061337
    :cond_5
    if-eqz v7, :cond_6

    .line 2061338
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->o:LX/D3w;

    iget-object v1, p1, LX/DvB;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    iget-object v2, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/04D;->VIDEO_ALBUM_PERMALINK:LX/04D;

    invoke-virtual {v0, v1, v2, v3}, LX/D3w;->a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;)V

    .line 2061339
    :goto_1
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->aj:Z

    if-eqz v0, :cond_0

    .line 2061340
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-object v1, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-wide v2, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ai:J

    const-string v1, "album_detail_view"

    invoke-virtual {v0, v2, v3, v1, v7}, LX/9XE;->a(JLjava/lang/String;Z)V

    goto/16 :goto_0

    .line 2061341
    :cond_6
    iget-object v0, p0, LX/Dwm;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v1, p1, LX/DvB;->a:Ljava/lang/String;

    iget-object v2, p1, LX/DvB;->b:Landroid/net/Uri;

    invoke-static {v0, v1, v2}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->a$redex0(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_1
.end method
