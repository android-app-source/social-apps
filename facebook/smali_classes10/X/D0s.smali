.class public LX/D0s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1Vu;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Vv;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/D0i;

.field public final d:LX/D0m;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Vu;LX/0Ot;LX/D0i;LX/D0m;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Vu;",
            "LX/0Ot",
            "<",
            "LX/1Vv;",
            ">;",
            "LX/D0i;",
            "LX/D0m;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1956093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1956094
    iput-object p2, p0, LX/D0s;->b:LX/0Ot;

    .line 1956095
    iput-object p1, p0, LX/D0s;->a:LX/1Vu;

    .line 1956096
    iput-object p5, p0, LX/D0s;->e:LX/0Ot;

    .line 1956097
    iput-object p3, p0, LX/D0s;->c:LX/D0i;

    .line 1956098
    iput-object p4, p0, LX/D0s;->d:LX/D0m;

    .line 1956099
    return-void
.end method

.method public static a(LX/0QB;)LX/D0s;
    .locals 9

    .prologue
    .line 1956100
    const-class v1, LX/D0s;

    monitor-enter v1

    .line 1956101
    :try_start_0
    sget-object v0, LX/D0s;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1956102
    sput-object v2, LX/D0s;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1956103
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1956104
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1956105
    new-instance v3, LX/D0s;

    invoke-static {v0}, LX/1Vu;->a(LX/0QB;)LX/1Vu;

    move-result-object v4

    check-cast v4, LX/1Vu;

    const/16 v5, 0x11f1

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/D0i;->a(LX/0QB;)LX/D0i;

    move-result-object v6

    check-cast v6, LX/D0i;

    invoke-static {v0}, LX/D0m;->a(LX/0QB;)LX/D0m;

    move-result-object v7

    check-cast v7, LX/D0m;

    const/16 v8, 0xafd

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/D0s;-><init>(LX/1Vu;LX/0Ot;LX/D0i;LX/D0m;LX/0Ot;)V

    .line 1956106
    move-object v0, v3

    .line 1956107
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1956108
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D0s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1956109
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1956110
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
