.class public LX/Cyx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Cyx;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lB;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1953915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1953916
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953917
    iput-object v0, p0, LX/Cyx;->a:LX/0Ot;

    .line 1953918
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953919
    iput-object v0, p0, LX/Cyx;->b:LX/0Ot;

    .line 1953920
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cyx;->c:Ljava/util/EnumMap;

    .line 1953921
    return-void
.end method

.method public static a(LX/0QB;)LX/Cyx;
    .locals 5

    .prologue
    .line 1953922
    sget-object v0, LX/Cyx;->d:LX/Cyx;

    if-nez v0, :cond_1

    .line 1953923
    const-class v1, LX/Cyx;

    monitor-enter v1

    .line 1953924
    :try_start_0
    sget-object v0, LX/Cyx;->d:LX/Cyx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1953925
    if-eqz v2, :cond_0

    .line 1953926
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1953927
    new-instance v3, LX/Cyx;

    invoke-direct {v3}, LX/Cyx;-><init>()V

    .line 1953928
    const/16 v4, 0x2ba

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xdf4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1953929
    iput-object v4, v3, LX/Cyx;->a:LX/0Ot;

    iput-object p0, v3, LX/Cyx;->b:LX/0Ot;

    .line 1953930
    move-object v0, v3

    .line 1953931
    sput-object v0, LX/Cyx;->d:LX/Cyx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1953932
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1953933
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1953934
    :cond_1
    sget-object v0, LX/Cyx;->d:LX/Cyx;

    return-object v0

    .line 1953935
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1953936
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
