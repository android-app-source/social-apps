.class public LX/DdY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field public final a:LX/2Og;

.field private final b:LX/6fA;

.field private final c:LX/2Ou;

.field private final d:LX/0c4;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019849
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/DdY;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Og;LX/6fA;LX/2Ou;LX/0c4;LX/0Or;)V
    .locals 0
    .param p4    # LX/0c4;
        .annotation runtime Lcom/facebook/messages/ipc/peer/MessageNotificationPeer;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Og;",
            "LX/6fA;",
            "LX/2Ou;",
            "Lcom/facebook/multiprocess/peer/state/StatefulPeerManager;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019843
    iput-object p1, p0, LX/DdY;->a:LX/2Og;

    .line 2019844
    iput-object p2, p0, LX/DdY;->b:LX/6fA;

    .line 2019845
    iput-object p3, p0, LX/DdY;->c:LX/2Ou;

    .line 2019846
    iput-object p4, p0, LX/DdY;->d:LX/0c4;

    .line 2019847
    iput-object p5, p0, LX/DdY;->e:LX/0Or;

    .line 2019848
    return-void
.end method

.method public static a(LX/0QB;)LX/DdY;
    .locals 13

    .prologue
    .line 2019801
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2019802
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2019803
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2019804
    if-nez v1, :cond_0

    .line 2019805
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2019806
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2019807
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2019808
    sget-object v1, LX/DdY;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2019809
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2019810
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2019811
    :cond_1
    if-nez v1, :cond_4

    .line 2019812
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2019813
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2019814
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2019815
    new-instance v7, LX/DdY;

    invoke-static {v0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v8

    check-cast v8, LX/2Og;

    invoke-static {v0}, LX/6fA;->a(LX/0QB;)LX/6fA;

    move-result-object v9

    check-cast v9, LX/6fA;

    invoke-static {v0}, LX/2Ou;->a(LX/0QB;)LX/2Ou;

    move-result-object v10

    check-cast v10, LX/2Ou;

    invoke-static {v0}, LX/0c4;->a(LX/0QB;)LX/0c4;

    move-result-object v11

    check-cast v11, LX/0c4;

    const/16 v12, 0x12cd

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct/range {v7 .. v12}, LX/DdY;-><init>(LX/2Og;LX/6fA;LX/2Ou;LX/0c4;LX/0Or;)V

    .line 2019816
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2019817
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2019818
    if-nez v1, :cond_2

    .line 2019819
    sget-object v0, LX/DdY;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdY;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2019820
    :goto_1
    if-eqz v0, :cond_3

    .line 2019821
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2019822
    :goto_3
    check-cast v0, LX/DdY;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2019823
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2019824
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2019825
    :catchall_1
    move-exception v0

    .line 2019826
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2019827
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2019828
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2019829
    :cond_2
    :try_start_8
    sget-object v0, LX/DdY;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdY;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2019830
    iget-wide v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 2019831
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_1

    .line 2019832
    iget-object v0, p0, LX/DdY;->c:LX/2Ou;

    invoke-virtual {v0, p1}, LX/2Ou;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    .line 2019833
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->c:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    move v0, v2

    .line 2019834
    :goto_0
    return v0

    :cond_0
    move v0, v3

    .line 2019835
    goto :goto_0

    .line 2019836
    :cond_1
    iget-object v0, p0, LX/DdY;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2019837
    iget-object v5, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v8

    move v4, v3

    :goto_1
    if-ge v4, v8, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2019838
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v9

    invoke-static {v0, v9}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    iget-wide v10, v1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->c:J

    cmp-long v1, v10, v6

    if-gez v1, :cond_2

    move v0, v3

    .line 2019839
    goto :goto_0

    .line 2019840
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2019841
    goto :goto_0
.end method
