.class public final LX/EKD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;)V
    .locals 0

    .prologue
    .line 2106391
    iput-object p1, p0, LX/EKD;->a:Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x7bc6a2e5

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2106392
    iget-object v1, p0, LX/EKD;->a:Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;

    .line 2106393
    iget-object v3, v1, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->c:Landroid/content/Context;

    new-instance v4, LX/8AA;

    sget-object p0, LX/8AB;->SEARCH:LX/8AB;

    invoke-direct {v4, p0}, LX/8AA;-><init>(LX/8AB;)V

    sget-object p0, LX/21D;->SEARCH:LX/21D;

    const-string p1, "keywordSearchPhoto"

    invoke-static {p0, p1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p0

    .line 2106394
    iput-object p0, v4, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2106395
    move-object v4, v4

    .line 2106396
    invoke-virtual {v4}, LX/8AA;->p()LX/8AA;

    move-result-object v4

    invoke-virtual {v4}, LX/8AA;->q()LX/8AA;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v3

    .line 2106397
    iget-object v4, v1, Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;->i:LX/1ay;

    .line 2106398
    iget-object p0, v4, LX/1ay;->c:LX/0id;

    iget-object p1, v4, LX/1ay;->a:Landroid/content/Context;

    const-string v1, "ComposerIntentLauncher"

    invoke-virtual {p0, p1, v1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2106399
    iget-object p0, v4, LX/1ay;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/1ay;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object p1

    iget-object v1, v4, LX/1ay;->a:Landroid/content/Context;

    invoke-interface {p0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2106400
    const v1, 0x521ba2cc

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
