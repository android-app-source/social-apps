.class public final LX/EqQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)V
    .locals 0

    .prologue
    .line 2171505
    iput-object p1, p0, LX/EqQ;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2171506
    iget-object v0, p0, LX/EqQ;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    const v1, 0x7f0d0f2a

    invoke-static {v0, v1}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->a(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2171507
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2171508
    iget-object v0, p0, LX/EqQ;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->r:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2171509
    return-void
.end method

.method public final a(LX/0Px;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2171510
    iget-object v0, p0, LX/EqQ;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->t:LX/ErH;

    .line 2171511
    iget-object v1, v0, LX/ErH;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2171512
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2171513
    iget-object v0, p0, LX/EqQ;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->t:LX/ErH;

    .line 2171514
    iget-boolean v1, v0, LX/ErH;->a:Z

    if-eq v1, p2, :cond_0

    .line 2171515
    iput-boolean p2, v0, LX/ErH;->a:Z

    .line 2171516
    invoke-static {v0}, LX/ErH;->d(LX/ErH;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1OM;->i_(I)V

    .line 2171517
    :cond_0
    iget-object v0, p0, LX/EqQ;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->r:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2171518
    iget-object v0, p0, LX/EqQ;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2171519
    return-void
.end method
