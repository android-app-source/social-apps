.class public LX/EoI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/EoI;


# instance fields
.field private final a:LX/0hB;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0hB;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2168105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2168106
    iput-object p1, p0, LX/EoI;->a:LX/0hB;

    .line 2168107
    iput-object p2, p0, LX/EoI;->b:Landroid/content/res/Resources;

    .line 2168108
    return-void
.end method

.method public static a(LX/0QB;)LX/EoI;
    .locals 5

    .prologue
    .line 2168091
    sget-object v0, LX/EoI;->c:LX/EoI;

    if-nez v0, :cond_1

    .line 2168092
    const-class v1, LX/EoI;

    monitor-enter v1

    .line 2168093
    :try_start_0
    sget-object v0, LX/EoI;->c:LX/EoI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2168094
    if-eqz v2, :cond_0

    .line 2168095
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2168096
    new-instance p0, LX/EoI;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, LX/EoI;-><init>(LX/0hB;Landroid/content/res/Resources;)V

    .line 2168097
    move-object v0, p0

    .line 2168098
    sput-object v0, LX/EoI;->c:LX/EoI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2168099
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2168100
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2168101
    :cond_1
    sget-object v0, LX/EoI;->c:LX/EoI;

    return-object v0

    .line 2168102
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2168103
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 2168104
    iget-object v0, p0, LX/EoI;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->f()I

    move-result v0

    iget-object v1, p0, LX/EoI;->b:Landroid/content/res/Resources;

    const v2, 0x7f0b220f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method
