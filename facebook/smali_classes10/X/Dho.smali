.class public final synthetic LX/Dho;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2031351
    invoke-static {}, LX/DhN;->values()[LX/DhN;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Dho;->c:[I

    :try_start_0
    sget-object v0, LX/Dho;->c:[I

    sget-object v1, LX/DhN;->VIDEO:LX/DhN;

    invoke-virtual {v1}, LX/DhN;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_0
    :try_start_1
    sget-object v0, LX/Dho;->c:[I

    sget-object v1, LX/DhN;->PHOTO:LX/DhN;

    invoke-virtual {v1}, LX/DhN;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_1
    :try_start_2
    sget-object v0, LX/Dho;->c:[I

    sget-object v1, LX/DhN;->STICKER:LX/DhN;

    invoke-virtual {v1}, LX/DhN;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_2
    :try_start_3
    sget-object v0, LX/Dho;->c:[I

    sget-object v1, LX/DhN;->TEXT:LX/DhN;

    invoke-virtual {v1}, LX/DhN;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    .line 2031352
    :goto_3
    invoke-static {}, LX/2MK;->values()[LX/2MK;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Dho;->b:[I

    :try_start_4
    sget-object v0, LX/Dho;->b:[I

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    invoke-virtual {v1}, LX/2MK;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_4
    :try_start_5
    sget-object v0, LX/Dho;->b:[I

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    invoke-virtual {v1}, LX/2MK;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    .line 2031353
    :goto_5
    invoke-static {}, LX/6eh;->values()[LX/6eh;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Dho;->a:[I

    :try_start_6
    sget-object v0, LX/Dho;->a:[I

    sget-object v1, LX/6eh;->PHOTOS:LX/6eh;

    invoke-virtual {v1}, LX/6eh;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_6
    :try_start_7
    sget-object v0, LX/Dho;->a:[I

    sget-object v1, LX/6eh;->NORMAL:LX/6eh;

    invoke-virtual {v1}, LX/6eh;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_7
    :try_start_8
    sget-object v0, LX/Dho;->a:[I

    sget-object v1, LX/6eh;->VIDEO_CLIP:LX/6eh;

    invoke-virtual {v1}, LX/6eh;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_8
    :try_start_9
    sget-object v0, LX/Dho;->a:[I

    sget-object v1, LX/6eh;->STICKER:LX/6eh;

    invoke-virtual {v1}, LX/6eh;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_9
    return-void

    :catch_0
    goto :goto_9

    :catch_1
    goto :goto_8

    :catch_2
    goto :goto_7

    :catch_3
    goto :goto_6

    :catch_4
    goto :goto_5

    :catch_5
    goto :goto_4

    :catch_6
    goto :goto_3

    :catch_7
    goto :goto_2

    :catch_8
    goto :goto_1

    :catch_9
    goto :goto_0
.end method
