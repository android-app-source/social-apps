.class public LX/Ckq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:LX/0p3;

.field public final d:LX/Ckw;

.field public final e:LX/0So;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Ckp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Ckw;LX/8bK;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1931516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931517
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Ckq;->f:Ljava/util/Map;

    .line 1931518
    iput-object p2, p0, LX/Ckq;->d:LX/Ckw;

    .line 1931519
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, LX/Ckq;->a:I

    .line 1931520
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, LX/Ckq;->b:I

    .line 1931521
    invoke-virtual {p3}, LX/8bK;->a()LX/0p3;

    move-result-object v0

    iput-object v0, p0, LX/Ckq;->c:LX/0p3;

    .line 1931522
    iput-object p4, p0, LX/Ckq;->e:LX/0So;

    .line 1931523
    return-void
.end method

.method public static a(LX/0QB;)LX/Ckq;
    .locals 7

    .prologue
    .line 1931505
    const-class v1, LX/Ckq;

    monitor-enter v1

    .line 1931506
    :try_start_0
    sget-object v0, LX/Ckq;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1931507
    sput-object v2, LX/Ckq;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1931508
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931509
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1931510
    new-instance p0, LX/Ckq;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v4

    check-cast v4, LX/Ckw;

    invoke-static {v0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v5

    check-cast v5, LX/8bK;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Ckq;-><init>(Landroid/content/Context;LX/Ckw;LX/8bK;LX/0So;)V

    .line 1931511
    move-object v0, p0

    .line 1931512
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1931513
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ckq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1931514
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1931515
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;IILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;ZZ)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 1931478
    iget-object v0, p0, LX/Ckq;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckp;

    .line 1931479
    if-nez v0, :cond_0

    .line 1931480
    new-instance v2, LX/Ckp;

    invoke-direct {v2, p0, p2, p3}, LX/Ckp;-><init>(LX/Ckq;II)V

    .line 1931481
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->NON_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-eq p4, v0, :cond_5

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT_ONLY:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-eq p4, v0, :cond_5

    move v0, v1

    .line 1931482
    :goto_0
    iput-boolean v0, v2, LX/Ckp;->b:Z

    .line 1931483
    iget-object v0, p0, LX/Ckq;->f:Ljava/util/Map;

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v2

    .line 1931484
    :cond_0
    iget-boolean v2, v0, LX/Ckp;->h:Z

    if-eqz v2, :cond_1

    iget-boolean v2, v0, LX/Ckp;->i:Z

    if-nez v2, :cond_2

    .line 1931485
    :cond_1
    iget-object v2, p0, LX/Ckq;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 1931486
    iput-wide v2, v0, LX/Ckp;->j:J

    .line 1931487
    :cond_2
    if-eqz p5, :cond_3

    iget-boolean v2, v0, LX/Ckp;->h:Z

    if-nez v2, :cond_3

    .line 1931488
    iput-boolean v1, v0, LX/Ckp;->h:Z

    .line 1931489
    iput-wide v4, v0, LX/Ckp;->e:J

    .line 1931490
    :cond_3
    if-eqz p6, :cond_4

    iget-boolean v2, v0, LX/Ckp;->i:Z

    if-nez v2, :cond_4

    .line 1931491
    iput-boolean v1, v0, LX/Ckp;->i:Z

    .line 1931492
    iput-wide v4, v0, LX/Ckp;->f:J

    .line 1931493
    :cond_4
    return-void

    .line 1931494
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 7

    .prologue
    .line 1931495
    iget-object v0, p0, LX/Ckq;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckp;

    .line 1931496
    if-eqz v0, :cond_0

    iget-boolean v1, v0, LX/Ckp;->i:Z

    if-nez v1, :cond_0

    .line 1931497
    iget-object v1, p0, LX/Ckq;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, v0, LX/Ckp;->j:J

    sub-long/2addr v2, v4

    .line 1931498
    iput-wide v2, v0, LX/Ckp;->f:J

    .line 1931499
    const/4 v1, 0x1

    .line 1931500
    iput-boolean v1, v0, LX/Ckp;->i:Z

    .line 1931501
    iput-object p2, v0, LX/Ckp;->k:Ljava/lang/String;

    .line 1931502
    iput-boolean p3, v0, LX/Ckp;->l:Z

    .line 1931503
    iput-boolean p4, v0, LX/Ckp;->m:Z

    .line 1931504
    :cond_0
    return-void
.end method
