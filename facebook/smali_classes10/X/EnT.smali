.class public LX/EnT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/EnT;


# instance fields
.field private final a:Ljava/lang/Boolean;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/EnS;

.field private final d:LX/EnU;

.field private final e:LX/Emx;

.field private final f:LX/0So;

.field private final g:Landroid/content/pm/PackageManager;

.field private final h:Lcom/facebook/content/SecureContextHelper;

.field private i:J


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;LX/0Ot;LX/EnS;LX/EnU;LX/Emx;LX/0So;Landroid/content/pm/PackageManager;Lcom/facebook/content/SecureContextHelper;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/EnS;",
            "LX/EnU;",
            "LX/Emx;",
            "LX/0So;",
            "Landroid/content/pm/PackageManager;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2167182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167183
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EnT;->i:J

    .line 2167184
    iput-object p1, p0, LX/EnT;->a:Ljava/lang/Boolean;

    .line 2167185
    iput-object p2, p0, LX/EnT;->b:LX/0Ot;

    .line 2167186
    iput-object p3, p0, LX/EnT;->c:LX/EnS;

    .line 2167187
    iput-object p4, p0, LX/EnT;->d:LX/EnU;

    .line 2167188
    iput-object p5, p0, LX/EnT;->e:LX/Emx;

    .line 2167189
    iput-object p6, p0, LX/EnT;->f:LX/0So;

    .line 2167190
    iput-object p7, p0, LX/EnT;->g:Landroid/content/pm/PackageManager;

    .line 2167191
    iput-object p8, p0, LX/EnT;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2167192
    return-void
.end method

.method private a(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167164
    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v1

    .line 2167165
    invoke-virtual {v1}, LX/0Rf;->size()I

    move-result v0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 2167166
    :goto_0
    return-object p1

    .line 2167167
    :cond_0
    iget-object v0, p0, LX/EnT;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "entity_cards_launcher_bad_parameters"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "List of initial entity IDs contained duplicates: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2167168
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/EnT;
    .locals 12

    .prologue
    .line 2167169
    sget-object v0, LX/EnT;->j:LX/EnT;

    if-nez v0, :cond_1

    .line 2167170
    const-class v1, LX/EnT;

    monitor-enter v1

    .line 2167171
    :try_start_0
    sget-object v0, LX/EnT;->j:LX/EnT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2167172
    if-eqz v2, :cond_0

    .line 2167173
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2167174
    new-instance v3, LX/EnT;

    invoke-static {v0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/EnS;->a(LX/0QB;)LX/EnS;

    move-result-object v6

    check-cast v6, LX/EnS;

    invoke-static {v0}, LX/EnU;->a(LX/0QB;)LX/EnU;

    move-result-object v7

    check-cast v7, LX/EnU;

    invoke-static {v0}, LX/Emx;->a(LX/0QB;)LX/Emx;

    move-result-object v8

    check-cast v8, LX/Emx;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v9

    check-cast v9, LX/0So;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v10

    check-cast v10, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v3 .. v11}, LX/EnT;-><init>(Ljava/lang/Boolean;LX/0Ot;LX/EnS;LX/EnU;LX/Emx;LX/0So;Landroid/content/pm/PackageManager;Lcom/facebook/content/SecureContextHelper;)V

    .line 2167175
    move-object v0, v3

    .line 2167176
    sput-object v0, LX/EnT;->j:LX/EnT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2167177
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2167178
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2167179
    :cond_1
    sget-object v0, LX/EnT;->j:LX/EnT;

    return-object v0

    .line 2167180
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2167181
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2167162
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, LX/EnT;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Integer;)V

    .line 2167163
    return-void
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Integer;)V
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2167144
    invoke-virtual {p0, p2}, LX/EnT;->a(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Entity Cards are not allowed on this device"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2167145
    iget-object v0, p0, LX/EnT;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 2167146
    iget-wide v2, p0, LX/EnT;->i:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 2167147
    :goto_0
    return-void

    .line 2167148
    :cond_0
    iput-wide v0, p0, LX/EnT;->i:J

    .line 2167149
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2167150
    iget-object v0, p0, LX/EnT;->e:LX/Emx;

    invoke-virtual {v0, v5, p2, p3, p5}, LX/Emx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2167151
    iget-object v0, p0, LX/EnT;->e:LX/Emx;

    sget-object v1, LX/Emw;->LAUNCH_ENTITY_CARD:LX/Emw;

    invoke-virtual {v0, v1}, LX/Emx;->a(LX/Emw;)V

    .line 2167152
    new-instance v7, Landroid/content/Intent;

    const-class v0, Lcom/facebook/entitycards/intent/EntityCardsActivity;

    invoke-direct {v7, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2167153
    new-instance v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;

    invoke-direct {p0, p4}, LX/EnT;->a(LX/0Px;)LX/0Px;

    move-result-object v3

    move-object v1, p2

    move-object v2, p3

    move-object v4, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/entitycards/intent/EntityCardsParameters;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2167154
    const-string v1, "entity_cards_fragment_parameters"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2167155
    new-instance v0, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    .line 2167156
    const-string v1, "entity_cards_config_extras_uuid"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2167157
    iget-object v1, p0, LX/EnT;->d:LX/EnU;

    invoke-virtual {v1, v0, p7}, LX/EnU;->a(Landroid/os/ParcelUuid;Landroid/os/Bundle;)V

    .line 2167158
    if-nez p8, :cond_1

    .line 2167159
    iget-object v0, p0, LX/EnT;->h:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v7, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2167160
    :goto_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0

    .line 2167161
    :cond_1
    iget-object v0, p0, LX/EnT;->h:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual/range {p8 .. p8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v7, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2167140
    iget-object v0, p0, LX/EnT;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EnT;->g:Landroid/content/pm/PackageManager;

    const-string v1, "android.hardware.screen.portrait"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167141
    iget-object v0, p0, LX/EnT;->c:LX/EnS;

    invoke-virtual {v0, p1}, LX/EnS;->a(Ljava/lang/String;)LX/EoA;

    move-result-object v0

    .line 2167142
    invoke-interface {v0}, LX/EoA;->b()LX/Enm;

    move-result-object v0

    invoke-interface {v0}, LX/Enm;->c()Z

    move-result v0

    move v0, v0

    .line 2167143
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
