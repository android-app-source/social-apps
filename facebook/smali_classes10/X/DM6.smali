.class public final enum LX/DM6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DM6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DM6;

.field public static final enum ADD_MEMBERS:LX/DM6;

.field public static final enum CHOOSE_PURPOSE:LX/DM6;

.field public static final enum COMPLETE:LX/DM6;

.field public static final enum GROUP_SETUP:LX/DM6;

.field public static final enum PHOTO_FLOW_CAMERA:LX/DM6;

.field public static final enum PHOTO_FLOW_GALLERY:LX/DM6;

.field public static final enum PHOTO_FLOW_SEARCH:LX/DM6;

.field public static final enum REVIEW:LX/DM6;

.field public static final enum SELECT_PRIVACY:LX/DM6;

.field public static final enum UNKNOWN:LX/DM6;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1989650
    new-instance v0, LX/DM6;

    const-string v1, "CHOOSE_PURPOSE"

    invoke-direct {v0, v1, v3}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->CHOOSE_PURPOSE:LX/DM6;

    .line 1989651
    new-instance v0, LX/DM6;

    const-string v1, "GROUP_SETUP"

    invoke-direct {v0, v1, v4}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->GROUP_SETUP:LX/DM6;

    .line 1989652
    new-instance v0, LX/DM6;

    const-string v1, "SELECT_PRIVACY"

    invoke-direct {v0, v1, v5}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->SELECT_PRIVACY:LX/DM6;

    .line 1989653
    new-instance v0, LX/DM6;

    const-string v1, "PHOTO_FLOW_CAMERA"

    invoke-direct {v0, v1, v6}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->PHOTO_FLOW_CAMERA:LX/DM6;

    .line 1989654
    new-instance v0, LX/DM6;

    const-string v1, "PHOTO_FLOW_GALLERY"

    invoke-direct {v0, v1, v7}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->PHOTO_FLOW_GALLERY:LX/DM6;

    .line 1989655
    new-instance v0, LX/DM6;

    const-string v1, "PHOTO_FLOW_SEARCH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->PHOTO_FLOW_SEARCH:LX/DM6;

    .line 1989656
    new-instance v0, LX/DM6;

    const-string v1, "ADD_MEMBERS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->ADD_MEMBERS:LX/DM6;

    .line 1989657
    new-instance v0, LX/DM6;

    const-string v1, "REVIEW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->REVIEW:LX/DM6;

    .line 1989658
    new-instance v0, LX/DM6;

    const-string v1, "COMPLETE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->COMPLETE:LX/DM6;

    .line 1989659
    new-instance v0, LX/DM6;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/DM6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM6;->UNKNOWN:LX/DM6;

    .line 1989660
    const/16 v0, 0xa

    new-array v0, v0, [LX/DM6;

    sget-object v1, LX/DM6;->CHOOSE_PURPOSE:LX/DM6;

    aput-object v1, v0, v3

    sget-object v1, LX/DM6;->GROUP_SETUP:LX/DM6;

    aput-object v1, v0, v4

    sget-object v1, LX/DM6;->SELECT_PRIVACY:LX/DM6;

    aput-object v1, v0, v5

    sget-object v1, LX/DM6;->PHOTO_FLOW_CAMERA:LX/DM6;

    aput-object v1, v0, v6

    sget-object v1, LX/DM6;->PHOTO_FLOW_GALLERY:LX/DM6;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DM6;->PHOTO_FLOW_SEARCH:LX/DM6;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DM6;->ADD_MEMBERS:LX/DM6;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DM6;->REVIEW:LX/DM6;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DM6;->COMPLETE:LX/DM6;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/DM6;->UNKNOWN:LX/DM6;

    aput-object v2, v0, v1

    sput-object v0, LX/DM6;->$VALUES:[LX/DM6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1989649
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DM6;
    .locals 1

    .prologue
    .line 1989661
    const-class v0, LX/DM6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DM6;

    return-object v0
.end method

.method public static values()[LX/DM6;
    .locals 1

    .prologue
    .line 1989648
    sget-object v0, LX/DM6;->$VALUES:[LX/DM6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DM6;

    return-object v0
.end method
