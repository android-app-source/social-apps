.class public LX/EQ9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/2SY;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2SY;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2SY;",
            "LX/0Ot",
            "<",
            "LX/2Sr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2118294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2118295
    iput-object p1, p0, LX/EQ9;->a:LX/2SY;

    .line 2118296
    iput-object p2, p0, LX/EQ9;->b:LX/0Ot;

    .line 2118297
    return-void
.end method

.method public static a(LX/0QB;)LX/EQ9;
    .locals 5

    .prologue
    .line 2118301
    const-class v1, LX/EQ9;

    monitor-enter v1

    .line 2118302
    :try_start_0
    sget-object v0, LX/EQ9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2118303
    sput-object v2, LX/EQ9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2118304
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2118305
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2118306
    new-instance v4, LX/EQ9;

    invoke-static {v0}, LX/2SY;->a(LX/0QB;)LX/2SY;

    move-result-object v3

    check-cast v3, LX/2SY;

    const/16 p0, 0x11b9

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/EQ9;-><init>(LX/2SY;LX/0Ot;)V

    .line 2118307
    move-object v0, v4

    .line 2118308
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2118309
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EQ9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2118310
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2118311
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SZ;
    .locals 1

    .prologue
    .line 2118298
    invoke-static {p1}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2118299
    iget-object v0, p0, LX/EQ9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SZ;

    .line 2118300
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EQ9;->a:LX/2SY;

    goto :goto_0
.end method
