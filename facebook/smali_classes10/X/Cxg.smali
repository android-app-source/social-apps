.class public LX/Cxg;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Cxf;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951811
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1951812
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Runnable;LX/CzB;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Cz2;LX/CzB;LX/CzB;)LX/Cxf;
    .locals 21

    .prologue
    .line 1951813
    new-instance v1, LX/Cxf;

    const-class v2, LX/1Q6;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/1Q6;

    invoke-static/range {p0 .. p0}, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a(LX/0QB;)Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    move-result-object v10

    check-cast v10, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    const-class v2, LX/1QC;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/1QC;

    invoke-static/range {p0 .. p0}, LX/1QD;->a(LX/0QB;)LX/1QD;

    move-result-object v12

    check-cast v12, LX/1QD;

    invoke-static/range {p0 .. p0}, LX/1QF;->a(LX/0QB;)LX/1QF;

    move-result-object v13

    check-cast v13, LX/1QF;

    invoke-static/range {p0 .. p0}, LX/1QG;->a(LX/0QB;)LX/1QG;

    move-result-object v14

    check-cast v14, LX/1QG;

    const-class v2, LX/CxI;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/CxI;

    const-class v2, LX/CxX;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/CxX;

    const-class v2, LX/CyA;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/CyA;

    const-class v2, LX/CyC;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/CyC;

    invoke-static/range {p0 .. p0}, LX/Cy1;->a(LX/0QB;)LX/Cy1;

    move-result-object v19

    check-cast v19, LX/Cy1;

    const-class v2, LX/Cx7;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/Cx7;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v20}, LX/Cxf;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/CzB;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Cz2;LX/CzB;LX/CzB;LX/1Q6;Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;LX/1QC;LX/1QD;LX/1QF;LX/1QG;LX/CxI;LX/CxX;LX/CyA;LX/CyC;LX/Cy1;LX/Cx7;)V

    .line 1951814
    return-object v1
.end method
