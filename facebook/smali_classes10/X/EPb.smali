.class public final LX/EPb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EPc;

.field public final synthetic b:Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;LX/EPc;)V
    .locals 0

    .prologue
    .line 2117197
    iput-object p1, p0, LX/EPb;->b:Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    iput-object p2, p0, LX/EPb;->a:LX/EPc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x19564105

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2117198
    new-instance v1, LX/EQJ;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/EQJ;-><init>(Landroid/content/Context;)V

    .line 2117199
    iget-object v2, p0, LX/EPb;->a:LX/EPc;

    iget-object v2, v2, LX/EPc;->b:Ljava/lang/String;

    .line 2117200
    iget-object v3, v1, LX/EQJ;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2117201
    iget-object v2, p0, LX/EPb;->b:Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    iget-object v3, p0, LX/EPb;->a:LX/EPc;

    .line 2117202
    new-instance v5, Landroid/text/SpannableStringBuilder;

    iget-object v6, v3, LX/EPc;->b:Ljava/lang/String;

    invoke-direct {v5, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 2117203
    new-instance v6, Landroid/text/SpannableStringBuilder;

    iget-object v7, v2, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->e:Landroid/content/res/Resources;

    const v8, 0x7f0822af

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2117204
    new-instance v7, Landroid/text/style/StyleSpan;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v8, 0x0

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    const/16 p0, 0x21

    invoke-virtual {v6, v7, v8, v9, p0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2117205
    move-object v6, v6

    .line 2117206
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    iget-object v6, v3, LX/EPc;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 2117207
    const/4 v6, 0x1

    invoke-static {v5, v6}, LX/7Gw;->a(Landroid/text/Spannable;I)Z

    .line 2117208
    move-object v2, v5

    .line 2117209
    iget-object v3, v1, LX/EQJ;->l:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2117210
    invoke-virtual {v1, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2117211
    const v1, 0x2db6459c

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
