.class public abstract LX/EWV;
.super LX/EWS;
.source ""

# interfaces
.implements LX/EWU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<BuilderType:",
        "LX/EWV;",
        ">",
        "LX/EWS",
        "<TBuilderType;>;",
        "LX/EWU;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2130043
    invoke-direct {p0}, LX/EWS;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;LX/EYP;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2130207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2130208
    invoke-virtual {p1}, LX/EYP;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2130209
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LX/EYP;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2130210
    :goto_0
    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    .line 2130211
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2130212
    :cond_0
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2130213
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2130214
    :cond_1
    invoke-virtual {p1}, LX/EYP;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static a(LX/EWT;Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWT;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2130192
    invoke-interface {p0}, LX/EWT;->e()LX/EYF;

    move-result-object v0

    invoke-virtual {v0}, LX/EYF;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2130193
    invoke-virtual {v0}, LX/EYP;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0, v0}, LX/EWT;->a(LX/EYP;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2130194
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LX/EYP;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2130195
    :cond_1
    invoke-interface {p0}, LX/EWT;->kb_()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2130196
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    .line 2130197
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 2130198
    invoke-virtual {v1}, LX/EYP;->f()LX/EYN;

    move-result-object v2

    sget-object v3, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v2, v3, :cond_2

    .line 2130199
    invoke-virtual {v1}, LX/EYP;->m()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2130200
    const/4 v2, 0x0

    .line 2130201
    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 2130202
    check-cast v0, LX/EWT;

    add-int/lit8 v3, v2, 0x1

    invoke-static {p1, v1, v2}, LX/EWV;->a(Ljava/lang/String;LX/EYP;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p2}, LX/EWV;->a(LX/EWT;Ljava/lang/String;Ljava/util/List;)V

    move v2, v3

    .line 2130203
    goto :goto_2

    .line 2130204
    :cond_3
    invoke-interface {p0, v1}, LX/EWT;->a(LX/EYP;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2130205
    check-cast v0, LX/EWT;

    const/4 v2, -0x1

    invoke-static {p1, v1, v2}, LX/EWV;->a(Ljava/lang/String;LX/EYP;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, LX/EWV;->a(LX/EWT;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_1

    .line 2130206
    :cond_4
    return-void
.end method

.method private static a(LX/EWU;LX/EYc;LX/EYP;LX/EWU;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWU;",
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;",
            "LX/EYP;",
            "LX/EWU;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2130188
    invoke-static {p0, p1, p2}, LX/EWV;->b(LX/EWU;LX/EYc;LX/EYP;)LX/EWY;

    move-result-object v0

    .line 2130189
    if-eqz v0, :cond_0

    .line 2130190
    invoke-interface {p3, v0}, LX/EWU;->c(LX/EWY;)LX/EWU;

    .line 2130191
    :cond_0
    return-void
.end method

.method private static a(LX/EWU;LX/EYc;LX/EYP;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWU;",
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2130184
    if-eqz p0, :cond_0

    .line 2130185
    invoke-interface {p0, p2, p3}, LX/EWU;->a(LX/EYP;Ljava/lang/Object;)LX/EWU;

    .line 2130186
    :goto_0
    return-void

    .line 2130187
    :cond_0
    invoke-virtual {p1, p2, p3}, LX/EYc;->b(LX/EYP;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/EWU;LX/EYc;LX/EYP;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWU;",
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;",
            "LX/EYP;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2130181
    if-eqz p0, :cond_0

    .line 2130182
    invoke-interface {p0, p2}, LX/EWT;->a(LX/EYP;)Z

    move-result v0

    .line 2130183
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, p2}, LX/EYc;->a(LX/EYP;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/EWd;LX/EZM;LX/EYZ;LX/EYF;LX/EWU;LX/EYc;I)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWd;",
            "LX/EZM;",
            "LX/EYZ;",
            "LX/EYF;",
            "LX/EWU;",
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2130059
    invoke-virtual {p3}, LX/EYF;->d()LX/EXf;

    move-result-object v0

    .line 2130060
    iget-boolean v4, v0, LX/EXf;->messageSetWireFormat_:Z

    move v0, v4

    .line 2130061
    if-eqz v0, :cond_7

    sget v0, LX/EZb;->a:I

    if-ne p6, v0, :cond_7

    .line 2130062
    const/4 v2, 0x0

    .line 2130063
    const/4 v0, 0x0

    move-object v3, v2

    move v4, v0

    move-object v0, v2

    .line 2130064
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/EWd;->a()I

    move-result v5

    .line 2130065
    if-eqz v5, :cond_4

    .line 2130066
    sget v6, LX/EZb;->c:I

    if-ne v5, v6, :cond_1

    .line 2130067
    invoke-virtual {p0}, LX/EWd;->l()I

    move-result v4

    .line 2130068
    if-eqz v4, :cond_0

    .line 2130069
    instance-of v5, p2, LX/EYa;

    if-eqz v5, :cond_0

    move-object v0, p2

    .line 2130070
    check-cast v0, LX/EYa;

    invoke-virtual {v0, p3, v4}, LX/EYa;->a(LX/EYF;I)LX/EYY;

    move-result-object v0

    goto :goto_0

    .line 2130071
    :cond_1
    sget v6, LX/EZb;->d:I

    if-ne v5, v6, :cond_3

    .line 2130072
    if-eqz v4, :cond_2

    .line 2130073
    if-eqz v0, :cond_2

    .line 2130074
    sget-boolean v3, LX/EYZ;->a:Z

    move v3, v3

    .line 2130075
    if-eqz v3, :cond_2

    .line 2130076
    iget-object v5, v0, LX/EYY;->a:LX/EYP;

    .line 2130077
    invoke-static {p4, p5, v5}, LX/EWV;->a(LX/EWU;LX/EYc;LX/EYP;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 2130078
    invoke-static {p4, p5, v5}, LX/EWV;->b(LX/EWU;LX/EYc;LX/EYP;)LX/EWY;

    move-result-object v3

    .line 2130079
    invoke-virtual {v3}, LX/EWY;->s()LX/EWU;

    move-result-object v3

    .line 2130080
    invoke-virtual {p0, v3, p2}, LX/EWd;->a(LX/EWR;LX/EYZ;)V

    .line 2130081
    invoke-interface {v3}, LX/EWU;->h()LX/EWY;

    move-result-object v3

    .line 2130082
    :goto_1
    if-eqz p4, :cond_1c

    .line 2130083
    invoke-interface {p4, v5, v3}, LX/EWU;->b(LX/EYP;Ljava/lang/Object;)LX/EWU;

    .line 2130084
    :goto_2
    move-object v3, v2

    .line 2130085
    goto :goto_0

    .line 2130086
    :cond_2
    invoke-virtual {p0}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    goto :goto_0

    .line 2130087
    :cond_3
    invoke-virtual {p0, v5}, LX/EWd;->b(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2130088
    :cond_4
    sget v2, LX/EZb;->b:I

    invoke-virtual {p0, v2}, LX/EWd;->a(I)V

    .line 2130089
    if-eqz v3, :cond_6

    if-eqz v4, :cond_6

    .line 2130090
    if-eqz v0, :cond_1a

    .line 2130091
    iget-object v4, v0, LX/EYY;->a:LX/EYP;

    .line 2130092
    invoke-static {p4, p5, v4}, LX/EWV;->a(LX/EWU;LX/EYc;LX/EYP;)Z

    move-result v2

    .line 2130093
    if-nez v2, :cond_5

    .line 2130094
    sget-boolean v5, LX/EYZ;->a:Z

    move v5, v5

    .line 2130095
    if-eqz v5, :cond_1e

    .line 2130096
    :cond_5
    if-eqz v2, :cond_1d

    .line 2130097
    invoke-static {p4, p5, v4}, LX/EWV;->b(LX/EWU;LX/EYc;LX/EYP;)LX/EWY;

    move-result-object v2

    .line 2130098
    invoke-virtual {v2}, LX/EWY;->s()LX/EWU;

    move-result-object v2

    .line 2130099
    invoke-interface {v2, v3, p2}, LX/EWU;->b(LX/EWc;LX/EYZ;)LX/EWU;

    .line 2130100
    invoke-interface {v2}, LX/EWU;->h()LX/EWY;

    move-result-object v2

    .line 2130101
    :goto_3
    invoke-static {p4, p5, v4, v2}, LX/EWV;->b(LX/EWU;LX/EYc;LX/EYP;Ljava/lang/Object;)V

    .line 2130102
    :cond_6
    :goto_4
    move v0, v1

    .line 2130103
    :goto_5
    return v0

    .line 2130104
    :cond_7
    and-int/lit8 v0, p6, 0x7

    move v5, v0

    .line 2130105
    ushr-int/lit8 v0, p6, 0x3

    move v6, v0

    .line 2130106
    invoke-virtual {p3, v6}, LX/EYF;->a(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2130107
    instance-of v0, p2, LX/EYa;

    if-eqz v0, :cond_a

    move-object v0, p2

    .line 2130108
    check-cast v0, LX/EYa;

    invoke-virtual {v0, p3, v6}, LX/EYa;->a(LX/EYF;I)LX/EYY;

    move-result-object v4

    .line 2130109
    if-nez v4, :cond_8

    move-object v4, v3

    .line 2130110
    :goto_6
    if-eqz v3, :cond_e

    .line 2130111
    invoke-virtual {v3}, LX/EYP;->j()LX/EZV;

    move-result-object v0

    invoke-static {v0, v2}, LX/EYc;->a(LX/EZV;Z)I

    move-result v0

    if-ne v5, v0, :cond_d

    move v0, v2

    .line 2130112
    :goto_7
    if-eqz v2, :cond_f

    .line 2130113
    invoke-virtual {p1, p6, p0}, LX/EZM;->a(ILX/EWd;)Z

    move-result v0

    goto :goto_5

    .line 2130114
    :cond_8
    iget-object v0, v4, LX/EYY;->a:LX/EYP;

    .line 2130115
    iget-object v3, v4, LX/EYY;->b:LX/EWY;

    .line 2130116
    if-nez v3, :cond_9

    invoke-virtual {v0}, LX/EYP;->f()LX/EYN;

    move-result-object v4

    sget-object v7, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v4, v7, :cond_9

    .line 2130117
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Message-typed extension lacked default instance: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/EYP;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object v4, v3

    move-object v3, v0

    .line 2130118
    goto :goto_6

    :cond_a
    move-object v4, v3

    .line 2130119
    goto :goto_6

    .line 2130120
    :cond_b
    if-eqz p4, :cond_c

    .line 2130121
    iget-object v0, p3, LX/EYF;->d:LX/EYQ;

    iget-object v0, v0, LX/EYQ;->h:LX/EYJ;

    iget-object v0, v0, LX/EYJ;->d:Ljava/util/Map;

    new-instance v4, LX/EYG;

    invoke-direct {v4, p3, v6}, LX/EYG;-><init>(LX/EYE;I)V

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    move-object v0, v0

    .line 2130122
    move-object v4, v3

    move-object v3, v0

    goto :goto_6

    :cond_c
    move-object v4, v3

    .line 2130123
    goto :goto_6

    .line 2130124
    :cond_d
    invoke-virtual {v3}, LX/EYP;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v3}, LX/EYP;->j()LX/EZV;

    move-result-object v0

    invoke-static {v0, v1}, LX/EYc;->a(LX/EZV;Z)I

    move-result v0

    if-ne v5, v0, :cond_e

    move v0, v1

    .line 2130125
    goto :goto_7

    :cond_e
    move v0, v2

    move v2, v1

    .line 2130126
    goto :goto_7

    .line 2130127
    :cond_f
    if-eqz v0, :cond_13

    .line 2130128
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v0

    .line 2130129
    invoke-virtual {p0, v0}, LX/EWd;->c(I)I

    move-result v0

    .line 2130130
    invoke-virtual {v3}, LX/EYP;->j()LX/EZV;

    move-result-object v2

    sget-object v4, LX/EZV;->ENUM:LX/EZV;

    if-ne v2, v4, :cond_11

    .line 2130131
    :goto_8
    invoke-virtual {p0}, LX/EWd;->s()I

    move-result v2

    if-lez v2, :cond_12

    .line 2130132
    invoke-virtual {p0}, LX/EWd;->m()I

    move-result v2

    .line 2130133
    invoke-virtual {v3}, LX/EYP;->u()LX/EYL;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/EYL;->a(I)LX/EYM;

    move-result-object v2

    .line 2130134
    if-nez v2, :cond_10

    move v0, v1

    .line 2130135
    goto/16 :goto_5

    .line 2130136
    :cond_10
    invoke-static {p4, p5, v3, v2}, LX/EWV;->a(LX/EWU;LX/EYc;LX/EYP;Ljava/lang/Object;)V

    goto :goto_8

    .line 2130137
    :cond_11
    :goto_9
    invoke-virtual {p0}, LX/EWd;->s()I

    move-result v2

    if-lez v2, :cond_12

    .line 2130138
    invoke-virtual {v3}, LX/EYP;->j()LX/EZV;

    move-result-object v2

    invoke-static {p0, v2}, LX/EYc;->a(LX/EWd;LX/EZV;)Ljava/lang/Object;

    move-result-object v2

    .line 2130139
    invoke-static {p4, p5, v3, v2}, LX/EWV;->a(LX/EWU;LX/EYc;LX/EYP;Ljava/lang/Object;)V

    goto :goto_9

    .line 2130140
    :cond_12
    invoke-virtual {p0, v0}, LX/EWd;->d(I)V

    :goto_a
    move v0, v1

    .line 2130141
    goto/16 :goto_5

    .line 2130142
    :cond_13
    sget-object v0, LX/EWP;->a:[I

    .line 2130143
    iget-object v2, v3, LX/EYP;->g:LX/EYO;

    move-object v2, v2

    .line 2130144
    invoke-virtual {v2}, LX/EYO;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2130145
    invoke-virtual {v3}, LX/EYP;->j()LX/EZV;

    move-result-object v0

    invoke-static {p0, v0}, LX/EYc;->a(LX/EWd;LX/EZV;)Ljava/lang/Object;

    move-result-object v0

    .line 2130146
    :cond_14
    :goto_b
    invoke-virtual {v3}, LX/EYP;->m()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 2130147
    invoke-static {p4, p5, v3, v0}, LX/EWV;->a(LX/EWU;LX/EYc;LX/EYP;Ljava/lang/Object;)V

    goto :goto_a

    .line 2130148
    :pswitch_0
    if-eqz v4, :cond_16

    .line 2130149
    invoke-virtual {v4}, LX/EWY;->t()LX/EWU;

    move-result-object v0

    .line 2130150
    :goto_c
    invoke-virtual {v3}, LX/EYP;->m()Z

    move-result v2

    if-nez v2, :cond_15

    .line 2130151
    invoke-static {p4, p5, v3, v0}, LX/EWV;->a(LX/EWU;LX/EYc;LX/EYP;LX/EWU;)V

    .line 2130152
    :cond_15
    invoke-virtual {v3}, LX/EYP;->e()I

    move-result v2

    invoke-virtual {p0, v2, v0, p2}, LX/EWd;->a(ILX/EWR;LX/EYZ;)V

    .line 2130153
    invoke-interface {v0}, LX/EWU;->h()LX/EWY;

    move-result-object v0

    goto :goto_b

    .line 2130154
    :cond_16
    invoke-interface {p4, v3}, LX/EWU;->c(LX/EYP;)LX/EWU;

    move-result-object v0

    goto :goto_c

    .line 2130155
    :pswitch_1
    if-eqz v4, :cond_18

    .line 2130156
    invoke-virtual {v4}, LX/EWY;->t()LX/EWU;

    move-result-object v0

    .line 2130157
    :goto_d
    invoke-virtual {v3}, LX/EYP;->m()Z

    move-result v2

    if-nez v2, :cond_17

    .line 2130158
    invoke-static {p4, p5, v3, v0}, LX/EWV;->a(LX/EWU;LX/EYc;LX/EYP;LX/EWU;)V

    .line 2130159
    :cond_17
    invoke-virtual {p0, v0, p2}, LX/EWd;->a(LX/EWR;LX/EYZ;)V

    .line 2130160
    invoke-interface {v0}, LX/EWU;->h()LX/EWY;

    move-result-object v0

    goto :goto_b

    .line 2130161
    :cond_18
    invoke-interface {p4, v3}, LX/EWU;->c(LX/EYP;)LX/EWU;

    move-result-object v0

    goto :goto_d

    .line 2130162
    :pswitch_2
    invoke-virtual {p0}, LX/EWd;->m()I

    move-result v2

    .line 2130163
    invoke-virtual {v3}, LX/EYP;->u()LX/EYL;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/EYL;->a(I)LX/EYM;

    move-result-object v0

    .line 2130164
    if-nez v0, :cond_14

    .line 2130165
    invoke-virtual {p1, v6, v2}, LX/EZM;->a(II)LX/EZM;

    move v0, v1

    .line 2130166
    goto/16 :goto_5

    .line 2130167
    :cond_19
    invoke-static {p4, p5, v3, v0}, LX/EWV;->b(LX/EWU;LX/EYc;LX/EYP;Ljava/lang/Object;)V

    goto :goto_a

    .line 2130168
    :cond_1a
    if-eqz v3, :cond_6

    .line 2130169
    invoke-static {}, LX/EZN;->c()LX/EZN;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/EZN;->a(LX/EWc;)LX/EZN;

    move-result-object v0

    invoke-virtual {v0}, LX/EZN;->a()LX/EZO;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, LX/EZM;->a(ILX/EZO;)LX/EZM;

    goto/16 :goto_4

    .line 2130170
    :cond_1b
    iget-object v3, v0, LX/EYY;->b:LX/EWY;

    invoke-virtual {v3}, LX/EWY;->i()LX/EWZ;

    move-result-object v3

    invoke-virtual {p0, v3, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v3

    check-cast v3, LX/EWY;

    goto/16 :goto_1

    .line 2130171
    :cond_1c
    invoke-virtual {p5, v5, v3}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 2130172
    :cond_1d
    iget-object v2, v0, LX/EYY;->b:LX/EWY;

    invoke-virtual {v2}, LX/EWY;->i()LX/EWZ;

    move-result-object v2

    .line 2130173
    invoke-static {v2, v3, p2}, LX/EWZ;->c(LX/EWZ;LX/EWc;LX/EYZ;)LX/EWW;

    move-result-object v5

    move-object v2, v5

    .line 2130174
    check-cast v2, LX/EWY;

    goto/16 :goto_3

    .line 2130175
    :cond_1e
    new-instance v2, LX/EYu;

    iget-object v5, v0, LX/EYY;->b:LX/EWY;

    invoke-direct {v2, v5, p2, v3}, LX/EYu;-><init>(LX/EWW;LX/EYZ;LX/EWc;)V

    .line 2130176
    if-eqz p4, :cond_20

    .line 2130177
    instance-of v5, p4, LX/EWy;

    if-eqz v5, :cond_1f

    .line 2130178
    invoke-interface {p4, v4, v2}, LX/EWU;->b(LX/EYP;Ljava/lang/Object;)LX/EWU;

    goto/16 :goto_4

    .line 2130179
    :cond_1f
    invoke-virtual {v2}, LX/EYu;->a()LX/EWW;

    move-result-object v2

    invoke-interface {p4, v4, v2}, LX/EWU;->b(LX/EYP;Ljava/lang/Object;)LX/EWU;

    goto/16 :goto_4

    .line 2130180
    :cond_20
    invoke-virtual {p5, v4, v2}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;)V

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/EWU;LX/EYc;LX/EYP;)LX/EWY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWU;",
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;",
            "LX/EYP;",
            ")",
            "Lcom/google/protobuf/Message;"
        }
    .end annotation

    .prologue
    .line 2130056
    if-eqz p0, :cond_0

    .line 2130057
    invoke-interface {p0, p2}, LX/EWT;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    .line 2130058
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, p2}, LX/EYc;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    goto :goto_0
.end method

.method public static b(LX/EWY;)LX/EZL;
    .locals 3

    .prologue
    .line 2130051
    new-instance v0, LX/EZL;

    .line 2130052
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2130053
    const-string v2, ""

    invoke-static {p0, v2, v1}, LX/EWV;->a(LX/EWT;Ljava/lang/String;Ljava/util/List;)V

    .line 2130054
    move-object v1, v1

    .line 2130055
    invoke-direct {v0, v1}, LX/EZL;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public static b(LX/EWU;LX/EYc;LX/EYP;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWU;",
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2130047
    if-eqz p0, :cond_0

    .line 2130048
    invoke-interface {p0, p2, p3}, LX/EWU;->b(LX/EYP;Ljava/lang/Object;)LX/EWU;

    .line 2130049
    :goto_0
    return-void

    .line 2130050
    :cond_0
    invoke-virtual {p1, p2, p3}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private c(LX/EWc;LX/EYZ;)LX/EWV;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWc;",
            "LX/EYZ;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130046
    invoke-super {p0, p1, p2}, LX/EWS;->a(LX/EWc;LX/EYZ;)LX/EWS;

    move-result-object v0

    check-cast v0, LX/EWV;

    return-object v0
.end method

.method private c([B)LX/EWV;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130045
    invoke-super {p0, p1}, LX/EWS;->a([B)LX/EWS;

    move-result-object v0

    check-cast v0, LX/EWV;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWc;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2130044
    invoke-direct {p0, p1, p2}, LX/EWV;->c(LX/EWc;LX/EYZ;)LX/EWV;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWd;)LX/EWS;
    .locals 1

    .prologue
    .line 2130215
    sget-object v0, LX/EYa;->c:LX/EYa;

    move-object v0, v0

    .line 2130216
    invoke-virtual {p0, p1, v0}, LX/EWV;->a(LX/EWd;LX/EYZ;)LX/EWV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a([B)LX/EWS;
    .locals 1

    .prologue
    .line 2130011
    invoke-direct {p0, p1}, LX/EWV;->c([B)LX/EWV;

    move-result-object v0

    return-object v0
.end method

.method public final a([BII)LX/EWS;
    .locals 1

    .prologue
    .line 2130012
    invoke-super {p0, p1, p2, p3}, LX/EWS;->a([BII)LX/EWS;

    move-result-object v0

    check-cast v0, LX/EWV;

    return-object v0
.end method

.method public a(LX/EWY;)LX/EWV;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/Message;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130013
    invoke-interface {p1}, LX/EWT;->e()LX/EYF;

    move-result-object v0

    invoke-virtual {p0}, LX/EWV;->e()LX/EYF;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2130014
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mergeFrom(Message) can only merge messages of the same type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2130015
    :cond_0
    invoke-interface {p1}, LX/EWT;->kb_()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2130016
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    .line 2130017
    invoke-virtual {v1}, LX/EYP;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2130018
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2130019
    invoke-virtual {p0, v1, v2}, LX/EWV;->a(LX/EYP;Ljava/lang/Object;)LX/EWU;

    goto :goto_1

    .line 2130020
    :cond_2
    invoke-virtual {v1}, LX/EYP;->f()LX/EYN;

    move-result-object v2

    sget-object v4, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v2, v4, :cond_4

    .line 2130021
    invoke-virtual {p0, v1}, LX/EWV;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EWY;

    .line 2130022
    invoke-interface {v2}, LX/EWT;->v()LX/EWY;

    move-result-object v4

    if-ne v2, v4, :cond_3

    .line 2130023
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, LX/EWV;->b(LX/EYP;Ljava/lang/Object;)LX/EWU;

    goto :goto_0

    .line 2130024
    :cond_3
    invoke-virtual {v2}, LX/EWY;->t()LX/EWU;

    move-result-object v4

    invoke-interface {v4, v2}, LX/EWU;->c(LX/EWY;)LX/EWU;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    invoke-interface {v2, v0}, LX/EWU;->c(LX/EWY;)LX/EWU;

    move-result-object v0

    invoke-interface {v0}, LX/EWU;->i()LX/EWY;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, LX/EWV;->b(LX/EYP;Ljava/lang/Object;)LX/EWU;

    goto :goto_0

    .line 2130025
    :cond_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, LX/EWV;->b(LX/EYP;Ljava/lang/Object;)LX/EWU;

    goto :goto_0

    .line 2130026
    :cond_5
    invoke-interface {p1}, LX/EWT;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWV;->a(LX/EZQ;)LX/EWV;

    .line 2130027
    return-object p0
.end method

.method public a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWd;",
            "LX/EYZ;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130028
    invoke-virtual {p0}, LX/EWV;->g()LX/EZQ;

    move-result-object v0

    invoke-static {v0}, LX/EZQ;->a(LX/EZQ;)LX/EZM;

    move-result-object v1

    .line 2130029
    :cond_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v6

    .line 2130030
    if-eqz v6, :cond_1

    .line 2130031
    invoke-virtual {p0}, LX/EWV;->e()LX/EYF;

    move-result-object v3

    const/4 v5, 0x0

    move-object v0, p1

    move-object v2, p2

    move-object v4, p0

    invoke-static/range {v0 .. v6}, LX/EWV;->a(LX/EWd;LX/EZM;LX/EYZ;LX/EYF;LX/EWU;LX/EYc;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2130032
    :cond_1
    invoke-virtual {v1}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWV;->b(LX/EZQ;)LX/EWU;

    .line 2130033
    return-object p0
.end method

.method public a(LX/EZQ;)LX/EWV;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EZQ;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130034
    invoke-virtual {p0}, LX/EWV;->g()LX/EZQ;

    move-result-object v0

    invoke-static {v0}, LX/EZQ;->a(LX/EZQ;)LX/EZM;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZM;->a(LX/EZQ;)LX/EZM;

    move-result-object v0

    invoke-virtual {v0}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWV;->b(LX/EZQ;)LX/EWU;

    .line 2130035
    return-object p0
.end method

.method public final synthetic b([B)LX/EWR;
    .locals 1

    .prologue
    .line 2130036
    invoke-direct {p0, p1}, LX/EWV;->c([B)LX/EWV;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2130037
    invoke-virtual {p0, p1, p2}, LX/EWV;->a(LX/EWd;LX/EYZ;)LX/EWV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(LX/EWc;LX/EYZ;)LX/EWU;
    .locals 1

    .prologue
    .line 2130038
    invoke-direct {p0, p1, p2}, LX/EWV;->c(LX/EWc;LX/EYZ;)LX/EWV;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()LX/EWV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation
.end method

.method public synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2130039
    invoke-virtual {p0, p1, p2}, LX/EWV;->a(LX/EWd;LX/EYZ;)LX/EWV;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2130040
    invoke-virtual {p0}, LX/EWV;->b()LX/EWV;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2130041
    invoke-virtual {p0, p1}, LX/EWV;->a(LX/EWY;)LX/EWV;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2130042
    invoke-virtual {p0}, LX/EWV;->b()LX/EWV;

    move-result-object v0

    return-object v0
.end method
