.class public final enum LX/EIT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EIT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EIT;

.field public static final enum DOMINANT_SPEAKER_VIEW:LX/EIT;

.field public static final enum GRID_VIEW:LX/EIT;

.field public static final enum NONE:LX/EIT;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2101553
    new-instance v0, LX/EIT;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/EIT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIT;->NONE:LX/EIT;

    .line 2101554
    new-instance v0, LX/EIT;

    const-string v1, "GRID_VIEW"

    invoke-direct {v0, v1, v3}, LX/EIT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIT;->GRID_VIEW:LX/EIT;

    .line 2101555
    new-instance v0, LX/EIT;

    const-string v1, "DOMINANT_SPEAKER_VIEW"

    invoke-direct {v0, v1, v4}, LX/EIT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    .line 2101556
    const/4 v0, 0x3

    new-array v0, v0, [LX/EIT;

    sget-object v1, LX/EIT;->NONE:LX/EIT;

    aput-object v1, v0, v2

    sget-object v1, LX/EIT;->GRID_VIEW:LX/EIT;

    aput-object v1, v0, v3

    sget-object v1, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    aput-object v1, v0, v4

    sput-object v0, LX/EIT;->$VALUES:[LX/EIT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2101558
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EIT;
    .locals 1

    .prologue
    .line 2101559
    const-class v0, LX/EIT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EIT;

    return-object v0
.end method

.method public static values()[LX/EIT;
    .locals 1

    .prologue
    .line 2101557
    sget-object v0, LX/EIT;->$VALUES:[LX/EIT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EIT;

    return-object v0
.end method
