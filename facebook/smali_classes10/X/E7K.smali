.class public final LX/E7K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E6y;

.field private final b:LX/5sc;

.field private final c:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

.field public d:Z


# direct methods
.method public constructor <init>(LX/E6y;LX/5sc;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;)V
    .locals 1

    .prologue
    .line 2081275
    iput-object p1, p0, LX/E7K;->a:LX/E6y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2081276
    iput-object p2, p0, LX/E7K;->b:LX/5sc;

    .line 2081277
    iget-object v0, p0, LX/E7K;->b:LX/5sc;

    invoke-interface {v0}, LX/5sc;->as_()Z

    move-result v0

    iput-boolean v0, p0, LX/E7K;->d:Z

    .line 2081278
    iput-object p3, p0, LX/E7K;->c:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2081279
    return-void
.end method

.method public static a(LX/E7K;)V
    .locals 4

    .prologue
    .line 2081284
    iget-object v2, p0, LX/E7K;->c:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iget-object v0, p0, LX/E7K;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wM;

    const v3, 0x7f0208fa

    iget-boolean v1, p0, LX/E7K;->d:Z

    if-eqz v1, :cond_0

    const v1, -0xa76f01

    :goto_0
    invoke-virtual {v0, v3, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2081285
    return-void

    .line 2081286
    :cond_0
    const v1, -0x423e37

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    const v1, -0x131399ad

    invoke-static {v0, v8, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 2081280
    iget-boolean v5, p0, LX/E7K;->d:Z

    .line 2081281
    iget-object v0, p0, LX/E7K;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->e:LX/961;

    iget-object v1, p0, LX/E7K;->b:LX/5sc;

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v5, :cond_0

    move v2, v8

    :goto_0
    const-string v4, "reaction_dialog"

    new-instance v9, LX/E7J;

    invoke-direct {v9, p0, v5}, LX/E7J;-><init>(LX/E7K;Z)V

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v9}, LX/961;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V

    .line 2081282
    const v0, -0x540ce37

    invoke-static {v0, v10}, LX/02F;->a(II)V

    return-void

    .line 2081283
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
