.class public final LX/EyZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/friends/model/FriendRequest;

.field public final synthetic c:LX/2iU;


# direct methods
.method public constructor <init>(LX/2iU;JLcom/facebook/friends/model/FriendRequest;)V
    .locals 0

    .prologue
    .line 2186389
    iput-object p1, p0, LX/EyZ;->c:LX/2iU;

    iput-wide p2, p0, LX/EyZ;->a:J

    iput-object p4, p0, LX/EyZ;->b:Lcom/facebook/friends/model/FriendRequest;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2186392
    iget-object v0, p0, LX/EyZ;->c:LX/2iU;

    iget-wide v2, p0, LX/EyZ;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/2iU;->a$redex0(LX/2iU;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2186393
    iget-object v0, p0, LX/EyZ;->c:LX/2iU;

    iget-object v1, p0, LX/EyZ;->b:Lcom/facebook/friends/model/FriendRequest;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1}, LX/2iU;->a$redex0(LX/2iU;Lcom/facebook/friends/model/FriendRequest;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Ljava/lang/Throwable;)V

    .line 2186394
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186390
    iget-object v0, p0, LX/EyZ;->c:LX/2iU;

    iget-wide v2, p0, LX/EyZ;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/2iU;->a$redex0(LX/2iU;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2186391
    return-void
.end method
