.class public LX/Eam;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/DoQ;

.field private final c:LX/DoP;

.field private final d:LX/DoR;

.field private final e:LX/DoO;

.field private final f:LX/Eap;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2142136
    const-class v0, LX/Eam;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Eam;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/DoQ;LX/DoP;LX/DoR;LX/DoO;LX/Eap;)V
    .locals 0

    .prologue
    .line 2142276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142277
    iput-object p1, p0, LX/Eam;->b:LX/DoQ;

    .line 2142278
    iput-object p2, p0, LX/Eam;->c:LX/DoP;

    .line 2142279
    iput-object p3, p0, LX/Eam;->d:LX/DoR;

    .line 2142280
    iput-object p4, p0, LX/Eam;->e:LX/DoO;

    .line 2142281
    iput-object p5, p0, LX/Eam;->f:LX/Eap;

    .line 2142282
    return-void
.end method

.method public constructor <init>(LX/DoS;LX/Eap;)V
    .locals 6

    .prologue
    .line 2142274
    move-object v0, p0

    move-object v1, p1

    move-object v2, p1

    move-object v3, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/Eam;-><init>(LX/DoQ;LX/DoP;LX/DoR;LX/DoO;LX/Eap;)V

    .line 2142275
    return-void
.end method

.method private b(LX/Ebh;LX/EbA;)LX/Ecs;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ebh;",
            "LX/EbA;",
            ")",
            "LX/Ecs",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2142216
    iget v0, p2, LX/EbA;->a:I

    move v0, v0

    .line 2142217
    iget-object v1, p2, LX/EbA;->e:LX/Eat;

    move-object v1, v1

    .line 2142218
    invoke-virtual {v1}, LX/Eat;->a()[B

    move-result-object v1

    const/4 v3, 0x1

    .line 2142219
    iget-object v2, p1, LX/Ebh;->a:LX/Ebj;

    invoke-virtual {v2}, LX/Ebj;->c()I

    move-result v2

    if-ne v2, v0, :cond_4

    iget-object v2, p1, LX/Ebh;->a:LX/Ebj;

    invoke-virtual {v2}, LX/Ebj;->b()[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v3

    .line 2142220
    :goto_0
    move v0, v2

    .line 2142221
    if-eqz v0, :cond_0

    .line 2142222
    sget-object v0, LX/Ect;->a:LX/Ect;

    move-object v0, v0

    .line 2142223
    :goto_1
    return-object v0

    .line 2142224
    :cond_0
    iget-object v0, p0, LX/Eam;->d:LX/DoR;

    .line 2142225
    iget v1, p2, LX/EbA;->d:I

    move v1, v1

    .line 2142226
    invoke-interface {v0, v1}, LX/DoR;->c(I)LX/Ebk;

    move-result-object v0

    invoke-virtual {v0}, LX/Ebk;->b()LX/Eau;

    move-result-object v0

    .line 2142227
    new-instance v1, LX/EbY;

    invoke-direct {v1}, LX/EbY;-><init>()V

    move-object v1, v1

    .line 2142228
    iget-object v2, p2, LX/EbA;->e:LX/Eat;

    move-object v2, v2

    .line 2142229
    iput-object v2, v1, LX/EbY;->f:LX/Eat;

    .line 2142230
    move-object v2, v1

    .line 2142231
    iget-object v3, p2, LX/EbA;->f:LX/Eae;

    move-object v3, v3

    .line 2142232
    iput-object v3, v2, LX/EbY;->e:LX/Eae;

    .line 2142233
    move-object v2, v2

    .line 2142234
    iget-object v3, p0, LX/Eam;->e:LX/DoO;

    invoke-interface {v3}, LX/DoO;->a()LX/Eaf;

    move-result-object v3

    .line 2142235
    iput-object v3, v2, LX/EbY;->a:LX/Eaf;

    .line 2142236
    move-object v2, v2

    .line 2142237
    iput-object v0, v2, LX/EbY;->b:LX/Eau;

    .line 2142238
    move-object v2, v2

    .line 2142239
    iput-object v0, v2, LX/EbY;->d:LX/Eau;

    .line 2142240
    iget-object v0, p2, LX/EbA;->c:LX/Ecs;

    move-object v0, v0

    .line 2142241
    invoke-virtual {v0}, LX/Ecs;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2142242
    iget-object v2, p0, LX/Eam;->c:LX/DoP;

    .line 2142243
    iget-object v0, p2, LX/EbA;->c:LX/Ecs;

    move-object v0, v0

    .line 2142244
    invoke-virtual {v0}, LX/Ecs;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0}, LX/DoP;->a(I)LX/Ebg;

    move-result-object v0

    invoke-virtual {v0}, LX/Ebg;->b()LX/Eau;

    move-result-object v0

    invoke-static {v0}, LX/Ecs;->a(Ljava/lang/Object;)LX/Ecs;

    move-result-object v0

    .line 2142245
    iput-object v0, v1, LX/EbY;->c:LX/Ecs;

    .line 2142246
    :goto_2
    iget-boolean v0, p1, LX/Ebh;->c:Z

    move v0, v0

    .line 2142247
    if-nez v0, :cond_1

    invoke-virtual {p1}, LX/Ebh;->d()V

    .line 2142248
    :cond_1
    iget-object v0, p1, LX/Ebh;->a:LX/Ebj;

    move-object v0, v0

    .line 2142249
    new-instance v4, LX/EbZ;

    iget-object v5, v1, LX/EbY;->a:LX/Eaf;

    iget-object v6, v1, LX/EbY;->b:LX/Eau;

    iget-object v7, v1, LX/EbY;->d:LX/Eau;

    iget-object v8, v1, LX/EbY;->c:LX/Ecs;

    iget-object v9, v1, LX/EbY;->e:LX/Eae;

    iget-object v10, v1, LX/EbY;->f:LX/Eat;

    invoke-direct/range {v4 .. v10}, LX/EbZ;-><init>(LX/Eaf;LX/Eau;LX/Eau;LX/Ecs;LX/Eae;LX/Eat;)V

    move-object v1, v4

    .line 2142250
    invoke-static {v0, v1}, LX/Ebd;->a(LX/Ebj;LX/EbZ;)V

    .line 2142251
    iget-object v0, p1, LX/Ebh;->a:LX/Ebj;

    move-object v0, v0

    .line 2142252
    iget-object v1, p0, LX/Eam;->e:LX/DoO;

    invoke-interface {v1}, LX/DoO;->b()I

    move-result v1

    invoke-virtual {v0, v1}, LX/Ebj;->d(I)V

    .line 2142253
    iget-object v0, p1, LX/Ebh;->a:LX/Ebj;

    move-object v0, v0

    .line 2142254
    iget v1, p2, LX/EbA;->b:I

    move v1, v1

    .line 2142255
    invoke-virtual {v0, v1}, LX/Ebj;->c(I)V

    .line 2142256
    iget-object v0, p1, LX/Ebh;->a:LX/Ebj;

    move-object v0, v0

    .line 2142257
    iget-object v1, p2, LX/EbA;->e:LX/Eat;

    move-object v1, v1

    .line 2142258
    invoke-virtual {v1}, LX/Eat;->a()[B

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ebj;->a([B)V

    .line 2142259
    iget-object v0, p2, LX/EbA;->c:LX/Ecs;

    move-object v0, v0

    .line 2142260
    invoke-virtual {v0}, LX/Ecs;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2142261
    iget-object v0, p2, LX/EbA;->c:LX/Ecs;

    move-object v0, v0

    .line 2142262
    invoke-virtual {v0}, LX/Ecs;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v1, LX/2PB;->a:I

    if-eq v0, v1, :cond_3

    .line 2142263
    iget-object v0, p2, LX/EbA;->c:LX/Ecs;

    move-object v0, v0

    .line 2142264
    goto/16 :goto_1

    .line 2142265
    :cond_2
    sget-object v0, LX/Ect;->a:LX/Ect;

    move-object v0, v0

    .line 2142266
    iput-object v0, v1, LX/EbY;->c:LX/Ecs;

    .line 2142267
    goto :goto_2

    .line 2142268
    :cond_3
    sget-object v0, LX/Ect;->a:LX/Ect;

    move-object v0, v0

    .line 2142269
    goto/16 :goto_1

    .line 2142270
    :cond_4
    iget-object v2, p1, LX/Ebh;->b:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ebj;

    .line 2142271
    invoke-virtual {v2}, LX/Ebj;->c()I

    move-result v5

    if-ne v5, v0, :cond_5

    invoke-virtual {v2}, LX/Ebj;->b()[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    .line 2142272
    goto/16 :goto_0

    .line 2142273
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/Ebh;LX/EbA;)LX/Ecs;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ebh;",
            "LX/EbA;",
            ")",
            "LX/Ecs",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2142208
    iget-object v0, p2, LX/EbA;->f:LX/Eae;

    move-object v0, v0

    .line 2142209
    iget-object v1, p0, LX/Eam;->e:LX/DoO;

    iget-object v2, p0, LX/Eam;->f:LX/Eap;

    invoke-interface {v1, v2, v0}, LX/DoO;->b(LX/Eap;LX/Eae;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2142210
    new-instance v1, LX/Eaq;

    iget-object v2, p0, LX/Eam;->f:LX/Eap;

    .line 2142211
    iget-object v3, v2, LX/Eap;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2142212
    invoke-direct {v1, v2, v0}, LX/Eaq;-><init>(Ljava/lang/String;LX/Eae;)V

    throw v1

    .line 2142213
    :cond_0
    invoke-direct {p0, p1, p2}, LX/Eam;->b(LX/Ebh;LX/EbA;)LX/Ecs;

    move-result-object v1

    .line 2142214
    iget-object v2, p0, LX/Eam;->e:LX/DoO;

    iget-object v3, p0, LX/Eam;->f:LX/Eap;

    invoke-interface {v2, v3, v0}, LX/DoO;->a(LX/Eap;LX/Eae;)V

    .line 2142215
    return-object v1
.end method

.method public final a(LX/Ebf;)V
    .locals 9

    .prologue
    .line 2142137
    sget-object v1, LX/Eao;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2142138
    :try_start_0
    iget-object v0, p0, LX/Eam;->e:LX/DoO;

    iget-object v2, p0, LX/Eam;->f:LX/Eap;

    .line 2142139
    iget-object v3, p1, LX/Ebf;->h:LX/Eae;

    move-object v3, v3

    .line 2142140
    invoke-interface {v0, v2, v3}, LX/DoO;->b(LX/Eap;LX/Eae;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2142141
    new-instance v0, LX/Eaq;

    iget-object v2, p0, LX/Eam;->f:LX/Eap;

    .line 2142142
    iget-object v3, v2, LX/Eap;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2142143
    iget-object v3, p1, LX/Ebf;->h:LX/Eae;

    move-object v3, v3

    .line 2142144
    invoke-direct {v0, v2, v3}, LX/Eaq;-><init>(Ljava/lang/String;LX/Eae;)V

    throw v0

    .line 2142145
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2142146
    :cond_0
    :try_start_1
    iget-object v0, p1, LX/Ebf;->f:LX/Eat;

    move-object v0, v0

    .line 2142147
    if-eqz v0, :cond_1

    .line 2142148
    iget-object v0, p1, LX/Ebf;->h:LX/Eae;

    move-object v0, v0

    .line 2142149
    iget-object v2, v0, LX/Eae;->a:LX/Eat;

    move-object v0, v2

    .line 2142150
    iget-object v2, p1, LX/Ebf;->f:LX/Eat;

    move-object v2, v2

    .line 2142151
    invoke-virtual {v2}, LX/Eat;->a()[B

    move-result-object v2

    .line 2142152
    iget-object v3, p1, LX/Ebf;->g:[B

    move-object v3, v3

    .line 2142153
    invoke-static {v0, v2, v3}, LX/Ear;->a(LX/Eat;[B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2142154
    new-instance v0, LX/Eag;

    const-string v2, "Invalid signature on device key!"

    invoke-direct {v0, v2}, LX/Eag;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142155
    :cond_1
    iget-object v0, p1, LX/Ebf;->f:LX/Eat;

    move-object v0, v0

    .line 2142156
    if-nez v0, :cond_2

    .line 2142157
    new-instance v0, LX/Eag;

    const-string v2, "No signed prekey!"

    invoke-direct {v0, v2}, LX/Eag;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142158
    :cond_2
    iget-object v0, p0, LX/Eam;->b:LX/DoQ;

    iget-object v2, p0, LX/Eam;->f:LX/Eap;

    invoke-interface {v0, v2}, LX/DoQ;->a(LX/Eap;)LX/Ebh;

    move-result-object v2

    .line 2142159
    invoke-static {}, LX/Ear;->a()LX/Eau;

    move-result-object v3

    .line 2142160
    iget-object v0, p1, LX/Ebf;->f:LX/Eat;

    move-object v4, v0

    .line 2142161
    iget-object v0, p1, LX/Ebf;->d:LX/Eat;

    move-object v0, v0

    .line 2142162
    if-nez v0, :cond_6

    .line 2142163
    sget-object v5, LX/Ect;->a:LX/Ect;

    move-object v5, v5

    .line 2142164
    :goto_0
    move-object v5, v5

    .line 2142165
    invoke-virtual {v5}, LX/Ecs;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2142166
    iget v0, p1, LX/Ebf;->c:I

    move v0, v0

    .line 2142167
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/Ecs;->a(Ljava/lang/Object;)LX/Ecs;

    move-result-object v0

    .line 2142168
    :goto_1
    new-instance v6, LX/EbW;

    invoke-direct {v6}, LX/EbW;-><init>()V

    move-object v6, v6

    .line 2142169
    iput-object v3, v6, LX/EbW;->b:LX/Eau;

    .line 2142170
    move-object v7, v6

    .line 2142171
    iget-object v8, p0, LX/Eam;->e:LX/DoO;

    invoke-interface {v8}, LX/DoO;->a()LX/Eaf;

    move-result-object v8

    .line 2142172
    iput-object v8, v7, LX/EbW;->a:LX/Eaf;

    .line 2142173
    move-object v7, v7

    .line 2142174
    iget-object v8, p1, LX/Ebf;->h:LX/Eae;

    move-object v8, v8

    .line 2142175
    iput-object v8, v7, LX/EbW;->c:LX/Eae;

    .line 2142176
    move-object v7, v7

    .line 2142177
    iput-object v4, v7, LX/EbW;->d:LX/Eat;

    .line 2142178
    move-object v7, v7

    .line 2142179
    iput-object v4, v7, LX/EbW;->e:LX/Eat;

    .line 2142180
    move-object v4, v7

    .line 2142181
    iput-object v5, v4, LX/EbW;->f:LX/Ecs;

    .line 2142182
    iget-boolean v4, v2, LX/Ebh;->c:Z

    move v4, v4

    .line 2142183
    if-nez v4, :cond_3

    invoke-virtual {v2}, LX/Ebh;->d()V

    .line 2142184
    :cond_3
    iget-object v4, v2, LX/Ebh;->a:LX/Ebj;

    move-object v4, v4

    .line 2142185
    invoke-virtual {v6}, LX/EbW;->a()LX/EbX;

    move-result-object v5

    invoke-static {v4, v5}, LX/Ebd;->a(LX/Ebj;LX/EbX;)V

    .line 2142186
    iget-object v4, v2, LX/Ebh;->a:LX/Ebj;

    move-object v4, v4

    .line 2142187
    iget v5, p1, LX/Ebf;->e:I

    move v5, v5

    .line 2142188
    iget-object v6, v3, LX/Eau;->a:LX/Eat;

    move-object v6, v6

    .line 2142189
    invoke-static {}, LX/Ecd;->w()LX/Ecd;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/Ecd;->b(I)LX/Ecd;

    move-result-object v7

    invoke-virtual {v6}, LX/Eat;->a()[B

    move-result-object v8

    invoke-static {v8}, LX/EWc;->a([B)LX/EWc;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/Ecd;->a(LX/EWc;)LX/Ecd;

    move-result-object v8

    .line 2142190
    invoke-virtual {v0}, LX/Ecs;->a()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2142191
    invoke-virtual {v0}, LX/Ecs;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v8, v7}, LX/Ecd;->a(I)LX/Ecd;

    .line 2142192
    :cond_4
    iget-object v7, v4, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v7}, LX/Ecf;->O()LX/EcK;

    move-result-object v7

    invoke-virtual {v8}, LX/Ecd;->l()LX/Ece;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/EcK;->a(LX/Ece;)LX/EcK;

    move-result-object v7

    invoke-virtual {v7}, LX/EcK;->l()LX/Ecf;

    move-result-object v7

    iput-object v7, v4, LX/Ebj;->a:LX/Ecf;

    .line 2142193
    iget-object v0, v2, LX/Ebh;->a:LX/Ebj;

    move-object v0, v0

    .line 2142194
    iget-object v4, p0, LX/Eam;->e:LX/DoO;

    invoke-interface {v4}, LX/DoO;->b()I

    move-result v4

    invoke-virtual {v0, v4}, LX/Ebj;->d(I)V

    .line 2142195
    iget-object v0, v2, LX/Ebh;->a:LX/Ebj;

    move-object v0, v0

    .line 2142196
    iget v4, p1, LX/Ebf;->a:I

    move v4, v4

    .line 2142197
    invoke-virtual {v0, v4}, LX/Ebj;->c(I)V

    .line 2142198
    iget-object v0, v2, LX/Ebh;->a:LX/Ebj;

    move-object v0, v0

    .line 2142199
    iget-object v4, v3, LX/Eau;->a:LX/Eat;

    move-object v3, v4

    .line 2142200
    invoke-virtual {v3}, LX/Eat;->a()[B

    move-result-object v3

    invoke-virtual {v0, v3}, LX/Ebj;->a([B)V

    .line 2142201
    iget-object v0, p0, LX/Eam;->b:LX/DoQ;

    iget-object v3, p0, LX/Eam;->f:LX/Eap;

    invoke-interface {v0, v3, v2}, LX/DoQ;->a(LX/Eap;LX/Ebh;)V

    .line 2142202
    iget-object v0, p0, LX/Eam;->e:LX/DoO;

    iget-object v2, p0, LX/Eam;->f:LX/Eap;

    .line 2142203
    iget-object v3, p1, LX/Ebf;->h:LX/Eae;

    move-object v3, v3

    .line 2142204
    invoke-interface {v0, v2, v3}, LX/DoO;->a(LX/Eap;LX/Eae;)V

    .line 2142205
    monitor-exit v1

    return-void

    .line 2142206
    :cond_5
    sget-object v0, LX/Ect;->a:LX/Ect;

    move-object v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2142207
    goto/16 :goto_1

    :cond_6
    new-instance v5, LX/Ecv;

    invoke-direct {v5, v0}, LX/Ecv;-><init>(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
