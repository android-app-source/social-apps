.class public abstract LX/D14;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field public b:F

.field public c:I

.field public d:I

.field public e:Lcom/facebook/storelocator/StoreLocatorMapDelegate;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1956248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;"
        }
    .end annotation
.end method

.method public a(I)Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;
    .locals 1

    .prologue
    .line 1956249
    invoke-virtual {p0}, LX/D14;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    return-object v0
.end method

.method public abstract a(LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
.end method

.method public b()Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;
    .locals 1

    .prologue
    .line 1956250
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->FULL:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    return-object v0
.end method
