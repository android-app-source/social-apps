.class public LX/E73;
.super LX/Cfk;
.source ""


# direct methods
.method public constructor <init>(LX/3Tx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081031
    invoke-direct {p0, p1}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2081032
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 1

    .prologue
    .line 2081033
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 7

    .prologue
    .line 2081034
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v1

    .line 2081035
    const v0, 0x7f031111

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v2

    .line 2081036
    const v0, 0x7f0d287a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2081037
    iget-object v3, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v3, v3

    .line 2081038
    const v4, 0x7f08222f

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081039
    return-object v2
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2081040
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
