.class public final enum LX/CrN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CrN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CrN;

.field public static final enum ASPECT_FIT:LX/CrN;

.field public static final enum ASPECT_FIT_ONLY:LX/CrN;

.field public static final enum ASPECT_FIT_SLIDE:LX/CrN;

.field public static final enum ASPECT_FIT_SLIDESHOW:LX/CrN;

.field public static final enum FULLSCREEN:LX/CrN;

.field public static final enum FULLSCREEN_SLIDE:LX/CrN;

.field public static final enum FULLSCREEN_SLIDESHOW:LX/CrN;

.field public static final enum NATIVE_ADS_ASPECT_FIT_ONLY:LX/CrN;

.field public static final enum NATIVE_ADS_ASPECT_FIT_ONLY_EDGE_TO_EDGE:LX/CrN;

.field public static final enum NATIVE_ADS_ASPECT_FIT_ONLY_MULTISHARE:LX/CrN;

.field public static final enum NON_ADJUSTED_FIT_TO_WIDTH_SLIDE:LX/CrN;

.field public static final enum NON_INTERACTIVE:LX/CrN;

.field public static final enum NON_INTERACTIVE_ASPECT_FIT:LX/CrN;

.field public static final enum SPHERICAL_PHOTO:LX/CrN;

.field public static final enum SPHERICAL_VIDEO:LX/CrN;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1940844
    new-instance v0, LX/CrN;

    const-string v1, "ASPECT_FIT"

    invoke-direct {v0, v1, v3}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->ASPECT_FIT:LX/CrN;

    .line 1940845
    new-instance v0, LX/CrN;

    const-string v1, "ASPECT_FIT_ONLY"

    invoke-direct {v0, v1, v4}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->ASPECT_FIT_ONLY:LX/CrN;

    .line 1940846
    new-instance v0, LX/CrN;

    const-string v1, "NATIVE_ADS_ASPECT_FIT_ONLY"

    invoke-direct {v0, v1, v5}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY:LX/CrN;

    .line 1940847
    new-instance v0, LX/CrN;

    const-string v1, "NATIVE_ADS_ASPECT_FIT_ONLY_MULTISHARE"

    invoke-direct {v0, v1, v6}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY_MULTISHARE:LX/CrN;

    .line 1940848
    new-instance v0, LX/CrN;

    const-string v1, "NATIVE_ADS_ASPECT_FIT_ONLY_EDGE_TO_EDGE"

    invoke-direct {v0, v1, v7}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY_EDGE_TO_EDGE:LX/CrN;

    .line 1940849
    new-instance v0, LX/CrN;

    const-string v1, "FULLSCREEN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->FULLSCREEN:LX/CrN;

    .line 1940850
    new-instance v0, LX/CrN;

    const-string v1, "NON_INTERACTIVE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->NON_INTERACTIVE:LX/CrN;

    .line 1940851
    new-instance v0, LX/CrN;

    const-string v1, "ASPECT_FIT_SLIDESHOW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->ASPECT_FIT_SLIDESHOW:LX/CrN;

    .line 1940852
    new-instance v0, LX/CrN;

    const-string v1, "FULLSCREEN_SLIDESHOW"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->FULLSCREEN_SLIDESHOW:LX/CrN;

    .line 1940853
    new-instance v0, LX/CrN;

    const-string v1, "ASPECT_FIT_SLIDE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->ASPECT_FIT_SLIDE:LX/CrN;

    .line 1940854
    new-instance v0, LX/CrN;

    const-string v1, "FULLSCREEN_SLIDE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->FULLSCREEN_SLIDE:LX/CrN;

    .line 1940855
    new-instance v0, LX/CrN;

    const-string v1, "NON_ADJUSTED_FIT_TO_WIDTH_SLIDE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->NON_ADJUSTED_FIT_TO_WIDTH_SLIDE:LX/CrN;

    .line 1940856
    new-instance v0, LX/CrN;

    const-string v1, "NON_INTERACTIVE_ASPECT_FIT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->NON_INTERACTIVE_ASPECT_FIT:LX/CrN;

    .line 1940857
    new-instance v0, LX/CrN;

    const-string v1, "SPHERICAL_VIDEO"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->SPHERICAL_VIDEO:LX/CrN;

    .line 1940858
    new-instance v0, LX/CrN;

    const-string v1, "SPHERICAL_PHOTO"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/CrN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrN;->SPHERICAL_PHOTO:LX/CrN;

    .line 1940859
    const/16 v0, 0xf

    new-array v0, v0, [LX/CrN;

    sget-object v1, LX/CrN;->ASPECT_FIT:LX/CrN;

    aput-object v1, v0, v3

    sget-object v1, LX/CrN;->ASPECT_FIT_ONLY:LX/CrN;

    aput-object v1, v0, v4

    sget-object v1, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY:LX/CrN;

    aput-object v1, v0, v5

    sget-object v1, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY_MULTISHARE:LX/CrN;

    aput-object v1, v0, v6

    sget-object v1, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY_EDGE_TO_EDGE:LX/CrN;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CrN;->FULLSCREEN:LX/CrN;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CrN;->NON_INTERACTIVE:LX/CrN;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CrN;->ASPECT_FIT_SLIDESHOW:LX/CrN;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/CrN;->FULLSCREEN_SLIDESHOW:LX/CrN;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/CrN;->ASPECT_FIT_SLIDE:LX/CrN;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/CrN;->FULLSCREEN_SLIDE:LX/CrN;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/CrN;->NON_ADJUSTED_FIT_TO_WIDTH_SLIDE:LX/CrN;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/CrN;->NON_INTERACTIVE_ASPECT_FIT:LX/CrN;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/CrN;->SPHERICAL_VIDEO:LX/CrN;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/CrN;->SPHERICAL_PHOTO:LX/CrN;

    aput-object v2, v0, v1

    sput-object v0, LX/CrN;->$VALUES:[LX/CrN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1940861
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CrN;
    .locals 1

    .prologue
    .line 1940862
    const-class v0, LX/CrN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CrN;

    return-object v0
.end method

.method public static values()[LX/CrN;
    .locals 1

    .prologue
    .line 1940860
    sget-object v0, LX/CrN;->$VALUES:[LX/CrN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CrN;

    return-object v0
.end method
