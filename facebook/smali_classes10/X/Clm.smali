.class public LX/Clm;
.super LX/Cll;
.source ""


# static fields
.field private static final i:Ljava/lang/String;


# instance fields
.field public d:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/ClD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Cig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1933065
    const-class v0, LX/Clm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Clm;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 1933066
    invoke-direct {p0, p1, p2}, LX/Cll;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;)V

    .line 1933067
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Clm;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v3

    check-cast v3, LX/Ckw;

    invoke-static {v0}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v4

    check-cast v4, LX/ClD;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object p2

    check-cast p2, LX/Chi;

    invoke-static {v0}, LX/Cig;->a(LX/0QB;)LX/Cig;

    move-result-object v0

    check-cast v0, LX/Cig;

    iput-object v3, v2, LX/Clm;->d:LX/Ckw;

    iput-object v4, v2, LX/Clm;->e:LX/ClD;

    iput-object p1, v2, LX/Clm;->f:LX/03V;

    iput-object p2, v2, LX/Clm;->g:LX/Chi;

    iput-object v0, v2, LX/Clm;->h:LX/Cig;

    .line 1933068
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1933069
    iget-object v0, p0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->G()Ljava/lang/String;

    move-result-object v0

    .line 1933070
    invoke-static {v0}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1933071
    iget-object v0, p0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->F()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1933072
    :cond_0
    :goto_0
    return-void

    .line 1933073
    :cond_1
    iget-object v0, p0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->F()Ljava/lang/String;

    move-result-object v0

    .line 1933074
    :cond_2
    iget-object v1, p0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->E()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v3, 0x5fcedbf5

    if-ne v1, v3, :cond_8

    .line 1933075
    iget-object v1, p0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 1933076
    :goto_1
    if-nez v0, :cond_3

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1933077
    :cond_3
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1933078
    if-eqz v0, :cond_4

    .line 1933079
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1933080
    invoke-static {v4}, LX/1H1;->f(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1933081
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1933082
    :cond_4
    const-string v4, "com.android.browser.headers"

    invoke-static {}, LX/Cs3;->a()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1933083
    const-string v4, "android.intent.category.BROWSABLE"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1933084
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1933085
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setSelector(Landroid/content/Intent;)V

    .line 1933086
    const-string v2, "extra_instant_articles_id"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1933087
    const-string v2, "extra_instant_articles_referrer"

    const-string v4, "instant_article_link_entity"

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1933088
    const-string v2, "extra_parent_article_click_source"

    iget-object v4, p0, LX/Clm;->g:LX/Chi;

    .line 1933089
    iget-object v5, v4, LX/Chi;->j:Ljava/lang/String;

    move-object v4, v5

    .line 1933090
    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1933091
    iget-object v2, p0, LX/Clm;->e:LX/ClD;

    invoke-virtual {p0}, LX/Cll;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/ClD;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 1933092
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1933093
    const-string v4, "click_source_document_chaining_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1933094
    iget-object v2, p0, LX/Clm;->e:LX/ClD;

    invoke-virtual {p0}, LX/Cll;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v2

    .line 1933095
    const/4 v4, -0x1

    if-eq v2, v4, :cond_5

    .line 1933096
    const-string v4, "click_source_document_depth"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1933097
    :cond_5
    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1933098
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1933099
    const-string v4, "article_ID"

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1933100
    :cond_6
    iget-object v4, p0, LX/Clm;->d:LX/Ckw;

    invoke-virtual {v4, v0, v2}, LX/Ckw;->b(Ljava/lang/String;Ljava/util/Map;)V

    .line 1933101
    iget-object v2, p0, LX/Clm;->d:LX/Ckw;

    const-string v4, "native_article_text_block"

    invoke-virtual {v2, v0, v4}, LX/Ckw;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1933102
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1933103
    iget-object v1, p0, LX/Clm;->h:LX/Cig;

    new-instance v2, LX/Cin;

    invoke-direct {v2}, LX/Cin;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1933104
    :cond_7
    iget-object v1, p0, LX/Cll;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, LX/Cll;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1933105
    :catch_0
    move-exception v1

    .line 1933106
    iget-object v2, p0, LX/Clm;->f:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/Clm;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_onClick"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error trying to launch url:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 1933107
    iput-object v1, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1933108
    move-object v0, v0

    .line 1933109
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/03V;->a(LX/0VG;)V

    goto/16 :goto_0

    :cond_8
    move-object v1, v2

    goto/16 :goto_1
.end method
