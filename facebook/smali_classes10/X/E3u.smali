.class public final LX/E3u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2eJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E2::",
        "LX/1PW;",
        ">",
        "Ljava/lang/Object;",
        "LX/2eJ",
        "<",
        "LX/E3x;",
        "TE2;>;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field private final b:LX/03V;

.field public c:LX/E2c;

.field private d:[J

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/1U8;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

.field private g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;


# direct methods
.method public constructor <init>(LX/03V;LX/E2c;Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 8

    .prologue
    .line 2075617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2075618
    iput-object p1, p0, LX/E3u;->b:LX/03V;

    .line 2075619
    iput-object p2, p0, LX/E3u;->c:LX/E2c;

    .line 2075620
    iget-object v0, p4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2075621
    invoke-interface {v0}, LX/9uc;->bU()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/E3u;->e:LX/0Px;

    .line 2075622
    iput-object p3, p0, LX/E3u;->f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

    .line 2075623
    iget-object v0, p0, LX/E3u;->e:LX/0Px;

    iget-object v1, p0, LX/E3u;->b:LX/03V;

    .line 2075624
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    new-array v4, v2, [J

    .line 2075625
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 2075626
    :try_start_0
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1U8;

    invoke-interface {v2}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    aput-wide v6, v4, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2075627
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2075628
    :catch_0
    sget-object v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthCounterUnitComponentPartDefinition;->a:Ljava/lang/String;

    const-string v5, "Neighborhood feed query returned with empty results"

    invoke-virtual {v1, v2, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2075629
    :cond_0
    move-object v0, v4

    .line 2075630
    iput-object v0, p0, LX/E3u;->d:[J

    .line 2075631
    iput-object p4, p0, LX/E3u;->g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075632
    return-void
.end method

.method public static a(LX/E3u;II)V
    .locals 3

    .prologue
    .line 2075613
    iget-object v0, p0, LX/E3u;->a:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_0

    .line 2075614
    :goto_0
    return-void

    .line 2075615
    :cond_0
    const-string v0, "%d/%d"

    add-int/lit8 v1, p1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2075616
    iget-object v1, p0, LX/E3u;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2075612
    iget-object v0, p0, LX/E3u;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/1RC;
    .locals 1

    .prologue
    .line 2075602
    iget-object v0, p0, LX/E3u;->f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoFullWidthPartDefinition;

    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2075607
    new-instance v1, LX/E3x;

    iget-object v0, p0, LX/E3u;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    iget-object v2, p0, LX/E3u;->d:[J

    iget-object v3, p0, LX/E3u;->g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075608
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v4

    .line 2075609
    iget-object v4, p0, LX/E3u;->g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075610
    iget-object p0, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, p0

    .line 2075611
    invoke-direct {v1, v0, v2, v3, v4}, LX/E3x;-><init>(LX/1U8;[JLjava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 2075603
    iget-object v0, p0, LX/E3u;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {p0, p1, v0}, LX/E3u;->a(LX/E3u;II)V

    .line 2075604
    iget-object v0, p0, LX/E3u;->c:LX/E2c;

    .line 2075605
    iput p1, v0, LX/E2c;->d:I

    .line 2075606
    return-void
.end method
