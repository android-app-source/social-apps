.class public LX/DVL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DVJ;


# instance fields
.field private a:LX/DVq;

.field private b:LX/DVp;

.field public c:LX/DVU;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Or;LX/DVq;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/DVq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2004949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2004950
    const-string v0, ""

    iput-object v0, p0, LX/DVL;->d:Ljava/lang/String;

    .line 2004951
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/DVL;->e:Ljava/lang/String;

    .line 2004952
    iput-object p2, p0, LX/DVL;->a:LX/DVq;

    .line 2004953
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2004954
    iget-object v0, p0, LX/DVL;->b:LX/DVp;

    if-eqz v0, :cond_0

    .line 2004955
    iget-object v0, p0, LX/DVL;->b:LX/DVp;

    invoke-virtual {v0}, LX/B1d;->e()V

    .line 2004956
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2004957
    iget-object v0, p0, LX/DVL;->b:LX/DVp;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/DVL;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2004958
    :cond_0
    iput-object p1, p0, LX/DVL;->d:Ljava/lang/String;

    .line 2004959
    iget-object v0, p0, LX/DVL;->a:LX/DVq;

    iget-object v1, p0, LX/DVL;->e:Ljava/lang/String;

    new-instance v2, LX/DVK;

    invoke-direct {v2, p0}, LX/DVK;-><init>(LX/DVL;)V

    .line 2004960
    new-instance v3, LX/DVp;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    move-object v5, v1

    move-object v6, p1

    move-object v8, v2

    invoke-direct/range {v3 .. v8}, LX/DVp;-><init>(LX/1Ck;Ljava/lang/String;Ljava/lang/String;LX/0tX;LX/B1b;)V

    .line 2004961
    move-object v0, v3

    .line 2004962
    iput-object v0, p0, LX/DVL;->b:LX/DVp;

    .line 2004963
    :cond_1
    iget-object v0, p0, LX/DVL;->b:LX/DVp;

    invoke-virtual {v0}, LX/B1d;->f()V

    .line 2004964
    return-void
.end method

.method public final a(Ljava/lang/String;LX/DVU;Z)V
    .locals 0

    .prologue
    .line 2004965
    iput-object p2, p0, LX/DVL;->c:LX/DVU;

    .line 2004966
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2004967
    iget-object v0, p0, LX/DVL;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DVL;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2004968
    iget-object v0, p0, LX/DVL;->c:LX/DVU;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
