.class public final LX/DBB;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/widget/Toast;

.field public final synthetic c:Landroid/content/res/Resources;

.field public final synthetic d:LX/DBC;


# direct methods
.method public constructor <init>(LX/DBC;Ljava/lang/String;Landroid/widget/Toast;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 1972346
    iput-object p1, p0, LX/DBB;->d:LX/DBC;

    iput-object p2, p0, LX/DBB;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DBB;->b:Landroid/widget/Toast;

    iput-object p4, p0, LX/DBB;->c:Landroid/content/res/Resources;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1972347
    iget-object v0, p0, LX/DBB;->d:LX/DBC;

    iget-object v0, v0, LX/DBC;->h:LX/Bl6;

    new-instance v1, LX/BlX;

    invoke-direct {v1}, LX/BlX;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1972348
    iget-object v0, p0, LX/DBB;->b:Landroid/widget/Toast;

    iget-object v1, p0, LX/DBB;->c:Landroid/content/res/Resources;

    const v2, 0x7f082f33

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 1972349
    iget-object v0, p0, LX/DBB;->b:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1972350
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1972351
    iget-object v0, p0, LX/DBB;->d:LX/DBC;

    iget-object v0, v0, LX/DBC;->h:LX/Bl6;

    new-instance v1, LX/BlL;

    iget-object v2, p0, LX/DBB;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, LX/BlL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1972352
    iget-object v0, p0, LX/DBB;->b:Landroid/widget/Toast;

    iget-object v1, p0, LX/DBB;->c:Landroid/content/res/Resources;

    const v2, 0x7f082f32

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 1972353
    iget-object v0, p0, LX/DBB;->b:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1972354
    return-void
.end method
