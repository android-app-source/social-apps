.class public LX/DFU;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFV;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DFU",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DFV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978230
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1978231
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DFU;->b:LX/0Zi;

    .line 1978232
    iput-object p1, p0, LX/DFU;->a:LX/0Ot;

    .line 1978233
    return-void
.end method

.method public static a(LX/0QB;)LX/DFU;
    .locals 4

    .prologue
    .line 1978234
    const-class v1, LX/DFU;

    monitor-enter v1

    .line 1978235
    :try_start_0
    sget-object v0, LX/DFU;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978236
    sput-object v2, LX/DFU;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978237
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978238
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978239
    new-instance v3, LX/DFU;

    const/16 p0, 0x20e5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DFU;-><init>(LX/0Ot;)V

    .line 1978240
    move-object v0, v3

    .line 1978241
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978242
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978243
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978244
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1978245
    check-cast p2, LX/DFT;

    .line 1978246
    iget-object v0, p0, LX/DFU;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DFV;

    iget-object v2, p2, LX/DFT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/DFT;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    iget-object v4, p2, LX/DFT;->c:LX/1Pc;

    iget-object v5, p2, LX/DFT;->d:LX/3mj;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/DFV;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;LX/1Pc;LX/3mj;)LX/1Dg;

    move-result-object v0

    .line 1978247
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1978248
    invoke-static {}, LX/1dS;->b()V

    .line 1978249
    const/4 v0, 0x0

    return-object v0
.end method
