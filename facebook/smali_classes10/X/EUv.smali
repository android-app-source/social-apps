.class public final LX/EUv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EUu;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic b:LX/EUx;

.field public final synthetic c:LX/ETi;

.field public final synthetic d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;Lcom/facebook/graphql/model/GraphQLActor;LX/EUx;LX/ETi;)V
    .locals 0

    .prologue
    .line 2126864
    iput-object p1, p0, LX/EUv;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    iput-object p2, p0, LX/EUv;->a:Lcom/facebook/graphql/model/GraphQLActor;

    iput-object p3, p0, LX/EUv;->b:LX/EUx;

    iput-object p4, p0, LX/EUv;->c:LX/ETi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 14

    .prologue
    .line 2126865
    iget-object v0, p0, LX/EUv;->a:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    .line 2126866
    iget-object v0, p0, LX/EUv;->b:LX/EUx;

    iget-object v0, v0, LX/EUx;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 2126867
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    .line 2126868
    if-eqz p1, :cond_2

    .line 2126869
    iget-object v0, p0, LX/EUv;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Gp;

    iget-object v4, p0, LX/EUv;->b:LX/EUx;

    iget-object v4, v4, LX/EUx;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v3}, LX/3Gp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2126870
    :goto_0
    iget-object v0, p0, LX/EUv;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    iget-object v3, p0, LX/EUv;->b:LX/EUx;

    iget-object v4, p0, LX/EUv;->c:LX/ETi;

    .line 2126871
    iget-object v5, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2xj;

    invoke-virtual {v5}, LX/2xj;->i()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2126872
    iget-object v5, v3, LX/EUx;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126873
    iget-object v6, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v6

    .line 2126874
    invoke-interface {v5}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 2126875
    iget-object v6, v3, LX/EUx;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126876
    iget-object v7, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v7

    .line 2126877
    iget-object v7, v3, LX/EUx;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126878
    iget-object v8, v7, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v8

    .line 2126879
    invoke-interface {v4, v6}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v8

    .line 2126880
    invoke-interface {v4, v6, v5}, LX/ETi;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v9

    .line 2126881
    iget-object v5, v3, LX/EUx;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126882
    iget-object v6, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v6

    .line 2126883
    if-eqz v5, :cond_3

    iget-object v5, v3, LX/EUx;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126884
    iget-object v6, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v6

    .line 2126885
    invoke-interface {v5}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v10

    .line 2126886
    :goto_1
    iget-object v5, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    move-object v11, v5

    check-cast v11, LX/3AW;

    sget-object v13, LX/04D;->VIDEO_HOME:LX/04D;

    if-eqz p1, :cond_4

    sget-object v5, LX/0JD;->SUBSCRIBE_BUTTON:LX/0JD;

    move-object v12, v5

    :goto_2
    new-instance v5, LX/7Qg;

    move-object v6, v1

    invoke-direct/range {v5 .. v10}, LX/7Qg;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    invoke-virtual {v11, v13, v12, v5}, LX/3AW;->a(LX/04D;LX/0JD;LX/7Qf;)V

    .line 2126887
    :cond_0
    iget-object v0, p0, LX/EUv;->c:LX/ETi;

    check-cast v0, LX/1Pr;

    iget-object v3, p0, LX/EUv;->b:LX/EUx;

    iget-object v3, v3, LX/EUx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0, v3, v1}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->b(LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)LX/D6V;

    move-result-object v0

    .line 2126888
    const/4 v3, 0x1

    .line 2126889
    iput-boolean v3, v0, LX/D6V;->a:Z

    .line 2126890
    if-eqz p1, :cond_1

    .line 2126891
    iget-object v0, p0, LX/EUv;->c:LX/ETi;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, LX/EUv;->a:Lcom/facebook/graphql/model/GraphQLActor;

    iget-object v4, p0, LX/EUv;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    .line 2126892
    new-instance v5, LX/EUw;

    invoke-direct {v5, v4, v1}, LX/EUw;-><init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;Ljava/lang/String;)V

    move-object v1, v5

    .line 2126893
    invoke-static {v0, v3, v2, v1}, LX/BUp;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLMedia;LX/Aek;)V

    .line 2126894
    :cond_1
    return-void

    .line 2126895
    :cond_2
    iget-object v0, p0, LX/EUv;->d:Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeFollowButtonPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Gp;

    iget-object v4, p0, LX/EUv;->b:LX/EUx;

    iget-object v4, v4, LX/EUx;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v3}, LX/3Gp;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2126896
    :cond_3
    const/4 v10, 0x0

    goto :goto_1

    .line 2126897
    :cond_4
    sget-object v5, LX/0JD;->UNSUBSCRIBE_BUTTON:LX/0JD;

    move-object v12, v5

    goto :goto_2
.end method
