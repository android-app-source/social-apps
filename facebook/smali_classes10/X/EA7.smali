.class public final LX/EA7;
.super LX/BNO;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/EA9;


# direct methods
.method public constructor <init>(LX/EA9;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2085308
    iput-object p1, p0, LX/EA7;->b:LX/EA9;

    iput-object p2, p0, LX/EA7;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/BNO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2085306
    iget-object v0, p0, LX/EA7;->b:LX/EA9;

    iget-object v1, p0, LX/EA7;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/EA9;->a$redex0(LX/EA9;Ljava/lang/String;)V

    .line 2085307
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 5

    .prologue
    .line 2085294
    iget-object v0, p0, LX/EA7;->b:LX/EA9;

    iget-object v1, p0, LX/EA7;->a:Ljava/lang/String;

    .line 2085295
    iget-object v2, v0, LX/EA9;->f:LX/EAH;

    new-instance v3, LX/EA8;

    invoke-direct {v3, v0, v1, p1}, LX/EA8;-><init>(LX/EA9;Ljava/lang/String;Lcom/facebook/fbservice/service/OperationResult;)V

    .line 2085296
    new-instance v4, LX/EB1;

    invoke-direct {v4}, LX/EB1;-><init>()V

    move-object v4, v4

    .line 2085297
    const-string p0, "page_id"

    invoke-virtual {v4, p0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2085298
    const-string p0, "feed_story_render_location"

    const-string v0, "reviews_feed"

    invoke-virtual {v4, p0, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2085299
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2085300
    invoke-static {v2, v4}, LX/EAH;->a(LX/EAH;LX/0zO;)V

    .line 2085301
    iget-object p0, v2, LX/EAH;->a:LX/0tX;

    invoke-virtual {p0, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2085302
    iget-object p0, v2, LX/EAH;->b:LX/1Ck;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string p1, "key_overall_rating_and_viewer_review"

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance p1, LX/EAE;

    invoke-direct {p1, v2, v3}, LX/EAE;-><init>(LX/EAH;LX/EA8;)V

    invoke-virtual {p0, v0, v4, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2085303
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2085304
    iget-object v0, p0, LX/EA7;->b:LX/EA9;

    iget-object v1, p0, LX/EA7;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/EA9;->a$redex0(LX/EA9;Ljava/lang/String;)V

    .line 2085305
    return-void
.end method
