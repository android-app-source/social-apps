.class public final LX/ELO;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/ELP;


# direct methods
.method public constructor <init>(LX/ELP;LX/CzL;)V
    .locals 0

    .prologue
    .line 2108785
    iput-object p1, p0, LX/ELO;->b:LX/ELP;

    iput-object p2, p0, LX/ELO;->a:LX/CzL;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2108795
    iget-object v0, p0, LX/ELO;->b:LX/ELP;

    iget-object v0, v0, LX/ELP;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    iget-object v1, p0, LX/ELO;->b:LX/ELP;

    iget-object v1, v1, LX/ELP;->a:LX/CzL;

    iget-object v2, p0, LX/ELO;->a:LX/CzL;

    iget-object v3, p0, LX/ELO;->b:LX/ELP;

    iget-object v3, v3, LX/ELP;->b:LX/CxA;

    const/4 p0, 0x0

    .line 2108796
    iget-object v4, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2Sc;

    sget-object v5, LX/3Ql;->FAILED_MUTATION:LX/3Ql;

    invoke-virtual {v4, v5, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2108797
    invoke-interface {v3, v2}, LX/CxA;->a(LX/CzL;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2108798
    new-instance v5, LX/ELU;

    .line 2108799
    iget-object v4, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2108800
    check-cast v4, LX/8d9;

    invoke-interface {v4}, LX/8d9;->dW_()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4, p0}, LX/ELU;-><init>(Ljava/lang/String;Z)V

    move-object v4, v3

    .line 2108801
    check-cast v4, LX/1Pr;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-interface {v4, v5, p0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2108802
    invoke-interface {v3, v2, v1}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2108803
    check-cast v3, LX/1Pq;

    invoke-interface {v3}, LX/1Pq;->iN_()V

    .line 2108804
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2108786
    iget-object v0, p0, LX/ELO;->b:LX/ELP;

    iget-object v0, v0, LX/ELP;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v1, p0, LX/ELO;->b:LX/ELP;

    iget-object v1, v1, LX/ELP;->b:LX/CxA;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/CvJ;->INLINE_EVENT_JOIN:LX/CvJ;

    iget-object v3, p0, LX/ELO;->b:LX/ELP;

    iget-object v3, v3, LX/ELP;->b:LX/CxA;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/ELO;->a:LX/CzL;

    invoke-interface {v3, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, LX/ELO;->a:LX/CzL;

    iget-object v5, p0, LX/ELO;->b:LX/ELP;

    iget-object v5, v5, LX/ELP;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    iget-object v5, v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CvY;

    iget-object v6, p0, LX/ELO;->b:LX/ELP;

    iget-object v6, v6, LX/ELP;->b:LX/CxA;

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    iget-object v7, p0, LX/ELO;->a:LX/CzL;

    .line 2108787
    iget-object v5, v7, LX/CzL;->a:Ljava/lang/Object;

    move-object v5, v5

    .line 2108788
    check-cast v5, LX/8d9;

    .line 2108789
    invoke-interface {v5}, LX/8d9;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2108790
    sget-object v8, LX/CvJ;->INLINE_EVENT_JOIN:LX/CvJ;

    invoke-interface {v5}, LX/8d9;->dW_()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v7}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object p1

    .line 2108791
    iget-object v5, v7, LX/CzL;->d:LX/0am;

    move-object v5, v5

    .line 2108792
    invoke-virtual {v5}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v8, p0, v6, p1, v5}, LX/CvY;->a(LX/CvJ;Ljava/lang/String;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v5, v5

    .line 2108793
    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CvJ;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2108794
    return-void
.end method
