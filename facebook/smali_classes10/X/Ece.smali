.class public final LX/Ece;
.super LX/EWp;
.source ""

# interfaces
.implements LX/Ecc;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ece;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/Ece;


# instance fields
.field public baseKey_:LX/EWc;

.field public bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public preKeyId_:I

.field public signedPreKeyId_:I

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2147526
    new-instance v0, LX/Ecb;

    invoke-direct {v0}, LX/Ecb;-><init>()V

    sput-object v0, LX/Ece;->a:LX/EWZ;

    .line 2147527
    new-instance v0, LX/Ece;

    invoke-direct {v0}, LX/Ece;-><init>()V

    .line 2147528
    sput-object v0, LX/Ece;->c:LX/Ece;

    invoke-direct {v0}, LX/Ece;->x()V

    .line 2147529
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2147521
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147522
    iput-byte v0, p0, LX/Ece;->memoizedIsInitialized:B

    .line 2147523
    iput v0, p0, LX/Ece;->memoizedSerializedSize:I

    .line 2147524
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2147525
    iput-object v0, p0, LX/Ece;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2147489
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147490
    iput-byte v0, p0, LX/Ece;->memoizedIsInitialized:B

    .line 2147491
    iput v0, p0, LX/Ece;->memoizedSerializedSize:I

    .line 2147492
    invoke-direct {p0}, LX/Ece;->x()V

    .line 2147493
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2147494
    const/4 v0, 0x0

    .line 2147495
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2147496
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2147497
    sparse-switch v3, :sswitch_data_0

    .line 2147498
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2147499
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2147500
    goto :goto_0

    .line 2147501
    :sswitch_1
    iget v3, p0, LX/Ece;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/Ece;->bitField0_:I

    .line 2147502
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/Ece;->preKeyId_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2147503
    :catch_0
    move-exception v0

    .line 2147504
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147505
    move-object v0, v0

    .line 2147506
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2147507
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/Ece;->unknownFields:LX/EZQ;

    .line 2147508
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2147509
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/Ece;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/Ece;->bitField0_:I

    .line 2147510
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Ece;->baseKey_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2147511
    :catch_1
    move-exception v0

    .line 2147512
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2147513
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147514
    move-object v0, v1

    .line 2147515
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2147516
    :sswitch_3
    :try_start_4
    iget v3, p0, LX/Ece;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/Ece;->bitField0_:I

    .line 2147517
    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v3

    iput v3, p0, LX/Ece;->signedPreKeyId_:I
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2147518
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ece;->unknownFields:LX/EZQ;

    .line 2147519
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2147520
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2147484
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2147485
    iput-byte v1, p0, LX/Ece;->memoizedIsInitialized:B

    .line 2147486
    iput v1, p0, LX/Ece;->memoizedSerializedSize:I

    .line 2147487
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ece;->unknownFields:LX/EZQ;

    .line 2147488
    return-void
.end method

.method public static a(LX/Ece;)LX/Ecd;
    .locals 1

    .prologue
    .line 2147483
    invoke-static {}, LX/Ecd;->w()LX/Ecd;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/Ecd;->a(LX/Ece;)LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method private x()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2147479
    iput v0, p0, LX/Ece;->preKeyId_:I

    .line 2147480
    iput v0, p0, LX/Ece;->signedPreKeyId_:I

    .line 2147481
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ece;->baseKey_:LX/EWc;

    .line 2147482
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2147477
    new-instance v0, LX/Ecd;

    invoke-direct {v0, p1}, LX/Ecd;-><init>(LX/EYd;)V

    .line 2147478
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2147461
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2147462
    iget v0, p0, LX/Ece;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2147463
    iget v0, p0, LX/Ece;->preKeyId_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2147464
    :cond_0
    iget v0, p0, LX/Ece;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 2147465
    iget-object v0, p0, LX/Ece;->baseKey_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2147466
    :cond_1
    iget v0, p0, LX/Ece;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 2147467
    const/4 v0, 0x3

    iget v1, p0, LX/Ece;->signedPreKeyId_:I

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(II)V

    .line 2147468
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2147469
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2147472
    iget-byte v1, p0, LX/Ece;->memoizedIsInitialized:B

    .line 2147473
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2147474
    :goto_0
    return v0

    .line 2147475
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2147476
    :cond_1
    iput-byte v0, p0, LX/Ece;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2147530
    iget v0, p0, LX/Ece;->memoizedSerializedSize:I

    .line 2147531
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2147532
    :goto_0
    return v0

    .line 2147533
    :cond_0
    const/4 v0, 0x0

    .line 2147534
    iget v1, p0, LX/Ece;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2147535
    iget v0, p0, LX/Ece;->preKeyId_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2147536
    :cond_1
    iget v1, p0, LX/Ece;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 2147537
    iget-object v1, p0, LX/Ece;->baseKey_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147538
    :cond_2
    iget v1, p0, LX/Ece;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 2147539
    const/4 v1, 0x3

    iget v2, p0, LX/Ece;->signedPreKeyId_:I

    invoke-static {v1, v2}, LX/EWf;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147540
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2147541
    iput v0, p0, LX/Ece;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2147471
    iget-object v0, p0, LX/Ece;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2147470
    sget-object v0, LX/Eck;->l:LX/EYn;

    const-class v1, LX/Ece;

    const-class v2, LX/Ecd;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ece;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2147460
    sget-object v0, LX/Ece;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2147459
    iget v1, p0, LX/Ece;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()LX/Ecd;
    .locals 1

    .prologue
    .line 2147458
    invoke-static {p0}, LX/Ece;->a(LX/Ece;)LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2147457
    invoke-virtual {p0}, LX/Ece;->q()LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2147456
    invoke-static {}, LX/Ecd;->w()LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2147455
    invoke-virtual {p0}, LX/Ece;->q()LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2147454
    sget-object v0, LX/Ece;->c:LX/Ece;

    return-object v0
.end method
