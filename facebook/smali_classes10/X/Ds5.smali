.class public LX/Ds5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Drs;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Ds5;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Drl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/2c4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0SI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final g:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2049995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049996
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/Ds5;->g:Ljava/util/Random;

    .line 2049997
    return-void
.end method

.method public static a(LX/0QB;)LX/Ds5;
    .locals 9

    .prologue
    .line 2049998
    sget-object v0, LX/Ds5;->h:LX/Ds5;

    if-nez v0, :cond_1

    .line 2049999
    const-class v1, LX/Ds5;

    monitor-enter v1

    .line 2050000
    :try_start_0
    sget-object v0, LX/Ds5;->h:LX/Ds5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2050001
    if-eqz v2, :cond_0

    .line 2050002
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2050003
    new-instance v3, LX/Ds5;

    invoke-direct {v3}, LX/Ds5;-><init>()V

    .line 2050004
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/Drl;->b(LX/0QB;)LX/Drl;

    move-result-object v7

    check-cast v7, LX/Drl;

    invoke-static {v0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v8

    check-cast v8, LX/2c4;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object p0

    check-cast p0, LX/0SI;

    .line 2050005
    iput-object v4, v3, LX/Ds5;->a:Landroid/content/Context;

    iput-object v5, v3, LX/Ds5;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v6, v3, LX/Ds5;->c:LX/0tX;

    iput-object v7, v3, LX/Ds5;->d:LX/Drl;

    iput-object v8, v3, LX/Ds5;->e:LX/2c4;

    iput-object p0, v3, LX/Ds5;->f:LX/0SI;

    .line 2050006
    move-object v0, v3

    .line 2050007
    sput-object v0, LX/Ds5;->h:LX/Ds5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2050008
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2050009
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2050010
    :cond_1
    sget-object v0, LX/Ds5;->h:LX/Ds5;

    return-object v0

    .line 2050011
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2050012
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Drw;)LX/3pb;
    .locals 5

    .prologue
    .line 2050013
    iget-object v0, p1, LX/Drw;->c:Lorg/json/JSONObject;

    move-object v1, v0

    .line 2050014
    iget-object v2, p0, LX/Ds5;->a:Landroid/content/Context;

    .line 2050015
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v3, v0

    .line 2050016
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 2050017
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v0, v0

    .line 2050018
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v3, v4, v0}, Lcom/facebook/notifications/tray/actions/PushNotificationsActionService;->a(Landroid/content/Context;Lcom/facebook/notifications/model/SystemTrayNotification;Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2050019
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2050020
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_PRIMARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_PRIMARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2050021
    iget-object v2, p0, LX/Ds5;->a:Landroid/content/Context;

    iget-object v3, p0, LX/Ds5;->g:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2050022
    new-instance v2, LX/3pX;

    const v3, 0x7f02156a

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v2}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2050023
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2050024
    const-string v0, "notification_id_extra"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2050025
    iget-object v0, p0, LX/Ds5;->f:LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ds5;->f:LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2050026
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2050027
    :goto_0
    new-instance v3, LX/4Eo;

    invoke-direct {v3}, LX/4Eo;-><init>()V

    invoke-virtual {v3, v0}, LX/4Eo;->a(Ljava/lang/String;)LX/4Eo;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/4Eo;->b(Ljava/lang/String;)LX/4Eo;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Eo;->a(Ljava/lang/Integer;)LX/4Eo;

    move-result-object v0

    .line 2050028
    invoke-static {}, LX/5Cv;->b()LX/15S;

    move-result-object v1

    .line 2050029
    const-string v3, "input"

    invoke-virtual {v1, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2050030
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2050031
    iget-object v1, p0, LX/Ds5;->f:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2050032
    iput-object v1, v0, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2050033
    iget-object v1, p0, LX/Ds5;->c:LX/0tX;

    sget-object v3, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v1, v0, v3}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2050034
    new-instance v1, LX/Ds4;

    invoke-direct {v1, p0, p1, v2}, LX/Ds4;-><init>(LX/Ds5;Landroid/content/Intent;Ljava/lang/String;)V

    iget-object v2, p0, LX/Ds5;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2050035
    return v4

    .line 2050036
    :cond_0
    iget-object v0, p0, LX/Ds5;->f:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2050037
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2050038
    goto :goto_0
.end method
