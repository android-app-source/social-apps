.class public LX/Dux;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static b:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;",
            "LX/Dv0;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DwH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2057901
    new-instance v0, LX/0aq;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    sput-object v0, LX/Dux;->b:LX/0aq;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DwH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2057898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057899
    iput-object p1, p0, LX/Dux;->a:LX/0Ot;

    .line 2057900
    return-void
.end method

.method public static a(LX/0QB;)LX/Dux;
    .locals 4

    .prologue
    .line 2057902
    const-class v1, LX/Dux;

    monitor-enter v1

    .line 2057903
    :try_start_0
    sget-object v0, LX/Dux;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2057904
    sput-object v2, LX/Dux;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2057905
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2057906
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2057907
    new-instance v3, LX/Dux;

    const/16 p0, 0x2e9c

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Dux;-><init>(LX/0Ot;)V

    .line 2057908
    move-object v0, v3

    .line 2057909
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2057910
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Dux;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2057911
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2057912
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;)LX/Dv0;
    .locals 2

    .prologue
    .line 2057891
    if-nez p1, :cond_0

    .line 2057892
    const/4 v0, 0x0

    .line 2057893
    :goto_0
    return-object v0

    .line 2057894
    :cond_0
    sget-object v0, LX/Dux;->b:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dv0;

    .line 2057895
    if-nez v0, :cond_1

    .line 2057896
    new-instance v0, LX/Dv0;

    iget-object v1, p0, LX/Dux;->a:LX/0Ot;

    invoke-direct {v0, v1}, LX/Dv0;-><init>(LX/0Ot;)V

    .line 2057897
    :cond_1
    sget-object v1, LX/Dux;->b:LX/0aq;

    invoke-virtual {v1, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2057889
    sget-object v0, LX/Dux;->b:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->a()V

    .line 2057890
    return-void
.end method
