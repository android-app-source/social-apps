.class public LX/DAO;
.super LX/3OP;
.source ""


# instance fields
.field private a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z


# virtual methods
.method public final a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "ARG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3L9",
            "<TT;TARG;>;TARG;)TT;"
        }
    .end annotation

    .prologue
    .line 1971211
    invoke-interface {p1, p0, p2}, LX/3L9;->a(LX/DAO;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1971212
    iput-boolean p1, p0, LX/DAO;->a:Z

    .line 1971213
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1971214
    iget-boolean v0, p0, LX/DAO;->a:Z

    return v0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1971215
    iput-boolean p1, p0, LX/DAO;->b:Z

    .line 1971216
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1971217
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 1971218
    :cond_0
    :goto_0
    return v0

    .line 1971219
    :cond_1
    check-cast p1, LX/DAO;

    .line 1971220
    invoke-virtual {p1}, LX/3OP;->a()Z

    move-result v1

    invoke-virtual {p0}, LX/3OP;->a()Z

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1971221
    iget-boolean v1, p1, LX/DAO;->b:Z

    move v1, v1

    .line 1971222
    iget-boolean v2, p0, LX/DAO;->b:Z

    move v2, v2

    .line 1971223
    if-ne v1, v2, :cond_0

    .line 1971224
    iget-boolean v1, p1, LX/DAO;->c:Z

    move v1, v1

    .line 1971225
    iget-boolean v2, p0, LX/DAO;->c:Z

    move v2, v2

    .line 1971226
    if-ne v1, v2, :cond_0

    .line 1971227
    iget-boolean v1, p1, LX/DAO;->d:Z

    move v1, v1

    .line 1971228
    iget-boolean v2, p0, LX/DAO;->d:Z

    move v2, v2

    .line 1971229
    if-ne v1, v2, :cond_0

    .line 1971230
    iget-boolean v1, p1, LX/DAO;->e:Z

    move v1, v1

    .line 1971231
    iget-boolean v2, p0, LX/DAO;->e:Z

    move v2, v2

    .line 1971232
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1971233
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, LX/DAO;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/DAO;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/DAO;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, LX/DAO;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, LX/DAO;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
