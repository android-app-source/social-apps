.class public final LX/D9S;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Landroid/graphics/Paint;

.field private final c:I

.field private d:I

.field private e:I

.field private f:Landroid/graphics/LinearGradient;

.field private g:Landroid/graphics/NinePatch;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;I)V
    .locals 3

    .prologue
    .line 1970111
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1970112
    const v0, -0x5ff7b01

    iput v0, p0, LX/D9S;->d:I

    .line 1970113
    const v0, -0x5ff820e

    iput v0, p0, LX/D9S;->e:I

    .line 1970114
    iput-object p1, p0, LX/D9S;->a:Landroid/content/res/Resources;

    .line 1970115
    iput p2, p0, LX/D9S;->c:I

    .line 1970116
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/D9S;->b:Landroid/graphics/Paint;

    .line 1970117
    iget-object v0, p0, LX/D9S;->b:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1970118
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1970119
    iget v0, p0, LX/D9S;->d:I

    if-ne v0, p1, :cond_0

    iget v0, p0, LX/D9S;->e:I

    if-ne v0, p2, :cond_0

    .line 1970120
    :goto_0
    return-void

    .line 1970121
    :cond_0
    iput p1, p0, LX/D9S;->d:I

    .line 1970122
    iput p2, p0, LX/D9S;->e:I

    .line 1970123
    const/4 v0, 0x0

    iput-object v0, p0, LX/D9S;->f:Landroid/graphics/LinearGradient;

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 1970124
    invoke-virtual {p0}, LX/D9S;->getBounds()Landroid/graphics/Rect;

    move-result-object v8

    .line 1970125
    iget-object v0, p0, LX/D9S;->f:Landroid/graphics/LinearGradient;

    if-nez v0, :cond_0

    .line 1970126
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, v8, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v8, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v8, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v4, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    iget v5, p0, LX/D9S;->d:I

    iget v6, p0, LX/D9S;->e:I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, LX/D9S;->f:Landroid/graphics/LinearGradient;

    .line 1970127
    iget-object v0, p0, LX/D9S;->b:Landroid/graphics/Paint;

    iget-object v1, p0, LX/D9S;->f:Landroid/graphics/LinearGradient;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1970128
    :cond_0
    iget-object v0, p0, LX/D9S;->g:Landroid/graphics/NinePatch;

    if-nez v0, :cond_1

    .line 1970129
    iget-object v0, p0, LX/D9S;->a:Landroid/content/res/Resources;

    iget v1, p0, LX/D9S;->c:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1970130
    new-instance v1, Landroid/graphics/NinePatch;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    iput-object v1, p0, LX/D9S;->g:Landroid/graphics/NinePatch;

    .line 1970131
    :cond_1
    iget-object v0, p0, LX/D9S;->g:Landroid/graphics/NinePatch;

    invoke-virtual {v0, p1, v8}, Landroid/graphics/NinePatch;->draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 1970132
    iget-object v0, p0, LX/D9S;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1970133
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1970134
    const/4 v0, -0x3

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1970135
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 1970136
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1970137
    iget v1, p0, LX/D9S;->h:I

    if-eq v0, v1, :cond_0

    .line 1970138
    iput v0, p0, LX/D9S;->h:I

    .line 1970139
    const/4 v0, 0x0

    iput-object v0, p0, LX/D9S;->f:Landroid/graphics/LinearGradient;

    .line 1970140
    :cond_0
    return-void
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 1970141
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1    # Landroid/graphics/ColorFilter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1970142
    iget-object v0, p0, LX/D9S;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1970143
    return-void
.end method
