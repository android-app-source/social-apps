.class public final LX/EQy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 2120014
    const/16 v18, 0x0

    .line 2120015
    const/16 v17, 0x0

    .line 2120016
    const/16 v16, 0x0

    .line 2120017
    const/4 v15, 0x0

    .line 2120018
    const/4 v14, 0x0

    .line 2120019
    const/4 v13, 0x0

    .line 2120020
    const/4 v12, 0x0

    .line 2120021
    const/4 v11, 0x0

    .line 2120022
    const/4 v10, 0x0

    .line 2120023
    const/4 v9, 0x0

    .line 2120024
    const/4 v8, 0x0

    .line 2120025
    const/4 v7, 0x0

    .line 2120026
    const/4 v6, 0x0

    .line 2120027
    const/4 v5, 0x0

    .line 2120028
    const/4 v4, 0x0

    .line 2120029
    const/4 v3, 0x0

    .line 2120030
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 2120031
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2120032
    const/4 v3, 0x0

    .line 2120033
    :goto_0
    return v3

    .line 2120034
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2120035
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_d

    .line 2120036
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 2120037
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2120038
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 2120039
    const-string v20, "action_link"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 2120040
    invoke-static/range {p0 .. p1}, LX/EQt;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 2120041
    :cond_2
    const-string v20, "action_link_if_moments_installed"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 2120042
    invoke-static/range {p0 .. p1}, LX/EQs;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 2120043
    :cond_3
    const-string v20, "always_show_interstitial"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 2120044
    const/4 v6, 0x1

    .line 2120045
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 2120046
    :cond_4
    const-string v20, "badge_count"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 2120047
    const/4 v5, 0x1

    .line 2120048
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto :goto_1

    .line 2120049
    :cond_5
    const-string v20, "description"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 2120050
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 2120051
    :cond_6
    const-string v20, "facepile_users"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 2120052
    invoke-static/range {p0 .. p1}, LX/EQu;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 2120053
    :cond_7
    const-string v20, "help_link"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 2120054
    invoke-static/range {p0 .. p1}, LX/EQv;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 2120055
    :cond_8
    const-string v20, "image"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 2120056
    invoke-static/range {p0 .. p1}, LX/EQw;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 2120057
    :cond_9
    const-string v20, "interstitial_xout_blocks_tab"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 2120058
    const/4 v4, 0x1

    .line 2120059
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 2120060
    :cond_a
    const-string v20, "replace_synced_tab_with_moments_tab"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 2120061
    const/4 v3, 0x1

    .line 2120062
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 2120063
    :cond_b
    const-string v20, "social_context"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 2120064
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 2120065
    :cond_c
    const-string v20, "title"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 2120066
    invoke-static/range {p0 .. p1}, LX/EQx;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 2120067
    :cond_d
    const/16 v19, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2120068
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2120069
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2120070
    if-eqz v6, :cond_e

    .line 2120071
    const/4 v6, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 2120072
    :cond_e
    if-eqz v5, :cond_f

    .line 2120073
    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15, v6}, LX/186;->a(III)V

    .line 2120074
    :cond_f
    const/4 v5, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 2120075
    const/4 v5, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 2120076
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->b(II)V

    .line 2120077
    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 2120078
    if-eqz v4, :cond_10

    .line 2120079
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 2120080
    :cond_10
    if-eqz v3, :cond_11

    .line 2120081
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 2120082
    :cond_11
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 2120083
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2120084
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2120085
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2120086
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2120087
    if-eqz v0, :cond_3

    .line 2120088
    const-string v1, "action_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120089
    const/4 v3, 0x0

    .line 2120090
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2120091
    invoke-virtual {p0, v0, v3}, LX/15i;->g(II)I

    move-result v1

    .line 2120092
    if-eqz v1, :cond_0

    .line 2120093
    const-string v1, "__type__"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120094
    invoke-static {p0, v0, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2120095
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2120096
    if-eqz v1, :cond_1

    .line 2120097
    const-string v3, "title"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120098
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2120099
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2120100
    if-eqz v1, :cond_2

    .line 2120101
    const-string v3, "url"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120102
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2120103
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2120104
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2120105
    if-eqz v0, :cond_7

    .line 2120106
    const-string v1, "action_link_if_moments_installed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120107
    const/4 v3, 0x0

    .line 2120108
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2120109
    invoke-virtual {p0, v0, v3}, LX/15i;->g(II)I

    move-result v1

    .line 2120110
    if-eqz v1, :cond_4

    .line 2120111
    const-string v1, "__type__"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120112
    invoke-static {p0, v0, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2120113
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2120114
    if-eqz v1, :cond_5

    .line 2120115
    const-string v3, "title"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120116
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2120117
    :cond_5
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2120118
    if-eqz v1, :cond_6

    .line 2120119
    const-string v3, "url"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120120
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2120121
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2120122
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2120123
    if-eqz v0, :cond_8

    .line 2120124
    const-string v1, "always_show_interstitial"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120125
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2120126
    :cond_8
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2120127
    if-eqz v0, :cond_9

    .line 2120128
    const-string v1, "badge_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120129
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2120130
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2120131
    if-eqz v0, :cond_a

    .line 2120132
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120133
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2120134
    :cond_a
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2120135
    if-eqz v0, :cond_c

    .line 2120136
    const-string v1, "facepile_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120137
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2120138
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_b

    .line 2120139
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/EQu;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2120140
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2120141
    :cond_b
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2120142
    :cond_c
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2120143
    if-eqz v0, :cond_10

    .line 2120144
    const-string v1, "help_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120145
    const/4 v2, 0x0

    .line 2120146
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2120147
    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v1

    .line 2120148
    if-eqz v1, :cond_d

    .line 2120149
    const-string v1, "__type__"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120150
    invoke-static {p0, v0, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2120151
    :cond_d
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2120152
    if-eqz v1, :cond_e

    .line 2120153
    const-string v2, "title"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120154
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2120155
    :cond_e
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2120156
    if-eqz v1, :cond_f

    .line 2120157
    const-string v2, "url"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120158
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2120159
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2120160
    :cond_10
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2120161
    if-eqz v0, :cond_12

    .line 2120162
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120163
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2120164
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2120165
    if-eqz v1, :cond_11

    .line 2120166
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120167
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2120168
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2120169
    :cond_12
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2120170
    if-eqz v0, :cond_13

    .line 2120171
    const-string v1, "interstitial_xout_blocks_tab"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120172
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2120173
    :cond_13
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2120174
    if-eqz v0, :cond_14

    .line 2120175
    const-string v1, "replace_synced_tab_with_moments_tab"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120176
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2120177
    :cond_14
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2120178
    if-eqz v0, :cond_15

    .line 2120179
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120180
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2120181
    :cond_15
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2120182
    if-eqz v0, :cond_17

    .line 2120183
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120184
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2120185
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2120186
    if-eqz v1, :cond_16

    .line 2120187
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2120188
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2120189
    :cond_16
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2120190
    :cond_17
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2120191
    return-void
.end method
