.class public final LX/Elo;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;)V
    .locals 0

    .prologue
    .line 2164736
    iput-object p1, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2164737
    iget-object v0, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    iget-object v1, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    invoke-virtual {v1}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->a(Landroid/net/Uri;)V

    .line 2164738
    iget-object v0, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    iget-object v0, v0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->s:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_onActivityCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error retrieving URL info"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2164739
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2164740
    move-object v1, v1

    .line 2164741
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2164742
    iget-object v0, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    invoke-virtual {v0}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->finish()V

    .line 2164743
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2164744
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2164745
    iget-object v0, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    iget-object v1, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    invoke-virtual {v1}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->a(Landroid/net/Uri;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2164746
    iget-object v0, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    invoke-virtual {v0}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->finish()V

    .line 2164747
    return-void
.end method

.method public final onThrowable(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2164748
    invoke-super {p0, p1}, LX/2h1;->onThrowable(Ljava/lang/Throwable;)V

    .line 2164749
    iget-object v0, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    iget-object v1, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    invoke-virtual {v1}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->a(Landroid/net/Uri;)V

    .line 2164750
    iget-object v0, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    iget-object v0, v0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->s:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_onActivityCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error retrieving URL info"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2164751
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2164752
    move-object v1, v1

    .line 2164753
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2164754
    iget-object v0, p0, LX/Elo;->a:Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    invoke-virtual {v0}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->finish()V

    .line 2164755
    return-void
.end method
