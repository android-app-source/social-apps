.class public LX/CqR;
.super LX/1P0;
.source ""

# interfaces
.implements LX/CqN;


# static fields
.field private static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/8YR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/CqQ;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/support/v7/widget/RecyclerView;

.field private g:LX/CtA;

.field public h:LX/1Od;

.field private i:Z

.field public t:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1939785
    new-instance v0, LX/CqO;

    invoke-direct {v0}, LX/CqO;-><init>()V

    sput-object v0, LX/CqR;->e:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/widget/RecyclerView;)V
    .locals 4

    .prologue
    .line 1939851
    invoke-direct {p0, p1}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 1939852
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/CqR;->d:LX/0YU;

    .line 1939853
    iput-object p2, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    .line 1939854
    iget-object v0, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/CqP;

    invoke-direct {v1, p0}, LX/CqP;-><init>(LX/CqR;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setViewCacheExtension(LX/1PB;)V

    .line 1939855
    const-class v0, LX/CqR;

    invoke-static {v0, p0, p1}, LX/CqR;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1939856
    sget-object v0, LX/CqR;->e:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/CqR;->a:LX/8YR;

    const/4 p2, 0x3

    .line 1939857
    invoke-static {p1}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v3

    .line 1939858
    const/16 p0, 0x7dd

    if-lt v3, p0, :cond_0

    .line 1939859
    iget-object v3, v2, LX/8YR;->a:LX/0ad;

    sget p0, LX/2yD;->T:I

    invoke-interface {v3, p0, p2}, LX/0ad;->a(II)I

    move-result v3

    .line 1939860
    :goto_0
    move v2, v3

    .line 1939861
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939862
    return-void

    .line 1939863
    :cond_0
    const/16 p0, 0x7dc

    if-ne v3, p0, :cond_1

    .line 1939864
    iget-object v3, v2, LX/8YR;->a:LX/0ad;

    sget p0, LX/2yD;->S:I

    invoke-interface {v3, p0, p2}, LX/0ad;->a(II)I

    move-result v3

    goto :goto_0

    .line 1939865
    :cond_1
    iget-object v3, v2, LX/8YR;->a:LX/0ad;

    sget p0, LX/2yD;->R:I

    invoke-interface {v3, p0, p2}, LX/0ad;->a(II)I

    move-result v3

    goto :goto_0
.end method

.method private static a(LX/1a1;)V
    .locals 1

    .prologue
    .line 1939848
    check-cast p0, LX/Cs4;

    invoke-virtual {p0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    check-cast v0, LX/CoB;

    .line 1939849
    invoke-virtual {v0}, LX/CoB;->e()V

    .line 1939850
    return-void
.end method

.method public static synthetic a(LX/CqR;Landroid/view/View;LX/1Od;)V
    .locals 0

    .prologue
    .line 1939847
    invoke-super {p0, p1, p2}, LX/1P0;->a(Landroid/view/View;LX/1Od;)V

    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/CqR;

    invoke-static {v2}, LX/8YR;->a(LX/0QB;)LX/8YR;

    move-result-object v0

    check-cast v0, LX/8YR;

    invoke-static {v2}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 p0, 0x259

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v0, p1, LX/CqR;->a:LX/8YR;

    iput-object v1, p1, LX/CqR;->b:LX/0Uh;

    iput-object v2, p1, LX/CqR;->c:LX/0Ot;

    return-void
.end method

.method public static b(LX/1a1;)V
    .locals 2

    .prologue
    .line 1939842
    instance-of v0, p0, LX/Cs4;

    if-eqz v0, :cond_0

    .line 1939843
    check-cast p0, LX/Cs4;

    invoke-virtual {p0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 1939844
    if-eqz v0, :cond_0

    .line 1939845
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 1939846
    :cond_0
    return-void
.end method

.method private c(Landroid/view/View;I)V
    .locals 5

    .prologue
    .line 1939822
    iget-object v0, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v2

    .line 1939823
    invoke-virtual {p0, p1}, LX/1OR;->e(Landroid/view/View;)V

    .line 1939824
    iget v0, v2, LX/1a1;->e:I

    move v3, v0

    .line 1939825
    iget-object v0, p0, LX/CqR;->d:LX/0YU;

    invoke-virtual {v0, v3}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqQ;

    .line 1939826
    if-nez v0, :cond_0

    .line 1939827
    new-instance v1, LX/CqQ;

    sget-object v0, LX/CqR;->e:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    sget-object v0, LX/CqR;->e:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    invoke-direct {v1, p0, v0}, LX/CqQ;-><init>(LX/CqR;I)V

    .line 1939828
    iget-object v0, p0, LX/CqR;->d:LX/0YU;

    invoke-virtual {v0, v3, v1}, LX/0YU;->a(ILjava/lang/Object;)V

    move-object v0, v1

    .line 1939829
    :cond_0
    iget-object v1, v0, LX/CqQ;->d:LX/CqR;

    iget-object v3, v2, LX/1a1;->a:Landroid/view/View;

    invoke-static {v1, v3}, LX/CqR;->n(LX/CqR;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1939830
    invoke-static {v0, p2}, LX/CqQ;->d(LX/CqQ;I)I

    move-result v1

    .line 1939831
    if-eq v1, p2, :cond_2

    .line 1939832
    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 1939833
    iget-object v3, v0, LX/CqQ;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1a1;

    .line 1939834
    if-eqz v3, :cond_1

    .line 1939835
    iget-object v4, v0, LX/CqQ;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v4, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939836
    iget-object v4, v0, LX/CqQ;->d:LX/CqR;

    iget-object p0, v3, LX/1a1;->a:Landroid/view/View;

    iget-object p1, v0, LX/CqQ;->d:LX/CqR;

    iget-object p1, p1, LX/CqR;->h:LX/1Od;

    invoke-static {v4, p0, p1}, LX/CqR;->a(LX/CqR;Landroid/view/View;LX/1Od;)V

    .line 1939837
    invoke-static {v3}, LX/CqR;->b(LX/1a1;)V

    .line 1939838
    :cond_1
    iget-object v1, v0, LX/CqQ;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939839
    :cond_2
    :goto_1
    return-void

    .line 1939840
    :cond_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1939841
    :cond_4
    iget-object v1, v0, LX/CqQ;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static d(LX/CqR;LX/1Od;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1939802
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqR;->i:Z

    move v3, v4

    .line 1939803
    :goto_0
    iget-object v0, p0, LX/CqR;->d:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 1939804
    iget-object v0, p0, LX/CqR;->d:LX/0YU;

    iget-object v1, p0, LX/CqR;->d:LX/0YU;

    invoke-virtual {v1, v3}, LX/0YU;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqQ;

    move v5, v4

    .line 1939805
    :goto_1
    iget-object v1, v0, LX/CqQ;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_1

    .line 1939806
    iget-object v1, v0, LX/CqQ;->a:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    .line 1939807
    iget-object v2, v1, LX/1a1;->a:Landroid/view/View;

    invoke-super {p0, v2, p1}, LX/1P0;->a(Landroid/view/View;LX/1Od;)V

    .line 1939808
    invoke-static {v1}, LX/CqR;->b(LX/1a1;)V

    .line 1939809
    instance-of v2, v1, LX/Cs4;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, LX/Cs4;

    invoke-virtual {v2}, LX/Cs4;->w()LX/CnT;

    move-result-object v2

    instance-of v2, v2, LX/CoB;

    if-eqz v2, :cond_0

    .line 1939810
    invoke-static {v1}, LX/CqR;->a(LX/1a1;)V

    .line 1939811
    :cond_0
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 1939812
    :cond_1
    iget-object v0, v0, LX/CqQ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1939813
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 1939814
    iget-object v1, v0, LX/1a1;->a:Landroid/view/View;

    invoke-super {p0, v1, p1}, LX/1P0;->a(Landroid/view/View;LX/1Od;)V

    .line 1939815
    invoke-static {v0}, LX/CqR;->b(LX/1a1;)V

    .line 1939816
    instance-of v1, v0, LX/Cs4;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, LX/Cs4;

    invoke-virtual {v1}, LX/Cs4;->w()LX/CnT;

    move-result-object v1

    instance-of v1, v1, LX/CoB;

    if-eqz v1, :cond_2

    .line 1939817
    invoke-static {v0}, LX/CqR;->a(LX/1a1;)V

    goto :goto_2

    .line 1939818
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1939819
    :cond_4
    iget-object v0, p0, LX/CqR;->d:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->b()V

    .line 1939820
    iput-boolean v4, p0, LX/CqR;->i:Z

    .line 1939821
    return-void
.end method

.method private m(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 1939795
    iget-object v0, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 1939796
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Cs4;

    if-eqz v1, :cond_0

    .line 1939797
    check-cast v0, LX/Cs4;

    .line 1939798
    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 1939799
    instance-of v1, v0, LX/Cnx;

    if-eqz v1, :cond_0

    .line 1939800
    check-cast v0, LX/Cnx;

    invoke-interface {v0}, LX/Cnx;->c()Z

    move-result v0

    .line 1939801
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(LX/CqR;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 1939788
    iget-object v0, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 1939789
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Cs4;

    if-eqz v1, :cond_0

    .line 1939790
    check-cast v0, LX/Cs4;

    .line 1939791
    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 1939792
    instance-of v1, v0, LX/Cny;

    if-eqz v1, :cond_0

    .line 1939793
    check-cast v0, LX/Cny;

    invoke-interface {v0}, LX/Cny;->b()Z

    move-result v0

    .line 1939794
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/1Od;)V
    .locals 1

    .prologue
    .line 1939786
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/1OR;->a(Landroid/view/View;LX/1Od;)V

    .line 1939787
    return-void
.end method

.method public final a(LX/1OM;LX/1OM;)V
    .locals 2

    .prologue
    .line 1939866
    invoke-super {p0, p1, p2}, LX/1P0;->a(LX/1OM;LX/1OM;)V

    .line 1939867
    new-instance v0, LX/CtA;

    iget-object v1, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast p2, LX/CoJ;

    invoke-direct {v0, v1, p0, p2}, LX/CtA;-><init>(Landroid/content/Context;LX/1P1;LX/CoJ;)V

    iput-object v0, p0, LX/CqR;->g:LX/CtA;

    .line 1939868
    return-void
.end method

.method public final a(LX/1Od;)V
    .locals 3

    .prologue
    .line 1939729
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1939730
    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 1939731
    invoke-direct {p0, v1}, LX/CqR;->m(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1939732
    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v2

    invoke-direct {p0, v1, v2}, LX/CqR;->c(Landroid/view/View;I)V

    .line 1939733
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1939734
    :cond_1
    invoke-super {p0, p1}, LX/1P0;->a(LX/1Od;)V

    .line 1939735
    return-void
.end method

.method public final a(LX/1Od;LX/1Ok;II)V
    .locals 0

    .prologue
    .line 1939726
    invoke-super {p0, p1, p2, p3, p4}, LX/1P0;->a(LX/1Od;LX/1Ok;II)V

    .line 1939727
    iput-object p1, p0, LX/CqR;->h:LX/1Od;

    .line 1939728
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V
    .locals 0

    .prologue
    .line 1939723
    invoke-super {p0, p1, p2}, LX/1P0;->a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V

    .line 1939724
    invoke-static {p0, p2}, LX/CqR;->d(LX/CqR;LX/1Od;)V

    .line 1939725
    return-void
.end method

.method public final a(Landroid/view/View;LX/1Od;)V
    .locals 2

    .prologue
    .line 1939716
    invoke-direct {p0, p1}, LX/CqR;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939717
    iget-object v0, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    .line 1939718
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 1939719
    iget-object v1, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1OM;->d(LX/1a1;)V

    .line 1939720
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/CqR;->c(Landroid/view/View;I)V

    .line 1939721
    :goto_0
    return-void

    .line 1939722
    :cond_0
    invoke-super {p0, p1, p2}, LX/1P0;->a(Landroid/view/View;LX/1Od;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;I)V
    .locals 5

    .prologue
    .line 1939708
    :try_start_0
    invoke-super {p0, p1, p2}, LX/1P0;->b(Landroid/view/View;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1939709
    :cond_0
    :goto_0
    return-void

    .line 1939710
    :catch_0
    move-exception v1

    .line 1939711
    iget-object v0, p0, LX/CqR;->b:LX/0Uh;

    const/16 v2, 0xaa

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1939712
    iget-object v0, p0, LX/CqR;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 1939713
    if-eqz v0, :cond_0

    .line 1939714
    const-string v2, "instant_articles"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error on block index "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1939715
    :cond_1
    throw v1
.end method

.method public final c(LX/1Ok;)I
    .locals 1

    .prologue
    .line 1939736
    iget-object v0, p0, LX/CqR;->g:LX/CtA;

    if-eqz v0, :cond_0

    .line 1939737
    iget-object v0, p0, LX/CqR;->g:LX/CtA;

    .line 1939738
    invoke-static {v0}, LX/CtA;->d(LX/CtA;)V

    .line 1939739
    iget p0, v0, LX/CtA;->f:I

    move v0, p0

    .line 1939740
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/1P0;->c(LX/1Ok;)I

    move-result v0

    goto :goto_0
.end method

.method public final c(LX/1Od;LX/1Ok;)V
    .locals 1

    .prologue
    .line 1939741
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqR;->t:Z

    .line 1939742
    invoke-super {p0, p1, p2}, LX/1P0;->c(LX/1Od;LX/1Ok;)V

    .line 1939743
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CqR;->t:Z

    .line 1939744
    return-void
.end method

.method public final c_(II)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1939745
    iget-object v0, p0, LX/CqR;->h:LX/1Od;

    if-nez v0, :cond_0

    move v0, v1

    .line 1939746
    :goto_0
    return v0

    .line 1939747
    :cond_0
    iget-boolean v0, p0, LX/CqR;->i:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 1939748
    goto :goto_0

    .line 1939749
    :cond_1
    if-ltz p1, :cond_2

    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v0

    if-lt p1, v0, :cond_3

    :cond_2
    move v0, v1

    .line 1939750
    goto :goto_0

    .line 1939751
    :cond_3
    invoke-virtual {p0}, LX/1P1;->l()I

    move-result v0

    if-lt p1, v0, :cond_4

    invoke-virtual {p0}, LX/1P1;->n()I

    move-result v0

    if-gt p1, v0, :cond_4

    move v0, v1

    .line 1939752
    goto :goto_0

    .line 1939753
    :cond_4
    iget-object v0, p0, LX/CqR;->d:LX/0YU;

    invoke-virtual {v0, p2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqQ;

    .line 1939754
    if-eqz v0, :cond_5

    iget-object v2, v0, LX/CqQ;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    move v0, v1

    .line 1939755
    goto :goto_0

    .line 1939756
    :cond_5
    if-eqz v0, :cond_6

    .line 1939757
    invoke-static {v0, p1}, LX/CqQ;->d(LX/CqQ;I)I

    move-result v2

    if-eq v2, p1, :cond_8

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 1939758
    if-nez v0, :cond_6

    move v0, v1

    .line 1939759
    goto :goto_0

    .line 1939760
    :cond_6
    :try_start_0
    iget-object v0, p0, LX/CqR;->h:LX/1Od;

    invoke-virtual {v0, p1}, LX/1Od;->c(I)Landroid/view/View;

    move-result-object v2

    .line 1939761
    iget-object v0, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    .line 1939762
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v3

    .line 1939763
    instance-of v0, v0, LX/CoJ;

    if-eqz v0, :cond_7

    .line 1939764
    iget-object v0, p0, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    .line 1939765
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v3

    .line 1939766
    check-cast v0, LX/CoJ;

    .line 1939767
    iget-boolean v3, v0, LX/CoJ;->B:Z

    if-nez v3, :cond_7

    .line 1939768
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/CoJ;->B:Z

    .line 1939769
    :cond_7
    invoke-virtual {p0, v2}, LX/1OR;->b(Landroid/view/View;)V

    .line 1939770
    new-instance v0, Lcom/facebook/richdocument/view/recycler/RichDocumentLayoutManager$2;

    invoke-direct {v0, p0, v2}, Lcom/facebook/richdocument/view/recycler/RichDocumentLayoutManager$2;-><init>(LX/CqR;Landroid/view/View;)V

    .line 1939771
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1939772
    invoke-direct {p0, v2, p1}, LX/CqR;->c(Landroid/view/View;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1939773
    const/4 v0, 0x1

    goto :goto_0

    .line 1939774
    :catch_0
    move v0, v1

    goto :goto_0

    :cond_8
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final e(LX/1Ok;)I
    .locals 1

    .prologue
    .line 1939775
    iget-object v0, p0, LX/CqR;->g:LX/CtA;

    if-eqz v0, :cond_0

    .line 1939776
    iget-object v0, p0, LX/CqR;->g:LX/CtA;

    .line 1939777
    invoke-static {v0}, LX/CtA;->d(LX/CtA;)V

    .line 1939778
    iget p0, v0, LX/CtA;->g:I

    move v0, p0

    .line 1939779
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/1P0;->e(LX/1Ok;)I

    move-result v0

    goto :goto_0
.end method

.method public final g(LX/1Ok;)I
    .locals 1

    .prologue
    .line 1939780
    iget-object v0, p0, LX/CqR;->g:LX/CtA;

    if-eqz v0, :cond_0

    .line 1939781
    iget-object v0, p0, LX/CqR;->g:LX/CtA;

    .line 1939782
    invoke-static {v0}, LX/CtA;->d(LX/CtA;)V

    .line 1939783
    iget p0, v0, LX/CtA;->e:I

    move v0, p0

    .line 1939784
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/1P0;->g(LX/1Ok;)I

    move-result v0

    goto :goto_0
.end method
