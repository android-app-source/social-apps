.class public LX/E2M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/3U9;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/E2M;


# instance fields
.field public final a:LX/E2B;

.field public final b:LX/1vg;

.field private final c:LX/E2i;


# direct methods
.method public constructor <init>(LX/E2B;LX/1vg;LX/E2i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2072676
    iput-object p1, p0, LX/E2M;->a:LX/E2B;

    .line 2072677
    iput-object p2, p0, LX/E2M;->b:LX/1vg;

    .line 2072678
    iput-object p3, p0, LX/E2M;->c:LX/E2i;

    .line 2072679
    return-void
.end method

.method public static a(LX/0QB;)LX/E2M;
    .locals 6

    .prologue
    .line 2072680
    sget-object v0, LX/E2M;->d:LX/E2M;

    if-nez v0, :cond_1

    .line 2072681
    const-class v1, LX/E2M;

    monitor-enter v1

    .line 2072682
    :try_start_0
    sget-object v0, LX/E2M;->d:LX/E2M;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072683
    if-eqz v2, :cond_0

    .line 2072684
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072685
    new-instance p0, LX/E2M;

    invoke-static {v0}, LX/E2B;->a(LX/0QB;)LX/E2B;

    move-result-object v3

    check-cast v3, LX/E2B;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-static {v0}, LX/E2i;->b(LX/0QB;)LX/E2i;

    move-result-object v5

    check-cast v5, LX/E2i;

    invoke-direct {p0, v3, v4, v5}, LX/E2M;-><init>(LX/E2B;LX/1vg;LX/E2i;)V

    .line 2072686
    move-object v0, p0

    .line 2072687
    sput-object v0, LX/E2M;->d:LX/E2M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072688
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072689
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072690
    :cond_1
    sget-object v0, LX/E2M;->d:LX/E2M;

    return-object v0

    .line 2072691
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072692
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1Pq;LX/5sc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/5sc;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2072693
    new-instance v0, LX/E2g;

    invoke-direct {v0, p1}, LX/E2g;-><init>(LX/5sc;)V

    .line 2072694
    check-cast p0, LX/1Pr;

    invoke-static {p0, p1, v0, p2}, LX/E2i;->a(LX/1Pr;LX/5sc;LX/E2g;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;LX/5sc;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/5sc;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "LX/5sc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2072695
    invoke-static {p4, p5, p3}, LX/E2M;->a(LX/1Pq;LX/5sc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v3

    .line 2072696
    if-nez v3, :cond_0

    const/4 v0, 0x1

    move v7, v0

    .line 2072697
    :goto_0
    invoke-static {p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    move-object v0, p4

    .line 2072698
    check-cast v0, LX/1Pr;

    invoke-static {v0, v7, p5, p3}, LX/E2i;->a(LX/1Pr;ZLX/5sc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2072699
    iget-object v8, p0, LX/E2M;->c:LX/E2i;

    new-instance v0, LX/E2L;

    move-object v1, p0

    move-object v2, p4

    move-object v4, p5

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LX/E2L;-><init>(LX/E2M;LX/1Pq;ZLX/5sc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v8, v7, p5, v0}, LX/E2i;->a(ZLX/5sc;LX/2h0;)V

    .line 2072700
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    aput-object v6, v0, v1

    invoke-interface {p4, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2072701
    check-cast p4, LX/3U9;

    invoke-interface {p4}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    .line 2072702
    iget-object v1, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2072703
    iget-object v2, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2072704
    sget-object v3, LX/Cfc;->SAVE_PAGE_TAP:LX/Cfc;

    invoke-virtual {v0, v1, v2, p2, v3}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 2072705
    return-void

    .line 2072706
    :cond_0
    const/4 v0, 0x0

    move v7, v0

    goto :goto_0
.end method
