.class public LX/EuJ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EuI;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2179433
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EuJ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2179430
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2179431
    iput-object p1, p0, LX/EuJ;->b:LX/0Ot;

    .line 2179432
    return-void
.end method

.method public static a(LX/0QB;)LX/EuJ;
    .locals 4

    .prologue
    .line 2179419
    const-class v1, LX/EuJ;

    monitor-enter v1

    .line 2179420
    :try_start_0
    sget-object v0, LX/EuJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2179421
    sput-object v2, LX/EuJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2179422
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2179423
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2179424
    new-instance v3, LX/EuJ;

    const/16 p0, 0x2231

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EuJ;-><init>(LX/0Ot;)V

    .line 2179425
    move-object v0, v3

    .line 2179426
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2179427
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EuJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2179428
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2179429
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 9

    .prologue
    .line 2179412
    check-cast p2, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;

    .line 2179413
    iget-object v0, p0, LX/EuJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;

    iget-object v1, p2, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    const/4 v6, 0x0

    .line 2179414
    sget-object v2, LX/0ax;->bE:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2179415
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2179416
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v5

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 2179417
    iget-object v3, v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v8, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2179418
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2179397
    check-cast p2, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;

    .line 2179398
    iget-object v0, p0, LX/EuJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;

    iget-object v1, p2, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-object v2, p2, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    const/4 p0, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2179399
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->m()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2179400
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    .line 2179401
    :goto_0
    move-object v0, v3

    .line 2179402
    return-object v0

    .line 2179403
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v6, :cond_3

    move v3, v4

    .line 2179404
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0207fa

    invoke-interface {v6, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v6

    .line 2179405
    const v7, 0x39f9bf29

    const/4 p2, 0x0

    invoke-static {p1, v7, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2179406
    invoke-interface {v6, v7}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->b:LX/EuG;

    invoke-virtual {v7, p1}, LX/EuG;->c(LX/1De;)LX/EuF;

    move-result-object v7

    invoke-virtual {v7, v1}, LX/EuF;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/EuF;

    move-result-object v7

    invoke-virtual {v7, v2}, LX/EuF;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/EuF;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->c:LX/EuD;

    invoke-virtual {v7, p1}, LX/EuD;->c(LX/1De;)LX/EuB;

    move-result-object v7

    invoke-virtual {v7, v1}, LX/EuB;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/EuB;

    move-result-object v7

    if-nez v3, :cond_4

    :goto_2
    invoke-virtual {v7, v4}, LX/EuB;->a(Z)LX/EuB;

    move-result-object v4

    sget-object v5, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    invoke-virtual {v4, v5}, LX/EuB;->a(LX/2h7;)LX/EuB;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    .line 2179407
    if-eqz v3, :cond_2

    .line 2179408
    iget-object v3, v0, Lcom/facebook/friending/center/components/FriendSuggestionItemComponentSpec;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Ets;

    invoke-virtual {v3, p1}, LX/Ets;->c(LX/1De;)LX/Etq;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/Etq;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Etq;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    .line 2179409
    :cond_2
    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto :goto_0

    :cond_3
    move v3, v5

    .line 2179410
    goto :goto_1

    :cond_4
    move v4, v5

    .line 2179411
    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2179391
    invoke-static {}, LX/1dS;->b()V

    .line 2179392
    iget v0, p1, LX/1dQ;->b:I

    .line 2179393
    packed-switch v0, :pswitch_data_0

    .line 2179394
    :goto_0
    return-object v2

    .line 2179395
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2179396
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/EuJ;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x39f9bf29
        :pswitch_0
    .end packed-switch
.end method
