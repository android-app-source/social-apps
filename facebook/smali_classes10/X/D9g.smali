.class public final LX/D9g;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/D9h;


# direct methods
.method public constructor <init>(LX/D9h;)V
    .locals 0

    .prologue
    .line 1970410
    iput-object p1, p0, LX/D9g;->a:LX/D9h;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 7

    .prologue
    .line 1970398
    iget-object v0, p0, LX/D9g;->a:LX/D9h;

    iget-object v0, v0, LX/D9h;->d:LX/0wd;

    if-ne p1, v0, :cond_1

    .line 1970399
    iget-object v0, p0, LX/D9g;->a:LX/D9h;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    .line 1970400
    iget-object v4, v0, LX/D9h;->c:LX/3Nk;

    double-to-float v5, v2

    .line 1970401
    iget-object v6, v4, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->setX(F)V

    .line 1970402
    invoke-static {v0}, LX/D9h;->b(LX/D9h;)V

    .line 1970403
    :cond_0
    :goto_0
    return-void

    .line 1970404
    :cond_1
    iget-object v0, p0, LX/D9g;->a:LX/D9h;

    iget-object v0, v0, LX/D9h;->e:LX/0wd;

    if-ne p1, v0, :cond_0

    .line 1970405
    iget-object v0, p0, LX/D9g;->a:LX/D9h;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    .line 1970406
    iget-object v4, v0, LX/D9h;->c:LX/3Nk;

    double-to-float v5, v2

    .line 1970407
    iget-object v6, v4, LX/3Nk;->a:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->setY(F)V

    .line 1970408
    invoke-static {v0}, LX/D9h;->b(LX/D9h;)V

    .line 1970409
    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1970388
    iget-object v0, p0, LX/D9g;->a:LX/D9h;

    .line 1970389
    iget-object v1, v0, LX/D9h;->d:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/D9h;->e:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1970390
    iget-object v1, v0, LX/D9h;->h:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v1, :cond_0

    .line 1970391
    iget-object v1, v0, LX/D9h;->h:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 p0, 0x0

    const p1, -0x491f22dc

    invoke-static {v1, p0, p1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1970392
    :cond_0
    iget-object v1, v0, LX/D9h;->g:LX/D9n;

    if-eqz v1, :cond_1

    .line 1970393
    iget-object v1, v0, LX/D9h;->g:LX/D9n;

    invoke-virtual {v1}, LX/D9n;->b()V

    .line 1970394
    :cond_1
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 1

    .prologue
    .line 1970395
    iget-object v0, p0, LX/D9g;->a:LX/D9h;

    iget-object v0, v0, LX/D9h;->g:LX/D9n;

    if-eqz v0, :cond_0

    .line 1970396
    iget-object v0, p0, LX/D9g;->a:LX/D9h;

    iget-object v0, v0, LX/D9h;->g:LX/D9n;

    invoke-virtual {v0}, LX/D9n;->a()V

    .line 1970397
    :cond_0
    return-void
.end method
