.class public LX/EIq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ECC;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/EIq;


# instance fields
.field private final a:Ljava/util/AbstractList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/AbstractList",
            "<",
            "LX/ECD;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private e:Lcom/facebook/rtc/voicemail/VoicemailHandler;

.field public f:LX/EIp;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/voicemail/VoicemailHandler;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2102426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2102427
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/EIq;->a:Ljava/util/AbstractList;

    .line 2102428
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2102429
    iput-object v0, p0, LX/EIq;->b:LX/0Ot;

    .line 2102430
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2102431
    iput-object v0, p0, LX/EIq;->c:LX/0Ot;

    .line 2102432
    iput-object p1, p0, LX/EIq;->e:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    .line 2102433
    iget-object v0, p0, LX/EIq;->e:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    .line 2102434
    iput-object p0, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->s:LX/ECC;

    .line 2102435
    sget-object v0, LX/EIp;->NONE:LX/EIp;

    iput-object v0, p0, LX/EIq;->f:LX/EIp;

    .line 2102436
    return-void
.end method

.method public static a(LX/0QB;)LX/EIq;
    .locals 5

    .prologue
    .line 2102437
    sget-object v0, LX/EIq;->g:LX/EIq;

    if-nez v0, :cond_1

    .line 2102438
    const-class v1, LX/EIq;

    monitor-enter v1

    .line 2102439
    :try_start_0
    sget-object v0, LX/EIq;->g:LX/EIq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2102440
    if-eqz v2, :cond_0

    .line 2102441
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2102442
    new-instance v4, LX/EIq;

    invoke-static {v0}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a(LX/0QB;)Lcom/facebook/rtc/voicemail/VoicemailHandler;

    move-result-object v3

    check-cast v3, Lcom/facebook/rtc/voicemail/VoicemailHandler;

    invoke-direct {v4, v3}, LX/EIq;-><init>(Lcom/facebook/rtc/voicemail/VoicemailHandler;)V

    .line 2102443
    const/16 v3, 0x271

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0x1430

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2102444
    iput-object v3, v4, LX/EIq;->b:LX/0Ot;

    iput-object p0, v4, LX/EIq;->c:LX/0Ot;

    .line 2102445
    move-object v0, v4

    .line 2102446
    sput-object v0, LX/EIq;->g:LX/EIq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2102447
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2102448
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2102449
    :cond_1
    sget-object v0, LX/EIq;->g:LX/EIq;

    return-object v0

    .line 2102450
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2102451
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/EIq;LX/EIp;)V
    .locals 3

    .prologue
    .line 2102360
    iget-object v0, p0, LX/EIq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2102361
    iget-object v0, p0, LX/EIq;->f:LX/EIp;

    if-ne v0, p1, :cond_1

    .line 2102362
    :cond_0
    return-void

    .line 2102363
    :cond_1
    iput-object p1, p0, LX/EIq;->f:LX/EIp;

    .line 2102364
    sget-object v0, LX/EIn;->a:[I

    iget-object v1, p0, LX/EIq;->f:LX/EIp;

    invoke-virtual {v1}, LX/EIp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2102365
    :goto_0
    iget-object v0, p0, LX/EIq;->a:Ljava/util/AbstractList;

    invoke-virtual {v0}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECD;

    .line 2102366
    iget-object v2, p0, LX/EIq;->f:LX/EIp;

    invoke-interface {v0, v2}, LX/ECD;->a(LX/EIp;)V

    goto :goto_1

    .line 2102367
    :pswitch_0
    iget-object v0, p0, LX/EIq;->d:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_2

    .line 2102368
    iget-object v0, p0, LX/EIq;->d:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2102369
    const/4 v0, 0x0

    iput-object v0, p0, LX/EIq;->d:Ljava/util/concurrent/Future;

    .line 2102370
    :cond_2
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a$redex0(LX/EIq;LX/EIo;)Z
    .locals 8

    .prologue
    .line 2102410
    iget-object v0, p0, LX/EIq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2102411
    iget-object v0, p0, LX/EIq;->e:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    .line 2102412
    iget-boolean v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->m:Z

    move v0, v1

    .line 2102413
    if-nez v0, :cond_0

    .line 2102414
    const/4 v0, 0x0

    .line 2102415
    :goto_0
    return v0

    .line 2102416
    :cond_0
    sget-object v0, LX/EIn;->b:[I

    invoke-virtual {p1}, LX/EIo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2102417
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported enum!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2102418
    :pswitch_0
    iget-object v0, p0, LX/EIq;->e:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    invoke-virtual {v0}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c()V

    .line 2102419
    :goto_1
    sget-object v0, LX/EIp;->FINISHED:LX/EIp;

    invoke-static {p0, v0}, LX/EIq;->a(LX/EIq;LX/EIp;)V

    .line 2102420
    const/4 v0, 0x1

    goto :goto_0

    .line 2102421
    :pswitch_1
    iget-object v0, p0, LX/EIq;->e:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    .line 2102422
    const v2, 0x7f070029

    invoke-static {v0, v2}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a(Lcom/facebook/rtc/voicemail/VoicemailHandler;I)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "final_tone"

    const-string v5, "3"

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    move-object v2, v0

    invoke-static/range {v2 .. v7}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a(Lcom/facebook/rtc/voicemail/VoicemailHandler;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;FLandroid/media/MediaPlayer$OnCompletionListener;)Z

    .line 2102423
    invoke-virtual {v0}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->c()V

    .line 2102424
    goto :goto_1

    .line 2102425
    :pswitch_2
    iget-object v0, p0, LX/EIq;->e:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    invoke-virtual {v0}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->d()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2102407
    iget-object v0, p0, LX/EIq;->e:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    invoke-virtual {v0}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->d()V

    .line 2102408
    sget-object v0, LX/EIp;->NONE:LX/EIp;

    invoke-static {p0, v0}, LX/EIq;->a(LX/EIq;LX/EIp;)V

    .line 2102409
    return-void
.end method

.method public final a(LX/ECD;)V
    .locals 2

    .prologue
    .line 2102405
    iget-object v0, p0, LX/EIq;->a:Ljava/util/AbstractList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/AbstractList;->add(ILjava/lang/Object;)V

    .line 2102406
    return-void
.end method

.method public final a(Landroid/net/Uri;JJLjava/lang/String;)V
    .locals 8

    .prologue
    .line 2102452
    iget-object v0, p0, LX/EIq;->a:Ljava/util/AbstractList;

    invoke-virtual {v0}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECD;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    .line 2102453
    invoke-interface/range {v0 .. v6}, LX/ECC;->a(Landroid/net/Uri;JJLjava/lang/String;)V

    goto :goto_0

    .line 2102454
    :cond_0
    return-void
.end method

.method public final a(JLjava/lang/String;)Z
    .locals 10

    .prologue
    .line 2102383
    iget-object v0, p0, LX/EIq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2102384
    iget-object v0, p0, LX/EIq;->e:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    .line 2102385
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->l:Z

    .line 2102386
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->m:Z

    .line 2102387
    iget-object v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->i:LX/2SD;

    invoke-virtual {v1}, LX/2SD;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    .line 2102388
    iget-object v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->f:LX/2SC;

    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2SC;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->p:Ljava/io/File;

    .line 2102389
    iput-object p3, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->q:Ljava/lang/String;

    .line 2102390
    iput-wide p1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->r:J

    .line 2102391
    new-instance v9, LX/EIl;

    invoke-direct {v9, v0}, LX/EIl;-><init>(Lcom/facebook/rtc/voicemail/VoicemailHandler;)V

    .line 2102392
    iget-object v4, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->h:LX/0ad;

    sget-short v5, LX/3Dx;->B:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 2102393
    if-eqz v4, :cond_1

    .line 2102394
    const v4, 0x7f070098

    invoke-static {v0, v4}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a(Lcom/facebook/rtc/voicemail/VoicemailHandler;I)Landroid/net/Uri;

    move-result-object v5

    .line 2102395
    :goto_0
    const-string v6, "greeting"

    const-string v7, "1"

    const v8, 0x3e99999a    # 0.3f

    move-object v4, v0

    invoke-static/range {v4 .. v9}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a(Lcom/facebook/rtc/voicemail/VoicemailHandler;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;FLandroid/media/MediaPlayer$OnCompletionListener;)Z

    move-result v4

    move v1, v4

    .line 2102396
    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->e:LX/2S7;

    const-string v3, "voicemail_prompt_started"

    invoke-virtual {v2, v3, v1}, LX/2S7;->b(Ljava/lang/String;Z)Z

    .line 2102397
    move v0, v1

    .line 2102398
    if-nez v0, :cond_0

    .line 2102399
    sget-object v0, LX/EIp;->NONE:LX/EIp;

    invoke-static {p0, v0}, LX/EIq;->a(LX/EIq;LX/EIp;)V

    .line 2102400
    const/4 v0, 0x0

    .line 2102401
    :goto_1
    return v0

    .line 2102402
    :cond_0
    sget-object v0, LX/EIp;->STARTED:LX/EIp;

    invoke-static {p0, v0}, LX/EIq;->a(LX/EIq;LX/EIp;)V

    .line 2102403
    const/4 v0, 0x1

    goto :goto_1

    .line 2102404
    :cond_1
    const v4, 0x7f070079

    invoke-static {v0, v4}, Lcom/facebook/rtc/voicemail/VoicemailHandler;->a(Lcom/facebook/rtc/voicemail/VoicemailHandler;I)Landroid/net/Uri;

    move-result-object v5

    goto :goto_0
.end method

.method public final a(Z)Z
    .locals 1

    .prologue
    .line 2102382
    if-eqz p1, :cond_0

    sget-object v0, LX/EIo;->YES:LX/EIo;

    :goto_0
    invoke-static {p0, v0}, LX/EIq;->a$redex0(LX/EIq;LX/EIo;)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, LX/EIo;->NO:LX/EIo;

    goto :goto_0
.end method

.method public final b()V
    .locals 7

    .prologue
    .line 2102374
    iget-object v0, p0, LX/EIq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2102375
    sget-object v0, LX/EIp;->RECORDING:LX/EIp;

    invoke-static {p0, v0}, LX/EIq;->a(LX/EIq;LX/EIp;)V

    .line 2102376
    iget-object v2, p0, LX/EIq;->d:Ljava/util/concurrent/Future;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/EIq;->d:Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2102377
    iget-object v2, p0, LX/EIq;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/facebook/rtc/voicemail/VoicemailHelper$1;

    invoke-direct {v3, p0}, Lcom/facebook/rtc/voicemail/VoicemailHelper$1;-><init>(LX/EIq;)V

    const-wide/32 v4, 0xea60

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5, v6}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v2

    iput-object v2, p0, LX/EIq;->d:Ljava/util/concurrent/Future;

    .line 2102378
    iget-object v0, p0, LX/EIq;->a:Ljava/util/AbstractList;

    invoke-virtual {v0}, Ljava/util/AbstractList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECD;

    .line 2102379
    invoke-interface {v0}, LX/ECC;->b()V

    goto :goto_1

    .line 2102380
    :cond_1
    return-void

    .line 2102381
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b(LX/ECD;)V
    .locals 1

    .prologue
    .line 2102372
    iget-object v0, p0, LX/EIq;->a:Ljava/util/AbstractList;

    invoke-virtual {v0, p1}, Ljava/util/AbstractList;->remove(Ljava/lang/Object;)Z

    .line 2102373
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2102371
    iget-object v0, p0, LX/EIq;->f:LX/EIp;

    sget-object v1, LX/EIp;->STARTED:LX/EIp;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/EIq;->f:LX/EIp;

    sget-object v1, LX/EIp;->RECORDING:LX/EIp;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
