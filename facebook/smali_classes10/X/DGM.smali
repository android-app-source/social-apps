.class public final LX/DGM;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DGN;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/DGK;

.field public b:LX/1Pd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/DGN;


# direct methods
.method public constructor <init>(LX/DGN;)V
    .locals 1

    .prologue
    .line 1979624
    iput-object p1, p0, LX/DGM;->d:LX/DGN;

    .line 1979625
    move-object v0, p1

    .line 1979626
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1979627
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1979628
    const-string v0, "NetEgoStorySetPhotoStoryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1979629
    if-ne p0, p1, :cond_1

    .line 1979630
    :cond_0
    :goto_0
    return v0

    .line 1979631
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1979632
    goto :goto_0

    .line 1979633
    :cond_3
    check-cast p1, LX/DGM;

    .line 1979634
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1979635
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1979636
    if-eq v2, v3, :cond_0

    .line 1979637
    iget-object v2, p0, LX/DGM;->a:LX/DGK;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DGM;->a:LX/DGK;

    iget-object v3, p1, LX/DGM;->a:LX/DGK;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1979638
    goto :goto_0

    .line 1979639
    :cond_5
    iget-object v2, p1, LX/DGM;->a:LX/DGK;

    if-nez v2, :cond_4

    .line 1979640
    :cond_6
    iget-object v2, p0, LX/DGM;->b:LX/1Pd;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DGM;->b:LX/1Pd;

    iget-object v3, p1, LX/DGM;->b:LX/1Pd;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1979641
    goto :goto_0

    .line 1979642
    :cond_8
    iget-object v2, p1, LX/DGM;->b:LX/1Pd;

    if-nez v2, :cond_7

    .line 1979643
    :cond_9
    iget-object v2, p0, LX/DGM;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/DGM;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DGM;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1979644
    goto :goto_0

    .line 1979645
    :cond_a
    iget-object v2, p1, LX/DGM;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
