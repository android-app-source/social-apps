.class public final LX/DNn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;)V
    .locals 0

    .prologue
    .line 1991960
    iput-object p1, p0, LX/DNn;->a:Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1991961
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1991962
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1991963
    if-nez p1, :cond_1

    .line 1991964
    :cond_0
    :goto_0
    return-void

    .line 1991965
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1991966
    if-nez v0, :cond_3

    .line 1991967
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1991968
    :goto_1
    invoke-static {v0}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v2

    .line 1991969
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1991970
    iget-object v0, p0, LX/DNn;->a:Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;

    iget-object v0, v0, Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;->a:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1991971
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    .line 1991972
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 1991973
    if-eqz v1, :cond_2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1991974
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1991975
    :cond_3
    iget-object v0, p0, LX/DNn;->a:Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;

    iget-object v0, v0, Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;->e:LX/B1W;

    .line 1991976
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1991977
    invoke-interface {v0, v1}, LX/B1W;->a(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1991978
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_5

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1991979
    iget-object v4, p0, LX/DNn;->a:Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;

    iget-object v4, v4, Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;->a:LX/0fz;

    invoke-virtual {v4, v0}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 1991980
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1991981
    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1991982
    iget-object v0, p0, LX/DNn;->a:Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;

    iget-object v0, v0, Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;->f:LX/DNe;

    invoke-interface {v0}, LX/DNe;->b()V

    goto :goto_0
.end method
