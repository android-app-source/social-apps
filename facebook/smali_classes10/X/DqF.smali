.class public LX/DqF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile h:LX/DqF;


# instance fields
.field private final b:LX/03V;

.field private final c:LX/0lB;

.field public final d:LX/1rp;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/1rU;

.field private final g:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2048147
    const-class v0, LX/DqF;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DqF;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0lB;LX/1rp;LX/0Ot;LX/1rU;LX/0W3;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0lB;",
            "LX/1rp;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            ">;",
            "LX/1rU;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048140
    iput-object p4, p0, LX/DqF;->e:LX/0Ot;

    .line 2048141
    iput-object p3, p0, LX/DqF;->d:LX/1rp;

    .line 2048142
    iput-object p1, p0, LX/DqF;->b:LX/03V;

    .line 2048143
    iput-object p2, p0, LX/DqF;->c:LX/0lB;

    .line 2048144
    iput-object p5, p0, LX/DqF;->f:LX/1rU;

    .line 2048145
    iput-object p6, p0, LX/DqF;->g:LX/0W3;

    .line 2048146
    return-void
.end method

.method public static a(LX/0QB;)LX/DqF;
    .locals 10

    .prologue
    .line 2048126
    sget-object v0, LX/DqF;->h:LX/DqF;

    if-nez v0, :cond_1

    .line 2048127
    const-class v1, LX/DqF;

    monitor-enter v1

    .line 2048128
    :try_start_0
    sget-object v0, LX/DqF;->h:LX/DqF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2048129
    if-eqz v2, :cond_0

    .line 2048130
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2048131
    new-instance v3, LX/DqF;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    invoke-static {v0}, LX/1rp;->a(LX/0QB;)LX/1rp;

    move-result-object v6

    check-cast v6, LX/1rp;

    const/16 v7, 0xe5e

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v8

    check-cast v8, LX/1rU;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v9

    check-cast v9, LX/0W3;

    invoke-direct/range {v3 .. v9}, LX/DqF;-><init>(LX/03V;LX/0lB;LX/1rp;LX/0Ot;LX/1rU;LX/0W3;)V

    .line 2048132
    move-object v0, v3

    .line 2048133
    sput-object v0, LX/DqF;->h:LX/DqF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2048134
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2048135
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2048136
    :cond_1
    sget-object v0, LX/DqF;->h:LX/DqF;

    return-object v0

    .line 2048137
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2048138
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/io/File;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2048148
    new-instance v0, Ljava/io/File;

    const-string v1, "notifications_client_json"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2048149
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2048150
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 2048151
    :try_start_0
    iget-object v3, p0, LX/DqF;->c:LX/0lB;

    .line 2048152
    iget-object v4, p0, LX/DqF;->f:LX/1rU;

    invoke-virtual {v4}, LX/1rU;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2048153
    iget-object v4, p0, LX/DqF;->d:LX/1rp;

    invoke-virtual {v4}, LX/1rp;->a()LX/2kW;

    move-result-object v4

    .line 2048154
    new-instance p1, LX/BDz;

    invoke-direct {p1}, LX/BDz;-><init>()V

    .line 2048155
    const/4 p0, 0x0

    invoke-virtual {v4, p0, p1}, LX/2kW;->a(Ljava/lang/String;LX/3Cf;)V

    .line 2048156
    iget-object p0, p1, LX/BDz;->a:LX/0Pz;

    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    move-object p1, p0

    .line 2048157
    move-object v4, p1

    .line 2048158
    :goto_0
    move-object v4, v4

    .line 2048159
    invoke-virtual {v3, v4}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2048160
    invoke-static {v2, v5}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 2048161
    invoke-static {v1, v5}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 2048162
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 2048163
    :catchall_0
    move-exception v0

    invoke-static {v2, v5}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 2048164
    invoke-static {v1, v5}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0

    :cond_0
    iget-object v4, p0, LX/DqF;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 2048165
    new-instance p1, LX/0Pz;

    invoke-direct {p1}, LX/0Pz;-><init>()V

    .line 2048166
    iget-object p0, v4, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->h:LX/2AD;

    invoke-virtual {p0}, LX/2AG;->b()Ljava/util/Collection;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2048167
    iget-object p0, v4, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->i:LX/2AD;

    invoke-virtual {p0}, LX/2AG;->b()Ljava/util/Collection;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2048168
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    move-object v4, p1

    .line 2048169
    goto :goto_0
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3

    .prologue
    .line 2048121
    :try_start_0
    const-string v0, "notifications_client_json"

    invoke-direct {p0, p1}, LX/DqF;->b(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2048122
    :goto_0
    return-object v0

    .line 2048123
    :catch_0
    move-exception v0

    .line 2048124
    iget-object v1, p0, LX/DqF;->b:LX/03V;

    sget-object v2, LX/DqF;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2048125
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2048115
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2048116
    :try_start_0
    invoke-direct {p0, p1}, LX/DqF;->b(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2048117
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v3, "notifications_client_json"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "text/plain"

    invoke-direct {v2, v3, v0, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2048118
    :goto_0
    return-object v1

    .line 2048119
    :catch_0
    move-exception v0

    .line 2048120
    iget-object v2, p0, LX/DqF;->b:LX/03V;

    sget-object v3, LX/DqF;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 2048114
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 2048113
    iget-object v0, p0, LX/DqF;->g:LX/0W3;

    sget-wide v2, LX/0X5;->ba:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
