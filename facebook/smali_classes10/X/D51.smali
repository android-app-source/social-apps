.class public final LX/D51;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

.field private final b:LX/D52;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;LX/D52;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1963195
    iput-object p1, p0, LX/D51;->a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1963196
    iput-object p2, p0, LX/D51;->b:LX/D52;

    .line 1963197
    iget-object v0, p2, LX/D52;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963198
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1963199
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, p3}, LX/1y1;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/D51;->c:Ljava/lang/String;

    .line 1963200
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1963201
    iget-object v0, p0, LX/D51;->b:LX/D52;

    iget-object v0, v0, LX/D52;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963202
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1963203
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1963204
    iget-object v1, p0, LX/D51;->a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->f:LX/1xv;

    iget-object v2, p0, LX/D51;->b:LX/D52;

    iget-object v2, v2, LX/D52;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/D51;->b:LX/D52;

    iget-boolean v3, v3, LX/D52;->b:Z

    const/4 v8, 0x0

    .line 1963205
    new-instance v4, LX/1y2;

    move-object v5, v1

    move-object v6, v2

    move v7, v3

    invoke-direct/range {v4 .. v8}, LX/1y2;-><init>(LX/1xv;Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V

    move-object v1, v4

    .line 1963206
    const v2, 0x106000b

    .line 1963207
    iput v2, v1, LX/1y2;->h:I

    .line 1963208
    move-object v1, v1

    .line 1963209
    invoke-virtual {v1}, LX/1y2;->d()Landroid/text/Spannable;

    move-result-object v1

    .line 1963210
    invoke-static {v0}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/D51;->a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    iget-object v2, v2, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->a:LX/1xc;

    invoke-virtual {v2, v0, v1}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1963211
    :goto_0
    new-instance v1, LX/1yT;

    invoke-static {v0}, LX/1yU;->a(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    return-object v1

    :cond_0
    move-object v0, v1

    .line 1963212
    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1963213
    iget-object v0, p0, LX/D51;->c:Ljava/lang/String;

    return-object v0
.end method
