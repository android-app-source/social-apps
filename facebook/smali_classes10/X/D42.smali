.class public final LX/D42;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;


# direct methods
.method public constructor <init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V
    .locals 0

    .prologue
    .line 1961530
    iput-object p1, p0, LX/D42;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x3a255376

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1961521
    invoke-interface {p3}, LX/0Yf;->isInitialStickyBroadcast()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/D42;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    .line 1961522
    iget-boolean v2, v0, LX/D3y;->j:Z

    move v0, v2

    .line 1961523
    if-eqz v0, :cond_0

    .line 1961524
    const-string v0, "state"

    const/4 v2, 0x1

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/45B;->CONNECTED:LX/45B;

    .line 1961525
    :goto_0
    sget-object v2, LX/45B;->DISCONNECTED:LX/45B;

    if-ne v0, v2, :cond_0

    .line 1961526
    iget-object v0, p0, LX/D42;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-static {v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->h(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    .line 1961527
    iget-object v0, p0, LX/D42;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-static {v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->f(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    .line 1961528
    :cond_0
    const v0, -0x345a2e8a    # -2.1734124E7f

    invoke-static {v0, v1}, LX/02F;->e(II)V

    return-void

    .line 1961529
    :cond_1
    sget-object v0, LX/45B;->DISCONNECTED:LX/45B;

    goto :goto_0
.end method
