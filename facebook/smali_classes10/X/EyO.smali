.class public LX/EyO;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/EyO;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2186032
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2186033
    const-string v0, "friends/center?source_ref={%s %s}&fc_tab={%s %s}&user_id={%s %s}&name={%s %s}&profile_pic={%s %s}&attachment={%s %s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "source_ref"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "unknown"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "fc_tab"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, LX/5P0;->SUGGESTIONS:LX/5P0;

    invoke-virtual {v3}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "user_id"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "default_id"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "default_name"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "profile_pic"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "default_profile_picture"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "attachment"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "NO_ATTACHMENT"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->NATIVE_FRIENDS_CENTER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2186034
    return-void
.end method

.method public static a(LX/0QB;)LX/EyO;
    .locals 3

    .prologue
    .line 2186035
    sget-object v0, LX/EyO;->a:LX/EyO;

    if-nez v0, :cond_1

    .line 2186036
    const-class v1, LX/EyO;

    monitor-enter v1

    .line 2186037
    :try_start_0
    sget-object v0, LX/EyO;->a:LX/EyO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2186038
    if-eqz v2, :cond_0

    .line 2186039
    :try_start_1
    new-instance v0, LX/EyO;

    invoke-direct {v0}, LX/EyO;-><init>()V

    .line 2186040
    move-object v0, v0

    .line 2186041
    sput-object v0, LX/EyO;->a:LX/EyO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2186042
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2186043
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2186044
    :cond_1
    sget-object v0, LX/EyO;->a:LX/EyO;

    return-object v0

    .line 2186045
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2186046
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
