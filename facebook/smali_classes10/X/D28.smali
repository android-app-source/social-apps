.class public final LX/D28;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/1FJ",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D1v;

.field public final synthetic b:I

.field public final synthetic c:LX/D2B;


# direct methods
.method public constructor <init>(LX/D2B;LX/D1v;I)V
    .locals 0

    .prologue
    .line 1958206
    iput-object p1, p0, LX/D28;->c:LX/D2B;

    iput-object p2, p0, LX/D28;->a:LX/D1v;

    iput p3, p0, LX/D28;->b:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1958203
    const-class v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    const-string v1, "Failed to extract bitmap %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1958204
    iget-object v0, p0, LX/D28;->a:LX/D1v;

    iget v1, p0, LX/D28;->b:I

    invoke-virtual {v0, v1}, LX/D1v;->a(I)V

    .line 1958205
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1958169
    check-cast p1, LX/1FJ;

    .line 1958170
    if-eqz p1, :cond_1

    .line 1958171
    iget-object v0, p0, LX/D28;->a:LX/D1v;

    iget v1, p0, LX/D28;->b:I

    const/4 v4, 0x0

    .line 1958172
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    .line 1958173
    iput v4, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->l:I

    .line 1958174
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1958175
    :cond_0
    :goto_0
    return-void

    .line 1958176
    :cond_1
    iget-object v0, p0, LX/D28;->a:LX/D1v;

    iget v1, p0, LX/D28;->b:I

    invoke-virtual {v0, v1}, LX/D1v;->a(I)V

    goto :goto_0

    .line 1958177
    :cond_2
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->p:LX/1FJ;

    if-eqz v2, :cond_3

    .line 1958178
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->p:LX/1FJ;

    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 1958179
    :cond_3
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    .line 1958180
    iput-object p1, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->p:LX/1FJ;

    .line 1958181
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-static {v2}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->e(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v2

    .line 1958182
    if-eqz v2, :cond_0

    .line 1958183
    iget-object v3, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v2}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->o()LX/D2G;

    move-result-object v2

    .line 1958184
    iput v1, v2, LX/D2G;->c:I

    .line 1958185
    move-object v2, v2

    .line 1958186
    invoke-virtual {v2}, LX/D2G;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v2

    .line 1958187
    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p0

    .line 1958188
    instance-of v1, p0, LX/D22;

    if-nez v1, :cond_7

    .line 1958189
    :goto_1
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->d:LX/D2l;

    if-eqz v2, :cond_4

    .line 1958190
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v3, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->d:LX/D2l;

    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 1958191
    iput-object v2, v3, LX/D2l;->b:Landroid/graphics/Bitmap;

    .line 1958192
    invoke-static {v3}, LX/D2l;->d(LX/D2l;)V

    .line 1958193
    :cond_4
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->g:Landroid/widget/ImageView;

    if-eqz v2, :cond_5

    .line 1958194
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v3, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->g:Landroid/widget/ImageView;

    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1958195
    :cond_5
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->m:LX/D2j;

    if-eqz v2, :cond_6

    .line 1958196
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v3, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->m:LX/D2j;

    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 1958197
    iput-object v2, v3, LX/D2j;->h:Landroid/graphics/Bitmap;

    .line 1958198
    invoke-virtual {v3}, LX/D2j;->invalidateSelf()V

    .line 1958199
    :cond_6
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    .line 1958200
    iput-boolean v4, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->j:Z

    .line 1958201
    iget-object v2, v0, LX/D1v;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-static {v2}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->k(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)V

    goto :goto_0

    .line 1958202
    :cond_7
    check-cast p0, LX/D22;

    invoke-interface {p0, v2}, LX/D22;->a(Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;)V

    goto :goto_1
.end method
