.class public LX/EU5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ETj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/ETj",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/ETQ;


# direct methods
.method public constructor <init>(LX/ETQ;)V
    .locals 0
    .param p1    # LX/ETQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125765
    iput-object p1, p0, LX/EU5;->a:LX/ETQ;

    .line 2125766
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125756
    iget-object v0, p0, LX/EU5;->a:LX/ETQ;

    invoke-virtual {v0, p1}, LX/ETQ;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)I

    move-result v0

    .line 2125757
    if-gtz v0, :cond_0

    .line 2125758
    const/4 v0, 0x0

    .line 2125759
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/EU5;->a:LX/ETQ;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125760
    iget-object v0, p0, LX/EU5;->a:LX/ETQ;

    invoke-virtual {v0, p1}, LX/ETQ;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)I

    move-result v0

    .line 2125761
    if-ltz v0, :cond_0

    iget-object v1, p0, LX/EU5;->a:LX/ETQ;

    invoke-virtual {v1}, LX/ETQ;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    .line 2125762
    :cond_0
    const/4 v0, 0x0

    .line 2125763
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LX/EU5;->a:LX/ETQ;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125755
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {p0, p1}, LX/EU5;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125754
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {p0, p1}, LX/EU5;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    return-object v0
.end method
