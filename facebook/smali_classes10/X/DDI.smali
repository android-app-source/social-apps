.class public LX/DDI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DDJ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DDI",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DDJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975328
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1975329
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DDI;->b:LX/0Zi;

    .line 1975330
    iput-object p1, p0, LX/DDI;->a:LX/0Ot;

    .line 1975331
    return-void
.end method

.method public static a(LX/0QB;)LX/DDI;
    .locals 4

    .prologue
    .line 1975317
    const-class v1, LX/DDI;

    monitor-enter v1

    .line 1975318
    :try_start_0
    sget-object v0, LX/DDI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975319
    sput-object v2, LX/DDI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975320
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975321
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975322
    new-instance v3, LX/DDI;

    const/16 p0, 0x1f58

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DDI;-><init>(LX/0Ot;)V

    .line 1975323
    move-object v0, v3

    .line 1975324
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975325
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DDI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975326
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1975332
    const v0, 0x66a602ec

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1975281
    check-cast p2, LX/DDH;

    .line 1975282
    iget-object v0, p0, LX/DDI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DDJ;

    iget-object v1, p2, LX/DDH;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/DDH;->b:LX/1Po;

    .line 1975283
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/DDJ;->f:LX/DD1;

    .line 1975284
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1975285
    check-cast v3, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v6

    .line 1975286
    if-nez v3, :cond_1

    .line 1975287
    const-string v7, ""

    .line 1975288
    :goto_0
    move-object v3, v7

    .line 1975289
    const/4 v7, 0x1

    .line 1975290
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a015d

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b124d

    invoke-interface {v5, v7, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x3

    const v7, 0x7f0b124d

    invoke-interface {v5, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x8

    invoke-interface {v5, v6, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    move-object v3, v5

    .line 1975291
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/DDJ;->c:LX/DDR;

    const/4 v5, 0x0

    .line 1975292
    new-instance v6, LX/DDQ;

    invoke-direct {v6, v4}, LX/DDQ;-><init>(LX/DDR;)V

    .line 1975293
    iget-object v7, v4, LX/DDR;->b:LX/0Zi;

    invoke-virtual {v7}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/DDP;

    .line 1975294
    if-nez v7, :cond_0

    .line 1975295
    new-instance v7, LX/DDP;

    invoke-direct {v7, v4}, LX/DDP;-><init>(LX/DDR;)V

    .line 1975296
    :cond_0
    invoke-static {v7, p1, v5, v5, v6}, LX/DDP;->a$redex0(LX/DDP;LX/1De;IILX/DDQ;)V

    .line 1975297
    move-object v6, v7

    .line 1975298
    move-object v5, v6

    .line 1975299
    move-object v4, v5

    .line 1975300
    iget-object v5, v4, LX/DDP;->a:LX/DDQ;

    iput-object v2, v5, LX/DDQ;->a:LX/1Po;

    .line 1975301
    iget-object v5, v4, LX/DDP;->e:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 1975302
    move-object v4, v4

    .line 1975303
    iget-object v5, v4, LX/DDP;->a:LX/DDQ;

    iput-object v1, v5, LX/DDQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975304
    iget-object v5, v4, LX/DDP;->e:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 1975305
    move-object v4, v4

    .line 1975306
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/3mm;->c(LX/1De;)LX/3mp;

    move-result-object v4

    iget-object v5, v0, LX/DDJ;->a:Landroid/content/res/Resources;

    const v6, 0x7f081c3c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3mp;->b(Ljava/lang/String;)LX/3mp;

    move-result-object v4

    .line 1975307
    const v5, 0x66a602ec

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1975308
    invoke-virtual {v4, v5}, LX/3mp;->a(LX/1dQ;)LX/3mp;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1975309
    return-object v0

    .line 1975310
    :cond_1
    invoke-static {v6}, LX/DD1;->c(LX/1PT;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1975311
    iget-object v7, v5, LX/DD1;->a:Landroid/content/res/Resources;

    const v8, 0x7f081c3a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1975312
    :cond_2
    invoke-static {v6}, LX/DD1;->b(LX/1PT;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1975313
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1975314
    :cond_3
    iget-object v7, v5, LX/DD1;->a:Landroid/content/res/Resources;

    const v8, 0x7f081c3a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1975315
    :cond_4
    iget-object v7, v5, LX/DD1;->a:Landroid/content/res/Resources;

    const v8, 0x7f081c3b

    const/4 v9, 0x1

    new-array v9, v9, [LX/47s;

    const/4 v10, 0x0

    new-instance v11, LX/47s;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v12

    new-instance p0, LX/DD0;

    invoke-direct {p0, v5, v3}, LX/DD0;-><init>(LX/DD1;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)V

    const/16 p2, 0x21

    invoke-direct {v11, v12, p0, p2}, LX/47s;-><init>(Ljava/lang/Object;Ljava/lang/Object;I)V

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, LX/47r;->a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;

    move-result-object v7

    goto/16 :goto_0

    .line 1975316
    :cond_5
    const-string v7, ""

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1975257
    invoke-static {}, LX/1dS;->b()V

    .line 1975258
    iget v0, p1, LX/1dQ;->b:I

    .line 1975259
    packed-switch v0, :pswitch_data_0

    .line 1975260
    :goto_0
    return-object v2

    .line 1975261
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1975262
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1975263
    check-cast v1, LX/DDH;

    .line 1975264
    iget-object v3, p0, LX/DDI;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DDJ;

    iget-object v4, v1, LX/DDH;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975265
    if-eqz v4, :cond_0

    .line 1975266
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1975267
    if-eqz v5, :cond_0

    .line 1975268
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1975269
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    if-nez v5, :cond_1

    .line 1975270
    :cond_0
    :goto_1
    goto :goto_0

    .line 1975271
    :cond_1
    iget-object p1, v3, LX/DDJ;->d:LX/DVF;

    .line 1975272
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1975273
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v5

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 p0, 0x0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1, v5, p2, p0, v1}, LX/DVF;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;ZLandroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    .line 1975274
    iget-object p1, v3, LX/DDJ;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-interface {p1, v5, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1975275
    iget-object p1, v3, LX/DDJ;->e:LX/1g8;

    .line 1975276
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1975277
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v5

    .line 1975278
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object p2

    const-string p0, "group_id"

    invoke-virtual {p2, p0, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object p2

    .line 1975279
    iget-object p0, p1, LX/1g8;->d:LX/0if;

    sget-object v1, LX/0ig;->aK:LX/0ih;

    const-string v3, "gpymi_see_all"

    const-string v0, "GROUP_FEED_PYMI"

    invoke-virtual {p0, v1, v3, v0, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1975280
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x66a602ec
        :pswitch_0
    .end packed-switch
.end method
