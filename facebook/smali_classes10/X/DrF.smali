.class public LX/DrF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/DrF;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;Ljava/util/concurrent/ExecutorService;LX/0Sh;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2049110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049111
    iput-object p1, p0, LX/DrF;->a:LX/0tX;

    .line 2049112
    iput-object p2, p0, LX/DrF;->b:LX/1Ck;

    .line 2049113
    iput-object p3, p0, LX/DrF;->c:Ljava/util/concurrent/Executor;

    .line 2049114
    iput-object p4, p0, LX/DrF;->d:LX/0Sh;

    .line 2049115
    return-void
.end method

.method public static a(LX/0QB;)LX/DrF;
    .locals 7

    .prologue
    .line 2049116
    sget-object v0, LX/DrF;->e:LX/DrF;

    if-nez v0, :cond_1

    .line 2049117
    const-class v1, LX/DrF;

    monitor-enter v1

    .line 2049118
    :try_start_0
    sget-object v0, LX/DrF;->e:LX/DrF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2049119
    if-eqz v2, :cond_0

    .line 2049120
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2049121
    new-instance p0, LX/DrF;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/DrF;-><init>(LX/0tX;LX/1Ck;Ljava/util/concurrent/ExecutorService;LX/0Sh;)V

    .line 2049122
    move-object v0, p0

    .line 2049123
    sput-object v0, LX/DrF;->e:LX/DrF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049124
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2049125
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2049126
    :cond_1
    sget-object v0, LX/DrF;->e:LX/DrF;

    return-object v0

    .line 2049127
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2049128
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
