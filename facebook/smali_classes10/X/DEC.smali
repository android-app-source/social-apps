.class public LX/DEC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1LV;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3mL;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/DE7;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/DE7;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1LV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3mL;",
            ">;",
            "LX/DE7;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1976475
    iput-object p1, p0, LX/DEC;->a:LX/0Ot;

    .line 1976476
    iput-object p3, p0, LX/DEC;->c:LX/DE7;

    .line 1976477
    iput-object p2, p0, LX/DEC;->b:LX/0Ot;

    .line 1976478
    return-void
.end method

.method public static a(LX/0QB;)LX/DEC;
    .locals 6

    .prologue
    .line 1976479
    const-class v1, LX/DEC;

    monitor-enter v1

    .line 1976480
    :try_start_0
    sget-object v0, LX/DEC;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976481
    sput-object v2, LX/DEC;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976482
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976483
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976484
    new-instance v4, LX/DEC;

    const/16 v3, 0x689

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x3b0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v3, LX/DE7;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DE7;

    invoke-direct {v4, v5, p0, v3}, LX/DEC;-><init>(LX/0Ot;LX/0Ot;LX/DE7;)V

    .line 1976485
    move-object v0, v4

    .line 1976486
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976487
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DEC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976488
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976489
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
