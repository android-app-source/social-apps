.class public final LX/E8S;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/E8N;


# direct methods
.method public constructor <init>(LX/E8N;)V
    .locals 0

    .prologue
    .line 2082465
    iput-object p1, p0, LX/E8S;->a:LX/E8N;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2082466
    iget-object v0, p0, LX/E8S;->a:LX/E8N;

    const/4 v1, 0x0

    .line 2082467
    iput-boolean v1, v0, LX/E8N;->f:Z

    .line 2082468
    iget-object v0, p0, LX/E8S;->a:LX/E8N;

    iget-object v0, v0, LX/E8N;->b:LX/2j3;

    iget-object v1, p0, LX/E8S;->a:LX/E8N;

    iget-object v1, v1, LX/E8N;->g:Ljava/lang/String;

    iget-object v2, p0, LX/E8S;->a:LX/E8N;

    iget-object v2, v2, LX/E8N;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, LX/2j3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2082469
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2082470
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 2082471
    iget-object v0, p0, LX/E8S;->a:LX/E8N;

    iget-object v0, v0, LX/E8N;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2082472
    iget-object v0, p0, LX/E8S;->a:LX/E8N;

    .line 2082473
    iput-boolean v3, v0, LX/E8N;->f:Z

    .line 2082474
    if-eqz p1, :cond_0

    .line 2082475
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2082476
    if-eqz v0, :cond_0

    .line 2082477
    iget-object v0, p0, LX/E8S;->a:LX/E8N;

    .line 2082478
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2082479
    invoke-virtual {v0, v1}, LX/E8N;->b(Ljava/lang/Object;)V

    .line 2082480
    iget-object v0, p0, LX/E8S;->a:LX/E8N;

    .line 2082481
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2082482
    invoke-virtual {v0, v1}, LX/E8N;->a(Ljava/lang/Object;)LX/0us;

    move-result-object v0

    .line 2082483
    if-eqz v0, :cond_0

    .line 2082484
    iget-object v1, p0, LX/E8S;->a:LX/E8N;

    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v2

    .line 2082485
    iput-object v2, v1, LX/E8N;->d:Ljava/lang/String;

    .line 2082486
    iget-object v1, p0, LX/E8S;->a:LX/E8N;

    invoke-interface {v0}, LX/0us;->b()Z

    move-result v0

    .line 2082487
    iput-boolean v0, v1, LX/E8N;->e:Z

    .line 2082488
    :cond_0
    iget-object v0, p0, LX/E8S;->a:LX/E8N;

    iget-object v0, v0, LX/E8N;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2082489
    iget-object v0, p0, LX/E8S;->a:LX/E8N;

    .line 2082490
    iput-boolean v3, v0, LX/E8N;->e:Z

    .line 2082491
    :cond_1
    return-void
.end method
