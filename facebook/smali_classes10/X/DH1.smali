.class public LX/DH1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/39c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39c",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/39c;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1981174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1981175
    iput-object p1, p0, LX/DH1;->a:LX/39c;

    .line 1981176
    return-void
.end method

.method public static a(LX/0QB;)LX/DH1;
    .locals 4

    .prologue
    .line 1981177
    const-class v1, LX/DH1;

    monitor-enter v1

    .line 1981178
    :try_start_0
    sget-object v0, LX/DH1;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981179
    sput-object v2, LX/DH1;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981180
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981181
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981182
    new-instance p0, LX/DH1;

    invoke-static {v0}, LX/39c;->a(LX/0QB;)LX/39c;

    move-result-object v3

    check-cast v3, LX/39c;

    invoke-direct {p0, v3}, LX/DH1;-><init>(LX/39c;)V

    .line 1981183
    move-object v0, p0

    .line 1981184
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981185
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DH1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981186
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981187
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
