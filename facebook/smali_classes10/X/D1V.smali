.class public final LX/D1V;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocatorNearbyLocationsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1956866
    const-class v1, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocatorNearbyLocationsQueryModel;

    const v0, 0x7ec54854

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "StoreLocatorNearbyLocationsQuery"

    const-string v6, "181fe828bf7adb592bef33aa6716d825"

    const-string v7, "store_locator"

    const-string v8, "10155217777831729"

    const-string v9, "10155259090371729"

    .line 1956867
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1956868
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1956869
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1956874
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1956875
    sparse-switch v0, :sswitch_data_0

    .line 1956876
    :goto_0
    return-object p1

    .line 1956877
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1956878
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1956879
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1956880
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x762d3e7c -> :sswitch_0
        -0x65578195 -> :sswitch_1
        0x683094a -> :sswitch_2
        0x1483e806 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1956870
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1956871
    :goto_1
    return v0

    .line 1956872
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1956873
    :pswitch_1
    const/16 v0, 0x14

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
