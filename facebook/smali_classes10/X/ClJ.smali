.class public final LX/ClJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public c:Z

.field public d:Z

.field public e:J

.field public f:J

.field public g:J

.field public h:J

.field public i:J

.field public j:J

.field public k:J

.field public l:J

.field public m:J

.field public n:J

.field public o:Z

.field public p:Z


# direct methods
.method public constructor <init>(ILcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;)V
    .locals 0

    .prologue
    .line 1932280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932281
    iput p1, p0, LX/ClJ;->a:I

    .line 1932282
    iput-object p2, p0, LX/ClJ;->b:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 1932283
    return-void
.end method

.method private static a(F)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1932284
    cmpg-float v1, p0, v0

    if-gez v1, :cond_0

    move p0, v0

    :cond_0
    return p0
.end method

.method public static a$redex0(LX/ClJ;J)F
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    const/high16 v5, 0x447a0000    # 1000.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1932285
    iget-wide v0, p0, LX/ClJ;->h:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1932286
    const/4 v0, 0x0

    .line 1932287
    :goto_0
    return v0

    .line 1932288
    :cond_0
    iget-wide v0, p0, LX/ClJ;->j:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 1932289
    iget-wide v0, p0, LX/ClJ;->h:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    mul-float/2addr v0, v4

    div-float/2addr v0, v5

    invoke-static {v0}, LX/ClJ;->a(F)F

    move-result v0

    goto :goto_0

    .line 1932290
    :cond_1
    iget-wide v0, p0, LX/ClJ;->j:J

    iget-wide v2, p0, LX/ClJ;->h:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    mul-float/2addr v0, v4

    div-float/2addr v0, v5

    invoke-static {v0}, LX/ClJ;->a(F)F

    move-result v0

    goto :goto_0
.end method

.method public static a$redex0(LX/ClJ;)Z
    .locals 4

    .prologue
    .line 1932291
    iget-wide v0, p0, LX/ClJ;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c$redex0(LX/ClJ;)F
    .locals 6

    .prologue
    .line 1932292
    const/high16 v0, 0x3f800000    # 1.0f

    iget-wide v2, p0, LX/ClJ;->f:J

    iget-wide v4, p0, LX/ClJ;->e:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public static d$redex0(LX/ClJ;)F
    .locals 6

    .prologue
    .line 1932293
    const/high16 v0, 0x3f800000    # 1.0f

    iget-wide v2, p0, LX/ClJ;->g:J

    iget-wide v4, p0, LX/ClJ;->f:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public static e$redex0(LX/ClJ;)F
    .locals 6

    .prologue
    .line 1932294
    const/high16 v0, 0x3f800000    # 1.0f

    iget-wide v2, p0, LX/ClJ;->i:J

    iget-wide v4, p0, LX/ClJ;->h:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method
