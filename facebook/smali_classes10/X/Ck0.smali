.class public LX/Ck0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/Cju;


# direct methods
.method public constructor <init>(LX/Cju;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1930422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930423
    iput-object p1, p0, LX/Ck0;->a:LX/Cju;

    .line 1930424
    return-void
.end method

.method public static a(LX/0QB;)LX/Ck0;
    .locals 4

    .prologue
    .line 1930411
    const-class v1, LX/Ck0;

    monitor-enter v1

    .line 1930412
    :try_start_0
    sget-object v0, LX/Ck0;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1930413
    sput-object v2, LX/Ck0;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1930414
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1930415
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1930416
    new-instance p0, LX/Ck0;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v3

    check-cast v3, LX/Cju;

    invoke-direct {p0, v3}, LX/Ck0;-><init>(LX/Cju;)V

    .line 1930417
    move-object v0, p0

    .line 1930418
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1930419
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ck0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1930420
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1930421
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1930409
    const v2, 0x7f0d0120

    const v4, 0x7f0d0120

    move-object v0, p0

    move-object v1, p1

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1930410
    return-void
.end method

.method public final a(Landroid/view/View;II)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1930399
    if-nez p1, :cond_0

    .line 1930400
    :goto_0
    return-void

    .line 1930401
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1930402
    if-nez v0, :cond_1

    .line 1930403
    new-instance v0, Lcom/facebook/richdocument/ham/HamViewUtils$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/richdocument/ham/HamViewUtils$1;-><init>(LX/Ck0;Landroid/view/View;II)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1930404
    :cond_1
    if-eqz p2, :cond_2

    .line 1930405
    iget-object v1, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v1, p2}, LX/Cju;->c(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1930406
    :cond_2
    if-eqz p3, :cond_3

    .line 1930407
    iget-object v1, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v1, p3}, LX/Cju;->c(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1930408
    :cond_3
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;IIII)V
    .locals 4

    .prologue
    .line 1930397
    iget-object v0, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v0, p2}, LX/Cju;->c(I)I

    move-result v0

    iget-object v1, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v1, p3}, LX/Cju;->c(I)I

    move-result v1

    iget-object v2, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v2, p4}, LX/Cju;->c(I)I

    move-result v2

    iget-object v3, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v3, p5}, LX/Cju;->c(I)I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 1930398
    return-void
.end method

.method public final b(Landroid/view/View;IIII)V
    .locals 4

    .prologue
    .line 1930393
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1930394
    iget-object v0, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v0, p2}, LX/Cju;->c(I)I

    move-result v0

    iget-object v1, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v1, p3}, LX/Cju;->c(I)I

    move-result v1

    iget-object v2, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v2, p4}, LX/Cju;->c(I)I

    move-result v2

    iget-object v3, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v3, p5}, LX/Cju;->c(I)I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 1930395
    :goto_0
    return-void

    .line 1930396
    :cond_0
    invoke-virtual/range {p0 .. p5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    goto :goto_0
.end method

.method public final c(Landroid/view/View;IIII)V
    .locals 7

    .prologue
    .line 1930384
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1930385
    new-instance v0, Lcom/facebook/richdocument/ham/HamViewUtils$2;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/richdocument/ham/HamViewUtils$2;-><init>(LX/Ck0;Landroid/view/View;IIII)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1930386
    :cond_0
    :goto_0
    return-void

    .line 1930387
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 1930388
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1930389
    iget-object v1, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v1, p2}, LX/Cju;->c(I)I

    move-result v1

    iget-object v2, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v2, p3}, LX/Cju;->c(I)I

    move-result v2

    iget-object v3, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v3, p4}, LX/Cju;->c(I)I

    move-result v3

    iget-object v4, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v4, p5}, LX/Cju;->c(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1930390
    invoke-static {}, LX/Crz;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1930391
    iget-object v1, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v1, p2}, LX/Cju;->c(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 1930392
    iget-object v1, p0, LX/Ck0;->a:LX/Cju;

    invoke-interface {v1, p4}, LX/Cju;->c(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    goto :goto_0
.end method
