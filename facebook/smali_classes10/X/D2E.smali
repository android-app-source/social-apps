.class public LX/D2E;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1958359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958360
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;LX/4gI;)Ljava/lang/String;
    .locals 7
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 1958361
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1958362
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const-string v0, "_data"

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v1, "datetaken"

    aput-object v1, v2, v0

    .line 1958363
    invoke-static {p1}, LX/4gB;->a(LX/4gI;)Ljava/lang/String;

    move-result-object v3

    .line 1958364
    :try_start_0
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v4, 0x0

    const-string v5, "date_modified DESC LIMIT 1"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1958365
    if-nez v1, :cond_2

    .line 1958366
    if-eqz v1, :cond_0

    .line 1958367
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v6

    .line 1958368
    :cond_1
    :goto_0
    return-object v0

    .line 1958369
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1958370
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1958371
    if-eqz v1, :cond_1

    .line 1958372
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1958373
    :cond_3
    if-eqz v1, :cond_4

    .line 1958374
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v0, v6

    .line 1958375
    goto :goto_0

    .line 1958376
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_5

    .line 1958377
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 1958378
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_1
.end method
