.class public final LX/Ewa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ewb;


# direct methods
.method public constructor <init>(LX/Ewb;)V
    .locals 0

    .prologue
    .line 2183275
    iput-object p1, p0, LX/Ewa;->a:LX/Ewb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2183276
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2183277
    check-cast p1, LX/0Px;

    const/4 v3, 0x0

    .line 2183278
    if-nez p1, :cond_0

    .line 2183279
    :goto_0
    return-void

    .line 2183280
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;

    .line 2183281
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;->j()Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;

    move-result-object v5

    .line 2183282
    iget-object v0, p0, LX/Ewa;->a:LX/Ewb;

    iget-object v0, v0, LX/Ewb;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/83X;

    .line 2183283
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/Ewi;

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    if-eq v1, v6, :cond_1

    move-object v1, v0

    .line 2183284
    check-cast v1, LX/Eut;

    .line 2183285
    iput-boolean v3, v1, LX/Eut;->f:Z

    .line 2183286
    invoke-virtual {v5}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2np;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2183287
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2183288
    :cond_2
    iget-object v0, p0, LX/Ewa;->a:LX/Ewb;

    iget-object v0, v0, LX/Ewb;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->r:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    if-eqz v0, :cond_3

    .line 2183289
    iget-object v0, p0, LX/Ewa;->a:LX/Ewb;

    iget-object v0, v0, LX/Ewb;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->r:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    invoke-virtual {v0}, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->d()V

    .line 2183290
    :cond_3
    iget-object v0, p0, LX/Ewa;->a:LX/Ewb;

    iget-object v0, v0, LX/Ewb;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    const v1, 0x4234d662

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method
