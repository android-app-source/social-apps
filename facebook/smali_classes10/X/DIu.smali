.class public final LX/DIu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DIv;


# direct methods
.method public constructor <init>(LX/DIv;)V
    .locals 0

    .prologue
    .line 1984532
    iput-object p1, p0, LX/DIu;->a:LX/DIv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1984533
    iget-object v0, p0, LX/DIu;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->i:LX/03V;

    const-string v1, "ComposerSellController"

    const-string v2, "Couldn\'t complete UserSaleGroupsQuery."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1984534
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1984535
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1984536
    if-eqz p1, :cond_0

    .line 1984537
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1984538
    if-eqz v0, :cond_0

    .line 1984539
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1984540
    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1984541
    :cond_0
    :goto_0
    return-void

    .line 1984542
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1984543
    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel;->a()LX/0Px;

    move-result-object v6

    .line 1984544
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1984545
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    move v5, v3

    :goto_1
    if-ge v5, v8, :cond_7

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel$NodesModel;

    .line 1984546
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    iget-object v1, p0, LX/DIu;->a:LX/DIv;

    iget-wide v12, v1, LX/DIv;->n:J

    cmp-long v1, v10, v12

    if-eqz v1, :cond_2

    .line 1984547
    const/4 v4, 0x0

    .line 1984548
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    .line 1984549
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v9, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1984550
    invoke-virtual {v9, v1, v3}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    if-eqz v1, :cond_6

    .line 1984551
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v9, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1984552
    invoke-virtual {v9, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v9, v1, v3}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_3
    if-eqz v1, :cond_8

    .line 1984553
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v4, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1984554
    :goto_4
    new-instance v4, LX/DJh;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserSaleGroupsGraphQLModels$UserSaleGroupsQueryModel$GroupsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v9, v0, v1}, LX/DJh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1984555
    :cond_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_3
    move v1, v3

    .line 1984556
    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_3

    :cond_6
    move v1, v3

    goto :goto_3

    .line 1984557
    :cond_7
    iget-object v0, p0, LX/DIu;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJT;

    .line 1984558
    iget-object v1, v0, LX/DJT;->a:LX/DJc;

    iget-object v1, v1, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v1, v7}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setCrossPostGroups(Ljava/util/List;)V

    .line 1984559
    goto/16 :goto_0

    :cond_8
    move-object v1, v4

    goto :goto_4
.end method
