.class public LX/Dp2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:LX/2PC;

.field private static final b:LX/2PC;

.field private static final h:Ljava/lang/Object;


# instance fields
.field public final c:LX/Dof;

.field private final d:LX/0TD;

.field private e:LX/Eaf;

.field private f:Z

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2043013
    new-instance v0, LX/2PC;

    const-string v1, "local_identity_key"

    invoke-direct {v0, v1}, LX/2PC;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dp2;->a:LX/2PC;

    .line 2043014
    new-instance v0, LX/2PC;

    const-string v1, "local_registration_id"

    invoke-direct {v0, v1}, LX/2PC;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dp2;->b:LX/2PC;

    .line 2043015
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Dp2;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Dof;LX/0TD;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2043016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2043017
    iput-object p1, p0, LX/Dp2;->c:LX/Dof;

    .line 2043018
    iput-object p2, p0, LX/Dp2;->d:LX/0TD;

    .line 2043019
    return-void
.end method

.method public static a(LX/0QB;)LX/Dp2;
    .locals 8

    .prologue
    .line 2043020
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2043021
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2043022
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2043023
    if-nez v1, :cond_0

    .line 2043024
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2043025
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2043026
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2043027
    sget-object v1, LX/Dp2;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2043028
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2043029
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2043030
    :cond_1
    if-nez v1, :cond_4

    .line 2043031
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2043032
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2043033
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2043034
    new-instance p0, LX/Dp2;

    invoke-static {v0}, LX/Dof;->b(LX/0QB;)LX/Dof;

    move-result-object v1

    check-cast v1, LX/Dof;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-direct {p0, v1, v7}, LX/Dp2;-><init>(LX/Dof;LX/0TD;)V

    .line 2043035
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2043036
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2043037
    if-nez v1, :cond_2

    .line 2043038
    sget-object v0, LX/Dp2;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dp2;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2043039
    :goto_1
    if-eqz v0, :cond_3

    .line 2043040
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2043041
    :goto_3
    check-cast v0, LX/Dp2;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2043042
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2043043
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2043044
    :catchall_1
    move-exception v0

    .line 2043045
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2043046
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2043047
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2043048
    :cond_2
    :try_start_8
    sget-object v0, LX/Dp2;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dp2;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()LX/Eaf;
    .locals 4

    .prologue
    .line 2043049
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Dp2;->e:LX/Eaf;

    if-eqz v0, :cond_0

    .line 2043050
    iget-object v0, p0, LX/Dp2;->e:LX/Eaf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2043051
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2043052
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Dp2;->d:LX/0TD;

    new-instance v1, LX/Dp1;

    invoke-direct {v1, p0}, LX/Dp1;-><init>(LX/Dp2;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2043053
    const v1, 0x51ec1dc8

    :try_start_2
    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eaf;

    iput-object v0, p0, LX/Dp2;->e:LX/Eaf;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2043054
    :goto_1
    :try_start_3
    iget-object v0, p0, LX/Dp2;->e:LX/Eaf;

    goto :goto_0

    .line 2043055
    :catch_0
    move-exception v0

    .line 2043056
    :goto_2
    const-string v1, "MessengerIdentityKeyGenerator"

    const-string v2, "Failed to get Tincan Identity Key future."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 2043057
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2043058
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final declared-synchronized b()I
    .locals 3

    .prologue
    .line 2043059
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/Dp2;->f:Z

    if-eqz v0, :cond_0

    .line 2043060
    iget v0, p0, LX/Dp2;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2043061
    :goto_0
    monitor-exit p0

    return v0

    .line 2043062
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Dp2;->c:LX/Dof;

    sget-object v1, LX/Dp2;->b:LX/2PC;

    invoke-virtual {v0, v1}, LX/Dod;->a(LX/2PC;)Ljava/lang/String;

    move-result-object v0

    .line 2043063
    if-eqz v0, :cond_1

    .line 2043064
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Dp2;->g:I

    .line 2043065
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Dp2;->f:Z

    .line 2043066
    iget v0, p0, LX/Dp2;->g:I

    goto :goto_0

    .line 2043067
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, LX/Ecq;->a(Z)I

    move-result v0

    iput v0, p0, LX/Dp2;->g:I

    .line 2043068
    iget-object v0, p0, LX/Dp2;->c:LX/Dof;

    sget-object v1, LX/Dp2;->b:LX/2PC;

    iget v2, p0, LX/Dp2;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Dod;->a(LX/2PC;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2043069
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
