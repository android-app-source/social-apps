.class public final LX/D7R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/D7Z;

.field public final synthetic b:LX/D7X;


# direct methods
.method public constructor <init>(LX/D7X;LX/D7Z;)V
    .locals 0

    .prologue
    .line 1967143
    iput-object p1, p0, LX/D7R;->b:LX/D7X;

    iput-object p2, p0, LX/D7R;->a:LX/D7Z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1967144
    iget-object v0, p0, LX/D7R;->a:LX/D7Z;

    .line 1967145
    iget-object v1, v0, LX/D7Z;->c:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 1967146
    iget v2, v1, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    move v1, v2

    .line 1967147
    const v2, 0x7f0d0cda

    if-ne v1, v2, :cond_1

    sget-object v1, LX/2ft;->WAIT_FOR_WIFI:LX/2ft;

    :goto_0
    move-object v0, v1

    .line 1967148
    iget-object v1, p0, LX/D7R;->b:LX/D7X;

    iget-object v1, v1, LX/D7X;->o:LX/2ft;

    if-eq v1, v0, :cond_0

    .line 1967149
    iget-object v1, p0, LX/D7R;->b:LX/D7X;

    .line 1967150
    iput-object v0, v1, LX/D7X;->o:LX/2ft;

    .line 1967151
    iget-object v0, p0, LX/D7R;->b:LX/D7X;

    iget-object v0, v0, LX/D7X;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/BUK;->b:LX/0Tn;

    iget-object v2, p0, LX/D7R;->b:LX/D7X;

    iget-object v2, v2, LX/D7X;->o:LX/2ft;

    iget v2, v2, LX/2ft;->mValue:I

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1967152
    :cond_0
    iget-object v0, p0, LX/D7R;->b:LX/D7X;

    iget-object v1, p0, LX/D7R;->b:LX/D7X;

    iget-object v1, v1, LX/D7X;->o:LX/2ft;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/D7X;->a$redex0(LX/D7X;LX/2ft;Ljava/lang/String;)V

    .line 1967153
    return-void

    :cond_1
    sget-object v1, LX/2ft;->NONE:LX/2ft;

    goto :goto_0
.end method
