.class public final LX/E8U;
.super LX/5Mq;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;LX/195;)V
    .locals 0

    .prologue
    .line 2082766
    iput-object p1, p0, LX/E8U;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    invoke-direct {p0, p2}, LX/5Mq;-><init>(LX/195;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 8

    .prologue
    .line 2082767
    iget-object v0, p0, LX/E8U;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-object v0, v0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 2082768
    if-lez p3, :cond_0

    iget-object v2, p0, LX/E8U;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-object v2, v2, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    .line 2082769
    iget-boolean v3, v2, LX/2ja;->m:Z

    move v2, v3

    .line 2082770
    if-eqz v2, :cond_0

    .line 2082771
    iget-object v2, p0, LX/E8U;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-object v2, v2, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    iget-object v3, p0, LX/E8U;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-wide v4, v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->u:J

    sub-long v4, v0, v4

    iget-object v3, p0, LX/E8U;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-wide v6, v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->v:J

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/2ja;->c(J)V

    .line 2082772
    :cond_0
    iget-object v2, p0, LX/E8U;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-object v2, v2, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->k:LX/2ja;

    .line 2082773
    invoke-static {v2, v0, v1}, LX/2ja;->g(LX/2ja;J)V

    .line 2082774
    iget-object v0, p0, LX/E8U;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 2082775
    iget-object v0, p0, LX/E8U;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    .line 2082776
    invoke-static {v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->P(Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;)V

    .line 2082777
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 2082778
    invoke-super {p0, p1, p2}, LX/5Mq;->b(Landroid/support/v7/widget/RecyclerView;I)V

    .line 2082779
    return-void
.end method
