.class public final LX/DjI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dix;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;)V
    .locals 0

    .prologue
    .line 2033058
    iput-object p1, p0, LX/DjI;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 2033059
    iget-object v0, p0, LX/DjI;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->e:LX/Dih;

    iget-object v1, p0, LX/DjI;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->r:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v1}, LX/DnS;->g(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DjI;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->r:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v2}, LX/DnS;->h(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/DjI;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->l:Ljava/lang/String;

    .line 2033060
    iget-object v4, v0, LX/Dih;->a:LX/0Zb;

    const-string v5, "profservices_booking_admin_cancel_appointment"

    invoke-static {v5, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "request_id"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "referrer"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2033061
    iget-object v0, p0, LX/DjI;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->q:LX/DkN;

    new-instance v1, LX/DjH;

    invoke-direct {v1, p0}, LX/DjH;-><init>(LX/DjI;)V

    .line 2033062
    iget-object v2, v0, LX/DkN;->b:LX/1Ck;

    const-string v3, "admin_cancel_appointment"

    new-instance v4, LX/Dk2;

    invoke-direct {v4, v0}, LX/Dk2;-><init>(LX/DkN;)V

    new-instance v5, LX/Dk3;

    invoke-direct {v5, v0, v1}, LX/Dk3;-><init>(LX/DkN;LX/DjG;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2033063
    return-void
.end method
