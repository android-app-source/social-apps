.class public LX/EeH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/growth/profile/SetNativeNameParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2152314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152315
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2152316
    check-cast p1, Lcom/facebook/api/growth/profile/SetNativeNameParams;

    .line 2152317
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2152318
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152319
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "first_name"

    .line 2152320
    iget-object v2, p1, Lcom/facebook/api/growth/profile/SetNativeNameParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2152321
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152322
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "last_name"

    .line 2152323
    iget-object v2, p1, Lcom/facebook/api/growth/profile/SetNativeNameParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2152324
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152325
    iget-object v0, p1, Lcom/facebook/api/growth/profile/SetNativeNameParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2152326
    if-eqz v0, :cond_0

    .line 2152327
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "first_name_extra"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152328
    :cond_0
    iget-object v0, p1, Lcom/facebook/api/growth/profile/SetNativeNameParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2152329
    if-eqz v0, :cond_1

    .line 2152330
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "last_name_extra"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152331
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "locale"

    .line 2152332
    iget-object v2, p1, Lcom/facebook/api/growth/profile/SetNativeNameParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 2152333
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152334
    new-instance v0, LX/14N;

    const-string v1, "SetNativeName"

    const-string v2, "POST"

    const-string v3, "method/user.setNativeName"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2152335
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2152336
    const/4 v0, 0x0

    return-object v0
.end method
