.class public LX/DSB;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/DSA;


# instance fields
.field private final a:LX/DRg;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/DSZ;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/DRg;LX/DSZ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # LX/DRg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/DSZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1999279
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1999280
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1999281
    iput-object v0, p0, LX/DSB;->b:LX/0Px;

    .line 1999282
    iput-object p1, p0, LX/DSB;->a:LX/DRg;

    .line 1999283
    iput-object p2, p0, LX/DSB;->c:LX/DSZ;

    .line 1999284
    iput-object p3, p0, LX/DSB;->e:Ljava/lang/String;

    .line 1999285
    iput-object p4, p0, LX/DSB;->d:Ljava/lang/String;

    .line 1999286
    return-void
.end method


# virtual methods
.method public final a()LX/DSZ;
    .locals 1

    .prologue
    .line 1999287
    iget-object v0, p0, LX/DSB;->c:LX/DSZ;

    return-object v0
.end method

.method public final a(I)LX/DSf;
    .locals 1

    .prologue
    .line 1999288
    iget-object v0, p0, LX/DSB;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1999289
    iget-object v0, p0, LX/DSB;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DSf;

    .line 1999290
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1999296
    invoke-static {}, LX/DS9;->values()[LX/DS9;

    move-result-object v0

    aget-object v0, v0, p1

    .line 1999297
    sget-object v1, LX/DS8;->a:[I

    invoke-virtual {v0}, LX/DS9;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1999298
    new-instance v0, Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    .line 1999299
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1999300
    const v1, 0x7f03081f

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1999301
    :pswitch_1
    new-instance v0, LX/DTt;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/DSB;->a:LX/DRg;

    invoke-direct {v0, v1, v2, v3}, LX/DTt;-><init>(Landroid/content/Context;LX/DRg;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 10

    .prologue
    .line 1999239
    invoke-static {}, LX/DS9;->values()[LX/DS9;

    move-result-object v0

    aget-object v0, v0, p4

    .line 1999240
    sget-object v1, LX/DS8;->a:[I

    invoke-virtual {v0}, LX/DS9;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1999241
    :goto_0
    return-void

    .line 1999242
    :pswitch_0
    check-cast p2, LX/DSf;

    check-cast p3, LX/DTt;

    .line 1999243
    iget-object v2, p2, LX/DSf;->d:LX/DUV;

    move-object v2, v2

    .line 1999244
    invoke-interface {v2}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v2

    .line 1999245
    iget-object v3, p0, LX/DSB;->c:LX/DSZ;

    .line 1999246
    iget-boolean v4, v3, LX/DSZ;->e:Z

    move v4, v4

    .line 1999247
    iget-object v3, p2, LX/DSf;->a:LX/DSb;

    move-object v5, v3

    .line 1999248
    iget-object v3, p0, LX/DSB;->c:LX/DSZ;

    .line 1999249
    iget-boolean v6, v3, LX/DSZ;->f:Z

    move v6, v6

    .line 1999250
    iget-object v3, p0, LX/DSB;->c:LX/DSZ;

    invoke-virtual {v3, v2}, LX/DSZ;->b(Ljava/lang/String;)Z

    move-result v7

    iget-object v2, p0, LX/DSB;->c:LX/DSZ;

    iget-object v3, p0, LX/DSB;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/DSZ;->c(Ljava/lang/String;)Z

    move-result v8

    iget-object v9, p0, LX/DSB;->e:Ljava/lang/String;

    move-object v2, p3

    move-object v3, p2

    invoke-virtual/range {v2 .. v9}, LX/DTt;->a(LX/DSf;ZLX/DSb;ZZZLjava/lang/String;)V

    .line 1999251
    goto :goto_0

    .line 1999252
    :pswitch_1
    iget-object v0, p0, LX/DSB;->c:LX/DSZ;

    .line 1999253
    iget-boolean p0, v0, LX/DSZ;->d:Z

    move v0, p0

    .line 1999254
    move v0, v0

    .line 1999255
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1999291
    if-eqz p1, :cond_0

    .line 1999292
    iput-object p1, p0, LX/DSB;->b:LX/0Px;

    .line 1999293
    const v0, 0x4b308392    # 1.1568018E7f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1999294
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/widget/listview/BetterListView;)V
    .locals 0

    .prologue
    .line 1999295
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/widget/listview/BetterListView;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1999261
    iget-object v0, p0, LX/DSB;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, LX/DSB;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DSf;

    .line 1999262
    iget-object v4, v0, LX/DSf;->d:LX/DUV;

    move-object v0, v4

    .line 1999263
    invoke-interface {v0}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1999264
    iget-object v0, p0, LX/DSB;->c:LX/DSZ;

    invoke-virtual {v0, p1}, LX/DSZ;->a(Ljava/lang/String;)V

    .line 1999265
    :cond_0
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v0

    if-gt v1, v0, :cond_1

    .line 1999266
    invoke-virtual {p2, v1}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1999267
    if-eqz v0, :cond_3

    instance-of v2, v0, LX/DTt;

    if-eqz v2, :cond_3

    .line 1999268
    check-cast v0, LX/DTt;

    .line 1999269
    iget-object v2, v0, LX/DTt;->o:Ljava/lang/String;

    move-object v2, v2

    .line 1999270
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1999271
    invoke-virtual {v0}, LX/DTt;->a()V

    .line 1999272
    :cond_1
    return-void

    .line 1999273
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1999274
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1999275
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1999276
    invoke-virtual {p0, v0}, LX/DSB;->a(LX/0Px;)V

    .line 1999277
    iget-object v0, p0, LX/DSB;->c:LX/DSZ;

    invoke-virtual {v0}, LX/DSZ;->a()V

    .line 1999278
    return-void
.end method

.method public final b(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1999258
    if-eqz p1, :cond_0

    .line 1999259
    iput-object p1, p0, LX/DSB;->b:LX/0Px;

    .line 1999260
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1999257
    iget-object v0, p0, LX/DSB;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DSB;->c:LX/DSZ;

    invoke-virtual {v0}, LX/DSZ;->b()I

    move-result v0

    iget-object v1, p0, LX/DSB;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 1999256
    iget-object v0, p0, LX/DSB;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {}, LX/DS9;->values()[LX/DS9;

    move-result-object v1

    array-length v1, v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1999238
    invoke-virtual {p0, p1}, LX/DSB;->a(I)LX/DSf;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1999237
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1999234
    iget-object v0, p0, LX/DSB;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1999235
    sget-object v0, LX/DS9;->MEMBER_ROW:LX/DS9;

    invoke-virtual {v0}, LX/DS9;->ordinal()I

    move-result v0

    .line 1999236
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/DS9;->LOADING_BAR:LX/DS9;

    invoke-virtual {v0}, LX/DS9;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1999233
    invoke-static {}, LX/DS9;->values()[LX/DS9;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
