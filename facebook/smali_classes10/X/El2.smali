.class public LX/El2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/HttpEntity;


# instance fields
.field private final a:LX/El1;

.field private final b:Lorg/apache/http/Header;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/El1;Lorg/apache/http/Header;)V
    .locals 0
    .param p2    # Lorg/apache/http/Header;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2163983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163984
    iput-object p1, p0, LX/El2;->a:LX/El1;

    .line 2163985
    iput-object p2, p0, LX/El2;->b:Lorg/apache/http/Header;

    .line 2163986
    return-void
.end method


# virtual methods
.method public final consumeContent()V
    .locals 0

    .prologue
    .line 2163982
    return-void
.end method

.method public final getContent()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 2163981
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final getContentEncoding()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 2163980
    iget-object v0, p0, LX/El2;->b:Lorg/apache/http/Header;

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 2163979
    iget-object v0, p0, LX/El2;->a:LX/El1;

    invoke-interface {v0}, LX/El1;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getContentType()Lorg/apache/http/Header;
    .locals 3

    .prologue
    .line 2163973
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "Content-Type"

    iget-object v2, p0, LX/El2;->a:LX/El1;

    invoke-interface {v2}, LX/El1;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final isChunked()Z
    .locals 4

    .prologue
    .line 2163978
    invoke-virtual {p0}, LX/El2;->getContentLength()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 2163977
    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 2163976
    const/4 v0, 0x1

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 2163974
    iget-object v0, p0, LX/El2;->a:LX/El1;

    invoke-interface {v0, p1}, LX/El1;->a(Ljava/io/OutputStream;)V

    .line 2163975
    return-void
.end method
