.class public LX/E19;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private a:LX/0wM;

.field private b:Landroid/view/LayoutInflater;

.field private c:LX/8Sa;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0wM;Landroid/view/LayoutInflater;LX/8Sa;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2069465
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2069466
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/E19;->d:Ljava/util/List;

    .line 2069467
    iput-object p1, p0, LX/E19;->a:LX/0wM;

    .line 2069468
    iput-object p2, p0, LX/E19;->b:Landroid/view/LayoutInflater;

    .line 2069469
    iput-object p3, p0, LX/E19;->c:LX/8Sa;

    .line 2069470
    return-void
.end method

.method private a(I)Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;
    .locals 1

    .prologue
    .line 2069471
    iget-object v0, p0, LX/E19;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2069472
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/E19;->d:Ljava/util/List;

    .line 2069473
    const v0, 0x3fece69f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2069474
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2069475
    iget-object v0, p0, LX/E19;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2069476
    invoke-direct {p0, p1}, LX/E19;->a(I)Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2069477
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2069478
    if-nez p2, :cond_0

    .line 2069479
    iget-object v0, p0, LX/E19;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f03102b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2069480
    :cond_0
    invoke-direct {p0, p1}, LX/E19;->a(I)Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;

    move-result-object v1

    .line 2069481
    const v0, 0x7f0d26d0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2069482
    iget-object v2, v1, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2069483
    sget-object v2, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2069484
    iget-object v3, v1, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v3}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v3

    sget-object v4, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-static {v3, v4}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v2

    .line 2069485
    iget-object v3, p0, LX/E19;->a:LX/0wM;

    const v4, -0x958e80

    invoke-virtual {v3, v2, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2069486
    iget-object v2, v1, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->y_()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2069487
    iget-object v2, v1, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->y_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2069488
    :cond_1
    const v0, 0x7f0d0b20

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    .line 2069489
    iget-boolean v1, v1, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->b:Z

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 2069490
    return-object p2
.end method
