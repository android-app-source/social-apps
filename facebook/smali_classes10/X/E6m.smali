.class public final LX/E6m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bni;


# instance fields
.field public final synthetic a:LX/7oY;

.field public final synthetic b:LX/E6n;


# direct methods
.method public constructor <init>(LX/E6n;LX/7oY;)V
    .locals 0

    .prologue
    .line 2080592
    iput-object p1, p0, LX/E6m;->b:LX/E6n;

    iput-object p2, p0, LX/E6m;->a:LX/7oY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 6

    .prologue
    .line 2080593
    iget-object v0, p0, LX/E6m;->a:LX/7oY;

    if-nez v0, :cond_0

    .line 2080594
    :goto_0
    return-void

    .line 2080595
    :cond_0
    iget-object v0, p0, LX/E6m;->b:LX/E6n;

    iget-object v0, v0, LX/E6n;->d:LX/7vW;

    iget-object v1, p0, LX/E6m;->a:LX/7oY;

    invoke-interface {v1}, LX/7oY;->e()Ljava/lang/String;

    move-result-object v1

    const-string v3, "unknown"

    const-string v4, "reaction_dialog"

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->REACTION_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 6

    .prologue
    .line 2080596
    iget-object v0, p0, LX/E6m;->a:LX/7oY;

    if-nez v0, :cond_0

    .line 2080597
    :goto_0
    return-void

    .line 2080598
    :cond_0
    iget-object v0, p0, LX/E6m;->b:LX/E6n;

    iget-object v0, v0, LX/E6n;->c:LX/7vZ;

    iget-object v1, p0, LX/E6m;->a:LX/7oY;

    invoke-interface {v1}, LX/7oY;->e()Ljava/lang/String;

    move-result-object v1

    const-string v3, "unknown"

    const-string v4, "reaction_dialog"

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->REACTION_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
