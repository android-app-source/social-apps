.class public LX/ElE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/CountDownLatch;

.field public volatile b:Ljava/io/IOException;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/CountDownLatch;

.field private volatile d:Ljava/io/IOException;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2164130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2164131
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/ElE;->a:Ljava/util/concurrent/CountDownLatch;

    .line 2164132
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/ElE;->c:Ljava/util/concurrent/CountDownLatch;

    .line 2164133
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2164134
    iget-object v0, p0, LX/ElE;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 2164135
    return-void
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 1
    .param p1    # Ljava/io/IOException;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2164136
    iput-object p1, p0, LX/ElE;->d:Ljava/io/IOException;

    .line 2164137
    iget-object v0, p0, LX/ElE;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 2164138
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2164139
    iget-object v0, p0, LX/ElE;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 2164140
    iget-object v0, p0, LX/ElE;->b:Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 2164141
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, LX/ElE;->b:Ljava/io/IOException;

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 2164142
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2164143
    iget-object v0, p0, LX/ElE;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 2164144
    iget-object v0, p0, LX/ElE;->d:Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 2164145
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, LX/ElE;->d:Ljava/io/IOException;

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 2164146
    :cond_0
    return-void
.end method
