.class public final LX/D3s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1961354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1961355
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1961356
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1961357
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLImage;

    .line 1961358
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1961359
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1961360
    new-instance v0, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;-><init>(Landroid/net/Uri;ILcom/facebook/graphql/model/GraphQLImage;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1961361
    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1961362
    new-array v0, p1, [Lcom/facebook/video/activity/FullScreenVideoPlayerBundleItem;

    return-object v0
.end method
