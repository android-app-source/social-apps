.class public final enum LX/Clb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Clb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Clb;

.field public static final enum AUTHORS_CONTRIBUTORS_HEADER:LX/Clb;

.field public static final enum BLOCK_QUOTE:LX/Clb;

.field public static final enum BODY:LX/Clb;

.field public static final enum BYLINE:LX/Clb;

.field public static final enum CAPTION_SUBTITLE:LX/Clb;

.field public static final enum CAPTION_TITLE:LX/Clb;

.field public static final enum CODE:LX/Clb;

.field public static final enum COPYRIGHT:LX/Clb;

.field public static final enum CREDITS:LX/Clb;

.field public static final enum HEADER_ONE:LX/Clb;

.field public static final enum HEADER_TWO:LX/Clb;

.field public static final enum INLINE_RELATED_ARTICLES_HEADER:LX/Clb;

.field public static final enum KICKER:LX/Clb;

.field public static final enum PULL_QUOTE:LX/Clb;

.field public static final enum PULL_QUOTE_ATTRIBUTION:LX/Clb;

.field public static final enum RELATED_ARTICLES:LX/Clb;

.field public static final enum RELATED_ARTICLES_HEADER:LX/Clb;

.field public static final enum SUBTITLE:LX/Clb;

.field public static final enum TITLE:LX/Clb;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1932547
    new-instance v0, LX/Clb;

    const-string v1, "KICKER"

    invoke-direct {v0, v1, v3}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->KICKER:LX/Clb;

    .line 1932548
    new-instance v0, LX/Clb;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v4}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->TITLE:LX/Clb;

    .line 1932549
    new-instance v0, LX/Clb;

    const-string v1, "SUBTITLE"

    invoke-direct {v0, v1, v5}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->SUBTITLE:LX/Clb;

    .line 1932550
    new-instance v0, LX/Clb;

    const-string v1, "HEADER_ONE"

    invoke-direct {v0, v1, v6}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->HEADER_ONE:LX/Clb;

    .line 1932551
    new-instance v0, LX/Clb;

    const-string v1, "HEADER_TWO"

    invoke-direct {v0, v1, v7}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->HEADER_TWO:LX/Clb;

    .line 1932552
    new-instance v0, LX/Clb;

    const-string v1, "BODY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->BODY:LX/Clb;

    .line 1932553
    new-instance v0, LX/Clb;

    const-string v1, "PULL_QUOTE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->PULL_QUOTE:LX/Clb;

    .line 1932554
    new-instance v0, LX/Clb;

    const-string v1, "PULL_QUOTE_ATTRIBUTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->PULL_QUOTE_ATTRIBUTION:LX/Clb;

    .line 1932555
    new-instance v0, LX/Clb;

    const-string v1, "RELATED_ARTICLES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->RELATED_ARTICLES:LX/Clb;

    .line 1932556
    new-instance v0, LX/Clb;

    const-string v1, "RELATED_ARTICLES_HEADER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->RELATED_ARTICLES_HEADER:LX/Clb;

    .line 1932557
    new-instance v0, LX/Clb;

    const-string v1, "INLINE_RELATED_ARTICLES_HEADER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->INLINE_RELATED_ARTICLES_HEADER:LX/Clb;

    .line 1932558
    new-instance v0, LX/Clb;

    const-string v1, "CAPTION_TITLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->CAPTION_TITLE:LX/Clb;

    .line 1932559
    new-instance v0, LX/Clb;

    const-string v1, "CAPTION_SUBTITLE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->CAPTION_SUBTITLE:LX/Clb;

    .line 1932560
    new-instance v0, LX/Clb;

    const-string v1, "CREDITS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->CREDITS:LX/Clb;

    .line 1932561
    new-instance v0, LX/Clb;

    const-string v1, "COPYRIGHT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->COPYRIGHT:LX/Clb;

    .line 1932562
    new-instance v0, LX/Clb;

    const-string v1, "BYLINE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->BYLINE:LX/Clb;

    .line 1932563
    new-instance v0, LX/Clb;

    const-string v1, "BLOCK_QUOTE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->BLOCK_QUOTE:LX/Clb;

    .line 1932564
    new-instance v0, LX/Clb;

    const-string v1, "CODE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->CODE:LX/Clb;

    .line 1932565
    new-instance v0, LX/Clb;

    const-string v1, "AUTHORS_CONTRIBUTORS_HEADER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/Clb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clb;->AUTHORS_CONTRIBUTORS_HEADER:LX/Clb;

    .line 1932566
    const/16 v0, 0x13

    new-array v0, v0, [LX/Clb;

    sget-object v1, LX/Clb;->KICKER:LX/Clb;

    aput-object v1, v0, v3

    sget-object v1, LX/Clb;->TITLE:LX/Clb;

    aput-object v1, v0, v4

    sget-object v1, LX/Clb;->SUBTITLE:LX/Clb;

    aput-object v1, v0, v5

    sget-object v1, LX/Clb;->HEADER_ONE:LX/Clb;

    aput-object v1, v0, v6

    sget-object v1, LX/Clb;->HEADER_TWO:LX/Clb;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Clb;->BODY:LX/Clb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Clb;->PULL_QUOTE:LX/Clb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Clb;->PULL_QUOTE_ATTRIBUTION:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Clb;->RELATED_ARTICLES:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Clb;->RELATED_ARTICLES_HEADER:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Clb;->INLINE_RELATED_ARTICLES_HEADER:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Clb;->CAPTION_TITLE:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Clb;->CAPTION_SUBTITLE:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Clb;->CREDITS:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Clb;->COPYRIGHT:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Clb;->BYLINE:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Clb;->BLOCK_QUOTE:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/Clb;->CODE:LX/Clb;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/Clb;->AUTHORS_CONTRIBUTORS_HEADER:LX/Clb;

    aput-object v2, v0, v1

    sput-object v0, LX/Clb;->$VALUES:[LX/Clb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1932545
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1932546
    return-void
.end method

.method public static from(Lcom/facebook/graphql/enums/GraphQLComposedBlockType;)LX/Clb;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1932567
    if-nez p0, :cond_0

    .line 1932568
    :goto_0
    return-object v0

    .line 1932569
    :cond_0
    sget-object v1, LX/Cla;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1932570
    :pswitch_0
    sget-object v0, LX/Clb;->BODY:LX/Clb;

    goto :goto_0

    .line 1932571
    :pswitch_1
    sget-object v0, LX/Clb;->BLOCK_QUOTE:LX/Clb;

    goto :goto_0

    .line 1932572
    :pswitch_2
    sget-object v0, LX/Clb;->PULL_QUOTE:LX/Clb;

    goto :goto_0

    .line 1932573
    :pswitch_3
    sget-object v0, LX/Clb;->CODE:LX/Clb;

    goto :goto_0

    .line 1932574
    :pswitch_4
    sget-object v0, LX/Clb;->HEADER_ONE:LX/Clb;

    goto :goto_0

    .line 1932575
    :pswitch_5
    sget-object v0, LX/Clb;->HEADER_TWO:LX/Clb;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/Clb;
    .locals 1

    .prologue
    .line 1932544
    const-class v0, LX/Clb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Clb;

    return-object v0
.end method

.method public static values()[LX/Clb;
    .locals 1

    .prologue
    .line 1932543
    sget-object v0, LX/Clb;->$VALUES:[LX/Clb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Clb;

    return-object v0
.end method
