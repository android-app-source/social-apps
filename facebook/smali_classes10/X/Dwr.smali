.class public LX/Dwr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 0
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2061711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2061712
    iput-object p1, p0, LX/Dwr;->a:Ljava/lang/Boolean;

    .line 2061713
    return-void
.end method

.method public static b(LX/0QB;)LX/Dwr;
    .locals 2

    .prologue
    .line 2061714
    new-instance v1, LX/Dwr;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-direct {v1, v0}, LX/Dwr;-><init>(Ljava/lang/Boolean;)V

    .line 2061715
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)LX/Dwq;
    .locals 2
    .param p1    # Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2061716
    if-nez p1, :cond_0

    .line 2061717
    sget-object v0, LX/Dwq;->LOADING:LX/Dwq;

    .line 2061718
    :goto_0
    return-object v0

    .line 2061719
    :cond_0
    iget-boolean v0, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->e:Z

    move v0, v0

    .line 2061720
    if-eqz v0, :cond_1

    .line 2061721
    sget-object v0, LX/Dwq;->NONE:LX/Dwq;

    goto :goto_0

    .line 2061722
    :cond_1
    iget-boolean v0, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->b:Z

    move v0, v0

    .line 2061723
    if-eqz v0, :cond_2

    .line 2061724
    iget-object v0, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    move-object v0, v0

    .line 2061725
    iget-boolean v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->j:Z

    move v0, v1

    .line 2061726
    if-eqz v0, :cond_2

    .line 2061727
    sget-object v0, LX/Dwq;->MOMENTS_PROMOTION:LX/Dwq;

    goto :goto_0

    .line 2061728
    :cond_2
    iget-boolean v0, p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    move v0, v0

    .line 2061729
    if-eqz v0, :cond_3

    .line 2061730
    sget-object v0, LX/Dwq;->MOMENTS_SEGUE:LX/Dwq;

    goto :goto_0

    .line 2061731
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-le v0, v1, :cond_4

    .line 2061732
    sget-object v0, LX/Dwq;->SYNC_UNSUPPORTED:LX/Dwq;

    goto :goto_0

    .line 2061733
    :cond_4
    iget-object v0, p0, LX/Dwr;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_6

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-gt v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2061734
    if-eqz v0, :cond_5

    sget-object v0, LX/Dwq;->SYNC:LX/Dwq;

    goto :goto_0

    :cond_5
    sget-object v0, LX/Dwq;->NONE:LX/Dwq;

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method
