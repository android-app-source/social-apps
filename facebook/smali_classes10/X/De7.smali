.class public final LX/De7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2021089
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2021090
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2021091
    :goto_0
    return v1

    .line 2021092
    :cond_0
    const-string v8, "is_messenger_user"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2021093
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v5, v3

    move v3, v2

    .line 2021094
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 2021095
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2021096
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2021097
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2021098
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2021099
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2021100
    :cond_2
    const-string v8, "is_partial"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2021101
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2021102
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2021103
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2021104
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2021105
    if-eqz v3, :cond_5

    .line 2021106
    invoke-virtual {p1, v2, v5}, LX/186;->a(IZ)V

    .line 2021107
    :cond_5
    if-eqz v0, :cond_6

    .line 2021108
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 2021109
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 2021110
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2021111
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2021112
    if-eqz v0, :cond_0

    .line 2021113
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2021114
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2021115
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2021116
    if-eqz v0, :cond_1

    .line 2021117
    const-string v1, "is_messenger_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2021118
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2021119
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2021120
    if-eqz v0, :cond_2

    .line 2021121
    const-string v1, "is_partial"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2021122
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2021123
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2021124
    return-void
.end method
