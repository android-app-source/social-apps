.class public final LX/DuD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 30

    .prologue
    .line 2055946
    const/16 v26, 0x0

    .line 2055947
    const-wide/16 v24, 0x0

    .line 2055948
    const/16 v23, 0x0

    .line 2055949
    const/16 v22, 0x0

    .line 2055950
    const/16 v21, 0x0

    .line 2055951
    const/16 v20, 0x0

    .line 2055952
    const-wide/16 v18, 0x0

    .line 2055953
    const/16 v17, 0x0

    .line 2055954
    const/16 v16, 0x0

    .line 2055955
    const/4 v15, 0x0

    .line 2055956
    const/4 v14, 0x0

    .line 2055957
    const/4 v13, 0x0

    .line 2055958
    const/4 v12, 0x0

    .line 2055959
    const/4 v11, 0x0

    .line 2055960
    const/4 v10, 0x0

    .line 2055961
    const/4 v9, 0x0

    .line 2055962
    const/4 v8, 0x0

    .line 2055963
    const/4 v7, 0x0

    .line 2055964
    const/4 v6, 0x0

    .line 2055965
    const/4 v5, 0x0

    .line 2055966
    const/4 v4, 0x0

    .line 2055967
    const/4 v3, 0x0

    .line 2055968
    const/4 v2, 0x0

    .line 2055969
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_19

    .line 2055970
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2055971
    const/4 v2, 0x0

    .line 2055972
    :goto_0
    return v2

    .line 2055973
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_12

    .line 2055974
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2055975
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2055976
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 2055977
    const-string v6, "buyer"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2055978
    invoke-static/range {p0 .. p1}, LX/DuK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto :goto_1

    .line 2055979
    :cond_1
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2055980
    const/4 v2, 0x1

    .line 2055981
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2055982
    :cond_2
    const-string v6, "for_sale_group_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2055983
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 2055984
    :cond_3
    const-string v6, "group_thread_fbid"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2055985
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 2055986
    :cond_4
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2055987
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 2055988
    :cond_5
    const-string v6, "location_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2055989
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 2055990
    :cond_6
    const-string v6, "modified_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2055991
    const/4 v2, 0x1

    .line 2055992
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v12, v2

    move-wide/from16 v24, v6

    goto/16 :goto_1

    .line 2055993
    :cond_7
    const-string v6, "peer_to_peer_platform_item"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2055994
    invoke-static/range {p0 .. p1}, LX/DuG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 2055995
    :cond_8
    const-string v6, "peer_to_peer_platform_product_item"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2055996
    invoke-static/range {p0 .. p1}, LX/DuC;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 2055997
    :cond_9
    const-string v6, "selected_shipping_address"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2055998
    invoke-static/range {p0 .. p1}, LX/Du8;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 2055999
    :cond_a
    const-string v6, "selected_shipping_option"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2056000
    invoke-static/range {p0 .. p1}, LX/DuI;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 2056001
    :cond_b
    const-string v6, "seller"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2056002
    invoke-static/range {p0 .. p1}, LX/DuK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2056003
    :cond_c
    const-string v6, "shipping_options"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2056004
    invoke-static/range {p0 .. p1}, LX/DuI;->b(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 2056005
    :cond_d
    const-string v6, "should_show_pay_button"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2056006
    const/4 v2, 0x1

    .line 2056007
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v2

    move/from16 v16, v6

    goto/16 :goto_1

    .line 2056008
    :cond_e
    const-string v6, "should_show_to_buyer"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 2056009
    const/4 v2, 0x1

    .line 2056010
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v10, v2

    move v15, v6

    goto/16 :goto_1

    .line 2056011
    :cond_f
    const-string v6, "should_show_to_seller"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 2056012
    const/4 v2, 0x1

    .line 2056013
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move v14, v6

    goto/16 :goto_1

    .line 2056014
    :cond_10
    const-string v6, "should_show_to_viewer"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2056015
    const/4 v2, 0x1

    .line 2056016
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v8, v2

    move v13, v6

    goto/16 :goto_1

    .line 2056017
    :cond_11
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2056018
    :cond_12
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2056019
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056020
    if-eqz v3, :cond_13

    .line 2056021
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2056022
    :cond_13
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056023
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056024
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056025
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056026
    if-eqz v12, :cond_14

    .line 2056027
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2056028
    :cond_14
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056029
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056030
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056031
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056032
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056033
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056034
    if-eqz v11, :cond_15

    .line 2056035
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2056036
    :cond_15
    if-eqz v10, :cond_16

    .line 2056037
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 2056038
    :cond_16
    if-eqz v9, :cond_17

    .line 2056039
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->a(IZ)V

    .line 2056040
    :cond_17
    if-eqz v8, :cond_18

    .line 2056041
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 2056042
    :cond_18
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_19
    move/from16 v27, v22

    move/from16 v28, v23

    move/from16 v29, v26

    move/from16 v22, v17

    move/from16 v23, v20

    move/from16 v26, v21

    move/from16 v17, v12

    move/from16 v20, v15

    move/from16 v21, v16

    move/from16 v16, v11

    move v15, v10

    move v12, v6

    move v11, v5

    move v10, v4

    move-wide/from16 v4, v24

    move-wide/from16 v24, v18

    move/from16 v19, v14

    move/from16 v18, v13

    move v13, v8

    move v14, v9

    move v9, v3

    move v8, v2

    move v3, v7

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2056043
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2056044
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056045
    if-eqz v0, :cond_0

    .line 2056046
    const-string v1, "buyer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056047
    invoke-static {p0, v0, p2}, LX/DuK;->a(LX/15i;ILX/0nX;)V

    .line 2056048
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2056049
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 2056050
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056051
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2056052
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056053
    if-eqz v0, :cond_2

    .line 2056054
    const-string v1, "for_sale_group_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056055
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056056
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056057
    if-eqz v0, :cond_3

    .line 2056058
    const-string v1, "group_thread_fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056059
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056060
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056061
    if-eqz v0, :cond_4

    .line 2056062
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056063
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056064
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056065
    if-eqz v0, :cond_5

    .line 2056066
    const-string v1, "location_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056067
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056068
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2056069
    cmp-long v2, v0, v4

    if-eqz v2, :cond_6

    .line 2056070
    const-string v2, "modified_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056071
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2056072
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056073
    if-eqz v0, :cond_7

    .line 2056074
    const-string v1, "peer_to_peer_platform_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056075
    invoke-static {p0, v0, p2, p3}, LX/DuG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2056076
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056077
    if-eqz v0, :cond_8

    .line 2056078
    const-string v1, "peer_to_peer_platform_product_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056079
    invoke-static {p0, v0, p2, p3}, LX/DuC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2056080
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056081
    if-eqz v0, :cond_9

    .line 2056082
    const-string v1, "selected_shipping_address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056083
    invoke-static {p0, v0, p2, p3}, LX/Du8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2056084
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056085
    if-eqz v0, :cond_a

    .line 2056086
    const-string v1, "selected_shipping_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056087
    invoke-static {p0, v0, p2}, LX/DuI;->a(LX/15i;ILX/0nX;)V

    .line 2056088
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056089
    if-eqz v0, :cond_b

    .line 2056090
    const-string v1, "seller"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056091
    invoke-static {p0, v0, p2}, LX/DuK;->a(LX/15i;ILX/0nX;)V

    .line 2056092
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056093
    if-eqz v0, :cond_d

    .line 2056094
    const-string v1, "shipping_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056095
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2056096
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_c

    .line 2056097
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/DuI;->a(LX/15i;ILX/0nX;)V

    .line 2056098
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2056099
    :cond_c
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2056100
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2056101
    if-eqz v0, :cond_e

    .line 2056102
    const-string v1, "should_show_pay_button"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056103
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2056104
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2056105
    if-eqz v0, :cond_f

    .line 2056106
    const-string v1, "should_show_to_buyer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056107
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2056108
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2056109
    if-eqz v0, :cond_10

    .line 2056110
    const-string v1, "should_show_to_seller"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056111
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2056112
    :cond_10
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2056113
    if-eqz v0, :cond_11

    .line 2056114
    const-string v1, "should_show_to_viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056115
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2056116
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2056117
    return-void
.end method
