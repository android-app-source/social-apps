.class public LX/Cvy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Cvy;


# instance fields
.field public final a:LX/11i;

.field private final b:LX/0go;

.field private final c:LX/2Fd;

.field public d:Z


# direct methods
.method public constructor <init>(LX/11i;LX/0go;LX/2Fd;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1949783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949784
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cvy;->d:Z

    .line 1949785
    iput-object p1, p0, LX/Cvy;->a:LX/11i;

    .line 1949786
    iput-object p2, p0, LX/Cvy;->b:LX/0go;

    .line 1949787
    iput-object p3, p0, LX/Cvy;->c:LX/2Fd;

    .line 1949788
    return-void
.end method

.method public static a(LX/0QB;)LX/Cvy;
    .locals 6

    .prologue
    .line 1949789
    sget-object v0, LX/Cvy;->e:LX/Cvy;

    if-nez v0, :cond_1

    .line 1949790
    const-class v1, LX/Cvy;

    monitor-enter v1

    .line 1949791
    :try_start_0
    sget-object v0, LX/Cvy;->e:LX/Cvy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1949792
    if-eqz v2, :cond_0

    .line 1949793
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1949794
    new-instance p0, LX/Cvy;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0go;->a(LX/0QB;)LX/0go;

    move-result-object v4

    check-cast v4, LX/0go;

    invoke-static {v0}, LX/2Fd;->a(LX/0QB;)LX/2Fd;

    move-result-object v5

    check-cast v5, LX/2Fd;

    invoke-direct {p0, v3, v4, v5}, LX/Cvy;-><init>(LX/11i;LX/0go;LX/2Fd;)V

    .line 1949795
    move-object v0, p0

    .line 1949796
    sput-object v0, LX/Cvy;->e:LX/Cvy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1949797
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1949798
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1949799
    :cond_1
    sget-object v0, LX/Cvy;->e:LX/Cvy;

    return-object v0

    .line 1949800
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1949801
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Cvy;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1949802
    iget-object v0, p0, LX/Cvy;->a:LX/11i;

    sget-object v1, LX/Cvx;->b:LX/Cvw;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1949803
    if-eqz v0, :cond_0

    .line 1949804
    const v1, -0x7980db06

    invoke-static {v0, p1, v1}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949805
    :cond_0
    return-void
.end method

.method public static h(LX/Cvy;)V
    .locals 4

    .prologue
    .line 1949806
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cvy;->d:Z

    .line 1949807
    iget-object v0, p0, LX/Cvy;->a:LX/11i;

    sget-object v1, LX/Cvx;->b:LX/Cvw;

    const-string v2, "immersive_views_on"

    const-string v3, "true"

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/11i;->a(LX/0Pq;LX/0P1;)LX/11o;

    .line 1949808
    iget-object v0, p0, LX/Cvy;->a:LX/11i;

    sget-object v1, LX/Cvx;->b:LX/Cvw;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1949809
    if-eqz v0, :cond_0

    .line 1949810
    const-string v1, "immersive_views_11_06"

    iget-object v2, p0, LX/Cvy;->c:LX/2Fd;

    .line 1949811
    iget-object v3, v2, LX/2Fd;->a:LX/0pZ;

    invoke-interface {v3, v2}, LX/0pZ;->c(LX/2Ff;)LX/3yG;

    move-result-object v3

    .line 1949812
    iget-object v2, v3, LX/3yG;->a:Ljava/lang/String;

    move-object v3, v2

    .line 1949813
    move-object v2, v3

    .line 1949814
    invoke-interface {v0, v1, v2}, LX/11o;->b(Ljava/lang/String;Ljava/lang/String;)LX/11o;

    .line 1949815
    :cond_0
    return-void
.end method

.method public static i(LX/Cvy;)V
    .locals 2

    .prologue
    .line 1949816
    iget-object v0, p0, LX/Cvy;->a:LX/11i;

    sget-object v1, LX/Cvx;->b:LX/Cvw;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 1949817
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 1949818
    const-string v0, "exited_search_via_search_result"

    invoke-static {p0, v0}, LX/Cvy;->a(LX/Cvy;Ljava/lang/String;)V

    .line 1949819
    invoke-static {p0}, LX/Cvy;->i(LX/Cvy;)V

    .line 1949820
    return-void
.end method
