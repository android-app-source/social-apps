.class public LX/CkL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:LX/Cka;

.field public B:LX/Cka;

.field public C:LX/Cka;

.field public D:LX/Cka;

.field public E:LX/Cka;

.field public F:LX/Cka;

.field public G:LX/Cka;

.field public H:LX/Cka;

.field public I:LX/Cka;

.field public a:Z

.field public b:I

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:LX/CkX;

.field public i:Ljava/lang/String;

.field public j:LX/CkS;

.field public k:LX/CkS;

.field public l:LX/CkO;

.field public m:LX/CkO;

.field public n:LX/CkO;

.field public o:LX/CkO;

.field public p:LX/CkO;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CkP;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/CkO;",
            ">;"
        }
    .end annotation
.end field

.field public s:Z

.field public t:I

.field public u:LX/CkS;

.field public v:LX/Cke;

.field public w:F

.field public x:F

.field public y:LX/Cka;

.field public z:LX/Cka;


# direct methods
.method public constructor <init>(LX/15i;ILX/CkX;ZILX/Cke;)V
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1930649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930650
    iput-boolean v3, p0, LX/CkL;->a:Z

    .line 1930651
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CkL;->q:Ljava/util/List;

    .line 1930652
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    .line 1930653
    const/4 v0, -0x1

    iput v0, p0, LX/CkL;->t:I

    .line 1930654
    iput-object p3, p0, LX/CkL;->h:LX/CkX;

    .line 1930655
    new-instance v0, LX/CkS;

    sget-object v1, LX/Ckb;->HEADLINE:LX/Ckb;

    const/16 v4, 0x9

    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v4

    invoke-direct {v0, v1, p1, v4}, LX/CkS;-><init>(LX/Ckb;LX/15i;I)V

    iput-object v0, p0, LX/CkL;->j:LX/CkS;

    .line 1930656
    new-instance v0, LX/CkS;

    sget-object v1, LX/Ckb;->DESCRIPTION:LX/Ckb;

    const/4 v4, 0x6

    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v4

    invoke-direct {v0, v1, p1, v4}, LX/CkS;-><init>(LX/Ckb;LX/15i;I)V

    iput-object v0, p0, LX/CkL;->k:LX/CkS;

    .line 1930657
    new-instance v0, LX/CkS;

    sget-object v1, LX/Ckb;->BYLINE:LX/Ckb;

    const/4 v4, 0x3

    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v4

    invoke-direct {v0, v1, p1, v4}, LX/CkS;-><init>(LX/Ckb;LX/15i;I)V

    iput-object v0, p0, LX/CkL;->u:LX/CkS;

    .line 1930658
    new-instance v1, LX/CkO;

    sget-object v4, LX/Ckb;->SOURCE_IMAGE:LX/Ckb;

    const/16 v0, 0xe

    const-class v5, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p1, p2, v0, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-direct {v1, v4, v0}, LX/CkO;-><init>(LX/Ckb;Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;)V

    iput-object v1, p0, LX/CkL;->l:LX/CkO;

    .line 1930659
    new-instance v1, LX/CkO;

    sget-object v4, LX/Ckb;->COVER_IMAGE:LX/Ckb;

    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p1, p2, v7, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-direct {v1, v4, v0}, LX/CkO;-><init>(LX/Ckb;Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;)V

    iput-object v1, p0, LX/CkL;->m:LX/CkO;

    .line 1930660
    new-instance v1, LX/CkO;

    sget-object v4, LX/Ckb;->BYLINE_AREA:LX/Ckb;

    const/4 v0, 0x2

    const-class v5, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p1, p2, v0, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-direct {v1, v4, v0}, LX/CkO;-><init>(LX/Ckb;Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;)V

    iput-object v1, p0, LX/CkL;->p:LX/CkO;

    .line 1930661
    new-instance v1, LX/CkO;

    sget-object v4, LX/Ckb;->COVER_VIDEO:LX/Ckb;

    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p1, p2, v7, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-direct {v1, v4, v0}, LX/CkO;-><init>(LX/Ckb;Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;)V

    iput-object v1, p0, LX/CkL;->n:LX/CkO;

    .line 1930662
    iget-object v0, p0, LX/CkL;->n:LX/CkO;

    sget-object v1, LX/Ckb;->COVER_VIDEO:LX/Ckb;

    iget-object v1, v1, LX/Ckb;->value:Ljava/lang/String;

    .line 1930663
    iput-object v1, v0, LX/CkO;->c:Ljava/lang/String;

    .line 1930664
    iget-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    iget-object v1, p0, LX/CkL;->j:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v4, p0, LX/CkL;->j:LX/CkS;

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930665
    iget-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    iget-object v1, p0, LX/CkL;->k:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v4, p0, LX/CkL;->k:LX/CkS;

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930666
    iget-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    iget-object v1, p0, LX/CkL;->u:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v4, p0, LX/CkL;->u:LX/CkS;

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930667
    iget-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    iget-object v1, p0, LX/CkL;->l:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v4, p0, LX/CkL;->l:LX/CkO;

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930668
    iget-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    iget-object v1, p0, LX/CkL;->m:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v4, p0, LX/CkL;->m:LX/CkO;

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930669
    iget-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    iget-object v1, p0, LX/CkL;->p:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v4, p0, LX/CkL;->p:LX/CkO;

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930670
    iget-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    iget-object v1, p0, LX/CkL;->n:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v4, p0, LX/CkL;->n:LX/CkO;

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930671
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p1, p2, v2, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 1930672
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    :goto_0
    move v4, v2

    .line 1930673
    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 1930674
    new-instance v5, LX/CkP;

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-direct {v5, v0, v4}, LX/CkP;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;I)V

    .line 1930675
    iget-object v0, p0, LX/CkL;->q:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1930676
    iget-object v0, v5, LX/CkO;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1930677
    iget-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    iget-object v6, v5, LX/CkO;->c:Ljava/lang/String;

    invoke-interface {v0, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930678
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 1930679
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1930680
    move-object v1, v0

    goto :goto_0

    .line 1930681
    :cond_2
    iput-boolean p4, p0, LX/CkL;->s:Z

    .line 1930682
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p1, p2, v3, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_5

    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p1, p2, v3, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkY;->b(Ljava/lang/String;)I

    move-result v0

    :goto_2
    iput v0, p0, LX/CkL;->b:I

    .line 1930683
    iput p5, p0, LX/CkL;->c:I

    .line 1930684
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p1, p2, v7, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1930685
    if-eqz v0, :cond_3

    move v2, v3

    :cond_3
    iput-boolean v2, p0, LX/CkL;->d:Z

    .line 1930686
    iget-boolean v1, p0, LX/CkL;->d:Z

    if-eqz v1, :cond_4

    .line 1930687
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->m()Z

    move-result v0

    iput-boolean v0, p0, LX/CkL;->e:Z

    .line 1930688
    :cond_4
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->h(II)Z

    move-result v0

    iput-boolean v0, p0, LX/CkL;->f:Z

    .line 1930689
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->h(II)Z

    move-result v0

    iput-boolean v0, p0, LX/CkL;->g:Z

    .line 1930690
    iput-object p6, p0, LX/CkL;->v:LX/Cke;

    .line 1930691
    return-void

    :cond_5
    move v0, v2

    .line 1930692
    goto :goto_2
.end method

.method private static a(LX/CkQ;LX/CkX;LX/Cka;)F
    .locals 2

    .prologue
    .line 1930770
    iget-object v0, p0, LX/CkQ;->b:LX/CkN;

    iget-object v0, v0, LX/CkN;->b:Ljava/lang/String;

    invoke-static {v0, p1, p2}, LX/CkY;->a(Ljava/lang/String;LX/CkX;LX/Cka;)F

    move-result v0

    .line 1930771
    iget-object v1, p0, LX/CkQ;->b:LX/CkN;

    iget-object v1, v1, LX/CkN;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1930772
    iget-object v1, p0, LX/CkQ;->b:LX/CkN;

    iget-object v1, v1, LX/CkN;->c:Ljava/lang/String;

    invoke-static {v1, p1, p2}, LX/CkY;->a(Ljava/lang/String;LX/CkX;LX/Cka;)F

    move-result v1

    add-float/2addr v0, v1

    .line 1930773
    :cond_0
    return v0
.end method

.method public static a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;
    .locals 15

    .prologue
    .line 1930774
    iget-object v2, p0, LX/CkL;->r:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CkO;

    .line 1930775
    if-nez v2, :cond_0

    .line 1930776
    new-instance v2, Landroid/graphics/RectF;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1930777
    :goto_0
    return-object v2

    .line 1930778
    :cond_0
    iget-object v3, v2, LX/CkO;->d:LX/Ckb;

    .line 1930779
    iget-object v8, v2, LX/CkO;->e:LX/CkR;

    .line 1930780
    new-instance v9, Landroid/graphics/PointF;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v9, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1930781
    if-nez v8, :cond_1

    .line 1930782
    new-instance v2, Landroid/graphics/RectF;

    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v4, v9, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p2

    iget v5, v0, LX/Cka;->a:F

    move-object/from16 v0, p2

    iget v6, v0, LX/Cka;->b:F

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    .line 1930783
    :cond_1
    iget-boolean v4, v8, LX/CkR;->i:Z

    if-eqz v4, :cond_2

    iget-object v4, v8, LX/CkR;->j:LX/CkQ;

    iget-object v4, v4, LX/CkQ;->a:LX/CkZ;

    sget-object v5, LX/CkZ;->EXACT:LX/CkZ;

    if-ne v4, v5, :cond_2

    .line 1930784
    iget-object v4, v8, LX/CkR;->j:LX/CkQ;

    iget-object v5, p0, LX/CkL;->h:LX/CkX;

    move-object/from16 v0, p2

    invoke-static {v4, v5, v0}, LX/CkL;->a(LX/CkQ;LX/CkX;LX/Cka;)F

    move-result v4

    move-object/from16 v0, p2

    iput v4, v0, LX/Cka;->a:F

    .line 1930785
    :cond_2
    iget-boolean v4, v8, LX/CkR;->k:Z

    if-eqz v4, :cond_3

    iget-object v4, v8, LX/CkR;->l:LX/CkQ;

    iget-object v4, v4, LX/CkQ;->a:LX/CkZ;

    sget-object v5, LX/CkZ;->EXACT:LX/CkZ;

    if-ne v4, v5, :cond_3

    .line 1930786
    iget-object v4, v8, LX/CkR;->l:LX/CkQ;

    iget-object v5, p0, LX/CkL;->h:LX/CkX;

    move-object/from16 v0, p2

    invoke-static {v4, v5, v0}, LX/CkL;->b(LX/CkQ;LX/CkX;LX/Cka;)F

    move-result v4

    move-object/from16 v0, p2

    iput v4, v0, LX/Cka;->b:F

    .line 1930787
    :cond_3
    iget-object v4, v8, LX/CkR;->a:LX/Ckc;

    sget-object v5, LX/Ckc;->LEFT:LX/Ckc;

    if-ne v4, v5, :cond_e

    .line 1930788
    iget-object v4, p0, LX/CkL;->h:LX/CkX;

    iget v4, v4, LX/CkX;->b:F

    iget v5, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 1930789
    :cond_4
    :goto_1
    invoke-static {v3}, LX/Ckb;->isTextElement(LX/Ckb;)Z

    move-result v4

    .line 1930790
    const/4 v5, 0x0

    .line 1930791
    const/4 v3, 0x0

    .line 1930792
    iget-object v6, v8, LX/CkR;->e:LX/Ckf;

    sget-object v7, LX/Ckf;->TOP:LX/Ckf;

    if-ne v6, v7, :cond_14

    .line 1930793
    iget v5, v9, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, LX/CkL;->h:LX/CkX;

    iget v6, v6, LX/CkX;->a:F

    invoke-virtual {v9, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    move v5, v4

    .line 1930794
    :cond_5
    :goto_2
    if-eqz v5, :cond_6

    .line 1930795
    iget v5, v9, Landroid/graphics/PointF;->x:F

    iget v6, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v6}, LX/Ckm;->a(F)F

    move-result v6

    invoke-virtual {v9, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 1930796
    :cond_6
    if-eqz v3, :cond_7

    .line 1930797
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v5, v9, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p2

    iget v6, v0, LX/Cka;->b:F

    add-float/2addr v5, v6

    move-object/from16 v0, p2

    invoke-static {v5, v0}, LX/Ckm;->a(FLX/Cka;)F

    move-result v5

    invoke-virtual {v9, v3, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 1930798
    :cond_7
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v5, v8, LX/CkR;->b:F

    add-float/2addr v3, v5

    iget v5, v9, Landroid/graphics/PointF;->y:F

    iget v6, v8, LX/CkR;->f:F

    add-float/2addr v5, v6

    invoke-virtual {v9, v3, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 1930799
    iget-boolean v3, v8, LX/CkR;->i:Z

    if-eqz v3, :cond_b

    iget-object v3, v8, LX/CkR;->j:LX/CkQ;

    iget-object v3, v3, LX/CkQ;->a:LX/CkZ;

    sget-object v5, LX/CkZ;->EXACT:LX/CkZ;

    if-eq v3, v5, :cond_b

    .line 1930800
    iget-object v3, v8, LX/CkR;->j:LX/CkQ;

    iget-object v3, v3, LX/CkQ;->a:LX/CkZ;

    sget-object v5, LX/CkZ;->MAX:LX/CkZ;

    if-ne v3, v5, :cond_8

    .line 1930801
    iget-object v3, v8, LX/CkR;->j:LX/CkQ;

    iget-object v5, p0, LX/CkL;->h:LX/CkX;

    new-instance v6, LX/Cka;

    iget v7, p0, LX/CkL;->w:F

    iget v10, p0, LX/CkL;->x:F

    invoke-direct {v6, v7, v10}, LX/Cka;-><init>(FF)V

    invoke-static {v3, v5, v6}, LX/CkL;->a(LX/CkQ;LX/CkX;LX/Cka;)F

    move-result v3

    .line 1930802
    move-object/from16 v0, p2

    iget v5, v0, LX/Cka;->a:F

    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, LX/Cka;->a:F

    .line 1930803
    :cond_8
    iget-object v3, v8, LX/CkR;->j:LX/CkQ;

    iget-object v3, v3, LX/CkQ;->a:LX/CkZ;

    invoke-static {v3}, LX/CkZ;->hasElementArgument(LX/CkZ;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1930804
    iget v3, v9, Landroid/graphics/PointF;->x:F

    .line 1930805
    iget v5, p0, LX/CkL;->w:F

    iget-object v6, p0, LX/CkL;->h:LX/CkX;

    iget v6, v6, LX/CkX;->b:F

    sub-float/2addr v5, v6

    .line 1930806
    iget-object v6, v8, LX/CkR;->j:LX/CkQ;

    invoke-virtual {v6}, LX/CkQ;->a()Ljava/lang/String;

    move-result-object v6

    .line 1930807
    move/from16 v0, p3

    invoke-direct {p0, v6, v0}, LX/CkL;->a(Ljava/lang/String;Z)Landroid/graphics/RectF;

    move-result-object v7

    .line 1930808
    if-eqz v6, :cond_9

    iget-object v10, v8, LX/CkR;->j:LX/CkQ;

    iget-object v10, v10, LX/CkQ;->a:LX/CkZ;

    sget-object v11, LX/CkZ;->BEFORE:LX/CkZ;

    if-ne v10, v11, :cond_9

    .line 1930809
    iget-object v5, v8, LX/CkR;->j:LX/CkQ;

    iget-object v10, p0, LX/CkL;->h:LX/CkX;

    new-instance v11, LX/Cka;

    iget v12, p0, LX/CkL;->w:F

    iget v13, p0, LX/CkL;->x:F

    invoke-direct {v11, v12, v13}, LX/Cka;-><init>(FF)V

    invoke-static {v5, v10, v11}, LX/CkL;->a(LX/CkQ;LX/CkX;LX/Cka;)F

    move-result v5

    .line 1930810
    iget v10, v7, Landroid/graphics/RectF;->left:F

    add-float/2addr v5, v10

    .line 1930811
    :cond_9
    if-eqz v6, :cond_a

    iget-object v6, v8, LX/CkR;->j:LX/CkQ;

    iget-object v6, v6, LX/CkQ;->a:LX/CkZ;

    sget-object v10, LX/CkZ;->AFTER:LX/CkZ;

    if-ne v6, v10, :cond_a

    .line 1930812
    iget-object v3, v8, LX/CkR;->j:LX/CkQ;

    iget-object v6, p0, LX/CkL;->h:LX/CkX;

    new-instance v10, LX/Cka;

    iget v11, p0, LX/CkL;->w:F

    iget v12, p0, LX/CkL;->x:F

    invoke-direct {v10, v11, v12}, LX/Cka;-><init>(FF)V

    invoke-static {v3, v6, v10}, LX/CkL;->a(LX/CkQ;LX/CkX;LX/Cka;)F

    move-result v3

    .line 1930813
    iget v6, v7, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v6

    .line 1930814
    :cond_a
    sub-float/2addr v5, v3

    .line 1930815
    move-object/from16 v0, p2

    iget v6, v0, LX/Cka;->a:F

    cmpl-float v6, v6, v5

    if-lez v6, :cond_b

    .line 1930816
    move-object/from16 v0, p2

    iput v5, v0, LX/Cka;->a:F

    .line 1930817
    iget v5, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v3, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 1930818
    :cond_b
    const/4 v3, 0x0

    .line 1930819
    iget-boolean v5, v8, LX/CkR;->k:Z

    if-eqz v5, :cond_d

    iget-object v5, v8, LX/CkR;->l:LX/CkQ;

    iget-object v5, v5, LX/CkQ;->a:LX/CkZ;

    sget-object v6, LX/CkZ;->EXACT:LX/CkZ;

    if-eq v5, v6, :cond_d

    .line 1930820
    iget-object v5, v8, LX/CkR;->l:LX/CkQ;

    iget-object v5, v5, LX/CkQ;->a:LX/CkZ;

    sget-object v6, LX/CkZ;->MAX:LX/CkZ;

    if-ne v5, v6, :cond_1d

    .line 1930821
    iget-object v5, v8, LX/CkR;->l:LX/CkQ;

    iget-object v6, p0, LX/CkL;->h:LX/CkX;

    new-instance v7, LX/Cka;

    iget v8, p0, LX/CkL;->w:F

    iget v10, p0, LX/CkL;->x:F

    invoke-direct {v7, v8, v10}, LX/Cka;-><init>(FF)V

    invoke-static {v5, v6, v7}, LX/CkL;->b(LX/CkQ;LX/CkX;LX/Cka;)F

    move-result v5

    .line 1930822
    move-object/from16 v0, p2

    iget v6, v0, LX/Cka;->b:F

    cmpl-float v6, v6, v5

    if-lez v6, :cond_c

    .line 1930823
    move-object/from16 v0, p2

    iput v5, v0, LX/Cka;->b:F

    move v3, v4

    .line 1930824
    :cond_c
    :goto_3
    if-eqz v3, :cond_d

    .line 1930825
    check-cast v2, LX/CkS;

    .line 1930826
    if-eqz p3, :cond_1e

    iget-object v2, v2, LX/CkS;->h:LX/Ckm;

    .line 1930827
    :goto_4
    move-object/from16 v0, p2

    iget v3, v0, LX/Cka;->b:F

    iget v4, v2, LX/Ckm;->c:F

    div-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-float v3, v4

    iget v4, v2, LX/Ckm;->c:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p2

    iput v3, v0, LX/Cka;->b:F

    .line 1930828
    move-object/from16 v0, p2

    iget v3, v0, LX/Cka;->b:F

    iget v2, v2, LX/Ckm;->c:F

    cmpg-float v2, v3, v2

    if-gez v2, :cond_d

    .line 1930829
    const/4 v2, 0x0

    move-object/from16 v0, p2

    iput v2, v0, LX/Cka;->b:F

    .line 1930830
    :cond_d
    new-instance v2, Landroid/graphics/RectF;

    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v4, v9, Landroid/graphics/PointF;->y:F

    iget v5, v9, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p2

    iget v6, v0, LX/Cka;->a:F

    add-float/2addr v5, v6

    iget v6, v9, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p2

    iget v7, v0, LX/Cka;->b:F

    add-float/2addr v6, v7

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_0

    .line 1930831
    :cond_e
    iget-object v4, v8, LX/CkR;->a:LX/Ckc;

    sget-object v5, LX/Ckc;->RIGHT:LX/Ckc;

    if-ne v4, v5, :cond_f

    .line 1930832
    iget v4, p0, LX/CkL;->w:F

    iget-object v5, p0, LX/CkL;->h:LX/CkX;

    iget v5, v5, LX/CkX;->b:F

    sub-float/2addr v4, v5

    move-object/from16 v0, p2

    iget v5, v0, LX/Cka;->a:F

    sub-float/2addr v4, v5

    iget v5, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_1

    .line 1930833
    :cond_f
    iget-object v4, v8, LX/CkR;->a:LX/Ckc;

    sget-object v5, LX/Ckc;->LEFT_FLUSH:LX/Ckc;

    if-ne v4, v5, :cond_10

    .line 1930834
    const/4 v4, 0x0

    iget v5, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_1

    .line 1930835
    :cond_10
    iget-object v4, v8, LX/CkR;->a:LX/Ckc;

    sget-object v5, LX/Ckc;->CENTER:LX/Ckc;

    if-ne v4, v5, :cond_11

    .line 1930836
    iget v4, p0, LX/CkL;->w:F

    move-object/from16 v0, p2

    iget v5, v0, LX/Cka;->a:F

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget v5, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_1

    .line 1930837
    :cond_11
    iget-object v4, v8, LX/CkR;->a:LX/Ckc;

    invoke-static {v4}, LX/Ckc;->hasElementArgument(LX/Ckc;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1930838
    iget-object v4, v8, LX/CkR;->d:Ljava/lang/String;

    move/from16 v0, p3

    invoke-direct {p0, v4, v0}, LX/CkL;->a(Ljava/lang/String;Z)Landroid/graphics/RectF;

    move-result-object v4

    .line 1930839
    iget-object v5, v8, LX/CkR;->a:LX/Ckc;

    sget-object v6, LX/Ckc;->LEFT_OF:LX/Ckc;

    if-ne v5, v6, :cond_12

    .line 1930840
    iget v4, v4, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p2

    iget v5, v0, LX/Cka;->a:F

    sub-float/2addr v4, v5

    iget v5, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_1

    .line 1930841
    :cond_12
    iget-object v5, v8, LX/CkR;->a:LX/Ckc;

    sget-object v6, LX/Ckc;->RIGHT_OF:LX/Ckc;

    if-ne v5, v6, :cond_13

    .line 1930842
    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget v5, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_1

    .line 1930843
    :cond_13
    iget-object v5, v8, LX/CkR;->a:LX/Ckc;

    sget-object v6, LX/Ckc;->CENTER_IN:LX/Ckc;

    if-ne v5, v6, :cond_4

    .line 1930844
    iget v5, v4, Landroid/graphics/RectF;->left:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget v5, v9, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_1

    .line 1930845
    :cond_14
    iget-object v6, v8, LX/CkR;->e:LX/Ckf;

    sget-object v7, LX/Ckf;->BOTTOM:LX/Ckf;

    if-ne v6, v7, :cond_15

    .line 1930846
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v6, p0, LX/CkL;->x:F

    iget-object v7, p0, LX/CkL;->h:LX/CkX;

    iget v7, v7, LX/CkX;->a:F

    sub-float/2addr v6, v7

    move-object/from16 v0, p2

    iget v7, v0, LX/Cka;->b:F

    sub-float/2addr v6, v7

    invoke-virtual {v9, v3, v6}, Landroid/graphics/PointF;->set(FF)V

    move v3, v4

    .line 1930847
    goto/16 :goto_2

    .line 1930848
    :cond_15
    iget-object v6, v8, LX/CkR;->e:LX/Ckf;

    sget-object v7, LX/Ckf;->TOP_FLUSH:LX/Ckf;

    if-ne v6, v7, :cond_16

    .line 1930849
    iget v6, v9, Landroid/graphics/PointF;->x:F

    const/4 v7, 0x0

    invoke-virtual {v9, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_2

    .line 1930850
    :cond_16
    iget-object v6, v8, LX/CkR;->e:LX/Ckf;

    sget-object v7, LX/Ckf;->BOTTOM_FLUSH:LX/Ckf;

    if-ne v6, v7, :cond_17

    .line 1930851
    iget v6, v9, Landroid/graphics/PointF;->x:F

    iget v7, p0, LX/CkL;->x:F

    move-object/from16 v0, p2

    iget v10, v0, LX/Cka;->b:F

    sub-float/2addr v7, v10

    invoke-virtual {v9, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_2

    .line 1930852
    :cond_17
    iget-object v6, v8, LX/CkR;->e:LX/Ckf;

    invoke-static {v6}, LX/Ckf;->hasElementArgument(LX/Ckf;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 1930853
    iget-object v6, v8, LX/CkR;->h:Ljava/lang/String;

    move/from16 v0, p3

    invoke-direct {p0, v6, v0}, LX/CkL;->a(Ljava/lang/String;Z)Landroid/graphics/RectF;

    move-result-object v6

    .line 1930854
    iget-object v7, v8, LX/CkR;->e:LX/Ckf;

    sget-object v10, LX/Ckf;->ABOVE:LX/Ckf;

    if-ne v7, v10, :cond_18

    .line 1930855
    iget v3, v9, Landroid/graphics/PointF;->x:F

    iget v6, v6, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p2

    iget v7, v0, LX/Cka;->b:F

    sub-float/2addr v6, v7

    invoke-virtual {v9, v3, v6}, Landroid/graphics/PointF;->set(FF)V

    move v3, v4

    .line 1930856
    goto/16 :goto_2

    .line 1930857
    :cond_18
    iget-object v7, v8, LX/CkR;->e:LX/Ckf;

    sget-object v10, LX/Ckf;->BELOW:LX/Ckf;

    if-ne v7, v10, :cond_19

    .line 1930858
    iget v5, v9, Landroid/graphics/PointF;->x:F

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v9, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    move v5, v4

    .line 1930859
    goto/16 :goto_2

    .line 1930860
    :cond_19
    iget-object v7, v8, LX/CkR;->e:LX/Ckf;

    sget-object v10, LX/Ckf;->CENTERED_IN:LX/Ckf;

    if-ne v7, v10, :cond_5

    .line 1930861
    if-eqz v4, :cond_1a

    .line 1930862
    iget v7, v9, Landroid/graphics/PointF;->x:F

    invoke-static {v6}, LX/Ckm;->b(Landroid/graphics/RectF;)F

    move-result v6

    invoke-virtual {v9, v7, v6}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_2

    .line 1930863
    :cond_1a
    iget v7, v9, Landroid/graphics/PointF;->x:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    move-object/from16 v0, p2

    iget v11, v0, LX/Cka;->b:F

    sub-float/2addr v6, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v6, v11

    add-float/2addr v6, v10

    invoke-virtual {v9, v7, v6}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_2

    .line 1930864
    :cond_1b
    iget-object v6, v8, LX/CkR;->e:LX/Ckf;

    invoke-static {v6}, LX/Ckf;->hasGridArgument(LX/Ckf;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1930865
    iget-object v6, v8, LX/CkR;->e:LX/Ckf;

    sget-object v7, LX/Ckf;->BASE_ON_GRID_LINE:LX/Ckf;

    if-ne v6, v7, :cond_1c

    .line 1930866
    iget v6, v9, Landroid/graphics/PointF;->x:F

    iget-object v7, p0, LX/CkL;->h:LX/CkX;

    iget v7, v7, LX/CkX;->a:F

    iget v10, v8, LX/CkR;->g:I

    int-to-float v10, v10

    iget-object v11, p0, LX/CkL;->h:LX/CkX;

    iget v11, v11, LX/CkX;->h:F

    mul-float/2addr v10, v11

    add-float/2addr v7, v10

    move-object/from16 v0, p2

    iget v10, v0, LX/Cka;->b:F

    sub-float/2addr v7, v10

    invoke-virtual {v9, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_2

    .line 1930867
    :cond_1c
    iget-object v6, v8, LX/CkR;->e:LX/Ckf;

    sget-object v7, LX/Ckf;->CENTERED_IN_GRID_LINE:LX/Ckf;

    if-ne v6, v7, :cond_5

    .line 1930868
    iget v6, v9, Landroid/graphics/PointF;->x:F

    iget-object v7, p0, LX/CkL;->h:LX/CkX;

    iget v7, v7, LX/CkX;->a:F

    iget v10, v8, LX/CkR;->g:I

    int-to-float v10, v10

    const/high16 v11, 0x3f000000    # 0.5f

    sub-float/2addr v10, v11

    iget-object v11, p0, LX/CkL;->h:LX/CkX;

    iget v11, v11, LX/CkX;->h:F

    mul-float/2addr v10, v11

    add-float/2addr v7, v10

    move-object/from16 v0, p2

    iget v10, v0, LX/Cka;->b:F

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-float/2addr v7, v10

    invoke-virtual {v9, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_2

    .line 1930869
    :cond_1d
    iget v6, v9, Landroid/graphics/PointF;->y:F

    .line 1930870
    iget v5, p0, LX/CkL;->x:F

    iget-object v7, p0, LX/CkL;->h:LX/CkX;

    iget v7, v7, LX/CkX;->a:F

    sub-float v7, v5, v7

    .line 1930871
    iget-object v5, v8, LX/CkR;->l:LX/CkQ;

    iget-object v5, v5, LX/CkQ;->a:LX/CkZ;

    sget-object v10, LX/CkZ;->AFTER:LX/CkZ;

    if-ne v5, v10, :cond_20

    .line 1930872
    iget-object v5, v8, LX/CkR;->l:LX/CkQ;

    invoke-virtual {v5}, LX/CkQ;->a()Ljava/lang/String;

    move-result-object v5

    .line 1930873
    move/from16 v0, p3

    invoke-direct {p0, v5, v0}, LX/CkL;->a(Ljava/lang/String;Z)Landroid/graphics/RectF;

    move-result-object v5

    .line 1930874
    iget v10, v5, Landroid/graphics/RectF;->bottom:F

    iget v11, p0, LX/CkL;->x:F

    cmpg-float v10, v10, v11

    if-gez v10, :cond_20

    .line 1930875
    iget-object v10, v8, LX/CkR;->l:LX/CkQ;

    iget-object v11, p0, LX/CkL;->h:LX/CkX;

    new-instance v12, LX/Cka;

    iget v13, p0, LX/CkL;->w:F

    iget v14, p0, LX/CkL;->x:F

    invoke-direct {v12, v13, v14}, LX/Cka;-><init>(FF)V

    invoke-static {v10, v11, v12}, LX/CkL;->b(LX/CkQ;LX/CkX;LX/Cka;)F

    move-result v10

    .line 1930876
    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v5, v10

    .line 1930877
    cmpg-float v10, v6, v5

    if-gez v10, :cond_20

    .line 1930878
    iget v6, v9, Landroid/graphics/PointF;->y:F

    sub-float v6, v5, v6

    .line 1930879
    iget v10, p0, LX/CkL;->x:F

    sub-float/2addr v10, v6

    iput v10, p0, LX/CkL;->x:F

    .line 1930880
    iget v10, v9, Landroid/graphics/PointF;->x:F

    iget v11, v9, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, v11

    invoke-virtual {v9, v10, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 1930881
    :goto_5
    iget-object v6, v8, LX/CkR;->l:LX/CkQ;

    iget-object v6, v6, LX/CkQ;->a:LX/CkZ;

    sget-object v10, LX/CkZ;->BEFORE:LX/CkZ;

    if-ne v6, v10, :cond_1f

    .line 1930882
    iget-object v6, v8, LX/CkR;->l:LX/CkQ;

    invoke-virtual {v6}, LX/CkQ;->a()Ljava/lang/String;

    move-result-object v6

    .line 1930883
    move/from16 v0, p3

    invoke-direct {p0, v6, v0}, LX/CkL;->a(Ljava/lang/String;Z)Landroid/graphics/RectF;

    move-result-object v6

    .line 1930884
    iget v10, v6, Landroid/graphics/RectF;->top:F

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_1f

    .line 1930885
    iget-object v7, v8, LX/CkR;->l:LX/CkQ;

    iget-object v8, p0, LX/CkL;->h:LX/CkX;

    new-instance v10, LX/Cka;

    iget v11, p0, LX/CkL;->w:F

    iget v12, p0, LX/CkL;->x:F

    invoke-direct {v10, v11, v12}, LX/Cka;-><init>(FF)V

    invoke-static {v7, v8, v10}, LX/CkL;->b(LX/CkQ;LX/CkX;LX/Cka;)F

    move-result v7

    .line 1930886
    iget v6, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v7

    .line 1930887
    :goto_6
    sub-float v5, v6, v5

    .line 1930888
    move-object/from16 v0, p2

    iget v6, v0, LX/Cka;->b:F

    cmpl-float v6, v6, v5

    if-lez v6, :cond_c

    .line 1930889
    const/4 v3, 0x0

    invoke-static {v5, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move-object/from16 v0, p2

    iput v3, v0, LX/Cka;->b:F

    move v3, v4

    goto/16 :goto_3

    .line 1930890
    :cond_1e
    iget-object v2, v2, LX/CkS;->b:LX/Ckm;

    goto/16 :goto_4

    :cond_1f
    move v6, v7

    goto :goto_6

    :cond_20
    move v5, v6

    goto :goto_5
.end method

.method private a(Ljava/lang/String;Z)Landroid/graphics/RectF;
    .locals 3

    .prologue
    .line 1930704
    iget-object v0, p0, LX/CkL;->r:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CkO;

    .line 1930705
    if-eqz p2, :cond_0

    const/4 p2, 0x0

    const/4 p1, 0x1

    .line 1930706
    if-nez v0, :cond_2

    .line 1930707
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1930708
    :goto_0
    move-object v1, v1

    .line 1930709
    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, v0, LX/CkO;->d:LX/Ckb;

    invoke-static {v0}, LX/Ckb;->isTextElement(LX/Ckb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1930710
    move-object v0, v1

    .line 1930711
    :goto_2
    return-object v0

    .line 1930712
    :cond_0
    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 1930713
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->BYLINE:LX/Ckb;

    if-ne v1, v2, :cond_c

    .line 1930714
    iget-object v1, p0, LX/CkL;->u:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->A:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    .line 1930715
    :goto_3
    move-object v1, v1

    .line 1930716
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 1930717
    :cond_2
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->BYLINE:LX/Ckb;

    if-ne v1, v2, :cond_3

    .line 1930718
    iget-object v1, p0, LX/CkL;->u:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->B:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto :goto_0

    .line 1930719
    :cond_3
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->BYLINE_AREA:LX/Ckb;

    if-ne v1, v2, :cond_4

    .line 1930720
    iget-object v1, p0, LX/CkL;->p:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->D:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto :goto_0

    .line 1930721
    :cond_4
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->DESCRIPTION:LX/Ckb;

    if-ne v1, v2, :cond_5

    .line 1930722
    iget-object v1, p0, LX/CkL;->k:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->z:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto :goto_0

    .line 1930723
    :cond_5
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->HEADLINE:LX/Ckb;

    if-ne v1, v2, :cond_6

    .line 1930724
    iget-object v1, p0, LX/CkL;->j:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->F:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto :goto_0

    .line 1930725
    :cond_6
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->COVER_IMAGE:LX/Ckb;

    if-ne v1, v2, :cond_7

    .line 1930726
    invoke-virtual {p0}, LX/CkL;->b()Landroid/graphics/RectF;

    move-result-object v1

    goto :goto_0

    .line 1930727
    :cond_7
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->OVERLAY:LX/Ckb;

    if-ne v1, v2, :cond_8

    .line 1930728
    iget-object v1, p0, LX/CkL;->o:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->H:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_0

    .line 1930729
    :cond_8
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->SOURCE_IMAGE:LX/Ckb;

    if-ne v1, v2, :cond_9

    .line 1930730
    invoke-virtual {p0}, LX/CkL;->m()Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_0

    .line 1930731
    :cond_9
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->COVER_VIDEO:LX/Ckb;

    if-ne v1, v2, :cond_a

    .line 1930732
    iget-object v1, p0, LX/CkL;->n:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->G:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_0

    .line 1930733
    :cond_a
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->BAR:LX/Ckb;

    if-ne v1, v2, :cond_b

    .line 1930734
    iget-object v1, v0, LX/CkO;->c:Ljava/lang/String;

    new-instance v2, LX/Cka;

    invoke-direct {v2, p2, p2}, LX/Cka;-><init>(FF)V

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_0

    .line 1930735
    :cond_b
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    goto/16 :goto_0

    .line 1930736
    :cond_c
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->BYLINE_AREA:LX/Ckb;

    if-ne v1, v2, :cond_d

    .line 1930737
    iget-object v1, p0, LX/CkL;->p:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->C:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_3

    .line 1930738
    :cond_d
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->DESCRIPTION:LX/Ckb;

    if-ne v1, v2, :cond_10

    .line 1930739
    iget-object v1, p0, LX/CkL;->k:LX/CkS;

    iget-object v1, v1, LX/CkS;->b:LX/Ckm;

    iget v1, v1, LX/Ckm;->b:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_e

    .line 1930740
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    goto/16 :goto_3

    .line 1930741
    :cond_e
    iget-boolean v1, p0, LX/CkL;->s:Z

    if-nez v1, :cond_f

    .line 1930742
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    goto/16 :goto_3

    .line 1930743
    :cond_f
    iget-object v1, p0, LX/CkL;->k:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->y:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_3

    .line 1930744
    :cond_10
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->HEADLINE:LX/Ckb;

    if-ne v1, v2, :cond_11

    .line 1930745
    iget-object v1, p0, LX/CkL;->j:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->E:LX/Cka;

    invoke-static {p0, v1, v2, p1}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_3

    .line 1930746
    :cond_11
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->COVER_IMAGE:LX/Ckb;

    if-ne v1, v2, :cond_12

    .line 1930747
    invoke-virtual {p0}, LX/CkL;->b()Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_3

    .line 1930748
    :cond_12
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->OVERLAY:LX/Ckb;

    if-ne v1, v2, :cond_13

    .line 1930749
    iget-object v1, p0, LX/CkL;->o:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->H:LX/Cka;

    invoke-static {p0, v1, v2, p2}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_3

    .line 1930750
    :cond_13
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->SOURCE_IMAGE:LX/Ckb;

    if-ne v1, v2, :cond_14

    .line 1930751
    invoke-virtual {p0}, LX/CkL;->m()Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_3

    .line 1930752
    :cond_14
    iget-object v1, v0, LX/CkO;->d:LX/Ckb;

    sget-object v2, LX/Ckb;->COVER_VIDEO:LX/Ckb;

    if-ne v1, v2, :cond_15

    .line 1930753
    iget-object v1, p0, LX/CkL;->n:LX/CkO;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CkL;->G:LX/Cka;

    invoke-static {p0, v1, v2, p2}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    goto/16 :goto_3

    .line 1930754
    :cond_15
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    goto/16 :goto_3
.end method

.method public static a(LX/CkL;LX/Ckb;FF)V
    .locals 1

    .prologue
    .line 1930755
    sget-object v0, LX/Ckb;->DESCRIPTION:LX/Ckb;

    if-ne p1, v0, :cond_1

    .line 1930756
    new-instance v0, LX/Cka;

    invoke-direct {v0, p2, p3}, LX/Cka;-><init>(FF)V

    iput-object v0, p0, LX/CkL;->z:LX/Cka;

    .line 1930757
    :cond_0
    :goto_0
    return-void

    .line 1930758
    :cond_1
    sget-object v0, LX/Ckb;->BYLINE:LX/Ckb;

    if-ne p1, v0, :cond_2

    .line 1930759
    new-instance v0, LX/Cka;

    invoke-direct {v0, p2, p3}, LX/Cka;-><init>(FF)V

    iput-object v0, p0, LX/CkL;->B:LX/Cka;

    goto :goto_0

    .line 1930760
    :cond_2
    sget-object v0, LX/Ckb;->BYLINE_AREA:LX/Ckb;

    if-ne p1, v0, :cond_3

    .line 1930761
    new-instance v0, LX/Cka;

    invoke-direct {v0, p2, p3}, LX/Cka;-><init>(FF)V

    iput-object v0, p0, LX/CkL;->D:LX/Cka;

    goto :goto_0

    .line 1930762
    :cond_3
    sget-object v0, LX/Ckb;->HEADLINE:LX/Ckb;

    if-ne p1, v0, :cond_4

    .line 1930763
    new-instance v0, LX/Cka;

    invoke-direct {v0, p2, p3}, LX/Cka;-><init>(FF)V

    iput-object v0, p0, LX/CkL;->F:LX/Cka;

    goto :goto_0

    .line 1930764
    :cond_4
    sget-object v0, LX/Ckb;->COVER_IMAGE:LX/Ckb;

    if-ne p1, v0, :cond_5

    .line 1930765
    new-instance v0, LX/Cka;

    invoke-direct {v0, p2, p3}, LX/Cka;-><init>(FF)V

    iput-object v0, p0, LX/CkL;->G:LX/Cka;

    goto :goto_0

    .line 1930766
    :cond_5
    sget-object v0, LX/Ckb;->OVERLAY:LX/Ckb;

    if-ne p1, v0, :cond_6

    .line 1930767
    new-instance v0, LX/Cka;

    invoke-direct {v0, p2, p3}, LX/Cka;-><init>(FF)V

    iput-object v0, p0, LX/CkL;->H:LX/Cka;

    goto :goto_0

    .line 1930768
    :cond_6
    sget-object v0, LX/Ckb;->SOURCE_IMAGE:LX/Ckb;

    if-ne p1, v0, :cond_0

    .line 1930769
    new-instance v0, LX/Cka;

    invoke-direct {v0, p2, p3}, LX/Cka;-><init>(FF)V

    iput-object v0, p0, LX/CkL;->I:LX/Cka;

    goto :goto_0
.end method

.method private static b(LX/CkQ;LX/CkX;LX/Cka;)F
    .locals 2

    .prologue
    .line 1930700
    iget-object v0, p0, LX/CkQ;->b:LX/CkN;

    iget-object v0, v0, LX/CkN;->b:Ljava/lang/String;

    invoke-static {v0, p1, p2}, LX/CkY;->b(Ljava/lang/String;LX/CkX;LX/Cka;)F

    move-result v0

    .line 1930701
    iget-object v1, p0, LX/CkQ;->b:LX/CkN;

    iget-object v1, v1, LX/CkN;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1930702
    iget-object v1, p0, LX/CkQ;->b:LX/CkN;

    iget-object v1, v1, LX/CkN;->c:Ljava/lang/String;

    invoke-static {v1, p1, p2}, LX/CkY;->b(Ljava/lang/String;LX/CkX;LX/Cka;)F

    move-result v1

    add-float/2addr v0, v1

    .line 1930703
    :cond_0
    return v0
.end method


# virtual methods
.method public final b()Landroid/graphics/RectF;
    .locals 3

    .prologue
    .line 1930699
    iget-object v0, p0, LX/CkL;->m:LX/CkO;

    iget-object v0, v0, LX/CkO;->c:Ljava/lang/String;

    iget-object v1, p0, LX/CkL;->G:LX/Cka;

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public final m()Landroid/graphics/RectF;
    .locals 4

    .prologue
    const/high16 v1, 0x42a00000    # 80.0f

    const/high16 v2, 0x42200000    # 40.0f

    .line 1930693
    iget-boolean v0, p0, LX/CkL;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CkL;->I:LX/Cka;

    iget v0, v0, LX/Cka;->b:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget-object v0, p0, LX/CkL;->I:LX/Cka;

    iget v0, v0, LX/Cka;->a:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    .line 1930694
    :cond_0
    iget-object v0, p0, LX/CkL;->I:LX/Cka;

    iget v0, v0, LX/Cka;->b:F

    div-float v0, v1, v0

    .line 1930695
    iget-object v1, p0, LX/CkL;->I:LX/Cka;

    iget v1, v1, LX/Cka;->a:F

    mul-float/2addr v1, v0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 1930696
    iget-object v0, p0, LX/CkL;->I:LX/Cka;

    iget v0, v0, LX/Cka;->a:F

    div-float v0, v2, v0

    .line 1930697
    :cond_1
    new-instance v1, LX/Cka;

    iget-object v2, p0, LX/CkL;->I:LX/Cka;

    iget v2, v2, LX/Cka;->a:F

    mul-float/2addr v2, v0

    iget-object v3, p0, LX/CkL;->I:LX/Cka;

    iget v3, v3, LX/Cka;->b:F

    mul-float/2addr v0, v3

    invoke-direct {v1, v2, v0}, LX/Cka;-><init>(FF)V

    iput-object v1, p0, LX/CkL;->I:LX/Cka;

    .line 1930698
    :cond_2
    iget-object v0, p0, LX/CkL;->l:LX/CkO;

    iget-object v0, v0, LX/CkO;->c:Ljava/lang/String;

    iget-object v1, p0, LX/CkL;->I:LX/Cka;

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method
