.class public LX/DFY;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFZ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DFY",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DFZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978460
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1978461
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DFY;->b:LX/0Zi;

    .line 1978462
    iput-object p1, p0, LX/DFY;->a:LX/0Ot;

    .line 1978463
    return-void
.end method

.method public static a(LX/0QB;)LX/DFY;
    .locals 4

    .prologue
    .line 1978449
    const-class v1, LX/DFY;

    monitor-enter v1

    .line 1978450
    :try_start_0
    sget-object v0, LX/DFY;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978451
    sput-object v2, LX/DFY;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978452
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978453
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978454
    new-instance v3, LX/DFY;

    const/16 p0, 0x20e7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DFY;-><init>(LX/0Ot;)V

    .line 1978455
    move-object v0, v3

    .line 1978456
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978457
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978458
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 7

    .prologue
    .line 1978423
    check-cast p1, LX/DFX;

    .line 1978424
    iget-object v0, p0, LX/DFY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DFZ;

    iget-object v1, p1, LX/DFX;->c:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    iget-object v2, p1, LX/DFX;->d:LX/1Fa;

    iget-object v3, p1, LX/DFX;->a:LX/2ep;

    iget-object v4, p1, LX/DFX;->e:LX/3mj;

    iget-object v5, p1, LX/DFX;->f:LX/1Pc;

    iget-boolean v6, p1, LX/DFX;->b:Z

    invoke-virtual/range {v0 .. v6}, LX/DFZ;->onClick(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;LX/3mj;LX/1Pc;Z)V

    .line 1978425
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1978448
    const v0, 0x3e406edd

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1978431
    check-cast p2, LX/DFX;

    .line 1978432
    iget-object v0, p0, LX/DFY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DFZ;

    iget-object v1, p2, LX/DFX;->a:LX/2ep;

    iget-boolean v2, p2, LX/DFX;->b:Z

    const/4 v5, 0x0

    const/4 v10, 0x2

    .line 1978433
    iget-object v3, v1, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v3}, LX/2et;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1978434
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    .line 1978435
    :goto_0
    move-object v0, v3

    .line 1978436
    return-object v0

    .line 1978437
    :cond_0
    if-eqz v2, :cond_1

    new-instance v3, LX/2f5;

    iget-object v4, v0, LX/DFZ;->a:LX/23P;

    iget-object v6, v0, LX/DFZ;->e:Landroid/content/res/Resources;

    const v7, 0x7f08186b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v5}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v4

    const v6, 0x7f02081c

    iget-object v7, v0, LX/DFZ;->e:Landroid/content/res/Resources;

    const v8, 0x7f0a00a3

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    const v9, 0x7f020b0b

    invoke-direct/range {v3 .. v9}, LX/2f5;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/Integer;II)V

    .line 1978438
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    .line 1978439
    const v5, 0x3e406edd

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1978440
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    iget v5, v3, LX/2f5;->f:I

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0f6c

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    .line 1978441
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    iget-object v6, v0, LX/DFZ;->c:LX/1vg;

    invoke-virtual {v6, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v6

    iget v7, v3, LX/2f5;->c:I

    invoke-virtual {v6, v7}, LX/2xv;->h(I)LX/2xv;

    move-result-object v6

    invoke-static {v0, v3}, LX/DFZ;->a(LX/DFZ;LX/2f5;)I

    move-result v7

    invoke-virtual {v6, v7}, LX/2xv;->i(I)LX/2xv;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/16 v6, 0x8

    const v7, 0x7f0b0f65

    invoke-interface {v5, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 1978442
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 v7, 0x1

    .line 1978443
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    iget-object v6, v3, LX/2f5;->a:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004d

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    .line 1978444
    invoke-static {v0, v3}, LX/DFZ;->a(LX/DFZ;LX/2f5;)I

    move-result v6

    invoke-virtual {v5, v6}, LX/1ne;->m(I)LX/1ne;

    .line 1978445
    move-object v3, v5

    .line 1978446
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 1978447
    :cond_1
    iget-object v3, v0, LX/DFZ;->d:LX/2et;

    iget-object v4, v1, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v3, v4}, LX/2et;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/2f5;

    move-result-object v3

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1978426
    invoke-static {}, LX/1dS;->b()V

    .line 1978427
    iget v0, p1, LX/1dQ;->b:I

    .line 1978428
    packed-switch v0, :pswitch_data_0

    .line 1978429
    :goto_0
    return-object v1

    .line 1978430
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/DFY;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3e406edd
        :pswitch_0
    .end packed-switch
.end method
