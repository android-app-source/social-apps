.class public LX/ERL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/ERL;


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2120770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120771
    iput-object p1, p0, LX/ERL;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2120772
    return-void
.end method

.method public static a(LX/0QB;)LX/ERL;
    .locals 4

    .prologue
    .line 2120773
    sget-object v0, LX/ERL;->b:LX/ERL;

    if-nez v0, :cond_1

    .line 2120774
    const-class v1, LX/ERL;

    monitor-enter v1

    .line 2120775
    :try_start_0
    sget-object v0, LX/ERL;->b:LX/ERL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2120776
    if-eqz v2, :cond_0

    .line 2120777
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2120778
    new-instance p0, LX/ERL;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/ERL;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2120779
    move-object v0, p0

    .line 2120780
    sput-object v0, LX/ERL;->b:LX/ERL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2120781
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2120782
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2120783
    :cond_1
    sget-object v0, LX/ERL;->b:LX/ERL;

    return-object v0

    .line 2120784
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2120785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 2120786
    iget-object v0, p0, LX/ERL;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2TR;->b:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2120787
    iget-object v0, p0, LX/ERL;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 2120788
    sget-object v1, LX/2TR;->b:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 2120789
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2120790
    return-void
.end method
