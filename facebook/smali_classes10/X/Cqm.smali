.class public LX/Cqm;
.super LX/Cqk;
.source ""


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 7

    .prologue
    .line 1940430
    invoke-direct {p0, p1, p2}, LX/Cqk;-><init>(LX/Ctg;LX/CrK;)V

    .line 1940431
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->a:LX/Cqw;

    .line 1940432
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940433
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_OVERLAY:LX/Cqc;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940434
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->b:LX/Cqw;

    .line 1940435
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940436
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_FULLSCREEN:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_OVERLAY:LX/Cqc;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940437
    return-void
.end method


# virtual methods
.method public final d()LX/Cqv;
    .locals 1

    .prologue
    .line 1940438
    sget-object v0, LX/Cqw;->b:LX/Cqw;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1940439
    invoke-super {p0}, LX/Cqk;->h()V

    .line 1940440
    iget-object v0, p0, LX/Cqi;->a:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1940441
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    .line 1940442
    iget-object v1, p0, LX/Cqi;->a:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1940443
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 1940444
    iget-object v0, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1940445
    check-cast v0, LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getMediaAspectRatio()F

    move-result v0

    .line 1940446
    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v1, LX/CoL;->q:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1940447
    if-nez v0, :cond_0

    .line 1940448
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    invoke-virtual {p0, v0}, LX/CqX;->e(LX/Cqv;)V

    .line 1940449
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 1940450
    invoke-virtual {p0}, LX/Cqk;->j()LX/Cqj;

    move-result-object v0

    .line 1940451
    sget-object v1, LX/Cqw;->b:LX/Cqw;

    invoke-virtual {v0, v1}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v0

    .line 1940452
    invoke-virtual {p0}, LX/Cqk;->i()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v1

    invoke-static {v0, v1}, LX/Cqj;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1940453
    invoke-virtual {v0}, LX/CrW;->e()I

    move-result v1

    invoke-virtual {v0}, LX/CrW;->f()I

    move-result v2

    invoke-virtual {p0, v1, v2}, LX/Cqi;->a(II)V

    .line 1940454
    invoke-virtual {v0}, LX/CrW;->e()I

    move-result v1

    invoke-virtual {v0}, LX/CrW;->f()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/Cqi;->b(II)V

    .line 1940455
    return-void
.end method
