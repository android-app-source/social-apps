.class public LX/DRf;
.super LX/1Cv;
.source ""


# instance fields
.field private a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1998522
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1998523
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1998465
    sget-object v0, LX/DRe;->UNIT:LX/DRe;

    invoke-virtual {v0}, LX/DRe;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1998466
    new-instance v0, LX/DRX;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DRX;-><init>(Landroid/content/Context;)V

    .line 1998467
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/DRV;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DRV;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 5

    .prologue
    .line 1998468
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/DRf;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 1998469
    check-cast p3, LX/DRX;

    .line 1998470
    iget-object v0, p0, LX/DRf;->b:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1998471
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v1

    .line 1998472
    iget-object v2, p3, LX/DRX;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1998473
    move-object v1, p3

    .line 1998474
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 1998475
    iget-object v3, v1, LX/DRX;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1998476
    move-object v1, v1

    .line 1998477
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->n()I

    move-result v2

    .line 1998478
    iget-object v3, v1, LX/DRX;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1998479
    iget-object v4, v1, LX/DRX;->e:Landroid/widget/ToggleButton;

    const/16 v3, 0x64

    if-ne v2, v3, :cond_3

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v4, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 1998480
    move-object v1, v1

    .line 1998481
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->n()I

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/DRX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)LX/DRX;

    .line 1998482
    :goto_1
    return-void

    .line 1998483
    :cond_0
    check-cast p3, LX/DRV;

    .line 1998484
    iget v0, p0, LX/DRf;->c:I

    .line 1998485
    iput v0, p3, LX/DRV;->a:I

    .line 1998486
    iget v1, p3, LX/DRV;->a:I

    if-ltz v1, :cond_1

    iget v1, p3, LX/DRV;->b:I

    if-lez v1, :cond_1

    .line 1998487
    invoke-static {p3}, LX/DRV;->b(LX/DRV;)V

    .line 1998488
    :cond_1
    move-object v0, p3

    .line 1998489
    iget-object v1, p0, LX/DRf;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1998490
    iput v1, v0, LX/DRV;->b:I

    .line 1998491
    iget v2, v0, LX/DRV;->a:I

    if-ltz v2, :cond_2

    iget v2, v0, LX/DRV;->b:I

    if-lez v2, :cond_2

    .line 1998492
    invoke-static {v0}, LX/DRV;->b(LX/DRV;)V

    .line 1998493
    :cond_2
    goto :goto_1

    .line 1998494
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1998495
    if-eqz p1, :cond_0

    .line 1998496
    iput-object p1, p0, LX/DRf;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1998497
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1998498
    iget-object v0, p0, LX/DRf;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_3

    .line 1998499
    iget-object v0, p0, LX/DRf;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1998500
    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_5

    .line 1998501
    iget-object v0, p0, LX/DRf;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1998502
    invoke-virtual {v4, v0, v2}, LX/15i;->j(II)I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1998503
    iget-object v0, p0, LX/DRf;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel;

    invoke-virtual {v4, v0, v1, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1998504
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/DRf;->b:LX/0Px;

    .line 1998505
    iget-object v0, p0, LX/DRf;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_3
    if-ge v2, v3, :cond_7

    iget-object v0, p0, LX/DRf;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel;

    .line 1998506
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$LearningCourseInformationModel$LearningCourseUnitModel$EdgesModel$NodeModel;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1998507
    add-int/lit8 v0, v1, 0x1

    .line 1998508
    :goto_4
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto :goto_3

    :cond_2
    move v0, v2

    .line 1998509
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    .line 1998510
    :cond_6
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1998511
    goto :goto_2

    .line 1998512
    :cond_7
    iput v1, p0, LX/DRf;->c:I

    .line 1998513
    const v0, 0x25e7a81f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1998514
    return-void

    :cond_8
    move v0, v1

    goto :goto_4
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1998515
    iget-object v0, p0, LX/DRf;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1998516
    if-lez p1, :cond_0

    .line 1998517
    iget-object v0, p0, LX/DRf;->b:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1998518
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1998519
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1998520
    if-nez p1, :cond_0

    sget-object v0, LX/DRe;->PROGRESS:LX/DRe;

    invoke-virtual {v0}, LX/DRe;->ordinal()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/DRe;->UNIT:LX/DRe;

    invoke-virtual {v0}, LX/DRe;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1998521
    sget-object v0, LX/DRe;->COUNT:LX/DRe;

    invoke-virtual {v0}, LX/DRe;->ordinal()I

    move-result v0

    return v0
.end method
