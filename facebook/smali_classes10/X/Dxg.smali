.class public LX/Dxg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0se;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0se;LX/0Ot;LX/0Or;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/photoset/gatekeeper/IsInFamilyTaggingAndroid;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0se;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2063134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2063135
    iput-object p1, p0, LX/Dxg;->e:LX/0Ot;

    .line 2063136
    iput-object p2, p0, LX/Dxg;->a:LX/0se;

    .line 2063137
    iput-object p3, p0, LX/Dxg;->d:LX/0Ot;

    .line 2063138
    iput-object p4, p0, LX/Dxg;->b:LX/0Or;

    .line 2063139
    iput-object p5, p0, LX/Dxg;->c:LX/0Or;

    .line 2063140
    return-void
.end method

.method public static b(LX/0QB;)LX/Dxg;
    .locals 6

    .prologue
    .line 2063141
    new-instance v0, LX/Dxg;

    const/16 v1, 0xafd

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v2

    check-cast v2, LX/0se;

    const/16 v3, 0x259

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x154d

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/Dxg;-><init>(LX/0Ot;LX/0se;LX/0Ot;LX/0Or;LX/0Or;)V

    .line 2063142
    return-object v0
.end method
