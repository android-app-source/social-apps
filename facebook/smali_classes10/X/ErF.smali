.class public LX/ErF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/EqQ;

.field public final c:LX/03V;

.field public final d:LX/0tX;

.field private final e:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public final g:Ljava/lang/String;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2172718
    const-class v0, LX/ErF;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ErF;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0tX;LX/1Ck;Ljava/lang/String;Ljava/util/List;LX/EqQ;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/EqQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tX;",
            "LX/1Ck;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/events/invite/EventsExtendedInvitePotentialGuestsFetcher$PotentialGuestsFetcherListener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2172720
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/ErF;->f:Z

    .line 2172721
    iput-object p1, p0, LX/ErF;->c:LX/03V;

    .line 2172722
    iput-object p2, p0, LX/ErF;->d:LX/0tX;

    .line 2172723
    iput-object p3, p0, LX/ErF;->e:LX/1Ck;

    .line 2172724
    iput-object p4, p0, LX/ErF;->g:Ljava/lang/String;

    .line 2172725
    iput-object p5, p0, LX/ErF;->h:Ljava/util/List;

    .line 2172726
    iput-object p6, p0, LX/ErF;->b:LX/EqQ;

    .line 2172727
    return-void
.end method

.method public static b(LX/2uF;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2uF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2172728
    if-nez p0, :cond_0

    .line 2172729
    const/4 v0, 0x0

    .line 2172730
    :goto_0
    return-object v0

    .line 2172731
    :cond_0
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2172732
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2172733
    new-instance v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    new-instance v1, Lcom/facebook/user/model/Name;

    const/4 v2, 0x2

    invoke-virtual {v3, v5, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x3

    invoke-virtual {v3, v5, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;ZZ)V

    .line 2172734
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2172735
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2172736
    iget-boolean v0, p0, LX/ErF;->f:Z

    if-nez v0, :cond_0

    .line 2172737
    :goto_0
    return-void

    .line 2172738
    :cond_0
    iget-object v0, p0, LX/ErF;->e:LX/1Ck;

    const-string v1, "FETCH_POTENTIAL_GUESTS_TASK"

    new-instance v2, LX/ErD;

    invoke-direct {v2, p0}, LX/ErD;-><init>(LX/ErF;)V

    .line 2172739
    new-instance v3, LX/ErE;

    invoke-direct {v3, p0}, LX/ErE;-><init>(LX/ErF;)V

    move-object v3, v3

    .line 2172740
    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
