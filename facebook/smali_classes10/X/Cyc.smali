.class public final enum LX/Cyc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cyc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cyc;

.field public static final enum IN_PROGRESS:LX/Cyc;

.field public static final enum LOADED_LAST:LX/Cyc;

.field public static final enum LOADED_WITH_NEXT:LX/Cyc;

.field public static final enum NOT_SENT:LX/Cyc;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1953353
    new-instance v0, LX/Cyc;

    const-string v1, "NOT_SENT"

    invoke-direct {v0, v1, v2}, LX/Cyc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cyc;->NOT_SENT:LX/Cyc;

    .line 1953354
    new-instance v0, LX/Cyc;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/Cyc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cyc;->IN_PROGRESS:LX/Cyc;

    .line 1953355
    new-instance v0, LX/Cyc;

    const-string v1, "LOADED_WITH_NEXT"

    invoke-direct {v0, v1, v4}, LX/Cyc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cyc;->LOADED_WITH_NEXT:LX/Cyc;

    .line 1953356
    new-instance v0, LX/Cyc;

    const-string v1, "LOADED_LAST"

    invoke-direct {v0, v1, v5}, LX/Cyc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cyc;->LOADED_LAST:LX/Cyc;

    .line 1953357
    const/4 v0, 0x4

    new-array v0, v0, [LX/Cyc;

    sget-object v1, LX/Cyc;->NOT_SENT:LX/Cyc;

    aput-object v1, v0, v2

    sget-object v1, LX/Cyc;->IN_PROGRESS:LX/Cyc;

    aput-object v1, v0, v3

    sget-object v1, LX/Cyc;->LOADED_WITH_NEXT:LX/Cyc;

    aput-object v1, v0, v4

    sget-object v1, LX/Cyc;->LOADED_LAST:LX/Cyc;

    aput-object v1, v0, v5

    sput-object v0, LX/Cyc;->$VALUES:[LX/Cyc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1953358
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cyc;
    .locals 1

    .prologue
    .line 1953359
    const-class v0, LX/Cyc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cyc;

    return-object v0
.end method

.method public static values()[LX/Cyc;
    .locals 1

    .prologue
    .line 1953360
    sget-object v0, LX/Cyc;->$VALUES:[LX/Cyc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cyc;

    return-object v0
.end method
