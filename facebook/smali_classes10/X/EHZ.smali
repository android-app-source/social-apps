.class public LX/EHZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/BAD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/BA4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Zr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:Landroid/os/Handler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:Landroid/view/WindowManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/6KO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private K:LX/2S7;

.field public L:I

.field private final M:LX/6KB;

.field public a:LX/BA6;

.field public b:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/view/TextureView;

.field public e:LX/6Jt;

.field private final f:LX/BAC;

.field public g:LX/6JF;

.field public final h:LX/6Ik;

.field public final i:LX/6Ik;

.field public j:LX/6JB;

.field public k:LX/6JC;

.field public l:LX/6Ia;

.field public m:LX/6JR;

.field public n:LX/6Kd;

.field private o:Landroid/content/Context;

.field private final p:LX/6KN;

.field public q:LX/EDx;

.field public r:I

.field public s:I

.field private final t:Ljava/lang/String;

.field public u:J

.field private v:J

.field private w:J

.field public x:I

.field public y:I

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2099805
    const-class v0, LX/EHZ;

    sput-object v0, LX/EHZ;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2S7;II)V
    .locals 9

    .prologue
    .line 2099771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2099772
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2099773
    iput-object v0, p0, LX/EHZ;->J:LX/0Ot;

    .line 2099774
    const/4 v0, 0x0

    invoke-static {v0}, LX/6KD;->a(LX/6KT;)LX/6KD;

    move-result-object v0

    invoke-virtual {v0}, LX/6KD;->a()LX/6KB;

    move-result-object v0

    iput-object v0, p0, LX/EHZ;->M:LX/6KB;

    .line 2099775
    iput-object p1, p0, LX/EHZ;->o:Landroid/content/Context;

    .line 2099776
    const-class v0, LX/EHZ;

    iget-object v1, p0, LX/EHZ;->o:Landroid/content/Context;

    invoke-static {v0, p0, v1}, LX/EHZ;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 2099777
    iput p3, p0, LX/EHZ;->x:I

    .line 2099778
    iput p4, p0, LX/EHZ;->y:I

    .line 2099779
    sget-object v0, LX/6JF;->FRONT:LX/6JF;

    iput-object v0, p0, LX/EHZ;->g:LX/6JF;

    .line 2099780
    new-instance v0, LX/EHW;

    invoke-direct {v0, p0}, LX/EHW;-><init>(LX/EHZ;)V

    move-object v0, v0

    .line 2099781
    iput-object v0, p0, LX/EHZ;->h:LX/6Ik;

    .line 2099782
    new-instance v0, LX/EHX;

    invoke-direct {v0, p0}, LX/EHX;-><init>(LX/EHZ;)V

    move-object v0, v0

    .line 2099783
    iput-object v0, p0, LX/EHZ;->i:LX/6Ik;

    .line 2099784
    iget-object v0, p0, LX/EHZ;->B:LX/BAD;

    iget-object v1, p0, LX/EHZ;->C:LX/BA4;

    invoke-virtual {v1}, LX/BA4;->a()LX/7ex;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BAD;->a(LX/7ex;)LX/BAC;

    move-result-object v0

    iput-object v0, p0, LX/EHZ;->f:LX/BAC;

    .line 2099785
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EHZ;->t:Ljava/lang/String;

    .line 2099786
    iput-object p2, p0, LX/EHZ;->K:LX/2S7;

    .line 2099787
    iget-object v0, p0, LX/EHZ;->K:LX/2S7;

    const-string v1, "camcore_sess_id"

    iget-object v2, p0, LX/EHZ;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2099788
    iget-object v0, p0, LX/EHZ;->I:LX/6KO;

    const-string v1, "RTC_Core_Camera_Capture"

    iget-object v2, p0, LX/EHZ;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/6KO;->a(Ljava/lang/String;Ljava/lang/String;)LX/6KN;

    move-result-object v0

    iput-object v0, p0, LX/EHZ;->p:LX/6KN;

    .line 2099789
    iget-object v0, p0, LX/EHZ;->G:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, LX/EHZ;->s:I

    .line 2099790
    new-instance v0, LX/6Jt;

    iget-object v1, p0, LX/EHZ;->D:LX/0Zr;

    iget-object v2, p0, LX/EHZ;->E:Landroid/content/res/Resources;

    iget-object v3, p0, LX/EHZ;->F:Landroid/os/Handler;

    iget-object v4, p0, LX/EHZ;->A:Ljava/util/concurrent/ExecutorService;

    iget-object v5, p0, LX/EHZ;->p:LX/6KN;

    iget-object v6, p0, LX/EHZ;->M:LX/6KB;

    iget v7, p0, LX/EHZ;->s:I

    .line 2099791
    new-instance v8, LX/EHY;

    invoke-direct {v8, p0}, LX/EHY;-><init>(LX/EHZ;)V

    move-object v8, v8

    .line 2099792
    invoke-direct/range {v0 .. v8}, LX/6Jt;-><init>(LX/0Zr;Landroid/content/res/Resources;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;LX/6KN;LX/6KB;ILX/6Jp;)V

    iput-object v0, p0, LX/EHZ;->e:LX/6Jt;

    .line 2099793
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2099794
    new-instance v1, LX/6KY;

    iget-object v2, p0, LX/EHZ;->f:LX/BAC;

    invoke-direct {v1, v2}, LX/6KY;-><init>(LX/61B;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2099795
    iget-object v1, p0, LX/EHZ;->e:LX/6Jt;

    invoke-virtual {v1, v0}, LX/6Jt;->a(Ljava/util/List;)V

    .line 2099796
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EHZ;->u:J

    .line 2099797
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EHZ;->w:J

    .line 2099798
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EHZ;->v:J

    .line 2099799
    invoke-static {}, LX/6J7;->a()LX/6J9;

    move-result-object v0

    sget-object v1, LX/6J9;->CAMERA1:LX/6J9;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/EHZ;->M:LX/6KB;

    .line 2099800
    iget-object v1, v0, LX/6KB;->a:LX/6KF;

    move-object v0, v1

    .line 2099801
    iget-boolean v1, v0, LX/6KF;->a:Z

    move v0, v1

    .line 2099802
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/EHZ;->z:Z

    .line 2099803
    return-void

    .line 2099804
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;II)LX/6JR;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;II)",
            "LX/6JR;"
        }
    .end annotation

    .prologue
    .line 2099683
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2099684
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JR;

    .line 2099685
    iget v2, v0, LX/6JR;->a:I

    if-ne v2, p1, :cond_0

    iget v2, v0, LX/6JR;->b:I

    if-ne v2, p2, :cond_0

    .line 2099686
    :goto_1
    return-object v0

    .line 2099687
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2099688
    :cond_1
    invoke-static {p0, p1, p2}, LX/6JL;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v0, p1

    check-cast v0, LX/EHZ;

    invoke-static {v11}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    const-class v2, LX/BAD;

    invoke-interface {v11, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/BAD;

    invoke-static {v11}, LX/BA4;->b(LX/0QB;)LX/BA4;

    move-result-object v3

    check-cast v3, LX/BA4;

    invoke-static {v11}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v4

    check-cast v4, LX/0Zr;

    invoke-static {v11}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v11}, LX/43r;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {v11}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    invoke-static {v11}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    const-class v9, LX/6KO;

    invoke-interface {v11, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/6KO;

    invoke-static {v11}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v10

    check-cast v10, LX/0So;

    const/16 p0, 0x3257

    invoke-static {v11, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    iput-object v1, v0, LX/EHZ;->A:Ljava/util/concurrent/ExecutorService;

    iput-object v2, v0, LX/EHZ;->B:LX/BAD;

    iput-object v3, v0, LX/EHZ;->C:LX/BA4;

    iput-object v4, v0, LX/EHZ;->D:LX/0Zr;

    iput-object v5, v0, LX/EHZ;->E:Landroid/content/res/Resources;

    iput-object v6, v0, LX/EHZ;->F:Landroid/os/Handler;

    iput-object v7, v0, LX/EHZ;->G:Landroid/view/WindowManager;

    iput-object v8, v0, LX/EHZ;->H:LX/03V;

    iput-object v9, v0, LX/EHZ;->I:LX/6KO;

    iput-object v10, v0, LX/EHZ;->b:LX/0So;

    iput-object v11, v0, LX/EHZ;->J:LX/0Ot;

    return-void
.end method

.method public static a$redex0(LX/EHZ;LX/6Ik;)V
    .locals 1

    .prologue
    .line 2099768
    iget-object v0, p0, LX/EHZ;->l:LX/6Ia;

    if-eqz v0, :cond_0

    .line 2099769
    iget-object v0, p0, LX/EHZ;->l:LX/6Ia;

    invoke-interface {v0, p1}, LX/6Ia;->b(LX/6Ik;)V

    .line 2099770
    :cond_0
    return-void
.end method

.method private static b(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2099767
    if-eq p0, v0, :cond_0

    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(LX/EHZ;)I
    .locals 1

    .prologue
    .line 2099766
    iget v0, p0, LX/EHZ;->s:I

    invoke-static {v0}, LX/EHZ;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/EHZ;->x:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/EHZ;->y:I

    goto :goto_0
.end method

.method public static m(LX/EHZ;)I
    .locals 1

    .prologue
    .line 2099765
    iget v0, p0, LX/EHZ;->s:I

    invoke-static {v0}, LX/EHZ;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/EHZ;->y:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/EHZ;->x:I

    goto :goto_0
.end method

.method public static n(LX/EHZ;)LX/6Ia;
    .locals 5

    .prologue
    .line 2099755
    invoke-static {}, LX/6JD;->a()Z

    move-result v0

    .line 2099756
    if-nez v0, :cond_0

    .line 2099757
    iget-object v0, p0, LX/EHZ;->o:Landroid/content/Context;

    iget-object v1, p0, LX/EHZ;->g:LX/6JF;

    iget-object v2, p0, LX/EHZ;->p:LX/6KN;

    iget-object v3, p0, LX/EHZ;->M:LX/6KB;

    .line 2099758
    iget-object v4, v3, LX/6KB;->a:LX/6KF;

    move-object v3, v4

    .line 2099759
    invoke-static {v0, v1, v2, v3}, LX/6J7;->a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6KF;)LX/6Ia;

    move-result-object v0

    .line 2099760
    :goto_0
    return-object v0

    .line 2099761
    :cond_0
    sget-object v0, LX/EHZ;->c:Ljava/lang/Class;

    const-string v1, "Forcing camera 1 api on emulator"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2099762
    iget-object v0, p0, LX/EHZ;->o:Landroid/content/Context;

    iget-object v1, p0, LX/EHZ;->g:LX/6JF;

    iget-object v2, p0, LX/EHZ;->p:LX/6KN;

    sget-object v3, LX/6J9;->CAMERA1:LX/6J9;

    iget-object v4, p0, LX/EHZ;->M:LX/6KB;

    .line 2099763
    iget-object p0, v4, LX/6KB;->a:LX/6KF;

    move-object v4, p0

    .line 2099764
    invoke-static {v0, v1, v2, v3, v4}, LX/6J7;->a(Landroid/content/Context;LX/6JF;LX/6KN;LX/6J9;LX/6KF;)LX/6Ia;

    move-result-object v0

    goto :goto_0
.end method

.method public static p(LX/EHZ;)V
    .locals 2

    .prologue
    .line 2099806
    iget-object v0, p0, LX/EHZ;->l:LX/6Ia;

    if-eqz v0, :cond_0

    .line 2099807
    iget-object v0, p0, LX/EHZ;->l:LX/6Ia;

    new-instance v1, LX/EHV;

    invoke-direct {v1, p0}, LX/EHV;-><init>(LX/EHZ;)V

    invoke-interface {v0, v1}, LX/6Ia;->b(LX/6Ik;)V

    .line 2099808
    :cond_0
    return-void
.end method

.method private r()V
    .locals 14

    .prologue
    .line 2099747
    iget-wide v0, p0, LX/EHZ;->v:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, LX/EHZ;->a:LX/BA6;

    if-eqz v0, :cond_0

    iget v0, p0, LX/EHZ;->r:I

    if-lez v0, :cond_0

    .line 2099748
    iget-object v1, p0, LX/EHZ;->K:LX/2S7;

    iget-object v0, p0, LX/EHZ;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->z()J

    move-result-wide v2

    iget-object v0, p0, LX/EHZ;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->y()J

    move-result-wide v4

    const/4 v6, 0x0

    sget-object v7, LX/79H;->WebRTCExpressionMask:LX/79H;

    iget-object v0, p0, LX/EHZ;->a:LX/BA6;

    .line 2099749
    iget-object v8, v0, LX/BA6;->m:Ljava/lang/String;

    move-object v8, v8

    .line 2099750
    iget-object v0, p0, LX/EHZ;->a:LX/BA6;

    .line 2099751
    iget-object v9, v0, LX/BA6;->n:Ljava/lang/String;

    move-object v9, v9

    .line 2099752
    iget-object v0, p0, LX/EHZ;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v10

    iget-wide v12, p0, LX/EHZ;->v:J

    sub-long/2addr v10, v12

    invoke-virtual/range {v1 .. v11}, LX/2S7;->a(JJZLX/79H;Ljava/lang/String;Ljava/lang/String;J)V

    .line 2099753
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EHZ;->v:J

    .line 2099754
    :cond_0
    return-void
.end method

.method public static s(LX/EHZ;)V
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    .line 2099735
    iget-wide v0, p0, LX/EHZ;->v:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EHZ;->a:LX/BA6;

    if-eqz v0, :cond_0

    iget v0, p0, LX/EHZ;->r:I

    if-lez v0, :cond_0

    .line 2099736
    iget-object v1, p0, LX/EHZ;->K:LX/2S7;

    iget-object v0, p0, LX/EHZ;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2099737
    iget-wide v12, v0, LX/EDx;->ac:J

    move-wide v2, v12

    .line 2099738
    iget-object v0, p0, LX/EHZ;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2099739
    iget-wide v12, v0, LX/EDx;->ak:J

    move-wide v4, v12

    .line 2099740
    const/4 v6, 0x1

    sget-object v7, LX/79H;->WebRTCExpressionMask:LX/79H;

    iget-object v0, p0, LX/EHZ;->a:LX/BA6;

    .line 2099741
    iget-object v8, v0, LX/BA6;->m:Ljava/lang/String;

    move-object v8, v8

    .line 2099742
    iget-object v0, p0, LX/EHZ;->a:LX/BA6;

    .line 2099743
    iget-object v9, v0, LX/BA6;->n:Ljava/lang/String;

    move-object v9, v9

    .line 2099744
    invoke-virtual/range {v1 .. v11}, LX/2S7;->a(JJZLX/79H;Ljava/lang/String;Ljava/lang/String;J)V

    .line 2099745
    iget-object v0, p0, LX/EHZ;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/EHZ;->v:J

    .line 2099746
    :cond_0
    return-void
.end method

.method public static t(LX/EHZ;)V
    .locals 1

    .prologue
    .line 2099732
    iget-object v0, p0, LX/EHZ;->q:LX/EDx;

    if-eqz v0, :cond_0

    .line 2099733
    iget-object v0, p0, LX/EHZ;->q:LX/EDx;

    invoke-virtual {v0}, LX/EDx;->a()V

    .line 2099734
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILX/BA6;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2099717
    iget v0, p0, LX/EHZ;->r:I

    if-eq v0, p1, :cond_0

    .line 2099718
    invoke-direct {p0}, LX/EHZ;->r()V

    .line 2099719
    :cond_0
    iput p1, p0, LX/EHZ;->r:I

    .line 2099720
    iput-object p2, p0, LX/EHZ;->a:LX/BA6;

    .line 2099721
    iget v0, p0, LX/EHZ;->r:I

    if-lez v0, :cond_1

    .line 2099722
    invoke-static {p0}, LX/EHZ;->s(LX/EHZ;)V

    .line 2099723
    :cond_1
    iget v0, p0, LX/EHZ;->r:I

    if-lez v0, :cond_3

    .line 2099724
    iget-wide v0, p0, LX/EHZ;->u:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    .line 2099725
    iget-object v0, p0, LX/EHZ;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/EHZ;->u:J

    .line 2099726
    :cond_2
    :goto_0
    new-instance v0, LX/BAE;

    invoke-direct {v0, p2}, LX/BAE;-><init>(LX/BA6;)V

    .line 2099727
    iget-object v1, p0, LX/EHZ;->e:LX/6Jt;

    iget-object v2, p0, LX/EHZ;->f:LX/BAC;

    invoke-virtual {v1, v0, v2}, LX/6Jt;->a(LX/7Sb;LX/6Jv;)V

    .line 2099728
    return-void

    .line 2099729
    :cond_3
    iget-wide v0, p0, LX/EHZ;->u:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_2

    .line 2099730
    iget-wide v0, p0, LX/EHZ;->w:J

    iget-object v2, p0, LX/EHZ;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/EHZ;->u:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/EHZ;->w:J

    .line 2099731
    iput-wide v6, p0, LX/EHZ;->u:J

    goto :goto_0
.end method

.method public final a(Landroid/view/TextureView;)V
    .locals 3

    .prologue
    .line 2099705
    iget-object v0, p0, LX/EHZ;->d:Landroid/view/TextureView;

    if-eq p1, v0, :cond_1

    .line 2099706
    if-eqz p1, :cond_0

    .line 2099707
    iget-object v0, p0, LX/EHZ;->e:LX/6Jt;

    .line 2099708
    iget-object v1, v0, LX/6Jt;->j:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2099709
    new-instance v1, LX/6Kw;

    invoke-virtual {p1}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v2

    invoke-direct {v1, p1, v2}, LX/6Kw;-><init>(Landroid/view/TextureView;Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 2099710
    iget-object v2, v0, LX/6Jt;->j:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2099711
    invoke-virtual {v0, v1}, LX/6Jt;->a(LX/6Kd;)V

    .line 2099712
    :cond_0
    iget-object v0, p0, LX/EHZ;->d:Landroid/view/TextureView;

    if-eqz v0, :cond_1

    .line 2099713
    iget-object v0, p0, LX/EHZ;->e:LX/6Jt;

    iget-object v1, p0, LX/EHZ;->d:Landroid/view/TextureView;

    invoke-virtual {v0, v1}, LX/6Jt;->a(Landroid/view/View;)V

    .line 2099714
    :cond_1
    iput-object p1, p0, LX/EHZ;->d:Landroid/view/TextureView;

    .line 2099715
    return-void

    .line 2099716
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 6

    .prologue
    .line 2099702
    iget-wide v0, p0, LX/EHZ;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2099703
    iget-wide v0, p0, LX/EHZ;->w:J

    iget-object v2, p0, LX/EHZ;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/EHZ;->u:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 2099704
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/EHZ;->w:J

    goto :goto_0
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 2099689
    iget-object v0, p0, LX/EHZ;->e:LX/6Jt;

    if-eqz v0, :cond_1

    .line 2099690
    iget-object v0, p0, LX/EHZ;->G:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, LX/EHZ;->s:I

    .line 2099691
    iget-object v0, p0, LX/EHZ;->e:LX/6Jt;

    iget v1, p0, LX/EHZ;->s:I

    invoke-virtual {v0, v1}, LX/6Jt;->a(I)V

    .line 2099692
    iget-object v0, p0, LX/EHZ;->n:LX/6Kd;

    if-eqz v0, :cond_0

    .line 2099693
    iget-object v0, p0, LX/EHZ;->e:LX/6Jt;

    iget-object v1, p0, LX/EHZ;->n:LX/6Kd;

    invoke-virtual {v0, v1}, LX/6Jt;->b(LX/6Kd;)V

    .line 2099694
    :cond_0
    new-instance v1, LX/EFT;

    iget-object v2, p0, LX/EHZ;->H:LX/03V;

    iget-object v0, p0, LX/EHZ;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->Z()Lcom/facebook/webrtc/MediaCaptureSink;

    move-result-object v0

    invoke-static {p0}, LX/EHZ;->l(LX/EHZ;)I

    move-result v3

    invoke-static {p0}, LX/EHZ;->m(LX/EHZ;)I

    move-result v4

    invoke-direct {v1, v2, v0, v3, v4}, LX/EFT;-><init>(LX/03V;Lcom/facebook/webrtc/MediaCaptureSink;II)V

    iput-object v1, p0, LX/EHZ;->n:LX/6Kd;

    .line 2099695
    iget-object v0, p0, LX/EHZ;->e:LX/6Jt;

    iget-object v1, p0, LX/EHZ;->n:LX/6Kd;

    invoke-virtual {v0, v1}, LX/6Jt;->a(LX/6Kd;)V

    .line 2099696
    iget v0, p0, LX/EHZ;->L:I

    if-lez v0, :cond_1

    .line 2099697
    iget-boolean v0, p0, LX/EHZ;->z:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/EHZ;->L:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2099698
    invoke-static {p0}, LX/EHZ;->p(LX/EHZ;)V

    .line 2099699
    :cond_1
    :goto_0
    return-void

    .line 2099700
    :cond_2
    iget-boolean v0, p0, LX/EHZ;->z:Z

    if-nez v0, :cond_1

    iget v0, p0, LX/EHZ;->L:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2099701
    invoke-static {p0}, LX/EHZ;->p(LX/EHZ;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2099676
    iget-object v0, p0, LX/EHZ;->l:LX/6Ia;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EHZ;->l:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2099677
    :goto_0
    return-void

    .line 2099678
    :cond_0
    new-instance v0, LX/EHU;

    invoke-direct {v0, p0}, LX/EHU;-><init>(LX/EHZ;)V

    invoke-static {p0, v0}, LX/EHZ;->a$redex0(LX/EHZ;LX/6Ik;)V

    .line 2099679
    iget-wide v0, p0, LX/EHZ;->u:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    .line 2099680
    iget-wide v0, p0, LX/EHZ;->w:J

    iget-object v2, p0, LX/EHZ;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/EHZ;->u:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/EHZ;->w:J

    .line 2099681
    iput-wide v6, p0, LX/EHZ;->u:J

    .line 2099682
    :cond_1
    invoke-direct {p0}, LX/EHZ;->r()V

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2099674
    sget-object v0, LX/6JF;->FRONT:LX/6JF;

    iput-object v0, p0, LX/EHZ;->g:LX/6JF;

    .line 2099675
    return-void
.end method
