.class public LX/E4n;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/E4n;


# instance fields
.field private final a:LX/E4b;


# direct methods
.method public constructor <init>(LX/E4b;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2077271
    iput-object p1, p0, LX/E4n;->a:LX/E4b;

    .line 2077272
    return-void
.end method

.method public static a(LX/0QB;)LX/E4n;
    .locals 4

    .prologue
    .line 2077273
    sget-object v0, LX/E4n;->b:LX/E4n;

    if-nez v0, :cond_1

    .line 2077274
    const-class v1, LX/E4n;

    monitor-enter v1

    .line 2077275
    :try_start_0
    sget-object v0, LX/E4n;->b:LX/E4n;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077276
    if-eqz v2, :cond_0

    .line 2077277
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077278
    new-instance p0, LX/E4n;

    invoke-static {v0}, LX/E4b;->a(LX/0QB;)LX/E4b;

    move-result-object v3

    check-cast v3, LX/E4b;

    invoke-direct {p0, v3}, LX/E4n;-><init>(LX/E4b;)V

    .line 2077279
    move-object v0, p0

    .line 2077280
    sput-object v0, LX/E4n;->b:LX/E4n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077281
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077282
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077283
    :cond_1
    sget-object v0, LX/E4n;->b:LX/E4n;

    return-object v0

    .line 2077284
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/0Px;LX/2km;Ljava/lang/String;Ljava/lang/String;)LX/1Dg;
    .locals 7
    .param p2    # LX/0Px;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/0Px",
            "<+",
            "LX/9uh;",
            ">;TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    .line 2077286
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    .line 2077287
    invoke-virtual {p2}, LX/0Px;->listIterator()LX/0Rb;

    move-result-object v3

    .line 2077288
    :cond_0
    :goto_0
    invoke-virtual {v3}, LX/0Rb;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2077289
    invoke-virtual {v3}, LX/0Rb;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uh;

    .line 2077290
    invoke-interface {v0}, LX/9uh;->b()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uh;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2077291
    iget-object v1, p0, LX/E4n;->a:LX/E4b;

    invoke-virtual {v1, p1}, LX/E4b;->c(LX/1De;)LX/E4Z;

    move-result-object v1

    invoke-interface {v0}, LX/9uh;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/E4Z;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/E4Z;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-interface {v0}, LX/9uh;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    if-nez v1, :cond_1

    const v1, 0x7f0a010e

    :goto_1
    invoke-virtual {v5, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-interface {v0}, LX/9uh;->b()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/E4Z;->a(LX/1X5;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/E4Z;->a(LX/2km;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/E4Z;->c(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/E4Z;->d(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x6

    const v4, 0x7f0b163d

    invoke-interface {v0, v1, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2077292
    invoke-virtual {v3}, LX/0Rb;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2077293
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const v1, 0x7f0215d6

    invoke-virtual {v0, v1}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    goto/16 :goto_0

    .line 2077294
    :cond_1
    const v1, 0x7f0a00d1

    goto :goto_1

    .line 2077295
    :cond_2
    invoke-interface {v2, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
