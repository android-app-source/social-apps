.class public LX/EMx;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EMv;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EMy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2111612
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EMx;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EMy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111613
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2111614
    iput-object p1, p0, LX/EMx;->b:LX/0Ot;

    .line 2111615
    return-void
.end method

.method public static a(LX/0QB;)LX/EMx;
    .locals 4

    .prologue
    .line 2111616
    const-class v1, LX/EMx;

    monitor-enter v1

    .line 2111617
    :try_start_0
    sget-object v0, LX/EMx;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111618
    sput-object v2, LX/EMx;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111619
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111620
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111621
    new-instance v3, LX/EMx;

    const/16 p0, 0x342c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EMx;-><init>(LX/0Ot;)V

    .line 2111622
    move-object v0, v3

    .line 2111623
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111624
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EMx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111625
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111626
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2111627
    check-cast p2, LX/EMw;

    .line 2111628
    iget-object v0, p0, LX/EMx;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EMy;

    iget-object v1, p2, LX/EMw;->a:Ljava/lang/CharSequence;

    const/4 p2, 0x2

    const/4 p0, 0x0

    .line 2111629
    iget-object v2, v0, LX/EMy;->a:LX/1vg;

    invoke-virtual {v2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v2

    const v3, 0x7f021940

    invoke-virtual {v2, v3}, LX/2xv;->h(I)LX/2xv;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, LX/2xv;->j(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 2111630
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x7

    const/16 v5, 0x18

    invoke-interface {v3, v4, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-interface {v3, v4}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v4, 0x3

    const/16 v5, 0xc

    invoke-interface {v2, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0e011e

    invoke-static {p1, p0, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0056

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    sget-object v4, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v3, v4}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2111631
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2111632
    invoke-static {}, LX/1dS;->b()V

    .line 2111633
    const/4 v0, 0x0

    return-object v0
.end method
