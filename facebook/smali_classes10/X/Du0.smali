.class public final LX/Du0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2055455
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2055456
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2055457
    :goto_0
    return v1

    .line 2055458
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2055459
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2055460
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2055461
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2055462
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2055463
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2055464
    invoke-static {p0, p1}, LX/DuJ;->b(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2055465
    :cond_2
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2055466
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2055467
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_9

    .line 2055468
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2055469
    :goto_2
    move v0, v3

    .line 2055470
    goto :goto_1

    .line 2055471
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2055472
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2055473
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2055474
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2055475
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 2055476
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2055477
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2055478
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_5

    if-eqz v6, :cond_5

    .line 2055479
    const-string v7, "has_next_page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2055480
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    goto :goto_3

    .line 2055481
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2055482
    :cond_7
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2055483
    if-eqz v0, :cond_8

    .line 2055484
    invoke-virtual {p1, v3, v5}, LX/186;->a(IZ)V

    .line 2055485
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v0, v3

    move v5, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2055486
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2055487
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2055488
    if-eqz v0, :cond_0

    .line 2055489
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2055490
    invoke-static {p0, v0, p2, p3}, LX/DuJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2055491
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2055492
    if-eqz v0, :cond_2

    .line 2055493
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2055494
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2055495
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2055496
    if-eqz v1, :cond_1

    .line 2055497
    const-string p1, "has_next_page"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2055498
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2055499
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2055500
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2055501
    return-void
.end method
