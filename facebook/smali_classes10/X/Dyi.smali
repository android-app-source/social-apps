.class public final LX/Dyi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/ipc/katana/model/GeoRegion;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/checkin/PlacePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V
    .locals 0

    .prologue
    .line 2064817
    iput-object p1, p0, LX/Dyi;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2064818
    sget-object v0, Lcom/facebook/places/checkin/PlacePickerFragment;->F:Ljava/lang/Class;

    const-string v1, "Error while get nearby regions"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2064819
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2064820
    check-cast p1, LX/0Px;

    .line 2064821
    iget-object v0, p0, LX/Dyi;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-static {v0, p1}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/0Px;)V

    .line 2064822
    return-void
.end method
