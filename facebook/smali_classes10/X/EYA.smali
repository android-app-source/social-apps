.class public final LX/EYA;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EY8;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EYA;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EYA;


# instance fields
.field public bitField0_:I

.field public isExtension_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public namePart_:Ljava/lang/Object;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2136580
    new-instance v0, LX/EY7;

    invoke-direct {v0}, LX/EY7;-><init>()V

    sput-object v0, LX/EYA;->a:LX/EWZ;

    .line 2136581
    new-instance v0, LX/EYA;

    invoke-direct {v0}, LX/EYA;-><init>()V

    .line 2136582
    sput-object v0, LX/EYA;->c:LX/EYA;

    invoke-direct {v0}, LX/EYA;->o()V

    .line 2136583
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2136584
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2136585
    iput-byte v0, p0, LX/EYA;->memoizedIsInitialized:B

    .line 2136586
    iput v0, p0, LX/EYA;->memoizedSerializedSize:I

    .line 2136587
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2136588
    iput-object v0, p0, LX/EYA;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2136589
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2136590
    iput-byte v0, p0, LX/EYA;->memoizedIsInitialized:B

    .line 2136591
    iput v0, p0, LX/EYA;->memoizedSerializedSize:I

    .line 2136592
    invoke-direct {p0}, LX/EYA;->o()V

    .line 2136593
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2136594
    const/4 v0, 0x0

    .line 2136595
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2136596
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2136597
    sparse-switch v3, :sswitch_data_0

    .line 2136598
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2136599
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2136600
    goto :goto_0

    .line 2136601
    :sswitch_1
    iget v3, p0, LX/EYA;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EYA;->bitField0_:I

    .line 2136602
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EYA;->namePart_:Ljava/lang/Object;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2136603
    :catch_0
    move-exception v0

    .line 2136604
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2136605
    move-object v0, v0

    .line 2136606
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2136607
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EYA;->unknownFields:LX/EZQ;

    .line 2136608
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2136609
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EYA;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EYA;->bitField0_:I

    .line 2136610
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v3

    iput-boolean v3, p0, LX/EYA;->isExtension_:Z
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2136611
    :catch_1
    move-exception v0

    .line 2136612
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2136613
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2136614
    move-object v0, v1

    .line 2136615
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2136616
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EYA;->unknownFields:LX/EZQ;

    .line 2136617
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2136618
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2136619
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2136620
    iput-byte v1, p0, LX/EYA;->memoizedIsInitialized:B

    .line 2136621
    iput v1, p0, LX/EYA;->memoizedSerializedSize:I

    .line 2136622
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EYA;->unknownFields:LX/EZQ;

    .line 2136623
    return-void
.end method

.method private static b(LX/EYA;)LX/EY9;
    .locals 1

    .prologue
    .line 2136624
    invoke-static {}, LX/EY9;->m()LX/EY9;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EY9;->a(LX/EYA;)LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method private n()LX/EWc;
    .locals 2

    .prologue
    .line 2136545
    iget-object v0, p0, LX/EYA;->namePart_:Ljava/lang/Object;

    .line 2136546
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2136547
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2136548
    iput-object v0, p0, LX/EYA;->namePart_:Ljava/lang/Object;

    .line 2136549
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 2136625
    const-string v0, ""

    iput-object v0, p0, LX/EYA;->namePart_:Ljava/lang/Object;

    .line 2136626
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EYA;->isExtension_:Z

    .line 2136627
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2136628
    new-instance v0, LX/EY9;

    invoke-direct {v0, p1}, LX/EY9;-><init>(LX/EYd;)V

    .line 2136629
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2136630
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2136631
    iget v0, p0, LX/EYA;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2136632
    invoke-direct {p0}, LX/EYA;->n()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2136633
    :cond_0
    iget v0, p0, LX/EYA;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2136634
    iget-boolean v0, p0, LX/EYA;->isExtension_:Z

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(IZ)V

    .line 2136635
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2136636
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2136559
    iget-byte v2, p0, LX/EYA;->memoizedIsInitialized:B

    .line 2136560
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 2136561
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 2136562
    goto :goto_0

    .line 2136563
    :cond_1
    invoke-virtual {p0}, LX/EYA;->j()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2136564
    iput-byte v1, p0, LX/EYA;->memoizedIsInitialized:B

    move v0, v1

    .line 2136565
    goto :goto_0

    .line 2136566
    :cond_2
    invoke-virtual {p0}, LX/EYA;->k()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2136567
    iput-byte v1, p0, LX/EYA;->memoizedIsInitialized:B

    move v0, v1

    .line 2136568
    goto :goto_0

    .line 2136569
    :cond_3
    iput-byte v0, p0, LX/EYA;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2136570
    iget v0, p0, LX/EYA;->memoizedSerializedSize:I

    .line 2136571
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2136572
    :goto_0
    return v0

    .line 2136573
    :cond_0
    const/4 v0, 0x0

    .line 2136574
    iget v1, p0, LX/EYA;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2136575
    invoke-direct {p0}, LX/EYA;->n()LX/EWc;

    move-result-object v0

    invoke-static {v2, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2136576
    :cond_1
    iget v1, p0, LX/EYA;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2136577
    iget-boolean v1, p0, LX/EYA;->isExtension_:Z

    invoke-static {v3, v1}, LX/EWf;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2136578
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2136579
    iput v0, p0, LX/EYA;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2136558
    iget-object v0, p0, LX/EYA;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2136557
    sget-object v0, LX/EYC;->J:LX/EYn;

    const-class v1, LX/EYA;

    const-class v2, LX/EY9;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EYA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2136556
    sget-object v0, LX/EYA;->a:LX/EWZ;

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2136555
    iget v1, p0, LX/EYA;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 2136554
    iget v0, p0, LX/EYA;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2136553
    invoke-static {p0}, LX/EYA;->b(LX/EYA;)LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2136552
    invoke-static {}, LX/EY9;->m()LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2136551
    invoke-static {p0}, LX/EYA;->b(LX/EYA;)LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2136550
    sget-object v0, LX/EYA;->c:LX/EYA;

    return-object v0
.end method
