.class public LX/Dpf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final public_key_with_id:LX/DpU;

.field public final signature:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2046613
    new-instance v0, LX/1sv;

    const-string v1, "SignedPublicKeyWithID"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dpf;->b:LX/1sv;

    .line 2046614
    new-instance v0, LX/1sw;

    const-string v1, "public_key_with_id"

    const/16 v2, 0xc

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpf;->c:LX/1sw;

    .line 2046615
    new-instance v0, LX/1sw;

    const-string v1, "signature"

    const/16 v2, 0xb

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpf;->d:LX/1sw;

    .line 2046616
    const/4 v0, 0x1

    sput-boolean v0, LX/Dpf;->a:Z

    return-void
.end method

.method public constructor <init>(LX/DpU;[B)V
    .locals 0

    .prologue
    .line 2046631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2046632
    iput-object p1, p0, LX/Dpf;->public_key_with_id:LX/DpU;

    .line 2046633
    iput-object p2, p0, LX/Dpf;->signature:[B

    .line 2046634
    return-void
.end method

.method public static b(LX/1su;)LX/Dpf;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2046617
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    .line 2046618
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2046619
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_2

    .line 2046620
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 2046621
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046622
    :pswitch_0
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xc

    if-ne v3, v4, :cond_0

    .line 2046623
    invoke-static {p0}, LX/DpU;->b(LX/1su;)LX/DpU;

    move-result-object v1

    goto :goto_0

    .line 2046624
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046625
    :pswitch_1
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xb

    if-ne v3, v4, :cond_1

    .line 2046626
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v0

    goto :goto_0

    .line 2046627
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046628
    :cond_2
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2046629
    new-instance v2, LX/Dpf;

    invoke-direct {v2, v1, v0}, LX/Dpf;-><init>(LX/DpU;[B)V

    .line 2046630
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2046585
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2046586
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2046587
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2046588
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SignedPublicKeyWithID"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046589
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046590
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046591
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046592
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046593
    const-string v4, "public_key_with_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046594
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046595
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046596
    iget-object v4, p0, LX/Dpf;->public_key_with_id:LX/DpU;

    if-nez v4, :cond_3

    .line 2046597
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046598
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046599
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046600
    const-string v4, "signature"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046601
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046602
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046603
    iget-object v0, p0, LX/Dpf;->signature:[B

    if-nez v0, :cond_4

    .line 2046604
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046605
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046606
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046607
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2046608
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2046609
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2046610
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2046611
    :cond_3
    iget-object v4, p0, LX/Dpf;->public_key_with_id:LX/DpU;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2046612
    :cond_4
    iget-object v0, p0, LX/Dpf;->signature:[B

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2046635
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2046636
    iget-object v0, p0, LX/Dpf;->public_key_with_id:LX/DpU;

    if-eqz v0, :cond_0

    .line 2046637
    sget-object v0, LX/Dpf;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046638
    iget-object v0, p0, LX/Dpf;->public_key_with_id:LX/DpU;

    invoke-virtual {v0, p1}, LX/DpU;->a(LX/1su;)V

    .line 2046639
    :cond_0
    iget-object v0, p0, LX/Dpf;->signature:[B

    if-eqz v0, :cond_1

    .line 2046640
    sget-object v0, LX/Dpf;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046641
    iget-object v0, p0, LX/Dpf;->signature:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2046642
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2046643
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2046644
    return-void
.end method

.method public final a(LX/Dpf;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2046568
    if-nez p1, :cond_1

    .line 2046569
    :cond_0
    :goto_0
    return v2

    .line 2046570
    :cond_1
    iget-object v0, p0, LX/Dpf;->public_key_with_id:LX/DpU;

    if-eqz v0, :cond_6

    move v0, v1

    .line 2046571
    :goto_1
    iget-object v3, p1, LX/Dpf;->public_key_with_id:LX/DpU;

    if-eqz v3, :cond_7

    move v3, v1

    .line 2046572
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 2046573
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2046574
    iget-object v0, p0, LX/Dpf;->public_key_with_id:LX/DpU;

    iget-object v3, p1, LX/Dpf;->public_key_with_id:LX/DpU;

    invoke-virtual {v0, v3}, LX/DpU;->a(LX/DpU;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2046575
    :cond_3
    iget-object v0, p0, LX/Dpf;->signature:[B

    if-eqz v0, :cond_8

    move v0, v1

    .line 2046576
    :goto_3
    iget-object v3, p1, LX/Dpf;->signature:[B

    if-eqz v3, :cond_9

    move v3, v1

    .line 2046577
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2046578
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2046579
    iget-object v0, p0, LX/Dpf;->signature:[B

    iget-object v3, p1, LX/Dpf;->signature:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_5
    move v2, v1

    .line 2046580
    goto :goto_0

    :cond_6
    move v0, v2

    .line 2046581
    goto :goto_1

    :cond_7
    move v3, v2

    .line 2046582
    goto :goto_2

    :cond_8
    move v0, v2

    .line 2046583
    goto :goto_3

    :cond_9
    move v3, v2

    .line 2046584
    goto :goto_4
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2046560
    if-nez p1, :cond_1

    .line 2046561
    :cond_0
    :goto_0
    return v0

    .line 2046562
    :cond_1
    instance-of v1, p1, LX/Dpf;

    if-eqz v1, :cond_0

    .line 2046563
    check-cast p1, LX/Dpf;

    invoke-virtual {p0, p1}, LX/Dpf;->a(LX/Dpf;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2046567
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2046564
    sget-boolean v0, LX/Dpf;->a:Z

    .line 2046565
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Dpf;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2046566
    return-object v0
.end method
