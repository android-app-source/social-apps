.class public LX/CrP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CqY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/CqY",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Float;


# direct methods
.method public constructor <init>(F)V
    .locals 1

    .prologue
    .line 1940933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1940934
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, LX/CrP;->a:Ljava/lang/Float;

    .line 1940935
    return-void
.end method

.method private constructor <init>(LX/CrP;)V
    .locals 1

    .prologue
    .line 1940931
    iget-object v0, p1, LX/CrP;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, LX/CrP;-><init>(F)V

    .line 1940932
    return-void
.end method


# virtual methods
.method public final a(LX/CqY;F)LX/CqY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CqY",
            "<",
            "Ljava/lang/Float;",
            ">;F)",
            "LX/CqY",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1940928
    invoke-interface {p1}, LX/CqY;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1940929
    iget-object v1, p0, LX/CrP;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {v1, v0, p2}, LX/CrU;->a(FFF)F

    move-result v0

    .line 1940930
    new-instance v1, LX/CrP;

    invoke-direct {v1, v0}, LX/CrP;-><init>(F)V

    return-object v1
.end method

.method public final a()LX/CrQ;
    .locals 1

    .prologue
    .line 1940927
    sget-object v0, LX/CrQ;->ANGLE:LX/CrQ;

    return-object v0
.end method

.method public final c()LX/CqY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/CqY",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1940926
    new-instance v0, LX/CrP;

    invoke-direct {v0, p0}, LX/CrP;-><init>(LX/CrP;)V

    return-object v0
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1940915
    iget-object v0, p0, LX/CrP;->a:Ljava/lang/Float;

    move-object v0, v0

    .line 1940916
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1940919
    if-ne p0, p1, :cond_0

    .line 1940920
    const/4 v0, 0x1

    .line 1940921
    :goto_0
    return v0

    .line 1940922
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 1940923
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1940924
    :cond_2
    check-cast p1, LX/CrP;

    .line 1940925
    iget-object v0, p0, LX/CrP;->a:Ljava/lang/Float;

    iget-object v1, p1, LX/CrP;->a:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1940918
    iget-object v0, p0, LX/CrP;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1940917
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "{type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/CrP;->a()LX/CrQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", v: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/CrP;->a:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
