.class public LX/Cow;
.super LX/Cos;
.source ""

# interfaces
.implements LX/CnE;
.implements LX/CnG;
.implements LX/CnH;
.implements LX/CnI;
.implements LX/CnJ;
.implements LX/Cov;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cos",
        "<",
        "LX/Cne;",
        "Lcom/facebook/richdocument/view/widget/RichDocumentImageView;",
        ">;",
        "Lcom/facebook/richdocument/view/block/ImageBlockView;",
        "LX/Cov;"
    }
.end annotation


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8bK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ckq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final k:Z

.field private final l:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field public o:I

.field public p:I

.field private q:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field public r:LX/Cov;

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1936399
    invoke-direct {p0, p1, p2}, LX/Cos;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 1936400
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v4, p0

    check-cast v4, LX/Cow;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v6

    check-cast v6, LX/8bK;

    invoke-static {v0}, LX/Ckq;->a(LX/0QB;)LX/Ckq;

    move-result-object v7

    check-cast v7, LX/Ckq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p2

    check-cast p2, LX/0ad;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v0

    check-cast v0, LX/Chi;

    iput-object v5, v4, LX/Cow;->a:LX/0Uh;

    iput-object v6, v4, LX/Cow;->b:LX/8bK;

    iput-object v7, v4, LX/Cow;->c:LX/Ckq;

    iput-object p2, v4, LX/Cow;->d:LX/0ad;

    iput-object v0, v4, LX/Cow;->e:LX/Chi;

    .line 1936401
    iget-object v0, p0, LX/Cow;->b:LX/8bK;

    invoke-virtual {v0}, LX/8bK;->a()LX/0p3;

    move-result-object v0

    .line 1936402
    iget-object v3, p0, LX/Cow;->a:LX/0Uh;

    const/16 v4, 0x3e7

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    iput-boolean v3, p0, LX/Cow;->k:Z

    .line 1936403
    iget-object v3, p0, LX/Cow;->a:LX/0Uh;

    const/16 v4, 0x3e9

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 1936404
    iget-boolean v4, p0, LX/Cow;->k:Z

    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    sget-object v3, LX/0p3;->GOOD:LX/0p3;

    invoke-virtual {v0, v3}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_1

    invoke-virtual {p0}, LX/Cow;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/Cow;->l:Z

    .line 1936405
    iget-object v0, p0, LX/Cow;->d:LX/0ad;

    sget-short v3, LX/2yD;->l:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Cow;->u:Z

    .line 1936406
    iget-object v0, p0, LX/Cow;->d:LX/0ad;

    sget-short v3, LX/2yD;->k:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Cow;->v:Z

    .line 1936407
    iget-boolean v0, p0, LX/Cow;->u:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/Cow;->v:Z

    if-eqz v0, :cond_2

    :goto_1
    iput-boolean v1, p0, LX/Cow;->w:Z

    .line 1936408
    iget-boolean v0, p0, LX/Cow;->l:Z

    if-eqz v0, :cond_0

    .line 1936409
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1936410
    const v1, 0x7f031201

    .line 1936411
    iget-object v3, p0, LX/Cos;->a:LX/Ctg;

    move-object v3, v3

    .line 1936412
    invoke-interface {v3}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    iput-object v0, p0, LX/Cow;->x:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    .line 1936413
    new-instance v0, LX/Ctv;

    .line 1936414
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 1936415
    iget-object v2, p0, LX/Cow;->x:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-direct {v0, v1, v2}, LX/Ctv;-><init>(LX/Ctg;Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;)V

    .line 1936416
    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936417
    iput-object v0, p0, LX/Cow;->r:LX/Cov;

    .line 1936418
    :cond_0
    new-instance v0, LX/CuI;

    invoke-direct {v0, p1}, LX/CuI;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936419
    new-instance v0, LX/CuA;

    invoke-direct {v0, p1}, LX/CuA;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936420
    new-instance v0, LX/Cty;

    invoke-direct {v0, p1}, LX/Cty;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936421
    new-instance v0, LX/Cu1;

    invoke-direct {v0, p1}, LX/Cu1;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936422
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    .line 1936423
    iput-object p0, v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->h:LX/Cov;

    .line 1936424
    return-void

    :cond_1
    move v0, v2

    .line 1936425
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1936426
    goto :goto_1
.end method

.method public static a(Landroid/view/View;)LX/Cow;
    .locals 2

    .prologue
    .line 1936438
    new-instance v1, LX/Cow;

    move-object v0, p0

    check-cast v0, LX/Ctg;

    invoke-direct {v1, v0, p0}, LX/Cow;-><init>(LX/Ctg;Landroid/view/View;)V

    return-object v1
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1936350
    invoke-super {p0, p1}, LX/Cos;->a(Landroid/os/Bundle;)V

    .line 1936351
    iput-boolean v0, p0, LX/Cow;->t:Z

    .line 1936352
    iput-boolean v0, p0, LX/Cow;->s:Z

    .line 1936353
    iput-object v1, p0, LX/Cow;->n:Ljava/lang/String;

    .line 1936354
    iput-object v1, p0, LX/Cow;->m:Ljava/lang/String;

    .line 1936355
    iput v0, p0, LX/Cow;->o:I

    .line 1936356
    iput v0, p0, LX/Cow;->p:I

    .line 1936357
    iput-object v1, p0, LX/Cow;->q:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1936358
    const-string v0, "ImageBlockViewImpl.reset#reset RichDocumentImageView"

    const v1, 0x4f79e79e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1936359
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->c()V

    .line 1936360
    const v0, -0x2026d3bc

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1936361
    return-void
.end method

.method public final a(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V
    .locals 9

    .prologue
    .line 1936427
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cow;->s:Z

    .line 1936428
    iget-object v0, p0, LX/Cow;->c:LX/Ckq;

    iget-object v1, p0, LX/Cow;->n:Ljava/lang/String;

    .line 1936429
    iget-object v2, v0, LX/Ckq;->f:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ckp;

    .line 1936430
    if-eqz v2, :cond_0

    iget-boolean v3, v2, LX/Ckp;->h:Z

    if-nez v3, :cond_0

    .line 1936431
    iget-object v3, v0, LX/Ckq;->e:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, v2, LX/Ckp;->j:J

    sub-long/2addr v4, v6

    .line 1936432
    iput-wide v4, v2, LX/Ckp;->e:J

    .line 1936433
    const/4 v3, 0x1

    .line 1936434
    iput-boolean v3, v2, LX/Ckp;->i:Z

    .line 1936435
    :cond_0
    iget-object v0, p0, LX/Cow;->r:LX/Cov;

    if-eqz v0, :cond_1

    .line 1936436
    iget-object v0, p0, LX/Cow;->r:LX/Cov;

    invoke-interface {v0, p1}, LX/Cov;->b(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V

    .line 1936437
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 1936374
    iget-boolean v0, p0, LX/Cow;->k:Z

    if-nez v0, :cond_0

    .line 1936375
    :goto_0
    return-void

    .line 1936376
    :cond_0
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1936377
    iput-object p1, p0, LX/Cow;->m:Ljava/lang/String;

    .line 1936378
    iput-object p9, p0, LX/Cow;->n:Ljava/lang/String;

    .line 1936379
    iput-object p8, p0, LX/Cow;->q:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1936380
    invoke-static {p8}, LX/CoV;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Cqw;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Cqw;)V

    .line 1936381
    iget-boolean v0, p0, LX/Cow;->l:Z

    if-eqz v0, :cond_0

    .line 1936382
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936383
    iget-object v1, p0, LX/Cow;->x:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-interface {v0, v1}, LX/Cq9;->a(LX/CnQ;)V

    .line 1936384
    :cond_0
    iget-boolean v0, p0, LX/Cow;->w:Z

    if-eqz v0, :cond_2

    if-eqz p5, :cond_2

    .line 1936385
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0, p5, p3, p4, v2}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(Ljava/lang/String;IILjava/lang/String;)V

    .line 1936386
    iput p6, p0, LX/Cow;->o:I

    .line 1936387
    iput p7, p0, LX/Cow;->p:I

    .line 1936388
    const-class v0, LX/Cu0;

    invoke-virtual {p0, v0}, LX/Cos;->b(Ljava/lang/Class;)V

    .line 1936389
    new-instance v0, LX/Cu0;

    .line 1936390
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 1936391
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v2

    check-cast v2, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/Cu0;-><init>(LX/Ctg;Lcom/facebook/richdocument/view/widget/RichDocumentImageView;Ljava/lang/String;II)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936392
    :goto_0
    const-class v0, LX/Cu1;

    invoke-virtual {p0, v0}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v0

    check-cast v0, LX/Cu1;

    .line 1936393
    if-eqz v0, :cond_1

    .line 1936394
    iput-object p9, v0, LX/Cu1;->b:Ljava/lang/String;

    .line 1936395
    :cond_1
    return-void

    .line 1936396
    :cond_2
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0, p2, p3, p4, v2}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(Ljava/lang/String;IILjava/lang/String;)V

    .line 1936397
    iput p3, p0, LX/Cow;->o:I

    .line 1936398
    iput p4, p0, LX/Cow;->p:I

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1936373
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 1936362
    invoke-super {p0, p1}, LX/Cos;->b(Landroid/os/Bundle;)V

    .line 1936363
    iget-object v0, p0, LX/Cow;->c:LX/Ckq;

    iget-object v1, p0, LX/Cow;->n:Ljava/lang/String;

    iget v2, p0, LX/Cow;->o:I

    iget v3, p0, LX/Cow;->p:I

    iget-object v4, p0, LX/Cow;->q:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iget-boolean v5, p0, LX/Cow;->s:Z

    iget-boolean v6, p0, LX/Cow;->t:Z

    invoke-virtual/range {v0 .. v6}, LX/Ckq;->a(Ljava/lang/String;IILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;ZZ)V

    .line 1936364
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1936365
    :goto_0
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;

    if-nez v1, :cond_0

    .line 1936366
    instance-of v1, v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    if-eqz v1, :cond_1

    .line 1936367
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936368
    invoke-interface {v0}, LX/Ctg;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    sget-object v1, LX/ClT;->UFI:LX/ClT;

    invoke-virtual {v0, v1}, LX/Cs7;->a(LX/ClT;)LX/CnQ;

    move-result-object v0

    .line 1936369
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/CnR;

    if-eqz v1, :cond_0

    .line 1936370
    check-cast v0, LX/CnR;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CnR;->setShowShareButton(Z)V

    .line 1936371
    :cond_0
    return-void

    .line 1936372
    :cond_1
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V
    .locals 5

    .prologue
    .line 1936344
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cow;->t:Z

    .line 1936345
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/ChJ;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1936346
    iget-object v1, p0, LX/Cow;->c:LX/Ckq;

    iget-object v2, p0, LX/Cow;->n:Ljava/lang/String;

    iget-boolean v3, p0, LX/Cow;->u:Z

    iget-boolean v4, p0, LX/Cow;->v:Z

    invoke-virtual {v1, v2, v0, v3, v4}, LX/Ckq;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1936347
    iget-object v0, p0, LX/Cow;->r:LX/Cov;

    if-eqz v0, :cond_0

    .line 1936348
    iget-object v0, p0, LX/Cow;->r:LX/Cov;

    invoke-interface {v0, p1}, LX/Cov;->b(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V

    .line 1936349
    :cond_0
    return-void
.end method

.method public final d()LX/CnN;
    .locals 6

    .prologue
    .line 1936343
    iget-object v0, p0, LX/Cos;->i:LX/CoV;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Cow;->e:LX/Chi;

    iget-object v3, p0, LX/Cow;->m:Ljava/lang/String;

    const v4, 0x4984e12

    const/16 v5, 0x3ed

    invoke-virtual/range {v0 .. v5}, LX/CoV;->a(Landroid/content/Context;LX/Chi;Ljava/lang/String;II)LX/CnN;

    move-result-object v0

    return-object v0
.end method
