.class public LX/Csm;
.super LX/0ht;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalComposer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/resources/ui/FbButton;

.field public n:Lcom/facebook/resources/ui/FbButton;

.field private o:I

.field private p:I

.field private q:Z

.field private final r:I

.field private final s:I

.field private final t:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1943212
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Csm;-><init>(Landroid/content/Context;I)V

    .line 1943213
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1943197
    invoke-direct {p0, p1, p2}, LX/0ht;-><init>(Landroid/content/Context;I)V

    .line 1943198
    const-class v0, LX/Csm;

    invoke-static {v0, p0, p1}, LX/Csm;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 1943199
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308ba

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1943200
    invoke-virtual {p0, v1}, LX/0ht;->d(Landroid/view/View;)V

    .line 1943201
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0ht;->c(Z)V

    .line 1943202
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0ht;->b(F)V

    .line 1943203
    const v0, 0x7f0d16a9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/Csm;->m:Lcom/facebook/resources/ui/FbButton;

    .line 1943204
    const v0, 0x7f0d16aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/Csm;->n:Lcom/facebook/resources/ui/FbButton;

    .line 1943205
    iget-object v0, p0, LX/Csm;->a:LX/0Uh;

    const/16 v1, 0xbc

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Csm;->l:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Csm;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1943206
    iget-object v0, p0, LX/Csm;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1943207
    :cond_0
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1943208
    const v1, 0x7f0b1287

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/Csm;->r:I

    .line 1943209
    const v1, 0x7f0b01d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/Csm;->s:I

    .line 1943210
    const v1, 0x7f0b01d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Csm;->t:I

    .line 1943211
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Csm;

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 p0, 0x3222

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v0, p1, LX/Csm;->a:LX/0Uh;

    iput-object v1, p1, LX/Csm;->l:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(IIZ)V
    .locals 0

    .prologue
    .line 1943193
    iput p1, p0, LX/Csm;->o:I

    .line 1943194
    iput p2, p0, LX/Csm;->p:I

    .line 1943195
    iput-boolean p3, p0, LX/Csm;->q:Z

    .line 1943196
    return-void
.end method

.method public final a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 14

    .prologue
    .line 1943143
    const/4 v1, -0x1

    move-object/from16 v0, p3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1943144
    const/4 v1, -0x1

    move-object/from16 v0, p3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1943145
    const/4 v1, 0x2

    new-array v2, v1, [I

    .line 1943146
    instance-of v1, p1, LX/CtG;

    if-eqz v1, :cond_2

    .line 1943147
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    .line 1943148
    :goto_0
    const/4 v1, 0x1

    aget v1, v2, v1

    iget v3, p0, LX/Csm;->r:I

    if-le v1, v3, :cond_3

    const/4 v1, 0x1

    .line 1943149
    :goto_1
    const/4 v3, 0x0

    aget v3, v2, v3

    iget v4, p0, LX/Csm;->o:I

    add-int/2addr v4, v3

    .line 1943150
    iget-boolean v3, p0, LX/Csm;->q:Z

    if-eqz v3, :cond_4

    .line 1943151
    const/4 v1, 0x1

    aget v1, v2, v1

    iget v2, p0, LX/Csm;->p:I

    add-int/2addr v1, v2

    move v2, v1

    .line 1943152
    :goto_2
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingBottom()I

    move-result v1

    iget v3, p0, LX/Csm;->s:I

    sub-int v5, v1, v3

    .line 1943153
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingTop()I

    move-result v1

    iget v3, p0, LX/Csm;->t:I

    sub-int v6, v1, v3

    .line 1943154
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 1943155
    iget v7, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1943156
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1943157
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v8

    .line 1943158
    iget v3, p0, LX/Csm;->r:I

    sub-int v3, v2, v3

    add-int v9, v3, v5

    .line 1943159
    sub-int/2addr v1, v2

    add-int/2addr v1, v6

    .line 1943160
    invoke-static {v9, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1943161
    const/4 v3, 0x0

    invoke-static {v7, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1943162
    const/high16 v10, -0x80000000

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1943163
    iget-object v10, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v10, v3, v1}, LX/5OY;->measure(II)V

    .line 1943164
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getMeasuredWidth()I

    move-result v10

    .line 1943165
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getMeasuredHeight()I

    move-result v11

    .line 1943166
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1943167
    div-int/lit8 v3, v10, 0x2

    sub-int v3, v4, v3

    if-lez v3, :cond_6

    div-int/lit8 v3, v10, 0x2

    sub-int v3, v4, v3

    .line 1943168
    :goto_3
    add-int v12, v3, v10

    iget-object v13, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v13}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingRight()I

    move-result v13

    add-int/2addr v13, v7

    if-le v12, v13, :cond_0

    .line 1943169
    sub-int v3, v7, v10

    iget-object v12, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v12}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingRight()I

    move-result v12

    add-int/2addr v3, v12

    .line 1943170
    :cond_0
    add-int/2addr v10, v3

    sub-int/2addr v7, v10

    .line 1943171
    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1943172
    iput v7, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 1943173
    if-ge v11, v9, :cond_7

    .line 1943174
    const v6, 0x7f0e028c

    move-object/from16 v0, p3

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1943175
    const/16 v6, 0x50

    iput v6, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move-object/from16 v0, p3

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1943176
    sub-int v2, v8, v2

    sub-int/2addr v2, v5

    move-object/from16 v0, p3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1943177
    iget-object v2, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    sget-object v5, LX/5OS;->ABOVE:LX/5OS;

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubShown(LX/5OS;)V

    .line 1943178
    :goto_4
    iget-object v2, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1943179
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    sub-int v2, v4, v3

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubOffset(I)V

    .line 1943180
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1943181
    const/4 v1, 0x0

    move-object/from16 v0, p3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1943182
    :cond_1
    return-void

    .line 1943183
    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    goto/16 :goto_0

    .line 1943184
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1943185
    :cond_4
    if-eqz v1, :cond_5

    .line 1943186
    const/4 v1, 0x1

    aget v1, v2, v1

    move v2, v1

    goto/16 :goto_2

    .line 1943187
    :cond_5
    iget v1, p0, LX/Csm;->r:I

    move v2, v1

    goto/16 :goto_2

    .line 1943188
    :cond_6
    iget-object v3, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v3}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingLeft()I

    move-result v3

    neg-int v3, v3

    goto :goto_3

    .line 1943189
    :cond_7
    const v5, 0x7f0e028b

    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1943190
    const/16 v5, 0x30

    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    move-object/from16 v0, p3

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1943191
    sub-int/2addr v2, v6

    move-object/from16 v0, p3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1943192
    iget-object v2, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    sget-object v5, LX/5OS;->BELOW:LX/5OS;

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubShown(LX/5OS;)V

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1943141
    iget-object v0, p0, LX/Csm;->m:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1943142
    return-void
.end method
