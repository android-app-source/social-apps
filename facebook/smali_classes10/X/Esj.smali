.class public LX/Esj;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLImage;

.field public final b:Landroid/widget/TextView;

.field public final c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2176380
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2176381
    const v0, 0x7f0307dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2176382
    const v0, 0x7f0d14c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Esj;->b:Landroid/widget/TextView;

    .line 2176383
    const v0, 0x7f0d0bbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Esj;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2176384
    const v0, 0x7f0d0bdf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Esj;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2176385
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2176386
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2176387
    iget-object v1, p0, LX/Esj;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    int-to-double v2, v0

    const-wide v4, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2176388
    iget-object v0, p0, LX/Esj;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->measure(II)V

    .line 2176389
    iget-object v0, p0, LX/Esj;->a:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Esj;->a:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 2176390
    invoke-virtual {p0}, LX/Esj;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, LX/Esj;->getPaddingBottom()I

    move-result v1

    invoke-virtual {p0, v6, v0, v6, v1}, LX/Esj;->setPadding(IIII)V

    .line 2176391
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 2176392
    return-void
.end method
