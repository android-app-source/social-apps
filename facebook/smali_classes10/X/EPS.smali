.class public LX/EPS;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EPQ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2116784
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EPS;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116785
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2116786
    iput-object p1, p0, LX/EPS;->b:LX/0Ot;

    .line 2116787
    return-void
.end method

.method public static a(LX/0QB;)LX/EPS;
    .locals 4

    .prologue
    .line 2116788
    const-class v1, LX/EPS;

    monitor-enter v1

    .line 2116789
    :try_start_0
    sget-object v0, LX/EPS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116790
    sput-object v2, LX/EPS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116791
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116792
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116793
    new-instance v3, LX/EPS;

    const/16 p0, 0x3494

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EPS;-><init>(LX/0Ot;)V

    .line 2116794
    move-object v0, v3

    .line 2116795
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116796
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EPS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116797
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116798
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2116799
    check-cast p2, LX/EPR;

    .line 2116800
    iget-object v0, p0, LX/EPS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentSpec;

    iget-object v1, p2, LX/EPR;->a:Landroid/net/Uri;

    iget-object v2, p2, LX/EPR;->b:Ljava/lang/CharSequence;

    const/4 p2, 0x0

    const/4 p0, 0x2

    .line 2116801
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0097

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-interface {v3, v4}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v3

    .line 2116802
    const v4, 0x6b06ce21

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2116803
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v5

    iget-object v3, v0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentSpec;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v3

    sget-object v6, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f0b1730

    invoke-interface {v3, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b1731

    invoke-interface {v3, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/16 v5, 0x8

    const v6, 0x7f0b1732

    invoke-interface {v3, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0e0120

    invoke-static {p1, p2, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b1732

    invoke-interface {v5, p0, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2116804
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2116805
    invoke-static {}, LX/1dS;->b()V

    .line 2116806
    iget v0, p1, LX/1dQ;->b:I

    .line 2116807
    packed-switch v0, :pswitch_data_0

    .line 2116808
    :goto_0
    return-object v2

    .line 2116809
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2116810
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2116811
    check-cast v1, LX/EPR;

    .line 2116812
    iget-object v3, p0, LX/EPS;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, LX/EPR;->c:LX/EPT;

    .line 2116813
    iget-object v4, v3, LX/EPT;->a:LX/CzL;

    .line 2116814
    iget-object v5, v4, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v5

    .line 2116815
    check-cast v4, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2116816
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object p2

    .line 2116817
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aK()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aK()Ljava/lang/String;

    move-result-object v5

    move-object p1, v5

    .line 2116818
    :goto_1
    iget-object v5, v3, LX/EPT;->c:Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    iget-object v5, v5, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->f:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v5, p0, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2116819
    iget-object v5, v3, LX/EPT;->c:Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    iget-object v5, v5, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2Sc;

    sget-object p0, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    const-string v1, "Shortcut (id: %s) has bad field (path: %s)"

    invoke-static {v1, p2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p0, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2116820
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->af()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->af()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .line 2116821
    :goto_2
    iget-object v4, v3, LX/EPT;->c:Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    iget-object v4, v4, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->f:LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v4, p1, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2116822
    iget-object v4, v3, LX/EPT;->c:Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    iget-object v4, v4, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2Sc;

    sget-object p1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    const-string p0, "Shortcut (id: %s) has bad field (fallback_path: %s)"

    invoke-static {p0, p2, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2116823
    :cond_0
    iget-object v4, v3, LX/EPT;->b:LX/Cxh;

    iget-object v5, v3, LX/EPT;->a:LX/CzL;

    invoke-interface {v4, v5}, LX/Cxh;->c(LX/CzL;)V

    .line 2116824
    goto :goto_0

    .line 2116825
    :cond_1
    const-string v5, ""

    move-object p1, v5

    goto :goto_1

    .line 2116826
    :cond_2
    const-string v4, ""

    move-object v5, v4

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x6b06ce21
        :pswitch_0
    .end packed-switch
.end method
