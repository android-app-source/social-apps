.class public final LX/Evt;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Evv;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Z

.field public c:LX/95R;

.field public final synthetic d:LX/Evv;


# direct methods
.method public constructor <init>(LX/Evv;)V
    .locals 1

    .prologue
    .line 2182532
    iput-object p1, p0, LX/Evt;->d:LX/Evv;

    .line 2182533
    move-object v0, p1

    .line 2182534
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2182535
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 2

    .prologue
    .line 2182536
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/Evt;

    .line 2182537
    if-nez p1, :cond_0

    .line 2182538
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Evt;->b:Z

    .line 2182539
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2182540
    if-ne p0, p1, :cond_1

    .line 2182541
    :cond_0
    :goto_0
    return v0

    .line 2182542
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2182543
    goto :goto_0

    .line 2182544
    :cond_3
    check-cast p1, LX/Evt;

    .line 2182545
    iget-boolean v2, p0, LX/Evt;->b:Z

    iget-boolean v3, p1, LX/Evt;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2182546
    goto :goto_0

    .line 2182547
    :cond_4
    iget-object v2, p0, LX/Evt;->c:LX/95R;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Evt;->c:LX/95R;

    iget-object v3, p1, LX/Evt;->c:LX/95R;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2182548
    goto :goto_0

    .line 2182549
    :cond_5
    iget-object v2, p1, LX/Evt;->c:LX/95R;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
