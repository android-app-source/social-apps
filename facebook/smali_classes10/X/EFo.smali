.class public final LX/EFo;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/EFp;


# direct methods
.method public constructor <init>(LX/EFp;)V
    .locals 0

    .prologue
    .line 2096402
    iput-object p1, p0, LX/EFo;->a:LX/EFp;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 2096403
    iget-object v0, p0, LX/EFo;->a:LX/EFp;

    iget-boolean v0, v0, LX/EFp;->n:Z

    if-eqz v0, :cond_0

    .line 2096404
    :goto_0
    return-void

    .line 2096405
    :cond_0
    iget-object v0, p0, LX/EFo;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->p:LX/0wd;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EFo;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->q:LX/0wd;

    if-eqz v0, :cond_1

    .line 2096406
    iget-object v0, p0, LX/EFo;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->p:LX/0wd;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2096407
    iget-object v0, p0, LX/EFo;->a:LX/EFp;

    iget-object v1, p0, LX/EFo;->a:LX/EFp;

    iget-object v1, v1, LX/EFp;->p:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-int v1, v2

    .line 2096408
    iput v1, v0, LX/EFp;->h:I

    .line 2096409
    :cond_1
    :goto_1
    iget-object v0, p0, LX/EFo;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->g:LX/EGb;

    invoke-virtual {v0}, LX/EGb;->f()V

    goto :goto_0

    .line 2096410
    :cond_2
    iget-object v0, p0, LX/EFo;->a:LX/EFp;

    iget-object v1, p0, LX/EFo;->a:LX/EFp;

    iget-object v1, v1, LX/EFp;->q:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-int v1, v2

    .line 2096411
    iput v1, v0, LX/EFp;->i:I

    .line 2096412
    goto :goto_1
.end method

.method public final b(LX/0wd;)V
    .locals 6

    .prologue
    .line 2096413
    iget-object v0, p0, LX/EFo;->a:LX/EFp;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2096414
    iget-object v1, v0, LX/EFp;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aX()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2096415
    iget-object v1, v0, LX/EFp;->g:LX/EGb;

    invoke-virtual {v1}, LX/EGb;->g()Landroid/graphics/Rect;

    move-result-object v1

    .line 2096416
    iget v2, v0, LX/EFp;->h:I

    iget v5, v1, Landroid/graphics/Rect;->left:I

    iget p1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, p1

    div-int/lit8 v5, v5, 0x2

    if-ge v2, v5, :cond_1

    move v2, v3

    .line 2096417
    :goto_0
    iget v5, v0, LX/EFp;->i:I

    iget p1, v1, Landroid/graphics/Rect;->top:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x2

    if-ge v5, v1, :cond_2

    .line 2096418
    :goto_1
    iget-object v1, v0, LX/EFp;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-static {v2, v3}, LX/EDt;->getCorner(ZZ)LX/EDt;

    move-result-object v2

    .line 2096419
    iput-object v2, v1, LX/EDx;->ck:LX/EDt;

    .line 2096420
    :cond_0
    iget-object v0, p0, LX/EFo;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->g:LX/EGb;

    .line 2096421
    iget-object v1, v0, LX/EGb;->a:LX/EGe;

    invoke-static {v1}, LX/EGe;->ar(LX/EGe;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2096422
    iget-object v1, v0, LX/EGb;->a:LX/EGe;

    invoke-static {v1}, LX/EGe;->an(LX/EGe;)V

    .line 2096423
    :goto_2
    return-void

    :cond_1
    move v2, v4

    .line 2096424
    goto :goto_0

    :cond_2
    move v3, v4

    .line 2096425
    goto :goto_1

    .line 2096426
    :cond_3
    invoke-virtual {v0}, LX/EGb;->k()V

    goto :goto_2
.end method
