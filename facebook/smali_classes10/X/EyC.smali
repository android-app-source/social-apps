.class public final LX/EyC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Eus;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;LX/Eus;I)V
    .locals 0

    .prologue
    .line 2185820
    iput-object p1, p0, LX/EyC;->c:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iput-object p2, p0, LX/EyC;->a:LX/Eus;

    iput p3, p0, LX/EyC;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x2

    const v1, -0x75a96b38

    invoke-static {v0, v5, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2185821
    iget-object v1, p0, LX/EyC;->a:LX/Eus;

    invoke-virtual {v1}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2185822
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v2, :cond_0

    .line 2185823
    new-instance v2, LX/0ju;

    iget-object v3, p0, LX/EyC;->c:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v3, v3, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->e:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0818b5

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    iget-object v3, p0, LX/EyC;->c:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v3, v3, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->k:Landroid/content/res/Resources;

    const v4, 0x7f0818b6

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, LX/EyC;->a:LX/Eus;

    invoke-virtual {v7}, LX/Eus;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    const v3, 0x7f080f7d

    new-instance v4, LX/EyB;

    invoke-direct {v4, p0, v1}, LX/EyB;-><init>(LX/EyC;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0818b7

    new-instance v3, LX/EyA;

    invoke-direct {v3, p0}, LX/EyA;-><init>(LX/EyC;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2185824
    :goto_0
    const v1, 0x5def69f5

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2185825
    :cond_0
    iget-object v2, p0, LX/EyC;->c:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v3, p0, LX/EyC;->a:LX/Eus;

    iget v4, p0, LX/EyC;->b:I

    invoke-static {v2, v3, v1, v4}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a$redex0(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;LX/Eus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;I)V

    goto :goto_0
.end method
