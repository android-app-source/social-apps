.class public final LX/Cye;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zT;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1953369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1953370
    instance-of v0, p0, Ljava/util/TreeMap;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Ljava/util/TreeMap;

    const-string v1, "filtered_query_arguments"

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/TreeMap;

    const-string v0, "bqf"

    invoke-virtual {p0, v0}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0zO;LX/0w5;LX/0t2;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO;",
            "LX/0w5",
            "<*>;",
            "LX/0t2;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1953371
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 1953372
    invoke-virtual {p1}, LX/0zO;->d()LX/0w7;

    move-result-object v1

    .line 1953373
    invoke-static {}, LX/1l6;->a()LX/1l6;

    move-result-object v2

    .line 1953374
    invoke-virtual {v1}, LX/0w7;->e()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v4, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1953375
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 1953376
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    .line 1953377
    invoke-static {v2}, LX/Cye;->a(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1953378
    :try_start_0
    move-object v5, v2

    check-cast v5, Ljava/util/TreeMap;

    move-object v6, v5

    invoke-virtual {v6}, Ljava/util/TreeMap;->clone()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/TreeMap;

    .line 1953379
    const-string v7, "filtered_query_arguments"

    invoke-virtual {v6, v7}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/TreeMap;

    invoke-virtual {v7}, Ljava/util/TreeMap;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/TreeMap;

    .line 1953380
    const-string p0, "tsid"

    const-string p1, ""

    invoke-virtual {v7, p0, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953381
    const-string p0, "bsid"

    const-string p1, ""

    invoke-virtual {v7, p0, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953382
    const-string p0, "query_source"

    const-string p1, ""

    invoke-virtual {v7, p0, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953383
    const-string p0, "filtered_query_arguments"

    invoke-virtual {v6, p0, v7}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953384
    invoke-virtual {v4, v6}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1953385
    :goto_1
    move-object v2, v4

    .line 1953386
    move-object v4, v2

    .line 1953387
    goto :goto_0

    .line 1953388
    :cond_0
    invoke-virtual {v1}, LX/0w7;->b()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1953389
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 1953390
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/4a1;

    iget-object v3, v3, LX/4a1;->a:LX/0zO;

    invoke-virtual {v3}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 1953391
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/4a1;

    iget-object v3, v3, LX/4a1;->b:Ljava/lang/String;

    invoke-virtual {v4, v3}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 1953392
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4a1;

    iget-object v2, v2, LX/4a1;->c:LX/4Zz;

    invoke-virtual {v2}, LX/4Zz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto :goto_2

    .line 1953393
    :cond_1
    invoke-virtual {v4}, LX/1l6;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1953394
    invoke-virtual {p3, v0, p2, v1}, LX/0t2;->a(LX/0gW;LX/0w5;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1953395
    :cond_2
    invoke-virtual {v4, v2}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto :goto_1

    .line 1953396
    :catch_0
    invoke-virtual {v4, v2}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    goto :goto_1
.end method
