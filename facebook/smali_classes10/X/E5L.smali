.class public LX/E5L;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E5J;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E5L;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5M;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078282
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5L;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5M;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078259
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078260
    iput-object p1, p0, LX/E5L;->b:LX/0Ot;

    .line 2078261
    return-void
.end method

.method public static a(LX/0QB;)LX/E5L;
    .locals 4

    .prologue
    .line 2078269
    sget-object v0, LX/E5L;->c:LX/E5L;

    if-nez v0, :cond_1

    .line 2078270
    const-class v1, LX/E5L;

    monitor-enter v1

    .line 2078271
    :try_start_0
    sget-object v0, LX/E5L;->c:LX/E5L;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078272
    if-eqz v2, :cond_0

    .line 2078273
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078274
    new-instance v3, LX/E5L;

    const/16 p0, 0x3125

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E5L;-><init>(LX/0Ot;)V

    .line 2078275
    move-object v0, v3

    .line 2078276
    sput-object v0, LX/E5L;->c:LX/E5L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078277
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078278
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078279
    :cond_1
    sget-object v0, LX/E5L;->c:LX/E5L;

    return-object v0

    .line 2078280
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078281
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2078264
    check-cast p2, LX/E5K;

    .line 2078265
    iget-object v0, p0, LX/E5L;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E5M;

    iget-object v1, p2, LX/E5K;->a:Ljava/lang/String;

    iget-object v2, p2, LX/E5K;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    iget-object v3, p2, LX/E5K;->c:Ljava/lang/String;

    const/4 p2, 0x1

    .line 2078266
    iget-object v4, v0, LX/E5M;->a:LX/0SG;

    invoke-static {v4, p1, v3, v1, v2}, LX/E6H;->a(LX/0SG;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    .line 2078267
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    const/16 v6, 0x8

    const p0, 0x7f0b163a

    invoke-interface {v5, v6, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    const p0, 0x7f0b0050

    invoke-virtual {v6, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const p0, 0x7f0a010c

    invoke-virtual {v6, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;->e()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    const p0, 0x7f0b004e

    invoke-virtual {v6, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const p0, 0x7f0a010e

    invoke-virtual {v6, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    const/high16 p0, 0x3fa00000    # 1.25f

    invoke-virtual {v6, p0}, LX/1ne;->j(F)LX/1ne;

    move-result-object v6

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    const/4 p0, 0x2

    invoke-virtual {v6, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v6, 0x4

    invoke-interface {v4, p2, v6}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    const/16 v6, 0x20

    invoke-interface {v4, v6}, LX/1Di;->r(I)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2078268
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2078262
    invoke-static {}, LX/1dS;->b()V

    .line 2078263
    const/4 v0, 0x0

    return-object v0
.end method
