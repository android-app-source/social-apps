.class public LX/CyY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/CyY;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Aw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0se;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8iF;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0sa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0sU;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/8ht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0tQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1953295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1953296
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953297
    iput-object v0, p0, LX/CyY;->a:LX/0Ot;

    .line 1953298
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953299
    iput-object v0, p0, LX/CyY;->b:LX/0Ot;

    .line 1953300
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953301
    iput-object v0, p0, LX/CyY;->c:LX/0Ot;

    .line 1953302
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953303
    iput-object v0, p0, LX/CyY;->d:LX/0Ot;

    .line 1953304
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953305
    iput-object v0, p0, LX/CyY;->e:LX/0Ot;

    .line 1953306
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953307
    iput-object v0, p0, LX/CyY;->f:LX/0Ot;

    .line 1953308
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953309
    iput-object v0, p0, LX/CyY;->g:LX/0Ot;

    .line 1953310
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953311
    iput-object v0, p0, LX/CyY;->h:LX/0Ot;

    .line 1953312
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953313
    iput-object v0, p0, LX/CyY;->i:LX/0Ot;

    .line 1953314
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953315
    iput-object v0, p0, LX/CyY;->j:LX/0Ot;

    .line 1953316
    return-void
.end method

.method private static a(I)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Px",
            "<",
            "LX/0m9;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1953290
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1953291
    invoke-static {}, LX/CyY;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1953292
    new-instance v5, LX/0m9;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v6}, LX/0m9;-><init>(LX/0mC;)V

    const-string v6, "role"

    invoke-virtual {v5, v6, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    const-string v5, "type"

    const-string v6, "photo"

    invoke-virtual {v0, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    const-string v5, "size"

    invoke-virtual {v0, v5, p0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953293
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1953294
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1953282
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1953283
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4FP;

    .line 1953284
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v4

    .line 1953285
    const-string v0, "add"

    const-string v5, "action"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1953286
    new-instance v0, LX/4FP;

    invoke-direct {v0}, LX/4FP;-><init>()V

    const-string v5, "add"

    invoke-virtual {v0, v5}, LX/4FP;->c(Ljava/lang/String;)LX/4FP;

    move-result-object v5

    const-string v0, "handle"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, LX/4FP;->b(Ljava/lang/String;)LX/4FP;

    move-result-object v5

    const-string v0, "name"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, LX/4FP;->a(Ljava/lang/String;)LX/4FP;

    move-result-object v5

    const-string v0, "value"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, LX/4FP;->d(Ljava/lang/String;)LX/4FP;

    move-result-object v0

    .line 1953287
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953288
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1953289
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/CwB;LX/8ci;Z)LX/0Px;
    .locals 7
    .param p2    # LX/8ci;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "LX/8ci;",
            "Z)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1953248
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 1953249
    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->J:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1953250
    sget-object v0, LX/7C9;->I18N_POST_SEARCH_EXPANSION:LX/7C9;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953251
    :cond_0
    :goto_0
    iget-object v0, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v2, LX/2SU;->w:I

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1953252
    sget-object v0, LX/7C9;->ATTACHED_HEADERS:LX/7C9;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953253
    :cond_1
    if-nez p3, :cond_2

    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v2, LX/2SU;->C:I

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1953254
    :cond_2
    sget-object v0, LX/7C9;->FAST_FILTERS:LX/7C9;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953255
    sget-object v0, LX/7C9;->FILTERS:LX/7C9;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953256
    :cond_3
    invoke-interface {p1}, LX/CwB;->jC_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-interface {p1}, LX/CwB;->jC_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    .line 1953257
    :cond_4
    iget-object v0, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v2, LX/2SU;->C:I

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1953258
    sget-object v0, LX/7C9;->FILTERS_AS_SEE_MORE:LX/7C9;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953259
    :cond_5
    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->bN:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1953260
    sget-object v0, LX/7C9;->KEYWORD_ONLY:LX/7C9;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953261
    :cond_6
    iget-object v0, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v2, LX/2SU;->W:I

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1953262
    sget-object v0, LX/7C9;->COMMERCE_GROUPS_SEARCH:LX/7C9;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953263
    :cond_7
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 1953264
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 1953265
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7C9;

    .line 1953266
    iget-object v0, v0, LX/7C9;->serverValue:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_1

    .line 1953267
    :cond_8
    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->L:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1953268
    sget-object v0, LX/7C9;->I18N_POST_SEARCH_USER_EXPANSION:LX/7C9;

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto/16 :goto_0

    .line 1953269
    :cond_9
    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v2, LX/100;->bF:C

    const-string v3, ""

    invoke-interface {v0, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1953270
    if-eqz v0, :cond_a

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1953271
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1953272
    array-length v5, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_a

    aget-object v6, v3, v2

    .line 1953273
    invoke-virtual {v1, v6}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953274
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1953275
    :cond_a
    if-eqz p3, :cond_b

    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->E:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1953276
    const-string v0, "PEOPLE_FILTERS_V2"

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953277
    :cond_b
    invoke-direct {p0, p3, p1}, LX/CyY;->a(ZLX/CwB;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1953278
    const-string v0, "NATIVE_TEMPLATES"

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953279
    :cond_c
    invoke-static {p0, p1, p2}, LX/CyY;->b(LX/CyY;LX/CwB;LX/8ci;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1953280
    const-string v0, "DENSE_RESULT_PAGE"

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1953281
    :cond_d
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/CwB;Z)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1953228
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1953229
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CENTRAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v6

    const/4 v2, 0x3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES_WITH_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v3, v0, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPELLER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v3, v0, v2

    const/4 v2, 0x5

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_MIXED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v3, v0, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WEATHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v3, v0, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WIKIPEDIA_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v3, v0, v2

    invoke-virtual {v1, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 1953230
    if-eqz p2, :cond_0

    .line 1953231
    new-array v0, v6, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPELLER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v2, v0, v5

    invoke-virtual {v1, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 1953232
    :cond_0
    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1953233
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953234
    :cond_1
    :goto_0
    iget-object v0, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v2, LX/2SU;->C:I

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1953235
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953236
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953237
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NAVIGATIONAL_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953238
    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->aO:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1953239
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953240
    :cond_3
    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->ag:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1953241
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953242
    :cond_4
    iget-object v0, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v2, LX/2SU;->i:I

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1953243
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ELECTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953244
    :cond_5
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1953245
    :cond_6
    iget-object v0, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v2, LX/2SU;->s:I

    invoke-virtual {v0, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1953246
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953247
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/CyY;
    .locals 15

    .prologue
    .line 1953213
    sget-object v0, LX/CyY;->m:LX/CyY;

    if-nez v0, :cond_1

    .line 1953214
    const-class v1, LX/CyY;

    monitor-enter v1

    .line 1953215
    :try_start_0
    sget-object v0, LX/CyY;->m:LX/CyY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1953216
    if-eqz v2, :cond_0

    .line 1953217
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1953218
    new-instance v3, LX/CyY;

    invoke-direct {v3}, LX/CyY;-><init>()V

    .line 1953219
    const/16 v4, 0xac0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1032

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32aa

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xf27

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x351a

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1223

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1b

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x122

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x130c

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xf9a

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {v0}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v14

    check-cast v14, LX/8ht;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object p0

    check-cast p0, LX/0tQ;

    .line 1953220
    iput-object v4, v3, LX/CyY;->a:LX/0Ot;

    iput-object v5, v3, LX/CyY;->b:LX/0Ot;

    iput-object v6, v3, LX/CyY;->c:LX/0Ot;

    iput-object v7, v3, LX/CyY;->d:LX/0Ot;

    iput-object v8, v3, LX/CyY;->e:LX/0Ot;

    iput-object v9, v3, LX/CyY;->f:LX/0Ot;

    iput-object v10, v3, LX/CyY;->g:LX/0Ot;

    iput-object v11, v3, LX/CyY;->h:LX/0Ot;

    iput-object v12, v3, LX/CyY;->i:LX/0Ot;

    iput-object v13, v3, LX/CyY;->j:LX/0Ot;

    iput-object v14, v3, LX/CyY;->k:LX/8ht;

    iput-object p0, v3, LX/CyY;->l:LX/0tQ;

    .line 1953221
    move-object v0, v3

    .line 1953222
    sput-object v0, LX/CyY;->m:LX/CyY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1953223
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1953224
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1953225
    :cond_1
    sget-object v0, LX/CyY;->m:LX/CyY;

    return-object v0

    .line 1953226
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1953227
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/4FP;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1953200
    new-instance v0, LX/0lp;

    invoke-direct {v0}, LX/0lp;-><init>()V

    .line 1953201
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 1953202
    :try_start_0
    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v0

    .line 1953203
    invoke-virtual {p0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v2

    .line 1953204
    invoke-virtual {v0}, LX/0nX;->f()V

    .line 1953205
    const-string v3, "name"

    const-string v4, "name"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1953206
    const-string v3, "action"

    const-string v4, "action"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1953207
    const-string v3, "value"

    const-string v4, "value"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1953208
    invoke-virtual {v0}, LX/0nX;->g()V

    .line 1953209
    invoke-virtual {v0}, LX/0nX;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1953210
    invoke-virtual {v1}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1953211
    :catch_0
    move-exception v0

    .line 1953212
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to convert filters to json string"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(LX/8ed;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V
    .locals 4

    .prologue
    .line 1953197
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_STATE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq p2, v0, :cond_0

    .line 1953198
    :goto_0
    return-void

    .line 1953199
    :cond_0
    const-string v1, "video_thumbnail_width"

    iget-object v0, p0, LX/CyY;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v2, 0x7f0b0633

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "video_thumbnail_height"

    iget-object v0, p0, LX/CyY;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v3, 0x7f0b0634

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    goto :goto_0
.end method

.method private a(LX/CwB;LX/8ci;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1953196
    invoke-static {p0, p1, p2}, LX/CyY;->b(LX/CyY;LX/CwB;LX/8ci;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/100;->ah:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private a(ZLX/CwB;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1953049
    iget-object v0, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v3, LX/2SU;->B:I

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    xor-int/2addr v0, p1

    if-nez v0, :cond_1

    move v0, v1

    .line 1953050
    :goto_0
    if-eqz v0, :cond_2

    invoke-interface {p2}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7BG;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7BG;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, LX/CyY;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/3Qm;->k:LX/0Tn;

    invoke-interface {v0, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 1953051
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1953052
    goto :goto_1
.end method

.method private static b()I
    .locals 2

    .prologue
    .line 1953195
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    sget-object v1, LX/0wC;->NUMBER_4:LX/0wC;

    invoke-virtual {v0, v1}, LX/0wC;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x60

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x30

    goto :goto_0
.end method

.method private static b(LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1953190
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1953191
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4FP;

    .line 1953192
    invoke-static {v0}, LX/CyY;->a(LX/4FP;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953193
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1953194
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/CyY;LX/CwB;LX/8ci;)Z
    .locals 2

    .prologue
    .line 1953186
    invoke-interface {p1}, LX/CwB;->jC_()LX/0Px;

    move-result-object v0

    .line 1953187
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1953188
    :goto_0
    iget-object v1, p0, LX/CyY;->k:LX/8ht;

    invoke-virtual {v1, v0, p2}, LX/8ht;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;LX/8ci;)Z

    move-result v0

    return v0

    .line 1953189
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0
.end method

.method private static c()LX/0wC;
    .locals 2

    .prologue
    .line 1953185
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    sget-object v1, LX/0wC;->NUMBER_4:LX/0wC;

    invoke-virtual {v0, v1}, LX/0wC;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/0wC;->NUMBER_2:LX/0wC;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    goto :goto_0
.end method

.method private static d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1953184
    const-string v0, "GRAMMAR"

    const-string v1, "NONE"

    const-string v2, "PROMOTED_ENTITY_MEDIA"

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static e()LX/0Px;
    .locals 7

    .prologue
    .line 1953170
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1953171
    invoke-static {}, LX/CyY;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1953172
    new-instance v5, LX/4FS;

    invoke-direct {v5}, LX/4FS;-><init>()V

    .line 1953173
    const-string v6, "role"

    invoke-virtual {v5, v6, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1953174
    move-object v0, v5

    .line 1953175
    const-string v5, "photo"

    .line 1953176
    const-string v6, "type"

    invoke-virtual {v0, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1953177
    move-object v0, v0

    .line 1953178
    const/16 v5, 0x9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 1953179
    const-string v6, "size"

    invoke-virtual {v0, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1953180
    move-object v0, v0

    .line 1953181
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953182
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1953183
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/CwB;LX/0gW;ZLjava/lang/String;LX/0Px;LX/0Px;Ljava/lang/Integer;ZZLjava/lang/String;Ljava/lang/String;LX/8ci;)V
    .locals 13
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # LX/8ci;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "LX/0gW;",
            "Z",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "Ljava/lang/Integer;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8ci;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1953057
    move-object/from16 v0, p12

    invoke-direct {p0, p1, v0}, LX/CyY;->a(LX/CwB;LX/8ci;)Z

    move-result v6

    .line 1953058
    if-eqz v6, :cond_13

    iget-object v2, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/100;->ai:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    move v5, v2

    .line 1953059
    :goto_0
    const-string v7, "filters_enabled"

    if-eqz p3, :cond_0

    iget-object v2, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    iget-object v3, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    move-object v4, p1

    check-cast v4, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/8hw;->a(LX/0Uh;LX/0ad;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    if-nez p3, :cond_14

    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    :cond_1
    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p2, v7, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1953060
    const-string v3, "see_more_filters_enabled"

    iget-object v2, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    sget v4, LX/2SU;->C:I

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v7}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1953061
    const-string v3, "use_video_creation_story_only"

    iget-object v2, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {v2}, LX/7Ax;->c(LX/0ad;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1953062
    const-string v3, "use_light_video_fragment"

    iget-object v2, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {v2}, LX/7Ax;->b(LX/0ad;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1953063
    iget-object v2, p0, LX/CyY;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0se;

    invoke-virtual {v2, p2}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 1953064
    const-string v3, "cover_photo_height"

    iget-object v2, p0, LX/CyY;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8iF;

    invoke-virtual {v2}, LX/8iF;->f()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1953065
    const-string v3, "profile_image_size"

    iget-object v2, p0, LX/CyY;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8iF;

    invoke-virtual {v2}, LX/8iF;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1953066
    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v7

    .line 1953067
    const/4 v2, 0x0

    .line 1953068
    if-eqz p3, :cond_32

    move-object v2, p1

    .line 1953069
    check-cast v2, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    move-object v4, v2

    .line 1953070
    :goto_2
    new-instance v8, LX/4FT;

    invoke-direct {v8}, LX/4FT;-><init>()V

    .line 1953071
    new-instance v9, LX/4FQ;

    invoke-direct {v9}, LX/4FQ;-><init>()V

    .line 1953072
    if-eqz p3, :cond_15

    .line 1953073
    invoke-static {v4}, LX/CyW;->fromDisplayStyle(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/CyW;

    move-result-object v2

    .line 1953074
    invoke-virtual {v2}, LX/CyW;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/4FQ;->b(Ljava/lang/String;)LX/4FQ;

    .line 1953075
    :cond_2
    :goto_3
    if-eqz p3, :cond_17

    .line 1953076
    move-object/from16 v0, p10

    invoke-virtual {v9, v0}, LX/4FQ;->c(Ljava/lang/String;)LX/4FQ;

    .line 1953077
    :goto_4
    invoke-static/range {p11 .. p11}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1953078
    if-eqz p3, :cond_18

    .line 1953079
    move-object/from16 v0, p11

    invoke-virtual {v9, v0}, LX/4FQ;->d(Ljava/lang/String;)LX/4FQ;

    .line 1953080
    :cond_3
    :goto_5
    if-eqz p3, :cond_19

    .line 1953081
    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, LX/CyY;->a(LX/CwB;Z)LX/0Px;

    move-result-object v2

    new-instance v3, LX/CyX;

    invoke-direct {v3, p0}, LX/CyX;-><init>(LX/CyY;)V

    invoke-static {v2, v3}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/4FQ;->d(Ljava/util/List;)LX/4FQ;

    .line 1953082
    :goto_6
    if-eqz p3, :cond_1a

    .line 1953083
    const/4 v2, 0x1

    move-object/from16 v0, p12

    invoke-direct {p0, p1, v0, v2}, LX/CyY;->a(LX/CwB;LX/8ci;Z)LX/0Px;

    move-result-object v2

    .line 1953084
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1953085
    invoke-virtual {v9, v2}, LX/4FQ;->b(Ljava/util/List;)LX/4FQ;

    .line 1953086
    :cond_4
    :goto_7
    if-eqz p3, :cond_1b

    .line 1953087
    invoke-virtual {v8, v7}, LX/4FT;->a(Ljava/lang/String;)LX/4FT;

    .line 1953088
    :goto_8
    if-eqz p6, :cond_5

    invoke-virtual/range {p6 .. p6}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1953089
    if-eqz p3, :cond_1c

    .line 1953090
    invoke-static/range {p6 .. p6}, LX/CyY;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/4FQ;->a(Ljava/util/List;)LX/4FQ;

    .line 1953091
    :cond_5
    :goto_9
    if-eqz p3, :cond_1e

    .line 1953092
    invoke-static/range {p8 .. p8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/4FQ;->a(Ljava/lang/Boolean;)LX/4FQ;

    move-result-object v3

    if-nez p8, :cond_1d

    const/4 v2, 0x1

    :goto_a
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/4FQ;->b(Ljava/lang/Boolean;)LX/4FQ;

    move-result-object v2

    invoke-static/range {p9 .. p9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4FQ;->c(Ljava/lang/Boolean;)LX/4FQ;

    .line 1953093
    :goto_b
    if-eqz p7, :cond_6

    invoke-virtual/range {p7 .. p7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_6

    .line 1953094
    if-eqz p3, :cond_20

    .line 1953095
    const-string v2, "modules_count"

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1953096
    :cond_6
    :goto_c
    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/7BG;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1953097
    iget-object v2, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-char v3, LX/100;->bI:C

    const-string v10, ""

    invoke-interface {v2, v3, v10}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1953098
    iget-object v2, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-char v10, LX/100;->bJ:C

    const-string v11, ""

    invoke-interface {v2, v10, v11}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1953099
    const-string v10, "always"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "filter_by_serp_source"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_22

    if-eqz p12, :cond_22

    invoke-virtual/range {p12 .. p12}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1953100
    :cond_7
    if-eqz p3, :cond_21

    .line 1953101
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/4FQ;->d(Ljava/lang/Boolean;)LX/4FQ;

    .line 1953102
    :cond_8
    :goto_d
    move/from16 v0, p3

    invoke-direct {p0, v0, p1}, LX/CyY;->a(ZLX/CwB;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1953103
    new-instance v2, LX/0zR;

    invoke-direct {v2}, LX/0zR;-><init>()V

    const-string v3, "701268200040015"

    invoke-virtual {v2, v3}, LX/0zR;->a(Ljava/lang/String;)LX/0zR;

    move-result-object v2

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v3}, LX/0wC;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0zR;->a(Ljava/lang/Double;)LX/0zR;

    move-result-object v3

    .line 1953104
    iget-object v2, p0, LX/CyY;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v10, LX/3Qm;->n:LX/0Tn;

    const/4 v11, 0x0

    invoke-interface {v2, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 1953105
    if-eqz p3, :cond_25

    .line 1953106
    invoke-virtual {v9, v3}, LX/4FQ;->a(LX/0zR;)LX/4FQ;

    .line 1953107
    const-string v3, "should_fetch_debug_info"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1953108
    :cond_9
    :goto_e
    if-eqz p3, :cond_26

    .line 1953109
    invoke-static {}, LX/CyY;->e()LX/0Px;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/4FQ;->c(Ljava/util/List;)LX/4FQ;

    .line 1953110
    :goto_f
    invoke-interface {p1}, LX/CwB;->e()Ljava/lang/Boolean;

    move-result-object v2

    .line 1953111
    if-eqz p3, :cond_27

    .line 1953112
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v3, v2, :cond_a

    .line 1953113
    const-string v2, "exact"

    invoke-virtual {v9, v2}, LX/4FQ;->a(Ljava/lang/String;)LX/4FQ;

    .line 1953114
    :cond_a
    :goto_10
    iget-object v2, p0, LX/CyY;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0tI;

    invoke-virtual {v2, p2}, LX/0tI;->a(LX/0gW;)V

    .line 1953115
    iget-object v2, p0, LX/CyY;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0sU;

    const-string v3, "SEARCH"

    invoke-virtual {v2, p2, v3}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 1953116
    if-eqz p3, :cond_28

    .line 1953117
    if-eqz p12, :cond_b

    .line 1953118
    invoke-virtual/range {p12 .. p12}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/4FQ;->e(Ljava/lang/String;)LX/4FQ;

    .line 1953119
    :cond_b
    :goto_11
    if-eqz p3, :cond_2a

    move-object v2, p2

    .line 1953120
    check-cast v2, LX/8ed;

    invoke-direct {p0, v2, v4}, LX/CyY;->a(LX/8ed;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    .line 1953121
    invoke-virtual {v8, v9}, LX/4FT;->a(LX/4FQ;)LX/4FT;

    .line 1953122
    const-string v2, "search_query"

    invoke-virtual {p2, v2, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1953123
    if-eqz p4, :cond_c

    .line 1953124
    const-string v2, "end_cursor"

    move-object/from16 v0, p4

    invoke-virtual {p2, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1953125
    :cond_c
    :goto_12
    const-string v3, "facepile_image_size"

    iget-object v2, p0, LX/CyY;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8iF;

    invoke-virtual {v2}, LX/8iF;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "facepile_count"

    invoke-static {v7}, LX/7BG;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2c

    const/16 v2, 0x9

    :goto_13
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v3

    const-string v4, "image_large_aspect_height"

    if-eqz v6, :cond_2d

    iget-object v2, p0, LX/CyY;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const v8, 0x7f0b16ea

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    :goto_14
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "image_large_aspect_width"

    if-eqz v6, :cond_2e

    iget-object v2, p0, LX/CyY;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const v8, 0x7f0b16ea

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    :goto_15
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "articles_thumbnail_size"

    iget-object v2, p0, LX/CyY;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8iF;

    invoke-virtual {v2}, LX/8iF;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "articles_favicon_size"

    iget-object v2, p0, LX/CyY;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8iF;

    invoke-virtual {v2}, LX/8iF;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "articles_favicon_enabled"

    iget-object v2, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-object v8, LX/0c0;->Cached:LX/0c0;

    sget-object v10, LX/0c1;->Off:LX/0c1;

    sget-short v11, LX/100;->W:S

    const/4 v12, 0x0

    invoke-interface {v2, v8, v10, v11, v12}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v2

    const-string v3, "emotional_icon_scale"

    invoke-static {}, LX/CyY;->c()LX/0wC;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "emotional_icon_size"

    invoke-static {}, LX/CyY;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "page_cta_enabled"

    iget-object v2, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v8, LX/100;->bq:S

    const/4 v10, 0x0

    invoke-interface {v2, v8, v10}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_d

    iget-object v2, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v8, LX/100;->bs:S

    const/4 v10, 0x0

    invoke-interface {v2, v8, v10}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2f

    :cond_d
    const/4 v2, 0x1

    :goto_16
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v2

    const-string v3, "feed_story_render_location"

    const-string v4, "search_results_page"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "load_redundant_fields"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v2

    const-string v3, "remove_attachment_feedback"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "disable_story_menu_actions"

    iget-object v2, p0, LX/CyY;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    sget v8, LX/2SU;->h:I

    const/4 v10, 0x0

    invoke-virtual {v2, v8, v10}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p12

    invoke-static {p0, p1, v0}, LX/CyY;->b(LX/CyY;LX/CwB;LX/8ci;)Z

    move-result v2

    if-eqz v2, :cond_30

    :cond_e
    const/4 v2, 0x1

    :goto_17
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v2

    const-string v3, "enable_download"

    iget-object v4, p0, LX/CyY;->l:LX/0tQ;

    invoke-virtual {v4}, LX/0tQ;->c()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "product_item_image_size"

    iget-object v2, p0, LX/CyY;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const v8, 0x7f0b16f3

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "scrubbing"

    const-string v4, "MPEG_DASH"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1953126
    if-eqz p5, :cond_f

    .line 1953127
    if-eqz p3, :cond_31

    .line 1953128
    move-object/from16 v0, p5

    invoke-virtual {v9, v0}, LX/4FQ;->e(Ljava/util/List;)LX/4FQ;

    .line 1953129
    :cond_f
    :goto_18
    if-eqz v6, :cond_10

    .line 1953130
    const-string v2, "dense_story_fetch_enabled"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1953131
    :cond_10
    if-eqz v5, :cond_11

    .line 1953132
    const-string v2, "dense_story_snippets_enabled"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1953133
    :cond_11
    invoke-static {v7}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1953134
    const-string v3, "product_seller_profile_photo_size"

    iget-object v2, p0, LX/CyY;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const v4, 0x7f0b1703

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1953135
    :cond_12
    return-void

    .line 1953136
    :cond_13
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_0

    .line 1953137
    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1953138
    :cond_15
    invoke-static {v7}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1953139
    const-string v2, "callsite"

    sget-object v3, LX/CyW;->COMMERCE_SEARCH:LX/CyW;

    invoke-virtual {v3}, LX/CyW;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1953140
    :cond_16
    invoke-static {v7}, LX/7BG;->e(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1953141
    const-string v2, "callsite"

    sget-object v3, LX/CyW;->COMMERCE_FOR_SALE_GROUP_SCOPED_SEARCH:LX/CyW;

    invoke-virtual {v3}, LX/CyW;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto/16 :goto_3

    .line 1953142
    :cond_17
    const-string v2, "tsid"

    move-object/from16 v0, p10

    invoke-virtual {p2, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto/16 :goto_4

    .line 1953143
    :cond_18
    const-string v2, "bsid"

    move-object/from16 v0, p11

    invoke-virtual {p2, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto/16 :goto_5

    .line 1953144
    :cond_19
    const-string v2, "supported_roles"

    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, LX/CyY;->a(LX/CwB;Z)LX/0Px;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    goto/16 :goto_6

    .line 1953145
    :cond_1a
    const-string v2, "supported_experiences"

    const/4 v3, 0x0

    move-object/from16 v0, p12

    invoke-direct {p0, p1, v0, v3}, LX/CyY;->a(LX/CwB;LX/8ci;Z)LX/0Px;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    goto/16 :goto_7

    .line 1953146
    :cond_1b
    const-string v2, "query"

    invoke-virtual {p2, v2, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto/16 :goto_8

    .line 1953147
    :cond_1c
    const-string v2, "filters"

    invoke-static/range {p6 .. p6}, LX/CyY;->b(LX/0Px;)LX/0Px;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    goto/16 :goto_9

    .line 1953148
    :cond_1d
    const/4 v2, 0x0

    goto/16 :goto_a

    .line 1953149
    :cond_1e
    const-string v2, "top_modules_only"

    invoke-static/range {p8 .. p8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v10, "top_modules_already_shown"

    if-nez p8, :cond_1f

    const/4 v2, 0x1

    :goto_19
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v10, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v2

    const-string v3, "first_unit_only"

    invoke-static/range {p9 .. p9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v2, v3, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    goto/16 :goto_b

    :cond_1f
    const/4 v2, 0x0

    goto :goto_19

    .line 1953150
    :cond_20
    const-string v2, "modules_count"

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto/16 :goto_c

    .line 1953151
    :cond_21
    const-string v2, "independent_module_or_first_unit"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    goto/16 :goto_d

    .line 1953152
    :cond_22
    instance-of v2, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v2, :cond_8

    move-object v2, p1

    .line 1953153
    check-cast v2, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 1953154
    const-string v10, "on_entity_suggestion_converted"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->r()Ljava/lang/String;

    move-result-object v10

    iget-object v3, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    sget-char v11, LX/100;->bK:C

    const-string v12, ""

    invoke-interface {v3, v11, v12}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v10, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_23

    invoke-virtual {v2}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->u()LX/CwI;

    move-result-object v3

    sget-object v10, LX/CwI;->ENTITY_REMOTE:LX/CwI;

    if-eq v3, v10, :cond_23

    invoke-virtual {v2}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->u()LX/CwI;

    move-result-object v2

    sget-object v3, LX/CwI;->ENTITY_BOOTSTRAP:LX/CwI;

    if-ne v2, v3, :cond_8

    .line 1953155
    :cond_23
    if-eqz p3, :cond_24

    .line 1953156
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v9, v2}, LX/4FQ;->d(Ljava/lang/Boolean;)LX/4FQ;

    goto/16 :goto_d

    .line 1953157
    :cond_24
    const-string v2, "independent_module_or_first_unit"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    goto/16 :goto_d

    .line 1953158
    :cond_25
    const-string v10, "nt_context"

    invoke-virtual {p2, v10, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1953159
    const-string v3, "should_fetch_debug_info"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    goto/16 :goto_e

    .line 1953160
    :cond_26
    const-string v2, "module_sizes"

    const/16 v3, 0x9

    invoke-static {v3}, LX/CyY;->a(I)LX/0Px;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    goto/16 :goto_f

    .line 1953161
    :cond_27
    const-string v3, "exact_match"

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    goto/16 :goto_10

    .line 1953162
    :cond_28
    const-string v3, "query_source"

    if-eqz p12, :cond_29

    invoke-virtual/range {p12 .. p12}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1a
    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto/16 :goto_11

    :cond_29
    const/4 v2, 0x0

    goto :goto_1a

    .line 1953163
    :cond_2a
    invoke-interface {p1}, LX/CwB;->h()LX/0P1;

    move-result-object v2

    .line 1953164
    sget-object v3, LX/7B4;->GROUP_COMMERCE:LX/7B4;

    invoke-virtual {v3}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;

    .line 1953165
    if-eqz v2, :cond_2b

    invoke-virtual {v2}, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->b()Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 1953166
    const-string v3, "for_sale"

    invoke-virtual {v2}, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1953167
    :cond_2b
    const-string v2, "central_photo_blur_radius"

    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    goto/16 :goto_12

    .line 1953168
    :cond_2c
    const/4 v2, 0x4

    goto/16 :goto_13

    :cond_2d
    iget-object v2, p0, LX/CyY;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-virtual {v2}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/16 :goto_14

    :cond_2e
    iget-object v2, p0, LX/CyY;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-virtual {v2}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/16 :goto_15

    :cond_2f
    const/4 v2, 0x0

    goto/16 :goto_16

    :cond_30
    const/4 v2, 0x0

    goto/16 :goto_17

    .line 1953169
    :cond_31
    const-string v2, "preloaded_story_ids"

    move-object/from16 v0, p5

    invoke-virtual {p2, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    goto/16 :goto_18

    :cond_32
    move-object v4, v2

    goto/16 :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 1953055
    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/100;->cc:F

    const/high16 v2, -0x40800000    # -1.0f

    invoke-interface {v0, v1, v2}, LX/0ad;->a(FF)F

    move-result v0

    .line 1953056
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(III)Z
    .locals 3

    .prologue
    .line 1953053
    iget-object v0, p0, LX/CyY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/100;->cc:F

    const/high16 v2, -0x40800000    # -1.0f

    invoke-interface {v0, v1, v2}, LX/0ad;->a(FF)F

    move-result v0

    .line 1953054
    int-to-float v1, p1

    int-to-float v2, p2

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    add-int/lit8 v1, p3, -0x1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
