.class public LX/E53;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/E53;


# instance fields
.field public final a:LX/E4m;


# direct methods
.method public constructor <init>(LX/E4m;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2077814
    iput-object p1, p0, LX/E53;->a:LX/E4m;

    .line 2077815
    return-void
.end method

.method public static a(LX/0QB;)LX/E53;
    .locals 4

    .prologue
    .line 2077816
    sget-object v0, LX/E53;->b:LX/E53;

    if-nez v0, :cond_1

    .line 2077817
    const-class v1, LX/E53;

    monitor-enter v1

    .line 2077818
    :try_start_0
    sget-object v0, LX/E53;->b:LX/E53;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077819
    if-eqz v2, :cond_0

    .line 2077820
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077821
    new-instance p0, LX/E53;

    invoke-static {v0}, LX/E4m;->a(LX/0QB;)LX/E4m;

    move-result-object v3

    check-cast v3, LX/E4m;

    invoke-direct {p0, v3}, LX/E53;-><init>(LX/E4m;)V

    .line 2077822
    move-object v0, p0

    .line 2077823
    sput-object v0, LX/E53;->b:LX/E53;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077824
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077825
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077826
    :cond_1
    sget-object v0, LX/E53;->b:LX/E53;

    return-object v0

    .line 2077827
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077828
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
