.class public LX/D0Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1955467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1955468
    return-void
.end method

.method public static a(Landroid/os/Bundle;)LX/7BH;
    .locals 1
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955445
    if-nez p0, :cond_1

    .line 1955446
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    .line 1955447
    :cond_0
    :goto_0
    return-object v0

    .line 1955448
    :cond_1
    const-string v0, "search_theme"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/7BH;

    .line 1955449
    if-nez v0, :cond_0

    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/D0Q;
    .locals 3

    .prologue
    .line 1955456
    const-class v1, LX/D0Q;

    monitor-enter v1

    .line 1955457
    :try_start_0
    sget-object v0, LX/D0Q;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1955458
    sput-object v2, LX/D0Q;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1955459
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1955460
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1955461
    new-instance v0, LX/D0Q;

    invoke-direct {v0}, LX/D0Q;-><init>()V

    .line 1955462
    move-object v0, v0

    .line 1955463
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1955464
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D0Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1955465
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1955466
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/view/ContextThemeWrapper;
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955450
    invoke-static {p1}, LX/D0Q;->a(Landroid/os/Bundle;)LX/7BH;

    move-result-object v0

    sget-object v1, LX/7BH;->DARK:LX/7BH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 1955451
    :goto_0
    new-instance v2, Landroid/view/ContextThemeWrapper;

    if-eqz v0, :cond_1

    const v1, 0x7f0e0948

    :goto_1
    invoke-direct {v2, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 1955452
    new-instance v1, Landroid/view/ContextThemeWrapper;

    if-eqz v0, :cond_2

    const v0, 0x7f0e0961

    :goto_2
    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v1

    .line 1955453
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1955454
    :cond_1
    const v1, 0x7f0e0949

    goto :goto_1

    .line 1955455
    :cond_2
    const v0, 0x7f0e0960

    goto :goto_2
.end method
