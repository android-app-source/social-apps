.class public final LX/DKf;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/DKg;


# direct methods
.method public constructor <init>(LX/DKg;LX/0TF;)V
    .locals 0

    .prologue
    .line 1987341
    iput-object p1, p0, LX/DKf;->b:LX/DKg;

    iput-object p2, p0, LX/DKf;->a:LX/0TF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    .prologue
    .line 1987342
    invoke-super {p0, p1}, LX/0Vd;->onCancel(Ljava/util/concurrent/CancellationException;)V

    .line 1987343
    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    const/4 v1, 0x0

    .line 1987344
    iput-boolean v1, v0, LX/DKg;->j:Z

    .line 1987345
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1987346
    iget-object v1, p0, LX/DKf;->b:LX/DKg;

    monitor-enter v1

    .line 1987347
    :try_start_0
    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    iget-boolean v0, v0, LX/DKg;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    iget-boolean v0, v0, LX/DKg;->j:Z

    if-eqz v0, :cond_0

    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_1

    .line 1987348
    :cond_0
    monitor-exit v1

    .line 1987349
    :goto_0
    return-void

    .line 1987350
    :cond_1
    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    const/4 v2, 0x0

    .line 1987351
    iput-object v2, v0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1987352
    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    const/4 v2, 0x0

    .line 1987353
    iput-boolean v2, v0, LX/DKg;->j:Z

    .line 1987354
    iget-object v0, p0, LX/DKf;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1987355
    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    iget-object v0, v0, LX/DKg;->e:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1987356
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1987357
    iget-object v1, p0, LX/DKf;->b:LX/DKg;

    monitor-enter v1

    .line 1987358
    :try_start_0
    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    iget-boolean v0, v0, LX/DKg;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    iget-boolean v0, v0, LX/DKg;->j:Z

    if-nez v0, :cond_1

    .line 1987359
    :cond_0
    monitor-exit v1

    .line 1987360
    :goto_0
    return-void

    .line 1987361
    :cond_1
    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    const/4 v2, 0x0

    .line 1987362
    iput-object v2, v0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1987363
    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    const/4 v2, 0x0

    .line 1987364
    iput-boolean v2, v0, LX/DKg;->j:Z

    .line 1987365
    iget-object v0, p0, LX/DKf;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1987366
    iget-object v0, p0, LX/DKf;->b:LX/DKg;

    iget-object v0, v0, LX/DKg;->e:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const v3, 0x3d5492b6

    invoke-static {v0, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1987367
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
