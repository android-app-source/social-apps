.class public final LX/D5G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;)V
    .locals 0

    .prologue
    .line 1963458
    iput-object p1, p0, LX/D5G;->a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 1963459
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 1963460
    iget-object v1, p0, LX/D5G;->a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 1963461
    iget-object v1, p0, LX/D5G;->a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v2, v0

    iget-object v2, p0, LX/D5G;->a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    iget-object v2, v2, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTranslationY(F)V

    .line 1963462
    return-void
.end method
