.class public LX/Dry;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Dry;


# instance fields
.field public a:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2049814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049815
    return-void
.end method

.method public static a(LX/0QB;)LX/Dry;
    .locals 4

    .prologue
    .line 2049816
    sget-object v0, LX/Dry;->b:LX/Dry;

    if-nez v0, :cond_1

    .line 2049817
    const-class v1, LX/Dry;

    monitor-enter v1

    .line 2049818
    :try_start_0
    sget-object v0, LX/Dry;->b:LX/Dry;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2049819
    if-eqz v2, :cond_0

    .line 2049820
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2049821
    new-instance p0, LX/Dry;

    invoke-direct {p0}, LX/Dry;-><init>()V

    .line 2049822
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    .line 2049823
    iput-object v3, p0, LX/Dry;->a:LX/0Zb;

    .line 2049824
    move-object v0, p0

    .line 2049825
    sput-object v0, LX/Dry;->b:LX/Dry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049826
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2049827
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2049828
    :cond_1
    sget-object v0, LX/Dry;->b:LX/Dry;

    return-object v0

    .line 2049829
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2049830
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
