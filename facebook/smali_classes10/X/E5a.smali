.class public LX/E5a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E5a;


# instance fields
.field private final a:LX/E1i;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/E1i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/E1i;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078644
    iput-object p2, p0, LX/E5a;->a:LX/E1i;

    .line 2078645
    iput-object p1, p0, LX/E5a;->b:LX/0Or;

    .line 2078646
    return-void
.end method

.method public static a(LX/0QB;)LX/E5a;
    .locals 5

    .prologue
    .line 2078647
    sget-object v0, LX/E5a;->c:LX/E5a;

    if-nez v0, :cond_1

    .line 2078648
    const-class v1, LX/E5a;

    monitor-enter v1

    .line 2078649
    :try_start_0
    sget-object v0, LX/E5a;->c:LX/E5a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078650
    if-eqz v2, :cond_0

    .line 2078651
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078652
    new-instance v4, LX/E5a;

    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v3

    check-cast v3, LX/E1i;

    invoke-direct {v4, p0, v3}, LX/E5a;-><init>(LX/0Or;LX/E1i;)V

    .line 2078653
    move-object v0, v4

    .line 2078654
    sput-object v0, LX/E5a;->c:LX/E5a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078655
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078656
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078657
    :cond_1
    sget-object v0, LX/E5a;->c:LX/E5a;

    return-object v0

    .line 2078658
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;LX/1U8;LX/2km;LX/1Pn;[JLjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2    # LX/1U8;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # [J
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 2078640
    invoke-interface {p4}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-interface {p2}, LX/1U8;->e()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    move-object v4, p5

    invoke-static/range {v1 .. v5}, LX/E1i;->a(Landroid/content/Context;J[JLjava/lang/String;)LX/Cfl;

    move-result-object v0

    .line 2078641
    invoke-interface {p3, p6, p7, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2078642
    return-void
.end method
