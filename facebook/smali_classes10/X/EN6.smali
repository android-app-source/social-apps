.class public final LX/EN6;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/view/View$OnClickListener;

.field public final synthetic b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2111991
    iput-object p1, p0, LX/EN6;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iput-object p2, p0, LX/EN6;->a:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2111989
    iget-object v0, p0, LX/EN6;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2111990
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2111986
    iget-object v0, p0, LX/EN6;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2111987
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2111988
    return-void
.end method
