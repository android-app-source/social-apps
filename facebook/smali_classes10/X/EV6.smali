.class public final LX/EV6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Tw;


# instance fields
.field public final synthetic a:Lcom/facebook/video/videohome/data/VideoHomeItem;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;Lcom/facebook/video/videohome/data/VideoHomeItem;I)V
    .locals 0

    .prologue
    .line 2127304
    iput-object p1, p0, LX/EV6;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    iput-object p2, p0, LX/EV6;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    iput p3, p0, LX/EV6;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 2127305
    iget-object v0, p0, LX/EV6;->c:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    iget-object v1, p0, LX/EV6;->a:Lcom/facebook/video/videohome/data/VideoHomeItem;

    iget v2, p0, LX/EV6;->b:I

    .line 2127306
    iget-object v3, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->f:LX/3AW;

    sget-object v4, LX/04D;->VIDEO_HOME:LX/04D;

    .line 2127307
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2127308
    iget-object v6, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v6

    .line 2127309
    invoke-interface {v6}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v6

    .line 2127310
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v0, LX/0JT;->VIDEO_HOME_SWIPE:LX/0JT;

    iget-object v0, v0, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2127311
    sget-object v0, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v0, v0, LX/04F;->value:Ljava/lang/String;

    iget-object v1, v4, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2127312
    sget-object v0, LX/0JS;->REACTION_UNIT_TYPE:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {p0, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2127313
    sget-object v0, LX/0JS;->UNIT_POSITION:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2127314
    sget-object v0, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    iget-object v0, v0, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {p0, v0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2127315
    invoke-static {v3, p0}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2127316
    return-void
.end method
