.class public abstract LX/Dnx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2041376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2041377
    iput-object p1, p0, LX/Dnx;->a:Ljava/lang/String;

    .line 2041378
    return-void
.end method


# virtual methods
.method public abstract A(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract B(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract C(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract D(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract E(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract F(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract G(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract H(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract I(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract J(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract K(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract L(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract M(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract N(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract O(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract P(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract Q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    const-wide/16 v4, 0xa

    .line 2041238
    const-string v0, "%s_handleOperation_%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Dnx;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 2041239
    iget-object v3, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v3, v3

    .line 2041240
    aput-object v3, v1, v2

    const v2, 0x401cdf2d

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;[Ljava/lang/Object;I)V

    .line 2041241
    :try_start_0
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2041242
    const-string v1, "init_threads_queue"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2041243
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2041244
    const v1, 0x27f55bc3

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    :goto_0
    return-object v0

    .line 2041245
    :cond_0
    :try_start_1
    const-string v1, "fetch_thread_list"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2041246
    invoke-virtual {p0, p1, p2}, LX/Dnx;->a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2041247
    const v1, 0x5fbee120

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2041248
    :cond_1
    :try_start_2
    const-string v1, "fetch_more_threads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2041249
    invoke-virtual {p0, p1, p2}, LX/Dnx;->b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2041250
    const v1, -0x7775751

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2041251
    :cond_2
    :try_start_3
    const-string v1, "fetch_thread"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2041252
    invoke-virtual {p0, p1, p2}, LX/Dnx;->c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2041253
    const v1, 0x1f4edd47

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2041254
    :cond_3
    :try_start_4
    const-string v1, "fetch_thread_by_participants"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2041255
    invoke-virtual {p0, p1, p2}, LX/Dnx;->d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    .line 2041256
    const v1, -0x61ffd4c

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2041257
    :cond_4
    :try_start_5
    const-string v1, "add_members"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2041258
    invoke-virtual {p0, p1, p2}, LX/Dnx;->e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 2041259
    const v1, 0x5f410195

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2041260
    :cond_5
    :try_start_6
    const-string v1, "create_thread"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2041261
    invoke-virtual {p0, p1, p2}, LX/Dnx;->g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    .line 2041262
    const v1, -0x6e07ed90

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2041263
    :cond_6
    :try_start_7
    const-string v1, "create_group"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2041264
    invoke-virtual {p0, p1, p2}, LX/Dnx;->f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    .line 2041265
    const v1, -0xc48d58c

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041266
    :cond_7
    :try_start_8
    const-string v1, "fetch_more_messages"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2041267
    invoke-virtual {p0, p1, p2}, LX/Dnx;->h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v0

    .line 2041268
    const v1, -0x341e8e98    # -2.9549264E7f

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041269
    :cond_8
    :try_start_9
    const-string v1, "remove_member"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2041270
    invoke-virtual {p0, p1, p2}, LX/Dnx;->i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v0

    .line 2041271
    const v1, 0x5b2a9e06

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041272
    :cond_9
    :try_start_a
    const-string v1, "mark_threads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2041273
    invoke-virtual {p0, p1, p2}, LX/Dnx;->j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v0

    .line 2041274
    const v1, -0x7cb18df0

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041275
    :cond_a
    :try_start_b
    const-string v1, "block_user"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2041276
    invoke-virtual {p0, p1, p2}, LX/Dnx;->k(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result-object v0

    .line 2041277
    const v1, -0x797c5ead

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041278
    :cond_b
    :try_start_c
    const-string v1, "delete_threads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2041279
    invoke-virtual {p0, p1, p2}, LX/Dnx;->l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-object v0

    .line 2041280
    const v1, -0x5b2c1726

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041281
    :cond_c
    :try_start_d
    const-string v1, "delete_all_tincan_threads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2041282
    invoke-virtual {p0, p1, p2}, LX/Dnx;->m(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result-object v0

    .line 2041283
    const v1, -0x67853732

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041284
    :cond_d
    :try_start_e
    const-string v1, "delete_messages"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2041285
    invoke-virtual {p0, p1, p2}, LX/Dnx;->n(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    move-result-object v0

    .line 2041286
    const v1, -0x1328463

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041287
    :cond_e
    :try_start_f
    const-string v1, "modify_thread"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2041288
    invoke-virtual {p0, p1, p2}, LX/Dnx;->o(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    move-result-object v0

    .line 2041289
    const v1, 0x4e8f8eb3

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041290
    :cond_f
    :try_start_10
    const-string v1, "mark_folder_seen"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2041291
    invoke-virtual {p0, p1, p2}, LX/Dnx;->p(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    move-result-object v0

    .line 2041292
    const v1, -0x421fe5cb

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041293
    :cond_10
    :try_start_11
    const-string v1, "save_draft"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 2041294
    invoke-virtual {p0, p1, p2}, LX/Dnx;->q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    move-result-object v0

    .line 2041295
    const v1, -0x630e49f0

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041296
    :cond_11
    :try_start_12
    const-string v1, "pushed_message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2041297
    invoke-virtual {p0, p1, p2}, LX/Dnx;->r(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    move-result-object v0

    .line 2041298
    const v1, -0x5035ed99

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041299
    :cond_12
    :try_start_13
    const-string v1, "update_user_settings"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2041300
    invoke-virtual {p0, p1, p2}, LX/Dnx;->s(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    move-result-object v0

    .line 2041301
    const v1, -0x11b9ca92

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041302
    :cond_13
    :try_start_14
    const-string v1, "search_thread_name_and_participants"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 2041303
    invoke-virtual {p0, p1, p2}, LX/Dnx;->t(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    move-result-object v0

    .line 2041304
    const v1, 0x7a2bbd58

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041305
    :cond_14
    :try_start_15
    const-string v1, "fetch_pinned_threads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 2041306
    invoke-virtual {p0, p1, p2}, LX/Dnx;->u(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    move-result-object v0

    .line 2041307
    const v1, -0x4882b8

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041308
    :cond_15
    :try_start_16
    const-string v1, "update_pinned_threads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 2041309
    invoke-virtual {p0, p1, p2}, LX/Dnx;->v(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    move-result-object v0

    .line 2041310
    const v1, -0x76fd7e47

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041311
    :cond_16
    :try_start_17
    const-string v1, "add_pinned_thread"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 2041312
    invoke-virtual {p0, p1, p2}, LX/Dnx;->w(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    move-result-object v0

    .line 2041313
    const v1, 0x700f4211

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041314
    :cond_17
    :try_start_18
    const-string v1, "unpin_thread"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 2041315
    invoke-virtual {p0, p1, p2}, LX/Dnx;->x(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    move-result-object v0

    .line 2041316
    const v1, 0x730793f4

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041317
    :cond_18
    :try_start_19
    const-string v1, "get_authenticated_attachment_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 2041318
    invoke-virtual {p0, p1, p2}, LX/Dnx;->y(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    move-result-object v0

    .line 2041319
    const v1, 0x10bd6e64

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041320
    :cond_19
    :try_start_1a
    const-string v1, "update_failed_message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 2041321
    invoke-virtual {p0, p1, p2}, LX/Dnx;->z(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    move-result-object v0

    .line 2041322
    const v1, 0x420ff0cb

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041323
    :cond_1a
    :try_start_1b
    const-string v1, "update_montage_audience_mode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 2041324
    invoke-virtual {p0, p1, p2}, LX/Dnx;->A(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    move-result-object v0

    .line 2041325
    const v1, 0x1203e6b0

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041326
    :cond_1b
    :try_start_1c
    const-string v1, "message_accept_request"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 2041327
    invoke-virtual {p0, p1, p2}, LX/Dnx;->B(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    move-result-object v0

    .line 2041328
    const v1, -0x997724b

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041329
    :cond_1c
    :try_start_1d
    const-string v1, "message_ignore_requests"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 2041330
    invoke-virtual {p0, p1, p2}, LX/Dnx;->C(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    move-result-object v0

    .line 2041331
    const v1, -0x404687ca

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041332
    :cond_1d
    :try_start_1e
    const-string v1, "create_local_admin_message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 2041333
    invoke-virtual {p0, p1, p2}, LX/Dnx;->D(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    move-result-object v0

    .line 2041334
    const v1, -0x34d165a5    # -1.1442779E7f

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041335
    :cond_1e
    :try_start_1f
    const-string v1, "received_sms"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 2041336
    invoke-virtual {p0, p1, p2}, LX/Dnx;->E(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    move-result-object v0

    .line 2041337
    const v1, -0x58d7b69d

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041338
    :cond_1f
    :try_start_20
    const-string v1, "group_invite_link"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 2041339
    invoke-virtual {p0, p1, p2}, LX/Dnx;->F(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    move-result-object v0

    .line 2041340
    const v1, 0x7a1e766f

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041341
    :cond_20
    :try_start_21
    const-string v1, "update_folder_counts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 2041342
    invoke-virtual {p0, p1, p2}, LX/Dnx;->G(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_0

    move-result-object v0

    .line 2041343
    const v1, 0x6278cbd

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041344
    :cond_21
    :try_start_22
    const-string v1, "add_admins_to_group"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 2041345
    invoke-virtual {p0, p1, p2}, LX/Dnx;->H(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    move-result-object v0

    .line 2041346
    const v1, -0x3fe0067f

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041347
    :cond_22
    :try_start_23
    const-string v1, "remove_admins_from_group"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 2041348
    invoke-virtual {p0, p1, p2}, LX/Dnx;->I(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_0

    move-result-object v0

    .line 2041349
    const v1, -0x4eb96076

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041350
    :cond_23
    :try_start_24
    const-string v1, "post_game_score"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 2041351
    invoke-virtual {p0, p1, p2}, LX/Dnx;->J(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_0

    move-result-object v0

    .line 2041352
    const v1, 0x1c991251

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041353
    :cond_24
    :try_start_25
    const-string v1, "save_username"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 2041354
    invoke-virtual {p0, p1, p2}, LX/Dnx;->K(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    move-result-object v0

    .line 2041355
    const v1, -0x7d703c77

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041356
    :cond_25
    :try_start_26
    const-string v1, "fetch_thread_queue_enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 2041357
    invoke-virtual {p0, p1, p2}, LX/Dnx;->L(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_0

    move-result-object v0

    .line 2041358
    const v1, 0x36f4cc38

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041359
    :cond_26
    :try_start_27
    const-string v1, "fetch_event_reminders_members"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 2041360
    invoke-virtual {p0, p1, p2}, LX/Dnx;->M(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_0

    move-result-object v0

    .line 2041361
    const v1, 0x3e1e9212

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041362
    :cond_27
    :try_start_28
    const-string v1, "fetch_tincan_identity_keys"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 2041363
    invoke-virtual {p0, p1, p2}, LX/Dnx;->N(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_0

    move-result-object v0

    .line 2041364
    const v1, 0x4189677c

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041365
    :cond_28
    :try_start_29
    const-string v1, "fetch_group_threads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 2041366
    invoke-virtual {p0, p1, p2}, LX/Dnx;->O(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_0

    move-result-object v0

    .line 2041367
    const v1, -0x78c56bab

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041368
    :cond_29
    :try_start_2a
    const-string v1, "update_agent_suggestions"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 2041369
    invoke-virtual {p0, p1, p2}, LX/Dnx;->Q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_0

    move-result-object v0

    .line 2041370
    const v1, -0x7c13529d

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041371
    :cond_2a
    :try_start_2b
    const-string v1, "fetch_room_threads"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 2041372
    invoke-virtual {p0, p1, p2}, LX/Dnx;->P(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_0

    move-result-object v0

    .line 2041373
    const v1, 0x8af8ce9

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2041374
    :cond_2b
    :try_start_2c
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_0

    move-result-object v0

    .line 2041375
    const v1, 0x44baba19

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x6ec319c1

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    throw v0
.end method

.method public abstract i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract k(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract m(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract n(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract o(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract p(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract r(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract s(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract t(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract u(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract v(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract w(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract x(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract y(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method

.method public abstract z(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
.end method
