.class public final enum LX/ClB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ClB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ClB;

.field public static final enum APP_BACKGROUNDED:LX/ClB;

.field public static final enum USER_EXITED_SESSION:LX/ClB;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1932069
    new-instance v0, LX/ClB;

    const-string v1, "APP_BACKGROUNDED"

    invoke-direct {v0, v1, v2}, LX/ClB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClB;->APP_BACKGROUNDED:LX/ClB;

    .line 1932070
    new-instance v0, LX/ClB;

    const-string v1, "USER_EXITED_SESSION"

    invoke-direct {v0, v1, v3}, LX/ClB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClB;->USER_EXITED_SESSION:LX/ClB;

    .line 1932071
    const/4 v0, 0x2

    new-array v0, v0, [LX/ClB;

    sget-object v1, LX/ClB;->APP_BACKGROUNDED:LX/ClB;

    aput-object v1, v0, v2

    sget-object v1, LX/ClB;->USER_EXITED_SESSION:LX/ClB;

    aput-object v1, v0, v3

    sput-object v0, LX/ClB;->$VALUES:[LX/ClB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1932067
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ClB;
    .locals 1

    .prologue
    .line 1932068
    const-class v0, LX/ClB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ClB;

    return-object v0
.end method

.method public static values()[LX/ClB;
    .locals 1

    .prologue
    .line 1932066
    sget-object v0, LX/ClB;->$VALUES:[LX/ClB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ClB;

    return-object v0
.end method
