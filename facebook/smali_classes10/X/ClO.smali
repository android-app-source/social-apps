.class public LX/ClO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/ClO;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/ClM;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1932425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932426
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ClO;->a:Ljava/util/List;

    .line 1932427
    sget-object v0, LX/Chk;->a:LX/0Tn;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/ClO;->b:Z

    .line 1932428
    return-void
.end method

.method public static a(LX/0QB;)LX/ClO;
    .locals 4

    .prologue
    .line 1932412
    sget-object v0, LX/ClO;->c:LX/ClO;

    if-nez v0, :cond_1

    .line 1932413
    const-class v1, LX/ClO;

    monitor-enter v1

    .line 1932414
    :try_start_0
    sget-object v0, LX/ClO;->c:LX/ClO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1932415
    if-eqz v2, :cond_0

    .line 1932416
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1932417
    new-instance p0, LX/ClO;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/ClO;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1932418
    move-object v0, p0

    .line 1932419
    sput-object v0, LX/ClO;->c:LX/ClO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1932420
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1932421
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1932422
    :cond_1
    sget-object v0, LX/ClO;->c:LX/ClO;

    return-object v0

    .line 1932423
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1932424
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/ClM;)V
    .locals 1

    .prologue
    .line 1932410
    iget-object v0, p0, LX/ClO;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1932411
    return-void
.end method
