.class public LX/EpZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2h7;

.field public final b:LX/5P2;

.field public final c:LX/2dj;

.field public final d:LX/Epg;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/Fqs;

.field public final h:LX/2hZ;

.field public final i:LX/2do;

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2h7;LX/5P2;LX/2dj;LX/Epg;Ljava/util/concurrent/ExecutorService;LX/0Or;LX/Fqs;LX/2hZ;LX/2do;)V
    .locals 1
    .param p1    # LX/2h7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/5P2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2h7;",
            "LX/5P2;",
            "LX/2dj;",
            "LX/Epg;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/Fqs;",
            "LX/2hZ;",
            "LX/2do;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2170388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170389
    const/4 v0, 0x0

    iput-object v0, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2170390
    iput-object p1, p0, LX/EpZ;->a:LX/2h7;

    .line 2170391
    iput-object p2, p0, LX/EpZ;->b:LX/5P2;

    .line 2170392
    iput-object p3, p0, LX/EpZ;->c:LX/2dj;

    .line 2170393
    iput-object p4, p0, LX/EpZ;->d:LX/Epg;

    .line 2170394
    iput-object p5, p0, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    .line 2170395
    iput-object p6, p0, LX/EpZ;->f:LX/0Or;

    .line 2170396
    iput-object p7, p0, LX/EpZ;->g:LX/Fqs;

    .line 2170397
    iput-object p8, p0, LX/EpZ;->h:LX/2hZ;

    .line 2170398
    iput-object p9, p0, LX/EpZ;->i:LX/2do;

    .line 2170399
    return-void
.end method

.method private static a()LX/2h8;
    .locals 1

    .prologue
    .line 2170387
    sget-object v0, LX/2h8;->ENTITY_CARDS:LX/2h8;

    return-object v0
.end method

.method private static a(LX/EpZ;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V
    .locals 11

    .prologue
    .line 2170383
    iget-object v0, p0, LX/EpZ;->c:LX/2dj;

    invoke-static {p0}, LX/EpZ;->d(LX/EpZ;)LX/2h9;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, LX/2dj;->a(JLX/2h9;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2170384
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object/from16 v1, p8

    move-wide v2, p1

    move-object v5, p4

    move-object/from16 v6, p5

    invoke-interface/range {v1 .. v6}, LX/EpL;->a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2170385
    iget-object v1, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-static/range {v0 .. v9}, LX/EpZ;->a$redex0(LX/EpZ;Lcom/google/common/util/concurrent/ListenableFuture;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V

    .line 2170386
    return-void
.end method

.method private static a(LX/EpZ;Landroid/content/Context;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V
    .locals 10

    .prologue
    .line 2170378
    iget-object v1, p0, LX/EpZ;->c:LX/2dj;

    invoke-static {}, LX/EpZ;->a()LX/2h8;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, LX/EpZ;->b:LX/5P2;

    move-wide v2, p2

    invoke-virtual/range {v1 .. v6}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v1

    iget-object v2, p0, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2170379
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object/from16 v1, p9

    move-wide v2, p2

    move-object v5, p5

    move-object/from16 v6, p6

    invoke-interface/range {v1 .. v6}, LX/EpL;->a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2170380
    iget-object v1, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, p0

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-static/range {v0 .. v9}, LX/EpZ;->a$redex0(LX/EpZ;Lcom/google/common/util/concurrent/ListenableFuture;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V

    .line 2170381
    iget-object v0, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {p0, v0, p2, p3, p1}, LX/EpZ;->a(LX/EpZ;Lcom/google/common/util/concurrent/ListenableFuture;JLandroid/content/Context;)V

    .line 2170382
    return-void
.end method

.method private static a(LX/EpZ;Landroid/content/Context;JLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V
    .locals 14

    .prologue
    .line 2170373
    iget-object v2, p0, LX/EpZ;->d:LX/Epg;

    move-object/from16 v0, p4

    invoke-virtual {v2, p1, v0}, LX/Epg;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/EpQ;

    move-object v4, p0

    move-object/from16 v5, p10

    move-wide/from16 v6, p2

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move-object/from16 v12, p5

    invoke-direct/range {v3 .. v12}, LX/EpQ;-><init>(LX/EpZ;LX/EpL;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    iget-object v4, p0, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2170374
    return-void
.end method

.method private static a(LX/EpZ;Lcom/google/common/util/concurrent/ListenableFuture;JLandroid/content/Context;)V
    .locals 2

    .prologue
    .line 2170400
    if-nez p1, :cond_0

    .line 2170401
    :goto_0
    return-void

    .line 2170402
    :cond_0
    new-instance v0, LX/EpX;

    invoke-direct {v0, p0, p2, p3, p4}, LX/EpX;-><init>(LX/EpZ;JLandroid/content/Context;)V

    iget-object v1, p0, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/EpZ;Lcom/google/common/util/concurrent/ListenableFuture;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V
    .locals 12

    .prologue
    .line 2170375
    if-nez p1, :cond_0

    .line 2170376
    :goto_0
    return-void

    .line 2170377
    :cond_0
    new-instance v1, LX/EpW;

    move-object v2, p0

    move-object/from16 v3, p9

    move-wide v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-direct/range {v1 .. v10}, LX/EpW;-><init>(LX/EpZ;LX/EpL;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZ)V

    iget-object v0, p0, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method private static b(LX/EpZ;Landroid/content/Context;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V
    .locals 12

    .prologue
    .line 2170371
    iget-object v0, p0, LX/EpZ;->d:LX/Epg;

    invoke-virtual {v0, p1}, LX/Epg;->a(Landroid/content/Context;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/EpR;

    move-object v2, p0

    move-object/from16 v3, p9

    move-wide v4, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p4

    invoke-direct/range {v1 .. v10}, LX/EpR;-><init>(LX/EpZ;LX/EpL;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    iget-object v2, p0, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2170372
    return-void
.end method

.method private static d(LX/EpZ;)LX/2h9;
    .locals 1

    .prologue
    .line 2170368
    iget-object v0, p0, LX/EpZ;->a:LX/2h7;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EpZ;->a:LX/2h7;

    iget-object v0, v0, LX/2h7;->friendRequestCancelRef:LX/2h9;

    if-eqz v0, :cond_0

    .line 2170369
    iget-object v0, p0, LX/EpZ;->a:LX/2h7;

    iget-object v0, v0, LX/2h7;->friendRequestCancelRef:LX/2h9;

    .line 2170370
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/2h9;->ENTITY_CARDS:LX/2h9;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5wN;LX/EpL;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 14

    .prologue
    .line 2170343
    iget-object v2, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v2}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2170344
    :cond_0
    :goto_0
    return-void

    .line 2170345
    :cond_1
    invoke-interface {p1}, LX/5wN;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    .line 2170346
    invoke-interface {p1}, LX/5wN;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v11

    .line 2170347
    invoke-interface {p1}, LX/5wN;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v12

    .line 2170348
    invoke-interface {p1}, LX/5wN;->d()Z

    move-result v9

    .line 2170349
    invoke-interface {p1}, LX/5wN;->e()Z

    move-result v10

    .line 2170350
    invoke-interface {p1}, LX/5wN;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v3, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    .line 2170351
    invoke-interface/range {v3 .. v8}, LX/EpL;->a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2170352
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object/from16 v0, p3

    if-ne v0, v2, :cond_3

    .line 2170353
    iget-object v2, p0, LX/EpZ;->c:LX/2dj;

    invoke-interface {p1}, LX/5wN;->c()Ljava/lang/String;

    move-result-object v3

    const-string v7, "ENTITY_CARDS"

    invoke-virtual {v2, v3, v7}, LX/2dj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2170354
    :cond_2
    :goto_1
    iget-object v2, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_0

    .line 2170355
    iget-object v3, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v2, p0

    move-object v7, v11

    move-object v8, v12

    move-object/from16 v11, p2

    invoke-static/range {v2 .. v11}, LX/EpZ;->a$redex0(LX/EpZ;Lcom/google/common/util/concurrent/ListenableFuture;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V

    goto :goto_0

    .line 2170356
    :cond_3
    move-object/from16 v0, p4

    if-eq v0, v12, :cond_5

    const/4 v2, 0x1

    move v7, v2

    .line 2170357
    :goto_2
    move-object/from16 v0, p3

    if-ne v11, v0, :cond_4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v11, v2, :cond_6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-object/from16 v0, p4

    if-ne v0, v2, :cond_6

    :cond_4
    const/4 v2, 0x1

    .line 2170358
    :goto_3
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-object/from16 v0, p4

    if-ne v0, v3, :cond_7

    const-string v3, "SEE_FIRST"

    .line 2170359
    :goto_4
    if-eqz v2, :cond_8

    .line 2170360
    iget-object v2, p0, LX/EpZ;->c:LX/2dj;

    invoke-interface {p1}, LX/5wN;->c()Ljava/lang/String;

    move-result-object v8

    const-string v13, "ENTITY_CARDS"

    invoke-virtual {v2, v8, v13}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2170361
    if-eqz v7, :cond_2

    .line 2170362
    iget-object v2, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v7, LX/EpV;

    invoke-direct {v7, p0, p1, v3}, LX/EpV;-><init>(LX/EpZ;LX/5wN;Ljava/lang/String;)V

    iget-object v3, p0, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v7, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 2170363
    :cond_5
    const/4 v2, 0x0

    move v7, v2

    goto :goto_2

    .line 2170364
    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    .line 2170365
    :cond_7
    const-string v3, "REGULAR_FOLLOW"

    goto :goto_4

    .line 2170366
    :cond_8
    if-eqz v7, :cond_2

    .line 2170367
    iget-object v2, p0, LX/EpZ;->c:LX/2dj;

    invoke-interface {p1}, LX/5wN;->c()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ENTITY_CARDS"

    invoke-virtual {v2, v7, v3, v8}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, p0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;LX/5wN;LX/EpL;)V
    .locals 23

    .prologue
    .line 2170329
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v2}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2170330
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2170331
    :cond_1
    invoke-interface/range {p2 .. p2}, LX/5wN;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2170332
    invoke-interface/range {p2 .. p2}, LX/5wN;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    .line 2170333
    invoke-interface/range {p2 .. p2}, LX/5wN;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v7

    .line 2170334
    invoke-interface/range {p2 .. p2}, LX/5wN;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v8

    .line 2170335
    invoke-interface/range {p2 .. p2}, LX/5wN;->d()Z

    move-result v9

    .line 2170336
    invoke-interface/range {p2 .. p2}, LX/5wN;->e()Z

    move-result v10

    .line 2170337
    if-eqz v6, :cond_0

    .line 2170338
    sget-object v2, LX/EpY;->a:[I

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v11, p3

    .line 2170339
    invoke-static/range {v2 .. v11}, LX/EpZ;->a(LX/EpZ;Landroid/content/Context;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V

    goto :goto_0

    .line 2170340
    :pswitch_2
    invoke-interface/range {p2 .. p2}, LX/5wN;->ce_()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move-wide v14, v4

    move-object/from16 v17, v6

    move-object/from16 v18, v7

    move-object/from16 v19, v8

    move/from16 v20, v9

    move/from16 v21, v10

    move-object/from16 v22, p3

    invoke-static/range {v12 .. v22}, LX/EpZ;->a(LX/EpZ;Landroid/content/Context;JLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V

    goto :goto_0

    :pswitch_3
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v11, p3

    .line 2170341
    invoke-static/range {v2 .. v11}, LX/EpZ;->b(LX/EpZ;Landroid/content/Context;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V

    goto :goto_0

    :pswitch_4
    move-object/from16 v3, p0

    move-object/from16 v11, p3

    .line 2170342
    invoke-static/range {v3 .. v11}, LX/EpZ;->a(LX/EpZ;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZZLX/EpL;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method
