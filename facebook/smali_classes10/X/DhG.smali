.class public LX/DhG;
.super LX/6Lb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;",
        "LX/Dh8;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DhA;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2030732
    invoke-direct {p0, p1}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 2030733
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2030734
    iput-object v0, p0, LX/DhG;->a:LX/0Ot;

    .line 2030735
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2030736
    iput-object v0, p0, LX/DhG;->b:LX/0Ot;

    .line 2030737
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 2030738
    check-cast p1, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2030739
    invoke-virtual {p2}, LX/6LZ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/6LZ;->a:Ljava/lang/Object;

    check-cast v0, LX/Dh8;

    invoke-virtual {v0}, LX/Dh8;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 2030740
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p2, LX/6LZ;->a:Ljava/lang/Object;

    check-cast v0, LX/Dh8;

    iget-object v0, v0, LX/Dh8;->b:Ljava/io/File;

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v1, v0

    .line 2030741
    :goto_1
    invoke-virtual {p2}, LX/6LZ;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, LX/6LZ;->a:Ljava/lang/Object;

    check-cast v0, LX/Dh8;

    invoke-virtual {v0}, LX/Dh8;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 2030742
    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p2, LX/6LZ;->a:Ljava/lang/Object;

    check-cast v0, LX/Dh8;

    iget-object v0, v0, LX/Dh8;->c:Ljava/io/File;

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2030743
    :goto_3
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v1, v4, v3

    aput-object v0, v4, v2

    invoke-static {v4}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2030744
    new-instance v1, LX/DhF;

    invoke-direct {v1, p1}, LX/DhF;-><init>(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)V

    .line 2030745
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2030746
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v3

    .line 2030747
    goto :goto_0

    .line 2030748
    :cond_1
    iget-object v0, p0, LX/DhG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;

    invoke-static {p1}, LX/DhA;->a(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p1, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;->a(Ljava/lang/String;Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 2030749
    goto :goto_2

    .line 2030750
    :cond_3
    iget-object v0, p0, LX/DhG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;

    invoke-static {p1}, LX/DhA;->b(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->c:Landroid/net/Uri;

    invoke-virtual {v0, v4, v5}, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;->a(Ljava/lang/String;Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_3
.end method

.method public final b(Ljava/lang/Object;)LX/6LZ;
    .locals 3

    .prologue
    .line 2030751
    check-cast p1, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    .line 2030752
    invoke-static {p1}, LX/DhA;->a(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)Ljava/lang/String;

    move-result-object v1

    .line 2030753
    iget-object v0, p0, LX/DhG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DhA;

    invoke-virtual {v0, v1}, LX/DhA;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 2030754
    invoke-static {p1}, LX/DhA;->b(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)Ljava/lang/String;

    move-result-object v2

    .line 2030755
    iget-object v0, p0, LX/DhG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DhA;

    invoke-virtual {v0, v2}, LX/DhA;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 2030756
    new-instance v2, LX/Dh8;

    invoke-direct {v2, p1, v1, v0}, LX/Dh8;-><init>(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;Ljava/io/File;Ljava/io/File;)V

    .line 2030757
    invoke-virtual {v2}, LX/Dh8;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, LX/Dh8;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2030758
    if-eqz v0, :cond_0

    .line 2030759
    invoke-static {v2}, LX/6LZ;->b(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    .line 2030760
    :goto_1
    return-object v0

    .line 2030761
    :cond_0
    invoke-virtual {v2}, LX/Dh8;->a()Z

    move-result v0

    invoke-virtual {v2}, LX/Dh8;->b()Z

    move-result v1

    xor-int/2addr v0, v1

    move v0, v0

    .line 2030762
    if-eqz v0, :cond_1

    .line 2030763
    invoke-static {v2}, LX/6LZ;->a(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    goto :goto_1

    .line 2030764
    :cond_1
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
