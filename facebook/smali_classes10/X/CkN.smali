.class public final LX/CkN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/16 v6, 0x2d

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1930906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930907
    if-nez p1, :cond_0

    .line 1930908
    :goto_0
    return-void

    .line 1930909
    :cond_0
    const/16 v0, 0x2b

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v3

    .line 1930910
    invoke-static {p1, v6}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v4

    .line 1930911
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    .line 1930912
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/CkN;->c:Ljava/lang/String;

    .line 1930913
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 1930914
    const-string v0, "-%s"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v5, v2

    invoke-static {v0, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CkN;->c:Ljava/lang/String;

    .line 1930915
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1930916
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v4, 0x3a

    invoke-static {v0, v4}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v4

    .line 1930917
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1930918
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/CkN;->a:Ljava/lang/String;

    .line 1930919
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_5

    .line 1930920
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v6}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/CkN;->b:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1930921
    goto :goto_1

    :cond_4
    move v0, v2

    .line 1930922
    goto :goto_2

    .line 1930923
    :cond_5
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v6}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/CkN;->a:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;)LX/CkN;
    .locals 1

    .prologue
    .line 1930924
    new-instance v0, LX/CkN;

    invoke-direct {v0, p0}, LX/CkN;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
