.class public final LX/EYP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EYE;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/EYE;",
        "Lcom/google/protobuf/FieldSet$FieldDescriptorLite",
        "<",
        "LX/EYP;",
        ">;",
        "Ljava/lang/Comparable",
        "<",
        "LX/EYP;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[LX/EZV;


# instance fields
.field public final b:I

.field public c:LX/EXL;

.field private final d:Ljava/lang/String;

.field private final e:LX/EYQ;

.field private final f:LX/EYF;

.field public g:LX/EYO;

.field public h:LX/EYF;

.field private i:LX/EYF;

.field private j:LX/EYL;

.field private k:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2137250
    invoke-static {}, LX/EZV;->values()[LX/EZV;

    move-result-object v0

    sput-object v0, LX/EYP;->a:[LX/EZV;

    .line 2137251
    invoke-static {}, LX/EYO;->values()[LX/EYO;

    move-result-object v0

    array-length v0, v0

    invoke-static {}, LX/EXK;->values()[LX/EXK;

    move-result-object v1

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 2137252
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "descriptor.proto has a new declared type but Desrciptors.java wasn\'t updated."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137253
    :cond_0
    return-void
.end method

.method private constructor <init>(LX/EXL;LX/EYQ;LX/EYF;IZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2137223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137224
    iput p4, p0, LX/EYP;->b:I

    .line 2137225
    iput-object p1, p0, LX/EYP;->c:LX/EXL;

    .line 2137226
    invoke-virtual {p1}, LX/EXL;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3, v0}, LX/EYT;->b(LX/EYQ;LX/EYF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->d:Ljava/lang/String;

    .line 2137227
    iput-object p2, p0, LX/EYP;->e:LX/EYQ;

    .line 2137228
    invoke-virtual {p1}, LX/EXL;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2137229
    iget-object v0, p1, LX/EXL;->type_:LX/EXK;

    move-object v0, v0

    .line 2137230
    invoke-static {v0}, LX/EYO;->valueOf(LX/EXK;)LX/EYO;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->g:LX/EYO;

    .line 2137231
    :cond_0
    invoke-virtual {p0}, LX/EYP;->e()I

    move-result v0

    if-gtz v0, :cond_1

    .line 2137232
    new-instance v0, LX/EYK;

    const-string v1, "Field numbers must be positive integers."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137233
    :cond_1
    iget-object v0, p1, LX/EXL;->options_:LX/EXR;

    move-object v0, v0

    .line 2137234
    iget-boolean v2, v0, LX/EXR;->packed_:Z

    move v0, v2

    .line 2137235
    if-eqz v0, :cond_2

    invoke-virtual {p0}, LX/EYP;->o()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2137236
    new-instance v0, LX/EYK;

    const-string v1, "[packed = true] can only be specified for repeated primitive fields."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137237
    :cond_2
    if-eqz p5, :cond_5

    .line 2137238
    invoke-virtual {p1}, LX/EXL;->y()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2137239
    new-instance v0, LX/EYK;

    const-string v1, "FieldDescriptorProto.extendee not set for extension field."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137240
    :cond_3
    iput-object v1, p0, LX/EYP;->h:LX/EYF;

    .line 2137241
    if-eqz p3, :cond_4

    .line 2137242
    iput-object p3, p0, LX/EYP;->f:LX/EYF;

    .line 2137243
    :goto_0
    iget-object v0, p2, LX/EYQ;->h:LX/EYJ;

    invoke-virtual {v0, p0}, LX/EYJ;->a(LX/EYE;)V

    .line 2137244
    return-void

    .line 2137245
    :cond_4
    iput-object v1, p0, LX/EYP;->f:LX/EYF;

    goto :goto_0

    .line 2137246
    :cond_5
    invoke-virtual {p1}, LX/EXL;->y()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2137247
    new-instance v0, LX/EYK;

    const-string v1, "FieldDescriptorProto.extendee set for non-extension field."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137248
    :cond_6
    iput-object p3, p0, LX/EYP;->h:LX/EYF;

    .line 2137249
    iput-object v1, p0, LX/EYP;->f:LX/EYF;

    goto :goto_0
.end method

.method public synthetic constructor <init>(LX/EXL;LX/EYQ;LX/EYF;IZB)V
    .locals 0

    .prologue
    .line 2137222
    invoke-direct/range {p0 .. p5}, LX/EYP;-><init>(LX/EXL;LX/EYQ;LX/EYF;IZ)V

    return-void
.end method

.method public static x(LX/EYP;)V
    .locals 8

    .prologue
    const/16 v5, 0x22

    const/4 v4, 0x0

    .line 2137124
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2137125
    iget-object v0, p0, LX/EYP;->e:LX/EYQ;

    iget-object v0, v0, LX/EYQ;->h:LX/EYJ;

    iget-object v1, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v1}, LX/EXL;->z()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/EYI;->TYPES_ONLY:LX/EYI;

    invoke-virtual {v0, v1, p0, v2}, LX/EYJ;->a(Ljava/lang/String;LX/EYE;LX/EYI;)LX/EYE;

    move-result-object v0

    .line 2137126
    instance-of v1, v0, LX/EYF;

    if-nez v1, :cond_0

    .line 2137127
    new-instance v0, LX/EYK;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v2}, LX/EXL;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not a message type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137128
    :cond_0
    check-cast v0, LX/EYF;

    iput-object v0, p0, LX/EYP;->h:LX/EYF;

    .line 2137129
    iget-object v0, p0, LX/EYP;->h:LX/EYF;

    move-object v0, v0

    .line 2137130
    invoke-virtual {p0}, LX/EYP;->e()I

    move-result v1

    invoke-virtual {v0, v1}, LX/EYF;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2137131
    new-instance v0, LX/EYK;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2137132
    iget-object v2, p0, LX/EYP;->h:LX/EYF;

    move-object v2, v2

    .line 2137133
    invoke-virtual {v2}, LX/EYF;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" does not declare "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/EYP;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as an extension number."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137134
    :cond_1
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->w()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2137135
    iget-object v0, p0, LX/EYP;->e:LX/EYQ;

    iget-object v0, v0, LX/EYQ;->h:LX/EYJ;

    iget-object v1, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v1}, LX/EXL;->x()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/EYI;->TYPES_ONLY:LX/EYI;

    invoke-virtual {v0, v1, p0, v2}, LX/EYJ;->a(Ljava/lang/String;LX/EYE;LX/EYI;)LX/EYE;

    move-result-object v0

    .line 2137136
    iget-object v1, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v1}, LX/EXL;->q()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2137137
    instance-of v1, v0, LX/EYF;

    if-eqz v1, :cond_3

    .line 2137138
    sget-object v1, LX/EYO;->MESSAGE:LX/EYO;

    iput-object v1, p0, LX/EYP;->g:LX/EYO;

    .line 2137139
    :cond_2
    :goto_0
    invoke-virtual {p0}, LX/EYP;->f()LX/EYN;

    move-result-object v1

    sget-object v2, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v1, v2, :cond_6

    .line 2137140
    instance-of v1, v0, LX/EYF;

    if-nez v1, :cond_5

    .line 2137141
    new-instance v0, LX/EYK;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v2}, LX/EXL;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not a message type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137142
    :cond_3
    instance-of v1, v0, LX/EYL;

    if-eqz v1, :cond_4

    .line 2137143
    sget-object v1, LX/EYO;->ENUM:LX/EYO;

    iput-object v1, p0, LX/EYP;->g:LX/EYO;

    goto :goto_0

    .line 2137144
    :cond_4
    new-instance v0, LX/EYK;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v2}, LX/EXL;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not a type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137145
    :cond_5
    check-cast v0, LX/EYF;

    iput-object v0, p0, LX/EYP;->i:LX/EYF;

    .line 2137146
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->A()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2137147
    new-instance v0, LX/EYK;

    const-string v1, "Messages can\'t have default values."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137148
    :cond_6
    invoke-virtual {p0}, LX/EYP;->f()LX/EYN;

    move-result-object v1

    sget-object v2, LX/EYN;->ENUM:LX/EYN;

    if-ne v1, v2, :cond_9

    .line 2137149
    instance-of v1, v0, LX/EYL;

    if-nez v1, :cond_7

    .line 2137150
    new-instance v0, LX/EYK;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v2}, LX/EXL;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not an enum type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137151
    :cond_7
    check-cast v0, LX/EYL;

    iput-object v0, p0, LX/EYP;->j:LX/EYL;

    .line 2137152
    :cond_8
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->A()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2137153
    invoke-virtual {p0}, LX/EYP;->m()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2137154
    new-instance v0, LX/EYK;

    const-string v1, "Repeated fields cannot have default values."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137155
    :cond_9
    new-instance v0, LX/EYK;

    const-string v1, "Field with primitive type has type_name."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137156
    :cond_a
    invoke-virtual {p0}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-eq v0, v1, :cond_b

    invoke-virtual {p0}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->ENUM:LX/EYN;

    if-ne v0, v1, :cond_8

    .line 2137157
    :cond_b
    new-instance v0, LX/EYK;

    const-string v1, "Field with message or enum type missing type_name."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137158
    :cond_c
    :try_start_0
    sget-object v0, LX/EYD;->a:[I

    .line 2137159
    iget-object v1, p0, LX/EYP;->g:LX/EYO;

    move-object v1, v1

    .line 2137160
    invoke-virtual {v1}, LX/EYO;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v0, :pswitch_data_0

    .line 2137161
    :cond_d
    :goto_1
    invoke-virtual {p0}, LX/EYP;->q()Z

    move-result v0

    if-nez v0, :cond_e

    .line 2137162
    iget-object v0, p0, LX/EYP;->e:LX/EYQ;

    iget-object v0, v0, LX/EYQ;->h:LX/EYJ;

    invoke-virtual {v0, p0}, LX/EYJ;->a(LX/EYP;)V

    .line 2137163
    :cond_e
    iget-object v0, p0, LX/EYP;->h:LX/EYF;

    if-eqz v0, :cond_19

    iget-object v0, p0, LX/EYP;->h:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->d()LX/EXf;

    move-result-object v0

    .line 2137164
    iget-boolean v1, v0, LX/EXf;->messageSetWireFormat_:Z

    move v0, v1

    .line 2137165
    if-eqz v0, :cond_19

    .line 2137166
    invoke-virtual {p0}, LX/EYP;->q()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2137167
    invoke-virtual {p0}, LX/EYP;->l()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2137168
    iget-object v0, p0, LX/EYP;->g:LX/EYO;

    move-object v0, v0

    .line 2137169
    sget-object v1, LX/EYO;->MESSAGE:LX/EYO;

    if-eq v0, v1, :cond_19

    .line 2137170
    :cond_f
    new-instance v0, LX/EYK;

    const-string v1, "Extensions of MessageSets must be optional messages."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137171
    :pswitch_0
    :try_start_1
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    .line 2137172
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v0, v6, v7}, LX/EZK;->a(Ljava/lang/String;ZZ)J

    move-result-wide v6

    long-to-int v6, v6

    move v0, v6

    .line 2137173
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2137174
    :catch_0
    move-exception v0

    .line 2137175
    new-instance v1, LX/EYK;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not parse default value: \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v3}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2137176
    :pswitch_1
    :try_start_2
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    .line 2137177
    invoke-static {v0, v6, v6}, LX/EZK;->a(Ljava/lang/String;ZZ)J

    move-result-wide v6

    long-to-int v6, v6

    move v0, v6

    .line 2137178
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto :goto_1

    .line 2137179
    :pswitch_2
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x1

    .line 2137180
    invoke-static {v0, v6, v6}, LX/EZK;->a(Ljava/lang/String;ZZ)J

    move-result-wide v6

    move-wide v0, v6

    .line 2137181
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137182
    :pswitch_3
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    .line 2137183
    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v0, v6, v7}, LX/EZK;->a(Ljava/lang/String;ZZ)J

    move-result-wide v6

    move-wide v0, v6

    .line 2137184
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137185
    :pswitch_4
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    const-string v1, "inf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2137186
    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137187
    :cond_10
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-inf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2137188
    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137189
    :cond_11
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    const-string v1, "nan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2137190
    const/high16 v0, 0x7fc00000    # NaNf

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137191
    :cond_12
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137192
    :pswitch_5
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    const-string v1, "inf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2137193
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137194
    :cond_13
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-inf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2137195
    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137196
    :cond_14
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    const-string v1, "nan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2137197
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137198
    :cond_15
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137199
    :pswitch_6
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137200
    :pswitch_7
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 2137201
    :pswitch_8
    :try_start_3
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/EZK;->a(Ljava/lang/CharSequence;)LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;
    :try_end_3
    .catch LX/EZH; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 2137202
    :catch_1
    move-exception v0

    .line 2137203
    :try_start_4
    new-instance v1, LX/EYK;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t parse default value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/EZH;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2137204
    :pswitch_9
    iget-object v0, p0, LX/EYP;->j:LX/EYL;

    iget-object v1, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v1}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v1

    .line 2137205
    iget-object v2, v0, LX/EYL;->d:LX/EYQ;

    iget-object v2, v2, LX/EYQ;->h:LX/EYJ;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, LX/EYL;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2e

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2137206
    sget-object v4, LX/EYI;->ALL_SYMBOLS:LX/EYI;

    invoke-static {v2, v3, v4}, LX/EYJ;->a(LX/EYJ;Ljava/lang/String;LX/EYI;)LX/EYE;

    move-result-object v4

    move-object v2, v4

    .line 2137207
    if-eqz v2, :cond_1a

    instance-of v3, v2, LX/EYM;

    if-eqz v3, :cond_1a

    .line 2137208
    check-cast v2, LX/EYM;

    .line 2137209
    :goto_2
    move-object v0, v2

    .line 2137210
    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    .line 2137211
    iget-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    if-nez v0, :cond_d

    .line 2137212
    new-instance v0, LX/EYK;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown enum default value: \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v2}, LX/EXL;->B()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x22

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137213
    :pswitch_a
    new-instance v0, LX/EYK;

    const-string v1, "Message type had default value."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_0

    .line 2137214
    :cond_16
    invoke-virtual {p0}, LX/EYP;->m()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2137215
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137216
    :cond_17
    sget-object v0, LX/EYD;->b:[I

    invoke-virtual {p0}, LX/EYP;->f()LX/EYN;

    move-result-object v1

    invoke-virtual {v1}, LX/EYN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2137217
    invoke-virtual {p0}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    iget-object v0, v0, LX/EYN;->defaultDefault:Ljava/lang/Object;

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137218
    :pswitch_b
    iget-object v0, p0, LX/EYP;->j:LX/EYL;

    invoke-virtual {v0}, LX/EYL;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137219
    :pswitch_c
    const/4 v0, 0x0

    iput-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    goto/16 :goto_1

    .line 2137220
    :cond_18
    new-instance v0, LX/EYK;

    const-string v1, "MessageSets cannot have fields, only extensions."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137221
    :cond_19
    return-void

    :cond_1a
    const/4 v2, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137123
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137122
    iget-object v0, p0, LX/EYP;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/EYQ;
    .locals 1

    .prologue
    .line 2137121
    iget-object v0, p0, LX/EYP;->e:LX/EYQ;

    return-object v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2137117
    check-cast p1, LX/EYP;

    .line 2137118
    iget-object v0, p1, LX/EYP;->h:LX/EYF;

    iget-object v1, p0, LX/EYP;->h:LX/EYF;

    if-eq v0, v1, :cond_0

    .line 2137119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptors can only be compared to other FieldDescriptors for fields of the same message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137120
    :cond_0
    invoke-virtual {p0}, LX/EYP;->e()I

    move-result v0

    invoke-virtual {p1}, LX/EYP;->e()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2137114
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    .line 2137115
    iget p0, v0, LX/EXL;->number_:I

    move v0, p0

    .line 2137116
    return v0
.end method

.method public final f()LX/EYN;
    .locals 1

    .prologue
    .line 2137113
    iget-object v0, p0, LX/EYP;->g:LX/EYO;

    invoke-virtual {v0}, LX/EYO;->getJavaType()LX/EYN;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/EZa;
    .locals 1

    .prologue
    .line 2137112
    invoke-virtual {p0}, LX/EYP;->j()LX/EZV;

    move-result-object v0

    invoke-virtual {v0}, LX/EZV;->getJavaType()LX/EZa;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/EWY;
    .locals 1

    .prologue
    .line 2137254
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    return-object v0
.end method

.method public final j()LX/EZV;
    .locals 2

    .prologue
    .line 2137111
    sget-object v0, LX/EYP;->a:[LX/EZV;

    iget-object v1, p0, LX/EYP;->g:LX/EYO;

    invoke-virtual {v1}, LX/EYO;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 2137108
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    .line 2137109
    iget-object v1, v0, LX/EXL;->label_:LX/EXI;

    move-object v0, v1

    .line 2137110
    sget-object v1, LX/EXI;->LABEL_REQUIRED:LX/EXI;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 2137105
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    .line 2137106
    iget-object v1, v0, LX/EXL;->label_:LX/EXI;

    move-object v0, v1

    .line 2137107
    sget-object v1, LX/EXI;->LABEL_OPTIONAL:LX/EXI;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2137102
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    .line 2137103
    iget-object v1, v0, LX/EXL;->label_:LX/EXI;

    move-object v0, v1

    .line 2137104
    sget-object v1, LX/EXI;->LABEL_REPEATED:LX/EXI;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 2137097
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    .line 2137098
    iget-object p0, v0, LX/EXL;->options_:LX/EXR;

    move-object v0, p0

    .line 2137099
    move-object v0, v0

    .line 2137100
    iget-boolean p0, v0, LX/EXR;->packed_:Z

    move v0, p0

    .line 2137101
    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 2137096
    invoke-virtual {p0}, LX/EYP;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EYP;->j()LX/EZV;

    move-result-object v0

    invoke-virtual {v0}, LX/EZV;->isPackable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2137093
    invoke-virtual {p0}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v0, v1, :cond_0

    .line 2137094
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FieldDescriptor.getDefaultValue() called on an embedded message field."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137095
    :cond_0
    iget-object v0, p0, LX/EYP;->k:Ljava/lang/Object;

    return-object v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2137083
    iget-object v0, p0, LX/EYP;->c:LX/EXL;

    invoke-virtual {v0}, LX/EXL;->y()Z

    move-result v0

    return v0
.end method

.method public final s()LX/EYF;
    .locals 2

    .prologue
    .line 2137090
    invoke-virtual {p0}, LX/EYP;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2137091
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This field is not an extension."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137092
    :cond_0
    iget-object v0, p0, LX/EYP;->f:LX/EYF;

    return-object v0
.end method

.method public final t()LX/EYF;
    .locals 2

    .prologue
    .line 2137087
    invoke-virtual {p0}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-eq v0, v1, :cond_0

    .line 2137088
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This field is not of message type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137089
    :cond_0
    iget-object v0, p0, LX/EYP;->i:LX/EYF;

    return-object v0
.end method

.method public final u()LX/EYL;
    .locals 2

    .prologue
    .line 2137084
    invoke-virtual {p0}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->ENUM:LX/EYN;

    if-eq v0, v1, :cond_0

    .line 2137085
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This field is not of enum type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137086
    :cond_0
    iget-object v0, p0, LX/EYP;->j:LX/EYL;

    return-object v0
.end method
