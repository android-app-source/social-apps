.class public final LX/Ckn;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;I)V
    .locals 0

    .prologue
    .line 1931338
    iput-object p1, p0, LX/Ckn;->b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    iput p2, p0, LX/Ckn;->a:I

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method

.method private a(LX/1ln;)V
    .locals 4

    .prologue
    .line 1931339
    if-nez p1, :cond_0

    .line 1931340
    :goto_0
    return-void

    .line 1931341
    :cond_0
    invoke-virtual {p1}, LX/1ln;->g()I

    move-result v0

    .line 1931342
    invoke-virtual {p1}, LX/1ln;->h()I

    move-result v1

    .line 1931343
    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    .line 1931344
    :cond_1
    iget-object v0, p0, LX/Ckn;->b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    iget-object v0, v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    .line 1931345
    :cond_2
    iget-object v2, p0, LX/Ckn;->b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    iget-object v2, v2, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1931346
    iget v2, p0, LX/Ckn;->a:I

    mul-int/2addr v0, v2

    div-int/2addr v0, v1

    .line 1931347
    iget-object v1, p0, LX/Ckn;->b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    iget-object v1, v1, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1931348
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1931349
    iget v0, p0, LX/Ckn;->a:I

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1931350
    iget-object v0, p0, LX/Ckn;->b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    iget-object v0, v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1931351
    check-cast p2, LX/1ln;

    invoke-direct {p0, p2}, LX/Ckn;->a(LX/1ln;)V

    return-void
.end method
