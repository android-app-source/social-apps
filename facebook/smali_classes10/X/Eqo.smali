.class public final LX/Eqo;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Equ;


# direct methods
.method public constructor <init>(LX/Equ;)V
    .locals 0

    .prologue
    .line 2172070
    iput-object p1, p0, LX/Eqo;->a:LX/Equ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2172037
    iget-object v1, p0, LX/Eqo;->a:LX/Equ;

    iget-object v1, v1, LX/Equ;->e:LX/0Vd;

    iget-object v2, p0, LX/Eqo;->a:LX/Equ;

    iget-object v2, v2, LX/Equ;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2172038
    invoke-static {v1, v2}, LX/Equ;->a(LX/0Vd;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2172039
    iget-object v0, p0, LX/Eqo;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->n:LX/Eqg;

    invoke-virtual {v0}, LX/Eqg;->a()V

    .line 2172040
    iget-object v0, p0, LX/Eqo;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->x:LX/ErU;

    invoke-virtual {v0}, LX/ErU;->b()V

    .line 2172041
    iget-object v0, p0, LX/Eqo;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->u:LX/03V;

    sget-object v1, LX/Equ;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch facebook friends"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2172042
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2172043
    move-object v1, v1

    .line 2172044
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2172045
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2172046
    :try_start_0
    iget-object v0, p0, LX/Eqo;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    const v1, 0x16e8d5d7

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2172047
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v2, v1

    .line 2172048
    :try_start_1
    iget-object v1, p0, LX/Eqo;->a:LX/Equ;

    iget-object v1, v1, LX/Equ;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    const v3, 0x7e116c8

    invoke-static {v1, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2172049
    :goto_0
    iget-object v2, p0, LX/Eqo;->a:LX/Equ;

    iget-object v2, v2, LX/Equ;->n:LX/Eqg;

    const/4 v5, 0x0

    .line 2172050
    iget-object v3, v2, LX/Eqg;->b:LX/0Px;

    if-nez v3, :cond_0

    .line 2172051
    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    iput-object v3, v2, LX/Eqg;->b:LX/0Px;

    .line 2172052
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p0

    .line 2172053
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p1

    move v4, v5

    :goto_1
    if-ge v4, p1, :cond_2

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2172054
    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2172055
    if-eqz v3, :cond_1

    .line 2172056
    invoke-virtual {p0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2172057
    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2172058
    :cond_2
    iget-object v3, v2, LX/Eqg;->b:LX/0Px;

    invoke-static {v2, v3}, LX/Eqg;->b(LX/Eqg;LX/0Px;)V

    .line 2172059
    iget-object v3, v2, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v3, v3, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    iget-object v4, v2, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v4, v4, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->s:LX/0Px;

    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    invoke-virtual {v3, v5, v4, p0}, LX/ErB;->a(ZLX/0Px;LX/0Px;)V

    .line 2172060
    :goto_2
    return-void

    .line 2172061
    :catch_0
    move-exception v0

    .line 2172062
    :goto_3
    invoke-virtual {p0, v0}, LX/Eqo;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2172063
    :catch_1
    move-exception v1

    .line 2172064
    :goto_4
    iget-object v3, p0, LX/Eqo;->a:LX/Equ;

    iget-object v3, v3, LX/Equ;->u:LX/03V;

    sget-object v4, LX/Equ;->a:Ljava/lang/String;

    const-string v5, "Failed to fetch suggested facebook friends"

    invoke-static {v4, v5}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v4

    .line 2172065
    iput-object v1, v4, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2172066
    move-object v1, v4

    .line 2172067
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/03V;->a(LX/0VG;)V

    move-object v1, v2

    goto :goto_0

    .line 2172068
    :catch_2
    move-exception v1

    goto :goto_4

    .line 2172069
    :catch_3
    move-exception v0

    goto :goto_3
.end method
