.class public LX/DS6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DS5;


# instance fields
.field private final a:I

.field private final b:I

.field private c:LX/DRg;

.field public d:LX/DSZ;

.field private e:LX/DRi;

.field public f:Ljava/lang/String;

.field private g:LX/DRk;

.field public h:Ljava/lang/String;

.field private i:Z


# direct methods
.method public constructor <init>(LX/DRg;LX/DSZ;LX/DRi;LX/DRk;Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p1    # LX/DRg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/DSZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/DRi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/DRk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1999157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1999158
    iput-object p1, p0, LX/DS6;->c:LX/DRg;

    .line 1999159
    iput-object p2, p0, LX/DS6;->d:LX/DSZ;

    .line 1999160
    iput-object p3, p0, LX/DS6;->e:LX/DRi;

    .line 1999161
    iput-object p4, p0, LX/DS6;->g:LX/DRk;

    .line 1999162
    invoke-virtual {p8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1f96

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/DS6;->a:I

    .line 1999163
    invoke-virtual {p8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0843

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/DS6;->b:I

    .line 1999164
    iput-object p5, p0, LX/DS6;->f:Ljava/lang/String;

    .line 1999165
    iput-object p7, p0, LX/DS6;->h:Ljava/lang/String;

    .line 1999166
    iput-boolean p6, p0, LX/DS6;->i:Z

    .line 1999167
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1999212
    iget v0, p0, LX/DS6;->a:I

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1999213
    sget-object v0, LX/DS4;->a:[I

    invoke-static {p1}, LX/DSa;->fromOrdinal(I)LX/DSa;

    move-result-object v1

    invoke-virtual {v1}, LX/DSa;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1999214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1999215
    :pswitch_0
    new-instance v0, LX/DTt;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/DS6;->c:LX/DRg;

    iget-boolean v3, p0, LX/DS6;->i:Z

    invoke-direct {v0, v1, v2, v3}, LX/DTt;-><init>(Landroid/content/Context;LX/DRg;Z)V

    .line 1999216
    :goto_0
    return-object v0

    .line 1999217
    :pswitch_1
    new-instance v0, LX/DSs;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DSs;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 1999218
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1999219
    const v1, 0x7f03081f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1999220
    :pswitch_3
    new-instance v0, LX/DSj;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DSj;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 1999221
    :pswitch_4
    new-instance v0, LX/DSn;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DSn;-><init>(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 11

    .prologue
    .line 1999169
    invoke-static {p3}, LX/DSa;->fromOrdinal(I)LX/DSa;

    move-result-object v0

    .line 1999170
    sget-object v1, LX/DS4;->a:[I

    invoke-virtual {v0}, LX/DSa;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1999171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1999172
    :pswitch_0
    check-cast p2, LX/DSs;

    check-cast p1, LX/DSW;

    .line 1999173
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1999174
    iget p4, p1, LX/DSW;->a:I

    invoke-virtual {v0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p4

    move-object v0, p4

    .line 1999175
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 1999176
    iget-object p1, p2, LX/DSs;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1999177
    const/4 p1, 0x1

    invoke-static {p2, p1}, LX/DSs;->setViewVisibility(LX/DSs;Z)V

    .line 1999178
    :goto_0
    return-void

    .line 1999179
    :pswitch_1
    check-cast p2, LX/DTt;

    check-cast p1, LX/DSf;

    .line 1999180
    iget-object v3, p1, LX/DSf;->d:LX/DUV;

    move-object v3, v3

    .line 1999181
    invoke-interface {v3}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v3

    .line 1999182
    sget-object v6, LX/DSb;->NOT_ADMIN:LX/DSb;

    .line 1999183
    iget-object v4, p0, LX/DS6;->d:LX/DSZ;

    invoke-virtual {v4, v3}, LX/DSZ;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1999184
    sget-object v6, LX/DSb;->ADMIN:LX/DSb;

    .line 1999185
    :cond_0
    :goto_1
    iget-object v4, p0, LX/DS6;->d:LX/DSZ;

    .line 1999186
    iget-boolean v5, v4, LX/DSZ;->e:Z

    move v5, v5

    .line 1999187
    iget-object v4, p0, LX/DS6;->d:LX/DSZ;

    .line 1999188
    iget-boolean v7, v4, LX/DSZ;->f:Z

    move v7, v7

    .line 1999189
    iget-object v4, p0, LX/DS6;->d:LX/DSZ;

    invoke-virtual {v4, v3}, LX/DSZ;->b(Ljava/lang/String;)Z

    move-result v8

    iget-object v3, p0, LX/DS6;->d:LX/DSZ;

    iget-object v4, p0, LX/DS6;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/DSZ;->c(Ljava/lang/String;)Z

    move-result v9

    iget-object v10, p0, LX/DS6;->f:Ljava/lang/String;

    move-object v3, p2

    move-object v4, p1

    invoke-virtual/range {v3 .. v10}, LX/DTt;->a(LX/DSf;ZLX/DSb;ZZZLjava/lang/String;)V

    .line 1999190
    goto :goto_0

    .line 1999191
    :pswitch_2
    iget-object v0, p0, LX/DS6;->d:LX/DSZ;

    .line 1999192
    iget-boolean p0, v0, LX/DSZ;->d:Z

    move v0, p0

    .line 1999193
    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1999194
    goto :goto_0

    .line 1999195
    :pswitch_3
    check-cast p2, LX/DSj;

    const/4 v0, 0x1

    check-cast p1, LX/DSg;

    .line 1999196
    iget-object v1, p1, LX/DSg;->a:LX/DSe;

    move-object v1, v1

    .line 1999197
    iget-object v2, p0, LX/DS6;->e:LX/DRi;

    const/4 v4, 0x0

    .line 1999198
    if-eqz v0, :cond_4

    move v3, v4

    :goto_3
    invoke-virtual {p2, v3}, LX/DSj;->setVisibility(I)V

    .line 1999199
    iget-object v3, p2, LX/DSj;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1999200
    new-instance v3, LX/DSi;

    invoke-direct {v3, p2, v2, v1}, LX/DSi;-><init>(LX/DSj;LX/DRi;LX/DSe;)V

    invoke-virtual {p2, v3}, LX/DSj;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1999201
    goto :goto_0

    .line 1999202
    :pswitch_4
    check-cast p2, LX/DSn;

    iget-object v0, p0, LX/DS6;->g:LX/DRk;

    .line 1999203
    iget-object v1, p2, LX/DSn;->a:Lcom/facebook/fig/button/FigButton;

    new-instance v2, LX/DSl;

    invoke-direct {v2, p2, v0}, LX/DSl;-><init>(LX/DSn;LX/DRk;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1999204
    goto :goto_0

    .line 1999205
    :cond_1
    const/4 p1, 0x0

    invoke-static {p2, p1}, LX/DSs;->setViewVisibility(LX/DSs;Z)V

    goto :goto_0

    .line 1999206
    :cond_2
    iget-object v4, p0, LX/DS6;->d:LX/DSZ;

    .line 1999207
    iget-object v5, v4, LX/DSZ;->b:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    move v4, v5

    .line 1999208
    if-eqz v4, :cond_0

    .line 1999209
    sget-object v6, LX/DSb;->MODERATOR:LX/DSb;

    goto :goto_1

    .line 1999210
    :cond_3
    const/16 v0, 0x8

    goto :goto_2

    .line 1999211
    :cond_4
    const/16 v3, 0x8

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1999168
    iget v0, p0, LX/DS6;->b:I

    return v0
.end method
