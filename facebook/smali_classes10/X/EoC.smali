.class public final LX/EoC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/EoA;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/EoA;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 2167972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167973
    iput-object p1, p0, LX/EoC;->a:LX/0QB;

    .line 2167974
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2167975
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/EoC;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2167976
    packed-switch p2, :pswitch_data_0

    .line 2167977
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2167978
    :pswitch_0
    invoke-static {p1}, LX/EzQ;->a(LX/0QB;)LX/EzQ;

    move-result-object v0

    .line 2167979
    :goto_0
    return-object v0

    .line 2167980
    :pswitch_1
    invoke-static {p1}, LX/IFM;->a(LX/0QB;)LX/IFM;

    move-result-object v0

    goto :goto_0

    .line 2167981
    :pswitch_2
    invoke-static {p1}, LX/Jxw;->a(LX/0QB;)LX/Jxw;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2167982
    const/4 v0, 0x3

    return v0
.end method
