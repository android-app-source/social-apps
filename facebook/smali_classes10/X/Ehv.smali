.class public final LX/Ehv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Landroid/app/Dialog;

.field public final synthetic b:Lcom/facebook/common/intent/AppChooserDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/common/intent/AppChooserDialogFragment;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 2158883
    iput-object p1, p0, LX/Ehv;->b:Lcom/facebook/common/intent/AppChooserDialogFragment;

    iput-object p2, p0, LX/Ehv;->a:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2158884
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ei2;

    .line 2158885
    iget-object v1, p0, LX/Ehv;->b:Lcom/facebook/common/intent/AppChooserDialogFragment;

    iget-object v1, v1, Lcom/facebook/common/intent/AppChooserDialogFragment;->o:LX/Ei1;

    .line 2158886
    iget-object v2, v0, LX/Ei2;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2158887
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 2158888
    const-string p2, "package_name"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2158889
    iget-object p2, v1, LX/Ei1;->c:Ljava/util/Map;

    if-eqz p2, :cond_0

    .line 2158890
    iget-object p2, v1, LX/Ei1;->c:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2158891
    :cond_0
    iget-object p2, v1, LX/Ei1;->b:LX/0Zb;

    new-instance p3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p4, LX/Ei0;->SELECTED:LX/Ei0;

    iget-object p4, p4, LX/Ei0;->eventName:Ljava/lang/String;

    invoke-direct {p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object p4, v1, LX/Ei1;->a:Ljava/lang/String;

    .line 2158892
    iput-object p4, p3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2158893
    move-object p3, p3

    .line 2158894
    invoke-virtual {p3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2158895
    iget-object v1, p0, LX/Ehv;->b:Lcom/facebook/common/intent/AppChooserDialogFragment;

    iget-object v1, v1, Lcom/facebook/common/intent/AppChooserDialogFragment;->n:Lcom/facebook/content/SecureContextHelper;

    .line 2158896
    iget-object v2, v0, LX/Ei2;->d:Landroid/content/Intent;

    move-object v0, v2

    .line 2158897
    iget-object v2, p0, LX/Ehv;->b:Lcom/facebook/common/intent/AppChooserDialogFragment;

    iget-object v2, v2, Lcom/facebook/common/intent/AppChooserDialogFragment;->p:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2158898
    iget-object v0, p0, LX/Ehv;->a:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 2158899
    return-void
.end method
