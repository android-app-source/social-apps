.class public final LX/DvB;
.super LX/Dv8;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

.field public final d:LX/DvW;

.field public final e:Lcom/facebook/graphql/model/GraphQLPhoto;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/graphql/model/GraphQLVideo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Lcom/facebook/graphql/model/GraphQLPhoto;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2058098
    invoke-direct {p0}, LX/Dv8;-><init>()V

    .line 2058099
    iput-object p1, p0, LX/DvB;->a:Ljava/lang/String;

    .line 2058100
    iput-object p2, p0, LX/DvB;->b:Landroid/net/Uri;

    .line 2058101
    iput-object p3, p0, LX/DvB;->c:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    .line 2058102
    iput-object p4, p0, LX/DvB;->d:LX/DvW;

    .line 2058103
    iput-object p5, p0, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2058104
    const/4 v0, 0x0

    iput-object v0, p0, LX/DvB;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 2058105
    iput-object p6, p0, LX/DvB;->g:Ljava/lang/String;

    .line 2058106
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Lcom/facebook/graphql/model/GraphQLVideo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2058107
    invoke-direct {p0}, LX/Dv8;-><init>()V

    .line 2058108
    iput-object p1, p0, LX/DvB;->a:Ljava/lang/String;

    .line 2058109
    iput-object v0, p0, LX/DvB;->b:Landroid/net/Uri;

    .line 2058110
    iput-object p2, p0, LX/DvB;->c:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    .line 2058111
    iput-object p3, p0, LX/DvB;->d:LX/DvW;

    .line 2058112
    iput-object v0, p0, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2058113
    iput-object p4, p0, LX/DvB;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 2058114
    iput-object v0, p0, LX/DvB;->g:Ljava/lang/String;

    .line 2058115
    return-void
.end method
