.class public final LX/Cma;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Z4;


# instance fields
.field public final synthetic a:LX/Cmb;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

.field private final d:I

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentInlineStyleRange;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentText$EntityRanges;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Cmb;Lcom/facebook/graphql/enums/GraphQLComposedBlockType;Ljava/lang/String;ILX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLComposedBlockType;",
            "Ljava/lang/String;",
            "I",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentInlineStyleRange;",
            ">;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentText$EntityRanges;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1933468
    iput-object p1, p0, LX/Cma;->a:LX/Cmb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933469
    iput-object p2, p0, LX/Cma;->c:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    .line 1933470
    iput-object p3, p0, LX/Cma;->b:Ljava/lang/String;

    .line 1933471
    iput p4, p0, LX/Cma;->d:I

    .line 1933472
    iput-object p5, p0, LX/Cma;->e:LX/0Px;

    .line 1933473
    iput-object p6, p0, LX/Cma;->f:LX/0Px;

    .line 1933474
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933475
    iget-object v0, p0, LX/Cma;->a:LX/Cmb;

    iget-object v0, v0, LX/Cmb;->b:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentText$EntityRanges;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1933476
    iget-object v0, p0, LX/Cma;->a:LX/Cmb;

    iget-object v0, v0, LX/Cmb;->e:LX/0Px;

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentInlineStyleRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1933477
    iget-object v0, p0, LX/Cma;->a:LX/Cmb;

    iget-object v0, v0, LX/Cmb;->d:LX/0Px;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933478
    iget-object v0, p0, LX/Cma;->a:LX/Cmb;

    iget-object v0, v0, LX/Cmb;->a:Ljava/lang/String;

    return-object v0
.end method
