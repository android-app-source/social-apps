.class public final enum LX/EPu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EPu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EPu;

.field public static final enum MEMORY:LX/EPu;

.field public static final enum NETWORK_BYPASS_CACHE_ONLY:LX/EPu;

.field public static final enum NETWORK_ONLY:LX/EPu;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2118038
    new-instance v0, LX/EPu;

    const-string v1, "MEMORY"

    invoke-direct {v0, v1, v2}, LX/EPu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EPu;->MEMORY:LX/EPu;

    .line 2118039
    new-instance v0, LX/EPu;

    const-string v1, "NETWORK_ONLY"

    invoke-direct {v0, v1, v3}, LX/EPu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EPu;->NETWORK_ONLY:LX/EPu;

    .line 2118040
    new-instance v0, LX/EPu;

    const-string v1, "NETWORK_BYPASS_CACHE_ONLY"

    invoke-direct {v0, v1, v4}, LX/EPu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EPu;->NETWORK_BYPASS_CACHE_ONLY:LX/EPu;

    .line 2118041
    const/4 v0, 0x3

    new-array v0, v0, [LX/EPu;

    sget-object v1, LX/EPu;->MEMORY:LX/EPu;

    aput-object v1, v0, v2

    sget-object v1, LX/EPu;->NETWORK_ONLY:LX/EPu;

    aput-object v1, v0, v3

    sget-object v1, LX/EPu;->NETWORK_BYPASS_CACHE_ONLY:LX/EPu;

    aput-object v1, v0, v4

    sput-object v0, LX/EPu;->$VALUES:[LX/EPu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2118042
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EPu;
    .locals 1

    .prologue
    .line 2118037
    const-class v0, LX/EPu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EPu;

    return-object v0
.end method

.method public static values()[LX/EPu;
    .locals 1

    .prologue
    .line 2118036
    sget-object v0, LX/EPu;->$VALUES:[LX/EPu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EPu;

    return-object v0
.end method
