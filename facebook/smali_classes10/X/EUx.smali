.class public final LX/EUx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final d:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/VideoChannelFollowSurfaces;
    .end annotation
.end field

.field public final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Z)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelFollowSurfaces;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2126903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2126904
    iput-object p1, p0, LX/EUx;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126905
    iput-object p2, p0, LX/EUx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2126906
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2126907
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/EUx;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2126908
    iput-object p3, p0, LX/EUx;->d:Ljava/lang/String;

    .line 2126909
    iput-boolean p4, p0, LX/EUx;->e:Z

    .line 2126910
    return-void
.end method
