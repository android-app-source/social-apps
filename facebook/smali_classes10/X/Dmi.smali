.class public final LX/Dmi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

.field public final synthetic c:LX/Dmn;


# direct methods
.method public constructor <init>(LX/Dmn;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V
    .locals 0

    .prologue
    .line 2039607
    iput-object p1, p0, LX/Dmi;->c:LX/Dmn;

    iput-object p2, p0, LX/Dmi;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Dmi;->b:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x4d9fd4a1    # 3.35189024E8f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2039608
    iget-object v0, p0, LX/Dmi;->c:LX/Dmn;

    iget-object v0, v0, LX/Dmn;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2039609
    iget-boolean v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v3

    .line 2039610
    if-nez v0, :cond_0

    .line 2039611
    const v0, -0x4faebc8f

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2039612
    :goto_0
    return-void

    .line 2039613
    :cond_0
    iget-object v0, p0, LX/Dmi;->a:Ljava/lang/String;

    invoke-static {v0}, LX/DkQ;->a(Ljava/lang/String;)LX/DkQ;

    move-result-object v2

    .line 2039614
    iget-object v0, p0, LX/Dmi;->c:LX/Dmn;

    iget-object v3, v0, LX/Dmn;->b:Landroid/content/Context;

    iget-object v0, p0, LX/Dmi;->c:LX/Dmn;

    iget-object v0, v0, LX/Dmn;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v4, p0, LX/Dmi;->b:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v0, v4}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2039615
    iget-object v2, p0, LX/Dmi;->c:LX/Dmn;

    iget-object v2, v2, LX/Dmn;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Dmi;->c:LX/Dmn;

    iget-object v3, v3, LX/Dmn;->b:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2039616
    iget-object v0, p0, LX/Dmi;->c:LX/Dmn;

    iget-object v0, v0, LX/Dmn;->d:LX/Dih;

    iget-object v2, p0, LX/Dmi;->b:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/Dih;->a(Ljava/lang/String;)V

    .line 2039617
    const v0, 0xa72856e

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
