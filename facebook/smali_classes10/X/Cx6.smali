.class public LX/Cx6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cx5;


# instance fields
.field private final a:LX/CzB;

.field private final b:LX/CxG;


# direct methods
.method public constructor <init>(LX/CzB;LX/CxG;)V
    .locals 0
    .param p1    # LX/CzB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CxG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951416
    iput-object p1, p0, LX/Cx6;->a:LX/CzB;

    .line 1951417
    iput-object p2, p0, LX/Cx6;->b:LX/CxG;

    .line 1951418
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1951419
    iget-object v0, p0, LX/Cx6;->b:LX/CxG;

    invoke-interface {v0, p1}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1951420
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1951421
    :goto_0
    return-void

    .line 1951422
    :cond_0
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v1, :cond_1

    .line 1951423
    iget-object v1, p0, LX/Cx6;->a:LX/CzB;

    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    .line 1951424
    iget-object v2, v1, LX/CzB;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 1951425
    new-instance p0, LX/4Wi;

    invoke-direct {p0}, LX/4Wi;-><init>()V

    .line 1951426
    invoke-virtual {v2}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1951427
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, LX/4Wi;->b:Ljava/lang/String;

    .line 1951428
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->m()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, LX/4Wi;->c:Ljava/lang/String;

    .line 1951429
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p1

    iput-object p1, p0, LX/4Wi;->d:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1951430
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object p1

    iput-object p1, p0, LX/4Wi;->e:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 1951431
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->l()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, LX/4Wi;->f:Ljava/lang/String;

    .line 1951432
    invoke-static {p0, v2}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 1951433
    move-object v2, p0

    .line 1951434
    iput-object p2, v2, LX/4Wi;->d:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1951435
    move-object v2, v2

    .line 1951436
    new-instance p0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    invoke-direct {p0, v2}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;-><init>(LX/4Wi;)V

    .line 1951437
    move-object v2, p0

    .line 1951438
    iget-object p0, v1, LX/CzB;->a:Ljava/util/List;

    invoke-interface {p0, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1951439
    goto :goto_0

    .line 1951440
    :cond_1
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    if-eqz v1, :cond_2

    .line 1951441
    iget-object v1, p0, LX/Cx6;->a:LX/CzB;

    check-cast p2, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 1951442
    iget-object v2, v1, LX/CzB;->a:Ljava/util/List;

    invoke-interface {v2, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1951443
    goto :goto_0

    .line 1951444
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported item: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
