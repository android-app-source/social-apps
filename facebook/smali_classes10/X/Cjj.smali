.class public final LX/Cjj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "Lcom/facebook/richdocument/genesis/BlockCreator;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "Lcom/facebook/richdocument/genesis/BlockCreator;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1929720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929721
    iput-object p1, p0, LX/Cjj;->a:LX/0QB;

    .line 1929722
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1929723
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/Cjj;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1929469
    packed-switch p2, :pswitch_data_0

    .line 1929470
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1929471
    :pswitch_0
    new-instance v0, LX/I3j;

    invoke-direct {v0}, LX/I3j;-><init>()V

    .line 1929472
    move-object v0, v0

    .line 1929473
    move-object v0, v0

    .line 1929474
    :goto_0
    return-object v0

    .line 1929475
    :pswitch_1
    new-instance v1, LX/I3k;

    invoke-static {p1}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v0

    check-cast v0, LX/Blh;

    invoke-direct {v1, v0}, LX/I3k;-><init>(LX/Blh;)V

    .line 1929476
    move-object v0, v1

    .line 1929477
    goto :goto_0

    .line 1929478
    :pswitch_2
    new-instance v0, LX/I3l;

    invoke-direct {v0}, LX/I3l;-><init>()V

    .line 1929479
    move-object v0, v0

    .line 1929480
    move-object v0, v0

    .line 1929481
    goto :goto_0

    .line 1929482
    :pswitch_3
    new-instance v0, LX/I3m;

    invoke-direct {v0}, LX/I3m;-><init>()V

    .line 1929483
    move-object v0, v0

    .line 1929484
    move-object v0, v0

    .line 1929485
    goto :goto_0

    .line 1929486
    :pswitch_4
    new-instance v0, LX/I3n;

    invoke-direct {v0}, LX/I3n;-><init>()V

    .line 1929487
    move-object v0, v0

    .line 1929488
    move-object v0, v0

    .line 1929489
    goto :goto_0

    .line 1929490
    :pswitch_5
    new-instance v0, LX/IXR;

    invoke-direct {v0}, LX/IXR;-><init>()V

    .line 1929491
    move-object v0, v0

    .line 1929492
    move-object v0, v0

    .line 1929493
    goto :goto_0

    .line 1929494
    :pswitch_6
    new-instance v0, LX/IXS;

    invoke-direct {v0}, LX/IXS;-><init>()V

    .line 1929495
    move-object v0, v0

    .line 1929496
    move-object v0, v0

    .line 1929497
    goto :goto_0

    .line 1929498
    :pswitch_7
    new-instance v0, LX/IXT;

    invoke-direct {v0}, LX/IXT;-><init>()V

    .line 1929499
    move-object v0, v0

    .line 1929500
    move-object v0, v0

    .line 1929501
    goto :goto_0

    .line 1929502
    :pswitch_8
    new-instance v0, LX/Gnc;

    invoke-direct {v0}, LX/Gnc;-><init>()V

    .line 1929503
    move-object v0, v0

    .line 1929504
    move-object v0, v0

    .line 1929505
    goto :goto_0

    .line 1929506
    :pswitch_9
    new-instance v0, LX/Gnd;

    invoke-direct {v0}, LX/Gnd;-><init>()V

    .line 1929507
    move-object v0, v0

    .line 1929508
    move-object v0, v0

    .line 1929509
    goto :goto_0

    .line 1929510
    :pswitch_a
    new-instance v0, LX/Gne;

    invoke-direct {v0}, LX/Gne;-><init>()V

    .line 1929511
    move-object v0, v0

    .line 1929512
    move-object v0, v0

    .line 1929513
    goto :goto_0

    .line 1929514
    :pswitch_b
    new-instance v0, LX/Gnf;

    invoke-direct {v0}, LX/Gnf;-><init>()V

    .line 1929515
    move-object v0, v0

    .line 1929516
    move-object v0, v0

    .line 1929517
    goto :goto_0

    .line 1929518
    :pswitch_c
    new-instance v0, LX/Gng;

    invoke-direct {v0}, LX/Gng;-><init>()V

    .line 1929519
    move-object v0, v0

    .line 1929520
    move-object v0, v0

    .line 1929521
    goto :goto_0

    .line 1929522
    :pswitch_d
    new-instance v0, LX/Gnh;

    invoke-direct {v0}, LX/Gnh;-><init>()V

    .line 1929523
    move-object v0, v0

    .line 1929524
    move-object v0, v0

    .line 1929525
    goto :goto_0

    .line 1929526
    :pswitch_e
    new-instance v0, LX/Gni;

    invoke-direct {v0}, LX/Gni;-><init>()V

    .line 1929527
    move-object v0, v0

    .line 1929528
    move-object v0, v0

    .line 1929529
    goto :goto_0

    .line 1929530
    :pswitch_f
    new-instance v0, LX/Gnj;

    invoke-direct {v0}, LX/Gnj;-><init>()V

    .line 1929531
    move-object v0, v0

    .line 1929532
    move-object v0, v0

    .line 1929533
    goto :goto_0

    .line 1929534
    :pswitch_10
    new-instance v0, LX/Gnk;

    invoke-direct {v0}, LX/Gnk;-><init>()V

    .line 1929535
    move-object v0, v0

    .line 1929536
    move-object v0, v0

    .line 1929537
    goto/16 :goto_0

    .line 1929538
    :pswitch_11
    new-instance v0, LX/Gnl;

    invoke-direct {v0}, LX/Gnl;-><init>()V

    .line 1929539
    move-object v0, v0

    .line 1929540
    move-object v0, v0

    .line 1929541
    goto/16 :goto_0

    .line 1929542
    :pswitch_12
    new-instance v0, LX/Gnm;

    invoke-direct {v0}, LX/Gnm;-><init>()V

    .line 1929543
    move-object v0, v0

    .line 1929544
    move-object v0, v0

    .line 1929545
    goto/16 :goto_0

    .line 1929546
    :pswitch_13
    new-instance v0, LX/Gnn;

    invoke-direct {v0}, LX/Gnn;-><init>()V

    .line 1929547
    move-object v0, v0

    .line 1929548
    move-object v0, v0

    .line 1929549
    goto/16 :goto_0

    .line 1929550
    :pswitch_14
    new-instance v0, LX/Gno;

    invoke-direct {v0}, LX/Gno;-><init>()V

    .line 1929551
    move-object v0, v0

    .line 1929552
    move-object v0, v0

    .line 1929553
    goto/16 :goto_0

    .line 1929554
    :pswitch_15
    new-instance v0, LX/Gnp;

    invoke-direct {v0}, LX/Gnp;-><init>()V

    .line 1929555
    move-object v0, v0

    .line 1929556
    move-object v0, v0

    .line 1929557
    goto/16 :goto_0

    .line 1929558
    :pswitch_16
    new-instance v0, LX/Gnq;

    invoke-direct {v0}, LX/Gnq;-><init>()V

    .line 1929559
    move-object v0, v0

    .line 1929560
    move-object v0, v0

    .line 1929561
    goto/16 :goto_0

    .line 1929562
    :pswitch_17
    new-instance v0, LX/Gnr;

    invoke-direct {v0}, LX/Gnr;-><init>()V

    .line 1929563
    move-object v0, v0

    .line 1929564
    move-object v0, v0

    .line 1929565
    goto/16 :goto_0

    .line 1929566
    :pswitch_18
    new-instance v0, LX/Gns;

    invoke-direct {v0}, LX/Gns;-><init>()V

    .line 1929567
    move-object v0, v0

    .line 1929568
    move-object v0, v0

    .line 1929569
    goto/16 :goto_0

    .line 1929570
    :pswitch_19
    new-instance v0, LX/Gnt;

    invoke-direct {v0}, LX/Gnt;-><init>()V

    .line 1929571
    move-object v0, v0

    .line 1929572
    move-object v0, v0

    .line 1929573
    goto/16 :goto_0

    .line 1929574
    :pswitch_1a
    new-instance v0, LX/Gnu;

    invoke-direct {v0}, LX/Gnu;-><init>()V

    .line 1929575
    move-object v0, v0

    .line 1929576
    move-object v0, v0

    .line 1929577
    goto/16 :goto_0

    .line 1929578
    :pswitch_1b
    new-instance v0, LX/Gnv;

    invoke-direct {v0}, LX/Gnv;-><init>()V

    .line 1929579
    move-object v0, v0

    .line 1929580
    move-object v0, v0

    .line 1929581
    goto/16 :goto_0

    .line 1929582
    :pswitch_1c
    new-instance v0, LX/Gnw;

    invoke-direct {v0}, LX/Gnw;-><init>()V

    .line 1929583
    move-object v0, v0

    .line 1929584
    move-object v0, v0

    .line 1929585
    goto/16 :goto_0

    .line 1929586
    :pswitch_1d
    new-instance v0, LX/Jto;

    invoke-direct {v0}, LX/Jto;-><init>()V

    .line 1929587
    move-object v0, v0

    .line 1929588
    move-object v0, v0

    .line 1929589
    goto/16 :goto_0

    .line 1929590
    :pswitch_1e
    new-instance v0, LX/Jtp;

    invoke-direct {v0}, LX/Jtp;-><init>()V

    .line 1929591
    move-object v0, v0

    .line 1929592
    move-object v0, v0

    .line 1929593
    goto/16 :goto_0

    .line 1929594
    :pswitch_1f
    new-instance v0, LX/Jtq;

    invoke-direct {v0}, LX/Jtq;-><init>()V

    .line 1929595
    move-object v0, v0

    .line 1929596
    move-object v0, v0

    .line 1929597
    goto/16 :goto_0

    .line 1929598
    :pswitch_20
    new-instance v0, LX/CjI;

    invoke-direct {v0}, LX/CjI;-><init>()V

    .line 1929599
    move-object v0, v0

    .line 1929600
    move-object v0, v0

    .line 1929601
    goto/16 :goto_0

    .line 1929602
    :pswitch_21
    new-instance v0, LX/CjJ;

    invoke-direct {v0}, LX/CjJ;-><init>()V

    .line 1929603
    move-object v0, v0

    .line 1929604
    move-object v0, v0

    .line 1929605
    goto/16 :goto_0

    .line 1929606
    :pswitch_22
    new-instance v0, LX/CjM;

    invoke-direct {v0}, LX/CjM;-><init>()V

    .line 1929607
    move-object v0, v0

    .line 1929608
    move-object v0, v0

    .line 1929609
    goto/16 :goto_0

    .line 1929610
    :pswitch_23
    new-instance v0, LX/CjN;

    invoke-direct {v0}, LX/CjN;-><init>()V

    .line 1929611
    move-object v0, v0

    .line 1929612
    move-object v0, v0

    .line 1929613
    goto/16 :goto_0

    .line 1929614
    :pswitch_24
    new-instance v0, LX/CjO;

    invoke-direct {v0}, LX/CjO;-><init>()V

    .line 1929615
    move-object v0, v0

    .line 1929616
    move-object v0, v0

    .line 1929617
    goto/16 :goto_0

    .line 1929618
    :pswitch_25
    new-instance v0, LX/CjP;

    invoke-direct {v0}, LX/CjP;-><init>()V

    .line 1929619
    move-object v0, v0

    .line 1929620
    move-object v0, v0

    .line 1929621
    goto/16 :goto_0

    .line 1929622
    :pswitch_26
    new-instance v0, LX/CjQ;

    invoke-direct {v0}, LX/CjQ;-><init>()V

    .line 1929623
    move-object v0, v0

    .line 1929624
    move-object v0, v0

    .line 1929625
    goto/16 :goto_0

    .line 1929626
    :pswitch_27
    new-instance v0, LX/CjR;

    invoke-direct {v0}, LX/CjR;-><init>()V

    .line 1929627
    move-object v0, v0

    .line 1929628
    move-object v0, v0

    .line 1929629
    goto/16 :goto_0

    .line 1929630
    :pswitch_28
    new-instance v0, LX/CjS;

    invoke-direct {v0}, LX/CjS;-><init>()V

    .line 1929631
    move-object v0, v0

    .line 1929632
    move-object v0, v0

    .line 1929633
    goto/16 :goto_0

    .line 1929634
    :pswitch_29
    new-instance v0, LX/CjT;

    invoke-direct {v0}, LX/CjT;-><init>()V

    .line 1929635
    move-object v0, v0

    .line 1929636
    move-object v0, v0

    .line 1929637
    goto/16 :goto_0

    .line 1929638
    :pswitch_2a
    new-instance v0, LX/CjU;

    invoke-direct {v0}, LX/CjU;-><init>()V

    .line 1929639
    move-object v0, v0

    .line 1929640
    move-object v0, v0

    .line 1929641
    goto/16 :goto_0

    .line 1929642
    :pswitch_2b
    new-instance v0, LX/CjV;

    invoke-direct {v0}, LX/CjV;-><init>()V

    .line 1929643
    move-object v0, v0

    .line 1929644
    move-object v0, v0

    .line 1929645
    goto/16 :goto_0

    .line 1929646
    :pswitch_2c
    new-instance v0, LX/CjW;

    invoke-direct {v0}, LX/CjW;-><init>()V

    .line 1929647
    move-object v0, v0

    .line 1929648
    move-object v0, v0

    .line 1929649
    goto/16 :goto_0

    .line 1929650
    :pswitch_2d
    new-instance v0, LX/CjX;

    invoke-direct {v0}, LX/CjX;-><init>()V

    .line 1929651
    move-object v0, v0

    .line 1929652
    move-object v0, v0

    .line 1929653
    goto/16 :goto_0

    .line 1929654
    :pswitch_2e
    new-instance v0, LX/CjY;

    invoke-direct {v0}, LX/CjY;-><init>()V

    .line 1929655
    move-object v0, v0

    .line 1929656
    move-object v0, v0

    .line 1929657
    goto/16 :goto_0

    .line 1929658
    :pswitch_2f
    new-instance v0, LX/CjZ;

    invoke-direct {v0}, LX/CjZ;-><init>()V

    .line 1929659
    move-object v0, v0

    .line 1929660
    move-object v0, v0

    .line 1929661
    goto/16 :goto_0

    .line 1929662
    :pswitch_30
    new-instance v0, LX/Cja;

    invoke-direct {v0}, LX/Cja;-><init>()V

    .line 1929663
    move-object v0, v0

    .line 1929664
    move-object v0, v0

    .line 1929665
    goto/16 :goto_0

    .line 1929666
    :pswitch_31
    new-instance v0, LX/Cjb;

    invoke-direct {v0}, LX/Cjb;-><init>()V

    .line 1929667
    move-object v0, v0

    .line 1929668
    move-object v0, v0

    .line 1929669
    goto/16 :goto_0

    .line 1929670
    :pswitch_32
    new-instance v1, LX/Cjc;

    invoke-static {p1}, LX/Crk;->a(LX/0QB;)LX/Crk;

    move-result-object v0

    check-cast v0, LX/Crk;

    invoke-direct {v1, v0}, LX/Cjc;-><init>(LX/Crk;)V

    .line 1929671
    move-object v0, v1

    .line 1929672
    goto/16 :goto_0

    .line 1929673
    :pswitch_33
    new-instance v0, LX/Cjd;

    invoke-direct {v0}, LX/Cjd;-><init>()V

    .line 1929674
    move-object v0, v0

    .line 1929675
    move-object v0, v0

    .line 1929676
    goto/16 :goto_0

    .line 1929677
    :pswitch_34
    new-instance v0, LX/Cje;

    invoke-direct {v0}, LX/Cje;-><init>()V

    .line 1929678
    move-object v0, v0

    .line 1929679
    move-object v0, v0

    .line 1929680
    goto/16 :goto_0

    .line 1929681
    :pswitch_35
    new-instance v0, LX/Cjf;

    invoke-direct {v0}, LX/Cjf;-><init>()V

    .line 1929682
    move-object v0, v0

    .line 1929683
    move-object v0, v0

    .line 1929684
    goto/16 :goto_0

    .line 1929685
    :pswitch_36
    new-instance v0, LX/Cjg;

    invoke-direct {v0}, LX/Cjg;-><init>()V

    .line 1929686
    move-object v0, v0

    .line 1929687
    move-object v0, v0

    .line 1929688
    goto/16 :goto_0

    .line 1929689
    :pswitch_37
    new-instance v0, LX/Cjh;

    invoke-direct {v0}, LX/Cjh;-><init>()V

    .line 1929690
    move-object v0, v0

    .line 1929691
    move-object v0, v0

    .line 1929692
    goto/16 :goto_0

    .line 1929693
    :pswitch_38
    new-instance v0, LX/Cji;

    invoke-direct {v0}, LX/Cji;-><init>()V

    .line 1929694
    move-object v0, v0

    .line 1929695
    move-object v0, v0

    .line 1929696
    goto/16 :goto_0

    .line 1929697
    :pswitch_39
    new-instance v0, LX/Cjk;

    invoke-direct {v0}, LX/Cjk;-><init>()V

    .line 1929698
    move-object v0, v0

    .line 1929699
    move-object v0, v0

    .line 1929700
    goto/16 :goto_0

    .line 1929701
    :pswitch_3a
    new-instance v0, LX/Cjl;

    invoke-direct {v0}, LX/Cjl;-><init>()V

    .line 1929702
    move-object v0, v0

    .line 1929703
    move-object v0, v0

    .line 1929704
    goto/16 :goto_0

    .line 1929705
    :pswitch_3b
    new-instance p0, LX/Cjm;

    invoke-static {p1}, LX/Crk;->a(LX/0QB;)LX/Crk;

    move-result-object v0

    check-cast v0, LX/Crk;

    invoke-static {p1}, LX/K2A;->b(LX/0QB;)LX/K29;

    move-result-object v1

    check-cast v1, LX/K29;

    invoke-direct {p0, v0, v1}, LX/Cjm;-><init>(LX/Crk;LX/K29;)V

    .line 1929706
    move-object v0, p0

    .line 1929707
    goto/16 :goto_0

    .line 1929708
    :pswitch_3c
    new-instance v0, LX/Cjn;

    invoke-direct {v0}, LX/Cjn;-><init>()V

    .line 1929709
    move-object v0, v0

    .line 1929710
    move-object v0, v0

    .line 1929711
    goto/16 :goto_0

    .line 1929712
    :pswitch_3d
    new-instance v0, LX/Cjo;

    invoke-direct {v0}, LX/Cjo;-><init>()V

    .line 1929713
    move-object v0, v0

    .line 1929714
    move-object v0, v0

    .line 1929715
    goto/16 :goto_0

    .line 1929716
    :pswitch_3e
    new-instance v0, LX/Cjp;

    invoke-direct {v0}, LX/Cjp;-><init>()V

    .line 1929717
    move-object v0, v0

    .line 1929718
    move-object v0, v0

    .line 1929719
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1929468
    const/16 v0, 0x3f

    return v0
.end method
