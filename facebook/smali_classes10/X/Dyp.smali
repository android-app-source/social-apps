.class public final enum LX/Dyp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dyp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dyp;

.field public static final enum FLAG:LX/Dyp;

.field public static final enum REPORT_DUPLICATES:LX/Dyp;

.field public static final enum SUGGEST_EDITS:LX/Dyp;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2064848
    new-instance v0, LX/Dyp;

    const-string v1, "SUGGEST_EDITS"

    invoke-direct {v0, v1, v2}, LX/Dyp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dyp;->SUGGEST_EDITS:LX/Dyp;

    .line 2064849
    new-instance v0, LX/Dyp;

    const-string v1, "REPORT_DUPLICATES"

    invoke-direct {v0, v1, v3}, LX/Dyp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dyp;->REPORT_DUPLICATES:LX/Dyp;

    .line 2064850
    new-instance v0, LX/Dyp;

    const-string v1, "FLAG"

    invoke-direct {v0, v1, v4}, LX/Dyp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dyp;->FLAG:LX/Dyp;

    .line 2064851
    const/4 v0, 0x3

    new-array v0, v0, [LX/Dyp;

    sget-object v1, LX/Dyp;->SUGGEST_EDITS:LX/Dyp;

    aput-object v1, v0, v2

    sget-object v1, LX/Dyp;->REPORT_DUPLICATES:LX/Dyp;

    aput-object v1, v0, v3

    sget-object v1, LX/Dyp;->FLAG:LX/Dyp;

    aput-object v1, v0, v4

    sput-object v0, LX/Dyp;->$VALUES:[LX/Dyp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2064853
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dyp;
    .locals 1

    .prologue
    .line 2064854
    const-class v0, LX/Dyp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dyp;

    return-object v0
.end method

.method public static values()[LX/Dyp;
    .locals 1

    .prologue
    .line 2064852
    sget-object v0, LX/Dyp;->$VALUES:[LX/Dyp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dyp;

    return-object v0
.end method
