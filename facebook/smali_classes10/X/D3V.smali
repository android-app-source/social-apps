.class public LX/D3V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:LX/1Ay;

.field private final b:LX/ClD;

.field private final c:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Z

.field private g:LX/D3U;

.field private h:D

.field private i:D

.field private j:D

.field private k:D


# direct methods
.method public constructor <init>(LX/1Ay;LX/ClD;Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1959894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1959895
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D3V;->e:Z

    .line 1959896
    iput-boolean v1, p0, LX/D3V;->f:Z

    .line 1959897
    iput-object p1, p0, LX/D3V;->a:LX/1Ay;

    .line 1959898
    new-instance v0, LX/D3U;

    invoke-direct {v0}, LX/D3U;-><init>()V

    iput-object v0, p0, LX/D3V;->g:LX/D3U;

    .line 1959899
    iput-object p2, p0, LX/D3V;->b:LX/ClD;

    .line 1959900
    iput-object p3, p0, LX/D3V;->c:Landroid/content/Context;

    .line 1959901
    return-void
.end method

.method public static a(LX/0QB;)LX/D3V;
    .locals 6

    .prologue
    .line 1959883
    const-class v1, LX/D3V;

    monitor-enter v1

    .line 1959884
    :try_start_0
    sget-object v0, LX/D3V;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1959885
    sput-object v2, LX/D3V;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1959886
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1959887
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1959888
    new-instance p0, LX/D3V;

    invoke-static {v0}, LX/1Ay;->a(LX/0QB;)LX/1Ay;

    move-result-object v3

    check-cast v3, LX/1Ay;

    invoke-static {v0}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v4

    check-cast v4, LX/ClD;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, LX/D3V;-><init>(LX/1Ay;LX/ClD;Landroid/content/Context;)V

    .line 1959889
    move-object v0, p0

    .line 1959890
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1959891
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D3V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1959892
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1959893
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/D3V;Z)V
    .locals 1

    .prologue
    .line 1959875
    if-eqz p1, :cond_0

    .line 1959876
    iget-boolean v0, p0, LX/D3V;->f:Z

    if-nez v0, :cond_1

    .line 1959877
    invoke-direct {p0}, LX/D3V;->f()V

    .line 1959878
    :goto_0
    iput-boolean p1, p0, LX/D3V;->e:Z

    .line 1959879
    return-void

    .line 1959880
    :cond_0
    iget-boolean v0, p0, LX/D3V;->f:Z

    if-nez v0, :cond_1

    .line 1959881
    invoke-direct {p0}, LX/D3V;->e()V

    goto :goto_0

    .line 1959882
    :cond_1
    invoke-direct {p0}, LX/D3V;->d()V

    goto :goto_0
.end method

.method public static b(LX/D3V;Z)V
    .locals 8

    .prologue
    .line 1959856
    iget-boolean v0, p0, LX/D3V;->e:Z

    if-eqz v0, :cond_3

    .line 1959857
    iget-boolean v0, p0, LX/D3V;->f:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, LX/D3V;->g:LX/D3U;

    .line 1959858
    const-wide/16 v4, 0x0

    .line 1959859
    iget-object v6, v0, LX/D3U;->a:Ljava/util/Date;

    if-eqz v6, :cond_0

    .line 1959860
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 1959861
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iget-object v6, v0, LX/D3U;->a:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-double v4, v4

    .line 1959862
    :cond_0
    move-wide v0, v4

    .line 1959863
    const-wide v2, 0x408f400000000000L    # 1000.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 1959864
    invoke-direct {p0}, LX/D3V;->e()V

    .line 1959865
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/D3V;->c(LX/D3V;Z)V

    .line 1959866
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/D3V;->h:D

    .line 1959867
    :goto_0
    iput-boolean p1, p0, LX/D3V;->f:Z

    .line 1959868
    return-void

    .line 1959869
    :cond_1
    iget-boolean v0, p0, LX/D3V;->f:Z

    if-nez v0, :cond_2

    .line 1959870
    invoke-direct {p0}, LX/D3V;->e()V

    goto :goto_0

    .line 1959871
    :cond_2
    invoke-direct {p0}, LX/D3V;->d()V

    goto :goto_0

    .line 1959872
    :cond_3
    iget-boolean v0, p0, LX/D3V;->f:Z

    if-nez v0, :cond_4

    if-eqz p1, :cond_4

    .line 1959873
    invoke-direct {p0}, LX/D3V;->f()V

    goto :goto_0

    .line 1959874
    :cond_4
    invoke-direct {p0}, LX/D3V;->d()V

    goto :goto_0
.end method

.method public static c(LX/D3V;Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1959902
    iget-wide v0, p0, LX/D3V;->j:D

    iget-wide v2, p0, LX/D3V;->i:D

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/D3V;->j:D

    .line 1959903
    iget-wide v0, p0, LX/D3V;->k:D

    iget-wide v2, p0, LX/D3V;->h:D

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/D3V;->k:D

    .line 1959904
    invoke-direct {p0}, LX/D3V;->g()V

    .line 1959905
    iget-object v0, p0, LX/D3V;->a:LX/1Ay;

    invoke-virtual {v0}, LX/1Ay;->a()V

    .line 1959906
    iput-wide v4, p0, LX/D3V;->i:D

    .line 1959907
    iput-wide v4, p0, LX/D3V;->h:D

    .line 1959908
    if-eqz p1, :cond_0

    .line 1959909
    const/4 v0, 0x0

    iput-object v0, p0, LX/D3V;->d:Ljava/lang/String;

    .line 1959910
    :cond_0
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1959853
    iget-wide v0, p0, LX/D3V;->h:D

    iget-object v2, p0, LX/D3V;->g:LX/D3U;

    invoke-virtual {v2}, LX/D3U;->a()D

    move-result-wide v2

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/D3V;->h:D

    .line 1959854
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/D3V;->i:D

    .line 1959855
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 1959850
    iget-wide v0, p0, LX/D3V;->i:D

    iget-object v2, p0, LX/D3V;->g:LX/D3U;

    invoke-virtual {v2}, LX/D3U;->a()D

    move-result-wide v2

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/D3V;->i:D

    .line 1959851
    invoke-direct {p0}, LX/D3V;->g()V

    .line 1959852
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 1959848
    iget-object v0, p0, LX/D3V;->g:LX/D3U;

    invoke-virtual {v0}, LX/D3U;->a()D

    .line 1959849
    return-void
.end method

.method private g()V
    .locals 7

    .prologue
    const-wide v4, 0x408f400000000000L    # 1000.0

    .line 1959841
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1959842
    iget-object v0, p0, LX/D3V;->b:LX/ClD;

    invoke-virtual {v0}, LX/ClD;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1959843
    const-string v0, "article_depth_level"

    iget-object v1, p0, LX/D3V;->b:LX/ClD;

    iget-object v2, p0, LX/D3V;->c:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1959844
    const-string v0, "article_aggregate_view_time"

    iget-wide v2, p0, LX/D3V;->j:D

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1959845
    const-string v0, "article_aggregate_load_time"

    iget-wide v2, p0, LX/D3V;->k:D

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1959846
    iget-object v0, p0, LX/D3V;->a:LX/1Ay;

    iget-object v1, p0, LX/D3V;->d:Ljava/lang/String;

    iget-wide v2, p0, LX/D3V;->h:D

    iget-wide v4, p0, LX/D3V;->i:D

    invoke-virtual/range {v0 .. v6}, LX/1Ay;->a(Ljava/lang/String;DDLjava/util/Map;)V

    .line 1959847
    return-void
.end method


# virtual methods
.method public final a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 1959837
    if-eqz p1, :cond_0

    .line 1959838
    iput-object p2, p0, LX/D3V;->d:Ljava/lang/String;

    .line 1959839
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/D3V;->b(LX/D3V;Z)V

    .line 1959840
    :cond_0
    return-void
.end method
