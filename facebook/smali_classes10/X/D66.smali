.class public LX/D66;
.super LX/C64;
.source ""


# instance fields
.field public b:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1965134
    invoke-direct {p0, p1}, LX/C64;-><init>(Landroid/content/Context;)V

    .line 1965135
    const-class v0, LX/D66;

    invoke-static {v0, p0}, LX/D66;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1965136
    const v0, 0x7f0d117e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/D66;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1965137
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/D66;

    invoke-static {p0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object p0

    check-cast p0, LX/154;

    iput-object p0, p1, LX/D66;->b:LX/154;

    return-void
.end method


# virtual methods
.method public getViewCountTextView()Lcom/facebook/resources/ui/FbTextView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1965133
    iget-object v0, p0, LX/D66;->c:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public setComments(I)V
    .locals 3

    .prologue
    .line 1965138
    const v0, 0x7f0d0702

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1965139
    const v1, 0x7f0f0072

    iget-object v2, p0, LX/D66;->b:LX/154;

    invoke-static {v0, p1, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;IILX/154;)V

    .line 1965140
    return-void
.end method

.method public setLikes(I)V
    .locals 3

    .prologue
    .line 1965130
    const v0, 0x7f0d0701

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1965131
    const v1, 0x7f0f0071

    iget-object v2, p0, LX/D66;->b:LX/154;

    invoke-static {v0, p1, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;IILX/154;)V

    .line 1965132
    return-void
.end method

.method public setPlayCountText(I)V
    .locals 3

    .prologue
    .line 1965125
    if-gtz p1, :cond_0

    invoke-virtual {p0}, LX/3VF;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/3VF;->setIsExpanded(Z)V

    .line 1965126
    const v0, 0x7f0d117e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1965127
    const v1, 0x7f0f0070

    iget-object v2, p0, LX/D66;->b:LX/154;

    invoke-static {v0, p1, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;IILX/154;)V

    .line 1965128
    return-void

    .line 1965129
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setShares(I)V
    .locals 3

    .prologue
    .line 1965121
    const v0, 0x7f0d0703

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    .line 1965122
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1965123
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f0073

    iget-object v2, p0, LX/D66;->b:LX/154;

    invoke-static {v0, p1, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;IILX/154;)V

    .line 1965124
    :cond_0
    return-void
.end method
