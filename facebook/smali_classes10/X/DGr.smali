.class public final LX/DGr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;

.field private final b:Lcom/facebook/graphql/model/GraphQLStorySet;

.field private final c:I

.field private final d:I

.field private final e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;LX/DGq;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1980821
    iput-object p1, p0, LX/DGr;->a:Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1980822
    iget-object v0, p2, LX/DGq;->a:LX/DGm;

    .line 1980823
    iget-object p1, v0, LX/DGm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, p1

    .line 1980824
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1980825
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    iput-object v0, p0, LX/DGr;->b:Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 1980826
    iget v0, p2, LX/DGq;->c:I

    iput v0, p0, LX/DGr;->c:I

    .line 1980827
    iget-object v0, p2, LX/DGq;->a:LX/DGm;

    .line 1980828
    iget p1, v0, LX/DGm;->c:I

    move v0, p1

    .line 1980829
    iput v0, p0, LX/DGr;->d:I

    .line 1980830
    iput-object p3, p0, LX/DGr;->e:Landroid/view/View$OnClickListener;

    .line 1980831
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x2475722e

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1980832
    iget-object v1, p0, LX/DGr;->a:Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->a:LX/2yK;

    sget-object v2, LX/0ig;->y:LX/0ih;

    iget-object v3, p0, LX/DGr;->b:Lcom/facebook/graphql/model/GraphQLStorySet;

    iget v4, p0, LX/DGr;->c:I

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "position:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, LX/DGr;->d:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;ILjava/lang/String;)V

    .line 1980833
    iget-object v1, p0, LX/DGr;->e:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1980834
    const v1, 0x7c59d7f9

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
