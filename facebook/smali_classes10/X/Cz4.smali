.class public LX/Cz4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0us;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;LX/0us;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;",
            "LX/0us;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1954125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954126
    iput-object p1, p0, LX/Cz4;->a:LX/0Px;

    .line 1954127
    iput-object p2, p0, LX/Cz4;->b:LX/0us;

    .line 1954128
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1954129
    iput-object v0, p0, LX/Cz4;->c:LX/0P1;

    .line 1954130
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1954131
    iput-object v0, p0, LX/Cz4;->d:LX/0P1;

    .line 1954132
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1954133
    iput-object v0, p0, LX/Cz4;->e:LX/0P1;

    .line 1954134
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cz4;->f:Ljava/lang/String;

    .line 1954135
    return-void
.end method

.method public constructor <init>(LX/0Px;LX/0us;LX/0P1;LX/0P1;LX/0P1;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;",
            "LX/0us;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1954136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954137
    iput-object p1, p0, LX/Cz4;->a:LX/0Px;

    .line 1954138
    iput-object p2, p0, LX/Cz4;->b:LX/0us;

    .line 1954139
    iput-object p3, p0, LX/Cz4;->c:LX/0P1;

    .line 1954140
    iput-object p4, p0, LX/Cz4;->d:LX/0P1;

    .line 1954141
    iput-object p5, p0, LX/Cz4;->e:LX/0P1;

    .line 1954142
    iput-object p6, p0, LX/Cz4;->f:Ljava/lang/String;

    .line 1954143
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954144
    iget-object v0, p0, LX/Cz4;->a:LX/0Px;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1954145
    iget-object v0, p0, LX/Cz4;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
