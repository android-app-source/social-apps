.class public LX/DAp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/DAo;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1971480
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DAp;-><init>(Ljava/util/Collection;Z)V

    .line 1971481
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1971482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1971483
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/DAp;->a:Ljava/util/Map;

    .line 1971484
    iput-boolean p2, p0, LX/DAp;->b:Z

    .line 1971485
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1971486
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v3

    .line 1971487
    iget-object v1, p0, LX/DAp;->a:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DAo;

    .line 1971488
    if-nez v1, :cond_1

    .line 1971489
    new-instance v1, LX/DAo;

    invoke-direct {v1}, LX/DAo;-><init>()V

    .line 1971490
    iget-object v4, p0, LX/DAp;->a:Ljava/util/Map;

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1971491
    :cond_1
    iget-object v3, v1, LX/DAo;->a:Ljava/lang/Float;

    if-eqz v3, :cond_2

    iget-object v3, v1, LX/DAo;->a:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 1971492
    iget v4, v0, Lcom/facebook/user/model/User;->m:F

    move v4, v4

    .line 1971493
    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 1971494
    :cond_2
    iget v3, v0, Lcom/facebook/user/model/User;->m:F

    move v3, v3

    .line 1971495
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v1, LX/DAo;->a:Ljava/lang/Float;

    .line 1971496
    :cond_3
    iget-boolean v3, v0, Lcom/facebook/user/model/User;->s:Z

    move v3, v3

    .line 1971497
    if-eqz v3, :cond_4

    .line 1971498
    iput-boolean v5, v1, LX/DAo;->b:Z

    .line 1971499
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1971500
    iput-boolean v5, v1, LX/DAo;->c:Z

    goto :goto_0

    .line 1971501
    :cond_5
    return-void
.end method

.method public static a(LX/DAp;ZZZZ)I
    .locals 1

    .prologue
    .line 1971502
    iget-boolean v0, p0, LX/DAp;->b:Z

    if-eqz v0, :cond_2

    .line 1971503
    if-nez p2, :cond_0

    if-eqz p4, :cond_1

    .line 1971504
    :cond_0
    invoke-static {p2, p4}, LX/DAp;->a(ZZ)I

    move-result v0

    .line 1971505
    :goto_0
    return v0

    .line 1971506
    :cond_1
    invoke-static {p1, p3}, LX/DAp;->a(ZZ)I

    move-result v0

    goto :goto_0

    .line 1971507
    :cond_2
    if-nez p1, :cond_3

    if-eqz p3, :cond_4

    .line 1971508
    :cond_3
    invoke-static {p1, p3}, LX/DAp;->a(ZZ)I

    move-result v0

    goto :goto_0

    .line 1971509
    :cond_4
    invoke-static {p2, p4}, LX/DAp;->a(ZZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(ZZ)I
    .locals 1

    .prologue
    .line 1971510
    if-ne p0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;Lcom/facebook/user/model/User;)I
    .locals 6

    .prologue
    .line 1971511
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v3

    .line 1971512
    invoke-virtual {p2}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v4

    .line 1971513
    iget-object v0, p0, LX/DAp;->a:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DAo;

    .line 1971514
    iget-object v1, p0, LX/DAp;->a:Ljava/util/Map;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DAo;

    .line 1971515
    iget-object v2, v1, LX/DAo;->a:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v5, v0, LX/DAo;->a:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    .line 1971516
    if-eqz v2, :cond_1

    move v0, v2

    .line 1971517
    :cond_0
    :goto_0
    return v0

    .line 1971518
    :cond_1
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 1971519
    if-eqz v2, :cond_2

    .line 1971520
    iget-boolean v3, v0, LX/DAo;->b:Z

    iget-boolean v4, v0, LX/DAo;->c:Z

    iget-boolean v5, v1, LX/DAo;->b:Z

    iget-boolean p1, v1, LX/DAo;->c:Z

    invoke-static {p0, v3, v4, v5, p1}, LX/DAp;->a(LX/DAp;ZZZZ)I

    move-result v3

    move v0, v3

    .line 1971521
    if-nez v0, :cond_0

    move v0, v2

    goto :goto_0

    .line 1971522
    :cond_2
    iget-boolean v0, p1, Lcom/facebook/user/model/User;->s:Z

    move v0, v0

    .line 1971523
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->b()Z

    move-result v1

    .line 1971524
    iget-boolean v2, p2, Lcom/facebook/user/model/User;->s:Z

    move v2, v2

    .line 1971525
    invoke-virtual {p2}, Lcom/facebook/user/model/User;->b()Z

    move-result v3

    invoke-static {p0, v0, v1, v2, v3}, LX/DAp;->a(LX/DAp;ZZZZ)I

    move-result v0

    move v0, v0

    .line 1971526
    if-nez v0, :cond_0

    .line 1971527
    iget v0, p2, Lcom/facebook/user/model/User;->m:F

    move v0, v0

    .line 1971528
    iget v1, p1, Lcom/facebook/user/model/User;->m:F

    move v1, v1

    .line 1971529
    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    .line 1971530
    if-nez v0, :cond_0

    .line 1971531
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1971532
    iget-object v0, p1, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v0, v0

    .line 1971533
    invoke-virtual {v0}, LX/0XG;->isPhoneContact()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1971534
    iget-object v0, p2, Lcom/facebook/user/model/User;->b:LX/0XG;

    move-object v0, v0

    .line 1971535
    invoke-virtual {v0}, LX/0XG;->isPhoneContact()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1971536
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/UserPhoneNumber;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 1971537
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {p2}, Lcom/facebook/user/model/User;->t()Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/user/model/UserPhoneNumber;->e()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1971538
    :goto_2
    invoke-static {v0, v1}, LX/DAp;->a(ZZ)I

    move-result v2

    .line 1971539
    :cond_3
    move v0, v2

    .line 1971540
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1971541
    goto :goto_1

    :cond_5
    move v1, v2

    .line 1971542
    goto :goto_2
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1971543
    check-cast p1, Lcom/facebook/user/model/User;

    check-cast p2, Lcom/facebook/user/model/User;

    invoke-virtual {p0, p1, p2}, LX/DAp;->a(Lcom/facebook/user/model/User;Lcom/facebook/user/model/User;)I

    move-result v0

    return v0
.end method
