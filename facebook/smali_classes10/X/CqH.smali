.class public LX/CqH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/CqH;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CsX;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/content/Context;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1939508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1939509
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CqH;->a:Ljava/util/List;

    .line 1939510
    return-void
.end method

.method public static a(LX/0QB;)LX/CqH;
    .locals 3

    .prologue
    .line 1939526
    sget-object v0, LX/CqH;->d:LX/CqH;

    if-nez v0, :cond_1

    .line 1939527
    const-class v1, LX/CqH;

    monitor-enter v1

    .line 1939528
    :try_start_0
    sget-object v0, LX/CqH;->d:LX/CqH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1939529
    if-eqz v2, :cond_0

    .line 1939530
    :try_start_1
    new-instance v0, LX/CqH;

    invoke-direct {v0}, LX/CqH;-><init>()V

    .line 1939531
    move-object v0, v0

    .line 1939532
    sput-object v0, LX/CqH;->d:LX/CqH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1939533
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1939534
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1939535
    :cond_1
    sget-object v0, LX/CqH;->d:LX/CqH;

    return-object v0

    .line 1939536
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1939537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CsX;)V
    .locals 2

    .prologue
    .line 1939517
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/CqH;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1939518
    :cond_0
    :goto_0
    return-void

    .line 1939519
    :cond_1
    invoke-virtual {p0}, LX/CqH;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CqH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/CqH;->c:I

    if-lt v0, v1, :cond_3

    .line 1939520
    :cond_2
    invoke-static {p1}, LX/Cs3;->b(Landroid/webkit/WebView;)V

    goto :goto_0

    .line 1939521
    :cond_3
    invoke-static {p1}, LX/Cs3;->c(Landroid/webkit/WebView;)V

    .line 1939522
    invoke-virtual {p1}, LX/CsX;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 1939523
    invoke-virtual {p1}, LX/CsX;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1939524
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1939525
    :cond_4
    iget-object v0, p0, LX/CqH;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1939516
    iget-object v0, p0, LX/CqH;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1939511
    iget-object v0, p0, LX/CqH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 1939512
    invoke-static {v0}, LX/Cs3;->b(Landroid/webkit/WebView;)V

    goto :goto_0

    .line 1939513
    :cond_0
    iget-object v0, p0, LX/CqH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1939514
    const/4 v0, 0x0

    iput-object v0, p0, LX/CqH;->b:Landroid/content/Context;

    .line 1939515
    return-void
.end method
