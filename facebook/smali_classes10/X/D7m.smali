.class public LX/D7m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/D7m;


# instance fields
.field public final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1967595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1967596
    iput-object p1, p0, LX/D7m;->a:LX/0Uh;

    .line 1967597
    return-void
.end method

.method public static a(LX/0QB;)LX/D7m;
    .locals 4

    .prologue
    .line 1967598
    sget-object v0, LX/D7m;->b:LX/D7m;

    if-nez v0, :cond_1

    .line 1967599
    const-class v1, LX/D7m;

    monitor-enter v1

    .line 1967600
    :try_start_0
    sget-object v0, LX/D7m;->b:LX/D7m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1967601
    if-eqz v2, :cond_0

    .line 1967602
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1967603
    new-instance p0, LX/D7m;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/D7m;-><init>(LX/0Uh;)V

    .line 1967604
    move-object v0, p0

    .line 1967605
    sput-object v0, LX/D7m;->b:LX/D7m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1967606
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1967607
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1967608
    :cond_1
    sget-object v0, LX/D7m;->b:LX/D7m;

    return-object v0

    .line 1967609
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1967610
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
