.class public final LX/DSP;
.super LX/DRr;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V
    .locals 0

    .prologue
    .line 1999375
    iput-object p1, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    invoke-direct {p0}, LX/DRr;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 10

    .prologue
    .line 1999376
    check-cast p1, LX/DTi;

    .line 1999377
    iget-object v0, p1, LX/DTi;->a:Ljava/lang/String;

    iget-object v1, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    .line 1999378
    iget-object v2, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1999379
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1999380
    :goto_0
    return-void

    .line 1999381
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v0, v1, :cond_6

    .line 1999382
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 1999383
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    iget-object v1, p1, LX/DTi;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1999384
    :cond_1
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    if-eqz v0, :cond_2

    .line 1999385
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    iget-object v1, p1, LX/DTi;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1999386
    :cond_2
    :goto_1
    iget-object v0, p1, LX/DTi;->b:Ljava/lang/String;

    iget-object v1, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1999387
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v1, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1999388
    iput-object v1, v0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1999389
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v1, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    .line 1999390
    iget-object v2, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object v1, v2

    .line 1999391
    iput-object v1, v0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1999392
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    .line 1999393
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 1999394
    if-eqz v0, :cond_3

    .line 1999395
    const-string v1, "group_admin_type"

    iget-object v2, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1999396
    :cond_3
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->H(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    .line 1999397
    sget-object v0, LX/DSK;->a:[I

    iget-object v1, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1999398
    sget-object v0, LX/DSb;->NOT_ADMIN:LX/DSb;

    .line 1999399
    :goto_2
    iget-object v1, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v2, p1, LX/DTi;->b:Ljava/lang/String;

    .line 1999400
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1999401
    iget-object v3, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->e:LX/0Px;

    move-object v6, v3

    .line 1999402
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v4, v3

    :goto_3
    if-ge v4, v7, :cond_5

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DSf;

    .line 1999403
    iget-object v8, v3, LX/DSf;->d:LX/DUV;

    move-object v8, v8

    .line 1999404
    invoke-interface {v8}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1999405
    new-instance v8, LX/DSf;

    .line 1999406
    iget-object v9, v3, LX/DSf;->d:LX/DUV;

    move-object v9, v9

    .line 1999407
    sget-object p0, LX/DSc;->NOT_BLOCKED:LX/DSc;

    .line 1999408
    iget-object p1, v3, LX/DSf;->e:LX/DS2;

    move-object v3, p1

    .line 1999409
    invoke-direct {v8, v9, v0, p0, v3}, LX/DSf;-><init>(LX/DUV;LX/DSb;LX/DSc;LX/DS2;)V

    invoke-virtual {v5, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1999410
    :goto_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 1999411
    :cond_4
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 1999412
    :cond_5
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->a(LX/0Px;)V

    .line 1999413
    goto/16 :goto_0

    .line 1999414
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v0, v1, :cond_8

    .line 1999415
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    if-eqz v0, :cond_7

    .line 1999416
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    iget-object v1, p1, LX/DTi;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1999417
    :cond_7
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    if-eqz v0, :cond_2

    .line 1999418
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    iget-object v1, p1, LX/DTi;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1999419
    :cond_8
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    if-eqz v0, :cond_9

    .line 1999420
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    iget-object v1, p1, LX/DTi;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1999421
    :cond_9
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    if-eqz v0, :cond_2

    .line 1999422
    iget-object v0, p0, LX/DSP;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    iget-object v1, p1, LX/DTi;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1999423
    :pswitch_0
    sget-object v0, LX/DSb;->ADMIN:LX/DSb;

    goto/16 :goto_2

    .line 1999424
    :pswitch_1
    sget-object v0, LX/DSb;->MODERATOR:LX/DSb;

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
