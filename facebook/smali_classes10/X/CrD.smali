.class public LX/CrD;
.super LX/Cqj;
.source ""


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1940701
    invoke-direct {p0, p1, p2}, LX/Cqj;-><init>(LX/Ctg;LX/CrK;)V

    .line 1940702
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->a:LX/Cqw;

    .line 1940703
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940704
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    sget-object v7, LX/Cqt;->PORTRAIT:LX/Cqt;

    invoke-direct/range {v0 .. v7}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;LX/Cqt;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940705
    sget-object v0, LX/Cqw;->c:LX/Cqw;

    invoke-virtual {p0, v0}, LX/CqX;->e(LX/Cqv;)V

    .line 1940706
    sget-object v0, LX/Cqw;->d:LX/Cqw;

    invoke-virtual {p0, v0}, LX/CqX;->e(LX/Cqv;)V

    .line 1940707
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->c:LX/Cqw;

    .line 1940708
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940709
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_MATCH_FRAME:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    sget-object v7, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    invoke-direct/range {v0 .. v7}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;LX/Cqt;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940710
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->d:LX/Cqw;

    .line 1940711
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940712
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_MATCH_FRAME:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    sget-object v7, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    invoke-direct/range {v0 .. v7}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;LX/Cqt;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940713
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->b:LX/Cqw;

    .line 1940714
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940715
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_MATCH_FRAME:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940716
    return-void
.end method
