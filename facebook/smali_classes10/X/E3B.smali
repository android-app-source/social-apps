.class public LX/E3B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1dp;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/03V;

.field public final b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/1X9;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2073686
    iput-object p1, p0, LX/E3B;->a:LX/03V;

    .line 2073687
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/1X9;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 2073688
    sget-object v1, LX/1X9;->BOX:LX/1X9;

    const p1, 0x7f0215d1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073689
    sget-object v1, LX/1X9;->TOP:LX/1X9;

    const p1, 0x7f0215d2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073690
    sget-object v1, LX/1X9;->MIDDLE:LX/1X9;

    const p1, 0x7f0a00d5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073691
    sget-object v1, LX/1X9;->BOTTOM:LX/1X9;

    const p1, 0x7f0215d0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073692
    sget-object v1, LX/1X9;->DIVIDER_TOP:LX/1X9;

    const p1, 0x7f0a00d5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073693
    sget-object v1, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    const p1, 0x7f0a00d5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073694
    sget-object v1, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    const p1, 0x7f0a00d5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073695
    sget-object v1, LX/1X9;->FOLLOW_UP:LX/1X9;

    const p1, 0x7f0a00d5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073696
    move-object v0, v0

    .line 2073697
    iput-object v0, p0, LX/E3B;->b:Ljava/util/EnumMap;

    .line 2073698
    return-void
.end method

.method public static a(LX/0QB;)LX/E3B;
    .locals 4

    .prologue
    .line 2073699
    const-class v1, LX/E3B;

    monitor-enter v1

    .line 2073700
    :try_start_0
    sget-object v0, LX/E3B;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073701
    sput-object v2, LX/E3B;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073702
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073703
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073704
    new-instance p0, LX/E3B;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/E3B;-><init>(LX/03V;)V

    .line 2073705
    move-object v0, p0

    .line 2073706
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073707
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/E3B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073708
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;LX/1X9;ILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2073710
    sget-object v0, LX/1X9;->BOX:LX/1X9;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/1X9;->TOP:LX/1X9;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    if-eq p2, v0, :cond_0

    .line 2073711
    iget-object v0, p0, LX/E3B;->a:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "Unsupported position: "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/1X9;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    const-string p4, " is passed"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, v1, p3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2073712
    :cond_0
    iget-object v0, p0, LX/E3B;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v0, v0

    .line 2073713
    return-object v0
.end method
