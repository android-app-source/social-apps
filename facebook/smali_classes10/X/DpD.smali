.class public LX/DpD;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field public a:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 0

    .prologue
    .line 2043732
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 2043733
    iput-object p1, p0, LX/DpD;->a:Ljava/nio/ByteBuffer;

    .line 2043734
    return-void
.end method


# virtual methods
.method public final read()I
    .locals 1

    .prologue
    .line 2043735
    iget-object v0, p0, LX/DpD;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2043736
    const/4 v0, -0x1

    .line 2043737
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/DpD;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 2043738
    iget-object v0, p0, LX/DpD;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2043739
    const/4 v0, -0x1

    .line 2043740
    :goto_0
    return v0

    .line 2043741
    :cond_0
    iget-object v0, p0, LX/DpD;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2043742
    iget-object v1, p0, LX/DpD;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1, p2, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method
