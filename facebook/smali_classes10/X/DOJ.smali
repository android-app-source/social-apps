.class public final enum LX/DOJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DOJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DOJ;

.field public static final enum Pin:LX/DOJ;

.field public static final enum Unpin:LX/DOJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1992221
    new-instance v0, LX/DOJ;

    const-string v1, "Pin"

    invoke-direct {v0, v1, v2}, LX/DOJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DOJ;->Pin:LX/DOJ;

    .line 1992222
    new-instance v0, LX/DOJ;

    const-string v1, "Unpin"

    invoke-direct {v0, v1, v3}, LX/DOJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DOJ;->Unpin:LX/DOJ;

    .line 1992223
    const/4 v0, 0x2

    new-array v0, v0, [LX/DOJ;

    sget-object v1, LX/DOJ;->Pin:LX/DOJ;

    aput-object v1, v0, v2

    sget-object v1, LX/DOJ;->Unpin:LX/DOJ;

    aput-object v1, v0, v3

    sput-object v0, LX/DOJ;->$VALUES:[LX/DOJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1992224
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DOJ;
    .locals 1

    .prologue
    .line 1992220
    const-class v0, LX/DOJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DOJ;

    return-object v0
.end method

.method public static values()[LX/DOJ;
    .locals 1

    .prologue
    .line 1992219
    sget-object v0, LX/DOJ;->$VALUES:[LX/DOJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DOJ;

    return-object v0
.end method
