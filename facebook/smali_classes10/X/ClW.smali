.class public LX/ClW;
.super LX/ClU;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public b:Lcom/facebook/graphql/model/GraphQLFeedback;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;LX/ClQ;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1932525
    sget-object v1, LX/ClT;->UFI:LX/ClT;

    sget-object v3, LX/ClS;->REGULAR:LX/ClS;

    sget-object v5, LX/ClR;->BELOW:LX/ClR;

    move-object v0, p0

    move-object v4, p3

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, LX/ClU;-><init>(LX/ClT;Ljava/lang/String;LX/ClS;LX/ClQ;LX/ClR;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    .line 1932526
    iput-object p1, p0, LX/ClW;->a:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 1932527
    iput-object p2, p0, LX/ClW;->b:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1932528
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;LX/Crz;)LX/ClW;
    .locals 2

    .prologue
    .line 1932529
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1932530
    :cond_0
    const/4 v0, 0x0

    .line 1932531
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, LX/ClW;

    invoke-virtual {p2}, LX/Crz;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/ClQ;->RIGHT:LX/ClQ;

    :goto_1
    invoke-direct {v1, p0, p1, v0}, LX/ClW;-><init>(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;LX/ClQ;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    sget-object v0, LX/ClQ;->LEFT:LX/ClQ;

    goto :goto_1
.end method
