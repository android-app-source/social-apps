.class public final enum LX/Clz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Clz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Clz;

.field public static final enum BASIC_LAYOUT:LX/Clz;

.field public static final enum BUTTON_COLOR:LX/Clz;

.field public static final enum CONTROL:LX/Clz;

.field public static final enum COVER_PHOTO:LX/Clz;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1933147
    new-instance v0, LX/Clz;

    const-string v1, "BASIC_LAYOUT"

    invoke-direct {v0, v1, v2}, LX/Clz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clz;->BASIC_LAYOUT:LX/Clz;

    .line 1933148
    new-instance v0, LX/Clz;

    const-string v1, "COVER_PHOTO"

    invoke-direct {v0, v1, v3}, LX/Clz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clz;->COVER_PHOTO:LX/Clz;

    .line 1933149
    new-instance v0, LX/Clz;

    const-string v1, "BUTTON_COLOR"

    invoke-direct {v0, v1, v4}, LX/Clz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clz;->BUTTON_COLOR:LX/Clz;

    .line 1933150
    new-instance v0, LX/Clz;

    const-string v1, "CONTROL"

    invoke-direct {v0, v1, v5}, LX/Clz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clz;->CONTROL:LX/Clz;

    .line 1933151
    const/4 v0, 0x4

    new-array v0, v0, [LX/Clz;

    sget-object v1, LX/Clz;->BASIC_LAYOUT:LX/Clz;

    aput-object v1, v0, v2

    sget-object v1, LX/Clz;->COVER_PHOTO:LX/Clz;

    aput-object v1, v0, v3

    sget-object v1, LX/Clz;->BUTTON_COLOR:LX/Clz;

    aput-object v1, v0, v4

    sget-object v1, LX/Clz;->CONTROL:LX/Clz;

    aput-object v1, v0, v5

    sput-object v0, LX/Clz;->$VALUES:[LX/Clz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1933152
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Clz;
    .locals 1

    .prologue
    .line 1933146
    const-class v0, LX/Clz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Clz;

    return-object v0
.end method

.method public static values()[LX/Clz;
    .locals 1

    .prologue
    .line 1933145
    sget-object v0, LX/Clz;->$VALUES:[LX/Clz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Clz;

    return-object v0
.end method
