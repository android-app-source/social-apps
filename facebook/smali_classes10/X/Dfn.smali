.class public final enum LX/Dfn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dfn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dfn;

.field public static final enum AUTO:LX/Dfn;

.field public static final enum LOADING:LX/Dfn;

.field public static final enum LOAD_MORE:LX/Dfn;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2027714
    new-instance v0, LX/Dfn;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v2}, LX/Dfn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfn;->AUTO:LX/Dfn;

    .line 2027715
    new-instance v0, LX/Dfn;

    const-string v1, "LOAD_MORE"

    invoke-direct {v0, v1, v3}, LX/Dfn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfn;->LOAD_MORE:LX/Dfn;

    .line 2027716
    new-instance v0, LX/Dfn;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, LX/Dfn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dfn;->LOADING:LX/Dfn;

    .line 2027717
    const/4 v0, 0x3

    new-array v0, v0, [LX/Dfn;

    sget-object v1, LX/Dfn;->AUTO:LX/Dfn;

    aput-object v1, v0, v2

    sget-object v1, LX/Dfn;->LOAD_MORE:LX/Dfn;

    aput-object v1, v0, v3

    sget-object v1, LX/Dfn;->LOADING:LX/Dfn;

    aput-object v1, v0, v4

    sput-object v0, LX/Dfn;->$VALUES:[LX/Dfn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2027718
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dfn;
    .locals 1

    .prologue
    .line 2027719
    const-class v0, LX/Dfn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dfn;

    return-object v0
.end method

.method public static values()[LX/Dfn;
    .locals 1

    .prologue
    .line 2027720
    sget-object v0, LX/Dfn;->$VALUES:[LX/Dfn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dfn;

    return-object v0
.end method
