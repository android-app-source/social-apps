.class public final LX/EBa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/activities/RtcCallPermissionActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2087647
    iput-object p1, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    iput-object p2, p0, LX/EBa;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2087648
    iget-object v0, p0, LX/EBa;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2087649
    iget-object v0, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->p:LX/3A0;

    iget-object v1, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    iget-object v1, v1, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->v:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    invoke-virtual {v0, v1}, LX/3A0;->c(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V

    .line 2087650
    :goto_0
    iget-object v0, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    invoke-virtual {v0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->finish()V

    .line 2087651
    return-void

    .line 2087652
    :cond_0
    iget-object v0, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->p:LX/3A0;

    iget-object v1, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    iget-object v1, v1, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->v:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    invoke-virtual {v0, v1}, LX/3A0;->b(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2087653
    iget-object v0, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->q:LX/2S7;

    iget-object v1, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    iget-object v1, v1, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->v:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-wide v2, v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    iget-object v2, v2, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->v:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v2, v2, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    iget-object v3, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    iget-object v3, v3, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->v:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-boolean v3, v3, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    sget-object v4, LX/79O;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 2087654
    iget-object v0, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    invoke-virtual {v0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->finish()V

    .line 2087655
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2087656
    iget-object v0, p0, LX/EBa;->b:Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    invoke-virtual {v0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->finish()V

    .line 2087657
    return-void
.end method
