.class public LX/ECT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public final a:LX/ECY;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:J

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/ECY;JJJLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2089869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2089870
    iput-object p1, p0, LX/ECT;->a:LX/ECY;

    .line 2089871
    iput-wide p2, p0, LX/ECT;->b:J

    .line 2089872
    iput-wide p4, p0, LX/ECT;->d:J

    .line 2089873
    iput-wide p6, p0, LX/ECT;->e:J

    .line 2089874
    iput-object p8, p0, LX/ECT;->c:Ljava/lang/String;

    .line 2089875
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2089866
    iput-object p1, p0, LX/ECT;->f:Ljava/lang/String;

    .line 2089867
    iput-object p2, p0, LX/ECT;->g:Ljava/lang/String;

    .line 2089868
    return-void
.end method

.method public final e()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2089864
    iget-wide v2, p0, LX/ECT;->e:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    .line 2089865
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LX/ECT;->d:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, LX/ECT;->e:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 2089863
    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 2089858
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 2089862
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 2089861
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 2089860
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 2089859
    const/4 v0, 0x0

    return v0
.end method
