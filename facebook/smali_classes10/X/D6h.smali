.class public final LX/D6h;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;)V
    .locals 0

    .prologue
    .line 1966033
    iput-object p1, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1966034
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1966035
    check-cast p1, LX/2ou;

    .line 1966036
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1966037
    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    .line 1966038
    iget-object v1, v0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->a:Landroid/view/View;

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1966039
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1966040
    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object p1, v0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->b:Landroid/view/View;

    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Li;

    invoke-interface {v0}, LX/7Lh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1966041
    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object p1, v0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->c:Landroid/view/View;

    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Li;

    invoke-interface {v0}, LX/7Lh;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1966042
    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    .line 1966043
    iget-object v1, v0, LX/2oy;->i:LX/2oj;

    if-nez v1, :cond_3

    .line 1966044
    :goto_2
    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    .line 1966045
    iget-object v1, v0, LX/2oy;->i:LX/2oj;

    if-nez v1, :cond_4

    .line 1966046
    :goto_3
    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Li;

    invoke-interface {v0}, LX/7Li;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Li;

    invoke-interface {v0}, LX/7Lh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1966047
    iget-object v0, p0, LX/D6h;->a:Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/plugins/PostPlaybackPlugin;->d:Lcom/facebook/video/player/CountdownRingContainer;

    invoke-virtual {v0}, Lcom/facebook/video/player/CountdownRingContainer;->a()V

    .line 1966048
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1966049
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1966050
    goto :goto_1

    .line 1966051
    :cond_3
    iget-object v1, v0, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/2pW;

    sget-object p1, LX/2pO;->HIDE:LX/2pO;

    invoke-direct {v2, p1}, LX/2pW;-><init>(LX/2pO;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    goto :goto_2

    .line 1966052
    :cond_4
    iget-object v1, v0, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/7ME;

    sget-object p1, LX/7MD;->ALWAYS_INVISIBLE:LX/7MD;

    invoke-direct {v2, p1}, LX/7ME;-><init>(LX/7MD;)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    goto :goto_3
.end method
