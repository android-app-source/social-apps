.class public LX/E4b;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E4b;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E4c;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E4b",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E4c;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076966
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2076967
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E4b;->b:LX/0Zi;

    .line 2076968
    iput-object p1, p0, LX/E4b;->a:LX/0Ot;

    .line 2076969
    return-void
.end method

.method public static a(LX/0QB;)LX/E4b;
    .locals 4

    .prologue
    .line 2076953
    sget-object v0, LX/E4b;->c:LX/E4b;

    if-nez v0, :cond_1

    .line 2076954
    const-class v1, LX/E4b;

    monitor-enter v1

    .line 2076955
    :try_start_0
    sget-object v0, LX/E4b;->c:LX/E4b;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2076956
    if-eqz v2, :cond_0

    .line 2076957
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2076958
    new-instance v3, LX/E4b;

    const/16 p0, 0x310d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E4b;-><init>(LX/0Ot;)V

    .line 2076959
    move-object v0, v3

    .line 2076960
    sput-object v0, LX/E4b;->c:LX/E4b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076961
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2076962
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2076963
    :cond_1
    sget-object v0, LX/E4b;->c:LX/E4b;

    return-object v0

    .line 2076964
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2076965
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 7

    .prologue
    .line 2076930
    check-cast p2, LX/E4a;

    .line 2076931
    iget-object v0, p0, LX/E4b;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E4c;

    iget-object v2, p2, LX/E4a;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p2, LX/E4a;->c:Ljava/lang/String;

    iget-object v4, p2, LX/E4a;->d:LX/2km;

    iget-object v5, p2, LX/E4a;->e:Ljava/lang/String;

    iget-object v6, p2, LX/E4a;->f:Ljava/lang/String;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/E4c;->onClick(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    .line 2076932
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2076970
    const v0, 0x5534db98

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 2076947
    check-cast p2, LX/E4a;

    .line 2076948
    iget-object v0, p0, LX/E4b;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/E4a;->a:LX/1X1;

    .line 2076949
    invoke-static {p1, v0}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object p0

    .line 2076950
    const p2, 0x5534db98

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 2076951
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 2076952
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2076941
    invoke-static {}, LX/1dS;->b()V

    .line 2076942
    iget v0, p1, LX/1dQ;->b:I

    .line 2076943
    packed-switch v0, :pswitch_data_0

    .line 2076944
    :goto_0
    return-object v2

    .line 2076945
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2076946
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/E4b;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5534db98
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/E4Z;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/E4b",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2076933
    new-instance v1, LX/E4a;

    invoke-direct {v1, p0}, LX/E4a;-><init>(LX/E4b;)V

    .line 2076934
    iget-object v2, p0, LX/E4b;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E4Z;

    .line 2076935
    if-nez v2, :cond_0

    .line 2076936
    new-instance v2, LX/E4Z;

    invoke-direct {v2, p0}, LX/E4Z;-><init>(LX/E4b;)V

    .line 2076937
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/E4Z;->a$redex0(LX/E4Z;LX/1De;IILX/E4a;)V

    .line 2076938
    move-object v1, v2

    .line 2076939
    move-object v0, v1

    .line 2076940
    return-object v0
.end method
