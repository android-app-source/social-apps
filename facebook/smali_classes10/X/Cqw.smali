.class public final LX/Cqw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cqv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Cqv",
        "<",
        "LX/Cqw;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/Cqw;

.field public static final b:LX/Cqw;

.field public static final c:LX/Cqw;

.field public static final d:LX/Cqw;


# instance fields
.field public final e:LX/Cqu;

.field public final f:LX/Cqt;

.field public final g:F


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1940540
    new-instance v0, LX/Cqw;

    sget-object v1, LX/Cqu;->COLLAPSED:LX/Cqu;

    sget-object v2, LX/Cqt;->PORTRAIT:LX/Cqt;

    invoke-direct {v0, v1, v2, v3}, LX/Cqw;-><init>(LX/Cqu;LX/Cqt;F)V

    sput-object v0, LX/Cqw;->a:LX/Cqw;

    .line 1940541
    new-instance v0, LX/Cqw;

    sget-object v1, LX/Cqu;->EXPANDED:LX/Cqu;

    sget-object v2, LX/Cqt;->PORTRAIT:LX/Cqt;

    invoke-direct {v0, v1, v2, v3}, LX/Cqw;-><init>(LX/Cqu;LX/Cqt;F)V

    sput-object v0, LX/Cqw;->b:LX/Cqw;

    .line 1940542
    new-instance v0, LX/Cqw;

    sget-object v1, LX/Cqu;->COLLAPSED:LX/Cqu;

    sget-object v2, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    invoke-direct {v0, v1, v2, v3}, LX/Cqw;-><init>(LX/Cqu;LX/Cqt;F)V

    sput-object v0, LX/Cqw;->c:LX/Cqw;

    .line 1940543
    new-instance v0, LX/Cqw;

    sget-object v1, LX/Cqu;->COLLAPSED:LX/Cqu;

    sget-object v2, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    invoke-direct {v0, v1, v2, v3}, LX/Cqw;-><init>(LX/Cqu;LX/Cqt;F)V

    sput-object v0, LX/Cqw;->d:LX/Cqw;

    return-void
.end method

.method public constructor <init>(LX/Cqu;LX/Cqt;F)V
    .locals 0

    .prologue
    .line 1940535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1940536
    iput-object p1, p0, LX/Cqw;->e:LX/Cqu;

    .line 1940537
    iput-object p2, p0, LX/Cqw;->f:LX/Cqt;

    .line 1940538
    iput p3, p0, LX/Cqw;->g:F

    .line 1940539
    return-void
.end method


# virtual methods
.method public final a(LX/Cqv;F)LX/Cqv;
    .locals 2

    .prologue
    .line 1940526
    check-cast p1, LX/Cqw;

    .line 1940527
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    .line 1940528
    :goto_0
    return-object p0

    .line 1940529
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-nez v0, :cond_1

    move-object p0, p1

    .line 1940530
    goto :goto_0

    .line 1940531
    :cond_1
    new-instance p0, LX/Cqw;

    .line 1940532
    iget-object v0, p1, LX/Cqw;->e:LX/Cqu;

    move-object v0, v0

    .line 1940533
    iget-object v1, p1, LX/Cqw;->f:LX/Cqt;

    move-object v1, v1

    .line 1940534
    invoke-direct {p0, v0, v1, p2}, LX/Cqw;-><init>(LX/Cqu;LX/Cqt;F)V

    goto :goto_0
.end method

.method public final e()LX/Cqw;
    .locals 3

    .prologue
    .line 1940519
    sget-object v0, LX/Cqw;->b:LX/Cqw;

    .line 1940520
    iget-object v1, p0, LX/Cqw;->e:LX/Cqu;

    sget-object v2, LX/Cqu;->EXPANDED:LX/Cqu;

    if-eq v1, v2, :cond_0

    .line 1940521
    sget-object v1, LX/Cqs;->a:[I

    iget-object v2, p0, LX/Cqw;->f:LX/Cqt;

    invoke-virtual {v2}, LX/Cqt;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1940522
    :cond_0
    :goto_0
    return-object v0

    .line 1940523
    :pswitch_0
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    goto :goto_0

    .line 1940524
    :pswitch_1
    sget-object v0, LX/Cqw;->c:LX/Cqw;

    goto :goto_0

    .line 1940525
    :pswitch_2
    sget-object v0, LX/Cqw;->d:LX/Cqw;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1940512
    if-ne p0, p1, :cond_1

    .line 1940513
    :cond_0
    :goto_0
    return v0

    .line 1940514
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1940515
    goto :goto_0

    .line 1940516
    :cond_3
    check-cast p1, LX/Cqw;

    .line 1940517
    iget-object v2, p0, LX/Cqw;->e:LX/Cqu;

    iget-object v3, p1, LX/Cqw;->e:LX/Cqu;

    invoke-virtual {v2, v3}, LX/Cqu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Cqw;->f:LX/Cqt;

    iget-object v3, p1, LX/Cqw;->f:LX/Cqt;

    invoke-virtual {v2, v3}, LX/Cqt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, LX/Cqw;->g:F

    iget v3, p1, LX/Cqw;->g:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1940518
    iget-object v0, p0, LX/Cqw;->e:LX/Cqu;

    invoke-virtual {v0}, LX/Cqu;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/Cqw;->f:LX/Cqt;

    invoke-virtual {v1}, LX/Cqt;->hashCode()I

    move-result v1

    iget v2, p0, LX/Cqw;->g:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1f

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    return v0
.end method
