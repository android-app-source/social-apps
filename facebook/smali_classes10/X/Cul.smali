.class public final LX/Cul;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Cuk;

.field public b:LX/Cuj;

.field public c:LX/Cuk;


# direct methods
.method public constructor <init>(LX/Cuk;LX/Cuj;LX/Cuk;)V
    .locals 0

    .prologue
    .line 1947136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1947137
    iput-object p1, p0, LX/Cul;->a:LX/Cuk;

    .line 1947138
    iput-object p2, p0, LX/Cul;->b:LX/Cuj;

    .line 1947139
    iput-object p3, p0, LX/Cul;->c:LX/Cuk;

    .line 1947140
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1947141
    if-ne p0, p1, :cond_1

    .line 1947142
    :cond_0
    :goto_0
    return v0

    .line 1947143
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1947144
    goto :goto_0

    .line 1947145
    :cond_3
    check-cast p1, LX/Cul;

    .line 1947146
    iget-object v2, p0, LX/Cul;->a:LX/Cuk;

    iget-object v3, p1, LX/Cul;->a:LX/Cuk;

    invoke-virtual {v2, v3}, LX/Cuk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Cul;->b:LX/Cuj;

    iget-object v3, p1, LX/Cul;->b:LX/Cuj;

    invoke-virtual {v2, v3}, LX/Cuj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Cul;->c:LX/Cuk;

    iget-object v3, p1, LX/Cul;->c:LX/Cuk;

    invoke-virtual {v2, v3}, LX/Cuk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1947147
    iget-object v0, p0, LX/Cul;->a:LX/Cuk;

    iget-object v1, p0, LX/Cul;->b:LX/Cuj;

    iget-object v2, p0, LX/Cul;->c:LX/Cuk;

    invoke-static {v0, v1, v2}, LX/1bi;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
