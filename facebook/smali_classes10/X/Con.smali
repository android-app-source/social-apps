.class public LX/Con;
.super LX/Coi;
.source ""

# interfaces
.implements LX/CoW;


# instance fields
.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:I

.field public final c:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1936098
    invoke-direct {p0, p1}, LX/Coi;-><init>(Landroid/view/View;)V

    .line 1936099
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/Con;->b:I

    .line 1936100
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b12b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/Con;->c:I

    .line 1936101
    const-class v0, LX/Con;

    invoke-static {v0, p0}, LX/Con;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936102
    iget-object v0, p0, LX/Con;->a:LX/Ck0;

    iget-object v1, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v2, 0x7f0d011d

    const v4, 0x7f0d011d

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936103
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Con;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object p0

    check-cast p0, LX/Ck0;

    iput-object p0, p1, LX/Con;->a:LX/Ck0;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1936104
    iget v0, p0, LX/Con;->b:I

    return v0
.end method
