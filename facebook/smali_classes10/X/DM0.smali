.class public final LX/DM0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 1989480
    const-wide/16 v6, 0x0

    .line 1989481
    const/4 v5, 0x0

    .line 1989482
    const/4 v4, 0x0

    .line 1989483
    const/4 v3, 0x0

    .line 1989484
    const/4 v2, 0x0

    .line 1989485
    const/4 v1, 0x0

    .line 1989486
    const/4 v0, 0x0

    .line 1989487
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 1989488
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1989489
    const/4 v0, 0x0

    .line 1989490
    :goto_0
    return v0

    .line 1989491
    :cond_0
    const-string v4, "download_url"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1989492
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v10, v1

    .line 1989493
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v4, :cond_7

    .line 1989494
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 1989495
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1989496
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v1, :cond_1

    .line 1989497
    const-string v4, "created_time"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1989498
    const/4 v0, 0x1

    .line 1989499
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 1989500
    :cond_2
    const-string v4, "file_id"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1989501
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 1989502
    :cond_3
    const-string v4, "name"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1989503
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 1989504
    :cond_4
    const-string v4, "original_post"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1989505
    invoke-static {p0, p1}, LX/DLy;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto :goto_1

    .line 1989506
    :cond_5
    const-string v4, "owner"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1989507
    invoke-static {p0, p1}, LX/DLz;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto :goto_1

    .line 1989508
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1989509
    :cond_7
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1989510
    if-eqz v0, :cond_8

    .line 1989511
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1989512
    :cond_8
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1989513
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1989514
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1989515
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1989516
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1989517
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v8, v3

    move v9, v4

    move v10, v5

    move v11, v2

    move-wide v2, v6

    move v6, v1

    move v7, v11

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1989518
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1989519
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1989520
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 1989521
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1989522
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1989523
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1989524
    if-eqz v0, :cond_1

    .line 1989525
    const-string v1, "download_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1989526
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1989527
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1989528
    if-eqz v0, :cond_2

    .line 1989529
    const-string v1, "file_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1989530
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1989531
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1989532
    if-eqz v0, :cond_3

    .line 1989533
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1989534
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1989535
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1989536
    if-eqz v0, :cond_4

    .line 1989537
    const-string v1, "original_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1989538
    invoke-static {p0, v0, p2}, LX/DLy;->a(LX/15i;ILX/0nX;)V

    .line 1989539
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1989540
    if-eqz v0, :cond_5

    .line 1989541
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1989542
    invoke-static {p0, v0, p2}, LX/DLz;->a(LX/15i;ILX/0nX;)V

    .line 1989543
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1989544
    return-void
.end method
