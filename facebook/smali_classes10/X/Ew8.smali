.class public final LX/Ew8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EwG;


# direct methods
.method public constructor <init>(LX/EwG;)V
    .locals 0

    .prologue
    .line 2182729
    iput-object p1, p0, LX/Ew8;->a:LX/EwG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x783bc2a2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2182730
    iget-object v1, p0, LX/Ew8;->a:LX/EwG;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2182731
    iget-object v4, v1, LX/EwG;->u:LX/Ew0;

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 2182732
    const/4 v5, 0x3

    new-array v5, v5, [Z

    aput-boolean v10, v5, v6

    iget-object v7, v4, LX/Ew0;->f:LX/0ad;

    sget-short v8, LX/2hr;->D:S

    invoke-interface {v7, v8, v6}, LX/0ad;->a(SZ)Z

    move-result v7

    aput-boolean v7, v5, v10

    const/4 v7, 0x2

    iget-object v8, v4, LX/Ew0;->f:LX/0ad;

    sget-short v9, LX/2hr;->C:S

    invoke-interface {v8, v9, v6}, LX/0ad;->a(SZ)Z

    move-result v8

    aput-boolean v8, v5, v7

    iput-object v5, v4, LX/Ew0;->b:[Z

    .line 2182733
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2182734
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move v5, v6

    .line 2182735
    :goto_0
    iget-object v9, v4, LX/Ew0;->b:[Z

    array-length v9, v9

    if-ge v5, v9, :cond_1

    .line 2182736
    iget-object v9, v4, LX/Ew0;->b:[Z

    aget-boolean v9, v9, v5

    if-eqz v9, :cond_0

    .line 2182737
    iget-object v9, v4, LX/Ew0;->c:[LX/5P1;

    aget-object v9, v9, v5

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2182738
    iget-object v9, v4, LX/Ew0;->c:[LX/5P1;

    aget-object v9, v9, v5

    .line 2182739
    sget-object v11, LX/Evz;->a:[I

    invoke-virtual {v9}, LX/5P1;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 2182740
    iget-object v11, v4, LX/Ew0;->a:Landroid/content/res/Resources;

    const v12, 0x7f080f98

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    :goto_1
    move-object v9, v11

    .line 2182741
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2182742
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2182743
    :cond_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v5, v10, :cond_3

    .line 2182744
    const/4 v5, 0x0

    iput-object v5, v4, LX/Ew0;->e:[Ljava/lang/CharSequence;

    .line 2182745
    :goto_2
    sget-object v4, LX/5P1;->NONE:LX/5P1;

    iput-object v4, v1, LX/EwG;->t:LX/5P1;

    .line 2182746
    new-instance v4, LX/0ju;

    const v5, 0x7f0e024b

    invoke-direct {v4, v2, v5}, LX/0ju;-><init>(Landroid/content/Context;I)V

    const v5, 0x7f080f95

    invoke-virtual {v4, v5}, LX/0ju;->a(I)LX/0ju;

    move-result-object v4

    const v5, 0x7f080017

    new-instance v6, LX/Ew2;

    invoke-direct {v6, v1}, LX/Ew2;-><init>(LX/EwG;)V

    invoke-virtual {v4, v5, v6}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    const v5, 0x7f080f96

    new-instance v6, LX/Ew1;

    invoke-direct {v6, v1}, LX/Ew1;-><init>(LX/EwG;)V

    invoke-virtual {v4, v5, v6}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    .line 2182747
    iget-object v5, v1, LX/EwG;->u:LX/Ew0;

    .line 2182748
    iget-object v6, v5, LX/Ew0;->e:[Ljava/lang/CharSequence;

    move-object v5, v6

    .line 2182749
    if-nez v5, :cond_2

    .line 2182750
    const v5, 0x7f080f97

    invoke-virtual {v4, v5}, LX/0ju;->b(I)LX/0ju;

    .line 2182751
    :goto_3
    invoke-virtual {v4}, LX/0ju;->a()LX/2EJ;

    move-result-object v4

    invoke-virtual {v4}, LX/2EJ;->show()V

    .line 2182752
    iget-object v4, v1, LX/EwG;->b:LX/Euk;

    iget-wide v6, v1, LX/EwG;->r:J

    .line 2182753
    iget-object v5, v4, LX/Euk;->a:LX/0if;

    sget-object v8, LX/0ig;->P:LX/0ih;

    sget-object v9, LX/Euj;->DELETE_ALL_CLICKED:LX/Euj;

    invoke-virtual {v9}, LX/Euj;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v6, v7, v9}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 2182754
    const v1, 0x3d239324

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2182755
    :cond_2
    const/4 v6, 0x0

    new-instance v7, LX/Ew3;

    invoke-direct {v7, v1}, LX/Ew3;-><init>(LX/EwG;)V

    invoke-virtual {v4, v5, v6, v7}, LX/0ju;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_3

    .line 2182756
    :cond_3
    new-array v5, v6, [LX/5P1;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [LX/5P1;

    iput-object v5, v4, LX/Ew0;->d:[LX/5P1;

    .line 2182757
    new-array v5, v6, [Ljava/lang/CharSequence;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/CharSequence;

    iput-object v5, v4, LX/Ew0;->e:[Ljava/lang/CharSequence;

    goto :goto_2

    .line 2182758
    :pswitch_0
    iget-object v11, v4, LX/Ew0;->a:Landroid/content/res/Resources;

    const v12, 0x7f080f98

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    .line 2182759
    :pswitch_1
    iget-object v11, v4, LX/Ew0;->a:Landroid/content/res/Resources;

    const v12, 0x7f080f9a

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    .line 2182760
    :pswitch_2
    iget-object v11, v4, LX/Ew0;->a:Landroid/content/res/Resources;

    const v12, 0x7f080f99

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
