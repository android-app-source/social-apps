.class public final enum LX/Dyq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dyq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dyq;

.field public static final enum EDIT_MENU:LX/Dyq;

.field public static final enum LONG_PRESS:LX/Dyq;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2064855
    new-instance v0, LX/Dyq;

    const-string v1, "LONG_PRESS"

    invoke-direct {v0, v1, v2}, LX/Dyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dyq;->LONG_PRESS:LX/Dyq;

    .line 2064856
    new-instance v0, LX/Dyq;

    const-string v1, "EDIT_MENU"

    invoke-direct {v0, v1, v3}, LX/Dyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dyq;->EDIT_MENU:LX/Dyq;

    .line 2064857
    const/4 v0, 0x2

    new-array v0, v0, [LX/Dyq;

    sget-object v1, LX/Dyq;->LONG_PRESS:LX/Dyq;

    aput-object v1, v0, v2

    sget-object v1, LX/Dyq;->EDIT_MENU:LX/Dyq;

    aput-object v1, v0, v3

    sput-object v0, LX/Dyq;->$VALUES:[LX/Dyq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2064858
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dyq;
    .locals 1

    .prologue
    .line 2064859
    const-class v0, LX/Dyq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dyq;

    return-object v0
.end method

.method public static values()[LX/Dyq;
    .locals 1

    .prologue
    .line 2064860
    sget-object v0, LX/Dyq;->$VALUES:[LX/Dyq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dyq;

    return-object v0
.end method
