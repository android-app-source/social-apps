.class public LX/Eld;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/ElQ;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/ElQ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164569
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/ElQ;
    .locals 5

    .prologue
    .line 2164557
    sget-object v0, LX/Eld;->a:LX/ElQ;

    if-nez v0, :cond_1

    .line 2164558
    const-class v1, LX/Eld;

    monitor-enter v1

    .line 2164559
    :try_start_0
    sget-object v0, LX/Eld;->a:LX/ElQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2164560
    if-eqz v2, :cond_0

    .line 2164561
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2164562
    invoke-static {v0}, LX/Ele;->a(LX/0QB;)LX/Ekx;

    move-result-object v3

    check-cast v3, LX/Ekx;

    const/16 v4, 0x19e

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/Elc;->a(LX/0QB;)LX/ElF;

    move-result-object v4

    check-cast v4, LX/ElF;

    invoke-static {v3, p0, v4}, LX/Elb;->a(LX/Ekx;LX/0Or;LX/ElF;)LX/ElQ;

    move-result-object v3

    move-object v0, v3

    .line 2164563
    sput-object v0, LX/Eld;->a:LX/ElQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2164564
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2164565
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2164566
    :cond_1
    sget-object v0, LX/Eld;->a:LX/ElQ;

    return-object v0

    .line 2164567
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2164568
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2164570
    invoke-static {p0}, LX/Ele;->a(LX/0QB;)LX/Ekx;

    move-result-object v0

    check-cast v0, LX/Ekx;

    const/16 v1, 0x19e

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/Elc;->a(LX/0QB;)LX/ElF;

    move-result-object v1

    check-cast v1, LX/ElF;

    invoke-static {v0, v2, v1}, LX/Elb;->a(LX/Ekx;LX/0Or;LX/ElF;)LX/ElQ;

    move-result-object v0

    return-object v0
.end method
