.class public final LX/E1u;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E1v;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/9qL;

.field public b:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

.field public c:LX/5sY;

.field public final synthetic d:LX/E1v;


# direct methods
.method public constructor <init>(LX/E1v;)V
    .locals 1

    .prologue
    .line 2071858
    iput-object p1, p0, LX/E1u;->d:LX/E1v;

    .line 2071859
    move-object v0, p1

    .line 2071860
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2071861
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2071862
    const-string v0, "ReactionCoreButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2071863
    if-ne p0, p1, :cond_1

    .line 2071864
    :cond_0
    :goto_0
    return v0

    .line 2071865
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2071866
    goto :goto_0

    .line 2071867
    :cond_3
    check-cast p1, LX/E1u;

    .line 2071868
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2071869
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2071870
    if-eq v2, v3, :cond_0

    .line 2071871
    iget-object v2, p0, LX/E1u;->a:LX/9qL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E1u;->a:LX/9qL;

    iget-object v3, p1, LX/E1u;->a:LX/9qL;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2071872
    goto :goto_0

    .line 2071873
    :cond_5
    iget-object v2, p1, LX/E1u;->a:LX/9qL;

    if-nez v2, :cond_4

    .line 2071874
    :cond_6
    iget-object v2, p0, LX/E1u;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E1u;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    iget-object v3, p1, LX/E1u;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2071875
    goto :goto_0

    .line 2071876
    :cond_8
    iget-object v2, p1, LX/E1u;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    if-nez v2, :cond_7

    .line 2071877
    :cond_9
    iget-object v2, p0, LX/E1u;->c:LX/5sY;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/E1u;->c:LX/5sY;

    iget-object v3, p1, LX/E1u;->c:LX/5sY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2071878
    goto :goto_0

    .line 2071879
    :cond_a
    iget-object v2, p1, LX/E1u;->c:LX/5sY;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
