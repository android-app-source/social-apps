.class public final LX/EiW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/confirmation/graphql/FBAddContactpointFragmentsModels$FBAddContactpointCoreMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/growth/model/Contactpoint;

.field public final synthetic c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/fragment/ConfContactpointFragment;Ljava/lang/String;Lcom/facebook/growth/model/Contactpoint;)V
    .locals 0

    .prologue
    .line 2159763
    iput-object p1, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iput-object p2, p0, LX/EiW;->a:Ljava/lang/String;

    iput-object p3, p0, LX/EiW;->b:Lcom/facebook/growth/model/Contactpoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2159778
    instance-of v1, p1, LX/4Ua;

    if-eqz v1, :cond_0

    .line 2159779
    check-cast p1, LX/4Ua;

    .line 2159780
    iget-object v1, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 2159781
    if-nez v1, :cond_1

    .line 2159782
    :cond_0
    :goto_0
    iget-object v1, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Ljava/lang/String;)V

    .line 2159783
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "error code"

    invoke-virtual {v1, v2, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "phone number"

    iget-object v2, p0, LX/EiW;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2159784
    iget-object v1, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->f:LX/2U9;

    sget-object v2, LX/Eiw;->INVALID_NUMBER:LX/Eiw;

    const-string v3, "native flow"

    invoke-virtual {v1, v2, v3, v0}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159785
    return-void

    .line 2159786
    :cond_1
    iget-object v0, v1, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2159764
    const/4 v4, 0x0

    .line 2159765
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "phone number"

    iget-object v2, p0, LX/EiW;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2159766
    iget-object v1, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->f:LX/2U9;

    sget-object v2, LX/Eiw;->VALID_NUMBER:LX/Eiw;

    const-string v3, "native flow"

    invoke-virtual {v1, v2, v3, v0}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159767
    iget-object v0, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159768
    iget-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->b:Z

    move v0, v1

    .line 2159769
    if-eqz v0, :cond_0

    .line 2159770
    iget-object v0, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->d:LX/EjB;

    invoke-virtual {v0}, LX/EjB;->a()V

    .line 2159771
    :cond_0
    iget-object v0, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    iget-object v1, p0, LX/EiW;->b:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0, v1}, Lcom/facebook/confirmation/model/AccountConfirmationData;->a(Lcom/facebook/growth/model/Contactpoint;)V

    .line 2159772
    iget-object v0, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    const/4 v1, 0x1

    .line 2159773
    iput-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->f:Z

    .line 2159774
    iget-object v0, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->f:LX/2U9;

    sget-object v1, LX/Eiw;->BACKGROUND_CONFIRM_START:LX/Eiw;

    invoke-virtual {v0, v1, v4, v4}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159775
    iget-object v0, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U8;

    iget-object v1, p0, LX/EiW;->b:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0, v1}, LX/2U8;->a(Lcom/facebook/growth/model/Contactpoint;)Z

    .line 2159776
    iget-object v0, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v1, p0, LX/EiW;->c:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    invoke-virtual {v1}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->o()LX/EiG;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(LX/EiG;)V

    .line 2159777
    return-void
.end method
