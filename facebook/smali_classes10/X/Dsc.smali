.class public final LX/Dsc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:LX/3Cv;


# direct methods
.method public constructor <init>(LX/3Cv;)V
    .locals 0

    .prologue
    .line 2050523
    iput-object p1, p0, LX/Dsc;->a:LX/3Cv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2050522
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 2050520
    iget-object v0, p0, LX/Dsc;->a:LX/3Cv;

    invoke-static {v0}, LX/3Cv;->h(LX/3Cv;)V

    .line 2050521
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2050519
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2050515
    iget-object v0, p0, LX/Dsc;->a:LX/3Cv;

    iget-object v0, v0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->setPct(F)V

    .line 2050516
    iget-object v0, p0, LX/Dsc;->a:LX/3Cv;

    iget-object v0, v0, LX/3Cv;->d:Lcom/facebook/notifications/widget/RevealAnimationOverlayView;

    invoke-virtual {v0, v2}, Lcom/facebook/notifications/widget/RevealAnimationOverlayView;->setVisibility(I)V

    .line 2050517
    iget-object v0, p0, LX/Dsc;->a:LX/3Cv;

    iget-object v0, v0, LX/3Cv;->b:Lcom/facebook/notifications/widget/CaspianNotificationsView;

    invoke-virtual {v0, v2}, Lcom/facebook/notifications/widget/CaspianNotificationsView;->setVisibility(I)V

    .line 2050518
    return-void
.end method
