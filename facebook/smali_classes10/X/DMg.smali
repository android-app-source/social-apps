.class public LX/DMg;
.super LX/DK1;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0gc;Ljava/lang/String;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 1990098
    invoke-direct {p0, p1}, LX/DK1;-><init>(LX/0gc;)V

    .line 1990099
    iput-object p2, p0, LX/DMg;->b:Ljava/lang/String;

    .line 1990100
    iput-object p3, p0, LX/DMg;->a:Landroid/content/res/Resources;

    .line 1990101
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1990111
    packed-switch p1, :pswitch_data_0

    .line 1990112
    const-string v0, ""

    :goto_0
    return-object v0

    .line 1990113
    :pswitch_0
    iget-object v0, p0, LX/DMg;->a:Landroid/content/res/Resources;

    const v1, 0x7f0830ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1990114
    :pswitch_1
    iget-object v0, p0, LX/DMg;->a:Landroid/content/res/Resources;

    const v1, 0x7f0830bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 1990103
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1990104
    const-string v0, "group_feed_id"

    iget-object v2, p0, LX/DMg;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1990105
    packed-switch p1, :pswitch_data_0

    .line 1990106
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1990107
    :pswitch_0
    new-instance v0, Lcom/facebook/groups/events/GroupUpcomingEventsFragment;

    invoke-direct {v0}, Lcom/facebook/groups/events/GroupUpcomingEventsFragment;-><init>()V

    .line 1990108
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1990109
    :pswitch_1
    new-instance v0, Lcom/facebook/groups/events/GroupPastEventsFragment;

    invoke-direct {v0}, Lcom/facebook/groups/events/GroupPastEventsFragment;-><init>()V

    .line 1990110
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1990102
    const/4 v0, 0x2

    return v0
.end method
