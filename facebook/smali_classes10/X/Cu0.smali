.class public LX/Cu0;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

.field private final b:Ljava/lang/String;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(LX/Ctg;Lcom/facebook/richdocument/view/widget/RichDocumentImageView;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 1945867
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1945868
    iput-object p2, p0, LX/Cu0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    .line 1945869
    iput-object p3, p0, LX/Cu0;->b:Ljava/lang/String;

    .line 1945870
    iput p4, p0, LX/Cu0;->c:I

    .line 1945871
    iput p5, p0, LX/Cu0;->d:I

    .line 1945872
    return-void
.end method


# virtual methods
.method public final a(LX/Crd;)Z
    .locals 4

    .prologue
    .line 1945873
    sget-object v0, LX/Crd;->CLICK_MEDIA:LX/Crd;

    if-ne p1, v0, :cond_0

    .line 1945874
    iget-object v0, p0, LX/Cu0;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 1945875
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 1945876
    move-object v0, v0

    .line 1945877
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1945878
    iget-object v1, p0, LX/Cu0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->setFadeDuration(I)V

    .line 1945879
    iget-object v1, p0, LX/Cu0;->a:Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    iget v2, p0, LX/Cu0;->c:I

    iget v3, p0, LX/Cu0;->d:I

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(LX/1bf;II)V

    .line 1945880
    :cond_0
    invoke-super {p0, p1}, LX/Cts;->a(LX/Crd;)Z

    move-result v0

    return v0
.end method
