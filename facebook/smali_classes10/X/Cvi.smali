.class public LX/Cvi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gT;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/6VT;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/6VT;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1949177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949178
    iput-object p1, p0, LX/Cvi;->a:Ljava/util/Map;

    .line 1949179
    return-void
.end method


# virtual methods
.method public final serialize(LX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 1949181
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1949182
    iget-object v0, p0, LX/Cvi;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1949183
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1949184
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1949185
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1949186
    const-string v2, "unit"

    invoke-virtual {p1, v2, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1949187
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1949188
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1949189
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 1949190
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6VT;

    .line 1949191
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1949192
    const-string v4, "type"

    const-string v5, "image"

    invoke-virtual {p1, v4, v5}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1949193
    const-string v4, "start"

    .line 1949194
    iget-wide v8, v1, LX/6VT;->a:J

    move-wide v6, v8

    .line 1949195
    invoke-virtual {p1, v4, v6, v7}, LX/0nX;->a(Ljava/lang/String;J)V

    .line 1949196
    const-string v4, "end"

    .line 1949197
    iget-wide v8, v1, LX/6VT;->b:J

    move-wide v6, v8

    .line 1949198
    invoke-virtual {p1, v4, v6, v7}, LX/0nX;->a(Ljava/lang/String;J)V

    .line 1949199
    const-string v4, "status"

    .line 1949200
    iget v5, v1, LX/6VT;->c:I

    move v1, v5

    .line 1949201
    invoke-virtual {p1, v4, v1}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 1949202
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1949203
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1949204
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    goto :goto_0

    .line 1949205
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1949206
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 2

    .prologue
    .line 1949180
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Serialization infrastructure does not support type serialization."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
