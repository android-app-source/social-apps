.class public final LX/ENs;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ENu;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleInterfaces$SearchResultsOpinionModule;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Ps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:I

.field public final synthetic e:LX/ENu;


# direct methods
.method public constructor <init>(LX/ENu;)V
    .locals 1

    .prologue
    .line 2113611
    iput-object p1, p0, LX/ENs;->e:LX/ENu;

    .line 2113612
    move-object v0, p1

    .line 2113613
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2113614
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2113615
    const-string v0, "SearchResultsOpinionSearchQueryDisplayComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2113616
    if-ne p0, p1, :cond_1

    .line 2113617
    :cond_0
    :goto_0
    return v0

    .line 2113618
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2113619
    goto :goto_0

    .line 2113620
    :cond_3
    check-cast p1, LX/ENs;

    .line 2113621
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2113622
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2113623
    if-eq v2, v3, :cond_0

    .line 2113624
    iget-object v2, p0, LX/ENs;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ENs;->a:LX/CzL;

    iget-object v3, p1, LX/ENs;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2113625
    goto :goto_0

    .line 2113626
    :cond_5
    iget-object v2, p1, LX/ENs;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2113627
    :cond_6
    iget-object v2, p0, LX/ENs;->b:LX/1Ps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ENs;->b:LX/1Ps;

    iget-object v3, p1, LX/ENs;->b:LX/1Ps;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2113628
    goto :goto_0

    .line 2113629
    :cond_8
    iget-object v2, p1, LX/ENs;->b:LX/1Ps;

    if-nez v2, :cond_7

    .line 2113630
    :cond_9
    iget-boolean v2, p0, LX/ENs;->c:Z

    iget-boolean v3, p1, LX/ENs;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2113631
    goto :goto_0

    .line 2113632
    :cond_a
    iget v2, p0, LX/ENs;->d:I

    iget v3, p1, LX/ENs;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2113633
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2113634
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/ENs;

    .line 2113635
    const/4 v1, 0x0

    iput v1, v0, LX/ENs;->d:I

    .line 2113636
    return-object v0
.end method
