.class public LX/DMe;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Integer;


# instance fields
.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/1Ck;

.field public final e:Landroid/content/res/Resources;

.field public final f:Ljava/lang/String;

.field private final g:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/DMT;

.field public j:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/EventTimeframe;
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:I

.field public n:LX/1My;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1990051
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, LX/DMe;->a:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/1Ck;Landroid/content/res/Resources;LX/1My;Ljava/lang/String;ILX/DMT;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/DMT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/EventTimeframe;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1990052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1990053
    new-instance v0, LX/DMa;

    invoke-direct {v0, p0}, LX/DMa;-><init>(LX/DMe;)V

    iput-object v0, p0, LX/DMe;->g:LX/0TF;

    .line 1990054
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1990055
    iput-object v0, p0, LX/DMe;->h:LX/0Px;

    .line 1990056
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DMe;->l:Z

    .line 1990057
    iput-object p1, p0, LX/DMe;->b:LX/0tX;

    .line 1990058
    iput-object p2, p0, LX/DMe;->c:Ljava/util/concurrent/ExecutorService;

    .line 1990059
    iput-object p3, p0, LX/DMe;->d:LX/1Ck;

    .line 1990060
    iput-object p4, p0, LX/DMe;->e:Landroid/content/res/Resources;

    .line 1990061
    iput-object p6, p0, LX/DMe;->f:Ljava/lang/String;

    .line 1990062
    iput-object p8, p0, LX/DMe;->i:LX/DMT;

    .line 1990063
    iput-object p9, p0, LX/DMe;->j:Ljava/lang/String;

    .line 1990064
    iput p7, p0, LX/DMe;->m:I

    .line 1990065
    iput-object p5, p0, LX/DMe;->n:LX/1My;

    .line 1990066
    return-void
.end method

.method public static a$redex0(LX/DMe;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 9

    .prologue
    .line 1990067
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1990068
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1990069
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1990070
    iget-object v0, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v0

    .line 1990071
    iget-wide v7, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v7

    .line 1990072
    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 1990073
    iget-object v0, p0, LX/DMe;->n:LX/1My;

    iget-object v2, p0, LX/DMe;->g:LX/0TF;

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1990074
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1990075
    iget-boolean v0, p0, LX/DMe;->l:Z

    if-nez v0, :cond_0

    .line 1990076
    iget-object v0, p0, LX/DMe;->d:LX/1Ck;

    sget-object v1, LX/DMd;->FETCH_GROUP_EVENTS:LX/DMd;

    new-instance v2, LX/DMb;

    invoke-direct {v2, p0}, LX/DMb;-><init>(LX/DMe;)V

    new-instance v3, LX/DMc;

    invoke-direct {v3, p0}, LX/DMc;-><init>(LX/DMe;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1990077
    :cond_0
    return-void
.end method

.method public final a(LX/7vF;Z)V
    .locals 6

    .prologue
    .line 1990078
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1990079
    iget-object v0, p0, LX/DMe;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/DMe;->h:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    .line 1990080
    iget-object v4, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1990081
    invoke-virtual {p1}, LX/7vF;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1990082
    iget-object v0, p1, LX/7vF;->b:Lcom/facebook/events/model/Event;

    move-object v0, v0

    .line 1990083
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1990084
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1990085
    :cond_0
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1990086
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/DMe;->h:LX/0Px;

    .line 1990087
    iget-object v0, p0, LX/DMe;->i:LX/DMT;

    iget-object v1, p0, LX/DMe;->h:LX/0Px;

    invoke-virtual {v0, v1, p2}, LX/DMT;->a(LX/0Px;Z)V

    .line 1990088
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1990089
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DMe;->l:Z

    .line 1990090
    const/4 v0, 0x0

    iput-object v0, p0, LX/DMe;->k:Ljava/lang/String;

    .line 1990091
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1990092
    iput-object v0, p0, LX/DMe;->h:LX/0Px;

    .line 1990093
    return-void
.end method
