.class public final LX/EtR;
.super LX/3ut;
.source ""


# instance fields
.field public g:Landroid/content/Context;

.field public h:LX/H8D;

.field public i:I

.field public j:Landroid/graphics/drawable/Drawable;

.field private k:LX/EtO;

.field private l:I

.field private m:LX/EtI;

.field private n:Landroid/view/accessibility/AccessibilityManager;

.field public o:LX/0wL;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3ux;IILandroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2178041
    invoke-direct {p0, p1, v0, v0}, LX/3ut;-><init>(Landroid/content/Context;II)V

    .line 2178042
    iput-object p1, p0, LX/EtR;->g:Landroid/content/Context;

    .line 2178043
    iput-object p2, p0, LX/EtR;->f:LX/3ux;

    .line 2178044
    iput p3, p0, LX/EtR;->i:I

    .line 2178045
    iput p4, p0, LX/EtR;->l:I

    .line 2178046
    iput-object p5, p0, LX/EtR;->j:Landroid/graphics/drawable/Drawable;

    .line 2178047
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, LX/EtR;->n:Landroid/view/accessibility/AccessibilityManager;

    .line 2178048
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v0

    check-cast v0, LX/0wL;

    iput-object v0, p0, LX/EtR;->o:LX/0wL;

    .line 2178049
    return-void
.end method

.method public static a$redex0(LX/EtR;LX/EtO;Z)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 2178026
    invoke-virtual {p1}, LX/EtO;->getItemData()LX/3v3;

    move-result-object v2

    .line 2178027
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->l()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2178028
    iget-object v1, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v1}, LX/3v0;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 2178029
    iget-object v3, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v3}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v3}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2178030
    add-int/lit8 v0, v0, 0x1

    .line 2178031
    iget-object v3, p0, LX/EtR;->k:LX/EtO;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v0

    .line 2178032
    :goto_0
    iget-object v3, p0, LX/EtR;->g:Landroid/content/Context;

    .line 2178033
    const v4, 0x7f083375

    .line 2178034
    if-eqz v2, :cond_0

    if-eqz p2, :cond_0

    .line 2178035
    invoke-virtual {v2}, LX/3v3;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2178036
    const v4, 0x7f083376

    .line 2178037
    :cond_0
    :goto_1
    move v2, v4

    .line 2178038
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, LX/EtO;->c()Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v7, v1

    move v1, v0

    move v0, v7

    goto :goto_0

    .line 2178039
    :cond_2
    invoke-virtual {v2}, LX/3v3;->isCheckable()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, LX/3v3;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2178040
    const v4, 0x7f083377

    goto :goto_1
.end method

.method public static h(LX/EtR;)Z
    .locals 1

    .prologue
    .line 2178025
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EtR;->n:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EtR;->n:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)LX/3ux;
    .locals 1

    .prologue
    .line 2178024
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    return-object v0
.end method

.method public final a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2178018
    invoke-virtual {p1}, LX/3v3;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 2178019
    if-nez v0, :cond_2

    .line 2178020
    instance-of v0, p2, LX/EtO;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EtR;->k:LX/EtO;

    if-ne p2, v0, :cond_1

    .line 2178021
    :cond_0
    const/4 p2, 0x0

    .line 2178022
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/3ut;->a(LX/3v3;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2178023
    :cond_2
    return-object v0
.end method

.method public final a(LX/3v0;Z)V
    .locals 0

    .prologue
    .line 2178015
    invoke-virtual {p0}, LX/EtR;->e()Z

    .line 2178016
    invoke-super {p0, p1, p2}, LX/3ut;->a(LX/3v0;Z)V

    .line 2178017
    return-void
.end method

.method public final a(LX/3v3;LX/3uq;)V
    .locals 1

    .prologue
    .line 2178008
    check-cast p2, LX/EtO;

    .line 2178009
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/EtO;->a(LX/3v3;I)V

    .line 2178010
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, LX/EtL;

    .line 2178011
    iput-object v0, p2, LX/EtO;->d:LX/3uw;

    .line 2178012
    invoke-virtual {p1}, LX/3v3;->getItemId()I

    move-result v0

    if-lez v0, :cond_0

    .line 2178013
    invoke-virtual {p1}, LX/3v3;->getItemId()I

    move-result v0

    invoke-virtual {p2, v0}, LX/EtO;->setId(I)V

    .line 2178014
    :cond_0
    return-void
.end method

.method public final a(LX/3v3;)Z
    .locals 1

    .prologue
    .line 2178050
    invoke-virtual {p1}, LX/3v3;->j()Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Z
    .locals 2

    .prologue
    .line 2178005
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, LX/EtR;->k:LX/EtO;

    if-ne v0, v1, :cond_0

    .line 2178006
    const/4 v0, 0x0

    .line 2178007
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, LX/3ut;->a(Landroid/view/ViewGroup;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;)LX/3uq;
    .locals 3

    .prologue
    .line 2178001
    new-instance v0, LX/EtO;

    iget-object v1, p0, LX/EtR;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/EtO;-><init>(Landroid/content/Context;)V

    .line 2178002
    iget v1, p0, LX/EtR;->i:I

    invoke-virtual {v0, v1}, LX/EtO;->a(I)V

    .line 2178003
    new-instance v1, LX/EtQ;

    invoke-direct {v1, p0}, LX/EtQ;-><init>(LX/EtR;)V

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 2178004
    return-object v0
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    .line 2177974
    invoke-super {p0, p1}, LX/3ut;->b(Z)V

    .line 2177975
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    if-nez v0, :cond_1

    .line 2177976
    :cond_0
    :goto_0
    return-void

    .line 2177977
    :cond_1
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v0

    .line 2177978
    :goto_1
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 2177979
    :goto_2
    if-eqz v0, :cond_6

    .line 2177980
    iget-object v0, p0, LX/EtR;->k:LX/EtO;

    if-nez v0, :cond_2

    .line 2177981
    new-instance v0, LX/EtO;

    iget-object v1, p0, LX/3ut;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/EtO;-><init>(Landroid/content/Context;)V

    .line 2177982
    const v1, 0x7f083374

    invoke-virtual {v0, v1}, LX/EtO;->setText(I)V

    .line 2177983
    iget v1, p0, LX/EtR;->i:I

    invoke-virtual {v0, v1}, LX/EtO;->a(I)V

    .line 2177984
    iget-object v1, p0, LX/EtR;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/EtO;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2177985
    const-string v1, "overflow_button"

    invoke-virtual {v0, v1}, LX/EtO;->setTag(Ljava/lang/Object;)V

    .line 2177986
    iget-object v1, p0, LX/EtR;->g:Landroid/content/Context;

    const v2, 0x7f083373

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EtO;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2177987
    new-instance v1, LX/EtP;

    invoke-direct {v1, p0}, LX/EtP;-><init>(LX/EtR;)V

    invoke-virtual {v0, v1}, LX/EtO;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2177988
    new-instance v1, LX/EtQ;

    invoke-direct {v1, p0}, LX/EtQ;-><init>(LX/EtR;)V

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 2177989
    move-object v0, v0

    .line 2177990
    iput-object v0, p0, LX/EtR;->k:LX/EtO;

    .line 2177991
    :cond_2
    iget-object v0, p0, LX/EtR;->k:LX/EtO;

    invoke-virtual {v0}, LX/EtO;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2177992
    iget-object v1, p0, LX/3ut;->f:LX/3ux;

    if-eq v0, v1, :cond_0

    .line 2177993
    if-eqz v0, :cond_3

    .line 2177994
    iget-object v1, p0, LX/EtR;->k:LX/EtO;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2177995
    :cond_3
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, LX/EtL;

    .line 2177996
    iget-object v1, p0, LX/EtR;->k:LX/EtO;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, LX/EtL;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2177997
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2177998
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 2177999
    :cond_6
    iget-object v0, p0, LX/EtR;->k:LX/EtO;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EtR;->k:LX/EtO;

    invoke-virtual {v0}, LX/EtO;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/3ut;->f:LX/3ux;

    if-ne v0, v1, :cond_0

    .line 2178000
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/EtR;->k:LX/EtO;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2177919
    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->j()Ljava/util/ArrayList;

    move-result-object v7

    .line 2177920
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v5

    move v1, v5

    move v3, v5

    move v4, v5

    .line 2177921
    :goto_0
    if-ge v6, v8, :cond_2

    .line 2177922
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 2177923
    invoke-virtual {v0}, LX/3v3;->l()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2177924
    add-int/lit8 v0, v4, 0x1

    move v10, v1

    move v1, v3

    move v3, v0

    move v0, v10

    .line 2177925
    :goto_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 2177926
    :cond_0
    invoke-virtual {v0}, LX/3v3;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2177927
    add-int/lit8 v0, v3, 0x1

    move v3, v4

    move v10, v0

    move v0, v1

    move v1, v10

    goto :goto_1

    :cond_1
    move v0, v2

    move v1, v3

    move v3, v4

    .line 2177928
    goto :goto_1

    .line 2177929
    :cond_2
    add-int v0, v4, v3

    iget v3, p0, LX/EtR;->l:I

    if-le v0, v3, :cond_6

    move v0, v2

    :goto_2
    or-int v3, v1, v0

    .line 2177930
    iget v1, p0, LX/EtR;->l:I

    .line 2177931
    iget v0, p0, LX/EtR;->l:I

    .line 2177932
    if-eqz v3, :cond_3

    .line 2177933
    add-int/lit8 v1, v1, -0x1

    .line 2177934
    add-int/lit8 v0, v0, -0x1

    .line 2177935
    :cond_3
    sub-int v3, v1, v4

    move v6, v5

    move v1, v0

    .line 2177936
    :goto_3
    if-ge v6, v8, :cond_b

    .line 2177937
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 2177938
    invoke-virtual {v0}, LX/3v3;->l()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2177939
    if-lez v1, :cond_7

    move v4, v2

    .line 2177940
    :goto_4
    if-eqz v4, :cond_4

    .line 2177941
    add-int/lit8 v1, v1, -0x1

    .line 2177942
    :cond_4
    invoke-virtual {v0, v4}, LX/3v3;->d(Z)V

    .line 2177943
    :cond_5
    :goto_5
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_3

    :cond_6
    move v0, v5

    .line 2177944
    goto :goto_2

    :cond_7
    move v4, v5

    .line 2177945
    goto :goto_4

    .line 2177946
    :cond_8
    invoke-virtual {v0}, LX/3v3;->k()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2177947
    if-lez v3, :cond_a

    if-lez v1, :cond_a

    move v4, v2

    .line 2177948
    :goto_6
    if-eqz v4, :cond_9

    .line 2177949
    add-int/lit8 v3, v3, -0x1

    .line 2177950
    add-int/lit8 v1, v1, -0x1

    .line 2177951
    :cond_9
    invoke-virtual {v0, v4}, LX/3v3;->d(Z)V

    goto :goto_5

    :cond_a
    move v4, v5

    .line 2177952
    goto :goto_6

    .line 2177953
    :cond_b
    iget-object v0, p0, LX/EtR;->h:LX/H8D;

    if-eqz v0, :cond_c

    .line 2177954
    iget-object v0, p0, LX/EtR;->h:LX/H8D;

    iget-object v1, p0, LX/3ut;->c:LX/3v0;

    .line 2177955
    iget-boolean v3, v0, LX/H8D;->c:Z

    if-eqz v3, :cond_d

    .line 2177956
    :cond_c
    return v2

    .line 2177957
    :cond_d
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/H8D;->c:Z

    .line 2177958
    invoke-virtual {v1}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v3, 0x0

    move v5, v3

    :goto_7
    if-ge v5, v7, :cond_c

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3v3;

    .line 2177959
    iget-object v4, v0, LX/H8D;->a:Ljava/util/Map;

    invoke-virtual {v3}, LX/3v3;->getItemId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, LX/3v3;->setIcon(I)Landroid/view/MenuItem;

    .line 2177960
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_7
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 2177967
    iget-object v0, p0, LX/EtR;->k:LX/EtO;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EtR;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3ut;->c:LX/3v0;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    if-eqz v0, :cond_0

    .line 2177968
    iget-object v0, p0, LX/3ut;->f:LX/3ux;

    check-cast v0, LX/EtL;

    .line 2177969
    invoke-virtual {v0}, LX/EtL;->getBottomSheetMenuStrategy()LX/EtJ;

    move-result-object v0

    .line 2177970
    new-instance v1, LX/EtI;

    iget-object v2, p0, LX/EtR;->g:Landroid/content/Context;

    iget-object v3, p0, LX/3ut;->c:LX/3v0;

    invoke-direct {v1, v2, v3, v0}, LX/EtI;-><init>(Landroid/content/Context;LX/3v0;LX/EtJ;)V

    iput-object v1, p0, LX/EtR;->m:LX/EtI;

    .line 2177971
    iget-object v0, p0, LX/EtR;->m:LX/EtI;

    invoke-virtual {v0}, LX/EtI;->a()V

    .line 2177972
    const/4 v0, 0x1

    .line 2177973
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2177963
    iget-object v0, p0, LX/EtR;->m:LX/EtI;

    if-eqz v0, :cond_0

    .line 2177964
    iget-object v0, p0, LX/EtR;->m:LX/EtI;

    invoke-virtual {v0}, LX/EtI;->c()V

    .line 2177965
    const/4 v0, 0x1

    .line 2177966
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2177962
    invoke-virtual {p0}, LX/EtR;->d()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2177961
    iget-object v0, p0, LX/EtR;->m:LX/EtI;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EtR;->m:LX/EtI;

    invoke-virtual {v0}, LX/EtI;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
