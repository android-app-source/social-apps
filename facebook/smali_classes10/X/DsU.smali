.class public final LX/DsU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3TV;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/widget/NotificationsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 0

    .prologue
    .line 2050462
    iput-object p1, p0, LX/DsU;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2050463
    iget-object v0, p0, LX/DsU;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-virtual {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v0, p1, Lcom/facebook/widget/text/BetterTextView;

    if-nez v0, :cond_1

    .line 2050464
    iget-object v0, p0, LX/DsU;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-virtual {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->k()LX/0g8;

    move-result-object v0

    invoke-interface {v0, p2}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2nq;

    .line 2050465
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2050466
    :cond_0
    iget-object v0, p0, LX/DsU;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->n:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/notifications/widget/NotificationsFragment;->i:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_long_click"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Null story edge in long click"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2050467
    :cond_1
    :goto_0
    return v6

    .line 2050468
    :cond_2
    iget-object v0, p0, LX/DsU;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->N:LX/3D1;

    iget-object v2, p0, LX/DsU;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v2, v2, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    iget-object v3, p0, LX/DsU;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v3, v3, Lcom/facebook/notifications/widget/NotificationsFragment;->O:LX/2jO;

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/2jO;->a(Ljava/lang/String;Ljava/lang/String;)LX/3D3;

    move-result-object v4

    move-object v3, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, LX/3D1;->a(LX/2nq;Landroid/content/Context;Landroid/view/View;LX/3D3;I)V

    goto :goto_0
.end method
