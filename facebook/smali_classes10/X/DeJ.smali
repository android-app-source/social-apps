.class public final LX/DeJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Landroid/net/Uri;",
        "Landroid/graphics/Typeface;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DeE;

.field public final synthetic b:LX/DeK;


# direct methods
.method public constructor <init>(LX/DeK;LX/DeE;)V
    .locals 0

    .prologue
    .line 2021302
    iput-object p1, p0, LX/DeJ;->b:LX/DeK;

    iput-object p2, p0, LX/DeJ;->a:LX/DeE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2021303
    check-cast p1, Landroid/net/Uri;

    .line 2021304
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021305
    iget-object v0, p0, LX/DeJ;->b:LX/DeK;

    iget-object v1, p0, LX/DeJ;->a:LX/DeE;

    .line 2021306
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021307
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021308
    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/graphics/Typeface;->createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 2021309
    iget-object p0, v0, LX/DeK;->d:Landroid/util/LruCache;

    invoke-virtual {p0, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021310
    move-object v0, v2

    .line 2021311
    return-object v0
.end method
