.class public final LX/ErE;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ErF;


# direct methods
.method public constructor <init>(LX/ErF;)V
    .locals 0

    .prologue
    .line 2172689
    iput-object p1, p0, LX/ErE;->a:LX/ErF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2172710
    iget-object v0, p0, LX/ErE;->a:LX/ErF;

    const/4 v1, 0x0

    .line 2172711
    iput-boolean v1, v0, LX/ErF;->f:Z

    .line 2172712
    iget-object v0, p0, LX/ErE;->a:LX/ErF;

    iget-object v0, v0, LX/ErF;->c:LX/03V;

    sget-object v1, LX/ErF;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch potential guests tokens"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2172713
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2172714
    move-object v1, v1

    .line 2172715
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2172716
    iget-object v0, p0, LX/ErE;->a:LX/ErF;

    iget-object v0, v0, LX/ErF;->b:LX/EqQ;

    invoke-virtual {v0}, LX/EqQ;->a()V

    .line 2172717
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2172690
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2172691
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 2172692
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Server returned null"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/ErE;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2172693
    :goto_1
    return-void

    .line 2172694
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2172695
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 2172696
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2172697
    iget-object v5, p0, LX/ErE;->a:LX/ErF;

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 2172698
    :goto_2
    iput-object v0, v5, LX/ErF;->i:Ljava/lang/String;

    .line 2172699
    iget-object v5, p0, LX/ErE;->a:LX/ErF;

    iget-object v0, p0, LX/ErE;->a:LX/ErF;

    iget-object v0, v0, LX/ErF;->i:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_4

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 2172700
    :goto_3
    iput-boolean v0, v5, LX/ErF;->f:Z

    .line 2172701
    const v0, 0x719af298

    invoke-static {v3, v4, v2, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2172702
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-static {v0}, LX/ErF;->b(LX/2uF;)LX/0Px;

    move-result-object v0

    .line 2172703
    if-nez v0, :cond_6

    .line 2172704
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Server returned null"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/ErE;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2172705
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2172706
    :cond_3
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    move v0, v2

    .line 2172707
    goto :goto_3

    .line 2172708
    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    .line 2172709
    :cond_6
    iget-object v1, p0, LX/ErE;->a:LX/ErF;

    iget-object v1, v1, LX/ErF;->b:LX/EqQ;

    iget-object v2, p0, LX/ErE;->a:LX/ErF;

    iget-boolean v2, v2, LX/ErF;->f:Z

    invoke-virtual {v1, v0, v2}, LX/EqQ;->a(LX/0Px;Z)V

    goto/16 :goto_1
.end method
