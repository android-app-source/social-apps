.class public final LX/EY3;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EXx;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EY3;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EY3;


# instance fields
.field public location_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EY2;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2136282
    new-instance v0, LX/EXw;

    invoke-direct {v0}, LX/EXw;-><init>()V

    sput-object v0, LX/EY3;->a:LX/EWZ;

    .line 2136283
    new-instance v0, LX/EY3;

    invoke-direct {v0}, LX/EY3;-><init>()V

    .line 2136284
    sput-object v0, LX/EY3;->c:LX/EY3;

    invoke-direct {v0}, LX/EY3;->l()V

    .line 2136285
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2136277
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2136278
    iput-byte v0, p0, LX/EY3;->memoizedIsInitialized:B

    .line 2136279
    iput v0, p0, LX/EY3;->memoizedSerializedSize:I

    .line 2136280
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2136281
    iput-object v0, p0, LX/EY3;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 2136243
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2136244
    iput-byte v1, p0, LX/EY3;->memoizedIsInitialized:B

    .line 2136245
    iput v1, p0, LX/EY3;->memoizedSerializedSize:I

    .line 2136246
    invoke-direct {p0}, LX/EY3;->l()V

    .line 2136247
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v3

    move v1, v0

    .line 2136248
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 2136249
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v4

    .line 2136250
    sparse-switch v4, :sswitch_data_0

    .line 2136251
    invoke-virtual {p0, p1, v3, p2, v4}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 2136252
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 2136253
    goto :goto_0

    .line 2136254
    :sswitch_1
    and-int/lit8 v4, v0, 0x1

    if-eq v4, v2, :cond_1

    .line 2136255
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/EY3;->location_:Ljava/util/List;

    .line 2136256
    or-int/lit8 v0, v0, 0x1

    .line 2136257
    :cond_1
    iget-object v4, p0, LX/EY3;->location_:Ljava/util/List;

    sget-object v5, LX/EY2;->a:LX/EWZ;

    invoke-virtual {p1, v5, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2136258
    :catch_0
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 2136259
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2136260
    move-object v0, v0

    .line 2136261
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2136262
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 2136263
    iget-object v1, p0, LX/EY3;->location_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EY3;->location_:Ljava/util/List;

    .line 2136264
    :cond_2
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EY3;->unknownFields:LX/EZQ;

    .line 2136265
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2136266
    :cond_3
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 2136267
    iget-object v0, p0, LX/EY3;->location_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY3;->location_:Ljava/util/List;

    .line 2136268
    :cond_4
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EY3;->unknownFields:LX/EZQ;

    .line 2136269
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2136270
    return-void

    .line 2136271
    :catch_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 2136272
    :try_start_2
    new-instance v4, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2136273
    iput-object p0, v4, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2136274
    move-object v0, v4

    .line 2136275
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2136276
    :catchall_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2136238
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2136239
    iput-byte v1, p0, LX/EY3;->memoizedIsInitialized:B

    .line 2136240
    iput v1, p0, LX/EY3;->memoizedSerializedSize:I

    .line 2136241
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EY3;->unknownFields:LX/EZQ;

    .line 2136242
    return-void
.end method

.method public static a(LX/EY3;)LX/EXy;
    .locals 1

    .prologue
    .line 2136237
    invoke-static {}, LX/EXy;->u()LX/EXy;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EXy;->a(LX/EY3;)LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 2136235
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY3;->location_:Ljava/util/List;

    .line 2136236
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2136233
    new-instance v0, LX/EXy;

    invoke-direct {v0, p1}, LX/EXy;-><init>(LX/EYd;)V

    .line 2136234
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    .line 2136227
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2136228
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EY3;->location_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2136229
    const/4 v2, 0x1

    iget-object v0, p0, LX/EY3;->location_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v2, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2136230
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2136231
    :cond_0
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2136232
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2136286
    iget-byte v1, p0, LX/EY3;->memoizedIsInitialized:B

    .line 2136287
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2136288
    :goto_0
    return v0

    .line 2136289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2136290
    :cond_1
    iput-byte v0, p0, LX/EY3;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2136219
    iget v1, p0, LX/EY3;->memoizedSerializedSize:I

    .line 2136220
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 2136221
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 2136222
    :goto_1
    iget-object v0, p0, LX/EY3;->location_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2136223
    const/4 v3, 0x1

    iget-object v0, p0, LX/EY3;->location_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2136224
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2136225
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2136226
    iput v0, p0, LX/EY3;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2136218
    iget-object v0, p0, LX/EY3;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2136217
    sget-object v0, LX/EYC;->L:LX/EYn;

    const-class v1, LX/EY3;

    const-class v2, LX/EXy;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EY3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2136216
    sget-object v0, LX/EY3;->a:LX/EWZ;

    return-object v0
.end method

.method public final j()LX/EXy;
    .locals 1

    .prologue
    .line 2136215
    invoke-static {p0}, LX/EY3;->a(LX/EY3;)LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2136214
    invoke-virtual {p0}, LX/EY3;->j()LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2136213
    invoke-static {}, LX/EXy;->u()LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2136212
    invoke-virtual {p0}, LX/EY3;->j()LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2136211
    sget-object v0, LX/EY3;->c:LX/EY3;

    return-object v0
.end method
