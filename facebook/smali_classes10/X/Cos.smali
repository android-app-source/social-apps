.class public LX/Cos;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnE;
.implements LX/CnH;
.implements LX/CnI;
.implements LX/CnJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/richdocument/presenter/BlockPresenter;",
        "V::",
        "LX/Ct1;",
        ">",
        "LX/Cod",
        "<TT;>;",
        "LX/CnE;",
        "LX/CnH;",
        "LX/CnI;",
        "LX/CnJ;"
    }
.end annotation


# instance fields
.field public final a:LX/Ctg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ctg",
            "<TV;>;"
        }
    .end annotation
.end field

.field public f:LX/CoR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/CrO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/CoV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1936250
    invoke-direct {p0, p2}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1936251
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Cos;

    invoke-static {v0}, LX/CoR;->a(LX/0QB;)LX/CoR;

    move-result-object v3

    check-cast v3, LX/CoR;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/CrO;->a(LX/0QB;)LX/CrO;

    move-result-object v5

    check-cast v5, LX/CrO;

    invoke-static {v0}, LX/CoV;->a(LX/0QB;)LX/CoV;

    move-result-object p2

    check-cast p2, LX/CoV;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v0

    check-cast v0, LX/Crz;

    iput-object v3, v2, LX/Cos;->f:LX/CoR;

    iput-object v4, v2, LX/Cos;->g:LX/03V;

    iput-object v5, v2, LX/Cos;->h:LX/CrO;

    iput-object p2, v2, LX/Cos;->i:LX/CoV;

    iput-object v0, v2, LX/Cos;->j:LX/Crz;

    .line 1936252
    iput-object p1, p0, LX/Cos;->a:LX/Ctg;

    .line 1936253
    invoke-interface {p1}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d16c1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    .line 1936254
    invoke-interface {p1}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v1

    const v2, 0x7f0d16c3

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/Ct1;

    .line 1936255
    iput-object v1, v0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    .line 1936256
    invoke-interface {p1, v0}, LX/Ctg;->setBody(Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;)V

    .line 1936257
    return-void
.end method

.method private c(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/Ctr;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 1936280
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936281
    invoke-interface {v0, p1}, LX/Ctf;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1936282
    invoke-virtual {p0, p1}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v0

    invoke-interface {v0}, LX/Ctr;->c()V

    .line 1936283
    const/4 v0, 0x1

    .line 1936284
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LX/Ctg;LX/CrN;Z)LX/CqX;
    .locals 2

    .prologue
    .line 1936279
    iget-object v0, p0, LX/Cos;->h:LX/CrO;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p2, v1, p1, p3}, LX/CrO;->a(LX/CrN;Landroid/content/Context;LX/Ctg;Z)LX/CqX;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)LX/Ctr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/Ctr;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1936277
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936278
    invoke-interface {v0, p1}, LX/Ctf;->b(Ljava/lang/Class;)LX/Ctr;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/ClV;)V
    .locals 2

    .prologue
    .line 1936267
    if-eqz p1, :cond_0

    .line 1936268
    iget-object v0, p1, LX/ClV;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1936269
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1936270
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1936271
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 1936272
    invoke-interface {v1}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->a(Landroid/content/Context;Landroid/view/ViewGroup;LX/ClV;)Lcom/facebook/richdocument/view/widget/AudioAnnotationView;

    move-result-object v0

    .line 1936273
    iget-object v1, p0, LX/Cos;->i:LX/CoV;

    invoke-virtual {v1, v0}, LX/CoV;->a(Landroid/view/View;)V

    .line 1936274
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 1936275
    invoke-interface {v1, v0}, LX/Cq9;->a(LX/CnQ;)V

    .line 1936276
    :cond_0
    return-void
.end method

.method public final a(LX/CoS;)V
    .locals 2

    .prologue
    .line 1936262
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936263
    invoke-interface {v0}, LX/Ctg;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    sget-object v1, LX/ClT;->AUDIO:LX/ClT;

    invoke-virtual {v0, v1}, LX/Cs7;->a(LX/ClT;)LX/CnQ;

    move-result-object v0

    .line 1936264
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;

    if-eqz v1, :cond_0

    .line 1936265
    check-cast v0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->a(LX/CoS;)V

    .line 1936266
    :cond_0
    return-void
.end method

.method public final a(LX/Cqw;)V
    .locals 1

    .prologue
    .line 1936258
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936259
    invoke-interface {v0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    .line 1936260
    iput-object p1, v0, LX/CqX;->d:LX/Cqv;

    .line 1936261
    return-void
.end method

.method public final a(LX/Ctr;)V
    .locals 1

    .prologue
    .line 1936247
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936248
    invoke-interface {v0, p1}, LX/Ctf;->a(LX/Ctr;)V

    .line 1936249
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1936217
    const-string v0, "MediaBlockView.reset"

    const v1, 0x39914b64

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1936218
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1936219
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v2, v0

    .line 1936220
    invoke-interface {v2}, LX/Ctg;->b()V

    .line 1936221
    if-eqz p1, :cond_8

    const-string v0, "strategyType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1936222
    const-string v0, "strategyType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CrN;->valueOf(Ljava/lang/String;)LX/CrN;

    move-result-object v0

    .line 1936223
    :goto_0
    move-object v3, v0

    .line 1936224
    const-string v0, "MediaBlockView.reset#getTransitionStrategy"

    const v1, -0x34bf1cb7    # -1.2641097E7f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1936225
    const/4 v0, 0x0

    .line 1936226
    if-eqz p1, :cond_0

    const-string v1, "isCoverMedia"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1936227
    const-string v1, "isCoverMedia"

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1936228
    :cond_0
    move v0, v0

    .line 1936229
    invoke-virtual {p0, v2, v3, v0}, LX/Cos;->a(LX/Ctg;LX/CrN;Z)LX/CqX;

    move-result-object v1

    .line 1936230
    const v0, -0xe550829

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v1

    .line 1936231
    check-cast v0, LX/Cqj;

    invoke-interface {v2, v0}, LX/Ctg;->setTransitionStrategy(LX/Cqj;)V

    .line 1936232
    sget-object v0, LX/CrN;->ASPECT_FIT_SLIDE:LX/CrN;

    if-eq v3, v0, :cond_1

    sget-object v0, LX/CrN;->FULLSCREEN_SLIDE:LX/CrN;

    if-ne v3, v0, :cond_5

    .line 1936233
    :cond_1
    const-class v0, LX/CuS;

    invoke-direct {p0, v0}, LX/Cos;->c(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1936234
    new-instance v0, LX/CuS;

    invoke-direct {v0, v2}, LX/CuS;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936235
    :cond_2
    :goto_1
    sget-object v0, LX/CrN;->FULLSCREEN:LX/CrN;

    if-ne v3, v0, :cond_6

    .line 1936236
    const-class v0, LX/Ctx;

    invoke-direct {p0, v0}, LX/Cos;->c(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1936237
    new-instance v0, LX/Ctx;

    invoke-direct {v0, v2}, LX/Ctx;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936238
    :cond_3
    :goto_2
    instance-of v0, v1, LX/Cqj;

    if-eqz v0, :cond_4

    .line 1936239
    check-cast v1, LX/Cqj;

    invoke-virtual {v1}, LX/Cqj;->k()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1936240
    const-class v0, LX/CuC;

    invoke-direct {p0, v0}, LX/Cos;->c(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1936241
    new-instance v0, LX/CuC;

    invoke-direct {v0, v2}, LX/CuC;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1936242
    :cond_4
    :goto_3
    const v0, -0x7ddc8b99

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1936243
    return-void

    .line 1936244
    :cond_5
    const-class v0, LX/CuS;

    invoke-virtual {p0, v0}, LX/Cos;->b(Ljava/lang/Class;)V

    goto :goto_1

    .line 1936245
    :cond_6
    const-class v0, LX/Ctx;

    invoke-virtual {p0, v0}, LX/Cos;->b(Ljava/lang/Class;)V

    goto :goto_2

    .line 1936246
    :cond_7
    const-class v0, LX/CuC;

    invoke-virtual {p0, v0}, LX/Cos;->b(Ljava/lang/Class;)V

    goto :goto_3

    :cond_8
    sget-object v0, LX/CrN;->ASPECT_FIT:LX/CrN;

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 3

    .prologue
    .line 1936285
    iget-object v0, p0, LX/Cos;->j:LX/Crz;

    invoke-static {p1, p2, v0}, LX/ClW;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;LX/Crz;)LX/ClW;

    move-result-object v1

    .line 1936286
    if-eqz v1, :cond_0

    .line 1936287
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936288
    invoke-interface {v0}, LX/Cq9;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    sget-object v2, LX/ClT;->UFI:LX/ClT;

    invoke-virtual {v0, v2}, LX/Cs7;->a(LX/ClT;)LX/CnQ;

    move-result-object v0

    check-cast v0, LX/CnR;

    .line 1936289
    if-nez v0, :cond_2

    .line 1936290
    iget-object v0, p0, LX/Cos;->i:LX/CoV;

    invoke-virtual {v0}, LX/CoV;->a()LX/CnR;

    move-result-object v0

    .line 1936291
    if-nez v0, :cond_1

    .line 1936292
    :cond_0
    :goto_0
    return-void

    .line 1936293
    :cond_1
    invoke-virtual {v0, v1}, LX/CnR;->setAnnotation(LX/ClW;)V

    .line 1936294
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 1936295
    invoke-interface {v1, v0}, LX/Cq9;->a(LX/CnQ;)V

    .line 1936296
    :goto_1
    if-eqz v0, :cond_0

    .line 1936297
    invoke-virtual {p0}, LX/Cos;->d()LX/CnN;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CnR;->setComposerLaunchParams(LX/CnN;)V

    goto :goto_0

    .line 1936298
    :cond_2
    invoke-virtual {v0, v1}, LX/CnR;->setAnnotation(LX/ClW;)V

    goto :goto_1
.end method

.method public a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;)V
    .locals 12

    .prologue
    .line 1936192
    iget-object v0, p0, LX/Cos;->i:LX/CoV;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1936193
    iget-object v2, p0, LX/Cos;->a:LX/Ctg;

    move-object v2, v2

    .line 1936194
    iget-object v3, v0, LX/CoV;->e:LX/Chi;

    .line 1936195
    iget-object v4, v3, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v9, v4

    .line 1936196
    if-eqz p1, :cond_1

    move-object v10, v2

    .line 1936197
    check-cast v10, Landroid/view/ViewGroup;

    .line 1936198
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v11, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1936199
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1936200
    new-instance v3, LX/ClY;

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/ClS;->MINI_LABEL:LX/ClS;

    iget-object v6, v0, LX/CoV;->d:LX/Crz;

    invoke-virtual {v6}, LX/Crz;->a()Z

    move-result v6

    if-eqz v6, :cond_3

    sget-object v6, LX/ClQ;->RIGHT:LX/ClQ;

    :goto_0
    sget-object v7, LX/ClR;->TOP:LX/ClR;

    move-object v8, p1

    invoke-direct/range {v3 .. v9}, LX/ClY;-><init>(Ljava/lang/String;LX/ClS;LX/ClQ;LX/ClR;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    .line 1936201
    invoke-static {v1, v10, v3}, Lcom/facebook/richdocument/view/widget/LocationAnnotationView;->a(Landroid/content/Context;Landroid/view/ViewGroup;LX/ClY;)Lcom/facebook/richdocument/view/widget/LocationAnnotationView;

    move-result-object v3

    .line 1936202
    invoke-virtual {v0, v3}, LX/CoV;->a(Landroid/view/View;)V

    .line 1936203
    invoke-interface {v2, v3}, LX/Cq9;->a(LX/CnQ;)V

    .line 1936204
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1936205
    new-instance v3, LX/ClU;

    sget-object v4, LX/ClT;->SUBTITLE:LX/ClT;

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/ClS;->REGULAR:LX/ClS;

    iget-object v7, v0, LX/CoV;->d:LX/Crz;

    invoke-virtual {v7}, LX/Crz;->a()Z

    move-result v7

    if-eqz v7, :cond_4

    sget-object v7, LX/ClQ;->RIGHT:LX/ClQ;

    :goto_1
    sget-object v8, LX/ClR;->TOP:LX/ClR;

    invoke-direct/range {v3 .. v9}, LX/ClU;-><init>(LX/ClT;Ljava/lang/String;LX/ClS;LX/ClQ;LX/ClR;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    .line 1936206
    iget-object v4, v0, LX/CoV;->b:LX/Crk;

    const v5, 0x7f0311fc

    invoke-virtual {v4, v5}, LX/Crk;->a(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    .line 1936207
    invoke-virtual {v4, v3}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setAnnotation(LX/ClU;)V

    .line 1936208
    invoke-virtual {v0, v4}, LX/CoV;->a(Landroid/view/View;)V

    .line 1936209
    invoke-interface {v2, v4}, LX/Cq9;->a(LX/CnQ;)V

    .line 1936210
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1936211
    const-class v0, LX/CuA;

    invoke-virtual {p0, v0}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v0

    check-cast v0, LX/CuA;

    .line 1936212
    if-eqz v0, :cond_2

    .line 1936213
    const/4 v1, 0x1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CuA;->a(ILjava/util/List;)V

    .line 1936214
    :cond_2
    return-void

    .line 1936215
    :cond_3
    sget-object v6, LX/ClQ;->LEFT:LX/ClQ;

    goto :goto_0

    .line 1936216
    :cond_4
    sget-object v7, LX/ClQ;->LEFT:LX/ClQ;

    goto :goto_1
.end method

.method public final a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)V
    .locals 2

    .prologue
    .line 1936188
    iget-object v0, p0, LX/Cos;->i:LX/CoV;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    .line 1936189
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 1936190
    invoke-virtual {v0, v1, p1, p2, p3}, LX/CoV;->a(LX/Cq9;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)V

    .line 1936191
    return-void
.end method

.method public a(II)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1936183
    const-class v0, LX/CuI;

    invoke-virtual {p0, v0}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v0

    check-cast v0, LX/CuI;

    .line 1936184
    if-nez v0, :cond_0

    move v0, v1

    .line 1936185
    :goto_0
    return v0

    .line 1936186
    :cond_0
    iget-object v2, v0, LX/CuI;->j:LX/CuG;

    move-object v0, v2

    .line 1936187
    sget-object v2, LX/CuG;->TOUCH:LX/CuG;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(LX/CoS;)V
    .locals 2

    .prologue
    .line 1936145
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936146
    invoke-interface {v0}, LX/Ctg;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    sget-object v1, LX/ClT;->AUDIO:LX/ClT;

    invoke-virtual {v0, v1}, LX/Cs7;->a(LX/ClT;)LX/CnQ;

    move-result-object v0

    .line 1936147
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;

    if-eqz v1, :cond_0

    .line 1936148
    check-cast v0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->b(LX/CoS;)V

    .line 1936149
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1936170
    invoke-super {p0, p1}, LX/Cod;->b(Landroid/os/Bundle;)V

    .line 1936171
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936172
    invoke-interface {v0}, LX/Ctg;->d()V

    .line 1936173
    iget-object v0, p0, LX/Cos;->f:LX/CoR;

    const/4 v1, 0x0

    iget-object v2, p0, LX/Cos;->g:LX/03V;

    .line 1936174
    instance-of v3, p0, LX/CnE;

    if-nez v3, :cond_0

    .line 1936175
    sget-object v3, LX/CoV;->a:Ljava/lang/String;

    const-string v4, "set up audio annotation auto play failed"

    invoke-static {v3, v4}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v3

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "The view is not audio annotation aware"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 1936176
    iput-object v4, v3, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1936177
    move-object v3, v3

    .line 1936178
    invoke-virtual {v3}, LX/0VK;->g()LX/0VG;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/03V;->a(LX/0VG;)V

    .line 1936179
    :goto_0
    return-void

    :cond_0
    move-object v3, p0

    .line 1936180
    check-cast v3, LX/CnE;

    .line 1936181
    new-instance v4, LX/CoT;

    invoke-direct {v4, v3}, LX/CoT;-><init>(LX/CnE;)V

    .line 1936182
    invoke-interface {p0}, LX/CnG;->c()Landroid/view/View;

    move-result-object v3

    new-instance v5, LX/CoQ;

    sget-object p1, LX/CoP;->PERCENTAGE:LX/CoP;

    invoke-direct {v5, p1, v1}, LX/CoQ;-><init>(LX/CoP;I)V

    invoke-virtual {v0, v3, v5, v4}, LX/CoR;->a(Landroid/view/View;LX/CoQ;LX/CoO;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/Ctr;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1936167
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936168
    invoke-interface {v0, p1}, LX/Ctf;->c(Ljava/lang/Class;)V

    .line 1936169
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1936154
    invoke-super {p0, p1}, LX/Cod;->c(Landroid/os/Bundle;)V

    .line 1936155
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936156
    invoke-interface {v0}, LX/Ctg;->e()V

    .line 1936157
    iget-object v0, p0, LX/Cos;->f:LX/CoR;

    iget-object v1, p0, LX/Cos;->g:LX/03V;

    .line 1936158
    instance-of v2, p0, LX/CnE;

    if-nez v2, :cond_0

    .line 1936159
    sget-object v2, LX/CoV;->a:Ljava/lang/String;

    const-string v3, "cancel audio annotation auto play failed"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string p1, "The view is not audio annotation aware"

    invoke-direct {v3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 1936160
    iput-object v3, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1936161
    move-object v2, v2

    .line 1936162
    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/03V;->a(LX/0VG;)V

    .line 1936163
    :goto_0
    return-void

    :cond_0
    move-object v2, p0

    .line 1936164
    check-cast v2, LX/CnE;

    .line 1936165
    invoke-interface {p0}, LX/CnG;->c()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/CoR;->a(Landroid/view/View;)V

    .line 1936166
    sget-object v3, LX/CoS;->ENFORCED:LX/CoS;

    invoke-interface {v2, v3}, LX/CnE;->b(LX/CoS;)V

    goto :goto_0
.end method

.method public d()LX/CnN;
    .locals 1

    .prologue
    .line 1936153
    const/4 v0, 0x0

    return-object v0
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 1936152
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/Ct1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 1936150
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1936151
    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    return-object v0
.end method
