.class public LX/DdM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/0aG;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/MessageDraft;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2Og;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019455
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/DdM;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/2Og;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019457
    iput-object p1, p0, LX/DdM;->a:LX/0aG;

    .line 2019458
    iput-object p2, p0, LX/DdM;->c:LX/2Og;

    .line 2019459
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/DdM;->b:Ljava/util/Map;

    .line 2019460
    return-void
.end method
