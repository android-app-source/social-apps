.class public final LX/E0Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$FBCheckinNearbyCityQuery$ClosestCity;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeCreationActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/home/HomeCreationActivity;)V
    .locals 0

    .prologue
    .line 2068617
    iput-object p1, p0, LX/E0Z;->a:Lcom/facebook/places/create/home/HomeCreationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;)V
    .locals 4
    .param p1    # Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2068618
    if-eqz p1, :cond_0

    .line 2068619
    iget-object v0, p0, LX/E0Z;->a:Lcom/facebook/places/create/home/HomeCreationActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    .line 2068620
    iget-object v0, p0, LX/E0Z;->a:Lcom/facebook/places/create/home/HomeCreationActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    .line 2068621
    iget-object v0, p0, LX/E0Z;->a:Lcom/facebook/places/create/home/HomeCreationActivity;

    invoke-virtual {v0}, Lcom/facebook/places/create/home/HomeCreationActivity;->o()V

    .line 2068622
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2068623
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2068624
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    invoke-direct {p0, p1}, LX/E0Z;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;)V

    return-void
.end method
