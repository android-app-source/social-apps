.class public final LX/Dk9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dj9;

.field public final synthetic b:LX/DkN;


# direct methods
.method public constructor <init>(LX/DkN;LX/Dj9;)V
    .locals 0

    .prologue
    .line 2033978
    iput-object p1, p0, LX/Dk9;->b:LX/DkN;

    iput-object p2, p0, LX/Dk9;->a:LX/Dj9;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2033979
    iget-object v0, p0, LX/Dk9;->a:LX/Dj9;

    invoke-interface {v0, p1}, LX/Dj9;->a(Ljava/lang/Throwable;)V

    .line 2033980
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033981
    check-cast p1, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    .line 2033982
    if-nez p1, :cond_0

    .line 2033983
    iget-object v0, p0, LX/Dk9;->a:LX/Dj9;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "result is NULL"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/Dj9;->a(Ljava/lang/Throwable;)V

    .line 2033984
    :goto_0
    return-void

    .line 2033985
    :cond_0
    iget-object v0, p0, LX/Dk9;->a:LX/Dj9;

    invoke-interface {v0, p1}, LX/Dj9;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V

    goto :goto_0
.end method
