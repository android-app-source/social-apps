.class public LX/Cko;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;

.field public b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1931461
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1931462
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cko;->c:Z

    .line 1931463
    invoke-virtual {p0}, LX/Cko;->b()V

    .line 1931464
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 1931465
    invoke-virtual {p0}, LX/Cko;->removeAllViews()V

    .line 1931466
    const v0, 0x7f0309f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1931467
    const v0, 0x7f0d1934

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;

    iput-object v0, p0, LX/Cko;->a:Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;

    .line 1931468
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cko;->b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;

    .line 1931469
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cko;->c:Z

    .line 1931470
    return-void
.end method

.method public getLinkCoverSpecViewFrame()Landroid/view/View;
    .locals 1

    .prologue
    .line 1931471
    const v0, 0x7f0d1931

    invoke-virtual {p0, v0}, LX/Cko;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
