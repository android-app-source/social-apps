.class public LX/CtI;
.super LX/Csu;
.source ""

# interfaces
.implements LX/02k;
.implements LX/CrZ;


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8bZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/Crb;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/richdocument/view/widget/media/RotatableViewAware;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/Ci7;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;LX/Crb;)V
    .locals 2

    .prologue
    .line 1944534
    const/high16 v0, 0x3e800000    # 0.25f

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, p1, v0, v1}, LX/Csu;-><init>(Landroid/support/v7/widget/RecyclerView;FF)V

    .line 1944535
    const-class v0, LX/CtI;

    invoke-static {v0, p0}, LX/CtI;->a(Ljava/lang/Class;LX/02k;)V

    .line 1944536
    iput-object p2, p0, LX/CtI;->c:LX/Crb;

    .line 1944537
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/CtI;->d:Ljava/util/List;

    .line 1944538
    new-instance v0, LX/CtH;

    invoke-direct {v0, p0}, LX/CtH;-><init>(LX/CtI;)V

    iput-object v0, p0, LX/CtI;->e:LX/Ci7;

    .line 1944539
    iget-object v0, p0, LX/CtI;->b:LX/8bZ;

    invoke-virtual {v0}, LX/8bZ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1944540
    iget-object v0, p0, LX/CtI;->a:LX/Chv;

    iget-object v1, p0, LX/CtI;->e:LX/Ci7;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1944541
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CtI;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v1

    check-cast v1, LX/Chv;

    invoke-static {p0}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object p0

    check-cast p0, LX/8bZ;

    iput-object v1, p1, LX/CtI;->a:LX/Chv;

    iput-object p0, p1, LX/CtI;->b:LX/8bZ;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/CrY;)V
    .locals 4

    .prologue
    .line 1944513
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1944514
    iget-object v0, p0, LX/CtI;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CuC;

    .line 1944515
    invoke-virtual {v0}, LX/CuC;->a()Landroid/view/View;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1944516
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1944517
    :cond_0
    :try_start_1
    invoke-virtual {p0, v1}, LX/Csu;->a(Ljava/util/Collection;)Landroid/view/View;

    move-result-object v1

    .line 1944518
    if-eqz v1, :cond_2

    .line 1944519
    iget-object v0, p0, LX/CtI;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CuC;

    .line 1944520
    invoke-virtual {v0}, LX/CuC;->a()Landroid/view/View;

    move-result-object v3

    if-ne v1, v3, :cond_1

    .line 1944521
    invoke-interface {v0, p1}, LX/CrZ;->a(LX/CrY;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1944522
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(LX/CuC;)V
    .locals 1

    .prologue
    .line 1944542
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CtI;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1944543
    iget-object v0, p0, LX/CtI;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1944544
    iget-object v0, p0, LX/CtI;->c:LX/Crb;

    invoke-virtual {v0, p0}, LX/Crb;->a(LX/CrZ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1944545
    :cond_0
    monitor-exit p0

    return-void

    .line 1944546
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 1944530
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CtI;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1944531
    iget-object v0, p0, LX/CtI;->c:LX/Crb;

    invoke-virtual {v0, p0}, LX/Crb;->b(LX/CrZ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1944532
    monitor-exit p0

    return-void

    .line 1944533
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/CuC;)V
    .locals 1

    .prologue
    .line 1944525
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CtI;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1944526
    iget-object v0, p0, LX/CtI;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1944527
    iget-object v0, p0, LX/CtI;->c:LX/Crb;

    invoke-virtual {v0, p0}, LX/Crb;->b(LX/CrZ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1944528
    :cond_0
    monitor-exit p0

    return-void

    .line 1944529
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1944523
    iget-object v0, p0, LX/Csu;->a:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 1944524
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
