.class public final LX/DsF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final synthetic a:LX/DsH;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/DsH;)V
    .locals 1

    .prologue
    .line 2050202
    iput-object p1, p0, LX/DsF;->a:LX/DsH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/DsF;->b:Ljava/util/List;

    .line 2050204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/DsF;->c:Ljava/util/List;

    .line 2050205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/DsF;->d:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2050201
    iget-object v0, p0, LX/DsF;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2050197
    iget-object v0, p0, LX/DsF;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2050198
    iget-object v0, p0, LX/DsF;->d:Ljava/util/List;

    iget-object v1, p0, LX/DsF;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2050199
    iget-object v0, p0, LX/DsF;->d:Ljava/util/List;

    iget-object v1, p0, LX/DsF;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2050200
    return-void
.end method

.method public final c(Ljava/util/Collection;)V
    .locals 1
    .param p1    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2050193
    if-eqz p1, :cond_0

    .line 2050194
    iget-object v0, p0, LX/DsF;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2050195
    :cond_0
    invoke-virtual {p0}, LX/DsF;->b()V

    .line 2050196
    return-void
.end method
