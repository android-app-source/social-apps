.class public final LX/EV4;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ETQ;

.field public final synthetic b:Lcom/facebook/video/videohome/data/VideoHomeItem;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public final synthetic d:Ljava/lang/ref/WeakReference;

.field public final synthetic e:LX/E2b;

.field public final synthetic f:LX/E2c;

.field public final synthetic g:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;Ljava/lang/ref/WeakReference;LX/E2b;LX/E2c;)V
    .locals 0

    .prologue
    .line 2127277
    iput-object p1, p0, LX/EV4;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    iput-object p2, p0, LX/EV4;->a:LX/ETQ;

    iput-object p3, p0, LX/EV4;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    iput-object p4, p0, LX/EV4;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iput-object p5, p0, LX/EV4;->d:Ljava/lang/ref/WeakReference;

    iput-object p6, p0, LX/EV4;->e:LX/E2b;

    iput-object p7, p0, LX/EV4;->f:LX/E2c;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2127278
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EV4;->a:LX/ETQ;

    invoke-virtual {v0}, LX/ETQ;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2127279
    iget-object v0, p0, LX/EV4;->a:LX/ETQ;

    invoke-virtual {v0, v1}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v2

    .line 2127280
    new-instance v0, LX/EUp;

    iget-object v3, p0, LX/EV4;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-direct {v0, v2, v3, v1}, LX/EUp;-><init>(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/video/videohome/data/VideoHomeItem;I)V

    .line 2127281
    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;->a(LX/EUp;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2127282
    iget-object v2, p0, LX/EV4;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    iget-object v2, v2, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorSpacePartDefinition;

    invoke-virtual {p1, v2, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2127283
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2127284
    :cond_1
    iget-object v0, p0, LX/EV4;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->i:LX/1vo;

    .line 2127285
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2127286
    invoke-interface {v3}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 2127287
    if-eqz v0, :cond_2

    invoke-interface {v0, v2}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2127288
    :cond_2
    instance-of v3, v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v3, :cond_0

    .line 2127289
    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {p1, v0, v2}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_1

    .line 2127290
    :cond_3
    iget-object v0, p0, LX/EV4;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/EV4;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-static {v0}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->c(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2127291
    iget-object v0, p0, LX/EV4;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->k:Lcom/facebook/video/videohome/partdefinitions/VideoHomeCreatorLoadingSpinnerPartDefinition;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2127292
    :cond_4
    return-void
.end method

.method public final c(I)V
    .locals 4

    .prologue
    .line 2127293
    iget-object v0, p0, LX/EV4;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETX;

    .line 2127294
    if-nez v0, :cond_0

    .line 2127295
    :goto_0
    return-void

    .line 2127296
    :cond_0
    iget-object v1, p0, LX/EV4;->a:LX/ETQ;

    invoke-virtual {v1}, LX/ETQ;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    move-object v1, v0

    .line 2127297
    check-cast v1, LX/1Pr;

    iget-object v2, p0, LX/EV4;->e:LX/E2b;

    iget-object v3, p0, LX/EV4;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-interface {v1, v2, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/E2c;

    .line 2127298
    iput p1, v1, LX/E2c;->d:I

    .line 2127299
    :cond_1
    iget-object v1, p0, LX/EV4;->g:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;

    iget-object v2, p0, LX/EV4;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    iget-object v3, p0, LX/EV4;->f:LX/E2c;

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;->a$redex0(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollPartDefinition;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETX;LX/E2c;)V

    goto :goto_0
.end method
