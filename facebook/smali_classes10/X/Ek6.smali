.class public LX/Ek6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/0if;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:J


# direct methods
.method public constructor <init>(LX/0SG;LX/0if;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2163028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163029
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ek6;->d:Z

    .line 2163030
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/Ek6;->e:J

    .line 2163031
    iput-object p1, p0, LX/Ek6;->a:LX/0SG;

    .line 2163032
    iput-object p2, p0, LX/Ek6;->b:LX/0if;

    .line 2163033
    return-void
.end method

.method public static a(LX/0QB;)LX/Ek6;
    .locals 5

    .prologue
    .line 2163017
    const-class v1, LX/Ek6;

    monitor-enter v1

    .line 2163018
    :try_start_0
    sget-object v0, LX/Ek6;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2163019
    sput-object v2, LX/Ek6;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2163020
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2163021
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2163022
    new-instance p0, LX/Ek6;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {p0, v3, v4}, LX/Ek6;-><init>(LX/0SG;LX/0if;)V

    .line 2163023
    move-object v0, p0

    .line 2163024
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2163025
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ek6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2163026
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2163027
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static c(LX/Ek6;)V
    .locals 9

    .prologue
    .line 2163034
    const/4 v1, 0x0

    .line 2163035
    iget-boolean v2, p0, LX/Ek6;->d:Z

    if-nez v2, :cond_1

    .line 2163036
    :goto_0
    move v0, v1

    .line 2163037
    if-nez v0, :cond_0

    .line 2163038
    invoke-virtual {p0}, LX/Ek6;->a()V

    .line 2163039
    :cond_0
    return-void

    .line 2163040
    :cond_1
    iget-object v2, p0, LX/Ek6;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v3

    iget-wide v5, p0, LX/Ek6;->e:J

    sub-long/2addr v3, v5

    sget v2, LX/Ek8;->b:I

    int-to-long v5, v2

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    cmp-long v2, v3, v5

    if-lez v2, :cond_2

    .line 2163041
    iput-boolean v1, p0, LX/Ek6;->d:Z

    goto :goto_0

    .line 2163042
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2163008
    sget-object v0, LX/0ig;->K:LX/0ih;

    const/4 v1, 0x0

    .line 2163009
    iput-boolean v1, v0, LX/0ih;->d:Z

    .line 2163010
    move-object v0, v0

    .line 2163011
    sget v1, LX/Ek8;->b:I

    .line 2163012
    iput v1, v0, LX/0ih;->c:I

    .line 2163013
    iget-object v0, p0, LX/Ek6;->b:LX/0if;

    sget-object v1, LX/0ig;->K:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2163014
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ek6;->d:Z

    .line 2163015
    iget-object v0, p0, LX/Ek6;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Ek6;->e:J

    .line 2163016
    return-void
.end method

.method public final a(LX/Ek7;Ljava/lang/String;LX/1rQ;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1rQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2163001
    invoke-static {p0}, LX/Ek6;->c(LX/Ek6;)V

    .line 2163002
    if-nez p3, :cond_0

    .line 2163003
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object p3

    .line 2163004
    :cond_0
    const-string v0, "qpid"

    iget-object v1, p0, LX/Ek6;->c:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 2163005
    iget-object v0, p0, LX/Ek6;->b:LX/0if;

    sget-object v1, LX/0ig;->K:LX/0ih;

    invoke-virtual {p1}, LX/Ek7;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2, p3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2163006
    iget-object v0, p0, LX/Ek6;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Ek6;->e:J

    .line 2163007
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2162997
    invoke-static {p0}, LX/Ek6;->c(LX/Ek6;)V

    .line 2162998
    iget-object v0, p0, LX/Ek6;->b:LX/0if;

    sget-object v1, LX/0ig;->K:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2162999
    iget-object v0, p0, LX/Ek6;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Ek6;->e:J

    .line 2163000
    return-void
.end method
