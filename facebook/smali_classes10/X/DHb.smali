.class public LX/DHb;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DHc;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DHb",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DHc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1982159
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1982160
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DHb;->b:LX/0Zi;

    .line 1982161
    iput-object p1, p0, LX/DHb;->a:LX/0Ot;

    .line 1982162
    return-void
.end method

.method public static a(LX/0QB;)LX/DHb;
    .locals 4

    .prologue
    .line 1982163
    const-class v1, LX/DHb;

    monitor-enter v1

    .line 1982164
    :try_start_0
    sget-object v0, LX/DHb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1982165
    sput-object v2, LX/DHb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1982166
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1982167
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1982168
    new-instance v3, LX/DHb;

    const/16 p0, 0x21d5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DHb;-><init>(LX/0Ot;)V

    .line 1982169
    move-object v0, v3

    .line 1982170
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1982171
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DHb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1982172
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1982173
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1982174
    check-cast p2, LX/DHa;

    .line 1982175
    iget-object v0, p0, LX/DHb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DHc;

    iget-object v1, p2, LX/DHa;->a:LX/DGm;

    iget-object v2, p2, LX/DHa;->b:LX/1Pb;

    const/4 p2, 0x1

    .line 1982176
    iget-object v3, v0, LX/DHc;->a:LX/1VD;

    invoke-virtual {v3, p1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v3

    .line 1982177
    iget-object p0, v1, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object p0, p0

    .line 1982178
    invoke-virtual {v3, p0}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1X4;->c(Z)LX/1X4;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1X4;->f(Z)LX/1X4;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1X4;->g(Z)LX/1X4;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1X4;->d(Z)LX/1X4;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1X4;->e(Z)LX/1X4;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1982179
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1982180
    invoke-static {}, LX/1dS;->b()V

    .line 1982181
    const/4 v0, 0x0

    return-object v0
.end method
