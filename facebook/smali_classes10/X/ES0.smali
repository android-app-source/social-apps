.class public final LX/ES0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field public final synthetic a:Lcom/facebook/vault/service/VaultSyncJobProcessor;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/service/VaultSyncJobProcessor;)V
    .locals 0

    .prologue
    .line 2121911
    iput-object p1, p0, LX/ES0;->a:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2121912
    sget-object v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->d:Ljava/lang/String;

    const-string v1, "An exception occurred in %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, p2, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2121913
    iget-object v0, p0, LX/ES0;->a:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    iget-object v0, v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->i:LX/ES8;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/ES8;->a(J)I

    .line 2121914
    iget-object v0, p0, LX/ES0;->a:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    invoke-virtual {v0}, Lcom/facebook/vault/service/VaultSyncJobProcessor;->stopSelf()V

    .line 2121915
    iget-object v0, p0, LX/ES0;->a:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    iget-object v0, v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2121916
    return-void
.end method
