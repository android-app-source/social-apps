.class public LX/EFT;
.super LX/EFS;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/03V;

.field private d:Lcom/facebook/webrtc/MediaCaptureSink;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2095927
    const-class v0, LX/EFT;

    sput-object v0, LX/EFT;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;Lcom/facebook/webrtc/MediaCaptureSink;II)V
    .locals 4

    .prologue
    .line 2095916
    if-eqz p2, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p3, p4, v0}, LX/EFS;-><init>(IIZ)V

    .line 2095917
    iput-object p1, p0, LX/EFT;->c:LX/03V;

    .line 2095918
    iget-boolean v0, p0, LX/EFS;->a:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 2095919
    iget-object v0, p0, LX/EFT;->c:LX/03V;

    const-string v1, "EncodingVideoOutput"

    const-string v2, "appLevelCamera = true, but mediaCaptureSink == null "

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2095920
    :cond_0
    iput-object p2, p0, LX/EFT;->d:Lcom/facebook/webrtc/MediaCaptureSink;

    .line 2095921
    if-gtz p3, :cond_1

    .line 2095922
    iget-object v0, p0, LX/EFT;->c:LX/03V;

    const-string v1, "EncodingVideoOutput"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid frame width: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2095923
    :cond_1
    if-gtz p4, :cond_2

    .line 2095924
    iget-object v0, p0, LX/EFT;->c:LX/03V;

    const-string v1, "EncodingVideoOutput"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid frame height: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2095925
    :cond_2
    return-void

    .line 2095926
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6Kl;)V
    .locals 1

    .prologue
    .line 2095905
    invoke-super {p0, p1}, LX/EFS;->a(LX/6Kl;)V

    .line 2095906
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2095907
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 2095913
    sget-object v0, LX/EFT;->b:Ljava/lang/Class;

    const-string v1, "onSurfaceDrawn Error: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2095914
    iget-object v0, p0, LX/EFT;->c:LX/03V;

    const-string v1, "MSQRD:EncodingVideoOutput"

    const-string v2, "onSurfaceDrawn threw an exception"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2095915
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 2095908
    iget-boolean v0, p0, LX/EFS;->a:Z

    if-eqz v0, :cond_1

    .line 2095909
    iget-object v0, p0, LX/EFT;->d:Lcom/facebook/webrtc/MediaCaptureSink;

    if-nez v0, :cond_0

    .line 2095910
    :goto_0
    return-void

    .line 2095911
    :cond_0
    iget-object v0, p0, LX/EFT;->d:Lcom/facebook/webrtc/MediaCaptureSink;

    new-instance v1, Lorg/webrtc/videoengine/ARGBBuffer;

    invoke-virtual {p0}, LX/EFS;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/EFS;->getHeight()I

    move-result v3

    invoke-direct {v1, p1, v2, v3}, Lorg/webrtc/videoengine/ARGBBuffer;-><init>(Ljava/nio/ByteBuffer;II)V

    invoke-virtual {v0, v1}, Lcom/facebook/webrtc/MediaCaptureSink;->onCapturedFrame(Lorg/webrtc/videoengine/VideoFrameBuffer;)V

    goto :goto_0

    .line 2095912
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p0}, LX/EFS;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/EFS;->getHeight()I

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a([BII)V

    goto :goto_0
.end method
