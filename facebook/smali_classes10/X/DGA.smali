.class public LX/DGA;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        "T::",
        "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
        ">",
        "LX/3mX",
        "<",
        "LX/DGK;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/DGQ;

.field private final d:LX/1Pd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pd;LX/25M;LX/DGQ;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/DGK;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;TE;",
            "LX/25M;",
            "LX/DGQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979318
    move-object v0, p4

    check-cast v0, LX/1Pq;

    invoke-direct {p0, p1, p2, v0, p5}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1979319
    iput-object p6, p0, LX/DGA;->c:LX/DGQ;

    .line 1979320
    iput-object p4, p0, LX/DGA;->d:LX/1Pd;

    .line 1979321
    iput-object p3, p0, LX/DGA;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979322
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1979359
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 1979324
    check-cast p2, LX/DGK;

    .line 1979325
    iget-object v0, p0, LX/DGA;->c:LX/DGQ;

    const/4 v1, 0x0

    .line 1979326
    new-instance v2, LX/DGP;

    invoke-direct {v2, v0}, LX/DGP;-><init>(LX/DGQ;)V

    .line 1979327
    iget-object v3, v0, LX/DGQ;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DGO;

    .line 1979328
    if-nez v3, :cond_0

    .line 1979329
    new-instance v3, LX/DGO;

    invoke-direct {v3, v0}, LX/DGO;-><init>(LX/DGQ;)V

    .line 1979330
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/DGO;->a$redex0(LX/DGO;LX/1De;IILX/DGP;)V

    .line 1979331
    move-object v2, v3

    .line 1979332
    move-object v1, v2

    .line 1979333
    move-object v0, v1

    .line 1979334
    iget-object v1, v0, LX/DGO;->a:LX/DGP;

    iput-object p2, v1, LX/DGP;->a:LX/DGK;

    .line 1979335
    iget-object v1, v0, LX/DGO;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1979336
    move-object v0, v0

    .line 1979337
    iget v1, p2, LX/DGK;->c:I

    move v1, v1

    .line 1979338
    iget-object v2, p0, LX/DGA;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979339
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 1979340
    check-cast v2, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-static {v2}, LX/25C;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/0Px;

    move-result-object v2

    .line 1979341
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1979342
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v3

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v3, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1979343
    invoke-static {v2}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 1979344
    if-eqz v3, :cond_1

    .line 1979345
    sget-object v2, LX/DG8;->NETEGO_FORSALE_PHOTO_STORY:LX/DG8;

    .line 1979346
    :goto_0
    move-object v1, v2

    .line 1979347
    iget-object v2, v0, LX/DGO;->a:LX/DGP;

    iput-object v1, v2, LX/DGP;->b:LX/DG8;

    .line 1979348
    iget-object v2, v0, LX/DGO;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1979349
    move-object v0, v0

    .line 1979350
    iget-object v1, p0, LX/DGA;->d:LX/1Pd;

    .line 1979351
    iget-object v2, v0, LX/DGO;->a:LX/DGP;

    iput-object v1, v2, LX/DGP;->c:LX/1Pd;

    .line 1979352
    iget-object v2, v0, LX/DGO;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1979353
    move-object v0, v0

    .line 1979354
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 1979355
    :cond_1
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 1979356
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x4ed245b

    if-ne v2, v3, :cond_2

    .line 1979357
    sget-object v2, LX/DG8;->NETEGO_VIDEO_STORY:LX/DG8;

    goto :goto_0

    .line 1979358
    :cond_2
    sget-object v2, LX/DG8;->NETEGO_PHOTO_STORY:LX/DG8;

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1979323
    const/4 v0, 0x0

    return v0
.end method
