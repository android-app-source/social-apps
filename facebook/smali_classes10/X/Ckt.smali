.class public LX/Ckt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/Ckt;


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/ClO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Cig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/Cii;

.field public final f:LX/Cij;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:I

.field public k:Z

.field public l:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1931532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931533
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Ckt;->d:Ljava/util/Set;

    .line 1931534
    new-instance v0, LX/Ckr;

    invoke-direct {v0, p0}, LX/Ckr;-><init>(LX/Ckt;)V

    iput-object v0, p0, LX/Ckt;->e:LX/Cii;

    .line 1931535
    new-instance v0, LX/Cks;

    invoke-direct {v0, p0}, LX/Cks;-><init>(LX/Ckt;)V

    iput-object v0, p0, LX/Ckt;->f:LX/Cij;

    .line 1931536
    return-void
.end method

.method public static a(LX/0QB;)LX/Ckt;
    .locals 6

    .prologue
    .line 1931537
    sget-object v0, LX/Ckt;->m:LX/Ckt;

    if-nez v0, :cond_1

    .line 1931538
    const-class v1, LX/Ckt;

    monitor-enter v1

    .line 1931539
    :try_start_0
    sget-object v0, LX/Ckt;->m:LX/Ckt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1931540
    if-eqz v2, :cond_0

    .line 1931541
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1931542
    new-instance p0, LX/Ckt;

    invoke-direct {p0}, LX/Ckt;-><init>()V

    .line 1931543
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/ClO;->a(LX/0QB;)LX/ClO;

    move-result-object v4

    check-cast v4, LX/ClO;

    invoke-static {v0}, LX/Cig;->a(LX/0QB;)LX/Cig;

    move-result-object v5

    check-cast v5, LX/Cig;

    .line 1931544
    iput-object v3, p0, LX/Ckt;->a:LX/0Zb;

    iput-object v4, p0, LX/Ckt;->b:LX/ClO;

    iput-object v5, p0, LX/Ckt;->c:LX/Cig;

    .line 1931545
    move-object v0, p0

    .line 1931546
    sput-object v0, LX/Ckt;->m:LX/Ckt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1931547
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1931548
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1931549
    :cond_1
    sget-object v0, LX/Ckt;->m:LX/Ckt;

    return-object v0

    .line 1931550
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1931551
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/ClD;Landroid/content/Context;)V
    .locals 10

    .prologue
    const-wide/16 v4, 0x3e8

    .line 1931552
    iget-object v0, p0, LX/Ckt;->a:LX/0Zb;

    const-string v1, "native_article_session_end"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1931553
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1931554
    const-string v1, "native_article_story"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1931555
    const-string v1, "instant_articles_session_id"

    .line 1931556
    iget-object v2, p1, LX/ClD;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1931557
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1931558
    const-string v1, "active_session_duration"

    .line 1931559
    iget-wide v6, p1, LX/ClD;->i:J

    move-wide v2, v6

    .line 1931560
    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1931561
    const-string v1, "active_session_duration_on_ia"

    .line 1931562
    iget-wide v6, p1, LX/ClD;->j:J

    move-wide v2, v6

    .line 1931563
    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1931564
    const-string v1, "number_of_unique_instant_articles_opened"

    iget-object v2, p0, LX/Ckt;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1931565
    const-string v1, "page_load_time"

    .line 1931566
    iget-wide v6, p1, LX/ClD;->n:J

    move-wide v2, v6

    .line 1931567
    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1931568
    const-string v1, "article_ID"

    iget-object v2, p0, LX/Ckt;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1931569
    const-string v1, "canonical_url"

    iget-object v2, p0, LX/Ckt;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1931570
    const-string v1, "number_of_instant_articles_from_original_publisher"

    iget v2, p0, LX/Ckt;->j:I

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1931571
    const-string v1, "browser_opened"

    iget-boolean v2, p0, LX/Ckt;->k:Z

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1931572
    const-string v1, "app_backgrounded"

    iget-boolean v2, p0, LX/Ckt;->l:Z

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1931573
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1931574
    :cond_0
    iget-object v0, p0, LX/Ckt;->b:LX/ClO;

    .line 1931575
    iget-boolean v1, v0, LX/ClO;->b:Z

    move v0, v1

    .line 1931576
    if-eqz v0, :cond_1

    .line 1931577
    new-instance v0, LX/ClL;

    const-string v1, "native_article_session_end"

    invoke-direct {v0, v1}, LX/ClL;-><init>(Ljava/lang/String;)V

    const-string v1, "session_id"

    .line 1931578
    iget-object v2, p1, LX/ClD;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1931579
    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v1, "active_session_duration"

    .line 1931580
    iget-wide v6, p1, LX/ClD;->i:J

    move-wide v2, v6

    .line 1931581
    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v1, "active_session_duration_on_ia"

    .line 1931582
    iget-wide v6, p1, LX/ClD;->j:J

    move-wide v2, v6

    .line 1931583
    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v1, "unique_articles"

    iget-object v2, p0, LX/Ckt;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v1, "page_load_time"

    .line 1931584
    iget-wide v6, p1, LX/ClD;->n:J

    move-wide v2, v6

    .line 1931585
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v1, "article_ID"

    iget-object v2, p0, LX/Ckt;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v1, "canonical_url"

    iget-object v2, p0, LX/Ckt;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v1, "articles_from_original_publisher"

    iget v2, p0, LX/Ckt;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v1, "browser_opened"

    iget-boolean v2, p0, LX/Ckt;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    const-string v1, "app_backgrounded"

    iget-boolean v2, p0, LX/Ckt;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v0

    invoke-virtual {v0}, LX/ClL;->a()LX/ClM;

    move-result-object v0

    .line 1931586
    iget-object v1, p0, LX/Ckt;->b:LX/ClO;

    invoke-virtual {v1, v0}, LX/ClO;->a(LX/ClM;)V

    .line 1931587
    if-eqz p2, :cond_1

    .line 1931588
    iget-object v0, p0, LX/Ckt;->b:LX/ClO;

    const/4 v5, 0x1

    .line 1931589
    iget-boolean v1, v0, LX/ClO;->b:Z

    if-nez v1, :cond_2

    .line 1931590
    :cond_1
    :goto_0
    const/4 v1, 0x0

    .line 1931591
    iget-object v0, p0, LX/Ckt;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1931592
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ckt;->g:Ljava/lang/String;

    .line 1931593
    iput v1, p0, LX/Ckt;->j:I

    .line 1931594
    iput-boolean v1, p0, LX/Ckt;->k:Z

    .line 1931595
    iput-boolean v1, p0, LX/Ckt;->l:Z

    .line 1931596
    iget-object v0, p0, LX/Ckt;->c:LX/Cig;

    iget-object v1, p0, LX/Ckt;->e:LX/Cii;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1931597
    iget-object v0, p0, LX/Ckt;->c:LX/Cig;

    iget-object v1, p0, LX/Ckt;->f:LX/Cij;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1931598
    return-void

    .line 1931599
    :cond_2
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1931600
    const/4 v1, 0x0

    .line 1931601
    iget-object v2, v0, LX/ClO;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ClM;

    .line 1931602
    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1931603
    const/4 p1, 0x0

    .line 1931604
    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v6, 0x40

    invoke-direct {v8, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1931605
    iget-object v6, v1, LX/ClM;->a:Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1931606
    iget-object v6, v1, LX/ClM;->b:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 1931607
    const-string v7, "  "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1931608
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_3

    const-string v7, "null"

    :goto_3
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1931609
    const-string v7, ": "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1931610
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_4

    const-string v6, "null"

    :goto_4
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1931611
    const-string v6, ",\n"

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1931612
    :cond_3
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 1931613
    :cond_4
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 1931614
    :cond_5
    const-string v6, ",\n"

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    .line 1931615
    const/4 v7, -0x1

    if-eq v6, v7, :cond_6

    .line 1931616
    add-int/lit8 v7, v6, 0x1

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1931617
    :cond_6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1931618
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1931619
    new-instance v6, Landroid/text/style/StyleSpan;

    const/4 v8, 0x1

    invoke-direct {v6, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    iget-object v8, v1, LX/ClM;->a:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v6, p1, v8, p1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1931620
    move-object v1, v7

    .line 1931621
    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1931622
    const-string v1, "\n"

    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_1

    .line 1931623
    :cond_7
    iget-object v1, v0, LX/ClO;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1931624
    move-object v1, v3

    .line 1931625
    new-instance v2, Landroid/widget/ScrollView;

    invoke-direct {v2, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 1931626
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1931627
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1931628
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 1931629
    new-instance v4, LX/ClN;

    invoke-direct {v4, v0, p2, v1}, LX/ClN;-><init>(LX/ClO;Landroid/content/Context;Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1931630
    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 1931631
    new-instance v1, LX/31Y;

    invoke-direct {v1, p2, v5}, LX/31Y;-><init>(Landroid/content/Context;I)V

    .line 1931632
    invoke-virtual {v1, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    .line 1931633
    const-string v2, "Done"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1931634
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    goto/16 :goto_0
.end method
