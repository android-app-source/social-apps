.class public final LX/CrA;
.super LX/Cr9;
.source ""


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 0

    .prologue
    .line 1940677
    invoke-direct {p0, p1, p2}, LX/Cr9;-><init>(LX/Ctg;LX/CrK;)V

    .line 1940678
    return-void
.end method


# virtual methods
.method public final l()V
    .locals 4

    .prologue
    .line 1940679
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1940680
    const/4 v1, 0x0

    .line 1940681
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, LX/Ctg;

    if-eqz v2, :cond_0

    .line 1940682
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/Ctg;

    invoke-interface {v1}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v1

    .line 1940683
    :cond_0
    instance-of v2, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v2, :cond_1

    .line 1940684
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1940685
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1940686
    if-eqz v1, :cond_2

    .line 1940687
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-interface {v1}, LX/Ct1;->getMediaAspectRatio()F

    move-result v1

    div-float v1, v3, v1

    int-to-float v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    .line 1940688
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p0, v2, v1}, LX/Cqi;->a(II)V

    .line 1940689
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0, v1}, LX/Cqi;->b(II)V

    .line 1940690
    return-void

    :cond_2
    move v1, v2

    goto :goto_0
.end method
