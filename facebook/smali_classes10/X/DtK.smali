.class public final enum LX/DtK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DtK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DtK;

.field public static final enum ALL:LX/DtK;

.field public static final enum INCOMING:LX/DtK;

.field public static final enum OUTGOING:LX/DtK;


# instance fields
.field private final mTypeName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2051922
    new-instance v0, LX/DtK;

    const-string v1, "ALL"

    const-string v2, "all_messenger_payments"

    invoke-direct {v0, v1, v3, v2}, LX/DtK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DtK;->ALL:LX/DtK;

    .line 2051923
    new-instance v0, LX/DtK;

    const-string v1, "INCOMING"

    const-string v2, "incoming_messenger_payments"

    invoke-direct {v0, v1, v4, v2}, LX/DtK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DtK;->INCOMING:LX/DtK;

    .line 2051924
    new-instance v0, LX/DtK;

    const-string v1, "OUTGOING"

    const-string v2, "outgoing_messenger_payments"

    invoke-direct {v0, v1, v5, v2}, LX/DtK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DtK;->OUTGOING:LX/DtK;

    .line 2051925
    const/4 v0, 0x3

    new-array v0, v0, [LX/DtK;

    sget-object v1, LX/DtK;->ALL:LX/DtK;

    aput-object v1, v0, v3

    sget-object v1, LX/DtK;->INCOMING:LX/DtK;

    aput-object v1, v0, v4

    sget-object v1, LX/DtK;->OUTGOING:LX/DtK;

    aput-object v1, v0, v5

    sput-object v0, LX/DtK;->$VALUES:[LX/DtK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2051919
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2051920
    iput-object p3, p0, LX/DtK;->mTypeName:Ljava/lang/String;

    .line 2051921
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DtK;
    .locals 1

    .prologue
    .line 2051916
    const-class v0, LX/DtK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DtK;

    return-object v0
.end method

.method public static values()[LX/DtK;
    .locals 1

    .prologue
    .line 2051918
    sget-object v0, LX/DtK;->$VALUES:[LX/DtK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DtK;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051917
    iget-object v0, p0, LX/DtK;->mTypeName:Ljava/lang/String;

    return-object v0
.end method
