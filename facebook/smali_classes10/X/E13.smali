.class public final LX/E13;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/network/PlaceCreationParams;

.field public final synthetic b:LX/E14;


# direct methods
.method public constructor <init>(LX/E14;Lcom/facebook/places/create/network/PlaceCreationParams;)V
    .locals 0

    .prologue
    .line 2069332
    iput-object p1, p0, LX/E13;->b:LX/E14;

    iput-object p2, p0, LX/E13;->a:Lcom/facebook/places/create/network/PlaceCreationParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2069333
    :try_start_0
    iget-object v0, p0, LX/E13;->b:LX/E14;

    iget-object v0, v0, LX/E14;->c:LX/11H;

    iget-object v1, p0, LX/E13;->b:LX/E14;

    iget-object v1, v1, LX/E14;->b:LX/E11;

    iget-object v2, p0, LX/E13;->a:Lcom/facebook/places/create/network/PlaceCreationParams;

    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2069334
    :catch_0
    move-exception v0

    .line 2069335
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 2069336
    iget-object v2, p0, LX/E13;->b:LX/E14;

    iget-object v2, v2, LX/E14;->d:LX/E10;

    invoke-virtual {v2, v1}, LX/E10;->a(Lcom/facebook/http/protocol/ApiErrorResult;)V

    .line 2069337
    throw v0
.end method
