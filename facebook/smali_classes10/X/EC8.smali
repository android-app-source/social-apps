.class public final enum LX/EC8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EC8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EC8;

.field public static final enum INCALL:LX/EC8;

.field public static final enum INCALL_GROUP_ESCALATED:LX/EC8;

.field public static final enum INCOMING_CALL:LX/EC8;

.field public static final enum NO_ANSWER:LX/EC8;

.field public static final enum REDIAL:LX/EC8;

.field public static final enum VIDEO_REQUEST:LX/EC8;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2088249
    new-instance v0, LX/EC8;

    const-string v1, "INCALL"

    invoke-direct {v0, v1, v3}, LX/EC8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC8;->INCALL:LX/EC8;

    .line 2088250
    new-instance v0, LX/EC8;

    const-string v1, "INCALL_GROUP_ESCALATED"

    invoke-direct {v0, v1, v4}, LX/EC8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC8;->INCALL_GROUP_ESCALATED:LX/EC8;

    .line 2088251
    new-instance v0, LX/EC8;

    const-string v1, "INCOMING_CALL"

    invoke-direct {v0, v1, v5}, LX/EC8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC8;->INCOMING_CALL:LX/EC8;

    .line 2088252
    new-instance v0, LX/EC8;

    const-string v1, "NO_ANSWER"

    invoke-direct {v0, v1, v6}, LX/EC8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC8;->NO_ANSWER:LX/EC8;

    .line 2088253
    new-instance v0, LX/EC8;

    const-string v1, "REDIAL"

    invoke-direct {v0, v1, v7}, LX/EC8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC8;->REDIAL:LX/EC8;

    .line 2088254
    new-instance v0, LX/EC8;

    const-string v1, "VIDEO_REQUEST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EC8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC8;->VIDEO_REQUEST:LX/EC8;

    .line 2088255
    const/4 v0, 0x6

    new-array v0, v0, [LX/EC8;

    sget-object v1, LX/EC8;->INCALL:LX/EC8;

    aput-object v1, v0, v3

    sget-object v1, LX/EC8;->INCALL_GROUP_ESCALATED:LX/EC8;

    aput-object v1, v0, v4

    sget-object v1, LX/EC8;->INCOMING_CALL:LX/EC8;

    aput-object v1, v0, v5

    sget-object v1, LX/EC8;->NO_ANSWER:LX/EC8;

    aput-object v1, v0, v6

    sget-object v1, LX/EC8;->REDIAL:LX/EC8;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EC8;->VIDEO_REQUEST:LX/EC8;

    aput-object v2, v0, v1

    sput-object v0, LX/EC8;->$VALUES:[LX/EC8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2088256
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EC8;
    .locals 1

    .prologue
    .line 2088257
    const-class v0, LX/EC8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EC8;

    return-object v0
.end method

.method public static values()[LX/EC8;
    .locals 1

    .prologue
    .line 2088258
    sget-object v0, LX/EC8;->$VALUES:[LX/EC8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EC8;

    return-object v0
.end method
