.class public final LX/EMU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8d1;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:LX/CzL;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/8d1;LX/1Pn;LX/CzL;)V
    .locals 0

    .prologue
    .line 2110742
    iput-object p1, p0, LX/EMU;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iput-object p2, p0, LX/EMU;->a:LX/8d1;

    iput-object p3, p0, LX/EMU;->b:LX/1Pn;

    iput-object p4, p0, LX/EMU;->c:LX/CzL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 17

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const v3, -0x70d7cfa1

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v9

    .line 2110743
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->a:LX/8d1;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->a:LX/8d1;

    invoke-interface {v1}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2110744
    :cond_0
    const/4 v1, 0x2

    const/4 v2, 0x2

    const v3, -0x3d724343

    invoke-static {v1, v2, v3, v9}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2110745
    :goto_0
    return-void

    .line 2110746
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->a:LX/8d1;

    invoke-interface {v1}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMU;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/DHs;->ALL_FRIENDS:LX/DHs;

    invoke-virtual {v1}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v1

    .line 2110747
    :goto_1
    sget-object v2, LX/0ax;->bO:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/EMU;->a:LX/8d1;

    invoke-interface {v3}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/DHr;->SEARCH:LX/DHr;

    invoke-virtual {v4}, LX/DHr;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 2110748
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 2110749
    const-string v1, "profile_name"

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMU;->a:LX/8d1;

    invoke-interface {v2}, LX/8d1;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2110750
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, LX/CvY;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v12

    sget-object v13, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v14, LX/7CM;->INLINE_FRIENDS_LINK:LX/7CM;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->b:LX/1Pn;

    check-cast v1, LX/CxP;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMU;->c:LX/CzL;

    invoke-interface {v1, v2}, LX/CxP;->b(LX/CzL;)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->c:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v1}, LX/CvY;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CvV;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v3, LX/7CM;->INLINE_FRIENDS_LINK:LX/7CM;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/EMU;->a:LX/8d1;

    invoke-interface {v4}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, LX/EMU;->a:LX/8d1;

    invoke-interface {v6}, LX/8d1;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    move-object v1, v8

    move-object v2, v12

    move-object v3, v13

    move-object v4, v14

    move v5, v15

    move-object/from16 v6, v16

    invoke-virtual/range {v1 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2110751
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMU;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17W;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMU;->b:LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v10, v11, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2110752
    const v1, -0xa567bc9

    invoke-static {v1, v9}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2110753
    :cond_2
    sget-object v1, LX/DHs;->SUGGESTIONS:LX/DHs;

    invoke-virtual {v1}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method
