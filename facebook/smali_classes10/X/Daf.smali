.class public LX/Daf;
.super LX/1ah;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:I

.field public static final c:I


# instance fields
.field private final d:I

.field private final e:Landroid/content/res/Resources;

.field private final f:LX/Dad;

.field public g:Z

.field public h:Z

.field public i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2015812
    const v0, 0x7f0219a6

    sput v0, LX/Daf;->a:I

    .line 2015813
    const v0, 0x7f0219a5

    sput v0, LX/Daf;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2015804
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-direct {p0, v0}, LX/1ah;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 2015805
    new-instance v0, LX/Dad;

    invoke-direct {v0}, LX/Dad;-><init>()V

    iput-object v0, p0, LX/Daf;->f:LX/Dad;

    .line 2015806
    iput-boolean v2, p0, LX/Daf;->g:Z

    .line 2015807
    iput-boolean v2, p0, LX/Daf;->h:Z

    .line 2015808
    const/4 v0, -0x1

    iput v0, p0, LX/Daf;->i:I

    .line 2015809
    iput-object p1, p0, LX/Daf;->e:Landroid/content/res/Resources;

    .line 2015810
    iget-object v0, p0, LX/Daf;->e:Landroid/content/res/Resources;

    const v1, 0x7f0b1f71

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Daf;->d:I

    .line 2015811
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 2015777
    iget-boolean v0, p0, LX/Daf;->g:Z

    if-eqz v0, :cond_0

    .line 2015778
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Daf;->g:Z

    .line 2015779
    iget-object v0, p0, LX/Daf;->e:Landroid/content/res/Resources;

    iget-boolean v1, p0, LX/Daf;->h:Z

    .line 2015780
    if-eqz v1, :cond_2

    sget v2, LX/Daf;->a:I

    :goto_0
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object v0, v2

    .line 2015781
    invoke-virtual {p0, v0}, LX/1ah;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 2015782
    :cond_0
    invoke-virtual {p0}, LX/Daf;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 2015783
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2015784
    iget-object v1, p0, LX/Daf;->f:LX/Dad;

    invoke-virtual {p0}, LX/Daf;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 2015785
    iget-object v3, v1, LX/Dad;->c:Landroid/graphics/Bitmap;

    if-eq v0, v3, :cond_1

    .line 2015786
    iput-object v0, v1, LX/Dad;->c:Landroid/graphics/Bitmap;

    .line 2015787
    new-instance v3, Landroid/graphics/BitmapShader;

    sget-object v4, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v5, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v3, v0, v4, v5}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v3, v1, LX/Dad;->d:Landroid/graphics/BitmapShader;

    .line 2015788
    iget-object v3, v1, LX/Dad;->a:Landroid/graphics/Paint;

    iget-object v4, v1, LX/Dad;->d:Landroid/graphics/BitmapShader;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2015789
    :cond_1
    iget-object v3, v1, LX/Dad;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 2015790
    iget-object v3, v1, LX/Dad;->b:Landroid/graphics/Matrix;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    iget v5, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p0

    sub-int/2addr v6, p0

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 2015791
    iget-object v3, v1, LX/Dad;->d:Landroid/graphics/BitmapShader;

    iget-object v4, v1, LX/Dad;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 2015792
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 2015793
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, v1, LX/Dad;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v3, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2015794
    return-void

    :cond_2
    sget v2, LX/Daf;->c:I

    goto/16 :goto_0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 2015795
    invoke-super {p0, p1}, LX/1ah;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 2015796
    iget v1, p0, LX/Daf;->i:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v2, p0, LX/Daf;->d:I

    if-ge v0, v2, :cond_2

    const/4 v0, 0x1

    .line 2015797
    :goto_0
    iget v2, p0, LX/Daf;->i:I

    if-ne v1, v2, :cond_0

    iget-boolean v2, p0, LX/Daf;->h:Z

    if-eq v0, v2, :cond_1

    .line 2015798
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/Daf;->g:Z

    .line 2015799
    invoke-virtual {p0}, LX/Daf;->invalidateSelf()V

    .line 2015800
    :cond_1
    iput v1, p0, LX/Daf;->i:I

    .line 2015801
    iput-boolean v0, p0, LX/Daf;->h:Z

    .line 2015802
    return-void

    .line 2015803
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
