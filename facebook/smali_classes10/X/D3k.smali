.class public final LX/D3k;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/uicontrib/calendar/CalendarView;

.field public final b:Ljava/util/Calendar;

.field public c:Ljava/util/Calendar;

.field private final d:Landroid/view/GestureDetector;

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Lcom/facebook/uicontrib/calendar/CalendarView;)V
    .locals 3

    .prologue
    .line 1960526
    iput-object p1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1960527
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    .line 1960528
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/calendar/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/D3j;

    invoke-direct {v2, p0}, LX/D3j;-><init>(LX/D3k;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/D3k;->d:Landroid/view/GestureDetector;

    .line 1960529
    invoke-static {p0}, LX/D3k;->f(LX/D3k;)V

    .line 1960530
    return-void
.end method

.method private a(Ljava/util/Calendar;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 1960531
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-eqz v0, :cond_0

    .line 1960532
    invoke-virtual {p0, p1, v1}, LX/D3k;->b(Ljava/util/Calendar;Z)V

    .line 1960533
    :goto_0
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v0, p1}, Lcom/facebook/uicontrib/calendar/CalendarView;->setMonthDisplayed(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960534
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 1960535
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v6, 0x80004

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 1960536
    return-void

    .line 1960537
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/D3k;->a(Ljava/util/Calendar;Z)V

    goto :goto_0
.end method

.method private a(Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 6

    .prologue
    const/16 v5, 0xe

    const/16 v4, 0xd

    const/16 v3, 0xc

    const/16 v2, 0xb

    .line 1960538
    sget-object v0, LX/D3f;->a:[I

    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->P:LX/D3g;

    invoke-virtual {v1}, LX/D3g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1960539
    :goto_0
    return-void

    .line 1960540
    :pswitch_0
    invoke-virtual {p2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v5, v0}, Ljava/util/Calendar;->set(II)V

    .line 1960541
    :pswitch_1
    invoke-virtual {p2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v4, v0}, Ljava/util/Calendar;->set(II)V

    .line 1960542
    :pswitch_2
    invoke-virtual {p2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v3, v0}, Ljava/util/Calendar;->set(II)V

    .line 1960543
    :pswitch_3
    invoke-virtual {p2, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p1, v2, v0}, Ljava/util/Calendar;->set(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private b(I)V
    .locals 7

    .prologue
    const/4 v3, 0x6

    const/4 v6, 0x2

    const/16 v4, 0xb

    .line 1960544
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    if-nez v0, :cond_1

    .line 1960545
    :cond_0
    :goto_0
    return-void

    .line 1960546
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1960547
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1960548
    iget-object v2, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1960549
    iget-object v2, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1960550
    invoke-virtual {v0, v3, p1}, Ljava/util/Calendar;->add(II)V

    .line 1960551
    invoke-virtual {v1, v3, p1}, Ljava/util/Calendar;->add(II)V

    .line 1960552
    iget-object v2, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    if-eqz v2, :cond_3

    .line 1960553
    iget-object v2, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1960554
    iget-object v2, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1960555
    iget-object v2, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-direct {p0, v0, v2}, LX/D3k;->a(Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 1960556
    :cond_2
    iget-object v2, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1960557
    iget-object v2, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1960558
    iget-object v2, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-direct {p0, v1, v2}, LX/D3k;->a(Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 1960559
    :cond_3
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1960560
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0x17

    if-ge v2, v3, :cond_5

    .line 1960561
    iget-object v2, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-direct {p0, v1, v2}, LX/D3k;->a(Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 1960562
    const/4 v2, 0x1

    invoke-virtual {v1, v4, v2}, Ljava/util/Calendar;->roll(II)V

    .line 1960563
    :cond_4
    :goto_1
    iget-object v2, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960564
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960565
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I

    move-result v0

    iput v0, p0, LX/D3k;->e:I

    .line 1960566
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I

    move-result v0

    iput v0, p0, LX/D3k;->f:I

    .line 1960567
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-eqz v0, :cond_6

    .line 1960568
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, LX/D3k;->g:I

    .line 1960569
    :goto_2
    const v0, -0x6b186418

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_0

    .line 1960570
    :cond_5
    iget-object v2, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-direct {p0, v0, v2}, LX/D3k;->a(Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 1960571
    const/4 v2, -0x1

    invoke-virtual {v0, v4, v2}, Ljava/util/Calendar;->roll(II)V

    goto :goto_1

    .line 1960572
    :cond_6
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, LX/D3k;->g:I

    goto :goto_2
.end method

.method private b(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1960573
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-wide v2, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->aa:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 1960574
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-wide v4, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->aa:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/D3k;)V
    .locals 3

    .prologue
    const/4 v2, 0x7

    .line 1960575
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I

    move-result v0

    iput v0, p0, LX/D3k;->e:I

    .line 1960576
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 1960577
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I

    move-result v0

    iput v0, p0, LX/D3k;->f:I

    .line 1960578
    :cond_0
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I

    move-result v0

    iput v0, p0, LX/D3k;->h:I

    .line 1960579
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    if-eq v0, v1, :cond_2

    .line 1960580
    :cond_1
    iget v0, p0, LX/D3k;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/D3k;->h:I

    .line 1960581
    :cond_2
    const v0, 0x7407a093

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1960582
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1960596
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960597
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)V

    .line 1960598
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1960522
    iget v0, p0, LX/D3k;->g:I

    if-ne v0, p1, :cond_0

    .line 1960523
    :goto_0
    return-void

    .line 1960524
    :cond_0
    iput p1, p0, LX/D3k;->g:I

    .line 1960525
    const v0, 0x29e8d4fb

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Calendar;Z)V
    .locals 6

    .prologue
    .line 1960583
    if-eqz p2, :cond_0

    .line 1960584
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-direct {p0, p1, v0}, LX/D3k;->a(Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 1960585
    :cond_0
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1960586
    :cond_1
    :goto_0
    return-void

    .line 1960587
    :cond_2
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1960588
    :cond_3
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    if-eqz v0, :cond_4

    .line 1960589
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1960590
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-object v1, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-direct {p0, v0}, LX/D3k;->b(I)V

    .line 1960591
    :cond_4
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960592
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I

    move-result v0

    iput v0, p0, LX/D3k;->e:I

    .line 1960593
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, LX/D3k;->g:I

    .line 1960594
    const v0, 0x10d28548

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 1960595
    :cond_5
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-direct {p0, p1, v0}, LX/D3k;->b(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1960513
    const/4 v0, 0x0

    iput-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    .line 1960514
    iput v2, p0, LX/D3k;->f:I

    .line 1960515
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, LX/D3k;->g:I

    .line 1960516
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    .line 1960517
    iput-boolean v2, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    .line 1960518
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->b:Ljava/util/Calendar;

    .line 1960519
    invoke-static {v0, v1, v3, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;ZZZ)V

    .line 1960520
    const v0, -0x79b9d9d4

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1960521
    return-void
.end method

.method public final b(Ljava/util/Calendar;Z)V
    .locals 6

    .prologue
    .line 1960499
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    if-nez v0, :cond_2

    .line 1960500
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1960501
    :cond_0
    :goto_0
    return-void

    .line 1960502
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    .line 1960503
    :cond_2
    if-eqz p2, :cond_3

    .line 1960504
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-direct {p0, p1, v0}, LX/D3k;->a(Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 1960505
    :cond_3
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1960506
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1960507
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-object v1, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-direct {p0, v0}, LX/D3k;->b(I)V

    goto :goto_0

    .line 1960508
    :cond_4
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-direct {p0, v0, p1}, LX/D3k;->b(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960509
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960510
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-static {v0, v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->b(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;)I

    move-result v0

    iput v0, p0, LX/D3k;->f:I

    .line 1960511
    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, LX/D3k;->g:I

    .line 1960512
    const v0, 0x2b9feffc

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1960469
    iget-object v0, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D3k;->c:Ljava/util/Calendar;

    .line 1960470
    :goto_0
    if-eqz v0, :cond_0

    .line 1960471
    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    .line 1960472
    invoke-static {v1, v0, v2, v2, v2}, Lcom/facebook/uicontrib/calendar/CalendarView;->a$redex0(Lcom/facebook/uicontrib/calendar/CalendarView;Ljava/util/Calendar;ZZZ)V

    .line 1960473
    :cond_0
    return-void

    .line 1960474
    :cond_1
    iget-object v0, p0, LX/D3k;->b:Ljava/util/Calendar;

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1960498
    iget v0, p0, LX/D3k;->h:I

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1960497
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1960496
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v2, 0x1

    const/4 v1, -0x1

    const/4 v5, -0x2

    .line 1960482
    if-eqz p2, :cond_0

    .line 1960483
    check-cast p2, LX/D3i;

    move-object v0, p2

    .line 1960484
    :goto_0
    iget v3, p0, LX/D3k;->e:I

    if-ne v3, p1, :cond_1

    iget-object v3, p0, LX/D3k;->b:Ljava/util/Calendar;

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 1960485
    :goto_1
    iget v4, p0, LX/D3k;->f:I

    if-ne v4, p1, :cond_2

    iget-object v1, p0, LX/D3k;->c:Ljava/util/Calendar;

    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 1960486
    :goto_2
    iget v1, p0, LX/D3k;->e:I

    if-lt p1, v1, :cond_3

    iget v1, p0, LX/D3k;->f:I

    if-gt p1, v1, :cond_3

    :goto_3
    iget v5, p0, LX/D3k;->g:I

    move v1, p1

    invoke-virtual/range {v0 .. v5}, LX/D3i;->a(IZIII)V

    .line 1960487
    return-object v0

    .line 1960488
    :cond_0
    new-instance v0, LX/D3i;

    iget-object v3, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v4, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-virtual {v4}, Lcom/facebook/uicontrib/calendar/CalendarView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v3, v4}, LX/D3i;-><init>(Lcom/facebook/uicontrib/calendar/CalendarView;Landroid/content/Context;)V

    .line 1960489
    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 1960490
    invoke-virtual {v0, v3}, LX/D3i;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1960491
    invoke-virtual {v0, v2}, LX/D3i;->setClickable(Z)V

    .line 1960492
    invoke-virtual {v0, p0}, LX/D3i;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    :cond_1
    move v3, v1

    .line 1960493
    goto :goto_1

    :cond_2
    move v4, v1

    .line 1960494
    goto :goto_2

    .line 1960495
    :cond_3
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1960475
    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/D3k;->d:Landroid/view/GestureDetector;

    invoke-virtual {v1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1960476
    check-cast p1, LX/D3i;

    .line 1960477
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-virtual {p1, v1, v2}, LX/D3i;->a(FLjava/util/Calendar;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1960478
    :cond_0
    :goto_0
    return v0

    .line 1960479
    :cond_1
    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v2, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v2, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1960480
    iget-object v1, p0, LX/D3k;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-direct {p0, v1}, LX/D3k;->a(Ljava/util/Calendar;)V

    goto :goto_0

    .line 1960481
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
