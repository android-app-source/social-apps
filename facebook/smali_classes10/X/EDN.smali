.class public final LX/EDN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/76H;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/3E8;


# direct methods
.method public constructor <init>(LX/3E8;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2091280
    iput-object p1, p0, LX/EDN;->c:LX/3E8;

    iput-object p2, p0, LX/EDN;->a:Ljava/lang/String;

    iput-object p3, p0, LX/EDN;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 2091281
    iget-object v0, p0, LX/EDN;->c:LX/3E8;

    iget-object v0, v0, LX/3E8;->g:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2091282
    iget-object v0, p0, LX/EDN;->c:LX/3E8;

    iget-object v0, v0, LX/3E8;->g:LX/2Tm;

    iget-object v1, p0, LX/EDN;->a:Ljava/lang/String;

    iget-object v2, p0, LX/EDN;->b:Ljava/lang/String;

    const/4 v3, -0x3

    const-string v4, "MQTT"

    const-string v5, "Mqtt send failure"

    .line 2091283
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2091284
    iget-object v6, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/facebook/webrtc/WebrtcEngine;->onMultiwayMessageSendError(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 2091285
    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2091286
    iget-object v0, p0, LX/EDN;->c:LX/3E8;

    iget-object v0, v0, LX/3E8;->g:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2091287
    iget-object v0, p0, LX/EDN;->c:LX/3E8;

    iget-object v0, v0, LX/3E8;->g:LX/2Tm;

    iget-object v1, p0, LX/EDN;->a:Ljava/lang/String;

    iget-object v2, p0, LX/EDN;->b:Ljava/lang/String;

    .line 2091288
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2091289
    iget-object p0, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/webrtc/WebrtcEngine;->onMultiwayMessageSendSuccess(Ljava/lang/String;Ljava/lang/String;)V

    .line 2091290
    :cond_0
    return-void
.end method
