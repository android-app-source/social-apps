.class public final LX/DDy;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DE0;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/DEI;

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/DE0;


# direct methods
.method public constructor <init>(LX/DE0;)V
    .locals 1

    .prologue
    .line 1976161
    iput-object p1, p0, LX/DDy;->c:LX/DE0;

    .line 1976162
    move-object v0, p1

    .line 1976163
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1976164
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1976160
    const-string v0, "ActionButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1976146
    if-ne p0, p1, :cond_1

    .line 1976147
    :cond_0
    :goto_0
    return v0

    .line 1976148
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1976149
    goto :goto_0

    .line 1976150
    :cond_3
    check-cast p1, LX/DDy;

    .line 1976151
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1976152
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1976153
    if-eq v2, v3, :cond_0

    .line 1976154
    iget-object v2, p0, LX/DDy;->a:LX/DEI;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DDy;->a:LX/DEI;

    iget-object v3, p1, LX/DDy;->a:LX/DEI;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1976155
    goto :goto_0

    .line 1976156
    :cond_5
    iget-object v2, p1, LX/DDy;->a:LX/DEI;

    if-nez v2, :cond_4

    .line 1976157
    :cond_6
    iget-object v2, p0, LX/DDy;->b:LX/1Po;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/DDy;->b:LX/1Po;

    iget-object v3, p1, LX/DDy;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1976158
    goto :goto_0

    .line 1976159
    :cond_7
    iget-object v2, p1, LX/DDy;->b:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
