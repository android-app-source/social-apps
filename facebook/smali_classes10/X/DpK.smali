.class public LX/DpK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;


# instance fields
.field public final identity_key:[B

.field public final lookup_result:LX/DpL;

.field public final msg_to:LX/DpM;

.field public final pre_key_with_id:LX/DpU;

.field public final signed_pre_key_with_id:LX/Dpf;

.field public final suggested_codename:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/16 v3, 0xc

    .line 2044324
    new-instance v0, LX/1sv;

    const-string v1, "LookupResponsePayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpK;->b:LX/1sv;

    .line 2044325
    new-instance v0, LX/1sw;

    const-string v1, "msg_to"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpK;->c:LX/1sw;

    .line 2044326
    new-instance v0, LX/1sw;

    const-string v1, "suggested_codename"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpK;->d:LX/1sw;

    .line 2044327
    new-instance v0, LX/1sw;

    const-string v1, "identity_key"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpK;->e:LX/1sw;

    .line 2044328
    new-instance v0, LX/1sw;

    const-string v1, "signed_pre_key_with_id"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpK;->f:LX/1sw;

    .line 2044329
    new-instance v0, LX/1sw;

    const-string v1, "pre_key_with_id"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpK;->g:LX/1sw;

    .line 2044330
    new-instance v0, LX/1sw;

    const-string v1, "lookup_result"

    invoke-direct {v0, v1, v3, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpK;->h:LX/1sw;

    .line 2044331
    const/4 v0, 0x1

    sput-boolean v0, LX/DpK;->a:Z

    return-void
.end method

.method public constructor <init>(LX/DpM;Ljava/lang/String;[BLX/Dpf;LX/DpU;LX/DpL;)V
    .locals 0

    .prologue
    .line 2044154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2044155
    iput-object p1, p0, LX/DpK;->msg_to:LX/DpM;

    .line 2044156
    iput-object p2, p0, LX/DpK;->suggested_codename:Ljava/lang/String;

    .line 2044157
    iput-object p3, p0, LX/DpK;->identity_key:[B

    .line 2044158
    iput-object p4, p0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    .line 2044159
    iput-object p5, p0, LX/DpK;->pre_key_with_id:LX/DpU;

    .line 2044160
    iput-object p6, p0, LX/DpK;->lookup_result:LX/DpL;

    .line 2044161
    return-void
.end method

.method public static b(LX/1su;)LX/DpK;
    .locals 10

    .prologue
    const/16 v9, 0xb

    const/16 v8, 0xc

    const/4 v6, 0x0

    .line 2044298
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v5, v6

    move-object v4, v6

    move-object v3, v6

    move-object v2, v6

    move-object v1, v6

    .line 2044299
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 2044300
    iget-byte v7, v0, LX/1sw;->b:B

    if-eqz v7, :cond_6

    .line 2044301
    iget-short v7, v0, LX/1sw;->c:S

    packed-switch v7, :pswitch_data_0

    .line 2044302
    :pswitch_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044303
    :pswitch_1
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v8, :cond_0

    .line 2044304
    invoke-static {p0}, LX/DpM;->b(LX/1su;)LX/DpM;

    move-result-object v1

    goto :goto_0

    .line 2044305
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044306
    :pswitch_2
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v9, :cond_1

    .line 2044307
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2044308
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044309
    :pswitch_3
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v9, :cond_2

    .line 2044310
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v3

    goto :goto_0

    .line 2044311
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044312
    :pswitch_4
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v8, :cond_3

    .line 2044313
    invoke-static {p0}, LX/Dpf;->b(LX/1su;)LX/Dpf;

    move-result-object v4

    goto :goto_0

    .line 2044314
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044315
    :pswitch_5
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v8, :cond_4

    .line 2044316
    invoke-static {p0}, LX/DpU;->b(LX/1su;)LX/DpU;

    move-result-object v5

    goto :goto_0

    .line 2044317
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044318
    :pswitch_6
    iget-byte v7, v0, LX/1sw;->b:B

    if-ne v7, v8, :cond_5

    .line 2044319
    invoke-static {p0}, LX/DpL;->b(LX/1su;)LX/DpL;

    move-result-object v6

    goto :goto_0

    .line 2044320
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044321
    :cond_6
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2044322
    new-instance v0, LX/DpK;

    invoke-direct/range {v0 .. v6}, LX/DpK;-><init>(LX/DpM;Ljava/lang/String;[BLX/Dpf;LX/DpU;LX/DpL;)V

    .line 2044323
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2044238
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2044239
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2044240
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2044241
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "LookupResponsePayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2044242
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044243
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044244
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044245
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044246
    const-string v4, "msg_to"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044247
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044248
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044249
    iget-object v4, p0, LX/DpK;->msg_to:LX/DpM;

    if-nez v4, :cond_3

    .line 2044250
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044251
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044252
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044253
    const-string v4, "suggested_codename"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044254
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044255
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044256
    iget-object v4, p0, LX/DpK;->suggested_codename:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 2044257
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044258
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044259
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044260
    const-string v4, "identity_key"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044261
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044262
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044263
    iget-object v4, p0, LX/DpK;->identity_key:[B

    if-nez v4, :cond_5

    .line 2044264
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044265
    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044266
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044267
    const-string v4, "signed_pre_key_with_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044268
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044269
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044270
    iget-object v4, p0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    if-nez v4, :cond_6

    .line 2044271
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044272
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044273
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044274
    const-string v4, "pre_key_with_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044275
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044276
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044277
    iget-object v4, p0, LX/DpK;->pre_key_with_id:LX/DpU;

    if-nez v4, :cond_7

    .line 2044278
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044279
    :goto_7
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044280
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044281
    const-string v4, "lookup_result"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044282
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044283
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044284
    iget-object v0, p0, LX/DpK;->lookup_result:LX/DpL;

    if-nez v0, :cond_8

    .line 2044285
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044286
    :goto_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044287
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044288
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2044289
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2044290
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2044291
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2044292
    :cond_3
    iget-object v4, p0, LX/DpK;->msg_to:LX/DpM;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2044293
    :cond_4
    iget-object v4, p0, LX/DpK;->suggested_codename:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2044294
    :cond_5
    iget-object v4, p0, LX/DpK;->identity_key:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2044295
    :cond_6
    iget-object v4, p0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2044296
    :cond_7
    iget-object v4, p0, LX/DpK;->pre_key_with_id:LX/DpU;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2044297
    :cond_8
    iget-object v0, p0, LX/DpK;->lookup_result:LX/DpL;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2044216
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2044217
    iget-object v0, p0, LX/DpK;->msg_to:LX/DpM;

    if-eqz v0, :cond_0

    .line 2044218
    sget-object v0, LX/DpK;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044219
    iget-object v0, p0, LX/DpK;->msg_to:LX/DpM;

    invoke-virtual {v0, p1}, LX/DpM;->a(LX/1su;)V

    .line 2044220
    :cond_0
    iget-object v0, p0, LX/DpK;->suggested_codename:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2044221
    sget-object v0, LX/DpK;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044222
    iget-object v0, p0, LX/DpK;->suggested_codename:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2044223
    :cond_1
    iget-object v0, p0, LX/DpK;->identity_key:[B

    if-eqz v0, :cond_2

    .line 2044224
    sget-object v0, LX/DpK;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044225
    iget-object v0, p0, LX/DpK;->identity_key:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2044226
    :cond_2
    iget-object v0, p0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v0, :cond_3

    .line 2044227
    sget-object v0, LX/DpK;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044228
    iget-object v0, p0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    invoke-virtual {v0, p1}, LX/Dpf;->a(LX/1su;)V

    .line 2044229
    :cond_3
    iget-object v0, p0, LX/DpK;->pre_key_with_id:LX/DpU;

    if-eqz v0, :cond_4

    .line 2044230
    sget-object v0, LX/DpK;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044231
    iget-object v0, p0, LX/DpK;->pre_key_with_id:LX/DpU;

    invoke-virtual {v0, p1}, LX/DpU;->a(LX/1su;)V

    .line 2044232
    :cond_4
    iget-object v0, p0, LX/DpK;->lookup_result:LX/DpL;

    if-eqz v0, :cond_5

    .line 2044233
    sget-object v0, LX/DpK;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044234
    iget-object v0, p0, LX/DpK;->lookup_result:LX/DpL;

    invoke-virtual {v0, p1}, LX/DpL;->a(LX/1su;)V

    .line 2044235
    :cond_5
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2044236
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2044237
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2044166
    if-nez p1, :cond_1

    .line 2044167
    :cond_0
    :goto_0
    return v0

    .line 2044168
    :cond_1
    instance-of v1, p1, LX/DpK;

    if-eqz v1, :cond_0

    .line 2044169
    check-cast p1, LX/DpK;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2044170
    if-nez p1, :cond_3

    .line 2044171
    :cond_2
    :goto_1
    move v0, v2

    .line 2044172
    goto :goto_0

    .line 2044173
    :cond_3
    iget-object v0, p0, LX/DpK;->msg_to:LX/DpM;

    if-eqz v0, :cond_10

    move v0, v1

    .line 2044174
    :goto_2
    iget-object v3, p1, LX/DpK;->msg_to:LX/DpM;

    if-eqz v3, :cond_11

    move v3, v1

    .line 2044175
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2044176
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044177
    iget-object v0, p0, LX/DpK;->msg_to:LX/DpM;

    iget-object v3, p1, LX/DpK;->msg_to:LX/DpM;

    invoke-virtual {v0, v3}, LX/DpM;->a(LX/DpM;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044178
    :cond_5
    iget-object v0, p0, LX/DpK;->suggested_codename:Ljava/lang/String;

    if-eqz v0, :cond_12

    move v0, v1

    .line 2044179
    :goto_4
    iget-object v3, p1, LX/DpK;->suggested_codename:Ljava/lang/String;

    if-eqz v3, :cond_13

    move v3, v1

    .line 2044180
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2044181
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044182
    iget-object v0, p0, LX/DpK;->suggested_codename:Ljava/lang/String;

    iget-object v3, p1, LX/DpK;->suggested_codename:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044183
    :cond_7
    iget-object v0, p0, LX/DpK;->identity_key:[B

    if-eqz v0, :cond_14

    move v0, v1

    .line 2044184
    :goto_6
    iget-object v3, p1, LX/DpK;->identity_key:[B

    if-eqz v3, :cond_15

    move v3, v1

    .line 2044185
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2044186
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044187
    iget-object v0, p0, LX/DpK;->identity_key:[B

    iget-object v3, p1, LX/DpK;->identity_key:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044188
    :cond_9
    iget-object v0, p0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v0, :cond_16

    move v0, v1

    .line 2044189
    :goto_8
    iget-object v3, p1, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v3, :cond_17

    move v3, v1

    .line 2044190
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2044191
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044192
    iget-object v0, p0, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    iget-object v3, p1, LX/DpK;->signed_pre_key_with_id:LX/Dpf;

    invoke-virtual {v0, v3}, LX/Dpf;->a(LX/Dpf;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044193
    :cond_b
    iget-object v0, p0, LX/DpK;->pre_key_with_id:LX/DpU;

    if-eqz v0, :cond_18

    move v0, v1

    .line 2044194
    :goto_a
    iget-object v3, p1, LX/DpK;->pre_key_with_id:LX/DpU;

    if-eqz v3, :cond_19

    move v3, v1

    .line 2044195
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2044196
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044197
    iget-object v0, p0, LX/DpK;->pre_key_with_id:LX/DpU;

    iget-object v3, p1, LX/DpK;->pre_key_with_id:LX/DpU;

    invoke-virtual {v0, v3}, LX/DpU;->a(LX/DpU;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044198
    :cond_d
    iget-object v0, p0, LX/DpK;->lookup_result:LX/DpL;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 2044199
    :goto_c
    iget-object v3, p1, LX/DpK;->lookup_result:LX/DpL;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 2044200
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 2044201
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044202
    iget-object v0, p0, LX/DpK;->lookup_result:LX/DpL;

    iget-object v3, p1, LX/DpK;->lookup_result:LX/DpL;

    invoke-virtual {v0, v3}, LX/DpL;->a(LX/DpL;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_f
    move v2, v1

    .line 2044203
    goto/16 :goto_1

    :cond_10
    move v0, v2

    .line 2044204
    goto/16 :goto_2

    :cond_11
    move v3, v2

    .line 2044205
    goto/16 :goto_3

    :cond_12
    move v0, v2

    .line 2044206
    goto/16 :goto_4

    :cond_13
    move v3, v2

    .line 2044207
    goto/16 :goto_5

    :cond_14
    move v0, v2

    .line 2044208
    goto :goto_6

    :cond_15
    move v3, v2

    .line 2044209
    goto :goto_7

    :cond_16
    move v0, v2

    .line 2044210
    goto :goto_8

    :cond_17
    move v3, v2

    .line 2044211
    goto :goto_9

    :cond_18
    move v0, v2

    .line 2044212
    goto :goto_a

    :cond_19
    move v3, v2

    .line 2044213
    goto :goto_b

    :cond_1a
    move v0, v2

    .line 2044214
    goto :goto_c

    :cond_1b
    move v3, v2

    .line 2044215
    goto :goto_d
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2044165
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2044162
    sget-boolean v0, LX/DpK;->a:Z

    .line 2044163
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpK;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2044164
    return-object v0
.end method
