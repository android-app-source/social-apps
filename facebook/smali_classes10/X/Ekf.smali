.class public LX/Ekf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "LX/EkU;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2163693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163694
    new-instance v0, LX/01J;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/01J;-><init>(I)V

    iput-object v0, p0, LX/Ekf;->a:LX/01J;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a([Ljava/lang/Object;)LX/EkU;
    .locals 6

    .prologue
    .line 2163682
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/String;

    .line 2163683
    const/4 v1, 0x1

    aget-object v1, p1, v1

    check-cast v1, Ljava/lang/String;

    .line 2163684
    const/4 v2, 0x4

    aget-object v2, p1, v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    .line 2163685
    const/4 v3, 0x2

    aget-object v3, p1, v3

    check-cast v3, [Ljava/lang/String;

    check-cast v3, [Ljava/lang/String;

    .line 2163686
    const/4 v4, 0x6

    aget-object v4, p1, v4

    check-cast v4, Ljava/lang/String;

    .line 2163687
    iget-object v5, p0, LX/Ekf;->a:LX/01J;

    invoke-virtual {v5, v0}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/EkU;

    .line 2163688
    if-nez v5, :cond_0

    .line 2163689
    new-instance v5, LX/EkU;

    invoke-direct {v5, v1, v2, v3, v4}, LX/EkU;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 2163690
    iget-object v1, p0, LX/Ekf;->a:LX/01J;

    invoke-virtual {v1, v0, v5}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2163691
    :cond_0
    monitor-exit p0

    return-object v5

    .line 2163692
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
