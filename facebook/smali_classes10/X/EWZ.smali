.class public abstract LX/EWZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType::",
        "LX/EWW;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/protobuf/Parser",
        "<TMessageType;>;"
    }
.end annotation


# static fields
.field public static final a:LX/EYZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2130360
    sget-object v0, LX/EYZ;->c:LX/EYZ;

    move-object v0, v0

    .line 2130361
    sput-object v0, LX/EWZ;->a:LX/EYZ;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2130352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/EWZ;[BIILX/EYZ;)LX/EWW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "LX/EYZ;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 2130340
    :try_start_0
    invoke-static {p1, p2, p3}, LX/EWd;->a([BII)LX/EWd;

    move-result-object v1

    .line 2130341
    invoke-virtual {p0, v1, p4}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2130342
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v2}, LX/EWd;->a(I)V
    :try_end_1
    .catch LX/EYr; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2130343
    return-object v0

    .line 2130344
    :catch_0
    move-exception v1

    .line 2130345
    :try_start_2
    iput-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2130346
    move-object v0, v1

    .line 2130347
    throw v0
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2130348
    :catch_1
    move-exception v0

    .line 2130349
    throw v0

    .line 2130350
    :catch_2
    move-exception v0

    .line 2130351
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(LX/EWW;)LX/EZL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)",
            "LX/EZL;"
        }
    .end annotation

    .prologue
    .line 2130337
    instance-of v0, p0, LX/EWX;

    if-eqz v0, :cond_0

    .line 2130338
    check-cast p0, LX/EWX;

    invoke-virtual {p0}, LX/EWX;->c()LX/EZL;

    move-result-object v0

    .line 2130339
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/EZL;

    invoke-direct {v0}, LX/EZL;-><init>()V

    goto :goto_0
.end method

.method public static b(LX/EWW;)LX/EWW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMessageType;)TMessageType;"
        }
    .end annotation

    .prologue
    .line 2130353
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/EWQ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2130354
    invoke-static {p0}, LX/EWZ;->a(LX/EWW;)LX/EZL;

    move-result-object v0

    .line 2130355
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, LX/EZL;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/EYr;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 2130356
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2130357
    move-object v0, v0

    .line 2130358
    throw v0

    .line 2130359
    :cond_0
    return-object p0
.end method

.method public static b(LX/EWZ;[BLX/EYZ;)LX/EWW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "LX/EYZ;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 2130334
    const/4 v0, 0x0

    array-length v1, p1

    .line 2130335
    invoke-static {p0, p1, v0, v1, p2}, LX/EWZ;->a(LX/EWZ;[BIILX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-static {v2}, LX/EWZ;->b(LX/EWW;)LX/EWW;

    move-result-object v2

    move-object v0, v2

    .line 2130336
    return-object v0
.end method

.method public static c(LX/EWZ;LX/EWc;LX/EYZ;)LX/EWW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWc;",
            "LX/EYZ;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 2130322
    :try_start_0
    invoke-virtual {p1}, LX/EWc;->h()LX/EWd;

    move-result-object v1

    .line 2130323
    invoke-virtual {p0, v1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2130324
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v2}, LX/EWd;->a(I)V
    :try_end_1
    .catch LX/EYr; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2130325
    return-object v0

    .line 2130326
    :catch_0
    move-exception v1

    .line 2130327
    :try_start_2
    iput-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2130328
    move-object v0, v1

    .line 2130329
    throw v0
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2130330
    :catch_1
    move-exception v0

    .line 2130331
    throw v0

    .line 2130332
    :catch_2
    move-exception v0

    .line 2130333
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static d(LX/EWZ;LX/EWc;LX/EYZ;)LX/EWW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWc;",
            "LX/EYZ;",
            ")TMessageType;"
        }
    .end annotation

    .prologue
    .line 2130321
    invoke-static {p0, p1, p2}, LX/EWZ;->c(LX/EWZ;LX/EWc;LX/EYZ;)LX/EWW;

    move-result-object v0

    invoke-static {v0}, LX/EWZ;->b(LX/EWW;)LX/EWW;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(LX/EWd;LX/EYZ;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWd;",
            "LX/EYZ;",
            ")TMessageType;"
        }
    .end annotation
.end method

.method public final a([B)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2130320
    sget-object v0, LX/EWZ;->a:LX/EYZ;

    invoke-static {p0, p1, v0}, LX/EWZ;->b(LX/EWZ;[BLX/EYZ;)LX/EWW;

    move-result-object v0

    return-object v0
.end method
