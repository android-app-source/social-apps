.class public final LX/ERt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/0lF;",
        "LX/ERu;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/vault/service/VaultQuotaClient$1;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/service/VaultQuotaClient$1;)V
    .locals 0

    .prologue
    .line 2121808
    iput-object p1, p0, LX/ERt;->a:Lcom/facebook/vault/service/VaultQuotaClient$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2121809
    check-cast p1, LX/0lF;

    .line 2121810
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    const-string v1, "vault_quota"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2121811
    new-instance v1, LX/ERu;

    const-string v2, "total"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->D()J

    move-result-wide v2

    const-string v4, "used"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, LX/ERu;-><init>(JJ)V

    return-object v1
.end method
