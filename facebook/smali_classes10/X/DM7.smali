.class public final enum LX/DM7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DM7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DM7;

.field public static final enum TYPE_CREATE_FLOW:LX/DM7;

.field public static final enum TYPE_EXISTING_GROUP:LX/DM7;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1989662
    new-instance v0, LX/DM7;

    const-string v1, "TYPE_EXISTING_GROUP"

    invoke-direct {v0, v1, v2}, LX/DM7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM7;->TYPE_EXISTING_GROUP:LX/DM7;

    .line 1989663
    new-instance v0, LX/DM7;

    const-string v1, "TYPE_CREATE_FLOW"

    invoke-direct {v0, v1, v3}, LX/DM7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DM7;->TYPE_CREATE_FLOW:LX/DM7;

    .line 1989664
    const/4 v0, 0x2

    new-array v0, v0, [LX/DM7;

    sget-object v1, LX/DM7;->TYPE_EXISTING_GROUP:LX/DM7;

    aput-object v1, v0, v2

    sget-object v1, LX/DM7;->TYPE_CREATE_FLOW:LX/DM7;

    aput-object v1, v0, v3

    sput-object v0, LX/DM7;->$VALUES:[LX/DM7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1989665
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DM7;
    .locals 1

    .prologue
    .line 1989666
    const-class v0, LX/DM7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DM7;

    return-object v0
.end method

.method public static values()[LX/DM7;
    .locals 1

    .prologue
    .line 1989667
    sget-object v0, LX/DM7;->$VALUES:[LX/DM7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DM7;

    return-object v0
.end method
