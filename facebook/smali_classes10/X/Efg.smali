.class public final LX/Efg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/Efh;

.field public b:LX/EfL;

.field private c:Z


# direct methods
.method public constructor <init>(LX/Efh;LX/EfL;Z)V
    .locals 0

    .prologue
    .line 2155043
    iput-object p1, p0, LX/Efg;->a:LX/Efh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2155044
    iput-object p2, p0, LX/Efg;->b:LX/EfL;

    .line 2155045
    iput-boolean p3, p0, LX/Efg;->c:Z

    .line 2155046
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2155047
    iget-boolean v0, p0, LX/Efg;->c:Z

    if-eqz v0, :cond_0

    .line 2155048
    :goto_0
    return-void

    .line 2155049
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Efg;->c:Z

    .line 2155050
    iget-object v0, p0, LX/Efg;->b:LX/EfL;

    iget-object v1, p0, LX/Efg;->a:LX/Efh;

    iget-object v1, v1, LX/Efh;->f:LX/AL7;

    .line 2155051
    iput-object v1, v0, LX/EfL;->B:LX/AL7;

    .line 2155052
    iget-object v0, p0, LX/Efg;->b:LX/EfL;

    iget-object v1, p0, LX/Efg;->a:LX/Efh;

    iget-object v1, v1, LX/Efh;->c:LX/EfA;

    .line 2155053
    iput-object v1, v0, LX/EfL;->y:LX/EfA;

    .line 2155054
    iget-object v0, p0, LX/Efg;->b:LX/EfL;

    iget-object v1, p0, LX/Efg;->a:LX/Efh;

    iget-object v1, v1, LX/Efh;->d:LX/AKu;

    invoke-virtual {v0, v1}, LX/EfL;->setReplyThreadGestureController(LX/AKu;)V

    .line 2155055
    iget-object v0, p0, LX/Efg;->b:LX/EfL;

    .line 2155056
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/EfL;->D:Z

    .line 2155057
    iget-object v1, v0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    if-nez v1, :cond_2

    .line 2155058
    invoke-virtual {v0}, LX/EfL;->e()V

    .line 2155059
    :goto_1
    iget-object v1, v0, LX/EfL;->B:LX/AL7;

    if-eqz v1, :cond_1

    .line 2155060
    iget-object v1, v0, LX/EfL;->B:LX/AL7;

    iget-object p0, v0, LX/EfL;->O:LX/AL6;

    .line 2155061
    iput-object p0, v1, LX/AL7;->g:LX/AL6;

    .line 2155062
    :cond_1
    goto :goto_0

    .line 2155063
    :cond_2
    invoke-static {v0}, LX/EfL;->f(LX/EfL;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2155064
    iget-boolean v0, p0, LX/Efg;->c:Z

    if-nez v0, :cond_0

    .line 2155065
    :goto_0
    return-void

    .line 2155066
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Efg;->c:Z

    .line 2155067
    iget-object v0, p0, LX/Efg;->b:LX/EfL;

    invoke-virtual {v0}, LX/EfL;->c()V

    .line 2155068
    iget-object v0, p0, LX/Efg;->b:LX/EfL;

    .line 2155069
    iput-object v1, v0, LX/EfL;->y:LX/EfA;

    .line 2155070
    iget-object v0, p0, LX/Efg;->b:LX/EfL;

    invoke-virtual {v0, v1}, LX/EfL;->setReplyThreadGestureController(LX/AKu;)V

    .line 2155071
    iget-object v0, p0, LX/Efg;->b:LX/EfL;

    .line 2155072
    iput-object v1, v0, LX/EfL;->B:LX/AL7;

    .line 2155073
    goto :goto_0
.end method
