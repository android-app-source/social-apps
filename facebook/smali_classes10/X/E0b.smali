.class public final LX/E0b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeEditActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/home/HomeEditActivity;)V
    .locals 0

    .prologue
    .line 2068748
    iput-object p1, p0, LX/E0b;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2068749
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2068750
    iget-object v0, p0, LX/E0b;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    invoke-static {v0}, Lcom/facebook/places/create/home/HomeEditActivity;->w(Lcom/facebook/places/create/home/HomeEditActivity;)V

    .line 2068751
    :goto_0
    return-void

    .line 2068752
    :cond_0
    iget-object v0, p0, LX/E0b;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->b(Lcom/facebook/places/create/home/HomeEditActivity;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2068753
    iget-object v0, p0, LX/E0b;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    invoke-static {v0, p1}, Lcom/facebook/places/create/home/HomeEditActivity;->b(Lcom/facebook/places/create/home/HomeEditActivity;Ljava/lang/Throwable;)V

    .line 2068754
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2068755
    check-cast p1, Ljava/lang/Boolean;

    invoke-direct {p0, p1}, LX/E0b;->a(Ljava/lang/Boolean;)V

    return-void
.end method
