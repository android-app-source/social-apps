.class public final enum LX/CiJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CiJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CiJ;

.field public static final enum NATIVE_ADS_VIDEO_SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiJ;

.field public static final enum NATIVE_ADS_VIDEO_SET_FOCUSED_VIEW:LX/CiJ;

.field public static final enum NATIVE_ADS_VIDEO_UNSET_FOCUSED_VIEW:LX/CiJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1928751
    new-instance v0, LX/CiJ;

    const-string v1, "NATIVE_ADS_VIDEO_SET_FOCUSED_VIEW"

    invoke-direct {v0, v1, v2}, LX/CiJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiJ;->NATIVE_ADS_VIDEO_SET_FOCUSED_VIEW:LX/CiJ;

    .line 1928752
    new-instance v0, LX/CiJ;

    const-string v1, "NATIVE_ADS_VIDEO_UNSET_FOCUSED_VIEW"

    invoke-direct {v0, v1, v3}, LX/CiJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiJ;->NATIVE_ADS_VIDEO_UNSET_FOCUSED_VIEW:LX/CiJ;

    .line 1928753
    new-instance v0, LX/CiJ;

    const-string v1, "NATIVE_ADS_VIDEO_SCROLL_FOCUSED_VIEW_TO_RECT"

    invoke-direct {v0, v1, v4}, LX/CiJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiJ;->NATIVE_ADS_VIDEO_SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiJ;

    .line 1928754
    const/4 v0, 0x3

    new-array v0, v0, [LX/CiJ;

    sget-object v1, LX/CiJ;->NATIVE_ADS_VIDEO_SET_FOCUSED_VIEW:LX/CiJ;

    aput-object v1, v0, v2

    sget-object v1, LX/CiJ;->NATIVE_ADS_VIDEO_UNSET_FOCUSED_VIEW:LX/CiJ;

    aput-object v1, v0, v3

    sget-object v1, LX/CiJ;->NATIVE_ADS_VIDEO_SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiJ;

    aput-object v1, v0, v4

    sput-object v0, LX/CiJ;->$VALUES:[LX/CiJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1928755
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CiJ;
    .locals 1

    .prologue
    .line 1928756
    const-class v0, LX/CiJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CiJ;

    return-object v0
.end method

.method public static values()[LX/CiJ;
    .locals 1

    .prologue
    .line 1928757
    sget-object v0, LX/CiJ;->$VALUES:[LX/CiJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CiJ;

    return-object v0
.end method
