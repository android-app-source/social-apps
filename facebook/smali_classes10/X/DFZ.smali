.class public LX/DFZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/23P;

.field private final b:LX/2ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2ew",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:LX/1vg;

.field public final d:LX/2et;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2et",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/23P;LX/2ew;LX/1vg;LX/2et;Landroid/content/res/Resources;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1978464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1978465
    iput-object p1, p0, LX/DFZ;->a:LX/23P;

    .line 1978466
    iput-object p2, p0, LX/DFZ;->b:LX/2ew;

    .line 1978467
    iput-object p3, p0, LX/DFZ;->c:LX/1vg;

    .line 1978468
    iput-object p4, p0, LX/DFZ;->d:LX/2et;

    .line 1978469
    iput-object p5, p0, LX/DFZ;->e:Landroid/content/res/Resources;

    .line 1978470
    return-void
.end method

.method public static a(LX/DFZ;LX/2f5;)I
    .locals 2

    .prologue
    .line 1978471
    iget-object v0, p1, LX/2f5;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DFZ;->e:Landroid/content/res/Resources;

    const v1, 0x7f0a09f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, LX/2f5;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/DFZ;
    .locals 9

    .prologue
    .line 1978472
    const-class v1, LX/DFZ;

    monitor-enter v1

    .line 1978473
    :try_start_0
    sget-object v0, LX/DFZ;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978474
    sput-object v2, LX/DFZ;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978475
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978476
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978477
    new-instance v3, LX/DFZ;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v4

    check-cast v4, LX/23P;

    invoke-static {v0}, LX/2ew;->a(LX/0QB;)LX/2ew;

    move-result-object v5

    check-cast v5, LX/2ew;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v6

    check-cast v6, LX/1vg;

    invoke-static {v0}, LX/2et;->a(LX/0QB;)LX/2et;

    move-result-object v7

    check-cast v7, LX/2et;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, LX/DFZ;-><init>(LX/23P;LX/2ew;LX/1vg;LX/2et;Landroid/content/res/Resources;)V

    .line 1978478
    move-object v0, v3

    .line 1978479
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978480
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978481
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978482
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;LX/3mj;LX/1Pc;Z)V
    .locals 6
    .param p1    # Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/1Fa;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/2ep;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/3mj;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1Pc;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            "LX/1Fa;",
            "LX/2ep;",
            "LX/3mj;",
            "TE;Z)V"
        }
    .end annotation

    .prologue
    .line 1978483
    if-eqz p6, :cond_0

    .line 1978484
    iget-object v0, p0, LX/DFZ;->b:LX/2ew;

    check-cast p5, LX/1Pq;

    invoke-virtual {v0, p1, p2, p5}, LX/2ew;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/1Pq;)V

    .line 1978485
    :goto_0
    return-void

    .line 1978486
    :cond_0
    iget-object v0, p0, LX/DFZ;->d:LX/2et;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/2et;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/2ep;LX/3mj;LX/1Pc;)V

    goto :goto_0
.end method
