.class public final LX/DDQ;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DDR;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/DDR;


# direct methods
.method public constructor <init>(LX/DDR;)V
    .locals 1

    .prologue
    .line 1975555
    iput-object p1, p0, LX/DDQ;->c:LX/DDR;

    .line 1975556
    move-object v0, p1

    .line 1975557
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1975558
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1975559
    const-string v0, "GroupsPeopleYouMayInviteHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1975560
    if-ne p0, p1, :cond_1

    .line 1975561
    :cond_0
    :goto_0
    return v0

    .line 1975562
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1975563
    goto :goto_0

    .line 1975564
    :cond_3
    check-cast p1, LX/DDQ;

    .line 1975565
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1975566
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1975567
    if-eq v2, v3, :cond_0

    .line 1975568
    iget-object v2, p0, LX/DDQ;->a:LX/1Po;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DDQ;->a:LX/1Po;

    iget-object v3, p1, LX/DDQ;->a:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1975569
    goto :goto_0

    .line 1975570
    :cond_5
    iget-object v2, p1, LX/DDQ;->a:LX/1Po;

    if-nez v2, :cond_4

    .line 1975571
    :cond_6
    iget-object v2, p0, LX/DDQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/DDQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DDQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1975572
    goto :goto_0

    .line 1975573
    :cond_7
    iget-object v2, p1, LX/DDQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
