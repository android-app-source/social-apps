.class public LX/Da7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/Da8;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Da7;


# instance fields
.field private final a:LX/5zm;


# direct methods
.method public constructor <init>(LX/5zm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2014547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2014548
    iput-object p1, p0, LX/Da7;->a:LX/5zm;

    .line 2014549
    return-void
.end method

.method private a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/4cQ;
    .locals 3

    .prologue
    .line 2014550
    iget-object v0, p0, LX/Da7;->a:LX/5zm;

    invoke-virtual {v0, p1}, LX/5zm;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zl;

    move-result-object v0

    .line 2014551
    if-nez v0, :cond_0

    .line 2014552
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Failed to attach image"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2014553
    :cond_0
    new-instance v1, LX/4cQ;

    const-string v2, "image"

    invoke-direct {v1, v2, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    return-object v1
.end method

.method public static a(LX/0QB;)LX/Da7;
    .locals 4

    .prologue
    .line 2014554
    sget-object v0, LX/Da7;->b:LX/Da7;

    if-nez v0, :cond_1

    .line 2014555
    const-class v1, LX/Da7;

    monitor-enter v1

    .line 2014556
    :try_start_0
    sget-object v0, LX/Da7;->b:LX/Da7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2014557
    if-eqz v2, :cond_0

    .line 2014558
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2014559
    new-instance p0, LX/Da7;

    invoke-static {v0}, LX/5zm;->a(LX/0QB;)LX/5zm;

    move-result-object v3

    check-cast v3, LX/5zm;

    invoke-direct {p0, v3}, LX/Da7;-><init>(LX/5zm;)V

    .line 2014560
    move-object v0, p0

    .line 2014561
    sput-object v0, LX/Da7;->b:LX/Da7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2014562
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2014563
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2014564
    :cond_1
    sget-object v0, LX/Da7;->b:LX/Da7;

    return-object v0

    .line 2014565
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2014566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2014567
    check-cast p1, LX/Da8;

    .line 2014568
    iget-object v0, p1, LX/Da8;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 2014569
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2014570
    iget-object v0, p1, LX/Da8;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2014571
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2014572
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2014573
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2014574
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tid"

    .line 2014575
    iget-object v3, p1, LX/Da8;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2014576
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2014577
    iget-object v1, p1, LX/Da8;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v1, v1

    .line 2014578
    invoke-direct {p0, v1}, LX/Da7;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/4cQ;

    move-result-object v1

    .line 2014579
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "setImageForideConversation"

    .line 2014580
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2014581
    move-object v2, v2

    .line 2014582
    const-string v3, "POST"

    .line 2014583
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2014584
    move-object v2, v2

    .line 2014585
    const-string v3, "method/messaging.setthreadimage"

    .line 2014586
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 2014587
    move-object v2, v2

    .line 2014588
    iput-object v0, v2, LX/14O;->g:Ljava/util/List;

    .line 2014589
    move-object v0, v2

    .line 2014590
    sget-object v2, LX/14S;->STRING:LX/14S;

    .line 2014591
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 2014592
    move-object v0, v0

    .line 2014593
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2014594
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 2014595
    move-object v0, v0

    .line 2014596
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2014597
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2014598
    const/4 v0, 0x0

    return-object v0
.end method
