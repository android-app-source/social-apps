.class public LX/Eco;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2148023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)B
    .locals 1

    .prologue
    .line 2148022
    shl-int/lit8 v0, p0, 0x4

    or-int/2addr v0, p1

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    return v0
.end method

.method public static a(B)I
    .locals 1

    .prologue
    .line 2148024
    and-int/lit16 v0, p0, 0xff

    shr-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public static varargs a([[B)[B
    .locals 4

    .prologue
    .line 2148015
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2148016
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 2148017
    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2148018
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2148019
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2148020
    :catch_0
    move-exception v0

    .line 2148021
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static a([BII)[[B
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2148009
    const/4 v0, 0x2

    new-array v0, v0, [[B

    .line 2148010
    new-array v1, p1, [B

    aput-object v1, v0, v2

    .line 2148011
    aget-object v1, v0, v2

    invoke-static {p0, v2, v1, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2148012
    new-array v1, p2, [B

    aput-object v1, v0, v3

    .line 2148013
    aget-object v1, v0, v3

    invoke-static {p0, p1, v1, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2148014
    return-object v0
.end method

.method public static a([BIII)[[B
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2147987
    if-eqz p0, :cond_0

    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    array-length v0, p0

    add-int v1, p1, p2

    add-int/2addr v1, p3

    if-ge v0, v1, :cond_3

    .line 2147988
    :cond_0
    new-instance v1, Ljava/text/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Input too small: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v3}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 2147989
    :cond_1
    const/4 v0, 0x0

    array-length v4, p0

    .line 2147990
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 2147991
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_2

    .line 2147992
    add-int p1, v0, v5

    aget-byte p1, p0, p1

    .line 2147993
    const-string p2, "(byte)0x"

    invoke-virtual {v6, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2147994
    sget-object p2, LX/Ecp;->a:[C

    shr-int/lit8 p3, p1, 0x4

    and-int/lit8 p3, p3, 0xf

    aget-char p2, p2, p3

    invoke-virtual {v6, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2147995
    sget-object p2, LX/Ecp;->a:[C

    and-int/lit8 p3, p1, 0xf

    aget-char p2, p2, p3

    invoke-virtual {v6, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2147996
    const-string p1, ", "

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2147997
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 2147998
    :cond_2
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    .line 2147999
    move-object v0, v0

    .line 2148000
    goto :goto_0

    .line 2148001
    :cond_3
    const/4 v0, 0x3

    new-array v0, v0, [[B

    .line 2148002
    new-array v1, p1, [B

    aput-object v1, v0, v3

    .line 2148003
    aget-object v1, v0, v3

    invoke-static {p0, v3, v1, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2148004
    new-array v1, p2, [B

    aput-object v1, v0, v2

    .line 2148005
    aget-object v1, v0, v2

    invoke-static {p0, p1, v1, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2148006
    new-array v1, p3, [B

    aput-object v1, v0, v4

    .line 2148007
    add-int v1, p1, p2

    aget-object v2, v0, v4

    invoke-static {p0, v1, v2, v3, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2148008
    return-object v0
.end method
