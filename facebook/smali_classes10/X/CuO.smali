.class public LX/CuO;
.super Lcom/facebook/video/player/plugins/Video360Plugin;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1946608
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/CuO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1946609
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1946610
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/CuO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946611
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1946612
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946613
    return-void
.end method

.method private setRollRotationEnabled(Z)V
    .locals 2

    .prologue
    .line 1946614
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->g()LX/2qW;

    move-result-object v0

    .line 1946615
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/7EB;

    if-eqz v1, :cond_0

    .line 1946616
    check-cast v0, LX/7EB;

    .line 1946617
    if-eqz p1, :cond_1

    sget-object v1, LX/7Cl;->FULL_SCREEN_CONTROLLER:LX/7Cl;

    .line 1946618
    :goto_0
    invoke-virtual {v0}, LX/2qW;->getRenderThreadParams()LX/7Cz;

    move-result-object p0

    iput-object v1, p0, LX/7Cz;->c:LX/7Cl;

    .line 1946619
    :cond_0
    return-void

    .line 1946620
    :cond_1
    sget-object v1, LX/7Cl;->VIDEO:LX/7Cl;

    goto :goto_0
.end method


# virtual methods
.method public final d()V
    .locals 1

    .prologue
    .line 1946621
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/CuO;->setRollRotationEnabled(Z)V

    .line 1946622
    invoke-super {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->d()V

    .line 1946623
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1946624
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/CuO;->setRollRotationEnabled(Z)V

    .line 1946625
    invoke-super {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->k()V

    .line 1946626
    return-void
.end method
