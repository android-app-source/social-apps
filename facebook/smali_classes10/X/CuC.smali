.class public LX/CuC;
.super LX/Cts;
.source ""

# interfaces
.implements LX/CrZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/facebook/richdocument/view/widget/media/RotatableViewAware;"
    }
.end annotation


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/Cqw;


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 1

    .prologue
    .line 1946222
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1946223
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/CuC;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v0

    check-cast v0, LX/Chv;

    iput-object v0, p0, LX/CuC;->a:LX/Chv;

    .line 1946224
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 1946193
    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v0

    .line 1946194
    iget-object v1, v0, LX/Cqw;->f:LX/Cqt;

    move-object v1, v1

    .line 1946195
    sget-object v2, LX/Cqt;->PORTRAIT:LX/Cqt;

    if-ne v1, v2, :cond_0

    .line 1946196
    iput-object v0, p0, LX/CuC;->b:LX/Cqw;

    .line 1946197
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 2

    .prologue
    .line 1946217
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946218
    invoke-interface {v0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0}, LX/Cqj;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1946219
    invoke-virtual {p0}, LX/Cts;->h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    .line 1946220
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->getMeasuredHeight()I

    move-result v1

    if-lez v1, :cond_0

    .line 1946221
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/CrY;)V
    .locals 3

    .prologue
    .line 1946202
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v1, v0

    .line 1946203
    sget-object v0, LX/CuB;->a:[I

    invoke-virtual {p1}, LX/CrY;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1946204
    :goto_0
    return-void

    .line 1946205
    :pswitch_0
    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v0

    .line 1946206
    iget-object v2, v0, LX/Cqw;->f:LX/Cqt;

    move-object v0, v2

    .line 1946207
    invoke-virtual {v0}, LX/Cqt;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1946208
    iget-object v0, p0, LX/CuC;->b:LX/Cqw;

    if-eqz v0, :cond_1

    .line 1946209
    iget-object v0, p0, LX/CuC;->b:LX/Cqw;

    invoke-interface {v1, v0}, LX/Ctg;->a(LX/Cqw;)V

    .line 1946210
    :cond_0
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/CuC;->b:LX/Cqw;

    goto :goto_0

    .line 1946211
    :cond_1
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946212
    invoke-interface {v0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0}, LX/CqX;->d()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    invoke-interface {v1, v0}, LX/Ctg;->a(LX/Cqw;)V

    goto :goto_1

    .line 1946213
    :pswitch_1
    invoke-direct {p0}, LX/CuC;->k()V

    .line 1946214
    sget-object v0, LX/Cqw;->c:LX/Cqw;

    invoke-interface {v1, v0}, LX/Ctg;->a(LX/Cqw;)V

    goto :goto_0

    .line 1946215
    :pswitch_2
    invoke-direct {p0}, LX/CuC;->k()V

    .line 1946216
    sget-object v0, LX/Cqw;->d:LX/Cqw;

    invoke-interface {v1, v0}, LX/Ctg;->a(LX/Cqw;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1946200
    iget-object v0, p0, LX/CuC;->a:LX/Chv;

    new-instance v1, LX/Cib;

    sget-object v2, LX/Cia;->REGISTER:LX/Cia;

    invoke-direct {v1, p0, v2}, LX/Cib;-><init>(LX/CuC;LX/Cia;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1946201
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1946198
    iget-object v0, p0, LX/CuC;->a:LX/Chv;

    new-instance v1, LX/Cib;

    sget-object v2, LX/Cia;->UNREGISTER:LX/Cia;

    invoke-direct {v1, p0, v2}, LX/Cib;-><init>(LX/CuC;LX/Cia;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1946199
    return-void
.end method
