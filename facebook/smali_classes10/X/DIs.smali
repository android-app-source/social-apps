.class public final LX/DIs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:LX/DIv;


# direct methods
.method public constructor <init>(LX/DIv;)V
    .locals 0

    .prologue
    .line 1984505
    iput-object p1, p0, LX/DIs;->a:LX/DIv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1984486
    iget-object v0, p0, LX/DIs;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->i:LX/03V;

    const-string v1, "ComposerSellController"

    const-string v2, "Couldn\'t complete PreferredMarketplaceQuery."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1984487
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1984488
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1984489
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1984490
    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;

    .line 1984491
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_1

    .line 1984492
    :cond_0
    :goto_0
    return-void

    .line 1984493
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x4f0e8639

    invoke-static {v1, v0, v4, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1984494
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 1984495
    :goto_1
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1984496
    const-class v3, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;

    invoke-virtual {v2, v0, v4, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;

    .line 1984497
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1984498
    iget-object v1, p0, LX/DIs;->a:LX/DIv;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1984499
    iput-wide v2, v1, LX/DIv;->l:J

    .line 1984500
    iget-object v0, p0, LX/DIs;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJT;

    iget-object v1, p0, LX/DIs;->a:LX/DIv;

    iget-wide v2, v1, LX/DIv;->l:J

    .line 1984501
    iget-object v1, v0, LX/DJT;->a:LX/DJc;

    iget-object v1, v1, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getShouldPostToMarketplace()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1984502
    iget-object v1, v0, LX/DJT;->a:LX/DJc;

    invoke-static {v1, v2, v3}, LX/DJc;->a$redex0(LX/DJc;J)V

    .line 1984503
    :cond_3
    goto :goto_0

    .line 1984504
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1
.end method
