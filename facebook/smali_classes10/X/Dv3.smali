.class public final enum LX/Dv3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dv3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dv3;

.field public static final enum ALBUM_POST_SECTION:LX/Dv3;

.field public static final enum MULTI_PHOTO_STORY:LX/Dv3;

.field public static final enum SINGLE_MEDIA:LX/Dv3;

.field public static final enum SINGLE_MEDIA_STORY:LX/Dv3;

.field public static final enum SINGLE_PHOTO:LX/Dv3;

.field public static final enum SINGLE_PHOTO_STORY:LX/Dv3;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2058015
    new-instance v0, LX/Dv3;

    const-string v1, "SINGLE_PHOTO_STORY"

    invoke-direct {v0, v1, v3}, LX/Dv3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dv3;->SINGLE_PHOTO_STORY:LX/Dv3;

    .line 2058016
    new-instance v0, LX/Dv3;

    const-string v1, "SINGLE_MEDIA_STORY"

    invoke-direct {v0, v1, v4}, LX/Dv3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dv3;->SINGLE_MEDIA_STORY:LX/Dv3;

    .line 2058017
    new-instance v0, LX/Dv3;

    const-string v1, "MULTI_PHOTO_STORY"

    invoke-direct {v0, v1, v5}, LX/Dv3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dv3;->MULTI_PHOTO_STORY:LX/Dv3;

    .line 2058018
    new-instance v0, LX/Dv3;

    const-string v1, "ALBUM_POST_SECTION"

    invoke-direct {v0, v1, v6}, LX/Dv3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dv3;->ALBUM_POST_SECTION:LX/Dv3;

    .line 2058019
    new-instance v0, LX/Dv3;

    const-string v1, "SINGLE_PHOTO"

    invoke-direct {v0, v1, v7}, LX/Dv3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dv3;->SINGLE_PHOTO:LX/Dv3;

    .line 2058020
    new-instance v0, LX/Dv3;

    const-string v1, "SINGLE_MEDIA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Dv3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dv3;->SINGLE_MEDIA:LX/Dv3;

    .line 2058021
    const/4 v0, 0x6

    new-array v0, v0, [LX/Dv3;

    sget-object v1, LX/Dv3;->SINGLE_PHOTO_STORY:LX/Dv3;

    aput-object v1, v0, v3

    sget-object v1, LX/Dv3;->SINGLE_MEDIA_STORY:LX/Dv3;

    aput-object v1, v0, v4

    sget-object v1, LX/Dv3;->MULTI_PHOTO_STORY:LX/Dv3;

    aput-object v1, v0, v5

    sget-object v1, LX/Dv3;->ALBUM_POST_SECTION:LX/Dv3;

    aput-object v1, v0, v6

    sget-object v1, LX/Dv3;->SINGLE_PHOTO:LX/Dv3;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Dv3;->SINGLE_MEDIA:LX/Dv3;

    aput-object v2, v0, v1

    sput-object v0, LX/Dv3;->$VALUES:[LX/Dv3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2058022
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dv3;
    .locals 1

    .prologue
    .line 2058023
    const-class v0, LX/Dv3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dv3;

    return-object v0
.end method

.method public static values()[LX/Dv3;
    .locals 1

    .prologue
    .line 2058024
    sget-object v0, LX/Dv3;->$VALUES:[LX/Dv3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dv3;

    return-object v0
.end method
