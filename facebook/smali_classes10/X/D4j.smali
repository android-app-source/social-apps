.class public LX/D4j;
.super LX/1Wg;
.source ""


# direct methods
.method public constructor <init>(LX/1V7;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 1962534
    invoke-direct {p0, p1, p2}, LX/1Wg;-><init>(LX/1V7;Landroid/content/res/Resources;)V

    .line 1962535
    return-void
.end method


# virtual methods
.method public final c()LX/1Wh;
    .locals 5

    .prologue
    .line 1962536
    new-instance v0, LX/1Wh;

    .line 1962537
    const/4 v1, 0x0

    move v1, v1

    .line 1962538
    iget-object v2, p0, LX/1Wg;->c:Landroid/content/res/Resources;

    move-object v2, v2

    .line 1962539
    const v3, 0x7f0b1101

    invoke-static {v2, v3}, LX/0tP;->b(Landroid/content/res/Resources;I)I

    move-result v2

    int-to-float v2, v2

    .line 1962540
    iget-object v3, p0, LX/1Wg;->c:Landroid/content/res/Resources;

    move-object v3, v3

    .line 1962541
    const v4, 0x7f0b1102

    invoke-static {v3, v4}, LX/0tP;->b(Landroid/content/res/Resources;I)I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v0, v1, v2, v3}, LX/1Wh;-><init>(FFF)V

    return-object v0
.end method

.method public d()Ljava/util/EnumMap;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap",
            "<",
            "LX/1Wi;",
            "LX/1Wj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1962542
    new-instance v9, Ljava/util/EnumMap;

    const-class v0, LX/1Wi;

    invoke-direct {v9, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 1962543
    sget-object v10, LX/1Wi;->TOP:LX/1Wi;

    new-instance v0, LX/1Wj;

    const v1, 0x7f020227

    invoke-virtual {p0}, LX/1Wg;->c()LX/1Wh;

    move-result-object v3

    sget-object v6, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    sget-object v7, LX/1Wl;->HIDDEN:LX/1Wl;

    sget-object v8, LX/1Wl;->HIDDEN:LX/1Wl;

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v8}, LX/1Wj;-><init>(IILX/1Wh;IILX/1Wk;LX/1Wl;LX/1Wl;)V

    invoke-virtual {v9, v10, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1962544
    return-object v9
.end method
