.class public final LX/Dbm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V
    .locals 0

    .prologue
    .line 2017554
    iput-object p1, p0, LX/Dbm;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x299fd97a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017555
    iget-object v1, p0, LX/Dbm;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    iget-object v1, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->c:LX/DbM;

    iget-object v2, p0, LX/Dbm;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    iget-wide v2, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 2017556
    const-string v3, "upload_photo_menu_add_photos_button_click"

    invoke-static {v1, v3, v2}, LX/DbM;->d(LX/DbM;Ljava/lang/String;Ljava/lang/String;)V

    .line 2017557
    iget-object v1, p0, LX/Dbm;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    .line 2017558
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, LX/8AA;

    sget-object v5, LX/8AB;->PHOTO_MENU_UPLOAD:LX/8AB;

    invoke-direct {v3, v5}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v3}, LX/8AA;->l()LX/8AA;

    move-result-object v3

    invoke-virtual {v3}, LX/8AA;->j()LX/8AA;

    move-result-object v3

    sget-object v5, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v3, v5}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v3

    .line 2017559
    iget-object v5, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->e:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x6592

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class p1, Landroid/app/Activity;

    invoke-static {v2, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-interface {v5, v3, p0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2017560
    const v1, 0x462a04d1

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
