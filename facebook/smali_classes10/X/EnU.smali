.class public LX/EnU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/EnU;


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/ParcelUuid;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2167193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167194
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/EnU;->a:Ljava/util/HashMap;

    .line 2167195
    return-void
.end method

.method public static a(LX/0QB;)LX/EnU;
    .locals 3

    .prologue
    .line 2167196
    sget-object v0, LX/EnU;->b:LX/EnU;

    if-nez v0, :cond_1

    .line 2167197
    const-class v1, LX/EnU;

    monitor-enter v1

    .line 2167198
    :try_start_0
    sget-object v0, LX/EnU;->b:LX/EnU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2167199
    if-eqz v2, :cond_0

    .line 2167200
    :try_start_1
    new-instance v0, LX/EnU;

    invoke-direct {v0}, LX/EnU;-><init>()V

    .line 2167201
    move-object v0, v0

    .line 2167202
    sput-object v0, LX/EnU;->b:LX/EnU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2167203
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2167204
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2167205
    :cond_1
    sget-object v0, LX/EnU;->b:LX/EnU;

    return-object v0

    .line 2167206
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2167207
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/os/ParcelUuid;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2167208
    iget-object v0, p0, LX/EnU;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167209
    return-void
.end method
