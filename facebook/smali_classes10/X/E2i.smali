.class public LX/E2i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/5up;


# direct methods
.method public constructor <init>(LX/5up;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2072865
    iput-object p1, p0, LX/E2i;->a:LX/5up;

    .line 2072866
    return-void
.end method

.method public static a(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2072867
    if-eqz p1, :cond_0

    const v0, 0x7f08178a

    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f081788

    goto :goto_0
.end method

.method public static a(LX/1Pr;ZLX/5sc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 2

    .prologue
    .line 2072868
    new-instance v0, LX/E2g;

    invoke-direct {v0, p2}, LX/E2g;-><init>(LX/5sc;)V

    invoke-interface {p0, v0, p3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2h;

    .line 2072869
    if-eqz p1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2072870
    :goto_0
    iput-object v1, v0, LX/E2h;->a:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2072871
    return-void

    .line 2072872
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_0
.end method

.method public static a(LX/1Pr;LX/5sc;LX/E2g;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    .line 2072873
    invoke-interface {p0, p2, p3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2h;

    .line 2072874
    iget-object v1, v0, LX/E2h;->a:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object v1, v1

    .line 2072875
    if-nez v1, :cond_0

    invoke-interface {p1}, LX/5sc;->n()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    .line 2072876
    :goto_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2072877
    :cond_0
    iget-object v1, v0, LX/E2h;->a:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object v0, v1

    .line 2072878
    goto :goto_0

    .line 2072879
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/5sc;)Z
    .locals 2
    .param p0    # LX/5sc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2072880
    if-eqz p0, :cond_1

    invoke-interface {p0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p0}, LX/5sc;->n()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v1, :cond_0

    invoke-interface {p0}, LX/5sc;->n()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v1, :cond_0

    invoke-interface {p0}, LX/5sc;->n()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/E2i;
    .locals 2

    .prologue
    .line 2072881
    new-instance v1, LX/E2i;

    invoke-static {p0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v0

    check-cast v0, LX/5up;

    invoke-direct {v1, v0}, LX/E2i;-><init>(LX/5up;)V

    .line 2072882
    return-object v1
.end method


# virtual methods
.method public final a(ZLX/5sc;LX/2h0;)V
    .locals 6

    .prologue
    .line 2072883
    iget-object v0, p0, LX/E2i;->a:LX/5up;

    if-eqz p1, :cond_0

    sget-object v1, LX/5uo;->SAVE:LX/5uo;

    :goto_0
    invoke-interface {p2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    const-string v3, "after_party"

    const-string v4, "toggle_button"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/5up;->a(LX/5uo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2072884
    return-void

    .line 2072885
    :cond_0
    sget-object v1, LX/5uo;->UNSAVE:LX/5uo;

    goto :goto_0
.end method
