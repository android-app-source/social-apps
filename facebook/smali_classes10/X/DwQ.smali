.class public final LX/DwQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final synthetic b:Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 0

    .prologue
    .line 2060358
    iput-object p1, p0, LX/DwQ;->b:Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    iput-object p2, p0, LX/DwQ;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2060359
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2060360
    check-cast p1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2060361
    if-eqz p1, :cond_2

    .line 2060362
    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2060363
    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 2060364
    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2060365
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    if-eqz v1, :cond_0

    .line 2060366
    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediaCountQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    iget-object v3, p0, LX/DwQ;->b:Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v0

    .line 2060367
    iput v0, v3, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->c:I

    .line 2060368
    iget-object v0, p0, LX/DwQ;->b:Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    iget-object v1, p0, LX/DwQ;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0, v1}, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->c(Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 2060369
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 2060370
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_1
.end method
