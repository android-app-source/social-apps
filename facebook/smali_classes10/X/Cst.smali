.class public final LX/Cst;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:LX/CsV;

.field private final b:Landroid/webkit/WebViewClient;


# direct methods
.method public constructor <init>(LX/CsV;Landroid/webkit/WebViewClient;)V
    .locals 0

    .prologue
    .line 1943552
    iput-object p1, p0, LX/Cst;->a:LX/CsV;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 1943553
    iput-object p2, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    .line 1943554
    return-void
.end method


# virtual methods
.method public final doUpdateVisitedHistory(Landroid/webkit/WebView;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1943555
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943556
    :goto_0
    return-void

    .line 1943557
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/WebViewClient;->doUpdateVisitedHistory(Landroid/webkit/WebView;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final onFormResubmission(Landroid/webkit/WebView;Landroid/os/Message;Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 1943558
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943559
    :goto_0
    return-void

    .line 1943560
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onFormResubmission(Landroid/webkit/WebView;Landroid/os/Message;Landroid/os/Message;)V

    goto :goto_0
.end method

.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1943573
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943574
    :goto_0
    return-void

    .line 1943575
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1943561
    iget-object v0, p0, LX/Cst;->a:LX/CsV;

    .line 1943562
    iget-object v1, v0, LX/CsV;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Csq;

    .line 1943563
    invoke-interface {v1}, LX/Csq;->a()V

    goto :goto_0

    .line 1943564
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_1

    .line 1943565
    :goto_1
    return-void

    .line 1943566
    :cond_1
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1943567
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943568
    :goto_0
    return-void

    .line 1943569
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public final onReceivedClientCertRequest(Landroid/webkit/WebView;Landroid/webkit/ClientCertRequest;)V
    .locals 1

    .prologue
    .line 1943570
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943571
    :goto_0
    return-void

    .line 1943572
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClient;->onReceivedClientCertRequest(Landroid/webkit/WebView;Landroid/webkit/ClientCertRequest;)V

    goto :goto_0
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1943534
    iget-object v0, p0, LX/Cst;->a:LX/CsV;

    .line 1943535
    iget-object v1, v0, LX/CsV;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Csq;

    .line 1943536
    invoke-interface {v1}, LX/Csq;->b()V

    goto :goto_0

    .line 1943537
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_1

    .line 1943538
    :goto_1
    return-void

    .line 1943539
    :cond_1
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1943546
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943547
    :goto_0
    return-void

    .line 1943548
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1943549
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943550
    :goto_0
    return-void

    .line 1943551
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 3

    .prologue
    .line 1943540
    iget-object v0, p0, LX/Cst;->a:LX/CsV;

    .line 1943541
    iget-object v1, v0, LX/CsV;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Csq;

    .line 1943542
    invoke-interface {v1}, LX/Csq;->c()V

    goto :goto_0

    .line 1943543
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_1

    .line 1943544
    :goto_1
    return-void

    .line 1943545
    :cond_1
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    goto :goto_1
.end method

.method public final onScaleChanged(Landroid/webkit/WebView;FF)V
    .locals 1

    .prologue
    .line 1943531
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943532
    :goto_0
    return-void

    .line 1943533
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onScaleChanged(Landroid/webkit/WebView;FF)V

    goto :goto_0
.end method

.method public final onTooManyRedirects(Landroid/webkit/WebView;Landroid/os/Message;Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 1943528
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943529
    :goto_0
    return-void

    .line 1943530
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onTooManyRedirects(Landroid/webkit/WebView;Landroid/os/Message;Landroid/os/Message;)V

    goto :goto_0
.end method

.method public final onUnhandledKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)V
    .locals 1

    .prologue
    .line 1943525
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943526
    :goto_0
    return-void

    .line 1943527
    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClient;->onUnhandledKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)V

    goto :goto_0
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;
    .locals 1

    .prologue
    .line 1943522
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943523
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    .line 1943524
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 1

    .prologue
    .line 1943519
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943520
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    .line 1943521
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final shouldOverrideKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1943516
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943517
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)Z

    move-result v0

    .line 1943518
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideKeyEvent(Landroid/webkit/WebView;Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1943513
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 1943514
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    .line 1943515
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Cst;->b:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
