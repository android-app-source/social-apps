.class public LX/D38;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CSJ;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/AEo;

.field private final c:LX/0Uh;

.field private final d:LX/2nC;


# direct methods
.method public constructor <init>(LX/AEo;LX/0Or;LX/0Uh;LX/2nC;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/saved/gating/annotations/IsSavedOfflineToastEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AEo;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2nC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1959716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1959717
    iput-object p1, p0, LX/D38;->b:LX/AEo;

    .line 1959718
    iput-object p2, p0, LX/D38;->a:LX/0Or;

    .line 1959719
    iput-object p3, p0, LX/D38;->c:LX/0Uh;

    .line 1959720
    iput-object p4, p0, LX/D38;->d:LX/2nC;

    .line 1959721
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1959722
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/D38;->c:LX/0Uh;

    const/16 v2, 0x1e

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1959723
    :cond_0
    :goto_0
    return v0

    .line 1959724
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 1959725
    const-class v1, Lcom/facebook/browser/lite/BrowserLiteActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1959726
    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 1959727
    iget-object v0, p0, LX/D38;->d:LX/2nC;

    invoke-virtual {v0}, LX/2nC;->a()V

    .line 1959728
    const/4 v0, 0x1

    return v0
.end method

.method public final c(Landroid/content/Intent;)LX/AEn;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1959729
    iget-object v0, p0, LX/D38;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "parent_story_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1959730
    :cond_0
    :goto_0
    return-object v1

    .line 1959731
    :cond_1
    const-string v0, "parent_story_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1959732
    iget-object v0, p0, LX/D38;->b:LX/AEo;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v2, "tracking_codes"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v2, v1

    move-object v4, v1

    invoke-virtual/range {v0 .. v5}, LX/AEo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;)Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;

    move-result-object v1

    goto :goto_0

    .line 1959733
    :cond_2
    iget-object v2, p0, LX/D38;->b:LX/AEo;

    const-string v0, "parent_story_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "tracking_codes"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-virtual/range {v2 .. v7}, LX/AEo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;)Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;

    move-result-object v1

    goto :goto_0
.end method
