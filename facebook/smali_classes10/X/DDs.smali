.class public final LX/DDs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/34w;


# direct methods
.method public constructor <init>(LX/34w;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1976062
    iput-object p1, p0, LX/DDs;->d:LX/34w;

    iput-object p2, p0, LX/DDs;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DDs;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p4, p0, LX/DDs;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1976063
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.INSERT"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1976064
    const-string v0, "vnd.android.cursor.dir/raw_contact"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1976065
    const-string v0, "phone"

    iget-object v2, p0, LX/DDs;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "phone_type"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1976066
    iget-object v0, p0, LX/DDs;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    .line 1976067
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1976068
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1976069
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1976070
    if-eqz v0, :cond_0

    .line 1976071
    const-string v2, "name"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1976072
    :cond_0
    iget-object v0, p0, LX/DDs;->d:LX/34w;

    const-string v2, "comcom_create_contact_click"

    iget-object v3, p0, LX/DDs;->a:Ljava/lang/String;

    iget-object v4, p0, LX/DDs;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v2, v3, v4}, LX/34w;->a$redex0(LX/34w;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1976073
    :try_start_0
    iget-object v0, p0, LX/DDs;->d:LX/34w;

    iget-object v0, v0, LX/34w;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DDs;->c:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1976074
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1976075
    :catch_0
    move-exception v0

    .line 1976076
    iget-object v1, p0, LX/DDs;->c:Landroid/content/Context;

    const v2, 0x7f08251d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1976077
    iget-object v2, p0, LX/DDs;->c:Landroid/content/Context;

    invoke-static {v2, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1976078
    iget-object v1, p0, LX/DDs;->d:LX/34w;

    iget-object v1, v1, LX/34w;->d:LX/03V;

    sget-object v2, LX/34w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
