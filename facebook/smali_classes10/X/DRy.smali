.class public final LX/DRy;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;",
        ">;"
    }
.end annotation


# instance fields
.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/0tX;LX/B1b;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1999080
    invoke-direct {p0, p1, p5, p6}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;)V

    .line 1999081
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1999082
    iput-object v0, p0, LX/DRy;->e:LX/0Px;

    .line 1999083
    iput-object p2, p0, LX/DRy;->g:Ljava/lang/String;

    .line 1999084
    iput-object p4, p0, LX/DRy;->f:Ljava/lang/Integer;

    .line 1999085
    iput-object p3, p0, LX/DRy;->h:Ljava/lang/String;

    .line 1999086
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1999044
    new-instance v0, LX/DUG;

    invoke-direct {v0}, LX/DUG;-><init>()V

    move-object v0, v0

    .line 1999045
    const-string v1, "end_cursor"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "group_id"

    iget-object v3, p0, LX/DRy;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_size"

    iget-object v3, p0, LX/DRy;->f:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "member_count"

    const-string v3, "12"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1999046
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1999047
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1999048
    iget-object v0, p0, LX/DRy;->e:LX/0Px;

    invoke-virtual {v6, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1999049
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v1, v0

    .line 1999050
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999051
    if-eqz v0, :cond_0

    .line 1999052
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999053
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1999054
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999055
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1999056
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999057
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel;->a()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 1999058
    :cond_0
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1999059
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    move v5, v2

    :goto_0
    if-ge v5, v7, :cond_2

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel$EdgesModel;

    .line 1999060
    if-eqz v0, :cond_1

    .line 1999061
    new-instance v8, LX/DSf;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel$EdgesModel;->a()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v0

    sget-object v9, LX/DSb;->NOT_ADMIN:LX/DSb;

    sget-object v10, LX/DSc;->BLOCKED:LX/DSc;

    invoke-direct {v8, v0, v9, v10, v3}, LX/DSf;-><init>(LX/DUV;LX/DSb;LX/DSc;LX/DS2;)V

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1999062
    :cond_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 1999063
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999064
    if-eqz v0, :cond_5

    .line 1999065
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999066
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1999067
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999068
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$GroupBlockedProfilesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1999069
    :goto_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iput-object v5, p0, LX/DRy;->e:LX/0Px;

    .line 1999070
    if-eqz v0, :cond_3

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iput-object v3, p0, LX/DRy;->a:Ljava/lang/String;

    .line 1999071
    if-eqz v0, :cond_4

    const/4 v3, 0x2

    invoke-virtual {v1, v0, v3}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_2
    iput-boolean v2, p0, LX/DRy;->b:Z

    .line 1999072
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 1999073
    return-void

    :cond_4
    move v2, v4

    .line 1999074
    goto :goto_2

    :cond_5
    move v0, v2

    move-object v1, v3

    goto :goto_1
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1999075
    const-string v0, "Group admin members fetch failed"

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1999076
    iget-object v0, p0, LX/DRy;->e:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1999077
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1999078
    iput-object v0, p0, LX/DRy;->e:LX/0Px;

    .line 1999079
    return-void
.end method
