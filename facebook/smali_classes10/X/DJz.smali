.class public final LX/DJz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

.field public final synthetic b:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;)V
    .locals 0

    .prologue
    .line 1986224
    iput-object p1, p0, LX/DJz;->b:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    iput-object p2, p0, LX/DJz;->a:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x1aa2737a

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1986225
    iget-object v0, p0, LX/DJz;->b:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 1986226
    new-instance v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;

    invoke-direct {v2}, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;-><init>()V

    .line 1986227
    iget-object v3, p0, LX/DJz;->b:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    iget-object v3, v3, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->f:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v4, p0, LX/DJz;->a:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->isChecked()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;Z)V

    .line 1986228
    new-instance v3, LX/DJx;

    invoke-direct {v3, p0}, LX/DJx;-><init>(LX/DJz;)V

    .line 1986229
    iput-object v3, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->m:Landroid/view/View$OnClickListener;

    .line 1986230
    new-instance v3, LX/DJy;

    invoke-direct {v3, p0}, LX/DJy;-><init>(LX/DJz;)V

    .line 1986231
    iput-object v3, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->n:Landroid/view/View$OnClickListener;

    .line 1986232
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v3, "FOR_SALE_POST_TO_MARKETPLACE_INFO"

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1986233
    const v0, 0x28e95724

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
