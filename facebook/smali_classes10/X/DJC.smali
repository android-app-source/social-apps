.class public final LX/DJC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;Z)V
    .locals 0

    .prologue
    .line 1984974
    iput-object p1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iput-boolean p2, p0, LX/DJC;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x1a085582

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1984975
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_3

    .line 1984976
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1984977
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1984978
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->j:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_2

    .line 1984979
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1984980
    :cond_0
    :goto_0
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->b:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1984981
    :cond_1
    :goto_1
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-static {v1}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->c(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;)V

    .line 1984982
    const v1, 0x71235283

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1984983
    :cond_2
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->k:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/DJC;->a:Z

    if-nez v1, :cond_0

    .line 1984984
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 1984985
    :cond_3
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1984986
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1984987
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->j:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_4

    .line 1984988
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 1984989
    :cond_4
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->k:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, LX/DJC;->a:Z

    if-nez v1, :cond_1

    .line 1984990
    iget-object v1, p0, LX/DJC;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method
