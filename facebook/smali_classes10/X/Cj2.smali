.class public final LX/Cj2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Ve",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CH5;

.field private final b:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(LX/CH5;LX/0Ve;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1928966
    iput-object p1, p0, LX/Cj2;->a:LX/CH5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1928967
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cj2;->c:Z

    .line 1928968
    iput-object p2, p0, LX/Cj2;->b:LX/0Ve;

    .line 1928969
    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 1

    .prologue
    .line 1928970
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cj2;->c:Z

    .line 1928971
    return-void
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 1928972
    iget-boolean v0, p0, LX/Cj2;->c:Z

    return v0
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1928973
    monitor-enter p0

    .line 1928974
    :try_start_0
    iget-boolean v0, p0, LX/Cj2;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Cj2;->b:LX/0Ve;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1928975
    :cond_0
    monitor-exit p0

    .line 1928976
    :goto_0
    return-void

    .line 1928977
    :cond_1
    invoke-virtual {p0}, LX/Cj2;->dispose()V

    .line 1928978
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1928979
    iget-object v0, p0, LX/Cj2;->b:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1928980
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1928981
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1928982
    monitor-enter p0

    .line 1928983
    :try_start_0
    iget-boolean v0, p0, LX/Cj2;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Cj2;->b:LX/0Ve;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1928984
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1928985
    if-nez v0, :cond_1

    .line 1928986
    :cond_0
    monitor-exit p0

    .line 1928987
    :goto_0
    return-void

    .line 1928988
    :cond_1
    invoke-virtual {p0}, LX/Cj2;->dispose()V

    .line 1928989
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1928990
    iget-object v0, p0, LX/Cj2;->b:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0

    .line 1928991
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
