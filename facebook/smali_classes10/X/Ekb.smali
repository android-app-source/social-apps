.class public final LX/Ekb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/crudolib/dbinsert/direct/DirectTransaction$TransactionState$TransactionListener;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2163621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163622
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/Ekb;->a:Ljava/util/ArrayList;

    .line 2163623
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ekb;->c:Z

    .line 2163624
    return-void
.end method


# virtual methods
.method public final a(Z)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2163625
    iget-boolean v0, p0, LX/Ekb;->c:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/Ekb;->c:Z

    .line 2163626
    iget v0, p0, LX/Ekb;->b:I

    if-gtz v0, :cond_1

    .line 2163627
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "depth="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/Ekb;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163628
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2163629
    :cond_1
    iget v0, p0, LX/Ekb;->b:I

    if-ne v0, v1, :cond_4

    .line 2163630
    const/4 v0, 0x0

    iget-object v1, p0, LX/Ekb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 2163631
    iget-object v0, p0, LX/Ekb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eka;

    iget-boolean v3, p0, LX/Ekb;->c:Z

    .line 2163632
    if-nez v3, :cond_5

    .line 2163633
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2163634
    :cond_3
    iget-object v0, p0, LX/Ekb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2163635
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ekb;->c:Z

    .line 2163636
    :cond_4
    iget v0, p0, LX/Ekb;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/Ekb;->b:I

    return v0

    .line 2163637
    :cond_5
    iget-object v4, v0, LX/Eka;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 2163638
    sget-object p1, LX/AUM;->a:LX/AUM;

    move-object p1, p1

    .line 2163639
    invoke-virtual {p1, v5}, LX/AUM;->a(Ljava/lang/Object;)V

    goto :goto_2
.end method
