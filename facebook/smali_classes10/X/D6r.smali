.class public final enum LX/D6r;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D6r;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D6r;

.field public static final enum FAILED:LX/D6r;

.field public static final enum FETCHING:LX/D6r;

.field public static final enum IDLE:LX/D6r;

.field public static final enum SUCCESS:LX/D6r;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1966217
    new-instance v0, LX/D6r;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, LX/D6r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D6r;->IDLE:LX/D6r;

    .line 1966218
    new-instance v0, LX/D6r;

    const-string v1, "FETCHING"

    invoke-direct {v0, v1, v3}, LX/D6r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D6r;->FETCHING:LX/D6r;

    .line 1966219
    new-instance v0, LX/D6r;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, LX/D6r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D6r;->SUCCESS:LX/D6r;

    .line 1966220
    new-instance v0, LX/D6r;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/D6r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D6r;->FAILED:LX/D6r;

    .line 1966221
    const/4 v0, 0x4

    new-array v0, v0, [LX/D6r;

    sget-object v1, LX/D6r;->IDLE:LX/D6r;

    aput-object v1, v0, v2

    sget-object v1, LX/D6r;->FETCHING:LX/D6r;

    aput-object v1, v0, v3

    sget-object v1, LX/D6r;->SUCCESS:LX/D6r;

    aput-object v1, v0, v4

    sget-object v1, LX/D6r;->FAILED:LX/D6r;

    aput-object v1, v0, v5

    sput-object v0, LX/D6r;->$VALUES:[LX/D6r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1966216
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D6r;
    .locals 1

    .prologue
    .line 1966223
    const-class v0, LX/D6r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D6r;

    return-object v0
.end method

.method public static values()[LX/D6r;
    .locals 1

    .prologue
    .line 1966222
    sget-object v0, LX/D6r;->$VALUES:[LX/D6r;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D6r;

    return-object v0
.end method
