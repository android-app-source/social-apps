.class public LX/Dv9;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/DvA;",
        "LX/Dv8;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Dv9;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2058083
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2058084
    return-void
.end method

.method public static a(LX/0QB;)LX/Dv9;
    .locals 3

    .prologue
    .line 2058085
    sget-object v0, LX/Dv9;->a:LX/Dv9;

    if-nez v0, :cond_1

    .line 2058086
    const-class v1, LX/Dv9;

    monitor-enter v1

    .line 2058087
    :try_start_0
    sget-object v0, LX/Dv9;->a:LX/Dv9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2058088
    if-eqz v2, :cond_0

    .line 2058089
    :try_start_1
    new-instance v0, LX/Dv9;

    invoke-direct {v0}, LX/Dv9;-><init>()V

    .line 2058090
    move-object v0, v0

    .line 2058091
    sput-object v0, LX/Dv9;->a:LX/Dv9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2058092
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2058093
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2058094
    :cond_1
    sget-object v0, LX/Dv9;->a:LX/Dv9;

    return-object v0

    .line 2058095
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2058096
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
