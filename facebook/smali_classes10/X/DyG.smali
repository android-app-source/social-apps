.class public final LX/DyG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLAlbumsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V
    .locals 0

    .prologue
    .line 2064067
    iput-object p1, p0, LX/DyG;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2064068
    iget-object v0, p0, LX/DyG;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchFamilyAlbum"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2064069
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2064070
    check-cast p1, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 2064071
    if-eqz p1, :cond_0

    .line 2064072
    iget-object v0, p0, LX/DyG;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    .line 2064073
    iput-object p1, v0, LX/9bF;->b:Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 2064074
    invoke-static {v0}, LX/9bF;->a(LX/9bF;)V

    .line 2064075
    const p0, -0x5f28573d

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2064076
    :cond_0
    return-void
.end method
