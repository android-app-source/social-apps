.class public final LX/Dk2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DkN;


# direct methods
.method public constructor <init>(LX/DkN;)V
    .locals 0

    .prologue
    .line 2033923
    iput-object p1, p0, LX/Dk2;->a:LX/DkN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2033924
    iget-object v0, p0, LX/Dk2;->a:LX/DkN;

    .line 2033925
    iget-object v1, v0, LX/DkN;->f:LX/Dka;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/DkN;->e:Ljava/lang/String;

    iget-object v4, v0, LX/DkN;->c:LX/DkQ;

    invoke-virtual {v4}, LX/DkQ;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, LX/DkN;->e:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, LX/Dka;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2033926
    return-object v0
.end method
