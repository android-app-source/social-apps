.class public LX/EOX;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EOV;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EOa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2114695
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EOX;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EOa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2114696
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2114697
    iput-object p1, p0, LX/EOX;->b:LX/0Ot;

    .line 2114698
    return-void
.end method

.method public static a(LX/0QB;)LX/EOX;
    .locals 4

    .prologue
    .line 2114699
    const-class v1, LX/EOX;

    monitor-enter v1

    .line 2114700
    :try_start_0
    sget-object v0, LX/EOX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114701
    sput-object v2, LX/EOX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114702
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114703
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114704
    new-instance v3, LX/EOX;

    const/16 p0, 0x3467

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EOX;-><init>(LX/0Ot;)V

    .line 2114705
    move-object v0, v3

    .line 2114706
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114707
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114708
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2114710
    check-cast p2, LX/EOW;

    .line 2114711
    iget-object v0, p0, LX/EOX;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EOa;

    iget-object v1, p2, LX/EOW;->a:LX/CzL;

    iget-object v2, p2, LX/EOW;->b:LX/EOS;

    iget v3, p2, LX/EOW;->c:I

    const/16 p2, 0x8

    const/4 v5, 0x0

    .line 2114712
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v6, -0x2

    invoke-interface {v4, p2, v6}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v9

    .line 2114713
    iget-object v4, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2114714
    check-cast v4, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->P()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v10

    .line 2114715
    invoke-static {p1}, LX/EOa;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    .line 2114716
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v7, v5

    move v8, v5

    :goto_0
    if-ge v7, v11, :cond_2

    invoke-virtual {v10, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2114717
    iget-object v5, v0, LX/EOa;->a:LX/EOj;

    invoke-virtual {v5, p1}, LX/EOj;->c(LX/1De;)LX/EOh;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->a()LX/8d2;

    move-result-object p0

    invoke-virtual {v1, p0, v8}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/EOh;->a(LX/CzL;)LX/EOh;

    move-result-object p0

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->b()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->b()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;->a()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {p0, v5}, LX/EOh;->b(Ljava/lang/String;)LX/EOh;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->b()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->b()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;->b()Ljava/lang/String;

    move-result-object v4

    :goto_2
    invoke-virtual {v5, v4}, LX/EOh;->c(Ljava/lang/String;)LX/EOh;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/EOh;->a(LX/EOS;)LX/EOh;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, p2, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2114718
    rem-int v4, v8, v3

    add-int/lit8 v5, v3, -0x1

    if-ne v4, v5, :cond_5

    const/4 v4, 0x1

    :goto_3
    move v4, v4

    .line 2114719
    if-eqz v4, :cond_4

    .line 2114720
    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v9, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2114721
    invoke-static {p1}, LX/EOa;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    .line 2114722
    :goto_4
    add-int/lit8 v6, v8, 0x1

    .line 2114723
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v8, v6

    move-object v6, v4

    goto :goto_0

    .line 2114724
    :cond_0
    const-string v5, ""

    goto :goto_1

    :cond_1
    const-string v4, ""

    goto :goto_2

    .line 2114725
    :cond_2
    :goto_5
    rem-int v4, v8, v3

    if-eqz v4, :cond_3

    .line 2114726
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2114727
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 2114728
    :cond_3
    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v9, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2114729
    invoke-interface {v9}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2114730
    return-object v0

    :cond_4
    move-object v4, v6

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2114731
    invoke-static {}, LX/1dS;->b()V

    .line 2114732
    const/4 v0, 0x0

    return-object v0
.end method
