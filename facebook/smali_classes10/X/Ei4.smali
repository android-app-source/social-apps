.class public LX/Ei4;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2159001
    const-class v0, LX/Ei4;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ei4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2159002
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2159003
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0W3;)LX/115;
    .locals 3
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2159004
    sget-wide v0, LX/0X5;->ho:J

    invoke-interface {p1, v0, v1}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v0

    .line 2159005
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0}, LX/115;->a(Landroid/content/ContentResolver;Ljava/lang/String;)LX/115;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 2159006
    :goto_0
    return-object v0

    .line 2159007
    :catch_0
    move-exception v0

    .line 2159008
    :goto_1
    sget-object v1, LX/Ei4;->a:Ljava/lang/String;

    const-string v2, "Error parsing intent switch-off criteria!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2159009
    new-instance v0, LX/115;

    invoke-direct {v0}, LX/115;-><init>()V

    goto :goto_0

    .line 2159010
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2159011
    return-void
.end method
