.class public LX/E71;
.super LX/Cfk;
.source ""


# instance fields
.field public a:LX/E1i;

.field public b:LX/3Tx;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/E1i;LX/3Tx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080943
    invoke-direct {p0, p2}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2080944
    iput-object p1, p0, LX/E71;->a:LX/E1i;

    .line 2080945
    iput-object p2, p0, LX/E71;->b:LX/3Tx;

    .line 2080946
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 1

    .prologue
    .line 2080942
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2080934
    const v0, 0x7f031102

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v2

    .line 2080935
    const v0, 0x7f0d2865

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2080936
    const v1, 0x7f0d2864

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2080937
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->gT_()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionAttributionFooterAttachmentFieldsModel$AttributionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionAttributionFooterAttachmentFieldsModel$AttributionModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2080938
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2080939
    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2080940
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->gT_()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionAttributionFooterAttachmentFieldsModel$AttributionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionAttributionFooterAttachmentFieldsModel$AttributionModel;->a()LX/3Ab;

    move-result-object v1

    new-instance v3, LX/E70;

    invoke-direct {v3, p0}, LX/E70;-><init>(LX/E71;)V

    invoke-virtual {v0, v1, v3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/3Ab;LX/7wC;)V

    .line 2080941
    return-object v2
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 1

    .prologue
    .line 2080930
    iput-object p1, p0, LX/E71;->c:Ljava/lang/String;

    .line 2080931
    iput-object p2, p0, LX/E71;->d:Ljava/lang/String;

    .line 2080932
    invoke-super {p0, p1, p2, p3}, LX/Cfk;->b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2080933
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->gT_()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionAttributionFooterAttachmentFieldsModel$AttributionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->gT_()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionAttributionFooterAttachmentFieldsModel$AttributionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionAttributionFooterAttachmentFieldsModel$AttributionModel;->a()LX/3Ab;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
