.class public LX/Esf;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Esd;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Esh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2176133
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Esf;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Esh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176109
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2176110
    iput-object p1, p0, LX/Esf;->b:LX/0Ot;

    .line 2176111
    return-void
.end method

.method public static a(LX/0QB;)LX/Esf;
    .locals 4

    .prologue
    .line 2176122
    const-class v1, LX/Esf;

    monitor-enter v1

    .line 2176123
    :try_start_0
    sget-object v0, LX/Esf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176124
    sput-object v2, LX/Esf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176125
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176126
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176127
    new-instance v3, LX/Esf;

    const/16 p0, 0x1f26

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Esf;-><init>(LX/0Ot;)V

    .line 2176128
    move-object v0, v3

    .line 2176129
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176130
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Esf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176131
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176132
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2176134
    check-cast p2, LX/Ese;

    .line 2176135
    iget-object v0, p0, LX/Esf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Esh;

    iget-object v1, p2, LX/Ese;->a:Ljava/util/List;

    .line 2176136
    if-nez v1, :cond_0

    .line 2176137
    const/4 v2, 0x0

    .line 2176138
    :goto_0
    move-object v0, v2

    .line 2176139
    return-object v0

    .line 2176140
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b1cfa

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    .line 2176141
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Esg;

    .line 2176142
    iget-object v5, v0, LX/Esh;->a:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    iget p0, v2, LX/Esg;->c:I

    invoke-virtual {v5, p0}, LX/2xv;->j(I)LX/2xv;

    move-result-object v5

    iget p0, v2, LX/Esg;->b:I

    invoke-virtual {v5, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    .line 2176143
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x1

    invoke-interface {p0, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object p0

    iget-object p2, v2, LX/Esg;->d:LX/1dQ;

    invoke-interface {p0, p2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object p0

    invoke-static {p1}, LX/8ym;->c(LX/1De;)LX/8yk;

    move-result-object p2

    const v1, 0x7f0b009c

    invoke-virtual {p2, v1}, LX/8yk;->h(I)LX/8yk;

    move-result-object p2

    iget-object v1, v2, LX/Esg;->a:Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, LX/8yk;->a(Ljava/lang/CharSequence;)LX/8yk;

    move-result-object p2

    const v1, 0x7f0e0228

    invoke-virtual {p2, v1}, LX/8yk;->i(I)LX/8yk;

    move-result-object p2

    invoke-virtual {p2, v5}, LX/8yk;->a(LX/1dc;)LX/8yk;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->d()LX/1X1;

    move-result-object v5

    invoke-interface {p0, v5}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v5

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v5, p0}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v5

    move-object v2, v5

    .line 2176144
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    goto :goto_1

    .line 2176145
    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v4

    const v5, 0x7f0a0442

    invoke-virtual {v4, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x6

    const p0, 0x7f0b0917

    invoke-interface {v4, v5, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0033

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {v4, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2176120
    invoke-static {}, LX/1dS;->b()V

    .line 2176121
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/Esd;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2176112
    new-instance v1, LX/Ese;

    invoke-direct {v1, p0}, LX/Ese;-><init>(LX/Esf;)V

    .line 2176113
    sget-object v2, LX/Esf;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Esd;

    .line 2176114
    if-nez v2, :cond_0

    .line 2176115
    new-instance v2, LX/Esd;

    invoke-direct {v2}, LX/Esd;-><init>()V

    .line 2176116
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Esd;->a$redex0(LX/Esd;LX/1De;IILX/Ese;)V

    .line 2176117
    move-object v1, v2

    .line 2176118
    move-object v0, v1

    .line 2176119
    return-object v0
.end method
