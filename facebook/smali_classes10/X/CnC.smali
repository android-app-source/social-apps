.class public LX/CnC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cmt;


# instance fields
.field private final a:LX/Cju;


# direct methods
.method public constructor <init>(LX/Cju;)V
    .locals 0

    .prologue
    .line 1933805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933806
    iput-object p1, p0, LX/CnC;->a:LX/Cju;

    .line 1933807
    return-void
.end method

.method public static a(Lcom/facebook/richdocument/view/widget/RichTextView;Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 1933816
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 1933817
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v0

    .line 1933818
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1933819
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/Cml;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1933808
    instance-of v0, p2, LX/Cn6;

    if-eqz v0, :cond_0

    .line 1933809
    check-cast p1, Landroid/view/ViewGroup;

    .line 1933810
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v0, :cond_0

    .line 1933811
    check-cast p2, LX/Cn6;

    .line 1933812
    iget-object v0, p2, LX/Cn6;->b:LX/Cmw;

    move-object v0, v0

    .line 1933813
    iget-object v1, p0, LX/CnC;->a:LX/Cju;

    invoke-static {v0, v1}, LX/Cn1;->a(LX/Cmw;LX/Cju;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1933814
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-static {v0, v1}, LX/CnC;->a(Lcom/facebook/richdocument/view/widget/RichTextView;Landroid/graphics/Rect;)V

    .line 1933815
    :cond_0
    return-void
.end method
