.class public LX/EdD;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile a:Z

.field private static b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2148900
    const/4 v0, 0x0

    sput-boolean v0, LX/EdD;->a:Z

    .line 2148901
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, LX/EdD;->b:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2148855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(I)Landroid/os/Bundle;
    .locals 8

    .prologue
    .line 2148856
    invoke-static {}, LX/EdL;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2148857
    const/4 v0, 0x0

    .line 2148858
    :goto_0
    return-object v0

    .line 2148859
    :cond_0
    sget-object v1, LX/EdD;->b:Landroid/util/SparseArray;

    monitor-enter v1

    .line 2148860
    :try_start_0
    sget-object v0, LX/EdD;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 2148861
    if-nez v0, :cond_9

    .line 2148862
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2148863
    sget-object v2, LX/EdD;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, p0, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2148864
    sget-object v2, LX/EdH;->e:LX/Ed0;

    move-object v2, v2

    .line 2148865
    if-eqz v2, :cond_8

    instance-of v2, v2, LX/Ed7;

    if-nez v2, :cond_8

    .line 2148866
    invoke-static {p0}, LX/EdL;->a(I)Landroid/telephony/SmsManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/SmsManager;->getCarrierConfigValues()Landroid/os/Bundle;

    move-result-object v2

    .line 2148867
    sget-object v3, LX/EdH;->e:LX/Ed0;

    move-object v3, v3

    .line 2148868
    invoke-interface {v3, p0}, LX/Ed0;->a(I)Landroid/os/Bundle;

    move-result-object v3

    .line 2148869
    if-eqz v2, :cond_a

    if-eqz v3, :cond_a

    .line 2148870
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2148871
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 2148872
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 2148873
    if-eqz v5, :cond_2

    if-eqz v7, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    :cond_2
    if-eqz v5, :cond_3

    if-eqz v7, :cond_4

    :cond_3
    if-nez v5, :cond_1

    if-eqz v7, :cond_1

    .line 2148874
    :cond_4
    if-eqz v5, :cond_5

    instance-of v7, v5, Ljava/lang/String;

    if-eqz v7, :cond_6

    .line 2148875
    :cond_5
    check-cast v5, Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2148876
    :cond_6
    instance-of v7, v5, Ljava/lang/Integer;

    if-eqz v7, :cond_7

    .line 2148877
    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 2148878
    :cond_7
    instance-of v7, v5, Ljava/lang/Boolean;

    if-eqz v7, :cond_1

    .line 2148879
    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 2148880
    :cond_8
    :goto_2
    sget-object v2, LX/EdH;->g:LX/Ed8;

    move-object v2, v2

    .line 2148881
    if-eqz v2, :cond_9

    instance-of v3, v2, LX/Ed8;

    if-nez v3, :cond_9

    .line 2148882
    const-string v3, "userAgent"

    invoke-virtual {v2}, LX/Ed8;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2148883
    const-string v3, "uaProfUrl"

    invoke-virtual {v2}, LX/Ed8;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2148884
    :cond_9
    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2148885
    :cond_a
    if-nez v2, :cond_8

    if-eqz v3, :cond_8

    .line 2148886
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_2
.end method

.method public static a(ILandroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;Z)V
    .locals 6

    .prologue
    .line 2148887
    invoke-static {}, LX/EdD;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p5, :cond_0

    .line 2148888
    invoke-static {p0}, LX/EdL;->b(I)I

    move-result v1

    .line 2148889
    invoke-static {v1}, LX/EdL;->a(I)Landroid/telephony/SmsManager;

    move-result-object v0

    .line 2148890
    invoke-static {v1}, LX/EdD;->a(I)Landroid/os/Bundle;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultimediaMessage(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/PendingIntent;)V

    .line 2148891
    :goto_0
    return-void

    .line 2148892
    :cond_0
    new-instance v0, Landroid_src/mmsv2/SendRequest;

    invoke-direct {v0, p3, p2, p4}, Landroid_src/mmsv2/SendRequest;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;)V

    invoke-static {p1, v0}, LX/EdH;->a(Landroid/content/Context;Landroid_src/mmsv2/MmsRequest;)V

    goto :goto_0
.end method

.method public static a(ILandroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 2148893
    invoke-static {}, LX/EdD;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2148894
    invoke-static {p0}, LX/EdL;->b(I)I

    move-result v1

    .line 2148895
    invoke-static {v1}, LX/EdL;->a(I)Landroid/telephony/SmsManager;

    move-result-object v0

    .line 2148896
    invoke-static {v1}, LX/EdD;->a(I)Landroid/os/Bundle;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->downloadMultimediaMessage(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Landroid/os/Bundle;Landroid/app/PendingIntent;)V

    .line 2148897
    :goto_0
    return-void

    .line 2148898
    :cond_0
    new-instance v0, Landroid_src/mmsv2/DownloadRequest;

    invoke-direct {v0, p2, p3, p4}, Landroid_src/mmsv2/DownloadRequest;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;)V

    invoke-static {p1, v0}, LX/EdH;->a(Landroid/content/Context;Landroid_src/mmsv2/MmsRequest;)V

    goto :goto_0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 2148899
    invoke-static {}, LX/EdL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, LX/EdD;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
