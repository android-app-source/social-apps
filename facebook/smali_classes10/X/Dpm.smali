.class public LX/Dpm;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2047215
    sget-object v0, LX/DpF;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, LX/Dpm;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2047214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([BLjava/lang/Long;Ljava/lang/Long;[BLjava/lang/String;Ljava/lang/String;[BLX/DpI;LX/Dpp;Ljava/lang/String;)LX/DpA;
    .locals 11

    .prologue
    .line 2047213
    new-instance v0, LX/DpA;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, LX/DpA;-><init>([BLjava/lang/Long;Ljava/lang/Long;[BLjava/lang/String;Ljava/lang/String;[BLX/DpI;LX/Dpp;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(JLjava/lang/String;)LX/DpM;
    .locals 2

    .prologue
    .line 2047216
    new-instance v0, LX/DpM;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, p2}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/DpM;LX/DpM;JIILX/DpO;[BLjava/lang/Long;)LX/DpN;
    .locals 10
    .param p0    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2047212
    new-instance v0, LX/DpN;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, LX/DpN;-><init>(Ljava/lang/Integer;LX/DpM;LX/DpM;Ljava/lang/Long;Ljava/lang/Integer;LX/DpO;[BLjava/lang/Long;)V

    return-object v0
.end method

.method private static a(LX/DpM;LX/DpM;JILX/DpO;[BLjava/lang/Long;)LX/DpN;
    .locals 10
    .param p0    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2047211
    new-instance v0, LX/DpN;

    sget v1, LX/Dpm;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v2, p0

    move-object v3, p1

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, LX/DpN;-><init>(Ljava/lang/Integer;LX/DpM;LX/DpM;Ljava/lang/Long;Ljava/lang/Integer;LX/DpO;[BLjava/lang/Long;)V

    return-object v0
.end method

.method public static a(LX/Dpe;LX/DpM;LX/DpM;JILX/DpO;[B)LX/DpN;
    .locals 9
    .param p0    # LX/Dpe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/DpM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2047206
    const/4 v7, 0x0

    move-object v0, p1

    move-object v1, p2

    move-wide v2, p3

    move v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    invoke-static/range {v0 .. v7}, LX/Dpm;->a(LX/DpM;LX/DpM;JILX/DpO;[BLjava/lang/Long;)LX/DpN;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILX/DpZ;[BLjava/lang/Integer;)LX/DpY;
    .locals 4
    .param p3    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2047207
    const/4 v0, 0x0

    .line 2047208
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_0

    .line 2047209
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2047210
    :cond_0
    new-instance v1, LX/DpY;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2, v0}, LX/DpY;-><init>(Ljava/lang/Integer;LX/DpZ;[BLjava/lang/Long;)V

    return-object v1
.end method
