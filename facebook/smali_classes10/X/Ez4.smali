.class public final LX/Ez4;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;)V
    .locals 0

    .prologue
    .line 2186772
    iput-object p1, p0, LX/Ez4;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 12

    .prologue
    const-wide/16 v2, 0x0

    .line 2186776
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v10, v0

    .line 2186777
    iget-object v0, p0, LX/Ez4;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 2186778
    iget-object v0, p0, LX/Ez4;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->v:LX/0P1;

    iget-object v1, p0, LX/Ez4;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->r:LX/Ez3;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ez2;

    .line 2186779
    iget-object v1, v0, LX/Ez2;->a:LX/BS8;

    .line 2186780
    iget-object v4, v1, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    move-object v1, v4

    .line 2186781
    invoke-virtual {v1, v10}, Lcom/facebook/uicontrib/fab/FabView;->setAlpha(F)V

    .line 2186782
    iget-object v0, v0, LX/Ez2;->a:LX/BS8;

    .line 2186783
    iget-object v1, v0, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    move-object v11, v1

    .line 2186784
    float-to-double v0, v10

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide v8, 0x4076800000000000L    # 360.0

    move-wide v6, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {v11, v0}, Lcom/facebook/uicontrib/fab/FabView;->setRotation(F)V

    .line 2186785
    iget-object v0, p0, LX/Ez4;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->v:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2186786
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LX/Ez4;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v3, v3, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->r:LX/Ez3;

    if-eq v1, v3, :cond_0

    .line 2186787
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ez2;

    .line 2186788
    iget-object v1, v0, LX/Ez2;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2186789
    iget-object v3, v0, LX/Ez2;->a:LX/BS8;

    iget-object v1, v0, LX/Ez2;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget-object v0, v0, LX/Ez2;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v10

    sub-float v0, v1, v0

    invoke-virtual {v3, v0}, LX/BS8;->setTranslationY(F)V

    goto :goto_0

    .line 2186790
    :cond_1
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 2186773
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2186774
    iget-object v0, p0, LX/Ez4;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2186775
    :cond_0
    return-void
.end method
