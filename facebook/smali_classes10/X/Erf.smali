.class public final LX/Erf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/InviteeReviewModeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/InviteeReviewModeFragment;)V
    .locals 0

    .prologue
    .line 2173244
    iput-object p1, p0, LX/Erf;->a:Lcom/facebook/events/invite/InviteeReviewModeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2173245
    iget-object v0, p0, LX/Erf;->a:Lcom/facebook/events/invite/InviteeReviewModeFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/InviteeReviewModeFragment;->a:LX/Ere;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/invite/EventInviteeToken;

    .line 2173246
    iget-object v1, p0, LX/Erf;->a:Lcom/facebook/events/invite/InviteeReviewModeFragment;

    const/4 p2, 0x1

    .line 2173247
    iget-object p0, v1, Lcom/facebook/events/invite/InviteeReviewModeFragment;->g:Ljava/util/LinkedList;

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 2173248
    iget-object p0, v1, Lcom/facebook/events/invite/InviteeReviewModeFragment;->g:Ljava/util/LinkedList;

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2173249
    iget-object p0, v1, Lcom/facebook/events/invite/InviteeReviewModeFragment;->e:LX/ErW;

    sget-object p1, LX/ErX;->REVIEW:LX/ErX;

    invoke-virtual {p0, p1, p2}, LX/ErW;->a(LX/ErX;I)V

    .line 2173250
    :cond_0
    :goto_0
    iget-object p0, v1, Lcom/facebook/events/invite/InviteeReviewModeFragment;->a:LX/Ere;

    const p1, -0x145a6a49

    invoke-static {p0, p1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2173251
    return-void

    .line 2173252
    :cond_1
    iget-object p0, v1, Lcom/facebook/events/invite/InviteeReviewModeFragment;->g:Ljava/util/LinkedList;

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2173253
    iget-object p0, v1, Lcom/facebook/events/invite/InviteeReviewModeFragment;->g:Ljava/util/LinkedList;

    invoke-virtual {p0, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 2173254
    iget-object p0, v1, Lcom/facebook/events/invite/InviteeReviewModeFragment;->e:LX/ErW;

    sget-object p1, LX/ErX;->REVIEW:LX/ErX;

    invoke-virtual {p0, p1, p2}, LX/ErW;->b(LX/ErX;I)V

    goto :goto_0
.end method
