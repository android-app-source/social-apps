.class public abstract LX/DvZ;
.super Landroid/widget/BaseAdapter;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2058738
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Class",
            "<+",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract a(I)Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2058737
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2058736
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
