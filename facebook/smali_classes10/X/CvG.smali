.class public LX/CvG;
.super LX/16T;
.source ""

# interfaces
.implements LX/Cv0;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static d:LX/0hs;

.field private static volatile e:LX/CvG;


# instance fields
.field public final a:LX/0tQ;

.field public final b:LX/0iA;

.field public c:Z


# direct methods
.method public constructor <init>(LX/0tQ;LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1947949
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1947950
    iput-object p1, p0, LX/CvG;->a:LX/0tQ;

    .line 1947951
    iput-object p2, p0, LX/CvG;->b:LX/0iA;

    .line 1947952
    return-void
.end method

.method public static a(LX/0QB;)LX/CvG;
    .locals 5

    .prologue
    .line 1947931
    sget-object v0, LX/CvG;->e:LX/CvG;

    if-nez v0, :cond_1

    .line 1947932
    const-class v1, LX/CvG;

    monitor-enter v1

    .line 1947933
    :try_start_0
    sget-object v0, LX/CvG;->e:LX/CvG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1947934
    if-eqz v2, :cond_0

    .line 1947935
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1947936
    new-instance p0, LX/CvG;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v3

    check-cast v3, LX/0tQ;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v4

    check-cast v4, LX/0iA;

    invoke-direct {p0, v3, v4}, LX/CvG;-><init>(LX/0tQ;LX/0iA;)V

    .line 1947937
    move-object v0, p0

    .line 1947938
    sput-object v0, LX/CvG;->e:LX/CvG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1947939
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1947940
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1947941
    :cond_1
    sget-object v0, LX/CvG;->e:LX/CvG;

    return-object v0

    .line 1947942
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1947943
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1947946
    iget-object v0, p0, LX/CvG;->a:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CvG;->a:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/CvG;->c:Z

    if-nez v0, :cond_0

    .line 1947947
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 1947948
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(LX/1A0;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1947953
    sget-object v0, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    if-ne p1, v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1947954
    if-eqz v0, :cond_3

    sget-object v0, LX/CvG;->d:LX/0hs;

    if-eqz v0, :cond_1

    sget-object v0, LX/CvG;->d:LX/0hs;

    .line 1947955
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 1947956
    if-nez v0, :cond_2

    :cond_1
    iget-boolean v0, p0, LX/CvG;->c:Z

    if-eqz v0, :cond_3

    .line 1947957
    :cond_2
    :goto_1
    return-void

    .line 1947958
    :cond_3
    new-instance v0, LX/0hs;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1947959
    sput-object v0, LX/CvG;->d:LX/0hs;

    const/4 v1, -0x1

    .line 1947960
    iput v1, v0, LX/0hs;->t:I

    .line 1947961
    sget-object v0, LX/CvG;->d:LX/0hs;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1947962
    sget-object v2, LX/CvF;->a:[I

    iget-object p1, p0, LX/CvG;->a:LX/0tQ;

    invoke-virtual {p1}, LX/0tQ;->m()LX/2qY;

    move-result-object p1

    invoke-virtual {p1}, LX/2qY;->ordinal()I

    move-result p1

    aget v2, v2, p1

    packed-switch v2, :pswitch_data_0

    .line 1947963
    const v2, 0x7f080d39

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_2
    move-object v1, v2

    .line 1947964
    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1947965
    sget-object v0, LX/CvG;->d:LX/0hs;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1947966
    sget-object v2, LX/CvF;->a:[I

    iget-object p1, p0, LX/CvG;->a:LX/0tQ;

    invoke-virtual {p1}, LX/0tQ;->m()LX/2qY;

    move-result-object p1

    invoke-virtual {p1}, LX/2qY;->ordinal()I

    move-result p1

    aget v2, v2, p1

    packed-switch v2, :pswitch_data_1

    .line 1947967
    const v2, 0x7f080d3b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_3
    move-object v1, v2

    .line 1947968
    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1947969
    sget-object v0, LX/CvG;->d:LX/0hs;

    invoke-virtual {v0, p2}, LX/0ht;->f(Landroid/view/View;)V

    .line 1947970
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CvG;->c:Z

    .line 1947971
    iget-object v0, p0, LX/CvG;->b:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4341"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1947972
    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 1947973
    :pswitch_0
    const v2, 0x7f080d38

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_2

    .line 1947974
    :pswitch_1
    const v2, 0x7f080d3a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_2

    .line 1947975
    :pswitch_2
    const v2, 0x7f080d3c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1947945
    const-string v0, "4341"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1947944
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_DOWNLOAD_STARTED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
