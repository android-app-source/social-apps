.class public LX/E5T;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E5R;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E5T;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5U;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078454
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5T;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5U;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078425
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078426
    iput-object p1, p0, LX/E5T;->b:LX/0Ot;

    .line 2078427
    return-void
.end method

.method public static a(LX/0QB;)LX/E5T;
    .locals 4

    .prologue
    .line 2078441
    sget-object v0, LX/E5T;->c:LX/E5T;

    if-nez v0, :cond_1

    .line 2078442
    const-class v1, LX/E5T;

    monitor-enter v1

    .line 2078443
    :try_start_0
    sget-object v0, LX/E5T;->c:LX/E5T;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078444
    if-eqz v2, :cond_0

    .line 2078445
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078446
    new-instance v3, LX/E5T;

    const/16 p0, 0x3129

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E5T;-><init>(LX/0Ot;)V

    .line 2078447
    move-object v0, v3

    .line 2078448
    sput-object v0, LX/E5T;->c:LX/E5T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078449
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078450
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078451
    :cond_1
    sget-object v0, LX/E5T;->c:LX/E5T;

    return-object v0

    .line 2078452
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078453
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2078438
    check-cast p2, LX/E5S;

    .line 2078439
    iget-object v0, p0, LX/E5T;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E5U;

    iget-object v2, p2, LX/E5S;->a:Landroid/text/SpannableStringBuilder;

    iget-boolean v3, p2, LX/E5S;->b:Z

    iget-object v4, p2, LX/E5S;->c:LX/174;

    iget-object v5, p2, LX/E5S;->d:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/E5U;->a(LX/1De;Landroid/text/SpannableStringBuilder;ZLX/174;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/1Dg;

    move-result-object v0

    .line 2078440
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2078436
    invoke-static {}, LX/1dS;->b()V

    .line 2078437
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/E5R;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2078428
    new-instance v1, LX/E5S;

    invoke-direct {v1, p0}, LX/E5S;-><init>(LX/E5T;)V

    .line 2078429
    sget-object v2, LX/E5T;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E5R;

    .line 2078430
    if-nez v2, :cond_0

    .line 2078431
    new-instance v2, LX/E5R;

    invoke-direct {v2}, LX/E5R;-><init>()V

    .line 2078432
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/E5R;->a$redex0(LX/E5R;LX/1De;IILX/E5S;)V

    .line 2078433
    move-object v1, v2

    .line 2078434
    move-object v0, v1

    .line 2078435
    return-object v0
.end method
