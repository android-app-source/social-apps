.class public LX/EEq;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/EEp;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2094358
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2094359
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/0gc;)LX/EEp;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/EGE;",
            ">;",
            "LX/0gc;",
            ")",
            "LX/EEp;"
        }
    .end annotation

    .prologue
    .line 2094360
    new-instance v0, LX/EEp;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    .line 2094361
    new-instance v6, LX/Dpx;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v8

    check-cast v8, LX/121;

    const-class v9, Landroid/content/Context;

    invoke-interface {p0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-static {p0}, LX/DbA;->a(LX/0QB;)LX/DbA;

    move-result-object v11

    check-cast v11, LX/DbA;

    invoke-direct/range {v6 .. v11}, LX/Dpx;-><init>(Lcom/facebook/content/SecureContextHelper;LX/121;Landroid/content/Context;Ljava/lang/Boolean;LX/DbA;)V

    .line 2094362
    move-object v3, v6

    .line 2094363
    check-cast v3, LX/Dpx;

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/EEp;-><init>(Landroid/content/Context;LX/0Uh;LX/Dpx;LX/0Px;LX/0gc;)V

    .line 2094364
    const/16 v1, 0x3257

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/Dd0;->b(LX/0QB;)LX/Dd0;

    move-result-object v2

    check-cast v2, LX/Dd0;

    invoke-static {p0}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v3

    check-cast v3, LX/2CH;

    invoke-static {p0}, LX/1tu;->a(LX/0QB;)LX/1tu;

    move-result-object v4

    check-cast v4, LX/1tu;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    .line 2094365
    iput-object v1, v0, LX/EEp;->q:LX/0Ot;

    iput-object v2, v0, LX/EEp;->r:LX/Dd0;

    iput-object v3, v0, LX/EEp;->s:LX/2CH;

    iput-object v4, v0, LX/EEp;->t:LX/1tu;

    iput-object v5, v0, LX/EEp;->u:LX/0ad;

    .line 2094366
    return-object v0
.end method
