.class public LX/Co1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1934708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1934709
    return-void
.end method

.method public static a(LX/Clu;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 1934710
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1934711
    instance-of v0, p0, LX/Clp;

    if-nez v0, :cond_0

    move-object v0, v2

    .line 1934712
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    .line 1934713
    check-cast v0, LX/Clp;

    .line 1934714
    instance-of v1, p0, LX/Cls;

    if-eqz v1, :cond_1

    move-object v1, p0

    check-cast v1, LX/Cls;

    invoke-interface {v1}, LX/Cls;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1934715
    const-string v1, "isCoverMedia"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1934716
    :cond_1
    invoke-interface {p0}, LX/Clu;->d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v1, v3, :cond_3

    .line 1934717
    invoke-interface {v0}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-ne v0, v1, :cond_2

    .line 1934718
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->FULLSCREEN_SLIDESHOW:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move-object v0, v2

    .line 1934719
    goto :goto_0

    .line 1934720
    :cond_2
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->ASPECT_FIT_SLIDESHOW:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1934721
    :cond_3
    invoke-interface {p0}, LX/Clu;->d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->NATIVE_AD:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v1, v3, :cond_4

    .line 1934722
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1934723
    :cond_4
    instance-of v1, p0, LX/Clv;

    if-eqz v1, :cond_7

    move-object v1, p0

    check-cast v1, LX/Clv;

    invoke-interface {v1}, LX/Clv;->iY_()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1934724
    check-cast p0, LX/Clv;

    .line 1934725
    invoke-interface {p0}, LX/Clv;->iZ_()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1934726
    sget-object v0, LX/Co0;->a:[I

    invoke-interface {p0}, LX/Clv;->iZ_()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 1934727
    :pswitch_0
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->ASPECT_FIT_SLIDE:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1934728
    :pswitch_1
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->FULLSCREEN_SLIDE:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1934729
    :cond_5
    invoke-interface {v0}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-ne v0, v1, :cond_6

    .line 1934730
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->FULLSCREEN_SLIDE:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1934731
    :cond_6
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->ASPECT_FIT_SLIDE:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1934732
    :cond_7
    invoke-interface {p0}, LX/Clu;->d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v1, v3, :cond_8

    instance-of v1, p0, LX/Clw;

    if-nez v1, :cond_a

    .line 1934733
    :cond_8
    const/4 v1, 0x0

    .line 1934734
    :goto_2
    move v1, v1

    .line 1934735
    if-eqz v1, :cond_9

    .line 1934736
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->SPHERICAL_PHOTO:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1934737
    :cond_9
    sget-object v1, LX/Co0;->a:[I

    invoke-interface {v0}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 1934738
    :pswitch_2
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->ASPECT_FIT:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1934739
    :pswitch_3
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->FULLSCREEN:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1934740
    :pswitch_4
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->ASPECT_FIT_ONLY:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1934741
    :pswitch_5
    const-string v0, "strategyType"

    sget-object v1, LX/CrN;->NON_INTERACTIVE:LX/CrN;

    invoke-virtual {v1}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1934742
    :cond_a
    check-cast p0, LX/Clw;

    .line 1934743
    invoke-interface {p0}, LX/Clw;->b()Z

    move-result v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(LX/CnG;LX/Clq;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 1934744
    instance-of v0, p0, LX/CnJ;

    if-eqz v0, :cond_0

    .line 1934745
    const-string v0, "PresenterUtils.setTextAnnotations"

    const v1, 0x9a659c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    move-object v0, p0

    .line 1934746
    check-cast v0, LX/CnJ;

    invoke-interface {p1}, LX/Clq;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    invoke-interface {p1}, LX/Clq;->iX_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v2

    invoke-interface {p1}, LX/Clq;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, LX/CnJ;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)V

    .line 1934747
    const v0, 0x57886ed3    # 3.00019136E14f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1934748
    :cond_0
    instance-of v0, p0, LX/CnI;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 1934749
    check-cast v0, LX/CnI;

    invoke-interface {p1}, LX/Clq;->f()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CnI;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;)V

    .line 1934750
    :cond_1
    instance-of v0, p0, LX/CnE;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 1934751
    check-cast v0, LX/CnE;

    invoke-interface {p1}, LX/Clq;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/Clq;->h()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v2

    invoke-interface {p1}, LX/Clq;->i()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    .line 1934752
    if-nez v3, :cond_5

    const/4 v7, 0x0

    .line 1934753
    :goto_0
    if-nez v3, :cond_6

    sget-object v8, LX/ClQ;->LEFT:LX/ClQ;

    .line 1934754
    :goto_1
    new-instance v4, LX/ClV;

    move-object v5, v1

    move-object v6, v2

    move-object v9, p2

    invoke-direct/range {v4 .. v9}, LX/ClV;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Ljava/lang/String;LX/ClQ;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    move-object v1, v4

    .line 1934755
    invoke-interface {v0, v1}, LX/CnE;->a(LX/ClV;)V

    .line 1934756
    :cond_2
    instance-of v0, p0, LX/CnH;

    if-eqz v0, :cond_3

    .line 1934757
    const-string v0, "PresenterUtils.setFeedback"

    const v1, 0x424ab345

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    move-object v0, p0

    .line 1934758
    check-cast v0, LX/CnH;

    invoke-interface {p1}, LX/Clq;->j()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v1

    invoke-interface {p1}, LX/Clq;->k()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/CnH;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1934759
    const v0, 0x70d69bd

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1934760
    :cond_3
    instance-of v0, p0, LX/Cos;

    if-eqz v0, :cond_4

    .line 1934761
    check-cast p0, LX/Cos;

    .line 1934762
    const-class v0, LX/CuA;

    invoke-virtual {p0, v0}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v0

    check-cast v0, LX/CuA;

    .line 1934763
    if-eqz v0, :cond_4

    .line 1934764
    iput-object p3, v0, LX/CuA;->e:Landroid/os/Bundle;

    .line 1934765
    :cond_4
    return-void

    .line 1934766
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->b()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 1934767
    :cond_6
    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->c()Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;

    move-result-object v4

    invoke-static {v4}, LX/ClQ;->from(Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;)LX/ClQ;

    move-result-object v8

    goto :goto_1
.end method
