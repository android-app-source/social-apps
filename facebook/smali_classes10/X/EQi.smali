.class public final LX/EQi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EQj;


# direct methods
.method public constructor <init>(LX/EQj;)V
    .locals 0

    .prologue
    .line 2119022
    iput-object p1, p0, LX/EQi;->a:LX/EQj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2119023
    sget-object v0, LX/EQj;->a:Ljava/lang/Class;

    const-string v1, "Failed to load Moments app info."

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2119024
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2119025
    check-cast p1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2119026
    if-eqz p1, :cond_0

    .line 2119027
    iget-object v0, p0, LX/EQi;->a:LX/EQj;

    .line 2119028
    iget-object p0, v0, LX/EQj;->d:LX/0Sh;

    invoke-virtual {p0}, LX/0Sh;->a()V

    .line 2119029
    iget-object p0, v0, LX/EQj;->f:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-static {p0, p1}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2119030
    :cond_0
    :goto_0
    return-void

    .line 2119031
    :cond_1
    iput-object p1, v0, LX/EQj;->f:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2119032
    iget-object p0, v0, LX/EQj;->d:LX/0Sh;

    invoke-virtual {p0}, LX/0Sh;->a()V

    .line 2119033
    iget-object p0, v0, LX/EQj;->e:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Dwz;

    .line 2119034
    if-eqz p0, :cond_2

    .line 2119035
    invoke-interface {p0}, LX/Dwz;->a()V

    goto :goto_1

    .line 2119036
    :cond_3
    goto :goto_0
.end method
