.class public LX/DdC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2019248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019249
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "no_reaction"

    :cond_0
    iput-object p1, p0, LX/DdC;->a:Ljava/lang/String;

    .line 2019250
    iput-object p2, p0, LX/DdC;->b:Ljava/lang/String;

    .line 2019251
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2019253
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/DdC;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2019254
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, LX/DdC;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, LX/DdC;

    iget-object v0, v0, LX/DdC;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/DdC;->b:Ljava/lang/String;

    check-cast p1, LX/DdC;

    iget-object v2, p1, LX/DdC;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2019252
    iget-object v0, p0, LX/DdC;->a:Ljava/lang/String;

    iget-object v1, p0, LX/DdC;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/1bi;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
