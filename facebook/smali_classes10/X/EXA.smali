.class public final LX/EXA;
.super LX/EX1;
.source ""

# interfaces
.implements LX/EX8;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EX1",
        "<",
        "LX/EXA;",
        ">;",
        "LX/EX8;"
    }
.end annotation


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXA;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EXA;


# instance fields
.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public uninterpretedOption_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2132752
    new-instance v0, LX/EX7;

    invoke-direct {v0}, LX/EX7;-><init>()V

    sput-object v0, LX/EXA;->a:LX/EWZ;

    .line 2132753
    new-instance v0, LX/EXA;

    invoke-direct {v0}, LX/EXA;-><init>()V

    .line 2132754
    sput-object v0, LX/EXA;->c:LX/EXA;

    invoke-direct {v0}, LX/EXA;->m()V

    .line 2132755
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2132747
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2132748
    iput-byte v0, p0, LX/EXA;->memoizedIsInitialized:B

    .line 2132749
    iput v0, p0, LX/EXA;->memoizedSerializedSize:I

    .line 2132750
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2132751
    iput-object v0, p0, LX/EXA;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 2132713
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2132714
    iput-byte v1, p0, LX/EXA;->memoizedIsInitialized:B

    .line 2132715
    iput v1, p0, LX/EXA;->memoizedSerializedSize:I

    .line 2132716
    invoke-direct {p0}, LX/EXA;->m()V

    .line 2132717
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v3

    move v1, v0

    .line 2132718
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 2132719
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v4

    .line 2132720
    sparse-switch v4, :sswitch_data_0

    .line 2132721
    invoke-virtual {p0, p1, v3, p2, v4}, LX/EX1;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 2132722
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 2132723
    goto :goto_0

    .line 2132724
    :sswitch_1
    and-int/lit8 v4, v0, 0x1

    if-eq v4, v2, :cond_1

    .line 2132725
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    .line 2132726
    or-int/lit8 v0, v0, 0x1

    .line 2132727
    :cond_1
    iget-object v4, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    sget-object v5, LX/EYB;->a:LX/EWZ;

    invoke-virtual {p1, v5, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2132728
    :catch_0
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 2132729
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2132730
    move-object v0, v0

    .line 2132731
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2132732
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 2132733
    iget-object v1, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    .line 2132734
    :cond_2
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EXA;->unknownFields:LX/EZQ;

    .line 2132735
    invoke-virtual {p0}, LX/EX1;->E()V

    throw v0

    .line 2132736
    :cond_3
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 2132737
    iget-object v0, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    .line 2132738
    :cond_4
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXA;->unknownFields:LX/EZQ;

    .line 2132739
    invoke-virtual {p0}, LX/EX1;->E()V

    .line 2132740
    return-void

    .line 2132741
    :catch_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 2132742
    :try_start_2
    new-instance v4, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2132743
    iput-object p0, v4, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2132744
    move-object v0, v4

    .line 2132745
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2132746
    :catchall_1
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1f3a -> :sswitch_1
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWy",
            "<",
            "LX/EXA;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 2132708
    invoke-direct {p0, p1}, LX/EX1;-><init>(LX/EWy;)V

    .line 2132709
    iput-byte v0, p0, LX/EXA;->memoizedIsInitialized:B

    .line 2132710
    iput v0, p0, LX/EXA;->memoizedSerializedSize:I

    .line 2132711
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXA;->unknownFields:LX/EZQ;

    .line 2132712
    return-void
.end method

.method public static a(LX/EXA;)LX/EX9;
    .locals 1

    .prologue
    .line 2132707
    invoke-static {}, LX/EX9;->x()LX/EX9;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EX9;->a(LX/EXA;)LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 2132705
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    .line 2132706
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2132703
    new-instance v0, LX/EX9;

    invoke-direct {v0, p1}, LX/EX9;-><init>(LX/EYd;)V

    .line 2132704
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    .line 2132695
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2132696
    invoke-virtual {p0}, LX/EX1;->G()LX/EYf;

    move-result-object v2

    .line 2132697
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2132698
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2132699
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2132700
    :cond_0
    const/high16 v0, 0x20000000

    invoke-virtual {v2, v0, p1}, LX/EYf;->a(ILX/EWf;)V

    .line 2132701
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2132702
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2132682
    iget-byte v0, p0, LX/EXA;->memoizedIsInitialized:B

    .line 2132683
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2132684
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2132685
    :goto_1
    iget-object v3, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move v3, v3

    .line 2132686
    if-ge v0, v3, :cond_3

    .line 2132687
    iget-object v3, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EYB;

    move-object v3, v3

    .line 2132688
    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2132689
    iput-byte v1, p0, LX/EXA;->memoizedIsInitialized:B

    goto :goto_0

    .line 2132690
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2132691
    :cond_3
    invoke-virtual {p0}, LX/EX1;->F()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2132692
    iput-byte v1, p0, LX/EXA;->memoizedIsInitialized:B

    goto :goto_0

    .line 2132693
    :cond_4
    iput-byte v2, p0, LX/EXA;->memoizedIsInitialized:B

    move v1, v2

    .line 2132694
    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2132756
    iget v1, p0, LX/EXA;->memoizedSerializedSize:I

    .line 2132757
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 2132758
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 2132759
    :goto_1
    iget-object v0, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2132760
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2132761
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2132762
    :cond_1
    invoke-virtual {p0}, LX/EX1;->H()I

    move-result v0

    add-int/2addr v0, v2

    .line 2132763
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2132764
    iput v0, p0, LX/EXA;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2132673
    iget-object v0, p0, LX/EXA;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2132681
    sget-object v0, LX/EYC;->B:LX/EYn;

    const-class v1, LX/EXA;

    const-class v2, LX/EX9;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2132680
    sget-object v0, LX/EXA;->a:LX/EWZ;

    return-object v0
.end method

.method public final j()LX/EX9;
    .locals 1

    .prologue
    .line 2132679
    invoke-static {p0}, LX/EXA;->a(LX/EXA;)LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2132678
    invoke-virtual {p0}, LX/EXA;->j()LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2132677
    invoke-static {}, LX/EX9;->x()LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2132676
    invoke-virtual {p0}, LX/EXA;->j()LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2132675
    sget-object v0, LX/EXA;->c:LX/EXA;

    return-object v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2132674
    invoke-super {p0}, LX/EX1;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
