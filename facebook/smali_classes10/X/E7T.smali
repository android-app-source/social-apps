.class public LX/E7T;
.super Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesInterfaces$MediaFetchFromReactionStory;",
        "LX/1U8;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/facebook/reaction/ReactionUtil;


# direct methods
.method public constructor <init>(LX/0Or;LX/E1i;LX/3Tx;LX/3Tz;Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V
    .locals 6
    .param p6    # Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;",
            "LX/E1i;",
            "LX/3Tx;",
            "LX/3Tz;",
            "Lcom/facebook/reaction/ReactionUtil;",
            "Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081703
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;-><init>(LX/0Or;LX/E1i;LX/3Tx;LX/3Tz;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V

    .line 2081704
    iput-object p5, p0, LX/E7T;->c:Lcom/facebook/reaction/ReactionUtil;

    .line 2081705
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/util/List;
    .locals 8
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesInterfaces$MediaFetchFromReactionStory;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/1U8;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2081706
    if-eqz p1, :cond_0

    .line 2081707
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2081708
    if-nez v0, :cond_1

    .line 2081709
    :cond_0
    :goto_0
    return-object v1

    .line 2081710
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2081711
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2081712
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;

    move-result-object v4

    .line 2081713
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2081714
    invoke-virtual {v4}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_3

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;

    .line 2081715
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kD;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/E7T;->a(LX/1U8;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2081716
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kD;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2081717
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2081718
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;->b()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaFetchFromReactionStoryModel$ReactionAttachmentsModel;->b()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2081719
    :goto_2
    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->h:Ljava/lang/String;

    .line 2081720
    move-object v1, v2

    .line 2081721
    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 2081722
    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V
    .locals 6

    .prologue
    .line 2081723
    iget-object v0, p0, LX/E7T;->c:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->g()LX/0Vd;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    .line 2081724
    iget-object p0, v0, Lcom/facebook/reaction/ReactionUtil;->p:LX/1V6;

    new-instance p1, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {p1, v2}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v4}, LX/1V6;->a(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;)LX/9h1;

    move-result-object p0

    .line 2081725
    invoke-virtual {p0, v3, v1}, LX/9gr;->b(ILjava/lang/String;)LX/0zO;

    move-result-object p0

    sget-object p1, LX/0zS;->d:LX/0zS;

    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p0

    .line 2081726
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    .line 2081727
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    invoke-virtual {p1, v2, p0, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2081728
    return-void
.end method

.method public final a(LX/1U8;)Z
    .locals 1
    .param p1    # LX/1U8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2081729
    invoke-static {p1}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->a(LX/1U8;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)LX/1U8;
    .locals 1

    .prologue
    .line 2081730
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->E()LX/1U8;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)LX/E7X;
    .locals 1

    .prologue
    .line 2081731
    new-instance v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;-><init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
