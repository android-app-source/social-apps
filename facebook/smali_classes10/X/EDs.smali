.class public final enum LX/EDs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDs;

.field public static final enum NONE:LX/EDs;

.field public static final enum NORMAL:LX/EDs;

.field public static final enum RECONNECTED:LX/EDs;

.field public static final enum RECONNECTING:LX/EDs;

.field public static final enum WEAK_CONNECTION:LX/EDs;

.field public static final enum WEAK_VIDEO_CONNECTION:LX/EDs;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2091961
    new-instance v0, LX/EDs;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/EDs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDs;->NONE:LX/EDs;

    .line 2091962
    new-instance v0, LX/EDs;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, LX/EDs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDs;->NORMAL:LX/EDs;

    .line 2091963
    new-instance v0, LX/EDs;

    const-string v1, "WEAK_CONNECTION"

    invoke-direct {v0, v1, v5}, LX/EDs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDs;->WEAK_CONNECTION:LX/EDs;

    .line 2091964
    new-instance v0, LX/EDs;

    const-string v1, "RECONNECTING"

    invoke-direct {v0, v1, v6}, LX/EDs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDs;->RECONNECTING:LX/EDs;

    .line 2091965
    new-instance v0, LX/EDs;

    const-string v1, "RECONNECTED"

    invoke-direct {v0, v1, v7}, LX/EDs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDs;->RECONNECTED:LX/EDs;

    .line 2091966
    new-instance v0, LX/EDs;

    const-string v1, "WEAK_VIDEO_CONNECTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EDs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDs;->WEAK_VIDEO_CONNECTION:LX/EDs;

    .line 2091967
    const/4 v0, 0x6

    new-array v0, v0, [LX/EDs;

    sget-object v1, LX/EDs;->NONE:LX/EDs;

    aput-object v1, v0, v3

    sget-object v1, LX/EDs;->NORMAL:LX/EDs;

    aput-object v1, v0, v4

    sget-object v1, LX/EDs;->WEAK_CONNECTION:LX/EDs;

    aput-object v1, v0, v5

    sget-object v1, LX/EDs;->RECONNECTING:LX/EDs;

    aput-object v1, v0, v6

    sget-object v1, LX/EDs;->RECONNECTED:LX/EDs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EDs;->WEAK_VIDEO_CONNECTION:LX/EDs;

    aput-object v2, v0, v1

    sput-object v0, LX/EDs;->$VALUES:[LX/EDs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2091960
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDs;
    .locals 1

    .prologue
    .line 2091958
    const-class v0, LX/EDs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDs;

    return-object v0
.end method

.method public static values()[LX/EDs;
    .locals 1

    .prologue
    .line 2091959
    sget-object v0, LX/EDs;->$VALUES:[LX/EDs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDs;

    return-object v0
.end method
