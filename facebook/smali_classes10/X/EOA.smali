.class public final LX/EOA;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EOC;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/C33;

.field public b:LX/1Ps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public final synthetic e:LX/EOC;


# direct methods
.method public constructor <init>(LX/EOC;)V
    .locals 1

    .prologue
    .line 2114026
    iput-object p1, p0, LX/EOA;->e:LX/EOC;

    .line 2114027
    move-object v0, p1

    .line 2114028
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2114029
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EOA;->c:Z

    .line 2114030
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2114031
    const-string v0, "SearchResultsOpinionSearchQueryStoryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2114032
    if-ne p0, p1, :cond_1

    .line 2114033
    :cond_0
    :goto_0
    return v0

    .line 2114034
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2114035
    goto :goto_0

    .line 2114036
    :cond_3
    check-cast p1, LX/EOA;

    .line 2114037
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2114038
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2114039
    if-eq v2, v3, :cond_0

    .line 2114040
    iget-object v2, p0, LX/EOA;->a:LX/C33;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EOA;->a:LX/C33;

    iget-object v3, p1, LX/EOA;->a:LX/C33;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2114041
    goto :goto_0

    .line 2114042
    :cond_5
    iget-object v2, p1, LX/EOA;->a:LX/C33;

    if-nez v2, :cond_4

    .line 2114043
    :cond_6
    iget-object v2, p0, LX/EOA;->b:LX/1Ps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EOA;->b:LX/1Ps;

    iget-object v3, p1, LX/EOA;->b:LX/1Ps;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2114044
    goto :goto_0

    .line 2114045
    :cond_8
    iget-object v2, p1, LX/EOA;->b:LX/1Ps;

    if-nez v2, :cond_7

    .line 2114046
    :cond_9
    iget-boolean v2, p0, LX/EOA;->c:Z

    iget-boolean v3, p1, LX/EOA;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2114047
    goto :goto_0

    .line 2114048
    :cond_a
    iget-boolean v2, p0, LX/EOA;->d:Z

    iget-boolean v3, p1, LX/EOA;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2114049
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2114050
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/EOA;

    .line 2114051
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/EOA;->d:Z

    .line 2114052
    return-object v0
.end method
