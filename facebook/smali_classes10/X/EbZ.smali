.class public LX/EbZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Eaf;

.field public final b:LX/Eau;

.field public final c:LX/Ecs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ecs",
            "<",
            "LX/Eau;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/Eau;

.field public final e:LX/Eae;

.field public final f:LX/Eat;


# direct methods
.method public constructor <init>(LX/Eaf;LX/Eau;LX/Eau;LX/Ecs;LX/Eae;LX/Eat;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Eaf;",
            "LX/Eau;",
            "LX/Eau;",
            "LX/Ecs",
            "<",
            "LX/Eau;",
            ">;",
            "LX/Eae;",
            "Lorg/whispersystems/libsignal/ecc/ECPublicKey;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2144049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144050
    iput-object p1, p0, LX/EbZ;->a:LX/Eaf;

    .line 2144051
    iput-object p2, p0, LX/EbZ;->b:LX/Eau;

    .line 2144052
    iput-object p3, p0, LX/EbZ;->d:LX/Eau;

    .line 2144053
    iput-object p4, p0, LX/EbZ;->c:LX/Ecs;

    .line 2144054
    iput-object p5, p0, LX/EbZ;->e:LX/Eae;

    .line 2144055
    iput-object p6, p0, LX/EbZ;->f:LX/Eat;

    .line 2144056
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-nez p6, :cond_1

    .line 2144057
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null value!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2144058
    :cond_1
    return-void
.end method
