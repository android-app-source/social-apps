.class public final LX/DFu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DFw;

.field public final synthetic b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;LX/DFw;)V
    .locals 0

    .prologue
    .line 1978933
    iput-object p1, p0, LX/DFu;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iput-object p2, p0, LX/DFu;->a:LX/DFw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x636b66e2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978934
    iget-object v1, p0, LX/DFu;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v2, p0, LX/DFu;->a:LX/DFw;

    iget-object v2, v2, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v1, v2}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->a(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;

    move-result-object v1

    .line 1978935
    iget-object v2, p0, LX/DFu;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    .line 1978936
    iget-object v3, v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "key_x_out_mutation"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v5, v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->i:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/DFq;

    .line 1978937
    iget-object v8, v5, LX/DFq;->a:LX/0tX;

    .line 1978938
    new-instance v7, LX/81h;

    invoke-direct {v7}, LX/81h;-><init>()V

    move-object v7, v7

    .line 1978939
    const-string v9, "input"

    new-instance v10, LX/4IN;

    invoke-direct {v10}, LX/4IN;-><init>()V

    iget-object p1, v5, LX/DFq;->b:Ljava/lang/String;

    .line 1978940
    const-string v5, "actor_id"

    invoke-virtual {v10, v5, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1978941
    move-object v10, v10

    .line 1978942
    const-string p1, "place_id"

    invoke-virtual {v10, p1, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1978943
    move-object v10, v10

    .line 1978944
    const-string p1, "EGO_MOBILE"

    .line 1978945
    const-string v5, "surface"

    invoke-virtual {v10, v5, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1978946
    move-object v10, v10

    .line 1978947
    invoke-virtual {v7, v9, v10}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/81h;

    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v5, v7

    .line 1978948
    new-instance v7, LX/DFv;

    invoke-direct {v7, v2}, LX/DFv;-><init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;)V

    invoke-virtual {v3, v6, v5, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1978949
    iget-object v2, p0, LX/DFu;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v3, p0, LX/DFu;->a:LX/DFw;

    .line 1978950
    iget-object v5, v3, LX/DFw;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    invoke-static {v5}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v5

    if-nez v5, :cond_0

    .line 1978951
    :goto_0
    const v1, -0xfa62045

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1978952
    :cond_0
    iget-object v5, v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->g:LX/0bH;

    new-instance v6, LX/1Nj;

    iget-object v7, v3, LX/DFw;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v1}, LX/1Nj;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
