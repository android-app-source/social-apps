.class public LX/E7Q;
.super LX/Cft;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cft",
        "<",
        "LX/E97;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/E1i;

.field private final b:Landroid/text/style/StyleSpan;

.field private final c:LX/8sz;


# direct methods
.method public constructor <init>(LX/E1i;LX/3Tx;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081540
    invoke-direct {p0, p2}, LX/Cft;-><init>(LX/3Tx;)V

    .line 2081541
    new-instance v0, LX/8sz;

    invoke-direct {v0}, LX/8sz;-><init>()V

    iput-object v0, p0, LX/E7Q;->c:LX/8sz;

    .line 2081542
    iput-object p1, p0, LX/E7Q;->a:LX/E1i;

    .line 2081543
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object v0, p0, LX/E7Q;->b:Landroid/text/style/StyleSpan;

    .line 2081544
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 3

    .prologue
    .line 2081545
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;

    move-result-object v0

    .line 2081546
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2081547
    const/4 v0, 0x0

    .line 2081548
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/E7Q;->a:LX/E1i;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/E1i;->f(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1a1;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    .line 2081549
    check-cast p1, LX/E97;

    .line 2081550
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2081551
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;

    move-result-object v1

    .line 2081552
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 2081553
    new-instance v3, Landroid/text/SpannableStringBuilder;

    const-string v4, "%s: %s"

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->gU_()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v2, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2081554
    iget-object v4, p0, LX/E7Q;->b:Landroid/text/style/StyleSpan;

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const/16 v6, 0x11

    invoke-virtual {v3, v4, v5, v2, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2081555
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2081556
    const v2, 0x7f0e0906

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2081557
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->e()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2081558
    const v1, 0x7f0a010a

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2081559
    iget-object v1, p0, LX/E7Q;->c:LX/8sz;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2081560
    return-void
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 2

    .prologue
    .line 2081561
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;

    move-result-object v0

    .line 2081562
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->gU_()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->e()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryTopicAttachmentFragmentModel$TopicModel;->e()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/1a1;
    .locals 2

    .prologue
    .line 2081563
    new-instance v0, LX/E97;

    const v1, 0x7f031181

    invoke-virtual {p0, v1}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E97;-><init>(Landroid/view/View;)V

    return-object v0
.end method
