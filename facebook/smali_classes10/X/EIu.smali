.class public LX/EIu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "LX/EIt;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2102520
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2102521
    check-cast p1, Ljava/lang/String;

    .line 2102522
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2102523
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "fields"

    const-string v3, "custom_voicemail"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2102524
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "custom_voicemail_read"

    .line 2102525
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2102526
    move-object v1, v1

    .line 2102527
    const-string v2, "GET"

    .line 2102528
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2102529
    move-object v1, v1

    .line 2102530
    iput-object p1, v1, LX/14O;->d:Ljava/lang/String;

    .line 2102531
    move-object v1, v1

    .line 2102532
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2102533
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2102534
    move-object v0, v1

    .line 2102535
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2102536
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2102537
    move-object v0, v0

    .line 2102538
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2102539
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2102540
    const-string v1, "custom_voicemail"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2102541
    const/4 v0, 0x0

    .line 2102542
    :goto_0
    return-object v0

    .line 2102543
    :cond_0
    const-string v1, "custom_voicemail"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2102544
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    .line 2102545
    const-string v2, "extension"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    .line 2102546
    const-string v3, "uri"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v3

    .line 2102547
    new-instance v0, LX/EIt;

    invoke-direct {v0, v1, v2, v3}, LX/EIt;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
