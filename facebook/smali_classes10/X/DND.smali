.class public final LX/DND;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:LX/DNJ;


# direct methods
.method public constructor <init>(LX/DNJ;)V
    .locals 0

    .prologue
    .line 1991057
    iput-object p1, p0, LX/DND;->a:LX/DNJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1991058
    packed-switch p2, :pswitch_data_0

    .line 1991059
    :goto_0
    return-void

    .line 1991060
    :pswitch_0
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    invoke-static {v0}, LX/DNJ;->l(LX/DNJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991061
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    iget-object v0, v0, LX/DNJ;->l:LX/1Kt;

    invoke-virtual {v0, p1}, LX/1Kt;->b(LX/0g8;)V

    .line 1991062
    :cond_0
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    const/4 v1, 0x0

    .line 1991063
    iput-boolean v1, v0, LX/DNJ;->L:Z

    .line 1991064
    goto :goto_0

    .line 1991065
    :pswitch_1
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    .line 1991066
    iput-boolean v1, v0, LX/DNJ;->K:Z

    .line 1991067
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    .line 1991068
    iput-boolean v1, v0, LX/DNJ;->L:Z

    .line 1991069
    goto :goto_0

    .line 1991070
    :pswitch_2
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    .line 1991071
    iput-boolean v1, v0, LX/DNJ;->L:Z

    .line 1991072
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/0g8;III)V
    .locals 2

    .prologue
    .line 1991073
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    .line 1991074
    const/4 v1, 0x0

    .line 1991075
    iget-object p1, v0, LX/DNJ;->v:LX/1Qq;

    invoke-interface {p1}, LX/1Qq;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 1991076
    :cond_0
    :goto_0
    move v1, v1

    .line 1991077
    if-eqz v1, :cond_1

    .line 1991078
    iget-object v1, v0, LX/DNJ;->F:LX/DNR;

    invoke-virtual {v1}, LX/DNR;->d()V

    .line 1991079
    :cond_1
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    iget-boolean v0, v0, LX/DNJ;->K:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    iget-boolean v0, v0, LX/DNJ;->L:Z

    if-eqz v0, :cond_3

    .line 1991080
    :cond_2
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    invoke-static {v0}, LX/DNJ;->l(LX/DNJ;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1991081
    iget-object v0, p0, LX/DND;->a:LX/DNJ;

    iget-object v0, v0, LX/DNJ;->l:LX/1Kt;

    iget-object v1, p0, LX/DND;->a:LX/DNJ;

    iget-object v1, v1, LX/DNJ;->B:LX/2iI;

    invoke-interface {v0, v1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    .line 1991082
    :cond_3
    return-void

    .line 1991083
    :cond_4
    if-lez p3, :cond_0

    if-lez p4, :cond_0

    .line 1991084
    add-int p1, p2, p3

    add-int/lit8 p1, p1, 0x5

    if-le p1, p4, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method
