.class public LX/EpN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BS1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/5wM;",
        ">",
        "Ljava/lang/Object;",
        "LX/BS1;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/EpL;

.field public final c:LX/2h7;

.field public final d:LX/5P2;

.field public e:LX/2do;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final f:LX/Emj;

.field public final g:LX/BRt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BRt",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final h:LX/EpB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EpB",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final i:LX/1R7;

.field private j:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/EnL;


# direct methods
.method public constructor <init>(LX/Emj;LX/EnL;LX/2h7;LX/5P2;LX/BRt;LX/EpB;)V
    .locals 1
    .param p1    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/EnL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2h7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/5P2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/BRt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/EpB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Emj;",
            "Lcom/facebook/entitycards/controller/EntityCardsActivityController;",
            "LX/2h7;",
            "LX/5P2;",
            "LX/BRt",
            "<TT;>;",
            "LX/EpB",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2170219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170220
    new-instance v0, LX/EpM;

    invoke-direct {v0, p0}, LX/EpM;-><init>(LX/EpN;)V

    iput-object v0, p0, LX/EpN;->b:LX/EpL;

    .line 2170221
    const/4 v0, 0x6

    invoke-static {v0}, LX/1R7;->a(I)LX/1R7;

    move-result-object v0

    iput-object v0, p0, LX/EpN;->i:LX/1R7;

    .line 2170222
    iput-object p1, p0, LX/EpN;->f:LX/Emj;

    .line 2170223
    iput-object p2, p0, LX/EpN;->k:LX/EnL;

    .line 2170224
    iput-object p3, p0, LX/EpN;->c:LX/2h7;

    .line 2170225
    iput-object p4, p0, LX/EpN;->d:LX/5P2;

    .line 2170226
    iput-object p5, p0, LX/EpN;->g:LX/BRt;

    .line 2170227
    iput-object p6, p0, LX/EpN;->h:LX/EpB;

    .line 2170228
    return-void
.end method


# virtual methods
.method public final a(IIIIZZ)V
    .locals 9
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 2170229
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v8, v7

    invoke-virtual/range {v0 .. v8}, LX/EpN;->a(IIIIZZZZ)V

    .line 2170230
    return-void
.end method

.method public final a(IIIIZZZZ)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2170231
    if-nez p6, :cond_1

    .line 2170232
    :cond_0
    :goto_0
    return-void

    .line 2170233
    :cond_1
    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0xb

    if-eq p1, v0, :cond_0

    const/16 v0, 0x9

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    .line 2170234
    iget-object v0, p0, LX/EpN;->i:LX/1R7;

    invoke-virtual {v0, p1}, LX/1R7;->b(I)V

    .line 2170235
    iget-object v0, p0, LX/EpN;->j:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    invoke-virtual {v0, v1, p1, v1, p2}, LX/AhO;->a(IIII)LX/3qv;

    move-result-object v0

    invoke-interface {v0, p4}, LX/3qv;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p7}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p8}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public final a(LX/5wM;Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2170236
    invoke-virtual {p2, p1}, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->setTag(Ljava/lang/Object;)V

    .line 2170237
    invoke-virtual {p2, p0}, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;->setPresenter(LX/EpN;)V

    .line 2170238
    iput-object p2, p0, LX/EpN;->j:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    .line 2170239
    invoke-virtual {p2}, LX/AhO;->b()V

    .line 2170240
    invoke-virtual {p2}, LX/AhO;->clear()V

    .line 2170241
    iget-object v0, p0, LX/EpN;->i:LX/1R7;

    invoke-virtual {v0}, LX/1R7;->c()V

    .line 2170242
    iget-object v0, p0, LX/EpN;->g:LX/BRt;

    const/4 v1, 0x1

    invoke-interface {v0, p1, p0, v1}, LX/BRt;->a(Ljava/lang/Object;LX/BS1;Z)V

    .line 2170243
    invoke-virtual {p2}, LX/AhO;->d()V

    .line 2170244
    iget-object v0, p0, LX/EpN;->f:LX/Emj;

    const-string v1, "ec_config_action_bar"

    sget-object v2, LX/EnC;->SUCCEEDED:LX/EnC;

    invoke-interface {p1}, LX/5wM;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, LX/Emj;->a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V

    .line 2170245
    return-void
.end method
