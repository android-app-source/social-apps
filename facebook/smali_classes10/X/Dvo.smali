.class public LX/Dvo;
.super LX/Dvj;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvs;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvl;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvq;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/Boolean;)V
    .locals 1
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dvs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvl;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvq;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2059260
    invoke-direct {p0}, LX/Dvj;-><init>()V

    .line 2059261
    iput-object p1, p0, LX/Dvo;->a:LX/0Ot;

    .line 2059262
    iput-object p2, p0, LX/Dvo;->b:LX/0Ot;

    .line 2059263
    iput-object p3, p0, LX/Dvo;->c:LX/0Ot;

    .line 2059264
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Dvo;->d:Z

    .line 2059265
    return-void
.end method

.method private static a(LX/Dvo;LX/0Px;LX/Dvm;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;",
            ">;",
            "LX/Dvm;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2059266
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2059267
    const/4 v3, 0x0

    .line 2059268
    const/4 v2, 0x0

    .line 2059269
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2059270
    invoke-direct {p0, p2}, LX/Dvo;->a(LX/Dvm;)Ljava/util/List;

    move-result-object v0

    .line 2059271
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2059272
    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2059273
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2059274
    :cond_0
    invoke-interface {v5, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2059275
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2059276
    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    goto :goto_0

    .line 2059277
    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 2059278
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2059279
    if-eqz v3, :cond_2

    iget-object v6, v3, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v6}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->q()Z

    move-result v6

    iget-object v7, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v7}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->q()Z

    move-result v7

    if-ne v6, v7, :cond_3

    iget-wide v6, v3, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->b:D

    iget-wide v8, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->b:D

    cmpg-double v6, v6, v8

    if-gez v6, :cond_3

    :cond_2
    iget-wide v6, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->b:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v6, v6, v8

    if-gtz v6, :cond_4

    :cond_3
    iget-boolean v6, p0, LX/Dvo;->d:Z

    if-eqz v6, :cond_5

    iget-object v6, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v6}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->q()Z

    move-result v6

    if-eqz v6, :cond_5

    :cond_4
    move-object v3, v0

    .line 2059280
    :cond_5
    add-int/lit8 v2, v2, 0x1

    .line 2059281
    const/4 v6, 0x6

    if-eq v2, v6, :cond_6

    invoke-interface {v5, v0}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne v0, v6, :cond_11

    .line 2059282
    :cond_6
    const/4 v2, 0x0

    .line 2059283
    if-eqz v3, :cond_10

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq v1, v0, :cond_7

    add-int/lit8 v0, v1, 0x1

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->q()Z

    move-result v0

    if-nez v0, :cond_10

    .line 2059284
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    .line 2059285
    iget-object v0, v3, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    sget-object v3, LX/Dvw;->SQUARE:LX/Dvw;

    if-eq v0, v3, :cond_10

    .line 2059286
    add-int/lit8 v0, v1, 0x1

    .line 2059287
    :goto_2
    const/4 v1, 0x0

    move v10, v0

    move v0, v2

    move-object v2, v1

    move v1, v10

    .line 2059288
    :goto_3
    add-int/lit8 v1, v1, 0x1

    move-object v3, v2

    move v2, v0

    goto :goto_1

    .line 2059289
    :cond_8
    const/4 v0, 0x3

    .line 2059290
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2059291
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2059292
    iget-boolean v5, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    if-eqz v5, :cond_9

    iget-object v5, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    sget-object v6, LX/Dvw;->LANDSCAPE:LX/Dvw;

    if-ne v5, v6, :cond_9

    .line 2059293
    new-instance v5, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;-><init>(LX/0Px;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2059294
    :cond_9
    iget-boolean v5, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    if-eqz v5, :cond_a

    iget-object v5, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    sget-object v6, LX/Dvw;->PORTRAIT:LX/Dvw;

    if-ne v5, v6, :cond_a

    .line 2059295
    const/4 v1, 0x4

    .line 2059296
    :cond_a
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2059297
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_f

    .line 2059298
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;-><init>(LX/0Px;)V

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2059299
    const/4 v0, 0x3

    .line 2059300
    invoke-interface {v2}, Ljava/util/List;->clear()V

    :goto_5
    move v1, v0

    .line 2059301
    goto :goto_4

    .line 2059302
    :cond_b
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 2059303
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;-><init>(LX/0Px;)V

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2059304
    :cond_c
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2059305
    iget-boolean v1, p2, LX/Dvm;->e:Z

    if-nez v1, :cond_e

    .line 2059306
    :goto_6
    return-object v0

    .line 2059307
    :goto_7
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    .line 2059308
    iget-object v2, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_d

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_d

    .line 2059309
    invoke-direct {p0, p2}, LX/Dvo;->a(LX/Dvm;)Ljava/util/List;

    move-result-object v2

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2059310
    const/4 v0, 0x0

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v0, v2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 2059311
    goto :goto_7

    :cond_d
    move-object v0, v1

    .line 2059312
    goto :goto_6

    :cond_e
    move-object v1, v0

    goto :goto_7

    :cond_f
    move v0, v1

    goto :goto_5

    :cond_10
    move v0, v1

    goto/16 :goto_2

    :cond_11
    move v0, v2

    move-object v2, v3

    goto/16 :goto_3
.end method

.method private a(LX/Dvm;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dvm;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2059313
    iget-object v0, p0, LX/Dvo;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2059314
    if-nez p1, :cond_0

    .line 2059315
    const/4 v0, 0x0

    .line 2059316
    :goto_0
    move-object v0, v0

    .line 2059317
    return-object v0

    .line 2059318
    :cond_0
    sget-object v0, LX/Dvq;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2059319
    if-nez v0, :cond_1

    .line 2059320
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2059321
    :cond_1
    sget-object p0, LX/Dvq;->a:LX/0aq;

    invoke-virtual {p0, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Dvm;LX/0Px;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dvm;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2059322
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2059323
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2059324
    :goto_0
    return-object v0

    .line 2059325
    :cond_1
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2059326
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 2059327
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v7, v0

    :goto_1
    if-ge v7, v9, :cond_4

    invoke-virtual {p2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    .line 2059328
    invoke-virtual {v5}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;->a()LX/Dv3;

    move-result-object v0

    sget-object v1, LX/Dv3;->SINGLE_MEDIA:LX/Dv3;

    if-ne v0, v1, :cond_2

    move-object v2, v5

    .line 2059329
    check-cast v2, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    .line 2059330
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    iget-object v1, v2, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    iget-object v3, p0, LX/Dvo;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v2, v2, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-static {v2}, LX/Dvs;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;)D

    move-result-wide v2

    move-object v4, v5

    check-cast v4, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    iget-object v4, v4, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->b:Ljava/lang/String;

    check-cast v5, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    iget-object v5, v5, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;DLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-object v0, v6

    .line 2059331
    :goto_2
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move-object v6, v0

    goto :goto_1

    .line 2059332
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;->a()LX/Dv3;

    move-result-object v0

    sget-object v1, LX/Dv3;->MULTI_PHOTO_STORY:LX/Dv3;

    if-ne v0, v1, :cond_6

    .line 2059333
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2059334
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2059335
    invoke-static {p0, v0, p1}, LX/Dvo;->a(LX/Dvo;LX/0Px;LX/Dvm;)LX/0Px;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2059336
    :cond_3
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2059337
    iget-object v0, p0, LX/Dvo;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dvl;

    check-cast v5, Lcom/facebook/photos/pandora/common/data/model/PandoraMultiPhotoStoryModel;

    invoke-virtual {v0, v5}, LX/Dvl;->a(Lcom/facebook/photos/pandora/common/data/model/PandoraMultiPhotoStoryModel;)LX/0Px;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-object v0, v1

    goto :goto_2

    .line 2059338
    :cond_4
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2059339
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2059340
    invoke-static {p0, v0, p1}, LX/Dvo;->a(LX/Dvo;LX/0Px;LX/Dvm;)LX/0Px;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2059341
    :cond_5
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move-object v0, v6

    goto :goto_2
.end method
