.class public LX/CvK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0kb;


# direct methods
.method public constructor <init>(LX/0kb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1948059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1948060
    iput-object p1, p0, LX/CvK;->a:LX/0kb;

    .line 1948061
    return-void
.end method

.method public static b(LX/0QB;)LX/CvK;
    .locals 2

    .prologue
    .line 1948062
    new-instance v1, LX/CvK;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-direct {v1, v0}, LX/CvK;-><init>(LX/0kb;)V

    .line 1948063
    return-object v1
.end method


# virtual methods
.method public final a(LX/0v6;)V
    .locals 4

    .prologue
    .line 1948064
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "X-FB-Search-SERP"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-FB-Connection-Subtype"

    iget-object v3, p0, LX/CvK;->a:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1948065
    iput-object v0, p1, LX/0v6;->k:LX/0Px;

    .line 1948066
    return-void
.end method

.method public final a(LX/0zO;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1948067
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "X-FB-Search-SERP"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-FB-Connection-Subtype"

    iget-object v3, p0, LX/CvK;->a:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1948068
    iput-object v0, p1, LX/0zO;->f:LX/0Px;

    .line 1948069
    return-void
.end method
