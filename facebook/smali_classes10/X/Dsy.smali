.class public LX/Dsy;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Dsy;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2050990
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2050991
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2050992
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->PAGE_FRIEND_INVITER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2050993
    sget-object v1, LX/0ax;->bo:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "{page_id}"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2050994
    return-void
.end method

.method public static a(LX/0QB;)LX/Dsy;
    .locals 3

    .prologue
    .line 2050995
    sget-object v0, LX/Dsy;->a:LX/Dsy;

    if-nez v0, :cond_1

    .line 2050996
    const-class v1, LX/Dsy;

    monitor-enter v1

    .line 2050997
    :try_start_0
    sget-object v0, LX/Dsy;->a:LX/Dsy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2050998
    if-eqz v2, :cond_0

    .line 2050999
    :try_start_1
    new-instance v0, LX/Dsy;

    invoke-direct {v0}, LX/Dsy;-><init>()V

    .line 2051000
    move-object v0, v0

    .line 2051001
    sput-object v0, LX/Dsy;->a:LX/Dsy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2051002
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2051003
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2051004
    :cond_1
    sget-object v0, LX/Dsy;->a:LX/Dsy;

    return-object v0

    .line 2051005
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2051006
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
