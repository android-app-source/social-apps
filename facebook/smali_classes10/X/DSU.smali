.class public final LX/DSU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V
    .locals 0

    .prologue
    .line 1999482
    iput-object p1, p0, LX/DSU;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x310d3dc8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1999483
    const-string v1, "extra_request_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1999484
    if-nez v1, :cond_0

    .line 1999485
    sget-object v1, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->x:Ljava/lang/Class;

    const-string v2, "There is no composer session id"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1999486
    const/16 v1, 0x27

    const v2, 0x5d5af87e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1999487
    :goto_0
    return-void

    .line 1999488
    :cond_0
    iget-object v2, p0, LX/DSU;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->w:LX/CGV;

    .line 1999489
    iget-object v3, v2, LX/CGV;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1999490
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1999491
    iget-object v1, p0, LX/DSU;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    .line 1999492
    invoke-virtual {v1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d()LX/DSA;

    move-result-object v2

    .line 1999493
    iget-object v3, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    move-object v3, v3

    .line 1999494
    invoke-interface {v2, v3}, LX/DSA;->a(Lcom/facebook/widget/listview/BetterListView;)V

    .line 1999495
    :cond_1
    const v1, 0x5d9eaff3

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
