.class public LX/DzG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6ZZ;
.implements LX/6Zx;


# static fields
.field public static final f:[Ljava/lang/String;


# instance fields
.field private final A:LX/AhV;

.field private B:LX/DzJ;

.field private C:LX/Dz8;

.field private D:LX/Dz7;

.field public a:Ljava/lang/ref/WeakReference;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0y3;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public c:LX/9j5;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public d:LX/0if;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public e:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public g:LX/0i4;

.field public h:LX/0i5;

.field public i:LX/0ad;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field private l:Landroid/view/View;

.field private m:LX/9jJ;

.field private n:Lcom/facebook/places/checkin/PlacePickerFragment;

.field public o:LX/6Zb;

.field public p:LX/0kL;

.field public q:Lcom/facebook/content/SecureContextHelper;

.field private r:LX/0Xl;

.field public s:LX/0Yb;

.field private t:LX/1Ml;

.field public u:Ljava/lang/String;

.field private final v:LX/0YZ;

.field private final w:Landroid/view/View$OnClickListener;

.field private final x:Landroid/view/View$OnClickListener;

.field private final y:Landroid/view/View$OnClickListener;

.field private final z:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2066664
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, LX/DzG;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/9jJ;Landroid/app/Activity;Lcom/facebook/places/checkin/PlacePickerFragment;LX/6Zb;LX/0ad;LX/9j5;LX/0y3;LX/0Xl;LX/1Ml;LX/0i4;LX/0s6;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/0if;LX/DzJ;LX/Dz8;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9jJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/places/checkin/PlacePickerFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/6Zb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2066622
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/DzG;->e:Z

    .line 2066623
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/DzG;->a:Ljava/lang/ref/WeakReference;

    .line 2066624
    iput-object p2, p0, LX/DzG;->m:LX/9jJ;

    .line 2066625
    iput-object p4, p0, LX/DzG;->n:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2066626
    iput-object p5, p0, LX/DzG;->o:LX/6Zb;

    .line 2066627
    iput-object p6, p0, LX/DzG;->i:LX/0ad;

    .line 2066628
    iput-object p7, p0, LX/DzG;->c:LX/9j5;

    .line 2066629
    iput-object p8, p0, LX/DzG;->b:LX/0y3;

    .line 2066630
    iput-object p9, p0, LX/DzG;->r:LX/0Xl;

    .line 2066631
    iput-object p10, p0, LX/DzG;->t:LX/1Ml;

    .line 2066632
    iput-object p11, p0, LX/DzG;->g:LX/0i4;

    .line 2066633
    move-object/from16 v0, p13

    iput-object v0, p0, LX/DzG;->p:LX/0kL;

    .line 2066634
    move-object/from16 v0, p14

    iput-object v0, p0, LX/DzG;->q:Lcom/facebook/content/SecureContextHelper;

    .line 2066635
    move-object/from16 v0, p15

    iput-object v0, p0, LX/DzG;->d:LX/0if;

    .line 2066636
    move-object/from16 v0, p16

    iput-object v0, p0, LX/DzG;->B:LX/DzJ;

    .line 2066637
    move-object/from16 v0, p17

    iput-object v0, p0, LX/DzG;->C:LX/Dz8;

    .line 2066638
    iput-object p1, p0, LX/DzG;->l:Landroid/view/View;

    .line 2066639
    invoke-virtual {p3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2066640
    invoke-virtual {p3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p12

    invoke-virtual {v0, v2, v3}, LX/01H;->c(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 2066641
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, LX/DzG;->u:Ljava/lang/String;

    .line 2066642
    iget-object v1, p0, LX/DzG;->u:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2066643
    const-string v1, "Facebook"

    iput-object v1, p0, LX/DzG;->u:Ljava/lang/String;

    .line 2066644
    :cond_0
    invoke-direct {p0}, LX/DzG;->p()LX/0YZ;

    move-result-object v1

    iput-object v1, p0, LX/DzG;->v:LX/0YZ;

    .line 2066645
    invoke-direct {p0}, LX/DzG;->q()Landroid/view/View$OnClickListener;

    move-result-object v1

    iput-object v1, p0, LX/DzG;->w:Landroid/view/View$OnClickListener;

    .line 2066646
    invoke-direct {p0}, LX/DzG;->r()Landroid/view/View$OnClickListener;

    move-result-object v1

    iput-object v1, p0, LX/DzG;->x:Landroid/view/View$OnClickListener;

    .line 2066647
    invoke-direct {p0}, LX/DzG;->s()Landroid/view/View$OnClickListener;

    move-result-object v1

    iput-object v1, p0, LX/DzG;->y:Landroid/view/View$OnClickListener;

    .line 2066648
    invoke-direct {p0}, LX/DzG;->t()Landroid/view/View$OnClickListener;

    move-result-object v1

    iput-object v1, p0, LX/DzG;->z:Landroid/view/View$OnClickListener;

    .line 2066649
    invoke-direct {p0}, LX/DzG;->u()LX/AhV;

    move-result-object v1

    iput-object v1, p0, LX/DzG;->A:LX/AhV;

    .line 2066650
    return-void
.end method

.method public static a$redex0(LX/DzG;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2066651
    iget-object v0, p0, LX/DzG;->d:LX/0if;

    sget-object v1, LX/0ig;->ay:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2066652
    return-void
.end method

.method private p()LX/0YZ;
    .locals 1

    .prologue
    .line 2066653
    new-instance v0, LX/Dz9;

    invoke-direct {v0, p0}, LX/Dz9;-><init>(LX/DzG;)V

    return-object v0
.end method

.method private q()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2066654
    new-instance v0, LX/DzA;

    invoke-direct {v0, p0}, LX/DzA;-><init>(LX/DzG;)V

    return-object v0
.end method

.method private r()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2066655
    new-instance v0, LX/DzB;

    invoke-direct {v0, p0}, LX/DzB;-><init>(LX/DzG;)V

    return-object v0
.end method

.method private s()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2066656
    new-instance v0, LX/DzC;

    invoke-direct {v0, p0}, LX/DzC;-><init>(LX/DzG;)V

    return-object v0
.end method

.method private t()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2066657
    new-instance v0, LX/DzD;

    invoke-direct {v0, p0}, LX/DzD;-><init>(LX/DzG;)V

    return-object v0
.end method

.method private u()LX/AhV;
    .locals 1

    .prologue
    .line 2066658
    new-instance v0, LX/DzE;

    invoke-direct {v0, p0}, LX/DzE;-><init>(LX/DzG;)V

    return-object v0
.end method

.method private v()V
    .locals 3

    .prologue
    .line 2066659
    iget-boolean v0, p0, LX/DzG;->e:Z

    if-nez v0, :cond_0

    .line 2066660
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DzG;->e:Z

    .line 2066661
    iget-object v0, p0, LX/DzG;->r:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "android.location.PROVIDERS_CHANGED"

    iget-object v2, p0, LX/DzG;->v:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    iget-object v2, p0, LX/DzG;->v:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/DzG;->s:LX/0Yb;

    .line 2066662
    iget-object v0, p0, LX/DzG;->s:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2066663
    :cond_0
    return-void
.end method

.method private w()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2066603
    invoke-virtual {p0}, LX/DzG;->j()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2066604
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v1

    iget-object v2, p0, LX/DzG;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, LX/Dz7;->f(Landroid/view/View$OnClickListener;)V

    .line 2066605
    const-string v1, "permissions_niem"

    invoke-static {p0, v1}, LX/DzG;->a$redex0(LX/DzG;Ljava/lang/String;)V

    .line 2066606
    :goto_0
    return v0

    .line 2066607
    :cond_0
    invoke-virtual {p0}, LX/DzG;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, LX/DzG;->k()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2066608
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v1

    iget-object v2, p0, LX/DzG;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, LX/Dz7;->e(Landroid/view/View$OnClickListener;)V

    .line 2066609
    const-string v1, "airplane_niem"

    invoke-static {p0, v1}, LX/DzG;->a$redex0(LX/DzG;Ljava/lang/String;)V

    goto :goto_0

    .line 2066610
    :cond_1
    invoke-virtual {p0}, LX/DzG;->k()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2066611
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v1

    iget-object v2, p0, LX/DzG;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, LX/Dz7;->d(Landroid/view/View$OnClickListener;)V

    .line 2066612
    const-string v1, "network_niem"

    invoke-static {p0, v1}, LX/DzG;->a$redex0(LX/DzG;Ljava/lang/String;)V

    goto :goto_0

    .line 2066613
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(LX/DzG;)V
    .locals 4

    .prologue
    .line 2066665
    iget-object v0, p0, LX/DzG;->n:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2066666
    sget-object v1, LX/Dys;->NIEM_CLICKED:LX/Dys;

    iput-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->T:LX/Dys;

    .line 2066667
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2066668
    iget-object v2, v1, LX/9j5;->a:LX/0Zb;

    const-string v3, "place_picker_app_location_niem_retry"

    invoke-static {v1, v3}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2066669
    invoke-static {v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->K(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    .line 2066670
    invoke-virtual {p0}, LX/DzG;->g()V

    .line 2066671
    return-void
.end method

.method public static y(LX/DzG;)V
    .locals 3

    .prologue
    .line 2066614
    iget-object v0, p0, LX/DzG;->c:LX/9j5;

    .line 2066615
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_nonintrusive_error_ls_settings_page_shown"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2066616
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2066617
    iget-object v2, p0, LX/DzG;->q:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/DzG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2066618
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2066619
    invoke-static {p0}, LX/DzG;->x(LX/DzG;)V

    .line 2066620
    return-void
.end method

.method public final a(LX/6ZY;)V
    .locals 2

    .prologue
    .line 2066531
    sget-object v0, LX/DzF;->a:[I

    invoke-virtual {p1}, LX/6ZY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2066532
    iget-object v0, p0, LX/DzG;->c:LX/9j5;

    const-string v1, "gms_upsell_result_failed"

    invoke-virtual {v0, v1}, LX/9j5;->g(Ljava/lang/String;)V

    .line 2066533
    invoke-static {p0}, LX/DzG;->y(LX/DzG;)V

    .line 2066534
    :goto_0
    return-void

    .line 2066535
    :pswitch_0
    iget-object v0, p0, LX/DzG;->c:LX/9j5;

    const-string v1, "gms_upsell_result_succeeded"

    invoke-virtual {v0, v1}, LX/9j5;->g(Ljava/lang/String;)V

    goto :goto_0

    .line 2066536
    :pswitch_1
    iget-object v0, p0, LX/DzG;->c:LX/9j5;

    const-string v1, "gms_upsell_result_canceled"

    invoke-virtual {v0, v1}, LX/9j5;->g(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2066537
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2066538
    return-void
.end method

.method public final c()LX/Dz7;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2066539
    iget-object v0, p0, LX/DzG;->D:LX/Dz7;

    if-nez v0, :cond_0

    .line 2066540
    iget-object v0, p0, LX/DzG;->l:Landroid/view/View;

    const v1, 0x7f0d2533

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2066541
    iget-object v1, p0, LX/DzG;->B:LX/DzJ;

    new-instance v2, LX/0zw;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iget-object v0, p0, LX/DzG;->u:Ljava/lang/String;

    .line 2066542
    new-instance v4, LX/DzI;

    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v2, v0, v3}, LX/DzI;-><init>(LX/0zw;Ljava/lang/String;Landroid/content/Context;)V

    .line 2066543
    move-object v0, v4

    .line 2066544
    iput-object v0, p0, LX/DzG;->D:LX/Dz7;

    .line 2066545
    :cond_0
    iget-object v0, p0, LX/DzG;->D:LX/Dz7;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2066546
    invoke-virtual {p0}, LX/DzG;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/DzG;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/DzG;->l()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/DzG;->n()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 2066547
    invoke-direct {p0}, LX/DzG;->v()V

    .line 2066548
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v0

    iget-object v1, p0, LX/DzG;->w:Landroid/view/View$OnClickListener;

    iget-object v2, p0, LX/DzG;->A:LX/AhV;

    invoke-virtual {v0, v1, v2}, LX/Dz7;->a(Landroid/view/View$OnClickListener;LX/AhV;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2066549
    :cond_0
    :goto_0
    return-void

    .line 2066550
    :cond_1
    invoke-direct {p0}, LX/DzG;->w()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2066551
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v0

    iget-object v1, p0, LX/DzG;->w:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, LX/Dz7;->a(Landroid/view/View$OnClickListener;)V

    .line 2066552
    const-string v0, "timeout_niem"

    invoke-static {p0, v0}, LX/DzG;->a$redex0(LX/DzG;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2066553
    invoke-direct {p0}, LX/DzG;->v()V

    .line 2066554
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v0

    iget-object v1, p0, LX/DzG;->w:Landroid/view/View$OnClickListener;

    iget-object v2, p0, LX/DzG;->A:LX/AhV;

    invoke-virtual {v0, v1, v2}, LX/Dz7;->a(Landroid/view/View$OnClickListener;LX/AhV;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2066555
    :cond_0
    :goto_0
    return-void

    .line 2066556
    :cond_1
    invoke-direct {p0}, LX/DzG;->w()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2066557
    invoke-virtual {p0}, LX/DzG;->n()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2066558
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v0

    iget-object v1, p0, LX/DzG;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, LX/Dz7;->c(Landroid/view/View$OnClickListener;)V

    .line 2066559
    iget-object v0, p0, LX/DzG;->c:LX/9j5;

    .line 2066560
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_nonintrusive_error_no_location"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2066561
    const-string v0, "location_disabled_niem"

    invoke-static {p0, v0}, LX/DzG;->a$redex0(LX/DzG;Ljava/lang/String;)V

    goto :goto_0

    .line 2066562
    :cond_2
    invoke-virtual {p0}, LX/DzG;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2066563
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v0

    iget-object v1, p0, LX/DzG;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, LX/Dz7;->b(Landroid/view/View$OnClickListener;)V

    .line 2066564
    iget-object v0, p0, LX/DzG;->c:LX/9j5;

    .line 2066565
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_nonintrusive_error_no_wireless"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2066566
    const-string v0, "non_optimal_niem"

    invoke-static {p0, v0}, LX/DzG;->a$redex0(LX/DzG;Ljava/lang/String;)V

    goto :goto_0

    .line 2066567
    :cond_3
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v0

    invoke-virtual {v0}, LX/Dz7;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2066568
    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v0

    invoke-virtual {v0}, LX/Dz7;->a()V

    goto :goto_0
.end method

.method public i()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2066569
    iget-object v0, p0, LX/DzG;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2066570
    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 2066571
    :goto_1
    return v0

    .line 2066572
    :cond_0
    iget-object v0, p0, LX/DzG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    .line 2066573
    :cond_1
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "airplane_mode_on"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2066574
    if-ne v0, v2, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public j()Z
    .locals 2

    .prologue
    .line 2066575
    iget-object v0, p0, LX/DzG;->m:LX/9jJ;

    .line 2066576
    iget-boolean v1, v0, LX/9jJ;->a:Z

    move v0, v1

    .line 2066577
    if-nez v0, :cond_0

    .line 2066578
    const/4 v0, 0x1

    .line 2066579
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/DzG;->t:LX/1Ml;

    sget-object v1, LX/DzG;->f:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public k()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2066580
    const-string v2, "places.connectivity_override"

    invoke-static {v2, v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2066581
    if-eqz v2, :cond_0

    .line 2066582
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2066583
    :goto_0
    return v0

    .line 2066584
    :cond_0
    iget-object v2, p0, LX/DzG;->a:Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_1

    .line 2066585
    :goto_1
    if-nez v0, :cond_2

    move v0, v1

    .line 2066586
    goto :goto_0

    .line 2066587
    :cond_1
    iget-object v0, p0, LX/DzG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_1

    .line 2066588
    :cond_2
    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2066589
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 2066590
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public l()Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2066591
    iget-object v0, p0, LX/DzG;->m:LX/9jJ;

    .line 2066592
    iget-boolean v1, v0, LX/9jJ;->c:Z

    move v0, v1

    .line 2066593
    if-nez v0, :cond_0

    .line 2066594
    const/4 v0, 0x0

    .line 2066595
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/DzG;->b:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    iget-object v0, v0, LX/1rv;->c:LX/0Rf;

    const-string v1, "network"

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 2066596
    iget-boolean v0, p0, LX/DzG;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/DzG;->c()LX/Dz7;

    move-result-object v0

    invoke-virtual {v0}, LX/Dz7;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2066597
    iget-object v1, p0, LX/DzG;->m:LX/9jJ;

    .line 2066598
    iget-boolean v2, v1, LX/9jJ;->b:Z

    move v1, v2

    .line 2066599
    if-nez v1, :cond_1

    .line 2066600
    :cond_0
    :goto_0
    return v0

    .line 2066601
    :cond_1
    iget-object v1, p0, LX/DzG;->b:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->a()LX/0yG;

    move-result-object v1

    .line 2066602
    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
