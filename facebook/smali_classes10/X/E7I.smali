.class public final LX/E7I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E6y;

.field public final b:LX/5sc;

.field private final c:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;


# direct methods
.method public constructor <init>(LX/E6y;LX/5sc;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;)V
    .locals 0

    .prologue
    .line 2081217
    iput-object p1, p0, LX/E7I;->a:LX/E6y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2081218
    iput-object p2, p0, LX/E7I;->b:LX/5sc;

    .line 2081219
    iput-object p3, p0, LX/E7I;->c:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2081220
    return-void
.end method

.method public static a(LX/E7I;)V
    .locals 3

    .prologue
    .line 2081221
    iget-object v1, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->g:Ljava/util/HashMap;

    iget-object v2, p0, LX/E7I;->b:LX/5sc;

    invoke-interface {v2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E7E;

    iget-object v2, p0, LX/E7I;->c:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-static {v1, v0, v2}, LX/E6y;->a$redex0(LX/E6y;LX/E7E;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;)V

    .line 2081222
    return-void
.end method

.method public static a$redex0(LX/E7I;Z)V
    .locals 6

    .prologue
    .line 2081223
    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E7I;->b:LX/5sc;

    invoke-interface {v0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->g:Ljava/util/HashMap;

    iget-object v1, p0, LX/E7I;->b:LX/5sc;

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2081224
    :cond_0
    :goto_0
    return-void

    .line 2081225
    :cond_1
    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v1, v0, LX/E6y;->g:Ljava/util/HashMap;

    iget-object v0, p0, LX/E7I;->b:LX/5sc;

    invoke-interface {v0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz p1, :cond_3

    sget-object v0, LX/E7E;->ACCEPTED:LX/E7E;

    :goto_1
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2081226
    invoke-static {p0}, LX/E7I;->a(LX/E7I;)V

    .line 2081227
    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    .line 2081228
    invoke-virtual {v0}, LX/Cfk;->c()LX/0o8;

    move-result-object v1

    move-object v0, v1

    .line 2081229
    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->i:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2081230
    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    .line 2081231
    invoke-virtual {v0}, LX/Cfk;->c()LX/0o8;

    move-result-object v1

    move-object v0, v1

    .line 2081232
    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v1

    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v2, v0, LX/E6y;->h:Ljava/lang/String;

    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v3, v0, LX/E6y;->i:Ljava/lang/String;

    new-instance v4, LX/Cfl;

    iget-object v0, p0, LX/E7I;->b:LX/5sc;

    invoke-interface {v0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v5

    if-eqz p1, :cond_4

    sget-object v0, LX/Cfc;->ACCEPT_ADMIN_INVITE_TAP:LX/Cfc;

    :goto_2
    invoke-direct {v4, v5, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;)V

    invoke-virtual {v1, v2, v3, v4}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2081233
    :cond_2
    if-eqz p1, :cond_5

    .line 2081234
    new-instance v0, LX/4He;

    invoke-direct {v0}, LX/4He;-><init>()V

    .line 2081235
    iget-object v1, p0, LX/E7I;->b:LX/5sc;

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    .line 2081236
    const-string v2, "page_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2081237
    new-instance v1, LX/9tx;

    invoke-direct {v1}, LX/9tx;-><init>()V

    move-object v1, v1

    .line 2081238
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/9tx;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2081239
    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2081240
    new-instance v1, LX/E7G;

    invoke-direct {v1, p0}, LX/E7G;-><init>(LX/E7I;)V

    .line 2081241
    iget-object v2, p0, LX/E7I;->a:LX/E6y;

    iget-object v2, v2, LX/E6y;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0

    .line 2081242
    :cond_3
    sget-object v0, LX/E7E;->DECLINED:LX/E7E;

    goto/16 :goto_1

    .line 2081243
    :cond_4
    sget-object v0, LX/Cfc;->DECLINE_ADMIN_INVITE_TAP:LX/Cfc;

    goto :goto_2

    .line 2081244
    :cond_5
    new-instance v0, LX/4Hf;

    invoke-direct {v0}, LX/4Hf;-><init>()V

    .line 2081245
    iget-object v1, p0, LX/E7I;->b:LX/5sc;

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    .line 2081246
    const-string v2, "page_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2081247
    new-instance v1, LX/9ty;

    invoke-direct {v1}, LX/9ty;-><init>()V

    move-object v1, v1

    .line 2081248
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/9ty;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2081249
    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2081250
    new-instance v1, LX/E7H;

    invoke-direct {v1, p0}, LX/E7H;-><init>(LX/E7I;)V

    .line 2081251
    iget-object v2, p0, LX/E7I;->a:LX/E6y;

    iget-object v2, v2, LX/E6y;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, -0x58ec5f84

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2081252
    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E7I;->b:LX/5sc;

    invoke-interface {v0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E7I;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->g:Ljava/util/HashMap;

    iget-object v2, p0, LX/E7I;->b:LX/5sc;

    invoke-interface {v2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2081253
    :cond_0
    const v0, 0x203adaf8

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2081254
    :goto_0
    return-void

    .line 2081255
    :cond_1
    new-instance v0, LX/6WS;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2081256
    const v2, 0x7f110021

    invoke-virtual {v0, v2}, LX/5OM;->b(I)V

    .line 2081257
    new-instance v2, LX/E7F;

    invoke-direct {v2, p0}, LX/E7F;-><init>(LX/E7I;)V

    .line 2081258
    iput-object v2, v0, LX/5OM;->p:LX/5OO;

    .line 2081259
    invoke-virtual {v0, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2081260
    invoke-virtual {v0, v4}, LX/0ht;->c(Z)V

    .line 2081261
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2081262
    const v0, 0x78617b

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
