.class public final LX/DJG;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

.field public final synthetic b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)V
    .locals 0

    .prologue
    .line 1985024
    iput-object p1, p0, LX/DJG;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iput-object p2, p0, LX/DJG;->a:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1985025
    new-instance v1, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;-><init>()V

    .line 1985026
    iget-object v0, p0, LX/DJG;->a:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v2, p0, LX/DJG;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-virtual {v2}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getIsMarketplaceSelected()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;Z)V

    .line 1985027
    new-instance v0, LX/DJE;

    invoke-direct {v0, p0}, LX/DJE;-><init>(LX/DJG;)V

    .line 1985028
    iput-object v0, v1, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->m:Landroid/view/View$OnClickListener;

    .line 1985029
    new-instance v0, LX/DJF;

    invoke-direct {v0, p0}, LX/DJF;-><init>(LX/DJG;)V

    .line 1985030
    iput-object v0, v1, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->n:Landroid/view/View$OnClickListener;

    .line 1985031
    iget-object v0, p0, LX/DJG;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "MARKETPLACE_INFO_DIALOG"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1985032
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1985033
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1985034
    iget-object v0, p0, LX/DJG;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1985035
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1985036
    return-void
.end method
