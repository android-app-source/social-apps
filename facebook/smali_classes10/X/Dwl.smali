.class public final LX/Dwl;
.super LX/9bh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 0

    .prologue
    .line 2061311
    iput-object p1, p0, LX/Dwl;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {p0}, LX/9bh;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 2061312
    iget-object v0, p0, LX/Dwl;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v1

    iget-object v0, p0, LX/Dwl;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v2, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v0, p0, LX/Dwl;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v3, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ab:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    const/4 v4, 0x0

    iget-object v0, p0, LX/Dwl;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    .line 2061313
    iget-object v5, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v5

    .line 2061314
    const-string v5, "disable_adding_photos_to_albums"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iget-object v0, p0, LX/Dwl;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-wide v6, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ai:J

    iget-object v0, p0, LX/Dwl;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-boolean v8, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->aj:Z

    invoke-virtual/range {v1 .. v8}, LX/Dwa;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/util/ArrayList;ZJZ)V

    .line 2061315
    return-void
.end method
