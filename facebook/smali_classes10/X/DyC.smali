.class public final LX/DyC;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V
    .locals 0

    .prologue
    .line 2064036
    iput-object p1, p0, LX/DyC;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2064034
    iget-object v0, p0, LX/DyC;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchInitialAlbumsList"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2064035
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2064037
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2064038
    iget-object v0, p0, LX/DyC;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->n:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2064039
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2064040
    if-nez v0, :cond_1

    .line 2064041
    :cond_0
    :goto_0
    return-void

    .line 2064042
    :cond_1
    iget-object v1, p0, LX/DyC;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a$redex0(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;Lcom/facebook/graphql/model/GraphQLAlbumsConnection;Z)V

    goto :goto_0
.end method
