.class public LX/Ele;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/Ekx;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/Ekx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164571
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Ekx;
    .locals 4

    .prologue
    .line 2164572
    sget-object v0, LX/Ele;->a:LX/Ekx;

    if-nez v0, :cond_1

    .line 2164573
    const-class v1, LX/Ele;

    monitor-enter v1

    .line 2164574
    :try_start_0
    sget-object v0, LX/Ele;->a:LX/Ekx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2164575
    if-eqz v2, :cond_0

    .line 2164576
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2164577
    invoke-static {v0}, LX/ElX;->a(LX/0QB;)LX/Ekr;

    move-result-object v3

    check-cast v3, LX/Ekr;

    const/16 p0, 0x15f5

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3, p0}, LX/Elb;->a(LX/Ekr;LX/0Or;)LX/Ekx;

    move-result-object v3

    move-object v0, v3

    .line 2164578
    sput-object v0, LX/Ele;->a:LX/Ekx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2164579
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2164580
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2164581
    :cond_1
    sget-object v0, LX/Ele;->a:LX/Ekx;

    return-object v0

    .line 2164582
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2164583
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2164584
    invoke-static {p0}, LX/ElX;->a(LX/0QB;)LX/Ekr;

    move-result-object v0

    check-cast v0, LX/Ekr;

    const/16 v1, 0x15f5

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0, v1}, LX/Elb;->a(LX/Ekr;LX/0Or;)LX/Ekx;

    move-result-object v0

    return-object v0
.end method
