.class public final LX/Euf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;",
        ">;",
        "LX/Euc;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Eui;


# direct methods
.method public constructor <init>(LX/Eui;)V
    .locals 0

    .prologue
    .line 2179804
    iput-object p1, p0, LX/Euf;->a:LX/Eui;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2179805
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2179806
    if-eqz p1, :cond_0

    .line 2179807
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179808
    if-eqz v0, :cond_0

    .line 2179809
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179810
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2179811
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179812
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2179813
    :cond_0
    new-instance v0, LX/Euc;

    .line 2179814
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2179815
    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/Euc;-><init>(LX/0Px;Ljava/lang/String;)V

    .line 2179816
    :goto_0
    return-object v0

    .line 2179817
    :cond_1
    iget-object v1, p0, LX/Euf;->a:LX/Eui;

    .line 2179818
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179819
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/Eui;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179820
    new-instance v1, LX/Euc;

    .line 2179821
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179822
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;->a()LX/0Px;

    move-result-object v2

    .line 2179823
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179824
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/Euc;-><init>(LX/0Px;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method
