.class public LX/EtG;
.super LX/Et9;
.source ""


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:LX/4BY;

.field private final e:Ljava/util/Timer;

.field private final f:J

.field private g:Ljava/lang/String;

.field private h:Ljava/util/TimerTask;


# direct methods
.method public constructor <init>(LX/EtA;Landroid/content/Context;Ljava/lang/String;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EtA",
            "<**>;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 2177507
    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/Et9;-><init>(LX/EtA;Ljava/util/concurrent/Executor;)V

    .line 2177508
    iput-object p2, p0, LX/EtG;->c:Landroid/content/Context;

    .line 2177509
    iput-object p3, p0, LX/EtG;->g:Ljava/lang/String;

    .line 2177510
    new-instance v0, LX/4BY;

    iget-object v1, p0, LX/EtG;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/EtG;->d:LX/4BY;

    .line 2177511
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LX/EtG;->e:Ljava/util/Timer;

    .line 2177512
    iput-wide p4, p0, LX/EtG;->f:J

    .line 2177513
    return-void
.end method

.method public static declared-synchronized c(LX/EtG;)V
    .locals 1

    .prologue
    .line 2177503
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EtG;->d:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->show()V

    .line 2177504
    const/4 v0, 0x0

    iput-object v0, p0, LX/EtG;->h:Ljava/util/TimerTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2177505
    monitor-exit p0

    return-void

    .line 2177506
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 2177502
    iget-object v0, p0, LX/EtG;->h:Ljava/util/TimerTask;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EtG;->d:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 2177495
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/EtG;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2177496
    :goto_0
    monitor-exit p0

    return-void

    .line 2177497
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/EtG;->d:LX/4BY;

    iget-object v1, p0, LX/EtG;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2177498
    iget-object v0, p0, LX/EtG;->d:LX/4BY;

    new-instance v1, LX/EtF;

    invoke-direct {v1, p0}, LX/EtF;-><init>(LX/EtG;)V

    invoke-virtual {v0, v1}, LX/4BY;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2177499
    new-instance v0, Lcom/facebook/feedplugins/goodwill/asynctask/SpinnerModalAsyncUITask$TimeoutTriggeringTimerTask;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/goodwill/asynctask/SpinnerModalAsyncUITask$TimeoutTriggeringTimerTask;-><init>(LX/EtG;)V

    iput-object v0, p0, LX/EtG;->h:Ljava/util/TimerTask;

    .line 2177500
    iget-object v0, p0, LX/EtG;->e:Ljava/util/Timer;

    iget-object v1, p0, LX/EtG;->h:Ljava/util/TimerTask;

    iget-wide v2, p0, LX/EtG;->f:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2177501
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 2177487
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/EtG;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2177488
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2177489
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/EtG;->h:Ljava/util/TimerTask;

    if-eqz v0, :cond_2

    .line 2177490
    iget-object v0, p0, LX/EtG;->h:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 2177491
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/EtG;->h:Ljava/util/TimerTask;

    .line 2177492
    iget-object v0, p0, LX/EtG;->d:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2177493
    iget-object v0, p0, LX/EtG;->d:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2177494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
