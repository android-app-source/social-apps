.class public final enum LX/ClS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ClS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ClS;

.field public static final enum EXTRA_LARGE:LX/ClS;

.field public static final enum LARGE:LX/ClS;

.field public static final enum MEDIUM:LX/ClS;

.field public static final enum MINI_LABEL:LX/ClS;

.field public static final enum REGULAR:LX/ClS;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1932469
    new-instance v0, LX/ClS;

    const-string v1, "MINI_LABEL"

    invoke-direct {v0, v1, v2}, LX/ClS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClS;->MINI_LABEL:LX/ClS;

    .line 1932470
    new-instance v0, LX/ClS;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v3}, LX/ClS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClS;->REGULAR:LX/ClS;

    .line 1932471
    new-instance v0, LX/ClS;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v4}, LX/ClS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClS;->LARGE:LX/ClS;

    .line 1932472
    new-instance v0, LX/ClS;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v5}, LX/ClS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClS;->MEDIUM:LX/ClS;

    .line 1932473
    new-instance v0, LX/ClS;

    const-string v1, "EXTRA_LARGE"

    invoke-direct {v0, v1, v6}, LX/ClS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClS;->EXTRA_LARGE:LX/ClS;

    .line 1932474
    const/4 v0, 0x5

    new-array v0, v0, [LX/ClS;

    sget-object v1, LX/ClS;->MINI_LABEL:LX/ClS;

    aput-object v1, v0, v2

    sget-object v1, LX/ClS;->REGULAR:LX/ClS;

    aput-object v1, v0, v3

    sget-object v1, LX/ClS;->LARGE:LX/ClS;

    aput-object v1, v0, v4

    sget-object v1, LX/ClS;->MEDIUM:LX/ClS;

    aput-object v1, v0, v5

    sget-object v1, LX/ClS;->EXTRA_LARGE:LX/ClS;

    aput-object v1, v0, v6

    sput-object v0, LX/ClS;->$VALUES:[LX/ClS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1932475
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static from(Lcom/facebook/graphql/enums/GraphQLTextAnnotationPresentationStyle;)LX/ClS;
    .locals 2

    .prologue
    .line 1932476
    if-eqz p0, :cond_0

    .line 1932477
    sget-object v0, LX/ClP;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationPresentationStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1932478
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1932479
    :pswitch_0
    sget-object v0, LX/ClS;->REGULAR:LX/ClS;

    goto :goto_0

    .line 1932480
    :pswitch_1
    sget-object v0, LX/ClS;->LARGE:LX/ClS;

    goto :goto_0

    .line 1932481
    :pswitch_2
    sget-object v0, LX/ClS;->MEDIUM:LX/ClS;

    goto :goto_0

    .line 1932482
    :pswitch_3
    sget-object v0, LX/ClS;->EXTRA_LARGE:LX/ClS;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/ClS;
    .locals 1

    .prologue
    .line 1932483
    const-class v0, LX/ClS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ClS;

    return-object v0
.end method

.method public static values()[LX/ClS;
    .locals 1

    .prologue
    .line 1932484
    sget-object v0, LX/ClS;->$VALUES:[LX/ClS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ClS;

    return-object v0
.end method
