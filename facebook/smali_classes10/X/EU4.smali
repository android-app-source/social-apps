.class public LX/EU4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/EU4;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7Ql;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/3E1;

.field private final c:LX/0SG;

.field private final d:LX/2xj;

.field private final e:LX/0oB;

.field private final f:LX/3AW;


# direct methods
.method public constructor <init>(LX/0SG;LX/3E1;LX/2xj;LX/3AW;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125676
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EU4;->a:Ljava/util/Map;

    .line 2125677
    iput-object p1, p0, LX/EU4;->c:LX/0SG;

    .line 2125678
    iput-object p2, p0, LX/EU4;->b:LX/3E1;

    .line 2125679
    iput-object p3, p0, LX/EU4;->d:LX/2xj;

    .line 2125680
    new-instance v0, LX/EU3;

    invoke-direct {v0, p0}, LX/EU3;-><init>(LX/EU4;)V

    iput-object v0, p0, LX/EU4;->e:LX/0oB;

    .line 2125681
    iget-object v0, p0, LX/EU4;->d:LX/2xj;

    iget-object v1, p0, LX/EU4;->e:LX/0oB;

    invoke-virtual {v0, v1}, LX/2xj;->a(LX/0oB;)V

    .line 2125682
    iput-object p4, p0, LX/EU4;->f:LX/3AW;

    .line 2125683
    return-void
.end method

.method public static a(LX/0QB;)LX/EU4;
    .locals 7

    .prologue
    .line 2125684
    sget-object v0, LX/EU4;->g:LX/EU4;

    if-nez v0, :cond_1

    .line 2125685
    const-class v1, LX/EU4;

    monitor-enter v1

    .line 2125686
    :try_start_0
    sget-object v0, LX/EU4;->g:LX/EU4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2125687
    if-eqz v2, :cond_0

    .line 2125688
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2125689
    new-instance p0, LX/EU4;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/3E1;->b(LX/0QB;)LX/3E1;

    move-result-object v4

    check-cast v4, LX/3E1;

    invoke-static {v0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v5

    check-cast v5, LX/2xj;

    invoke-static {v0}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v6

    check-cast v6, LX/3AW;

    invoke-direct {p0, v3, v4, v5, v6}, LX/EU4;-><init>(LX/0SG;LX/3E1;LX/2xj;LX/3AW;)V

    .line 2125690
    move-object v0, p0

    .line 2125691
    sput-object v0, LX/EU4;->g:LX/EU4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2125692
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2125693
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2125694
    :cond_1
    sget-object v0, LX/EU4;->g:LX/EU4;

    return-object v0

    .line 2125695
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2125696
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125697
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2125698
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2125699
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2125700
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/7Ql;)V
    .locals 9

    .prologue
    .line 2125701
    iget-object v0, p0, LX/EU4;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p1, LX/7Ql;->b:J

    sub-long v2, v0, v2

    .line 2125702
    const-wide/16 v0, 0x64

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/EU4;->d:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2125703
    iget-object v0, p0, LX/EU4;->b:LX/3E1;

    iget-object v1, p1, LX/7Ql;->a:LX/7Qm;

    iget-object v1, v1, LX/7Qm;->b:LX/0lF;

    const-string v4, "video_home"

    iget-object v5, p1, LX/7Ql;->c:Lcom/facebook/graphql/model/FeedUnit;

    const/4 v6, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, LX/3E1;->a(LX/0lF;JLjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;ILjava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2125704
    invoke-virtual {p1, v0}, LX/7Ql;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2125705
    iget-object v1, p0, LX/EU4;->b:LX/3E1;

    invoke-virtual {v1, v0}, LX/3E1;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2125706
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 2125707
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EU4;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2125708
    iget-object v2, p0, LX/EU4;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Ql;

    .line 2125709
    if-eqz v0, :cond_0

    .line 2125710
    invoke-direct {p0, v0}, LX/EU4;->a(LX/7Ql;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2125711
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2125712
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/EU4;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2125713
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(IILcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 15

    .prologue
    .line 2125714
    monitor-enter p0

    :try_start_0
    invoke-static/range {p4 .. p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2125715
    move-object/from16 v0, p3

    invoke-virtual {p0, v0}, LX/EU4;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2125716
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->m()Ljava/lang/String;

    move-result-object v14

    .line 2125717
    invoke-static/range {p4 .. p4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 2125718
    invoke-virtual {v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/EU4;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v11

    .line 2125719
    iget-object v2, p0, LX/EU4;->a:Ljava/util/Map;

    new-instance v3, LX/7Ql;

    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->n()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/EU4;->c:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v12

    move/from16 v7, p1

    move/from16 v8, p2

    move-object/from16 v9, p5

    move-object/from16 v10, p3

    invoke-direct/range {v3 .. v13}, LX/7Ql;-><init>(Ljava/lang/String;LX/0lF;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;J)V

    invoke-interface {v2, v14, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2125720
    monitor-exit p0

    return-void

    .line 2125721
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final declared-synchronized a(LX/04D;LX/7Qk;)V
    .locals 4

    .prologue
    .line 2125722
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EU4;->f:LX/3AW;

    .line 2125723
    iget-object v1, v0, LX/3AW;->c:LX/1CC;

    invoke-virtual {v1}, LX/1CC;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2125724
    iget-object v1, v0, LX/3AW;->n:Ljava/util/Set;

    iget-object v2, p2, LX/7Qk;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2125725
    if-eqz v1, :cond_2

    .line 2125726
    :cond_0
    const/4 v1, 0x0

    .line 2125727
    :goto_1
    move-object v0, v1

    .line 2125728
    if-eqz v0, :cond_1

    .line 2125729
    iget-object v1, p0, LX/EU4;->b:LX/3E1;

    .line 2125730
    invoke-static {v1}, LX/3E1;->d(LX/3E1;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2125731
    iget-object v2, v1, LX/3E1;->h:LX/3E4;

    invoke-virtual {v2, v0}, LX/3E4;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2125732
    :cond_1
    :goto_2
    monitor-exit p0

    return-void

    .line 2125733
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2125734
    :cond_2
    :try_start_1
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "video_home_vpv"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v2, v2, LX/04F;->value:Ljava/lang/String;

    iget-object v3, p1, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/0JS;->EVENT_TARGET:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    const-string v3, "channel"

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/0JS;->TARGET_ID:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    iget-object v3, p2, LX/7Qk;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/0JS;->UNIT_POSITION:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    iget v3, p2, LX/7Qk;->d:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/0JS;->POSITION_IN_UNIT:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    iget v3, p2, LX/7Qk;->e:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/0JS;->REACTION_UNIT_TYPE:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    iget-object v3, p2, LX/7Qk;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    iget-object v3, p2, LX/7Qk;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/0JS;->SESSION_ID:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    iget-object v3, v0, LX/3AW;->c:LX/1CC;

    .line 2125735
    iget-object v0, v3, LX/1CC;->d:Ljava/lang/String;

    move-object v3, v0

    .line 2125736
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "video_home"

    .line 2125737
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2125738
    move-object v1, v1

    .line 2125739
    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2125740
    :cond_4
    iget-object v2, v1, LX/3E1;->h:LX/3E4;

    invoke-virtual {v2, v0}, LX/3E4;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_2
.end method

.method public final declared-synchronized a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 2

    .prologue
    .line 2125741
    monitor-enter p0

    .line 2125742
    :try_start_0
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2125743
    iget-object v1, p0, LX/EU4;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Ql;

    .line 2125744
    if-eqz v0, :cond_0

    .line 2125745
    invoke-direct {p0, v0}, LX/EU4;->a(LX/7Ql;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2125746
    :cond_0
    monitor-exit p0

    return-void

    .line 2125747
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 5

    .prologue
    .line 2125748
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EU4;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Ql;

    .line 2125749
    iget-object v2, p0, LX/EU4;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2125750
    iput-wide v2, v0, LX/7Ql;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2125751
    goto :goto_0

    .line 2125752
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2125753
    :cond_0
    monitor-exit p0

    return-void
.end method
