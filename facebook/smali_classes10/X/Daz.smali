.class public LX/Daz;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/text/BetterTextView;

.field public b:Lcom/facebook/widget/text/BetterTextView;

.field public c:Landroid/widget/ImageView;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2016707
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2016708
    const v0, 0x7f030284

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2016709
    const v0, 0x7f0d092b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Daz;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2016710
    const v0, 0x7f0d092c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Daz;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2016711
    const v0, 0x7f0d092d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Daz;->c:Landroid/widget/ImageView;

    .line 2016712
    iget-object v0, p0, LX/Daz;->c:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, LX/Daz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0a05f0

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object p1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, p1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2016713
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 2016714
    iget-object v2, p0, LX/Daz;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2016715
    if-eqz p2, :cond_0

    .line 2016716
    iget-object v2, p0, LX/Daz;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2016717
    iget-object v2, p0, LX/Daz;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2016718
    :goto_0
    iget-object v2, p0, LX/Daz;->c:Landroid/widget/ImageView;

    if-eqz p3, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2016719
    iput-object p4, p0, LX/Daz;->d:Ljava/lang/String;

    .line 2016720
    return-void

    .line 2016721
    :cond_0
    iget-object v2, p0, LX/Daz;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2016722
    goto :goto_1
.end method

.method public getTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2016723
    iget-object v0, p0, LX/Daz;->a:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Daz;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/Daz;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
