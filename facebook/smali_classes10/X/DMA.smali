.class public LX/DMA;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1989677
    const-class v0, LX/DMA;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DMA;->a:Ljava/lang/String;

    .line 1989678
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/DMA;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1989679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1989680
    iput-object p1, p0, LX/DMA;->c:LX/03V;

    .line 1989681
    return-void
.end method
