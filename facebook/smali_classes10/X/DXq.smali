.class public final LX/DXq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2010897
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 2010898
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2010899
    :goto_0
    return v1

    .line 2010900
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_7

    .line 2010901
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2010902
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2010903
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 2010904
    const-string v12, "can_viewer_expire"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 2010905
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v10, v4

    move v4, v2

    goto :goto_1

    .line 2010906
    :cond_1
    const-string v12, "can_viewer_regenerate"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2010907
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v9, v3

    move v3, v2

    goto :goto_1

    .line 2010908
    :cond_2
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2010909
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2010910
    :cond_3
    const-string v12, "inviter"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2010911
    invoke-static {p0, p1}, LX/DXp;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2010912
    :cond_4
    const-string v12, "is_expired"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2010913
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 2010914
    :cond_5
    const-string v12, "short_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2010915
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2010916
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2010917
    :cond_7
    const/4 v11, 0x6

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2010918
    if-eqz v4, :cond_8

    .line 2010919
    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 2010920
    :cond_8
    if-eqz v3, :cond_9

    .line 2010921
    invoke-virtual {p1, v2, v9}, LX/186;->a(IZ)V

    .line 2010922
    :cond_9
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 2010923
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2010924
    if-eqz v0, :cond_a

    .line 2010925
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 2010926
    :cond_a
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2010927
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2010928
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2010929
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2010930
    if-eqz v0, :cond_0

    .line 2010931
    const-string v1, "can_viewer_expire"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2010932
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2010933
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2010934
    if-eqz v0, :cond_1

    .line 2010935
    const-string v1, "can_viewer_regenerate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2010936
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2010937
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2010938
    if-eqz v0, :cond_2

    .line 2010939
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2010940
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2010941
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2010942
    if-eqz v0, :cond_3

    .line 2010943
    const-string v1, "inviter"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2010944
    invoke-static {p0, v0, p2}, LX/DXp;->a(LX/15i;ILX/0nX;)V

    .line 2010945
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2010946
    if-eqz v0, :cond_4

    .line 2010947
    const-string v1, "is_expired"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2010948
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2010949
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2010950
    if-eqz v0, :cond_5

    .line 2010951
    const-string v1, "short_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2010952
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2010953
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2010954
    return-void
.end method
