.class public final LX/DjK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DjL;


# direct methods
.method public constructor <init>(LX/DjL;)V
    .locals 0

    .prologue
    .line 2033065
    iput-object p1, p0, LX/DjK;->a:LX/DjL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x2cb3b59

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033066
    iget-object v1, p0, LX/DjK;->a:LX/DjL;

    iget-object v1, v1, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->i:LX/0Uh;

    const/16 v2, 0x61e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2033067
    iget-object v1, p0, LX/DjK;->a:LX/DjL;

    iget-object v1, v1, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    .line 2033068
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, LX/Djc;->a:I

    iget-object v6, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->r:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->u()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$UserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$UserModel;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->r:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {v7}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->r:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {v8}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->k()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->g:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {v4 .. v9}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;

    move-result-object v4

    .line 2033069
    iget-object v5, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->j:Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0x14

    invoke-interface {v5, v4, v6, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2033070
    :goto_0
    const v1, 0x5759ddba

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2033071
    :cond_0
    iget-object v1, p0, LX/DjK;->a:LX/DjL;

    iget-object v1, v1, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    .line 2033072
    sget-object v2, LX/Diw;->ADMIN_CANCEL:LX/Diw;

    invoke-static {v2}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;->a(LX/Diw;)Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;

    move-result-object v2

    .line 2033073
    iget-object v3, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->s:LX/Dix;

    .line 2033074
    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;->m:LX/Dix;

    .line 2033075
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    const-string v4, "appointment_confirmation_dialog_fragment_tag"

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2033076
    goto :goto_0
.end method
