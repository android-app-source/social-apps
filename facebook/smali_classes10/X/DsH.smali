.class public LX/DsH;
.super Landroid/widget/BaseAdapter;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private final a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field public final b:LX/DqA;

.field public final c:LX/3Cl;

.field private final d:LX/2jO;

.field private final e:LX/1rn;

.field public final f:I

.field public final g:LX/DsF;

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/0V6;LX/DqA;LX/3Cl;LX/2jO;LX/1rn;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2050293
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2050294
    new-instance v0, LX/DsF;

    invoke-direct {v0, p0}, LX/DsF;-><init>(LX/DsH;)V

    iput-object v0, p0, LX/DsH;->g:LX/DsF;

    .line 2050295
    iput-object p1, p0, LX/DsH;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 2050296
    iput-object p3, p0, LX/DsH;->b:LX/DqA;

    .line 2050297
    iput-object p4, p0, LX/DsH;->c:LX/3Cl;

    .line 2050298
    iput-object p5, p0, LX/DsH;->d:LX/2jO;

    .line 2050299
    iput-object p6, p0, LX/DsH;->e:LX/1rn;

    .line 2050300
    invoke-virtual {p2}, LX/0V6;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1e

    :goto_0
    iput v0, p0, LX/DsH;->f:I

    .line 2050301
    return-void

    .line 2050302
    :cond_0
    const/16 v0, 0x64

    goto :goto_0
.end method

.method private c(I)Z
    .locals 1

    .prologue
    .line 2050303
    if-nez p1, :cond_0

    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    invoke-virtual {v0}, LX/DqA;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)LX/DsG;
    .locals 1

    .prologue
    .line 2050304
    invoke-direct {p0, p1}, LX/DsH;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2050305
    sget-object v0, LX/DsG;->VIEW_TYPE_INLINE_ACTION_NUX:LX/DsG;

    .line 2050306
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/DsG;->VIEW_TYPE_NOTIFICATION:LX/DsG;

    goto :goto_0
.end method

.method public static e(LX/DsH;I)I
    .locals 1

    .prologue
    .line 2050331
    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    invoke-virtual {v0}, LX/DqA;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    .line 2050332
    add-int/lit8 p1, p1, -0x1

    .line 2050333
    :cond_0
    return p1
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 2050307
    invoke-virtual {p0, p1}, LX/DsH;->b(I)LX/2nq;

    move-result-object v0

    .line 2050308
    if-nez v0, :cond_1

    .line 2050309
    :cond_0
    :goto_0
    return-void

    .line 2050310
    :cond_1
    invoke-static {v0, p2}, LX/BDX;->a(LX/2nq;Ljava/lang/String;)LX/2nq;

    move-result-object v1

    .line 2050311
    if-eqz v1, :cond_0

    .line 2050312
    iget-object v2, p0, LX/DsH;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2050313
    iget-object v2, p0, LX/DsH;->e:LX/1rn;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1}, LX/2nq;->n()LX/BAy;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/1rn;->a(Ljava/lang/String;LX/BAy;)V

    .line 2050314
    iget-object v0, p0, LX/DsH;->g:LX/DsF;

    invoke-static {p0, p1}, LX/DsH;->e(LX/DsH;I)I

    move-result v2

    .line 2050315
    iget-object v3, v0, LX/DsF;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sub-int v3, v2, v3

    .line 2050316
    const/16 p1, 0x1e

    if-ge v2, p1, :cond_3

    iget-object p1, v0, LX/DsF;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ge v2, p1, :cond_3

    .line 2050317
    iget-object v3, v0, LX/DsF;->b:Ljava/util/List;

    invoke-interface {v3, v2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2050318
    :cond_2
    :goto_1
    iget-object v0, p0, LX/DsH;->g:LX/DsF;

    invoke-virtual {v0}, LX/DsF;->b()V

    .line 2050319
    const v0, -0x58e3ba84

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2050320
    :cond_3
    if-ltz v3, :cond_2

    iget-object p1, v0, LX/DsF;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ge v3, p1, :cond_2

    .line 2050321
    iget-object p1, v0, LX/DsF;->c:Ljava/util/List;

    invoke-interface {p1, v3, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2050322
    iget-object v0, p0, LX/DsH;->g:LX/DsF;

    .line 2050323
    iget-object v1, v0, LX/DsF;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2050324
    if-eqz p1, :cond_0

    .line 2050325
    iget-object v1, v0, LX/DsF;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2050326
    :cond_0
    invoke-virtual {v0}, LX/DsF;->b()V

    .line 2050327
    iget-boolean v0, p0, LX/DsH;->h:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 2050328
    iget-object v0, p0, LX/DsH;->g:LX/DsF;

    iget-object v1, p0, LX/DsH;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->d()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/DsF;->c(Ljava/util/Collection;)V

    .line 2050329
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DsH;->h:Z

    .line 2050330
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 11

    .prologue
    .line 2050262
    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    const/4 v1, 0x0

    .line 2050263
    iget-boolean v2, v0, LX/DqA;->j:Z

    move v2, v2

    .line 2050264
    if-eqz v2, :cond_3

    .line 2050265
    :cond_0
    :goto_0
    move v0, v1

    .line 2050266
    if-eqz v0, :cond_1

    .line 2050267
    const v0, 0x24d5271d

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2050268
    :goto_1
    return-void

    .line 2050269
    :cond_1
    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    .line 2050270
    iget-boolean v1, v0, LX/DqA;->n:Z

    move v0, v1

    .line 2050271
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    .line 2050272
    iget-boolean v1, v0, LX/DqA;->j:Z

    move v0, v1

    .line 2050273
    if-eqz v0, :cond_2

    .line 2050274
    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    invoke-virtual {v0}, LX/DqA;->e()V

    .line 2050275
    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    invoke-virtual {v0}, LX/DqA;->h()V

    goto :goto_1

    .line 2050276
    :cond_2
    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    .line 2050277
    iput-boolean p1, v0, LX/DqA;->o:Z

    .line 2050278
    goto :goto_1

    .line 2050279
    :cond_3
    iget-object v2, v0, LX/DqA;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0hM;->K:LX/0Tn;

    const-wide/16 v5, 0x0

    invoke-interface {v2, v3, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v3

    .line 2050280
    iget-object v2, v0, LX/DqA;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v5

    sub-long v3, v5, v3

    .line 2050281
    iget-object v2, v0, LX/DqA;->b:LX/1rx;

    .line 2050282
    iget-object v7, v2, LX/1rx;->a:LX/0ad;

    sget v8, LX/15r;->n:I

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, LX/0ad;->a(II)I

    move-result v7

    int-to-long v7, v7

    .line 2050283
    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    move-wide v5, v7

    .line 2050284
    cmp-long v2, v3, v5

    if-lez v2, :cond_0

    .line 2050285
    const/4 v3, 0x1

    .line 2050286
    iput-boolean v3, v0, LX/DqA;->k:Z

    .line 2050287
    iget-object v1, v0, LX/DqA;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->J:LX/0Tn;

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2050288
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final b(I)LX/2nq;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2050289
    invoke-virtual {p0, p1}, LX/DsH;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2050290
    instance-of v1, v0, LX/2nq;

    if-nez v1, :cond_0

    .line 2050291
    const/4 v0, 0x0

    .line 2050292
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/2nq;

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2050261
    iget-object v0, p0, LX/DsH;->g:LX/DsF;

    invoke-virtual {v0}, LX/DsF;->a()I

    move-result v0

    return v0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2050258
    iget-object v0, p0, LX/DsH;->g:LX/DsF;

    invoke-virtual {v0}, LX/DsF;->a()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    invoke-virtual {v0}, LX/DqA;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2050259
    :goto_0
    invoke-virtual {p0}, LX/DsH;->c()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 2050260
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2050253
    invoke-direct {p0, p1}, LX/DsH;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2050254
    const/4 v0, 0x0

    .line 2050255
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DsH;->g:LX/DsF;

    invoke-static {p0, p1}, LX/DsH;->e(LX/DsH;I)I

    move-result v1

    .line 2050256
    iget-object p0, v0, LX/DsF;->d:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/2nq;

    move-object v0, p0

    .line 2050257
    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2050252
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2050251
    invoke-direct {p0, p1}, LX/DsH;->d(I)LX/DsG;

    move-result-object v0

    invoke-virtual {v0}, LX/DsG;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedClass"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 2050214
    invoke-direct {p0, p1}, LX/DsH;->d(I)LX/DsG;

    move-result-object v0

    sget-object v1, LX/DsG;->VIEW_TYPE_INLINE_ACTION_NUX:LX/DsG;

    if-ne v0, v1, :cond_0

    .line 2050215
    iget-object v0, p0, LX/DsH;->b:LX/DqA;

    invoke-virtual {v0, p1, p2, p3, v9}, LX/DqA;->a(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    .line 2050216
    :goto_0
    return-object v1

    .line 2050217
    :cond_0
    if-eqz p2, :cond_2

    move-object v1, p2

    .line 2050218
    :goto_1
    invoke-virtual {p0, p1}, LX/DsH;->b(I)LX/2nq;

    move-result-object v5

    .line 2050219
    if-eqz v5, :cond_1

    invoke-interface {v5}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2050220
    invoke-interface {v5}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    .line 2050221
    iget-object v0, p0, LX/DsH;->d:LX/2jO;

    .line 2050222
    iget-object v3, v0, LX/2jO;->i:LX/DqC;

    if-eqz v3, :cond_5

    iget-object v3, v0, LX/2jO;->i:LX/DqC;

    .line 2050223
    iget-object v4, v3, LX/DqC;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2050224
    if-eqz v3, :cond_5

    .line 2050225
    iget-object v3, v0, LX/2jO;->i:LX/DqC;

    .line 2050226
    iget-object v4, v3, LX/DqC;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2050227
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 2050228
    :goto_2
    move v0, v3

    .line 2050229
    if-eqz v0, :cond_4

    .line 2050230
    const/4 v0, 0x1

    move v8, v0

    .line 2050231
    :goto_3
    iget-object v0, p0, LX/DsH;->d:LX/2jO;

    invoke-interface {v5}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/2jO;->a(Ljava/lang/String;Ljava/lang/String;)LX/3D3;

    .line 2050232
    invoke-interface {v5}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2050233
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->af()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLIcon;->j()Ljava/lang/String;

    move-result-object v2

    .line 2050234
    :goto_4
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 2050235
    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v3

    .line 2050236
    invoke-static {v0}, LX/1xl;->e(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v4

    .line 2050237
    iget-object v0, p0, LX/DsH;->c:LX/3Cl;

    move-object v7, v6

    invoke-virtual/range {v0 .. v7}, LX/3Cl;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2nq;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2050238
    if-eqz v8, :cond_1

    .line 2050239
    iget-object v0, p0, LX/DsH;->d:LX/2jO;

    .line 2050240
    iget-object v2, v0, LX/2jO;->i:LX/DqC;

    move-object v2, v2

    .line 2050241
    if-eqz v2, :cond_1

    .line 2050242
    iget-object v0, p0, LX/DsH;->c:LX/3Cl;

    invoke-virtual {v0, v1, v2, v5, p1}, LX/3Cl;->a(Landroid/view/View;LX/DqC;LX/2nq;I)V

    .line 2050243
    instance-of v0, v1, LX/3Cv;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2050244
    check-cast v0, LX/3Cv;

    sget-object v3, LX/3Cw;->POST_FEEDBACK:LX/3Cw;

    .line 2050245
    iget-boolean v4, v2, LX/DqC;->f:Z

    move v4, v4

    .line 2050246
    invoke-virtual {v0, v3, v4}, LX/3Cv;->a(LX/3Cw;Z)V

    .line 2050247
    iput-boolean v9, v2, LX/DqC;->f:Z

    .line 2050248
    :cond_1
    const v0, 0x7f0d0090

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 2050249
    :cond_2
    iget-object v0, p0, LX/DsH;->c:LX/3Cl;

    invoke-virtual {v0, p3}, LX/3Cl;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_1

    :cond_3
    move-object v2, v6

    .line 2050250
    goto :goto_4

    :cond_4
    move v8, v9

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2050213
    invoke-static {}, LX/DsG;->values()[LX/DsG;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 2050212
    invoke-direct {p0, p1}, LX/DsH;->d(I)LX/DsG;

    move-result-object v0

    sget-object v1, LX/DsG;->VIEW_TYPE_NOTIFICATION:LX/DsG;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
