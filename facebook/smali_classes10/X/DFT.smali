.class public final LX/DFT;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DFU;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

.field public c:LX/1Pc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:LX/3mj;

.field public final synthetic e:LX/DFU;


# direct methods
.method public constructor <init>(LX/DFU;)V
    .locals 1

    .prologue
    .line 1978205
    iput-object p1, p0, LX/DFT;->e:LX/DFU;

    .line 1978206
    move-object v0, p1

    .line 1978207
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1978208
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1978229
    const-string v0, "PersonYouMayKnowComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1978209
    if-ne p0, p1, :cond_1

    .line 1978210
    :cond_0
    :goto_0
    return v0

    .line 1978211
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1978212
    goto :goto_0

    .line 1978213
    :cond_3
    check-cast p1, LX/DFT;

    .line 1978214
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1978215
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1978216
    if-eq v2, v3, :cond_0

    .line 1978217
    iget-object v2, p0, LX/DFT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DFT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DFT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1978218
    goto :goto_0

    .line 1978219
    :cond_5
    iget-object v2, p1, LX/DFT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1978220
    :cond_6
    iget-object v2, p0, LX/DFT;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DFT;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    iget-object v3, p1, LX/DFT;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1978221
    goto :goto_0

    .line 1978222
    :cond_8
    iget-object v2, p1, LX/DFT;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    if-nez v2, :cond_7

    .line 1978223
    :cond_9
    iget-object v2, p0, LX/DFT;->c:LX/1Pc;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/DFT;->c:LX/1Pc;

    iget-object v3, p1, LX/DFT;->c:LX/1Pc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1978224
    goto :goto_0

    .line 1978225
    :cond_b
    iget-object v2, p1, LX/DFT;->c:LX/1Pc;

    if-nez v2, :cond_a

    .line 1978226
    :cond_c
    iget-object v2, p0, LX/DFT;->d:LX/3mj;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/DFT;->d:LX/3mj;

    iget-object v3, p1, LX/DFT;->d:LX/3mj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1978227
    goto :goto_0

    .line 1978228
    :cond_d
    iget-object v2, p1, LX/DFT;->d:LX/3mj;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
