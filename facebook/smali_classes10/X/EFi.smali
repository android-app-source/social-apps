.class public final enum LX/EFi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EFi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EFi;

.field public static final enum Activity:LX/EFi;

.field public static final enum ChatHead:LX/EFi;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2096165
    new-instance v0, LX/EFi;

    const-string v1, "Activity"

    invoke-direct {v0, v1, v2}, LX/EFi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFi;->Activity:LX/EFi;

    .line 2096166
    new-instance v0, LX/EFi;

    const-string v1, "ChatHead"

    invoke-direct {v0, v1, v3}, LX/EFi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFi;->ChatHead:LX/EFi;

    .line 2096167
    const/4 v0, 0x2

    new-array v0, v0, [LX/EFi;

    sget-object v1, LX/EFi;->Activity:LX/EFi;

    aput-object v1, v0, v2

    sget-object v1, LX/EFi;->ChatHead:LX/EFi;

    aput-object v1, v0, v3

    sput-object v0, LX/EFi;->$VALUES:[LX/EFi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2096164
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EFi;
    .locals 1

    .prologue
    .line 2096163
    const-class v0, LX/EFi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EFi;

    return-object v0
.end method

.method public static values()[LX/EFi;
    .locals 1

    .prologue
    .line 2096162
    sget-object v0, LX/EFi;->$VALUES:[LX/EFi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EFi;

    return-object v0
.end method
