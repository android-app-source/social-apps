.class public LX/D2j;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:I

.field private final c:I

.field private final d:F

.field private final e:Landroid/graphics/RectF;

.field private final f:Landroid/graphics/RectF;

.field private final g:Landroid/graphics/Rect;

.field public h:Landroid/graphics/Bitmap;

.field public i:I

.field public j:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 1959228
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1959229
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/D2j;->a:Landroid/graphics/Paint;

    .line 1959230
    iget-object v0, p0, LX/D2j;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1959231
    const v0, 0x7f0a00d2

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/D2j;->b:I

    .line 1959232
    const v0, 0x7f0a00d5

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/D2j;->c:I

    .line 1959233
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/D2j;->e:Landroid/graphics/RectF;

    .line 1959234
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/D2j;->f:Landroid/graphics/RectF;

    .line 1959235
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/D2j;->g:Landroid/graphics/Rect;

    .line 1959236
    const v0, 0x7f0b1da7

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/D2j;->d:F

    .line 1959237
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x40800000    # 4.0f

    .line 1959207
    iget-object v0, p0, LX/D2j;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1959208
    iget-object v0, p0, LX/D2j;->a:Landroid/graphics/Paint;

    iget v1, p0, LX/D2j;->d:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1959209
    iget-object v0, p0, LX/D2j;->a:Landroid/graphics/Paint;

    iget v1, p0, LX/D2j;->b:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1959210
    iget-object v0, p0, LX/D2j;->e:Landroid/graphics/RectF;

    iget-object v1, p0, LX/D2j;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1959211
    iget-object v0, p0, LX/D2j;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1959212
    iget-object v0, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 1959213
    iget-object v0, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 1959214
    iget-object v0, p0, LX/D2j;->g:Landroid/graphics/Rect;

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 1959215
    iget-object v0, p0, LX/D2j;->g:Landroid/graphics/Rect;

    iget-object v1, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1959216
    iget-object v0, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 1959217
    iget-object v1, p0, LX/D2j;->g:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1959218
    iget-object v1, p0, LX/D2j;->g:Landroid/graphics/Rect;

    iget-object v2, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1959219
    :goto_0
    iget-object v0, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    iget-object v1, p0, LX/D2j;->g:Landroid/graphics/Rect;

    iget-object v2, p0, LX/D2j;->f:Landroid/graphics/RectF;

    iget-object v3, p0, LX/D2j;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1959220
    :goto_1
    return-void

    .line 1959221
    :cond_0
    iget-object v0, p0, LX/D2j;->g:Landroid/graphics/Rect;

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 1959222
    iget-object v0, p0, LX/D2j;->g:Landroid/graphics/Rect;

    iget-object v1, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1959223
    iget-object v0, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 1959224
    iget-object v1, p0, LX/D2j;->g:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1959225
    iget-object v1, p0, LX/D2j;->g:Landroid/graphics/Rect;

    iget-object v2, p0, LX/D2j;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 1959226
    :cond_1
    iget-object v0, p0, LX/D2j;->a:Landroid/graphics/Paint;

    iget v1, p0, LX/D2j;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1959227
    iget-object v0, p0, LX/D2j;->f:Landroid/graphics/RectF;

    iget-object v1, p0, LX/D2j;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 1959206
    iget v0, p0, LX/D2j;->i:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 1959205
    iget v0, p0, LX/D2j;->j:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1959238
    const/4 v0, -0x3

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 1959199
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 1959200
    iget-object v0, p0, LX/D2j;->e:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1959201
    iget-object v0, p0, LX/D2j;->e:Landroid/graphics/RectF;

    iget v1, p0, LX/D2j;->d:F

    div-float/2addr v1, v3

    iget v2, p0, LX/D2j;->d:F

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 1959202
    iget-object v0, p0, LX/D2j;->f:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1959203
    iget-object v0, p0, LX/D2j;->f:Landroid/graphics/RectF;

    iget v1, p0, LX/D2j;->d:F

    iget v2, p0, LX/D2j;->d:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 1959204
    return-void
.end method

.method public final setAlpha(I)V
    .locals 2

    .prologue
    .line 1959198
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "setAlpha not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2

    .prologue
    .line 1959197
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "setColorFilter not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
