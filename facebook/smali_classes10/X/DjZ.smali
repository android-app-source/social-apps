.class public final LX/DjZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Djb;


# direct methods
.method public constructor <init>(LX/Djb;)V
    .locals 0

    .prologue
    .line 2033327
    iput-object p1, p0, LX/DjZ;->a:LX/Djb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2033328
    iget-object v0, p0, LX/DjZ;->a:LX/Djb;

    iget-object v0, v0, LX/Djb;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->b:LX/Dka;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DjZ;->a:LX/Djb;

    iget-object v2, v2, LX/Djb;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2033329
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2033330
    iget-object v3, p0, LX/DjZ;->a:LX/Djb;

    iget-object v3, v3, LX/Djb;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->h:Ljava/lang/String;

    iget-object v4, p0, LX/DjZ;->a:LX/Djb;

    iget-object v4, v4, LX/Djb;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;->g:Ljava/lang/String;

    iget-object v5, p0, LX/DjZ;->a:LX/Djb;

    iget-object v5, v5, LX/Djb;->a:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/Dka;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
