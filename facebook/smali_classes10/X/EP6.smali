.class public LX/EP6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2115875
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/CzL;Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "LX/A3T;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLSavedState;",
            ")",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2115876
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2115877
    check-cast v0, LX/A3T;

    invoke-interface {v0}, LX/A3T;->aD()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2115878
    const/4 v0, 0x0

    .line 2115879
    :goto_0
    return-object v0

    .line 2115880
    :cond_0
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2115881
    check-cast v0, LX/A3T;

    invoke-interface {v0}, LX/A3T;->aD()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v0

    .line 2115882
    invoke-static {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;->a(Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;)Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v0

    .line 2115883
    new-instance v1, LX/A4P;

    invoke-direct {v1}, LX/A4P;-><init>()V

    .line 2115884
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    iput-object v2, v1, LX/A4P;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2115885
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/A4P;->b:Ljava/lang/String;

    .line 2115886
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;->d()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    iput-object v2, v1, LX/A4P;->c:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2115887
    move-object v0, v1

    .line 2115888
    iput-object p1, v0, LX/A4P;->c:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2115889
    move-object v0, v0

    .line 2115890
    invoke-virtual {v0}, LX/A4P;->a()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v1

    .line 2115891
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2115892
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-static {v0}, LX/8dX;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)LX/8dX;

    move-result-object v0

    .line 2115893
    iput-object v1, v0, LX/8dX;->af:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    .line 2115894
    move-object v0, v0

    .line 2115895
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2115896
    iget-object v1, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 2115897
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v2

    .line 2115898
    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-static {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    move-result-object v2

    invoke-static {v2}, LX/8dV;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;)LX/8dV;

    move-result-object v2

    .line 2115899
    iput-object v0, v2, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2115900
    move-object v2, v2

    .line 2115901
    invoke-virtual {v2}, LX/8dV;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    move-result-object v2

    .line 2115902
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object p0

    .line 2115903
    invoke-static {p0}, LX/8dU;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;)LX/8dU;

    move-result-object p0

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2115904
    iput-object v2, p0, LX/8dU;->a:LX/0Px;

    .line 2115905
    move-object v2, p0

    .line 2115906
    invoke-virtual {v2}, LX/8dU;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v2

    .line 2115907
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object p0

    .line 2115908
    invoke-static {p0}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object p0

    .line 2115909
    iput-object v2, p0, LX/8dQ;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    .line 2115910
    move-object v2, p0

    .line 2115911
    invoke-virtual {v2}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v2

    .line 2115912
    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object p0

    .line 2115913
    invoke-static {p0}, LX/8dO;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/8dO;

    move-result-object p0

    .line 2115914
    iput-object v2, p0, LX/8dO;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2115915
    move-object v2, p0

    .line 2115916
    invoke-virtual {v2}, LX/8dO;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v2

    .line 2115917
    move-object v0, v2

    .line 2115918
    goto/16 :goto_0
.end method
