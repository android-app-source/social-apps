.class public abstract LX/EZd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EZc;


# instance fields
.field private final a:LX/EZh;

.field private b:LX/EZg;


# direct methods
.method public constructor <init>(LX/EZh;LX/EZg;)V
    .locals 0

    .prologue
    .line 2139594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2139595
    iput-object p1, p0, LX/EZd;->a:LX/EZh;

    .line 2139596
    iput-object p2, p0, LX/EZd;->b:LX/EZg;

    .line 2139597
    return-void
.end method


# virtual methods
.method public a(LX/EZg;)V
    .locals 0

    .prologue
    .line 2139598
    iput-object p1, p0, LX/EZd;->b:LX/EZg;

    .line 2139599
    return-void
.end method

.method public a()[B
    .locals 5

    .prologue
    .line 2139600
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, LX/EZd;->a(I)[B

    move-result-object v0

    .line 2139601
    const/16 v2, 0x20

    const/16 v4, 0x1f

    const/4 v3, 0x0

    .line 2139602
    new-array v1, v2, [B

    .line 2139603
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2139604
    aget-byte v2, v1, v3

    and-int/lit16 v2, v2, 0xf8

    int-to-byte v2, v2

    aput-byte v2, v1, v3

    .line 2139605
    aget-byte v2, v1, v4

    and-int/lit8 v2, v2, 0x7f

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    .line 2139606
    aget-byte v2, v1, v4

    or-int/lit8 v2, v2, 0x40

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    .line 2139607
    move-object v0, v1

    .line 2139608
    return-object v0
.end method

.method public a(I)[B
    .locals 2

    .prologue
    .line 2139609
    new-array v0, p1, [B

    .line 2139610
    iget-object v1, p0, LX/EZd;->b:LX/EZg;

    invoke-virtual {v1, v0}, LX/EZg;->a([B)V

    .line 2139611
    return-object v0
.end method

.method public calculateAgreement([B[B)[B
    .locals 13

    .prologue
    .line 2139612
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 2139613
    const/16 v4, 0x20

    const/4 v3, 0x0

    const/16 v1, 0xa

    .line 2139614
    new-array v5, v4, [B

    .line 2139615
    new-array v6, v1, [I

    .line 2139616
    new-array v7, v1, [I

    .line 2139617
    new-array v8, v1, [I

    .line 2139618
    new-array v9, v1, [I

    .line 2139619
    new-array v10, v1, [I

    .line 2139620
    new-array v11, v1, [I

    .line 2139621
    new-array v12, v1, [I

    move v1, v3

    .line 2139622
    :goto_0
    if-ge v1, v4, :cond_0

    aget-byte v2, p1, v1

    aput-byte v2, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2139623
    :cond_0
    invoke-static {v6, p2}, LX/EZu;->a([I[B)V

    .line 2139624
    invoke-static {v7}, LX/EZp;->a([I)V

    .line 2139625
    invoke-static {v8}, LX/EZo;->a([I)V

    .line 2139626
    invoke-static {v9, v6}, LX/EZs;->a([I[I)V

    .line 2139627
    invoke-static {v10}, LX/EZp;->a([I)V

    .line 2139628
    const/16 v1, 0xfe

    move v2, v3

    :goto_1
    if-ltz v1, :cond_1

    .line 2139629
    div-int/lit8 v4, v1, 0x8

    aget-byte v4, v5, v4

    and-int/lit8 p0, v1, 0x7

    ushr-int/2addr v4, p0

    .line 2139630
    and-int/lit8 v4, v4, 0x1

    .line 2139631
    xor-int/2addr v2, v4

    .line 2139632
    invoke-static {v7, v9, v2}, LX/EZt;->a([I[II)V

    .line 2139633
    invoke-static {v8, v10, v2}, LX/EZt;->a([I[II)V

    .line 2139634
    invoke-static {v11, v9, v10}, LX/Ea4;->a([I[I[I)V

    .line 2139635
    invoke-static {v12, v7, v8}, LX/Ea4;->a([I[I[I)V

    .line 2139636
    invoke-static {v7, v7, v8}, LX/EZq;->a([I[I[I)V

    .line 2139637
    invoke-static {v8, v9, v10}, LX/EZq;->a([I[I[I)V

    .line 2139638
    invoke-static {v10, v11, v7}, LX/EZz;->a([I[I[I)V

    .line 2139639
    invoke-static {v8, v8, v12}, LX/EZz;->a([I[I[I)V

    .line 2139640
    invoke-static {v11, v12}, LX/Ea3;->a([I[I)V

    .line 2139641
    invoke-static {v12, v7}, LX/Ea3;->a([I[I)V

    .line 2139642
    invoke-static {v9, v10, v8}, LX/EZq;->a([I[I[I)V

    .line 2139643
    invoke-static {v8, v10, v8}, LX/Ea4;->a([I[I[I)V

    .line 2139644
    invoke-static {v7, v12, v11}, LX/EZz;->a([I[I[I)V

    .line 2139645
    invoke-static {v12, v12, v11}, LX/Ea4;->a([I[I[I)V

    .line 2139646
    invoke-static {v8, v8}, LX/Ea3;->a([I[I)V

    .line 2139647
    invoke-static {v10, v12}, LX/EZy;->a([I[I)V

    .line 2139648
    invoke-static {v9, v9}, LX/Ea3;->a([I[I)V

    .line 2139649
    invoke-static {v11, v11, v10}, LX/EZq;->a([I[I[I)V

    .line 2139650
    invoke-static {v10, v6, v8}, LX/EZz;->a([I[I[I)V

    .line 2139651
    invoke-static {v8, v12, v11}, LX/EZz;->a([I[I[I)V

    .line 2139652
    add-int/lit8 v1, v1, -0x1

    move v2, v4

    goto :goto_1

    .line 2139653
    :cond_1
    invoke-static {v7, v9, v2}, LX/EZt;->a([I[II)V

    .line 2139654
    invoke-static {v8, v10, v2}, LX/EZt;->a([I[II)V

    .line 2139655
    invoke-static {v8, v8}, LX/EZv;->a([I[I)V

    .line 2139656
    invoke-static {v7, v7, v8}, LX/EZz;->a([I[I[I)V

    .line 2139657
    invoke-static {v0, v7}, LX/Ea5;->a([B[I)V

    .line 2139658
    return-object v0
.end method

.method public calculateSignature([B[B[B)[B
    .locals 6

    .prologue
    .line 2139659
    const/16 v0, 0x40

    new-array v1, v0, [B

    .line 2139660
    iget-object v0, p0, LX/EZd;->a:LX/EZh;

    array-length v4, p3

    move-object v2, p2

    move-object v3, p3

    move-object v5, p1

    invoke-static/range {v0 .. v5}, LX/EZn;->a(LX/EZh;[B[B[BI[B)I

    move-result v0

    if-eqz v0, :cond_0

    .line 2139661
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Message exceeds max length!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2139662
    :cond_0
    return-object v1
.end method

.method public generatePublicKey([B)[B
    .locals 7

    .prologue
    .line 2139663
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 2139664
    const/16 v5, 0xa

    .line 2139665
    new-instance v1, LX/EaI;

    invoke-direct {v1}, LX/EaI;-><init>()V

    .line 2139666
    new-array v2, v5, [I

    .line 2139667
    new-array v3, v5, [I

    .line 2139668
    new-array v4, v5, [I

    .line 2139669
    new-array v5, v5, [I

    .line 2139670
    invoke-static {v1, p1}, LX/EaU;->a(LX/EaI;[B)V

    .line 2139671
    iget-object v6, v1, LX/EaI;->b:[I

    iget-object p0, v1, LX/EaI;->c:[I

    invoke-static {v2, v6, p0}, LX/EZq;->a([I[I[I)V

    .line 2139672
    iget-object v6, v1, LX/EaI;->c:[I

    iget-object v1, v1, LX/EaI;->b:[I

    invoke-static {v3, v6, v1}, LX/Ea4;->a([I[I[I)V

    .line 2139673
    invoke-static {v4, v3}, LX/EZv;->a([I[I)V

    .line 2139674
    invoke-static {v5, v2, v4}, LX/EZz;->a([I[I[I)V

    .line 2139675
    invoke-static {v0, v5}, LX/Ea5;->a([B[I)V

    .line 2139676
    return-object v0
.end method

.method public verifySignature([B[B[B)Z
    .locals 11

    .prologue
    .line 2139677
    iget-object v0, p0, LX/EZd;->a:LX/EZh;

    array-length v1, p2

    .line 2139678
    const/16 v2, 0xa

    new-array v2, v2, [I

    .line 2139679
    const/16 v3, 0xa

    new-array v3, v3, [I

    .line 2139680
    const/16 v4, 0xa

    new-array v6, v4, [I

    .line 2139681
    const/16 v4, 0xa

    new-array v7, v4, [I

    .line 2139682
    const/16 v4, 0xa

    new-array v9, v4, [I

    .line 2139683
    const/16 v4, 0xa

    new-array v10, v4, [I

    .line 2139684
    const/16 v4, 0x20

    new-array v8, v4, [B

    .line 2139685
    add-int/lit8 v4, v1, 0x40

    new-array v5, v4, [B

    .line 2139686
    add-int/lit8 v4, v1, 0x40

    new-array v4, v4, [B

    .line 2139687
    invoke-static {v2, p1}, LX/EZu;->a([I[B)V

    .line 2139688
    invoke-static {v9}, LX/EZp;->a([I)V

    .line 2139689
    invoke-static {v3, v2, v9}, LX/Ea4;->a([I[I[I)V

    .line 2139690
    invoke-static {v6, v2, v9}, LX/EZq;->a([I[I[I)V

    .line 2139691
    invoke-static {v7, v6}, LX/EZv;->a([I[I)V

    .line 2139692
    invoke-static {v10, v3, v7}, LX/EZz;->a([I[I[I)V

    .line 2139693
    invoke-static {v8, v10}, LX/Ea5;->a([B[I)V

    .line 2139694
    const/16 v2, 0x1f

    aget-byte v3, v8, v2

    and-int/lit8 v3, v3, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v8, v2

    .line 2139695
    const/16 v2, 0x1f

    aget-byte v3, v8, v2

    const/16 v6, 0x3f

    aget-byte v6, p3, v6

    and-int/lit16 v6, v6, 0x80

    or-int/2addr v3, v6

    int-to-byte v3, v3

    aput-byte v3, v8, v2

    .line 2139696
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v6, 0x40

    invoke-static {p3, v2, v5, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2139697
    const/16 v2, 0x3f

    aget-byte v3, v5, v2

    and-int/lit8 v3, v3, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v5, v2

    .line 2139698
    const/4 v2, 0x0

    const/16 v3, 0x40

    invoke-static {p2, v2, v5, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2139699
    add-int/lit8 v2, v1, 0x40

    int-to-long v6, v2

    move-object v3, v0

    invoke-static/range {v3 .. v8}, LX/EaX;->a(LX/EZh;[B[BJ[B)I

    move-result v2

    move v0, v2

    .line 2139700
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
