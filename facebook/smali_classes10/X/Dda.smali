.class public final enum LX/Dda;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dda;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dda;

.field public static final enum DATA_KNOWN_TO_BE_STALE:LX/Dda;

.field public static final enum DEFAULT:LX/Dda;

.field public static final enum MQTT_CONNECTED:LX/Dda;

.field public static final enum MQTT_DISCONNECTED_AND_NOT_RECENTLY_UPDATED:LX/Dda;

.field public static final enum MQTT_RECENTLY_DISCONNECTED:LX/Dda;

.field public static final enum OLD_DATA:LX/Dda;

.field public static final enum OLD_DATA_PAGES_MANAGER:LX/Dda;

.field public static final enum SPECIFIC_INTENTION:LX/Dda;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2019854
    new-instance v0, LX/Dda;

    const-string v1, "SPECIFIC_INTENTION"

    invoke-direct {v0, v1, v3}, LX/Dda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dda;->SPECIFIC_INTENTION:LX/Dda;

    .line 2019855
    new-instance v0, LX/Dda;

    const-string v1, "MQTT_RECENTLY_DISCONNECTED"

    invoke-direct {v0, v1, v4}, LX/Dda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dda;->MQTT_RECENTLY_DISCONNECTED:LX/Dda;

    .line 2019856
    new-instance v0, LX/Dda;

    const-string v1, "MQTT_DISCONNECTED_AND_NOT_RECENTLY_UPDATED"

    invoke-direct {v0, v1, v5}, LX/Dda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dda;->MQTT_DISCONNECTED_AND_NOT_RECENTLY_UPDATED:LX/Dda;

    .line 2019857
    new-instance v0, LX/Dda;

    const-string v1, "MQTT_CONNECTED"

    invoke-direct {v0, v1, v6}, LX/Dda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dda;->MQTT_CONNECTED:LX/Dda;

    .line 2019858
    new-instance v0, LX/Dda;

    const-string v1, "OLD_DATA"

    invoke-direct {v0, v1, v7}, LX/Dda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dda;->OLD_DATA:LX/Dda;

    .line 2019859
    new-instance v0, LX/Dda;

    const-string v1, "OLD_DATA_PAGES_MANAGER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Dda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dda;->OLD_DATA_PAGES_MANAGER:LX/Dda;

    .line 2019860
    new-instance v0, LX/Dda;

    const-string v1, "DATA_KNOWN_TO_BE_STALE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Dda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dda;->DATA_KNOWN_TO_BE_STALE:LX/Dda;

    .line 2019861
    new-instance v0, LX/Dda;

    const-string v1, "DEFAULT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Dda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dda;->DEFAULT:LX/Dda;

    .line 2019862
    const/16 v0, 0x8

    new-array v0, v0, [LX/Dda;

    sget-object v1, LX/Dda;->SPECIFIC_INTENTION:LX/Dda;

    aput-object v1, v0, v3

    sget-object v1, LX/Dda;->MQTT_RECENTLY_DISCONNECTED:LX/Dda;

    aput-object v1, v0, v4

    sget-object v1, LX/Dda;->MQTT_DISCONNECTED_AND_NOT_RECENTLY_UPDATED:LX/Dda;

    aput-object v1, v0, v5

    sget-object v1, LX/Dda;->MQTT_CONNECTED:LX/Dda;

    aput-object v1, v0, v6

    sget-object v1, LX/Dda;->OLD_DATA:LX/Dda;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Dda;->OLD_DATA_PAGES_MANAGER:LX/Dda;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Dda;->DATA_KNOWN_TO_BE_STALE:LX/Dda;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Dda;->DEFAULT:LX/Dda;

    aput-object v2, v0, v1

    sput-object v0, LX/Dda;->$VALUES:[LX/Dda;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2019863
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dda;
    .locals 1

    .prologue
    .line 2019864
    const-class v0, LX/Dda;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dda;

    return-object v0
.end method

.method public static values()[LX/Dda;
    .locals 1

    .prologue
    .line 2019865
    sget-object v0, LX/Dda;->$VALUES:[LX/Dda;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dda;

    return-object v0
.end method
