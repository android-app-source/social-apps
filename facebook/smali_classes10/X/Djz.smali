.class public LX/Djz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Ck;

.field private final b:Landroid/content/Context;

.field private final c:LX/DkQ;

.field private final d:LX/0tX;

.field public final e:LX/03V;

.field public f:Z

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/DkQ;LX/0tX;LX/1Ck;Landroid/content/Context;LX/03V;)V
    .locals 1
    .param p1    # LX/DkQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2033896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2033897
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Djz;->f:Z

    .line 2033898
    iput-object p2, p0, LX/Djz;->d:LX/0tX;

    .line 2033899
    iput-object p3, p0, LX/Djz;->a:LX/1Ck;

    .line 2033900
    iput-object p4, p0, LX/Djz;->b:Landroid/content/Context;

    .line 2033901
    iput-object p1, p0, LX/Djz;->c:LX/DkQ;

    .line 2033902
    iput-object p5, p0, LX/Djz;->e:LX/03V;

    .line 2033903
    return-void
.end method

.method public static c(LX/Djz;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2033904
    new-instance v0, LX/Dke;

    invoke-direct {v0}, LX/Dke;-><init>()V

    move-object v2, v0

    .line 2033905
    iget-object v0, p0, LX/Djz;->c:LX/DkQ;

    invoke-virtual {v0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v0

    .line 2033906
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_PAST_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-ne v0, v1, :cond_0

    .line 2033907
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->PAST_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    .line 2033908
    const-string v0, "CONFIRMED_TIME_DESC"

    .line 2033909
    :goto_0
    const-string v3, "page_id"

    iget-object v4, p0, LX/Djz;->c:LX/DkQ;

    invoke-virtual {v4}, LX/DkQ;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "booking_time"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v3, "num_appointments"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v3, "profile_pic_size"

    iget-object v4, p0, LX/Djz;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b1e6c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v3, "after"

    iget-object v4, p0, LX/Djz;->g:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "orderby"

    invoke-virtual {v1, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2033910
    iget-object v0, p0, LX/Djz;->d:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2033911
    new-instance v1, LX/Djy;

    invoke-direct {v1, p0}, LX/Djy;-><init>(LX/Djz;)V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2033912
    :cond_0
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_UPCOMING_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-ne v0, v1, :cond_1

    .line 2033913
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;->FUTURE_ONLY:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingTimeFilter;

    .line 2033914
    const-string v0, "CONFIRMED_TIME"

    goto :goto_0

    .line 2033915
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Query Scenario "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not supported"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
