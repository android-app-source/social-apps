.class public LX/Do5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2041448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2041449
    return-void
.end method

.method public static a(LX/Do4;LX/Do1;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Do4",
            "<TT;>;",
            "LX/Do1",
            "<TT;>;)",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 2041450
    new-instance v2, Ljava/util/HashMap;

    iget-object v0, p0, LX/Do4;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 2041451
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v3

    .line 2041452
    iget-object v4, p0, LX/Do4;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Do3;

    .line 2041453
    :try_start_0
    iget-object v6, v0, LX/Do3;->a:Ljava/lang/Object;

    iget-object v7, v0, LX/Do3;->b:LX/1qM;

    iget-object v0, v0, LX/Do3;->c:LX/1qK;

    invoke-interface {v7, v0}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    invoke-virtual {v2, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2041454
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2041455
    :catch_0
    move-exception v0

    .line 2041456
    invoke-virtual {v3, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_1

    .line 2041457
    :cond_0
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 2041458
    invoke-interface {p1, v2, v0}, LX/Do1;->a(Ljava/util/Map;LX/0Rf;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2041459
    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2041460
    new-instance v2, LX/Do0;

    invoke-direct {v2, v1, v0}, LX/Do0;-><init>(Landroid/os/Parcelable;Ljava/util/Collection;)V

    throw v2

    .line 2041461
    :cond_1
    return-object v1
.end method
