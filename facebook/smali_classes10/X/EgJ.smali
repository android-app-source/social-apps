.class public final LX/EgJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/bookmark/client/BookmarkClient;


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/google/common/util/concurrent/SettableFuture;J)V
    .locals 1

    .prologue
    .line 2156015
    iput-object p1, p0, LX/EgJ;->c:Lcom/facebook/bookmark/client/BookmarkClient;

    iput-object p2, p0, LX/EgJ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-wide p3, p0, LX/EgJ;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2156016
    iget-object v0, p0, LX/EgJ;->c:Lcom/facebook/bookmark/client/BookmarkClient;

    iget-wide v2, p0, LX/EgJ;->b:J

    .line 2156017
    iget-object v1, v0, Lcom/facebook/bookmark/client/BookmarkClient;->i:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2156018
    const-string v1, "handleRemoveFromFavoritesFailure"

    invoke-static {v0, p1, v1}, Lcom/facebook/bookmark/client/BookmarkClient;->a$redex0(Lcom/facebook/bookmark/client/BookmarkClient;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2156019
    iget-object v0, p0, LX/EgJ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2156020
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2156021
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2156022
    iget-object v0, p0, LX/EgJ;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x7948696a

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2156023
    return-void
.end method
