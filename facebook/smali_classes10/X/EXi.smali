.class public final LX/EXi;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EXh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EXi;",
        ">;",
        "LX/EXh;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field public e:LX/EXn;

.field public f:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/EXn;",
            "LX/EXm;",
            "LX/EXl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2134923
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2134924
    const-string v0, ""

    iput-object v0, p0, LX/EXi;->b:Ljava/lang/Object;

    .line 2134925
    const-string v0, ""

    iput-object v0, p0, LX/EXi;->c:Ljava/lang/Object;

    .line 2134926
    const-string v0, ""

    iput-object v0, p0, LX/EXi;->d:Ljava/lang/Object;

    .line 2134927
    sget-object v0, LX/EXn;->c:LX/EXn;

    move-object v0, v0

    .line 2134928
    iput-object v0, p0, LX/EXi;->e:LX/EXn;

    .line 2134929
    invoke-direct {p0}, LX/EXi;->m()V

    .line 2134930
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2134931
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2134932
    const-string v0, ""

    iput-object v0, p0, LX/EXi;->b:Ljava/lang/Object;

    .line 2134933
    const-string v0, ""

    iput-object v0, p0, LX/EXi;->c:Ljava/lang/Object;

    .line 2134934
    const-string v0, ""

    iput-object v0, p0, LX/EXi;->d:Ljava/lang/Object;

    .line 2134935
    sget-object v0, LX/EXn;->c:LX/EXn;

    move-object v0, v0

    .line 2134936
    iput-object v0, p0, LX/EXi;->e:LX/EXn;

    .line 2134937
    invoke-direct {p0}, LX/EXi;->m()V

    .line 2134938
    return-void
.end method

.method private d(LX/EWY;)LX/EXi;
    .locals 1

    .prologue
    .line 2134939
    instance-of v0, p1, LX/EXj;

    if-eqz v0, :cond_0

    .line 2134940
    check-cast p1, LX/EXj;

    invoke-virtual {p0, p1}, LX/EXi;->a(LX/EXj;)LX/EXi;

    move-result-object p0

    .line 2134941
    :goto_0
    return-object p0

    .line 2134942
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EXi;
    .locals 4

    .prologue
    .line 2134943
    const/4 v2, 0x0

    .line 2134944
    :try_start_0
    sget-object v0, LX/EXj;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXj;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2134945
    if-eqz v0, :cond_0

    .line 2134946
    invoke-virtual {p0, v0}, LX/EXi;->a(LX/EXj;)LX/EXi;

    .line 2134947
    :cond_0
    return-object p0

    .line 2134948
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2134949
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2134950
    check-cast v0, LX/EXj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2134951
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2134952
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2134953
    invoke-virtual {p0, v1}, LX/EXi;->a(LX/EXj;)LX/EXi;

    :cond_1
    throw v0

    .line 2134954
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 2134955
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2134956
    iget-object v0, p0, LX/EXi;->f:LX/EZ7;

    if-nez v0, :cond_0

    .line 2134957
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/EXi;->e:LX/EXn;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2134958
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2134959
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/EXi;->f:LX/EZ7;

    .line 2134960
    const/4 v0, 0x0

    iput-object v0, p0, LX/EXi;->e:LX/EXn;

    .line 2134961
    :cond_0
    return-void
.end method

.method public static n()LX/EXi;
    .locals 1

    .prologue
    .line 2134962
    new-instance v0, LX/EXi;

    invoke-direct {v0}, LX/EXi;-><init>()V

    return-object v0
.end method

.method private u()LX/EXi;
    .locals 2

    .prologue
    .line 2135024
    invoke-static {}, LX/EXi;->n()LX/EXi;

    move-result-object v0

    invoke-direct {p0}, LX/EXi;->y()LX/EXj;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EXi;->a(LX/EXj;)LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method private x()LX/EXj;
    .locals 2

    .prologue
    .line 2134963
    invoke-direct {p0}, LX/EXi;->y()LX/EXj;

    move-result-object v0

    .line 2134964
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2134965
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2134966
    :cond_0
    return-object v0
.end method

.method private y()LX/EXj;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2134967
    new-instance v2, LX/EXj;

    invoke-direct {v2, p0}, LX/EXj;-><init>(LX/EWj;)V

    .line 2134968
    iget v3, p0, LX/EXi;->a:I

    .line 2134969
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 2134970
    :goto_0
    iget-object v1, p0, LX/EXi;->b:Ljava/lang/Object;

    .line 2134971
    iput-object v1, v2, LX/EXj;->name_:Ljava/lang/Object;

    .line 2134972
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2134973
    or-int/lit8 v0, v0, 0x2

    .line 2134974
    :cond_0
    iget-object v1, p0, LX/EXi;->c:Ljava/lang/Object;

    .line 2134975
    iput-object v1, v2, LX/EXj;->inputType_:Ljava/lang/Object;

    .line 2134976
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2134977
    or-int/lit8 v0, v0, 0x4

    .line 2134978
    :cond_1
    iget-object v1, p0, LX/EXi;->d:Ljava/lang/Object;

    .line 2134979
    iput-object v1, v2, LX/EXj;->outputType_:Ljava/lang/Object;

    .line 2134980
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 2134981
    or-int/lit8 v0, v0, 0x8

    move v1, v0

    .line 2134982
    :goto_1
    iget-object v0, p0, LX/EXi;->f:LX/EZ7;

    if-nez v0, :cond_2

    .line 2134983
    iget-object v0, p0, LX/EXi;->e:LX/EXn;

    .line 2134984
    iput-object v0, v2, LX/EXj;->options_:LX/EXn;

    .line 2134985
    :goto_2
    iput v1, v2, LX/EXj;->bitField0_:I

    .line 2134986
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2134987
    return-object v2

    .line 2134988
    :cond_2
    iget-object v0, p0, LX/EXi;->f:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EXn;

    .line 2134989
    iput-object v0, v2, LX/EXj;->options_:LX/EXn;

    .line 2134990
    goto :goto_2

    :cond_3
    move v1, v0

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2134991
    invoke-direct {p0, p1}, LX/EXi;->d(LX/EWY;)LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2134992
    invoke-direct {p0, p1, p2}, LX/EXi;->d(LX/EWd;LX/EYZ;)LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EXj;)LX/EXi;
    .locals 3

    .prologue
    .line 2134993
    sget-object v0, LX/EXj;->c:LX/EXj;

    move-object v0, v0

    .line 2134994
    if-ne p1, v0, :cond_0

    .line 2134995
    :goto_0
    return-object p0

    .line 2134996
    :cond_0
    const/4 v0, 0x1

    .line 2134997
    iget v1, p1, LX/EXj;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_5

    :goto_1
    move v0, v0

    .line 2134998
    if-eqz v0, :cond_1

    .line 2134999
    iget v0, p0, LX/EXi;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EXi;->a:I

    .line 2135000
    iget-object v0, p1, LX/EXj;->name_:Ljava/lang/Object;

    iput-object v0, p0, LX/EXi;->b:Ljava/lang/Object;

    .line 2135001
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135002
    :cond_1
    iget v0, p1, LX/EXj;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2135003
    if-eqz v0, :cond_2

    .line 2135004
    iget v0, p0, LX/EXi;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EXi;->a:I

    .line 2135005
    iget-object v0, p1, LX/EXj;->inputType_:Ljava/lang/Object;

    iput-object v0, p0, LX/EXi;->c:Ljava/lang/Object;

    .line 2135006
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135007
    :cond_2
    iget v0, p1, LX/EXj;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2135008
    if-eqz v0, :cond_3

    .line 2135009
    iget v0, p0, LX/EXi;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EXi;->a:I

    .line 2135010
    iget-object v0, p1, LX/EXj;->outputType_:Ljava/lang/Object;

    iput-object v0, p0, LX/EXi;->d:Ljava/lang/Object;

    .line 2135011
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135012
    :cond_3
    invoke-virtual {p1}, LX/EXj;->p()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2135013
    iget-object v0, p1, LX/EXj;->options_:LX/EXn;

    move-object v0, v0

    .line 2135014
    iget-object v1, p0, LX/EXi;->f:LX/EZ7;

    if-nez v1, :cond_9

    .line 2135015
    iget v1, p0, LX/EXi;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_8

    iget-object v1, p0, LX/EXi;->e:LX/EXn;

    .line 2135016
    sget-object v2, LX/EXn;->c:LX/EXn;

    move-object v2, v2

    .line 2135017
    if-eq v1, v2, :cond_8

    .line 2135018
    iget-object v1, p0, LX/EXi;->e:LX/EXn;

    invoke-static {v1}, LX/EXn;->a(LX/EXn;)LX/EXm;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/EXm;->a(LX/EXn;)LX/EXm;

    move-result-object v1

    invoke-virtual {v1}, LX/EXm;->l()LX/EXn;

    move-result-object v1

    iput-object v1, p0, LX/EXi;->e:LX/EXn;

    .line 2135019
    :goto_4
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135020
    :goto_5
    iget v1, p0, LX/EXi;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, LX/EXi;->a:I

    .line 2135021
    :cond_4
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 2135022
    :cond_8
    iput-object v0, p0, LX/EXi;->e:LX/EXn;

    goto :goto_4

    .line 2135023
    :cond_9
    iget-object v1, p0, LX/EXi;->f:LX/EZ7;

    invoke-virtual {v1, v0}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto :goto_5
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2134914
    iget v0, p0, LX/EXi;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2134915
    if-eqz v0, :cond_0

    .line 2134916
    iget-object v0, p0, LX/EXi;->f:LX/EZ7;

    if-nez v0, :cond_2

    .line 2134917
    iget-object v0, p0, LX/EXi;->e:LX/EXn;

    .line 2134918
    :goto_1
    move-object v0, v0

    .line 2134919
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2134920
    const/4 v0, 0x0

    .line 2134921
    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/EXi;->f:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->c()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EXn;

    goto :goto_1
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2134922
    invoke-direct {p0, p1, p2}, LX/EXi;->d(LX/EWd;LX/EYZ;)LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2134900
    invoke-direct {p0}, LX/EXi;->u()LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2134901
    invoke-direct {p0, p1, p2}, LX/EXi;->d(LX/EWd;LX/EYZ;)LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2134902
    invoke-direct {p0}, LX/EXi;->u()LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2134903
    invoke-direct {p0, p1}, LX/EXi;->d(LX/EWY;)LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2134904
    invoke-direct {p0}, LX/EXi;->u()LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2134905
    sget-object v0, LX/EYC;->r:LX/EYn;

    const-class v1, LX/EXj;

    const-class v2, LX/EXi;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2134906
    sget-object v0, LX/EYC;->q:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2134907
    invoke-direct {p0}, LX/EXi;->u()LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2134908
    invoke-direct {p0}, LX/EXi;->y()LX/EXj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2134909
    invoke-direct {p0}, LX/EXi;->x()LX/EXj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2134910
    invoke-direct {p0}, LX/EXi;->y()LX/EXj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2134911
    invoke-direct {p0}, LX/EXi;->x()LX/EXj;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2134912
    sget-object v0, LX/EXj;->c:LX/EXj;

    move-object v0, v0

    .line 2134913
    return-object v0
.end method
