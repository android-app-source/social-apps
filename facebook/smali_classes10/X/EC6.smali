.class public final enum LX/EC6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EC6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EC6;

.field public static final enum FLOAT:LX/EC6;

.field public static final enum FULL:LX/EC6;

.field public static final enum MULTI_VIEW:LX/EC6;

.field public static final enum NONE:LX/EC6;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2088232
    new-instance v0, LX/EC6;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/EC6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC6;->NONE:LX/EC6;

    .line 2088233
    new-instance v0, LX/EC6;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v3}, LX/EC6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC6;->FLOAT:LX/EC6;

    .line 2088234
    new-instance v0, LX/EC6;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v4}, LX/EC6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC6;->FULL:LX/EC6;

    .line 2088235
    new-instance v0, LX/EC6;

    const-string v1, "MULTI_VIEW"

    invoke-direct {v0, v1, v5}, LX/EC6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EC6;->MULTI_VIEW:LX/EC6;

    .line 2088236
    const/4 v0, 0x4

    new-array v0, v0, [LX/EC6;

    sget-object v1, LX/EC6;->NONE:LX/EC6;

    aput-object v1, v0, v2

    sget-object v1, LX/EC6;->FLOAT:LX/EC6;

    aput-object v1, v0, v3

    sget-object v1, LX/EC6;->FULL:LX/EC6;

    aput-object v1, v0, v4

    sget-object v1, LX/EC6;->MULTI_VIEW:LX/EC6;

    aput-object v1, v0, v5

    sput-object v0, LX/EC6;->$VALUES:[LX/EC6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2088237
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EC6;
    .locals 1

    .prologue
    .line 2088238
    const-class v0, LX/EC6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EC6;

    return-object v0
.end method

.method public static values()[LX/EC6;
    .locals 1

    .prologue
    .line 2088239
    sget-object v0, LX/EC6;->$VALUES:[LX/EC6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EC6;

    return-object v0
.end method
