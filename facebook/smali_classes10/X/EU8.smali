.class public LX/EU8;
.super LX/ETf;
.source ""


# instance fields
.field public final a:LX/3DC;

.field public final b:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

.field private final c:LX/0xX;

.field private final d:LX/1CC;


# direct methods
.method public constructor <init>(LX/3DC;Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;LX/0xX;LX/1CC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125861
    invoke-direct {p0}, LX/ETf;-><init>()V

    .line 2125862
    iput-object p1, p0, LX/EU8;->a:LX/3DC;

    .line 2125863
    iput-object p2, p0, LX/EU8;->b:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    .line 2125864
    iput-object p3, p0, LX/EU8;->c:LX/0xX;

    .line 2125865
    iput-object p4, p0, LX/EU8;->d:LX/1CC;

    .line 2125866
    return-void
.end method

.method private a(LX/0Px;IIZ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/9uc;",
            ">;IIZ)V"
        }
    .end annotation

    .prologue
    .line 2125835
    if-ltz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2125836
    add-int/lit8 v0, p2, 0x1

    move v1, v0

    :goto_1
    add-int v0, p2, p3

    if-gt v1, v0, :cond_2

    .line 2125837
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2125838
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2125839
    if-nez v0, :cond_3

    .line 2125840
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2125841
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2125842
    :cond_2
    return-void

    .line 2125843
    :cond_3
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2125844
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v3, v4, :cond_0

    .line 2125845
    if-eqz p4, :cond_5

    .line 2125846
    iget-object v3, p0, LX/EU8;->b:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    const/4 v9, 0x2

    .line 2125847
    iget-object v5, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->f:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 2125848
    iget-wide v7, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->k:J

    sub-long/2addr v5, v7

    .line 2125849
    iget-object v7, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->e:LX/19j;

    iget-wide v7, v7, LX/19j;->aQ:J

    sub-long v5, v7, v5

    .line 2125850
    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-gtz v7, :cond_6

    .line 2125851
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    .line 2125852
    iget-object v5, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->c:LX/3DC;

    sget-object v6, LX/379;->VIDEO_HOME:LX/379;

    invoke-virtual {v5, v2, v6}, LX/3DC;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    .line 2125853
    :cond_4
    :goto_3
    goto :goto_2

    .line 2125854
    :cond_5
    iget-object v3, p0, LX/EU8;->a:LX/3DC;

    sget-object v4, LX/379;->VIDEO_HOME:LX/379;

    invoke-virtual {v3, v2, v4}, LX/3DC;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    goto :goto_2

    .line 2125855
    :cond_6
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    .line 2125856
    iget-object v7, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->j:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7, v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 2125857
    iget-object v7, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->i:Landroid/os/Handler;

    if-nez v7, :cond_7

    .line 2125858
    new-instance v7, LX/ETW;

    invoke-direct {v7, v3}, LX/ETW;-><init>(Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;)V

    iput-object v7, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->i:Landroid/os/Handler;

    .line 2125859
    :cond_7
    iget-object v7, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->i:Landroid/os/Handler;

    invoke-virtual {v7, v9}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-nez v7, :cond_4

    .line 2125860
    iget-object v7, v3, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->i:Landroid/os/Handler;

    invoke-virtual {v7, v9, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_3
.end method

.method private static b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/0Px",
            "<",
            "LX/9uc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2125816
    instance-of v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_0

    .line 2125817
    check-cast p0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125818
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2125819
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2125820
    :goto_0
    move-object v0, v0

    .line 2125821
    :goto_1
    return-object v0

    .line 2125822
    :cond_0
    invoke-static {p0}, LX/Cfu;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v0

    .line 2125823
    if-nez v0, :cond_1

    .line 2125824
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2125825
    :cond_1
    move-object v0, v0

    .line 2125826
    goto :goto_1

    .line 2125827
    :cond_2
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2125828
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    .line 2125829
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v2}, LX/ETQ;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 2125830
    invoke-virtual {v2, v0}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v3

    .line 2125831
    iget-object p0, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, p0

    .line 2125832
    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2125833
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2125834
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/EU8;
    .locals 5

    .prologue
    .line 2125813
    new-instance v4, LX/EU8;

    invoke-static {p0}, LX/3DC;->b(LX/0QB;)LX/3DC;

    move-result-object v0

    check-cast v0, LX/3DC;

    invoke-static {p0}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->a(LX/0QB;)Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v2

    check-cast v2, LX/0xX;

    invoke-static {p0}, LX/1CC;->a(LX/0QB;)LX/1CC;

    move-result-object v3

    check-cast v3, LX/1CC;

    invoke-direct {v4, v0, v1, v2, v3}, LX/EU8;-><init>(LX/3DC;Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;LX/0xX;LX/1CC;)V

    .line 2125814
    move-object v0, v4

    .line 2125815
    return-object v0
.end method


# virtual methods
.method public final a(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 3

    .prologue
    .line 2125808
    iget-object v0, p0, LX/EU8;->d:LX/1CC;

    invoke-virtual {v0}, LX/1CC;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2125809
    invoke-static {p4}, LX/EU8;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-direct {p0, v0, p1, v1, v2}, LX/EU8;->a(LX/0Px;IIZ)V

    .line 2125810
    :cond_0
    return-void
.end method

.method public final b(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 3

    .prologue
    .line 2125811
    invoke-static {p4}, LX/EU8;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, LX/EU8;->a(LX/0Px;IIZ)V

    .line 2125812
    return-void
.end method
