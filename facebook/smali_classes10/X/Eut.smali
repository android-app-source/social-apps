.class public LX/Eut;
.super LX/Eus;
.source ""

# interfaces
.implements LX/83X;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Z

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Z

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>(LX/Eur;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2180052
    invoke-direct {p0, p1}, LX/Eus;-><init>(LX/Euq;)V

    .line 2180053
    iget-object v0, p1, LX/Eur;->c:Ljava/lang/String;

    iput-object v0, p0, LX/Eut;->b:Ljava/lang/String;

    .line 2180054
    iget-boolean v0, p1, LX/Eur;->b:Z

    iput-boolean v0, p0, LX/Eut;->g:Z

    .line 2180055
    iget-object v0, p0, LX/Eut;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/Eut;->c:Z

    .line 2180056
    iget-boolean v0, p1, LX/Eur;->a:Z

    iput-boolean v0, p0, LX/Eut;->e:Z

    .line 2180057
    iput-boolean v1, p0, LX/Eut;->f:Z

    .line 2180058
    iget-object v0, p1, LX/Eur;->d:Ljava/lang/String;

    iput-object v0, p0, LX/Eut;->d:Ljava/lang/String;

    .line 2180059
    return-void

    .line 2180060
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final e()I
    .locals 2

    .prologue
    .line 2180061
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FriendRequestItemModel does not support this operation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2180062
    iget-object v0, p0, LX/Eut;->d:Ljava/lang/String;

    return-object v0
.end method
