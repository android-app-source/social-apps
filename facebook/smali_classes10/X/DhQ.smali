.class public LX/DhQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/messaging/model/threads/ThreadSummary;

.field public final b:Lcom/facebook/messaging/model/messages/MessagesCollection;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static newBuilder()LX/DhP;
    .locals 1

    .prologue
    .line 2030941
    new-instance v0, LX/DhP;

    invoke-direct {v0}, LX/DhP;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2030943
    if-ne p0, p1, :cond_1

    .line 2030944
    :cond_0
    :goto_0
    return v0

    .line 2030945
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2030946
    goto :goto_0

    .line 2030947
    :cond_3
    check-cast p1, LX/DhQ;

    .line 2030948
    iget-object v2, p0, LX/DhQ;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v3, p1, LX/DhQ;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/DhQ;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v3, p1, LX/DhQ;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/DhQ;->c:LX/0Px;

    iget-object v3, p1, LX/DhQ;->c:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2030942
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/DhQ;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/DhQ;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/DhQ;->c:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
