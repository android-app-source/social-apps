.class public final LX/DTV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/DTZ;


# direct methods
.method public constructor <init>(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2000413
    iput-object p1, p0, LX/DTV;->d:LX/DTZ;

    iput-object p2, p0, LX/DTV;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DTV;->b:Landroid/content/Context;

    iput-object p4, p0, LX/DTV;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    .line 2000414
    new-instance v1, LX/DTU;

    invoke-direct {v1, p0}, LX/DTU;-><init>(LX/DTV;)V

    .line 2000415
    iget-object v0, p0, LX/DTV;->d:LX/DTZ;

    iget-object v2, p0, LX/DTV;->b:Landroid/content/Context;

    const v3, 0x7f082fab

    const v4, 0x7f082fa8

    iget-object v5, p0, LX/DTV;->d:LX/DTZ;

    const v6, 0x7f082fa9

    iget-object v7, p0, LX/DTV;->c:Ljava/lang/String;

    invoke-static {v5, v6, v7}, LX/DTZ;->a$redex0(LX/DTZ;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v5

    .line 2000416
    new-instance v6, LX/0ju;

    invoke-direct {v6, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2000417
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000418
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p0, 0x7f082f93

    invoke-virtual {v7, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance p0, LX/DTH;

    invoke-direct {p0, v0}, LX/DTH;-><init>(LX/DTZ;)V

    invoke-virtual {v6, v7, p0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000419
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000420
    invoke-virtual {v6, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000421
    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v6

    .line 2000422
    invoke-virtual {v6}, LX/2EJ;->show()V

    .line 2000423
    const v7, 0x7f0d0578

    invoke-virtual {v6, v7}, LX/2EJ;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2000424
    if-eqz v6, :cond_0

    instance-of v7, v6, Landroid/widget/TextView;

    if-eqz v7, :cond_0

    .line 2000425
    check-cast v6, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2000426
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
