.class public final LX/DyZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/checkin/PlacePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V
    .locals 0

    .prologue
    .line 2064744
    iput-object p1, p0, LX/DyZ;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 2064745
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    .line 2064746
    iget-object v0, p0, LX/DyZ;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2064747
    iget-boolean v1, v0, LX/9j5;->m:Z

    if-nez v1, :cond_0

    .line 2064748
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_first_scroll"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/9j5;->a(LX/9j5;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2064749
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/9j5;->m:Z

    .line 2064750
    iget-object v0, p0, LX/DyZ;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iget-object v1, p0, LX/DyZ;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v1, v1, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v1

    iget-object v2, p0, LX/DyZ;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v2, v2, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v2}, Lcom/facebook/widget/listview/BetterListView;->getLastVisiblePosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/9j5;->a(II)V

    .line 2064751
    iget-object v0, p0, LX/DyZ;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-static {v0, p1}, Lcom/facebook/places/checkin/PlacePickerFragment;->c(Lcom/facebook/places/checkin/PlacePickerFragment;Landroid/view/View;)V

    .line 2064752
    return-void
.end method
