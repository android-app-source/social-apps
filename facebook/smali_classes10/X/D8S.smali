.class public LX/D8S;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/media/AudioManager;

.field private final b:LX/1qa;

.field private final c:LX/2mZ;

.field public final d:LX/Bug;

.field public final e:LX/99w;

.field private final f:LX/1C2;

.field private final g:LX/2nF;

.field public final h:LX/D8K;

.field private final i:LX/D8I;

.field private final j:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

.field public final k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

.field public l:Z

.field public m:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/394;

.field public o:F

.field public p:F

.field public q:F

.field public r:Lcom/facebook/video/player/RichVideoPlayer;

.field public s:LX/2pa;

.field public t:LX/04D;

.field public u:LX/04g;

.field public v:Landroid/view/ViewGroup;

.field public w:LX/D8g;

.field public x:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;LX/1qa;LX/2mZ;LX/Bug;LX/99w;LX/1C2;LX/2nF;LX/D8K;LX/D8J;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1968682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1968683
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, LX/D8S;->a:Landroid/media/AudioManager;

    .line 1968684
    iput-object p4, p0, LX/D8S;->b:LX/1qa;

    .line 1968685
    iput-object p5, p0, LX/D8S;->c:LX/2mZ;

    .line 1968686
    iput-object p6, p0, LX/D8S;->d:LX/Bug;

    .line 1968687
    iput-object p9, p0, LX/D8S;->g:LX/2nF;

    .line 1968688
    iput-object p2, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    .line 1968689
    iput-object p3, p0, LX/D8S;->j:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    .line 1968690
    new-instance v0, LX/D8P;

    invoke-direct {v0, p0}, LX/D8P;-><init>(LX/D8S;)V

    .line 1968691
    new-instance p2, LX/D8I;

    invoke-static {p11}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object p1

    check-cast p1, LX/189;

    const/16 p3, 0x123

    invoke-static {p11, p3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p3

    invoke-direct {p2, v0, p1, p3}, LX/D8I;-><init>(LX/0QK;LX/189;LX/0Or;)V

    .line 1968692
    move-object v0, p2

    .line 1968693
    iput-object v0, p0, LX/D8S;->i:LX/D8I;

    .line 1968694
    iput-object p7, p0, LX/D8S;->e:LX/99w;

    .line 1968695
    iput-object p8, p0, LX/D8S;->f:LX/1C2;

    .line 1968696
    iput-object p10, p0, LX/D8S;->h:LX/D8K;

    .line 1968697
    return-void
.end method

.method public static a(LX/D8S;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2pa;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/2pa;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x1

    .line 1968650
    if-nez p1, :cond_0

    move-object v0, v1

    .line 1968651
    :goto_0
    return-object v0

    .line 1968652
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1968653
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1968654
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1968655
    goto :goto_0

    .line 1968656
    :cond_1
    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1968657
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-static {v3}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v3

    .line 1968658
    if-nez v3, :cond_2

    move-object v0, v1

    .line 1968659
    goto :goto_0

    .line 1968660
    :cond_2
    iget-object v1, p0, LX/D8S;->c:LX/2mZ;

    invoke-virtual {v1, v2, v3}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v1

    .line 1968661
    invoke-virtual {v1}, LX/3Im;->a()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v2

    .line 1968662
    iget-object v1, p0, LX/D8S;->b:LX/1qa;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    sget-object v4, LX/26P;->Video:LX/26P;

    invoke-virtual {v1, v0, v4}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v4

    .line 1968663
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v0

    .line 1968664
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v1

    .line 1968665
    if-eqz v1, :cond_5

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    int-to-double v8, v0

    mul-double/2addr v6, v8

    int-to-double v0, v1

    div-double v0, v6, v0

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 1968666
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 1968667
    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v1, v3, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v5, "CoverImageParamsKey"

    invoke-virtual {v3, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1968668
    iget-object v3, p0, LX/D8S;->w:LX/D8g;

    sget-object v4, LX/D8g;->WATCH_AND_BROWSE:LX/D8g;

    if-ne v3, v4, :cond_6

    iget-object v3, p0, LX/D8S;->g:LX/2nF;

    invoke-virtual {v3}, LX/2nF;->a()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1968669
    const-string v3, "CanDismissWatchAndMoreVideoPlayer"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1968670
    :cond_3
    :goto_2
    iget-object v3, p0, LX/D8S;->w:LX/D8g;

    sget-object v4, LX/D8g;->WATCH_AND_LEADGEN:LX/D8g;

    if-ne v3, v4, :cond_4

    .line 1968671
    const-string v3, "CanCloseWatchAndMore"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1968672
    :cond_4
    new-instance v3, LX/2pZ;

    invoke-direct {v3}, LX/2pZ;-><init>()V

    .line 1968673
    iput-object v2, v3, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1968674
    move-object v2, v3

    .line 1968675
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 1968676
    iput-wide v4, v2, LX/2pZ;->e:D

    .line 1968677
    move-object v0, v2

    .line 1968678
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    goto/16 :goto_0

    .line 1968679
    :cond_5
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 1968680
    :cond_6
    iget-object v3, p0, LX/D8S;->w:LX/D8g;

    sget-object v4, LX/D8g;->WATCH_AND_INSTALL:LX/D8g;

    if-ne v3, v4, :cond_3

    .line 1968681
    const-string v3, "CanCloseWatchAndMore"

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_2
.end method

.method public static a(LX/D8S;II)V
    .locals 2

    .prologue
    .line 1968645
    int-to-float v0, p1

    iput v0, p0, LX/D8S;->o:F

    .line 1968646
    iget v0, p0, LX/D8S;->o:F

    int-to-float v1, p2

    add-float/2addr v0, v1

    iput v0, p0, LX/D8S;->p:F

    .line 1968647
    iget v0, p0, LX/D8S;->p:F

    iput v0, p0, LX/D8S;->q:F

    .line 1968648
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D8S;->l:Z

    .line 1968649
    return-void
.end method

.method private static a(LX/D8S;IILjava/util/Map;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1968698
    iget-object v0, p0, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 1968699
    iget-object v0, p0, LX/D8S;->f:LX/1C2;

    sget-object v2, LX/04G;->WATCH_AND_BROWSE:LX/04G;

    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v4, p0, LX/D8S;->s:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, LX/D8S;->t:LX/04D;

    iget-object v6, p0, LX/D8S;->u:LX/04g;

    iget-object v6, v6, LX/04g;->value:Ljava/lang/String;

    iget-object v7, p0, LX/D8S;->s:LX/2pa;

    iget-object v9, v7, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move v7, p1

    move v8, p2

    move-object/from16 v10, p3

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1968700
    iget-object v0, p0, LX/D8S;->f:LX/1C2;

    sget-object v2, LX/04G;->WATCH_AND_BROWSE:LX/04G;

    sget-object v3, LX/04g;->BY_PLAYER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, p0, LX/D8S;->s:LX/2pa;

    iget-object v5, v5, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v6, p0, LX/D8S;->t:LX/04D;

    iget-object v7, p0, LX/D8S;->s:LX/2pa;

    iget-object v7, v7, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1968701
    return-void
.end method

.method public static a(LX/D8S;LX/7Jv;)V
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 1968642
    iget-object v0, p0, LX/D8S;->f:LX/1C2;

    iget-object v1, p0, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    sget-object v3, LX/04G;->WATCH_AND_BROWSE:LX/04G;

    iget-object v4, p0, LX/D8S;->s:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v5, p0, LX/D8S;->t:LX/04D;

    iget-object v6, p0, LX/D8S;->u:LX/04g;

    iget-object v6, v6, LX/04g;->value:Ljava/lang/String;

    iget v7, p1, LX/7Jv;->c:I

    iget v8, p1, LX/7Jv;->d:I

    iget-object v9, p0, LX/D8S;->s:LX/2pa;

    iget-object v9, v9, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v11, v10

    move-object v12, v10

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1968643
    iget-object v0, p0, LX/D8S;->f:LX/1C2;

    iget-object v1, p0, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->WATCH_AND_BROWSE:LX/04G;

    sget-object v3, LX/04g;->BY_PLAYER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, p0, LX/D8S;->s:LX/2pa;

    iget-object v5, v5, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v6, p0, LX/D8S;->t:LX/04D;

    iget-object v7, p0, LX/D8S;->s:LX/2pa;

    iget-object v7, v7, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1968644
    return-void
.end method


# virtual methods
.method public final a(LX/D8U;IIILcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/ViewGroup;LX/04D;LX/04g;LX/3FT;LX/D8g;LX/D7g;)V
    .locals 8
    .param p1    # LX/D8U;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/D7g;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin$DismissVideoCallback;",
            "III",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/ViewGroup;",
            "LX/04D;",
            "LX/04g;",
            "LX/3FT;",
            "LX/D8g;",
            "LX/D7g;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1968576
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968577
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968578
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968579
    invoke-static/range {p8 .. p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968580
    invoke-static/range {p9 .. p9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968581
    invoke-static/range {p10 .. p10}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968582
    move-object/from16 v0, p8

    iput-object v0, p0, LX/D8S;->u:LX/04g;

    .line 1968583
    iput-object p6, p0, LX/D8S;->v:Landroid/view/ViewGroup;

    .line 1968584
    iput-object p7, p0, LX/D8S;->t:LX/04D;

    .line 1968585
    iput-object p5, p0, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968586
    move-object/from16 v0, p10

    iput-object v0, p0, LX/D8S;->w:LX/D8g;

    .line 1968587
    invoke-static {p0, p5}, LX/D8S;->a(LX/D8S;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2pa;

    move-result-object v1

    iput-object v1, p0, LX/D8S;->s:LX/2pa;

    .line 1968588
    iput p2, p0, LX/D8S;->x:I

    .line 1968589
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1968590
    const/4 v2, 0x0

    iget v3, p0, LX/D8S;->x:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 1968591
    iget-object v2, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-virtual {v2, v1}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1968592
    iget-object v1, p0, LX/D8S;->v:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1968593
    iget-object v2, p0, LX/D8S;->i:LX/D8I;

    iget-object v1, p0, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v1}, LX/D8I;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1968594
    iget-object v1, p0, LX/D8S;->e:LX/99w;

    invoke-virtual {v1}, LX/99w;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1968595
    iget-object v1, p0, LX/D8S;->e:LX/99w;

    iget-object v2, p0, LX/D8S;->i:LX/D8I;

    invoke-virtual {v2}, LX/D8I;->a()[LX/0b2;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/99w;->a([LX/0b2;)V

    .line 1968596
    :goto_0
    iget-object v1, p0, LX/D8S;->d:LX/Bug;

    iget-object v3, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    iget-object v4, p0, LX/D8S;->s:LX/2pa;

    sget-object v5, LX/Bue;->WATCH_AND_BROWSE:LX/Bue;

    const/4 v6, 0x0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object/from16 v2, p9

    invoke-virtual/range {v1 .. v7}, LX/Bug;->a(LX/3FT;LX/3FT;LX/2pa;LX/Bue;LX/7Lf;Ljava/lang/Boolean;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    iput-object v1, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968597
    iget-object v1, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 1968598
    iget-object v1, p0, LX/D8S;->n:LX/394;

    if-eqz v1, :cond_0

    .line 1968599
    iget-object v1, p0, LX/D8S;->n:LX/394;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-interface {v1, v2}, LX/394;->a(LX/04g;)V

    .line 1968600
    :cond_0
    iget-object v1, p0, LX/D8S;->j:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    invoke-virtual {v1, p7}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1968601
    iget-object v1, p0, LX/D8S;->h:LX/D8K;

    iget-object v2, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-virtual {v1, v2}, LX/D8K;->a(Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;)V

    .line 1968602
    iget-object v1, p0, LX/D8S;->h:LX/D8K;

    iget-object v2, p0, LX/D8S;->j:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    invoke-virtual {v1, v2}, LX/D8K;->a(Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;)V

    .line 1968603
    iget-object v1, p0, LX/D8S;->h:LX/D8K;

    invoke-virtual {v1, p1}, LX/D8K;->a(LX/D8U;)V

    .line 1968604
    iget-object v1, p0, LX/D8S;->h:LX/D8K;

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/D8K;->a(LX/D7g;)V

    .line 1968605
    iget-object v1, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    new-instance v2, LX/D8Q;

    invoke-direct {v2, p0}, LX/D8Q;-><init>(LX/D8S;)V

    invoke-virtual {v1, v2}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->setupFullscreenButtonClickHandler(LX/0QK;)V

    .line 1968606
    iget-object v1, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, p7}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1968607
    iget-object v2, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    iget-object v3, p0, LX/D8S;->s:LX/2pa;

    iget-object v1, p0, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->a(LX/2pa;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1968608
    iget-object v1, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-virtual {v1, p1}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->setupDismissPlayerButton(LX/D8U;)V

    .line 1968609
    iget-object v1, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->setVisibility(I)V

    .line 1968610
    iget-object v1, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v2, 0x0

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1968611
    iget-object v1, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2, p3}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    .line 1968612
    sget-object v1, LX/D8g;->WATCH_AND_SHOP:LX/D8g;

    move-object/from16 v0, p10

    if-ne v0, v1, :cond_2

    new-instance v1, LX/D8R;

    invoke-direct {v1, p0}, LX/D8R;-><init>(LX/D8S;)V

    .line 1968613
    :goto_1
    invoke-static {p0, p4, p3, v1}, LX/D8S;->a(LX/D8S;IILjava/util/Map;)V

    .line 1968614
    iget v1, p0, LX/D8S;->x:I

    iget-object v2, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->getVideoSize()LX/3FO;

    move-result-object v2

    iget v2, v2, LX/3FO;->b:I

    invoke-static {p0, v1, v2}, LX/D8S;->a(LX/D8S;II)V

    .line 1968615
    return-void

    .line 1968616
    :cond_1
    iget-object v1, p0, LX/D8S;->e:LX/99w;

    invoke-virtual {v1}, LX/99w;->b()V

    goto/16 :goto_0

    .line 1968617
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(F)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1968619
    iget v0, p0, LX/D8S;->o:F

    iget v1, p0, LX/D8S;->p:F

    add-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/D8S;->q:F

    .line 1968620
    iget-object v0, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    .line 1968621
    iget-object v1, v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    move-object v0, v1

    .line 1968622
    if-eqz v0, :cond_1

    .line 1968623
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    .line 1968624
    iget-object p1, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    move-object v1, p1

    .line 1968625
    iget v1, v1, LX/3FO;->b:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    .line 1968626
    iget-object v0, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->s()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    .line 1968627
    iget-object v1, v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    const-class p1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-virtual {v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iput-object v1, v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    .line 1968628
    iget-object v1, v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    if-nez v1, :cond_4

    .line 1968629
    const/4 v1, 0x0

    .line 1968630
    :goto_0
    move v0, v1

    .line 1968631
    if-nez v0, :cond_0

    .line 1968632
    iget-object v0, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER_SWIPE:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1968633
    iget-object v0, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->b()V

    .line 1968634
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D8S;->l:Z

    .line 1968635
    :cond_1
    :goto_1
    return v2

    .line 1968636
    :cond_2
    iget-object v0, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1968637
    iget-object v0, p0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1968638
    iget-object v0, p0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->b()V

    .line 1968639
    :cond_3
    iput-boolean v2, p0, LX/D8S;->l:Z

    goto :goto_1

    :cond_4
    iget-object v1, v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->k:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    .line 1968640
    iget-boolean p1, v1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->r:Z

    move v1, p1

    .line 1968641
    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1968618
    iget v0, p0, LX/D8S;->o:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, LX/D8S;->q:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
