.class public final LX/EKI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/Cxh;

.field public final synthetic c:LX/EKJ;


# direct methods
.method public constructor <init>(LX/EKJ;LX/CzL;LX/Cxh;)V
    .locals 0

    .prologue
    .line 2106432
    iput-object p1, p0, LX/EKI;->c:LX/EKJ;

    iput-object p2, p0, LX/EKI;->a:LX/CzL;

    iput-object p3, p0, LX/EKI;->b:LX/Cxh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x3b81720

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2106433
    iget-object v0, p0, LX/EKI;->a:LX/CzL;

    .line 2106434
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v1

    .line 2106435
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2106436
    iget-object v0, p0, LX/EKI;->a:LX/CzL;

    .line 2106437
    iget-object v1, v0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v1

    .line 2106438
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->r()LX/8dH;

    move-result-object v3

    .line 2106439
    if-eqz v3, :cond_0

    .line 2106440
    iget-object v0, p0, LX/EKI;->c:LX/EKJ;

    iget-object v1, p0, LX/EKI;->a:LX/CzL;

    iget-object v4, p0, LX/EKI;->b:LX/Cxh;

    check-cast v4, LX/Cxi;

    move-object v5, p1

    .line 2106441
    iget-object v7, v0, LX/EKJ;->d:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/EPK;

    const/4 v10, 0x0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v11

    .line 2106442
    iget-object v8, v1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v8, v8

    .line 2106443
    invoke-static {v8}, LX/8eM;->d(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    move-object v8, v1

    move-object v9, v4

    move-object v12, v3

    invoke-virtual/range {v7 .. v13}, LX/EPK;->a(LX/CzL;LX/Cxi;ZLjava/lang/String;LX/8dH;Lcom/facebook/graphql/enums/GraphQLObjectType;)Landroid/view/View$OnClickListener;

    move-result-object v7

    .line 2106444
    invoke-interface {v7, v5}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2106445
    :goto_0
    const v0, 0x1cfc74e7

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2106446
    :cond_0
    iget-object v0, p0, LX/EKI;->b:LX/Cxh;

    invoke-interface {v0, v2}, LX/Cxh;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2106447
    iget-object v1, p0, LX/EKI;->c:LX/EKJ;

    iget-object v0, p0, LX/EKI;->b:LX/Cxh;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/EKJ;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    goto :goto_0
.end method
