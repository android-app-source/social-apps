.class public final LX/DrL;
.super LX/CS9;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V
    .locals 0

    .prologue
    .line 2049154
    iput-object p1, p0, LX/DrL;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    invoke-direct {p0}, LX/CS9;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 2049155
    check-cast p1, LX/CS8;

    .line 2049156
    iget-object v0, p1, LX/CS8;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DrL;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->o:LX/Dr9;

    iget-object v1, p1, LX/CS8;->a:Ljava/lang/String;

    const/4 v3, 0x0

    .line 2049157
    iget-object v2, v0, LX/Dr9;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;

    .line 2049158
    iget-object v4, v2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v4, v4

    .line 2049159
    if-eqz v4, :cond_0

    .line 2049160
    iget-object v4, v2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v4, v4

    .line 2049161
    invoke-interface {v4}, LX/BCR;->c()LX/BCP;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2049162
    iget-object v4, v2, Lcom/facebook/notifications/settings/data/NotifOptionSetNode;->a:LX/BCR;

    move-object v2, v4

    .line 2049163
    invoke-interface {v2}, LX/BCR;->c()LX/BCP;

    move-result-object v2

    invoke-interface {v2}, LX/BCP;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    move v4, v3

    :goto_0
    if-ge v4, p1, :cond_0

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BCO;

    .line 2049164
    invoke-interface {v2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2049165
    const/4 v2, 0x1

    .line 2049166
    :goto_1
    move v0, v2

    .line 2049167
    if-eqz v0, :cond_1

    .line 2049168
    iget-object v0, p0, LX/DrL;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    invoke-static {v0}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->d(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    .line 2049169
    :cond_1
    return-void

    .line 2049170
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    :cond_3
    move v2, v3

    .line 2049171
    goto :goto_1
.end method
