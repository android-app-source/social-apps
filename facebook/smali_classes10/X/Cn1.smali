.class public LX/Cn1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1933683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933684
    return-void
.end method

.method public static a(LX/Cmv;LX/Cju;)I
    .locals 3

    .prologue
    .line 1933697
    if-nez p0, :cond_0

    .line 1933698
    const/4 v0, 0x0

    .line 1933699
    :goto_0
    return v0

    .line 1933700
    :cond_0
    const/4 v0, 0x0

    .line 1933701
    sget-object v1, LX/Cn0;->b:[I

    .line 1933702
    iget-object v2, p0, LX/Cmv;->d:LX/Cmu;

    move-object v2, v2

    .line 1933703
    invoke-virtual {v2}, LX/Cmu;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1933704
    :goto_1
    iget v1, p0, LX/Cmv;->e:F

    move v1, v1

    .line 1933705
    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 1933706
    :pswitch_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1933707
    goto :goto_1

    .line 1933708
    :pswitch_1
    iget v0, p0, LX/Cmv;->f:I

    move v0, v0

    .line 1933709
    invoke-interface {p1, v0}, LX/Cju;->b(I)F

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/Cmw;LX/Cju;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 1933685
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1933686
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 1933687
    iget-object v1, p0, LX/Cmw;->c:LX/Cmv;

    move-object v1, v1

    .line 1933688
    invoke-static {v1, p1}, LX/Cn1;->a(LX/Cmv;LX/Cju;)I

    move-result v1

    .line 1933689
    iget-object v2, p0, LX/Cmw;->d:LX/Cmv;

    move-object v2, v2

    .line 1933690
    invoke-static {v2, p1}, LX/Cn1;->a(LX/Cmv;LX/Cju;)I

    move-result v2

    .line 1933691
    iget-object v3, p0, LX/Cmw;->e:LX/Cmv;

    move-object v3, v3

    .line 1933692
    invoke-static {v3, p1}, LX/Cn1;->a(LX/Cmv;LX/Cju;)I

    move-result v3

    .line 1933693
    iget-object v4, p0, LX/Cmw;->f:LX/Cmv;

    move-object v4, v4

    .line 1933694
    invoke-static {v4, p1}, LX/Cn1;->a(LX/Cmv;LX/Cju;)I

    move-result v4

    .line 1933695
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1933696
    :cond_0
    return-object v0
.end method
