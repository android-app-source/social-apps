.class public LX/EUH;
.super LX/1LV;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile h:LX/EUH;


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/0pf;

.field public final d:LX/ETB;

.field public final e:LX/3By;

.field private final f:LX/3AW;

.field public g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/videohome/fragment/VideoHomeFeedLoggingViewportEventListener$VpvEventListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2125947
    const-class v0, LX/EUH;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EUH;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0SG;LX/0pf;LX/ETB;LX/3By;LX/3AW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/1B3;",
            ">;>;",
            "LX/0SG;",
            "LX/0pf;",
            "LX/ETB;",
            "LX/3By;",
            "LX/3AW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125948
    invoke-direct {p0, p1}, LX/1LV;-><init>(LX/0Ot;)V

    .line 2125949
    iput-object p2, p0, LX/EUH;->b:LX/0SG;

    .line 2125950
    iput-object p3, p0, LX/EUH;->c:LX/0pf;

    .line 2125951
    iput-object p4, p0, LX/EUH;->d:LX/ETB;

    .line 2125952
    iput-object p5, p0, LX/EUH;->e:LX/3By;

    .line 2125953
    iput-object p6, p0, LX/EUH;->f:LX/3AW;

    .line 2125954
    return-void
.end method

.method public static b(LX/0QB;)LX/EUH;
    .locals 10

    .prologue
    .line 2125955
    sget-object v0, LX/EUH;->h:LX/EUH;

    if-nez v0, :cond_1

    .line 2125956
    const-class v1, LX/EUH;

    monitor-enter v1

    .line 2125957
    :try_start_0
    sget-object v0, LX/EUH;->h:LX/EUH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2125958
    if-eqz v2, :cond_0

    .line 2125959
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2125960
    new-instance v3, LX/EUH;

    invoke-static {v0}, LX/1LW;->a(LX/0QB;)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v6

    check-cast v6, LX/0pf;

    invoke-static {v0}, LX/ETB;->a(LX/0QB;)LX/ETB;

    move-result-object v7

    check-cast v7, LX/ETB;

    invoke-static {v0}, LX/3By;->a(LX/0QB;)LX/3By;

    move-result-object v8

    check-cast v8, LX/3By;

    invoke-static {v0}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v9

    check-cast v9, LX/3AW;

    invoke-direct/range {v3 .. v9}, LX/EUH;-><init>(LX/0Ot;LX/0SG;LX/0pf;LX/ETB;LX/3By;LX/3AW;)V

    .line 2125961
    move-object v0, v3

    .line 2125962
    sput-object v0, LX/EUH;->h:LX/EUH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2125963
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2125964
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2125965
    :cond_1
    sget-object v0, LX/EUH;->h:LX/EUH;

    return-object v0

    .line 2125966
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2125967
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2125968
    invoke-super {p0, p1}, LX/1LV;->a(Ljava/lang/Object;)V

    .line 2125969
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 2125970
    if-nez v2, :cond_1

    .line 2125971
    :cond_0
    :goto_0
    return-void

    .line 2125972
    :cond_1
    instance-of v0, v2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_2

    .line 2125973
    iget-object v0, p0, LX/EUH;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 2125974
    iget-object v0, p0, LX/EUH;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zB;

    .line 2125975
    if-eqz v0, :cond_2

    move-object v1, v2

    .line 2125976
    check-cast v1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125977
    invoke-static {v1}, LX/2zB;->c(Lcom/facebook/video/videohome/data/VideoHomeItem;)Ljava/lang/String;

    move-result-object v3

    .line 2125978
    if-nez v3, :cond_4

    .line 2125979
    :cond_2
    :goto_1
    instance-of v0, v2, Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    if-eqz v0, :cond_0

    .line 2125980
    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    .line 2125981
    iget-object v1, p0, LX/EUH;->e:LX/3By;

    sget-object v2, LX/0JS;->EVENT_TARGET:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    const-string v3, "spinner"

    invoke-virtual {v1, v0, v2, v3}, LX/3By;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2125982
    iget-object v1, p0, LX/EUH;->d:LX/ETB;

    invoke-virtual {v1}, LX/ETB;->i()Z

    move-result v2

    .line 2125983
    iget-object v3, p0, LX/EUH;->e:LX/3By;

    sget-object v1, LX/0JS;->EVENT_TARGET_INFO:LX/0JS;

    iget-object v4, v1, LX/0JS;->value:Ljava/lang/String;

    if-eqz v2, :cond_5

    const-string v1, "unit_component_spinner"

    :goto_2
    invoke-virtual {v3, v0, v4, v1}, LX/3By;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2125984
    iget-object v1, p0, LX/EUH;->d:LX/ETB;

    .line 2125985
    iget-object v3, v1, LX/ETB;->t:LX/ETQ;

    move-object v1, v3

    .line 2125986
    invoke-virtual {v1}, LX/ETQ;->size()I

    move-result v3

    if-nez v3, :cond_8

    .line 2125987
    const/4 v1, 0x0

    .line 2125988
    :goto_3
    move-object v1, v1

    .line 2125989
    if-nez v1, :cond_6

    .line 2125990
    :cond_3
    :goto_4
    goto :goto_0

    .line 2125991
    :cond_4
    iget-object p1, v0, LX/2zB;->b:Ljava/util/Set;

    invoke-interface {p1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2125992
    :cond_5
    const-string v1, "unit_spinner"

    goto :goto_2

    .line 2125993
    :cond_6
    iget-object v3, p0, LX/EUH;->e:LX/3By;

    sget-object v4, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    iget-object v4, v4, LX/0JS;->value:Ljava/lang/String;

    .line 2125994
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2125995
    invoke-interface {v5}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v4, v5}, LX/3By;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2125996
    iget-object v3, p0, LX/EUH;->e:LX/3By;

    sget-object v4, LX/0JS;->REACTION_UNIT_TYPE:LX/0JS;

    iget-object v4, v4, LX/0JS;->value:Ljava/lang/String;

    .line 2125997
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2125998
    invoke-virtual {v3, v0, v4, v5}, LX/3By;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2125999
    iget-object v3, p0, LX/EUH;->d:LX/ETB;

    .line 2126000
    iget-object v4, v3, LX/ETB;->t:LX/ETQ;

    move-object v3, v4

    .line 2126001
    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/ETR;->a(LX/ETQ;Ljava/lang/String;)I

    move-result v4

    .line 2126002
    if-ltz v4, :cond_7

    .line 2126003
    iget-object v5, p0, LX/EUH;->e:LX/3By;

    sget-object p1, LX/0JS;->UNIT_POSITION:LX/0JS;

    iget-object p1, p1, LX/0JS;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v0, p1, v4}, LX/3By;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2126004
    :cond_7
    if-eqz v2, :cond_3

    .line 2126005
    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v3, v2, v1}, LX/ETR;->a(LX/ETQ;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v1

    .line 2126006
    if-ltz v1, :cond_3

    .line 2126007
    iget-object v2, p0, LX/EUH;->e:LX/3By;

    sget-object v3, LX/0JS;->POSITION_IN_UNIT:LX/0JS;

    iget-object v3, v3, LX/0JS;->value:Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, LX/3By;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v1}, LX/ETQ;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v1

    goto :goto_3
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2126008
    invoke-super {p0, p1}, LX/1LV;->b(Ljava/lang/Object;)V

    .line 2126009
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 2126010
    if-nez v0, :cond_1

    .line 2126011
    :cond_0
    :goto_0
    return-void

    .line 2126012
    :cond_1
    instance-of v1, v0, Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    if-eqz v1, :cond_0

    .line 2126013
    iget-object v1, p0, LX/EUH;->c:LX/0pf;

    invoke-virtual {v1, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v1

    .line 2126014
    iget-object v2, p0, LX/EUH;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2126015
    iget-wide v6, v1, LX/1g0;->a:J

    move-wide v4, v6

    .line 2126016
    sub-long/2addr v2, v4

    .line 2126017
    iget-object v1, p0, LX/EUH;->f:LX/3AW;

    .line 2126018
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "vpv_duration"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v5, LX/0JS;->VPV_DURATION:LX/0JS;

    iget-object v5, v5, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v4, v5, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2126019
    iget-object v5, v1, LX/3AW;->i:LX/3Bx;

    invoke-virtual {v5, v4, v0}, LX/3Bx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2126020
    invoke-virtual {v4}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    .line 2126021
    invoke-static {v1, v4}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2126022
    goto :goto_0
.end method
