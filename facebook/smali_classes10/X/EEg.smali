.class public final LX/EEg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EEp;


# direct methods
.method public constructor <init>(LX/EEp;)V
    .locals 0

    .prologue
    .line 2094135
    iput-object p1, p0, LX/EEg;->a:LX/EEp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    const v0, -0x2b6a0984

    invoke-static {v4, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2094136
    check-cast p1, Lcom/facebook/widget/text/BetterButton;

    .line 2094137
    invoke-virtual {p1, v5}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2094138
    invoke-virtual {p1}, Lcom/facebook/widget/text/BetterButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2094139
    iget-object v1, p0, LX/EEg;->a:LX/EEp;

    iget-object v1, v1, LX/EEp;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v5

    .line 2094140
    iget-object v0, v1, LX/EDx;->ad:LX/EFw;

    if-nez v0, :cond_0

    .line 2094141
    const-string v0, "WebrtcUiHandler"

    const-string v5, "Trying to invite participants to a null conference call object"

    invoke-static {v0, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2094142
    :goto_0
    const v0, -0x5de7479c

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2094143
    :cond_0
    if-eqz v3, :cond_1

    array-length v0, v3

    if-nez v0, :cond_2

    .line 2094144
    :cond_1
    const-string v0, "WebrtcUiHandler"

    const-string v5, "Invalid invitees list"

    invoke-static {v0, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2094145
    :cond_2
    iget-object v0, v1, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v0, v3}, LX/EFw;->b([Ljava/lang/String;)V

    goto :goto_0
.end method
