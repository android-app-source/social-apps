.class public LX/Chj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Chj;


# instance fields
.field public final a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "LX/ChL;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z


# direct methods
.method public constructor <init>(LX/8bL;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1928496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1928497
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    .line 1928498
    invoke-virtual {p1}, LX/8bL;->h()Z

    move-result v0

    iput-boolean v0, p0, LX/Chj;->b:Z

    .line 1928499
    return-void
.end method

.method public static a(LX/0QB;)LX/Chj;
    .locals 4

    .prologue
    .line 1928515
    sget-object v0, LX/Chj;->c:LX/Chj;

    if-nez v0, :cond_1

    .line 1928516
    const-class v1, LX/Chj;

    monitor-enter v1

    .line 1928517
    :try_start_0
    sget-object v0, LX/Chj;->c:LX/Chj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1928518
    if-eqz v2, :cond_0

    .line 1928519
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1928520
    new-instance p0, LX/Chj;

    invoke-static {v0}, LX/8bL;->b(LX/0QB;)LX/8bL;

    move-result-object v3

    check-cast v3, LX/8bL;

    invoke-direct {p0, v3}, LX/Chj;-><init>(LX/8bL;)V

    .line 1928521
    move-object v0, p0

    .line 1928522
    sput-object v0, LX/Chj;->c:LX/Chj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1928523
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1928524
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1928525
    :cond_1
    sget-object v0, LX/Chj;->c:LX/Chj;

    return-object v0

    .line 1928526
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1928527
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/ChL;
    .locals 1

    .prologue
    .line 1928512
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1928513
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ChL;

    .line 1928514
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/ChL;)V
    .locals 2

    .prologue
    .line 1928528
    if-nez p1, :cond_1

    .line 1928529
    :cond_0
    :goto_0
    return-void

    .line 1928530
    :cond_1
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1928531
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1928532
    :cond_2
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 1928533
    iget-boolean v0, p0, LX/Chj;->b:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ChL;

    invoke-interface {v0}, LX/ChL;->k()LX/ChZ;

    move-result-object v0

    sget-object v1, LX/ChZ;->NA:LX/ChZ;

    if-eq v0, v1, :cond_4

    .line 1928534
    :cond_3
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ChL;

    invoke-interface {v0}, LX/ChL;->f()V

    .line 1928535
    :cond_4
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b()LX/ChL;
    .locals 3

    .prologue
    .line 1928507
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/Chj;->c()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1928508
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ChL;

    .line 1928509
    iget-object v1, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ChL;

    .line 1928510
    iget-object v2, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928511
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public b(LX/ChL;)V
    .locals 2

    .prologue
    .line 1928501
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1928502
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1928503
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/Chj;->b:Z

    if-nez v0, :cond_0

    invoke-interface {p1}, LX/ChL;->k()LX/ChZ;

    move-result-object v0

    sget-object v1, LX/ChZ;->NA:LX/ChZ;

    if-eq v0, v1, :cond_1

    .line 1928504
    :cond_0
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ChL;

    invoke-interface {p1}, LX/ChL;->k()LX/ChZ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/ChL;->a(LX/ChZ;)V

    .line 1928505
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ChL;

    invoke-interface {v0}, LX/ChL;->e()V

    .line 1928506
    :cond_1
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1928500
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    return v0
.end method
