.class public final LX/EVB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final d:LX/EUB;

.field private final e:I

.field private final f:I

.field public final g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TE;>;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/video/videohome/data/VideoHomeItem;

.field public i:Lcom/facebook/video/videohome/data/VideoHomeItem;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EUB;IILX/ETa;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/EUB;",
            "IITE;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2127550
    iput-object p1, p0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2127551
    iput-object v0, p0, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127552
    iput-object v0, p0, LX/EVB;->i:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127553
    iput-object p2, p0, LX/EVB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2127554
    iput-object p3, p0, LX/EVB;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127555
    iput-object p4, p0, LX/EVB;->d:LX/EUB;

    .line 2127556
    iput p5, p0, LX/EVB;->e:I

    .line 2127557
    iput p6, p0, LX/EVB;->f:I

    .line 2127558
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p7}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/EVB;->g:Ljava/lang/ref/WeakReference;

    .line 2127559
    iget-object v0, p0, LX/EVB;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    instance-of v0, v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_0

    .line 2127560
    iget-object v0, p0, LX/EVB;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    iput-object v0, p0, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127561
    iget-object v0, p0, LX/EVB;->h:Lcom/facebook/video/videohome/data/VideoHomeItem;

    iput-object v0, p0, LX/EVB;->i:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127562
    :cond_0
    return-void
.end method

.method private a(LX/ETZ;)LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ETZ;",
            ")",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2127563
    new-instance v0, LX/EV9;

    invoke-direct {v0, p0, p1}, LX/EV9;-><init>(LX/EVB;LX/ETZ;)V

    return-object v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/ETa;)LX/395;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/395;"
        }
    .end annotation

    .prologue
    .line 2127564
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2127565
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2127566
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 2127567
    invoke-virtual {p1, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 2127568
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 2127569
    check-cast p2, LX/1Po;

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v6

    .line 2127570
    invoke-static {v5}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 2127571
    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    .line 2127572
    new-instance v2, LX/0AW;

    invoke-direct {v2, v0}, LX/0AW;-><init>(LX/162;)V

    .line 2127573
    iput-boolean v1, v2, LX/0AW;->d:Z

    .line 2127574
    move-object v0, v2

    .line 2127575
    invoke-virtual {v0}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v2

    .line 2127576
    new-instance v0, LX/395;

    new-instance v1, LX/0AV;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 2127577
    iget-object v7, p0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v7, v7, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->a:LX/1qa;

    sget-object p1, LX/26P;->Video:LX/26P;

    invoke-virtual {v7, v3, p1}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v7

    move-object v3, v7

    .line 2127578
    invoke-direct/range {v0 .. v5}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2127579
    invoke-virtual {v0, v6}, LX/395;->a(LX/04D;)LX/395;

    .line 2127580
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    const v0, -0x6dcc55

    invoke-static {v1, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v9

    .line 2127581
    iget-object v0, p0, LX/EVB;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/ETa;

    .line 2127582
    if-nez v7, :cond_0

    .line 2127583
    const v0, -0x59406f0b

    invoke-static {v1, v1, v0, v9}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2127584
    :goto_0
    return-void

    .line 2127585
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0f8;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 2127586
    if-nez v0, :cond_1

    .line 2127587
    const v0, -0x51f313f

    invoke-static {v0, v9}, LX/02F;->a(II)V

    goto :goto_0

    .line 2127588
    :cond_1
    iget-object v1, p0, LX/EVB;->d:LX/EUB;

    .line 2127589
    iget-object v2, v1, LX/EUB;->d:LX/3FR;

    move-object v1, v2

    .line 2127590
    invoke-interface {v0}, LX/0f8;->i()LX/0hE;

    move-result-object v2

    .line 2127591
    iget-object v0, p0, LX/EVB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, v0, v7}, LX/EVB;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/ETa;)LX/395;

    move-result-object v3

    .line 2127592
    iget-object v0, p0, LX/EVB;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    instance-of v0, v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_2

    .line 2127593
    sget-object v0, LX/ETZ;->NEXT:LX/ETZ;

    invoke-direct {p0, v0}, LX/EVB;->a(LX/ETZ;)LX/0QK;

    move-result-object v0

    invoke-interface {v2, v0}, LX/0hE;->setNextStoryFinder(LX/0QK;)V

    .line 2127594
    sget-object v0, LX/ETZ;->PREVIOUS:LX/ETZ;

    invoke-direct {p0, v0}, LX/EVB;->a(LX/ETZ;)LX/0QK;

    move-result-object v0

    invoke-interface {v2, v0}, LX/0hE;->setPreviousStoryFinder(LX/0QK;)V

    .line 2127595
    :cond_2
    invoke-interface {v2, v4}, LX/0hE;->setAllowLooping(Z)V

    .line 2127596
    invoke-interface {v2, v4}, LX/0hE;->setLogExitingPauseEvent(Z)V

    .line 2127597
    new-instance v0, LX/EVA;

    invoke-direct {v0, p0, v2, v3, v7}, LX/EVA;-><init>(LX/EVB;LX/0hE;LX/395;LX/ETa;)V

    invoke-interface {v2, v0}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 2127598
    iget-object v0, p0, LX/EVB;->d:LX/EUB;

    invoke-virtual {v0}, LX/EUB;->b()LX/2oO;

    move-result-object v0

    .line 2127599
    if-eqz v0, :cond_3

    .line 2127600
    invoke-virtual {v0}, LX/2oO;->a()V

    .line 2127601
    invoke-virtual {v0}, LX/2oO;->i()V

    .line 2127602
    :cond_3
    iget-object v0, p0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->c:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->c:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->k()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2127603
    iget-object v0, p0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->c:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->d()V

    .line 2127604
    :cond_4
    if-nez v1, :cond_8

    iget-object v0, p0, LX/EVB;->d:LX/EUB;

    invoke-virtual {v0}, LX/EUB;->a()I

    move-result v0

    .line 2127605
    :goto_1
    invoke-virtual {v3, v0}, LX/395;->b(I)LX/395;

    .line 2127606
    invoke-virtual {v3, v0}, LX/395;->a(I)LX/395;

    .line 2127607
    instance-of v0, v1, LX/3FT;

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 2127608
    check-cast v0, LX/3FT;

    .line 2127609
    iput-object v0, v3, LX/395;->h:LX/3FT;

    .line 2127610
    :cond_5
    invoke-interface {v2, v3}, LX/0hE;->a(LX/395;)V

    .line 2127611
    iget-object v0, p0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v10, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->f:LX/3AW;

    sget-object v11, LX/04D;->VIDEO_HOME:LX/04D;

    .line 2127612
    iget-object v0, v3, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v0, v0

    .line 2127613
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2127614
    if-eqz v0, :cond_9

    sget-object v0, LX/0JD;->LIVE_VIDEO:LX/0JD;

    move-object v8, v0

    :goto_3
    new-instance v0, LX/7Qm;

    iget-object v1, p0, LX/EVB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2127615
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 2127616
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/EVB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    iget-object v3, p0, LX/EVB;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127617
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2127618
    iget v4, p0, LX/EVB;->e:I

    iget v5, p0, LX/EVB;->f:I

    iget-object v6, p0, LX/EVB;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127619
    iget-object p1, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, p1

    .line 2127620
    invoke-interface {v6}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/7Qm;-><init>(Ljava/lang/String;LX/0lF;Ljava/lang/String;IILjava/lang/String;)V

    .line 2127621
    iget-object v1, v10, LX/3AW;->c:LX/1CC;

    invoke-virtual {v1}, LX/1CC;->i()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, v10, LX/3AW;->h:LX/0xX;

    .line 2127622
    iget-object v2, v1, LX/0xX;->d:LX/0Uh;

    const/16 v3, 0x4b3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 2127623
    if-eqz v1, :cond_6

    .line 2127624
    iget-object v1, v10, LX/3AW;->j:LX/03V;

    sget-object v2, LX/3AW;->a:Ljava/lang/String;

    const-string v3, "logVideoHomeClickStory() without started session"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2127625
    :cond_6
    invoke-static {v11, v8}, LX/3AW;->a(LX/04D;LX/0JD;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2127626
    invoke-interface {v0, v1}, LX/7Qf;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2127627
    invoke-static {v10, v1}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2127628
    iget-object v0, p0, LX/EVB;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    instance-of v0, v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_7

    .line 2127629
    check-cast v7, LX/ETh;

    iget-object v0, p0, LX/EVB;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v7, v0}, LX/ETh;->d(Ljava/lang/Object;)Z

    .line 2127630
    :cond_7
    const v0, -0x28792047

    invoke-static {v0, v9}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2127631
    :cond_8
    invoke-interface {v1}, LX/3FR;->getSeekPosition()I

    move-result v0

    goto/16 :goto_1

    .line 2127632
    :cond_9
    sget-object v0, LX/0JD;->FULLSCREEN_VIDEO:LX/0JD;

    move-object v8, v0

    goto :goto_3

    :cond_a
    const/4 v0, 0x0

    goto :goto_2
.end method
