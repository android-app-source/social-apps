.class public final LX/EbM;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EbK;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EbM;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EbM;


# instance fields
.field public bitField0_:I

.field public chainKey_:LX/EWc;

.field public id_:I

.field public iteration_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public signingKey_:LX/EWc;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2143643
    new-instance v0, LX/EbJ;

    invoke-direct {v0}, LX/EbJ;-><init>()V

    sput-object v0, LX/EbM;->a:LX/EWZ;

    .line 2143644
    new-instance v0, LX/EbM;

    invoke-direct {v0}, LX/EbM;-><init>()V

    .line 2143645
    sput-object v0, LX/EbM;->c:LX/EbM;

    invoke-direct {v0}, LX/EbM;->y()V

    .line 2143646
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2143638
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2143639
    iput-byte v0, p0, LX/EbM;->memoizedIsInitialized:B

    .line 2143640
    iput v0, p0, LX/EbM;->memoizedSerializedSize:I

    .line 2143641
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2143642
    iput-object v0, p0, LX/EbM;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2143604
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2143605
    iput-byte v0, p0, LX/EbM;->memoizedIsInitialized:B

    .line 2143606
    iput v0, p0, LX/EbM;->memoizedSerializedSize:I

    .line 2143607
    invoke-direct {p0}, LX/EbM;->y()V

    .line 2143608
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2143609
    const/4 v0, 0x0

    .line 2143610
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2143611
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2143612
    sparse-switch v3, :sswitch_data_0

    .line 2143613
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2143614
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2143615
    goto :goto_0

    .line 2143616
    :sswitch_1
    iget v3, p0, LX/EbM;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EbM;->bitField0_:I

    .line 2143617
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EbM;->id_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2143618
    :catch_0
    move-exception v0

    .line 2143619
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2143620
    move-object v0, v0

    .line 2143621
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2143622
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EbM;->unknownFields:LX/EZQ;

    .line 2143623
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2143624
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EbM;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EbM;->bitField0_:I

    .line 2143625
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EbM;->iteration_:I
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2143626
    :catch_1
    move-exception v0

    .line 2143627
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2143628
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2143629
    move-object v0, v1

    .line 2143630
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2143631
    :sswitch_3
    :try_start_4
    iget v3, p0, LX/EbM;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/EbM;->bitField0_:I

    .line 2143632
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EbM;->chainKey_:LX/EWc;

    goto :goto_0

    .line 2143633
    :sswitch_4
    iget v3, p0, LX/EbM;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, LX/EbM;->bitField0_:I

    .line 2143634
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EbM;->signingKey_:LX/EWc;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2143635
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EbM;->unknownFields:LX/EZQ;

    .line 2143636
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2143637
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2143557
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2143558
    iput-byte v1, p0, LX/EbM;->memoizedIsInitialized:B

    .line 2143559
    iput v1, p0, LX/EbM;->memoizedSerializedSize:I

    .line 2143560
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EbM;->unknownFields:LX/EZQ;

    .line 2143561
    return-void
.end method

.method private static a(LX/EbM;)LX/EbL;
    .locals 1

    .prologue
    .line 2143603
    invoke-static {}, LX/EbL;->u()LX/EbL;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EbL;->a(LX/EbM;)LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method private y()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2143598
    iput v0, p0, LX/EbM;->id_:I

    .line 2143599
    iput v0, p0, LX/EbM;->iteration_:I

    .line 2143600
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbM;->chainKey_:LX/EWc;

    .line 2143601
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbM;->signingKey_:LX/EWc;

    .line 2143602
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2143596
    new-instance v0, LX/EbL;

    invoke-direct {v0, p1}, LX/EbL;-><init>(LX/EYd;)V

    .line 2143597
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2143585
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2143586
    iget v0, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2143587
    iget v0, p0, LX/EbM;->id_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2143588
    :cond_0
    iget v0, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2143589
    iget v0, p0, LX/EbM;->iteration_:I

    invoke-virtual {p1, v2, v0}, LX/EWf;->c(II)V

    .line 2143590
    :cond_1
    iget v0, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2143591
    const/4 v0, 0x3

    iget-object v1, p0, LX/EbM;->chainKey_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2143592
    :cond_2
    iget v0, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2143593
    iget-object v0, p0, LX/EbM;->signingKey_:LX/EWc;

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2143594
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2143595
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2143580
    iget-byte v1, p0, LX/EbM;->memoizedIsInitialized:B

    .line 2143581
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2143582
    :goto_0
    return v0

    .line 2143583
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2143584
    :cond_1
    iput-byte v0, p0, LX/EbM;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2143566
    iget v0, p0, LX/EbM;->memoizedSerializedSize:I

    .line 2143567
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2143568
    :goto_0
    return v0

    .line 2143569
    :cond_0
    const/4 v0, 0x0

    .line 2143570
    iget v1, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2143571
    iget v0, p0, LX/EbM;->id_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2143572
    :cond_1
    iget v1, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2143573
    iget v1, p0, LX/EbM;->iteration_:I

    invoke-static {v3, v1}, LX/EWf;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143574
    :cond_2
    iget v1, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2143575
    const/4 v1, 0x3

    iget-object v2, p0, LX/EbM;->chainKey_:LX/EWc;

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143576
    :cond_3
    iget v1, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2143577
    iget-object v1, p0, LX/EbM;->signingKey_:LX/EWc;

    invoke-static {v4, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143578
    :cond_4
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2143579
    iput v0, p0, LX/EbM;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2143565
    iget-object v0, p0, LX/EbM;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2143550
    sget-object v0, LX/EbV;->j:LX/EYn;

    const-class v1, LX/EbM;

    const-class v2, LX/EbL;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EbM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2143551
    sget-object v0, LX/EbM;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2143552
    iget v1, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2143553
    iget v0, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 2143554
    iget v0, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 2143555
    iget v0, p0, LX/EbM;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2143556
    invoke-static {p0}, LX/EbM;->a(LX/EbM;)LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2143562
    invoke-static {}, LX/EbL;->u()LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2143563
    invoke-static {p0}, LX/EbM;->a(LX/EbM;)LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2143564
    sget-object v0, LX/EbM;->c:LX/EbM;

    return-object v0
.end method
