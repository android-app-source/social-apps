.class public LX/EKv;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EKt;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EKw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2107467
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EKv;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EKw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107464
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2107465
    iput-object p1, p0, LX/EKv;->b:LX/0Ot;

    .line 2107466
    return-void
.end method

.method public static a(LX/0QB;)LX/EKv;
    .locals 4

    .prologue
    .line 2107468
    const-class v1, LX/EKv;

    monitor-enter v1

    .line 2107469
    :try_start_0
    sget-object v0, LX/EKv;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107470
    sput-object v2, LX/EKv;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107471
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107472
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107473
    new-instance v3, LX/EKv;

    const/16 p0, 0x33d9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EKv;-><init>(LX/0Ot;)V

    .line 2107474
    move-object v0, v3

    .line 2107475
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107476
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107477
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107478
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2107434
    check-cast p2, LX/EKu;

    .line 2107435
    iget-object v0, p0, LX/EKv;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EKw;

    iget-object v1, p2, LX/EKu;->a:LX/0Px;

    const/4 v3, 0x2

    const/4 v5, 0x0

    .line 2107436
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x4

    invoke-interface {v2, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    .line 2107437
    const/4 v4, 0x0

    .line 2107438
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v9

    move v8, v4

    move v3, v4

    :goto_0
    if-ge v8, v9, :cond_1

    invoke-virtual {v1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;

    .line 2107439
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v6, v3

    move v3, v4

    :goto_1
    if-ge v3, v11, :cond_0

    invoke-virtual {v10, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    .line 2107440
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->a()I

    move-result v2

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 2107441
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2107442
    :cond_0
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v3, v6

    goto :goto_0

    .line 2107443
    :cond_1
    move v8, v3

    .line 2107444
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v9

    move v6, v5

    :goto_2
    if-ge v6, v9, :cond_3

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;

    .line 2107445
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v4, v5

    :goto_3
    if-ge v4, v11, :cond_2

    invoke-virtual {v10, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    .line 2107446
    iget-object v12, v0, LX/EKw;->a:LX/EKQ;

    invoke-virtual {v12, p1}, LX/EKQ;->c(LX/1De;)LX/EKO;

    move-result-object v12

    .line 2107447
    iget-object p0, v12, LX/EKO;->a:LX/EKP;

    iput-object v3, p0, LX/EKP;->a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    .line 2107448
    iget-object p0, v12, LX/EKO;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2107449
    move-object v3, v12

    .line 2107450
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsPartyInfoModel;->b()Ljava/lang/String;

    move-result-object v12

    .line 2107451
    iget-object p0, v3, LX/EKO;->a:LX/EKP;

    iput-object v12, p0, LX/EKP;->b:Ljava/lang/String;

    .line 2107452
    iget-object p0, v3, LX/EKO;->d:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2107453
    move-object v3, v3

    .line 2107454
    iget-object v12, v3, LX/EKO;->a:LX/EKP;

    iput v8, v12, LX/EKP;->c:I

    .line 2107455
    iget-object v12, v3, LX/EKO;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v12, p0}, Ljava/util/BitSet;->set(I)V

    .line 2107456
    move-object v3, v3

    .line 2107457
    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2107458
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 2107459
    :cond_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_2

    .line 2107460
    :cond_3
    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2107461
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2107462
    invoke-static {}, LX/1dS;->b()V

    .line 2107463
    const/4 v0, 0x0

    return-object v0
.end method
