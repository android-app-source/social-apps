.class public LX/Eoj;
.super LX/Emg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Emg",
        "<",
        "LX/Ep9;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

.field public final b:LX/Emj;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EoZ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/EpN;

.field private final e:LX/Eph;

.field private final f:LX/Ept;

.field public final g:LX/Eny;

.field private final h:LX/1My;

.field private final i:LX/2Ip;

.field private final j:LX/84a;

.field private final k:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final l:LX/Eo9;

.field private final m:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;LX/Emj;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/2h7;LX/5P2;LX/BS5;LX/EpK;LX/EpO;LX/Epi;LX/Epu;LX/0Or;LX/2do;LX/0Ot;LX/0ad;)V
    .locals 8
    .param p1    # Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Eo3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/EnL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/Enk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/Eny;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/2h7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/5P2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;",
            "LX/Emj;",
            "LX/Eo3;",
            "Lcom/facebook/entitycards/controller/EntityCardsActivityController;",
            "Lcom/facebook/entitycards/model/EntityCardFetchErrorService;",
            "Lcom/facebook/entitycards/model/EntityCardMutationService;",
            "LX/2h7;",
            "LX/5P2;",
            "LX/BS5;",
            "LX/EpK;",
            "LX/EpO;",
            "LX/Epi;",
            "LX/Epu;",
            "LX/0Or",
            "<",
            "LX/EoZ;",
            ">;",
            "LX/2do;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2168519
    invoke-direct {p0}, LX/Emg;-><init>()V

    .line 2168520
    new-instance v1, LX/Eoe;

    invoke-direct {v1, p0}, LX/Eoe;-><init>(LX/Eoj;)V

    iput-object v1, p0, LX/Eoj;->i:LX/2Ip;

    .line 2168521
    new-instance v1, LX/Eof;

    invoke-direct {v1, p0}, LX/Eof;-><init>(LX/Eoj;)V

    iput-object v1, p0, LX/Eoj;->j:LX/84a;

    .line 2168522
    new-instance v1, LX/Eog;

    invoke-direct {v1, p0}, LX/Eog;-><init>(LX/Eoj;)V

    iput-object v1, p0, LX/Eoj;->k:LX/0TF;

    .line 2168523
    new-instance v1, LX/Eoh;

    invoke-direct {v1, p0}, LX/Eoh;-><init>(LX/Eoj;)V

    iput-object v1, p0, LX/Eoj;->l:LX/Eo9;

    .line 2168524
    new-instance v1, LX/Eoi;

    invoke-direct {v1, p0}, LX/Eoi;-><init>(LX/Eoj;)V

    iput-object v1, p0, LX/Eoj;->m:Landroid/view/View$OnClickListener;

    .line 2168525
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2168526
    iput-object p1, p0, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2168527
    iput-object p2, p0, LX/Eoj;->b:LX/Emj;

    .line 2168528
    iput-object p6, p0, LX/Eoj;->g:LX/Eny;

    move-object/from16 v1, p11

    move-object v2, p2

    move-object v3, p4

    move-object v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    .line 2168529
    invoke-virtual/range {v1 .. v7}, LX/EpO;->a(LX/Emj;LX/EnL;LX/2h7;LX/5P2;LX/BRt;LX/EpB;)LX/EpN;

    move-result-object v1

    iput-object v1, p0, LX/Eoj;->d:LX/EpN;

    .line 2168530
    move-object/from16 v0, p12

    invoke-virtual {v0, p5, p2}, LX/Epi;->a(LX/Enk;LX/Emj;)LX/Eph;

    move-result-object v1

    iput-object v1, p0, LX/Eoj;->e:LX/Eph;

    .line 2168531
    move-object/from16 v0, p13

    invoke-virtual {v0, p2}, LX/Epu;->a(LX/Emj;)LX/Ept;

    move-result-object v1

    iput-object v1, p0, LX/Eoj;->f:LX/Ept;

    .line 2168532
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Eoj;->c:LX/0Or;

    .line 2168533
    invoke-direct {p0, p3}, LX/Eoj;->a(LX/Eo3;)V

    .line 2168534
    sget-short v1, LX/2ez;->k:S

    const/4 v2, 0x0

    move-object/from16 v0, p17

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2168535
    invoke-interface/range {p16 .. p16}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1My;

    iput-object v1, p0, LX/Eoj;->h:LX/1My;

    .line 2168536
    invoke-direct {p0}, LX/Eoj;->c()V

    .line 2168537
    :goto_0
    return-void

    .line 2168538
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, LX/Eoj;->h:LX/1My;

    .line 2168539
    move-object/from16 v0, p15

    invoke-direct {p0, v0}, LX/Eoj;->a(LX/2do;)V

    goto :goto_0
.end method

.method private a(LX/2do;)V
    .locals 1

    .prologue
    .line 2168516
    iget-object v0, p0, LX/Eoj;->i:LX/2Ip;

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2168517
    iget-object v0, p0, LX/Eoj;->j:LX/84a;

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2168518
    return-void
.end method

.method private a(LX/Eo3;)V
    .locals 1

    .prologue
    .line 2168514
    iget-object v0, p0, LX/Eoj;->l:LX/Eo9;

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2168515
    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    .line 2168511
    iget-object v1, p0, LX/Eoj;->h:LX/1My;

    iget-object v2, p0, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v4, 0x0

    iget-object v0, p0, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-virtual {v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 2168512
    iget-object v1, p0, LX/Eoj;->h:LX/1My;

    iget-object v2, p0, LX/Eoj;->k:LX/0TF;

    iget-object v3, p0, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-virtual {v3}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2168513
    return-void
.end method


# virtual methods
.method public final a(LX/Ep9;)V
    .locals 2

    .prologue
    .line 2168410
    iget-object v0, p0, LX/Eoj;->f:LX/Ept;

    .line 2168411
    iget-object v1, p1, LX/Ep9;->c:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    move-object v1, v1

    .line 2168412
    invoke-virtual {v0, v1}, LX/Ept;->b(Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;)V

    .line 2168413
    invoke-super {p0, p1}, LX/Emg;->b(Ljava/lang/Object;)V

    .line 2168414
    return-void
.end method

.method public final b()V
    .locals 15

    .prologue
    .line 2168423
    invoke-virtual {p0}, LX/Eme;->a()LX/0am;

    move-result-object v0

    .line 2168424
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2168425
    :goto_0
    return-void

    .line 2168426
    :cond_0
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ep9;

    .line 2168427
    iget-object v1, p0, LX/Eoj;->f:LX/Ept;

    iget-object v2, p0, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2168428
    iget-object v3, v0, LX/Ep9;->c:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    move-object v3, v3

    .line 2168429
    invoke-interface {v2}, LX/Eon;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, LX/Ept;->c:Ljava/lang/String;

    .line 2168430
    iput-object v3, v1, LX/Ept;->d:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    .line 2168431
    invoke-virtual {v3, v2}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->setTag(Ljava/lang/Object;)V

    .line 2168432
    iput-object v1, v3, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->d:LX/Ept;

    .line 2168433
    invoke-interface {v2}, LX/Eon;->ce_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2168434
    invoke-virtual {v3}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 2168435
    if-eqz v4, :cond_3

    .line 2168436
    iget-object v5, v1, LX/Ept;->f:LX/EpA;

    invoke-virtual {v5}, LX/EpA;->a()I

    move-result v5

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2168437
    :goto_1
    invoke-virtual {v3, v4}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2168438
    iget-object v7, v1, LX/Ept;->f:LX/EpA;

    invoke-virtual {v7}, LX/EpA;->a()I

    move-result v8

    .line 2168439
    invoke-interface {v2}, LX/Eon;->y()LX/Eos;

    move-result-object v7

    .line 2168440
    if-eqz v7, :cond_5

    invoke-interface {v7}, LX/Eos;->b()LX/Eor;

    move-result-object v9

    if-eqz v9, :cond_5

    invoke-interface {v7}, LX/Eos;->b()LX/Eor;

    move-result-object v9

    invoke-interface {v9}, LX/Eor;->b()LX/1Fb;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 2168441
    invoke-interface {v7}, LX/Eos;->b()LX/Eor;

    move-result-object v9

    invoke-interface {v9}, LX/Eor;->b()LX/1Fb;

    move-result-object v9

    .line 2168442
    invoke-interface {v9}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 2168443
    invoke-interface {v7}, LX/Eos;->a()LX/1f8;

    move-result-object v10

    .line 2168444
    if-eqz v10, :cond_4

    new-instance v11, Landroid/graphics/PointF;

    invoke-interface {v10}, LX/1f8;->a()D

    move-result-wide v13

    double-to-float v12, v13

    invoke-interface {v10}, LX/1f8;->b()D

    move-result-wide v13

    double-to-float v10, v13

    invoke-direct {v11, v12, v10}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2168445
    :goto_2
    invoke-interface {v2}, LX/Eon;->c()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7}, LX/Eos;->b()LX/Eor;

    move-result-object v7

    invoke-interface {v7}, LX/Eor;->c()Ljava/lang/String;

    move-result-object v7

    const/4 v12, 0x0

    .line 2168446
    if-nez v7, :cond_6

    .line 2168447
    invoke-virtual {v3, v12, v12}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PointF;)V

    .line 2168448
    :goto_3
    invoke-interface {v2}, LX/Eon;->ce_()Ljava/lang/String;

    move-result-object v10

    iget-object v12, v1, LX/Ept;->a:LX/1Ai;

    move-object v7, v3

    invoke-virtual/range {v7 .. v12}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(ILandroid/net/Uri;Ljava/lang/String;Landroid/graphics/PointF;LX/1Ai;)V

    .line 2168449
    :goto_4
    invoke-interface {v2}, LX/Eon;->B()LX/1Fb;

    move-result-object v4

    invoke-interface {v2}, LX/Eon;->C()LX/1Fb;

    move-result-object v5

    iget-object v6, v1, LX/Ept;->b:LX/1Ai;

    invoke-virtual {v3, v4, v5, v6}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(LX/1Fb;LX/1Fb;LX/1Ai;)V

    .line 2168450
    invoke-interface {v2}, LX/Eon;->ce_()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2}, LX/Eon;->x()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2}, LX/Eon;->z()Z

    move-result v6

    invoke-interface {v2}, LX/Eon;->A()Z

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 2168451
    iget-object v1, p0, LX/Eoj;->d:LX/EpN;

    iget-object v2, p0, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2168452
    iget-object v3, v0, LX/Ep9;->d:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    move-object v3, v3

    .line 2168453
    invoke-virtual {v1, v2, v3}, LX/EpN;->a(LX/5wM;Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;)V

    .line 2168454
    iget-object v1, p0, LX/Eoj;->e:LX/Eph;

    iget-object v2, p0, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2168455
    iget-object v3, v0, LX/Ep9;->e:Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;

    move-object v3, v3

    .line 2168456
    iput-object v3, v1, LX/Eph;->h:Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;

    .line 2168457
    invoke-virtual {v3, v2}, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->setTag(Ljava/lang/Object;)V

    .line 2168458
    iput-object v1, v3, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->f:LX/Eph;

    .line 2168459
    iget-object v4, v1, LX/Eph;->a:LX/Enk;

    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2168460
    iget-object v6, v4, LX/Enk;->a:LX/Enl;

    invoke-virtual {v6, v5}, LX/Enl;->a(Ljava/lang/String;)Z

    move-result v6

    move v4, v6

    .line 2168461
    if-eqz v4, :cond_7

    .line 2168462
    sget-object v4, LX/Epk;->ERROR:LX/Epk;

    .line 2168463
    :goto_5
    sget-object v5, LX/Epk;->READY:LX/Epk;

    if-ne v4, v5, :cond_9

    .line 2168464
    iget-object v5, v1, LX/Eph;->d:LX/Emj;

    const-string v6, "ec_config_context_rows"

    sget-object v7, LX/EnC;->SUCCEEDED:LX/EnC;

    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v6, v7, v8}, LX/Emj;->a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V

    .line 2168465
    :cond_1
    :goto_6
    invoke-virtual {v3, v4}, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->setState(LX/Epk;)V

    .line 2168466
    const/4 v5, 0x0

    .line 2168467
    invoke-virtual {v3}, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->a()V

    .line 2168468
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2168469
    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v6

    if-nez v6, :cond_c

    .line 2168470
    const/4 v6, 0x0

    .line 2168471
    :goto_7
    move-object v7, v6

    .line 2168472
    if-nez v7, :cond_a

    .line 2168473
    :cond_2
    iget-object v1, v0, LX/Ep9;->f:Landroid/view/View;

    move-object v1, v1

    .line 2168474
    iget-object v2, p0, LX/Eoj;->m:Landroid/view/View$OnClickListener;

    invoke-static {v1, v2}, LX/Epp;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 2168475
    iput-object p0, v0, LX/Ep9;->a:LX/Eoj;

    .line 2168476
    goto/16 :goto_0

    .line 2168477
    :cond_3
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v5, -0x1

    iget-object v6, v1, LX/Ept;->f:LX/EpA;

    invoke-virtual {v6}, LX/EpA;->a()I

    move-result v6

    invoke-direct {v4, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    goto/16 :goto_1

    .line 2168478
    :cond_4
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 2168479
    :cond_5
    invoke-interface {v2}, LX/Eon;->ce_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7, v8}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(Ljava/lang/String;I)V

    .line 2168480
    invoke-interface {v2}, LX/Eon;->c()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ec_config_cover_photo"

    sget-object v9, LX/EnC;->SUCCEEDED:LX/EnC;

    invoke-static {v1, v7, v8, v9}, LX/Ept;->a$redex0(LX/Ept;Ljava/lang/String;Ljava/lang/String;LX/EnC;)V

    goto/16 :goto_4

    .line 2168481
    :cond_6
    iget-object v12, v1, LX/Ept;->h:LX/0Or;

    invoke-interface {v12}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/BA0;

    invoke-virtual {v3}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, LX/BQ3;->a(Landroid/content/Context;)F

    move-result v13

    invoke-virtual {v12, v7, v13}, LX/BA0;->a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v12

    .line 2168482
    new-instance v13, LX/Eps;

    invoke-direct {v13, v1, v3, v10, v11}, LX/Eps;-><init>(LX/Ept;Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;Ljava/lang/String;Landroid/graphics/PointF;)V

    iget-object v14, v1, LX/Ept;->i:Ljava/util/concurrent/Executor;

    invoke-static {v12, v13, v14}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_3

    .line 2168483
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v4

    if-nez v4, :cond_8

    .line 2168484
    sget-object v4, LX/Epk;->LOADING_MOVING_SPINNER:LX/Epk;

    goto :goto_5

    .line 2168485
    :cond_8
    sget-object v4, LX/Epk;->READY:LX/Epk;

    goto/16 :goto_5

    .line 2168486
    :cond_9
    sget-object v5, LX/Epk;->ERROR:LX/Epk;

    if-ne v4, v5, :cond_1

    .line 2168487
    iget-object v5, v1, LX/Eph;->d:LX/Emj;

    const-string v6, "ec_config_context_rows"

    sget-object v7, LX/EnC;->FAILED:LX/EnC;

    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v6, v7, v8}, LX/Emj;->a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 2168488
    :cond_a
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    iget v6, v1, LX/Eph;->g:I

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v8

    move v6, v5

    .line 2168489
    :goto_8
    if-ge v6, v8, :cond_2

    .line 2168490
    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;

    .line 2168491
    invoke-virtual {v3, v6}, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->a(I)Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    move-result-object v9

    .line 2168492
    invoke-static {v9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2168493
    invoke-virtual {v9, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setVisibility(I)V

    .line 2168494
    invoke-static {v1, v9, v4}, LX/Eph;->a(LX/Eph;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;)Z

    .line 2168495
    add-int/lit8 v4, v6, 0x1

    iget v10, v1, LX/Eph;->g:I

    if-ge v4, v10, :cond_b

    const/4 v4, 0x1

    .line 2168496
    :goto_9
    iput-boolean v4, v9, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->l:Z

    .line 2168497
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_8

    :cond_b
    move v4, v5

    .line 2168498
    goto :goto_9

    .line 2168499
    :cond_c
    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v7

    .line 2168500
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v8, v6

    :goto_a
    if-ge v8, v9, :cond_10

    invoke-virtual {v7, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;

    .line 2168501
    invoke-static {v6}, LX/Epl;->a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;)Z

    move-result v6

    if-nez v6, :cond_f

    .line 2168502
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2168503
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    const/4 v6, 0x0

    move v8, v6

    :goto_b
    if-ge v8, v4, :cond_e

    invoke-virtual {v7, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;

    .line 2168504
    invoke-static {v6}, LX/Epl;->a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2168505
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2168506
    :cond_d
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_b

    .line 2168507
    :cond_e
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v6, v6

    .line 2168508
    goto/16 :goto_7

    .line 2168509
    :cond_f
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_a

    :cond_10
    move-object v6, v7

    .line 2168510
    goto/16 :goto_7
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2168422
    check-cast p1, LX/Ep9;

    invoke-virtual {p0, p1}, LX/Eoj;->a(LX/Ep9;)V

    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2168415
    check-cast p1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2168416
    invoke-static {p1}, LX/Eov;->a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;)LX/Eov;

    move-result-object v0

    iget-object v1, p0, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-virtual {v1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    .line 2168417
    iput-object v1, v0, LX/Eov;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2168418
    move-object v0, v0

    .line 2168419
    invoke-virtual {v0}, LX/Eov;->a()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    move-result-object v0

    iput-object v0, p0, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2168420
    invoke-virtual {p0}, LX/Eoj;->b()V

    .line 2168421
    return-void
.end method
