.class public LX/DAF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/contacts/model/PhonebookContact;",
        ">;",
        "Ljava/util/ArrayList",
        "<",
        "Landroid/os/Bundle;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0lp;

.field private final b:Landroid/telephony/TelephonyManager;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0lp;Landroid/telephony/TelephonyManager;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0lp;",
            "Landroid/telephony/TelephonyManager;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970911
    iput-object p1, p0, LX/DAF;->c:LX/0Or;

    .line 1970912
    iput-object p2, p0, LX/DAF;->a:LX/0lp;

    .line 1970913
    iput-object p3, p0, LX/DAF;->b:Landroid/telephony/TelephonyManager;

    .line 1970914
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 11

    .prologue
    .line 1970915
    check-cast p1, LX/0Px;

    .line 1970916
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1970917
    iget-object v0, p0, LX/DAF;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1970918
    iget-object v2, p0, LX/DAF;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v2

    .line 1970919
    iget-object v3, p0, LX/DAF;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v3

    .line 1970920
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1970921
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "country_code"

    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1970922
    :cond_0
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1970923
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "sim_country"

    invoke-direct {v0, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1970924
    :cond_1
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1970925
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "network_country"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1970926
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "top_contacts"

    .line 1970927
    new-instance v5, Ljava/io/StringWriter;

    invoke-direct {v5}, Ljava/io/StringWriter;-><init>()V

    .line 1970928
    iget-object v3, p0, LX/DAF;->a:LX/0lp;

    invoke-virtual {v3, v5}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v6

    .line 1970929
    invoke-virtual {v6}, LX/0nX;->d()V

    .line 1970930
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_5

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/model/PhonebookContact;

    .line 1970931
    invoke-virtual {v6}, LX/0nX;->f()V

    .line 1970932
    const-string v8, "name"

    invoke-virtual {v6, v8}, LX/0nX;->g(Ljava/lang/String;)V

    .line 1970933
    const-string v8, "formatted"

    iget-object v9, v3, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    invoke-virtual {v6, v8, v9}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970934
    iget-object v8, v3, Lcom/facebook/contacts/model/PhonebookContact;->c:Ljava/lang/String;

    .line 1970935
    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1970936
    const-string v9, "first"

    invoke-virtual {v6, v9, v8}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970937
    :cond_3
    iget-object v8, v3, Lcom/facebook/contacts/model/PhonebookContact;->d:Ljava/lang/String;

    .line 1970938
    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1970939
    const-string v9, "last"

    invoke-virtual {v6, v9, v8}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970940
    :cond_4
    invoke-virtual {v6}, LX/0nX;->g()V

    .line 1970941
    iget-object v8, v3, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    .line 1970942
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1970943
    :goto_1
    invoke-virtual {v6}, LX/0nX;->g()V

    .line 1970944
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1970945
    :cond_5
    invoke-virtual {v6}, LX/0nX;->e()V

    .line 1970946
    invoke-virtual {v6}, LX/0nX;->flush()V

    .line 1970947
    invoke-virtual {v5}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 1970948
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1970949
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "MatchTopSMSContacts"

    .line 1970950
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 1970951
    move-object v0, v0

    .line 1970952
    const-string v2, "POST"

    .line 1970953
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 1970954
    move-object v0, v0

    .line 1970955
    const-string v2, "/me/top_contact_logs_contacts"

    .line 1970956
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 1970957
    move-object v0, v0

    .line 1970958
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1970959
    move-object v0, v0

    .line 1970960
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1970961
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1970962
    move-object v0, v0

    .line 1970963
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1970964
    :cond_6
    const-string v9, "phones"

    invoke-virtual {v6, v9}, LX/0nX;->f(Ljava/lang/String;)V

    .line 1970965
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    .line 1970966
    invoke-virtual {v6}, LX/0nX;->f()V

    .line 1970967
    const-string v10, "type"

    invoke-virtual {v8}, Lcom/facebook/contacts/model/PhonebookPhoneNumber;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v10, p0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970968
    const-string v10, "number"

    iget-object v8, v8, Lcom/facebook/contacts/model/PhonebookPhoneNumber;->a:Ljava/lang/String;

    invoke-virtual {v6, v10, v8}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970969
    invoke-virtual {v6}, LX/0nX;->g()V

    goto :goto_2

    .line 1970970
    :cond_7
    invoke-virtual {v6}, LX/0nX;->e()V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1970971
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1970972
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1970973
    const-string v2, "top_contacts"

    invoke-virtual {v0, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 1970974
    :goto_0
    return-object v0

    .line 1970975
    :cond_0
    const-string v2, "top_contacts"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1970976
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1970977
    const-string v4, "id"

    invoke-virtual {v0, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1970978
    const-string v4, "id"

    const-string v5, "id"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970979
    :cond_1
    const-string v4, "name"

    invoke-virtual {v0, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1970980
    const-string v4, "name"

    const-string v5, "name"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970981
    :cond_2
    const-string v4, "profile_pic"

    invoke-virtual {v0, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1970982
    const-string v4, "profile_pic"

    const-string v5, "profile_pic"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970983
    :cond_3
    const-string v4, "phone_number"

    invoke-virtual {v0, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1970984
    const-string v4, "phone_number"

    const-string v5, "phone_number"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970985
    :cond_4
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 1970986
    goto :goto_0
.end method
