.class public LX/Cy2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cxc;


# instance fields
.field private final a:LX/CzE;


# direct methods
.method public constructor <init>(LX/CzE;)V
    .locals 0
    .param p1    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952366
    iput-object p1, p0, LX/Cy2;->a:LX/CzE;

    .line 1952367
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLNode;)LX/Cz6;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1952368
    iget-object v0, p0, LX/Cy2;->a:LX/CzE;

    invoke-virtual {v0, p1}, LX/CzE;->b(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1952369
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1952370
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cz6;

    .line 1952371
    :goto_0
    return-object v0

    .line 1952372
    :cond_0
    iget-object v0, p0, LX/Cy2;->a:LX/CzE;

    invoke-virtual {v0, p1}, LX/CzE;->c(Lcom/facebook/graphql/model/GraphQLNode;)LX/0am;

    move-result-object v0

    .line 1952373
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1952374
    invoke-direct {p0, p1}, LX/Cy2;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/Cz6;

    move-result-object v0

    .line 1952375
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/Cz6;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1952376
    invoke-direct {p0, p1}, LX/Cy2;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/Cz6;

    move-result-object v0

    .line 1952377
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/Cz6;->m()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
