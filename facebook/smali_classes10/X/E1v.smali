.class public LX/E1v;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E1t;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E1v;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2071880
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E1v;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071900
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2071901
    iput-object p1, p0, LX/E1v;->b:LX/0Ot;

    .line 2071902
    return-void
.end method

.method public static a(LX/0QB;)LX/E1v;
    .locals 4

    .prologue
    .line 2071887
    sget-object v0, LX/E1v;->c:LX/E1v;

    if-nez v0, :cond_1

    .line 2071888
    const-class v1, LX/E1v;

    monitor-enter v1

    .line 2071889
    :try_start_0
    sget-object v0, LX/E1v;->c:LX/E1v;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2071890
    if-eqz v2, :cond_0

    .line 2071891
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2071892
    new-instance v3, LX/E1v;

    const/16 p0, 0x30af

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E1v;-><init>(LX/0Ot;)V

    .line 2071893
    move-object v0, v3

    .line 2071894
    sput-object v0, LX/E1v;->c:LX/E1v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071895
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2071896
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2071897
    :cond_1
    sget-object v0, LX/E1v;->c:LX/E1v;

    return-object v0

    .line 2071898
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2071899
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2071883
    check-cast p2, LX/E1u;

    .line 2071884
    iget-object v0, p0, LX/E1v;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;

    iget-object v1, p2, LX/E1u;->a:LX/9qL;

    iget-object v2, p2, LX/E1u;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    iget-object v3, p2, LX/E1u;->c:LX/5sY;

    .line 2071885
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    invoke-static {v2}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;)I

    move-result v5

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    if-nez v3, :cond_0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->b:LX/E28;

    invoke-virtual {v5, p1}, LX/E28;->c(LX/1De;)LX/E26;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/E26;->a(LX/9qL;)LX/E26;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v5, p0}, LX/1Di;->b(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2071886
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    iget-object v4, v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-interface {v3}, LX/5sY;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v4

    sget-object p2, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreButtonComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object p0

    const p2, 0x7f0a010a

    invoke-virtual {p0, p2}, LX/1nh;->i(I)LX/1nh;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-static {v2}, LX/CgW;->b(Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;)I

    move-result p0

    const p2, 0x7f0b163d

    invoke-interface {v4, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const p0, 0x7f0b1631

    invoke-interface {v4, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const p0, 0x7f0b1631

    invoke-interface {v4, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2071881
    invoke-static {}, LX/1dS;->b()V

    .line 2071882
    const/4 v0, 0x0

    return-object v0
.end method
