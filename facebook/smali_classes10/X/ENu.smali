.class public LX/ENu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ENx;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ENu",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ENx;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113651
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2113652
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ENu;->b:LX/0Zi;

    .line 2113653
    iput-object p1, p0, LX/ENu;->a:LX/0Ot;

    .line 2113654
    return-void
.end method

.method public static a(LX/0QB;)LX/ENu;
    .locals 4

    .prologue
    .line 2113674
    const-class v1, LX/ENu;

    monitor-enter v1

    .line 2113675
    :try_start_0
    sget-object v0, LX/ENu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113676
    sput-object v2, LX/ENu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113677
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113678
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113679
    new-instance v3, LX/ENu;

    const/16 p0, 0x3455

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ENu;-><init>(LX/0Ot;)V

    .line 2113680
    move-object v0, v3

    .line 2113681
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113682
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113683
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113684
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2113671
    check-cast p2, LX/ENs;

    .line 2113672
    iget-object v0, p0, LX/ENu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ENx;

    iget-object v2, p2, LX/ENs;->a:LX/CzL;

    iget-object v3, p2, LX/ENs;->b:LX/1Ps;

    iget-boolean v4, p2, LX/ENs;->c:Z

    iget v5, p2, LX/ENs;->d:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/ENx;->a(LX/1De;LX/CzL;LX/1Ps;ZI)LX/1Dg;

    move-result-object v0

    .line 2113673
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2113669
    invoke-static {}, LX/1dS;->b()V

    .line 2113670
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 2113665
    check-cast p1, LX/ENs;

    .line 2113666
    check-cast p2, LX/ENs;

    .line 2113667
    iget v0, p1, LX/ENs;->d:I

    iput v0, p2, LX/ENs;->d:I

    .line 2113668
    return-void
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 2113656
    check-cast p2, LX/ENs;

    .line 2113657
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 2113658
    iget-object v0, p0, LX/ENu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2113659
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2113660
    iput-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2113661
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2113662
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/ENs;->d:I

    .line 2113663
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 2113664
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2113655
    const/4 v0, 0x1

    return v0
.end method
