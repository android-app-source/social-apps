.class public final LX/EiR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/confirmation/graphql/FBResendContactpointCodeFragmentsModels$FBResendContactpointCodeCoreMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V
    .locals 0

    .prologue
    .line 2159429
    iput-object p1, p0, LX/EiR;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2159430
    instance-of v1, p1, LX/4Ua;

    if-eqz v1, :cond_2

    .line 2159431
    check-cast p1, LX/4Ua;

    .line 2159432
    iget-object v1, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 2159433
    if-nez v1, :cond_0

    :goto_0
    move-object v1, v0

    .line 2159434
    :goto_1
    if-nez v1, :cond_1

    new-instance v0, LX/27k;

    const v1, 0x7f080039

    invoke-direct {v0, v1}, LX/27k;-><init>(I)V

    .line 2159435
    :goto_2
    iget-object v1, p0, LX/EiR;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->c:LX/0kL;

    invoke-virtual {v1, v0}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2159436
    return-void

    .line 2159437
    :cond_0
    iget-object v0, v1, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    goto :goto_0

    .line 2159438
    :cond_1
    new-instance v0, LX/27k;

    invoke-direct {v0, v1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2159439
    iget-object v0, p0, LX/EiR;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0833dd

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2159440
    iget-object v0, p0, LX/EiR;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-static {v0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->D(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    .line 2159441
    return-void
.end method
