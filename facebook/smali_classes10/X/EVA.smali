.class public final LX/EVA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/394;


# instance fields
.field public final synthetic a:LX/EVB;

.field private final b:LX/0hE;

.field private final c:LX/395;

.field private final d:LX/ETa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/EVB;LX/0hE;LX/395;LX/ETa;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hE;",
            "LX/395;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2127525
    iput-object p1, p0, LX/EVA;->a:LX/EVB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2127526
    iput-object p2, p0, LX/EVA;->b:LX/0hE;

    .line 2127527
    iput-object p3, p0, LX/EVA;->c:LX/395;

    .line 2127528
    iput-object p4, p0, LX/EVA;->d:LX/ETa;

    .line 2127529
    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 8

    .prologue
    .line 2127530
    iget-object v0, p0, LX/EVA;->b:LX/0hE;

    invoke-interface {v0}, LX/0hE;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2127531
    iget-object v0, p0, LX/EVA;->a:LX/EVB;

    iget-object v0, v0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->d:LX/1C2;

    iget-object v1, p0, LX/EVA;->c:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/EVA;->c:LX/395;

    invoke-virtual {v4}, LX/395;->p()I

    move-result v4

    iget-object v5, p0, LX/EVA;->c:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/EVA;->c:LX/395;

    .line 2127532
    iget-object v7, v6, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    move-object v6, v7

    .line 2127533
    iget-object v7, v6, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v6, v7

    .line 2127534
    iget-object v7, p0, LX/EVA;->c:LX/395;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 2127535
    :cond_0
    return-void
.end method

.method public final a(LX/04g;LX/7Jv;)V
    .locals 8

    .prologue
    .line 2127536
    iget-object v0, p0, LX/EVA;->a:LX/EVB;

    iget-object v0, v0, LX/EVB;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    instance-of v0, v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_0

    .line 2127537
    iget-object v0, p0, LX/EVA;->d:LX/ETa;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2127538
    :cond_0
    iget-object v0, p0, LX/EVA;->a:LX/EVB;

    iget-object v0, v0, LX/EVB;->d:LX/EUB;

    iget v1, p2, LX/7Jv;->c:I

    invoke-virtual {v0, v1}, LX/EUB;->a(I)V

    .line 2127539
    iget-object v0, p0, LX/EVA;->a:LX/EVB;

    iget-object v0, v0, LX/EVB;->d:LX/EUB;

    invoke-virtual {v0}, LX/EUB;->b()LX/2oO;

    move-result-object v0

    .line 2127540
    if-eqz v0, :cond_1

    .line 2127541
    iget-boolean v1, p2, LX/7Jv;->b:Z

    iget-boolean v2, p2, LX/7Jv;->a:Z

    invoke-virtual {v0, v1, v2}, LX/2oO;->a(ZZ)V

    .line 2127542
    :cond_1
    iget-object v0, p0, LX/EVA;->b:LX/0hE;

    invoke-interface {v0}, LX/0hE;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2127543
    iget-object v0, p0, LX/EVA;->a:LX/EVB;

    iget-object v0, v0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->d:LX/1C2;

    iget-object v1, p0, LX/EVA;->c:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget v4, p2, LX/7Jv;->c:I

    iget-object v5, p0, LX/EVA;->c:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/EVA;->c:LX/395;

    .line 2127544
    iget-object v7, v6, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    move-object v6, v7

    .line 2127545
    iget-object v7, v6, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v6, v7

    .line 2127546
    iget-object v7, p0, LX/EVA;->c:LX/395;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 2127547
    :cond_2
    iget-object v0, p0, LX/EVA;->a:LX/EVB;

    iget-object v0, v0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->c:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EVA;->a:LX/EVB;

    iget-object v0, v0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->c:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2127548
    iget-object v0, p0, LX/EVA;->a:LX/EVB;

    iget-object v0, v0, LX/EVB;->a:Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePlayFullscreenVideoPartDefinition;->c:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->e()V

    .line 2127549
    :cond_3
    return-void
.end method
