.class public LX/Eem;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2153411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/ParcelFileDescriptor;Ljava/io/File;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2153396
    :try_start_0
    new-instance v7, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v7, p0}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2153397
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2153398
    :try_start_2
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 2153399
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 2153400
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2153401
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->close()V

    .line 2153402
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    return-void

    .line 2153403
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_0

    .line 2153404
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;->close()V

    .line 2153405
    :cond_0
    if-eqz v1, :cond_1

    .line 2153406
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_1
    throw v0

    .line 2153407
    :catchall_1
    move-exception v0

    move-object v2, v7

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v1, v6

    move-object v2, v7

    goto :goto_0
.end method

.method public static a(Landroid/content/SharedPreferences;Lcom/facebook/appupdate/ReleaseInfo;)Z
    .locals 2

    .prologue
    .line 2153409
    iget-object v0, p1, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, LX/Ef3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2153410
    if-lez v0, :cond_0

    iget v1, p1, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()J
    .locals 2

    .prologue
    .line 2153408
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method
