.class public final LX/EPG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Ps;

.field public final synthetic b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

.field public final synthetic c:LX/EPI;


# direct methods
.method public constructor <init>(LX/EPI;LX/1Ps;Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;)V
    .locals 0

    .prologue
    .line 2116370
    iput-object p1, p0, LX/EPG;->c:LX/EPI;

    iput-object p2, p0, LX/EPG;->a:LX/1Ps;

    iput-object p3, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 15

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x32c4565b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2116371
    iget-object v12, p0, LX/EPG;->c:LX/EPI;

    iget-object v13, p0, LX/EPG;->a:LX/1Ps;

    new-instance v0, LX/EPH;

    iget-object v1, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    iget-object v2, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    iget-object v3, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    iget-object v4, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->o()LX/0Px;

    move-result-object v4

    iget-object v5, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    iget-object v6, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-virtual {v6}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->m()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v7, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-virtual {v7}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->k()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iget-object v8, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-virtual {v8}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->n()LX/0am;

    move-result-object v8

    invoke-virtual {v8}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iget-object v9, p0, LX/EPG;->a:LX/1Ps;

    check-cast v9, LX/CxV;

    invoke-interface {v9}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->y()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    iget-object v10, p0, LX/EPG;->a:LX/1Ps;

    check-cast v10, LX/CxG;

    iget-object v14, p0, LX/EPG;->b:Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-interface {v10, v14}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v10

    invoke-direct/range {v0 .. v10}, LX/EPH;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;LX/0Px;LX/CvV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;I)V

    invoke-static {v12, v13, v0}, LX/EPI;->a$redex0(LX/EPI;LX/1Ps;LX/EPH;)V

    .line 2116372
    const/4 v0, 0x2

    const/4 v1, 0x2

    const v2, 0x56a81e31

    invoke-static {v0, v1, v2, v11}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
