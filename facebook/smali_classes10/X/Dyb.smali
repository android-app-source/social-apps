.class public final LX/Dyb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/107;


# instance fields
.field public final synthetic a:Lcom/facebook/places/checkin/PlacePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V
    .locals 0

    .prologue
    .line 2064760
    iput-object p1, p0, LX/Dyb;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2064761
    iget-object v0, p0, LX/Dyb;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    invoke-virtual {v0}, LX/Dyt;->c()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2064762
    iget-object v0, p0, LX/Dyb;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    .line 2064763
    invoke-static {v0}, LX/Dyt;->h(LX/Dyt;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2064764
    iget-object v1, v0, LX/Dyt;->h:LX/9j9;

    .line 2064765
    iget-object p0, v1, LX/9j9;->c:LX/9jB;

    invoke-virtual {p0}, LX/9jB;->b()V

    .line 2064766
    iget-object v1, v0, LX/Dyt;->f:LX/9j5;

    .line 2064767
    iget-object p0, v1, LX/9j5;->a:LX/0Zb;

    const-string p1, "place_picker_people_to_place_skip"

    invoke-static {v1, p1}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2064768
    :cond_0
    :goto_0
    iget-object v1, v0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 p0, -0x1

    .line 2064769
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 2064770
    invoke-static {v0, p1}, LX/Dyt;->a(LX/Dyt;Landroid/content/Intent;)V

    .line 2064771
    move-object p1, p1

    .line 2064772
    invoke-virtual {v1, p0, p1}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(ILandroid/content/Intent;)V

    .line 2064773
    return-void

    .line 2064774
    :cond_1
    invoke-static {v0}, LX/Dyt;->g(LX/Dyt;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2064775
    iget-object v1, v0, LX/Dyt;->f:LX/9j5;

    .line 2064776
    iget-object p0, v1, LX/9j5;->a:LX/0Zb;

    const-string p1, "place_picker_minutiae_to_place_skip"

    invoke-static {v1, p1}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2064777
    goto :goto_0
.end method
