.class public final LX/ESX;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Lcom/facebook/photos/base/photos/VaultPhoto;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

.field private b:Lcom/facebook/photos/base/photos/VaultPhoto;

.field private final c:LX/ERZ;

.field private final d:LX/ERY;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;LX/ERZ;LX/ERY;)V
    .locals 0

    .prologue
    .line 2123095
    iput-object p1, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 2123096
    iput-object p2, p0, LX/ESX;->c:LX/ERZ;

    .line 2123097
    iput-object p3, p0, LX/ESX;->d:LX/ERY;

    .line 2123098
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2123099
    check-cast p1, [Lcom/facebook/photos/base/photos/VaultPhoto;

    const/4 v4, 0x0

    .line 2123100
    aget-object v0, p1, v4

    iput-object v0, p0, LX/ESX;->b:Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2123101
    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, LX/ESX;->publishProgress([Ljava/lang/Object;)V

    .line 2123102
    :try_start_0
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->d:LX/11H;

    iget-object v1, p0, LX/ESX;->c:LX/ERZ;

    iget-object v2, p0, LX/ESX;->b:Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2123103
    iget-wide v5, v2, LX/74w;->a:J

    move-wide v2, v5

    .line 2123104
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2123105
    :try_start_1
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->d:LX/11H;

    iget-object v1, p0, LX/ESX;->d:LX/ERY;

    iget-object v2, p0, LX/ESX;->b:Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2123106
    iget-wide v5, v2, LX/74w;->a:J

    move-wide v2, v5

    .line 2123107
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2123108
    :goto_0
    return-object v0

    .line 2123109
    :catch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2123110
    :catch_1
    move-exception v0

    .line 2123111
    const-class v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    const-string v2, "Error running SingleMethodRunner"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2123112
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2123074
    check-cast p1, Ljava/lang/Boolean;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2123075
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2123076
    new-array v0, v6, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, LX/ESX;->publishProgress([Ljava/lang/Object;)V

    .line 2123077
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    iget-object v1, p0, LX/ESX;->b:Lcom/facebook/photos/base/photos/VaultPhoto;

    invoke-interface {v0, v1}, LX/ES9;->a(Lcom/facebook/photos/base/photos/VaultPhoto;)V

    .line 2123078
    new-array v0, v7, [Ljava/lang/String;

    iget-object v1, p0, LX/ESX;->b:Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2123079
    iget-wide v8, v1, LX/74w;->a:J

    move-wide v2, v8

    .line 2123080
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 2123081
    iget-object v1, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, LX/DbE;->a:Landroid/net/Uri;

    const-string v3, "%s = ?"

    new-array v4, v7, [Ljava/lang/Object;

    sget-object v5, LX/DbD;->b:LX/0U1;

    .line 2123082
    iget-object v8, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v8

    .line 2123083
    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2123084
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    invoke-virtual {v0}, LX/ESY;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2123085
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    invoke-virtual {v0, v7}, LX/ESY;->cancel(Z)Z

    .line 2123086
    :cond_0
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    new-instance v1, LX/ESY;

    iget-object v2, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {v1, v2}, LX/ESY;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    .line 2123087
    iput-object v1, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    .line 2123088
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e:LX/0Sh;

    iget-object v1, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    new-array v2, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 2123089
    :cond_1
    :goto_0
    return-void

    .line 2123090
    :cond_2
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    iget-object v1, p0, LX/ESX;->b:Lcom/facebook/photos/base/photos/VaultPhoto;

    invoke-virtual {v1}, Lcom/facebook/photos/base/photos/VaultPhoto;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, LX/ES9;->b(Landroid/net/Uri;)V

    .line 2123091
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    .line 2123092
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v1

    .line 2123093
    invoke-virtual {p0}, LX/ESX;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2123094
    new-instance v1, Lcom/facebook/vault/ui/VaultDeleteFailedDialog;

    invoke-direct {v1}, Lcom/facebook/vault/ui/VaultDeleteFailedDialog;-><init>()V

    const-string v2, "delete_failed_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onProgressUpdate([Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2123072
    iget-object v0, p0, LX/ESX;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    iget-object v1, p0, LX/ESX;->b:Lcom/facebook/photos/base/photos/VaultPhoto;

    invoke-virtual {v1}, Lcom/facebook/photos/base/photos/VaultPhoto;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, LX/ES9;->a(Landroid/net/Uri;)V

    .line 2123073
    return-void
.end method
