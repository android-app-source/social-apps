.class public final LX/Dbc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/3rL",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLInterfaces$PhotoMenusData$PagePhotoMenus$Nodes$PagePhotoMenuPhotos$;",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

.field public final synthetic b:LX/Dbe;


# direct methods
.method public constructor <init>(LX/Dbe;Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;)V
    .locals 0

    .prologue
    .line 2017298
    iput-object p1, p0, LX/Dbc;->b:LX/Dbe;

    iput-object p2, p0, LX/Dbc;->a:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2017299
    iget-object v0, p0, LX/Dbc;->a:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    .line 2017300
    iget-object p0, v0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2017301
    iget-object p0, v0, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    const p1, 0x7f0828d7

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(Ljava/lang/CharSequence;)V

    .line 2017302
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2017303
    check-cast p1, LX/3rL;

    .line 2017304
    iget-object v2, p0, LX/Dbc;->a:Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;

    iget-object v0, p1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    iget-object v1, p1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2017305
    iget-object v3, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2017306
    iget-object v3, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    const v4, 0x7f0828d9

    invoke-virtual {v2, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(Ljava/lang/CharSequence;)V

    .line 2017307
    if-nez v0, :cond_0

    .line 2017308
    :goto_0
    return-void

    .line 2017309
    :cond_0
    iput-boolean v1, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->p:Z

    .line 2017310
    iget-object v3, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->o:Ljava/lang/String;

    iget-object v4, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2017311
    iget-object p0, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v4, p0

    .line 2017312
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-boolean v3, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->p:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 2017313
    if-eqz v3, :cond_1

    .line 2017314
    iget-object v3, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->j:LX/3iH;

    iget-object v4, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->o:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2017315
    iget-object v4, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->g:LX/1Ck;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "page_photo_menu_fetch_viewer_context"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->o:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/Dbf;

    invoke-direct {p1, v2, v0}, LX/Dbf;-><init>(Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;)V

    invoke-virtual {v4, p0, v3, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2017316
    :cond_1
    iget-object v3, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuFragment;->n:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
