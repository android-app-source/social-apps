.class public final enum LX/ED5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ED5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ED5;

.field public static final enum ADD_NEW:LX/ED5;

.field public static final enum EMPTY:LX/ED5;

.field public static final enum ROSTER:LX/ED5;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2090859
    new-instance v0, LX/ED5;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v2}, LX/ED5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ED5;->EMPTY:LX/ED5;

    .line 2090860
    new-instance v0, LX/ED5;

    const-string v1, "ROSTER"

    invoke-direct {v0, v1, v3}, LX/ED5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ED5;->ROSTER:LX/ED5;

    .line 2090861
    new-instance v0, LX/ED5;

    const-string v1, "ADD_NEW"

    invoke-direct {v0, v1, v4}, LX/ED5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ED5;->ADD_NEW:LX/ED5;

    .line 2090862
    const/4 v0, 0x3

    new-array v0, v0, [LX/ED5;

    sget-object v1, LX/ED5;->EMPTY:LX/ED5;

    aput-object v1, v0, v2

    sget-object v1, LX/ED5;->ROSTER:LX/ED5;

    aput-object v1, v0, v3

    sget-object v1, LX/ED5;->ADD_NEW:LX/ED5;

    aput-object v1, v0, v4

    sput-object v0, LX/ED5;->$VALUES:[LX/ED5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2090865
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ED5;
    .locals 1

    .prologue
    .line 2090864
    const-class v0, LX/ED5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ED5;

    return-object v0
.end method

.method public static values()[LX/ED5;
    .locals 1

    .prologue
    .line 2090863
    sget-object v0, LX/ED5;->$VALUES:[LX/ED5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ED5;

    return-object v0
.end method
