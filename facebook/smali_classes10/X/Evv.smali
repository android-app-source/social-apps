.class public LX/Evv;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "LX/BcS;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Evx;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Evv",
            "<TTUserInfo;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Evx;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2182646
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2182647
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Evv;->b:LX/0Zi;

    .line 2182648
    iput-object p1, p0, LX/Evv;->a:LX/0Ot;

    .line 2182649
    return-void
.end method

.method public static a(LX/BcP;LX/BcP;)LX/BcQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcP;",
            "LX/BcP;",
            ")",
            "LX/BcQ",
            "<",
            "LX/BdG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2182645
    const v0, 0x374ca36a

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Evv;
    .locals 4

    .prologue
    .line 2182634
    const-class v1, LX/Evv;

    monitor-enter v1

    .line 2182635
    :try_start_0
    sget-object v0, LX/Evv;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2182636
    sput-object v2, LX/Evv;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2182637
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2182638
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2182639
    new-instance v3, LX/Evv;

    const/16 p0, 0x223d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Evv;-><init>(LX/0Ot;)V

    .line 2182640
    move-object v0, v3

    .line 2182641
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2182642
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Evv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2182643
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2182644
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/BcP;Z)V
    .locals 3

    .prologue
    .line 2182628
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 2182629
    if-nez v0, :cond_0

    .line 2182630
    :goto_0
    return-void

    .line 2182631
    :cond_0
    check-cast v0, LX/Evt;

    .line 2182632
    new-instance v1, LX/Evu;

    iget-object v2, v0, LX/Evt;->d:LX/Evv;

    invoke-direct {v1, v2, p1}, LX/Evu;-><init>(LX/Evv;Z)V

    move-object v0, v1

    .line 2182633
    invoke-virtual {p0, v0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method

.method public static b(LX/BcP;LX/BcP;)LX/BcQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcP;",
            "LX/BcP;",
            ")",
            "LX/BcQ",
            "<",
            "LX/BcM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2182564
    const v0, -0x44c0eb4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 2182595
    invoke-static {}, LX/1dS;->b()V

    .line 2182596
    iget v0, p1, LX/BcQ;->b:I

    .line 2182597
    sparse-switch v0, :sswitch_data_0

    move-object v0, v6

    .line 2182598
    :goto_0
    return-object v0

    .line 2182599
    :sswitch_0
    check-cast p2, LX/BdG;

    .line 2182600
    iget-object v2, p2, LX/BdG;->b:Ljava/lang/Object;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v4

    check-cast v0, LX/BcP;

    .line 2182601
    iget-object v4, p0, LX/Evv;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Evx;

    .line 2182602
    iget-object p0, v4, LX/Evx;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Etz;

    const/4 v1, 0x0

    .line 2182603
    new-instance v3, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;

    invoke-direct {v3, p0}, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;-><init>(LX/Etz;)V

    .line 2182604
    sget-object v4, LX/Etz;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Ety;

    .line 2182605
    if-nez v4, :cond_0

    .line 2182606
    new-instance v4, LX/Ety;

    invoke-direct {v4}, LX/Ety;-><init>()V

    .line 2182607
    :cond_0
    invoke-static {v4, v0, v1, v1, v3}, LX/Ety;->a$redex0(LX/Ety;LX/1De;IILcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;)V

    .line 2182608
    move-object v3, v4

    .line 2182609
    move-object v1, v3

    .line 2182610
    move-object p0, v1

    .line 2182611
    check-cast v2, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2182612
    iget-object v1, p0, LX/Ety;->a:Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;

    iput-object v2, v1, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2182613
    iget-object v1, p0, LX/Ety;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2182614
    move-object p0, p0

    .line 2182615
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    move-object v4, p0

    .line 2182616
    move-object v0, v4

    .line 2182617
    goto :goto_0

    .line 2182618
    :sswitch_1
    check-cast p2, LX/BcM;

    .line 2182619
    iget-boolean v1, p2, LX/BcM;->a:Z

    iget-object v2, p2, LX/BcM;->b:LX/BcL;

    iget-object v3, p2, LX/BcM;->c:Ljava/lang/Throwable;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v4, v0, v4

    check-cast v4, LX/BcP;

    move-object v0, p0

    .line 2182620
    iget-object p0, v0, LX/Evv;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2182621
    sget-object p0, LX/Evw;->a:[I

    invoke-virtual {v2}, LX/BcL;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_0

    .line 2182622
    :goto_1
    invoke-static {v4, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 2182623
    move-object v0, v6

    .line 2182624
    goto :goto_0

    .line 2182625
    :pswitch_0
    invoke-static {v4, v5}, LX/Evv;->a(LX/BcP;Z)V

    goto :goto_1

    .line 2182626
    :pswitch_1
    const/4 p0, 0x1

    invoke-static {v4, p0}, LX/Evv;->a(LX/BcP;Z)V

    goto :goto_1

    .line 2182627
    :pswitch_2
    invoke-static {v4, v5}, LX/Evv;->a(LX/BcP;Z)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x44c0eb4 -> :sswitch_1
        0x374ca36a -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 2182591
    check-cast p1, LX/Evt;

    .line 2182592
    check-cast p2, LX/Evt;

    .line 2182593
    iget-boolean v0, p1, LX/Evt;->b:Z

    iput-boolean v0, p2, LX/Evt;->b:Z

    .line 2182594
    return-void
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 9

    .prologue
    .line 2182573
    check-cast p3, LX/Evt;

    .line 2182574
    iget-object v0, p0, LX/Evv;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Evx;

    iget-boolean v1, p3, LX/Evt;->b:Z

    iget-object v2, p3, LX/Evt;->c:LX/95R;

    const/16 v7, 0x14

    .line 2182575
    if-eqz v2, :cond_1

    .line 2182576
    const/4 v3, 0x0

    .line 2182577
    :try_start_0
    invoke-virtual {v2}, LX/95R;->a()LX/2kW;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2182578
    :goto_0
    if-eqz v3, :cond_0

    .line 2182579
    invoke-static {p1}, LX/Bd3;->b(LX/BcP;)LX/Bd0;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/Bd0;->a(LX/2kW;)LX/Bd0;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/Bd0;->b(I)LX/Bd0;

    move-result-object v3

    invoke-static {p1, p1}, LX/Evv;->a(LX/BcP;LX/BcP;)LX/BcQ;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Bd0;->b(LX/BcQ;)LX/Bd0;

    move-result-object v3

    invoke-static {p1, p1}, LX/Evv;->b(LX/BcP;LX/BcP;)LX/BcQ;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Bd0;->c(LX/BcQ;)LX/Bd0;

    move-result-object v3

    invoke-virtual {v3}, LX/Bd0;->b()LX/BcO;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2182580
    :cond_0
    :goto_1
    return-void

    .line 2182581
    :cond_1
    iget-object v3, v0, LX/Evx;->c:LX/1rs;

    if-nez v3, :cond_2

    .line 2182582
    new-instance v3, LX/EuR;

    invoke-direct {v3}, LX/EuR;-><init>()V

    iput-object v3, v0, LX/Evx;->c:LX/1rs;

    .line 2182583
    :cond_2
    iget-object v3, v0, LX/Evx;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bcw;

    invoke-virtual {v3, p1}, LX/Bcw;->c(LX/BcP;)LX/Bct;

    move-result-object v3

    iget-object v4, v0, LX/Evx;->c:LX/1rs;

    invoke-virtual {v3, v4}, LX/Bct;->a(LX/1rs;)LX/Bct;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/Bct;->a(I)LX/Bct;

    move-result-object v3

    invoke-static {p1, p1}, LX/Evv;->a(LX/BcP;LX/BcP;)LX/BcQ;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Bct;->b(LX/BcQ;)LX/Bct;

    move-result-object v3

    invoke-static {p1, p1}, LX/Evv;->b(LX/BcP;LX/BcP;)LX/BcQ;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Bct;->c(LX/BcQ;)LX/Bct;

    move-result-object v3

    const-string v4, "FriendsCenterPassConfig"

    invoke-virtual {v3, v4}, LX/Bct;->b(Ljava/lang/String;)LX/Bct;

    move-result-object v3

    const-wide/16 v5, 0x258

    .line 2182584
    iget-object v8, v3, LX/Bct;->a:LX/Bcu;

    iput-wide v5, v8, LX/Bcu;->f:J

    .line 2182585
    move-object v3, v3

    .line 2182586
    iget-object v4, v3, LX/Bct;->a:LX/Bcu;

    iput v7, v4, LX/Bcu;->g:I

    .line 2182587
    move-object v3, v3

    .line 2182588
    invoke-virtual {v3}, LX/Bct;->b()LX/BcO;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2182589
    if-eqz v1, :cond_0

    .line 2182590
    invoke-static {p1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v3

    invoke-static {p1}, LX/Bdj;->c(LX/1De;)LX/Bdh;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object v3

    invoke-virtual {v3}, LX/Bcc;->b()LX/BcO;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catch_0
    goto/16 :goto_0
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 2

    .prologue
    .line 2182565
    check-cast p2, LX/Evt;

    .line 2182566
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v0

    .line 2182567
    iget-object v1, p0, LX/Evv;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2182568
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2182569
    iput-object v1, v0, LX/1np;->a:Ljava/lang/Object;

    .line 2182570
    iget-object v1, v0, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2182571
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/Evt;->b:Z

    .line 2182572
    return-void
.end method
