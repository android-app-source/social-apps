.class public LX/E0T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0So;

.field public c:Lcom/facebook/places/create/home/HomeActivityModel;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/places/create/home/HomeActivityLoggerData;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2068518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068519
    iput-object p1, p0, LX/E0T;->a:LX/0Zb;

    .line 2068520
    iput-object p2, p0, LX/E0T;->b:LX/0So;

    .line 2068521
    return-void
.end method

.method public static a(LX/0QB;)LX/E0T;
    .locals 1

    .prologue
    .line 2068517
    invoke-static {p0}, LX/E0T;->b(LX/0QB;)LX/E0T;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/E0T;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2068506
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068507
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068508
    const-string v0, "query"

    invoke-virtual {p1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068509
    const-string v0, "results_list_id"

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068510
    const-string v0, "place_picker_session_id"

    iget-object v1, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068511
    iget-object p2, v1, Lcom/facebook/places/create/home/HomeActivityLoggerData;->d:Ljava/lang/String;

    move-object v1, p2

    .line 2068512
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068513
    const-string v0, "composer_session_id"

    iget-object v1, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068514
    iget-object p0, v1, Lcom/facebook/places/create/home/HomeActivityLoggerData;->c:Ljava/lang/String;

    move-object v1, p0

    .line 2068515
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068516
    return-object p1
.end method

.method public static b(LX/0QB;)LX/E0T;
    .locals 3

    .prologue
    .line 2068504
    new-instance v2, LX/E0T;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-direct {v2, v0, v1}, LX/E0T;-><init>(LX/0Zb;LX/0So;)V

    .line 2068505
    return-object v2
.end method

.method public static b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2068500
    sget-object v0, LX/E0S;->a:[I

    iget-object v1, p0, LX/E0T;->c:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->k:LX/E0V;

    invoke-virtual {v1}, LX/E0V;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2068501
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2068502
    :pswitch_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "creation"

    aput-object v2, v1, v3

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2068503
    :pswitch_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "edit"

    aput-object v2, v1, v3

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 2068461
    const-string v0, "home_%s"

    invoke-static {p0, v0}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/E0T;->c(LX/E0T;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2068462
    iget-object v1, p0, LX/E0T;->c:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068463
    iget-object v1, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068464
    iget-object v2, v1, Lcom/facebook/places/create/home/HomeActivityLoggerData;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2068465
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068466
    const-string v1, "name"

    iget-object v2, p0, LX/E0T;->c:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v2, v2, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068467
    const-string v1, "city"

    iget-object v2, p0, LX/E0T;->c:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v2, v2, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068468
    const-string v1, "address"

    iget-object v2, p0, LX/E0T;->c:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v2, v2, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068469
    const-string v1, "neighborhood"

    iget-object v2, p0, LX/E0T;->c:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v2, v2, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068470
    iget-object v1, p0, LX/E0T;->c:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v1, :cond_0

    .line 2068471
    const-string v1, "privacy"

    iget-object v2, p0, LX/E0T;->c:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v2, v2, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068472
    :cond_0
    const-string v1, "home_session_id"

    iget-object v2, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068473
    iget-object p1, v2, Lcom/facebook/places/create/home/HomeActivityLoggerData;->a:Ljava/lang/String;

    move-object v2, p1

    .line 2068474
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068475
    const-string v1, "composer_session_id"

    iget-object v2, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068476
    iget-object p1, v2, Lcom/facebook/places/create/home/HomeActivityLoggerData;->c:Ljava/lang/String;

    move-object v2, p1

    .line 2068477
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068478
    const-string v1, "entry_flow"

    iget-object v2, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068479
    iget-object p1, v2, Lcom/facebook/places/create/home/HomeActivityLoggerData;->f:Ljava/lang/String;

    move-object v2, p1

    .line 2068480
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068481
    move-object v0, v0

    .line 2068482
    return-object v0
.end method

.method public static c(LX/E0T;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 8

    .prologue
    .line 2068485
    iget-object v0, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068486
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068487
    iget-object v2, v1, Lcom/facebook/places/create/home/HomeActivityLoggerData;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2068488
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2068489
    move-object v0, v0

    .line 2068490
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2068491
    move-object v0, v0

    .line 2068492
    iget-object v1, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068493
    iget-wide v6, v1, Lcom/facebook/places/create/home/HomeActivityLoggerData;->e:J

    move-wide v2, v6

    .line 2068494
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2068495
    iget-object v1, p0, LX/E0T;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-object v1, p0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068496
    iget-wide v6, v1, Lcom/facebook/places/create/home/HomeActivityLoggerData;->e:J

    move-wide v4, v6

    .line 2068497
    sub-long/2addr v2, v4

    .line 2068498
    const-string v1, "place_picker_milliseconds_since_start"

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068499
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final u()V
    .locals 2

    .prologue
    .line 2068483
    iget-object v0, p0, LX/E0T;->a:LX/0Zb;

    const-string v1, "home_%s_network_error"

    invoke-static {p0, v1}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068484
    return-void
.end method
