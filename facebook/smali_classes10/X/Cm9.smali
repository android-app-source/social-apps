.class public abstract LX/Cm9;
.super LX/Cm5;
.source ""

# interfaces
.implements LX/Clq;


# instance fields
.field private final a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

.field private final b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

.field private final c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

.field private final d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

.field private final g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

.field private final h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field private final i:Lcom/facebook/graphql/model/GraphQLFeedback;


# direct methods
.method public constructor <init>(LX/Cm8;)V
    .locals 1

    .prologue
    .line 1933206
    invoke-direct {p0, p1}, LX/Cm5;-><init>(LX/Cm7;)V

    .line 1933207
    iget-object v0, p1, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    iput-object v0, p0, LX/Cm9;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 1933208
    iget-object v0, p1, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    iput-object v0, p0, LX/Cm9;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 1933209
    iget-object v0, p1, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    iput-object v0, p0, LX/Cm9;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 1933210
    iget-object v0, p1, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    iput-object v0, p0, LX/Cm9;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 1933211
    iget-object v0, p1, LX/Cm8;->e:Ljava/lang/String;

    iput-object v0, p0, LX/Cm9;->e:Ljava/lang/String;

    .line 1933212
    iget-object v0, p1, LX/Cm8;->f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    iput-object v0, p0, LX/Cm9;->f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 1933213
    iget-object v0, p1, LX/Cm8;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    iput-object v0, p0, LX/Cm9;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 1933214
    iget-object v0, p1, LX/Cm8;->h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    iput-object v0, p0, LX/Cm9;->h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 1933215
    iget-object v0, p1, LX/Cm8;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, LX/Cm9;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1933216
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1

    .prologue
    .line 1933217
    iget-object v0, p0, LX/Cm9;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    return-object v0
.end method

.method public final e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1

    .prologue
    .line 1933218
    iget-object v0, p0, LX/Cm9;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    return-object v0
.end method

.method public final f()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;
    .locals 1

    .prologue
    .line 1933219
    iget-object v0, p0, LX/Cm9;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1933220
    iget-object v0, p0, LX/Cm9;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;
    .locals 1

    .prologue
    .line 1933221
    iget-object v0, p0, LX/Cm9;->f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    return-object v0
.end method

.method public final i()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1

    .prologue
    .line 1933222
    iget-object v0, p0, LX/Cm9;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    return-object v0
.end method

.method public final iX_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;
    .locals 1

    .prologue
    .line 1933223
    iget-object v0, p0, LX/Cm9;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    return-object v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;
    .locals 1

    .prologue
    .line 1933224
    iget-object v0, p0, LX/Cm9;->h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 1

    .prologue
    .line 1933225
    iget-object v0, p0, LX/Cm9;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    return-object v0
.end method
