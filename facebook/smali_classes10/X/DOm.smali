.class public LX/DOm;
.super LX/DOb;
.source ""


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0Or;LX/0SG;LX/DOl;LX/B0n;LX/1e0;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/DOK;LX/14w;LX/0ad;LX/0Or;LX/03V;LX/0Or;LX/1Qj;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;LX/0pf;LX/0Uh;LX/88k;)V
    .locals 0
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsfeedSpamReportingEnabled;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsGroupCommerceNewDeleteInterceptEnabled;
        .end annotation
    .end param
    .param p18    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p19    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p27    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .param p32    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .param p35    # LX/1Qj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/DOl;",
            "LX/B0n;",
            "LX/1e0;",
            "LX/0Ot",
            "<",
            "LX/00H;",
            ">;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/DOK;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/1Qj;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0wL;",
            "LX/0pf;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/88k;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992793
    invoke-direct/range {p0 .. p49}, LX/DOb;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0Or;LX/0SG;LX/DOl;LX/B0n;LX/1e0;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/DOK;LX/14w;LX/0ad;LX/0Or;LX/03V;LX/0Or;LX/1Qj;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;LX/0pf;LX/0Uh;LX/88k;)V

    .line 1992794
    return-void
.end method


# virtual methods
.method public final j(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1992795
    const/4 v0, 0x0

    return v0
.end method

.method public final l(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1992796
    invoke-virtual {p0, p1}, LX/DOb;->n(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    return v0
.end method
