.class public final LX/D4X;
.super LX/1Yy;
.source ""


# instance fields
.field public final synthetic a:LX/D4Z;


# direct methods
.method public constructor <init>(LX/D4Z;)V
    .locals 0

    .prologue
    .line 1962261
    iput-object p1, p0, LX/D4X;->a:LX/D4Z;

    invoke-direct {p0}, LX/1Yy;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 1962262
    check-cast p1, LX/1Zj;

    .line 1962263
    iget-object v0, p0, LX/D4X;->a:LX/D4Z;

    iget-object v0, v0, LX/D4Z;->g:LX/D6S;

    iget-object v1, p1, LX/1Zj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/D6S;->a(Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1962264
    if-eqz v2, :cond_0

    .line 1962265
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962266
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1962267
    :cond_0
    :goto_0
    return-void

    .line 1962268
    :cond_1
    iget-object v0, p0, LX/D4X;->a:LX/D4Z;

    iget-object v1, v0, LX/D4Z;->b:LX/189;

    iget-object v0, p0, LX/D4X;->a:LX/D4Z;

    iget-object v0, v0, LX/D4Z;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1962269
    iget-object v1, p0, LX/D4X;->a:LX/D4Z;

    iget-object v1, v1, LX/D4Z;->a:LX/0QK;

    .line 1962270
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1962271
    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1962272
    new-instance v3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    const-string v1, "video_channel_feed_ufi"

    const-string v4, "video_channel_feed"

    invoke-direct {v3, v0, v1, v4}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    .line 1962273
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962274
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/1zt;->c:LX/1zt;

    move-object v1, v0

    .line 1962275
    :goto_1
    iget-object v0, p0, LX/D4X;->a:LX/D4Z;

    iget-object v4, v0, LX/D4Z;->e:LX/20h;

    .line 1962276
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962277
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v4, v0, v1, v3, v2}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/0Ve;)V

    goto :goto_0

    .line 1962278
    :cond_2
    iget-object v0, p0, LX/D4X;->a:LX/D4Z;

    iget-object v0, v0, LX/D4Z;->f:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->e()LX/1zt;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method
