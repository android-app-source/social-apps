.class public final LX/E90;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/E93;


# direct methods
.method public constructor <init>(LX/E93;)V
    .locals 0

    .prologue
    .line 2083587
    iput-object p1, p0, LX/E90;->a:LX/E93;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2083588
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2083589
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2083590
    if-eqz p1, :cond_0

    .line 2083591
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2083592
    if-nez v0, :cond_1

    .line 2083593
    :cond_0
    :goto_0
    return-void

    .line 2083594
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2083595
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2083596
    iget-object v1, p0, LX/E90;->a:LX/E93;

    iget-object v1, v1, LX/E93;->x:LX/E1l;

    .line 2083597
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 2083598
    iget-object v3, v1, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Cfo;

    .line 2083599
    invoke-interface {v3}, LX/Cfo;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v5

    .line 2083600
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2083601
    :goto_1
    move-object v2, v3

    .line 2083602
    instance-of v3, v2, Lcom/facebook/reaction/common/ReactionCardNode;

    if-eqz v3, :cond_3

    .line 2083603
    check-cast v2, Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2083604
    iput-object v0, v2, Lcom/facebook/reaction/common/ReactionCardNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2083605
    :cond_3
    iget-object v0, p0, LX/E90;->a:LX/E93;

    invoke-virtual {v0}, LX/E8m;->k()V

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method
