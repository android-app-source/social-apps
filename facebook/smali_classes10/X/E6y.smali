.class public LX/E6y;
.super LX/Cft;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cft",
        "<",
        "LX/E97;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/E1i;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/961;

.field public final f:Ljava/util/concurrent/Executor;

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/E7E;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/E1i;LX/3Tx;LX/0Ot;LX/0Ot;LX/0Ot;LX/961;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E1i;",
            "LX/3Tx;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/961;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080873
    invoke-direct {p0, p2}, LX/Cft;-><init>(LX/3Tx;)V

    .line 2080874
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/E6y;->g:Ljava/util/HashMap;

    .line 2080875
    iput-object p1, p0, LX/E6y;->a:LX/E1i;

    .line 2080876
    iput-object p3, p0, LX/E6y;->b:LX/0Ot;

    .line 2080877
    iput-object p4, p0, LX/E6y;->c:LX/0Ot;

    .line 2080878
    iput-object p5, p0, LX/E6y;->d:LX/0Ot;

    .line 2080879
    iput-object p6, p0, LX/E6y;->e:LX/961;

    .line 2080880
    iput-object p7, p0, LX/E6y;->f:Ljava/util/concurrent/Executor;

    .line 2080881
    return-void
.end method

.method private static a(Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2080868
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2080869
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 2080870
    invoke-virtual {p0, p2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2080871
    invoke-virtual {p0, p3}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080872
    return-void
.end method

.method public static a$redex0(LX/E6y;LX/E7E;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;)V
    .locals 3

    .prologue
    .line 2080857
    const v1, 0x7f0207df

    .line 2080858
    const v0, -0x423e37

    .line 2080859
    sget-object v2, LX/E7E;->ACCEPTED:LX/E7E;

    if-ne p1, v2, :cond_0

    .line 2080860
    const v1, 0x7f020889

    .line 2080861
    const v0, -0xa76f01

    move v2, v1

    move v1, v0

    .line 2080862
    :goto_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setEnableActionButton(Z)V

    .line 2080863
    iget-object v0, p0, LX/E6y;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-virtual {v0, v2, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2080864
    return-void

    .line 2080865
    :cond_0
    sget-object v2, LX/E7E;->DECLINED:LX/E7E;

    if-ne p1, v2, :cond_2

    .line 2080866
    const v1, 0x7f02089e

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2080867
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v2, v1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 5
    .param p1    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2080848
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    .line 2080849
    invoke-interface {v0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2080850
    const/4 v0, 0x0

    .line 2080851
    :goto_0
    return-object v0

    .line 2080852
    :cond_0
    invoke-interface {v0}, LX/5sc;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x403827a

    if-ne v1, v2, :cond_1

    .line 2080853
    iget-object v0, p0, LX/E6y;->a:LX/E1i;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v1

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E1i;->c(Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    goto :goto_0

    .line 2080854
    :cond_1
    invoke-interface {v0}, LX/5sc;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x25d6af

    if-ne v1, v2, :cond_2

    .line 2080855
    iget-object v0, p0, LX/E6y;->a:LX/E1i;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v2

    invoke-interface {v2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v3

    invoke-interface {v3}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->PROFILE_TAP:LX/Cfc;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/E1i;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    goto :goto_0

    .line 2080856
    :cond_2
    iget-object v1, p0, LX/E6y;->a:LX/E1i;

    sget-object v2, LX/Cfc;->PROFILE_TAP:LX/Cfc;

    invoke-virtual {v1, v0, v2}, LX/E1i;->a(LX/5sc;LX/Cfc;)LX/Cfl;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1a1;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)V
    .locals 8

    .prologue
    .line 2080882
    check-cast p1, LX/E97;

    .line 2080883
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2080884
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v1

    .line 2080885
    invoke-interface {v1}, LX/5sc;->l()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2080886
    invoke-interface {v1}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2080887
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2080888
    sget-object v1, LX/6VG;->NONE:LX/6VG;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonTheme(LX/6VG;)V

    .line 2080889
    const v1, 0x7f0a010a

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2080890
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->A()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2080891
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->A()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2080892
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->l()LX/174;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2080893
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->l()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2080894
    :cond_1
    const/4 v3, 0x0

    .line 2080895
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;

    .line 2080896
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionAttachmentActionFragmentModel;->e()LX/0Px;

    move-result-object v7

    .line 2080897
    if-eqz v7, :cond_4

    .line 2080898
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result p1

    move v2, v3

    :goto_1
    if-ge v2, p1, :cond_4

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    .line 2080899
    invoke-virtual {p0, v0, p2, v1}, LX/Cfk;->a(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2080900
    :cond_2
    return-void

    .line 2080901
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2080902
    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;)Z
    .locals 7

    .prologue
    const v1, -0x423e37

    const/4 v2, 0x1

    .line 2080822
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v3

    .line 2080823
    check-cast p1, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2080824
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->WRITE_ON_TIMELINE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    invoke-virtual {p3, v0}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2080825
    iget-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v0, v0

    .line 2080826
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2080827
    iget-object v1, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v1, v1

    .line 2080828
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f02080f

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v4, LX/E7C;

    invoke-direct {v4, p0, v3}, LX/E7C;-><init>(LX/E6y;LX/5sc;)V

    invoke-static {p1, v0, v1, v4}, LX/E6y;->a(Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V

    move v0, v2

    .line 2080829
    :goto_0
    return v0

    .line 2080830
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->SEND_MESSAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    invoke-virtual {p3, v0}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2080831
    iget-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v0, v0

    .line 2080832
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2080833
    iget-object v1, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v1, v1

    .line 2080834
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f02092d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v4, LX/E7D;

    invoke-direct {v4, p0, v3}, LX/E7D;-><init>(LX/E6y;LX/5sc;)V

    invoke-static {p1, v0, v1, v4}, LX/E6y;->a(Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V

    move v0, v2

    .line 2080835
    goto :goto_0

    .line 2080836
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKED_PAGE_OPTIONS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    invoke-virtual {p3, v0}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2080837
    iget-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v0, v0

    .line 2080838
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f083107

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, LX/E6y;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wM;

    const v5, 0x7f0207df

    invoke-virtual {v0, v5, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v1, LX/E7N;

    invoke-direct {v1, p0, v3}, LX/E7N;-><init>(LX/E6y;LX/5sc;)V

    invoke-static {p1, v4, v0, v1}, LX/E6y;->a(Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V

    move v0, v2

    .line 2080839
    goto :goto_0

    .line 2080840
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->LIKE_PAGE_IN_ATTACHMENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    invoke-virtual {p3, v0}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2080841
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f083109

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, LX/E6y;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wM;

    const v5, 0x7f0208fa

    invoke-interface {v3}, LX/5sc;->as_()Z

    move-result v6

    if-eqz v6, :cond_3

    const v1, -0xa76f01

    :cond_3
    invoke-virtual {v0, v5, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v1, LX/E7K;

    invoke-direct {v1, p0, v3, p1}, LX/E7K;-><init>(LX/E6y;LX/5sc;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;)V

    invoke-static {p1, v4, v0, v1}, LX/E6y;->a(Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V

    move v0, v2

    .line 2080842
    goto/16 :goto_0

    .line 2080843
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->ADMIN_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;

    invoke-virtual {p3, v0}, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentActionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2080844
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f083106

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, LX/E6y;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wM;

    const v5, 0x7f0207df

    invoke-virtual {v0, v5, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v1, LX/E7I;

    invoke-direct {v1, p0, v3, p1}, LX/E7I;-><init>(LX/E6y;LX/5sc;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;)V

    invoke-static {p1, v4, v0, v1}, LX/E6y;->a(Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V

    .line 2080845
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, LX/E6y;->a$redex0(LX/E6y;LX/E7E;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;)V

    move v0, v2

    .line 2080846
    goto/16 :goto_0

    .line 2080847
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 1

    .prologue
    .line 2080814
    iput-object p1, p0, LX/E6y;->h:Ljava/lang/String;

    .line 2080815
    iput-object p2, p0, LX/E6y;->i:Ljava/lang/String;

    .line 2080816
    invoke-super {p0, p1, p2, p3}, LX/Cft;->b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2080821
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/1a1;
    .locals 2

    .prologue
    .line 2080817
    const v0, 0x7f031104

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2080818
    new-instance v1, LX/8sz;

    invoke-direct {v1}, LX/8sz;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2080819
    sget-object v1, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2080820
    new-instance v1, LX/E97;

    invoke-direct {v1, v0}, LX/E97;-><init>(Landroid/view/View;)V

    return-object v1
.end method
