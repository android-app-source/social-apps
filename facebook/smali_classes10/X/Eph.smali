.class public LX/Eph;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D3l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/D3l",
        "<",
        "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/Enk;

.field private final b:LX/EpA;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Epm;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/Emj;

.field public final e:LX/Epl;

.field private final f:LX/11R;

.field public final g:I

.field public h:Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Enk;LX/EpA;LX/0Or;LX/Emj;LX/Epl;LX/EpA;LX/11R;)V
    .locals 1
    .param p1    # LX/Enk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/entitycards/model/EntityCardFetchErrorService;",
            "LX/EpA;",
            "LX/0Or",
            "<",
            "LX/Epm;",
            ">;",
            "LX/Emj;",
            "LX/Epl;",
            "LX/EpA;",
            "LX/11R;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2170509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170510
    iput-object p1, p0, LX/Eph;->a:LX/Enk;

    .line 2170511
    iput-object p2, p0, LX/Eph;->b:LX/EpA;

    .line 2170512
    iput-object p3, p0, LX/Eph;->c:LX/0Or;

    .line 2170513
    iput-object p4, p0, LX/Eph;->d:LX/Emj;

    .line 2170514
    iput-object p5, p0, LX/Eph;->e:LX/Epl;

    .line 2170515
    iput-object p7, p0, LX/Eph;->f:LX/11R;

    .line 2170516
    invoke-virtual {p6}, LX/EpA;->d()I

    move-result v0

    iput v0, p0, LX/Eph;->g:I

    .line 2170517
    return-void
.end method

.method private static a(LX/Eph;Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 2170448
    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2170449
    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2170450
    :cond_0
    :goto_0
    return-object v1

    .line 2170451
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->o()J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->j()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2170452
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->o()J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    .line 2170453
    iget-object v0, p0, LX/Eph;->f:LX/11R;

    sget-object v2, LX/1lB;->EVENTS_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->o()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2170454
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->j()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel$ApplicationModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2170455
    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->j()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel$ApplicationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel$ApplicationModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2170456
    :cond_3
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    .line 2170457
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v8

    aput-object v1, v2, v9

    invoke-static {p2, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    :goto_2
    move-object v1, v0

    .line 2170458
    goto :goto_0

    .line 2170459
    :cond_5
    if-nez v0, :cond_4

    .line 2170460
    new-array v0, v9, [Ljava/lang/Object;

    aput-object v1, v0, v8

    invoke-static {p3, v0}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(LX/Eph;Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 2170474
    iput-object p0, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->b:LX/D3l;

    .line 2170475
    iget-object v0, p0, LX/Eph;->b:LX/EpA;

    .line 2170476
    iget-object v1, v0, LX/EpA;->b:Landroid/content/res/Resources;

    const v2, 0x7f0b2228

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    move v1, v1

    .line 2170477
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 2170478
    const/4 v0, 0x0

    .line 2170479
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2170480
    :cond_0
    :goto_0
    move v2, v0

    .line 2170481
    const/4 v0, 0x0

    .line 2170482
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 2170483
    :cond_1
    :goto_1
    move v3, v0

    .line 2170484
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2170485
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2170486
    :goto_2
    move-object v4, v0

    .line 2170487
    const-string v5, "entity_cards"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIILandroid/net/Uri;Ljava/lang/String;)V

    .line 2170488
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getUnformattedSubtitleDatetimeApplication()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getUnformattedSubtitleApplication()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p2, v0, v1}, LX/Eph;->a(LX/Eph;Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2170489
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->p()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_3

    move v0, v6

    :goto_3
    const v3, 0x7f0b004f

    invoke-static {v7, v3}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v3

    invoke-virtual {p1, v2, v0, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2170490
    const v0, 0x7f0b004e

    invoke-static {v7, v0}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    invoke-virtual {p1, v1, v6, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2170491
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_8

    .line 2170492
    const/4 v0, 0x0

    .line 2170493
    :goto_4
    move-object v0, v0

    .line 2170494
    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2170495
    const v0, 0x7f0b222c

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->c(I)V

    .line 2170496
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b222b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2170497
    iput v0, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->k:I

    .line 2170498
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_2
    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 2170499
    invoke-virtual {p1, v0, p2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ZLjava/lang/Object;)V

    .line 2170500
    return v6

    .line 2170501
    :cond_3
    const/4 v0, 0x2

    goto :goto_3

    .line 2170502
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2170503
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v2

    sub-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/16 :goto_0

    .line 2170504
    :cond_5
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2170505
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a()I

    move-result v3

    sub-int v3, v1, v3

    div-int/lit8 v3, v3, 0x2

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/16 :goto_1

    .line 2170506
    :cond_6
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2170507
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_2

    .line 2170508
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_8
    invoke-virtual {p2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_4

    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public final a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)V
    .locals 7

    .prologue
    .line 2170461
    if-nez p1, :cond_1

    .line 2170462
    :cond_0
    :goto_0
    return-void

    .line 2170463
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;

    .line 2170464
    if-eqz v6, :cond_0

    .line 2170465
    iget-object v0, p0, LX/Eph;->h:Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;

    const-string v1, "tried to handle an input event after dropping the view"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2170466
    iget-object v0, p0, LX/Eph;->h:Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;

    invoke-virtual {v0, p1}, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)I

    move-result v4

    .line 2170467
    const/4 v0, -0x1

    if-eq v4, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2170468
    invoke-virtual {v6}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v6}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v0

    move-object v3, v0

    .line 2170469
    :goto_2
    iget-object v0, p0, LX/Eph;->h:Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;

    invoke-virtual {v0}, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2170470
    iget-object v0, p0, LX/Eph;->d:LX/Emj;

    sget-object v1, LX/Emo;->CONTEXT_ITEM:LX/Emo;

    invoke-virtual {v5}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-interface/range {v0 .. v5}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2170471
    iget-object v0, p0, LX/Eph;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Epm;

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v5}, LX/Epm;->a(Landroid/content/Context;LX/5vW;LX/Eoo;)V

    goto :goto_0

    .line 2170472
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2170473
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-object v3, v0

    goto :goto_2
.end method
