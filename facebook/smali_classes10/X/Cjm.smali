.class public LX/Cjm;
.super LX/CjH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CjH",
        "<",
        "Lcom/facebook/richdocument/optional/Image360PhotoBlockView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/Crk;

.field private final b:LX/K29;


# direct methods
.method public constructor <init>(LX/Crk;LX/K29;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929749
    const v0, 0x7f0311fa

    move v0, v0

    .line 1929750
    const/16 v1, 0x1b

    invoke-direct {p0, v0, v1}, LX/CjH;-><init>(II)V

    .line 1929751
    iput-object p1, p0, LX/Cjm;->a:LX/Crk;

    .line 1929752
    iput-object p2, p0, LX/Cjm;->b:LX/K29;

    .line 1929753
    return-void
.end method

.method private static a(LX/K1w;)LX/CnT;
    .locals 1

    .prologue
    .line 1929748
    new-instance v0, LX/Co9;

    invoke-direct {v0, p0}, LX/Co9;-><init>(LX/K1w;)V

    return-object v0
.end method

.method private b(Landroid/view/View;)LX/K1w;
    .locals 1

    .prologue
    .line 1929745
    new-instance v0, LX/K1w;

    move-object p0, p1

    check-cast p0, LX/Ctg;

    invoke-direct {v0, p0, p1}, LX/K1w;-><init>(LX/Ctg;Landroid/view/View;)V

    move-object p0, v0

    .line 1929746
    move-object v0, p0

    .line 1929747
    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;)LX/CnG;
    .locals 1

    .prologue
    .line 1929737
    invoke-direct {p0, p1}, LX/Cjm;->b(Landroid/view/View;)LX/K1w;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/CnG;)LX/CnT;
    .locals 1

    .prologue
    .line 1929744
    check-cast p1, LX/K1w;

    invoke-static {p1}, LX/Cjm;->a(LX/K1w;)LX/CnT;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)LX/Cs4;
    .locals 2

    .prologue
    .line 1929738
    iget-object v0, p0, LX/Cjm;->a:LX/Crk;

    .line 1929739
    const p1, 0x7f0311fa

    move v1, p1

    .line 1929740
    invoke-virtual {v0, v1}, LX/Crk;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1929741
    invoke-direct {p0, v0}, LX/Cjm;->b(Landroid/view/View;)LX/K1w;

    move-result-object v0

    .line 1929742
    invoke-static {v0}, LX/Cjm;->a(LX/K1w;)LX/CnT;

    .line 1929743
    new-instance v1, LX/Cs4;

    invoke-direct {v1, v0}, LX/Cs4;-><init>(LX/CnG;)V

    return-object v1
.end method
