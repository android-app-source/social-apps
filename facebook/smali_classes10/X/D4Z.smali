.class public LX/D4Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9DK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/9DK",
        "<",
        "LX/D6S;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/189;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0SG;

.field public final e:LX/20h;

.field public final f:LX/1zf;

.field public g:LX/D6S;


# direct methods
.method public constructor <init>(LX/0QK;LX/189;LX/0Or;LX/0SG;LX/20h;LX/1zf;)V
    .locals 0
    .param p1    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;",
            "LX/189;",
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/0SG;",
            "LX/20h;",
            "LX/1zf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1962288
    iput-object p1, p0, LX/D4Z;->a:LX/0QK;

    .line 1962289
    iput-object p2, p0, LX/D4Z;->b:LX/189;

    .line 1962290
    iput-object p3, p0, LX/D4Z;->c:LX/0Or;

    .line 1962291
    iput-object p4, p0, LX/D4Z;->d:LX/0SG;

    .line 1962292
    iput-object p5, p0, LX/D4Z;->e:LX/20h;

    .line 1962293
    iput-object p6, p0, LX/D4Z;->f:LX/1zf;

    .line 1962294
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1962295
    check-cast p1, LX/D6S;

    .line 1962296
    iput-object p1, p0, LX/D4Z;->g:LX/D6S;

    .line 1962297
    return-void
.end method
