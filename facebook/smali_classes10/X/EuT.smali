.class public LX/EuT;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final c:LX/0TD;

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dl;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2179619
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "first_name"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, LX/EuT;->b:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(LX/0TD;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2179620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179621
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179622
    iput-object v0, p0, LX/EuT;->d:LX/0Ot;

    .line 2179623
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179624
    iput-object v0, p0, LX/EuT;->e:LX/0Ot;

    .line 2179625
    const/4 v0, 0x0

    iput v0, p0, LX/EuT;->f:I

    .line 2179626
    iput-object p1, p0, LX/EuT;->c:LX/0TD;

    .line 2179627
    new-instance v0, LX/4a7;

    invoke-direct {v0}, LX/4a7;-><init>()V

    const/4 p1, 0x1

    .line 2179628
    iput-boolean p1, v0, LX/4a7;->b:Z

    .line 2179629
    move-object v0, v0

    .line 2179630
    invoke-virtual {v0}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    move-object v0, v0

    .line 2179631
    iput-object v0, p0, LX/EuT;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179632
    return-void
.end method
