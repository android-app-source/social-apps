.class public final LX/CtF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/CtG;

.field private final b:I

.field private final c:Landroid/content/res/ColorStateList;

.field private final d:F

.field private final e:D

.field private final f:Landroid/content/res/ColorStateList;

.field private final g:Landroid/content/res/ColorStateList;

.field private final h:Landroid/graphics/Typeface;

.field private final i:I

.field private final j:F

.field private final k:F

.field private final l:F

.field private final m:Landroid/text/method/TransformationMethod;

.field private final n:Z

.field private final o:F

.field private final p:Ljava/lang/String;

.field private final q:I

.field private final r:D

.field private final s:Landroid/graphics/drawable/Drawable;

.field private final t:I

.field private final u:I

.field private final v:I

.field private final w:I


# direct methods
.method public constructor <init>(LX/CtG;LX/CtG;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1944182
    iput-object p1, p0, LX/CtF;->a:LX/CtG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1944183
    iget v0, p2, LX/CtG;->b:I

    iput v0, p0, LX/CtF;->q:I

    .line 1944184
    iget-wide v0, p2, LX/CtG;->f:D

    iput-wide v0, p0, LX/CtF;->r:D

    .line 1944185
    invoke-virtual {p2}, LX/CtG;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, LX/CtF;->c:Landroid/content/res/ColorStateList;

    .line 1944186
    invoke-virtual {p2}, LX/CtG;->getTextSize()F

    move-result v0

    iput v0, p0, LX/CtF;->d:F

    .line 1944187
    iget-wide v0, p2, LX/CtG;->e:D

    iput-wide v0, p0, LX/CtF;->e:D

    .line 1944188
    invoke-virtual {p2}, LX/CtG;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, LX/CtF;->f:Landroid/content/res/ColorStateList;

    .line 1944189
    invoke-virtual {p2}, LX/CtG;->getLinkTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, LX/CtF;->g:Landroid/content/res/ColorStateList;

    .line 1944190
    invoke-virtual {p2}, LX/CtG;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, LX/CtF;->h:Landroid/graphics/Typeface;

    .line 1944191
    invoke-virtual {p2}, LX/CtG;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/CtF;->s:Landroid/graphics/drawable/Drawable;

    .line 1944192
    invoke-virtual {p2}, LX/CtG;->getPaddingLeft()I

    move-result v0

    iput v0, p0, LX/CtF;->t:I

    .line 1944193
    invoke-virtual {p2}, LX/CtG;->getPaddingTop()I

    move-result v0

    iput v0, p0, LX/CtF;->u:I

    .line 1944194
    invoke-virtual {p2}, LX/CtG;->getPaddingRight()I

    move-result v0

    iput v0, p0, LX/CtF;->v:I

    .line 1944195
    invoke-virtual {p2}, LX/CtG;->getPaddingBottom()I

    move-result v0

    iput v0, p0, LX/CtF;->w:I

    .line 1944196
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1944197
    invoke-virtual {p2}, LX/CtG;->getHighlightColor()I

    move-result v0

    iput v0, p0, LX/CtF;->b:I

    .line 1944198
    invoke-virtual {p2}, LX/CtG;->getShadowColor()I

    move-result v0

    iput v0, p0, LX/CtF;->i:I

    .line 1944199
    invoke-virtual {p2}, LX/CtG;->getShadowDx()F

    move-result v0

    iput v0, p0, LX/CtF;->j:F

    .line 1944200
    invoke-virtual {p2}, LX/CtG;->getShadowDy()F

    move-result v0

    iput v0, p0, LX/CtF;->k:F

    .line 1944201
    invoke-virtual {p2}, LX/CtG;->getShadowRadius()F

    move-result v0

    iput v0, p0, LX/CtF;->l:F

    .line 1944202
    :goto_0
    invoke-virtual {p2}, LX/CtG;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v0

    iput-object v0, p0, LX/CtF;->m:Landroid/text/method/TransformationMethod;

    .line 1944203
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-le v0, v1, :cond_1

    .line 1944204
    invoke-virtual {p2}, LX/CtG;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->isElegantTextHeight()Z

    move-result v0

    iput-boolean v0, p0, LX/CtF;->n:Z

    .line 1944205
    invoke-virtual {p2}, LX/CtG;->getLetterSpacing()F

    move-result v0

    iput v0, p0, LX/CtF;->o:F

    .line 1944206
    invoke-virtual {p2}, LX/CtG;->getFontFeatureSettings()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CtF;->p:Ljava/lang/String;

    .line 1944207
    :goto_1
    return-void

    .line 1944208
    :cond_0
    iput v3, p0, LX/CtF;->b:I

    .line 1944209
    iput v3, p0, LX/CtF;->i:I

    .line 1944210
    iput v2, p0, LX/CtF;->j:F

    .line 1944211
    iput v2, p0, LX/CtF;->k:F

    .line 1944212
    iput v2, p0, LX/CtF;->l:F

    goto :goto_0

    .line 1944213
    :cond_1
    iput-boolean v3, p0, LX/CtF;->n:Z

    .line 1944214
    iput v2, p0, LX/CtF;->o:F

    .line 1944215
    const/4 v0, 0x0

    iput-object v0, p0, LX/CtF;->p:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/CtG;)V
    .locals 5

    .prologue
    .line 1944216
    iget v0, p0, LX/CtF;->b:I

    invoke-virtual {p1, v0}, LX/CtG;->setHighlightColor(I)V

    .line 1944217
    iget-object v0, p0, LX/CtF;->c:Landroid/content/res/ColorStateList;

    invoke-virtual {p1, v0}, LX/CtG;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1944218
    const/4 v0, 0x0

    iget v1, p0, LX/CtF;->d:F

    invoke-virtual {p1, v0, v1}, LX/CtG;->setTextSize(IF)V

    .line 1944219
    iget-wide v0, p0, LX/CtF;->e:D

    .line 1944220
    iput-wide v0, p1, LX/CtG;->e:D

    .line 1944221
    iget-object v0, p0, LX/CtF;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {p1, v0}, LX/CtG;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 1944222
    iget-object v0, p0, LX/CtF;->g:Landroid/content/res/ColorStateList;

    invoke-virtual {p1, v0}, LX/CtG;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 1944223
    iget-object v0, p0, LX/CtF;->h:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, LX/CtG;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1944224
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1944225
    iget v0, p0, LX/CtF;->i:I

    if-eqz v0, :cond_0

    .line 1944226
    iget v0, p0, LX/CtF;->l:F

    iget v1, p0, LX/CtF;->j:F

    iget v2, p0, LX/CtF;->k:F

    iget v3, p0, LX/CtF;->i:I

    invoke-virtual {p1, v0, v1, v2, v3}, LX/CtG;->setShadowLayer(FFFI)V

    .line 1944227
    :cond_0
    iget-object v0, p0, LX/CtF;->m:Landroid/text/method/TransformationMethod;

    invoke-virtual {p1, v0}, LX/CtG;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1944228
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-le v0, v1, :cond_1

    .line 1944229
    iget-boolean v0, p0, LX/CtF;->n:Z

    invoke-virtual {p1, v0}, LX/CtG;->setElegantTextHeight(Z)V

    .line 1944230
    iget v0, p0, LX/CtF;->o:F

    invoke-virtual {p1, v0}, LX/CtG;->setLetterSpacing(F)V

    .line 1944231
    iget-object v0, p0, LX/CtF;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/CtG;->setFontFeatureSettings(Ljava/lang/String;)V

    .line 1944232
    :cond_1
    iget v0, p0, LX/CtF;->q:I

    invoke-virtual {p1, v0}, LX/CtG;->a(I)V

    .line 1944233
    iget-wide v0, p0, LX/CtF;->r:D

    .line 1944234
    iput-wide v0, p1, LX/CtG;->f:D

    .line 1944235
    iget-object v0, p0, LX/CtF;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, LX/CtG;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1944236
    iget v0, p0, LX/CtF;->t:I

    iget v1, p0, LX/CtF;->u:I

    iget v2, p0, LX/CtF;->v:I

    iget v3, p0, LX/CtF;->w:I

    invoke-virtual {p1, v0, v1, v2, v3}, LX/CtG;->setPadding(IIII)V

    .line 1944237
    return-void
.end method
