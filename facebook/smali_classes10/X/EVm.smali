.class public final LX/EVm;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/EVo;


# direct methods
.method public constructor <init>(LX/EVo;)V
    .locals 0

    .prologue
    .line 2128954
    iput-object p1, p0, LX/EVm;->a:LX/EVo;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 11

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 2128957
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v10, v0

    .line 2128958
    float-to-double v0, v10

    const-wide/16 v2, 0x0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    move-wide v8, v4

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 2128959
    iget-object v1, p0, LX/EVm;->a:LX/EVo;

    iget-object v1, v1, LX/EVo;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 2128960
    iget-object v1, p0, LX/EVm;->a:LX/EVo;

    iget-object v1, v1, LX/EVo;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 2128961
    iget-object v0, p0, LX/EVm;->a:LX/EVo;

    invoke-static {v0}, LX/EVo;->h(LX/EVo;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EVm;->a:LX/EVo;

    invoke-static {v0}, LX/EVo;->k(LX/EVo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2128962
    :cond_0
    iget-object v0, p0, LX/EVm;->a:LX/EVo;

    iget-object v0, v0, LX/EVo;->e:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setAlpha(F)V

    .line 2128963
    :cond_1
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 2128964
    iget-object v0, p0, LX/EVm;->a:LX/EVo;

    invoke-static {v0}, LX/EVo;->k(LX/EVo;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2128965
    iget-object v0, p0, LX/EVm;->a:LX/EVo;

    sget-object v1, LX/EVn;->HIDDEN:LX/EVn;

    .line 2128966
    iput-object v1, v0, LX/EVo;->h:LX/EVn;

    .line 2128967
    :cond_0
    :goto_0
    return-void

    .line 2128968
    :cond_1
    iget-object v0, p0, LX/EVm;->a:LX/EVo;

    invoke-static {v0}, LX/EVo;->h(LX/EVo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2128969
    iget-object v0, p0, LX/EVm;->a:LX/EVo;

    sget-object v1, LX/EVn;->SHOWN:LX/EVn;

    .line 2128970
    iput-object v1, v0, LX/EVo;->h:LX/EVn;

    .line 2128971
    goto :goto_0
.end method

.method public final c(LX/0wd;)V
    .locals 2

    .prologue
    .line 2128955
    iget-object v0, p0, LX/EVm;->a:LX/EVo;

    iget-object v0, v0, LX/EVo;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2128956
    return-void
.end method
