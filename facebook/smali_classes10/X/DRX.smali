.class public LX/DRX;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Lcom/facebook/resources/ui/FbTextView;

.field public final d:Lcom/facebook/resources/ui/FbTextView;

.field public final e:Landroid/widget/ToggleButton;

.field public final f:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1998339
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DRX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1998340
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1998341
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/DRX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1998342
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 1998343
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1998344
    const-class v0, LX/DRX;

    invoke-static {v0, p0}, LX/DRX;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1998345
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030856

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1998346
    invoke-virtual {p0, v2}, LX/DRX;->setOrientation(I)V

    .line 1998347
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/DRX;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1998348
    const v0, 0x7f0d15c0

    invoke-virtual {p0, v0}, LX/DRX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DRX;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1998349
    const v0, 0x7f0d15c1

    invoke-virtual {p0, v0}, LX/DRX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DRX;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1998350
    const v0, 0x7f0d15bf

    invoke-virtual {p0, v0}, LX/DRX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, LX/DRX;->e:Landroid/widget/ToggleButton;

    .line 1998351
    const v0, 0x7f0d15c2

    invoke-virtual {p0, v0}, LX/DRX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/DRX;->f:Landroid/widget/ProgressBar;

    .line 1998352
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/DRX;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0xc

    invoke-static {v2, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    iput-object v1, p1, LX/DRX;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v2, p1, LX/DRX;->b:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)LX/DRX;
    .locals 6

    .prologue
    .line 1998353
    new-instance v0, LX/DRW;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/DRW;-><init>(LX/DRX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, LX/DRX;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1998354
    return-object p0
.end method
