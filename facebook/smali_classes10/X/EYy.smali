.class public LX/EYy;
.super LX/EWc;
.source ""


# instance fields
.field public final c:[B

.field private d:I


# direct methods
.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 2138329
    invoke-direct {p0}, LX/EWc;-><init>()V

    .line 2138330
    const/4 v0, 0x0

    iput v0, p0, LX/EYy;->d:I

    .line 2138331
    iput-object p1, p0, LX/EYy;->c:[B

    .line 2138332
    return-void
.end method


# virtual methods
.method public final a(I)B
    .locals 1

    .prologue
    .line 2138328
    iget-object v0, p0, LX/EYy;->c:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public final a(III)I
    .locals 7

    .prologue
    .line 2138295
    const/4 v0, 0x0

    move v0, v0

    .line 2138296
    add-int/2addr v0, p2

    .line 2138297
    iget-object v1, p0, LX/EYy;->c:[B

    add-int v2, v0, p3

    const/16 v6, -0x20

    const/16 p0, -0x60

    const/4 v4, -0x1

    const/16 p3, -0x41

    .line 2138298
    if-eqz p1, :cond_e

    .line 2138299
    if-lt v0, v2, :cond_0

    .line 2138300
    :goto_0
    move v0, p1

    .line 2138301
    return v0

    .line 2138302
    :cond_0
    int-to-byte p2, p1

    .line 2138303
    if-ge p2, v6, :cond_2

    .line 2138304
    const/16 v3, -0x3e

    if-lt p2, v3, :cond_1

    add-int/lit8 v3, v0, 0x1

    aget-byte v5, v1, v0

    if-le v5, p3, :cond_d

    :cond_1
    move p1, v4

    .line 2138305
    goto :goto_0

    .line 2138306
    :cond_2
    const/16 v3, -0x10

    if-ge p2, v3, :cond_8

    .line 2138307
    shr-int/lit8 v3, p1, 0x8

    xor-int/lit8 v3, v3, -0x1

    int-to-byte v3, v3

    .line 2138308
    if-nez v3, :cond_3

    .line 2138309
    add-int/lit8 v5, v0, 0x1

    aget-byte v3, v1, v0

    .line 2138310
    if-lt v5, v2, :cond_4

    .line 2138311
    invoke-static {p2, v3}, LX/EZU;->a(II)I

    move-result p1

    goto :goto_0

    :cond_3
    move v5, v0

    .line 2138312
    :cond_4
    if-gt v3, p3, :cond_7

    if-ne p2, v6, :cond_5

    if-lt v3, p0, :cond_7

    :cond_5
    const/16 v6, -0x13

    if-ne p2, v6, :cond_6

    if-ge v3, p0, :cond_7

    :cond_6
    add-int/lit8 v0, v5, 0x1

    aget-byte v3, v1, v5

    if-le v3, p3, :cond_e

    :cond_7
    move p1, v4

    .line 2138313
    goto :goto_0

    .line 2138314
    :cond_8
    shr-int/lit8 v3, p1, 0x8

    xor-int/lit8 v3, v3, -0x1

    int-to-byte v5, v3

    .line 2138315
    const/4 v3, 0x0

    .line 2138316
    if-nez v5, :cond_9

    .line 2138317
    add-int/lit8 v6, v0, 0x1

    aget-byte v5, v1, v0

    .line 2138318
    if-lt v6, v2, :cond_f

    .line 2138319
    invoke-static {p2, v5}, LX/EZU;->a(II)I

    move-result p1

    goto :goto_0

    .line 2138320
    :cond_9
    shr-int/lit8 v3, p1, 0x10

    int-to-byte v3, v3

    move p0, v5

    move v6, v0

    .line 2138321
    :goto_1
    if-nez v3, :cond_a

    .line 2138322
    add-int/lit8 v5, v6, 0x1

    aget-byte v3, v1, v6

    .line 2138323
    if-lt v5, v2, :cond_b

    .line 2138324
    invoke-static {p2, p0, v3}, LX/EZU;->a(III)I

    move-result p1

    goto :goto_0

    :cond_a
    move v5, v6

    .line 2138325
    :cond_b
    if-gt p0, p3, :cond_c

    shl-int/lit8 v6, p2, 0x1c

    add-int/lit8 p0, p0, 0x70

    add-int/2addr v6, p0

    shr-int/lit8 v6, v6, 0x1e

    if-nez v6, :cond_c

    if-gt v3, p3, :cond_c

    add-int/lit8 v0, v5, 0x1

    aget-byte v3, v1, v5

    if-le v3, p3, :cond_e

    :cond_c
    move p1, v4

    .line 2138326
    goto :goto_0

    :cond_d
    move v0, v3

    .line 2138327
    :cond_e
    invoke-static {v1, v0, v2}, LX/EZU;->b([BII)I

    move-result p1

    goto :goto_0

    :cond_f
    move p0, v5

    goto :goto_1
.end method

.method public final a()LX/EWa;
    .locals 2

    .prologue
    .line 2138294
    new-instance v0, LX/EYx;

    invoke-direct {v0, p0}, LX/EYx;-><init>(LX/EYy;)V

    return-object v0
.end method

.method public final a(LX/EYy;II)Z
    .locals 7

    .prologue
    .line 2138247
    invoke-virtual {p1}, LX/EYy;->b()I

    move-result v0

    if-le p3, v0, :cond_0

    .line 2138248
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Length too large: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/EYy;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2138249
    :cond_0
    add-int v0, p2, p3

    invoke-virtual {p1}, LX/EYy;->b()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 2138250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ran off end of other: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LX/EYy;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2138251
    :cond_1
    iget-object v2, p0, LX/EYy;->c:[B

    .line 2138252
    iget-object v3, p1, LX/EYy;->c:[B

    .line 2138253
    const/4 v0, 0x0

    move v0, v0

    .line 2138254
    add-int v4, v0, p3

    .line 2138255
    const/4 v0, 0x0

    move v1, v0

    .line 2138256
    const/4 v0, 0x0

    move v0, v0

    .line 2138257
    add-int/2addr v0, p2

    .line 2138258
    :goto_0
    if-ge v1, v4, :cond_3

    .line 2138259
    aget-byte v5, v2, v1

    aget-byte v6, v3, v0

    if-eq v5, v6, :cond_2

    .line 2138260
    const/4 v0, 0x0

    .line 2138261
    :goto_1
    return v0

    .line 2138262
    :cond_2
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2138263
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2138293
    iget-object v0, p0, LX/EYy;->c:[B

    array-length v0, v0

    return v0
.end method

.method public final b(III)I
    .locals 5

    .prologue
    .line 2138287
    iget-object v1, p0, LX/EYy;->c:[B

    .line 2138288
    const/4 v0, 0x0

    move v0, v0

    .line 2138289
    add-int/2addr v0, p2

    add-int v2, v0, p3

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2138290
    mul-int/lit8 v3, p1, 0x1f

    aget-byte v4, v1, v0

    add-int p1, v3, v4

    .line 2138291
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2138292
    :cond_0
    return p1
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2138284
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/EYy;->c:[B

    .line 2138285
    const/4 v2, 0x0

    move v2, v2

    .line 2138286
    invoke-virtual {p0}, LX/EYy;->b()I

    move-result v3

    invoke-direct {v0, v1, v2, v3, p1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    return-object v0
.end method

.method public final b([BIII)V
    .locals 1

    .prologue
    .line 2138333
    iget-object v0, p0, LX/EYy;->c:[B

    invoke-static {v0, p2, p1, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2138334
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2138271
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 2138272
    :goto_0
    return v0

    .line 2138273
    :cond_0
    instance-of v0, p1, LX/EWc;

    if-nez v0, :cond_1

    move v0, v2

    .line 2138274
    goto :goto_0

    .line 2138275
    :cond_1
    invoke-virtual {p0}, LX/EYy;->b()I

    move-result v3

    move-object v0, p1

    check-cast v0, LX/EWc;

    invoke-virtual {v0}, LX/EWc;->b()I

    move-result v0

    if-eq v3, v0, :cond_2

    move v0, v2

    .line 2138276
    goto :goto_0

    .line 2138277
    :cond_2
    invoke-virtual {p0}, LX/EYy;->b()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 2138278
    goto :goto_0

    .line 2138279
    :cond_3
    instance-of v0, p1, LX/EYy;

    if-eqz v0, :cond_4

    .line 2138280
    check-cast p1, LX/EYy;

    invoke-virtual {p0}, LX/EYy;->b()I

    move-result v0

    invoke-virtual {p0, p1, v2, v0}, LX/EYy;->a(LX/EYy;II)Z

    move-result v0

    goto :goto_0

    .line 2138281
    :cond_4
    instance-of v0, p1, LX/EZ6;

    if-eqz v0, :cond_5

    .line 2138282
    invoke-virtual {p1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 2138283
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Has a new type of ByteString been created? Found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 2138267
    const/4 v0, 0x0

    move v0, v0

    .line 2138268
    iget-object v1, p0, LX/EYy;->c:[B

    invoke-virtual {p0}, LX/EYy;->b()I

    move-result v2

    add-int/2addr v2, v0

    .line 2138269
    invoke-static {v1, v0, v2}, LX/EZU;->b([BII)I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2138270
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 2138264
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, LX/EYy;->c:[B

    .line 2138265
    const/4 v2, 0x0

    move v2, v2

    .line 2138266
    invoke-virtual {p0}, LX/EYy;->b()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    return-object v0
.end method

.method public final h()LX/EWd;
    .locals 3

    .prologue
    .line 2138244
    iget-object v0, p0, LX/EYy;->c:[B

    .line 2138245
    const/4 v1, 0x0

    move v1, v1

    .line 2138246
    invoke-virtual {p0}, LX/EYy;->b()I

    move-result v2

    invoke-static {v0, v1, v2}, LX/EWd;->a([BII)LX/EWd;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2138236
    iget v0, p0, LX/EYy;->d:I

    .line 2138237
    if-nez v0, :cond_1

    .line 2138238
    invoke-virtual {p0}, LX/EYy;->b()I

    move-result v0

    .line 2138239
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v0}, LX/EYy;->b(III)I

    move-result v0

    .line 2138240
    if-nez v0, :cond_0

    .line 2138241
    const/4 v0, 0x1

    .line 2138242
    :cond_0
    iput v0, p0, LX/EYy;->d:I

    .line 2138243
    :cond_1
    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 2138235
    iget v0, p0, LX/EYy;->d:I

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 2138234
    invoke-virtual {p0}, LX/EYy;->a()LX/EWa;

    move-result-object v0

    return-object v0
.end method
