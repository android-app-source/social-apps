.class public final LX/EGL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/EGe;


# direct methods
.method public constructor <init>(LX/EGe;)V
    .locals 0

    .prologue
    .line 2097141
    iput-object p1, p0, LX/EGL;->a:LX/EGe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x111c6e68

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2097142
    iget-object v1, p0, LX/EGL;->a:LX/EGe;

    const-string v2, "reason"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    .line 2097143
    if-eqz v2, :cond_1

    const-string v4, "homekey"

    invoke-virtual {v4, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2097144
    iget-object v4, v1, LX/EGe;->m:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v1, LX/EGe;->T:J

    .line 2097145
    invoke-static {v1}, LX/EGe;->aw(LX/EGe;)V

    .line 2097146
    invoke-static {v1, v6}, LX/EGe;->d(LX/EGe;Z)V

    .line 2097147
    invoke-static {v1}, LX/EGe;->ax(LX/EGe;)V

    .line 2097148
    invoke-static {v1}, LX/EGe;->W(LX/EGe;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2097149
    const/4 v4, 0x1

    invoke-static {v1, v4, v6}, LX/EGe;->b(LX/EGe;ZZ)V

    .line 2097150
    :cond_0
    iget-object v4, v1, LX/EGe;->j:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/facebook/rtc/services/BackgroundVideoCallService$7;

    invoke-direct {v5, v1}, Lcom/facebook/rtc/services/BackgroundVideoCallService$7;-><init>(LX/EGe;)V

    const-wide/16 v6, 0x3e8

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v6, v7, v8}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v4

    iput-object v4, v1, LX/EGe;->u:Ljava/util/concurrent/ScheduledFuture;

    .line 2097151
    :cond_1
    const/16 v1, 0x27

    const v2, 0x60d891e0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
