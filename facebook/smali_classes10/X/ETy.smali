.class public LX/ETy;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/7ze;
.implements LX/1Pf;
.implements LX/BwV;
.implements LX/02k;
.implements LX/1PW;
.implements LX/1Px;
.implements LX/3U7;
.implements LX/2kn;
.implements LX/7Lk;
.implements LX/ETX;
.implements LX/ETa;
.implements LX/ETb;
.implements LX/ETd;
.implements LX/ETe;
.implements LX/ETh;
.implements LX/ETi;
.implements LX/ETj;


# instance fields
.field private final A:LX/ETl;

.field private final B:LX/ETt;

.field private final C:LX/ETv;

.field private final D:LX/ETn;

.field private final E:LX/ETp;

.field private final F:LX/EU5;

.field public final G:LX/2xj;

.field public H:LX/0oB;

.field public I:J

.field private final n:LX/ETB;

.field private final o:LX/EUA;

.field private final p:LX/ESm;

.field private final q:LX/Bur;

.field public final r:LX/EUf;

.field public final s:LX/Bwd;

.field public final t:LX/ETk;

.field private final u:LX/ETc;

.field private final v:LX/ETf;

.field private final w:LX/ETs;

.field private final x:LX/1PT;

.field private final y:LX/2kt;

.field private final z:LX/ETX;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/1PY;LX/Bwd;LX/ETX;LX/ETQ;LX/ETB;LX/ETc;LX/ETr;LX/ETs;LX/EU7;LX/EU8;LX/2kq;LX/EUA;LX/ESm;LX/2xj;LX/BwE;LX/EUg;LX/ETm;LX/EU6;LX/ETq;LX/ETo;LX/ETu;LX/ETw;LX/0xX;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/Bwd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/ETX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/ETQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125597
    invoke-direct {p0, p1, p3, p5}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 2125598
    new-instance v1, LX/ETk;

    invoke-direct {v1}, LX/ETk;-><init>()V

    iput-object v1, p0, LX/ETy;->t:LX/ETk;

    .line 2125599
    iput-object p2, p0, LX/ETy;->x:LX/1PT;

    .line 2125600
    iput-object p9, p0, LX/ETy;->n:LX/ETB;

    .line 2125601
    iput-object p11, p0, LX/ETy;->v:LX/ETf;

    .line 2125602
    iput-object p12, p0, LX/ETy;->w:LX/ETs;

    .line 2125603
    iget-object v1, p0, LX/ETy;->w:LX/ETs;

    invoke-virtual {v1, p0}, LX/ETs;->a(LX/ETy;)V

    .line 2125604
    invoke-static {p4}, LX/2kq;->a(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/2kt;

    move-result-object v1

    iput-object v1, p0, LX/ETy;->y:LX/2kt;

    .line 2125605
    move-object/from16 v0, p16

    iput-object v0, p0, LX/ETy;->o:LX/EUA;

    .line 2125606
    move-object/from16 v0, p17

    iput-object v0, p0, LX/ETy;->p:LX/ESm;

    .line 2125607
    iput-object p6, p0, LX/ETy;->s:LX/Bwd;

    .line 2125608
    sget-object v1, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    const-string v2, "video_home"

    move-object/from16 v0, p19

    invoke-virtual {v0, p0, v1, v2}, LX/BwE;->a(LX/1Pf;LX/0wD;Ljava/lang/String;)LX/Bur;

    move-result-object v1

    iput-object v1, p0, LX/ETy;->q:LX/Bur;

    .line 2125609
    sget-object v1, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    const-string v2, "video_home"

    move-object/from16 v0, p20

    invoke-virtual {v0, p0, v1, v2}, LX/EUg;->a(LX/1Pf;LX/0wD;Ljava/lang/String;)LX/EUf;

    move-result-object v1

    iput-object v1, p0, LX/ETy;->r:LX/EUf;

    .line 2125610
    iget-object v1, p0, LX/ETy;->r:LX/EUf;

    invoke-virtual {v1, p0}, LX/EUf;->a(LX/7Lk;)V

    .line 2125611
    iput-object p7, p0, LX/ETy;->z:LX/ETX;

    .line 2125612
    iput-object p10, p0, LX/ETy;->u:LX/ETc;

    .line 2125613
    move-object/from16 v0, p21

    invoke-virtual {v0, p8}, LX/ETm;->a(LX/ETQ;)LX/ETl;

    move-result-object v1

    iput-object v1, p0, LX/ETy;->A:LX/ETl;

    .line 2125614
    invoke-static {p8}, LX/EU6;->a(LX/ETQ;)LX/EU5;

    move-result-object v1

    iput-object v1, p0, LX/ETy;->F:LX/EU5;

    .line 2125615
    move-object/from16 v0, p23

    invoke-virtual {v0, p8}, LX/ETq;->a(LX/ETQ;)LX/ETp;

    move-result-object v1

    iput-object v1, p0, LX/ETy;->E:LX/ETp;

    .line 2125616
    move-object/from16 v0, p25

    invoke-virtual {v0, p8}, LX/ETu;->a(LX/ETQ;)LX/ETt;

    move-result-object v1

    iput-object v1, p0, LX/ETy;->B:LX/ETt;

    .line 2125617
    invoke-static {p8}, LX/ETw;->a(LX/ETQ;)LX/ETv;

    move-result-object v1

    iput-object v1, p0, LX/ETy;->C:LX/ETv;

    .line 2125618
    move-object/from16 v0, p18

    iput-object v0, p0, LX/ETy;->G:LX/2xj;

    .line 2125619
    move-object/from16 v0, p13

    invoke-virtual {p11, v0}, LX/ETr;->a(LX/ETe;)V

    .line 2125620
    move-object/from16 v0, p14

    invoke-virtual {p11, v0}, LX/ETr;->a(LX/ETe;)V

    .line 2125621
    new-instance v1, LX/ETx;

    invoke-direct {v1, p0}, LX/ETx;-><init>(LX/ETy;)V

    iput-object v1, p0, LX/ETy;->H:LX/0oB;

    .line 2125622
    iget-object v1, p0, LX/ETy;->G:LX/2xj;

    iget-object v2, p0, LX/ETy;->H:LX/0oB;

    invoke-virtual {v1, v2}, LX/2xj;->a(LX/0oB;)V

    .line 2125623
    iget-object v1, p0, LX/ETy;->r:LX/EUf;

    invoke-virtual {v1, p0}, LX/EUf;->a(LX/7Lk;)V

    .line 2125624
    invoke-static {p8}, LX/ETo;->a(LX/ETQ;)LX/ETn;

    move-result-object v1

    iput-object v1, p0, LX/ETy;->D:LX/ETn;

    .line 2125625
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 1

    .prologue
    .line 2125626
    iget-object v0, p0, LX/ETy;->B:LX/ETt;

    invoke-virtual {v0, p1, p2}, LX/ETt;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)LX/EUB;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/EUB;"
        }
    .end annotation

    .prologue
    .line 2125627
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2125628
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2125629
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2125630
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    .line 2125631
    iget-object v2, p0, LX/ETy;->o:LX/EUA;

    .line 2125632
    new-instance v4, LX/EU9;

    const-class v3, LX/1VK;

    invoke-interface {v2, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1VK;

    invoke-direct {v4, p1, v1, p2, v3}, LX/EU9;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;Ljava/lang/String;LX/1VK;)V

    .line 2125633
    move-object v1, v4

    .line 2125634
    invoke-virtual {p0, v1, v0}, LX/1Qj;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EUB;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/ETZ;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125635
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125636
    iget-object v0, p0, LX/ETy;->A:LX/ETl;

    invoke-virtual {v0, p1, p2}, LX/ETl;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETZ;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    return-object v0
.end method

.method public final a(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 1

    .prologue
    .line 2125637
    iget-object v0, p0, LX/ETy;->v:LX/ETf;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/ETf;->a(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2125638
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2125639
    check-cast p1, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;

    .line 2125640
    iget-object v0, p0, LX/ETy;->p:LX/ESm;

    .line 2125641
    iget-object p0, v0, LX/ESm;->b:LX/1Aa;

    move-object v0, p0

    .line 2125642
    iget-boolean p0, v0, LX/1Aa;->n:Z

    if-eqz p0, :cond_0

    .line 2125643
    invoke-static {v0}, LX/1Aa;->g(LX/1Aa;)Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2125644
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;LX/2oV;)V
    .locals 1

    .prologue
    .line 2125645
    check-cast p1, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;

    .line 2125646
    iget-object v0, p0, LX/ETy;->p:LX/ESm;

    .line 2125647
    iget-object p0, v0, LX/ESm;->b:LX/1Aa;

    invoke-virtual {p0, p1, p2}, LX/1Aa;->a(Landroid/view/View;LX/2oV;)V

    .line 2125648
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 1

    .prologue
    .line 2125649
    iget-object v0, p0, LX/ETy;->v:LX/ETf;

    invoke-virtual {v0, p1}, LX/ETf;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2125650
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETO;)V
    .locals 1

    .prologue
    .line 2125651
    iget-object v0, p0, LX/ETy;->z:LX/ETX;

    invoke-interface {v0, p1, p2}, LX/ETX;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETO;)V

    .line 2125652
    return-void
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 1

    .prologue
    .line 2125574
    iget-object v0, p0, LX/ETy;->n:LX/ETB;

    check-cast p2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v0, p2, p1}, LX/ETB;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;)V

    .line 2125575
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2125653
    iget-object v0, p0, LX/ETy;->u:LX/ETc;

    invoke-virtual {v0, p1}, LX/ETc;->a(Ljava/lang/Object;)V

    .line 2125654
    return-void
.end method

.method public final a(Ljava/lang/Object;LX/ESz;)Z
    .locals 1

    .prologue
    .line 2125655
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125656
    iget-object v0, p0, LX/ETy;->D:LX/ETn;

    invoke-virtual {v0, p1, p2}, LX/ETn;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ESz;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2125593
    iget-object v0, p0, LX/ETy;->E:LX/ETp;

    invoke-virtual {v0}, LX/ETp;->b()V

    .line 2125594
    return-void
.end method

.method public final b(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 1

    .prologue
    .line 2125595
    iget-object v0, p0, LX/ETy;->v:LX/ETf;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/ETf;->b(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2125596
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2125568
    check-cast p1, Lcom/facebook/video/videohome/views/VideoHomeVideoPlayerView;

    .line 2125569
    iget-object v0, p0, LX/ETy;->p:LX/ESm;

    .line 2125570
    iget-object p0, v0, LX/ESm;->b:LX/1Aa;

    move-object v0, p0

    .line 2125571
    iget-boolean p0, v0, LX/1Aa;->n:Z

    if-eqz p0, :cond_0

    .line 2125572
    invoke-static {v0}, LX/1Aa;->g(LX/1Aa;)Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2125573
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2125576
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125577
    iget-object v0, p0, LX/ETy;->D:LX/ETn;

    invoke-virtual {v0, p1}, LX/ETn;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    return v0
.end method

.method public final b_(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2125578
    iget-object v0, p0, LX/ETy;->B:LX/ETt;

    invoke-virtual {v0, p1}, LX/ETt;->b_(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2125579
    iget-object v0, p0, LX/ETy;->x:LX/1PT;

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2125580
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125581
    iget-object v0, p0, LX/ETy;->E:LX/ETp;

    invoke-virtual {v0, p1}, LX/ETp;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2125582
    iget-object v0, p0, LX/ETy;->E:LX/ETp;

    invoke-virtual {v0}, LX/ETp;->d()Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2125583
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125584
    iget-object v0, p0, LX/ETy;->E:LX/ETp;

    invoke-virtual {v0, p1}, LX/ETp;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    return v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 2125585
    iget-object v0, p0, LX/ETy;->q:LX/Bur;

    return-object v0
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125586
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125587
    iget-object v0, p0, LX/ETy;->F:LX/EU5;

    invoke-virtual {v0, p1}, LX/EU5;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125588
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125589
    iget-object v0, p0, LX/ETy;->F:LX/EU5;

    invoke-virtual {v0, p1}, LX/EU5;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    return-object v0
.end method

.method public final kM_()LX/Bwd;
    .locals 1

    .prologue
    .line 2125590
    iget-object v0, p0, LX/ETy;->s:LX/Bwd;

    return-object v0
.end method

.method public final n()LX/1SX;
    .locals 1

    .prologue
    .line 2125591
    invoke-virtual {p0}, LX/ETy;->e()LX/1SX;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125592
    iget-object v0, p0, LX/ETy;->y:LX/2kt;

    invoke-virtual {v0}, LX/2kt;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v0

    return-object v0
.end method
