.class public final LX/DNl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TD;

.field public final synthetic b:LX/B1W;

.field public final synthetic c:LX/9M3;

.field public final synthetic d:LX/0tX;

.field public final synthetic e:LX/DNp;


# direct methods
.method public constructor <init>(LX/DNp;LX/0TD;LX/B1W;LX/9M3;LX/0tX;)V
    .locals 0

    .prologue
    .line 1991956
    iput-object p1, p0, LX/DNl;->e:LX/DNp;

    iput-object p2, p0, LX/DNl;->a:LX/0TD;

    iput-object p3, p0, LX/DNl;->b:LX/B1W;

    iput-object p4, p0, LX/DNl;->c:LX/9M3;

    iput-object p5, p0, LX/DNl;->d:LX/0tX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 1991933
    check-cast p1, LX/0Px;

    .line 1991934
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991935
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1991936
    new-instance v1, LX/DNk;

    invoke-direct {v1, p0}, LX/DNk;-><init>(LX/DNl;)V

    iget-object v2, p0, LX/DNl;->a:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1991937
    :goto_0
    return-object v0

    .line 1991938
    :cond_0
    new-instance v1, LX/0v6;

    const-string v0, "GroupsFeedConsistency"

    invoke-direct {v1, v0}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 1991939
    iget-object v0, p0, LX/DNl;->b:LX/B1W;

    invoke-interface {v0}, LX/B1W;->a()LX/0zO;

    move-result-object v0

    .line 1991940
    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 1991941
    invoke-virtual {v1, v0}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1991942
    iget-object v2, p0, LX/DNl;->b:LX/B1W;

    invoke-interface {v2}, LX/B1W;->b()LX/0TF;

    move-result-object v2

    iget-object v3, p0, LX/DNl;->a:LX/0TD;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1991943
    iget-object v2, p0, LX/DNl;->c:LX/9M3;

    .line 1991944
    new-instance v3, LX/9M0;

    invoke-direct {v3}, LX/9M0;-><init>()V

    .line 1991945
    const-string v4, "story_ids"

    invoke-virtual {v3, v4, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1991946
    move-object v3, v3

    .line 1991947
    const-class v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v4

    invoke-static {v3, v4}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v3

    .line 1991948
    const/4 v4, 0x1

    .line 1991949
    iput-boolean v4, v3, LX/0zO;->p:Z

    .line 1991950
    iget-object v4, v2, LX/9M3;->a:LX/0sZ;

    invoke-virtual {v4, v3}, LX/0sZ;->a(LX/0zO;)LX/1kt;

    move-result-object v4

    .line 1991951
    new-instance p1, LX/9M2;

    invoke-direct {p1, v2, v4}, LX/9M2;-><init>(LX/9M3;LX/1kt;)V

    move-object v4, p1

    .line 1991952
    iput-object v4, v3, LX/0zO;->g:LX/1kt;

    .line 1991953
    move-object v2, v3

    .line 1991954
    invoke-virtual {v1, v2}, LX/0v6;->a(LX/0zO;)LX/0zX;

    .line 1991955
    iget-object v2, p0, LX/DNl;->d:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0v6;)V

    goto :goto_0
.end method
