.class public LX/EPI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2116385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2116386
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116387
    iput-object v0, p0, LX/EPI;->a:LX/0Ot;

    .line 2116388
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116389
    iput-object v0, p0, LX/EPI;->b:LX/0Ot;

    .line 2116390
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116391
    iput-object v0, p0, LX/EPI;->c:LX/0Ot;

    .line 2116392
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116393
    iput-object v0, p0, LX/EPI;->d:LX/0Ot;

    .line 2116394
    return-void
.end method

.method public static a$redex0(LX/EPI;LX/1Ps;LX/EPH;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Ps;",
            ":",
            "LX/1Pn;",
            ":",
            "LX/CxV;",
            ":",
            "LX/Cxa;",
            ">(TE;",
            "LX/EPH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2116400
    iget-object v0, p0, LX/EPI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    move-object v1, p1

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v2

    iget v3, p2, LX/EPH;->j:I

    iget-object v4, p2, LX/EPH;->e:LX/CvV;

    iget-object v1, p0, LX/EPI;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v1, p1

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    iget-object v5, p2, LX/EPH;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v6, p2, LX/EPH;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v7, p2, LX/EPH;->f:Ljava/lang/String;

    iget v8, p2, LX/EPH;->j:I

    invoke-static {v1, v5, v6, v7, v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v4, v1}, LX/CvY;->c(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2116401
    iget-object v0, p0, LX/EPI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nD;

    iget-object v1, p2, LX/EPH;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    iget-object v2, p2, LX/EPH;->g:Ljava/lang/String;

    iget-object v3, p2, LX/EPH;->h:Ljava/lang/String;

    move-object v4, p1

    check-cast v4, LX/CxV;

    invoke-interface {v4}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v4

    invoke-interface {v4}, LX/8ef;->m()Ljava/lang/String;

    move-result-object v4

    move-object v5, p1

    check-cast v5, LX/CxV;

    invoke-static {v5}, LX/EPK;->a(LX/CxV;)LX/8ci;

    move-result-object v5

    iget-object v6, p2, LX/EPH;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v7, p2, LX/EPH;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v8, p1

    check-cast v8, LX/CxV;

    invoke-interface {v8}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    .line 2116402
    iget-object v9, v8, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v8, v9

    .line 2116403
    iget-object v9, p2, LX/EPH;->d:LX/0Px;

    .line 2116404
    sget-object v10, LX/0Q7;->a:LX/0Px;

    move-object v10, v10

    .line 2116405
    const/4 v11, 0x0

    invoke-virtual/range {v0 .. v11}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/0Px;LX/0Px;Z)Landroid/content/Intent;

    move-result-object v1

    .line 2116406
    invoke-static {v1}, LX/1nD;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2116407
    check-cast v0, LX/Cxa;

    invoke-interface {v0}, LX/Cxa;->u()V

    .line 2116408
    :cond_0
    iget-object v0, p0, LX/EPI;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    check-cast p1, LX/1Pn;

    invoke-interface {p1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2116409
    return-void
.end method

.method public static b(LX/0QB;)LX/EPI;
    .locals 5

    .prologue
    .line 2116396
    new-instance v0, LX/EPI;

    invoke-direct {v0}, LX/EPI;-><init>()V

    .line 2116397
    const/16 v1, 0x32d4

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1140

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x455

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x348e

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 2116398
    iput-object v1, v0, LX/EPI;->a:LX/0Ot;

    iput-object v2, v0, LX/EPI;->b:LX/0Ot;

    iput-object v3, v0, LX/EPI;->c:LX/0Ot;

    iput-object v4, v0, LX/EPI;->d:LX/0Ot;

    .line 2116399
    return-object v0
.end method


# virtual methods
.method public final a(LX/CzL;LX/Cxi;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/Cxi;",
            ":",
            "LX/1Ps;",
            ":",
            "LX/1Pn;",
            ":",
            "LX/CxV;",
            ":",
            "LX/CxP;",
            ":",
            "LX/Cxa;",
            ">(",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsSeeMorePivotModuleInterfaces$SearchResultsSeeMorePivotModule;",
            ">;TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2116395
    iget-object v0, p0, LX/EPI;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPK;

    invoke-virtual {v0, p1, p2}, LX/EPK;->a(LX/CzL;LX/Cxi;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method
