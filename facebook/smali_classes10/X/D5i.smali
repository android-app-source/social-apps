.class public final LX/D5i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 0

    .prologue
    .line 1963801
    iput-object p1, p0, LX/D5i;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1963802
    check-cast p1, Ljava/lang/String;

    .line 1963803
    iget-object v0, p0, LX/D5i;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    const/4 v3, 0x0

    .line 1963804
    invoke-static {v0, p1}, LX/D6S;->e(LX/D6S;Ljava/lang/String;)I

    move-result v1

    .line 1963805
    if-gez v1, :cond_2

    move-object v1, v3

    .line 1963806
    :goto_0
    move-object v0, v1

    .line 1963807
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/182;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v1, p0

    .line 1963808
    :cond_2
    if-lez v1, :cond_3

    .line 1963809
    add-int/lit8 p0, v1, -0x1

    .line 1963810
    iget-object v1, v0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963811
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1963812
    check-cast v2, Lcom/facebook/graphql/model/FeedUnit;

    .line 1963813
    invoke-static {v2}, LX/D6S;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1963814
    invoke-static {v1}, LX/D6S;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v1, v3

    .line 1963815
    goto :goto_0
.end method
