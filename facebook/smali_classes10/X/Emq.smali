.class public LX/Emq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Emj;


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/2h7;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/lang/String;

.field public final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Emj;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hd;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/String;

.field private final l:LX/Jxq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0am;LX/Jxq;)V
    .locals 5
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/Jxq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2hd;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "LX/2h7;",
            ">;",
            "Lcom/facebook/entitycards/analytics/EntityCardAnalyticsExtraProvider;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2166289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166290
    invoke-static {p6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2166291
    invoke-static {p7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2166292
    iput-object p1, p0, LX/Emq;->a:LX/0Zb;

    .line 2166293
    iput-object p3, p0, LX/Emq;->b:LX/0Ot;

    .line 2166294
    iput-object p6, p0, LX/Emq;->c:Ljava/lang/String;

    .line 2166295
    iput-object p7, p0, LX/Emq;->d:Ljava/lang/String;

    .line 2166296
    iput-object p8, p0, LX/Emq;->e:Ljava/lang/String;

    .line 2166297
    iput-object p9, p0, LX/Emq;->f:LX/0am;

    .line 2166298
    const-string v0, "_"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "entity_cards"

    aput-object v4, v3, v2

    iget-object v2, p0, LX/Emq;->d:Ljava/lang/String;

    aput-object v2, v3, v1

    invoke-static {v0, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Emq;->g:Ljava/lang/String;

    .line 2166299
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    .line 2166300
    iput-object p4, p0, LX/Emq;->i:LX/0Ot;

    .line 2166301
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Emq;->j:Ljava/util/Set;

    .line 2166302
    invoke-interface {p2}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Emq;->k:Ljava/lang/String;

    .line 2166303
    iput-object p10, p0, LX/Emq;->l:LX/Jxq;

    .line 2166304
    invoke-static {p0}, LX/Emq;->e(LX/Emq;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p9}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2166305
    invoke-interface {p5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "entity_cards_missing_friending_location"

    const-string v2, "PYMK cards should have a present friending location"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2166306
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 2166307
    goto :goto_0

    :cond_2
    move v0, v2

    .line 2166308
    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0P1;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2166283
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 2166284
    const-string v1, "instance_id"

    invoke-virtual {v0, v1, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2166285
    const-string v1, "surface"

    invoke-virtual {v0, v1, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2166286
    const-string v1, "surface_source_id"

    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {v0, v1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2166287
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    .line 2166288
    :cond_0
    const-string p2, ""

    goto :goto_0
.end method

.method public static a(LX/Emq;LX/Emn;)LX/0oG;
    .locals 3

    .prologue
    .line 2166275
    iget-object v0, p0, LX/Emq;->a:LX/0Zb;

    iget-object v1, p1, LX/Emn;->name:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2166276
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2166277
    const-string v1, "entity_cards"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2166278
    iget-object v1, p0, LX/Emq;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 2166279
    const-string v1, "instance_id"

    iget-object v2, p0, LX/Emq;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166280
    const-string v1, "surface"

    iget-object v2, p0, LX/Emq;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166281
    const-string v1, "surface_source_id"

    iget-object v2, p0, LX/Emq;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166282
    :cond_0
    return-object v0
.end method

.method public static e(LX/Emq;)Z
    .locals 2

    .prologue
    .line 2166274
    iget-object v0, p0, LX/Emq;->e:Ljava/lang/String;

    const-string v1, "pymk"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2166202
    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emj;

    .line 2166203
    invoke-interface {v0}, LX/Emj;->a()V

    .line 2166204
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2166205
    :cond_0
    return-void
.end method

.method public final a(LX/Emj;)V
    .locals 1

    .prologue
    .line 2166272
    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2166273
    return-void
.end method

.method public final a(LX/Emn;LX/0am;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Emn;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2166266
    invoke-static {p0, p1}, LX/Emq;->a(LX/Emq;LX/Emn;)LX/0oG;

    move-result-object v1

    .line 2166267
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2166268
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2166269
    const-string v2, "identifier"

    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166270
    :cond_0
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2166271
    :cond_1
    return-void
.end method

.method public final a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Emo;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2166247
    sget-object v0, LX/Emo;->ACTION_BAR:LX/Emo;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/Emo;->CONTEXT_ITEM:LX/Emo;

    if-ne p1, v0, :cond_1

    .line 2166248
    :cond_0
    invoke-virtual {p3}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2166249
    invoke-virtual {p4}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2166250
    :cond_1
    sget-object v0, LX/Emn;->TAP:LX/Emn;

    invoke-static {p0, v0}, LX/Emq;->a(LX/Emq;LX/Emn;)LX/0oG;

    move-result-object v1

    .line 2166251
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2166252
    const-string v0, "identifier"

    invoke-virtual {v1, v0, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166253
    const-string v0, "tap_action_surface"

    iget-object v2, p1, LX/Emo;->name:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166254
    invoke-virtual {p3}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2166255
    const-string v2, "target_type"

    invoke-virtual {p3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166256
    :cond_2
    invoke-virtual {p4}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2166257
    const-string v0, "position"

    invoke-virtual {p4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166258
    :cond_3
    iget-object v0, p0, LX/Emq;->l:LX/Jxq;

    if-eqz v0, :cond_4

    .line 2166259
    iget-object v0, p0, LX/Emq;->l:LX/Jxq;

    .line 2166260
    const-string v2, "entity_card_click"

    invoke-static {v0, p5, v1, v2}, LX/Jxq;->a(LX/Jxq;Ljava/lang/Object;LX/0oG;Ljava/lang/String;)V

    .line 2166261
    :cond_4
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2166262
    :cond_5
    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_6

    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emj;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 2166263
    invoke-interface/range {v0 .. v5}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2166264
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2166265
    :cond_6
    return-void
.end method

.method public final a(LX/EnB;)V
    .locals 3

    .prologue
    .line 2166243
    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emj;

    .line 2166244
    invoke-interface {v0, p1}, LX/Emj;->a(LX/EnB;)V

    .line 2166245
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2166246
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2166237
    iget-object v0, p0, LX/Emq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_entity_card"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2166238
    sget-object v0, LX/Emn;->ENTITY_CARDS_NAVIGATED_TO_ENTITY:LX/Emn;

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Emq;->a(LX/Emn;LX/0am;)V

    .line 2166239
    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emj;

    .line 2166240
    invoke-interface {v0, p1}, LX/Emj;->a(Ljava/lang/String;)V

    .line 2166241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2166242
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2166233
    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emj;

    .line 2166234
    invoke-interface {v0, p1, p2, p3}, LX/Emj;->a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V

    .line 2166235
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2166236
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;D)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2166214
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2166215
    sget-object v0, LX/Emn;->ENTITY_CARDS_IMPRESSION:LX/Emn;

    invoke-static {p0, v0}, LX/Emq;->a(LX/Emq;LX/Emn;)LX/0oG;

    move-result-object v0

    .line 2166216
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2166217
    const-string v1, "identifier"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166218
    const-string v1, "visible_interval"

    invoke-virtual {v0, v1, p3, p4}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 2166219
    iget-object v1, p0, LX/Emq;->l:LX/Jxq;

    if-eqz v1, :cond_0

    .line 2166220
    iget-object v1, p0, LX/Emq;->l:LX/Jxq;

    .line 2166221
    const-string v2, "entity_card_vpv"

    invoke-static {v1, p2, v0, v2}, LX/Jxq;->a(LX/Jxq;Ljava/lang/Object;LX/0oG;Ljava/lang/String;)V

    .line 2166222
    :cond_0
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2166223
    :cond_1
    invoke-static {p0}, LX/Emq;->e(LX/Emq;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/Emq;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2166224
    if-eqz v0, :cond_5

    .line 2166225
    iget-object v0, p0, LX/Emq;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2166226
    :cond_2
    return-void

    :cond_3
    move v0, v8

    .line 2166227
    goto :goto_0

    .line 2166228
    :cond_4
    iget-object v0, p0, LX/Emq;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2hd;

    iget-object v2, p0, LX/Emq;->k:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v6, LX/2hC;->ENTITY_CARDS:LX/2hC;

    iget-object v0, p0, LX/Emq;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2h7;

    iget-object v7, v0, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    invoke-virtual/range {v1 .. v7}, LX/2hd;->a(Ljava/lang/String;Ljava/lang/String;JLX/2hC;LX/2hC;)V

    .line 2166229
    iget-object v0, p0, LX/Emq;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2166230
    :cond_5
    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_2
    if-ge v8, v1, :cond_2

    iget-object v0, p0, LX/Emq;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emj;

    .line 2166231
    invoke-interface {v0, p1, p2, p3, p4}, LX/Emj;->a(Ljava/lang/String;Ljava/lang/Object;D)V

    .line 2166232
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(ZI)V
    .locals 3

    .prologue
    .line 2166208
    sget-object v0, LX/Emn;->ENTITY_CARDS_PAGE_DOWNLOAD:LX/Emn;

    invoke-static {p0, v0}, LX/Emq;->a(LX/Emq;LX/Emn;)LX/0oG;

    move-result-object v0

    .line 2166209
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2166210
    const-string v1, "query_success"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166211
    const-string v1, "entity_count"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2166212
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2166213
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2166207
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2166206
    iget-object v0, p0, LX/Emq;->g:Ljava/lang/String;

    return-object v0
.end method
