.class public final LX/EpT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/EpL;

.field public final synthetic c:LX/EpZ;


# direct methods
.method public constructor <init>(LX/EpZ;JLX/EpL;)V
    .locals 0

    .prologue
    .line 2170303
    iput-object p1, p0, LX/EpT;->c:LX/EpZ;

    iput-wide p2, p0, LX/EpT;->a:J

    iput-object p4, p0, LX/EpT;->b:LX/EpL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170304
    iget-object v0, p0, LX/EpT;->c:LX/EpZ;

    iget-object v0, v0, LX/EpZ;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2170305
    iget-object v2, p0, LX/EpT;->c:LX/EpZ;

    iget-object v2, v2, LX/EpZ;->c:LX/2dj;

    iget-wide v4, p0, LX/EpT;->a:J

    invoke-virtual {v2, v0, v1, v4, v5}, LX/2dj;->a(JJ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2170306
    new-instance v1, LX/EpS;

    invoke-direct {v1, p0}, LX/EpS;-><init>(LX/EpT;)V

    iget-object v2, p0, LX/EpT;->c:LX/EpZ;

    iget-object v2, v2, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2170307
    return-object v0
.end method
