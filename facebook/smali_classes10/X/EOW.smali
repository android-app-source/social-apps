.class public final LX/EOW;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EOX;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsBlendedPhotoSocialModuleInterfaces$SearchResultsBlendedPhotoSocialModule;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/EOS;

.field public c:I

.field public final synthetic d:LX/EOX;


# direct methods
.method public constructor <init>(LX/EOX;)V
    .locals 1

    .prologue
    .line 2114674
    iput-object p1, p0, LX/EOW;->d:LX/EOX;

    .line 2114675
    move-object v0, p1

    .line 2114676
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2114677
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2114678
    const-string v0, "SearchResultsBlendedPhotoSocialGridComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2114679
    if-ne p0, p1, :cond_1

    .line 2114680
    :cond_0
    :goto_0
    return v0

    .line 2114681
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2114682
    goto :goto_0

    .line 2114683
    :cond_3
    check-cast p1, LX/EOW;

    .line 2114684
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2114685
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2114686
    if-eq v2, v3, :cond_0

    .line 2114687
    iget-object v2, p0, LX/EOW;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EOW;->a:LX/CzL;

    iget-object v3, p1, LX/EOW;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2114688
    goto :goto_0

    .line 2114689
    :cond_5
    iget-object v2, p1, LX/EOW;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2114690
    :cond_6
    iget-object v2, p0, LX/EOW;->b:LX/EOS;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EOW;->b:LX/EOS;

    iget-object v3, p1, LX/EOW;->b:LX/EOS;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2114691
    goto :goto_0

    .line 2114692
    :cond_8
    iget-object v2, p1, LX/EOW;->b:LX/EOS;

    if-nez v2, :cond_7

    .line 2114693
    :cond_9
    iget v2, p0, LX/EOW;->c:I

    iget v3, p1, LX/EOW;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2114694
    goto :goto_0
.end method
