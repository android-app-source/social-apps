.class public final LX/E9s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/5tj;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Landroid/view/View;

.field public final synthetic e:LX/E9x;


# direct methods
.method public constructor <init>(LX/E9x;LX/5tj;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2084907
    iput-object p1, p0, LX/E9s;->e:LX/E9x;

    iput-object p2, p0, LX/E9s;->a:LX/5tj;

    iput-object p3, p0, LX/E9s;->b:Ljava/lang/String;

    iput-object p4, p0, LX/E9s;->c:Ljava/lang/String;

    iput-object p5, p0, LX/E9s;->d:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x202bddc7

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2084908
    iget-object v1, p0, LX/E9s;->a:LX/5tj;

    invoke-interface {v1}, LX/5tj;->bh_()LX/1VU;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2084909
    iget-object v1, p0, LX/E9s;->e:LX/E9x;

    iget-object v1, v1, LX/E9x;->g:LX/79D;

    iget-object v2, p0, LX/E9s;->b:Ljava/lang/String;

    iget-object v3, p0, LX/E9s;->c:Ljava/lang/String;

    iget-object v4, p0, LX/E9s;->a:LX/5tj;

    invoke-static {v4}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/79C;->REVIEW_ROW_VIEW:LX/79C;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/79D;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/79C;)V

    .line 2084910
    iget-object v1, p0, LX/E9s;->e:LX/E9x;

    iget-object v2, p0, LX/E9s;->d:Landroid/view/View;

    iget-object v3, p0, LX/E9s;->a:LX/5tj;

    invoke-interface {v3}, LX/5tj;->bh_()LX/1VU;

    move-result-object v3

    iget-object v4, p0, LX/E9s;->b:Ljava/lang/String;

    .line 2084911
    new-instance v5, LX/8qL;

    invoke-direct {v5}, LX/8qL;-><init>()V

    invoke-static {v3}, LX/9JZ;->a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p0

    .line 2084912
    iput-object p0, v5, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2084913
    move-object v5, v5

    .line 2084914
    invoke-interface {v3}, LX/1VU;->j()Ljava/lang/String;

    move-result-object p0

    .line 2084915
    iput-object p0, v5, LX/8qL;->d:Ljava/lang/String;

    .line 2084916
    move-object v5, v5

    .line 2084917
    new-instance p0, LX/21A;

    invoke-direct {p0}, LX/21A;-><init>()V

    .line 2084918
    iput-object v4, p0, LX/21A;->c:Ljava/lang/String;

    .line 2084919
    move-object p0, p0

    .line 2084920
    invoke-virtual {p0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object p0

    .line 2084921
    iput-object p0, v5, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2084922
    move-object v5, v5

    .line 2084923
    invoke-virtual {v5}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v5

    .line 2084924
    iget-object p0, v1, LX/E9x;->b:LX/1nI;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {p0, p1, v5}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    .line 2084925
    :cond_0
    const v1, 0x62de646e

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
