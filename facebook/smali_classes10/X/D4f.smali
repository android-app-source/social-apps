.class public LX/D4f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/D4f;


# instance fields
.field public final a:LX/1Uj;


# direct methods
.method public constructor <init>(LX/1Uj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1962450
    iput-object p1, p0, LX/D4f;->a:LX/1Uj;

    .line 1962451
    return-void
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 4

    .prologue
    .line 1962437
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 1962438
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1962439
    :cond_0
    :goto_0
    return v0

    .line 1962440
    :cond_1
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1962441
    if-ltz v0, :cond_2

    if-ge v0, p1, :cond_2

    move p1, v0

    .line 1962442
    :cond_2
    move v0, p1

    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 1962443
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, LX/D4f;->a(C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1962444
    :goto_2
    move v0, v0

    .line 1962445
    invoke-static {p0, p1}, LX/D4f;->c(Ljava/lang/String;I)I

    move-result v1

    .line 1962446
    sub-int v2, v0, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    sub-int v3, p1, v1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-le v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 1962447
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, p1

    .line 1962448
    goto :goto_2
.end method

.method public static a(LX/0QB;)LX/D4f;
    .locals 4

    .prologue
    .line 1962424
    sget-object v0, LX/D4f;->b:LX/D4f;

    if-nez v0, :cond_1

    .line 1962425
    const-class v1, LX/D4f;

    monitor-enter v1

    .line 1962426
    :try_start_0
    sget-object v0, LX/D4f;->b:LX/D4f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1962427
    if-eqz v2, :cond_0

    .line 1962428
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1962429
    new-instance p0, LX/D4f;

    invoke-static {v0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v3

    check-cast v3, LX/1Uj;

    invoke-direct {p0, v3}, LX/D4f;-><init>(LX/1Uj;)V

    .line 1962430
    move-object v0, p0

    .line 1962431
    sput-object v0, LX/D4f;->b:LX/D4f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962432
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1962433
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1962434
    :cond_1
    sget-object v0, LX/D4f;->b:LX/D4f;

    return-object v0

    .line 1962435
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1962436
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1962423
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\u2026 "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(C)Z
    .locals 1

    .prologue
    .line 1962404
    const/16 v0, 0x20

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;IILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 1962410
    if-eqz p0, :cond_2

    if-eqz p3, :cond_2

    if-lez p1, :cond_2

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-ge p1, v0, :cond_2

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-static {p0, p1, p3}, LX/D4f;->c(Landroid/text/SpannableStringBuilder;ILjava/lang/String;)I

    move-result v1

    if-le v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1962411
    if-nez v0, :cond_0

    .line 1962412
    if-eqz p0, :cond_3

    if-eqz p3, :cond_3

    if-lez p2, :cond_3

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    .line 1962413
    if-nez p0, :cond_4

    .line 1962414
    :goto_1
    move v0, v0

    .line 1962415
    if-ge p2, v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1962416
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1962417
    :cond_4
    const/4 v1, 0x1

    .line 1962418
    :goto_4
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    if-ge v0, p1, :cond_6

    .line 1962419
    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result p1

    const/16 p3, 0xa

    if-ne p1, p3, :cond_5

    .line 1962420
    add-int/lit8 v1, v1, 0x1

    .line 1962421
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    move v0, v1

    .line 1962422
    goto :goto_1
.end method

.method public static c(Landroid/text/SpannableStringBuilder;ILjava/lang/String;)I
    .locals 2

    .prologue
    .line 1962409
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/D4f;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {p2}, LX/D4f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private static c(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 1962405
    move v0, p1

    :goto_0
    if-ltz v0, :cond_0

    .line 1962406
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, LX/D4f;->a(C)Z

    move-result v1

    if-eqz v1, :cond_1

    move p1, v0

    .line 1962407
    :cond_0
    return p1

    .line 1962408
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method
