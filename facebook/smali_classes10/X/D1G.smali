.class public LX/D1G;
.super LX/D14;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final f:LX/BiM;

.field private g:LX/D1E;


# direct methods
.method public constructor <init>(LX/BiM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1956470
    invoke-direct {p0}, LX/D14;-><init>()V

    .line 1956471
    iput-object p1, p0, LX/D1G;->f:LX/BiM;

    .line 1956472
    return-void
.end method

.method public static a(LX/0QB;)LX/D1G;
    .locals 4

    .prologue
    .line 1956459
    const-class v1, LX/D1G;

    monitor-enter v1

    .line 1956460
    :try_start_0
    sget-object v0, LX/D1G;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1956461
    sput-object v2, LX/D1G;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1956462
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1956463
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1956464
    new-instance p0, LX/D1G;

    invoke-static {v0}, LX/BiM;->b(LX/0QB;)LX/BiM;

    move-result-object v3

    check-cast v3, LX/BiM;

    invoke-direct {p0, v3}, LX/D1G;-><init>(LX/BiM;)V

    .line 1956465
    move-object v0, p0

    .line 1956466
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1956467
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D1G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1956468
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1956469
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1956444
    iget-object v0, p0, LX/D1G;->g:LX/D1E;

    .line 1956445
    iget-object p0, v0, LX/D1E;->d:LX/0Px;

    move-object v0, p0

    .line 1956446
    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1956453
    iget-object v0, p0, LX/D1G;->g:LX/D1E;

    .line 1956454
    iput-object p1, v0, LX/D1E;->d:LX/0Px;

    .line 1956455
    iget-object v0, p0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->removeAllViews()V

    .line 1956456
    iget-object v0, p0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget v1, p0, LX/D14;->c:I

    iget v2, p0, LX/D14;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->h(II)V

    .line 1956457
    iget-object v0, p0, LX/D1G;->g:LX/D1E;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1956458
    return-void
.end method

.method public final a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 2

    .prologue
    .line 1956447
    iput-object p1, p0, LX/D1G;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1956448
    new-instance v0, LX/D1E;

    invoke-direct {v0, p0}, LX/D1E;-><init>(LX/D1G;)V

    iput-object v0, p0, LX/D1G;->g:LX/D1E;

    .line 1956449
    iget-object v0, p0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, LX/D1G;->g:LX/D1E;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1956450
    iget-object v0, p0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    new-instance v1, LX/D1B;

    invoke-direct {v1, p0}, LX/D1B;-><init>(LX/D1G;)V

    .line 1956451
    iput-object v1, v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->n:LX/2ec;

    .line 1956452
    return-void
.end method
