.class public final LX/DEq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final synthetic c:LX/2e2;


# direct methods
.method public constructor <init>(LX/2e2;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 0

    .prologue
    .line 1977371
    iput-object p1, p0, LX/DEq;->c:LX/2e2;

    iput-object p2, p0, LX/DEq;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    iput-object p3, p0, LX/DEq;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1977358
    iget-object v0, p0, LX/DEq;->c:LX/2e2;

    iget-object v1, p0, LX/DEq;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->t()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DEq;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 1977359
    new-instance v3, LX/857;

    invoke-direct {v3}, LX/857;-><init>()V

    move-object v3, v3

    .line 1977360
    const-string v4, "node_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "pymk_size_param"

    iget-object v6, v0, LX/2e2;->f:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "after_param"

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "first_param"

    iget-object v6, v0, LX/2e2;->l:LX/2e3;

    const/16 p0, 0x14

    .line 1977361
    iget-boolean v1, v6, LX/2e3;->c:Z

    if-nez v1, :cond_0

    .line 1977362
    :goto_0
    move v6, p0

    .line 1977363
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "media_type"

    iget-object v6, v0, LX/2e2;->b:LX/0rq;

    invoke-virtual {v6}, LX/0rq;->b()LX/0wF;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1977364
    iget-object v4, v0, LX/2e2;->k:LX/2dl;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    const-string v5, "FEED_PYMK_QUERY_TAG"

    invoke-virtual {v4, v3, v5}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1977365
    new-instance v4, LX/DEs;

    invoke-direct {v4, v0}, LX/DEs;-><init>(LX/2e2;)V

    iget-object v5, v0, LX/2e2;->c:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 1977366
    return-object v0

    .line 1977367
    :cond_0
    sget-object v1, LX/33c;->a:[I

    iget-object v2, v6, LX/2e3;->a:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1977368
    :pswitch_0
    iget-object v1, v6, LX/2e3;->b:LX/0ad;

    sget v2, LX/2e4;->h:I

    invoke-interface {v1, v2, p0}, LX/0ad;->a(II)I

    move-result p0

    goto :goto_0

    .line 1977369
    :pswitch_1
    iget-object v1, v6, LX/2e3;->b:LX/0ad;

    sget v2, LX/2e4;->f:I

    invoke-interface {v1, v2, p0}, LX/0ad;->a(II)I

    move-result p0

    goto :goto_0

    .line 1977370
    :pswitch_2
    iget-object v1, v6, LX/2e3;->b:LX/0ad;

    sget v2, LX/2e4;->d:I

    invoke-interface {v1, v2, p0}, LX/0ad;->a(II)I

    move-result p0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
