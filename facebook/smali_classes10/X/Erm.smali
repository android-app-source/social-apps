.class public final LX/Erm;
.super LX/Erj;
.source ""


# instance fields
.field public final c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final d:Lcom/facebook/feed/model/ClientFeedUnitEdge;

.field public final synthetic e:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;Lcom/facebook/feed/model/ClientFeedUnitEdge;)V
    .locals 0

    .prologue
    .line 2173399
    iput-object p1, p0, LX/Erm;->e:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    .line 2173400
    invoke-direct {p0, p1, p3}, LX/Erj;-><init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/String;)V

    .line 2173401
    iput-object p2, p0, LX/Erm;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2173402
    iput-object p4, p0, LX/Erm;->d:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 2173403
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "IgnoreExecutorServiceSubmitResult"
        }
    .end annotation

    .prologue
    .line 2173404
    iget-object v0, p0, LX/Erm;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Erm;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->n(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2173405
    :cond_0
    :goto_0
    return-void

    .line 2173406
    :cond_1
    const/4 v0, 0x0

    .line 2173407
    iget-object v1, p0, LX/Erm;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2173408
    invoke-static {v1}, LX/2yp;->a(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2173409
    :cond_2
    :goto_1
    move-object v0, v0

    .line 2173410
    if-eqz v0, :cond_0

    .line 2173411
    const/4 v2, 0x0

    .line 2173412
    iget-object v1, p0, LX/Erm;->d:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/Erm;->d:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2173413
    iget-object v1, p0, LX/Erm;->d:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 2173414
    instance-of v3, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_4

    .line 2173415
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2173416
    :goto_2
    move-object v1, v1

    .line 2173417
    if-eqz v1, :cond_0

    .line 2173418
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    .line 2173419
    if-eqz v2, :cond_0

    .line 2173420
    iget-object v2, p0, LX/Erm;->e:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, LX/Erj;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    .line 2173421
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->Z()Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    move-result-object v2

    .line 2173422
    if-nez v2, :cond_6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;->HTML_ONLY:Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    :goto_3
    move-object v2, v2

    .line 2173423
    new-instance v3, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;

    invoke-direct {v3, p0, v0, v2, v1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$LinkFetcher$1;-><init>(LX/Erm;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2173424
    iget-object v0, p0, LX/Erm;->e:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    const v1, -0xb68eb67

    invoke-static {v0, v3, v1}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 2173425
    :cond_3
    invoke-static {v1}, LX/2yo;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 2173426
    if-eqz v1, :cond_2

    .line 2173427
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v1, v2

    .line 2173428
    goto :goto_2

    :cond_5
    move-object v1, v2

    .line 2173429
    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;->k()Lcom/facebook/graphql/enums/GraphQLBrowserPrefetchType;

    move-result-object v2

    goto :goto_3
.end method
