.class public abstract LX/Dn8;
.super LX/1a1;
.source ""


# instance fields
.field public final m:Landroid/view/View;

.field public final n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final o:Landroid/widget/TextView;

.field public final p:Landroid/widget/TextView;

.field public final q:LX/Dln;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/Dlo;)V
    .locals 11

    .prologue
    .line 2040019
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2040020
    const v0, 0x7f0d059a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Dn8;->m:Landroid/view/View;

    .line 2040021
    const v0, 0x7f0d270a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Dn8;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2040022
    const v0, 0x7f0d270c

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Dn8;->o:Landroid/widget/TextView;

    .line 2040023
    const v0, 0x7f0d270b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Dn8;->p:Landroid/widget/TextView;

    .line 2040024
    iget-object v0, p0, LX/Dn8;->m:Landroid/view/View;

    iget-object v1, p0, LX/Dn8;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/Dn8;->o:Landroid/widget/TextView;

    iget-object v3, p0, LX/Dn8;->p:Landroid/widget/TextView;

    .line 2040025
    new-instance v4, LX/Dln;

    const-class v5, Landroid/content/Context;

    invoke-interface {p2, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p2}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    move-object v7, v0

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    invoke-direct/range {v4 .. v10}, LX/Dln;-><init>(Landroid/content/Context;LX/0wM;Landroid/view/View;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 2040026
    move-object v0, v4

    .line 2040027
    iput-object v0, p0, LX/Dn8;->q:LX/Dln;

    .line 2040028
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
.end method
