.class public LX/Dz1;
.super LX/Dyu;
.source ""


# instance fields
.field public final a:LX/0ad;

.field private final b:Landroid/content/Context;

.field private final c:LX/DzM;

.field private final d:Ljava/util/Locale;

.field private e:LX/9j5;

.field public f:LX/9jN;

.field public g:Ljava/lang/String;

.field public h:LX/9jG;


# direct methods
.method public constructor <init>(LX/0ad;Landroid/content/Context;LX/DzM;Ljava/util/Locale;LX/9j5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066341
    invoke-direct {p0}, LX/Dyu;-><init>()V

    .line 2066342
    iput-object p1, p0, LX/Dz1;->a:LX/0ad;

    .line 2066343
    iput-object p2, p0, LX/Dz1;->b:Landroid/content/Context;

    .line 2066344
    iput-object p3, p0, LX/Dz1;->c:LX/DzM;

    .line 2066345
    iput-object p4, p0, LX/Dz1;->d:Ljava/util/Locale;

    .line 2066346
    iput-object p5, p0, LX/Dz1;->e:LX/9j5;

    .line 2066347
    return-void
.end method


# virtual methods
.method public final a()LX/9jL;
    .locals 1

    .prologue
    .line 2066348
    sget-object v0, LX/9jL;->TextOnlyRow:LX/9jL;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2066349
    check-cast p1, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2066350
    if-nez p1, :cond_0

    .line 2066351
    new-instance p1, Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v0, p0, LX/Dz1;->b:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2066352
    iget-object v0, p0, LX/Dz1;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02191a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 2066353
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, -0x1

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2066354
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2066355
    :cond_0
    sget-object v0, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2066356
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2066357
    iget-object v0, p0, LX/Dz1;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0816fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2066358
    iget-object v0, p0, LX/Dz1;->d:Ljava/util/Locale;

    iget-object v1, p0, LX/Dz1;->g:Ljava/lang/String;

    iget-object v2, p0, LX/Dz1;->g:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/DzM;->a(Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2066359
    return-object p1
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2066360
    iget-object v0, p0, LX/Dz1;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2066361
    iget-object v0, p0, LX/Dz1;->h:LX/9jG;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Dz1;->h:LX/9jG;

    sget-object v1, LX/9jG;->EVENT:LX/9jG;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/Dz1;->h:LX/9jG;

    invoke-virtual {v0}, LX/9jG;->isSocialSearchType()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2066362
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dz1;->a:LX/0ad;

    sget-char v1, LX/9j3;->b:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "top"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dz1;->f:LX/9jN;

    .line 2066363
    iget-object v1, v0, LX/9jN;->b:Ljava/util/List;

    move-object v0, v1

    .line 2066364
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2066365
    if-eqz v0, :cond_0

    .line 2066366
    iget-object v0, p0, LX/Dz1;->e:LX/9j5;

    invoke-virtual {p0}, LX/Dz1;->e()Ljava/lang/String;

    move-result-object v1

    .line 2066367
    iget-object v2, v0, LX/9j5;->a:LX/0Zb;

    const-string v3, "place_picker_phrase_row_shown"

    invoke-static {v0, v3}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "qe_group"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2066368
    sget-object v0, LX/9jL;->TextOnlyRow:LX/9jL;

    iget-object v1, p0, LX/Dz1;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2066369
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 2066370
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2066371
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2066372
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2066373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/Dz1;->a:LX/0ad;

    sget-char v2, LX/9j3;->b:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Dz1;->a:LX/0ad;

    sget-char v2, LX/9j3;->a:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
