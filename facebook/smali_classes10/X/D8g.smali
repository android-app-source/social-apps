.class public final enum LX/D8g;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D8g;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D8g;

.field public static final enum WATCH_AND_BROWSE:LX/D8g;

.field public static final enum WATCH_AND_BROWSE_OFFER:LX/D8g;

.field public static final enum WATCH_AND_DIRECT_INSTALL:LX/D8g;

.field public static final enum WATCH_AND_INSTALL:LX/D8g;

.field public static final enum WATCH_AND_LEADGEN:LX/D8g;

.field public static final enum WATCH_AND_LOCAL:LX/D8g;

.field public static final enum WATCH_AND_SHOP:LX/D8g;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1969150
    new-instance v0, LX/D8g;

    const-string v1, "WATCH_AND_BROWSE"

    invoke-direct {v0, v1, v3}, LX/D8g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8g;->WATCH_AND_BROWSE:LX/D8g;

    .line 1969151
    new-instance v0, LX/D8g;

    const-string v1, "WATCH_AND_SHOP"

    invoke-direct {v0, v1, v4}, LX/D8g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8g;->WATCH_AND_SHOP:LX/D8g;

    .line 1969152
    new-instance v0, LX/D8g;

    const-string v1, "WATCH_AND_DIRECT_INSTALL"

    invoke-direct {v0, v1, v5}, LX/D8g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8g;->WATCH_AND_DIRECT_INSTALL:LX/D8g;

    .line 1969153
    new-instance v0, LX/D8g;

    const-string v1, "WATCH_AND_INSTALL"

    invoke-direct {v0, v1, v6}, LX/D8g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8g;->WATCH_AND_INSTALL:LX/D8g;

    .line 1969154
    new-instance v0, LX/D8g;

    const-string v1, "WATCH_AND_LEADGEN"

    invoke-direct {v0, v1, v7}, LX/D8g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8g;->WATCH_AND_LEADGEN:LX/D8g;

    .line 1969155
    new-instance v0, LX/D8g;

    const-string v1, "WATCH_AND_LOCAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/D8g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8g;->WATCH_AND_LOCAL:LX/D8g;

    .line 1969156
    new-instance v0, LX/D8g;

    const-string v1, "WATCH_AND_BROWSE_OFFER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/D8g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8g;->WATCH_AND_BROWSE_OFFER:LX/D8g;

    .line 1969157
    const/4 v0, 0x7

    new-array v0, v0, [LX/D8g;

    sget-object v1, LX/D8g;->WATCH_AND_BROWSE:LX/D8g;

    aput-object v1, v0, v3

    sget-object v1, LX/D8g;->WATCH_AND_SHOP:LX/D8g;

    aput-object v1, v0, v4

    sget-object v1, LX/D8g;->WATCH_AND_DIRECT_INSTALL:LX/D8g;

    aput-object v1, v0, v5

    sget-object v1, LX/D8g;->WATCH_AND_INSTALL:LX/D8g;

    aput-object v1, v0, v6

    sget-object v1, LX/D8g;->WATCH_AND_LEADGEN:LX/D8g;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/D8g;->WATCH_AND_LOCAL:LX/D8g;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/D8g;->WATCH_AND_BROWSE_OFFER:LX/D8g;

    aput-object v2, v0, v1

    sput-object v0, LX/D8g;->$VALUES:[LX/D8g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1969149
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D8g;
    .locals 1

    .prologue
    .line 1969148
    const-class v0, LX/D8g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D8g;

    return-object v0
.end method

.method public static values()[LX/D8g;
    .locals 1

    .prologue
    .line 1969147
    sget-object v0, LX/D8g;->$VALUES:[LX/D8g;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D8g;

    return-object v0
.end method
