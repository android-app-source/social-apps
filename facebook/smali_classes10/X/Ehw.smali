.class public final LX/Ehw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field public final synthetic a:Lcom/facebook/common/intent/AppChooserDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/common/intent/AppChooserDialogFragment;)V
    .locals 0

    .prologue
    .line 2158900
    iput-object p1, p0, LX/Ehw;->a:Lcom/facebook/common/intent/AppChooserDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onShow(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 2158901
    iget-object v0, p0, LX/Ehw;->a:Lcom/facebook/common/intent/AppChooserDialogFragment;

    iget-object v0, v0, Lcom/facebook/common/intent/AppChooserDialogFragment;->o:LX/Ei1;

    iget-object v1, p0, LX/Ehw;->a:Lcom/facebook/common/intent/AppChooserDialogFragment;

    iget-object v1, v1, Lcom/facebook/common/intent/AppChooserDialogFragment;->q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 2158902
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2158903
    const-string v3, "app_count"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2, v3, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2158904
    iget-object v3, v0, LX/Ei1;->c:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 2158905
    iget-object v3, v0, LX/Ei1;->c:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2158906
    :cond_0
    iget-object v3, v0, LX/Ei1;->b:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/Ei0;->OPENED:LX/Ei0;

    iget-object p1, p1, LX/Ei0;->eventName:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, LX/Ei1;->a:Ljava/lang/String;

    .line 2158907
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2158908
    move-object p0, p0

    .line 2158909
    invoke-virtual {p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2158910
    return-void
.end method
