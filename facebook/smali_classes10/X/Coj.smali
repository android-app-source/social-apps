.class public LX/Coj;
.super LX/Coi;
.source ""


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CIg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 1936009
    invoke-direct {p0, p1}, LX/Coi;-><init>(Landroid/view/View;)V

    .line 1936010
    const-class v0, LX/Coj;

    invoke-static {v0, p0}, LX/Coj;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936011
    new-instance v0, LX/Cn7;

    new-instance v1, LX/Cn4;

    iget-object v2, p0, LX/Coj;->a:LX/Cju;

    invoke-direct {v1, v2}, LX/Cn4;-><init>(LX/Cju;)V

    new-instance v2, LX/Cof;

    invoke-direct {v2, p0}, LX/Cof;-><init>(LX/Coj;)V

    new-instance v3, LX/Cn3;

    invoke-direct {v3}, LX/Cn3;-><init>()V

    new-instance v4, LX/Cn2;

    invoke-direct {v4}, LX/Cn2;-><init>()V

    new-instance v5, LX/Cog;

    invoke-direct {v5, p0}, LX/Cog;-><init>(LX/Coj;)V

    new-instance v6, LX/Coh;

    invoke-direct {v6, p0}, LX/Coh;-><init>(LX/Coj;)V

    invoke-direct/range {v0 .. v6}, LX/Cn7;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;LX/Cmm;LX/Cmt;)V

    .line 1936012
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1936013
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Coj;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v2

    check-cast v2, LX/Ck0;

    invoke-static {p0}, LX/CIg;->a(LX/0QB;)LX/CIg;

    move-result-object p0

    check-cast p0, LX/CIg;

    iput-object v1, p1, LX/Coj;->a:LX/Cju;

    iput-object v2, p1, LX/Coj;->b:LX/Ck0;

    iput-object p0, p1, LX/Coj;->c:LX/CIg;

    return-void
.end method
