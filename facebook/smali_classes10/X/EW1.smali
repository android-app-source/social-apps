.class public final LX/EW1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field public final synthetic a:LX/EWC;


# direct methods
.method public constructor <init>(LX/EWC;)V
    .locals 0

    .prologue
    .line 2129269
    iput-object p1, p0, LX/EW1;->a:LX/EWC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2129270
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    invoke-virtual {v0}, LX/EWC;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    .line 2129271
    iget-boolean v1, v0, LX/EWC;->s:Z

    move v0, v1

    .line 2129272
    if-eqz v0, :cond_1

    :cond_0
    move v0, v7

    .line 2129273
    :goto_0
    return v0

    .line 2129274
    :cond_1
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->O:Landroid/widget/AdapterView$OnItemLongClickListener;

    if-eqz v0, :cond_2

    .line 2129275
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->O:Landroid/widget/AdapterView$OnItemLongClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    .line 2129276
    :cond_2
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    .line 2129277
    iget-boolean v1, v0, LX/EWC;->s:Z

    move v0, v1

    .line 2129278
    if-nez v0, :cond_3

    move v0, v6

    .line 2129279
    goto :goto_0

    .line 2129280
    :cond_3
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    .line 2129281
    iput v7, v0, LX/EWC;->d:I

    .line 2129282
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    .line 2129283
    iput v7, v0, LX/EWC;->e:I

    .line 2129284
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    iget-object v1, p0, LX/EW1;->a:LX/EWC;

    iget v1, v1, LX/EWC;->f:I

    iget-object v2, p0, LX/EW1;->a:LX/EWC;

    iget v2, v2, LX/EWC;->g:I

    invoke-virtual {v0, v1, v2}, LX/EWC;->pointToPosition(II)I

    move-result v0

    .line 2129285
    iget-object v1, p0, LX/EW1;->a:LX/EWC;

    invoke-virtual {v1}, LX/EWC;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, v0, v1

    .line 2129286
    iget-object v2, p0, LX/EW1;->a:LX/EWC;

    invoke-virtual {v2, v1}, LX/EWC;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2129287
    iget-object v2, p0, LX/EW1;->a:LX/EWC;

    iget-object v3, p0, LX/EW1;->a:LX/EWC;

    invoke-virtual {v3}, LX/EWC;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 2129288
    iput-wide v4, v2, LX/EWC;->k:J

    .line 2129289
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->K:LX/EVz;

    invoke-interface {v0, v1}, LX/EVz;->a(Landroid/view/View;)V

    .line 2129290
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->x:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2129291
    if-eqz v1, :cond_4

    .line 2129292
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2129293
    :cond_4
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    .line 2129294
    iput-boolean v6, v0, LX/EWC;->l:Z

    .line 2129295
    iget-object v0, p0, LX/EW1;->a:LX/EWC;

    iget-object v1, p0, LX/EW1;->a:LX/EWC;

    iget-wide v2, v1, LX/EWC;->k:J

    invoke-static {v0, v2, v3}, LX/EWC;->a(LX/EWC;J)V

    move v0, v6

    .line 2129296
    goto :goto_0
.end method
