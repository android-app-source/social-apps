.class public LX/Dsx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dsw;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dsl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dsw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dsl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2050970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050971
    iput-object p1, p0, LX/Dsx;->a:LX/0Or;

    .line 2050972
    iput-object p2, p0, LX/Dsx;->b:LX/0Ot;

    .line 2050973
    iput-object p3, p0, LX/Dsx;->c:LX/0Ot;

    .line 2050974
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2050975
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2050976
    const-string v1, "send_page_like_invite"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2050977
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2050978
    const-string v1, "sendPageLikeInviteParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;

    .line 2050979
    iget-object v1, p0, LX/Dsx;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/Dsx;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2050980
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2050981
    move-object v0, v0

    .line 2050982
    :goto_0
    return-object v0

    .line 2050983
    :cond_0
    const-string v1, "friends_you_may_invite"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2050984
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2050985
    const-string v1, "fetchFriendsYouMayInviteParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/friendinviter/protocol/FetchFriendsYouMayInviteMethod$Params;

    .line 2050986
    iget-object v1, p0, LX/Dsx;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/Dsx;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2050987
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2050988
    goto :goto_0

    .line 2050989
    :cond_1
    new-instance v1, LX/4B3;

    invoke-direct {v1, v0}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v1
.end method
