.class public LX/CqZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CqY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/CqY",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 1940056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1940057
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/CqZ;->a:Ljava/lang/Boolean;

    .line 1940058
    return-void
.end method


# virtual methods
.method public final a(LX/CqY;F)LX/CqY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CqY",
            "<",
            "Ljava/lang/Boolean;",
            ">;F)",
            "LX/CqY",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1940059
    new-instance v1, LX/CqZ;

    invoke-interface {p1}, LX/CqY;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v1, v0}, LX/CqZ;-><init>(Z)V

    return-object v1
.end method

.method public final a()LX/CrQ;
    .locals 1

    .prologue
    .line 1940060
    sget-object v0, LX/CrQ;->FADES_WITH_CONTROLS:LX/CrQ;

    return-object v0
.end method

.method public final c()LX/CqY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/CqY",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1940061
    new-instance v0, LX/CqZ;

    iget-object v1, p0, LX/CqZ;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {v0, v1}, LX/CqZ;-><init>(Z)V

    return-object v0
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1940062
    iget-object v0, p0, LX/CqZ;->a:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1940063
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1940064
    if-ne p0, p1, :cond_0

    .line 1940065
    const/4 v0, 0x1

    .line 1940066
    :goto_0
    return v0

    .line 1940067
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 1940068
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1940069
    :cond_2
    check-cast p1, LX/CqZ;

    .line 1940070
    iget-object v0, p0, LX/CqZ;->a:Ljava/lang/Boolean;

    iget-object v1, p1, LX/CqZ;->a:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1940071
    iget-object v0, p0, LX/CqZ;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    return v0
.end method
