.class public LX/E4c;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E4c;


# instance fields
.field private final a:LX/E1f;

.field private final b:LX/E6E;


# direct methods
.method public constructor <init>(LX/E1f;LX/E6E;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2076972
    iput-object p1, p0, LX/E4c;->a:LX/E1f;

    .line 2076973
    iput-object p2, p0, LX/E4c;->b:LX/E6E;

    .line 2076974
    return-void
.end method

.method public static a(LX/0QB;)LX/E4c;
    .locals 5

    .prologue
    .line 2076975
    sget-object v0, LX/E4c;->c:LX/E4c;

    if-nez v0, :cond_1

    .line 2076976
    const-class v1, LX/E4c;

    monitor-enter v1

    .line 2076977
    :try_start_0
    sget-object v0, LX/E4c;->c:LX/E4c;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2076978
    if-eqz v2, :cond_0

    .line 2076979
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2076980
    new-instance p0, LX/E4c;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v3

    check-cast v3, LX/E1f;

    invoke-static {v0}, LX/E6E;->b(LX/0QB;)LX/E6E;

    move-result-object v4

    check-cast v4, LX/E6E;

    invoke-direct {p0, v3, v4}, LX/E4c;-><init>(LX/E1f;LX/E6E;)V

    .line 2076981
    move-object v0, p0

    .line 2076982
    sput-object v0, LX/E4c;->c:LX/E4c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076983
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2076984
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2076985
    :cond_1
    sget-object v0, LX/E4c;->c:LX/E4c;

    return-object v0

    .line 2076986
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2076987
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p2    # Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "Ljava/lang/String;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2076988
    if-nez p2, :cond_0

    .line 2076989
    :goto_0
    return-void

    .line 2076990
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->OPEN_VERTICAL_ACTION_SHEET:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_1

    .line 2076991
    iget-object v0, p0, LX/E4c;->b:LX/E6E;

    move-object v1, p4

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object v5, p4

    check-cast v5, LX/3U9;

    move-object v6, p4

    check-cast v6, LX/2kp;

    move-object v1, p2

    move-object v2, p4

    move-object v3, p3

    move-object v7, p5

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, LX/E6E;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Landroid/content/Context;LX/3U9;LX/2kp;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2076992
    :cond_1
    iget-object v0, p0, LX/E4c;->a:LX/E1f;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v3, 0x0

    :goto_1
    move-object v1, p4

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2076993
    iget-object v4, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2076994
    move-object v1, p4

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2076995
    iget-object v5, v1, LX/2jY;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2076996
    move-object v1, p4

    check-cast v1, LX/2kn;

    invoke-interface {v1}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v8

    move-object v1, p2

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v8}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v0

    .line 2076997
    invoke-interface {p4, p5, p6, p3, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0

    .line 2076998
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method
