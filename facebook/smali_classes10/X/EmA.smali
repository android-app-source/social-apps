.class public final LX/EmA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2KK;


# direct methods
.method public constructor <init>(LX/2KK;)V
    .locals 0

    .prologue
    .line 2165191
    iput-object p1, p0, LX/EmA;->a:LX/2KK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2165189
    sget-object v0, LX/2KK;->b:Ljava/lang/Class;

    const-string v1, "DeviceDiscoverySocket connection failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2165190
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2165163
    check-cast p1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;

    .line 2165164
    if-nez p1, :cond_0

    .line 2165165
    :goto_0
    return-void

    .line 2165166
    :cond_0
    iget-object v0, p0, LX/EmA;->a:LX/2KK;

    const-string v1, "subscription_update"

    invoke-static {v0, v1}, LX/2KK;->b$redex0(LX/2KK;Ljava/lang/String;)V

    .line 2165167
    iget-object v0, p0, LX/EmA;->a:LX/2KK;

    .line 2165168
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2165169
    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "tcp://"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 2165170
    invoke-virtual {p1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2165171
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    .line 2165172
    invoke-virtual {p1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2165173
    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "tcp://"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 2165174
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2165175
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    .line 2165176
    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    .line 2165177
    const/16 v2, 0x20

    .line 2165178
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->a()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 p0, 0x0

    invoke-virtual {v5, v1, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2165179
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2165180
    const-string v1, "bytes_to_read"

    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 2165181
    :goto_1
    :try_start_2
    new-instance v2, LX/EmC;

    const/16 v5, 0xf

    invoke-direct {v2, v5, v3, v4, v1}, LX/EmC;-><init>(ILjava/net/InetAddress;II)V

    .line 2165182
    invoke-static {v0, v2}, LX/2KK;->a(LX/2KK;LX/EmC;)V
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2165183
    :goto_2
    goto :goto_0

    .line 2165184
    :catch_0
    :try_start_3
    move-exception v1

    .line 2165185
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 2165186
    sget-object v5, LX/2KK;->b:Ljava/lang/Class;

    const-string p0, "Failed to read json command, will use default bytesToRead"

    invoke-static {v5, p0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_1

    move v1, v2

    goto :goto_1

    .line 2165187
    :catch_1
    move-exception v1

    .line 2165188
    sget-object v2, LX/2KK;->b:Ljava/lang/Class;

    const-string v3, "Could not connect to socket endpoint"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
