.class public LX/Cmv;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/Cmv;

.field public static final b:LX/Cmv;

.field public static final c:LX/Cmv;


# instance fields
.field public final d:LX/Cmu;

.field public final e:F

.field public final f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1933641
    new-instance v0, LX/Cmv;

    sget-object v1, LX/Cmu;->UNSET:LX/Cmu;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, LX/Cmv;-><init>(LX/Cmu;FI)V

    sput-object v0, LX/Cmv;->a:LX/Cmv;

    .line 1933642
    invoke-static {v3}, LX/Cmv;->a(F)LX/Cmv;

    move-result-object v0

    sput-object v0, LX/Cmv;->b:LX/Cmv;

    .line 1933643
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->DOCUMENT_MARGIN:Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    .line 1933644
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, LX/Cmv;->a(Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;F)LX/Cmv;

    move-result-object v1

    move-object v0, v1

    .line 1933645
    sput-object v0, LX/Cmv;->c:LX/Cmv;

    return-void
.end method

.method private constructor <init>(LX/Cmu;FI)V
    .locals 0

    .prologue
    .line 1933636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933637
    iput-object p1, p0, LX/Cmv;->d:LX/Cmu;

    .line 1933638
    iput p2, p0, LX/Cmv;->e:F

    .line 1933639
    iput p3, p0, LX/Cmv;->f:I

    .line 1933640
    return-void
.end method

.method public static a(F)LX/Cmv;
    .locals 3

    .prologue
    .line 1933635
    new-instance v0, LX/Cmv;

    sget-object v1, LX/Cmu;->PIXEL:LX/Cmu;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, LX/Cmv;-><init>(LX/Cmu;FI)V

    return-object v0
.end method

.method public static a(FI)LX/Cmv;
    .locals 2

    .prologue
    .line 1933646
    new-instance v0, LX/Cmv;

    sget-object v1, LX/Cmu;->HAM_VALUE:LX/Cmu;

    invoke-direct {v0, v1, p0, p1}, LX/Cmv;-><init>(LX/Cmu;FI)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;F)LX/Cmv;
    .locals 5

    .prologue
    .line 1933624
    new-instance v0, LX/Cmv;

    sget-object v1, LX/Cmu;->HAM_VALUE:LX/Cmu;

    const/4 v2, 0x0

    .line 1933625
    if-nez p0, :cond_0

    .line 1933626
    :goto_0
    move v2, v2

    .line 1933627
    invoke-direct {v0, v1, p1, v2}, LX/Cmv;-><init>(LX/Cmu;FI)V

    return-object v0

    .line 1933628
    :cond_0
    sget-object v3, LX/Cn0;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1933629
    :pswitch_0
    const v2, 0x7f0d011b

    goto :goto_0

    .line 1933630
    :pswitch_1
    const v2, 0x7f0d011c

    goto :goto_0

    .line 1933631
    :pswitch_2
    const v2, 0x7f0d011d

    goto :goto_0

    .line 1933632
    :pswitch_3
    const v2, 0x7f0d011e

    goto :goto_0

    .line 1933633
    :pswitch_4
    const v2, 0x7f0d011f

    goto :goto_0

    .line 1933634
    :pswitch_5
    const v2, 0x7f0d0120

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final d()Z
    .locals 2

    .prologue
    .line 1933623
    iget-object v0, p0, LX/Cmv;->d:LX/Cmu;

    sget-object v1, LX/Cmu;->UNSET:LX/Cmu;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1933622
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, LX/Cmv;->e:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Cmv;->d:LX/Cmu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
