.class public final LX/Dbs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

.field public final synthetic b:I


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;I)V
    .locals 0

    .prologue
    .line 2017682
    iput-object p1, p0, LX/Dbs;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    iput p2, p0, LX/Dbs;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x5a4a08ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017683
    iget-object v1, p0, LX/Dbs;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    iget v2, p0, LX/Dbs;->b:I

    .line 2017684
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2017685
    iget-object v4, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v2, v4}, LX/0PB;->checkPositionIndex(II)I

    .line 2017686
    iget-object v4, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->c:LX/DbM;

    iget-wide v6, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 2017687
    const-string v6, "upload_photo_menu_delete_photo_button_click"

    invoke-static {v4, v6, v5}, LX/DbM;->d(LX/DbM;Ljava/lang/String;Ljava/lang/String;)V

    .line 2017688
    iget-object v4, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2017689
    iget-object v4, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 2017690
    invoke-static {v1, v2}, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->d(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;I)V

    .line 2017691
    const v1, 0x50805b8

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
