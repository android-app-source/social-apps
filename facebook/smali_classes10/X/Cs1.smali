.class public final enum LX/Cs1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cs1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cs1;

.field public static final enum DOMAIN_NAME:LX/Cs1;

.field public static final enum SHORT_DOMAIN_NAME:LX/Cs1;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1941629
    new-instance v0, LX/Cs1;

    const-string v1, "DOMAIN_NAME"

    invoke-direct {v0, v1, v2}, LX/Cs1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cs1;->DOMAIN_NAME:LX/Cs1;

    .line 1941630
    new-instance v0, LX/Cs1;

    const-string v1, "SHORT_DOMAIN_NAME"

    invoke-direct {v0, v1, v3}, LX/Cs1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cs1;->SHORT_DOMAIN_NAME:LX/Cs1;

    .line 1941631
    const/4 v0, 0x2

    new-array v0, v0, [LX/Cs1;

    sget-object v1, LX/Cs1;->DOMAIN_NAME:LX/Cs1;

    aput-object v1, v0, v2

    sget-object v1, LX/Cs1;->SHORT_DOMAIN_NAME:LX/Cs1;

    aput-object v1, v0, v3

    sput-object v0, LX/Cs1;->$VALUES:[LX/Cs1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1941632
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cs1;
    .locals 1

    .prologue
    .line 1941633
    const-class v0, LX/Cs1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cs1;

    return-object v0
.end method

.method public static values()[LX/Cs1;
    .locals 1

    .prologue
    .line 1941634
    sget-object v0, LX/Cs1;->$VALUES:[LX/Cs1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cs1;

    return-object v0
.end method
