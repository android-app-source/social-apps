.class public final LX/EGb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/EGe;


# direct methods
.method public constructor <init>(LX/EGe;)V
    .locals 0

    .prologue
    .line 2097453
    iput-object p1, p0, LX/EGb;->a:LX/EGe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 2097454
    iget-object v0, p0, LX/EGb;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->am(LX/EGe;)V

    .line 2097455
    return-void
.end method

.method public final g()Landroid/graphics/Rect;
    .locals 7

    .prologue
    .line 2097439
    iget-object v0, p0, LX/EGb;->a:LX/EGe;

    const/4 v2, 0x0

    .line 2097440
    iget-object v1, v0, LX/EGe;->W:LX/EHn;

    if-eqz v1, :cond_1

    .line 2097441
    iget-object v1, v0, LX/EGe;->W:LX/EHn;

    invoke-virtual {v1}, LX/EHn;->getPaddingLeft()I

    move-result v1

    iget-object v3, v0, LX/EGe;->W:LX/EHn;

    invoke-virtual {v3}, LX/EHn;->getPaddingRight()I

    move-result v3

    add-int/2addr v3, v1

    .line 2097442
    iget-object v1, v0, LX/EGe;->W:LX/EHn;

    invoke-virtual {v1}, LX/EHn;->getPaddingTop()I

    move-result v1

    iget-object v4, v0, LX/EGe;->W:LX/EHn;

    invoke-virtual {v4}, LX/EHn;->getPaddingBottom()I

    move-result v4

    add-int/2addr v1, v4

    .line 2097443
    :goto_0
    iget-object v4, v0, LX/EGe;->s:LX/EFs;

    .line 2097444
    iget-boolean v5, v4, LX/EFs;->d:Z

    move v4, v5

    .line 2097445
    if-nez v4, :cond_0

    .line 2097446
    iget v4, v0, LX/EGe;->L:I

    .line 2097447
    :goto_1
    new-instance v5, Landroid/graphics/Rect;

    iget v6, v0, LX/EGe;->K:I

    .line 2097448
    iget p0, v0, LX/EGe;->F:I

    move p0, p0

    .line 2097449
    sub-int/2addr v6, p0

    sub-int v3, v6, v3

    iget v6, v0, LX/EGe;->J:I

    .line 2097450
    iget p0, v0, LX/EGe;->G:I

    move p0, p0

    .line 2097451
    sub-int/2addr v6, p0

    sub-int v1, v6, v1

    iget v6, v0, LX/EGe;->L:I

    sub-int/2addr v1, v6

    add-int/2addr v1, v4

    invoke-direct {v5, v2, v4, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v0, v5

    .line 2097452
    return-object v0

    :cond_0
    move v4, v2

    goto :goto_1

    :cond_1
    move v1, v2

    move v3, v2

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2097437
    iget-object v0, p0, LX/EGb;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->ap(LX/EGe;)V

    .line 2097438
    return-void
.end method
