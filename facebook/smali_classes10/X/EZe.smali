.class public LX/EZe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/EZc;


# direct methods
.method private constructor <init>(LX/EZc;)V
    .locals 0

    .prologue
    .line 2139701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2139702
    iput-object p1, p0, LX/EZe;->a:LX/EZc;

    .line 2139703
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/EZe;
    .locals 1

    .prologue
    .line 2139742
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EZe;->a(Ljava/lang/String;LX/EZg;)LX/EZe;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;LX/EZg;)LX/EZe;
    .locals 2

    .prologue
    .line 2139728
    const-string v0, "native"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/EZe;

    .line 2139729
    const-string v1, "NativeCurve25519Provider"

    invoke-static {v1, p1}, LX/EZe;->b(Ljava/lang/String;LX/EZg;)LX/EZc;

    move-result-object v1

    move-object v1, v1

    .line 2139730
    invoke-direct {v0, v1}, LX/EZe;-><init>(LX/EZc;)V

    .line 2139731
    :goto_0
    return-object v0

    .line 2139732
    :cond_0
    const-string v0, "java"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, LX/EZe;

    .line 2139733
    const-string v1, "JavaCurve25519Provider"

    invoke-static {v1, p1}, LX/EZe;->b(Ljava/lang/String;LX/EZg;)LX/EZc;

    move-result-object v1

    move-object v1, v1

    .line 2139734
    invoke-direct {v0, v1}, LX/EZe;-><init>(LX/EZc;)V

    goto :goto_0

    .line 2139735
    :cond_1
    const-string v0, "j2me"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, LX/EZe;

    .line 2139736
    const-string v1, "J2meCurve25519Provider"

    invoke-static {v1, p1}, LX/EZe;->b(Ljava/lang/String;LX/EZg;)LX/EZc;

    move-result-object v1

    move-object v1, v1

    .line 2139737
    invoke-direct {v0, v1}, LX/EZe;-><init>(LX/EZc;)V

    goto :goto_0

    .line 2139738
    :cond_2
    const-string v0, "best"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, LX/EZe;

    .line 2139739
    const-string v1, "OpportunisticCurve25519Provider"

    invoke-static {v1, p1}, LX/EZe;->b(Ljava/lang/String;LX/EZg;)LX/EZc;

    move-result-object v1

    move-object v1, v1

    .line 2139740
    invoke-direct {v0, v1}, LX/EZe;-><init>(LX/EZc;)V

    goto :goto_0

    .line 2139741
    :cond_3
    new-instance v0, LX/EZj;

    invoke-direct {v0, p0}, LX/EZj;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Ljava/lang/String;LX/EZg;)LX/EZc;
    .locals 2

    .prologue
    .line 2139718
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "org.whispersystems.curve25519."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZc;

    .line 2139719
    if-eqz p1, :cond_0

    .line 2139720
    invoke-interface {v0, p1}, LX/EZc;->a(LX/EZg;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2139721
    :cond_0
    return-object v0

    .line 2139722
    :catch_0
    move-exception v0

    .line 2139723
    new-instance v1, LX/EZj;

    invoke-direct {v1, v0}, LX/EZj;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2139724
    :catch_1
    move-exception v0

    .line 2139725
    new-instance v1, LX/EZj;

    invoke-direct {v1, v0}, LX/EZj;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2139726
    :catch_2
    move-exception v0

    .line 2139727
    new-instance v1, LX/EZj;

    invoke-direct {v1, v0}, LX/EZj;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a([B[B[B)Z
    .locals 2

    .prologue
    .line 2139713
    if-eqz p1, :cond_0

    array-length v0, p1

    const/16 v1, 0x20

    if-eq v0, v1, :cond_1

    .line 2139714
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid public key!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2139715
    :cond_1
    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    array-length v0, p3

    const/16 v1, 0x40

    if-eq v0, v1, :cond_3

    .line 2139716
    :cond_2
    const/4 v0, 0x0

    .line 2139717
    :goto_0
    return v0

    :cond_3
    iget-object v0, p0, LX/EZe;->a:LX/EZc;

    invoke-interface {v0, p1, p2, p3}, LX/EZc;->verifySignature([B[B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public final a([B[B)[B
    .locals 2

    .prologue
    const/16 v1, 0x20

    .line 2139708
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2139709
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Keys must not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2139710
    :cond_1
    array-length v0, p1

    if-ne v0, v1, :cond_2

    array-length v0, p2

    if-eq v0, v1, :cond_3

    .line 2139711
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Keys must be 32 bytes!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2139712
    :cond_3
    iget-object v0, p0, LX/EZe;->a:LX/EZc;

    invoke-interface {v0, p2, p1}, LX/EZc;->calculateAgreement([B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public final b([B[B)[B
    .locals 2

    .prologue
    .line 2139704
    if-eqz p1, :cond_0

    array-length v0, p1

    const/16 v1, 0x20

    if-eq v0, v1, :cond_1

    .line 2139705
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid private key length!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2139706
    :cond_1
    iget-object v0, p0, LX/EZe;->a:LX/EZc;

    const/16 v1, 0x40

    invoke-interface {v0, v1}, LX/EZc;->a(I)[B

    move-result-object v0

    .line 2139707
    iget-object v1, p0, LX/EZe;->a:LX/EZc;

    invoke-interface {v1, v0, p1, p2}, LX/EZc;->calculateSignature([B[B[B)[B

    move-result-object v0

    return-object v0
.end method
