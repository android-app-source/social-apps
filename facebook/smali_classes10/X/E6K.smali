.class public final LX/E6K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E6L;


# direct methods
.method public constructor <init>(LX/E6L;)V
    .locals 0

    .prologue
    .line 2079979
    iput-object p1, p0, LX/E6K;->a:LX/E6L;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x10e437ed

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2079980
    iget-object v1, p0, LX/E6K;->a:LX/E6L;

    iget-object v1, v1, LX/E6L;->b:LX/E6N;

    if-eqz v1, :cond_0

    .line 2079981
    iget-object v1, p0, LX/E6K;->a:LX/E6L;

    iget-object v1, v1, LX/E6L;->b:LX/E6N;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2079982
    invoke-virtual {v1}, LX/E6N;->a()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2079983
    :cond_0
    :goto_0
    const v1, -0x20d773d5

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2079984
    :cond_1
    new-instance v6, LX/6WS;

    iget-object v3, v1, LX/E6N;->a:Landroid/content/Context;

    invoke-direct {v6, v3}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2079985
    const v3, 0x7f110009

    invoke-virtual {v6, v3}, LX/5OM;->b(I)V

    .line 2079986
    invoke-virtual {v6, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2079987
    invoke-virtual {v6}, LX/5OM;->c()LX/5OG;

    move-result-object v7

    .line 2079988
    const v3, 0x7f0d3209

    invoke-virtual {v7, v3}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    check-cast v3, LX/3Ai;

    .line 2079989
    const v8, 0x7f0d320a

    invoke-virtual {v7, v8}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    .line 2079990
    const v9, 0x7f0d320b

    invoke-virtual {v7, v9}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    .line 2079991
    invoke-static {v1}, LX/E6N;->c(LX/E6N;)Ljava/lang/String;

    move-result-object v10

    .line 2079992
    invoke-static {v1}, LX/E6N;->d(LX/E6N;)Ljava/lang/String;

    move-result-object v11

    .line 2079993
    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    invoke-static {v11}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 2079994
    iget-object v10, v1, LX/E6N;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v12, 0x7f082228

    new-array p0, v4, [Ljava/lang/Object;

    aput-object v11, p0, v5

    invoke-virtual {v10, v12, p0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, LX/3Ai;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2079995
    const v10, 0x7f082229

    invoke-virtual {v3, v10}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 2079996
    invoke-virtual {v3, v4}, LX/3Ai;->setVisible(Z)Landroid/view/MenuItem;

    .line 2079997
    iget-object v3, v1, LX/E6N;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v10, 0x7f08222a

    new-array v12, v4, [Ljava/lang/Object;

    aput-object v11, v12, v5

    invoke-virtual {v3, v10, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v8, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2079998
    iget-object v3, v1, LX/E6N;->i:LX/2jY;

    .line 2079999
    iget-object v10, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v10

    .line 2080000
    invoke-static {v3}, LX/2s8;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v4

    :goto_1
    invoke-interface {v8, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2080001
    :goto_2
    invoke-static {v1}, LX/E6N;->e(LX/E6N;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    move v5, v4

    :cond_2
    invoke-interface {v9, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2080002
    invoke-virtual {v7}, LX/5OG;->hasVisibleItems()Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2080003
    new-instance v3, LX/E6M;

    invoke-direct {v3, v1}, LX/E6M;-><init>(LX/E6N;)V

    .line 2080004
    iput-object v3, v6, LX/5OM;->p:LX/5OO;

    .line 2080005
    invoke-virtual {v6}, LX/0ht;->d()V

    .line 2080006
    goto/16 :goto_0

    :cond_3
    move v3, v5

    .line 2080007
    goto :goto_1

    .line 2080008
    :cond_4
    invoke-virtual {v3, v5}, LX/3Ai;->setVisible(Z)Landroid/view/MenuItem;

    .line 2080009
    invoke-interface {v8, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method
