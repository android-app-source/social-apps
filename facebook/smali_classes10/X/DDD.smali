.class public final LX/DDD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Po;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/DDY;

.field public final synthetic d:LX/DDF;


# direct methods
.method public constructor <init>(LX/DDF;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DDY;)V
    .locals 0

    .prologue
    .line 1975132
    iput-object p1, p0, LX/DDD;->d:LX/DDF;

    iput-object p2, p0, LX/DDD;->a:LX/1Po;

    iput-object p3, p0, LX/DDD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/DDD;->c:LX/DDY;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1975133
    iget-object v0, p0, LX/DDD;->d:LX/DDF;

    iget-object v0, v0, LX/DDF;->f:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1975134
    const/4 v1, 0x0

    iget-object v2, p0, LX/DDD;->a:LX/1Po;

    iget-object v3, p0, LX/DDD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, LX/DDD;->c:LX/DDY;

    .line 1975135
    invoke-static {v1, v2, v3, v4}, LX/DDF;->a(ZLX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DDY;)V

    .line 1975136
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1975137
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1975138
    const/4 v2, 0x0

    .line 1975139
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975140
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->m()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1975141
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975142
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    move v1, v0

    .line 1975143
    :goto_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975144
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1975145
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975146
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    move v0, v0

    .line 1975147
    if-nez v0, :cond_2

    const/4 v1, 0x0

    .line 1975148
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975149
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1975150
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975151
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_1
    add-int/lit8 v2, v0, 0x0

    .line 1975152
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975153
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1975154
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975155
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_2
    add-int/2addr v2, v0

    .line 1975156
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975157
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1975158
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975159
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_3
    add-int/2addr v2, v0

    .line 1975160
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975161
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1975162
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975163
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_4
    add-int/2addr v2, v0

    .line 1975164
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975165
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1975166
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975167
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_5
    add-int/2addr v2, v0

    .line 1975168
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975169
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1975170
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1975171
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    :cond_1
    add-int v0, v2, v1

    .line 1975172
    move v0, v0

    .line 1975173
    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 1975174
    :cond_2
    iget-object v0, p0, LX/DDD;->d:LX/DDF;

    iget-object v0, v0, LX/DDF;->f:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1975175
    const/4 v1, 0x0

    iget-object v2, p0, LX/DDD;->a:LX/1Po;

    iget-object v3, p0, LX/DDD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, LX/DDD;->c:LX/DDY;

    .line 1975176
    invoke-static {v1, v2, v3, v4}, LX/DDF;->a(ZLX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DDY;)V

    .line 1975177
    :cond_3
    return-void

    :cond_4
    move v1, v2

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 1975178
    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 1975179
    goto/16 :goto_2

    :cond_7
    move v0, v1

    .line 1975180
    goto :goto_3

    :cond_8
    move v0, v1

    .line 1975181
    goto :goto_4

    :cond_9
    move v0, v1

    .line 1975182
    goto :goto_5
.end method
