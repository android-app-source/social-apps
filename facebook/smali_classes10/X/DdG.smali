.class public LX/DdG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final c:LX/DdI;

.field private static final l:Ljava/lang/Object;


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/annotations/IsGlobalDeletePlaceholderEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final d:Landroid/content/Context;

.field private final e:LX/2Mk;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mDeletedMessagePlaceholders"
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/DdF;

.field private k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019314
    new-instance v0, LX/DdI;

    invoke-direct {v0}, LX/DdI;-><init>()V

    sput-object v0, LX/DdG;->c:LX/DdI;

    .line 2019315
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/DdG;->l:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/2Mk;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsGlobalDeletePlaceholderEnabled;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Mk;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019317
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/DdG;->f:Ljava/util/Map;

    .line 2019318
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/DdG;->g:Ljava/util/Set;

    .line 2019319
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/DdG;->h:Ljava/util/Map;

    .line 2019320
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/DdG;->i:Ljava/util/Set;

    .line 2019321
    iput-object p1, p0, LX/DdG;->d:Landroid/content/Context;

    .line 2019322
    iput-object p2, p0, LX/DdG;->a:LX/0Or;

    .line 2019323
    iput-object p3, p0, LX/DdG;->e:LX/2Mk;

    .line 2019324
    iput-object p4, p0, LX/DdG;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2019325
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DdG;->k:Z

    .line 2019326
    return-void
.end method

.method public static a(LX/0QB;)LX/DdG;
    .locals 10

    .prologue
    .line 2019327
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2019328
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2019329
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2019330
    if-nez v1, :cond_0

    .line 2019331
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2019332
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2019333
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2019334
    sget-object v1, LX/DdG;->l:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2019335
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2019336
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2019337
    :cond_1
    if-nez v1, :cond_4

    .line 2019338
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2019339
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2019340
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2019341
    new-instance v9, LX/DdG;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v7, 0x14e0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v7

    check-cast v7, LX/2Mk;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v9, v1, p0, v7, v8}, LX/DdG;-><init>(Landroid/content/Context;LX/0Or;LX/2Mk;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 2019342
    move-object v1, v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2019343
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2019344
    if-nez v1, :cond_2

    .line 2019345
    sget-object v0, LX/DdG;->l:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdG;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2019346
    :goto_1
    if-eqz v0, :cond_3

    .line 2019347
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2019348
    :goto_3
    check-cast v0, LX/DdG;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2019349
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2019350
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2019351
    :catchall_1
    move-exception v0

    .line 2019352
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2019353
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2019354
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2019355
    :cond_2
    :try_start_8
    sget-object v0, LX/DdG;->l:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdG;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/DdG;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 2019356
    iget-object v1, p0, LX/DdG;->f:Ljava/util/Map;

    monitor-enter v1

    .line 2019357
    :try_start_0
    iget-object v0, p0, LX/DdG;->f:Ljava/util/Map;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2019358
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2019359
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2019360
    iget-object v0, p0, LX/DdG;->f:Ljava/util/Map;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019361
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2019362
    iget-object v0, p0, LX/DdG;->j:LX/DdF;

    if-nez v0, :cond_1

    .line 2019363
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DdG;->k:Z

    .line 2019364
    :cond_1
    iget-object v0, p0, LX/DdG;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019365
    return-void

    .line 2019366
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 8

    .prologue
    .line 2019367
    iget-object v1, p0, LX/DdG;->f:Ljava/util/Map;

    monitor-enter v1

    .line 2019368
    :try_start_0
    iget-object v0, p0, LX/DdG;->f:Ljava/util/Map;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2019369
    monitor-exit v1

    .line 2019370
    :cond_0
    return-void

    .line 2019371
    :cond_1
    iget-object v0, p0, LX/DdG;->f:Ljava/util/Map;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2019372
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2019373
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2019374
    iget-wide v4, v0, Lcom/facebook/messaging/model/messages/Message;->c:J

    iget-wide v6, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_2

    .line 2019375
    iget-object v1, p0, LX/DdG;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledFuture;

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2019376
    invoke-static {p0, v0}, LX/DdG;->b(LX/DdG;Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0

    .line 2019377
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 2019378
    iget-object v0, p0, LX/DdG;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DdG;->e:LX/2Mk;

    invoke-virtual {v0, p1}, LX/2Mk;->s(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DdG;->i:Ljava/util/Set;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2019379
    :cond_0
    :goto_0
    return-void

    .line 2019380
    :cond_1
    iget-object v1, p0, LX/DdG;->f:Ljava/util/Map;

    monitor-enter v1

    .line 2019381
    :try_start_0
    iget-object v6, p0, LX/DdG;->d:Landroid/content/Context;

    const v7, 0x7f08025f

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p2, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2019382
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v7

    iget-object v8, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    move-result-object v7

    .line 2019383
    iput-object v6, v7, LX/6f7;->f:Ljava/lang/String;

    .line 2019384
    move-object v6, v7

    .line 2019385
    iget-object v7, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2019386
    iput-object v7, v6, LX/6f7;->n:Ljava/lang/String;

    .line 2019387
    move-object v6, v6

    .line 2019388
    iget-object v7, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2019389
    iput-object v7, v6, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2019390
    move-object v6, v6

    .line 2019391
    iget-object v7, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2019392
    iput-object v7, v6, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2019393
    move-object v6, v6

    .line 2019394
    sget-object v7, LX/2uW;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/2uW;

    .line 2019395
    iput-object v7, v6, LX/6f7;->l:LX/2uW;

    .line 2019396
    move-object v6, v6

    .line 2019397
    iget-wide v8, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 2019398
    iput-wide v8, v6, LX/6f7;->c:J

    .line 2019399
    move-object v6, v6

    .line 2019400
    iget-wide v8, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    .line 2019401
    iput-wide v8, v6, LX/6f7;->d:J

    .line 2019402
    move-object v6, v6

    .line 2019403
    invoke-virtual {v6}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v6

    move-object v3, v6

    .line 2019404
    iget-object v2, p0, LX/DdG;->f:Ljava/util/Map;

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 2019405
    if-nez v2, :cond_2

    .line 2019406
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 2019407
    iget-object v4, p0, LX/DdG;->f:Ljava/util/Map;

    iget-object v5, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019408
    :cond_2
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2019409
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_3

    .line 2019410
    sget-object v4, LX/DdG;->c:LX/DdI;

    invoke-static {v2, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2019411
    :cond_3
    iget-object v2, p0, LX/DdG;->g:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2019412
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
