.class public final LX/E4l;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E4m;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/9uh;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public final synthetic e:LX/E4m;


# direct methods
.method public constructor <init>(LX/E4m;)V
    .locals 1

    .prologue
    .line 2077223
    iput-object p1, p0, LX/E4l;->e:LX/E4m;

    .line 2077224
    move-object v0, p1

    .line 2077225
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2077226
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2077227
    const-string v0, "ReactionBreadcrumbsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2077228
    if-ne p0, p1, :cond_1

    .line 2077229
    :cond_0
    :goto_0
    return v0

    .line 2077230
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2077231
    goto :goto_0

    .line 2077232
    :cond_3
    check-cast p1, LX/E4l;

    .line 2077233
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2077234
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2077235
    if-eq v2, v3, :cond_0

    .line 2077236
    iget-object v2, p0, LX/E4l;->a:LX/0Px;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E4l;->a:LX/0Px;

    iget-object v3, p1, LX/E4l;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2077237
    goto :goto_0

    .line 2077238
    :cond_5
    iget-object v2, p1, LX/E4l;->a:LX/0Px;

    if-nez v2, :cond_4

    .line 2077239
    :cond_6
    iget-object v2, p0, LX/E4l;->b:LX/2km;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E4l;->b:LX/2km;

    iget-object v3, p1, LX/E4l;->b:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2077240
    goto :goto_0

    .line 2077241
    :cond_8
    iget-object v2, p1, LX/E4l;->b:LX/2km;

    if-nez v2, :cond_7

    .line 2077242
    :cond_9
    iget-object v2, p0, LX/E4l;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E4l;->c:Ljava/lang/String;

    iget-object v3, p1, LX/E4l;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2077243
    goto :goto_0

    .line 2077244
    :cond_b
    iget-object v2, p1, LX/E4l;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2077245
    :cond_c
    iget-object v2, p0, LX/E4l;->d:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/E4l;->d:Ljava/lang/String;

    iget-object v3, p1, LX/E4l;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2077246
    goto :goto_0

    .line 2077247
    :cond_d
    iget-object v2, p1, LX/E4l;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
