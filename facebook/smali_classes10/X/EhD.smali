.class public final enum LX/EhD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EhD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EhD;

.field public static final enum APP:LX/EhD;

.field public static final enum FRIEND_LIST:LX/EhD;

.field public static final enum GROUP:LX/EhD;

.field public static final enum INTEREST_LIST:LX/EhD;

.field public static final enum NEWSFEED:LX/EhD;

.field public static final enum PROFILE:LX/EhD;

.field public static final enum UNKNOWN:LX/EhD;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2158208
    new-instance v0, LX/EhD;

    const-string v1, "APP"

    invoke-direct {v0, v1, v3}, LX/EhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhD;->APP:LX/EhD;

    .line 2158209
    new-instance v0, LX/EhD;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v4}, LX/EhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhD;->PROFILE:LX/EhD;

    .line 2158210
    new-instance v0, LX/EhD;

    const-string v1, "NEWSFEED"

    invoke-direct {v0, v1, v5}, LX/EhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhD;->NEWSFEED:LX/EhD;

    .line 2158211
    new-instance v0, LX/EhD;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v6}, LX/EhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhD;->GROUP:LX/EhD;

    .line 2158212
    new-instance v0, LX/EhD;

    const-string v1, "FRIEND_LIST"

    invoke-direct {v0, v1, v7}, LX/EhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhD;->FRIEND_LIST:LX/EhD;

    .line 2158213
    new-instance v0, LX/EhD;

    const-string v1, "INTEREST_LIST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhD;->INTEREST_LIST:LX/EhD;

    .line 2158214
    new-instance v0, LX/EhD;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EhD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhD;->UNKNOWN:LX/EhD;

    .line 2158215
    const/4 v0, 0x7

    new-array v0, v0, [LX/EhD;

    sget-object v1, LX/EhD;->APP:LX/EhD;

    aput-object v1, v0, v3

    sget-object v1, LX/EhD;->PROFILE:LX/EhD;

    aput-object v1, v0, v4

    sget-object v1, LX/EhD;->NEWSFEED:LX/EhD;

    aput-object v1, v0, v5

    sget-object v1, LX/EhD;->GROUP:LX/EhD;

    aput-object v1, v0, v6

    sget-object v1, LX/EhD;->FRIEND_LIST:LX/EhD;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EhD;->INTEREST_LIST:LX/EhD;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EhD;->UNKNOWN:LX/EhD;

    aput-object v2, v0, v1

    sput-object v0, LX/EhD;->$VALUES:[LX/EhD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2158207
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static lookup(Ljava/lang/String;)LX/EhD;
    .locals 1

    .prologue
    .line 2158205
    :try_start_0
    invoke-static {p0}, LX/EhD;->valueOf(Ljava/lang/String;)LX/EhD;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2158206
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/EhD;->UNKNOWN:LX/EhD;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/EhD;
    .locals 1

    .prologue
    .line 2158203
    const-class v0, LX/EhD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EhD;

    return-object v0
.end method

.method public static values()[LX/EhD;
    .locals 1

    .prologue
    .line 2158204
    sget-object v0, LX/EhD;->$VALUES:[LX/EhD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EhD;

    return-object v0
.end method
