.class public abstract LX/Cud;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/CnQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/CnQ",
        "<",
        "LX/Clj;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/Cui;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/Cui;

.field private final c:LX/Clj;

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1946981
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Cud;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1946982
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1946979
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Cud;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946980
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1946951
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946952
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/Cui;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LX/Cud;->a:Ljava/util/EnumMap;

    .line 1946953
    sget-object v0, LX/Cui;->NONE:LX/Cui;

    iput-object v0, p0, LX/Cud;->b:LX/Cui;

    .line 1946954
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cud;->d:Z

    .line 1946955
    invoke-virtual {p0}, LX/Cud;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1946956
    iget-object v0, p0, LX/Cud;->a:Ljava/util/EnumMap;

    sget-object v1, LX/Cui;->PLAY_ICON:LX/Cui;

    invoke-virtual {p0}, LX/Cud;->getPlayIcon()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1946957
    iget-object v0, p0, LX/Cud;->a:Ljava/util/EnumMap;

    sget-object v1, LX/Cui;->PAUSE_ICON:LX/Cui;

    invoke-virtual {p0}, LX/Cud;->getPauseIcon()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1946958
    iget-object v0, p0, LX/Cud;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1946959
    if-eqz v0, :cond_0

    .line 1946960
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1946961
    :cond_1
    new-instance v0, LX/Clj;

    invoke-direct {v0}, LX/Clj;-><init>()V

    iput-object v0, p0, LX/Cud;->c:LX/Clj;

    .line 1946962
    sget-object v0, LX/Cui;->NONE:LX/Cui;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/Cud;->a(LX/Cui;Z)V

    .line 1946963
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1946975
    iget-object v0, p0, LX/Cud;->b:LX/Cui;

    sget-object v1, LX/Cui;->NONE:LX/Cui;

    if-eq v0, v1, :cond_0

    .line 1946976
    iget-object v0, p0, LX/Cud;->a:Ljava/util/EnumMap;

    iget-object v1, p0, LX/Cud;->b:LX/Cui;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cue;

    .line 1946977
    invoke-virtual {v0}, LX/CsC;->b()V

    .line 1946978
    :cond_0
    return-void
.end method

.method public final a(LX/Cui;Z)V
    .locals 4

    .prologue
    .line 1946965
    sget-object v0, LX/Cui;->NONE:LX/Cui;

    invoke-static {p1, v0}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cui;

    iput-object v0, p0, LX/Cud;->b:LX/Cui;

    .line 1946966
    iget-object v0, p0, LX/Cud;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1946967
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1946968
    if-eqz v1, :cond_0

    .line 1946969
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    iget-object v3, p0, LX/Cud;->b:LX/Cui;

    if-ne v0, v3, :cond_1

    iget-boolean v0, p0, LX/Cud;->d:Z

    if-eqz v0, :cond_1

    .line 1946970
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1946971
    check-cast v1, LX/Cue;

    invoke-virtual {v1, p2}, LX/Cue;->setLoading(Z)V

    goto :goto_0

    .line 1946972
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1946973
    :cond_2
    invoke-virtual {p0}, LX/Cud;->invalidate()V

    .line 1946974
    return-void
.end method

.method public abstract a(LX/Cuw;LX/Cub;)V
.end method

.method public final c()Landroid/view/View;
    .locals 0

    .prologue
    .line 1946964
    return-object p0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1946983
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic getAnnotation()LX/ClU;
    .locals 1

    .prologue
    .line 1946949
    invoke-virtual {p0}, LX/Cud;->getAnnotation()LX/Clj;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotation()LX/Clj;
    .locals 1

    .prologue
    .line 1946948
    iget-object v0, p0, LX/Cud;->c:LX/Clj;

    return-object v0
.end method

.method public abstract getContentView()I
.end method

.method public abstract getPauseIcon()Landroid/view/View;
.end method

.method public abstract getPlayIcon()Landroid/view/View;
.end method

.method public setIsOverlay(Z)V
    .locals 0

    .prologue
    .line 1946950
    return-void
.end method
