.class public final enum LX/DSa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DSa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DSa;

.field public static final enum GroupMemberRow:LX/DSa;

.field public static final enum Header:LX/DSa;

.field public static final enum LoadingBar:LX/DSa;

.field public static final enum SeeMoreBar:LX/DSa;

.field public static final enum SelfIntro:LX/DSa;

.field private static final values:[LX/DSa;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1999725
    new-instance v0, LX/DSa;

    const-string v1, "GroupMemberRow"

    invoke-direct {v0, v1, v2}, LX/DSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSa;->GroupMemberRow:LX/DSa;

    .line 1999726
    new-instance v0, LX/DSa;

    const-string v1, "Header"

    invoke-direct {v0, v1, v3}, LX/DSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSa;->Header:LX/DSa;

    .line 1999727
    new-instance v0, LX/DSa;

    const-string v1, "LoadingBar"

    invoke-direct {v0, v1, v4}, LX/DSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSa;->LoadingBar:LX/DSa;

    .line 1999728
    new-instance v0, LX/DSa;

    const-string v1, "SeeMoreBar"

    invoke-direct {v0, v1, v5}, LX/DSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSa;->SeeMoreBar:LX/DSa;

    .line 1999729
    new-instance v0, LX/DSa;

    const-string v1, "SelfIntro"

    invoke-direct {v0, v1, v6}, LX/DSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSa;->SelfIntro:LX/DSa;

    .line 1999730
    const/4 v0, 0x5

    new-array v0, v0, [LX/DSa;

    sget-object v1, LX/DSa;->GroupMemberRow:LX/DSa;

    aput-object v1, v0, v2

    sget-object v1, LX/DSa;->Header:LX/DSa;

    aput-object v1, v0, v3

    sget-object v1, LX/DSa;->LoadingBar:LX/DSa;

    aput-object v1, v0, v4

    sget-object v1, LX/DSa;->SeeMoreBar:LX/DSa;

    aput-object v1, v0, v5

    sget-object v1, LX/DSa;->SelfIntro:LX/DSa;

    aput-object v1, v0, v6

    sput-object v0, LX/DSa;->$VALUES:[LX/DSa;

    .line 1999731
    invoke-static {}, LX/DSa;->values()[LX/DSa;

    move-result-object v0

    sput-object v0, LX/DSa;->values:[LX/DSa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1999732
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromOrdinal(I)LX/DSa;
    .locals 1

    .prologue
    .line 1999733
    sget-object v0, LX/DSa;->values:[LX/DSa;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/DSa;
    .locals 1

    .prologue
    .line 1999734
    const-class v0, LX/DSa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DSa;

    return-object v0
.end method

.method public static values()[LX/DSa;
    .locals 1

    .prologue
    .line 1999735
    sget-object v0, LX/DSa;->$VALUES:[LX/DSa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DSa;

    return-object v0
.end method
