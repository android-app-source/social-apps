.class public LX/Dpx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/121;

.field public final c:Landroid/content/Context;

.field public final d:Ljava/lang/Boolean;

.field public final e:LX/DbA;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/121;Landroid/content/Context;Ljava/lang/Boolean;LX/DbA;)V
    .locals 4
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2047739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047740
    iput-object p1, p0, LX/Dpx;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2047741
    iput-object p2, p0, LX/Dpx;->b:LX/121;

    .line 2047742
    iput-object p3, p0, LX/Dpx;->c:Landroid/content/Context;

    .line 2047743
    iput-object p4, p0, LX/Dpx;->d:Ljava/lang/Boolean;

    .line 2047744
    iput-object p5, p0, LX/Dpx;->e:LX/DbA;

    .line 2047745
    iget-object v0, p0, LX/Dpx;->b:LX/121;

    sget-object v1, LX/0yY;->VIEW_TIMELINE_INTERSTITIAL:LX/0yY;

    iget-object v2, p0, LX/Dpx;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080e41

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Dpv;

    invoke-direct {v3, p0}, LX/Dpv;-><init>(LX/Dpx;)V

    invoke-virtual {v0, v1, v2, v3}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 2047746
    return-void
.end method
