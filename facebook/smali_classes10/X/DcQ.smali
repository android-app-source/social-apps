.class public LX/DcQ;
.super LX/2s5;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/03V;

.field private final c:I

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLInterfaces$AvailableMenusQuery$PageProductLists$Nodes;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2018386
    const-class v0, LX/DcQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DcQ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0gc;LX/0Px;Ljava/lang/Integer;)V
    .locals 3
    .param p2    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0gc;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLInterfaces$AvailableMenusQuery$PageProductLists$Nodes;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018361
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2018362
    iput-object p3, p0, LX/DcQ;->e:LX/0Px;

    .line 2018363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/DcQ;->d:Ljava/util/ArrayList;

    .line 2018364
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2018365
    iget-object v1, p0, LX/DcQ;->d:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2018366
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2018367
    :cond_0
    iput-object p1, p0, LX/DcQ;->b:LX/03V;

    .line 2018368
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/DcQ;->c:I

    .line 2018369
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 2018382
    iget-object v0, p0, LX/DcQ;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel$PageProductListsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel$PageProductListsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2018383
    iget-object v0, p0, LX/DcQ;->b:LX/03V;

    sget-object v1, LX/DcQ;->a:Ljava/lang/String;

    const-string v2, "Null tab title"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018384
    const/4 v0, 0x0

    .line 2018385
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DcQ;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel$PageProductListsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel$PageProductListsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 2018371
    iget-object v0, p0, LX/DcQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;

    move-object v0, v0

    .line 2018372
    if-nez v0, :cond_0

    .line 2018373
    iget-object v0, p0, LX/DcQ;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel$PageProductListsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel$PageProductListsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, LX/DcQ;->c:I

    .line 2018374
    new-instance v2, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;

    invoke-direct {v2}, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;-><init>()V

    .line 2018375
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2018376
    const-string v4, "local_content_menu_id"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018377
    const-string v4, "local_content_padding_top"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2018378
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2018379
    move-object v0, v2

    .line 2018380
    iget-object v1, p0, LX/DcQ;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2018381
    :cond_0
    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2018370
    iget-object v0, p0, LX/DcQ;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
