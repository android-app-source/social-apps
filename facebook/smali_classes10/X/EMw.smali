.class public final LX/EMw;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EMx;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public final synthetic b:LX/EMx;


# direct methods
.method public constructor <init>(LX/EMx;)V
    .locals 1

    .prologue
    .line 2111596
    iput-object p1, p0, LX/EMw;->b:LX/EMx;

    .line 2111597
    move-object v0, p1

    .line 2111598
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2111599
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2111600
    const-string v0, "SearchResultsTimelineHeaderCardComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2111601
    if-ne p0, p1, :cond_1

    .line 2111602
    :cond_0
    :goto_0
    return v0

    .line 2111603
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2111604
    goto :goto_0

    .line 2111605
    :cond_3
    check-cast p1, LX/EMw;

    .line 2111606
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2111607
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2111608
    if-eq v2, v3, :cond_0

    .line 2111609
    iget-object v2, p0, LX/EMw;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/EMw;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/EMw;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2111610
    goto :goto_0

    .line 2111611
    :cond_4
    iget-object v2, p1, LX/EMw;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
