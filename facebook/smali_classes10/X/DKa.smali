.class public LX/DKa;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field public final b:LX/18V;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/DKb;

.field public final f:LX/DWD;

.field public final g:LX/DKc;

.field private final h:LX/DKJ;

.field public final i:LX/0SI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1987254
    const-class v0, LX/DKa;

    sput-object v0, LX/DKa;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/18V;LX/DKb;LX/DWD;Ljava/util/concurrent/ExecutorService;LX/DKc;LX/DKJ;LX/0SI;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1987255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1987256
    iput-object p1, p0, LX/DKa;->b:LX/18V;

    .line 1987257
    iput-object p6, p0, LX/DKa;->h:LX/DKJ;

    .line 1987258
    iput-object p5, p0, LX/DKa;->g:LX/DKc;

    .line 1987259
    iput-object p2, p0, LX/DKa;->e:LX/DKb;

    .line 1987260
    iput-object p3, p0, LX/DKa;->f:LX/DWD;

    .line 1987261
    iput-object p4, p0, LX/DKa;->c:Ljava/util/concurrent/ExecutorService;

    .line 1987262
    iput-object p7, p0, LX/DKa;->i:LX/0SI;

    .line 1987263
    iput-object p8, p0, LX/DKa;->d:Ljava/util/concurrent/ExecutorService;

    .line 1987264
    return-void
.end method

.method public static a(LX/0QB;)LX/DKa;
    .locals 1

    .prologue
    .line 1987265
    invoke-static {p0}, LX/DKa;->b(LX/0QB;)LX/DKa;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/DKa;LX/4Fi;LX/0Px;LX/0Px;LX/F4e;Landroid/net/Uri;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4Fi;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$GroupCreationActionProgressListener;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1987266
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 1987267
    iget-object v10, p0, LX/DKa;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p4

    move-object v5, p2

    move-object v6, p3

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;-><init>(LX/DKa;LX/4Fi;Lcom/google/common/util/concurrent/SettableFuture;LX/F4e;LX/0Px;LX/0Px;Ljava/lang/String;ZLandroid/net/Uri;)V

    const v1, 0x75ba3030

    invoke-static {v10, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1987268
    return-object v3
.end method

.method public static a$redex0(LX/DKa;LX/DKF;LX/F4e;)V
    .locals 3

    .prologue
    .line 1987269
    iget-object v0, p0, LX/DKa;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$1;-><init>(LX/DKa;LX/F4e;LX/DKF;)V

    const v2, -0x20a7965b

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1987270
    return-void
.end method

.method public static b(LX/0QB;)LX/DKa;
    .locals 15

    .prologue
    .line 1987271
    new-instance v0, LX/DKa;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v1

    check-cast v1, LX/18V;

    .line 1987272
    new-instance v3, LX/DKb;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-direct {v3, v2}, LX/DKb;-><init>(LX/0tX;)V

    .line 1987273
    move-object v2, v3

    .line 1987274
    check-cast v2, LX/DKb;

    invoke-static {p0}, LX/DWD;->b(LX/0QB;)LX/DWD;

    move-result-object v3

    check-cast v3, LX/DWD;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    .line 1987275
    new-instance v5, LX/DKc;

    invoke-direct {v5}, LX/DKc;-><init>()V

    .line 1987276
    move-object v5, v5

    .line 1987277
    move-object v5, v5

    .line 1987278
    check-cast v5, LX/DKc;

    .line 1987279
    new-instance v9, LX/DKJ;

    invoke-static {p0}, LX/8OJ;->b(LX/0QB;)LX/8OJ;

    move-result-object v10

    check-cast v10, LX/8OJ;

    invoke-static {p0}, LX/73w;->b(LX/0QB;)LX/73w;

    move-result-object v11

    check-cast v11, LX/73w;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0cX;->a(LX/0QB;)LX/0cX;

    move-result-object v14

    check-cast v14, LX/0cX;

    invoke-direct/range {v9 .. v14}, LX/DKJ;-><init>(LX/8OJ;LX/73w;LX/03V;Ljava/util/concurrent/ExecutorService;LX/0cX;)V

    .line 1987280
    move-object v6, v9

    .line 1987281
    check-cast v6, LX/DKJ;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v7

    check-cast v7, LX/0SI;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v0 .. v8}, LX/DKa;-><init>(LX/18V;LX/DKb;LX/DWD;Ljava/util/concurrent/ExecutorService;LX/DKc;LX/DKJ;LX/0SI;Ljava/util/concurrent/ExecutorService;)V

    .line 1987282
    return-object v0
.end method


# virtual methods
.method public final a(LX/4Fi;LX/0Px;LX/0Px;LX/F4e;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4Fi;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$GroupCreationActionProgressListener;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1987283
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v7}, LX/DKa;->a(LX/DKa;LX/4Fi;LX/0Px;LX/0Px;LX/F4e;Landroid/net/Uri;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4Fi;LX/0Px;LX/0Px;LX/F4e;Landroid/net/Uri;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4Fi;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$GroupCreationActionProgressListener;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1987284
    iget-object v0, p0, LX/DKa;->i:LX/0SI;

    invoke-interface {v0, p7}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1987285
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v7}, LX/DKa;->a(LX/DKa;LX/4Fi;LX/0Px;LX/0Px;LX/F4e;Landroid/net/Uri;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;Lcom/google/common/util/concurrent/SettableFuture;LX/F4e;Z)V
    .locals 7
    .param p4    # LX/F4e;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$GroupCreationActionProgressListener;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1987286
    iget-object v0, p0, LX/DKa;->h:LX/DKJ;

    invoke-virtual {v0, p2, p1}, LX/DKJ;->a(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 1987287
    new-instance v0, LX/DKZ;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, LX/DKZ;-><init>(LX/DKa;LX/F4e;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;Z)V

    iget-object v1, p0, LX/DKa;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1987288
    return-void
.end method
