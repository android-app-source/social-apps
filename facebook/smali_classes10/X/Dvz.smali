.class public final enum LX/Dvz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dvz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dvz;

.field public static final enum LANDSCAPE:LX/Dvz;

.field public static final enum PORTRAIT:LX/Dvz;

.field public static final enum SQUARE:LX/Dvz;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2059473
    new-instance v0, LX/Dvz;

    const-string v1, "SQUARE"

    invoke-direct {v0, v1, v2}, LX/Dvz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dvz;->SQUARE:LX/Dvz;

    .line 2059474
    new-instance v0, LX/Dvz;

    const-string v1, "PORTRAIT"

    invoke-direct {v0, v1, v3}, LX/Dvz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dvz;->PORTRAIT:LX/Dvz;

    .line 2059475
    new-instance v0, LX/Dvz;

    const-string v1, "LANDSCAPE"

    invoke-direct {v0, v1, v4}, LX/Dvz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dvz;->LANDSCAPE:LX/Dvz;

    .line 2059476
    const/4 v0, 0x3

    new-array v0, v0, [LX/Dvz;

    sget-object v1, LX/Dvz;->SQUARE:LX/Dvz;

    aput-object v1, v0, v2

    sget-object v1, LX/Dvz;->PORTRAIT:LX/Dvz;

    aput-object v1, v0, v3

    sget-object v1, LX/Dvz;->LANDSCAPE:LX/Dvz;

    aput-object v1, v0, v4

    sput-object v0, LX/Dvz;->$VALUES:[LX/Dvz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2059477
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dvz;
    .locals 1

    .prologue
    .line 2059478
    const-class v0, LX/Dvz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dvz;

    return-object v0
.end method

.method public static values()[LX/Dvz;
    .locals 1

    .prologue
    .line 2059479
    sget-object v0, LX/Dvz;->$VALUES:[LX/Dvz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dvz;

    return-object v0
.end method
