.class public LX/EJH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1vg;


# direct methods
.method public constructor <init>(LX/1vg;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2103752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2103753
    iput-object p1, p0, LX/EJH;->a:LX/1vg;

    .line 2103754
    return-void
.end method

.method public static a(LX/0QB;)LX/EJH;
    .locals 4

    .prologue
    .line 2103741
    const-class v1, LX/EJH;

    monitor-enter v1

    .line 2103742
    :try_start_0
    sget-object v0, LX/EJH;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103743
    sput-object v2, LX/EJH;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103744
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103745
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103746
    new-instance p0, LX/EJH;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-direct {p0, v3}, LX/EJH;-><init>(LX/1vg;)V

    .line 2103747
    move-object v0, p0

    .line 2103748
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103749
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EJH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103750
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
