.class public final LX/EcQ;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EcP;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EcQ;",
        ">;",
        "LX/EcP;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2146640
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2146641
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcQ;->c:LX/EWc;

    .line 2146642
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2146643
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2146644
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcQ;->c:LX/EWc;

    .line 2146645
    return-void
.end method

.method private d(LX/EWY;)LX/EcQ;
    .locals 1

    .prologue
    .line 2146646
    instance-of v0, p1, LX/EcR;

    if-eqz v0, :cond_0

    .line 2146647
    check-cast p1, LX/EcR;

    invoke-virtual {p0, p1}, LX/EcQ;->a(LX/EcR;)LX/EcQ;

    move-result-object p0

    .line 2146648
    :goto_0
    return-object p0

    .line 2146649
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EcQ;
    .locals 4

    .prologue
    .line 2146650
    const/4 v2, 0x0

    .line 2146651
    :try_start_0
    sget-object v0, LX/EcR;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcR;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2146652
    if-eqz v0, :cond_0

    .line 2146653
    invoke-virtual {p0, v0}, LX/EcQ;->a(LX/EcR;)LX/EcQ;

    .line 2146654
    :cond_0
    return-object p0

    .line 2146655
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2146656
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2146657
    check-cast v0, LX/EcR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2146658
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2146659
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2146660
    invoke-virtual {p0, v1}, LX/EcQ;->a(LX/EcR;)LX/EcQ;

    :cond_1
    throw v0

    .line 2146661
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static w()LX/EcQ;
    .locals 1

    .prologue
    .line 2146662
    new-instance v0, LX/EcQ;

    invoke-direct {v0}, LX/EcQ;-><init>()V

    return-object v0
.end method

.method private x()LX/EcQ;
    .locals 2

    .prologue
    .line 2146663
    invoke-static {}, LX/EcQ;->w()LX/EcQ;

    move-result-object v0

    invoke-virtual {p0}, LX/EcQ;->m()LX/EcR;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcQ;->a(LX/EcR;)LX/EcQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2146664
    invoke-direct {p0, p1}, LX/EcQ;->d(LX/EWY;)LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2146665
    invoke-direct {p0, p1, p2}, LX/EcQ;->d(LX/EWd;LX/EYZ;)LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/EcQ;
    .locals 1

    .prologue
    .line 2146666
    iget v0, p0, LX/EcQ;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EcQ;->a:I

    .line 2146667
    iput p1, p0, LX/EcQ;->b:I

    .line 2146668
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146669
    return-object p0
.end method

.method public final a(LX/EWc;)LX/EcQ;
    .locals 1

    .prologue
    .line 2146609
    if-nez p1, :cond_0

    .line 2146610
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146611
    :cond_0
    iget v0, p0, LX/EcQ;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EcQ;->a:I

    .line 2146612
    iput-object p1, p0, LX/EcQ;->c:LX/EWc;

    .line 2146613
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146614
    return-object p0
.end method

.method public final a(LX/EcR;)LX/EcQ;
    .locals 2

    .prologue
    .line 2146670
    sget-object v0, LX/EcR;->c:LX/EcR;

    move-object v0, v0

    .line 2146671
    if-ne p1, v0, :cond_0

    .line 2146672
    :goto_0
    return-object p0

    .line 2146673
    :cond_0
    const/4 v0, 0x1

    .line 2146674
    iget v1, p1, LX/EcR;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    move v0, v0

    .line 2146675
    if-eqz v0, :cond_1

    .line 2146676
    iget v0, p1, LX/EcR;->index_:I

    move v0, v0

    .line 2146677
    invoke-virtual {p0, v0}, LX/EcQ;->a(I)LX/EcQ;

    .line 2146678
    :cond_1
    iget v0, p1, LX/EcR;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2146679
    if-eqz v0, :cond_2

    .line 2146680
    iget-object v0, p1, LX/EcR;->key_:LX/EWc;

    move-object v0, v0

    .line 2146681
    invoke-virtual {p0, v0}, LX/EcQ;->a(LX/EWc;)LX/EcQ;

    .line 2146682
    :cond_2
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2146683
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2146684
    invoke-direct {p0, p1, p2}, LX/EcQ;->d(LX/EWd;LX/EYZ;)LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2146639
    invoke-direct {p0}, LX/EcQ;->x()LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2146685
    invoke-direct {p0, p1, p2}, LX/EcQ;->d(LX/EWd;LX/EYZ;)LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2146605
    invoke-direct {p0}, LX/EcQ;->x()LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2146606
    invoke-direct {p0, p1}, LX/EcQ;->d(LX/EWY;)LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2146607
    invoke-direct {p0}, LX/EcQ;->x()LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2146608
    sget-object v0, LX/Eck;->f:LX/EYn;

    const-class v1, LX/EcR;

    const-class v2, LX/EcQ;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2146615
    sget-object v0, LX/Eck;->e:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2146616
    invoke-direct {p0}, LX/EcQ;->x()LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2146617
    invoke-virtual {p0}, LX/EcQ;->m()LX/EcR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2146618
    invoke-virtual {p0}, LX/EcQ;->l()LX/EcR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2146619
    invoke-virtual {p0}, LX/EcQ;->m()LX/EcR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2146620
    invoke-virtual {p0}, LX/EcQ;->l()LX/EcR;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EcR;
    .locals 2

    .prologue
    .line 2146621
    invoke-virtual {p0}, LX/EcQ;->m()LX/EcR;

    move-result-object v0

    .line 2146622
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2146623
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2146624
    :cond_0
    return-object v0
.end method

.method public final m()LX/EcR;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2146625
    new-instance v2, LX/EcR;

    invoke-direct {v2, p0}, LX/EcR;-><init>(LX/EWj;)V

    .line 2146626
    iget v3, p0, LX/EcQ;->a:I

    .line 2146627
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2146628
    :goto_0
    iget v1, p0, LX/EcQ;->b:I

    .line 2146629
    iput v1, v2, LX/EcR;->index_:I

    .line 2146630
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2146631
    or-int/lit8 v0, v0, 0x2

    .line 2146632
    :cond_0
    iget-object v1, p0, LX/EcQ;->c:LX/EWc;

    .line 2146633
    iput-object v1, v2, LX/EcR;->key_:LX/EWc;

    .line 2146634
    iput v0, v2, LX/EcR;->bitField0_:I

    .line 2146635
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2146636
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2146637
    sget-object v0, LX/EcR;->c:LX/EcR;

    move-object v0, v0

    .line 2146638
    return-object v0
.end method
