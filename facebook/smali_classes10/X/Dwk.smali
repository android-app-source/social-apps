.class public final LX/Dwk;
.super Landroid/database/DataSetObserver;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 0

    .prologue
    .line 2061262
    iput-object p1, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 7

    .prologue
    .line 2061263
    iget-object v0, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2061264
    :cond_0
    :goto_0
    return-void

    .line 2061265
    :cond_1
    goto :goto_1

    .line 2061266
    :goto_1
    iget-object v0, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v1, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v1

    .line 2061267
    iget-object v2, v1, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    move-object v1, v2

    .line 2061268
    invoke-static {v1}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/4Vp;

    move-result-object v1

    iget-object v2, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v2, v2, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 2061269
    iput-object v2, v1, LX/4Vp;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2061270
    move-object v1, v1

    .line 2061271
    invoke-virtual {v1}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 2061272
    iput-object v1, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2061273
    iget-object v0, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2061274
    iget-object v0, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v1, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v1, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2061275
    goto :goto_3

    .line 2061276
    :cond_2
    :goto_2
    iget-object v0, p0, LX/Dwk;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->W:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->setVisibility(I)V

    goto :goto_0

    .line 2061277
    :goto_3
    iget-object v2, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Dxn;

    .line 2061278
    iget-object v3, v2, LX/Dxn;->a:LX/0h5;

    move-object v2, v3

    .line 2061279
    if-nez v2, :cond_6

    .line 2061280
    iget-object v2, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->I:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0zG;

    invoke-interface {v2}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0h5;

    move-object v3, v2

    .line 2061281
    :goto_4
    if-eqz v3, :cond_3

    .line 2061282
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f081271

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2061283
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->WALL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->COVER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->MOBILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->APP:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v4, :cond_2

    .line 2061284
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2061285
    invoke-static {v1}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->e(Lcom/facebook/graphql/model/GraphQLAlbum;)I

    move-result v2

    const v6, 0x25d6af

    if-ne v2, v6, :cond_a

    .line 2061286
    iget-object v2, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061287
    iget-object v6, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v6

    .line 2061288
    invoke-static {v1, v2}, LX/Dxo;->b(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061289
    iget-object v6, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v6

    .line 2061290
    invoke-static {v1, v2}, LX/Dxo;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    move v2, v5

    .line 2061291
    :goto_5
    iget-object v6, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ad:LX/8A4;

    if-eqz v6, :cond_9

    if-eqz v2, :cond_8

    iget-object v2, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ad:LX/8A4;

    sget-object v6, LX/8A3;->EDIT_PROFILE:LX/8A3;

    invoke-virtual {v2, v6}, LX/8A4;->a(LX/8A3;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2061292
    :goto_6
    move v2, v5

    .line 2061293
    if-eqz v2, :cond_2

    .line 2061294
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    iget-object v2, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->n:LX/01T;

    sget-object v5, LX/01T;->PAA:LX/01T;

    if-ne v2, v5, :cond_5

    const v2, 0x7f020723

    .line 2061295
    :goto_7
    iput v2, v4, LX/108;->i:I

    .line 2061296
    move-object v2, v4

    .line 2061297
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0811dd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2061298
    iput-object v4, v2, LX/108;->j:Ljava/lang/String;

    .line 2061299
    move-object v2, v2

    .line 2061300
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 2061301
    new-instance v4, LX/Dwf;

    invoke-direct {v4, v0, v1}, LX/Dwf;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;Lcom/facebook/graphql/model/GraphQLAlbum;)V

    .line 2061302
    if-eqz v3, :cond_2

    .line 2061303
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v3, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2061304
    invoke-interface {v3, v4}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto/16 :goto_2

    .line 2061305
    :cond_5
    const v2, 0x7f02004b

    goto :goto_7

    :cond_6
    move-object v3, v2

    goto/16 :goto_4

    :cond_7
    move v2, v4

    .line 2061306
    goto :goto_5

    :cond_8
    move v5, v4

    .line 2061307
    goto :goto_6

    :cond_9
    move v5, v2

    goto :goto_6

    .line 2061308
    :cond_a
    iget-object v2, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061309
    iget-object v4, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2061310
    invoke-static {v1, v2}, LX/Dxo;->b(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v5

    goto :goto_6
.end method
