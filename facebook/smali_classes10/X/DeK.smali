.class public LX/DeK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/DeK;


# instance fields
.field public a:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/messaging/font/FontLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DeD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "LX/DeE;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2021312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2021313
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/DeK;->d:Landroid/util/LruCache;

    .line 2021314
    return-void
.end method

.method public static a(LX/0QB;)LX/DeK;
    .locals 6

    .prologue
    .line 2021315
    sget-object v0, LX/DeK;->e:LX/DeK;

    if-nez v0, :cond_1

    .line 2021316
    const-class v1, LX/DeK;

    monitor-enter v1

    .line 2021317
    :try_start_0
    sget-object v0, LX/DeK;->e:LX/DeK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2021318
    if-eqz v2, :cond_0

    .line 2021319
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2021320
    new-instance p0, LX/DeK;

    invoke-direct {p0}, LX/DeK;-><init>()V

    .line 2021321
    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v0}, Lcom/facebook/messaging/font/FontLoader;->a(LX/0QB;)Lcom/facebook/messaging/font/FontLoader;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/font/FontLoader;

    invoke-static {v0}, LX/DeD;->a(LX/0QB;)LX/DeD;

    move-result-object v5

    check-cast v5, LX/DeD;

    .line 2021322
    iput-object v3, p0, LX/DeK;->a:LX/0TD;

    iput-object v4, p0, LX/DeK;->b:Lcom/facebook/messaging/font/FontLoader;

    iput-object v5, p0, LX/DeK;->c:LX/DeD;

    .line 2021323
    move-object v0, p0

    .line 2021324
    sput-object v0, LX/DeK;->e:LX/DeK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2021325
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2021326
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2021327
    :cond_1
    sget-object v0, LX/DeK;->e:LX/DeK;

    return-object v0

    .line 2021328
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2021329
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/DeK;LX/DeE;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DeE;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2021330
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021331
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021332
    iget-object v0, p0, LX/DeK;->c:LX/DeD;

    .line 2021333
    invoke-virtual {v0, p1}, LX/2Vx;->a(LX/2WG;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2021334
    invoke-virtual {v0, p1}, LX/2Vx;->c(LX/2WG;)LX/1gI;

    move-result-object v1

    check-cast v1, LX/1gH;

    .line 2021335
    if-eqz v1, :cond_1

    .line 2021336
    iget-object v2, v1, LX/1gH;->a:Ljava/io/File;

    move-object v1, v2

    .line 2021337
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2021338
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2021339
    :goto_0
    move-object v0, v1

    .line 2021340
    if-nez v0, :cond_0

    .line 2021341
    iget-object v0, p0, LX/DeK;->b:Lcom/facebook/messaging/font/FontLoader;

    .line 2021342
    iget-object v1, v0, Lcom/facebook/messaging/font/FontLoader;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 2021343
    if-nez v1, :cond_2

    const/4 v1, 0x0

    .line 2021344
    :goto_1
    if-eqz v1, :cond_3

    .line 2021345
    :goto_2
    move-object v0, v1

    .line 2021346
    :goto_3
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_3

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2021347
    :cond_2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 2021348
    :cond_3
    iget-object v1, v0, Lcom/facebook/messaging/font/FontLoader;->c:LX/0TD;

    new-instance v2, LX/DeG;

    invoke-direct {v2, v0, p1, p2}, LX/DeG;-><init>(Lcom/facebook/messaging/font/FontLoader;LX/DeE;Ljava/lang/String;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2021349
    new-instance v2, LX/DeH;

    invoke-direct {v2, v0, p1}, LX/DeH;-><init>(Lcom/facebook/messaging/font/FontLoader;LX/DeE;)V

    iget-object p0, v0, Lcom/facebook/messaging/font/FontLoader;->c:LX/0TD;

    invoke-static {v1, v2, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2021350
    iget-object v2, v0, Lcom/facebook/messaging/font/FontLoader;->b:Ljava/util/Map;

    new-instance p0, Ljava/lang/ref/WeakReference;

    invoke-direct {p0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method
