.class public final LX/EJa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic b:LX/Cxe;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/Cxe;)V
    .locals 0

    .prologue
    .line 2104592
    iput-object p1, p0, LX/EJa;->c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

    iput-object p2, p0, LX/EJa;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object p3, p0, LX/EJa;->b:LX/Cxe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x329bda94

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2104593
    iget-object v0, p0, LX/EJa;->c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->d:LX/1nD;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    iget-object v2, p0, LX/EJa;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hw()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/EJa;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->hu()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/EJa;->b:LX/Cxe;

    check-cast v4, LX/CxV;

    invoke-interface {v4}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v4

    .line 2104594
    iget-object v5, v4, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v4, v5

    .line 2104595
    sget-object v5, LX/8ci;->z:LX/8ci;

    iget-object v6, p0, LX/EJa;->b:LX/Cxe;

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    .line 2104596
    iget-object p1, v6, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v6, p1

    .line 2104597
    invoke-virtual/range {v0 .. v6}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v1

    .line 2104598
    iget-object v0, p0, LX/EJa;->c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

    iget-object v2, v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/EJa;->b:LX/Cxe;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2104599
    iget-object v0, p0, LX/EJa;->b:LX/Cxe;

    iget-object v1, p0, LX/EJa;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v0, v1}, LX/Cxe;->c(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 2104600
    const v0, -0x1eede7fa

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
