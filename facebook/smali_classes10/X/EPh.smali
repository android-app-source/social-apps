.class public final LX/EPh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final c:LX/093;

.field public final d:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public final f:LX/04D;


# direct methods
.method public constructor <init>(LX/CzL;Lcom/facebook/graphql/model/GraphQLStory;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/093;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            "LX/04D;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2117396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2117397
    iput-object p1, p0, LX/EPh;->a:LX/CzL;

    .line 2117398
    iput-object p2, p0, LX/EPh;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2117399
    iput-object p3, p0, LX/EPh;->c:LX/093;

    .line 2117400
    iput-object p4, p0, LX/EPh;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2117401
    iput-object p5, p0, LX/EPh;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 2117402
    iput-object p6, p0, LX/EPh;->f:LX/04D;

    .line 2117403
    return-void
.end method
