.class public final LX/DPg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DPC;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

.field public final synthetic b:LX/DPp;

.field public final synthetic c:LX/DPo;


# direct methods
.method public constructor <init>(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/DPp;)V
    .locals 0

    .prologue
    .line 1993655
    iput-object p1, p0, LX/DPg;->c:LX/DPo;

    iput-object p2, p0, LX/DPg;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    iput-object p3, p0, LX/DPg;->b:LX/DPp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1993656
    iget-object v0, p0, LX/DPg;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DPg;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1993657
    :goto_0
    iget-object v1, p0, LX/DPg;->b:LX/DPp;

    invoke-virtual {v1, v0}, LX/DPp;->a(Z)V

    .line 1993658
    return-void

    .line 1993659
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1993660
    iget-object v0, p0, LX/DPg;->c:LX/DPo;

    iget-object v0, v0, LX/DPo;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/DPo;->a:Ljava/lang/String;

    const-string v2, "Group leave action failed."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1993661
    return-void
.end method
