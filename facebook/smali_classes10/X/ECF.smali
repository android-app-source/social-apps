.class public final LX/ECF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# instance fields
.field public final synthetic a:LX/ECG;


# direct methods
.method public constructor <init>(LX/ECG;)V
    .locals 0

    .prologue
    .line 2089545
    iput-object p1, p0, LX/ECF;->a:LX/ECG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAudioFocusChange(I)V
    .locals 2

    .prologue
    .line 2089546
    packed-switch p1, :pswitch_data_0

    .line 2089547
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2089548
    :pswitch_1
    iget-object v0, p0, LX/ECF;->a:LX/ECG;

    iget-object v0, v0, LX/ECG;->d:LX/ECI;

    .line 2089549
    iget-object v1, v0, LX/ECI;->a:LX/ECO;

    .line 2089550
    iget-object v0, v1, LX/ECO;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, v1, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2089551
    iget-object v0, v1, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 2089552
    :cond_1
    goto :goto_0

    .line 2089553
    :pswitch_2
    iget-object v0, p0, LX/ECF;->a:LX/ECG;

    iget-object v0, v0, LX/ECG;->d:LX/ECI;

    .line 2089554
    iget-object v1, v0, LX/ECI;->a:LX/ECO;

    invoke-static {v1}, LX/ECO;->j(LX/ECO;)V

    .line 2089555
    goto :goto_0

    .line 2089556
    :pswitch_3
    iget-object v0, p0, LX/ECF;->a:LX/ECG;

    iget-object v0, v0, LX/ECG;->d:LX/ECI;

    .line 2089557
    iget-object v1, v0, LX/ECI;->a:LX/ECO;

    invoke-virtual {v1}, LX/ECO;->f()V

    .line 2089558
    iget-object v1, v0, LX/ECI;->a:LX/ECO;

    iget-object v1, v1, LX/ECO;->h:LX/EDe;

    .line 2089559
    iget-object p1, v1, LX/EDe;->a:LX/EDx;

    iget p1, p1, LX/EDx;->am:I

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    const/4 p1, 0x1

    :goto_1
    move v1, p1

    .line 2089560
    move v0, v1

    .line 2089561
    if-eqz v0, :cond_0

    .line 2089562
    iget-object v0, p0, LX/ECF;->a:LX/ECG;

    const-string v1, "Lost audio focus"

    invoke-static {v0, v1}, LX/ECG;->a$redex0(LX/ECG;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
