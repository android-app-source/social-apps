.class public final LX/EHf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/views/RtcSpringDragView;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/views/RtcSpringDragView;)V
    .locals 0

    .prologue
    .line 2100175
    iput-object p1, p0, LX/EHf;->a:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 2100169
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2100170
    iget-object v1, p0, LX/EHf;->a:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v1, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->setScaleX(F)V

    .line 2100171
    iget-object v1, p0, LX/EHf;->a:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v1, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->setScaleY(F)V

    .line 2100172
    iget-object v0, p0, LX/EHf;->a:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v1, p0, LX/EHf;->a:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-static {v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->getBounds(Lcom/facebook/rtc/views/RtcSpringDragView;)LX/EHi;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(Lcom/facebook/rtc/views/RtcSpringDragView;LX/EHi;)LX/EHm;

    move-result-object v0

    .line 2100173
    iget-object v1, p0, LX/EHf;->a:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v2, v0, LX/EHm;->a:I

    iget v0, v0, LX/EHm;->b:I

    invoke-static {v1, v2, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->b(Lcom/facebook/rtc/views/RtcSpringDragView;II)V

    .line 2100174
    return-void
.end method
