.class public final LX/DEy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;LX/1Pr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 1977536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1977537
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/DEy;->a:Ljava/lang/ref/SoftReference;

    .line 1977538
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/DEy;->b:Ljava/lang/ref/SoftReference;

    .line 1977539
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/DEy;->c:Ljava/lang/ref/SoftReference;

    .line 1977540
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1977541
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1977542
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 1977543
    iget-object v0, p0, LX/DEy;->a:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1977544
    iget-object v1, p0, LX/DEy;->b:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Pr;

    .line 1977545
    iget-object v2, p0, LX/DEy;->c:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 1977546
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    .line 1977547
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 1977548
    if-nez v4, :cond_1

    .line 1977549
    :cond_0
    :goto_0
    return-void

    .line 1977550
    :cond_1
    new-instance v5, LX/2en;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, LX/2eo;->a(LX/1Fa;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-direct {v5, v4, v2}, LX/2en;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1977551
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1977552
    check-cast v2, LX/0jW;

    invoke-interface {v1, v5, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2ep;

    .line 1977553
    iget-boolean v4, v2, LX/2ep;->b:Z

    .line 1977554
    iget-object v6, v2, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1977555
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1977556
    check-cast v2, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->u()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    .line 1977557
    if-eq v7, v6, :cond_0

    .line 1977558
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v7, v2, :cond_2

    move v2, v3

    .line 1977559
    :goto_1
    new-instance v4, LX/2ep;

    invoke-direct {v4, v7, v2}, LX/2ep;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-interface {v1, v5, v4}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1977560
    check-cast v1, LX/1Pq;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1
.end method
