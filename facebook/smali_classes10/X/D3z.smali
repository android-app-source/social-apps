.class public LX/D3z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/15m;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/BSM;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:Landroid/os/Handler;

.field private final f:Ljava/lang/Runnable;

.field public g:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1961464
    const-class v0, LX/D3z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/D3z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/BSM;Lcom/facebook/content/SecureContextHelper;Landroid/os/Handler;)V
    .locals 1
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1961465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961466
    new-instance v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationCoordinator$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationCoordinator$1;-><init>(LX/D3z;)V

    iput-object v0, p0, LX/D3z;->f:Ljava/lang/Runnable;

    .line 1961467
    iput-object p2, p0, LX/D3z;->c:LX/BSM;

    .line 1961468
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/D3z;->b:Landroid/content/Context;

    .line 1961469
    iput-object p3, p0, LX/D3z;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1961470
    iput-object p4, p0, LX/D3z;->e:Landroid/os/Handler;

    .line 1961471
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1961472
    iget-object v0, p0, LX/D3z;->g:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 1961473
    iget-object v0, p0, LX/D3z;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/D3z;->f:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1961474
    const/4 v0, 0x0

    iput-object v0, p0, LX/D3z;->g:Landroid/content/Intent;

    .line 1961475
    :cond_0
    iget-boolean v0, p0, LX/D3z;->h:Z

    if-eqz v0, :cond_1

    .line 1961476
    iget-object v0, p0, LX/D3z;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1961477
    new-instance v1, Landroid/content/Intent;

    sget-object v2, LX/D3x;->a:Ljava/lang/Class;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video.playback.control.action.close"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_analytics"

    const-string v3, "close_by_foreground"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    move-object v1, v1

    .line 1961478
    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 1961479
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D3z;->h:Z

    .line 1961480
    :cond_1
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/video/backgroundplay/control/model/ControlInitData;)V
    .locals 3

    .prologue
    .line 1961481
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/D3x;->a:Ljava/lang/Class;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1961482
    const-string v1, "video.playback.control.action.initialize"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1961483
    sget-object v1, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1961484
    move-object v0, v0

    .line 1961485
    iput-object v0, p0, LX/D3z;->g:Landroid/content/Intent;

    .line 1961486
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D3z;->h:Z

    .line 1961487
    iget-object v0, p0, LX/D3z;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/D3z;->f:Ljava/lang/Runnable;

    const v2, -0xdce9e5e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1961488
    return-void
.end method

.method public final a()Z
    .locals 5

    .prologue
    .line 1961489
    iget-boolean v0, p0, LX/D3z;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D3z;->c:LX/BSM;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1961490
    invoke-virtual {v0}, LX/BSM;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1961491
    :cond_0
    :goto_0
    move v0, v1

    .line 1961492
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1961493
    :cond_2
    iget-object v3, v0, LX/BSM;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/BSN;->b:LX/0Tn;

    sget-object p0, LX/BSN;->c:LX/0Tn;

    invoke-virtual {p0}, LX/0To;->a()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, v4, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1961494
    sget-object v4, LX/BSN;->c:LX/0Tn;

    invoke-virtual {v4}, LX/0To;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v1, v2

    .line 1961495
    goto :goto_0

    .line 1961496
    :cond_3
    sget-object v4, LX/BSN;->d:LX/0Tn;

    invoke-virtual {v4}, LX/0To;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1961497
    iget-object v3, v0, LX/BSM;->a:LX/1CD;

    invoke-virtual {v3}, LX/1CD;->a()LX/45B;

    move-result-object v3

    sget-object v4, LX/45B;->CONNECTED:LX/45B;

    if-ne v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .line 1961498
    :cond_4
    sget-object v4, LX/BSN;->e:LX/0Tn;

    invoke-virtual {v4}, LX/0To;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 1961499
    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1961500
    iget-object v0, p0, LX/D3z;->c:LX/BSM;

    .line 1961501
    iget-object v1, v0, LX/BSM;->b:LX/15d;

    const/4 v2, 0x0

    .line 1961502
    iget-object p0, v1, LX/15d;->f:Ljava/lang/Boolean;

    if-nez p0, :cond_2

    .line 1961503
    invoke-static {v1}, LX/15d;->e(LX/15d;)Z

    move-result p0

    .line 1961504
    if-nez p0, :cond_0

    iget-object p0, v1, LX/15d;->c:LX/0ad;

    sget-short v0, LX/BSC;->c:S

    invoke-interface {p0, v0, v2}, LX/0ad;->a(SZ)Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 v2, 0x1

    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, LX/15d;->f:Ljava/lang/Boolean;

    .line 1961505
    :cond_2
    iget-object v2, v1, LX/15d;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 1961506
    move v0, v1

    .line 1961507
    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1961508
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D3z;->i:Z

    .line 1961509
    return-void
.end method
