.class public LX/EQ0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/EPx;

.field public final b:LX/0TD;

.field public final c:LX/0tX;

.field private final d:LX/1mR;

.field public final e:LX/0tc;

.field public final f:LX/0ti;

.field public final g:LX/0Uh;

.field private final h:LX/2Sb;


# direct methods
.method public constructor <init>(LX/EPx;LX/0TD;LX/0tX;LX/1mR;LX/0tc;LX/0ti;LX/0Uh;LX/2Sb;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2118094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2118095
    iput-object p1, p0, LX/EQ0;->a:LX/EPx;

    .line 2118096
    iput-object p2, p0, LX/EQ0;->b:LX/0TD;

    .line 2118097
    iput-object p3, p0, LX/EQ0;->c:LX/0tX;

    .line 2118098
    iput-object p4, p0, LX/EQ0;->d:LX/1mR;

    .line 2118099
    iput-object p5, p0, LX/EQ0;->e:LX/0tc;

    .line 2118100
    iput-object p6, p0, LX/EQ0;->f:LX/0ti;

    .line 2118101
    iput-object p7, p0, LX/EQ0;->g:LX/0Uh;

    .line 2118102
    iput-object p8, p0, LX/EQ0;->h:LX/2Sb;

    .line 2118103
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2118104
    iget-object v0, p0, LX/EQ0;->d:LX/1mR;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
