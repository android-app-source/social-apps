.class public final LX/DkU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DjX;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/Dka;


# direct methods
.method public constructor <init>(LX/Dka;LX/DjX;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2034273
    iput-object p1, p0, LX/DkU;->d:LX/Dka;

    iput-object p2, p0, LX/DkU;->a:LX/DjX;

    iput-object p3, p0, LX/DkU;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DkU;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2034274
    iget-object v0, p0, LX/DkU;->d:LX/Dka;

    iget-object v0, v0, LX/Dka;->c:LX/Dih;

    const-string v1, "decline_mutation"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/DkU;->b:Ljava/lang/String;

    iget-object v4, p0, LX/DkU;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Dih;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2034275
    iget-object v0, p0, LX/DkU;->d:LX/Dka;

    iget-object v0, v0, LX/Dka;->d:LX/03V;

    const-class v1, LX/Dka;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "decline_mutation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/DkU;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034276
    iget-object v0, p0, LX/DkU;->d:LX/Dka;

    iget-object v0, v0, LX/Dka;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08003c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2034277
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2034278
    iget-object v0, p0, LX/DkU;->a:LX/DjX;

    if-eqz v0, :cond_0

    .line 2034279
    iget-object v0, p0, LX/DkU;->a:LX/DjX;

    .line 2034280
    iget-object p0, v0, LX/DjX;->a:LX/DjY;

    iget-object p0, p0, LX/DjY;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2034281
    iget-object p0, v0, LX/DjX;->a:LX/DjY;

    iget-object p0, p0, LX/DjY;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2034282
    :cond_0
    return-void
.end method
