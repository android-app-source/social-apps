.class public LX/ErK;
.super LX/7HT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HT",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/03V;

.field private final d:LX/0tX;

.field public e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2172817
    const-class v0, LX/ErK;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ErK;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/ErO;LX/03V;LX/0tX;LX/1Ck;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172811
    invoke-direct {p0, p5, p2}, LX/7HT;-><init>(LX/1Ck;LX/7Hq;)V

    .line 2172812
    iput-object p1, p0, LX/ErK;->b:Ljava/util/concurrent/ExecutorService;

    .line 2172813
    iput-object p3, p0, LX/ErK;->c:LX/03V;

    .line 2172814
    iput-object p4, p0, LX/ErK;->d:LX/0tX;

    .line 2172815
    iput-object p6, p0, LX/ErK;->e:Ljava/lang/String;

    .line 2172816
    return-void
.end method

.method public static b(LX/2uF;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2uF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2172806
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2172807
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v9, v0, LX/1vs;->b:I

    .line 2172808
    new-instance v0, Lcom/facebook/events/invite/EventsExtendedInviteUserToken;

    const/4 v1, 0x6

    invoke-virtual {v6, v9, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    new-instance v2, Lcom/facebook/user/model/Name;

    const/4 v3, 0x5

    invoke-virtual {v6, v9, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-virtual {v6, v9, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v6, v9, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v6, v9, v5}, LX/15i;->h(II)Z

    move-result v5

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, LX/15i;->h(II)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/events/invite/EventsExtendedInviteUserToken;-><init>(Lcom/facebook/user/model/UserKey;Lcom/facebook/user/model/Name;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 2172809
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2172810
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/7B6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7Hc",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2172798
    iget-object v0, p0, LX/ErK;->d:LX/0tX;

    iget-object v1, p1, LX/7B6;->b:Ljava/lang/String;

    .line 2172799
    new-instance v2, LX/7nv;

    invoke-direct {v2}, LX/7nv;-><init>()V

    move-object v2, v2

    .line 2172800
    const-string v3, "event_id"

    iget-object v4, p0, LX/ErK;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2172801
    const-string v3, "search_query"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2172802
    const-string v3, "first_count"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2172803
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    move-object v1, v2

    .line 2172804
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2172805
    new-instance v1, LX/ErJ;

    invoke-direct {v1, p0, p1}, LX/ErJ;-><init>(LX/ErK;LX/7B6;)V

    iget-object v2, p0, LX/ErK;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2172797
    const-string v0, "FETCH_REMOTE_CONTACTS_TASK"

    return-object v0
.end method

.method public final a(LX/7B6;Ljava/lang/Throwable;)V
    .locals 3
    .param p1    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2172791
    iget-object v0, p0, LX/ErK;->c:LX/03V;

    sget-object v1, LX/ErK;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch local invitees"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2172792
    iput-object p2, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2172793
    move-object v1, v1

    .line 2172794
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2172795
    return-void
.end method

.method public final b()LX/7HY;
    .locals 1

    .prologue
    .line 2172796
    sget-object v0, LX/7HY;->REMOTE:LX/7HY;

    return-object v0
.end method
