.class public final LX/CtD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/method/TransformationMethod;


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

.field private final b:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;)V
    .locals 1

    .prologue
    .line 1944178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1944179
    iput-object p2, p0, LX/CtD;->a:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    .line 1944180
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v0, p0, LX/CtD;->b:Ljava/util/Locale;

    .line 1944181
    return-void
.end method


# virtual methods
.method public final getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1944172
    iget-object v1, p0, LX/CtD;->a:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->ALL_CAPS:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    if-ne v1, v2, :cond_2

    .line 1944173
    if-nez p1, :cond_1

    move-object p1, v0

    .line 1944174
    :cond_0
    :goto_0
    return-object p1

    .line 1944175
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/CtD;->b:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1944176
    :cond_2
    iget-object v1, p0, LX/CtD;->a:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;->ALL_LOWER_CASE:Lcom/facebook/graphql/enums/GraphQLCapitalizationStyle;

    if-ne v1, v2, :cond_0

    .line 1944177
    if-nez p1, :cond_3

    move-object p1, v0

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/CtD;->b:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public final onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1944171
    return-void
.end method
