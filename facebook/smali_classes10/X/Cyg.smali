.class public final LX/Cyg;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cyl;

.field public final synthetic b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/Cyn;


# direct methods
.method public constructor <init>(LX/Cyn;LX/Cyl;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1953471
    iput-object p1, p0, LX/Cyg;->d:LX/Cyn;

    iput-object p2, p0, LX/Cyg;->a:LX/Cyl;

    iput-object p3, p0, LX/Cyg;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iput-object p4, p0, LX/Cyg;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1953472
    iget-object v0, p0, LX/Cyg;->d:LX/Cyn;

    iget-object v1, p0, LX/Cyg;->d:LX/Cyn;

    iget-object v1, v1, LX/Cyn;->s:LX/Fdy;

    iget-object v2, p0, LX/Cyg;->a:LX/Cyl;

    invoke-static {v0, p1, v1, v2}, LX/Cyn;->a$redex0(LX/Cyn;Ljava/lang/Throwable;LX/Fdy;LX/Cyl;)V

    .line 1953473
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1953474
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 1953475
    iget-object v0, p0, LX/Cyg;->a:LX/Cyl;

    invoke-virtual {v0}, LX/Cyl;->b()V

    .line 1953476
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1953477
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v3

    .line 1953478
    const-string v0, "CombinedResults were null"

    invoke-static {v3, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953479
    iget-object v0, p0, LX/Cyg;->d:LX/Cyn;

    iget-object v0, v0, LX/Cyn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cve;

    iget-object v1, p0, LX/Cyg;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iget-object v4, p0, LX/Cyg;->c:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->c()Ljava/lang/String;

    move-result-object v5

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, LX/Cve;->a(LX/CwB;ZLcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;Ljava/lang/String;)V

    .line 1953480
    iget-object v0, p0, LX/Cyg;->d:LX/Cyn;

    new-instance v1, LX/Cym;

    iget-object v3, p0, LX/Cyg;->c:Ljava/lang/String;

    invoke-direct {v1, p1, v3}, LX/Cym;-><init>(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)V

    iget-object v3, p0, LX/Cyg;->d:LX/Cyn;

    iget-object v3, v3, LX/Cyn;->p:LX/Cyd;

    sget-object v4, LX/Cyd;->LOAD:LX/Cyd;

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-static {v0, v1, v2}, LX/Cyn;->a$redex0(LX/Cyn;LX/Cym;Z)V

    .line 1953481
    iget-object v0, p0, LX/Cyg;->d:LX/Cyn;

    invoke-static {v0}, LX/Cyn;->k(LX/Cyn;)V

    .line 1953482
    return-void
.end method
