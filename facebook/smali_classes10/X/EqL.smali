.class public final LX/EqL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWy;


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V
    .locals 0

    .prologue
    .line 2171306
    iput-object p1, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2171300
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->N:LX/0ad;

    sget-short v1, LX/347;->t:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171301
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    invoke-static {v0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->n(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V

    .line 2171302
    :goto_0
    return-void

    .line 2171303
    :cond_0
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v1, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v1, v1, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->x:Ljava/lang/String;

    .line 2171304
    iget-object v2, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    iget-object p0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result p0

    new-array p0, p0, [Ljava/lang/String;

    invoke-interface {v2, p0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2171305
    goto :goto_0
.end method

.method public final a(LX/8QL;)V
    .locals 1

    .prologue
    .line 2171297
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    .line 2171298
    invoke-static {v0, p1}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->a$redex0(Lcom/facebook/events/invite/EventsExtendedInviteActivity;LX/8QL;)V

    .line 2171299
    return-void
.end method

.method public final a(Z)V
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 2171267
    if-eqz p1, :cond_1

    move v2, v1

    .line 2171268
    :goto_0
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->getMeasuredHeight()I

    move-result v0

    .line 2171269
    :goto_1
    iget-object v3, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v3, v3, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->z:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    const/16 v4, 0xc8

    .line 2171270
    iget-object v6, v3, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v6, v6

    .line 2171271
    if-nez v6, :cond_3

    .line 2171272
    :goto_2
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->N:LX/0ad;

    sget-short v2, LX/347;->t:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2171273
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->P:LX/3kp;

    .line 2171274
    iput v5, v0, LX/3kp;->b:I

    .line 2171275
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->P:LX/3kp;

    sget-object v1, LX/7vb;->f:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2171276
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->P:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    invoke-static {v0}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->s(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-boolean v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->r:Z

    if-nez v0, :cond_0

    .line 2171277
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    invoke-direct {v0, v1, v5}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2171278
    const v1, 0x7f081eb6

    invoke-virtual {v0, v1}, LX/0hs;->a(I)V

    .line 2171279
    const v1, 0x7f081eb7

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 2171280
    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2171281
    const/4 v1, -0x1

    .line 2171282
    iput v1, v0, LX/0hs;->t:I

    .line 2171283
    iget-object v1, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v1, v1, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    .line 2171284
    iget-object v2, v1, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    move-object v1, v2

    .line 2171285
    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2171286
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    const/4 v1, 0x1

    .line 2171287
    iput-boolean v1, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->r:Z

    .line 2171288
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->P:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 2171289
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->J:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4158"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2171290
    :cond_0
    return-void

    .line 2171291
    :cond_1
    iget-object v0, p0, LX/EqL;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->getMeasuredHeight()I

    move-result v0

    move v2, v0

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 2171292
    goto/16 :goto_1

    .line 2171293
    :cond_3
    const/4 v7, 0x2

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput v2, v7, v8

    const/4 v8, 0x1

    aput v0, v7, v8

    invoke-static {v7}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v7

    .line 2171294
    int-to-long v8, v4

    invoke-virtual {v7, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2171295
    new-instance v8, LX/Eqh;

    invoke-direct {v8, v3, v6}, LX/Eqh;-><init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;Landroid/view/View;)V

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2171296
    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_2
.end method
