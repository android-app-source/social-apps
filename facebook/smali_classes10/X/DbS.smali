.class public final LX/DbS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;

.field public final synthetic b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final synthetic c:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 0

    .prologue
    .line 2017113
    iput-object p1, p0, LX/DbS;->c:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    iput-object p2, p0, LX/DbS;->a:Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;

    iput-object p3, p0, LX/DbS;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x68be8f39

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017114
    iget-object v1, p0, LX/DbS;->c:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    iget-object v1, v1, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->b:LX/DbM;

    iget-object v2, p0, LX/DbS;->c:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    iget-object v2, v2, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->d:Ljava/lang/String;

    iget-object v3, p0, LX/DbS;->c:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    iget-object v3, v3, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->e:Ljava/lang/String;

    iget-object v4, p0, LX/DbS;->a:Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 2017115
    const-string v7, "menu_viewer_food_photo_tap"

    invoke-static {v2, v7, v3}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 2017116
    const-string p1, "photo_id"

    invoke-virtual {v7, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2017117
    iget-object p1, v1, LX/DbM;->a:LX/0Zb;

    invoke-interface {p1, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017118
    iget-object v1, p0, LX/DbS;->c:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    iget-object v1, v1, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->e:Ljava/lang/String;

    const-string v2, "FOOD"

    invoke-static {v1, v2, v6}, LX/9hF;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/9hE;

    move-result-object v1

    sget-object v2, LX/74S;->FOOD_PHOTOS:LX/74S;

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v1

    iget-object v2, p0, LX/DbS;->a:Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v1

    invoke-virtual {v1}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 2017119
    iget-object v2, p0, LX/DbS;->c:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    iget-object v2, v2, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;->c:LX/23R;

    iget-object v3, p0, LX/DbS;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v1, v6}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2017120
    const v1, 0x1848b222

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
