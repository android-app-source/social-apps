.class public final LX/Cyp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/results/protocol/elections/SearchElectionQueryModels$SearchElectionQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cyr;


# direct methods
.method public constructor <init>(LX/Cyr;)V
    .locals 0

    .prologue
    .line 1953782
    iput-object p1, p0, LX/Cyp;->a:LX/Cyr;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 1953783
    iget-object v0, p0, LX/Cyp;->a:LX/Cyr;

    invoke-static {v0}, LX/Cyr;->b$redex0(LX/Cyr;)V

    .line 1953784
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1953785
    iget-object v0, p0, LX/Cyp;->a:LX/Cyr;

    invoke-static {v0}, LX/Cyr;->b$redex0(LX/Cyr;)V

    .line 1953786
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1953787
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1953788
    if-eqz p1, :cond_0

    .line 1953789
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1953790
    if-eqz v0, :cond_0

    .line 1953791
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1953792
    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchElectionQueryModels$SearchElectionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchElectionQueryModels$SearchElectionQueryModel;->a()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1953793
    :cond_0
    iget-object v0, p0, LX/Cyp;->a:LX/Cyr;

    iget-object v0, v0, LX/Cyr;->c:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_GRAPH_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    const-string v2, "Result elections data invalid"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 1953794
    iget-object v0, p0, LX/Cyp;->a:LX/Cyr;

    invoke-static {v0}, LX/Cyr;->b$redex0(LX/Cyr;)V

    .line 1953795
    :goto_0
    return-void

    .line 1953796
    :cond_1
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    .line 1953797
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1953798
    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchElectionQueryModels$SearchElectionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchElectionQueryModels$SearchElectionQueryModel;->a()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v0

    .line 1953799
    iput-object v0, v1, LX/8dX;->aB:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 1953800
    move-object v0, v1

    .line 1953801
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1953802
    iget-object v1, p0, LX/Cyp;->a:LX/Cyr;

    iget-object v1, v1, LX/Cyr;->d:LX/Cwy;

    if-nez v1, :cond_2

    .line 1953803
    iget-object v0, p0, LX/Cyp;->a:LX/Cyr;

    invoke-static {v0}, LX/Cyr;->b$redex0(LX/Cyr;)V

    goto :goto_0

    .line 1953804
    :cond_2
    iget-object v1, p0, LX/Cyp;->a:LX/Cyr;

    iget-object v1, v1, LX/Cyr;->d:LX/Cwy;

    invoke-interface {v1, v0}, LX/Cwy;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
