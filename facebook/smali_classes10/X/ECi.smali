.class public LX/ECi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Z

.field public final c:LX/EBx;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/EDq;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;LX/EBx;)V
    .locals 4
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/EBx;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2090301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2090302
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ECi;->d:Ljava/util/Map;

    .line 2090303
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2090304
    iput-object v0, p0, LX/ECi;->e:LX/0Ot;

    .line 2090305
    iput-object p1, p0, LX/ECi;->a:Landroid/content/Context;

    .line 2090306
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/ECi;->b:Z

    .line 2090307
    iput-object p3, p0, LX/ECi;->c:LX/EBx;

    .line 2090308
    iget-object v0, p0, LX/ECi;->d:Ljava/util/Map;

    sget-object v1, LX/EDq;->EARPIECE:LX/EDq;

    iget-object v2, p0, LX/ECi;->a:Landroid/content/Context;

    const v3, 0x7f0806ff

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090309
    iget-object v0, p0, LX/ECi;->d:Ljava/util/Map;

    sget-object v1, LX/EDq;->SPEAKERPHONE:LX/EDq;

    iget-object v2, p0, LX/ECi;->a:Landroid/content/Context;

    const v3, 0x7f0806fd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090310
    iget-object v0, p0, LX/ECi;->d:Ljava/util/Map;

    sget-object v1, LX/EDq;->BLUETOOTH:LX/EDq;

    iget-object v2, p0, LX/ECi;->a:Landroid/content/Context;

    const v3, 0x7f0806fc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090311
    iget-object v0, p0, LX/ECi;->d:Ljava/util/Map;

    sget-object v1, LX/EDq;->HEADSET:LX/EDq;

    iget-object v2, p0, LX/ECi;->a:Landroid/content/Context;

    const v3, 0x7f0806fe

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090312
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2090313
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2090314
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2090315
    iget-object v0, p0, LX/ECi;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2090316
    iget-boolean v3, v0, LX/EDx;->be:Z

    move v0, v3

    .line 2090317
    if-eqz v0, :cond_2

    .line 2090318
    iget-object v0, p0, LX/ECi;->d:Ljava/util/Map;

    sget-object v3, LX/EDq;->HEADSET:LX/EDq;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2090319
    sget-object v0, LX/EDq;->HEADSET:LX/EDq;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2090320
    :cond_0
    :goto_0
    iget-object v0, p0, LX/ECi;->d:Ljava/util/Map;

    sget-object v3, LX/EDq;->SPEAKERPHONE:LX/EDq;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2090321
    sget-object v0, LX/EDq;->SPEAKERPHONE:LX/EDq;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2090322
    iget-object v0, p0, LX/ECi;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2090323
    iget-object v0, p0, LX/ECi;->d:Ljava/util/Map;

    sget-object v3, LX/EDq;->BLUETOOTH:LX/EDq;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2090324
    sget-object v0, LX/EDq;->BLUETOOTH:LX/EDq;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2090325
    :cond_1
    new-instance v3, LX/31Y;

    iget-object v0, p0, LX/ECi;->a:Landroid/content/Context;

    invoke-direct {v3, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    new-instance v1, LX/ECh;

    invoke-direct {v1, p0, v2}, LX/ECh;-><init>(LX/ECi;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v0, v1}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2090326
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2090327
    return-void

    .line 2090328
    :cond_2
    iget-object v0, p0, LX/ECi;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2090329
    iget-boolean v3, v0, LX/EDx;->bh:Z

    move v0, v3

    .line 2090330
    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/ECi;->b:Z

    if-eqz v0, :cond_0

    .line 2090331
    iget-object v0, p0, LX/ECi;->d:Ljava/util/Map;

    sget-object v3, LX/EDq;->EARPIECE:LX/EDq;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2090332
    sget-object v0, LX/EDq;->EARPIECE:LX/EDq;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
