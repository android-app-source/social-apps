.class public final LX/Dsh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "LX/0Px",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;)V
    .locals 0

    .prologue
    .line 2050582
    iput-object p1, p0, LX/Dsh;->a:Lcom/facebook/pages/common/friendinviter/fragments/PageFriendInviterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2050583
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2050584
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 2050585
    if-nez p1, :cond_0

    .line 2050586
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 2050587
    :goto_0
    return-object v0

    .line 2050588
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    .line 2050589
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2050590
    :cond_1
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0

    .line 2050591
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2050592
    invoke-virtual {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_4

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel$NodesModel;

    .line 2050593
    invoke-virtual {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel$NodesModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    .line 2050594
    invoke-virtual {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 2050595
    if-eqz v6, :cond_3

    .line 2050596
    new-instance v7, LX/0XI;

    invoke-direct {v7}, LX/0XI;-><init>()V

    sget-object v8, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v7

    new-instance v8, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 2050597
    iput-object v8, v7, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2050598
    move-object v0, v7

    .line 2050599
    invoke-virtual {v6}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 2050600
    iput-object v6, v0, LX/0XI;->n:Ljava/lang/String;

    .line 2050601
    move-object v0, v0

    .line 2050602
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2050603
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2050604
    :cond_4
    new-instance v0, LX/Dsg;

    invoke-direct {v0, p0}, LX/Dsg;-><init>(LX/Dsh;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2050605
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2050606
    sget-object v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2050607
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method
