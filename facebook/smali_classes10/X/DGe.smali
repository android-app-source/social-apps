.class public final LX/DGe;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DGf;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/1dl;

.field public final synthetic d:LX/DGf;


# direct methods
.method public constructor <init>(LX/DGf;)V
    .locals 1

    .prologue
    .line 1980303
    iput-object p1, p0, LX/DGe;->d:LX/DGf;

    .line 1980304
    move-object v0, p1

    .line 1980305
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1980306
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->a:LX/1dl;

    iput-object v0, p0, LX/DGe;->c:LX/1dl;

    .line 1980307
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1980285
    const-string v0, "StorySetHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1980286
    if-ne p0, p1, :cond_1

    .line 1980287
    :cond_0
    :goto_0
    return v0

    .line 1980288
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1980289
    goto :goto_0

    .line 1980290
    :cond_3
    check-cast p1, LX/DGe;

    .line 1980291
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1980292
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1980293
    if-eq v2, v3, :cond_0

    .line 1980294
    iget-object v2, p0, LX/DGe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DGe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DGe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1980295
    goto :goto_0

    .line 1980296
    :cond_5
    iget-object v2, p1, LX/DGe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1980297
    :cond_6
    iget-object v2, p0, LX/DGe;->b:LX/1Pk;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DGe;->b:LX/1Pk;

    iget-object v3, p1, LX/DGe;->b:LX/1Pk;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1980298
    goto :goto_0

    .line 1980299
    :cond_8
    iget-object v2, p1, LX/DGe;->b:LX/1Pk;

    if-nez v2, :cond_7

    .line 1980300
    :cond_9
    iget-object v2, p0, LX/DGe;->c:LX/1dl;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/DGe;->c:LX/1dl;

    iget-object v3, p1, LX/DGe;->c:LX/1dl;

    invoke-virtual {v2, v3}, LX/1dl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1980301
    goto :goto_0

    .line 1980302
    :cond_a
    iget-object v2, p1, LX/DGe;->c:LX/1dl;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
