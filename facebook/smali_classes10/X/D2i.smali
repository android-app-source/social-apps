.class public final LX/D2i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;)V
    .locals 0

    .prologue
    .line 1959123
    iput-object p1, p0, LX/D2i;->a:Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1959124
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1959125
    iget-object v0, p0, LX/D2i;->a:Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;

    iget-boolean v0, v0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->b:Z

    if-eqz v0, :cond_0

    .line 1959126
    iget-object v0, p0, LX/D2i;->a:Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;

    iget-object v1, p0, LX/D2i;->a:Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;

    iget-object v1, v1, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->post(Ljava/lang/Runnable;)Z

    .line 1959127
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1959128
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1959129
    return-void
.end method
