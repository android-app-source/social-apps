.class public final LX/Eh8;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Eh9;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/FBn;

.field public b:Landroid/view/View$OnClickListener;

.field public c:Landroid/view/View$OnClickListener;

.field public final synthetic d:LX/Eh9;


# direct methods
.method public constructor <init>(LX/Eh9;)V
    .locals 1

    .prologue
    .line 2158037
    iput-object p1, p0, LX/Eh8;->d:LX/Eh9;

    .line 2158038
    move-object v0, p1

    .line 2158039
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2158040
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2158041
    const-string v0, "SettingsItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2158042
    if-ne p0, p1, :cond_1

    .line 2158043
    :cond_0
    :goto_0
    return v0

    .line 2158044
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2158045
    goto :goto_0

    .line 2158046
    :cond_3
    check-cast p1, LX/Eh8;

    .line 2158047
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2158048
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2158049
    if-eq v2, v3, :cond_0

    .line 2158050
    iget-object v2, p0, LX/Eh8;->a:LX/FBn;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Eh8;->a:LX/FBn;

    iget-object v3, p1, LX/Eh8;->a:LX/FBn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2158051
    goto :goto_0

    .line 2158052
    :cond_5
    iget-object v2, p1, LX/Eh8;->a:LX/FBn;

    if-nez v2, :cond_4

    .line 2158053
    :cond_6
    iget-object v2, p0, LX/Eh8;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Eh8;->b:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/Eh8;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2158054
    goto :goto_0

    .line 2158055
    :cond_8
    iget-object v2, p1, LX/Eh8;->b:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_7

    .line 2158056
    :cond_9
    iget-object v2, p0, LX/Eh8;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Eh8;->c:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/Eh8;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2158057
    goto :goto_0

    .line 2158058
    :cond_a
    iget-object v2, p1, LX/Eh8;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
