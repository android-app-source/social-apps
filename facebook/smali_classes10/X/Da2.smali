.class public LX/Da2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Da2;


# instance fields
.field public final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2014438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2014439
    iput-object p1, p0, LX/Da2;->a:Landroid/content/res/Resources;

    .line 2014440
    return-void
.end method

.method public static a(LX/0QB;)LX/Da2;
    .locals 4

    .prologue
    .line 2014441
    sget-object v0, LX/Da2;->b:LX/Da2;

    if-nez v0, :cond_1

    .line 2014442
    const-class v1, LX/Da2;

    monitor-enter v1

    .line 2014443
    :try_start_0
    sget-object v0, LX/Da2;->b:LX/Da2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2014444
    if-eqz v2, :cond_0

    .line 2014445
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2014446
    new-instance p0, LX/Da2;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/Da2;-><init>(Landroid/content/res/Resources;)V

    .line 2014447
    move-object v0, p0

    .line 2014448
    sput-object v0, LX/Da2;->b:LX/Da2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2014449
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2014450
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2014451
    :cond_1
    sget-object v0, LX/Da2;->b:LX/Da2;

    return-object v0

    .line 2014452
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2014453
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
