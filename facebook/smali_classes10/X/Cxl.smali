.class public LX/Cxl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cxk;


# instance fields
.field private final A:LX/CxW;

.field private final B:LX/Cxb;

.field private final C:LX/CyJ;

.field private final D:LX/1QK;

.field private final a:LX/Cx3;

.field private final b:LX/1Pz;

.field private final c:LX/1Q0;

.field private final d:LX/1QL;

.field private final e:LX/Cxw;

.field private final f:LX/CxD;

.field private final g:LX/1Q2;

.field private final h:LX/CyF;

.field private final i:LX/1Q3;

.field private final j:LX/1Q4;

.field private final k:LX/1QM;

.field private final l:LX/1QP;

.field private final m:LX/1Q7;

.field private final n:LX/CxL;

.field private final o:LX/1QQ;

.field private final p:LX/CxN;

.field private final q:Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

.field private final r:LX/1QR;

.field private final s:LX/1QD;

.field private final t:LX/1QS;

.field private final u:LX/1QF;

.field private final v:LX/1QG;

.field private final w:LX/99Q;

.field private final x:LX/1PU;

.field private final y:LX/CxY;

.field private final z:LX/CxS;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;LX/CzA;LX/CyE;Ljava/lang/String;Landroid/content/Context;LX/CzA;LX/1PT;LX/CxF;Ljava/lang/Runnable;LX/1PY;LX/CzA;LX/CzA;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Cx4;LX/1Pz;LX/1Q0;LX/1Q1;LX/Cxx;LX/CxE;LX/1Q2;LX/CyG;LX/1Q3;LX/1Q4;LX/1Q5;LX/1Q6;LX/1Q7;LX/CxM;LX/1QA;LX/CxO;Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;LX/1QC;LX/1QD;LX/1QE;LX/1QF;LX/1QG;LX/99Q;LX/1QI;LX/CxZ;LX/CxT;LX/CxX;LX/Cxb;LX/CyK;LX/1QK;)V
    .locals 2
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CzA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/CzA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/CyE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/CzA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/CxF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/CzA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # LX/CzA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951879
    move-object/from16 v0, p15

    invoke-virtual {v0, p0, p0, p0}, LX/Cx4;->a(LX/CxA;LX/1Pq;LX/1Pr;)LX/Cx3;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->a:LX/Cx3;

    .line 1951880
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Cxl;->b:LX/1Pz;

    .line 1951881
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Cxl;->c:LX/1Q0;

    .line 1951882
    move-object/from16 v0, p18

    invoke-virtual {v0, p0}, LX/1Q1;->a(LX/1Po;)LX/1QL;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->d:LX/1QL;

    .line 1951883
    move-object/from16 v0, p19

    invoke-virtual {v0, p1, p2}, LX/Cxx;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;)LX/Cxw;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->e:LX/Cxw;

    .line 1951884
    invoke-static {p3, p0}, LX/CxE;->a(LX/CzA;LX/CxP;)LX/CxD;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->f:LX/CxD;

    .line 1951885
    move-object/from16 v0, p21

    iput-object v0, p0, LX/Cxl;->g:LX/1Q2;

    .line 1951886
    invoke-static {p4}, LX/CyG;->a(LX/CyE;)LX/CyF;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->h:LX/CyF;

    .line 1951887
    move-object/from16 v0, p23

    iput-object v0, p0, LX/Cxl;->i:LX/1Q3;

    .line 1951888
    move-object/from16 v0, p24

    iput-object v0, p0, LX/Cxl;->j:LX/1Q4;

    .line 1951889
    move-object/from16 v0, p25

    invoke-virtual {v0, p5}, LX/1Q5;->a(Ljava/lang/String;)LX/1QM;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->k:LX/1QM;

    .line 1951890
    invoke-static {p6}, LX/1Q6;->a(Landroid/content/Context;)LX/1QP;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->l:LX/1QP;

    .line 1951891
    move-object/from16 v0, p27

    iput-object v0, p0, LX/Cxl;->m:LX/1Q7;

    .line 1951892
    invoke-static {p7}, LX/CxM;->a(LX/CzA;)LX/CxL;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->n:LX/CxL;

    .line 1951893
    invoke-static {p8}, LX/1QA;->a(LX/1PT;)LX/1QQ;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->o:LX/1QQ;

    .line 1951894
    invoke-static {p9}, LX/CxO;->a(LX/CxF;)LX/CxN;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->p:LX/CxN;

    .line 1951895
    move-object/from16 v0, p31

    iput-object v0, p0, LX/Cxl;->q:Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    .line 1951896
    invoke-static {p10}, LX/1QC;->a(Ljava/lang/Runnable;)LX/1QR;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->r:LX/1QR;

    .line 1951897
    move-object/from16 v0, p33

    iput-object v0, p0, LX/Cxl;->s:LX/1QD;

    .line 1951898
    move-object/from16 v0, p34

    invoke-virtual {v0, p0}, LX/1QE;->a(LX/1Pf;)LX/1QS;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->t:LX/1QS;

    .line 1951899
    move-object/from16 v0, p35

    iput-object v0, p0, LX/Cxl;->u:LX/1QF;

    .line 1951900
    move-object/from16 v0, p36

    iput-object v0, p0, LX/Cxl;->v:LX/1QG;

    .line 1951901
    move-object/from16 v0, p37

    iput-object v0, p0, LX/Cxl;->w:LX/99Q;

    .line 1951902
    move-object/from16 v0, p38

    invoke-virtual {v0, p11}, LX/1QI;->a(LX/1PY;)LX/1PU;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->x:LX/1PU;

    .line 1951903
    invoke-static {p12}, LX/CxZ;->a(LX/CzA;)LX/CxY;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->y:LX/CxY;

    .line 1951904
    invoke-static {p13}, LX/CxT;->a(LX/CzA;)LX/CxS;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->z:LX/CxS;

    .line 1951905
    invoke-static/range {p14 .. p14}, LX/CxX;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)LX/CxW;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->A:LX/CxW;

    .line 1951906
    move-object/from16 v0, p42

    iput-object v0, p0, LX/Cxl;->B:LX/Cxb;

    .line 1951907
    move-object/from16 v0, p43

    invoke-virtual {v0, p0}, LX/CyK;->a(LX/1Pr;)LX/CyJ;

    move-result-object v1

    iput-object v1, p0, LX/Cxl;->C:LX/CyJ;

    .line 1951908
    move-object/from16 v0, p44

    iput-object v0, p0, LX/Cxl;->D:LX/1QK;

    .line 1951909
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I
    .locals 1

    .prologue
    .line 1951923
    iget-object v0, p0, LX/Cxl;->z:LX/CxS;

    invoke-virtual {v0, p1}, LX/CxS;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1951922
    iget-object v0, p0, LX/Cxl;->n:LX/CxL;

    invoke-virtual {v0, p1}, LX/CxL;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a()LX/1Q9;
    .locals 1

    .prologue
    .line 1951921
    iget-object v0, p0, LX/Cxl;->m:LX/1Q7;

    invoke-virtual {v0}, LX/1Q7;->a()LX/1Q9;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;
    .locals 6

    .prologue
    .line 1951920
    iget-object v0, p0, LX/Cxl;->c:LX/1Q0;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Q0;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/CyM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I)",
            "LX/CyM;"
        }
    .end annotation

    .prologue
    .line 1951919
    iget-object v0, p0, LX/Cxl;->C:LX/CyJ;

    invoke-virtual {v0, p1, p2}, LX/CyJ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/CyM;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/Cyv;
    .locals 1

    .prologue
    .line 1951918
    iget-object v0, p0, LX/Cxl;->y:LX/CxY;

    invoke-virtual {v0, p1}, LX/CxY;->a(I)LX/Cyv;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1951917
    iget-object v0, p0, LX/Cxl;->u:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1951867
    iget-object v0, p0, LX/Cxl;->u:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1R6;)V
    .locals 1

    .prologue
    .line 1951914
    iget-object v0, p0, LX/Cxl;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a(LX/1R6;)V

    .line 1951915
    return-void
.end method

.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 1951912
    iget-object v0, p0, LX/Cxl;->w:LX/99Q;

    invoke-virtual {v0, p1}, LX/99Q;->a(LX/1Rb;)V

    .line 1951913
    return-void
.end method

.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1951910
    iget-object v0, p0, LX/Cxl;->q:Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1951911
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1951844
    iget-object v0, p0, LX/Cxl;->w:LX/99Q;

    invoke-virtual {v0, p1, p2}, LX/99Q;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1951845
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1951876
    iget-object v0, p0, LX/Cxl;->w:LX/99Q;

    invoke-virtual {v0, p1, p2, p3}, LX/99Q;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1951877
    return-void
.end method

.method public final a(LX/22C;)V
    .locals 1

    .prologue
    .line 1951874
    iget-object v0, p0, LX/Cxl;->j:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->a(LX/22C;)V

    .line 1951875
    return-void
.end method

.method public final a(LX/34p;)V
    .locals 1

    .prologue
    .line 1951872
    iget-object v0, p0, LX/Cxl;->j:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->a(LX/34p;)V

    .line 1951873
    return-void
.end method

.method public final a(LX/5Oj;)V
    .locals 1

    .prologue
    .line 1951870
    iget-object v0, p0, LX/Cxl;->x:LX/1PU;

    invoke-virtual {v0, p1}, LX/1PU;->a(LX/5Oj;)V

    .line 1951871
    return-void
.end method

.method public final a(LX/CyI;)V
    .locals 1

    .prologue
    .line 1951868
    iget-object v0, p0, LX/Cxl;->h:LX/CyF;

    invoke-virtual {v0, p1}, LX/CyF;->a(LX/CyI;)V

    .line 1951869
    return-void
.end method

.method public final a(LX/CyI;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CyI;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1951928
    iget-object v0, p0, LX/Cxl;->h:LX/CyF;

    invoke-virtual {v0, p1, p2}, LX/CyF;->a(LX/CyI;LX/0Px;)V

    .line 1951929
    return-void
.end method

.method public final a(LX/CzL;LX/0P1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1951957
    iget-object v0, p0, LX/Cxl;->e:LX/Cxw;

    invoke-virtual {v0, p1, p2}, LX/Cxw;->a(LX/CzL;LX/0P1;)V

    .line 1951958
    return-void
.end method

.method public final a(LX/CzL;LX/CzL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/CzL",
            "<+TT;>;",
            "LX/CzL",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1951955
    iget-object v0, p0, LX/Cxl;->f:LX/CxD;

    invoke-virtual {v0, p1, p2}, LX/CxD;->a(LX/CzL;LX/CzL;)V

    .line 1951956
    return-void
.end method

.method public final a(LX/D0P;Ljava/lang/String;LX/CzL;LX/0gW;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/D0P;",
            "Ljava/lang/String;",
            "LX/CzL",
            "<+TT;>;",
            "LX/0gW",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 1951953
    iget-object v0, p0, LX/Cxl;->a:LX/Cx3;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/Cx3;->a(LX/D0P;Ljava/lang/String;LX/CzL;LX/0gW;)V

    .line 1951954
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1951951
    iget-object v0, p0, LX/Cxl;->v:LX/1QG;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1QG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1951952
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1951949
    iget-object v0, p0, LX/Cxl;->d:LX/1QL;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1QL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1951950
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1951947
    iget-object v0, p0, LX/Cxl;->f:LX/CxD;

    invoke-virtual {v0, p1}, LX/CxD;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1951948
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 1951945
    iget-object v0, p0, LX/Cxl;->g:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1951946
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1951943
    iget-object v0, p0, LX/Cxl;->g:LX/1Q2;

    invoke-virtual {v0, p1, p2}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    .line 1951944
    return-void
.end method

.method public final a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)V
    .locals 1

    .prologue
    .line 1951941
    iget-object v0, p0, LX/Cxl;->e:LX/Cxw;

    invoke-virtual {v0, p1}, LX/Cxw;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)V

    .line 1951942
    return-void
.end method

.method public final a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V
    .locals 1

    .prologue
    .line 1951926
    iget-object v0, p0, LX/Cxl;->f:LX/CxD;

    invoke-virtual {v0, p1, p2}, LX/CxD;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V

    .line 1951927
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1951939
    iget-object v0, p0, LX/Cxl;->q:Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    invoke-virtual {v0, p1}, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a(Ljava/lang/String;)V

    .line 1951940
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1951937
    iget-object v0, p0, LX/Cxl;->b:LX/1Pz;

    invoke-virtual {v0, p1, p2}, LX/1Pz;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1951938
    return-void
.end method

.method public final a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 1951935
    iget-object v0, p0, LX/Cxl;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1951936
    return-void
.end method

.method public final a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1951933
    iget-object v0, p0, LX/Cxl;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Ljava/lang/Object;)V

    .line 1951934
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 1951932
    iget-object v0, p0, LX/Cxl;->u:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/CzL;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1951931
    iget-object v0, p0, LX/Cxl;->f:LX/CxD;

    invoke-virtual {v0, p1}, LX/CxD;->a(LX/CzL;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1951930
    iget-object v0, p0, LX/Cxl;->f:LX/CxD;

    invoke-virtual {v0, p1}, LX/CxD;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/CzL;)I
    .locals 1

    .prologue
    .line 1951916
    iget-object v0, p0, LX/Cxl;->z:LX/CxS;

    invoke-virtual {v0, p1}, LX/CxS;->b(LX/CzL;)I

    move-result v0

    return v0
.end method

.method public final b(LX/1R6;)V
    .locals 1

    .prologue
    .line 1951924
    iget-object v0, p0, LX/Cxl;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->b(LX/1R6;)V

    .line 1951925
    return-void
.end method

.method public final b(LX/22C;)V
    .locals 1

    .prologue
    .line 1951840
    iget-object v0, p0, LX/Cxl;->j:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->b(LX/22C;)V

    .line 1951841
    return-void
.end method

.method public final b(LX/5Oj;)V
    .locals 1

    .prologue
    .line 1951838
    iget-object v0, p0, LX/Cxl;->x:LX/1PU;

    invoke-virtual {v0, p1}, LX/1PU;->b(LX/5Oj;)V

    .line 1951839
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1951836
    iget-object v0, p0, LX/Cxl;->e:LX/Cxw;

    invoke-virtual {v0, p1}, LX/Cxw;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1951837
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 1951834
    iget-object v0, p0, LX/Cxl;->g:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1951835
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1951832
    iget-object v0, p0, LX/Cxl;->u:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 1951833
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1951830
    iget-object v0, p0, LX/Cxl;->b:LX/1Pz;

    invoke-virtual {v0, p1, p2}, LX/1Pz;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1951831
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1951828
    iget-object v0, p0, LX/Cxl;->s:LX/1QD;

    invoke-virtual {v0, p1}, LX/1QD;->b(Z)V

    .line 1951829
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 1951827
    iget-object v0, p0, LX/Cxl;->o:LX/1QQ;

    invoke-virtual {v0}, LX/1QQ;->c()LX/1PT;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;
    .locals 1

    .prologue
    .line 1951826
    iget-object v0, p0, LX/Cxl;->f:LX/CxD;

    invoke-virtual {v0, p1}, LX/CxD;->c(Ljava/lang/String;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/CzL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1951815
    iget-object v0, p0, LX/Cxl;->e:LX/Cxw;

    invoke-virtual {v0, p1}, LX/Cxw;->c(LX/CzL;)V

    .line 1951816
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 1951825
    iget-object v0, p0, LX/Cxl;->g:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final d(LX/CzL;)LX/CyM;
    .locals 1

    .prologue
    .line 1951824
    iget-object v0, p0, LX/Cxl;->C:LX/CyJ;

    invoke-virtual {v0, p1}, LX/CyJ;->d(LX/CzL;)LX/CyM;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 1951823
    iget-object v0, p0, LX/Cxl;->t:LX/1QS;

    invoke-virtual {v0}, LX/1QS;->e()LX/1SX;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 1951822
    iget-object v0, p0, LX/Cxl;->v:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsModule()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "analyticsModule"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 1951821
    iget-object v0, p0, LX/Cxl;->k:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getAnalyticsModule()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1951820
    iget-object v0, p0, LX/Cxl;->l:LX/1QP;

    invoke-virtual {v0}, LX/1QP;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getFontFoundry()LX/1QO;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 1951819
    iget-object v0, p0, LX/Cxl;->k:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getFontFoundry()LX/1QO;

    move-result-object v0

    return-object v0
.end method

.method public getNavigator()LX/5KM;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "navigator"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 1951818
    iget-object v0, p0, LX/Cxl;->k:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getNavigator()LX/5KM;

    move-result-object v0

    return-object v0
.end method

.method public getTraitCollection()LX/1QN;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "traitCollection"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 1951817
    iget-object v0, p0, LX/Cxl;->k:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getTraitCollection()LX/1QN;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 1951856
    iget-object v0, p0, LX/Cxl;->v:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951866
    iget-object v0, p0, LX/Cxl;->v:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->i()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 1951865
    iget-object v0, p0, LX/Cxl;->v:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 1951863
    iget-object v0, p0, LX/Cxl;->r:LX/1QR;

    invoke-virtual {v0}, LX/1QR;->iN_()V

    .line 1951864
    return-void
.end method

.method public final iO_()Z
    .locals 1

    .prologue
    .line 1951862
    iget-object v0, p0, LX/Cxl;->s:LX/1QD;

    invoke-virtual {v0}, LX/1QD;->iO_()Z

    move-result v0

    return v0
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951861
    iget-object v0, p0, LX/Cxl;->v:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->j()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1951859
    iget-object v0, p0, LX/Cxl;->v:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->k()V

    .line 1951860
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1951858
    iget-object v0, p0, LX/Cxl;->w:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->l()Z

    move-result v0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 1951857
    iget-object v0, p0, LX/Cxl;->w:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final m_(Z)V
    .locals 1

    .prologue
    .line 1951842
    iget-object v0, p0, LX/Cxl;->r:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->m_(Z)V

    .line 1951843
    return-void
.end method

.method public final n()LX/1SX;
    .locals 1

    .prologue
    .line 1951855
    iget-object v0, p0, LX/Cxl;->p:LX/CxN;

    invoke-virtual {v0}, LX/CxN;->n()LX/1SX;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 1951854
    iget-object v0, p0, LX/Cxl;->w:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1951852
    iget-object v0, p0, LX/Cxl;->w:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->p()V

    .line 1951853
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 1951851
    iget-object v0, p0, LX/Cxl;->w:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->q()Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1951850
    iget-object v0, p0, LX/Cxl;->D:LX/1QK;

    invoke-virtual {v0}, LX/1QK;->r()Z

    move-result v0

    return v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 1951849
    iget-object v0, p0, LX/Cxl;->y:LX/CxY;

    invoke-virtual {v0}, LX/CxY;->s()I

    move-result v0

    return v0
.end method

.method public final t()Lcom/facebook/search/results/model/SearchResultsMutableContext;
    .locals 1

    .prologue
    .line 1951848
    iget-object v0, p0, LX/Cxl;->A:LX/CxW;

    invoke-virtual {v0}, LX/CxW;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    return-object v0
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 1951846
    iget-object v0, p0, LX/Cxl;->B:LX/Cxb;

    invoke-virtual {v0}, LX/Cxb;->u()V

    .line 1951847
    return-void
.end method
