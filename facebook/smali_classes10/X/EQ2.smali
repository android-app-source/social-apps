.class public final enum LX/EQ2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EQ2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EQ2;

.field public static final enum DO_NOT_UPDATE:LX/EQ2;

.field public static final enum USE_GRAPH_API:LX/EQ2;

.field public static final enum USE_SERP_ENDPOINT:LX/EQ2;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2118130
    new-instance v0, LX/EQ2;

    const-string v1, "DO_NOT_UPDATE"

    invoke-direct {v0, v1, v2}, LX/EQ2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQ2;->DO_NOT_UPDATE:LX/EQ2;

    .line 2118131
    new-instance v0, LX/EQ2;

    const-string v1, "USE_GRAPH_API"

    invoke-direct {v0, v1, v3}, LX/EQ2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQ2;->USE_GRAPH_API:LX/EQ2;

    .line 2118132
    new-instance v0, LX/EQ2;

    const-string v1, "USE_SERP_ENDPOINT"

    invoke-direct {v0, v1, v4}, LX/EQ2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQ2;->USE_SERP_ENDPOINT:LX/EQ2;

    .line 2118133
    const/4 v0, 0x3

    new-array v0, v0, [LX/EQ2;

    sget-object v1, LX/EQ2;->DO_NOT_UPDATE:LX/EQ2;

    aput-object v1, v0, v2

    sget-object v1, LX/EQ2;->USE_GRAPH_API:LX/EQ2;

    aput-object v1, v0, v3

    sget-object v1, LX/EQ2;->USE_SERP_ENDPOINT:LX/EQ2;

    aput-object v1, v0, v4

    sput-object v0, LX/EQ2;->$VALUES:[LX/EQ2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2118129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EQ2;
    .locals 1

    .prologue
    .line 2118134
    const-class v0, LX/EQ2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EQ2;

    return-object v0
.end method

.method public static values()[LX/EQ2;
    .locals 1

    .prologue
    .line 2118128
    sget-object v0, LX/EQ2;->$VALUES:[LX/EQ2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EQ2;

    return-object v0
.end method
