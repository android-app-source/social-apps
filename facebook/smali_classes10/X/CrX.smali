.class public LX/CrX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/hardware/SensorEventListener;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/Crc;

.field private final c:[F

.field public final d:[F

.field public e:I

.field public f:Z

.field public g:Z

.field public h:F

.field public i:F

.field public j:F

.field public k:F

.field public final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/richdocument/view/transition/motion/MediaTiltEventListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 1941170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941171
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, LX/CrX;->c:[F

    .line 1941172
    sget v0, LX/CoL;->i:I

    new-array v0, v0, [F

    iput-object v0, p0, LX/CrX;->d:[F

    .line 1941173
    const/4 v0, 0x0

    iput v0, p0, LX/CrX;->e:I

    .line 1941174
    sget v0, LX/CoL;->h:F

    iput v0, p0, LX/CrX;->i:F

    .line 1941175
    sget v0, LX/CoL;->j:F

    iput v0, p0, LX/CrX;->j:F

    .line 1941176
    sget v0, LX/CoL;->k:F

    iput v0, p0, LX/CrX;->k:F

    .line 1941177
    iput-object p1, p0, LX/CrX;->a:Landroid/content/Context;

    .line 1941178
    new-instance v0, LX/Crc;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, LX/Crc;-><init>(F)V

    iput-object v0, p0, LX/CrX;->b:LX/Crc;

    .line 1941179
    iget-object v0, p0, LX/CrX;->b:LX/Crc;

    const-wide/16 v2, 0x6e

    .line 1941180
    iput-wide v2, v0, LX/Crc;->b:J

    .line 1941181
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/CrX;->l:Ljava/util/List;

    .line 1941182
    return-void
.end method

.method private static a(FJ)F
    .locals 5

    .prologue
    .line 1941169
    float-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    long-to-float v2, p1

    float-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private static c(LX/CrX;)V
    .locals 2

    .prologue
    .line 1941164
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CrX;->f:Z

    .line 1941165
    iget-object v0, p0, LX/CrX;->a:Landroid/content/Context;

    move-object v0, v0

    .line 1941166
    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 1941167
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 1941168
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1941160
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CrX;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1941161
    invoke-static {p0}, LX/CrX;->c(LX/CrX;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1941162
    monitor-exit p0

    return-void

    .line 1941163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CuI;)V
    .locals 2

    .prologue
    .line 1941146
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CrX;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1941147
    iget-object v0, p0, LX/CrX;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1941148
    const/4 p1, 0x1

    const/4 v1, 0x0

    .line 1941149
    iget-boolean v0, p0, LX/CrX;->f:Z

    if-eqz v0, :cond_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1941150
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1941151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1941152
    :cond_1
    iput-boolean p1, p0, LX/CrX;->f:Z

    .line 1941153
    iput-boolean p1, p0, LX/CrX;->g:Z

    .line 1941154
    iput v1, p0, LX/CrX;->h:F

    .line 1941155
    iget-object v0, p0, LX/CrX;->d:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 1941156
    const/4 v0, 0x0

    iput v0, p0, LX/CrX;->e:I

    .line 1941157
    iget-object v0, p0, LX/CrX;->a:Landroid/content/Context;

    move-object v0, v0

    .line 1941158
    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 1941159
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    const/4 p1, 0x2

    invoke-virtual {v0, p0, v1, p1}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0
.end method

.method public final declared-synchronized b(LX/CuI;)V
    .locals 1

    .prologue
    .line 1941141
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CrX;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1941142
    iget-object v0, p0, LX/CrX;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941143
    invoke-static {p0}, LX/CrX;->c(LX/CrX;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1941144
    :cond_0
    monitor-exit p0

    return-void

    .line 1941145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 1941140
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 1941091
    iget-boolean v0, p0, LX/CrX;->f:Z

    if-nez v0, :cond_1

    .line 1941092
    :cond_0
    :goto_0
    return-void

    .line 1941093
    :cond_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1941094
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v1, p0, LX/CrX;->c:[F

    const/4 v2, 0x3

    invoke-static {v0, v6, v1, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1941095
    iget-object v0, p0, LX/CrX;->b:LX/Crc;

    iget-wide v2, p1, Landroid/hardware/SensorEvent;->timestamp:J

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    iget-object v1, p0, LX/CrX;->c:[F

    invoke-virtual {v0, v2, v3, v1}, LX/Crc;->a(J[F)V

    .line 1941096
    iget-object v0, p0, LX/CrX;->b:LX/Crc;

    .line 1941097
    iget-wide v7, v0, LX/Crc;->e:J

    move-wide v0, v7

    .line 1941098
    iget-object v2, p0, LX/CrX;->b:LX/Crc;

    .line 1941099
    iget-object v3, v2, LX/Crc;->c:[F

    if-eqz v3, :cond_5

    iget-object v3, v2, LX/Crc;->c:[F

    invoke-static {v3}, LX/Crc;->a([F)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_1
    move v2, v3

    .line 1941100
    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1941101
    iget-object v2, p0, LX/CrX;->b:LX/Crc;

    .line 1941102
    iget-object v3, v2, LX/Crc;->c:[F

    move-object v2, v3

    .line 1941103
    aget v3, v2, v6

    invoke-static {v3, v0, v1}, LX/CrX;->a(FJ)F

    move-result v3

    .line 1941104
    const/high16 v4, -0x40800000    # -1.0f

    const/4 v5, 0x1

    aget v5, v2, v5

    invoke-static {v5, v0, v1}, LX/CrX;->a(FJ)F

    move-result v5

    mul-float/2addr v4, v5

    .line 1941105
    const/4 v5, 0x2

    aget v2, v2, v5

    invoke-static {v2, v0, v1}, LX/CrX;->a(FJ)F

    move-result v0

    .line 1941106
    iget-object v1, p0, LX/CrX;->d:[F

    iget v2, p0, LX/CrX;->e:I

    aput v4, v1, v2

    .line 1941107
    iget v1, p0, LX/CrX;->e:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, LX/CrX;->d:[F

    array-length v2, v2

    rem-int/2addr v1, v2

    iput v1, p0, LX/CrX;->e:I

    .line 1941108
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1941109
    const/4 v6, 0x0

    .line 1941110
    iget-object v7, p0, LX/CrX;->d:[F

    array-length v8, v7

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v8, :cond_2

    aget p1, v7, v1

    .line 1941111
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    add-float/2addr v6, p1

    .line 1941112
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1941113
    :cond_2
    iget-object v1, p0, LX/CrX;->d:[F

    array-length v1, v1

    int-to-float v1, v1

    div-float v1, v6, v1

    move v1, v1

    .line 1941114
    iget v6, p0, LX/CrX;->j:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_6

    .line 1941115
    :cond_3
    :goto_3
    move v0, v5

    .line 1941116
    if-eqz v0, :cond_0

    .line 1941117
    const/high16 v3, -0x40800000    # -1.0f

    .line 1941118
    iget-boolean v0, p0, LX/CrX;->g:Z

    if-eqz v0, :cond_9

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LX/CrX;->i:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_9

    .line 1941119
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CrX;->g:Z

    .line 1941120
    :cond_4
    :goto_4
    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 1941121
    :cond_6
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 1941122
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v7, p0, LX/CrX;->k:F

    mul-float/2addr v7, v6

    cmpl-float v1, v1, v7

    if-lez v1, :cond_7

    move v1, v2

    .line 1941123
    :goto_5
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v7

    iget v8, p0, LX/CrX;->k:F

    mul-float/2addr v6, v8

    cmpl-float v6, v7, v6

    if-lez v6, :cond_8

    move v6, v2

    .line 1941124
    :goto_6
    if-nez v1, :cond_3

    if-nez v6, :cond_3

    move v5, v2

    goto :goto_3

    :cond_7
    move v1, v5

    .line 1941125
    goto :goto_5

    :cond_8
    move v6, v5

    .line 1941126
    goto :goto_6

    .line 1941127
    :cond_9
    iget v0, p0, LX/CrX;->h:F

    .line 1941128
    iget v1, p0, LX/CrX;->h:F

    add-float/2addr v1, v4

    iput v1, p0, LX/CrX;->h:F

    .line 1941129
    iget v1, p0, LX/CrX;->h:F

    iget v2, p0, LX/CrX;->i:F

    mul-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_c

    .line 1941130
    iget v1, p0, LX/CrX;->i:F

    mul-float/2addr v1, v3

    iput v1, p0, LX/CrX;->h:F

    .line 1941131
    :cond_a
    :goto_7
    iget v1, p0, LX/CrX;->h:F

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LX/CrX;->j:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 1941132
    iget v0, p0, LX/CrX;->h:F

    iget v1, p0, LX/CrX;->i:F

    div-float/2addr v0, v1

    .line 1941133
    iget-object v1, p0, LX/CrX;->l:Ljava/util/List;

    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1941134
    iget-object v1, p0, LX/CrX;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CuI;

    .line 1941135
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/CuI;->a(Ljava/lang/Float;)V

    .line 1941136
    goto :goto_8

    .line 1941137
    :cond_b
    goto :goto_4

    .line 1941138
    :cond_c
    iget v1, p0, LX/CrX;->h:F

    iget v2, p0, LX/CrX;->i:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_a

    .line 1941139
    iget v1, p0, LX/CrX;->i:F

    iput v1, p0, LX/CrX;->h:F

    goto :goto_7
.end method
