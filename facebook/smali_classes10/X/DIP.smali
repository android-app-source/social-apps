.class public LX/DIP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/nux/NuxFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;Landroid/support/v4/view/ViewPager;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/nux/NuxFragment;",
            ">;",
            "Landroid/support/v4/view/ViewPager;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1983443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1983444
    iput-object p1, p0, LX/DIP;->a:LX/0Px;

    move v1, v2

    .line 1983445
    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1983446
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/nux/NuxFragment;

    .line 1983447
    iput-boolean v4, v0, Lcom/facebook/goodfriends/nux/NuxFragment;->c:Z

    .line 1983448
    iput-object p2, v0, Lcom/facebook/goodfriends/nux/NuxFragment;->a:Landroid/support/v4/view/ViewPager;

    .line 1983449
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_0

    move v3, v4

    .line 1983450
    :goto_1
    iput-boolean v3, v0, Lcom/facebook/goodfriends/nux/NuxFragment;->b:Z

    .line 1983451
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, v2

    .line 1983452
    goto :goto_1

    .line 1983453
    :cond_1
    return-void
.end method
