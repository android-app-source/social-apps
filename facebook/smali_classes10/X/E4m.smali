.class public LX/E4m;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E4m;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E4n;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E4m",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E4n;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077266
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2077267
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E4m;->b:LX/0Zi;

    .line 2077268
    iput-object p1, p0, LX/E4m;->a:LX/0Ot;

    .line 2077269
    return-void
.end method

.method public static a(LX/0QB;)LX/E4m;
    .locals 4

    .prologue
    .line 2077253
    sget-object v0, LX/E4m;->c:LX/E4m;

    if-nez v0, :cond_1

    .line 2077254
    const-class v1, LX/E4m;

    monitor-enter v1

    .line 2077255
    :try_start_0
    sget-object v0, LX/E4m;->c:LX/E4m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077256
    if-eqz v2, :cond_0

    .line 2077257
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077258
    new-instance v3, LX/E4m;

    const/16 p0, 0x3113

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E4m;-><init>(LX/0Ot;)V

    .line 2077259
    move-object v0, v3

    .line 2077260
    sput-object v0, LX/E4m;->c:LX/E4m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077261
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077262
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077263
    :cond_1
    sget-object v0, LX/E4m;->c:LX/E4m;

    return-object v0

    .line 2077264
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077265
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2077250
    check-cast p2, LX/E4l;

    .line 2077251
    iget-object v0, p0, LX/E4m;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E4n;

    iget-object v2, p2, LX/E4l;->a:LX/0Px;

    iget-object v3, p2, LX/E4l;->b:LX/2km;

    iget-object v4, p2, LX/E4l;->c:Ljava/lang/String;

    iget-object v5, p2, LX/E4l;->d:Ljava/lang/String;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/E4n;->a(LX/1De;LX/0Px;LX/2km;Ljava/lang/String;Ljava/lang/String;)LX/1Dg;

    move-result-object v0

    .line 2077252
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2077248
    invoke-static {}, LX/1dS;->b()V

    .line 2077249
    const/4 v0, 0x0

    return-object v0
.end method
