.class public final LX/Djs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/4Ji;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:LX/Djv;


# direct methods
.method public constructor <init>(LX/Djv;Ljava/lang/String;Ljava/lang/String;LX/4Ji;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2033807
    iput-object p1, p0, LX/Djs;->g:LX/Djv;

    iput-object p2, p0, LX/Djs;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Djs;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Djs;->c:LX/4Ji;

    iput-object p5, p0, LX/Djs;->d:Ljava/lang/String;

    iput-object p6, p0, LX/Djs;->e:Ljava/lang/String;

    iput-object p7, p0, LX/Djs;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2033808
    iget-object v0, p0, LX/Djs;->g:LX/Djv;

    iget-object v1, p0, LX/Djs;->a:Ljava/lang/String;

    iget-object v2, p0, LX/Djs;->b:Ljava/lang/String;

    iget-object v3, p0, LX/Djs;->c:LX/4Ji;

    iget-object v4, p0, LX/Djs;->d:Ljava/lang/String;

    iget-object v5, p0, LX/Djs;->e:Ljava/lang/String;

    iget-object v6, p0, LX/Djs;->f:Ljava/lang/String;

    .line 2033809
    new-instance v7, LX/Djo;

    invoke-direct {v7}, LX/Djo;-><init>()V

    move-object v7, v7

    .line 2033810
    new-instance v8, LX/4JA;

    invoke-direct {v8}, LX/4JA;-><init>()V

    .line 2033811
    const-string p0, "action"

    invoke-virtual {v8, p0, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033812
    move-object v8, v8

    .line 2033813
    const-string p0, "actor_id"

    invoke-virtual {v8, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033814
    move-object v8, v8

    .line 2033815
    const-string p0, "client_mutation_id"

    invoke-virtual {v8, p0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033816
    move-object v8, v8

    .line 2033817
    const-string p0, "component_flow_request_id"

    invoke-virtual {v8, p0, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033818
    move-object v8, v8

    .line 2033819
    if-eqz v6, :cond_0

    .line 2033820
    const-string p0, "request_note_id"

    invoke-virtual {v8, p0, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033821
    :cond_0
    if-eqz v3, :cond_1

    .line 2033822
    const-string p0, "body"

    invoke-virtual {v8, p0, v3}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2033823
    :cond_1
    const-string p0, "input"

    invoke-virtual {v7, p0, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2033824
    iget-object v8, v0, LX/Djv;->a:LX/0tX;

    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v7, v7

    .line 2033825
    move-object v0, v7

    .line 2033826
    return-object v0
.end method
