.class public LX/E4S;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z

.field public b:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2076708
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/E4S;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2076709
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2076704
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2076705
    const v0, 0x7f031155

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2076706
    const v0, 0x7f0d28ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/E4S;->b:Landroid/widget/LinearLayout;

    .line 2076707
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2076684
    iget-boolean v0, p0, LX/E4S;->a:Z

    return v0
.end method

.method public final addView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2076702
    iget-object v0, p0, LX/E4S;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2076703
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6ed8f02a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2076698
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2076699
    const/4 v1, 0x1

    .line 2076700
    iput-boolean v1, p0, LX/E4S;->a:Z

    .line 2076701
    const/16 v1, 0x2d

    const v2, -0x34a7fa78    # -1.4157192E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5884ce26

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2076694
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2076695
    const/4 v1, 0x0

    .line 2076696
    iput-boolean v1, p0, LX/E4S;->a:Z

    .line 2076697
    const/16 v1, 0x2d

    const v2, 0x6e18b7d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final removeAllViews()V
    .locals 1

    .prologue
    .line 2076692
    iget-object v0, p0, LX/E4S;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2076693
    return-void
.end method

.method public setBottomMargin(F)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2076685
    invoke-virtual {p0}, LX/E4S;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2076686
    if-nez v0, :cond_0

    .line 2076687
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2076688
    :cond_0
    invoke-virtual {p0}, LX/E4S;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 2076689
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2076690
    invoke-virtual {p0, v0}, LX/E4S;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2076691
    return-void
.end method
