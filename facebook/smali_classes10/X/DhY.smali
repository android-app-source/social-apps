.class public final enum LX/DhY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DhY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DhY;

.field public static final enum CENTER:LX/DhY;

.field public static final enum LEFT:LX/DhY;

.field public static final enum RIGHT:LX/DhY;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2031039
    new-instance v0, LX/DhY;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LX/DhY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhY;->LEFT:LX/DhY;

    .line 2031040
    new-instance v0, LX/DhY;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3}, LX/DhY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhY;->CENTER:LX/DhY;

    .line 2031041
    new-instance v0, LX/DhY;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, LX/DhY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhY;->RIGHT:LX/DhY;

    .line 2031042
    const/4 v0, 0x3

    new-array v0, v0, [LX/DhY;

    sget-object v1, LX/DhY;->LEFT:LX/DhY;

    aput-object v1, v0, v2

    sget-object v1, LX/DhY;->CENTER:LX/DhY;

    aput-object v1, v0, v3

    sget-object v1, LX/DhY;->RIGHT:LX/DhY;

    aput-object v1, v0, v4

    sput-object v0, LX/DhY;->$VALUES:[LX/DhY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2031038
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static from(Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;)LX/DhY;
    .locals 2

    .prologue
    .line 2031033
    sget-object v0, LX/DhX;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMessengerMontageAssetHorizontalAlignmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2031034
    :pswitch_0
    sget-object v0, LX/DhY;->CENTER:LX/DhY;

    .line 2031035
    :goto_0
    return-object v0

    .line 2031036
    :pswitch_1
    sget-object v0, LX/DhY;->LEFT:LX/DhY;

    goto :goto_0

    .line 2031037
    :pswitch_2
    sget-object v0, LX/DhY;->RIGHT:LX/DhY;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/DhY;
    .locals 1

    .prologue
    .line 2031032
    const-class v0, LX/DhY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DhY;

    return-object v0
.end method

.method public static values()[LX/DhY;
    .locals 1

    .prologue
    .line 2031031
    sget-object v0, LX/DhY;->$VALUES:[LX/DhY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DhY;

    return-object v0
.end method
