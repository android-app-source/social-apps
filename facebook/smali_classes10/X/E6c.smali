.class public final enum LX/E6c;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E6c;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E6c;

.field public static final enum FIG_STANDARD_PADDING:LX/E6c;

.field public static final enum MULTI_ATTACHMENT_PADDING:LX/E6c;

.field public static final enum NO_PADDING:LX/E6c;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2080399
    new-instance v0, LX/E6c;

    const-string v1, "NO_PADDING"

    invoke-direct {v0, v1, v2}, LX/E6c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E6c;->NO_PADDING:LX/E6c;

    .line 2080400
    new-instance v0, LX/E6c;

    const-string v1, "FIG_STANDARD_PADDING"

    invoke-direct {v0, v1, v3}, LX/E6c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E6c;->FIG_STANDARD_PADDING:LX/E6c;

    .line 2080401
    new-instance v0, LX/E6c;

    const-string v1, "MULTI_ATTACHMENT_PADDING"

    invoke-direct {v0, v1, v4}, LX/E6c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E6c;->MULTI_ATTACHMENT_PADDING:LX/E6c;

    .line 2080402
    const/4 v0, 0x3

    new-array v0, v0, [LX/E6c;

    sget-object v1, LX/E6c;->NO_PADDING:LX/E6c;

    aput-object v1, v0, v2

    sget-object v1, LX/E6c;->FIG_STANDARD_PADDING:LX/E6c;

    aput-object v1, v0, v3

    sget-object v1, LX/E6c;->MULTI_ATTACHMENT_PADDING:LX/E6c;

    aput-object v1, v0, v4

    sput-object v0, LX/E6c;->$VALUES:[LX/E6c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2080403
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/E6c;
    .locals 1

    .prologue
    .line 2080404
    const-class v0, LX/E6c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E6c;

    return-object v0
.end method

.method public static values()[LX/E6c;
    .locals 1

    .prologue
    .line 2080405
    sget-object v0, LX/E6c;->$VALUES:[LX/E6c;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E6c;

    return-object v0
.end method
