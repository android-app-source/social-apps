.class public LX/DHY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/3mL;

.field public final c:LX/1LV;

.field private final d:LX/DHW;

.field public final e:LX/2yK;

.field public final f:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1982063
    const-class v0, LX/DHY;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DHY;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/3mL;LX/1LV;LX/DHW;LX/2yK;LX/03V;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1982064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1982065
    iput-object p1, p0, LX/DHY;->b:LX/3mL;

    .line 1982066
    iput-object p2, p0, LX/DHY;->c:LX/1LV;

    .line 1982067
    iput-object p3, p0, LX/DHY;->d:LX/DHW;

    .line 1982068
    iput-object p4, p0, LX/DHY;->e:LX/2yK;

    .line 1982069
    iput-object p5, p0, LX/DHY;->f:LX/03V;

    .line 1982070
    return-void
.end method

.method public static a(LX/0QB;)LX/DHY;
    .locals 9

    .prologue
    .line 1982071
    const-class v1, LX/DHY;

    monitor-enter v1

    .line 1982072
    :try_start_0
    sget-object v0, LX/DHY;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1982073
    sput-object v2, LX/DHY;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1982074
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1982075
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1982076
    new-instance v3, LX/DHY;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v4

    check-cast v4, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v5

    check-cast v5, LX/1LV;

    const-class v6, LX/DHW;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/DHW;

    invoke-static {v0}, LX/2yK;->b(LX/0QB;)LX/2yK;

    move-result-object v7

    check-cast v7, LX/2yK;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v3 .. v8}, LX/DHY;-><init>(LX/3mL;LX/1LV;LX/DHW;LX/2yK;LX/03V;)V

    .line 1982077
    move-object v0, v3

    .line 1982078
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1982079
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DHY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1982080
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1982081
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pn;LX/DGZ;)LX/1Dg;
    .locals 11
    .param p2    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/DGZ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "LX/DGZ;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1982082
    iget-object v0, p3, LX/DGZ;->a:LX/99i;

    iget-object v0, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1982083
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1982084
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 1982085
    new-instance v1, LX/DHX;

    invoke-direct {v1, p0, v0}, LX/DHX;-><init>(LX/DHY;Lcom/facebook/graphql/model/GraphQLStorySet;)V

    .line 1982086
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 1982087
    iput-object v3, v2, LX/3mP;->d:LX/25L;

    .line 1982088
    move-object v2, v2

    .line 1982089
    iput-object v0, v2, LX/3mP;->e:LX/0jW;

    .line 1982090
    move-object v0, v2

    .line 1982091
    const/16 v2, 0x8

    .line 1982092
    iput v2, v0, LX/3mP;->b:I

    .line 1982093
    move-object v0, v0

    .line 1982094
    iput-object v1, v0, LX/3mP;->g:LX/25K;

    .line 1982095
    move-object v0, v0

    .line 1982096
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v0

    .line 1982097
    iget-object v1, p0, LX/DHY;->d:LX/DHW;

    const/4 v4, 0x0

    .line 1982098
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1982099
    iget-object v2, p3, LX/DGZ;->a:LX/99i;

    iget-object v6, v2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1982100
    iget-object v2, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1982101
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v2}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    move v3, v4

    .line 1982102
    :goto_0
    if-ge v3, v7, :cond_3

    .line 1982103
    new-instance v2, LX/DGm;

    invoke-direct {v2, v6, v3, v4, v4}, LX/DGm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V

    .line 1982104
    iget-object v8, v2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v8, v8

    .line 1982105
    if-eqz v8, :cond_0

    .line 1982106
    iget-object v8, v2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v8, v8

    .line 1982107
    invoke-static {v8}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v8

    if-nez v8, :cond_2

    .line 1982108
    :cond_0
    iget-object v8, p0, LX/DHY;->f:LX/03V;

    sget-object v9, LX/DHY;->a:Ljava/lang/String;

    .line 1982109
    iget-object v10, v2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v10

    .line 1982110
    if-nez v2, :cond_1

    const-string v2, "null story"

    :goto_1
    invoke-virtual {v8, v9, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1982111
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1982112
    :cond_1
    const-string v2, "null story attachment"

    goto :goto_1

    .line 1982113
    :cond_2
    new-instance v8, LX/DHU;

    iget-object v9, p3, LX/DGZ;->b:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-direct {v8, v2, v9}, LX/DHU;-><init>(LX/DGm;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1982114
    :cond_3
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 1982115
    new-instance v4, LX/DHV;

    move-object v7, p2

    check-cast v7, LX/1Pn;

    invoke-static {v1}, LX/DGu;->a(LX/0QB;)LX/DGu;

    move-result-object v9

    check-cast v9, LX/DGu;

    move-object v5, p1

    move-object v6, v2

    move-object v8, v0

    invoke-direct/range {v4 .. v9}, LX/DHV;-><init>(Landroid/content/Context;LX/0Px;LX/1Pn;LX/25M;LX/DGu;)V

    .line 1982116
    move-object v0, v4

    .line 1982117
    iget-object v1, p0, LX/DHY;->b:LX/3mL;

    invoke-virtual {v1, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x6

    const v2, 0x7f0b0f1f

    invoke-interface {v0, v1, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x7

    const v2, 0x7f0b0f20

    invoke-interface {v0, v1, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
