.class public final LX/EZ9;
.super LX/EZ8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EZ8",
        "<TFieldDescriptorType;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 2138816
    invoke-direct {p0, p1}, LX/EZ8;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2138817
    iget-boolean v0, p0, LX/EZ8;->d:Z

    move v0, v0

    .line 2138818
    if-nez v0, :cond_3

    .line 2138819
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, LX/EZ8;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2138820
    invoke-virtual {p0, v1}, LX/EZ8;->b(I)Ljava/util/Map$Entry;

    move-result-object v2

    .line 2138821
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2138822
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2138823
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2138824
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2138825
    :cond_1
    invoke-virtual {p0}, LX/EZ8;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2138826
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    invoke-virtual {v1}, LX/EYP;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2138827
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2138828
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2138829
    :cond_3
    invoke-super {p0}, LX/EZ8;->a()V

    .line 2138830
    return-void
.end method

.method public final synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2138831
    check-cast p1, LX/EYP;

    invoke-super {p0, p1, p2}, LX/EZ8;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
