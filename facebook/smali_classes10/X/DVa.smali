.class public LX/DVa;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DVa;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2005421
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2005422
    invoke-virtual {p0}, LX/398;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005423
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2005424
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUP_MEMBER_PICKER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2005425
    const-string v1, "group/members/search/{%s}"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "group_feed_id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2005426
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/DVa;
    .locals 3

    .prologue
    .line 2005427
    sget-object v0, LX/DVa;->a:LX/DVa;

    if-nez v0, :cond_1

    .line 2005428
    const-class v1, LX/DVa;

    monitor-enter v1

    .line 2005429
    :try_start_0
    sget-object v0, LX/DVa;->a:LX/DVa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2005430
    if-eqz v2, :cond_0

    .line 2005431
    :try_start_1
    new-instance v0, LX/DVa;

    invoke-direct {v0}, LX/DVa;-><init>()V

    .line 2005432
    move-object v0, v0

    .line 2005433
    sput-object v0, LX/DVa;->a:LX/DVa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2005434
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2005435
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2005436
    :cond_1
    sget-object v0, LX/DVa;->a:LX/DVa;

    return-object v0

    .line 2005437
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2005438
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
