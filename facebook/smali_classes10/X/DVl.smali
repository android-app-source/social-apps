.class public final LX/DVl;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final e:Landroid/content/res/Resources;

.field private final f:Z

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Ck;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZLX/0tX;LX/B1b;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2005762
    invoke-direct {p0, p1, p6, p7}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;)V

    .line 2005763
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005764
    iput-object v0, p0, LX/DVl;->g:LX/0Px;

    .line 2005765
    iput-object p2, p0, LX/DVl;->e:Landroid/content/res/Resources;

    .line 2005766
    iput-object p3, p0, LX/DVl;->h:Ljava/lang/String;

    .line 2005767
    iput-object p4, p0, LX/DVl;->i:Ljava/lang/String;

    .line 2005768
    iput-boolean p5, p0, LX/DVl;->f:Z

    .line 2005769
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005718
    new-instance v0, LX/DWh;

    invoke-direct {v0}, LX/DWh;-><init>()V

    move-object v0, v0

    .line 2005719
    const-string v1, "group_id"

    iget-object v2, p0, LX/DVl;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005720
    const-string v1, "search_term"

    iget-object v2, p0, LX/DVl;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005721
    const-string v1, "member_count_to_fetch"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2005722
    const-string v1, "afterCursor"

    iget-object v2, p0, LX/B1d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005723
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2005724
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2005725
    iget-object v0, p0, LX/DVl;->g:LX/0Px;

    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2005726
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005727
    if-eqz v0, :cond_4

    .line 2005728
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005729
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2005730
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005731
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    move-result-object v6

    .line 2005732
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_0

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel$EdgesModel;

    .line 2005733
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel$EdgesModel;->a()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0, v3}, LX/DVf;->a(LX/DWH;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005734
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2005735
    :cond_0
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2005736
    :goto_1
    iget-boolean v6, p0, LX/DVl;->f:Z

    if-eqz v6, :cond_1

    sget-object v6, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    iget-object v7, p0, LX/DVl;->i:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2005737
    iget-object v6, p0, LX/DVl;->i:Ljava/lang/String;

    .line 2005738
    new-instance v7, LX/0XI;

    invoke-direct {v7}, LX/0XI;-><init>()V

    sget-object v8, LX/0XG;->EMAIL:LX/0XG;

    invoke-virtual {v7, v8, v6}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v7

    new-instance v8, Landroid/net/Uri$Builder;

    invoke-direct {v8}, Landroid/net/Uri$Builder;-><init>()V

    const-string p1, "res"

    invoke-virtual {v8, p1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const p1, 0x7f020dca

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, p1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2005739
    iput-object v8, v7, LX/0XI;->n:Ljava/lang/String;

    .line 2005740
    move-object v7, v7

    .line 2005741
    new-instance v8, Lcom/facebook/user/model/UserEmailAddress;

    invoke-direct {v8, v6}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    .line 2005742
    iput-object v8, v7, LX/0XI;->c:Ljava/util/List;

    .line 2005743
    move-object v7, v7

    .line 2005744
    iput-object v6, v7, LX/0XI;->h:Ljava/lang/String;

    .line 2005745
    move-object v7, v7

    .line 2005746
    iget-object v8, p0, LX/DVl;->e:Landroid/content/res/Resources;

    const p1, 0x7f083089

    invoke-virtual {v8, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2005747
    iput-object v8, v7, LX/0XI;->x:Ljava/lang/String;

    .line 2005748
    move-object v7, v7

    .line 2005749
    invoke-virtual {v7}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v7

    move-object v6, v7

    .line 2005750
    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005751
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iput-object v5, p0, LX/DVl;->g:LX/0Px;

    .line 2005752
    if-eqz v0, :cond_2

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iput-object v3, p0, LX/DVl;->a:Ljava/lang/String;

    .line 2005753
    if-eqz v0, :cond_3

    invoke-virtual {v2, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_2
    iput-boolean v1, p0, LX/DVl;->b:Z

    .line 2005754
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 2005755
    return-void

    :cond_3
    move v1, v4

    .line 2005756
    goto :goto_2

    :cond_4
    move v0, v1

    move-object v2, v3

    goto/16 :goto_1
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2005757
    const-string v0, "Group suggested search member fetch failed"

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005758
    iget-object v0, p0, LX/DVl;->g:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 2005759
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005760
    iput-object v0, p0, LX/DVl;->g:LX/0Px;

    .line 2005761
    return-void
.end method
