.class public LX/DkQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2034250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2034251
    iput-object p1, p0, LX/DkQ;->a:Landroid/os/Bundle;

    .line 2034252
    return-void
.end method

.method public static a(Landroid/os/Bundle;)LX/DkQ;
    .locals 1

    .prologue
    .line 2034249
    new-instance v0, LX/DkQ;

    invoke-direct {v0, p0}, LX/DkQ;-><init>(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/DkQ;
    .locals 3

    .prologue
    .line 2034243
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2034244
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2034245
    const-string v1, "arg_appointments_query_scenario"

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENT_REQUESTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2034246
    const-string v1, "arg_appointments_query_param_user_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034247
    new-instance v1, LX/DkQ;

    invoke-direct {v1, v0}, LX/DkQ;-><init>(Landroid/os/Bundle;)V

    return-object v1

    .line 2034248
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)LX/DkQ;
    .locals 3

    .prologue
    .line 2034237
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2034238
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2034239
    const-string v1, "arg_appointments_query_scenario"

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2034240
    const-string v1, "arg_appointments_query_param_user_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034241
    new-instance v1, LX/DkQ;

    invoke-direct {v1, v0}, LX/DkQ;-><init>(Landroid/os/Bundle;)V

    return-object v1

    .line 2034242
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)LX/DkQ;
    .locals 3

    .prologue
    .line 2034231
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2034232
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2034233
    const-string v1, "arg_appointments_query_scenario"

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS_WITH_A_PAGE:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2034234
    const-string v1, "arg_appointments_query_param_page_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034235
    new-instance v1, LX/DkQ;

    invoke-direct {v1, v0}, LX/DkQ;-><init>(Landroid/os/Bundle;)V

    return-object v1

    .line 2034236
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)LX/DkQ;
    .locals 3

    .prologue
    .line 2034225
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2034226
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2034227
    const-string v1, "arg_appointments_query_scenario"

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->QUERY_AN_APPOINTMENT_DETAILS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2034228
    const-string v1, "arg_an_appointment_details_query_param_appointment_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034229
    new-instance v1, LX/DkQ;

    invoke-direct {v1, v0}, LX/DkQ;-><init>(Landroid/os/Bundle;)V

    return-object v1

    .line 2034230
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;
    .locals 2

    .prologue
    .line 2034224
    iget-object v0, p0, LX/DkQ;->a:Landroid/os/Bundle;

    const-string v1, "arg_appointments_query_scenario"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2034197
    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v0

    .line 2034198
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENT_REQUESTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-eq v0, v1, :cond_0

    .line 2034199
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong query scenario "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2034200
    :cond_0
    iget-object v0, p0, LX/DkQ;->a:Landroid/os/Bundle;

    const-string v1, "arg_appointments_query_param_user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2034220
    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v0

    .line 2034221
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS_WITH_A_PAGE:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_PAST_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_UPCOMING_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-eq v0, v1, :cond_0

    .line 2034222
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong query scenario "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2034223
    :cond_0
    iget-object v0, p0, LX/DkQ;->a:Landroid/os/Bundle;

    const-string v1, "arg_appointments_query_param_page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2034216
    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v0

    .line 2034217
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->QUERY_AN_APPOINTMENT_DETAILS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-eq v0, v1, :cond_0

    .line 2034218
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong query scenario "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2034219
    :cond_0
    iget-object v0, p0, LX/DkQ;->a:Landroid/os/Bundle;

    const-string v1, "arg_an_appointment_details_query_param_appointment_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2034201
    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2034202
    :try_start_0
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENT_REQUESTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2034203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; userid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/DkQ;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2034204
    :cond_0
    :goto_0
    return-object v0

    .line 2034205
    :cond_1
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2034206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; userid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/DkQ;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2034207
    :cond_2
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_UPCOMING_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_PAST_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2034208
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; pageid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/DkQ;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2034209
    :cond_4
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2034210
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS_WITH_A_PAGE:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2034211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; pageid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/DkQ;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2034212
    :cond_5
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->QUERY_AN_APPOINTMENT_DETAILS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    invoke-virtual {p0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2034213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; appointmentid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, LX/DkQ;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 2034214
    :catch_0
    move-exception v1

    .line 2034215
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
