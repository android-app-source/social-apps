.class public abstract LX/Cwh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ReturnType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1950951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/EntityTypeaheadUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/search/model/NullStateModuleSuggestionUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/SeeMoreResultPageUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/search/model/SeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/SeeMoreTypeaheadUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/ShortcutTypeaheadUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/search/model/TrendingTypeaheadUnit;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/TrendingTypeaheadUnit;",
            ")TReturnType;"
        }
    .end annotation
.end method
