.class public final LX/CuH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ctn;


# instance fields
.field public final synthetic a:LX/CuI;

.field private final b:I

.field private c:F

.field private d:F

.field private e:LX/CuF;


# direct methods
.method public constructor <init>(LX/CuI;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1946288
    iput-object p1, p0, LX/CuH;->a:LX/CuI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1946289
    sget-object v0, LX/CuF;->WAITING_FOR_DOWN:LX/CuF;

    iput-object v0, p0, LX/CuH;->e:LX/CuF;

    .line 1946290
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1284

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/CuH;->b:I

    .line 1946291
    return-void
.end method

.method private a(F)V
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v0, -0x40800000    # -1.0f

    .line 1946276
    iget-object v2, p0, LX/CuH;->a:LX/CuI;

    invoke-virtual {v2}, LX/Cts;->h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v2

    .line 1946277
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    .line 1946278
    iget-object v3, p0, LX/CuH;->a:LX/CuI;

    invoke-virtual {v3}, LX/Cts;->i()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1946279
    sget v4, LX/CoL;->l:F

    int-to-float v5, v3

    mul-float/2addr v4, v5

    int-to-float v2, v2

    div-float v2, v4, v2

    .line 1946280
    iget v4, p0, LX/CuH;->c:F

    sub-float v4, p1, v4

    mul-float/2addr v2, v4

    .line 1946281
    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1946282
    neg-float v2, v2

    iget v3, p0, LX/CuH;->d:F

    add-float/2addr v2, v3

    .line 1946283
    cmpg-float v3, v2, v0

    if-gez v3, :cond_0

    .line 1946284
    :goto_0
    iget-object v1, p0, LX/CuH;->a:LX/CuI;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/CuI;->a(Ljava/lang/Float;)V

    .line 1946285
    return-void

    .line 1946286
    :cond_0
    cmpl-float v0, v2, v1

    if-lez v0, :cond_1

    move v0, v1

    .line 1946287
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1946255
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LX/CuH;->e:LX/CuF;

    sget-object v3, LX/CuF;->WAITING_FOR_DOWN:LX/CuF;

    if-ne v2, v3, :cond_1

    .line 1946256
    sget-object v1, LX/CuF;->WAITING_FOR_MOVES:LX/CuF;

    iput-object v1, p0, LX/CuH;->e:LX/CuF;

    .line 1946257
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, LX/CuH;->c:F

    .line 1946258
    iget-object v1, p0, LX/CuH;->a:LX/CuI;

    iget v1, v1, LX/CuI;->k:F

    iput v1, p0, LX/CuH;->d:F

    .line 1946259
    :cond_0
    :goto_0
    return v0

    .line 1946260
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v4, :cond_2

    iget-object v2, p0, LX/CuH;->e:LX/CuF;

    sget-object v3, LX/CuF;->WAITING_FOR_MOVES:LX/CuF;

    if-ne v2, v3, :cond_2

    .line 1946261
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, LX/CuH;->c:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 1946262
    iget v3, p0, LX/CuH;->b:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 1946263
    sget-object v0, LX/CuF;->ACCEPTING_MOVE_EVENTS:LX/CuF;

    iput-object v0, p0, LX/CuH;->e:LX/CuF;

    .line 1946264
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-direct {p0, v0}, LX/CuH;->a(F)V

    move v0, v1

    .line 1946265
    goto :goto_0

    .line 1946266
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v4, :cond_3

    iget-object v2, p0, LX/CuH;->e:LX/CuF;

    sget-object v3, LX/CuF;->ACCEPTING_MOVE_EVENTS:LX/CuF;

    if-ne v2, v3, :cond_3

    move v0, v1

    .line 1946267
    goto :goto_0

    .line 1946268
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 1946269
    sget-object v1, LX/CuF;->WAITING_FOR_DOWN:LX/CuF;

    iput-object v1, p0, LX/CuH;->e:LX/CuF;

    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1946270
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, LX/CuH;->e:LX/CuF;

    sget-object v2, LX/CuF;->ACCEPTING_MOVE_EVENTS:LX/CuF;

    if-ne v1, v2, :cond_0

    .line 1946271
    sget-object v1, LX/CuF;->WAITING_FOR_DOWN:LX/CuF;

    iput-object v1, p0, LX/CuH;->e:LX/CuF;

    .line 1946272
    :goto_0
    return v0

    .line 1946273
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, LX/CuH;->e:LX/CuF;

    sget-object v2, LX/CuF;->ACCEPTING_MOVE_EVENTS:LX/CuF;

    if-ne v1, v2, :cond_1

    .line 1946274
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-direct {p0, v1}, LX/CuH;->a(F)V

    goto :goto_0

    .line 1946275
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
