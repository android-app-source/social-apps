.class public final LX/D2d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/3rL",
        "<",
        "Lcom/facebook/photos/upload/operation/UploadOperation;",
        "LX/D2S;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7TD;

.field public final synthetic b:LX/D2e;


# direct methods
.method public constructor <init>(LX/D2e;LX/7TD;)V
    .locals 0

    .prologue
    .line 1958895
    iput-object p1, p0, LX/D2d;->b:LX/D2e;

    iput-object p2, p0, LX/D2d;->a:LX/7TD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()LX/3rL;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/3rL",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadOperation;",
            "LX/D2S;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1958896
    const/4 v1, 0x0

    .line 1958897
    iget-object v0, p0, LX/D2d;->b:LX/D2e;

    iget-object v0, v0, LX/D2e;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1958898
    iget-object v3, p0, LX/D2d;->a:LX/7TD;

    if-eqz v3, :cond_4

    .line 1958899
    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->o()LX/D2G;

    move-result-object v0

    iget-object v1, p0, LX/D2d;->a:LX/7TD;

    iget-object v1, v1, LX/7TD;->a:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1958900
    iput-object v1, v0, LX/D2G;->a:Landroid/net/Uri;

    .line 1958901
    move-object v0, v0

    .line 1958902
    invoke-virtual {v0}, LX/D2G;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v0

    move-object v13, v0

    move v1, v2

    .line 1958903
    :goto_0
    invoke-virtual {v13}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1958904
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1958905
    iget v0, v13, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->d:I

    move v0, v0

    .line 1958906
    if-eq v0, v2, :cond_0

    .line 1958907
    iget v0, v13, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->d:I

    move v0, v0

    .line 1958908
    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 1958909
    :cond_0
    iget-object v0, v13, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 1958910
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1958911
    const-string v6, "camera"

    .line 1958912
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "."

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1958913
    new-instance v3, Ljava/io/File;

    sget-object v5, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v5}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    const-string v7, "Facebook"

    invoke-direct {v3, v5, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1958914
    invoke-static {v3}, LX/04M;->a(Ljava/io/File;)V

    .line 1958915
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v7, "yyyyMMdd_HHmmss"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 1958916
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1958917
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VID_"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v8, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1958918
    invoke-static {v7, v8}, LX/1t3;->a(Ljava/io/File;Ljava/io/File;)V

    .line 1958919
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1958920
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1958921
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1958922
    iget-object v5, p0, LX/D2d;->b:LX/D2e;

    iget-object v5, v5, LX/D2e;->d:LX/D2h;

    iget-object v5, v5, LX/D2h;->a:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v14, v2

    .line 1958923
    :goto_1
    new-instance v2, LX/74m;

    invoke-direct {v2}, LX/74m;-><init>()V

    invoke-virtual {v2, v0}, LX/74m;->c(Ljava/lang/String;)LX/74m;

    move-result-object v0

    .line 1958924
    if-eqz v4, :cond_1

    .line 1958925
    invoke-virtual {v0, v4}, LX/74m;->d(Ljava/lang/String;)LX/74m;

    .line 1958926
    :cond_1
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v3

    .line 1958927
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1958928
    iget-object v0, p0, LX/D2d;->b:LX/D2e;

    iget-object v0, v0, LX/D2e;->b:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {v0, v4, v5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1958929
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1958930
    const-string v0, "thumbnail_bitmap"

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 1958931
    const-string v0, "frame_offset"

    .line 1958932
    iget v2, v13, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    move v2, v2

    .line 1958933
    int-to-long v8, v2

    invoke-virtual {v4, v0, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1958934
    const/4 v0, 0x0

    .line 1958935
    if-nez v1, :cond_2

    .line 1958936
    iget-object v0, v13, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v0

    .line 1958937
    const-string v1, "video_creative_editing_metadata"

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1958938
    :cond_2
    iget-object v1, p0, LX/D2d;->b:LX/D2e;

    iget-object v1, v1, LX/D2e;->d:LX/D2h;

    iget-object v1, v1, LX/D2h;->b:LX/8LV;

    iget-object v2, p0, LX/D2d;->b:LX/D2e;

    iget-object v2, v2, LX/D2e;->d:LX/D2h;

    iget-object v2, v2, LX/D2h;->d:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v5, p0, LX/D2d;->b:LX/D2e;

    iget-object v5, v5, LX/D2e;->c:Ljava/lang/String;

    .line 1958939
    iget-object v7, v13, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->f:Ljava/lang/String;

    move-object v7, v7

    .line 1958940
    invoke-virtual {v13}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->l()J

    move-result-wide v8

    iget-object v10, p0, LX/D2d;->b:LX/D2e;

    iget-object v10, v10, LX/D2e;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1958941
    iget-object v11, v10, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v10, v11

    .line 1958942
    invoke-virtual {v10}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayId()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, LX/D2d;->b:LX/D2e;

    iget-object v11, v11, LX/D2e;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1958943
    iget-object v12, v11, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v11, v12

    .line 1958944
    invoke-virtual {v11}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getMsqrdMaskId()Ljava/lang/String;

    move-result-object v11

    .line 1958945
    iget-object v12, v13, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->h:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v12, v12

    .line 1958946
    invoke-virtual/range {v1 .. v12}, LX/8LV;->a(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    .line 1958947
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1958948
    iget-object v3, v13, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    move-object v3, v3

    .line 1958949
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1958950
    invoke-static {v2}, LX/BTk;->b(Landroid/media/MediaMetadataRetriever;)F

    move-result v3

    .line 1958951
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1958952
    new-instance v2, LX/D2R;

    invoke-direct {v2}, LX/D2R;-><init>()V

    iget-object v4, p0, LX/D2d;->b:LX/D2e;

    iget-object v4, v4, LX/D2e;->c:Ljava/lang/String;

    .line 1958953
    iput-object v4, v2, LX/D2R;->a:Ljava/lang/String;

    .line 1958954
    move-object v2, v2

    .line 1958955
    const-string v4, "uploading"

    invoke-virtual {v2, v4}, LX/D2R;->b(Ljava/lang/String;)LX/D2R;

    move-result-object v2

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1958956
    iget-object v5, v2, LX/D2R;->b:LX/5w0;

    .line 1958957
    iput-object v4, v5, LX/5w0;->b:Ljava/lang/String;

    .line 1958958
    move-object v2, v2

    .line 1958959
    iget-object v4, p0, LX/D2d;->b:LX/D2e;

    iget-object v4, v4, LX/D2e;->d:LX/D2h;

    iget-object v4, v4, LX/D2h;->h:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/D2R;->a(J)LX/D2R;

    move-result-object v2

    .line 1958960
    iput-object v0, v2, LX/D2R;->d:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1958961
    move-object v0, v2

    .line 1958962
    iput v3, v0, LX/D2R;->e:F

    .line 1958963
    move-object v0, v0

    .line 1958964
    invoke-virtual {v0}, LX/D2R;->a()LX/D2S;

    move-result-object v0

    .line 1958965
    new-instance v2, LX/3rL;

    invoke-direct {v2, v1, v0}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 1958966
    :catch_0
    move-object v0, v2

    move-object v14, v2

    .line 1958967
    goto/16 :goto_1

    .line 1958968
    :cond_3
    iget-object v0, v13, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 1958969
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 1958970
    iget-object v2, p0, LX/D2d;->b:LX/D2e;

    iget-object v2, v2, LX/D2e;->d:LX/D2h;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1958971
    iget-object v5, v2, LX/D2h;->k:LX/1Er;

    const-string v6, "profile-video"

    sget-object v7, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v5, v6, v3, v7}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v5

    .line 1958972
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1958973
    invoke-static {v6, v5}, LX/1t3;->a(Ljava/io/File;Ljava/io/File;)V

    .line 1958974
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    move-object v2, v5

    .line 1958975
    const-string v6, "upload"

    move-object v14, v2

    goto/16 :goto_1

    :cond_4
    move-object v13, v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1958976
    invoke-direct {p0}, LX/D2d;->a()LX/3rL;

    move-result-object v0

    return-object v0
.end method
