.class public LX/CuL;
.super LX/Cts;
.source ""

# interfaces
.implements LX/Ctn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Float;",
        ">;",
        "LX/Ctn;"
    }
.end annotation


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:I

.field public d:Z

.field private e:Z

.field public f:Z

.field public g:LX/Cqt;


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1946524
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1946525
    const/16 v0, 0x1e

    iput v0, p0, LX/CuL;->c:I

    .line 1946526
    iput-boolean v1, p0, LX/CuL;->d:Z

    .line 1946527
    iput-boolean v1, p0, LX/CuL;->e:Z

    .line 1946528
    iput-boolean v1, p0, LX/CuL;->f:Z

    .line 1946529
    sget-object v0, LX/Cqt;->PORTRAIT:LX/Cqt;

    iput-object v0, p0, LX/CuL;->g:LX/Cqt;

    .line 1946530
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/CuL;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p1

    check-cast p1, LX/0hB;

    invoke-static {v0}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v0

    check-cast v0, LX/CqV;

    iput-object p1, p0, LX/CuL;->a:LX/0hB;

    iput-object v0, p0, LX/CuL;->b:LX/CqV;

    .line 1946531
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1946532
    iput-boolean p1, p0, LX/CuL;->d:Z

    .line 1946533
    iget-object v3, p0, LX/CuL;->b:LX/CqV;

    iget-boolean v0, p0, LX/CuL;->d:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sget-object v4, LX/CqU;->SPHERICAL_PHOTO:LX/CqU;

    invoke-virtual {v3, v0, v4}, LX/CqV;->a(ZLX/CqU;)V

    .line 1946534
    iget-object v0, p0, LX/CuL;->b:LX/CqV;

    iget-boolean v3, p0, LX/CuL;->d:Z

    if-nez v3, :cond_1

    :goto_1
    sget-object v2, LX/CqU;->SPHERICAL_PHOTO:LX/CqU;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/CqV;->a(ZLX/CqU;Landroid/view/View;)V

    .line 1946535
    invoke-virtual {p0}, LX/Cts;->h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->requestDisallowInterceptTouchEvent(Z)V

    .line 1946536
    return-void

    :cond_0
    move v0, v2

    .line 1946537
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1946538
    goto :goto_1
.end method

.method public final a(LX/Crd;)Z
    .locals 1

    .prologue
    .line 1946539
    sget-object v0, LX/Crd;->CLICK_MEDIA:LX/Crd;

    if-ne p1, v0, :cond_1

    .line 1946540
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/CuL;->a(Z)V

    .line 1946541
    :cond_0
    :goto_0
    invoke-super {p0, p1}, LX/Cts;->a(LX/Crd;)Z

    move-result v0

    return v0

    .line 1946542
    :cond_1
    sget-object v0, LX/Crd;->BACK:LX/Crd;

    if-ne p1, v0, :cond_0

    .line 1946543
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/CuL;->a(Z)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1946544
    const/high16 v2, 0x41f00000    # 30.0f

    iget-object v3, p0, LX/CuL;->a:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->b()F

    move-result v3

    mul-float/2addr v2, v3

    .line 1946545
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, LX/CuL;->a:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->c()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v4, v2

    cmpl-float v3, v3, v4

    if-gez v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpg-float v3, v3, v2

    if-ltz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v3, v3, v2

    if-ltz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, LX/CuL;->a:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->d()I

    move-result v4

    int-to-float v4, v4

    sub-float v2, v4, v2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_9

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1946546
    if-eqz v2, :cond_2

    .line 1946547
    :cond_1
    :goto_1
    return v1

    .line 1946548
    :cond_2
    iget-boolean v2, p0, LX/CuL;->d:Z

    if-eqz v2, :cond_3

    move v1, v0

    .line 1946549
    goto :goto_1

    .line 1946550
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_4

    .line 1946551
    iput-boolean v0, p0, LX/CuL;->e:Z

    goto :goto_1

    .line 1946552
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_6

    .line 1946553
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v2

    .line 1946554
    if-nez v2, :cond_a

    .line 1946555
    const/4 v2, 0x0

    .line 1946556
    :goto_2
    move v2, v2

    .line 1946557
    if-eqz v2, :cond_6

    .line 1946558
    iget-boolean v2, p0, LX/CuL;->e:Z

    if-eqz v2, :cond_8

    .line 1946559
    iput-boolean v0, p0, LX/CuL;->f:Z

    .line 1946560
    iget-boolean v2, p0, LX/CuL;->f:Z

    if-eqz v2, :cond_5

    iget-boolean v2, p0, LX/CuL;->d:Z

    if-eqz v2, :cond_b

    .line 1946561
    :cond_5
    :goto_3
    iput-boolean v1, p0, LX/CuL;->e:Z

    move v1, v0

    goto :goto_1

    .line 1946562
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    .line 1946563
    :cond_7
    iput-boolean v1, p0, LX/CuL;->e:Z

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    const/4 v2, 0x0

    goto :goto_0

    .line 1946564
    :cond_a
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1946565
    invoke-virtual {v2, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1946566
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    goto :goto_2

    .line 1946567
    :cond_b
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/CuL;->a(Z)V

    .line 1946568
    iget-object v2, p0, LX/Cts;->a:LX/Ctg;

    move-object v2, v2

    .line 1946569
    sget-object v3, LX/Crd;->CLICK_MEDIA:LX/Crd;

    invoke-interface {v2, v3}, LX/Cre;->a(LX/Crd;)Z

    .line 1946570
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/CuL;->f:Z

    goto :goto_3
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 1946571
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v0

    const/4 v4, 0x0

    .line 1946572
    iget-object v1, p0, LX/CuL;->g:LX/Cqt;

    if-nez v1, :cond_1

    .line 1946573
    :cond_0
    :goto_0
    move-object v1, p1

    .line 1946574
    invoke-virtual {v0, v1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1946575
    const/4 v0, 0x1

    return v0

    .line 1946576
    :cond_1
    iget-object v1, p0, LX/CuL;->g:LX/Cqt;

    sget-object v2, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    if-ne v1, v2, :cond_2

    .line 1946577
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 1946578
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1946579
    invoke-virtual {v1, v4, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    move-object p1, v1

    .line 1946580
    goto :goto_0

    .line 1946581
    :cond_2
    iget-object v1, p0, LX/CuL;->g:LX/Cqt;

    sget-object v2, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    if-ne v1, v2, :cond_0

    .line 1946582
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 1946583
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    neg-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1946584
    invoke-virtual {v1, v4, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    move-object p1, v1

    .line 1946585
    goto :goto_0
.end method
