.class public LX/Cv5;
.super LX/16T;
.source ""

# interfaces
.implements LX/16E;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:Z

.field public static b:Landroid/view/View;

.field private static volatile e:LX/Cv5;


# instance fields
.field public final c:LX/0tQ;

.field public d:LX/Cv4;


# direct methods
.method public constructor <init>(LX/0tQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1947697
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1947698
    iput-object p1, p0, LX/Cv5;->c:LX/0tQ;

    .line 1947699
    return-void
.end method

.method public static a(LX/0QB;)LX/Cv5;
    .locals 4

    .prologue
    .line 1947684
    sget-object v0, LX/Cv5;->e:LX/Cv5;

    if-nez v0, :cond_1

    .line 1947685
    const-class v1, LX/Cv5;

    monitor-enter v1

    .line 1947686
    :try_start_0
    sget-object v0, LX/Cv5;->e:LX/Cv5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1947687
    if-eqz v2, :cond_0

    .line 1947688
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1947689
    new-instance p0, LX/Cv5;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v3

    check-cast v3, LX/0tQ;

    invoke-direct {p0, v3}, LX/Cv5;-><init>(LX/0tQ;)V

    .line 1947690
    move-object v0, p0

    .line 1947691
    sput-object v0, LX/Cv5;->e:LX/Cv5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1947692
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1947693
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1947694
    :cond_1
    sget-object v0, LX/Cv5;->e:LX/Cv5;

    return-object v0

    .line 1947695
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1947696
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1947681
    sget-boolean v0, LX/Cv5;->a:Z

    if-eqz v0, :cond_0

    .line 1947682
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 1947683
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1947654
    if-eqz p2, :cond_0

    instance-of v0, p2, LX/Cv4;

    if-eqz v0, :cond_0

    .line 1947655
    check-cast p2, LX/Cv4;

    iput-object p2, p0, LX/Cv5;->d:LX/Cv4;

    .line 1947656
    :cond_0
    iget-object v0, p0, LX/Cv5;->d:LX/Cv4;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cv5;->d:LX/Cv4;

    .line 1947657
    iget v1, v0, LX/Cv4;->b:I

    move v0, v1

    .line 1947658
    if-ltz v0, :cond_1

    iget-object v0, p0, LX/Cv5;->d:LX/Cv4;

    .line 1947659
    iget v1, v0, LX/Cv4;->b:I

    move v0, v1

    .line 1947660
    iget-object v1, p0, LX/Cv5;->d:LX/Cv4;

    .line 1947661
    iget-object v2, v1, LX/Cv4;->a:LX/0g8;

    move-object v1, v2

    .line 1947662
    invoke-interface {v1}, LX/0g8;->s()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 1947663
    :cond_1
    :goto_0
    return-void

    .line 1947664
    :cond_2
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1947665
    new-instance v0, Lcom/facebook/saved/common/nux/OfflineVideoSavedBookmarkNuxInterstitialController$1;

    invoke-direct {v0, p0}, Lcom/facebook/saved/common/nux/OfflineVideoSavedBookmarkNuxInterstitialController$1;-><init>(LX/Cv5;)V

    const v2, -0x3ac279ef

    invoke-static {v1, v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1947666
    const-class v0, LX/0f0;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f0;

    .line 1947667
    if-eqz v0, :cond_1

    .line 1947668
    const-class v2, LX/0f3;

    invoke-interface {v0, v2}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f3;

    .line 1947669
    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/0f3;->n()Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    sget-object v2, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    if-ne v0, v2, :cond_1

    .line 1947670
    new-instance v0, LX/0hs;

    const/4 v2, 0x2

    invoke-direct {v0, p1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1947671
    const/4 v2, -0x1

    .line 1947672
    iput v2, v0, LX/0hs;->t:I

    .line 1947673
    sget-object v2, LX/Cv3;->a:[I

    iget-object v3, p0, LX/Cv5;->c:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->m()LX/2qY;

    move-result-object v3

    invoke-virtual {v3}, LX/2qY;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1947674
    const v2, 0x7f080d39

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v2, v2

    .line 1947675
    invoke-virtual {v0, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1947676
    const v2, 0x7f080d3d

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1947677
    new-instance v2, Lcom/facebook/saved/common/nux/OfflineVideoSavedBookmarkNuxInterstitialController$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/saved/common/nux/OfflineVideoSavedBookmarkNuxInterstitialController$2;-><init>(LX/Cv5;LX/0hs;)V

    const-wide/16 v4, 0x3e8

    const v0, 0x2d8476a0

    invoke-static {v1, v2, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1947678
    const/4 v0, 0x0

    sput-boolean v0, LX/Cv5;->a:Z

    goto :goto_0

    .line 1947679
    :pswitch_0
    const v2, 0x7f080d38

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1947680
    :pswitch_1
    const v2, 0x7f080d3a

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1947652
    const-string v0, "4290"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1947653
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BOOKMARK_TAB_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
