.class public final LX/EYf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/EX1;

.field private final b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method public constructor <init>(LX/EX1;Z)V
    .locals 2

    .prologue
    .line 2137944
    iput-object p1, p0, LX/EYf;->a:LX/EX1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137945
    iget-object v0, p0, LX/EYf;->a:LX/EX1;

    iget-object v0, v0, LX/EX1;->extensions:LX/EYc;

    .line 2137946
    iget-boolean v1, v0, LX/EYc;->c:Z

    if-eqz v1, :cond_1

    .line 2137947
    new-instance v1, LX/EYt;

    iget-object p1, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {p1}, LX/EZ8;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    invoke-direct {v1, p1}, LX/EYt;-><init>(Ljava/util/Iterator;)V

    .line 2137948
    :goto_0
    move-object v0, v1

    .line 2137949
    iput-object v0, p0, LX/EYf;->b:Ljava/util/Iterator;

    .line 2137950
    iget-object v0, p0, LX/EYf;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2137951
    iget-object v0, p0, LX/EYf;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    .line 2137952
    :cond_0
    iput-boolean p2, p0, LX/EYf;->d:Z

    .line 2137953
    return-void

    :cond_1
    iget-object v1, v0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v1}, LX/EZ8;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/EWf;)V
    .locals 3

    .prologue
    .line 2137954
    :goto_0
    iget-object v0, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    invoke-virtual {v0}, LX/EYP;->e()I

    move-result v0

    if-ge v0, p1, :cond_3

    .line 2137955
    iget-object v0, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2137956
    iget-boolean v1, p0, LX/EYf;->d:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/EYP;->g()LX/EZa;

    move-result-object v1

    sget-object v2, LX/EZa;->MESSAGE:LX/EZa;

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2137957
    iget-object v1, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    instance-of v1, v1, LX/EYs;

    if-eqz v1, :cond_0

    .line 2137958
    invoke-virtual {v0}, LX/EYP;->e()I

    move-result v1

    iget-object v0, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    check-cast v0, LX/EYs;

    .line 2137959
    iget-object v2, v0, LX/EYs;->a:Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYu;

    move-object v0, v2

    .line 2137960
    invoke-virtual {v0}, LX/EYu;->c()LX/EWc;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, LX/EWf;->b(ILX/EWc;)V

    .line 2137961
    :goto_1
    iget-object v0, p0, LX/EYf;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2137962
    iget-object v0, p0, LX/EYf;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    goto :goto_0

    .line 2137963
    :cond_0
    invoke-virtual {v0}, LX/EYP;->e()I

    move-result v1

    iget-object v0, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    invoke-virtual {p2, v1, v0}, LX/EWf;->c(ILX/EWW;)V

    goto :goto_1

    .line 2137964
    :cond_1
    iget-object v1, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1, p2}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;LX/EWf;)V

    goto :goto_1

    .line 2137965
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/EYf;->c:Ljava/util/Map$Entry;

    goto :goto_0

    .line 2137966
    :cond_3
    return-void
.end method
