.class public final LX/DCN;
.super LX/1NN;
.source ""


# instance fields
.field public final synthetic a:LX/DCO;


# direct methods
.method public constructor <init>(LX/DCO;)V
    .locals 0

    .prologue
    .line 1973949
    iput-object p1, p0, LX/DCN;->a:LX/DCO;

    invoke-direct {p0}, LX/1NN;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/HideableUnit;LX/1Nd;)V
    .locals 5

    .prologue
    .line 1973950
    invoke-static {p1}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v0

    iget-object v1, p0, LX/DCN;->a:LX/DCO;

    iget-object v1, v1, LX/DCO;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v1, p2, LX/1Nd;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    iget v4, p2, LX/1Nd;->e:I

    invoke-virtual {v0, v2, v3, v1, v4}, LX/6X8;->a(JLcom/facebook/graphql/enums/StoryVisibility;I)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    .line 1973951
    iget-object v1, p0, LX/DCN;->a:LX/DCO;

    iget-object v1, v1, LX/DCO;->e:LX/0qq;

    invoke-virtual {v1, v0}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1973952
    iget-object v0, p0, LX/DCN;->a:LX/DCO;

    iget-object v0, v0, LX/DCO;->f:LX/0g4;

    invoke-interface {v0}, LX/0g4;->d()V

    .line 1973953
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1973954
    check-cast p1, LX/1Nd;

    .line 1973955
    iget-object v0, p1, LX/1Nd;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1973956
    iget-object v0, p1, LX/1Nd;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1973957
    iget-object v0, p0, LX/DCN;->a:LX/DCO;

    iget-object v0, v0, LX/DCO;->e:LX/0qq;

    iget-object v1, p1, LX/1Nd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qq;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1973958
    :goto_0
    if-eqz v0, :cond_0

    .line 1973959
    invoke-direct {p0, v0, p1}, LX/DCN;->a(Lcom/facebook/graphql/model/HideableUnit;LX/1Nd;)V

    .line 1973960
    :cond_0
    return-void

    .line 1973961
    :cond_1
    iget-object v0, p0, LX/DCN;->a:LX/DCO;

    iget-object v0, v0, LX/DCO;->e:LX/0qq;

    iget-object v1, p1, LX/1Nd;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qq;->c(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 1973962
    :cond_2
    iget-object v0, p0, LX/DCN;->a:LX/DCO;

    iget-object v0, v0, LX/DCO;->e:LX/0qq;

    iget-object v1, p1, LX/1Nd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0qq;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1973963
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1973964
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1973965
    instance-of v2, v0, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v2, :cond_3

    .line 1973966
    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    invoke-direct {p0, v0, p1}, LX/DCN;->a(Lcom/facebook/graphql/model/HideableUnit;LX/1Nd;)V

    goto :goto_1
.end method
