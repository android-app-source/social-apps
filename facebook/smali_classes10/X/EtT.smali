.class public LX/EtT;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EtU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2178083
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EtT;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EtU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2178080
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2178081
    iput-object p1, p0, LX/EtT;->b:LX/0Ot;

    .line 2178082
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2178077
    check-cast p2, LX/EtS;

    .line 2178078
    iget-object v0, p0, LX/EtT;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EtU;

    iget v2, p2, LX/EtS;->a:I

    iget-object v3, p2, LX/EtS;->b:Ljava/lang/CharSequence;

    iget v4, p2, LX/EtS;->c:I

    iget-boolean v5, p2, LX/EtS;->d:Z

    iget-object v6, p2, LX/EtS;->e:LX/1dQ;

    iget-object v7, p2, LX/EtS;->f:LX/1dQ;

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, LX/EtU;->a(LX/1De;ILjava/lang/CharSequence;IZLX/1dQ;LX/1dQ;)LX/1Dg;

    move-result-object v0

    .line 2178079
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2178075
    invoke-static {}, LX/1dS;->b()V

    .line 2178076
    const/4 v0, 0x0

    return-object v0
.end method
