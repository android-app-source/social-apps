.class public final enum LX/DfY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DfY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DfY;

.field public static final enum V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL:LX/DfY;

.field public static final enum V2_ACTIVE_NOW_TICKER:LX/DfY;

.field public static final enum V2_ANNOUNCEMENT:LX/DfY;

.field public static final enum V2_BYMM:LX/DfY;

.field public static final enum V2_BYMM_VERTICAL:LX/DfY;

.field public static final enum V2_CONTACTS_YOU_MAY_KNOW:LX/DfY;

.field public static final enum V2_CONTACTS_YOU_MAY_KNOW_ITEM:LX/DfY;

.field public static final enum V2_CONVERSATION_STARTER:LX/DfY;

.field public static final enum V2_DIRECT_M:LX/DfY;

.field public static final enum V2_GAME_SUGGESTION:LX/DfY;

.field public static final enum V2_HIDDEN_UNIT:LX/DfY;

.field public static final enum V2_HORIZONTAL_TILES_UNIT_ITEM:LX/DfY;

.field public static final enum V2_HORIZONTAL_TILE_ITEM:LX/DfY;

.field public static final enum V2_INVITE_FB_FRIENDS:LX/DfY;

.field public static final enum V2_INVITE_FB_FRIENDS_ITEM:LX/DfY;

.field public static final enum V2_LOAD_MORE_THREADS_PLACEHOLDER:LX/DfY;

.field public static final enum V2_MESSAGE_REQUEST_HEADER:LX/DfY;

.field public static final enum V2_MESSAGE_REQUEST_THREADS:LX/DfY;

.field public static final enum V2_MESSENGER_ADS_CLASSIC_UNIT:LX/DfY;

.field public static final enum V2_MESSENGER_ADS_HSCROLL_UNIT:LX/DfY;

.field public static final enum V2_MESSENGER_ADS_SINGLE_IMAGE:LX/DfY;

.field public static final enum V2_MONTAGE_COMPOSER_HEADER:LX/DfY;

.field public static final enum V2_MONTAGE_COMPOSER_HEADER_ITEM:LX/DfY;

.field public static final enum V2_MONTAGE_NUX_ITEM:LX/DfY;

.field public static final enum V2_MORE_CONVERSATIONS_FOOTER:LX/DfY;

.field public static final enum V2_MORE_FOOTER:LX/DfY;

.field public static final enum V2_RANKED_USER:LX/DfY;

.field public static final enum V2_RECENT_THREADS_PLACEHOLDER:LX/DfY;

.field public static final enum V2_ROOM_SUGGESTION:LX/DfY;

.field public static final enum V2_ROOM_SUGGESTION_CREATE_ROOM:LX/DfY;

.field public static final enum V2_ROOM_SUGGESTION_ITEM:LX/DfY;

.field public static final enum V2_ROOM_SUGGESTION_SEE_MORE:LX/DfY;

.field public static final enum V2_RTC_RECOMMENDATION:LX/DfY;

.field public static final enum V2_SECTION_HEADER:LX/DfY;

.field public static final enum V2_SEE_ALL_FOOTER:LX/DfY;

.field public static final enum V2_SUBSCRIPTION_CONTENT:LX/DfY;

.field public static final enum V2_SUBSCRIPTION_NUX:LX/DfY;

.field public static final enum V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER:LX/DfY;

.field public static final enum V2_THREAD:LX/DfY;

.field public static final enum V2_TRENDING_GIFS:LX/DfY;

.field public static final enum V2_TRENDING_GIF_ITEM:LX/DfY;

.field public static final enum V2_UNKNOWN_TYPE:LX/DfY;

.field public static final enum V2_USER_WITH_STATUS:LX/DfY;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2027425
    new-instance v0, LX/DfY;

    const-string v1, "V2_RECENT_THREADS_PLACEHOLDER"

    invoke-direct {v0, v1, v3}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_RECENT_THREADS_PLACEHOLDER:LX/DfY;

    .line 2027426
    new-instance v0, LX/DfY;

    const-string v1, "V2_THREAD"

    invoke-direct {v0, v1, v4}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_THREAD:LX/DfY;

    .line 2027427
    new-instance v0, LX/DfY;

    const-string v1, "V2_CONVERSATION_STARTER"

    invoke-direct {v0, v1, v5}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_CONVERSATION_STARTER:LX/DfY;

    .line 2027428
    new-instance v0, LX/DfY;

    const-string v1, "V2_SECTION_HEADER"

    invoke-direct {v0, v1, v6}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_SECTION_HEADER:LX/DfY;

    .line 2027429
    new-instance v0, LX/DfY;

    const-string v1, "V2_MESSAGE_REQUEST_HEADER"

    invoke-direct {v0, v1, v7}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MESSAGE_REQUEST_HEADER:LX/DfY;

    .line 2027430
    new-instance v0, LX/DfY;

    const-string v1, "V2_USER_WITH_STATUS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_USER_WITH_STATUS:LX/DfY;

    .line 2027431
    new-instance v0, LX/DfY;

    const-string v1, "V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL:LX/DfY;

    .line 2027432
    new-instance v0, LX/DfY;

    const-string v1, "V2_ACTIVE_NOW_TICKER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_ACTIVE_NOW_TICKER:LX/DfY;

    .line 2027433
    new-instance v0, LX/DfY;

    const-string v1, "V2_RTC_RECOMMENDATION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_RTC_RECOMMENDATION:LX/DfY;

    .line 2027434
    new-instance v0, LX/DfY;

    const-string v1, "V2_TRENDING_GIFS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_TRENDING_GIFS:LX/DfY;

    .line 2027435
    new-instance v0, LX/DfY;

    const-string v1, "V2_TRENDING_GIF_ITEM"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_TRENDING_GIF_ITEM:LX/DfY;

    .line 2027436
    new-instance v0, LX/DfY;

    const-string v1, "V2_LOAD_MORE_THREADS_PLACEHOLDER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_LOAD_MORE_THREADS_PLACEHOLDER:LX/DfY;

    .line 2027437
    new-instance v0, LX/DfY;

    const-string v1, "V2_MONTAGE_COMPOSER_HEADER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MONTAGE_COMPOSER_HEADER:LX/DfY;

    .line 2027438
    new-instance v0, LX/DfY;

    const-string v1, "V2_MONTAGE_COMPOSER_HEADER_ITEM"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MONTAGE_COMPOSER_HEADER_ITEM:LX/DfY;

    .line 2027439
    new-instance v0, LX/DfY;

    const-string v1, "V2_MONTAGE_NUX_ITEM"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MONTAGE_NUX_ITEM:LX/DfY;

    .line 2027440
    new-instance v0, LX/DfY;

    const-string v1, "V2_BYMM"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_BYMM:LX/DfY;

    .line 2027441
    new-instance v0, LX/DfY;

    const-string v1, "V2_BYMM_VERTICAL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_BYMM_VERTICAL:LX/DfY;

    .line 2027442
    new-instance v0, LX/DfY;

    const-string v1, "V2_MORE_FOOTER"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MORE_FOOTER:LX/DfY;

    .line 2027443
    new-instance v0, LX/DfY;

    const-string v1, "V2_SEE_ALL_FOOTER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_SEE_ALL_FOOTER:LX/DfY;

    .line 2027444
    new-instance v0, LX/DfY;

    const-string v1, "V2_MORE_CONVERSATIONS_FOOTER"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MORE_CONVERSATIONS_FOOTER:LX/DfY;

    .line 2027445
    new-instance v0, LX/DfY;

    const-string v1, "V2_MESSAGE_REQUEST_THREADS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MESSAGE_REQUEST_THREADS:LX/DfY;

    .line 2027446
    new-instance v0, LX/DfY;

    const-string v1, "V2_ANNOUNCEMENT"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_ANNOUNCEMENT:LX/DfY;

    .line 2027447
    new-instance v0, LX/DfY;

    const-string v1, "V2_CONTACTS_YOU_MAY_KNOW"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_CONTACTS_YOU_MAY_KNOW:LX/DfY;

    .line 2027448
    new-instance v0, LX/DfY;

    const-string v1, "V2_CONTACTS_YOU_MAY_KNOW_ITEM"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_CONTACTS_YOU_MAY_KNOW_ITEM:LX/DfY;

    .line 2027449
    new-instance v0, LX/DfY;

    const-string v1, "V2_INVITE_FB_FRIENDS"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_INVITE_FB_FRIENDS:LX/DfY;

    .line 2027450
    new-instance v0, LX/DfY;

    const-string v1, "V2_INVITE_FB_FRIENDS_ITEM"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_INVITE_FB_FRIENDS_ITEM:LX/DfY;

    .line 2027451
    new-instance v0, LX/DfY;

    const-string v1, "V2_SUBSCRIPTION_CONTENT"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_SUBSCRIPTION_CONTENT:LX/DfY;

    .line 2027452
    new-instance v0, LX/DfY;

    const-string v1, "V2_SUBSCRIPTION_NUX"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_SUBSCRIPTION_NUX:LX/DfY;

    .line 2027453
    new-instance v0, LX/DfY;

    const-string v1, "V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER:LX/DfY;

    .line 2027454
    new-instance v0, LX/DfY;

    const-string v1, "V2_HORIZONTAL_TILE_ITEM"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_HORIZONTAL_TILE_ITEM:LX/DfY;

    .line 2027455
    new-instance v0, LX/DfY;

    const-string v1, "V2_HORIZONTAL_TILES_UNIT_ITEM"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_HORIZONTAL_TILES_UNIT_ITEM:LX/DfY;

    .line 2027456
    new-instance v0, LX/DfY;

    const-string v1, "V2_RANKED_USER"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_RANKED_USER:LX/DfY;

    .line 2027457
    new-instance v0, LX/DfY;

    const-string v1, "V2_ROOM_SUGGESTION"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_ROOM_SUGGESTION:LX/DfY;

    .line 2027458
    new-instance v0, LX/DfY;

    const-string v1, "V2_ROOM_SUGGESTION_ITEM"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_ROOM_SUGGESTION_ITEM:LX/DfY;

    .line 2027459
    new-instance v0, LX/DfY;

    const-string v1, "V2_ROOM_SUGGESTION_CREATE_ROOM"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_ROOM_SUGGESTION_CREATE_ROOM:LX/DfY;

    .line 2027460
    new-instance v0, LX/DfY;

    const-string v1, "V2_ROOM_SUGGESTION_SEE_MORE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_ROOM_SUGGESTION_SEE_MORE:LX/DfY;

    .line 2027461
    new-instance v0, LX/DfY;

    const-string v1, "V2_HIDDEN_UNIT"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_HIDDEN_UNIT:LX/DfY;

    .line 2027462
    new-instance v0, LX/DfY;

    const-string v1, "V2_GAME_SUGGESTION"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_GAME_SUGGESTION:LX/DfY;

    .line 2027463
    new-instance v0, LX/DfY;

    const-string v1, "V2_MESSENGER_ADS_CLASSIC_UNIT"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MESSENGER_ADS_CLASSIC_UNIT:LX/DfY;

    .line 2027464
    new-instance v0, LX/DfY;

    const-string v1, "V2_MESSENGER_ADS_HSCROLL_UNIT"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MESSENGER_ADS_HSCROLL_UNIT:LX/DfY;

    .line 2027465
    new-instance v0, LX/DfY;

    const-string v1, "V2_MESSENGER_ADS_SINGLE_IMAGE"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_MESSENGER_ADS_SINGLE_IMAGE:LX/DfY;

    .line 2027466
    new-instance v0, LX/DfY;

    const-string v1, "V2_DIRECT_M"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_DIRECT_M:LX/DfY;

    .line 2027467
    new-instance v0, LX/DfY;

    const-string v1, "V2_UNKNOWN_TYPE"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, LX/DfY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DfY;->V2_UNKNOWN_TYPE:LX/DfY;

    .line 2027468
    const/16 v0, 0x2b

    new-array v0, v0, [LX/DfY;

    sget-object v1, LX/DfY;->V2_RECENT_THREADS_PLACEHOLDER:LX/DfY;

    aput-object v1, v0, v3

    sget-object v1, LX/DfY;->V2_THREAD:LX/DfY;

    aput-object v1, v0, v4

    sget-object v1, LX/DfY;->V2_CONVERSATION_STARTER:LX/DfY;

    aput-object v1, v0, v5

    sget-object v1, LX/DfY;->V2_SECTION_HEADER:LX/DfY;

    aput-object v1, v0, v6

    sget-object v1, LX/DfY;->V2_MESSAGE_REQUEST_HEADER:LX/DfY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DfY;->V2_USER_WITH_STATUS:LX/DfY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DfY;->V2_ACTIVE_NOW_PRESENCE_DISABLED_UPSELL:LX/DfY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DfY;->V2_ACTIVE_NOW_TICKER:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DfY;->V2_RTC_RECOMMENDATION:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/DfY;->V2_TRENDING_GIFS:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/DfY;->V2_TRENDING_GIF_ITEM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/DfY;->V2_LOAD_MORE_THREADS_PLACEHOLDER:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/DfY;->V2_MONTAGE_COMPOSER_HEADER:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/DfY;->V2_MONTAGE_COMPOSER_HEADER_ITEM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/DfY;->V2_MONTAGE_NUX_ITEM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/DfY;->V2_BYMM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/DfY;->V2_BYMM_VERTICAL:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/DfY;->V2_MORE_FOOTER:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/DfY;->V2_SEE_ALL_FOOTER:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/DfY;->V2_MORE_CONVERSATIONS_FOOTER:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/DfY;->V2_MESSAGE_REQUEST_THREADS:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/DfY;->V2_ANNOUNCEMENT:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/DfY;->V2_CONTACTS_YOU_MAY_KNOW:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/DfY;->V2_CONTACTS_YOU_MAY_KNOW_ITEM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/DfY;->V2_INVITE_FB_FRIENDS:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/DfY;->V2_INVITE_FB_FRIENDS_ITEM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/DfY;->V2_SUBSCRIPTION_CONTENT:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/DfY;->V2_SUBSCRIPTION_NUX:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/DfY;->V2_SUGGESTED_SUBSCRIPTIONS_NUX_HEADER:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/DfY;->V2_HORIZONTAL_TILE_ITEM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/DfY;->V2_HORIZONTAL_TILES_UNIT_ITEM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/DfY;->V2_RANKED_USER:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/DfY;->V2_ROOM_SUGGESTION:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/DfY;->V2_ROOM_SUGGESTION_ITEM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/DfY;->V2_ROOM_SUGGESTION_CREATE_ROOM:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/DfY;->V2_ROOM_SUGGESTION_SEE_MORE:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/DfY;->V2_HIDDEN_UNIT:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/DfY;->V2_GAME_SUGGESTION:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/DfY;->V2_MESSENGER_ADS_CLASSIC_UNIT:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/DfY;->V2_MESSENGER_ADS_HSCROLL_UNIT:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/DfY;->V2_MESSENGER_ADS_SINGLE_IMAGE:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/DfY;->V2_DIRECT_M:LX/DfY;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/DfY;->V2_UNKNOWN_TYPE:LX/DfY;

    aput-object v2, v0, v1

    sput-object v0, LX/DfY;->$VALUES:[LX/DfY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2027469
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DfY;
    .locals 1

    .prologue
    .line 2027470
    const-class v0, LX/DfY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DfY;

    return-object v0
.end method

.method public static values()[LX/DfY;
    .locals 1

    .prologue
    .line 2027471
    sget-object v0, LX/DfY;->$VALUES:[LX/DfY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DfY;

    return-object v0
.end method
