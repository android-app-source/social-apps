.class public final LX/EHj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/views/RtcSpringDragView;

.field private b:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/views/RtcSpringDragView;)V
    .locals 3

    .prologue
    .line 2100188
    iput-object p1, p0, LX/EHj;->a:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2100189
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p1}, Lcom/facebook/rtc/views/RtcSpringDragView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/EHk;

    invoke-direct {v2, p1}, LX/EHk;-><init>(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/EHj;->b:Landroid/view/GestureDetector;

    .line 2100190
    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2100191
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    .line 2100192
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    .line 2100193
    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2100194
    iget-object v2, p0, LX/EHj;->b:Landroid/view/GestureDetector;

    invoke-virtual {v2, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 2100195
    neg-float v0, v0

    neg-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2100196
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    if-nez v2, :cond_1

    .line 2100197
    iget-object v0, p0, LX/EHj;->a:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-static {v0, v3}, Lcom/facebook/rtc/views/RtcSpringDragView;->setNearestCorner(Lcom/facebook/rtc/views/RtcSpringDragView;Z)V

    .line 2100198
    :cond_1
    return v2
.end method
