.class public LX/ELm;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/Cxh;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/CxV;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ELm",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109711
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2109712
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ELm;->b:LX/0Zi;

    .line 2109713
    iput-object p1, p0, LX/ELm;->a:LX/0Ot;

    .line 2109714
    return-void
.end method

.method public static a(LX/0QB;)LX/ELm;
    .locals 4

    .prologue
    .line 2109700
    const-class v1, LX/ELm;

    monitor-enter v1

    .line 2109701
    :try_start_0
    sget-object v0, LX/ELm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109702
    sput-object v2, LX/ELm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109703
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109704
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109705
    new-instance v3, LX/ELm;

    const/16 p0, 0x3403

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ELm;-><init>(LX/0Ot;)V

    .line 2109706
    move-object v0, v3

    .line 2109707
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109708
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ELm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109709
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109710
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2109699
    const v0, 0x403cb335

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2109683
    check-cast p2, LX/ELl;

    .line 2109684
    iget-object v0, p0, LX/ELm;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;

    iget-object v2, p2, LX/ELl;->a:LX/CxA;

    iget-object v3, p2, LX/ELl;->b:LX/CzL;

    iget-object v4, p2, LX/ELl;->c:Ljava/lang/String;

    iget-object v5, p2, LX/ELl;->d:Ljava/lang/String;

    iget-boolean v6, p2, LX/ELl;->e:Z

    iget-object v7, p2, LX/ELl;->f:Ljava/lang/String;

    iget-object v8, p2, LX/ELl;->g:Ljava/lang/CharSequence;

    iget-object v9, p2, LX/ELl;->h:Ljava/lang/CharSequence;

    iget-boolean v10, p2, LX/ELl;->i:Z

    iget-object v11, p2, LX/ELl;->j:Ljava/lang/CharSequence;

    iget-object v12, p2, LX/ELl;->k:Ljava/lang/CharSequence;

    move-object v1, p1

    invoke-virtual/range {v0 .. v12}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->a(LX/1De;LX/CxA;LX/CzL;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)LX/1Dg;

    move-result-object v0

    .line 2109685
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2109686
    invoke-static {}, LX/1dS;->b()V

    .line 2109687
    iget v0, p1, LX/1dQ;->b:I

    .line 2109688
    packed-switch v0, :pswitch_data_0

    .line 2109689
    :goto_0
    return-object v2

    .line 2109690
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2109691
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2109692
    check-cast v1, LX/ELl;

    .line 2109693
    iget-object v3, p0, LX/ELm;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;

    iget-object v4, v1, LX/ELl;->a:LX/CxA;

    iget-object v5, v1, LX/ELl;->b:LX/CzL;

    iget-object p1, v1, LX/ELl;->c:Ljava/lang/String;

    .line 2109694
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2109695
    :goto_1
    goto :goto_0

    .line 2109696
    :cond_0
    iget-object p2, v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->e:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    const p2, 0x25d6af

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, p0, v1

    invoke-static {p2, p0}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2109697
    iget-object p2, v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->f:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p2, v1, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2109698
    check-cast v4, LX/Cxh;

    invoke-interface {v4, v5}, LX/Cxh;->c(LX/CzL;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x403cb335
        :pswitch_0
    .end packed-switch
.end method
