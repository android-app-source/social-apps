.class public LX/DKD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1986781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1986782
    return-void
.end method

.method public static a(LX/5QS;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
    .end annotation

    .prologue
    .line 1986774
    sget-object v0, LX/DKC;->a:[I

    invoke-virtual {p0}, LX/5QS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1986775
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported group list type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1986776
    :pswitch_0
    const-string v0, "community_suggested_groups"

    .line 1986777
    :goto_0
    return-object v0

    .line 1986778
    :pswitch_1
    const-string v0, "community_forum_groups"

    goto :goto_0

    .line 1986779
    :pswitch_2
    const-string v0, "community_all_subgroups"

    goto :goto_0

    .line 1986780
    :pswitch_3
    const-string v0, "community_viewer_subgroups"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)Z
    .locals 2

    .prologue
    .line 1986773
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/5QS;)I
    .locals 2
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1986764
    sget-object v0, LX/DKC;->a:[I

    invoke-virtual {p0}, LX/5QS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1986765
    const v0, 0x7f082ff2

    :goto_0
    return v0

    .line 1986766
    :pswitch_0
    const v0, 0x7f082fe9

    goto :goto_0

    .line 1986767
    :pswitch_1
    const v0, 0x7f082fed

    goto :goto_0

    .line 1986768
    :pswitch_2
    const v0, 0x7f082fef

    goto :goto_0

    .line 1986769
    :pswitch_3
    const v0, 0x7f082fee

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 2

    .prologue
    .line 1986770
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    .line 1986771
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v1, p0}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1986772
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
