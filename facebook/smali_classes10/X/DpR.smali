.class public LX/DpR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final participants_list:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/DpM;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2045423
    new-instance v0, LX/1sv;

    const-string v1, "ParticipantsListPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpR;->b:LX/1sv;

    .line 2045424
    new-instance v0, LX/1sw;

    const-string v1, "participants_list"

    const/16 v2, 0xd

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpR;->c:LX/1sw;

    .line 2045425
    const/4 v0, 0x1

    sput-boolean v0, LX/DpR;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/DpM;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2045420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2045421
    iput-object p1, p0, LX/DpR;->participants_list:Ljava/util/Map;

    .line 2045422
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2045400
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2045401
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2045402
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2045403
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ParticipantsListPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045404
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045405
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045406
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045407
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045408
    const-string v4, "participants_list"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045409
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045410
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045411
    iget-object v0, p0, LX/DpR;->participants_list:Ljava/util/Map;

    if-nez v0, :cond_3

    .line 2045412
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045413
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045414
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045415
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2045416
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 2045417
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 2045418
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 2045419
    :cond_3
    iget-object v0, p0, LX/DpR;->participants_list:Ljava/util/Map;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 6

    .prologue
    .line 2045426
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2045427
    iget-object v0, p0, LX/DpR;->participants_list:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 2045428
    sget-object v0, LX/DpR;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045429
    new-instance v0, LX/7H3;

    const/16 v1, 0xa

    const/16 v2, 0xf

    iget-object v3, p0, LX/DpR;->participants_list:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 2045430
    iget-object v0, p0, LX/DpR;->participants_list:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2045431
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, LX/1su;->a(J)V

    .line 2045432
    new-instance v3, LX/1u3;

    const/16 v4, 0xc

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v3, v4, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v3}, LX/1su;->a(LX/1u3;)V

    .line 2045433
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DpM;

    .line 2045434
    invoke-virtual {v0, p1}, LX/DpM;->a(LX/1su;)V

    goto :goto_0

    .line 2045435
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2045436
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2045437
    return-void
.end method

.method public final a(LX/DpR;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2045390
    if-nez p1, :cond_1

    .line 2045391
    :cond_0
    :goto_0
    return v2

    .line 2045392
    :cond_1
    iget-object v0, p0, LX/DpR;->participants_list:Ljava/util/Map;

    if-eqz v0, :cond_4

    move v0, v1

    .line 2045393
    :goto_1
    iget-object v3, p1, LX/DpR;->participants_list:Ljava/util/Map;

    if-eqz v3, :cond_5

    move v3, v1

    .line 2045394
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 2045395
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2045396
    iget-object v0, p0, LX/DpR;->participants_list:Ljava/util/Map;

    iget-object v3, p1, LX/DpR;->participants_list:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v1

    .line 2045397
    goto :goto_0

    :cond_4
    move v0, v2

    .line 2045398
    goto :goto_1

    :cond_5
    move v3, v2

    .line 2045399
    goto :goto_2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2045386
    if-nez p1, :cond_1

    .line 2045387
    :cond_0
    :goto_0
    return v0

    .line 2045388
    :cond_1
    instance-of v1, p1, LX/DpR;

    if-eqz v1, :cond_0

    .line 2045389
    check-cast p1, LX/DpR;

    invoke-virtual {p0, p1}, LX/DpR;->a(LX/DpR;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2045385
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2045382
    sget-boolean v0, LX/DpR;->a:Z

    .line 2045383
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpR;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2045384
    return-object v0
.end method
