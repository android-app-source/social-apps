.class public final LX/DOE;
.super LX/DO0;
.source ""


# instance fields
.field public final synthetic a:LX/DOF;


# direct methods
.method public constructor <init>(LX/DOF;)V
    .locals 0

    .prologue
    .line 1992141
    iput-object p1, p0, LX/DOE;->a:LX/DOF;

    invoke-direct {p0}, LX/DO0;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1992142
    check-cast p1, LX/DNz;

    .line 1992143
    iget-object v0, p0, LX/DOE;->a:LX/DOF;

    iget-object v0, v0, LX/DOF;->d:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1992144
    iget-object v0, p1, LX/DNz;->b:LX/DOJ;

    iget-object v1, p0, LX/DOE;->a:LX/DOF;

    iget-object v1, v1, LX/DOF;->g:LX/DOJ;

    if-ne v0, v1, :cond_2

    .line 1992145
    sget-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 1992146
    :goto_0
    iget-object v1, p0, LX/DOE;->a:LX/DOF;

    iget-object v1, v1, LX/DOF;->c:LX/189;

    iget-object v2, p1, LX/DNz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2, v0}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/StoryVisibility;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1992147
    iget-object v1, p0, LX/DOE;->a:LX/DOF;

    iget-object v1, v1, LX/DOF;->e:LX/0qq;

    .line 1992148
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1992149
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1, v0}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1992150
    iget-object v0, p0, LX/DOE;->a:LX/DOF;

    iget-object v0, v0, LX/DOF;->f:LX/DOD;

    invoke-interface {v0}, LX/DNe;->b()V

    .line 1992151
    :cond_0
    iget-boolean v0, p1, LX/DNz;->c:Z

    if-nez v0, :cond_1

    .line 1992152
    iget-object v0, p0, LX/DOE;->a:LX/DOF;

    iget-object v0, v0, LX/DOF;->f:LX/DOD;

    invoke-interface {v0}, LX/DOD;->a()V

    .line 1992153
    :cond_1
    return-void

    .line 1992154
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    goto :goto_0
.end method
