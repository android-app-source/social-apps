.class public LX/D8M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/1VK;

.field private final b:LX/D8G;

.field private final c:LX/2mz;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/1VK;LX/D8G;Landroid/content/Context;LX/2mz;)V
    .locals 0
    .param p4    # LX/2mz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1968502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1968503
    iput-object p3, p0, LX/D8M;->d:Landroid/content/Context;

    .line 1968504
    iput-object p1, p0, LX/D8M;->a:LX/1VK;

    .line 1968505
    iput-object p2, p0, LX/D8M;->b:LX/D8G;

    .line 1968506
    iput-object p4, p0, LX/D8M;->c:LX/2mz;

    .line 1968507
    return-void
.end method


# virtual methods
.method public final a(LX/D8O;LX/1Po;)LX/D8F;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/D8O;",
            "TE;)",
            "LX/D8F;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1968508
    iget-object v0, p1, LX/D8O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1968509
    if-eqz v1, :cond_0

    .line 1968510
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1968511
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v4

    .line 1968512
    :goto_0
    return-object v0

    .line 1968513
    :cond_1
    iget-object v2, p1, LX/D8O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968514
    if-eqz v2, :cond_2

    .line 1968515
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1968516
    if-eqz v0, :cond_2

    .line 1968517
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1968518
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1968519
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1968520
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    move-object v0, v4

    .line 1968521
    goto :goto_0

    .line 1968522
    :cond_3
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1968523
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 1968524
    new-instance v3, LX/2oK;

    iget-object v2, p0, LX/D8M;->a:LX/1VK;

    invoke-direct {v3, v1, v0, v2}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    move-object v0, p2

    .line 1968525
    check-cast v0, LX/1Pr;

    .line 1968526
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1968527
    check-cast v2, LX/0jW;

    invoke-interface {v0, v3, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2oL;

    .line 1968528
    if-nez v3, :cond_4

    move-object v0, v4

    .line 1968529
    goto :goto_0

    .line 1968530
    :cond_4
    iget-object v0, p0, LX/D8M;->c:LX/2mz;

    iget-object v2, p0, LX/D8M;->d:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, LX/2mz;->b(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_5

    move-object v0, v4

    .line 1968531
    goto :goto_0

    .line 1968532
    :cond_5
    iget-object v0, p0, LX/D8M;->b:LX/D8G;

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-static {v2}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v2

    iget-object v4, p1, LX/D8O;->b:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v5, p0, LX/D8M;->c:LX/2mz;

    .line 1968533
    new-instance v6, LX/D8F;

    invoke-static {v0}, LX/13l;->a(LX/0QB;)LX/13l;

    move-result-object v7

    check-cast v7, LX/13l;

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    invoke-direct/range {v6 .. v12}, LX/D8F;-><init>(LX/13l;Lcom/facebook/feed/rows/core/props/FeedProps;LX/04D;LX/2oL;Ljava/util/concurrent/atomic/AtomicReference;LX/2mz;)V

    .line 1968534
    move-object v0, v6

    .line 1968535
    goto :goto_0
.end method
