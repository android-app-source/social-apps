.class public final enum LX/DjN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DjN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DjN;

.field public static final enum DISCARD_CHANGES:LX/DjN;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2033200
    new-instance v0, LX/DjN;

    const-string v1, "DISCARD_CHANGES"

    invoke-direct {v0, v1, v2}, LX/DjN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DjN;->DISCARD_CHANGES:LX/DjN;

    .line 2033201
    const/4 v0, 0x1

    new-array v0, v0, [LX/DjN;

    sget-object v1, LX/DjN;->DISCARD_CHANGES:LX/DjN;

    aput-object v1, v0, v2

    sput-object v0, LX/DjN;->$VALUES:[LX/DjN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2033203
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DjN;
    .locals 1

    .prologue
    .line 2033204
    const-class v0, LX/DjN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DjN;

    return-object v0
.end method

.method public static values()[LX/DjN;
    .locals 1

    .prologue
    .line 2033202
    sget-object v0, LX/DjN;->$VALUES:[LX/DjN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DjN;

    return-object v0
.end method
