.class public LX/Ed8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field private b:Z

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2148410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2148411
    iput-object p1, p0, LX/Ed8;->a:Landroid/content/Context;

    .line 2148412
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2148413
    iget-boolean v1, p0, LX/Ed8;->b:Z

    if-eqz v1, :cond_1

    .line 2148414
    :cond_0
    :goto_0
    return-void

    .line 2148415
    :cond_1
    const/4 v1, 0x0

    .line 2148416
    monitor-enter p0

    .line 2148417
    :try_start_0
    iget-boolean v2, p0, LX/Ed8;->b:Z

    if-nez v2, :cond_5

    .line 2148418
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_6

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2148419
    if-eqz v1, :cond_2

    .line 2148420
    iget-object v1, p0, LX/Ed8;->a:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 2148421
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getMmsUserAgent()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/Ed8;->c:Ljava/lang/String;

    .line 2148422
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getMmsUAProfUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/Ed8;->d:Ljava/lang/String;

    .line 2148423
    :cond_2
    iget-object v1, p0, LX/Ed8;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2148424
    const-string v1, "Android MmsLib/1.0"

    iput-object v1, p0, LX/Ed8;->c:Ljava/lang/String;

    .line 2148425
    :cond_3
    iget-object v1, p0, LX/Ed8;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2148426
    const-string v1, "http://www.gstatic.com/android/sms/mms_ua_profile.xml"

    iput-object v1, p0, LX/Ed8;->d:Ljava/lang/String;

    .line 2148427
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Ed8;->b:Z

    .line 2148428
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2148429
    if-eqz v0, :cond_0

    .line 2148430
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loaded user agent info: UA="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/Ed8;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UAProfUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Ed8;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2148431
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148432
    invoke-direct {p0}, LX/Ed8;->c()V

    .line 2148433
    iget-object v0, p0, LX/Ed8;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148434
    invoke-direct {p0}, LX/Ed8;->c()V

    .line 2148435
    iget-object v0, p0, LX/Ed8;->d:Ljava/lang/String;

    return-object v0
.end method
