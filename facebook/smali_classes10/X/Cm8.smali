.class public abstract LX/Cm8;
.super LX/Cm7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/Clq;",
        ":",
        "LX/Clr;",
        ">",
        "LX/Cm7",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

.field public b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

.field public c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

.field public d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

.field public g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

.field public h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

.field public i:Lcom/facebook/graphql/model/GraphQLFeedback;


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 1933204
    invoke-direct {p0, p1}, LX/Cm7;-><init>(I)V

    .line 1933205
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;
    .locals 0

    .prologue
    .line 1933197
    iput-object p1, p0, LX/Cm8;->h:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 1933198
    iput-object p2, p0, LX/Cm8;->i:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1933199
    return-object p0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;
    .locals 0

    .prologue
    .line 1933200
    iput-object p1, p0, LX/Cm8;->e:Ljava/lang/String;

    .line 1933201
    iput-object p2, p0, LX/Cm8;->f:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 1933202
    iput-object p3, p0, LX/Cm8;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 1933203
    return-object p0
.end method
