.class public final enum LX/Ewp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ewp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ewp;

.field public static final enum FRIEND_REQUEST:LX/Ewp;

.field public static final enum FRIEND_REQUEST_ACCEPTED:LX/Ewp;

.field public static final enum FRIEND_REQUEST_BULK_ACTION:LX/Ewp;

.field public static final enum FRIEND_REQUEST_HEADER:LX/Ewp;

.field public static final enum LOADING_DELETE_ALL:LX/Ewp;

.field public static final enum LOADING_MORE:LX/Ewp;

.field public static final enum NO_FRIEND_REQUESTS:LX/Ewp;

.field public static final enum PERSON_YOU_MAY_KNOW:LX/Ewp;

.field public static final enum PYMK_HEADER:LX/Ewp;

.field public static final enum RESPONDED_PERSON_YOU_MAY_KNOW:LX/Ewp;

.field public static final enum RESPONDED_REQUEST:LX/Ewp;

.field public static final enum SEE_ALL_FRIEND_REQUESTS:LX/Ewp;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2183791
    new-instance v0, LX/Ewp;

    const-string v1, "FRIEND_REQUEST_HEADER"

    invoke-direct {v0, v1, v3}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->FRIEND_REQUEST_HEADER:LX/Ewp;

    .line 2183792
    new-instance v0, LX/Ewp;

    const-string v1, "PYMK_HEADER"

    invoke-direct {v0, v1, v4}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->PYMK_HEADER:LX/Ewp;

    .line 2183793
    new-instance v0, LX/Ewp;

    const-string v1, "FRIEND_REQUEST"

    invoke-direct {v0, v1, v5}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->FRIEND_REQUEST:LX/Ewp;

    .line 2183794
    new-instance v0, LX/Ewp;

    const-string v1, "FRIEND_REQUEST_ACCEPTED"

    invoke-direct {v0, v1, v6}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->FRIEND_REQUEST_ACCEPTED:LX/Ewp;

    .line 2183795
    new-instance v0, LX/Ewp;

    const-string v1, "FRIEND_REQUEST_BULK_ACTION"

    invoke-direct {v0, v1, v7}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->FRIEND_REQUEST_BULK_ACTION:LX/Ewp;

    .line 2183796
    new-instance v0, LX/Ewp;

    const-string v1, "RESPONDED_REQUEST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->RESPONDED_REQUEST:LX/Ewp;

    .line 2183797
    new-instance v0, LX/Ewp;

    const-string v1, "PERSON_YOU_MAY_KNOW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->PERSON_YOU_MAY_KNOW:LX/Ewp;

    .line 2183798
    new-instance v0, LX/Ewp;

    const-string v1, "RESPONDED_PERSON_YOU_MAY_KNOW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/Ewp;

    .line 2183799
    new-instance v0, LX/Ewp;

    const-string v1, "NO_FRIEND_REQUESTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->NO_FRIEND_REQUESTS:LX/Ewp;

    .line 2183800
    new-instance v0, LX/Ewp;

    const-string v1, "SEE_ALL_FRIEND_REQUESTS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->SEE_ALL_FRIEND_REQUESTS:LX/Ewp;

    .line 2183801
    new-instance v0, LX/Ewp;

    const-string v1, "LOADING_MORE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->LOADING_MORE:LX/Ewp;

    .line 2183802
    new-instance v0, LX/Ewp;

    const-string v1, "LOADING_DELETE_ALL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/Ewp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ewp;->LOADING_DELETE_ALL:LX/Ewp;

    .line 2183803
    const/16 v0, 0xc

    new-array v0, v0, [LX/Ewp;

    sget-object v1, LX/Ewp;->FRIEND_REQUEST_HEADER:LX/Ewp;

    aput-object v1, v0, v3

    sget-object v1, LX/Ewp;->PYMK_HEADER:LX/Ewp;

    aput-object v1, v0, v4

    sget-object v1, LX/Ewp;->FRIEND_REQUEST:LX/Ewp;

    aput-object v1, v0, v5

    sget-object v1, LX/Ewp;->FRIEND_REQUEST_ACCEPTED:LX/Ewp;

    aput-object v1, v0, v6

    sget-object v1, LX/Ewp;->FRIEND_REQUEST_BULK_ACTION:LX/Ewp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Ewp;->RESPONDED_REQUEST:LX/Ewp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Ewp;->PERSON_YOU_MAY_KNOW:LX/Ewp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Ewp;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/Ewp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Ewp;->NO_FRIEND_REQUESTS:LX/Ewp;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Ewp;->SEE_ALL_FRIEND_REQUESTS:LX/Ewp;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Ewp;->LOADING_MORE:LX/Ewp;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Ewp;->LOADING_DELETE_ALL:LX/Ewp;

    aput-object v2, v0, v1

    sput-object v0, LX/Ewp;->$VALUES:[LX/Ewp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2183805
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ewp;
    .locals 1

    .prologue
    .line 2183806
    const-class v0, LX/Ewp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ewp;

    return-object v0
.end method

.method public static values()[LX/Ewp;
    .locals 1

    .prologue
    .line 2183804
    sget-object v0, LX/Ewp;->$VALUES:[LX/Ewp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ewp;

    return-object v0
.end method
