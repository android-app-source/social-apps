.class public LX/D4m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public final a:LX/0SG;

.field public b:Landroid/view/Window;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/D5z;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/D5e;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:J


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1962665
    const/4 v0, 0x1

    iput v0, p0, LX/D4m;->f:I

    .line 1962666
    iput-object p1, p0, LX/D4m;->a:LX/0SG;

    .line 1962667
    return-void
.end method

.method public static a(LX/0QB;)LX/D4m;
    .locals 4

    .prologue
    .line 1962598
    const-class v1, LX/D4m;

    monitor-enter v1

    .line 1962599
    :try_start_0
    sget-object v0, LX/D4m;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962600
    sput-object v2, LX/D4m;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962601
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962602
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1962603
    new-instance p0, LX/D4m;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/D4m;-><init>(LX/0SG;)V

    .line 1962604
    move-object v0, p0

    .line 1962605
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962606
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D4m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962607
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962608
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static j(LX/D4m;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    .line 1962638
    invoke-direct {p0}, LX/D4m;->l()Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    move-result-object v0

    .line 1962639
    invoke-static {p0}, LX/D4m;->m(LX/D4m;)LX/D5z;

    move-result-object v6

    .line 1962640
    if-nez v0, :cond_1

    .line 1962641
    :cond_0
    :goto_0
    return-void

    .line 1962642
    :cond_1
    iget-object v1, p0, LX/D4m;->e:LX/D5e;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, LX/D4m;->i:Z

    if-nez v1, :cond_2

    .line 1962643
    iget-object v1, p0, LX/D4m;->e:LX/D5e;

    .line 1962644
    iget-object v2, v1, LX/D5e;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v2, v2, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v2}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1962645
    :cond_2
    if-nez v6, :cond_3

    .line 1962646
    iput-boolean v4, p0, LX/D4m;->i:Z

    goto :goto_0

    .line 1962647
    :cond_3
    iput-boolean v4, p0, LX/D4m;->g:Z

    .line 1962648
    iget-object v1, v6, LX/D5z;->q:LX/2pa;

    move-object v1, v1

    .line 1962649
    invoke-virtual {v6}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v2

    .line 1962650
    invoke-virtual {v6}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v3

    .line 1962651
    invoke-virtual {v6}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v5

    .line 1962652
    iget-object v7, v5, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v5, v7

    .line 1962653
    if-eqz v5, :cond_7

    invoke-virtual {v5}, LX/2pb;->f()LX/2oi;

    move-result-object v5

    .line 1962654
    :goto_1
    const/4 v7, 0x0

    .line 1962655
    if-eqz v1, :cond_4

    iget-object v8, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v8, :cond_4

    iget-object v8, v6, LX/D5z;->f:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v9, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, v9, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g(Ljava/lang/String;)LX/2oN;

    move-result-object v8

    sget-object v9, LX/2oN;->NONE:LX/2oN;

    if-eq v8, v9, :cond_4

    .line 1962656
    const/4 v7, 0x1

    .line 1962657
    :cond_4
    iget-object v8, v6, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v8}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v8

    if-nez v8, :cond_5

    if-eqz v7, :cond_6

    .line 1962658
    :cond_5
    iget-object v7, v6, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v8, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v7, v8}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1962659
    :cond_6
    iget-boolean v6, p0, LX/D4m;->j:Z

    if-nez v6, :cond_8

    :goto_2
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a(LX/2pa;IIZLX/2oi;)V

    .line 1962660
    iget-object v0, p0, LX/D4m;->b:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1962661
    iget-object v0, p0, LX/D4m;->b:Landroid/view/Window;

    invoke-static {v0}, LX/470;->a(Landroid/view/Window;)V

    goto :goto_0

    .line 1962662
    :cond_7
    sget-object v5, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    goto :goto_1

    .line 1962663
    :cond_8
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public static k(LX/D4m;)V
    .locals 8

    .prologue
    .line 1962615
    invoke-direct {p0}, LX/D4m;->l()Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    move-result-object v1

    .line 1962616
    invoke-static {p0}, LX/D4m;->m(LX/D4m;)LX/D5z;

    move-result-object v2

    .line 1962617
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1962618
    :cond_0
    :goto_0
    return-void

    .line 1962619
    :cond_1
    iget-object v0, p0, LX/D4m;->e:LX/D5e;

    if-eqz v0, :cond_2

    .line 1962620
    iget-object v0, p0, LX/D4m;->e:LX/D5e;

    .line 1962621
    iget-object v3, v0, LX/D5e;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v3, v3, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v3}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1962622
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D4m;->g:Z

    .line 1962623
    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->getCurrentPositionMs()I

    move-result v3

    .line 1962624
    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a()Z

    move-result v4

    .line 1962625
    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->getVideoResolution()LX/2oi;

    move-result-object v0

    .line 1962626
    if-nez v0, :cond_3

    .line 1962627
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    .line 1962628
    :cond_3
    sget-object v5, LX/04G;->CHANNEL_PLAYER:LX/04G;

    invoke-virtual {v1, v5}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a(LX/04G;)V

    .line 1962629
    iget-object v1, v2, LX/D5z;->q:LX/2pa;

    move-object v1, v1

    .line 1962630
    iget-object v5, v2, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v6, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v5, v0, v6}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oi;LX/04g;)V

    .line 1962631
    iget-object v5, v2, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v6, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v5, v3, v6}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1962632
    if-eqz v4, :cond_4

    .line 1962633
    iget-object v5, v2, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v6, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v5, v6}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1962634
    :cond_4
    const-class v5, LX/31H;

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1962635
    iget-object v6, v2, LX/D5z;->p:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v7, 0x0

    invoke-virtual {v6, v1, v5, v7}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2pa;LX/0Px;Z)V

    .line 1962636
    iget-object v0, p0, LX/D4m;->b:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1962637
    iget-object v0, p0, LX/D4m;->b:Landroid/view/Window;

    invoke-static {v0}, LX/470;->b(Landroid/view/Window;)V

    goto :goto_0
.end method

.method private l()Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1962668
    iget-object v0, p0, LX/D4m;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/D4m;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    goto :goto_0
.end method

.method public static m(LX/D4m;)LX/D5z;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1962614
    iget-object v0, p0, LX/D4m;->d:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/D4m;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D5z;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1962611
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D4m;->h:Z

    .line 1962612
    invoke-static {p0}, LX/D4m;->j(LX/D4m;)V

    .line 1962613
    return-void
.end method

.method public final a(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V
    .locals 1

    .prologue
    .line 1962609
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D4m;->c:Ljava/lang/ref/WeakReference;

    .line 1962610
    return-void
.end method
