.class public final LX/E1T;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentInterfaces$GravitySettingsGraphQlFragment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;)V
    .locals 0

    .prologue
    .line 2069789
    iput-object p1, p0, LX/E1T;->a:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2069790
    iget-object v0, p0, LX/E1T;->a:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    sget-object v1, LX/E1W;->ERROR:LX/E1W;

    invoke-static {v0, v1}, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;LX/E1W;)V

    .line 2069791
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2069792
    check-cast p1, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 2069793
    iget-object v0, p0, LX/E1T;->a:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    invoke-static {v0, p1}, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V

    .line 2069794
    iget-object v0, p0, LX/E1T;->a:Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;

    sget-object v1, LX/E1W;->SETTINGS:LX/E1W;

    invoke-static {v0, v1}, Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;->a$redex0(Lcom/facebook/placetips/settings/ui/PlaceTipsSettingsFragment;LX/E1W;)V

    .line 2069795
    return-void
.end method
