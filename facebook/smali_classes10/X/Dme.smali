.class public final LX/Dme;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

.field public final synthetic b:LX/Dmn;


# direct methods
.method public constructor <init>(LX/Dmn;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V
    .locals 0

    .prologue
    .line 2039588
    iput-object p1, p0, LX/Dme;->b:LX/Dmn;

    iput-object p2, p0, LX/Dme;->a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x760a9a87

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2039589
    iget-object v0, p0, LX/Dme;->a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    invoke-static {v0}, LX/DkQ;->f(Ljava/lang/String;)LX/DkQ;

    move-result-object v2

    .line 2039590
    iget-object v0, p0, LX/Dme;->b:LX/Dmn;

    iget-object v3, v0, LX/Dmn;->b:Landroid/content/Context;

    iget-object v0, p0, LX/Dme;->b:LX/Dmn;

    iget-object v0, v0, LX/Dmn;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v4, p0, LX/Dme;->a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v0, v4}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2039591
    iget-object v2, p0, LX/Dme;->b:LX/Dmn;

    iget-object v2, v2, LX/Dmn;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Dme;->b:LX/Dmn;

    iget-object v3, v3, LX/Dmn;->b:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2039592
    const v0, -0x3ea75832

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
