.class public LX/ELI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ELG;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ELJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2108448
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ELI;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ELJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108445
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2108446
    iput-object p1, p0, LX/ELI;->b:LX/0Ot;

    .line 2108447
    return-void
.end method

.method public static a(LX/0QB;)LX/ELI;
    .locals 4

    .prologue
    .line 2108449
    const-class v1, LX/ELI;

    monitor-enter v1

    .line 2108450
    :try_start_0
    sget-object v0, LX/ELI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108451
    sput-object v2, LX/ELI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2108452
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108453
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2108454
    new-instance v3, LX/ELI;

    const/16 p0, 0x33ed

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ELI;-><init>(LX/0Ot;)V

    .line 2108455
    move-object v0, v3

    .line 2108456
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2108457
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ELI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2108458
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2108459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2108437
    check-cast p2, LX/ELH;

    .line 2108438
    iget-object v0, p0, LX/ELI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/ELH;->a:Ljava/lang/CharSequence;

    iget-object v1, p2, LX/ELH;->b:Ljava/lang/CharSequence;

    const/4 p2, 0x7

    const/4 p0, 0x5

    .line 2108439
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00d5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/8ym;->c(LX/1De;)LX/8yk;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/8yk;->a(Ljava/lang/CharSequence;)LX/8yk;

    move-result-object v3

    const v4, 0x7f0e0134

    invoke-virtual {v3, v4}, LX/8yk;->i(I)LX/8yk;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 2108440
    const v4, 0x7f3987a

    const/4 v0, 0x0

    invoke-static {p1, v4, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2108441
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1747

    invoke-interface {v3, p2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1748

    invoke-interface {v3, p0, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/8ym;->c(LX/1De;)LX/8yk;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/8yk;->a(Ljava/lang/CharSequence;)LX/8yk;

    move-result-object v3

    const v4, 0x7f0e0134

    invoke-virtual {v3, v4}, LX/8yk;->i(I)LX/8yk;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 2108442
    const v4, 0x7f3a4b5

    const/4 v0, 0x0

    invoke-static {p1, v4, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2108443
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1747

    invoke-interface {v3, p2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1748

    invoke-interface {v3, p0, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2108444
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2108419
    invoke-static {}, LX/1dS;->b()V

    .line 2108420
    iget v0, p1, LX/1dQ;->b:I

    .line 2108421
    sparse-switch v0, :sswitch_data_0

    .line 2108422
    :goto_0
    return-object v2

    .line 2108423
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2108424
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2108425
    check-cast v1, LX/ELH;

    .line 2108426
    iget-object p1, p0, LX/ELI;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/ELH;->c:Landroid/view/View$OnClickListener;

    .line 2108427
    if-eqz p1, :cond_0

    .line 2108428
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2108429
    :cond_0
    goto :goto_0

    .line 2108430
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2108431
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2108432
    check-cast v1, LX/ELH;

    .line 2108433
    iget-object p1, p0, LX/ELI;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/ELH;->d:Landroid/view/View$OnClickListener;

    .line 2108434
    if-eqz p1, :cond_1

    .line 2108435
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2108436
    :cond_1
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f3987a -> :sswitch_0
        0x7f3a4b5 -> :sswitch_1
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/ELG;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2108411
    new-instance v1, LX/ELH;

    invoke-direct {v1, p0}, LX/ELH;-><init>(LX/ELI;)V

    .line 2108412
    sget-object v2, LX/ELI;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ELG;

    .line 2108413
    if-nez v2, :cond_0

    .line 2108414
    new-instance v2, LX/ELG;

    invoke-direct {v2}, LX/ELG;-><init>()V

    .line 2108415
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/ELG;->a$redex0(LX/ELG;LX/1De;IILX/ELH;)V

    .line 2108416
    move-object v1, v2

    .line 2108417
    move-object v0, v1

    .line 2108418
    return-object v0
.end method
