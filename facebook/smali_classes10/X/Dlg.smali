.class public LX/Dlg;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/62U;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/2hU;

.field private final c:LX/6RZ;

.field public final d:LX/DnQ;

.field private final e:LX/0SG;

.field private final f:Landroid/view/LayoutInflater;

.field private final g:I

.field public final h:LX/Dip;

.field public final i:LX/Diq;

.field public final j:LX/DnT;

.field public k:LX/4nS;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Dji;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/lang/String;

.field public n:Z

.field public o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILX/Dip;LX/Diq;LX/DnT;LX/2hU;LX/6RZ;LX/DnQ;LX/0SG;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Dip;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Diq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2038522
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2038523
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    .line 2038524
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dlg;->o:Z

    .line 2038525
    iput-object p1, p0, LX/Dlg;->a:Landroid/content/Context;

    .line 2038526
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/Dlg;->f:Landroid/view/LayoutInflater;

    .line 2038527
    iput p2, p0, LX/Dlg;->g:I

    .line 2038528
    iput-object p3, p0, LX/Dlg;->h:LX/Dip;

    .line 2038529
    iput-object p4, p0, LX/Dlg;->i:LX/Diq;

    .line 2038530
    iput-object p5, p0, LX/Dlg;->j:LX/DnT;

    .line 2038531
    iput-object p6, p0, LX/Dlg;->b:LX/2hU;

    .line 2038532
    iput-object p7, p0, LX/Dlg;->c:LX/6RZ;

    .line 2038533
    iput-object p8, p0, LX/Dlg;->d:LX/DnQ;

    .line 2038534
    iput-object p9, p0, LX/Dlg;->e:LX/0SG;

    .line 2038535
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2038390
    const/4 v2, 0x0

    .line 2038391
    packed-switch p2, :pswitch_data_0

    .line 2038392
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2038393
    :pswitch_0
    iget-object v0, p0, LX/Dlg;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f030106

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038394
    new-instance v0, LX/Dlf;

    invoke-direct {v0, v1}, LX/Dlf;-><init>(Landroid/view/View;)V

    .line 2038395
    :goto_0
    return-object v0

    .line 2038396
    :pswitch_1
    iget-object v0, p0, LX/Dlg;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f030102

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038397
    iget-object v0, p0, LX/Dlg;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200a9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2038398
    invoke-static {v1, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2038399
    new-instance v0, LX/Dld;

    invoke-direct {v0, p0, v1}, LX/Dld;-><init>(LX/Dlg;Landroid/view/View;)V

    goto :goto_0

    .line 2038400
    :pswitch_2
    iget-object v0, p0, LX/Dlg;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f030103

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038401
    new-instance v0, LX/62U;

    invoke-direct {v0, v1}, LX/62U;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2038402
    :pswitch_3
    iget-object v0, p0, LX/Dlg;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f030100

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038403
    new-instance v0, LX/Dle;

    invoke-direct {v0, v1}, LX/Dle;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2038404
    :pswitch_4
    iget-object v0, p0, LX/Dlg;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f030101

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2038405
    new-instance v0, LX/Dlb;

    invoke-direct {v0, p0, v1}, LX/Dlb;-><init>(LX/Dlg;Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 13

    .prologue
    .line 2038406
    check-cast p1, LX/62U;

    .line 2038407
    iget v0, p1, LX/1a1;->e:I

    move v0, v0

    .line 2038408
    packed-switch v0, :pswitch_data_0

    .line 2038409
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown item view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2038410
    :pswitch_0
    check-cast p1, LX/Dlf;

    iget-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dji;

    .line 2038411
    iget-object v1, v0, LX/Dji;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2038412
    iget-object v1, p1, LX/Dlf;->m:Lcom/facebook/fig/sectionheader/FigSectionHeader;

    invoke-virtual {v1, v0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2038413
    :goto_0
    :pswitch_1
    return-void

    .line 2038414
    :pswitch_2
    check-cast p1, LX/Dld;

    iget-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dji;

    .line 2038415
    iget-object v1, v0, LX/Dji;->c:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

    move-object v0, v1

    .line 2038416
    const/4 v12, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2038417
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->l()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    .line 2038418
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 2038419
    iget-object v5, p1, LX/Dld;->m:LX/Dlg;

    iget-object v5, v5, LX/Dlg;->d:LX/DnQ;

    .line 2038420
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 2038421
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->j()Ljava/lang/String;

    move-result-object v6

    .line 2038422
    :goto_1
    move-object v5, v6

    .line 2038423
    iget-object v6, p1, LX/Dld;->o:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v6, v5}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2038424
    iget-object v5, p1, LX/Dld;->m:LX/Dlg;

    iget-object v5, v5, LX/Dlg;->d:LX/DnQ;

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2038425
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 2038426
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->k()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    .line 2038427
    if-eqz v6, :cond_4

    move v6, v7

    :goto_2
    if-eqz v6, :cond_7

    .line 2038428
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->k()LX/1vs;

    move-result-object v6

    iget-object v9, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2038429
    invoke-virtual {v9, v6, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    :goto_3
    if-eqz v7, :cond_8

    .line 2038430
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->n()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$UserModel;->k()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    invoke-virtual {v7, v6, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 2038431
    :goto_4
    move-object v5, v6

    .line 2038432
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2038433
    iget-object v6, p1, LX/Dld;->n:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;

    invoke-virtual {v6, v5, v2}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentCustomerProfilePictureView;->a(Landroid/net/Uri;Ljava/util/Date;)V

    .line 2038434
    iget-object v2, p1, LX/Dld;->m:LX/Dlg;

    iget-object v2, v2, LX/Dlg;->d:LX/DnQ;

    const/4 v5, 0x0

    .line 2038435
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    move-result-object v6

    if-nez v6, :cond_9

    .line 2038436
    :goto_5
    move-object v5, v5

    .line 2038437
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;->a()J

    move-result-wide v8

    cmp-long v2, v6, v8

    if-eqz v2, :cond_0

    move v2, v3

    .line 2038438
    :goto_6
    if-eqz v2, :cond_1

    iget-object v2, p1, LX/62U;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f082bf6

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p1, LX/Dld;->m:LX/Dlg;

    iget-object v8, v8, LX/Dlg;->j:LX/DnT;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, LX/DnT;->e(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    iget-object v4, p1, LX/Dld;->m:LX/Dlg;

    iget-object v4, v4, LX/Dlg;->j:LX/DnT;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    iget-object v3, p1, LX/Dld;->m:LX/Dlg;

    iget-object v3, v3, LX/Dlg;->j:LX/DnT;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;->a()J

    move-result-wide v8

    invoke-virtual {v3, v8, v9}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v12

    invoke-virtual {v2, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2038439
    :goto_7
    if-eqz v5, :cond_2

    .line 2038440
    iget-object v3, p1, LX/Dld;->o:Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2038441
    :goto_8
    iget-object v2, p1, LX/62U;->l:Landroid/view/View;

    new-instance v3, LX/Dlc;

    invoke-direct {v3, p1, v0}, LX/Dlc;-><init>(LX/Dld;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2038442
    goto/16 :goto_0

    .line 2038443
    :pswitch_3
    check-cast p1, LX/Dle;

    iget v0, p0, LX/Dlg;->g:I

    invoke-virtual {p1, v0}, LX/Dle;->c(I)V

    goto/16 :goto_0

    :cond_0
    move v2, v4

    .line 2038444
    goto :goto_6

    .line 2038445
    :cond_1
    iget-object v2, p1, LX/62U;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f082bf7

    new-array v7, v12, [Ljava/lang/Object;

    iget-object v8, p1, LX/Dld;->m:LX/Dlg;

    iget-object v8, v8, LX/Dlg;->j:LX/DnT;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, LX/DnT;->e(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    iget-object v4, p1, LX/Dld;->m:LX/Dlg;

    iget-object v4, v4, LX/Dlg;->j:LX/DnT;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-virtual {v2, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    .line 2038446
    :cond_2
    iget-object v3, p1, LX/Dld;->o:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v3, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_8

    .line 2038447
    :cond_3
    iget-object v6, v5, LX/DnQ;->a:LX/03V;

    const-string v7, "appointment calendar"

    const-string v8, "missing customer name"

    invoke-virtual {v6, v7, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2038448
    iget-object v6, v5, LX/DnQ;->b:Landroid/content/Context;

    const v7, 0x7f082bd0

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    :cond_4
    move v6, v8

    .line 2038449
    goto/16 :goto_2

    :cond_5
    move v6, v8

    goto/16 :goto_2

    :cond_6
    move v7, v8

    goto/16 :goto_3

    :cond_7
    move v7, v8

    goto/16 :goto_3

    .line 2038450
    :cond_8
    iget-object v6, v5, LX/DnQ;->a:LX/03V;

    const-string v7, "appointment calendar"

    const-string v8, "missing profile uri"

    invoke-virtual {v6, v7, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2038451
    const-string v6, ""

    goto/16 :goto_4

    .line 2038452
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 2038453
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel$ProductItemModel;->j()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_5

    .line 2038454
    :cond_a
    iget-object v6, v2, LX/DnQ;->a:LX/03V;

    const-string v7, "appointment calendar"

    const-string v8, "missing service name"

    invoke-virtual {v6, v7, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2038455
    iget-object v0, p0, LX/Dlg;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2038456
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2038457
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2038458
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

    .line 2038459
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->l()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    .line 2038460
    iget-object v1, p0, LX/Dlg;->c:LX/6RZ;

    .line 2038461
    invoke-static {v2, v3}, LX/6Rc;->a(J)LX/6Rc;

    move-result-object v7

    .line 2038462
    invoke-static {v2, v3}, LX/6Rc;->b(J)LX/6Rc;

    move-result-object v10

    .line 2038463
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 2038464
    sget-object v12, LX/6RX;->a:[I

    invoke-virtual {v1, v8, v9, v2, v3}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object p1

    invoke-virtual {p1}, LX/6RY;->ordinal()I

    move-result p1

    aget v12, v12, p1

    packed-switch v12, :pswitch_data_0

    .line 2038465
    :cond_0
    invoke-virtual {v10, v8, v9}, LX/6Rc;->c(J)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2038466
    invoke-virtual {v1, v11}, LX/6RZ;->f(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 2038467
    :goto_1
    move-object v1, v7

    .line 2038468
    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2038469
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2038470
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2038471
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2038472
    invoke-interface {v4, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2038473
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2038474
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2038475
    iget-object v2, p0, LX/Dlg;->m:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2038476
    iget-object v2, p0, LX/Dlg;->l:Ljava/util/List;

    .line 2038477
    new-instance v3, LX/Dji;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v3, v5, v0, v6}, LX/Dji;-><init>(ILjava/lang/String;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;)V

    move-object v3, v3

    .line 2038478
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2038479
    iput-object v0, p0, LX/Dlg;->m:Ljava/lang/String;

    .line 2038480
    :cond_4
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

    .line 2038481
    iget-object v3, p0, LX/Dlg;->l:Ljava/util/List;

    .line 2038482
    new-instance v5, LX/Dji;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7, v0}, LX/Dji;-><init>(ILjava/lang/String;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;)V

    move-object v0, v5

    .line 2038483
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2038484
    :cond_5
    return-void

    .line 2038485
    :pswitch_0
    iget-object v7, v1, LX/6RZ;->o:Ljava/lang/String;

    goto :goto_1

    .line 2038486
    :pswitch_1
    iget-object v7, v1, LX/6RZ;->p:Ljava/lang/String;

    goto :goto_1

    .line 2038487
    :pswitch_2
    iget-object v7, v1, LX/6RZ;->q:Ljava/lang/String;

    goto :goto_1

    .line 2038488
    :pswitch_3
    iget-object v7, v1, LX/6RZ;->f:Ljava/text/DateFormat;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 2038489
    :pswitch_4
    iget-object v7, v1, LX/6RZ;->d:Landroid/content/Context;

    const v10, 0x7f0800a4

    invoke-virtual {v7, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    .line 2038490
    :pswitch_5
    invoke-virtual {v7, v8, v9}, LX/6Rc;->c(J)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2038491
    invoke-virtual {v1, v11}, LX/6RZ;->h(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    .line 2038492
    :cond_6
    invoke-virtual {v1, v11}, LX/6RZ;->g(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2038493
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dlg;->n:Z

    .line 2038494
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2038495
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    .line 2038496
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2038497
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Dlg;->o:Z

    .line 2038498
    iget-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2038499
    iget-object v0, p0, LX/Dlg;->k:LX/4nS;

    if-eqz v0, :cond_0

    .line 2038500
    iget-object v0, p0, LX/Dlg;->k:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 2038501
    :cond_0
    new-instance v0, LX/62h;

    iget-object v1, p0, LX/Dlg;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/62h;-><init>(Landroid/content/Context;)V

    .line 2038502
    new-instance v1, LX/DlZ;

    invoke-direct {v1, p0}, LX/DlZ;-><init>(LX/Dlg;)V

    invoke-virtual {v0, v1}, LX/62h;->setRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 2038503
    iget-object v1, p0, LX/Dlg;->b:LX/2hU;

    const/16 v2, 0x2710

    invoke-virtual {v1, v0, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    iput-object v0, p0, LX/Dlg;->k:LX/4nS;

    .line 2038504
    iget-object v0, p0, LX/Dlg;->k:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->a()V

    .line 2038505
    :cond_1
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2038506
    iget-boolean v0, p0, LX/Dlg;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2038507
    const/4 v0, 0x2

    .line 2038508
    :goto_0
    return v0

    .line 2038509
    :cond_0
    iget-boolean v0, p0, LX/Dlg;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2038510
    const/4 v0, 0x4

    goto :goto_0

    .line 2038511
    :cond_1
    iget-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2038512
    const/4 v0, 0x3

    goto :goto_0

    .line 2038513
    :cond_2
    iget-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dji;

    .line 2038514
    iget p0, v0, LX/Dji;->a:I

    move v0, p0

    .line 2038515
    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2038516
    iget-boolean v0, p0, LX/Dlg;->n:Z

    if-eqz v0, :cond_0

    .line 2038517
    iget-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2038518
    :goto_0
    return v0

    .line 2038519
    :cond_0
    iget-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2038520
    const/4 v0, 0x1

    goto :goto_0

    .line 2038521
    :cond_1
    iget-object v0, p0, LX/Dlg;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
