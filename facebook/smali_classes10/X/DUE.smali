.class public final LX/DUE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2001953
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 2001954
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2001955
    :goto_0
    return v1

    .line 2001956
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 2001957
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2001958
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2001959
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 2001960
    const-string v7, "can_viewer_claim_adminship"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2001961
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 2001962
    :cond_1
    const-string v7, "group_admin_profiles"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2001963
    invoke-static {p0, p1}, LX/DUD;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2001964
    :cond_2
    const-string v7, "group_moderators"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2001965
    invoke-static {p0, p1}, LX/DUD;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2001966
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2001967
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2001968
    if-eqz v0, :cond_5

    .line 2001969
    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 2001970
    :cond_5
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2001971
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2001972
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2001973
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2001974
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2001975
    if-eqz v0, :cond_0

    .line 2001976
    const-string v1, "can_viewer_claim_adminship"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001977
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2001978
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2001979
    if-eqz v0, :cond_1

    .line 2001980
    const-string v1, "group_admin_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001981
    invoke-static {p0, v0, p2, p3}, LX/DUD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2001982
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2001983
    if-eqz v0, :cond_2

    .line 2001984
    const-string v1, "group_moderators"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001985
    invoke-static {p0, v0, p2, p3}, LX/DUD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2001986
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2001987
    return-void
.end method
