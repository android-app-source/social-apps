.class public final LX/EOw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Ps;

.field public final synthetic b:LX/CzL;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;LX/1Ps;LX/CzL;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2115568
    iput-object p1, p0, LX/EOw;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    iput-object p2, p0, LX/EOw;->a:LX/1Ps;

    iput-object p3, p0, LX/EOw;->b:LX/CzL;

    iput-object p4, p0, LX/EOw;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v0, 0x1

    const v1, -0x4b7d562e

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2115569
    iget-object v0, p0, LX/EOw;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->i:LX/CvY;

    iget-object v1, p0, LX/EOw;->a:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->OPEN_LINK_BY_IMAGE:LX/8ch;

    iget-object v3, p0, LX/EOw;->a:LX/1Ps;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/EOw;->b:LX/CzL;

    .line 2115570
    iget-object v5, v4, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v4, v5

    .line 2115571
    invoke-interface {v3, v4}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v3

    iget-object v4, p0, LX/EOw;->b:LX/CzL;

    .line 2115572
    iget-object v5, p0, LX/EOw;->a:LX/1Ps;

    check-cast v5, LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v7

    iget-object v5, p0, LX/EOw;->a:LX/1Ps;

    check-cast v5, LX/CxP;

    iget-object v8, p0, LX/EOw;->b:LX/CzL;

    .line 2115573
    iget-object v9, v8, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v8, v9

    .line 2115574
    invoke-interface {v5, v8}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v8

    sget-object v9, LX/8ch;->OPEN_LINK_BY_IMAGE:LX/8ch;

    iget-object v5, p0, LX/EOw;->b:LX/CzL;

    .line 2115575
    iget-object p1, v5, LX/CzL;->d:LX/0am;

    move-object v5, p1

    .line 2115576
    invoke-virtual {v5}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v7, v8, v9, v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/8ch;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2115577
    iget-object v0, p0, LX/EOw;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    iget-object v1, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->h:LX/1xP;

    iget-object v2, p0, LX/EOw;->c:Landroid/content/Context;

    iget-object v0, p0, LX/EOw;->b:LX/CzL;

    .line 2115578
    iget-object v3, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v3

    .line 2115579
    check-cast v0, LX/A3T;

    invoke-interface {v0}, LX/A3T;->O()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, v11, v11}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    .line 2115580
    const v0, 0x1ae169c0

    invoke-static {v10, v10, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
