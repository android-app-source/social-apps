.class public final LX/EW4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/EWC;


# direct methods
.method public constructor <init>(LX/EWC;)V
    .locals 0

    .prologue
    .line 2129335
    iput-object p1, p0, LX/EW4;->a:LX/EWC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2129333
    iget-object v0, p0, LX/EW4;->a:LX/EWC;

    invoke-virtual {v0}, LX/EWC;->invalidate()V

    .line 2129334
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2129330
    iget-object v0, p0, LX/EW4;->a:LX/EWC;

    .line 2129331
    invoke-static {v0, p1}, LX/EWC;->a$redex0(LX/EWC;Landroid/view/View;)V

    .line 2129332
    return-void
.end method

.method public final b(Landroid/view/View;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2129328
    iget-object v0, p0, LX/EW4;->a:LX/EWC;

    invoke-virtual {v0, p1}, LX/EWC;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 2129329
    iget-object v1, p0, LX/EW4;->a:LX/EWC;

    invoke-virtual {v1}, LX/EWC;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, LX/EW4;->a:LX/EWC;

    invoke-interface {v1, v0, v2, v3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2129324
    iget-object v0, p0, LX/EW4;->a:LX/EWC;

    const/4 v1, 0x1

    .line 2129325
    iput-boolean v1, v0, LX/EWC;->v:Z

    .line 2129326
    iget-object v0, p0, LX/EW4;->a:LX/EWC;

    invoke-static {v0}, LX/EWC;->e(LX/EWC;)V

    .line 2129327
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2129318
    iget-object v0, p0, LX/EW4;->a:LX/EWC;

    const/4 v1, 0x0

    .line 2129319
    iput-boolean v1, v0, LX/EWC;->v:Z

    .line 2129320
    iget-object v0, p0, LX/EW4;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->u:LX/EWA;

    if-eqz v0, :cond_0

    .line 2129321
    :cond_0
    iget-object v0, p0, LX/EW4;->a:LX/EWC;

    invoke-static {v0}, LX/EWC;->e(LX/EWC;)V

    .line 2129322
    return-void
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2129323
    iget-object v0, p0, LX/EW4;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->M:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method
