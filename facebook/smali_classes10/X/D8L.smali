.class public final LX/D8L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;)V
    .locals 0

    .prologue
    .line 1968451
    iput-object p1, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1968373
    const/4 v0, 0x0

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 11

    .prologue
    .line 1968430
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v0, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v0, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    const v7, 0x7fffffff

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 1968431
    iget-object v1, v0, LX/D8f;->a:LX/D7g;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/D8f;->a:LX/D7g;

    invoke-interface {v1}, LX/D7g;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, LX/D8f;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p2}, LX/D8f;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/D8f;->b:LX/D8a;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/D8f;->c:Landroid/widget/Scroller;

    if-nez v1, :cond_3

    .line 1968432
    :cond_0
    :goto_0
    move v0, v2

    .line 1968433
    if-nez v0, :cond_2

    .line 1968434
    :cond_1
    const/4 v0, 0x0

    .line 1968435
    :goto_1
    return v0

    .line 1968436
    :cond_2
    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v0, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/D8f;->b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    .line 1968437
    const/4 v0, 0x1

    goto :goto_1

    .line 1968438
    :cond_3
    iget-object v1, v0, LX/D8f;->b:LX/D8a;

    iget-object v3, v0, LX/D8f;->a:LX/D7g;

    invoke-interface {v3}, LX/D7g;->b()F

    move-result v3

    iget-object v4, v0, LX/D8f;->a:LX/D7g;

    invoke-interface {v4}, LX/D7g;->d()I

    move-result v4

    iget-object v5, v0, LX/D8f;->a:LX/D7g;

    invoke-interface {v5}, LX/D7g;->c()I

    move-result v5

    invoke-virtual {v1, v3, v4, v5}, LX/D8a;->a(FII)V

    .line 1968439
    iget-object v1, v0, LX/D8f;->b:LX/D8a;

    invoke-virtual {v1}, LX/D8a;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    move v2, v10

    .line 1968440
    goto :goto_0

    .line 1968441
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    move v1, v10

    .line 1968442
    :goto_2
    if-eqz v1, :cond_6

    move v2, v10

    .line 1968443
    goto :goto_0

    :cond_5
    move v1, v2

    .line 1968444
    goto :goto_2

    .line 1968445
    :cond_6
    iget-object v1, v0, LX/D8f;->b:LX/D8a;

    invoke-virtual {v1}, LX/D8a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1968446
    iget-object v1, v0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v1, v10}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 1968447
    iget-object v1, v0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v4, v3

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v5, v3

    move v3, v2

    move v6, v2

    move v8, v2

    move v9, v7

    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 1968448
    iget-object v1, v0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getFinalY()I

    move-result v1

    .line 1968449
    iget-object v3, v0, LX/D8f;->a:LX/D7g;

    invoke-interface {v3}, LX/D7g;->c()I

    move-result v3

    if-le v1, v3, :cond_0

    move v2, v10

    .line 1968450
    goto :goto_0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 1968429
    return-void
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1968380
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v2, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v2, v2, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    if-nez v2, :cond_1

    .line 1968381
    :cond_0
    :goto_0
    return v0

    .line 1968382
    :cond_1
    iget-object v2, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-boolean v2, v2, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->f:Z

    if-nez v2, :cond_2

    iget-object v2, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v2, v2, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    invoke-virtual {v2, p1, p2}, LX/D8f;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1968383
    iget-object v2, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    .line 1968384
    iput-boolean v1, v2, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->g:Z

    .line 1968385
    iget-object v1, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 1968386
    iput v2, v1, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->h:F

    .line 1968387
    iget-object v1, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1968388
    iput v2, v1, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->i:F

    .line 1968389
    goto :goto_0

    .line 1968390
    :cond_2
    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    .line 1968391
    iput-boolean v1, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->f:Z

    .line 1968392
    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-boolean v0, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->g:Z

    if-nez v0, :cond_3

    .line 1968393
    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 1968394
    iput v2, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->h:F

    .line 1968395
    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1968396
    iput v2, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->i:F

    .line 1968397
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 1968398
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 1968399
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v2, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget v2, v2, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->h:F

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 1968400
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget v3, v3, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->i:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 1968401
    iget-object v3, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v3, v3, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1968402
    iget-object v6, v3, LX/D8f;->a:LX/D7g;

    if-eqz v6, :cond_4

    iget-object v6, v3, LX/D8f;->a:LX/D7g;

    invoke-interface {v6}, LX/D7g;->i()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, v3, LX/D8f;->b:LX/D8a;

    if-nez v6, :cond_5

    .line 1968403
    :cond_4
    :goto_1
    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    .line 1968404
    iput-boolean v1, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->g:Z

    .line 1968405
    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 1968406
    iput v2, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->h:F

    .line 1968407
    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 1968408
    iput v2, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->i:F

    .line 1968409
    move v0, v1

    .line 1968410
    goto/16 :goto_0

    .line 1968411
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-gt v6, v4, :cond_6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-le v6, v4, :cond_7

    .line 1968412
    :cond_6
    goto :goto_1

    .line 1968413
    :cond_7
    iget-object v6, v3, LX/D8f;->b:LX/D8a;

    iget-object p3, v3, LX/D8f;->a:LX/D7g;

    invoke-interface {p3}, LX/D7g;->e()I

    move-result p3

    iget-object p4, v3, LX/D8f;->a:LX/D7g;

    invoke-interface {p4}, LX/D7g;->g()I

    move-result p4

    sub-int/2addr p3, p4

    invoke-static {v5, p3}, Ljava/lang/Math;->max(II)I

    move-result p3

    iget-object p4, v3, LX/D8f;->a:LX/D7g;

    invoke-interface {p4}, LX/D7g;->f()I

    move-result p4

    invoke-virtual {v6, v5, p3, v5, p4}, LX/D8a;->a(IIII)V

    .line 1968414
    iget-object v5, v3, LX/D8f;->b:LX/D8a;

    iget-object v6, v3, LX/D8f;->a:LX/D7g;

    invoke-interface {v6}, LX/D7g;->b()F

    move-result v6

    iget-object p3, v3, LX/D8f;->a:LX/D7g;

    invoke-interface {p3}, LX/D7g;->d()I

    move-result p3

    iget-object p4, v3, LX/D8f;->a:LX/D7g;

    invoke-interface {p4}, LX/D7g;->c()I

    move-result p4

    invoke-virtual {v5, v6, p3, p4}, LX/D8a;->a(FII)V

    .line 1968415
    iget-object v5, v3, LX/D8f;->b:LX/D8a;

    int-to-float v6, v0

    int-to-float p3, v2

    invoke-virtual {v5, v6, p3}, LX/D8a;->a(FF)V

    .line 1968416
    iget-object v5, v3, LX/D8f;->a:LX/D7g;

    iget-object v6, v3, LX/D8f;->b:LX/D8a;

    .line 1968417
    iget p3, v6, LX/D8a;->j:F

    move v6, p3

    .line 1968418
    invoke-interface {v5, v6}, LX/D7g;->a(F)V

    .line 1968419
    iget-object v5, v3, LX/D8f;->a:LX/D7g;

    iget-object v6, v3, LX/D8f;->b:LX/D8a;

    .line 1968420
    iget p3, v6, LX/D8a;->k:I

    move v6, p3

    .line 1968421
    invoke-interface {v5, v6}, LX/D7g;->a(I)V

    .line 1968422
    iget-object v5, v3, LX/D8f;->a:LX/D7g;

    iget-object v6, v3, LX/D8f;->b:LX/D8a;

    .line 1968423
    iget p3, v6, LX/D8a;->l:I

    move v6, p3

    .line 1968424
    invoke-interface {v5, v6}, LX/D7g;->b(I)V

    .line 1968425
    iget-object v5, v3, LX/D8f;->e:LX/D8S;

    if-eqz v5, :cond_4

    .line 1968426
    iget-object v5, v3, LX/D8f;->e:LX/D8S;

    iget-object v6, v3, LX/D8f;->b:LX/D8a;

    .line 1968427
    iget p3, v6, LX/D8a;->j:F

    move v6, p3

    .line 1968428
    invoke-virtual {v5, v6}, LX/D8S;->a(F)Z

    goto/16 :goto_1
.end method

.method public final onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 1968379
    return-void
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1968374
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v0, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v0, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    invoke-virtual {v0, p1}, LX/D8f;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1968375
    :cond_0
    const/4 v0, 0x0

    .line 1968376
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D8L;->a:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v0, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    .line 1968377
    iget-object p0, v0, LX/D8f;->a:LX/D7g;

    invoke-interface {p0}, LX/D7g;->h()Z

    move-result p0

    move v0, p0

    .line 1968378
    goto :goto_0
.end method
