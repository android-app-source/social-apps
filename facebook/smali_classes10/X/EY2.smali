.class public final LX/EY2;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EY0;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EY2;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EY2;


# instance fields
.field public bitField0_:I

.field public leadingComments_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private pathMemoizedSerializedSize:I

.field public path_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private spanMemoizedSerializedSize:I

.field public span_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public trailingComments_:Ljava/lang/Object;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2136114
    new-instance v0, LX/EXz;

    invoke-direct {v0}, LX/EXz;-><init>()V

    sput-object v0, LX/EY2;->a:LX/EWZ;

    .line 2136115
    new-instance v0, LX/EY2;

    invoke-direct {v0}, LX/EY2;-><init>()V

    .line 2136116
    sput-object v0, LX/EY2;->c:LX/EY2;

    invoke-direct {v0}, LX/EY2;->q()V

    .line 2136117
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2136118
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2136119
    iput v0, p0, LX/EY2;->pathMemoizedSerializedSize:I

    .line 2136120
    iput v0, p0, LX/EY2;->spanMemoizedSerializedSize:I

    .line 2136121
    iput-byte v0, p0, LX/EY2;->memoizedIsInitialized:B

    .line 2136122
    iput v0, p0, LX/EY2;->memoizedSerializedSize:I

    .line 2136123
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2136124
    iput-object v0, p0, LX/EY2;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 2136125
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2136126
    iput v1, p0, LX/EY2;->pathMemoizedSerializedSize:I

    .line 2136127
    iput v1, p0, LX/EY2;->spanMemoizedSerializedSize:I

    .line 2136128
    iput-byte v1, p0, LX/EY2;->memoizedIsInitialized:B

    .line 2136129
    iput v1, p0, LX/EY2;->memoizedSerializedSize:I

    .line 2136130
    invoke-direct {p0}, LX/EY2;->q()V

    .line 2136131
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v3

    move v1, v0

    .line 2136132
    :cond_0
    :goto_0
    if-nez v0, :cond_9

    .line 2136133
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v4

    .line 2136134
    sparse-switch v4, :sswitch_data_0

    .line 2136135
    invoke-virtual {p0, p1, v3, p2, v4}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 2136136
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 2136137
    goto :goto_0

    .line 2136138
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 2136139
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/EY2;->path_:Ljava/util/List;

    .line 2136140
    or-int/lit8 v1, v1, 0x1

    .line 2136141
    :cond_1
    iget-object v4, p0, LX/EY2;->path_:Ljava/util/List;

    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2136142
    :catch_0
    move-exception v0

    .line 2136143
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2136144
    move-object v0, v0

    .line 2136145
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2136146
    :catchall_0
    move-exception v0

    and-int/lit8 v4, v1, 0x1

    if-ne v4, v2, :cond_2

    .line 2136147
    iget-object v2, p0, LX/EY2;->path_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EY2;->path_:Ljava/util/List;

    .line 2136148
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_3

    .line 2136149
    iget-object v1, p0, LX/EY2;->span_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EY2;->span_:Ljava/util/List;

    .line 2136150
    :cond_3
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EY2;->unknownFields:LX/EZQ;

    .line 2136151
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2136152
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, LX/EWd;->r()I

    move-result v4

    .line 2136153
    invoke-virtual {p1, v4}, LX/EWd;->c(I)I

    move-result v4

    .line 2136154
    and-int/lit8 v5, v1, 0x1

    if-eq v5, v2, :cond_4

    invoke-virtual {p1}, LX/EWd;->s()I

    move-result v5

    if-lez v5, :cond_4

    .line 2136155
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, LX/EY2;->path_:Ljava/util/List;

    .line 2136156
    or-int/lit8 v1, v1, 0x1

    .line 2136157
    :cond_4
    :goto_1
    invoke-virtual {p1}, LX/EWd;->s()I

    move-result v5

    if-lez v5, :cond_5

    .line 2136158
    iget-object v5, p0, LX/EY2;->path_:Ljava/util/List;

    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2136159
    :catch_1
    move-exception v0

    .line 2136160
    :try_start_3
    new-instance v4, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2136161
    iput-object p0, v4, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2136162
    move-object v0, v4

    .line 2136163
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2136164
    :cond_5
    :try_start_4
    invoke-virtual {p1, v4}, LX/EWd;->d(I)V

    goto/16 :goto_0

    .line 2136165
    :sswitch_3
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_6

    .line 2136166
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/EY2;->span_:Ljava/util/List;

    .line 2136167
    or-int/lit8 v1, v1, 0x2

    .line 2136168
    :cond_6
    iget-object v4, p0, LX/EY2;->span_:Ljava/util/List;

    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2136169
    :sswitch_4
    invoke-virtual {p1}, LX/EWd;->r()I

    move-result v4

    .line 2136170
    invoke-virtual {p1, v4}, LX/EWd;->c(I)I

    move-result v4

    .line 2136171
    and-int/lit8 v5, v1, 0x2

    if-eq v5, v7, :cond_7

    invoke-virtual {p1}, LX/EWd;->s()I

    move-result v5

    if-lez v5, :cond_7

    .line 2136172
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, LX/EY2;->span_:Ljava/util/List;

    .line 2136173
    or-int/lit8 v1, v1, 0x2

    .line 2136174
    :cond_7
    :goto_2
    invoke-virtual {p1}, LX/EWd;->s()I

    move-result v5

    if-lez v5, :cond_8

    .line 2136175
    iget-object v5, p0, LX/EY2;->span_:Ljava/util/List;

    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2136176
    :cond_8
    invoke-virtual {p1, v4}, LX/EWd;->d(I)V

    goto/16 :goto_0

    .line 2136177
    :sswitch_5
    iget v4, p0, LX/EY2;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/EY2;->bitField0_:I

    .line 2136178
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v4

    iput-object v4, p0, LX/EY2;->leadingComments_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 2136179
    :sswitch_6
    iget v4, p0, LX/EY2;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, LX/EY2;->bitField0_:I

    .line 2136180
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v4

    iput-object v4, p0, LX/EY2;->trailingComments_:Ljava/lang/Object;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2136181
    :cond_9
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_a

    .line 2136182
    iget-object v0, p0, LX/EY2;->path_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY2;->path_:Ljava/util/List;

    .line 2136183
    :cond_a
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_b

    .line 2136184
    iget-object v0, p0, LX/EY2;->span_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY2;->span_:Ljava/util/List;

    .line 2136185
    :cond_b
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EY2;->unknownFields:LX/EZQ;

    .line 2136186
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2136187
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x12 -> :sswitch_4
        0x1a -> :sswitch_5
        0x22 -> :sswitch_6
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2136188
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2136189
    iput v1, p0, LX/EY2;->pathMemoizedSerializedSize:I

    .line 2136190
    iput v1, p0, LX/EY2;->spanMemoizedSerializedSize:I

    .line 2136191
    iput-byte v1, p0, LX/EY2;->memoizedIsInitialized:B

    .line 2136192
    iput v1, p0, LX/EY2;->memoizedSerializedSize:I

    .line 2136193
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EY2;->unknownFields:LX/EZQ;

    .line 2136194
    return-void
.end method

.method private static e(LX/EY2;)LX/EY1;
    .locals 1

    .prologue
    .line 2136195
    invoke-static {}, LX/EY1;->m()LX/EY1;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EY1;->a(LX/EY2;)LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method private o()LX/EWc;
    .locals 2

    .prologue
    .line 2136196
    iget-object v0, p0, LX/EY2;->leadingComments_:Ljava/lang/Object;

    .line 2136197
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2136198
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2136199
    iput-object v0, p0, LX/EY2;->leadingComments_:Ljava/lang/Object;

    .line 2136200
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private p()LX/EWc;
    .locals 2

    .prologue
    .line 2136201
    iget-object v0, p0, LX/EY2;->trailingComments_:Ljava/lang/Object;

    .line 2136202
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2136203
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2136204
    iput-object v0, p0, LX/EY2;->trailingComments_:Ljava/lang/Object;

    .line 2136205
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 2136206
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY2;->path_:Ljava/util/List;

    .line 2136207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY2;->span_:Ljava/util/List;

    .line 2136208
    const-string v0, ""

    iput-object v0, p0, LX/EY2;->leadingComments_:Ljava/lang/Object;

    .line 2136209
    const-string v0, ""

    iput-object v0, p0, LX/EY2;->trailingComments_:Ljava/lang/Object;

    .line 2136210
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2136091
    new-instance v0, LX/EY1;

    invoke-direct {v0, p1}, LX/EY1;-><init>(LX/EYd;)V

    .line 2136092
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2136093
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2136094
    iget-object v0, p0, LX/EY2;->path_:Ljava/util/List;

    move-object v0, v0

    .line 2136095
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2136096
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, LX/EWf;->l(I)V

    .line 2136097
    iget v0, p0, LX/EY2;->pathMemoizedSerializedSize:I

    invoke-virtual {p1, v0}, LX/EWf;->l(I)V

    :cond_0
    move v1, v2

    .line 2136098
    :goto_0
    iget-object v0, p0, LX/EY2;->path_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2136099
    iget-object v0, p0, LX/EY2;->path_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/EWf;->a(I)V

    .line 2136100
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2136101
    :cond_1
    iget-object v0, p0, LX/EY2;->span_:Ljava/util/List;

    move-object v0, v0

    .line 2136102
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 2136103
    const/16 v0, 0x12

    invoke-virtual {p1, v0}, LX/EWf;->l(I)V

    .line 2136104
    iget v0, p0, LX/EY2;->spanMemoizedSerializedSize:I

    invoke-virtual {p1, v0}, LX/EWf;->l(I)V

    .line 2136105
    :cond_2
    :goto_1
    iget-object v0, p0, LX/EY2;->span_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2136106
    iget-object v0, p0, LX/EY2;->span_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/EWf;->a(I)V

    .line 2136107
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2136108
    :cond_3
    iget v0, p0, LX/EY2;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 2136109
    const/4 v0, 0x3

    invoke-direct {p0}, LX/EY2;->o()LX/EWc;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2136110
    :cond_4
    iget v0, p0, LX/EY2;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 2136111
    const/4 v0, 0x4

    invoke-direct {p0}, LX/EY2;->p()LX/EWc;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2136112
    :cond_5
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2136113
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2136086
    iget-byte v1, p0, LX/EY2;->memoizedIsInitialized:B

    .line 2136087
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2136088
    :goto_0
    return v0

    .line 2136089
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2136090
    :cond_1
    iput-byte v0, p0, LX/EY2;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2136059
    iget v0, p0, LX/EY2;->memoizedSerializedSize:I

    .line 2136060
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2136061
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 2136062
    :goto_1
    iget-object v0, p0, LX/EY2;->path_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2136063
    iget-object v0, p0, LX/EY2;->path_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/EWf;->g(I)I

    move-result v0

    add-int/2addr v3, v0

    .line 2136064
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2136065
    :cond_1
    add-int/lit8 v0, v3, 0x0

    .line 2136066
    iget-object v1, p0, LX/EY2;->path_:Ljava/util/List;

    move-object v1, v1

    .line 2136067
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2136068
    add-int/lit8 v0, v0, 0x1

    .line 2136069
    invoke-static {v3}, LX/EWf;->g(I)I

    move-result v1

    add-int/2addr v0, v1

    move v1, v0

    .line 2136070
    :goto_2
    iput v3, p0, LX/EY2;->pathMemoizedSerializedSize:I

    move v3, v2

    .line 2136071
    :goto_3
    iget-object v0, p0, LX/EY2;->span_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2136072
    iget-object v0, p0, LX/EY2;->span_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/EWf;->g(I)I

    move-result v0

    add-int/2addr v0, v3

    .line 2136073
    add-int/lit8 v2, v2, 0x1

    move v3, v0

    goto :goto_3

    .line 2136074
    :cond_2
    add-int v0, v1, v3

    .line 2136075
    iget-object v1, p0, LX/EY2;->span_:Ljava/util/List;

    move-object v1, v1

    .line 2136076
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2136077
    add-int/lit8 v0, v0, 0x1

    .line 2136078
    invoke-static {v3}, LX/EWf;->g(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 2136079
    :cond_3
    iput v3, p0, LX/EY2;->spanMemoizedSerializedSize:I

    .line 2136080
    iget v1, p0, LX/EY2;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 2136081
    const/4 v1, 0x3

    invoke-direct {p0}, LX/EY2;->o()LX/EWc;

    move-result-object v2

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2136082
    :cond_4
    iget v1, p0, LX/EY2;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 2136083
    const/4 v1, 0x4

    invoke-direct {p0}, LX/EY2;->p()LX/EWc;

    move-result-object v2

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2136084
    :cond_5
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2136085
    iput v0, p0, LX/EY2;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_6
    move v1, v0

    goto :goto_2
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2136058
    iget-object v0, p0, LX/EY2;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2136057
    sget-object v0, LX/EYC;->N:LX/EYn;

    const-class v1, LX/EY2;

    const-class v2, LX/EY1;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EY2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2136056
    sget-object v0, LX/EY2;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2136055
    invoke-static {p0}, LX/EY2;->e(LX/EY2;)LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2136054
    invoke-static {}, LX/EY1;->m()LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2136053
    invoke-static {p0}, LX/EY2;->e(LX/EY2;)LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2136052
    sget-object v0, LX/EY2;->c:LX/EY2;

    return-object v0
.end method
