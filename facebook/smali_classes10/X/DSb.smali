.class public final enum LX/DSb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DSb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DSb;

.field public static final enum ADMIN:LX/DSb;

.field public static final enum MODERATOR:LX/DSb;

.field public static final enum NOT_ADMIN:LX/DSb;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1999739
    new-instance v0, LX/DSb;

    const-string v1, "ADMIN"

    invoke-direct {v0, v1, v2}, LX/DSb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSb;->ADMIN:LX/DSb;

    .line 1999740
    new-instance v0, LX/DSb;

    const-string v1, "MODERATOR"

    invoke-direct {v0, v1, v3}, LX/DSb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSb;->MODERATOR:LX/DSb;

    .line 1999741
    new-instance v0, LX/DSb;

    const-string v1, "NOT_ADMIN"

    invoke-direct {v0, v1, v4}, LX/DSb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSb;->NOT_ADMIN:LX/DSb;

    .line 1999742
    const/4 v0, 0x3

    new-array v0, v0, [LX/DSb;

    sget-object v1, LX/DSb;->ADMIN:LX/DSb;

    aput-object v1, v0, v2

    sget-object v1, LX/DSb;->MODERATOR:LX/DSb;

    aput-object v1, v0, v3

    sget-object v1, LX/DSb;->NOT_ADMIN:LX/DSb;

    aput-object v1, v0, v4

    sput-object v0, LX/DSb;->$VALUES:[LX/DSb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1999738
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DSb;
    .locals 1

    .prologue
    .line 1999737
    const-class v0, LX/DSb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DSb;

    return-object v0
.end method

.method public static values()[LX/DSb;
    .locals 1

    .prologue
    .line 1999736
    sget-object v0, LX/DSb;->$VALUES:[LX/DSb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DSb;

    return-object v0
.end method
