.class public final LX/D2q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Sp;


# instance fields
.field public final synthetic a:LX/D2z;


# direct methods
.method public constructor <init>(LX/D2z;)V
    .locals 0

    .prologue
    .line 1959343
    iput-object p1, p0, LX/D2q;->a:LX/D2z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/0wD;
    .locals 1

    .prologue
    .line 1959344
    iget-object v0, p0, LX/D2q;->a:LX/D2z;

    invoke-virtual {v0}, LX/D2z;->g()LX/0wD;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1959345
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v2

    .line 1959346
    iget-object v3, p0, LX/D2q;->a:LX/D2z;

    invoke-virtual {v3, v2}, LX/D2z;->a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 1959347
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1959348
    :cond_0
    :goto_0
    return v0

    .line 1959349
    :cond_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-ne v2, v3, :cond_2

    .line 1959350
    iget-object v0, p0, LX/D2q;->a:LX/D2z;

    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, p1}, LX/D2z;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    goto :goto_0

    .line 1959351
    :cond_2
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    if-ne v2, v3, :cond_3

    .line 1959352
    iget-object v2, p0, LX/D2q;->a:LX/D2z;

    invoke-virtual {v2, p1}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1959353
    goto :goto_0
.end method
