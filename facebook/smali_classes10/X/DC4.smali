.class public LX/DC4;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:LX/DC7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pG;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/widget/LinearLayout;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/widget/LinearLayout;

.field private f:Lcom/facebook/fig/button/FigButton;

.field private g:Lcom/facebook/fig/button/FigButton;

.field private h:Lcom/facebook/fbui/glyph/GlyphView;

.field public i:Landroid/widget/ProgressBar;

.field public j:Lcom/facebook/resources/ui/FbTextView;

.field public final k:LX/DC2;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1973657
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1973658
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1973659
    iput-object v0, p0, LX/DC4;->b:LX/0Ot;

    .line 1973660
    new-instance v0, LX/DC3;

    invoke-direct {v0, p0}, LX/DC3;-><init>(LX/DC4;)V

    iput-object v0, p0, LX/DC4;->k:LX/DC2;

    .line 1973661
    const-class v0, LX/DC4;

    invoke-static {v0, p0}, LX/DC4;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1973662
    const v0, 0x7f030c5f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1973663
    const v0, 0x7f0d1e6f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/DC4;->c:Landroid/widget/LinearLayout;

    .line 1973664
    const v0, 0x7f0d1e71

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/DC4;->d:Landroid/widget/LinearLayout;

    .line 1973665
    const v0, 0x7f0d1e72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/DC4;->e:Landroid/widget/LinearLayout;

    .line 1973666
    const v0, 0x7f0d1e70

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/DC4;->f:Lcom/facebook/fig/button/FigButton;

    .line 1973667
    const v0, 0x7f0d08c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/DC4;->g:Lcom/facebook/fig/button/FigButton;

    .line 1973668
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/DC4;->h:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1973669
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/DC4;->i:Landroid/widget/ProgressBar;

    .line 1973670
    const v0, 0x7f0d17e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DC4;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 1973671
    iget-object v0, p0, LX/DC4;->f:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/DBz;

    invoke-direct {v1, p0}, LX/DBz;-><init>(LX/DC4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1973672
    iget-object v0, p0, LX/DC4;->h:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/DC0;

    invoke-direct {v1, p0}, LX/DC0;-><init>(LX/DC4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1973673
    iget-object v0, p0, LX/DC4;->g:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/DC1;

    invoke-direct {v1, p0}, LX/DC1;-><init>(LX/DC4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1973674
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/DC4;

    invoke-static {v2}, LX/Err;->a(LX/0QB;)LX/Err;

    move-result-object v1

    check-cast v1, LX/DC7;

    const/16 p0, 0x5ec

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, LX/DC4;->a:LX/DC7;

    iput-object v2, p1, LX/DC4;->b:LX/0Ot;

    return-void
.end method

.method public static b(LX/DC4;)V
    .locals 2

    .prologue
    .line 1973675
    invoke-virtual {p0}, LX/DC4;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1973676
    new-instance v1, Lcom/facebook/feed/offlinefeed/header/OfflineFeedHeaderView$4;

    invoke-direct {v1, p0}, Lcom/facebook/feed/offlinefeed/header/OfflineFeedHeaderView$4;-><init>(LX/DC4;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1973677
    return-void
.end method
