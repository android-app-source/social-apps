.class public final enum LX/Ej0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ej0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ej0;

.field public static final enum ANDROID_AUTO_SMS_API:LX/Ej0;

.field public static final enum ANDROID_DIALOG_API:LX/Ej0;

.field public static final enum MOBILE_CONF_EMAIL:LX/Ej0;

.field public static final enum MOBILE_SUBNO:LX/Ej0;

.field public static final enum UNKNOWN:LX/Ej0;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2160718
    new-instance v0, LX/Ej0;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/Ej0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ej0;->UNKNOWN:LX/Ej0;

    .line 2160719
    new-instance v0, LX/Ej0;

    const-string v1, "ANDROID_AUTO_SMS_API"

    invoke-direct {v0, v1, v3}, LX/Ej0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ej0;->ANDROID_AUTO_SMS_API:LX/Ej0;

    .line 2160720
    new-instance v0, LX/Ej0;

    const-string v1, "ANDROID_DIALOG_API"

    invoke-direct {v0, v1, v4}, LX/Ej0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ej0;->ANDROID_DIALOG_API:LX/Ej0;

    .line 2160721
    new-instance v0, LX/Ej0;

    const-string v1, "MOBILE_CONF_EMAIL"

    invoke-direct {v0, v1, v5}, LX/Ej0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ej0;->MOBILE_CONF_EMAIL:LX/Ej0;

    .line 2160722
    new-instance v0, LX/Ej0;

    const-string v1, "MOBILE_SUBNO"

    invoke-direct {v0, v1, v6}, LX/Ej0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ej0;->MOBILE_SUBNO:LX/Ej0;

    .line 2160723
    const/4 v0, 0x5

    new-array v0, v0, [LX/Ej0;

    sget-object v1, LX/Ej0;->UNKNOWN:LX/Ej0;

    aput-object v1, v0, v2

    sget-object v1, LX/Ej0;->ANDROID_AUTO_SMS_API:LX/Ej0;

    aput-object v1, v0, v3

    sget-object v1, LX/Ej0;->ANDROID_DIALOG_API:LX/Ej0;

    aput-object v1, v0, v4

    sget-object v1, LX/Ej0;->MOBILE_CONF_EMAIL:LX/Ej0;

    aput-object v1, v0, v5

    sget-object v1, LX/Ej0;->MOBILE_SUBNO:LX/Ej0;

    aput-object v1, v0, v6

    sput-object v0, LX/Ej0;->$VALUES:[LX/Ej0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2160724
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static safeValueOf(Ljava/lang/String;)LX/Ej0;
    .locals 1

    .prologue
    .line 2160725
    if-nez p0, :cond_0

    sget-object v0, LX/Ej0;->UNKNOWN:LX/Ej0;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/Ej0;->valueOf(Ljava/lang/String;)LX/Ej0;

    move-result-object v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ej0;
    .locals 1

    .prologue
    .line 2160726
    const-class v0, LX/Ej0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ej0;

    return-object v0
.end method

.method public static values()[LX/Ej0;
    .locals 1

    .prologue
    .line 2160727
    sget-object v0, LX/Ej0;->$VALUES:[LX/Ej0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ej0;

    return-object v0
.end method
