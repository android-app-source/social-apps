.class public LX/CpE;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CoX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cnd;",
        ">;",
        "LX/CoX;"
    }
.end annotation


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1936815
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1936816
    const-class v0, LX/CpE;

    invoke-static {v0, p0}, LX/CpE;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936817
    new-instance v0, LX/Cmz;

    new-instance v1, LX/Cn4;

    iget-object v2, p0, LX/CpE;->a:LX/Cju;

    invoke-direct {v1, v2}, LX/Cn4;-><init>(LX/Cju;)V

    invoke-direct {v0, v1, v3, v3, v3}, LX/Cmz;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V

    .line 1936818
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1936819
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CpE;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object p0

    check-cast p0, LX/Cju;

    iput-object p0, p1, LX/CpE;->a:LX/Cju;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 1936822
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 1936821
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 1936820
    return-void
.end method
