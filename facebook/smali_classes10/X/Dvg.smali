.class public LX/Dvg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dvf;


# instance fields
.field private final a:LX/Dvp;


# direct methods
.method public constructor <init>(LX/Dvp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2059110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059111
    iput-object p1, p0, LX/Dvg;->a:LX/Dvp;

    .line 2059112
    return-void
.end method

.method public static a(LX/0QB;)LX/Dvg;
    .locals 1

    .prologue
    .line 2059113
    invoke-static {p0}, LX/Dvg;->b(LX/0QB;)LX/Dvg;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Dvg;
    .locals 2

    .prologue
    .line 2059114
    new-instance v1, LX/Dvg;

    const-class v0, LX/Dvp;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Dvp;

    invoke-direct {v1, v0}, LX/Dvg;-><init>(LX/Dvp;)V

    .line 2059115
    return-object v1
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "LX/Dvi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2059116
    iget-object v0, p0, LX/Dvg;->a:LX/Dvp;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Dvp;->a(Ljava/lang/Boolean;)LX/Dvo;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
