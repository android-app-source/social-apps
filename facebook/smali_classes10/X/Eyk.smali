.class public final LX/Eyk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/util/List",
        "<+",
        "LX/2lr;",
        ">;>;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V
    .locals 0

    .prologue
    .line 2186501
    iput-object p1, p0, LX/Eyk;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iput-boolean p2, p0, LX/Eyk;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2186502
    iget-object v0, p0, LX/Eyk;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-boolean v1, p0, LX/Eyk;->a:Z

    invoke-static {v0, v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->e(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    .line 2186503
    iget-object v0, p0, LX/Eyk;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 2186504
    iget-object v1, v0, LX/2iK;->i:LX/86b;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/2iK;->i:LX/86b;

    .line 2186505
    iget-object v2, v1, LX/86b;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    iget v0, v1, LX/86b;->e:I

    if-ge v2, v0, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2186506
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2186507
    if-eqz v0, :cond_0

    .line 2186508
    iget-object v0, p0, LX/Eyk;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->w(Lcom/facebook/friending/jewel/FriendRequestsFragment;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, LX/Eyk;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->v(Lcom/facebook/friending/jewel/FriendRequestsFragment;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2186509
    :goto_2
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Eyk;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->w(Lcom/facebook/friending/jewel/FriendRequestsFragment;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
