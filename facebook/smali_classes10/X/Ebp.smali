.class public final LX/Ebp;
.super LX/EWp;
.source ""

# interfaces
.implements LX/Ebn;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ebp;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/Ebp;


# instance fields
.field public bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public privateKey_:LX/EWc;

.field public publicKey_:LX/EWc;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2144692
    new-instance v0, LX/Ebm;

    invoke-direct {v0}, LX/Ebm;-><init>()V

    sput-object v0, LX/Ebp;->a:LX/EWZ;

    .line 2144693
    new-instance v0, LX/Ebp;

    invoke-direct {v0}, LX/Ebp;-><init>()V

    .line 2144694
    sput-object v0, LX/Ebp;->c:LX/Ebp;

    invoke-direct {v0}, LX/Ebp;->q()V

    .line 2144695
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2144687
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2144688
    iput-byte v0, p0, LX/Ebp;->memoizedIsInitialized:B

    .line 2144689
    iput v0, p0, LX/Ebp;->memoizedSerializedSize:I

    .line 2144690
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2144691
    iput-object v0, p0, LX/Ebp;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2144657
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2144658
    iput-byte v0, p0, LX/Ebp;->memoizedIsInitialized:B

    .line 2144659
    iput v0, p0, LX/Ebp;->memoizedSerializedSize:I

    .line 2144660
    invoke-direct {p0}, LX/Ebp;->q()V

    .line 2144661
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2144662
    const/4 v0, 0x0

    .line 2144663
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2144664
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2144665
    sparse-switch v3, :sswitch_data_0

    .line 2144666
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2144667
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2144668
    goto :goto_0

    .line 2144669
    :sswitch_1
    iget v3, p0, LX/Ebp;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/Ebp;->bitField0_:I

    .line 2144670
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Ebp;->publicKey_:LX/EWc;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2144671
    :catch_0
    move-exception v0

    .line 2144672
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2144673
    move-object v0, v0

    .line 2144674
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2144675
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/Ebp;->unknownFields:LX/EZQ;

    .line 2144676
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2144677
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/Ebp;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/Ebp;->bitField0_:I

    .line 2144678
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Ebp;->privateKey_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2144679
    :catch_1
    move-exception v0

    .line 2144680
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2144681
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2144682
    move-object v0, v1

    .line 2144683
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2144684
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ebp;->unknownFields:LX/EZQ;

    .line 2144685
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2144686
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2144652
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2144653
    iput-byte v1, p0, LX/Ebp;->memoizedIsInitialized:B

    .line 2144654
    iput v1, p0, LX/Ebp;->memoizedSerializedSize:I

    .line 2144655
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ebp;->unknownFields:LX/EZQ;

    .line 2144656
    return-void
.end method

.method private static a(LX/Ebp;)LX/Ebo;
    .locals 1

    .prologue
    .line 2144651
    invoke-static {}, LX/Ebo;->u()LX/Ebo;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/Ebo;->a(LX/Ebp;)LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 2144648
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebp;->publicKey_:LX/EWc;

    .line 2144649
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebp;->privateKey_:LX/EWc;

    .line 2144650
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2144646
    new-instance v0, LX/Ebo;

    invoke-direct {v0, p1}, LX/Ebo;-><init>(LX/EYd;)V

    .line 2144647
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2144639
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2144640
    iget v0, p0, LX/Ebp;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2144641
    iget-object v0, p0, LX/Ebp;->publicKey_:LX/EWc;

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2144642
    :cond_0
    iget v0, p0, LX/Ebp;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2144643
    iget-object v0, p0, LX/Ebp;->privateKey_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2144644
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2144645
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2144696
    iget-byte v1, p0, LX/Ebp;->memoizedIsInitialized:B

    .line 2144697
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2144698
    :goto_0
    return v0

    .line 2144699
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2144700
    :cond_1
    iput-byte v0, p0, LX/Ebp;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2144622
    iget v0, p0, LX/Ebp;->memoizedSerializedSize:I

    .line 2144623
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2144624
    :goto_0
    return v0

    .line 2144625
    :cond_0
    const/4 v0, 0x0

    .line 2144626
    iget v1, p0, LX/Ebp;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2144627
    iget-object v0, p0, LX/Ebp;->publicKey_:LX/EWc;

    invoke-static {v2, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2144628
    :cond_1
    iget v1, p0, LX/Ebp;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2144629
    iget-object v1, p0, LX/Ebp;->privateKey_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2144630
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2144631
    iput v0, p0, LX/Ebp;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2144638
    iget-object v0, p0, LX/Ebp;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2144637
    sget-object v0, LX/Eck;->t:LX/EYn;

    const-class v1, LX/Ebp;

    const-class v2, LX/Ebo;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ebp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2144636
    sget-object v0, LX/Ebp;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2144635
    invoke-static {p0}, LX/Ebp;->a(LX/Ebp;)LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2144634
    invoke-static {}, LX/Ebo;->u()LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2144633
    invoke-static {p0}, LX/Ebp;->a(LX/Ebp;)LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2144632
    sget-object v0, LX/Ebp;->c:LX/Ebp;

    return-object v0
.end method
