.class public final LX/Dj3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BmO;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V
    .locals 0

    .prologue
    .line 2032843
    iput-object p1, p0, LX/Dj3;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 3

    .prologue
    .line 2032831
    iget-object v0, p0, LX/Dj3;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-static {v0}, LX/Djn;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)LX/Djn;

    move-result-object v0

    .line 2032832
    iput-object p1, v0, LX/Djn;->p:Ljava/util/Calendar;

    .line 2032833
    iget-object v1, p0, LX/Dj3;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v0}, LX/Djn;->a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    move-result-object v0

    .line 2032834
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    .line 2032835
    iget-object v0, p0, LX/Dj3;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    iget-object v1, p0, LX/Dj3;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)V

    .line 2032836
    if-eqz p1, :cond_0

    .line 2032837
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 2032838
    const/16 v1, 0xa

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 2032839
    iget-object v1, p0, LX/Dj3;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    .line 2032840
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->l:Ljava/util/Calendar;

    .line 2032841
    iget-object v0, p0, LX/Dj3;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2032842
    :cond_0
    return-void
.end method
