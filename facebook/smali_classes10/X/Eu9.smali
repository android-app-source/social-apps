.class public final LX/Eu9;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Eu9;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Eu7;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/EuA;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2179107
    const/4 v0, 0x0

    sput-object v0, LX/Eu9;->a:LX/Eu9;

    .line 2179108
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Eu9;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2179109
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2179110
    new-instance v0, LX/EuA;

    invoke-direct {v0}, LX/EuA;-><init>()V

    iput-object v0, p0, LX/Eu9;->c:LX/EuA;

    .line 2179111
    return-void
.end method

.method public static c(LX/1De;)LX/Eu7;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2179112
    new-instance v1, LX/Eu8;

    invoke-direct {v1}, LX/Eu8;-><init>()V

    .line 2179113
    sget-object v2, LX/Eu9;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Eu7;

    .line 2179114
    if-nez v2, :cond_0

    .line 2179115
    new-instance v2, LX/Eu7;

    invoke-direct {v2}, LX/Eu7;-><init>()V

    .line 2179116
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Eu7;->a$redex0(LX/Eu7;LX/1De;IILX/Eu8;)V

    .line 2179117
    move-object v1, v2

    .line 2179118
    move-object v0, v1

    .line 2179119
    return-object v0
.end method

.method public static declared-synchronized q()LX/Eu9;
    .locals 2

    .prologue
    .line 2179120
    const-class v1, LX/Eu9;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Eu9;->a:LX/Eu9;

    if-nez v0, :cond_0

    .line 2179121
    new-instance v0, LX/Eu9;

    invoke-direct {v0}, LX/Eu9;-><init>()V

    sput-object v0, LX/Eu9;->a:LX/Eu9;

    .line 2179122
    :cond_0
    sget-object v0, LX/Eu9;->a:LX/Eu9;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2179123
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2179124
    check-cast p2, LX/Eu8;

    .line 2179125
    iget-object v0, p2, LX/Eu8;->a:Ljava/lang/CharSequence;

    iget v1, p2, LX/Eu8;->b:I

    iget v2, p2, LX/Eu8;->c:I

    const/4 p2, 0x1

    .line 2179126
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/1ne;->a(Landroid/content/res/ColorStateList;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->a(Z)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2179127
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2179128
    invoke-static {}, LX/1dS;->b()V

    .line 2179129
    const/4 v0, 0x0

    return-object v0
.end method
