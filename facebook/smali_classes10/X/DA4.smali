.class public LX/DA4;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rr",
        "<",
        "Lcom/facebook/contactlogs/data/ContactLogMetadata;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:LX/DA2;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;LX/DA2;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/DA2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970777
    invoke-direct {p0}, LX/0Rr;-><init>()V

    .line 1970778
    iput-object p1, p0, LX/DA4;->a:Landroid/database/Cursor;

    .line 1970779
    iput-object p2, p0, LX/DA4;->b:LX/DA2;

    .line 1970780
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1970781
    iget-object v0, p0, LX/DA4;->b:LX/DA2;

    invoke-virtual {v0}, LX/DA2;->a()LX/DA1;

    move-result-object v0

    .line 1970782
    :cond_0
    iget-object v1, p0, LX/DA4;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1970783
    sget-object v1, LX/DA1;->CALL_LOG:LX/DA1;

    invoke-virtual {v0, v1}, LX/DA1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, LX/DA1;->MMS_LOG:LX/DA1;

    invoke-virtual {v0, v1}, LX/DA1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, LX/DA1;->SMS_LOG:LX/DA1;

    invoke-virtual {v0, v1}, LX/DA1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1970784
    :cond_1
    iget-object v0, p0, LX/DA4;->b:LX/DA2;

    iget-object v1, p0, LX/DA4;->a:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, LX/DA2;->a(Landroid/database/Cursor;)Lcom/facebook/contactlogs/data/ContactLogMetadata;

    move-result-object v0

    .line 1970785
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contactlogs/data/ContactLogMetadata;

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1970786
    iget-object v0, p0, LX/DA4;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1970787
    return-void
.end method
