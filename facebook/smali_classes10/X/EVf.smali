.class public LX/EVf;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field private final a:I

.field private b:Landroid/content/Context;

.field public c:Z

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field private g:Ljava/lang/String;

.field public h:Z

.field public i:Landroid/graphics/drawable/Drawable;

.field public j:I

.field public k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128669
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EVf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128670
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 2128671
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128672
    iput v2, p0, LX/EVf;->a:I

    .line 2128673
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EVf;->h:Z

    .line 2128674
    const v0, 0x7f0315c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2128675
    iput-object p1, p0, LX/EVf;->b:Landroid/content/Context;

    .line 2128676
    const v0, 0x7f0d3117

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EVf;->d:Landroid/view/View;

    .line 2128677
    const v0, 0x7f0d19bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/EVf;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2128678
    const v0, 0x7f0d3118

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/EVf;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2128679
    iget-object v0, p0, LX/EVf;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021a71

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/EVf;->i:Landroid/graphics/drawable/Drawable;

    .line 2128680
    iget-object v0, p0, LX/EVf;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/EVf;->j:I

    .line 2128681
    iget-object v0, p0, LX/EVf;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/EVf;->k:I

    .line 2128682
    invoke-virtual {p0}, LX/EVf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082241

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EVf;->g:Ljava/lang/String;

    .line 2128683
    iget-object v0, p0, LX/EVf;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EVf;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 2128684
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EVf;->h:Z

    .line 2128685
    iget-object v0, p0, LX/EVf;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/EVf;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2128686
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2128687
    iget-boolean v0, p0, LX/EVf;->c:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1f883219

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128688
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2128689
    const/4 v1, 0x1

    .line 2128690
    iput-boolean v1, p0, LX/EVf;->c:Z

    .line 2128691
    const/16 v1, 0x2d

    const v2, 0x467c0015

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x78222c3a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128692
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2128693
    const/4 v1, 0x0

    .line 2128694
    iput-boolean v1, p0, LX/EVf;->c:Z

    .line 2128695
    const/16 v1, 0x2d

    const v2, -0x2afb369b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
