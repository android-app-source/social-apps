.class public LX/Doe;
.super LX/Dod;
.source ""


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042237
    const-string v0, "keychain"

    sget-object v1, LX/3S0;->a:LX/0U1;

    sget-object v2, LX/3S0;->b:LX/0U1;

    invoke-direct {p0, p1, v0, v1, v2}, LX/Dod;-><init>(LX/0Or;Ljava/lang/String;LX/0U1;LX/0U1;)V

    .line 2042238
    return-void
.end method


# virtual methods
.method public final a(LX/2PC;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2042239
    invoke-virtual {p0, p1}, LX/Dod;->b(LX/2PC;)[B

    move-result-object v1

    .line 2042240
    if-nez v1, :cond_0

    .line 2042241
    const/4 v0, 0x0

    .line 2042242
    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2042243
    :catch_0
    move-exception v0

    .line 2042244
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to decode key from keychain"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(LX/2PC;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2042245
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/Dod;->a(LX/2PC;[B)V

    .line 2042246
    return-void
.end method
