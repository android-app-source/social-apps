.class public LX/E3F;
.super LX/1V8;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/1Uc;

.field public static final b:LX/1Uc;

.field private static e:LX/0Xm;


# instance fields
.field private final c:LX/1Ub;

.field private final d:LX/1Ub;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x41400000    # 12.0f

    const/4 v1, 0x0

    .line 2073758
    new-instance v0, LX/1Ue;

    invoke-direct {v0, v2, v2}, LX/1Ue;-><init>(FF)V

    sput-object v0, LX/E3F;->a:LX/1Uc;

    .line 2073759
    new-instance v0, LX/1Ue;

    invoke-direct {v0, v1, v1}, LX/1Ue;-><init>(FF)V

    sput-object v0, LX/E3F;->b:LX/1Uc;

    return-void
.end method

.method public constructor <init>(LX/14w;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v3, 0x41100000    # 9.0f

    const/4 v2, 0x0

    .line 2073760
    invoke-direct {p0, p1}, LX/1V8;-><init>(LX/14w;)V

    .line 2073761
    new-instance v0, LX/1Ub;

    sget-object v1, LX/E3F;->b:LX/1Uc;

    invoke-direct {v0, v2, v3, v2, v1}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iput-object v0, p0, LX/E3F;->c:LX/1Ub;

    .line 2073762
    new-instance v0, LX/1Ub;

    sget-object v1, LX/E3F;->b:LX/1Uc;

    invoke-direct {v0, v3, v2, v2, v1}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    iput-object v0, p0, LX/E3F;->d:LX/1Ub;

    .line 2073763
    return-void
.end method

.method public static a(LX/0QB;)LX/E3F;
    .locals 4

    .prologue
    .line 2073764
    const-class v1, LX/E3F;

    monitor-enter v1

    .line 2073765
    :try_start_0
    sget-object v0, LX/E3F;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073766
    sput-object v2, LX/E3F;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073767
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073768
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073769
    new-instance p0, LX/E3F;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    invoke-direct {p0, v3}, LX/E3F;-><init>(LX/14w;)V

    .line 2073770
    move-object v0, p0

    .line 2073771
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073772
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/E3F;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073773
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1Ua;LX/1X9;I)LX/1Ub;
    .locals 5

    .prologue
    .line 2073775
    invoke-virtual {p0}, LX/1V8;->b()Ljava/util/EnumMap;

    move-result-object v0

    .line 2073776
    iget-object v1, p1, LX/1Ua;->s:LX/1UZ;

    move-object v1, v1

    .line 2073777
    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ub;

    .line 2073778
    const-string v1, "Cannot resolve style %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2073779
    iget-object v4, p1, LX/1Ua;->s:LX/1UZ;

    move-object v4, v4

    .line 2073780
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073781
    iget-object v1, p1, LX/1Ua;->t:LX/1Ub;

    move-object v1, v1

    .line 2073782
    invoke-virtual {v1, v0}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    .line 2073783
    sget-object v1, LX/E3E;->a:[I

    invoke-virtual {p2}, LX/1X9;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2073784
    :goto_0
    return-object v0

    .line 2073785
    :pswitch_0
    iget-object v1, p0, LX/E3F;->d:LX/1Ub;

    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    goto :goto_0

    .line 2073786
    :pswitch_1
    iget-object v1, p0, LX/E3F;->c:LX/1Ub;

    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    goto :goto_0

    .line 2073787
    :pswitch_2
    iget-object v1, p0, LX/E3F;->d:LX/1Ub;

    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    iget-object v1, p0, LX/E3F;->c:LX/1Ub;

    invoke-virtual {v0, v1}, LX/1Ub;->a(LX/1Ub;)LX/1Ub;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()Ljava/util/EnumMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap",
            "<",
            "LX/1UZ;",
            "LX/1Ub;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v5, 0x41100000    # 9.0f

    const/4 v4, 0x0

    .line 2073788
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/1UZ;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 2073789
    sget-object v1, LX/1UZ;->DEFAULT:LX/1UZ;

    new-instance v2, LX/1Ub;

    sget-object v3, LX/E3F;->a:LX/1Uc;

    invoke-direct {v2, v5, v5, v4, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073790
    sget-object v1, LX/1UZ;->ZERO:LX/1UZ;

    new-instance v2, LX/1Ub;

    sget-object v3, LX/E3F;->a:LX/1Uc;

    invoke-direct {v2, v4, v4, v4, v3}, LX/1Ub;-><init>(FFFLX/1Uc;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2073791
    return-object v0
.end method

.method public final b(LX/1Ua;LX/1X9;I)LX/1Ub;
    .locals 1

    .prologue
    .line 2073792
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
