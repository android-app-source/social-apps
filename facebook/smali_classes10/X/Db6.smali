.class public LX/Db6;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/Db0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/CompoundButton;

.field public d:Z

.field public e:LX/DZN;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2016751
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2016752
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Db6;->d:Z

    .line 2016753
    const/4 v0, 0x0

    iput-object v0, p0, LX/Db6;->e:LX/DZN;

    .line 2016754
    const-class v0, LX/Db6;

    invoke-static {v0, p0}, LX/Db6;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2016755
    iget-object v0, p0, LX/Db6;->a:LX/Db0;

    invoke-virtual {v0, p1}, LX/Db0;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03145b

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2016756
    const v0, 0x7f0d092b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Db6;->b:Landroid/widget/TextView;

    .line 2016757
    const v0, 0x7f0d2e65

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2016758
    const v2, 0x7f031003

    move v1, v2

    .line 2016759
    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2016760
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2016761
    const v0, 0x7f0d2e66

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, LX/Db6;->c:Landroid/widget/CompoundButton;

    .line 2016762
    iget-object v0, p0, LX/Db6;->c:Landroid/widget/CompoundButton;

    new-instance v1, LX/Db4;

    invoke-direct {v1, p0}, LX/Db4;-><init>(LX/Db6;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2016763
    new-instance v0, LX/Db5;

    invoke-direct {v0, p0}, LX/Db5;-><init>(LX/Db6;)V

    invoke-virtual {p0, v0}, LX/Db6;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2016764
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Db6;

    invoke-static {p0}, LX/Db0;->a(LX/0QB;)LX/Db0;

    move-result-object p0

    check-cast p0, LX/Db0;

    iput-object p0, p1, LX/Db6;->a:LX/Db0;

    return-void
.end method


# virtual methods
.method public setDelegate(LX/DZN;)V
    .locals 2

    .prologue
    .line 2016765
    if-eqz p1, :cond_0

    .line 2016766
    iput-object p1, p0, LX/Db6;->e:LX/DZN;

    .line 2016767
    iget-object v0, p0, LX/Db6;->b:Landroid/widget/TextView;

    iget-object v1, p0, LX/Db6;->e:LX/DZN;

    invoke-interface {v1}, LX/DZN;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2016768
    iget-object v0, p0, LX/Db6;->c:Landroid/widget/CompoundButton;

    iget-object v1, p0, LX/Db6;->e:LX/DZN;

    invoke-interface {v1}, LX/DZN;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2016769
    iget-object v0, p0, LX/Db6;->e:LX/DZN;

    invoke-interface {v0}, LX/DZN;->b()Z

    move-result v0

    .line 2016770
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Db6;->d:Z

    .line 2016771
    iget-object v1, p0, LX/Db6;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 2016772
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Db6;->d:Z

    .line 2016773
    :cond_0
    return-void
.end method
