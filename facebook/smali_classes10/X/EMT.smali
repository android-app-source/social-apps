.class public final LX/EMT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8d1;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:LX/CzL;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/8d1;LX/1Pn;LX/CzL;)V
    .locals 0

    .prologue
    .line 2110741
    iput-object p1, p0, LX/EMT;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iput-object p2, p0, LX/EMT;->a:LX/8d1;

    iput-object p3, p0, LX/EMT;->b:LX/1Pn;

    iput-object p4, p0, LX/EMT;->c:LX/CzL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 17

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const v3, -0x4888a881

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 2110731
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->a:LX/8d1;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->a:LX/8d1;

    invoke-interface {v1}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2110732
    :cond_0
    const/4 v1, 0x2

    const/4 v2, 0x2

    const v3, 0x6501c90a

    invoke-static {v1, v2, v3, v10}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2110733
    :goto_0
    return-void

    .line 2110734
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->a:LX/8d1;

    invoke-interface {v1}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMT;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/0ax;->bY:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMT;->a:LX/8d1;

    invoke-interface {v2}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v9, v1

    .line 2110735
    :goto_1
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 2110736
    const-string v1, "profile_name"

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMT;->a:LX/8d1;

    invoke-interface {v2}, LX/8d1;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2110737
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, LX/CvY;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v12

    sget-object v13, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v14, LX/7CM;->INLINE_PHOTOS_LINK:LX/7CM;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->b:LX/1Pn;

    check-cast v1, LX/CxP;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMT;->c:LX/CzL;

    invoke-interface {v1, v2}, LX/CxP;->b(LX/CzL;)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->c:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v1}, LX/CvY;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CvV;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v3, LX/7CM;->INLINE_PHOTOS_LINK:LX/7CM;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/EMT;->a:LX/8d1;

    invoke-interface {v4}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, LX/EMT;->a:LX/8d1;

    invoke-interface {v6}, LX/8d1;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    move-object v1, v8

    move-object v2, v12

    move-object v3, v13

    move-object v4, v14

    move v5, v15

    move-object/from16 v6, v16

    invoke-virtual/range {v1 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2110738
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EMT;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17W;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMT;->b:LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v9, v11, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2110739
    const v1, -0x50235357

    invoke-static {v1, v10}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2110740
    :cond_2
    sget-object v1, LX/0ax;->bZ:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EMT;->a:LX/8d1;

    invoke-interface {v2}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v9, v1

    goto/16 :goto_1
.end method
