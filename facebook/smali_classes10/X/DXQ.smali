.class public final LX/DXQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;)V
    .locals 0

    .prologue
    .line 2010206
    iput-object p1, p0, LX/DXQ;->a:Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2010207
    iget-object v0, p0, LX/DXQ;->a:Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083096

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2010208
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2010209
    check-cast p1, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010210
    iget-object v0, p0, LX/DXQ;->a:Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;

    .line 2010211
    iget-object v1, v0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->d:LX/1dV;

    if-nez v1, :cond_0

    .line 2010212
    :goto_0
    return-void

    .line 2010213
    :cond_0
    iget-object v1, v0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->d:LX/1dV;

    iget-object v2, v0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->a:LX/DXJ;

    iget-object p0, v0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->d:LX/1dV;

    .line 2010214
    iget-object v0, p0, LX/1dV;->k:LX/1De;

    move-object p0, v0

    .line 2010215
    invoke-virtual {v2, p0}, LX/DXJ;->c(LX/1De;)LX/DXH;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/DXH;->a(Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;)LX/DXH;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1dV;->a(LX/1X1;)V

    goto :goto_0
.end method
