.class public LX/Eb1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/Eb2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2142848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142849
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Eb1;->a:Ljava/util/LinkedList;

    .line 2142850
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    .line 2142851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142852
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Eb1;->a:Ljava/util/LinkedList;

    .line 2142853
    sget-object v0, LX/Ec1;->a:LX/EWZ;

    invoke-virtual {v0, p1}, LX/EWZ;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ec1;

    move-object v0, v0

    .line 2142854
    iget-object v1, v0, LX/Ec1;->senderKeyStates_:Ljava/util/List;

    move-object v0, v1

    .line 2142855
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcH;

    .line 2142856
    iget-object v2, p0, LX/Eb1;->a:Ljava/util/LinkedList;

    new-instance v3, LX/Eb2;

    invoke-direct {v3, v0}, LX/Eb2;-><init>(LX/EcH;)V

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2142857
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)LX/Eb2;
    .locals 3

    .prologue
    .line 2142858
    iget-object v0, p0, LX/Eb1;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eb2;

    .line 2142859
    invoke-virtual {v0}, LX/Eb2;->a()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 2142860
    return-object v0

    .line 2142861
    :cond_1
    new-instance v0, LX/Eah;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No keys for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eah;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2142862
    iget-object v0, p0, LX/Eb1;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final b()LX/Eb2;
    .locals 2

    .prologue
    .line 2142863
    iget-object v0, p0, LX/Eb1;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2142864
    iget-object v0, p0, LX/Eb1;->a:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eb2;

    return-object v0

    .line 2142865
    :cond_0
    new-instance v0, LX/Eah;

    const-string v1, "No key state in record!"

    invoke-direct {v0, v1}, LX/Eah;-><init>(Ljava/lang/String;)V

    throw v0
.end method
