.class public LX/ElB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public volatile a:LX/ElA;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2164105
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 2164106
    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    .line 2164107
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v1, v0}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-interface {p1, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2164108
    iget-object v0, p0, LX/ElB;->a:LX/ElA;

    .line 2164109
    iput-object p1, v0, LX/ElA;->b:Lorg/apache/http/HttpResponse;

    .line 2164110
    invoke-virtual {v0, p1}, LX/El5;->a(Lorg/apache/http/HttpMessage;)V

    .line 2164111
    iget-object v1, v0, LX/ElA;->a:LX/ElE;

    invoke-virtual {v1}, LX/ElE;->a()V

    .line 2164112
    iget-object v0, p0, LX/ElB;->a:LX/ElA;

    .line 2164113
    iget-object v1, v0, LX/ElA;->a:LX/ElE;

    invoke-virtual {v1}, LX/ElE;->c()V

    .line 2164114
    const/4 v0, 0x0

    return-object v0
.end method
