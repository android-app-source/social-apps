.class public final enum LX/ChZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ChZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ChZ;

.field public static final enum NA:LX/ChZ;

.field public static final enum OFF:LX/ChZ;

.field public static final enum ON:LX/ChZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1927935
    new-instance v0, LX/ChZ;

    const-string v1, "ON"

    invoke-direct {v0, v1, v3, v4}, LX/ChZ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/ChZ;->ON:LX/ChZ;

    .line 1927936
    new-instance v0, LX/ChZ;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v4, v3}, LX/ChZ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/ChZ;->OFF:LX/ChZ;

    .line 1927937
    new-instance v0, LX/ChZ;

    const-string v1, "NA"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, LX/ChZ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/ChZ;->NA:LX/ChZ;

    .line 1927938
    const/4 v0, 0x3

    new-array v0, v0, [LX/ChZ;

    sget-object v1, LX/ChZ;->ON:LX/ChZ;

    aput-object v1, v0, v3

    sget-object v1, LX/ChZ;->OFF:LX/ChZ;

    aput-object v1, v0, v4

    sget-object v1, LX/ChZ;->NA:LX/ChZ;

    aput-object v1, v0, v5

    sput-object v0, LX/ChZ;->$VALUES:[LX/ChZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1927939
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1927940
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ChZ;
    .locals 1

    .prologue
    .line 1927941
    const-class v0, LX/ChZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ChZ;

    return-object v0
.end method

.method public static values()[LX/ChZ;
    .locals 1

    .prologue
    .line 1927942
    sget-object v0, LX/ChZ;->$VALUES:[LX/ChZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ChZ;

    return-object v0
.end method
