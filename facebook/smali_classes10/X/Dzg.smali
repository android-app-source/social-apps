.class public final LX/Dzg;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0am",
        "<+",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 0

    .prologue
    .line 2067120
    iput-object p1, p0, LX/Dzg;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2067121
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2067122
    check-cast p1, LX/0am;

    .line 2067123
    iget-object v0, p0, LX/Dzg;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    .line 2067124
    iput-object p1, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->t:LX/0am;

    .line 2067125
    iget-object v0, p0, LX/Dzg;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->t:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dzg;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067126
    iget-object v1, p0, LX/Dzg;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, p0, LX/Dzg;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->t:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {v1, v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->a$redex0(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2067127
    :cond_0
    return-void
.end method
