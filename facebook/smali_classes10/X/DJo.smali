.class public final LX/DJo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/widget/LinearLayout;

.field public final synthetic b:Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 1985866
    iput-object p1, p0, LX/DJo;->b:Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;

    iput-object p2, p0, LX/DJo;->a:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 1985867
    iget-object v0, p0, LX/DJo;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1985868
    new-instance v0, LX/4KN;

    invoke-direct {v0}, LX/4KN;-><init>()V

    iget-object v1, p0, LX/DJo;->b:Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;

    iget-object v1, v1, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->u:Ljava/lang/String;

    .line 1985869
    const-string v2, "group_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1985870
    move-object v0, v0

    .line 1985871
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1985872
    const-string v2, "seen_marketplace_cross_post_intercept"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1985873
    move-object v0, v0

    .line 1985874
    new-instance v1, LX/9Ke;

    invoke-direct {v1}, LX/9Ke;-><init>()V

    move-object v1, v1

    .line 1985875
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1985876
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1985877
    iget-object v1, p0, LX/DJo;->b:Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;

    iget-object v1, v1, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->r:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1985878
    return-void
.end method
