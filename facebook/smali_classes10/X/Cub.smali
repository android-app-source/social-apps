.class public LX/Cub;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:LX/0wT;

.field private static final h:Ljava/lang/Class;


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:LX/0wd;

.field private final f:LX/CuZ;

.field private final g:LX/Cua;

.field public i:LX/Cud;

.field public j:LX/Cuu;

.field public k:LX/2pM;

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Landroid/os/Handler;

.field private p:Ljava/lang/Runnable;

.field public q:Ljava/lang/Runnable;

.field public r:LX/Cuw;

.field public s:LX/CuK;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1946934
    sget-wide v0, LX/CoL;->F:D

    sget-wide v2, LX/CoL;->G:D

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/Cub;->d:LX/0wT;

    .line 1946935
    const-class v0, LX/Cub;

    sput-object v0, LX/Cub;->h:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Ctg;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1946919
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1946920
    new-instance v0, LX/Cua;

    invoke-direct {v0, p0}, LX/Cua;-><init>(LX/Cub;)V

    iput-object v0, p0, LX/Cub;->g:LX/Cua;

    .line 1946921
    const-class v0, LX/Cub;

    invoke-static {v0, p0}, LX/Cub;->a(Ljava/lang/Class;LX/02k;)V

    .line 1946922
    iget-object v0, p0, LX/Cub;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/Cub;->d:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1946923
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1946924
    move-object v0, v0

    .line 1946925
    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Cub;->e:LX/0wd;

    .line 1946926
    new-instance v0, LX/CuZ;

    invoke-direct {v0, p0}, LX/CuZ;-><init>(LX/Cub;)V

    iput-object v0, p0, LX/Cub;->f:LX/CuZ;

    .line 1946927
    iget-object v0, p0, LX/Cub;->e:LX/0wd;

    iget-object v1, p0, LX/Cub;->f:LX/CuZ;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1946928
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/Cub;->o:Landroid/os/Handler;

    .line 1946929
    new-instance v0, Lcom/facebook/richdocument/view/widget/media/plugins/VideoControlsPlugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/view/widget/media/plugins/VideoControlsPlugin$1;-><init>(LX/Cub;)V

    iput-object v0, p0, LX/Cub;->p:Ljava/lang/Runnable;

    .line 1946930
    iget-object v0, p0, LX/Cub;->c:LX/0Uh;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Cub;->n:Z

    .line 1946931
    iget-boolean v0, p0, LX/Cub;->n:Z

    if-eqz v0, :cond_0

    .line 1946932
    new-instance v0, Lcom/facebook/richdocument/view/widget/media/plugins/VideoControlsPlugin$2;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/view/widget/media/plugins/VideoControlsPlugin$2;-><init>(LX/Cub;)V

    iput-object v0, p0, LX/Cub;->q:Ljava/lang/Runnable;

    .line 1946933
    :cond_0
    return-void
.end method

.method private static a(LX/Cub;Z)V
    .locals 2

    .prologue
    .line 1946913
    iget-object v0, p0, LX/Cub;->k:LX/2pM;

    if-eqz v0, :cond_0

    .line 1946914
    iget-object v0, p0, LX/Cub;->i:LX/Cud;

    sget-object v1, LX/Cui;->NONE:LX/Cui;

    invoke-virtual {v0, v1, p1}, LX/Cud;->a(LX/Cui;Z)V

    .line 1946915
    iget-object v0, p0, LX/Cub;->f:LX/CuZ;

    sget-object v1, LX/Cui;->NONE:LX/Cui;

    invoke-virtual {v0, v1, p1}, LX/CuZ;->a(LX/Cui;Z)V

    .line 1946916
    :goto_0
    return-void

    .line 1946917
    :cond_0
    iget-object v0, p0, LX/Cub;->i:LX/Cud;

    sget-object v1, LX/Cui;->PLAY_ICON:LX/Cui;

    invoke-virtual {v0, v1, p1}, LX/Cud;->a(LX/Cui;Z)V

    .line 1946918
    iget-object v0, p0, LX/Cub;->f:LX/CuZ;

    sget-object v1, LX/Cui;->PLAY_ICON:LX/Cui;

    invoke-virtual {v0, v1, p1}, LX/CuZ;->a(LX/Cui;Z)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cub;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v2

    check-cast v2, LX/0wW;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v1, p1, LX/Cub;->a:LX/Cju;

    iput-object v2, p1, LX/Cub;->b:LX/0wW;

    iput-object p0, p1, LX/Cub;->c:LX/0Uh;

    return-void
.end method

.method public static a$redex0(LX/Cub;F)V
    .locals 7

    .prologue
    .line 1946891
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946892
    invoke-interface {v0}, LX/Ctg;->getCurrentLayout()LX/CrS;

    move-result-object v2

    .line 1946893
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946894
    invoke-interface {v0}, LX/Ctg;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    .line 1946895
    invoke-virtual {p0}, LX/Cub;->a()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v3

    .line 1946896
    invoke-virtual {v0}, LX/Cs7;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1946897
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v1

    sget-object v5, LX/CrQ;->FADES_WITH_CONTROLS:LX/CrQ;

    const-class v6, LX/CqZ;

    invoke-interface {v2, v1, v5, v6}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v1

    check-cast v1, LX/CqZ;

    .line 1946898
    iget-object v5, v1, LX/CqZ;->a:Ljava/lang/Boolean;

    move-object v1, v5

    .line 1946899
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1946900
    if-eqz v1, :cond_0

    .line 1946901
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v1

    .line 1946902
    instance-of v0, v1, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    if-eqz v0, :cond_5

    sget-object v0, LX/2qV;->PAUSED:LX/2qV;

    if-eq v3, v0, :cond_1

    sget-object v0, LX/2qV;->PREPARED:LX/2qV;

    if-eq v3, v0, :cond_1

    sget-object v0, LX/2qV;->ERROR:LX/2qV;

    if-eq v3, v0, :cond_1

    sget-object v0, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v3, v0, :cond_5

    .line 1946903
    :cond_1
    const/4 v0, 0x0

    .line 1946904
    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1946905
    invoke-interface {v2, v1}, LX/CrS;->a(Landroid/view/View;)LX/CrR;

    move-result-object v5

    .line 1946906
    if-eqz v5, :cond_2

    .line 1946907
    new-instance v6, LX/CrV;

    invoke-direct {v6, v0}, LX/CrV;-><init>(F)V

    invoke-virtual {v5, v6}, LX/CrR;->a(LX/CqY;)V

    .line 1946908
    :cond_2
    sget v5, LX/CoL;->s:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_3

    .line 1946909
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1946910
    :goto_2
    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 1946911
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 1946912
    :cond_4
    return-void

    :cond_5
    move v0, p1

    goto :goto_1
.end method

.method public static a$redex0(LX/Cub;LX/Cuj;LX/Cuk;Z)V
    .locals 12

    .prologue
    const-wide/16 v10, 0xbb8

    const-wide/16 v8, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/4 v4, 0x0

    .line 1946846
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946847
    if-eqz v0, :cond_0

    .line 1946848
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946849
    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1946850
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946851
    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1946852
    :cond_0
    :goto_0
    return-void

    .line 1946853
    :cond_1
    iget-object v0, p2, LX/Cuk;->a:LX/Cqu;

    move-object v0, v0

    .line 1946854
    iget-boolean v1, p2, LX/Cuk;->b:Z

    move v1, v1

    .line 1946855
    iget-boolean v2, p2, LX/Cuk;->c:Z

    move v2, v2

    .line 1946856
    iget-boolean v3, p0, LX/Cub;->n:Z

    if-nez v3, :cond_4

    .line 1946857
    if-eqz v2, :cond_3

    .line 1946858
    if-eqz v1, :cond_2

    .line 1946859
    iget-object v1, p0, LX/Cub;->i:LX/Cud;

    sget-object v2, LX/Cui;->PAUSE_ICON:LX/Cui;

    invoke-virtual {v1, v2, v4}, LX/Cud;->a(LX/Cui;Z)V

    .line 1946860
    iget-object v1, p0, LX/Cub;->f:LX/CuZ;

    sget-object v2, LX/Cui;->PAUSE_ICON:LX/Cui;

    invoke-virtual {v1, v2, v4}, LX/CuZ;->a(LX/Cui;Z)V

    .line 1946861
    iget-object v1, p0, LX/Cub;->o:Landroid/os/Handler;

    iget-object v2, p0, LX/Cub;->p:Ljava/lang/Runnable;

    const v3, -0x2b0bc52

    invoke-static {v1, v2, v10, v11, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1946862
    :goto_1
    iget-object v1, p0, LX/Cub;->e:LX/0wd;

    invoke-virtual {v1, v6, v7}, LX/0wd;->b(D)LX/0wd;

    .line 1946863
    :goto_2
    if-eqz p3, :cond_a

    sget-object v1, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    if-ne p1, v1, :cond_a

    .line 1946864
    const/4 v1, 0x0

    .line 1946865
    :goto_3
    move v1, v1

    .line 1946866
    if-eqz v1, :cond_0

    .line 1946867
    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v1

    .line 1946868
    iget-object v2, v1, LX/Cqw;->e:LX/Cqu;

    move-object v1, v2

    .line 1946869
    invoke-virtual {v1, v0}, LX/Cqu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1946870
    sget-object v1, LX/Cqu;->COLLAPSED:LX/Cqu;

    if-ne v0, v1, :cond_9

    .line 1946871
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946872
    sget-object v1, LX/Cqw;->a:LX/Cqw;

    invoke-interface {v0, v1}, LX/Ctg;->a(LX/Cqw;)V

    goto :goto_0

    .line 1946873
    :cond_2
    iget-object v1, p0, LX/Cub;->i:LX/Cud;

    sget-object v2, LX/Cui;->PLAY_ICON:LX/Cui;

    invoke-virtual {v1, v2, v4}, LX/Cud;->a(LX/Cui;Z)V

    .line 1946874
    iget-object v1, p0, LX/Cub;->f:LX/CuZ;

    sget-object v2, LX/Cui;->PLAY_ICON:LX/Cui;

    invoke-virtual {v1, v2, v4}, LX/CuZ;->a(LX/Cui;Z)V

    goto :goto_1

    .line 1946875
    :cond_3
    iget-object v1, p0, LX/Cub;->f:LX/CuZ;

    sget-object v2, LX/Cui;->NONE:LX/Cui;

    invoke-virtual {v1, v2, v4}, LX/CuZ;->a(LX/Cui;Z)V

    .line 1946876
    iget-object v1, p0, LX/Cub;->e:LX/0wd;

    invoke-virtual {v1, v8, v9}, LX/0wd;->b(D)LX/0wd;

    goto :goto_2

    .line 1946877
    :cond_4
    iget-object v3, p0, LX/Cub;->r:LX/Cuw;

    invoke-virtual {v3}, LX/Cuw;->d()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, LX/Cub;->r:LX/Cuw;

    invoke-virtual {v3}, LX/Cuw;->c()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1946878
    :cond_5
    const/4 v1, 0x1

    invoke-static {p0, v1}, LX/Cub;->a(LX/Cub;Z)V

    .line 1946879
    iget-object v1, p0, LX/Cub;->e:LX/0wd;

    invoke-virtual {v1, v6, v7}, LX/0wd;->b(D)LX/0wd;

    goto :goto_2

    .line 1946880
    :cond_6
    if-eqz v2, :cond_8

    .line 1946881
    if-eqz v1, :cond_7

    .line 1946882
    iget-object v1, p0, LX/Cub;->i:LX/Cud;

    sget-object v2, LX/Cui;->PAUSE_ICON:LX/Cui;

    invoke-virtual {v1, v2, v4}, LX/Cud;->a(LX/Cui;Z)V

    .line 1946883
    iget-object v1, p0, LX/Cub;->f:LX/CuZ;

    sget-object v2, LX/Cui;->PAUSE_ICON:LX/Cui;

    invoke-virtual {v1, v2, v4}, LX/CuZ;->a(LX/Cui;Z)V

    .line 1946884
    iget-object v1, p0, LX/Cub;->o:Landroid/os/Handler;

    iget-object v2, p0, LX/Cub;->p:Ljava/lang/Runnable;

    const v3, -0x3877c88f

    invoke-static {v1, v2, v10, v11, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1946885
    :goto_4
    iget-object v1, p0, LX/Cub;->e:LX/0wd;

    invoke-virtual {v1, v6, v7}, LX/0wd;->b(D)LX/0wd;

    goto :goto_2

    .line 1946886
    :cond_7
    invoke-static {p0, v4}, LX/Cub;->a(LX/Cub;Z)V

    goto :goto_4

    .line 1946887
    :cond_8
    iget-object v1, p0, LX/Cub;->f:LX/CuZ;

    sget-object v2, LX/Cui;->NONE:LX/Cui;

    invoke-virtual {v1, v2, v4}, LX/CuZ;->a(LX/Cui;Z)V

    .line 1946888
    iget-object v1, p0, LX/Cub;->e:LX/0wd;

    invoke-virtual {v1, v8, v9}, LX/0wd;->b(D)LX/0wd;

    goto/16 :goto_2

    .line 1946889
    :cond_9
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946890
    sget-object v1, LX/Cqw;->b:LX/Cqw;

    invoke-interface {v0, v1}, LX/Ctg;->a(LX/Cqw;)V

    goto/16 :goto_0

    :cond_a
    const/4 v1, 0x1

    goto/16 :goto_3
.end method


# virtual methods
.method public final a()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;
    .locals 1

    .prologue
    .line 1946845
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    return-object v0
.end method

.method public final a(LX/Crd;)Z
    .locals 3

    .prologue
    .line 1946785
    const/4 v0, 0x0

    .line 1946786
    sget-object v1, LX/CuY;->a:[I

    invoke-virtual {p1}, LX/Crd;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1946787
    :cond_0
    :goto_0
    return v0

    .line 1946788
    :pswitch_0
    sget-object v0, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    invoke-virtual {p0, v0}, LX/Cub;->a(LX/Cuj;)Z

    move-result v0

    goto :goto_0

    .line 1946789
    :pswitch_1
    iget-object v1, p0, LX/Cub;->r:LX/Cuw;

    .line 1946790
    iget-boolean v2, v1, LX/Cuw;->j:Z

    move v1, v2

    .line 1946791
    if-nez v1, :cond_0

    .line 1946792
    sget-object v0, LX/Cuj;->USER_SCROLL_FINISHED:LX/Cuj;

    invoke-virtual {p0, v0}, LX/Cub;->a(LX/Cuj;)Z

    move-result v0

    goto :goto_0

    .line 1946793
    :pswitch_2
    sget-object v0, LX/Cuj;->USER_PRESSED_BACK:LX/Cuj;

    invoke-virtual {p0, v0}, LX/Cub;->a(LX/Cuj;)Z

    move-result v0

    goto :goto_0

    .line 1946794
    :pswitch_3
    sget-object v0, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    invoke-virtual {p0, v0}, LX/Cub;->a(LX/Cuj;)Z

    move-result v0

    goto :goto_0

    .line 1946795
    :pswitch_4
    sget-object v0, LX/Cuj;->USER_CONTROLLER_PAUSED:LX/Cuj;

    invoke-virtual {p0, v0}, LX/Cub;->a(LX/Cuj;)Z

    move-result v0

    goto :goto_0

    .line 1946796
    :pswitch_5
    sget-object v0, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    invoke-virtual {p0, v0}, LX/Cub;->a(LX/Cuj;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(LX/Cuj;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1946830
    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, LX/Cuj;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 1946831
    iget-object v2, p0, LX/Cub;->j:LX/Cuu;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Cub;->j:LX/Cuu;

    .line 1946832
    iget-object v3, v2, LX/Cuu;->p:LX/Cut;

    .line 1946833
    iget-object v4, v3, LX/Cut;->a:LX/Cus;

    sget-object v2, LX/Cus;->CONSUMING_PAUSE:LX/Cus;

    if-ne v4, v2, :cond_2

    .line 1946834
    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    if-ne p1, v4, :cond_2

    .line 1946835
    const/4 v4, 0x1

    .line 1946836
    :goto_0
    move v3, v4

    .line 1946837
    move v2, v3

    .line 1946838
    if-eqz v2, :cond_0

    .line 1946839
    :goto_1
    return v0

    .line 1946840
    :cond_0
    iget-object v0, p0, LX/Cub;->r:LX/Cuw;

    if-nez v0, :cond_1

    move v0, v1

    .line 1946841
    goto :goto_1

    .line 1946842
    :cond_1
    iget-object v0, p0, LX/Cub;->r:LX/Cuw;

    invoke-virtual {v0, p1}, LX/Cuw;->a(LX/Cuj;)Z

    move-result v0

    goto :goto_1

    .line 1946843
    :cond_2
    sget-object v4, LX/Cus;->IDLE:LX/Cus;

    iput-object v4, v3, LX/Cut;->a:LX/Cus;

    .line 1946844
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final b(LX/CrS;)V
    .locals 2

    .prologue
    .line 1946827
    iget-boolean v0, p0, LX/Cub;->m:Z

    if-eqz v0, :cond_0

    .line 1946828
    iget-object v0, p0, LX/Cub;->e:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {p0, v0}, LX/Cub;->a$redex0(LX/Cub;F)V

    .line 1946829
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1946815
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cub;->l:Z

    .line 1946816
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cub;->m:Z

    .line 1946817
    iget-boolean v0, p0, LX/Cub;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cub;->r:LX/Cuw;

    if-eqz v0, :cond_0

    .line 1946818
    iget-object v0, p0, LX/Cub;->r:LX/Cuw;

    .line 1946819
    iget-object p0, v0, LX/Cuw;->e:LX/Cuv;

    if-eqz p0, :cond_0

    .line 1946820
    iget-object p0, v0, LX/Cuw;->e:LX/Cuv;

    const/4 v0, 0x0

    .line 1946821
    iput-boolean v0, p0, LX/Cuv;->b:Z

    .line 1946822
    iput-boolean v0, p0, LX/Cuv;->c:Z

    .line 1946823
    iput-boolean v0, p0, LX/Cuv;->d:Z

    .line 1946824
    iput-boolean v0, p0, LX/Cuv;->e:Z

    .line 1946825
    iput-boolean v0, p0, LX/Cuv;->f:Z

    .line 1946826
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 8

    .prologue
    .line 1946806
    iget-object v0, p0, LX/Cub;->r:LX/Cuw;

    iget-object v1, p0, LX/Cub;->g:LX/Cua;

    const/4 v5, 0x0

    .line 1946807
    iput-object v1, v0, LX/Cuw;->f:LX/Cua;

    .line 1946808
    iget-object v2, v0, LX/Cuw;->f:LX/Cua;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/Cuw;->b:LX/Cum;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/Cuw;->b:LX/Cum;

    .line 1946809
    iget-object v3, v2, LX/Cum;->b:LX/Cuk;

    move-object v2, v3

    .line 1946810
    if-eqz v2, :cond_0

    .line 1946811
    iget-object v2, v0, LX/Cuw;->f:LX/Cua;

    sget-object v3, LX/Cuj;->APPLICATION_INIT_LISTENER:LX/Cuj;

    iget-object v4, v0, LX/Cuw;->b:LX/Cum;

    .line 1946812
    iget-object v6, v4, LX/Cum;->b:LX/Cuk;

    move-object v4, v6

    .line 1946813
    iget-boolean v7, v0, LX/Cuw;->j:Z

    move v6, v5

    invoke-virtual/range {v2 .. v7}, LX/Cua;->a(LX/Cuj;LX/Cuk;ZZZ)V

    .line 1946814
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1946797
    iget-boolean v0, p0, LX/Cub;->n:Z

    if-eqz v0, :cond_1

    .line 1946798
    iget-object v0, p0, LX/Cub;->o:Landroid/os/Handler;

    iget-object v1, p0, LX/Cub;->p:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1946799
    iget-object v0, p0, LX/Cub;->o:Landroid/os/Handler;

    iget-object v1, p0, LX/Cub;->q:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1946800
    iget-object v0, p0, LX/Cub;->i:LX/Cud;

    if-eqz v0, :cond_1

    .line 1946801
    iget-object v0, p0, LX/Cub;->i:LX/Cud;

    .line 1946802
    iget-object v1, v0, LX/Cud;->a:Ljava/util/EnumMap;

    invoke-virtual {v1}, Ljava/util/EnumMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1946803
    if-eqz v1, :cond_0

    .line 1946804
    check-cast v1, LX/Cue;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, LX/Cue;->setLoading(Z)V

    goto :goto_0

    .line 1946805
    :cond_1
    return-void
.end method
