.class public LX/DH9;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pn;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DHC;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DH9",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DHC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981432
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1981433
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DH9;->b:LX/0Zi;

    .line 1981434
    iput-object p1, p0, LX/DH9;->a:LX/0Ot;

    .line 1981435
    return-void
.end method

.method public static a(LX/0QB;)LX/DH9;
    .locals 4

    .prologue
    .line 1981421
    const-class v1, LX/DH9;

    monitor-enter v1

    .line 1981422
    :try_start_0
    sget-object v0, LX/DH9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981423
    sput-object v2, LX/DH9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981424
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981425
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981426
    new-instance v3, LX/DH9;

    const/16 p0, 0x21c7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DH9;-><init>(LX/0Ot;)V

    .line 1981427
    move-object v0, v3

    .line 1981428
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981429
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DH9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981430
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1981377
    check-cast p2, LX/DH8;

    .line 1981378
    iget-object v0, p0, LX/DH9;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DHC;

    iget-object v1, p2, LX/DH8;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/DH8;->b:LX/1Pb;

    .line 1981379
    invoke-static {p1}, LX/AqR;->c(LX/1De;)LX/AqP;

    move-result-object v4

    iget-object v3, v0, LX/DHC;->a:LX/1xT;

    invoke-virtual {v3, p1}, LX/1xT;->c(LX/1De;)LX/1xY;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1xY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1xY;

    move-result-object v5

    move-object v3, v2

    check-cast v3, LX/1Po;

    invoke-virtual {v5, v3}, LX/1xY;->a(LX/1Po;)LX/1xY;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/AqP;->a(LX/1X5;)LX/AqP;

    move-result-object v4

    iget-object v3, v0, LX/DHC;->b:LX/DHK;

    const/4 v5, 0x0

    .line 1981380
    new-instance v6, LX/DHJ;

    invoke-direct {v6, v3}, LX/DHJ;-><init>(LX/DHK;)V

    .line 1981381
    iget-object v7, v3, LX/DHK;->b:LX/0Zi;

    invoke-virtual {v7}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/DHI;

    .line 1981382
    if-nez v7, :cond_0

    .line 1981383
    new-instance v7, LX/DHI;

    invoke-direct {v7, v3}, LX/DHI;-><init>(LX/DHK;)V

    .line 1981384
    :cond_0
    invoke-static {v7, p1, v5, v5, v6}, LX/DHI;->a$redex0(LX/DHI;LX/1De;IILX/DHJ;)V

    .line 1981385
    move-object v6, v7

    .line 1981386
    move-object v5, v6

    .line 1981387
    move-object v3, v5

    .line 1981388
    iget-object v5, v3, LX/DHI;->a:LX/DHJ;

    iput-object v1, v5, LX/DHJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981389
    iget-object v5, v3, LX/DHI;->e:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 1981390
    move-object v5, v3

    .line 1981391
    move-object v3, v2

    check-cast v3, LX/1Pn;

    .line 1981392
    iget-object v6, v5, LX/DHI;->a:LX/DHJ;

    iput-object v3, v6, LX/DHJ;->b:LX/1Pn;

    .line 1981393
    iget-object v6, v5, LX/DHI;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1981394
    move-object v3, v5

    .line 1981395
    invoke-virtual {v4, v3}, LX/AqP;->b(LX/1X5;)LX/AqP;

    move-result-object v3

    .line 1981396
    const/4 v5, 0x0

    const/4 p2, 0x0

    .line 1981397
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1981398
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    .line 1981399
    sparse-switch v4, :sswitch_data_0

    .line 1981400
    const-string v6, "SingleCreatorSet"

    const-string v7, "Unsupported Actor Type: %d"

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p0, p2

    invoke-static {v6, v7, p0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v4, v5

    .line 1981401
    :goto_0
    move-object v4, v4

    .line 1981402
    if-eqz v4, :cond_1

    .line 1981403
    invoke-virtual {v3, v4}, LX/AqP;->c(LX/1X5;)LX/AqP;

    .line 1981404
    :cond_1
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1981405
    return-object v0

    .line 1981406
    :sswitch_0
    iget-object v4, v0, LX/DHC;->f:LX/14w;

    invoke-static {v1, v4}, LX/1ys;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1981407
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1981408
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    const v5, -0x7a50adf8

    invoke-static {v4, v5}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    .line 1981409
    iget-object v5, v0, LX/DHC;->d:LX/1ym;

    invoke-virtual {v5, p1}, LX/1ym;->c(LX/1De;)LX/BsV;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/BsV;->a(LX/1Pb;)LX/BsV;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/BsV;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BsV;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/BsV;->b(Ljava/lang/String;)LX/BsV;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/BsV;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/BsV;

    move-result-object v4

    const-string v5, "FEED_UNCONNECTED_STORY"

    invoke-virtual {v4, v5}, LX/BsV;->c(Ljava/lang/String;)LX/BsV;

    move-result-object v4

    const-string v5, "FEED_UNCONNECTED_STORY"

    invoke-virtual {v4, v5}, LX/BsV;->d(Ljava/lang/String;)LX/BsV;

    move-result-object v4

    new-instance v5, LX/DHA;

    invoke-direct {v5, v0, v1}, LX/DHA;-><init>(LX/DHC;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1981410
    iget-object v6, v4, LX/BsV;->a:LX/BsW;

    iput-object v5, v6, LX/BsW;->i:LX/DHA;

    .line 1981411
    move-object v4, v4

    .line 1981412
    goto :goto_0

    .line 1981413
    :cond_2
    const-string v4, "SingleCreatorSet"

    const-string v6, "Failed to attach Follow button to header"

    invoke-static {v4, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v5

    .line 1981414
    goto :goto_0

    .line 1981415
    :sswitch_1
    iget-object v4, v0, LX/DHC;->c:LX/1yk;

    invoke-virtual {v4, p1}, LX/1yk;->c(LX/1De;)LX/358;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/358;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/358;

    move-result-object v4

    new-instance v5, LX/DHB;

    invoke-direct {v5, v0, v1}, LX/DHB;-><init>(LX/DHC;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1981416
    iget-object v6, v4, LX/358;->a:LX/357;

    iput-object v5, v6, LX/357;->d:LX/DHB;

    .line 1981417
    move-object v4, v4

    .line 1981418
    check-cast v2, LX/1Pd;

    invoke-virtual {v4, v2}, LX/358;->a(LX/1Pd;)LX/358;

    move-result-object v4

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1981419
    invoke-static {}, LX/1dS;->b()V

    .line 1981420
    const/4 v0, 0x0

    return-object v0
.end method
