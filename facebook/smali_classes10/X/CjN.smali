.class public LX/CjN;
.super LX/CjH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CjH",
        "<",
        "Lcom/facebook/richdocument/view/block/BylineBlockView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929335
    const v0, 0x7f031200

    const/16 v1, 0xe

    invoke-direct {p0, v0, v1}, LX/CjH;-><init>(II)V

    .line 1929336
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)LX/CnG;
    .locals 3

    .prologue
    .line 1929337
    const v0, 0x7f0d2a36

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1929338
    const v1, 0x7f0d2a39

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1929339
    const v2, 0x7f0d2a38

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1929340
    new-instance p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;-><init>(Landroid/view/View;Lcom/facebook/richdocument/view/widget/RichTextView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/widget/LinearLayout;)V

    move-object v0, p0

    .line 1929341
    return-object v0
.end method

.method public final a(LX/CnG;)LX/CnT;
    .locals 1

    .prologue
    .line 1929333
    check-cast p1, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    .line 1929334
    new-instance v0, LX/Cnc;

    invoke-direct {v0, p1}, LX/Cnc;-><init>(Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;)V

    return-object v0
.end method
