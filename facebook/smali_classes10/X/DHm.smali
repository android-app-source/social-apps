.class public LX/DHm;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field private a:Z

.field public final b:Lcom/facebook/resources/ui/FbTextView;

.field public final c:Lcom/facebook/resources/ui/FbTextView;

.field public final d:Lcom/facebook/resources/ui/FbTextView;

.field public final e:Landroid/widget/ImageView;

.field public final f:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1982362
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 1982363
    const v0, 0x7f0313e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1982364
    const v0, 0x7f0d2ddf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DHm;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1982365
    const v0, 0x7f0d2de0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DHm;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1982366
    const v0, 0x7f0d2de2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DHm;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1982367
    const v0, 0x7f0d2dde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/DHm;->e:Landroid/widget/ImageView;

    .line 1982368
    const v0, 0x7f0d1507

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/DHm;->f:Landroid/widget/LinearLayout;

    .line 1982369
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1982370
    iget-boolean v0, p0, LX/DHm;->a:Z

    return v0
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 1982371
    iput-boolean p1, p0, LX/DHm;->a:Z

    .line 1982372
    return-void
.end method
