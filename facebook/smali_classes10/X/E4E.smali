.class public LX/E4E;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotosUnitComponentDataController$Listener;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final c:LX/1P1;

.field private d:I

.field public final e:Ljava/lang/String;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9uc;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
            "<",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "*TE;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final i:LX/3Tz;

.field private j:LX/E4I;


# direct methods
.method public constructor <init>(LX/1Pn;LX/1P1;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3Tz;LX/E4J;LX/1vo;)V
    .locals 6
    .param p1    # LX/1Pn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1P1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/1P1;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/3Tz;",
            "LX/E4J;",
            "LX/1vo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076405
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2076406
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E4E;->f:Ljava/util/List;

    .line 2076407
    invoke-interface {p1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/E4E;->a:Landroid/content/Context;

    .line 2076408
    iput-object p1, p0, LX/E4E;->b:LX/1Pn;

    .line 2076409
    iput-object p2, p0, LX/E4E;->c:LX/1P1;

    .line 2076410
    iput-object p4, p0, LX/E4E;->i:LX/3Tz;

    .line 2076411
    iget-object v0, p0, LX/E4E;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v0, v0

    .line 2076412
    iget-object v2, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2076413
    invoke-interface {v2}, LX/9uc;->cR()D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, LX/E4E;->d:I

    .line 2076414
    iget-object v0, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2076415
    check-cast v0, LX/9uf;

    .line 2076416
    invoke-interface {v0}, LX/9uf;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v2

    .line 2076417
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v1

    .line 2076418
    new-instance v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2076419
    iget-object v4, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2076420
    iget-object v5, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2076421
    invoke-direct {v3, v1, v4, v5}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, LX/E4E;->h:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2076422
    const/4 v4, 0x0

    .line 2076423
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    .line 2076424
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->CORE_IMAGE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v5, v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v3, v4

    .line 2076425
    :goto_0
    move-object v1, v3

    .line 2076426
    iput-object v1, p0, LX/E4E;->g:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 2076427
    invoke-interface {v0}, LX/9uf;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/E4E;->e:Ljava/lang/String;

    .line 2076428
    iget-object v1, p0, LX/E4E;->e:Ljava/lang/String;

    check-cast p1, LX/2kp;

    invoke-interface {p1}, LX/2kp;->t()LX/2jY;

    move-result-object v3

    .line 2076429
    new-instance v5, LX/E4I;

    invoke-direct {v5, v1, p0, v3}, LX/E4I;-><init>(Ljava/lang/String;LX/E4E;LX/2jY;)V

    .line 2076430
    invoke-static {p5}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/ReactionUtil;

    .line 2076431
    iput-object v4, v5, LX/E4I;->a:Lcom/facebook/reaction/ReactionUtil;

    .line 2076432
    move-object v1, v5

    .line 2076433
    iput-object v1, p0, LX/E4E;->j:LX/E4I;

    .line 2076434
    iget-object v1, p0, LX/E4E;->j:LX/E4I;

    invoke-interface {v0}, LX/9uf;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->b()LX/0us;

    move-result-object v0

    .line 2076435
    invoke-static {v1, v0}, LX/E4I;->a$redex0(LX/E4I;LX/0us;)V

    .line 2076436
    iget-object v3, v1, LX/E4I;->c:LX/E4E;

    invoke-static {v2}, LX/E4I;->b(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/E4E;->a(Ljava/util/List;)V

    .line 2076437
    return-void

    .line 2076438
    :cond_0
    invoke-virtual {p6, v3}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v3

    .line 2076439
    instance-of v5, v3, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v5, :cond_1

    .line 2076440
    check-cast v3, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    goto :goto_0

    :cond_1
    move-object v3, v4

    .line 2076441
    goto :goto_0
.end method

.method public static f(LX/E4E;)Z
    .locals 1

    .prologue
    .line 2076404
    iget-object v0, p0, LX/E4E;->g:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2076399
    if-nez p2, :cond_0

    .line 2076400
    new-instance v0, LX/E4K;

    iget-object v1, p0, LX/E4E;->h:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p0, LX/E4E;->g:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    iget-object v3, p0, LX/E4E;->b:LX/1Pn;

    invoke-direct {v0, v1, v2, v3}, LX/E4K;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Pn;)V

    .line 2076401
    :goto_0
    return-object v0

    .line 2076402
    :cond_0
    new-instance v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, LX/E4E;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2076403
    new-instance v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;

    iget v2, p0, LX/E4E;->d:I

    invoke-direct {v0, v1, v2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;-><init>(Lcom/facebook/drawee/fbpipeline/FbDraweeView;I)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2076379
    instance-of v0, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;

    if-eqz v0, :cond_1

    .line 2076380
    iget-object v0, p0, LX/E4E;->f:Ljava/util/List;

    .line 2076381
    invoke-static {p0}, LX/E4E;->f(LX/E4E;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 2076382
    :goto_0
    sub-int v1, p2, v1

    move v1, v1

    .line 2076383
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2076384
    check-cast p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;

    new-instance v1, LX/E4D;

    invoke-direct {v1, p0, v0}, LX/E4D;-><init>(LX/E4E;LX/9uc;)V

    const/4 p0, 0x0

    .line 2076385
    invoke-interface {v0}, LX/9uc;->ab()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, LX/9uc;->ab()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2076386
    invoke-interface {v0}, LX/9uc;->ab()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    .line 2076387
    iget-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2076388
    iget-object v3, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    sget-object p2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2076389
    iput-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->m:Ljava/lang/String;

    .line 2076390
    :cond_0
    :goto_1
    iget-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2076391
    :goto_2
    return-void

    .line 2076392
    :cond_1
    check-cast p1, LX/E4K;

    iget v0, p0, LX/E4E;->d:I

    .line 2076393
    iget-object v1, p1, LX/E4K;->l:LX/5eI;

    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, LX/5eI;->a(Landroid/view/View;)V

    .line 2076394
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2076395
    goto :goto_2

    .line 2076396
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 2076397
    :cond_3
    iget-object v2, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2076398
    iput-object p0, p1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/photos/ReactionPhotoViewHolder;->m:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/9uc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2076375
    iget-object v0, p0, LX/E4E;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2076376
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2076377
    invoke-virtual {p0}, LX/E4E;->d()V

    .line 2076378
    return-void
.end method

.method public final d()V
    .locals 8

    .prologue
    .line 2076369
    iget-object v0, p0, LX/E4E;->c:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    if-lt v0, v1, :cond_0

    .line 2076370
    iget-object v0, p0, LX/E4E;->j:LX/E4I;

    .line 2076371
    iget-boolean v2, v0, LX/E4I;->f:Z

    if-nez v2, :cond_0

    iget-object v2, v0, LX/E4I;->e:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 2076372
    :cond_0
    :goto_0
    return-void

    .line 2076373
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/E4I;->f:Z

    .line 2076374
    iget-object v2, v0, LX/E4I;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v3, v0, LX/E4I;->e:Ljava/lang/String;

    new-instance v4, LX/E4H;

    invoke-direct {v4, v0}, LX/E4H;-><init>(LX/E4I;)V

    const/4 v5, 0x4

    iget-object v6, v0, LX/E4I;->b:Ljava/lang/String;

    iget-object v7, v0, LX/E4I;->d:LX/2jY;

    invoke-virtual/range {v2 .. v7}, Lcom/facebook/reaction/ReactionUtil;->a(Ljava/lang/String;LX/0Ve;ILjava/lang/String;LX/2jY;)V

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2076366
    if-nez p1, :cond_0

    invoke-static {p0}, LX/E4E;->f(LX/E4E;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2076367
    const/4 v0, 0x0

    .line 2076368
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2076365
    iget-object v0, p0, LX/E4E;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {p0}, LX/E4E;->f(LX/E4E;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
