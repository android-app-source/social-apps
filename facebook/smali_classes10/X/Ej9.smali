.class public final LX/Ej9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2160893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 2160894
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2160895
    const-string v3, ""

    .line 2160896
    const-string v2, ""

    .line 2160897
    const-string v1, "normalized"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2160898
    const-string v5, "type"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/growth/model/ContactpointType;->valueOf(Ljava/lang/String;)Lcom/facebook/growth/model/ContactpointType;

    move-result-object v5

    .line 2160899
    const-string v6, "conf_surface"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2160900
    const-string v6, "conf_surface"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "fb4a_confirmation_qp"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2160901
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2160902
    :cond_0
    sget-object v6, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    if-ne v5, v6, :cond_2

    .line 2160903
    invoke-static {v1}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v1

    .line 2160904
    :goto_0
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    invoke-direct {v5, p1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2160905
    const-string v6, "extra_cancel_allowed"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2160906
    const-string v6, "extra_contactpoint"

    invoke-virtual {v5, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2160907
    const-string v1, "extra_for_phone_number_confirmation"

    invoke-virtual {v5, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2160908
    const-string v1, "extra_phone_number_acquisition_quick_promotion_id"

    invoke-virtual {v5, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2160909
    const-string v1, "extra_phone_number_acquisition_quick_promotion_type"

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2160910
    const-string v1, "conf_surface"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2160911
    const-string v1, "extra_ref"

    const-string v2, "conf_surface"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2160912
    :cond_1
    const-string v1, "qp"

    invoke-virtual {v5, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2160913
    return-object v5

    .line 2160914
    :cond_2
    sget-object v2, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v5, v2, :cond_3

    .line 2160915
    const-string v2, "country"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2160916
    invoke-static {v1, v2}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v1

    .line 2160917
    const-string v2, "phone_number"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 2160918
    const-string v2, "quick_promotion"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2160919
    const-string v2, "quick_promotion_type"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2160920
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown ContactpointType"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
