.class public LX/DFB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/DFL;

.field public final b:LX/2e9;


# direct methods
.method public constructor <init>(LX/DFL;LX/2e9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1977831
    iput-object p1, p0, LX/DFB;->a:LX/DFL;

    .line 1977832
    iput-object p2, p0, LX/DFB;->b:LX/2e9;

    .line 1977833
    return-void
.end method

.method public static a(LX/0QB;)LX/DFB;
    .locals 5

    .prologue
    .line 1977834
    const-class v1, LX/DFB;

    monitor-enter v1

    .line 1977835
    :try_start_0
    sget-object v0, LX/DFB;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1977836
    sput-object v2, LX/DFB;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1977837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1977839
    new-instance p0, LX/DFB;

    invoke-static {v0}, LX/DFL;->a(LX/0QB;)LX/DFL;

    move-result-object v3

    check-cast v3, LX/DFL;

    invoke-static {v0}, LX/2e9;->a(LX/0QB;)LX/2e9;

    move-result-object v4

    check-cast v4, LX/2e9;

    invoke-direct {p0, v3, v4}, LX/DFB;-><init>(LX/DFL;LX/2e9;)V

    .line 1977840
    move-object v0, p0

    .line 1977841
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1977842
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977843
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1977844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
