.class public final LX/E9L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;)V
    .locals 0

    .prologue
    .line 2084144
    iput-object p1, p0, LX/E9L;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2084143
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 2084138
    iget-object v0, p0, LX/E9L;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    iget-object v0, v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->q:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->setVisibility(I)V

    .line 2084139
    iget-object v0, p0, LX/E9L;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    iget-object v0, v0, LX/E8t;->a:LX/E8q;

    .line 2084140
    iget-object v1, v0, LX/E8q;->d:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 2084141
    iget-object v1, v0, LX/E8q;->d:Landroid/view/View;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2084142
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2084136
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2084137
    return-void
.end method
