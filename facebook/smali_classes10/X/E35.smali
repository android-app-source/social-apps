.class public LX/E35;
.super LX/5OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/5OM;"
    }
.end annotation


# instance fields
.field private final l:LX/D9E;

.field public final m:LX/E1f;


# direct methods
.method public constructor <init>(LX/2km;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;LX/D9E;LX/E1f;)V
    .locals 9
    .param p1    # LX/2km;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/D9E;",
            "LX/E1f;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2073580
    move-object v0, p1

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2073581
    iput-object p5, p0, LX/E35;->l:LX/D9E;

    .line 2073582
    iput-object p6, p0, LX/E35;->m:LX/E1f;

    .line 2073583
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2073584
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v6

    .line 2073585
    :goto_1
    iget-object v1, p0, LX/E35;->l:LX/D9E;

    invoke-virtual {p0}, LX/5OM;->c()LX/5OG;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/D9E;->a(LX/5OG;Ljava/lang/CharSequence;)Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;

    move-result-object v8

    .line 2073586
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v6

    .line 2073587
    :goto_2
    invoke-virtual {v8, v0}, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->a(Ljava/lang/String;)Landroid/view/MenuItem;

    .line 2073588
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->hi_()LX/174;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v6

    .line 2073589
    :goto_3
    invoke-virtual {v8, v0}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2073590
    new-instance v0, LX/E34;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/E34;-><init>(LX/E35;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2073591
    invoke-virtual {p0}, LX/5OM;->c()LX/5OG;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/5OG;->a(LX/3Ai;)V

    goto :goto_0

    .line 2073592
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2073593
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2073594
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->hi_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 2073595
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/5OM;->a(Z)V

    .line 2073596
    return-void
.end method
