.class public final LX/ExU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0zS;

.field public final synthetic b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)V
    .locals 0

    .prologue
    .line 2184762
    iput-object p1, p0, LX/ExU;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iput-object p2, p0, LX/ExU;->a:LX/0zS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2184763
    const/16 v2, 0x14

    .line 2184764
    iget-object v0, p0, LX/ExU;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->b:Z

    if-eqz v0, :cond_0

    .line 2184765
    iget-object v0, p0, LX/ExU;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->f:LX/Eui;

    sget-object v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v3, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    iget-object v3, v3, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    iget-object v4, p0, LX/ExU;->a:LX/0zS;

    iget-object v5, p0, LX/ExU;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    invoke-static {v5}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->d(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)LX/0TF;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/Eui;->a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2184766
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/ExU;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->f:LX/Eui;

    sget-object v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v3, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    iget-object v3, v3, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    iget-object v4, p0, LX/ExU;->a:LX/0zS;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Eui;->a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
