.class public final LX/DQR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DQQ;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/info/GroupInfoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoFragment;)V
    .locals 0

    .prologue
    .line 1994339
    iput-object p1, p0, LX/DQR;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V
    .locals 2

    .prologue
    .line 1994340
    iget-object v0, p0, LX/DQR;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v1, p0, LX/DQR;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v1, v1, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v1}, LX/DQq;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DQq;

    move-result-object v1

    .line 1994341
    iput-object p1, v1, LX/DQq;->ah:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 1994342
    move-object v1, v1

    .line 1994343
    invoke-virtual {v1}, LX/DQq;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/groups/info/GroupInfoFragment;->a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    .line 1994344
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;)V
    .locals 2

    .prologue
    .line 1994345
    iget-object v0, p0, LX/DQR;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v1, p0, LX/DQR;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v1, v1, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v1}, LX/DQq;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DQq;

    move-result-object v1

    .line 1994346
    iput-object p1, v1, LX/DQq;->ai:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 1994347
    move-object v1, v1

    .line 1994348
    invoke-virtual {v1}, LX/DQq;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/groups/info/GroupInfoFragment;->a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    .line 1994349
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V
    .locals 2

    .prologue
    .line 1994350
    iget-object v0, p0, LX/DQR;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v1, p0, LX/DQR;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v1, v1, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v1}, LX/DQq;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DQq;

    move-result-object v1

    .line 1994351
    iput-object p1, v1, LX/DQq;->aj:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 1994352
    move-object v1, v1

    .line 1994353
    invoke-virtual {v1}, LX/DQq;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/groups/info/GroupInfoFragment;->a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    .line 1994354
    return-void
.end method
