.class public LX/DpG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final is_canonical:Ljava/lang/Boolean;

.field public final thread_members:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2043838
    new-instance v0, LX/1sv;

    const-string v1, "CreateThreadPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpG;->b:LX/1sv;

    .line 2043839
    new-instance v0, LX/1sw;

    const-string v1, "thread_members"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpG;->c:LX/1sw;

    .line 2043840
    new-instance v0, LX/1sw;

    const-string v1, "is_canonical"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpG;->d:LX/1sw;

    .line 2043841
    const/4 v0, 0x1

    sput-boolean v0, LX/DpG;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2043842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2043843
    iput-object p1, p0, LX/DpG;->thread_members:Ljava/util/List;

    .line 2043844
    iput-object p2, p0, LX/DpG;->is_canonical:Ljava/lang/Boolean;

    .line 2043845
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2043846
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2043847
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2043848
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2043849
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CreateThreadPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2043850
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043851
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043852
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043853
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043854
    const-string v4, "thread_members"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043855
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043856
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043857
    iget-object v4, p0, LX/DpG;->thread_members:Ljava/util/List;

    if-nez v4, :cond_3

    .line 2043858
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043859
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043860
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043861
    const-string v4, "is_canonical"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043862
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043863
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043864
    iget-object v0, p0, LX/DpG;->is_canonical:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    .line 2043865
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043866
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043867
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043868
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2043869
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2043870
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2043871
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2043872
    :cond_3
    iget-object v4, p0, LX/DpG;->thread_members:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2043873
    :cond_4
    iget-object v0, p0, LX/DpG;->is_canonical:Ljava/lang/Boolean;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    .line 2043874
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2043875
    iget-object v0, p0, LX/DpG;->thread_members:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2043876
    sget-object v0, LX/DpG;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043877
    new-instance v0, LX/1u3;

    const/16 v1, 0xa

    iget-object v2, p0, LX/DpG;->thread_members:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2043878
    iget-object v0, p0, LX/DpG;->thread_members:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2043879
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, LX/1su;->a(J)V

    goto :goto_0

    .line 2043880
    :cond_0
    iget-object v0, p0, LX/DpG;->is_canonical:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 2043881
    sget-object v0, LX/DpG;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043882
    iget-object v0, p0, LX/DpG;->is_canonical:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2043883
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2043884
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2043885
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2043886
    if-nez p1, :cond_1

    .line 2043887
    :cond_0
    :goto_0
    return v0

    .line 2043888
    :cond_1
    instance-of v1, p1, LX/DpG;

    if-eqz v1, :cond_0

    .line 2043889
    check-cast p1, LX/DpG;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2043890
    if-nez p1, :cond_3

    .line 2043891
    :cond_2
    :goto_1
    move v0, v2

    .line 2043892
    goto :goto_0

    .line 2043893
    :cond_3
    iget-object v0, p0, LX/DpG;->thread_members:Ljava/util/List;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2043894
    :goto_2
    iget-object v3, p1, LX/DpG;->thread_members:Ljava/util/List;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2043895
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2043896
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043897
    iget-object v0, p0, LX/DpG;->thread_members:Ljava/util/List;

    iget-object v3, p1, LX/DpG;->thread_members:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043898
    :cond_5
    iget-object v0, p0, LX/DpG;->is_canonical:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2043899
    :goto_4
    iget-object v3, p1, LX/DpG;->is_canonical:Ljava/lang/Boolean;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2043900
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2043901
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043902
    iget-object v0, p0, LX/DpG;->is_canonical:Ljava/lang/Boolean;

    iget-object v3, p1, LX/DpG;->is_canonical:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 2043903
    goto :goto_1

    :cond_8
    move v0, v2

    .line 2043904
    goto :goto_2

    :cond_9
    move v3, v2

    .line 2043905
    goto :goto_3

    :cond_a
    move v0, v2

    .line 2043906
    goto :goto_4

    :cond_b
    move v3, v2

    .line 2043907
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2043908
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2043909
    sget-boolean v0, LX/DpG;->a:Z

    .line 2043910
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpG;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2043911
    return-object v0
.end method
