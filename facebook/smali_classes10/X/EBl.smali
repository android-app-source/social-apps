.class public final LX/EBl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

.field public b:I

.field public c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 1

    .prologue
    .line 2087802
    iput-object p1, p0, LX/EBl;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2087803
    const/4 v0, 0x0

    iput v0, p0, LX/EBl;->b:I

    .line 2087804
    const/4 v0, 0x0

    iput-object v0, p0, LX/EBl;->c:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x14a3cea8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2087805
    iget v1, p0, LX/EBl;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/EBl;->b:I

    .line 2087806
    iget v1, p0, LX/EBl;->b:I

    const/4 v2, 0x5

    if-lt v1, v2, :cond_1

    .line 2087807
    const/4 v1, 0x0

    iput v1, p0, LX/EBl;->b:I

    .line 2087808
    iget-object v1, p0, LX/EBl;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v1, v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->az:Lcom/facebook/rtc/views/RtcSnakeView;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/RtcSnakeView;->a()V

    .line 2087809
    :cond_0
    :goto_0
    const v1, 0x1e41f491

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2087810
    :cond_1
    iget v1, p0, LX/EBl;->b:I

    if-lez v1, :cond_0

    iget-object v1, p0, LX/EBl;->c:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 2087811
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, LX/EBl;->c:Landroid/os/Handler;

    .line 2087812
    iget-object v1, p0, LX/EBl;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/rtc/activities/WebrtcIncallActivity$17$1;

    invoke-direct {v2, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$17$1;-><init>(LX/EBl;)V

    const-wide/16 v4, 0xfa0

    const v3, 0x41f10ff9

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
