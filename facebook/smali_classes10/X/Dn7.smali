.class public final LX/Dn7;
.super LX/Dn5;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2040012
    iput-object p1, p0, LX/Dn7;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-direct {p0, p2}, LX/Dn5;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 3

    .prologue
    .line 2040013
    iget-object v0, p0, LX/Dn5;->m:Landroid/widget/TextView;

    iget-object v1, p0, LX/Dn7;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082ba5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040014
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v0

    .line 2040015
    iget-object v1, p0, LX/Dn5;->n:Landroid/widget/TextView;

    iget-object v2, p0, LX/Dn7;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-static {v2, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2040016
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->s()Ljava/lang/String;

    move-result-object v0

    .line 2040017
    iget-object v1, p0, LX/Dn5;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040018
    return-void
.end method
