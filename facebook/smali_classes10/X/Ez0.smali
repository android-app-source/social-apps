.class public final LX/Ez0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ez3;

.field public final synthetic b:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;LX/Ez3;)V
    .locals 0

    .prologue
    .line 2186740
    iput-object p1, p0, LX/Ez0;->b:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iput-object p2, p0, LX/Ez0;->a:LX/Ez3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x18855a5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186741
    iget-object v1, p0, LX/Ez0;->b:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    const/4 p1, 0x0

    .line 2186742
    iget-object v2, v1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->v:LX/0P1;

    invoke-virtual {v2}, LX/0P1;->values()LX/0Py;

    move-result-object v2

    invoke-virtual {v2}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ez2;

    .line 2186743
    iget-object v5, v2, LX/Ez2;->a:LX/BS8;

    .line 2186744
    iget-object v1, v5, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    move-object v5, v1

    .line 2186745
    invoke-virtual {v5, p1}, Lcom/facebook/uicontrib/fab/FabView;->setEnabled(Z)V

    .line 2186746
    iget-object v2, v2, LX/Ez2;->a:LX/BS8;

    .line 2186747
    iget-object v5, v2, LX/BS8;->b:Landroid/widget/TextView;

    move-object v2, v5

    .line 2186748
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 2186749
    :cond_0
    iget-object v1, p0, LX/Ez0;->b:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->t:LX/Eyw;

    iget-object v2, p0, LX/Ez0;->a:LX/Ez3;

    .line 2186750
    iget-object v4, v2, LX/Ez3;->d:Ljava/lang/String;

    move-object v2, v4

    .line 2186751
    const-string v4, "sprout_button_clicked"

    invoke-static {v1, v4}, LX/Eyw;->c(LX/Eyw;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "sprout_button_name"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2186752
    iget-object v5, v1, LX/Eyw;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2186753
    iget-object v1, p0, LX/Ez0;->a:LX/Ez3;

    .line 2186754
    iget-object v2, v1, LX/Ez3;->e:Ljava/lang/Runnable;

    move-object v1, v2

    .line 2186755
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 2186756
    const v1, -0x5fe32aca

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
