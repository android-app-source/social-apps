.class public LX/EuD;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EuB;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EuE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2179183
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EuD;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EuE;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2179224
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2179225
    iput-object p1, p0, LX/EuD;->b:LX/0Ot;

    .line 2179226
    return-void
.end method

.method public static a(LX/0QB;)LX/EuD;
    .locals 4

    .prologue
    .line 2179213
    const-class v1, LX/EuD;

    monitor-enter v1

    .line 2179214
    :try_start_0
    sget-object v0, LX/EuD;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2179215
    sput-object v2, LX/EuD;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2179216
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2179217
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2179218
    new-instance v3, LX/EuD;

    const/16 p0, 0x222d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EuD;-><init>(LX/0Ot;)V

    .line 2179219
    move-object v0, v3

    .line 2179220
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2179221
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EuD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2179222
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2179223
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 6

    .prologue
    .line 2179227
    check-cast p1, LX/EuC;

    .line 2179228
    iget-object v0, p0, LX/EuD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EuE;

    iget-object v1, p1, LX/EuC;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2179229
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2179230
    iget-object v2, v0, LX/EuE;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2dj;

    sget-object v3, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    invoke-virtual {v2, v4, v5, v3}, LX/2dj;->a(JLX/2hC;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2179231
    iget-object v2, v0, LX/EuE;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2do;

    new-instance v3, LX/2iB;

    invoke-direct {v3, v4, v5}, LX/2iB;-><init>(J)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2179232
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2179197
    check-cast p2, LX/EuC;

    .line 2179198
    iget-object v0, p0, LX/EuD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EuE;

    iget-object v1, p2, LX/EuC;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-boolean v2, p2, LX/EuC;->b:Z

    iget-object v3, p2, LX/EuC;->c:LX/2h7;

    const/4 v5, 0x0

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 v8, 0x1

    const/4 p0, 0x2

    const/4 v7, 0x0

    .line 2179199
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->n()LX/1vs;

    move-result-object v4

    iget-object v6, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v6, v4, v7}, LX/15i;->j(II)I

    move-result v9

    .line 2179200
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v4, v6, :cond_1

    move v4, v8

    .line 2179201
    :goto_0
    if-gtz v9, :cond_0

    if-eqz v4, :cond_2

    :cond_0
    move v6, v8

    .line 2179202
    :goto_1
    if-eqz v6, :cond_4

    .line 2179203
    if-eqz v4, :cond_3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v9, 0x7f080f85

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2179204
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v9

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    const v11, 0x7f0b0052

    invoke-virtual {v10, v11}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    const v11, 0x7f0a010d

    invoke-virtual {v10, v11}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v10

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v10, v11}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const v11, 0x7f0b08a6

    invoke-interface {v10, p0, v11}, LX/1Di;->c(II)LX/1Di;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    if-nez v6, :cond_5

    move-object v4, v5

    :goto_3
    invoke-interface {v9, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    if-nez v2, :cond_6

    :goto_4
    invoke-interface {v6, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2179205
    return-object v0

    :cond_1
    move v4, v7

    .line 2179206
    goto :goto_0

    :cond_2
    move v6, v7

    .line 2179207
    goto :goto_1

    .line 2179208
    :cond_3
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v10, 0x7f0f005c

    new-array v11, v8, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v7

    invoke-virtual {v4, v10, v9, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_4
    move-object v4, v5

    .line 2179209
    goto :goto_2

    .line 2179210
    :cond_5
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v6, 0x7f0b0050

    invoke-virtual {v4, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v6, 0x7f0a010e

    invoke-virtual {v4, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v6, 0x7f0b08a6

    invoke-interface {v4, p0, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    goto :goto_3

    :cond_6
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    iget-object v4, v0, LX/EuE;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Eto;

    invoke-virtual {v4, p1}, LX/Eto;->c(LX/1De;)LX/Etn;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/Etn;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Etn;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/Etn;->a(LX/2h7;)LX/Etn;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/Etn;->a(Z)LX/Etn;

    move-result-object v4

    const v7, 0x7f0b004d

    invoke-virtual {v4, v7}, LX/Etn;->h(I)LX/Etn;

    move-result-object v4

    const v7, 0x7f0b0088

    invoke-virtual {v4, v7}, LX/Etn;->i(I)LX/Etn;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v5

    iget-object v4, v0, LX/EuE;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Eu4;

    invoke-virtual {v4, p1}, LX/Eu4;->c(LX/1De;)LX/Eu2;

    move-result-object v4

    const v7, 0x7f080f78

    invoke-virtual {v4, v7}, LX/Eu2;->h(I)LX/Eu2;

    move-result-object v4

    const v7, 0x7f0b004d

    invoke-virtual {v4, v7}, LX/Eu2;->l(I)LX/Eu2;

    move-result-object v4

    const v7, 0x7f080f78

    invoke-virtual {v4, v7}, LX/Eu2;->j(I)LX/Eu2;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 2179211
    const v7, 0x4718ddfa

    const/4 v9, 0x0

    invoke-static {p1, v7, v9}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2179212
    invoke-interface {v4, v7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x164

    invoke-interface {v4, v5}, LX/1Di;->n(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v8, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b08a6

    invoke-interface {v4, p0, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    goto/16 :goto_4
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2179192
    invoke-static {}, LX/1dS;->b()V

    .line 2179193
    iget v0, p1, LX/1dQ;->b:I

    .line 2179194
    packed-switch v0, :pswitch_data_0

    .line 2179195
    :goto_0
    return-object v1

    .line 2179196
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/EuD;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4718ddfa
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/EuB;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2179184
    new-instance v1, LX/EuC;

    invoke-direct {v1, p0}, LX/EuC;-><init>(LX/EuD;)V

    .line 2179185
    sget-object v2, LX/EuD;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EuB;

    .line 2179186
    if-nez v2, :cond_0

    .line 2179187
    new-instance v2, LX/EuB;

    invoke-direct {v2}, LX/EuB;-><init>()V

    .line 2179188
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EuB;->a$redex0(LX/EuB;LX/1De;IILX/EuC;)V

    .line 2179189
    move-object v1, v2

    .line 2179190
    move-object v0, v1

    .line 2179191
    return-object v0
.end method
