.class public LX/D8f;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/D7g;

.field public b:LX/D8a;

.field public c:Landroid/widget/Scroller;

.field public d:Landroid/animation/ValueAnimator;

.field public e:LX/D8S;


# direct methods
.method public constructor <init>(LX/D7g;ILandroid/content/Context;LX/D8S;)V
    .locals 3

    .prologue
    .line 1969099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1969100
    iput-object p1, p0, LX/D8f;->a:LX/D7g;

    .line 1969101
    new-instance v0, LX/D8a;

    neg-int v1, p2

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/D8a;-><init>(II)V

    iput-object v0, p0, LX/D8f;->b:LX/D8a;

    .line 1969102
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/D8f;->c:Landroid/widget/Scroller;

    .line 1969103
    iput-object p4, p0, LX/D8f;->e:LX/D8S;

    .line 1969104
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1969105
    iget-object v2, p0, LX/D8f;->a:LX/D7g;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v2}, LX/D7g;->i()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1969106
    :cond_0
    :goto_0
    return v0

    .line 1969107
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-gt v2, v1, :cond_0

    .line 1969108
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v3}, LX/D7g;->a()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1969109
    iget-object v2, p0, LX/D8f;->a:LX/D7g;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v2}, LX/D7g;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1}, LX/D8f;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p2}, LX/D8f;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/D8f;->b:LX/D8a;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1969110
    :cond_1
    :goto_0
    return v0

    .line 1969111
    :cond_2
    iget-object v2, p0, LX/D8f;->b:LX/D8a;

    iget-object v3, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v3}, LX/D7g;->b()F

    move-result v3

    iget-object v4, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v4}, LX/D7g;->d()I

    move-result v4

    iget-object v5, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v5}, LX/D7g;->c()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, LX/D8a;->a(FII)V

    .line 1969112
    iget-object v2, p0, LX/D8f;->b:LX/D8a;

    invoke-virtual {v2}, LX/D8a;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1969113
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_5

    move v2, v0

    .line 1969114
    :goto_1
    if-eqz v2, :cond_3

    iget-object v3, p0, LX/D8f;->b:LX/D8a;

    .line 1969115
    iget v4, v3, LX/D8a;->g:F

    iget v5, v3, LX/D8a;->b:I

    .line 1969116
    int-to-float p1, v5

    sub-float p1, v4, p1

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    const v3, 0x3dcccccd    # 0.1f

    cmpg-float p1, p1, v3

    if-gtz p1, :cond_6

    const/4 p1, 0x1

    :goto_2
    move v4, p1

    .line 1969117
    move v3, v4

    .line 1969118
    if-nez v3, :cond_1

    .line 1969119
    :cond_3
    if-nez v2, :cond_4

    iget-object v2, p0, LX/D8f;->b:LX/D8a;

    invoke-virtual {v2}, LX/D8a;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/D8f;->b:LX/D8a;

    .line 1969120
    iget v3, v2, LX/D8a;->i:I

    if-lez v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    move v2, v3

    .line 1969121
    if-eqz v2, :cond_1

    :cond_4
    move v0, v1

    .line 1969122
    goto :goto_0

    :cond_5
    move v2, v1

    .line 1969123
    goto :goto_1

    :cond_6
    const/4 p1, 0x0

    goto :goto_2

    :cond_7
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1969124
    iget-object v0, p0, LX/D8f;->a:LX/D7g;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v0}, LX/D7g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, LX/D8f;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1969125
    :cond_0
    const/4 v0, 0x0

    .line 1969126
    :goto_0
    return v0

    .line 1969127
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 1969128
    iget-object v0, p0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 1969129
    iget-object v0, p0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1969130
    const/4 v0, 0x0

    iput-object v0, p0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    .line 1969131
    :cond_2
    iget-object v0, p0, LX/D8f;->c:Landroid/widget/Scroller;

    if-eqz v0, :cond_3

    .line 1969132
    iget-object v0, p0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1969133
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10

    .prologue
    const v6, 0x7fffffff

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 1969134
    iget-object v0, p0, LX/D8f;->a:LX/D7g;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v0}, LX/D7g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8f;->b:LX/D8a;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8f;->c:Landroid/widget/Scroller;

    if-nez v0, :cond_1

    .line 1969135
    :cond_0
    :goto_0
    return v1

    .line 1969136
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-gt v0, v9, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-gt v0, v9, :cond_0

    .line 1969137
    iget-object v0, p0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v0, v9}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 1969138
    iget-object v0, p0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v3, v2

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v4, v2

    move v2, v1

    move v5, v1

    move v7, v1

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 1969139
    iget-object v0, p0, LX/D8f;->b:LX/D8a;

    iget-object v2, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v2}, LX/D7g;->e()I

    move-result v2

    iget-object v3, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v3}, LX/D7g;->g()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v3}, LX/D7g;->f()I

    move-result v3

    invoke-virtual {v0, v1, v2, v1, v3}, LX/D8a;->a(IIII)V

    .line 1969140
    iget-object v0, p0, LX/D8f;->b:LX/D8a;

    iget-object v1, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v1}, LX/D7g;->b()F

    move-result v1

    iget-object v2, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v2}, LX/D7g;->d()I

    move-result v2

    iget-object v3, p0, LX/D8f;->a:LX/D7g;

    invoke-interface {v3}, LX/D7g;->c()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/D8a;->a(FII)V

    .line 1969141
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    .line 1969142
    iget-object v0, p0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/D8f;->c:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getDuration()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1969143
    iget-object v0, p0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    new-instance v1, LX/D8e;

    invoke-direct {v1, p0, p3, p4}, LX/D8e;-><init>(LX/D8f;FF)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1969144
    iget-object v0, p0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    move v1, v9

    .line 1969145
    goto :goto_0

    .line 1969146
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
