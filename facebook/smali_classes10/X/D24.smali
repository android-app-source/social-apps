.class public final LX/D24;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D25;


# direct methods
.method public constructor <init>(LX/D25;)V
    .locals 0

    .prologue
    .line 1958082
    iput-object p1, p0, LX/D24;->a:LX/D25;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 12

    .prologue
    .line 1958083
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1958084
    iget-object v0, p0, LX/D24;->a:LX/D25;

    iget-object v0, v0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    const/4 v1, 0x1

    .line 1958085
    iput-boolean v1, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->y:Z

    .line 1958086
    iget-object v0, p0, LX/D24;->a:LX/D25;

    iget-object v0, v0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    iget-object v1, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->r:LX/D23;

    iget-object v0, p0, LX/D24;->a:LX/D25;

    iget-object v2, v0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x5

    const/16 v6, 0x3ea

    const/4 v7, 0x0

    iget-object v0, p0, LX/D24;->a:LX/D25;

    iget-object v8, v0, LX/D25;->a:Ljava/lang/String;

    iget-object v0, p0, LX/D24;->a:LX/D25;

    iget-object v9, v0, LX/D25;->b:Lcom/facebook/share/model/ComposerAppAttribution;

    iget-object v0, p0, LX/D24;->a:LX/D25;

    iget-wide v10, v0, LX/D25;->c:J

    move-object v4, p1

    invoke-virtual/range {v1 .. v11}, LX/D23;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;IILX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V

    .line 1958087
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1958088
    iget-object v0, p0, LX/D24;->a:LX/D25;

    iget-object v0, v0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    const-string v1, "Failed to covert content Uri to file Uri: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1958089
    invoke-static {v0, v1, v2}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->a$redex0(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1958090
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1958091
    check-cast p1, Landroid/net/Uri;

    invoke-direct {p0, p1}, LX/D24;->a(Landroid/net/Uri;)V

    return-void
.end method
