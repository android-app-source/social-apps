.class public LX/EVb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/EVb;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/14v;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/2zB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/14v;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2128486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2128487
    iput-object p1, p0, LX/EVb;->a:LX/0Ot;

    .line 2128488
    return-void
.end method

.method public static a(LX/0QB;)LX/EVb;
    .locals 4

    .prologue
    .line 2128489
    sget-object v0, LX/EVb;->c:LX/EVb;

    if-nez v0, :cond_1

    .line 2128490
    const-class v1, LX/EVb;

    monitor-enter v1

    .line 2128491
    :try_start_0
    sget-object v0, LX/EVb;->c:LX/EVb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2128492
    if-eqz v2, :cond_0

    .line 2128493
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2128494
    new-instance v3, LX/EVb;

    const/16 p0, 0x1367

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EVb;-><init>(LX/0Ot;)V

    .line 2128495
    move-object v0, v3

    .line 2128496
    sput-object v0, LX/EVb;->c:LX/EVb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2128497
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2128498
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2128499
    :cond_1
    sget-object v0, LX/EVb;->c:LX/EVb;

    return-object v0

    .line 2128500
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2128501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
