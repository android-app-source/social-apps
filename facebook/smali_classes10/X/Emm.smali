.class public final enum LX/Emm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Emm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Emm;

.field public static final enum BACK_BUTTON_TAP:LX/Emm;

.field public static final enum PAGER_TAP:LX/Emm;

.field public static final enum SWIPE_DOWN:LX/Emm;

.field public static final enum USER_BLOCKED:LX/Emm;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2166156
    new-instance v0, LX/Emm;

    const-string v1, "SWIPE_DOWN"

    const-string v2, "swipe_down"

    invoke-direct {v0, v1, v3, v2}, LX/Emm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emm;->SWIPE_DOWN:LX/Emm;

    .line 2166157
    new-instance v0, LX/Emm;

    const-string v1, "BACK_BUTTON_TAP"

    const-string v2, "back_button_tap"

    invoke-direct {v0, v1, v4, v2}, LX/Emm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emm;->BACK_BUTTON_TAP:LX/Emm;

    .line 2166158
    new-instance v0, LX/Emm;

    const-string v1, "USER_BLOCKED"

    const-string v2, "user_blocked"

    invoke-direct {v0, v1, v5, v2}, LX/Emm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emm;->USER_BLOCKED:LX/Emm;

    .line 2166159
    new-instance v0, LX/Emm;

    const-string v1, "PAGER_TAP"

    const-string v2, "pager_tap"

    invoke-direct {v0, v1, v6, v2}, LX/Emm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emm;->PAGER_TAP:LX/Emm;

    .line 2166160
    const/4 v0, 0x4

    new-array v0, v0, [LX/Emm;

    sget-object v1, LX/Emm;->SWIPE_DOWN:LX/Emm;

    aput-object v1, v0, v3

    sget-object v1, LX/Emm;->BACK_BUTTON_TAP:LX/Emm;

    aput-object v1, v0, v4

    sget-object v1, LX/Emm;->USER_BLOCKED:LX/Emm;

    aput-object v1, v0, v5

    sget-object v1, LX/Emm;->PAGER_TAP:LX/Emm;

    aput-object v1, v0, v6

    sput-object v0, LX/Emm;->$VALUES:[LX/Emm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2166161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2166162
    iput-object p3, p0, LX/Emm;->name:Ljava/lang/String;

    .line 2166163
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Emm;
    .locals 1

    .prologue
    .line 2166164
    const-class v0, LX/Emm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Emm;

    return-object v0
.end method

.method public static values()[LX/Emm;
    .locals 1

    .prologue
    .line 2166165
    sget-object v0, LX/Emm;->$VALUES:[LX/Emm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Emm;

    return-object v0
.end method
