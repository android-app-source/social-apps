.class public LX/EI9;
.super Landroid/widget/RelativeLayout;
.source ""

# interfaces
.implements LX/7fD;
.implements LX/7fE;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Lcom/facebook/fbui/glyph/GlyphButton;

.field public B:Landroid/view/View;

.field public C:Lcom/facebook/fbui/glyph/GlyphButton;

.field public D:Lcom/facebook/fbui/glyph/GlyphButton;

.field public E:Landroid/view/View;

.field public F:Landroid/view/View;

.field public G:Landroid/view/View;

.field public H:Landroid/view/View;

.field public I:Z

.field public J:Ljava/lang/String;

.field public K:Z

.field public L:Z

.field public M:Z

.field public N:Z

.field public O:Landroid/graphics/drawable/ColorDrawable;

.field public P:Landroid/graphics/drawable/ColorDrawable;

.field public Q:LX/7fE;

.field private R:LX/EI5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:I

.field private final T:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

.field public U:LX/EHG;

.field public V:LX/EGd;

.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/EFm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

.field public g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

.field public h:Landroid/view/View;

.field public i:Landroid/view/View;

.field public j:Landroid/view/View;

.field public k:Landroid/view/View;

.field public l:Landroid/view/View;

.field public m:Landroid/view/View;

.field public n:Landroid/view/View;

.field public o:Landroid/view/View;

.field public p:Lcom/facebook/resources/ui/FbTextView;

.field public q:Lcom/facebook/fbui/glyph/GlyphView;

.field public r:Lcom/facebook/user/tiles/UserTileView;

.field public s:Lcom/facebook/fbui/glyph/GlyphView;

.field public t:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/rtc/views/GroupRingNoticeView;",
            ">;"
        }
    .end annotation
.end field

.field public v:Landroid/view/animation/ScaleAnimation;

.field public w:Landroid/graphics/Point;

.field public x:F

.field private y:I

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2100789
    const-class v0, LX/EI9;

    sput-object v0, LX/EI9;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2100790
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2100791
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2100792
    iput-object v0, p0, LX/EI9;->e:LX/0Ot;

    .line 2100793
    iput-object v2, p0, LX/EI9;->q:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2100794
    iput-object v2, p0, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    .line 2100795
    iput v1, p0, LX/EI9;->y:I

    .line 2100796
    iput-boolean v1, p0, LX/EI9;->z:Z

    .line 2100797
    iput-boolean v1, p0, LX/EI9;->I:Z

    .line 2100798
    iput-boolean v1, p0, LX/EI9;->M:Z

    .line 2100799
    iput-boolean v1, p0, LX/EI9;->N:Z

    .line 2100800
    const/16 v0, 0x24

    iput v0, p0, LX/EI9;->S:I

    .line 2100801
    sget-object v0, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v0, v0

    .line 2100802
    iput-object v0, p0, LX/EI9;->T:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    .line 2100803
    const-class v0, LX/EI9;

    invoke-static {v0, p0}, LX/EI9;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2100804
    invoke-virtual {p0}, LX/EI9;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2100805
    const v1, 0x7f031243

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2100806
    const v0, 0x7f0d2ae3

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->m:Landroid/view/View;

    .line 2100807
    iget-object v0, p0, LX/EI9;->m:Landroid/view/View;

    new-instance v1, LX/EHx;

    invoke-direct {v1, p0}, LX/EHx;-><init>(LX/EI9;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100808
    const v0, 0x7f0d2add

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->j:Landroid/view/View;

    .line 2100809
    const v0, 0x7f0d2adf

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcFloatingSelfView;

    iput-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    .line 2100810
    const v0, 0x7f0d2ade

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iput-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    .line 2100811
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h()V

    .line 2100812
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v1, p0, LX/EI9;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setPeerName(Ljava/lang/String;)V

    .line 2100813
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setVideoSizeChangedListener(LX/7fE;)V

    .line 2100814
    const v0, 0x7f0d2aed

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->k:Landroid/view/View;

    .line 2100815
    const v0, 0x7f0d2ae2

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->l:Landroid/view/View;

    .line 2100816
    const v0, 0x7f0d2ae6

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->n:Landroid/view/View;

    .line 2100817
    const v0, 0x7f0d2aea

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/EI9;->q:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2100818
    const v0, 0x7f0d2aeb

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EI9;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2100819
    const v0, 0x7f0d2ae4

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->o:Landroid/view/View;

    .line 2100820
    const v0, 0x7f0d2ae8

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/EI9;->r:Lcom/facebook/user/tiles/UserTileView;

    .line 2100821
    const v0, 0x7f0d2ae9

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/EI9;->s:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2100822
    const v0, 0x7f0d2ae0

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->t:LX/4ob;

    .line 2100823
    const v0, 0x7f0d2ae1

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->u:LX/4ob;

    .line 2100824
    iget-object v0, p0, LX/EI9;->d:LX/EFm;

    iget-object v1, p0, LX/EI9;->r:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v0, v1}, LX/EFm;->a(Lcom/facebook/user/tiles/UserTileView;)V

    .line 2100825
    const v0, 0x7f0d2af0

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, LX/EI9;->A:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2100826
    const v0, 0x7f0d2af1

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->B:Landroid/view/View;

    .line 2100827
    iget-object v0, p0, LX/EI9;->B:Landroid/view/View;

    new-instance v1, LX/EHy;

    invoke-direct {v1, p0}, LX/EHy;-><init>(LX/EI9;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100828
    const v0, 0x7f0d1728

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, LX/EI9;->D:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2100829
    iget-object v0, p0, LX/EI9;->D:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EHz;

    invoke-direct {v1, p0}, LX/EHz;-><init>(LX/EI9;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100830
    const v0, 0x7f0d2af2

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->E:Landroid/view/View;

    .line 2100831
    iget-object v0, p0, LX/EI9;->E:Landroid/view/View;

    new-instance v1, LX/EI0;

    invoke-direct {v1, p0}, LX/EI0;-><init>(LX/EI9;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100832
    const v0, 0x7f0d2aee

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->H:Landroid/view/View;

    .line 2100833
    iget-object v0, p0, LX/EI9;->A:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EI1;

    invoke-direct {v1, p0}, LX/EI1;-><init>(LX/EI9;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100834
    const v0, 0x7f0d2ad3

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, LX/EI9;->C:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2100835
    iget-object v0, p0, LX/EI9;->C:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EI2;

    invoke-direct {v1, p0}, LX/EI2;-><init>(LX/EI9;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100836
    const v0, 0x7f0d2af3

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->F:Landroid/view/View;

    .line 2100837
    iget-object v0, p0, LX/EI9;->F:Landroid/view/View;

    new-instance v1, LX/EI3;

    invoke-direct {v1, p0}, LX/EI3;-><init>(LX/EI9;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100838
    const v0, 0x7f0d2af4

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->G:Landroid/view/View;

    .line 2100839
    iget-object v0, p0, LX/EI9;->G:Landroid/view/View;

    new-instance v1, LX/EHo;

    invoke-direct {v1, p0}, LX/EHo;-><init>(LX/EI9;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100840
    const v0, 0x7f0d2aef

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EI9;->i:Landroid/view/View;

    .line 2100841
    iget-object v0, p0, LX/EI9;->i:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 2100842
    new-instance v0, LX/EHp;

    invoke-direct {v0, p0}, LX/EHp;-><init>(LX/EI9;)V

    .line 2100843
    iget-object v1, p0, LX/EI9;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100844
    const v1, 0x7f0d2aec

    invoke-static {p0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/EI9;->h:Landroid/view/View;

    .line 2100845
    iget-object v1, p0, LX/EI9;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2100846
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/EI9;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/EI9;->O:Landroid/graphics/drawable/ColorDrawable;

    .line 2100847
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/EI9;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/EI9;->P:Landroid/graphics/drawable/ColorDrawable;

    .line 2100848
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 2100849
    iget-object v0, p0, LX/EI9;->r:Lcom/facebook/user/tiles/UserTileView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2100850
    iget-object v0, p0, LX/EI9;->s:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2100851
    iget-object v0, p0, LX/EI9;->s:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2100852
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 2100853
    invoke-virtual {p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2100854
    invoke-virtual {p0}, Landroid/view/View;->clearAnimation()V

    .line 2100855
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2100856
    return-void

    .line 2100857
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private a(Lcom/facebook/fbui/glyph/GlyphButton;LX/EI4;)V
    .locals 3

    .prologue
    .line 2100858
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2100859
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2100860
    invoke-virtual {p1}, Lcom/facebook/fbui/glyph/GlyphButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 2100861
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2100862
    :goto_0
    return-void

    .line 2100863
    :cond_0
    sget-object v0, LX/EHw;->c:[I

    invoke-virtual {p2}, LX/EI4;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2100864
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported color!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2100865
    :pswitch_0
    const v1, 0x7f0a0a55

    .line 2100866
    const v0, 0x7f021760

    .line 2100867
    :goto_1
    invoke-virtual {p1, p2}, Lcom/facebook/fbui/glyph/GlyphButton;->setTag(Ljava/lang/Object;)V

    .line 2100868
    invoke-virtual {p0}, LX/EI9;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 2100869
    invoke-virtual {p0}, LX/EI9;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2100870
    :pswitch_1
    const v1, 0x7f0a0a56

    .line 2100871
    const v0, 0x7f021768

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, LX/EI9;

    invoke-static {v4}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v4}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {v4}, LX/EFm;->b(LX/0QB;)LX/EFm;

    move-result-object v3

    check-cast v3, LX/EFm;

    const/16 p0, 0x3257

    invoke-static {v4, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    iput-object v1, p1, LX/EI9;->a:Ljava/util/concurrent/Executor;

    iput-object v2, p1, LX/EI9;->c:LX/0ad;

    iput-object v3, p1, LX/EI9;->d:LX/EFm;

    iput-object v4, p1, LX/EI9;->e:LX/0Ot;

    return-void
.end method

.method private a(ZI)V
    .locals 1

    .prologue
    .line 2100872
    invoke-virtual {p0}, LX/EI9;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/EI9;->a(ZLjava/lang/String;)V

    .line 2100873
    return-void
.end method

.method private a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 2100759
    iget-object v0, p0, LX/EI9;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2100760
    iget-object v0, p0, LX/EI9;->n:Landroid/view/View;

    invoke-static {p0, p1, v0}, LX/EI9;->a$redex0(LX/EI9;ZLandroid/view/View;)V

    .line 2100761
    return-void
.end method

.method private static a(LX/EI9;LX/EI6;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2100874
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v1, LX/EI8;->END_CALL_STATE:LX/EI8;

    if-ne v0, v1, :cond_1

    .line 2100875
    :cond_0
    :goto_0
    return v3

    .line 2100876
    :cond_1
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v1, LX/EI8;->END_CALL_STATE_WITH_RETRY:LX/EI8;

    if-ne v0, v1, :cond_4

    move v1, v2

    .line 2100877
    :goto_1
    sget-object v0, LX/EHw;->b:[I

    invoke-virtual {p1}, LX/EI6;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2100878
    :pswitch_0
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->INCOMING_INSTANT:LX/EI8;

    if-eq v0, v4, :cond_2

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->NONE:LX/EI8;

    if-ne v0, v4, :cond_3

    iget-boolean v0, p0, LX/EI9;->L:Z

    if-eqz v0, :cond_3

    :cond_2
    move v3, v2

    .line 2100879
    :cond_3
    or-int/2addr v3, v1

    .line 2100880
    goto :goto_0

    :cond_4
    move v1, v3

    .line 2100881
    goto :goto_1

    .line 2100882
    :pswitch_1
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->BOTH:LX/EI8;

    if-eq v0, v4, :cond_5

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->PEER:LX/EI8;

    if-eq v0, v4, :cond_5

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->SELF:LX/EI8;

    if-ne v0, v4, :cond_6

    :cond_5
    move v0, v2

    .line 2100883
    :goto_2
    if-nez v1, :cond_7

    :goto_3
    and-int v3, v0, v2

    .line 2100884
    goto :goto_0

    :cond_6
    move v0, v3

    .line 2100885
    goto :goto_2

    :cond_7
    move v2, v3

    .line 2100886
    goto :goto_3

    .line 2100887
    :pswitch_2
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->SELF:LX/EI8;

    if-eq v0, v4, :cond_9

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->BOTH:LX/EI8;

    if-eq v0, v4, :cond_9

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->PEER:LX/EI8;

    if-eq v0, v4, :cond_9

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->OUTGOING_INSTANT:LX/EI8;

    if-eq v0, v4, :cond_9

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->NONE:LX/EI8;

    if-ne v0, v4, :cond_8

    iget-boolean v0, p0, LX/EI9;->L:Z

    if-eqz v0, :cond_9

    :cond_8
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    if-ne v0, v4, :cond_a

    invoke-direct {p0}, LX/EI9;->m()Z

    move-result v0

    if-nez v0, :cond_a

    :cond_9
    move v0, v2

    .line 2100888
    :goto_4
    if-nez v1, :cond_b

    :goto_5
    and-int v3, v0, v2

    .line 2100889
    goto/16 :goto_0

    :cond_a
    move v0, v3

    .line 2100890
    goto :goto_4

    :cond_b
    move v2, v3

    .line 2100891
    goto :goto_5

    .line 2100892
    :pswitch_3
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->I()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->SELF:LX/EI8;

    if-eq v0, v4, :cond_c

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->BOTH:LX/EI8;

    if-eq v0, v4, :cond_c

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->OUTGOING_INSTANT:LX/EI8;

    if-eq v0, v4, :cond_c

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->NONE:LX/EI8;

    if-ne v0, v4, :cond_d

    iget-boolean v0, p0, LX/EI9;->K:Z

    if-nez v0, :cond_d

    iget-boolean v0, p0, LX/EI9;->L:Z

    if-nez v0, :cond_d

    :cond_c
    move v0, v2

    .line 2100893
    :goto_6
    if-nez v1, :cond_e

    :goto_7
    and-int v3, v0, v2

    .line 2100894
    goto/16 :goto_0

    :cond_d
    move v0, v3

    .line 2100895
    goto :goto_6

    :cond_e
    move v2, v3

    .line 2100896
    goto :goto_7

    .line 2100897
    :pswitch_4
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v1, LX/EI8;->HIDDEN:LX/EI8;

    if-eq v0, v1, :cond_10

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v1, LX/EI8;->END_CALL_STATE:LX/EI8;

    if-eq v0, v1, :cond_10

    .line 2100898
    :goto_8
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v1, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    if-ne v0, v1, :cond_f

    invoke-direct {p0}, LX/EI9;->m()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_f
    move v3, v2

    goto/16 :goto_0

    :cond_10
    move v2, v3

    .line 2100899
    goto :goto_8

    .line 2100900
    :pswitch_5
    invoke-direct {p0}, LX/EI9;->m()Z

    move-result v3

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a$redex0(LX/EI9;ZLandroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 2100901
    if-eqz p1, :cond_0

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2100902
    :goto_0
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2100903
    new-instance v1, LX/EHs;

    invoke-direct {v1, p0, p1, p2}, LX/EHs;-><init>(LX/EI9;ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2100904
    invoke-virtual {p2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2100905
    return-void

    .line 2100906
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    goto :goto_0
.end method

.method private static b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2100907
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2100908
    if-nez v0, :cond_0

    .line 2100909
    :goto_0
    return-void

    .line 2100910
    :cond_0
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 2100911
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 2100912
    if-eq v1, v2, :cond_1

    .line 2100913
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    .line 2100914
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2100915
    :cond_2
    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    goto :goto_0
.end method

.method private static b(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 2100916
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2100917
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2100918
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2100919
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2100920
    return-void

    .line 2100921
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method private b(ZZ)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2100922
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setVisibility(I)V

    .line 2100923
    if-nez p1, :cond_2

    .line 2100924
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->c()V

    .line 2100925
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->b()V

    .line 2100926
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i()V

    .line 2100927
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2100928
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2100929
    iget-object v3, v0, LX/EDx;->af:LX/EGE;

    move-object v0, v3

    .line 2100930
    if-eqz v0, :cond_1

    .line 2100931
    invoke-virtual {p0}, LX/EI9;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080700

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v0, v0, LX/EGE;->d:Ljava/lang/String;

    aput-object v0, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2100932
    iget-object v1, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v1, v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a(Ljava/lang/String;)V

    .line 2100933
    :goto_0
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->g()V

    .line 2100934
    :cond_0
    :goto_1
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v1, p0, LX/EI9;->w:Landroid/graphics/Point;

    sget-object v2, LX/EI7;->CENTER:LX/EI7;

    iget v3, p0, LX/EI9;->x:F

    invoke-virtual {v0, v1, v2, v3}, LX/EHF;->a(Landroid/graphics/Point;LX/EI7;F)V

    .line 2100935
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-static {v0}, LX/EI9;->b(Landroid/view/View;)V

    .line 2100936
    return-void

    .line 2100937
    :cond_1
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2100938
    :cond_2
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->d()V

    .line 2100939
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setOneShotDrawListener(LX/7fD;)V

    .line 2100940
    if-nez p2, :cond_0

    .line 2100941
    iget-object v3, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-static {p0}, LX/EI9;->q(LX/EI9;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->b(Z)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method private b(LX/EI8;)Z
    .locals 8

    .prologue
    .line 2100643
    new-instance v2, LX/EI5;

    iget-object v3, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->K()Z

    move-result v4

    iget-object v3, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    .line 2100644
    invoke-virtual {v3}, LX/EDx;->j()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2100645
    iget-object v5, v3, LX/EDx;->af:LX/EGE;

    move-object v5, v5

    .line 2100646
    if-eqz v5, :cond_1

    .line 2100647
    iget-object v5, v3, LX/EDx;->af:LX/EGE;

    move-object v5, v5

    .line 2100648
    iget-boolean v5, v5, LX/EGE;->i:Z

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    :goto_0
    move v5, v5

    .line 2100649
    iget-object v3, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->bk()I

    move-result v6

    iget-object v3, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    .line 2100650
    iget-object v7, v3, LX/EDx;->af:LX/EGE;

    move-object v7, v7

    .line 2100651
    move-object v3, p1

    invoke-direct/range {v2 .. v7}, LX/EI5;-><init>(LX/EI8;ZZILX/EGE;)V

    move-object v0, v2

    .line 2100652
    iget-object v1, p0, LX/EI9;->R:LX/EI5;

    invoke-virtual {v0, v1}, LX/EI5;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2100653
    const/4 v0, 0x0

    .line 2100654
    :goto_1
    return v0

    .line 2100655
    :cond_0
    iget-boolean v1, v0, LX/EI5;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-boolean v1, v0, LX/EI5;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2100656
    iput-object v0, p0, LX/EI9;->R:LX/EI5;

    .line 2100657
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, LX/EDx;->N()Z

    move-result v5

    goto :goto_0
.end method

.method public static c(LX/EI9;Z)V
    .locals 10

    .prologue
    const/16 v8, 0x8

    const v9, 0x3fb33333    # 1.4f

    const/4 v4, 0x1

    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 2100942
    iget-object v0, p0, LX/EI9;->w:Landroid/graphics/Point;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    if-nez v0, :cond_1

    .line 2100943
    :cond_0
    :goto_0
    return-void

    .line 2100944
    :cond_1
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v5

    .line 2100945
    invoke-direct {p0, v5}, LX/EI9;->b(LX/EI8;)Z

    .line 2100946
    sget-object v6, LX/EI7;->TOP_RIGHT:LX/EI7;

    .line 2100947
    iget-object v0, p0, LX/EI9;->n:Landroid/view/View;

    invoke-static {v0, v2}, LX/EI9;->a(Landroid/view/View;Z)V

    .line 2100948
    iget-object v0, p0, LX/EI9;->m:Landroid/view/View;

    invoke-static {v0, v2}, LX/EI9;->a(Landroid/view/View;Z)V

    .line 2100949
    sget-object v0, LX/EI8;->OUTGOING_INSTANT:LX/EI8;

    if-eq v5, v0, :cond_2

    iget-object v0, p0, LX/EI9;->o:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2100950
    iget-object v0, p0, LX/EI9;->o:Landroid/view/View;

    invoke-static {v0, v2}, LX/EI9;->a(Landroid/view/View;Z)V

    .line 2100951
    :cond_2
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2100952
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v0, v0, LX/EI5;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v0, v0, LX/EI5;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2100953
    iget-object v3, p0, LX/EI9;->j:Landroid/view/View;

    sget-object v0, LX/EI8;->HIDDEN:LX/EI8;

    if-ne v5, v0, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2100954
    const/4 v3, 0x0

    .line 2100955
    sget-object v0, LX/EHw;->a:[I

    invoke-virtual {v5}, LX/EI8;->ordinal()I

    move-result v7

    aget v0, v0, v7

    packed-switch v0, :pswitch_data_0

    .line 2100956
    :cond_3
    :goto_2
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2100957
    sget-object v0, LX/EI8;->GROUP_COUNTDOWN:LX/EI8;

    if-ne v5, v0, :cond_16

    .line 2100958
    iget-object v0, p0, LX/EI9;->t:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2100959
    iget-object v0, p0, LX/EI9;->t:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;

    .line 2100960
    iget-boolean v1, v0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->b:Z

    move v1, v1

    .line 2100961
    if-nez v1, :cond_18

    .line 2100962
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->b(Z)V

    .line 2100963
    iget-object v1, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 2100964
    iget v2, v1, LX/EDx;->ca:I

    move v1, v2

    .line 2100965
    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->setDuration(I)V

    .line 2100966
    new-instance v1, LX/EHv;

    invoke-direct {v1, p0}, LX/EHv;-><init>(LX/EI9;)V

    .line 2100967
    iput-object v1, v0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->g:LX/EHM;

    .line 2100968
    iget-object v1, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 2100969
    iget-boolean v2, v1, LX/EDx;->ai:Z

    move v1, v2

    .line 2100970
    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a(Z)V

    .line 2100971
    :cond_4
    :goto_3
    invoke-static {p0}, LX/EI9;->s(LX/EI9;)V

    .line 2100972
    :cond_5
    iget-object v0, p0, LX/EI9;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 2100973
    goto :goto_1

    .line 2100974
    :pswitch_0
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setVisibility(I)V

    .line 2100975
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a()V

    .line 2100976
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v6, p0, LX/EI9;->w:Landroid/graphics/Point;

    sget-object v7, LX/EI7;->CENTER:LX/EI7;

    iget v8, p0, LX/EI9;->x:F

    invoke-virtual {v0, v6, v7, v8}, LX/EHF;->a(Landroid/graphics/Point;LX/EI7;F)V

    .line 2100977
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v6, p0, LX/EI9;->J:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setPeerName(Ljava/lang/String;)V

    .line 2100978
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setVisibility(I)V

    .line 2100979
    iget-boolean v0, p0, LX/EI9;->L:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, LX/EI9;->K:Z

    if-nez v0, :cond_7

    .line 2100980
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    iget-object v2, p0, LX/EI9;->w:Landroid/graphics/Point;

    sget-object v6, LX/EI7;->CENTER:LX/EI7;

    invoke-virtual {v0, v2, v6, v9}, LX/EHF;->a(Landroid/graphics/Point;LX/EI7;F)V

    .line 2100981
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->b()V

    .line 2100982
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->c()V

    .line 2100983
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, v4}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setTimeOverlayColor(Z)V

    .line 2100984
    invoke-direct {p0}, LX/EI9;->n()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2100985
    invoke-static {p0}, LX/EI9;->o(LX/EI9;)V

    .line 2100986
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setVisibility(I)V

    move-object v0, v3

    .line 2100987
    :goto_4
    iget-object v1, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-static {v1}, LX/EI9;->b(Landroid/view/View;)V

    move-object v3, v0

    .line 2100988
    goto/16 :goto_2

    .line 2100989
    :cond_7
    iget-object v0, p0, LX/EI9;->O:Landroid/graphics/drawable/ColorDrawable;

    .line 2100990
    iget-object v2, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v2, v1}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setVisibility(I)V

    goto :goto_4

    .line 2100991
    :pswitch_1
    invoke-static {p0}, LX/EI9;->o(LX/EI9;)V

    .line 2100992
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v0, v0, LX/EI5;->c:Z

    invoke-direct {p0, v0, p1}, LX/EI9;->b(ZZ)V

    .line 2100993
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v0, v0, LX/EI5;->c:Z

    if-nez v0, :cond_8

    .line 2100994
    iget-object v3, p0, LX/EI9;->P:Landroid/graphics/drawable/ColorDrawable;

    .line 2100995
    :cond_8
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2100996
    :pswitch_2
    invoke-static {p0}, LX/EI9;->o(LX/EI9;)V

    .line 2100997
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v0, v0, LX/EI5;->c:Z

    invoke-direct {p0, v0, p1}, LX/EI9;->b(ZZ)V

    .line 2100998
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setVisibility(I)V

    .line 2100999
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    .line 2101000
    iget v2, p0, LX/EI9;->S:I

    .line 2101001
    invoke-virtual {p0}, LX/EI9;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    float-to-int v4, v4

    mul-int/2addr v4, v2

    move v4, v4

    .line 2101002
    iget-object v2, p0, LX/EI9;->w:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v7, p0, LX/EI9;->w:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->x:I

    if-le v2, v7, :cond_19

    .line 2101003
    mul-int/lit8 v2, v4, 0x4

    div-int/lit8 v2, v2, 0x3

    .line 2101004
    :goto_5
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7, v4, v2}, Landroid/graphics/Point;-><init>(II)V

    move-object v2, v7

    .line 2101005
    invoke-virtual {v0, v2, v6, v9}, LX/EHF;->a(Landroid/graphics/Point;LX/EI7;F)V

    .line 2101006
    if-nez p1, :cond_9

    .line 2101007
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->b()V

    .line 2101008
    :cond_9
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v0, v0, LX/EI5;->b:Z

    if-eqz v0, :cond_a

    .line 2101009
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->c()V

    goto/16 :goto_2

    .line 2101010
    :cond_a
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2101011
    :pswitch_3
    iget-boolean v0, p0, LX/EI9;->M:Z

    if-nez v0, :cond_b

    .line 2101012
    iget-object v0, p0, LX/EI9;->m:Landroid/view/View;

    invoke-static {p0, v4, v0}, LX/EI9;->a$redex0(LX/EI9;ZLandroid/view/View;)V

    .line 2101013
    :cond_b
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setVisibility(I)V

    .line 2101014
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setVisibility(I)V

    .line 2101015
    if-eqz p1, :cond_c

    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v0, v0, LX/EI5;->c:Z

    if-nez v0, :cond_f

    .line 2101016
    :cond_c
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-static {p0}, LX/EI9;->q(LX/EI9;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v1, v1, LX/EI5;->c:Z

    if-nez v1, :cond_e

    :cond_d
    move v2, v4

    :cond_e
    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->b(Z)V

    .line 2101017
    :cond_f
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v1, p0, LX/EI9;->w:Landroid/graphics/Point;

    sget-object v2, LX/EI7;->CENTER:LX/EI7;

    iget v4, p0, LX/EI9;->x:F

    invoke-virtual {v0, v1, v2, v4}, LX/EHF;->a(Landroid/graphics/Point;LX/EI7;F)V

    .line 2101018
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v0, v0, LX/EI5;->c:Z

    if-eqz v0, :cond_3

    .line 2101019
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setOneShotDrawListener(LX/7fD;)V

    goto/16 :goto_2

    .line 2101020
    :pswitch_4
    invoke-static {p0}, LX/EI9;->o(LX/EI9;)V

    .line 2101021
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setVisibility(I)V

    .line 2101022
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    iget-object v4, p0, LX/EI9;->w:Landroid/graphics/Point;

    sget-object v6, LX/EI7;->CENTER:LX/EI7;

    invoke-virtual {v0, v4, v6, v9}, LX/EHF;->a(Landroid/graphics/Point;LX/EI7;F)V

    .line 2101023
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->b()V

    .line 2101024
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-boolean v0, v0, LX/EI5;->b:Z

    if-eqz v0, :cond_11

    .line 2101025
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->c()V

    .line 2101026
    :goto_6
    sget-object v0, LX/EI8;->OUTGOING_INSTANT:LX/EI8;

    if-ne v5, v0, :cond_10

    iget-object v0, p0, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    if-nez v0, :cond_10

    .line 2101027
    iget-object v0, p0, LX/EI9;->q:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2101028
    :cond_10
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setVisibility(I)V

    .line 2101029
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->c()V

    goto/16 :goto_2

    .line 2101030
    :cond_11
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->a()V

    goto :goto_6

    .line 2101031
    :pswitch_5
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->c()V

    .line 2101032
    iget-object v0, p0, LX/EI9;->t:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101033
    iget-boolean v1, v0, LX/EDx;->bZ:Z

    move v0, v1

    .line 2101034
    if-eqz v0, :cond_3

    .line 2101035
    iget-object v0, p0, LX/EI9;->t:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a()V

    goto/16 :goto_2

    .line 2101036
    :pswitch_6
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2101037
    iget-object v0, p0, LX/EI9;->P:Landroid/graphics/drawable/ColorDrawable;

    .line 2101038
    :goto_7
    iget-object v1, p0, LX/EI9;->n:Landroid/view/View;

    if-eqz v1, :cond_12

    .line 2101039
    iget-object v1, p0, LX/EI9;->n:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2101040
    :cond_12
    iget-object v1, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v1, v2}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setVisibility(I)V

    .line 2101041
    iget-object v1, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v3, p0, LX/EI9;->w:Landroid/graphics/Point;

    sget-object v6, LX/EI7;->CENTER:LX/EI7;

    iget v7, p0, LX/EI9;->x:F

    invoke-virtual {v1, v3, v6, v7}, LX/EHF;->a(Landroid/graphics/Point;LX/EI7;F)V

    .line 2101042
    iget-object v3, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    sget-object v1, LX/EI8;->END_CALL_STATE_WITH_RETRY:LX/EI8;

    if-ne v5, v1, :cond_14

    move v1, v4

    :goto_8
    invoke-virtual {v3, v1}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a(Z)V

    .line 2101043
    iget-object v1, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v1, v8}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setVisibility(I)V

    .line 2101044
    iget-object v1, p0, LX/EI9;->H:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 2101045
    sget-object v1, LX/EI8;->END_CALL_STATE:LX/EI8;

    if-eq v5, v1, :cond_15

    :goto_9
    invoke-static {p0, v4}, LX/EI9;->setButtonsHolderVisibility(LX/EI9;Z)V

    move-object v3, v0

    .line 2101046
    goto/16 :goto_2

    .line 2101047
    :cond_13
    iget-object v0, p0, LX/EI9;->O:Landroid/graphics/drawable/ColorDrawable;

    goto :goto_7

    :cond_14
    move v1, v2

    .line 2101048
    goto :goto_8

    :cond_15
    move v4, v2

    .line 2101049
    goto :goto_9

    .line 2101050
    :pswitch_7
    iget-object v3, p0, LX/EI9;->O:Landroid/graphics/drawable/ColorDrawable;

    .line 2101051
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setVisibility(I)V

    .line 2101052
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a()V

    .line 2101053
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v1, p0, LX/EI9;->w:Landroid/graphics/Point;

    sget-object v2, LX/EI7;->CENTER:LX/EI7;

    iget v6, p0, LX/EI9;->x:F

    invoke-virtual {v0, v1, v2, v6}, LX/EHF;->a(Landroid/graphics/Point;LX/EI7;F)V

    .line 2101054
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0, v8}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setVisibility(I)V

    .line 2101055
    iget-object v0, p0, LX/EI9;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 2101056
    invoke-static {p0, v4}, LX/EI9;->setButtonsHolderVisibility(LX/EI9;Z)V

    goto/16 :goto_2

    .line 2101057
    :cond_16
    iget-object v0, p0, LX/EI9;->t:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    goto/16 :goto_3

    :cond_17
    move-object v0, v3

    goto/16 :goto_4

    .line 2101058
    :cond_18
    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->d()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2101059
    iget-object v1, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 2101060
    iget-boolean v2, v1, LX/EDx;->ai:Z

    move v1, v2

    .line 2101061
    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a(Z)V

    goto/16 :goto_3

    .line 2101062
    :cond_19
    mul-int/lit8 v2, v4, 0x4

    div-int/lit8 v2, v2, 0x3

    move v8, v4

    move v4, v2

    move v2, v8

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 2101063
    invoke-virtual {p0}, LX/EI9;->j()Z

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    .line 2101064
    :goto_0
    invoke-virtual {p0}, LX/EI9;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v0, :cond_1

    const v0, 0x7f0400c1

    :goto_1
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2101065
    new-instance v1, LX/EHr;

    invoke-direct {v1, p0, p1}, LX/EHr;-><init>(LX/EI9;Z)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2101066
    iget-object v1, p0, LX/EI9;->H:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2101067
    return-void

    .line 2101068
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2101069
    :cond_1
    const v0, 0x7f0400c0

    goto :goto_1
.end method

.method private e(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 2101070
    iget-object v0, p0, LX/EI9;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2101071
    iget-object v0, p0, LX/EI9;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 2101072
    iget-object v0, p0, LX/EI9;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2101073
    :cond_0
    invoke-virtual {p0}, LX/EI9;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0400bc

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2101074
    new-instance v1, LX/EHt;

    invoke-direct {v1, p0}, LX/EHt;-><init>(LX/EI9;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2101075
    if-eqz p1, :cond_1

    .line 2101076
    iget-object v1, p0, LX/EI9;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2101077
    :goto_0
    return-void

    .line 2101078
    :cond_1
    iget-object v0, p0, LX/EI9;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 2101079
    iget-object v0, p0, LX/EI9;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private m()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2101080
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v1

    sget-object v2, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/EI9;->c:LX/0ad;

    sget-short v2, LX/3Dx;->z:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 2101081
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101082
    iget-boolean v1, v0, LX/EDx;->aB:Z

    move v0, v1

    .line 2101083
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->af()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(LX/EI9;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2100762
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2100763
    iget-object v0, p0, LX/EI9;->r:Lcom/facebook/user/tiles/UserTileView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2100764
    iget-object v0, p0, LX/EI9;->s:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2100765
    sget-object v0, LX/EHw;->a:[I

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v1

    invoke-virtual {v1}, LX/EI8;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2100766
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2100767
    :pswitch_1
    invoke-direct {p0}, LX/EI9;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2100768
    const v0, 0x7f08072d

    invoke-direct {p0, v2, v0}, LX/EI9;->a(ZI)V

    goto :goto_0

    .line 2100769
    :pswitch_2
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2100770
    iget-object v0, p0, LX/EI9;->d:LX/EFm;

    iget-object v1, p0, LX/EI9;->r:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v0, v1}, LX/EFm;->a(Lcom/facebook/user/tiles/UserTileView;)V

    .line 2100771
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-object v1, v0, LX/EI5;->e:LX/EGE;

    .line 2100772
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2100773
    iget-boolean v3, v0, LX/EDx;->ag:Z

    move v0, v3

    .line 2100774
    if-eqz v0, :cond_1

    .line 2100775
    const v0, 0x7f020d05

    invoke-direct {p0, v0}, LX/EI9;->a(I)V

    .line 2100776
    :cond_1
    if-nez v1, :cond_2

    const-string v0, ""

    :goto_1
    invoke-direct {p0, v2, v0}, LX/EI9;->a(ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, v1, LX/EGE;->d:Ljava/lang/String;

    goto :goto_1

    .line 2100777
    :pswitch_3
    iget-boolean v0, p0, LX/EI9;->N:Z

    if-eqz v0, :cond_4

    .line 2100778
    iget-object v0, p0, LX/EI9;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 2100779
    const/4 v0, 0x0

    iget-object v1, p0, LX/EI9;->o:Landroid/view/View;

    invoke-static {p0, v0, v1}, LX/EI9;->a$redex0(LX/EI9;ZLandroid/view/View;)V

    .line 2100780
    :cond_3
    const v0, 0x7f0807eb

    invoke-direct {p0, v2, v0}, LX/EI9;->a(ZI)V

    goto :goto_0

    .line 2100781
    :cond_4
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v1, LX/EI8;->OUTGOING_INSTANT:LX/EI8;

    if-ne v0, v1, :cond_5

    .line 2100782
    iget-object v0, p0, LX/EI9;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2100783
    iget-object v0, p0, LX/EI9;->o:Landroid/view/View;

    invoke-static {p0, v2, v0}, LX/EI9;->a$redex0(LX/EI9;ZLandroid/view/View;)V

    goto :goto_0

    .line 2100784
    :cond_5
    iget-object v0, p0, LX/EI9;->d:LX/EFm;

    iget-object v1, p0, LX/EI9;->r:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v0, v1}, LX/EFm;->a(Lcom/facebook/user/tiles/UserTileView;)V

    .line 2100785
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2100786
    const v0, 0x7f020d48

    invoke-direct {p0, v0}, LX/EI9;->a(I)V

    .line 2100787
    const v0, 0x7f08080b

    invoke-direct {p0, v2, v0}, LX/EI9;->a(ZI)V

    goto/16 :goto_0

    .line 2100788
    :cond_6
    const v0, 0x7f0807eb

    invoke-direct {p0, v2, v0}, LX/EI9;->a(ZI)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static q(LX/EI9;)Z
    .locals 4

    .prologue
    .line 2100613
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->getLastRedrawTime()J

    move-result-wide v0

    .line 2100614
    iget-object v2, p0, LX/EI9;->T:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/16 v2, 0x6d6

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(LX/EI9;)V
    .locals 3

    .prologue
    .line 2100615
    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bo()LX/EGp;

    move-result-object v1

    .line 2100616
    iget-object v0, p0, LX/EI9;->u:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->b()Z

    move-result v2

    .line 2100617
    if-nez v1, :cond_1

    .line 2100618
    iget-object v0, p0, LX/EI9;->u:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2100619
    :cond_0
    :goto_0
    return-void

    .line 2100620
    :cond_1
    iget-object v0, p0, LX/EI9;->u:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/GroupRingNoticeView;->setModeAndShow(LX/EGp;)V

    .line 2100621
    if-nez v2, :cond_0

    .line 2100622
    iget-object v0, p0, LX/EI9;->u:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    new-instance v1, LX/EHu;

    invoke-direct {v1, p0}, LX/EHu;-><init>(LX/EI9;)V

    .line 2100623
    iput-object v1, v0, Lcom/facebook/rtc/views/GroupRingNoticeView;->a:LX/EGo;

    .line 2100624
    iget-object v0, p0, LX/EI9;->u:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->a()V

    goto :goto_0
.end method

.method public static setButtonsHolderVisibility(LX/EI9;Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2100625
    iget-object v3, p0, LX/EI9;->H:Landroid/view/View;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2100626
    if-eqz p1, :cond_3

    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->af()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->Y()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 2100627
    :goto_1
    iget-object v3, p0, LX/EI9;->h:Landroid/view/View;

    if-eqz v0, :cond_4

    :goto_2
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2100628
    if-eqz v0, :cond_0

    .line 2100629
    iget-object v0, p0, LX/EI9;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 2100630
    :cond_0
    iget-object v0, p0, LX/EI9;->V:LX/EGd;

    if-eqz v0, :cond_1

    .line 2100631
    iget-object v0, p0, LX/EI9;->V:LX/EGd;

    invoke-interface {v0, p1}, LX/EGd;->b(Z)V

    .line 2100632
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 2100633
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2100634
    goto :goto_1

    :cond_4
    move v1, v2

    .line 2100635
    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2100636
    iget-object v0, p0, LX/EI9;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;-><init>(LX/EI9;)V

    const v2, 0x4c279e89    # 4.3940388E7f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2100637
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 2100638
    iget-object v0, p0, LX/EI9;->Q:LX/7fE;

    if-eqz v0, :cond_0

    .line 2100639
    iget-object v0, p0, LX/EI9;->Q:LX/7fE;

    invoke-interface {v0, p1, p2}, LX/7fE;->a(II)V

    .line 2100640
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2100641
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, p1}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a(Ljava/lang/String;)V

    .line 2100642
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2100658
    if-eqz p1, :cond_0

    .line 2100659
    iget-object v0, p0, LX/EI9;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2100660
    iget-object v0, p0, LX/EI9;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 2100661
    :goto_0
    return-void

    .line 2100662
    :cond_0
    iget-object v0, p0, LX/EI9;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2100663
    sget-object v0, LX/EI6;->ACCEPT:LX/EI6;

    invoke-static {p0, v0}, LX/EI9;->a(LX/EI9;LX/EI6;)Z

    move-result v0

    .line 2100664
    sget-object v2, LX/EI6;->MUTE:LX/EI6;

    invoke-static {p0, v2}, LX/EI9;->a(LX/EI9;LX/EI6;)Z

    move-result v2

    .line 2100665
    sget-object v3, LX/EI6;->SWITCH_CAMERA:LX/EI6;

    invoke-static {p0, v3}, LX/EI9;->a(LX/EI9;LX/EI6;)Z

    move-result v3

    .line 2100666
    sget-object v4, LX/EI6;->TOGGLE_VIDEO:LX/EI6;

    invoke-static {p0, v4}, LX/EI9;->a(LX/EI9;LX/EI6;)Z

    move-result v4

    .line 2100667
    sget-object v5, LX/EI6;->NEW_VOICEMAIL_BUTTONS:LX/EI6;

    invoke-static {p0, v5}, LX/EI9;->a(LX/EI9;LX/EI6;)Z

    move-result v5

    .line 2100668
    sget-object v6, LX/EI6;->DECLINE:LX/EI6;

    invoke-static {p0, v6}, LX/EI9;->a(LX/EI9;LX/EI6;)Z

    move-result v6

    .line 2100669
    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v1

    const/4 v8, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x5

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    .line 2100670
    iget-object v7, p0, LX/EI9;->A:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-static {v7, v0}, LX/EI9;->b(Landroid/view/View;Z)V

    .line 2100671
    if-eqz v0, :cond_0

    .line 2100672
    iget-boolean v0, p0, LX/EI9;->K:Z

    if-eqz v0, :cond_3

    .line 2100673
    iget-object v0, p0, LX/EI9;->A:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {p0}, LX/EI9;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020f71

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2100674
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EI9;->C:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-static {v0, v6}, LX/EI9;->b(Landroid/view/View;Z)V

    .line 2100675
    iget-object v0, p0, LX/EI9;->D:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-static {v0, v2}, LX/EI9;->b(Landroid/view/View;Z)V

    .line 2100676
    iget-object v0, p0, LX/EI9;->D:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphButton;->setSelected(Z)V

    .line 2100677
    if-eqz v2, :cond_1

    .line 2100678
    iget-object v2, p0, LX/EI9;->D:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v6, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    if-ne v0, v6, :cond_4

    sget-object v0, LX/EI4;->BLUE:LX/EI4;

    :goto_1
    invoke-direct {p0, v2, v0}, LX/EI9;->a(Lcom/facebook/fbui/glyph/GlyphButton;LX/EI4;)V

    .line 2100679
    :cond_1
    iget-object v0, p0, LX/EI9;->E:Landroid/view/View;

    invoke-static {v0, v3}, LX/EI9;->b(Landroid/view/View;Z)V

    .line 2100680
    iget-object v0, p0, LX/EI9;->B:Landroid/view/View;

    invoke-static {v0, v4}, LX/EI9;->b(Landroid/view/View;Z)V

    .line 2100681
    iget-object v0, p0, LX/EI9;->B:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setSelected(Z)V

    .line 2100682
    iget-object v0, p0, LX/EI9;->F:Landroid/view/View;

    invoke-static {v0, v5}, LX/EI9;->b(Landroid/view/View;Z)V

    .line 2100683
    iget-object v0, p0, LX/EI9;->G:Landroid/view/View;

    invoke-static {v0, v5}, LX/EI9;->b(Landroid/view/View;Z)V

    .line 2100684
    if-eqz v5, :cond_2

    .line 2100685
    iget-object v2, p0, LX/EI9;->G:Landroid/view/View;

    iget-object v0, p0, LX/EI9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->au()Z

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 2100686
    :cond_2
    iget-object v2, p0, LX/EI9;->i:Landroid/view/View;

    invoke-direct {p0}, LX/EI9;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2100687
    return-void

    .line 2100688
    :cond_3
    iget-object v0, p0, LX/EI9;->A:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {p0}, LX/EI9;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020d48

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2100689
    :cond_4
    sget-object v0, LX/EI4;->DARK:LX/EI4;

    goto :goto_1

    .line 2100690
    :cond_5
    const/16 v0, 0x8

    goto :goto_2
.end method

.method public final a(ZZZZ)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2100691
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v1

    sget-object v2, LX/EI8;->NONE:LX/EI8;

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v1

    sget-object v2, LX/EI8;->END_CALL_STATE_WITH_RETRY:LX/EI8;

    if-ne v1, v2, :cond_1

    .line 2100692
    :cond_0
    const/4 p1, 0x1

    move p3, v0

    move p2, v0

    .line 2100693
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p0}, LX/EI9;->j()Z

    move-result v1

    if-eq v1, p1, :cond_b

    .line 2100694
    :cond_2
    iget-object v1, p0, LX/EI9;->H:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2100695
    iget-object v1, p0, LX/EI9;->H:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 2100696
    :cond_3
    if-nez p2, :cond_7

    .line 2100697
    invoke-static {p0, p1}, LX/EI9;->setButtonsHolderVisibility(LX/EI9;Z)V

    .line 2100698
    iget-object v1, p0, LX/EI9;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2100699
    iget-object v1, p0, LX/EI9;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 2100700
    :cond_4
    iget-object v1, p0, LX/EI9;->l:Landroid/view/View;

    if-eqz p4, :cond_6

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2100701
    :cond_5
    :goto_1
    return-void

    .line 2100702
    :cond_6
    const/16 v0, 0x8

    goto :goto_0

    .line 2100703
    :cond_7
    if-eqz p3, :cond_a

    if-eqz p1, :cond_a

    .line 2100704
    invoke-direct {p0, p1}, LX/EI9;->d(Z)V

    .line 2100705
    :cond_8
    :goto_2
    if-eqz p1, :cond_d

    if-eqz p4, :cond_d

    iget-object v1, p0, LX/EI9;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, LX/EI9;->u:LX/4ob;

    invoke-virtual {v1}, LX/4ob;->b()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, LX/EI9;->u:LX/4ob;

    invoke-virtual {v1}, LX/4ob;->c()Z

    move-result v1

    if-nez v1, :cond_d

    .line 2100706
    :cond_9
    invoke-direct {p0, p4}, LX/EI9;->e(Z)V

    goto :goto_1

    .line 2100707
    :cond_a
    iget-object v1, p0, LX/EI9;->H:Landroid/view/View;

    invoke-static {p0, p1, v1}, LX/EI9;->a$redex0(LX/EI9;ZLandroid/view/View;)V

    goto :goto_2

    .line 2100708
    :cond_b
    invoke-virtual {p0}, LX/EI9;->j()Z

    move-result v1

    if-ne v1, p1, :cond_8

    .line 2100709
    if-nez p2, :cond_c

    .line 2100710
    invoke-static {p0, p1}, LX/EI9;->setButtonsHolderVisibility(LX/EI9;Z)V

    goto :goto_1

    .line 2100711
    :cond_c
    if-eqz p3, :cond_8

    if-eqz p1, :cond_8

    .line 2100712
    invoke-direct {p0, p1}, LX/EI9;->d(Z)V

    goto :goto_2

    .line 2100713
    :cond_d
    if-nez p4, :cond_5

    .line 2100714
    invoke-direct {p0, v0}, LX/EI9;->e(Z)V

    goto :goto_1
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2100715
    iget-boolean v0, p0, LX/EI9;->z:Z

    if-eqz v0, :cond_0

    .line 2100716
    iget-object v0, p0, LX/EI9;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2100717
    iget v1, p0, LX/EI9;->y:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2100718
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EI9;->z:Z

    .line 2100719
    iget-object v1, p0, LX/EI9;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2100720
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setAlpha(F)V

    .line 2100721
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2100722
    iget-boolean v0, p0, LX/EI9;->z:Z

    if-nez v0, :cond_0

    .line 2100723
    iget-object v0, p0, LX/EI9;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2100724
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v1, p0, LX/EI9;->y:I

    .line 2100725
    const/16 v1, -0x1388

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2100726
    iget-object v1, p0, LX/EI9;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2100727
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setAlpha(F)V

    .line 2100728
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EI9;->z:Z

    .line 2100729
    :cond_0
    return-void
.end method

.method public getExpressionButton()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2100730
    iget-object v0, p0, LX/EI9;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2100731
    iget-object v0, p0, LX/EI9;->h:Landroid/view/View;

    .line 2100732
    :goto_0
    return-object v0

    .line 2100733
    :cond_0
    iget-object v0, p0, LX/EI9;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2100734
    iget-object v0, p0, LX/EI9;->i:Landroid/view/View;

    goto :goto_0

    .line 2100735
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPeerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2100736
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    .line 2100737
    iget-object p0, v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    move-object v0, p0

    .line 2100738
    return-object v0
.end method

.method public getSelfTextureView()Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 2100739
    iget-object v0, p0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    .line 2100740
    iget-object p0, v0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    move-object v0, p0

    .line 2100741
    return-object v0
.end method

.method public getViewType()LX/EI8;
    .locals 1

    .prologue
    .line 2100742
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EI9;->R:LX/EI5;

    iget-object v0, v0, LX/EI5;->a:LX/EI8;

    goto :goto_0
.end method

.method public final h()I
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2100743
    invoke-static {}, LX/EI6;->values()[LX/EI6;

    move-result-object v5

    array-length v6, v5

    move v4, v2

    move v3, v2

    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v0, v5, v4

    .line 2100744
    invoke-static {p0, v0}, LX/EI9;->a(LX/EI9;LX/EI6;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    add-int/2addr v3, v0

    .line 2100745
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 2100746
    goto :goto_1

    .line 2100747
    :cond_1
    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->NONE:LX/EI8;

    if-eq v0, v4, :cond_2

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->INCOMING_INSTANT:LX/EI8;

    if-eq v0, v4, :cond_2

    invoke-virtual {p0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v4, LX/EI8;->OUTGOING_INSTANT:LX/EI8;

    if-ne v0, v4, :cond_3

    :cond_2
    move v2, v1

    .line 2100748
    :cond_3
    if-eq v3, v1, :cond_4

    const/4 v0, 0x3

    if-ne v3, v0, :cond_5

    if-nez v2, :cond_5

    .line 2100749
    :cond_4
    add-int/lit8 v0, v3, 0x1

    .line 2100750
    :goto_2
    return v0

    :cond_5
    move v0, v3

    goto :goto_2
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2100751
    iget-object v0, p0, LX/EI9;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPeerName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2100752
    iput-object p1, p0, LX/EI9;->J:Ljava/lang/String;

    .line 2100753
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    if-eqz v0, :cond_0

    .line 2100754
    iget-object v0, p0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0, p1}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setPeerName(Ljava/lang/String;)V

    .line 2100755
    :cond_0
    return-void
.end method

.method public setViewType(LX/EI8;)V
    .locals 1

    .prologue
    .line 2100756
    invoke-direct {p0, p1}, LX/EI9;->b(LX/EI8;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2100757
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EI9;->c(LX/EI9;Z)V

    .line 2100758
    :cond_0
    return-void
.end method
