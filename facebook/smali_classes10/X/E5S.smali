.class public final LX/E5S;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5T;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/text/SpannableStringBuilder;

.field public b:Z

.field public c:LX/174;

.field public d:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public final synthetic e:LX/E5T;


# direct methods
.method public constructor <init>(LX/E5T;)V
    .locals 1

    .prologue
    .line 2078401
    iput-object p1, p0, LX/E5S;->e:LX/E5T;

    .line 2078402
    move-object v0, p1

    .line 2078403
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2078404
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078405
    const-string v0, "ReactionTruncatedParagraphUnitComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2078406
    if-ne p0, p1, :cond_1

    .line 2078407
    :cond_0
    :goto_0
    return v0

    .line 2078408
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078409
    goto :goto_0

    .line 2078410
    :cond_3
    check-cast p1, LX/E5S;

    .line 2078411
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078412
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078413
    if-eq v2, v3, :cond_0

    .line 2078414
    iget-object v2, p0, LX/E5S;->a:Landroid/text/SpannableStringBuilder;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E5S;->a:Landroid/text/SpannableStringBuilder;

    iget-object v3, p1, LX/E5S;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2078415
    goto :goto_0

    .line 2078416
    :cond_5
    iget-object v2, p1, LX/E5S;->a:Landroid/text/SpannableStringBuilder;

    if-nez v2, :cond_4

    .line 2078417
    :cond_6
    iget-boolean v2, p0, LX/E5S;->b:Z

    iget-boolean v3, p1, LX/E5S;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2078418
    goto :goto_0

    .line 2078419
    :cond_7
    iget-object v2, p0, LX/E5S;->c:LX/174;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/E5S;->c:LX/174;

    iget-object v3, p1, LX/E5S;->c:LX/174;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 2078420
    goto :goto_0

    .line 2078421
    :cond_9
    iget-object v2, p1, LX/E5S;->c:LX/174;

    if-nez v2, :cond_8

    .line 2078422
    :cond_a
    iget-object v2, p0, LX/E5S;->d:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E5S;->d:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iget-object v3, p1, LX/E5S;->d:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2078423
    goto :goto_0

    .line 2078424
    :cond_b
    iget-object v2, p1, LX/E5S;->d:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
