.class public LX/Ey5;
.super LX/3x6;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0hL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0hL;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2185761
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 2185762
    iput-object p1, p0, LX/Ey5;->a:Landroid/content/res/Resources;

    .line 2185763
    iput-object p2, p0, LX/Ey5;->b:LX/0hL;

    .line 2185764
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2185765
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    invoke-virtual {v0}, LX/1a3;->f()I

    move-result v0

    .line 2185766
    if-eqz v0, :cond_0

    if-ne v0, v3, :cond_3

    .line 2185767
    :cond_0
    const/4 v1, 0x0

    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 2185768
    :goto_0
    rem-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_1

    iget-object v1, p0, LX/Ey5;->b:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_4

    iget-object v0, p0, LX/Ey5;->b:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2185769
    :cond_2
    iget-object v0, p0, LX/Ey5;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0f7b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 2185770
    :goto_1
    return-void

    .line 2185771
    :cond_3
    iget-object v1, p0, LX/Ey5;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b0f7c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0

    .line 2185772
    :cond_4
    iget-object v0, p0, LX/Ey5;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0f7b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    goto :goto_1
.end method
