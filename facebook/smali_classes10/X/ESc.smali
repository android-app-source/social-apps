.class public final enum LX/ESc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ESc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ESc;

.field public static final enum HIDDEN:LX/ESc;

.field public static final enum VISIBLE:LX/ESc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2123201
    new-instance v0, LX/ESc;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v2}, LX/ESc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESc;->VISIBLE:LX/ESc;

    .line 2123202
    new-instance v0, LX/ESc;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v3}, LX/ESc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESc;->HIDDEN:LX/ESc;

    .line 2123203
    const/4 v0, 0x2

    new-array v0, v0, [LX/ESc;

    sget-object v1, LX/ESc;->VISIBLE:LX/ESc;

    aput-object v1, v0, v2

    sget-object v1, LX/ESc;->HIDDEN:LX/ESc;

    aput-object v1, v0, v3

    sput-object v0, LX/ESc;->$VALUES:[LX/ESc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2123204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ESc;
    .locals 1

    .prologue
    .line 2123205
    const-class v0, LX/ESc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ESc;

    return-object v0
.end method

.method public static values()[LX/ESc;
    .locals 1

    .prologue
    .line 2123206
    sget-object v0, LX/ESc;->$VALUES:[LX/ESc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ESc;

    return-object v0
.end method
