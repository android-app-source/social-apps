.class public LX/ENZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ENa;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ENZ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ENa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112838
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2112839
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ENZ;->b:LX/0Zi;

    .line 2112840
    iput-object p1, p0, LX/ENZ;->a:LX/0Ot;

    .line 2112841
    return-void
.end method

.method public static a(LX/0QB;)LX/ENZ;
    .locals 4

    .prologue
    .line 2112842
    const-class v1, LX/ENZ;

    monitor-enter v1

    .line 2112843
    :try_start_0
    sget-object v0, LX/ENZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112844
    sput-object v2, LX/ENZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112845
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112846
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112847
    new-instance v3, LX/ENZ;

    const/16 p0, 0x3445

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ENZ;-><init>(LX/0Ot;)V

    .line 2112848
    move-object v0, v3

    .line 2112849
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112850
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112851
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112852
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2112853
    check-cast p2, LX/ENY;

    .line 2112854
    iget-object v0, p0, LX/ENZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ENa;

    iget-object v1, p2, LX/ENY;->a:LX/CzL;

    iget-object v2, p2, LX/ENY;->b:LX/1Ps;

    const/4 v8, 0x0

    .line 2112855
    iget-object v3, v1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v3, v3

    .line 2112856
    invoke-static {v3}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2112857
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v4

    .line 2112858
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v4

    .line 2112859
    :goto_0
    iget-object v5, v1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v5, v5

    .line 2112860
    invoke-static {v4, v5}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v6

    .line 2112861
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v3

    .line 2112862
    iget-object v5, v1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v5, v5

    .line 2112863
    invoke-static {v3, v5}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v7

    .line 2112864
    iget-object v3, v0, LX/ENa;->f:LX/0Uh;

    const/16 v5, 0x3ce

    invoke-virtual {v3, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_3

    const v3, -0xb1a99b

    move v5, v3

    .line 2112865
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    iget-object v8, v0, LX/ENa;->h:LX/ENd;

    const/4 v9, 0x0

    .line 2112866
    new-instance p0, LX/ENc;

    invoke-direct {p0, v8}, LX/ENc;-><init>(LX/ENd;)V

    .line 2112867
    iget-object p2, v8, LX/ENd;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/ENb;

    .line 2112868
    if-nez p2, :cond_0

    .line 2112869
    new-instance p2, LX/ENb;

    invoke-direct {p2, v8}, LX/ENb;-><init>(LX/ENd;)V

    .line 2112870
    :cond_0
    invoke-static {p2, p1, v9, v9, p0}, LX/ENb;->a$redex0(LX/ENb;LX/1De;IILX/ENc;)V

    .line 2112871
    move-object p0, p2

    .line 2112872
    move-object v9, p0

    .line 2112873
    move-object v8, v9

    .line 2112874
    iget-object v9, v8, LX/ENb;->a:LX/ENc;

    iput-object v6, v9, LX/ENc;->a:LX/CzL;

    .line 2112875
    iget-object v9, v8, LX/ENb;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v9, p0}, Ljava/util/BitSet;->set(I)V

    .line 2112876
    move-object v6, v8

    .line 2112877
    iget-object v8, v6, LX/ENb;->a:LX/ENc;

    iput-object v2, v8, LX/ENc;->b:LX/1Ps;

    .line 2112878
    iget-object v8, v6, LX/ENb;->e:Ljava/util/BitSet;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 2112879
    move-object v6, v6

    .line 2112880
    invoke-interface {v3, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    if-nez v4, :cond_4

    const/4 v3, 0x0

    :goto_2
    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    iget-object v6, v0, LX/ENa;->e:LX/1V0;

    sget-object v8, LX/ENa;->d:LX/1X6;

    iget-object v3, v0, LX/ENa;->g:LX/ENV;

    const/4 v9, 0x0

    .line 2112881
    new-instance p0, LX/ENU;

    invoke-direct {p0, v3}, LX/ENU;-><init>(LX/ENV;)V

    .line 2112882
    iget-object p2, v3, LX/ENV;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/ENT;

    .line 2112883
    if-nez p2, :cond_1

    .line 2112884
    new-instance p2, LX/ENT;

    invoke-direct {p2, v3}, LX/ENT;-><init>(LX/ENV;)V

    .line 2112885
    :cond_1
    invoke-static {p2, p1, v9, v9, p0}, LX/ENT;->a$redex0(LX/ENT;LX/1De;IILX/ENU;)V

    .line 2112886
    move-object p0, p2

    .line 2112887
    move-object v9, p0

    .line 2112888
    move-object v3, v9

    .line 2112889
    iget-object v9, v3, LX/ENT;->a:LX/ENU;

    iput-object v7, v9, LX/ENU;->a:LX/CzL;

    .line 2112890
    iget-object v9, v3, LX/ENT;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v9, p0}, Ljava/util/BitSet;->set(I)V

    .line 2112891
    move-object v7, v3

    .line 2112892
    move-object v3, v2

    check-cast v3, LX/1Pg;

    .line 2112893
    iget-object v9, v7, LX/ENT;->a:LX/ENU;

    iput-object v3, v9, LX/ENU;->b:LX/1Pg;

    .line 2112894
    iget-object v9, v7, LX/ENT;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v9, p0}, Ljava/util/BitSet;->set(I)V

    .line 2112895
    move-object v3, v7

    .line 2112896
    iget-object v7, v3, LX/ENT;->a:LX/ENU;

    iput v5, v7, LX/ENU;->c:I

    .line 2112897
    iget-object v7, v3, LX/ENT;->e:Ljava/util/BitSet;

    const/4 v9, 0x2

    invoke-virtual {v7, v9}, Ljava/util/BitSet;->set(I)V

    .line 2112898
    move-object v3, v3

    .line 2112899
    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-virtual {v6, p1, v2, v8, v3}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2112900
    return-object v0

    .line 2112901
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v4

    goto/16 :goto_0

    .line 2112902
    :cond_3
    const v3, -0x6e685d

    move v5, v3

    goto/16 :goto_1

    .line 2112903
    :cond_4
    iget-object v3, v0, LX/ENa;->e:LX/1V0;

    sget-object v4, LX/ENa;->c:LX/1X6;

    iget-object v8, v0, LX/ENa;->i:LX/ENh;

    const/4 v9, 0x0

    .line 2112904
    new-instance p0, LX/ENg;

    invoke-direct {p0, v8}, LX/ENg;-><init>(LX/ENh;)V

    .line 2112905
    iget-object p2, v8, LX/ENh;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/ENf;

    .line 2112906
    if-nez p2, :cond_5

    .line 2112907
    new-instance p2, LX/ENf;

    invoke-direct {p2, v8}, LX/ENf;-><init>(LX/ENh;)V

    .line 2112908
    :cond_5
    invoke-static {p2, p1, v9, v9, p0}, LX/ENf;->a$redex0(LX/ENf;LX/1De;IILX/ENg;)V

    .line 2112909
    move-object p0, p2

    .line 2112910
    move-object v9, p0

    .line 2112911
    move-object v8, v9

    .line 2112912
    iget-object v9, v8, LX/ENf;->a:LX/ENg;

    iput-object v1, v9, LX/ENg;->a:LX/CzL;

    .line 2112913
    iget-object v9, v8, LX/ENf;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v9, p0}, Ljava/util/BitSet;->set(I)V

    .line 2112914
    move-object v8, v8

    .line 2112915
    iget-object v9, v8, LX/ENf;->a:LX/ENg;

    iput-object v2, v9, LX/ENg;->b:LX/1Ps;

    .line 2112916
    iget-object v9, v8, LX/ENf;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v9, p0}, Ljava/util/BitSet;->set(I)V

    .line 2112917
    move-object v8, v8

    .line 2112918
    const v9, 0x7f08114e

    .line 2112919
    iget-object p0, v8, LX/ENf;->a:LX/ENg;

    invoke-virtual {v8, v9}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, LX/ENg;->c:Ljava/lang/CharSequence;

    .line 2112920
    iget-object p0, v8, LX/ENf;->e:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2112921
    move-object v8, v8

    .line 2112922
    const v9, 0x7f0a00d9

    .line 2112923
    iget-object p0, v8, LX/ENf;->a:LX/ENg;

    invoke-virtual {v8, v9}, LX/1Dp;->d(I)I

    move-result p2

    iput p2, p0, LX/ENg;->d:I

    .line 2112924
    iget-object p0, v8, LX/ENf;->e:Ljava/util/BitSet;

    const/4 p2, 0x3

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2112925
    move-object v8, v8

    .line 2112926
    invoke-virtual {v8}, LX/1X5;->d()LX/1X1;

    move-result-object v8

    invoke-virtual {v3, p1, v2, v4, v8}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v3

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2112927
    invoke-static {}, LX/1dS;->b()V

    .line 2112928
    const/4 v0, 0x0

    return-object v0
.end method
