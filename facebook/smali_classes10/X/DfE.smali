.class public final LX/DfE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 29

    .prologue
    .line 2026183
    const/16 v25, 0x0

    .line 2026184
    const/16 v24, 0x0

    .line 2026185
    const/16 v23, 0x0

    .line 2026186
    const/16 v22, 0x0

    .line 2026187
    const/16 v21, 0x0

    .line 2026188
    const/16 v20, 0x0

    .line 2026189
    const/16 v19, 0x0

    .line 2026190
    const/16 v18, 0x0

    .line 2026191
    const/16 v17, 0x0

    .line 2026192
    const/16 v16, 0x0

    .line 2026193
    const/4 v15, 0x0

    .line 2026194
    const/4 v14, 0x0

    .line 2026195
    const/4 v13, 0x0

    .line 2026196
    const/4 v12, 0x0

    .line 2026197
    const/4 v11, 0x0

    .line 2026198
    const/4 v10, 0x0

    .line 2026199
    const/4 v9, 0x0

    .line 2026200
    const/4 v8, 0x0

    .line 2026201
    const/4 v7, 0x0

    .line 2026202
    const/4 v6, 0x0

    .line 2026203
    const/4 v5, 0x0

    .line 2026204
    const/4 v4, 0x0

    .line 2026205
    const/4 v3, 0x0

    .line 2026206
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    .line 2026207
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2026208
    const/4 v3, 0x0

    .line 2026209
    :goto_0
    return v3

    .line 2026210
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2026211
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_12

    .line 2026212
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v26

    .line 2026213
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2026214
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    if-eqz v26, :cond_1

    .line 2026215
    const-string v27, "__type__"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_2

    const-string v27, "__typename"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 2026216
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v25

    goto :goto_1

    .line 2026217
    :cond_3
    const-string v27, "config_type"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 2026218
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentUnitConfigType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentUnitConfigType;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    goto :goto_1

    .line 2026219
    :cond_4
    const-string v27, "header_description"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 2026220
    invoke-static/range {p0 .. p1}, LX/DfQ;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 2026221
    :cond_5
    const-string v27, "id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 2026222
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto :goto_1

    .line 2026223
    :cond_6
    const-string v27, "layout"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 2026224
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2ActiveNowLayoutType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2ActiveNowLayoutType;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    goto/16 :goto_1

    .line 2026225
    :cond_7
    const-string v27, "layout_style"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 2026226
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2AdsUnitLayout;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2AdsUnitLayout;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    goto/16 :goto_1

    .line 2026227
    :cond_8
    const-string v27, "layout_type"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 2026228
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2BotsYMMLayoutType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2BotsYMMLayoutType;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    goto/16 :goto_1

    .line 2026229
    :cond_9
    const-string v27, "max_hours_back"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 2026230
    const/4 v9, 0x1

    .line 2026231
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    goto/16 :goto_1

    .line 2026232
    :cond_a
    const-string v27, "max_item_count"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 2026233
    const/4 v8, 0x1

    .line 2026234
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto/16 :goto_1

    .line 2026235
    :cond_b
    const-string v27, "max_num_threads_to_show"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 2026236
    const/4 v7, 0x1

    .line 2026237
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 2026238
    :cond_c
    const-string v27, "min_num_threads_to_show"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 2026239
    const/4 v6, 0x1

    .line 2026240
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto/16 :goto_1

    .line 2026241
    :cond_d
    const-string v27, "num_items_to_show"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 2026242
    const/4 v5, 0x1

    .line 2026243
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 2026244
    :cond_e
    const-string v27, "see_more_style"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 2026245
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2RecentSeeMoreType;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    goto/16 :goto_1

    .line 2026246
    :cond_f
    const-string v27, "show_app_badge"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_10

    .line 2026247
    const/4 v4, 0x1

    .line 2026248
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 2026249
    :cond_10
    const-string v27, "show_highlight_badge"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_11

    .line 2026250
    const/4 v3, 0x1

    .line 2026251
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 2026252
    :cond_11
    const-string v27, "title_badge"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 2026253
    invoke-static/range {p0 .. p1}, LX/DfR;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 2026254
    :cond_12
    const/16 v26, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2026255
    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2026256
    const/16 v25, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2026257
    const/16 v24, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2026258
    const/16 v23, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2026259
    const/16 v22, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2026260
    const/16 v21, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2026261
    const/16 v20, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2026262
    if-eqz v9, :cond_13

    .line 2026263
    const/4 v9, 0x7

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v9, v1, v2}, LX/186;->a(III)V

    .line 2026264
    :cond_13
    if-eqz v8, :cond_14

    .line 2026265
    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 2026266
    :cond_14
    if-eqz v7, :cond_15

    .line 2026267
    const/16 v7, 0x9

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v7, v1, v8}, LX/186;->a(III)V

    .line 2026268
    :cond_15
    if-eqz v6, :cond_16

    .line 2026269
    const/16 v6, 0xa

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15, v7}, LX/186;->a(III)V

    .line 2026270
    :cond_16
    if-eqz v5, :cond_17

    .line 2026271
    const/16 v5, 0xb

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14, v6}, LX/186;->a(III)V

    .line 2026272
    :cond_17
    const/16 v5, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 2026273
    if-eqz v4, :cond_18

    .line 2026274
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 2026275
    :cond_18
    if-eqz v3, :cond_19

    .line 2026276
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 2026277
    :cond_19
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 2026278
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2026279
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2026280
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2026281
    if-eqz v0, :cond_0

    .line 2026282
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026283
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2026284
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2026285
    if-eqz v0, :cond_1

    .line 2026286
    const-string v0, "config_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026287
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026288
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026289
    if-eqz v0, :cond_2

    .line 2026290
    const-string v1, "header_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026291
    invoke-static {p0, v0, p2}, LX/DfQ;->a(LX/15i;ILX/0nX;)V

    .line 2026292
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026293
    if-eqz v0, :cond_3

    .line 2026294
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026295
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026296
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2026297
    if-eqz v0, :cond_4

    .line 2026298
    const-string v0, "layout"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026299
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026300
    :cond_4
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2026301
    if-eqz v0, :cond_5

    .line 2026302
    const-string v0, "layout_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026303
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026304
    :cond_5
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 2026305
    if-eqz v0, :cond_6

    .line 2026306
    const-string v0, "layout_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026307
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026308
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2026309
    if-eqz v0, :cond_7

    .line 2026310
    const-string v1, "max_hours_back"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026311
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2026312
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2026313
    if-eqz v0, :cond_8

    .line 2026314
    const-string v1, "max_item_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026315
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2026316
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2026317
    if-eqz v0, :cond_9

    .line 2026318
    const-string v1, "max_num_threads_to_show"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026319
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2026320
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2026321
    if-eqz v0, :cond_a

    .line 2026322
    const-string v1, "min_num_threads_to_show"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026323
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2026324
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2026325
    if-eqz v0, :cond_b

    .line 2026326
    const-string v1, "num_items_to_show"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026327
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2026328
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026329
    if-eqz v0, :cond_c

    .line 2026330
    const-string v0, "see_more_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026331
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026332
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2026333
    if-eqz v0, :cond_d

    .line 2026334
    const-string v1, "show_app_badge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026335
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2026336
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2026337
    if-eqz v0, :cond_e

    .line 2026338
    const-string v1, "show_highlight_badge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026339
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2026340
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026341
    if-eqz v0, :cond_f

    .line 2026342
    const-string v1, "title_badge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026343
    invoke-static {p0, v0, p2}, LX/DfR;->a(LX/15i;ILX/0nX;)V

    .line 2026344
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2026345
    return-void
.end method
