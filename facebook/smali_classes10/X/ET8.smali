.class public final LX/ET8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "LX/0jT;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ETB;


# direct methods
.method public constructor <init>(LX/ETB;)V
    .locals 0

    .prologue
    .line 2123995
    iput-object p1, p0, LX/ET8;->a:LX/ETB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2123996
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2123997
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2123998
    if-eqz p1, :cond_0

    .line 2123999
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2124000
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    .line 2124001
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2124002
    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2124003
    iget-object v1, p0, LX/ET8;->a:LX/ETB;

    .line 2124004
    iget-object v2, v1, LX/ETB;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ESx;

    iget-object p0, v1, LX/ETB;->t:LX/ETQ;

    .line 2124005
    invoke-static {v2, p0, v0}, LX/ESx;->b(LX/ESx;LX/ETQ;Lcom/facebook/graphql/model/GraphQLActor;)Z

    move-result p1

    move v2, p1

    .line 2124006
    if-eqz v2, :cond_1

    .line 2124007
    invoke-static {v1}, LX/ETB;->u(LX/ETB;)V

    .line 2124008
    :cond_0
    :goto_0
    return-void

    :cond_1
    goto :goto_0
.end method
