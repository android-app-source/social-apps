.class public LX/DdV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0lB;

.field private final c:LX/34H;

.field private final d:LX/2Mk;

.field private final e:LX/34G;

.field private final f:LX/6hW;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6cV;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3N4;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/Do7;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3N6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0lB;LX/34H;LX/2Mk;LX/34G;LX/6hW;LX/0Ot;LX/0Or;LX/0Or;LX/Do7;LX/0Ot;)V
    .locals 0
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsUpdateThreadWithOlderMsgEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0lB;",
            "LX/34H;",
            "LX/2Mk;",
            "LX/34G;",
            "LX/6hW;",
            "LX/0Ot",
            "<",
            "LX/6cV;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/3N4;",
            ">;",
            "LX/Do7;",
            "LX/0Ot",
            "<",
            "LX/3N6;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019779
    iput-object p1, p0, LX/DdV;->a:LX/0Or;

    .line 2019780
    iput-object p2, p0, LX/DdV;->b:LX/0lB;

    .line 2019781
    iput-object p3, p0, LX/DdV;->c:LX/34H;

    .line 2019782
    iput-object p4, p0, LX/DdV;->d:LX/2Mk;

    .line 2019783
    iput-object p5, p0, LX/DdV;->e:LX/34G;

    .line 2019784
    iput-object p6, p0, LX/DdV;->f:LX/6hW;

    .line 2019785
    iput-object p7, p0, LX/DdV;->g:LX/0Ot;

    .line 2019786
    iput-object p8, p0, LX/DdV;->h:LX/0Or;

    .line 2019787
    iput-object p9, p0, LX/DdV;->i:LX/0Or;

    .line 2019788
    iput-object p10, p0, LX/DdV;->j:LX/Do7;

    .line 2019789
    iput-object p11, p0, LX/DdV;->k:LX/0Ot;

    .line 2019790
    return-void
.end method

.method private static a(LX/0Rf;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Rf",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;TK;TV;)",
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2019771
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2019772
    if-eqz p2, :cond_0

    .line 2019773
    invoke-virtual {v1, p1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2019774
    :cond_0
    invoke-virtual {p0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2019775
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2019776
    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map$Entry;)LX/0P2;

    goto :goto_0

    .line 2019777
    :cond_2
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;LX/0Px;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            "Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadEventReminder;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2019706
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2019707
    invoke-static {p0}, LX/2Mk;->A(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2019708
    new-instance v3, LX/6ft;

    invoke-direct {v3}, LX/6ft;-><init>()V

    .line 2019709
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventId:Ljava/lang/String;

    .line 2019710
    iput-object v1, v3, LX/6ft;->a:Ljava/lang/String;

    .line 2019711
    const/4 v1, 0x1

    .line 2019712
    iput-boolean v1, v3, LX/6ft;->e:Z

    .line 2019713
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_4

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2019714
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2019715
    iget-object v6, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2019716
    invoke-virtual {v3}, LX/6ft;->i()Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2019717
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2019718
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2019719
    :cond_1
    invoke-static {p0}, LX/2Mk;->x(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2019720
    new-instance v0, LX/6ft;

    invoke-direct {v0}, LX/6ft;-><init>()V

    .line 2019721
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventId:Ljava/lang/String;

    .line 2019722
    iput-object v1, v0, LX/6ft;->a:Ljava/lang/String;

    .line 2019723
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventType:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2019724
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventType:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v1

    .line 2019725
    iput-object v1, v0, LX/6ft;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2019726
    :cond_2
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTrackRsvp:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2019727
    const-string v1, "1"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTrackRsvp:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 2019728
    iput-boolean v1, v0, LX/6ft;->g:Z

    .line 2019729
    :cond_3
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTime:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2019730
    iput-wide v4, v0, LX/6ft;->c:J

    .line 2019731
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTitle:Ljava/lang/String;

    .line 2019732
    iput-object v1, v0, LX/6ft;->d:Ljava/lang/String;

    .line 2019733
    invoke-interface {v2, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2019734
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 2019735
    iget-object v3, p0, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-virtual {v1, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2019736
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 2019737
    iput-object v1, v0, LX/6ft;->f:LX/0P1;

    .line 2019738
    invoke-virtual {v0}, LX/6ft;->i()Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2019739
    :cond_4
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2019740
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 2019741
    :cond_5
    invoke-static {p0}, LX/2Mk;->y(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-static {p0}, LX/2Mk;->z(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2019742
    :cond_6
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_4

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2019743
    iget-object v4, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2019744
    iget-object v5, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2019745
    new-instance v4, LX/6ft;

    invoke-direct {v4, v0}, LX/6ft;-><init>(Lcom/facebook/messaging/model/threads/ThreadEventReminder;)V

    .line 2019746
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventId:Ljava/lang/String;

    .line 2019747
    iput-object v0, v4, LX/6ft;->a:Ljava/lang/String;

    .line 2019748
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventType:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2019749
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventType:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v0

    .line 2019750
    iput-object v0, v4, LX/6ft;->b:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2019751
    :cond_7
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTrackRsvp:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 2019752
    const-string v0, "1"

    iget-object v5, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTrackRsvp:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2019753
    iput-boolean v0, v4, LX/6ft;->g:Z

    .line 2019754
    :cond_8
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTime:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 2019755
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTime:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2019756
    iput-wide v6, v4, LX/6ft;->c:J

    .line 2019757
    :cond_9
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTitle:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 2019758
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventTitle:Ljava/lang/String;

    .line 2019759
    iput-object v0, v4, LX/6ft;->d:Ljava/lang/String;

    .line 2019760
    :cond_a
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventLocationName:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 2019761
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventLocationName:Ljava/lang/String;

    .line 2019762
    iput-object v0, v4, LX/6ft;->h:Ljava/lang/String;

    .line 2019763
    :cond_b
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestId:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestStatus:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2019764
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    .line 2019765
    new-instance v5, Lcom/facebook/user/model/UserKey;

    sget-object v6, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v7, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestId:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    iget-object v6, p1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->guestStatus:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2019766
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 2019767
    iput-object v0, v4, LX/6ft;->f:LX/0P1;

    .line 2019768
    :cond_c
    invoke-virtual {v4}, LX/6ft;->i()Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2019769
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    .line 2019770
    :cond_d
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;)LX/6g5;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2019691
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->MISSED_CALL:LX/2uW;

    if-eq v0, v3, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->MISSED_VIDEO_CALL:LX/2uW;

    if-ne v0, v3, :cond_2

    :cond_0
    move v0, v2

    .line 2019692
    :goto_0
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v4, LX/2uW;->MISSED_VIDEO_CALL:LX/2uW;

    if-ne v3, v4, :cond_3

    .line 2019693
    :goto_1
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->CALL_LOG:LX/2uW;

    if-ne v1, v3, :cond_1

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2019694
    iget-object v0, p0, LX/DdV;->f:LX/6hW;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->b()LX/0Px;

    move-result-object v1

    .line 2019695
    new-instance v2, LX/6hV;

    const/16 v3, 0x12cd

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v3, v1}, LX/6hV;-><init>(LX/0Or;LX/0Px;)V

    .line 2019696
    move-object v1, v2

    .line 2019697
    iget-object v0, v1, LX/6hV;->a:LX/6hS;

    iget-boolean v0, v0, LX/6hS;->c:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2019698
    iget-object v2, v1, LX/6hV;->a:LX/6hS;

    iget-boolean v2, v2, LX/6hS;->g:Z

    move v2, v2

    .line 2019699
    :cond_1
    if-eqz v0, :cond_5

    .line 2019700
    if-eqz v2, :cond_4

    sget-object v0, LX/6g5;->VIDEO_CALL:LX/6g5;

    .line 2019701
    :goto_3
    return-object v0

    :cond_2
    move v0, v1

    .line 2019702
    goto :goto_0

    :cond_3
    move v2, v1

    .line 2019703
    goto :goto_1

    .line 2019704
    :cond_4
    sget-object v0, LX/6g5;->VOICE_CALL:LX/6g5;

    goto :goto_3

    .line 2019705
    :cond_5
    sget-object v0, LX/6g5;->NONE:LX/6g5;

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(LX/0QB;)LX/DdV;
    .locals 1

    .prologue
    .line 2019690
    invoke-static {p0}, LX/DdV;->b(LX/0QB;)LX/DdV;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/messaging/model/threads/NicknamesMap;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/NicknamesMap;
    .locals 2

    .prologue
    .line 2019791
    iget-object v0, p0, LX/DdV;->b:LX/0lB;

    invoke-virtual {p1, v0}, Lcom/facebook/messaging/model/threads/NicknamesMap;->a(LX/0lC;)LX/0P1;

    move-result-object v0

    .line 2019792
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-static {v0, p2, p3}, LX/DdV;->a(LX/0Rf;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    .line 2019793
    new-instance v1, Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/model/threads/NicknamesMap;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method private a(LX/DdQ;LX/6jT;)Z
    .locals 4

    .prologue
    .line 2019682
    iget-object v0, p0, LX/DdV;->j:LX/Do7;

    .line 2019683
    iget-object v1, v0, LX/Do7;->a:LX/0Uh;

    const/16 v2, 0x135

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2019684
    if-eqz v0, :cond_0

    sget-object v0, LX/DdQ;->MESSAGE_DELETED:LX/DdQ;

    if-ne p1, v0, :cond_0

    .line 2019685
    const/4 v0, 0x1

    .line 2019686
    :goto_0
    return v0

    .line 2019687
    :cond_0
    iget-object v0, p0, LX/DdV;->j:LX/Do7;

    invoke-virtual {v0}, LX/Do7;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2019688
    const/4 v0, 0x0

    goto :goto_0

    .line 2019689
    :cond_1
    iget-boolean v0, p2, LX/6jT;->c:Z

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 6
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2019674
    if-nez p1, :cond_1

    .line 2019675
    :cond_0
    :goto_0
    return v0

    .line 2019676
    :cond_1
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->PENDING_SEND:LX/2uW;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->FAILED_SEND:LX/2uW;

    if-eq v2, v3, :cond_0

    .line 2019677
    :cond_2
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->REGULAR:LX/2uW;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->PENDING_SEND:LX/2uW;

    if-ne v2, v3, :cond_3

    move v0, v1

    .line 2019678
    goto :goto_0

    .line 2019679
    :cond_3
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->FAILED_SEND:LX/2uW;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->PENDING_SEND:LX/2uW;

    if-ne v2, v3, :cond_0

    .line 2019680
    iget-wide v2, p0, Lcom/facebook/messaging/model/messages/Message;->d:J

    iget-wide v4, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    move v0, v1

    .line 2019681
    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;LX/DdQ;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2019664
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->INCOMING_CALL:LX/2uW;

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 2019665
    :goto_0
    sget-object v3, LX/DdQ;->MESSAGE_DELETED:LX/DdQ;

    if-eq p3, v3, :cond_0

    if-eqz v0, :cond_3

    .line 2019666
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2019667
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 2019668
    goto :goto_0

    :cond_2
    move v1, v2

    .line 2019669
    goto :goto_1

    .line 2019670
    :cond_3
    iget-object v0, p0, LX/DdV;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2019671
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2019672
    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    .line 2019673
    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/DdV;
    .locals 12

    .prologue
    .line 2019662
    new-instance v0, LX/DdV;

    const/16 v1, 0x19e

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v2

    check-cast v2, LX/0lB;

    invoke-static {p0}, LX/34H;->b(LX/0QB;)LX/34H;

    move-result-object v3

    check-cast v3, LX/34H;

    invoke-static {p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v4

    check-cast v4, LX/2Mk;

    invoke-static {p0}, LX/34G;->b(LX/0QB;)LX/34G;

    move-result-object v5

    check-cast v5, LX/34G;

    const-class v6, LX/6hW;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/6hW;

    const/16 v7, 0x26f9

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x14f4

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0xdb8

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/Do7;->a(LX/0QB;)LX/Do7;

    move-result-object v10

    check-cast v10, LX/Do7;

    const/16 v11, 0xce4

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, LX/DdV;-><init>(LX/0Or;LX/0lB;LX/34H;LX/2Mk;LX/34G;LX/6hW;LX/0Ot;LX/0Or;LX/0Or;LX/Do7;LX/0Ot;)V

    .line 2019663
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 13

    .prologue
    .line 2019579
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019580
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019581
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2019582
    iget-object v4, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2019583
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2019584
    iget-object v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->n:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_1

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2019585
    iget-object v8, v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    iget-object v9, v4, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v8, v9}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2019586
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2019587
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2019588
    :cond_1
    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 2019589
    if-eqz v2, :cond_19

    .line 2019590
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 2019591
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x78

    if-le v3, v4, :cond_19

    .line 2019592
    const/4 v3, 0x0

    const/16 v4, 0x78

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2019593
    :goto_1
    const/4 v2, 0x0

    .line 2019594
    iget-object v4, p2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2019595
    iget-object v2, p0, LX/DdV;->e:LX/34G;

    invoke-virtual {v2, p2}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 2019596
    :goto_2
    iget-object v8, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2019597
    iget-wide v6, p2, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 2019598
    invoke-static {v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 2019599
    iget-object v2, p0, LX/DdV;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6cV;

    iget-wide v8, v8, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-virtual {v2, v8, v9, v6, v7}, LX/6cV;->a(JJ)J

    move-result-wide v6

    .line 2019600
    :cond_2
    :goto_3
    invoke-direct {p0, p2}, LX/DdV;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6g5;

    move-result-object v8

    .line 2019601
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, LX/6g6;->e(J)LX/6g6;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/6g6;->c(Ljava/lang/String;)LX/6g6;

    move-result-object v2

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v2, v3}, LX/6g6;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6g6;

    move-result-object v3

    iget-object v2, p0, LX/DdV;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3N4;

    invoke-virtual {v2, p1, p2}, LX/3N4;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadMediaPreview;)LX/6g6;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/6g6;->c(Ljava/util/List;)LX/6g6;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/6g6;->a(LX/6g5;)LX/6g6;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/6g6;->d(Ljava/lang/String;)LX/6g6;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/6g6;->d(Z)LX/6g6;

    move-result-object v4

    .line 2019602
    const-wide/16 v2, -0x1

    cmp-long v2, p3, v2

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    move v3, v2

    .line 2019603
    :goto_4
    if-eqz v3, :cond_3

    .line 2019604
    sget-object v2, LX/DdU;->a:[I

    invoke-virtual/range {p5 .. p5}, LX/DdQ;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 2019605
    :cond_3
    :goto_5
    move-object/from16 v0, p5

    invoke-direct {p0, p1, p2, v0}, LX/DdV;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;LX/DdQ;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual/range {p7 .. p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v2

    if-nez v2, :cond_14

    .line 2019606
    :cond_4
    invoke-virtual {v4, v6, v7}, LX/6g6;->f(J)LX/6g6;

    .line 2019607
    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, LX/6g6;->h(J)LX/6g6;

    .line 2019608
    :goto_6
    invoke-static {p2}, LX/2Mk;->i(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v2, LX/DdQ;->MESSAGE_DELETED:LX/DdQ;

    move-object/from16 v0, p5

    if-eq v0, v2, :cond_5

    .line 2019609
    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->r()I

    move-result v2

    invoke-virtual {v4, v2}, LX/6g6;->a(I)LX/6g6;

    .line 2019610
    :cond_5
    invoke-static {p2}, LX/2Mk;->e(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v2, LX/DdQ;->MESSAGE_DELETED:LX/DdQ;

    move-object/from16 v0, p5

    if-eq v0, v2, :cond_7

    .line 2019611
    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 2019612
    if-eqz v2, :cond_7

    .line 2019613
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadCustomization;->newBuilder()LX/6fr;

    move-result-object v5

    iget-object v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v5, v6}, LX/6fr;->a(Lcom/facebook/messaging/model/threads/ThreadCustomization;)LX/6fr;

    move-result-object v5

    .line 2019614
    invoke-static {p2}, LX/2Mk;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 2019615
    invoke-virtual {v2}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->n()I

    move-result v2

    invoke-virtual {v5, v2}, LX/6fr;->b(I)LX/6fr;

    .line 2019616
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, LX/6fr;->c(I)LX/6fr;

    .line 2019617
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, LX/6fr;->a(I)LX/6fr;

    .line 2019618
    :cond_6
    :goto_7
    invoke-virtual {v5}, LX/6fr;->g()Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadCustomization;)LX/6g6;

    .line 2019619
    :cond_7
    invoke-static {p2}, LX/2Mk;->x(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static {p2}, LX/2Mk;->y(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static {p2}, LX/2Mk;->z(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static {p2}, LX/2Mk;->A(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, LX/DdQ;->MESSAGE_DELETED:LX/DdQ;

    move-object/from16 v0, p5

    if-eq v0, v2, :cond_9

    .line 2019620
    :cond_8
    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 2019621
    if-eqz v2, :cond_9

    .line 2019622
    invoke-virtual {v2}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->E()Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    move-result-object v2

    .line 2019623
    if-eqz v2, :cond_9

    iget-object v5, v2, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;->eventId:Ljava/lang/String;

    if-eqz v5, :cond_9

    .line 2019624
    iget-object v5, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->L:LX/0Px;

    invoke-static {p2, v2, v5}, LX/DdV;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;LX/0Px;)LX/0Px;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/6g6;->d(Ljava/util/List;)LX/6g6;

    .line 2019625
    :cond_9
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    invoke-virtual {v2}, LX/6ek;->isMessageRequestFolders()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/DdV;->d:LX/2Mk;

    invoke-virtual {v2, p2}, LX/2Mk;->s(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2019626
    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    invoke-virtual {v4, v2}, LX/6g6;->a(LX/6ek;)LX/6g6;

    .line 2019627
    :cond_a
    invoke-static {p2}, LX/2Mk;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 2019628
    iget-wide v6, p2, Lcom/facebook/messaging/model/messages/Message;->g:J

    invoke-virtual {v4, v6, v7}, LX/6g6;->b(J)LX/6g6;

    move-result-object v2

    iget-wide v6, p2, Lcom/facebook/messaging/model/messages/Message;->g:J

    invoke-virtual {v2, v6, v7}, LX/6g6;->c(J)LX/6g6;

    .line 2019629
    if-eqz v3, :cond_b

    .line 2019630
    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, LX/6g6;->d(J)LX/6g6;

    .line 2019631
    :cond_b
    invoke-static {p2}, LX/2Mk;->p(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->C()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2019632
    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Q:LX/0P1;

    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->A()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/facebook/messaging/model/threads/ThreadGameData;

    iget-object v6, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v6, v6, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v6}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p2, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    invoke-virtual {v7}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->B()I

    move-result v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/messaging/model/threads/ThreadGameData;-><init>(Ljava/lang/String;I)V

    invoke-static {v2, v3, v5}, LX/DdV;->a(LX/0Rf;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/6g6;->a(Ljava/util/Map;)LX/6g6;

    .line 2019633
    :cond_c
    iget-boolean v2, p2, Lcom/facebook/messaging/model/messages/Message;->M:Z

    invoke-virtual {v4, v2}, LX/6g6;->h(Z)LX/6g6;

    .line 2019634
    iget-object v2, p0, LX/DdV;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3N6;

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v5, p2, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v3, v5}, LX/3N6;->a(Ljava/lang/String;Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/6g6;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)LX/6g6;

    .line 2019635
    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->N:Ljava/lang/String;

    invoke-virtual {v4, v2}, LX/6g6;->f(Ljava/lang/String;)LX/6g6;

    .line 2019636
    invoke-virtual {v4}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    return-object v2

    .line 2019637
    :cond_d
    invoke-static {p2}, LX/2Mk;->o(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-eqz v4, :cond_e

    iget-object v4, p2, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 2019638
    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    move-object v4, v2

    goto/16 :goto_2

    .line 2019639
    :cond_e
    iget-object v4, p0, LX/DdV;->c:LX/34H;

    invoke-virtual {v4, p2}, LX/34H;->a(Lcom/facebook/messaging/model/messages/Message;)LX/CMk;

    move-result-object v4

    if-eqz v4, :cond_f

    .line 2019640
    iget-object v2, p0, LX/DdV;->e:LX/34G;

    iget-object v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v2, p2, v4}, LX/34G;->d(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_2

    .line 2019641
    :cond_f
    invoke-static {p2}, LX/34G;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 2019642
    iget-object v2, p0, LX/DdV;->e:LX/34G;

    iget-object v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v2, p2, v4}, LX/34G;->e(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_2

    .line 2019643
    :cond_10
    iget-object v4, p0, LX/DdV;->e:LX/34G;

    invoke-virtual {v4, p2}, LX/34G;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 2019644
    iget-object v2, p0, LX/DdV;->e:LX/34G;

    iget-object v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v2, p2, v4}, LX/34G;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_2

    .line 2019645
    :cond_11
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 2019646
    iget-object v2, p0, LX/DdV;->e:LX/34G;

    iget-object v4, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v2, p2, v4}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_2

    .line 2019647
    :cond_12
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-direct {p0, v0, v1}, LX/DdV;->a(LX/DdQ;LX/6jT;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2019648
    iget-wide v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    goto/16 :goto_3

    .line 2019649
    :cond_13
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_4

    .line 2019650
    :pswitch_0
    iget-wide v8, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->l:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    invoke-virtual {v4, v8, v9}, LX/6g6;->g(J)LX/6g6;

    goto/16 :goto_5

    .line 2019651
    :pswitch_1
    iget-wide v8, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->l:J

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    invoke-virtual {v4, v8, v9}, LX/6g6;->g(J)LX/6g6;

    goto/16 :goto_5

    .line 2019652
    :cond_14
    iget-wide v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->m:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, LX/6g6;->h(J)LX/6g6;

    goto/16 :goto_6

    .line 2019653
    :cond_15
    invoke-static {p2}, LX/2Mk;->g(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 2019654
    invoke-virtual {v2}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->o()Ljava/lang/String;

    move-result-object v2

    .line 2019655
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 2019656
    const/4 v2, 0x0

    .line 2019657
    :cond_16
    invoke-virtual {v5, v2}, LX/6fr;->a(Ljava/lang/String;)LX/6fr;

    goto/16 :goto_7

    .line 2019658
    :cond_17
    invoke-static {p2}, LX/2Mk;->h(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2019659
    invoke-virtual {v2}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->q()Ljava/lang/String;

    move-result-object v6

    .line 2019660
    if-eqz v6, :cond_6

    .line 2019661
    iget-object v7, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget-object v7, v7, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->p()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v7, v6, v2}, LX/DdV;->a(Lcom/facebook/messaging/model/threads/NicknamesMap;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/NicknamesMap;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/6fr;->a(Lcom/facebook/messaging/model/threads/NicknamesMap;)LX/6fr;

    goto/16 :goto_7

    :cond_18
    move-object v4, v2

    goto/16 :goto_2

    :cond_19
    move-object v3, v2

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 10
    .param p3    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2019571
    iget-object v0, p0, LX/DdV;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, LX/2Mk;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/facebook/messaging/model/messages/MessagesCollection;->c()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/facebook/messaging/model/messages/MessagesCollection;->c()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    .line 2019572
    invoke-virtual/range {v1 .. v8}, LX/DdV;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2019573
    :goto_0
    return-object v0

    .line 2019574
    :cond_1
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v0

    .line 2019575
    invoke-virtual {v0, p4, p5}, LX/6g6;->d(J)LX/6g6;

    .line 2019576
    invoke-static {p2}, LX/2Mk;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2019577
    iget-wide v2, p2, Lcom/facebook/messaging/model/messages/Message;->g:J

    invoke-virtual {v0, v2, v3}, LX/6g6;->b(J)LX/6g6;

    move-result-object v1

    iget-wide v2, p2, Lcom/facebook/messaging/model/messages/Message;->g:J

    invoke-virtual {v1, v2, v3}, LX/6g6;->c(J)LX/6g6;

    .line 2019578
    :cond_2
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    goto :goto_0
.end method
