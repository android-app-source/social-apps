.class public final enum LX/CwW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CwW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CwW;

.field public static final enum BOOTSTRAP_ENTITIES:LX/CwW;

.field public static final enum PARALLEL_PRIMARY:LX/CwW;

.field public static final enum PARALLEL_SECONDARY:LX/CwW;

.field public static final enum SINGLE:LX/CwW;

.field public static final enum STREAMING:LX/CwW;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1950779
    new-instance v0, LX/CwW;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v2}, LX/CwW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwW;->SINGLE:LX/CwW;

    .line 1950780
    new-instance v0, LX/CwW;

    const-string v1, "PARALLEL_PRIMARY"

    invoke-direct {v0, v1, v3}, LX/CwW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwW;->PARALLEL_PRIMARY:LX/CwW;

    .line 1950781
    new-instance v0, LX/CwW;

    const-string v1, "PARALLEL_SECONDARY"

    invoke-direct {v0, v1, v4}, LX/CwW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwW;->PARALLEL_SECONDARY:LX/CwW;

    .line 1950782
    new-instance v0, LX/CwW;

    const-string v1, "STREAMING"

    invoke-direct {v0, v1, v5}, LX/CwW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwW;->STREAMING:LX/CwW;

    .line 1950783
    new-instance v0, LX/CwW;

    const-string v1, "BOOTSTRAP_ENTITIES"

    invoke-direct {v0, v1, v6}, LX/CwW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwW;->BOOTSTRAP_ENTITIES:LX/CwW;

    .line 1950784
    const/4 v0, 0x5

    new-array v0, v0, [LX/CwW;

    sget-object v1, LX/CwW;->SINGLE:LX/CwW;

    aput-object v1, v0, v2

    sget-object v1, LX/CwW;->PARALLEL_PRIMARY:LX/CwW;

    aput-object v1, v0, v3

    sget-object v1, LX/CwW;->PARALLEL_SECONDARY:LX/CwW;

    aput-object v1, v0, v4

    sget-object v1, LX/CwW;->STREAMING:LX/CwW;

    aput-object v1, v0, v5

    sget-object v1, LX/CwW;->BOOTSTRAP_ENTITIES:LX/CwW;

    aput-object v1, v0, v6

    sput-object v0, LX/CwW;->$VALUES:[LX/CwW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1950785
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CwW;
    .locals 1

    .prologue
    .line 1950786
    const-class v0, LX/CwW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CwW;

    return-object v0
.end method

.method public static values()[LX/CwW;
    .locals 1

    .prologue
    .line 1950787
    sget-object v0, LX/CwW;->$VALUES:[LX/CwW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CwW;

    return-object v0
.end method
