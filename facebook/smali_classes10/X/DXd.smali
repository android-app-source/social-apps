.class public LX/DXd;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DXa;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DXe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2010537
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DXd;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DXe;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2010538
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2010539
    iput-object p1, p0, LX/DXd;->b:LX/0Ot;

    .line 2010540
    return-void
.end method

.method public static a(LX/0QB;)LX/DXd;
    .locals 4

    .prologue
    .line 2010526
    const-class v1, LX/DXd;

    monitor-enter v1

    .line 2010527
    :try_start_0
    sget-object v0, LX/DXd;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2010528
    sput-object v2, LX/DXd;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2010529
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2010530
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2010531
    new-instance v3, LX/DXd;

    const/16 p0, 0x2487

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DXd;-><init>(LX/0Ot;)V

    .line 2010532
    move-object v0, v3

    .line 2010533
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2010534
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DXd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2010535
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2010536
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2010541
    check-cast p2, LX/DXb;

    .line 2010542
    iget-object v0, p0, LX/DXd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DXe;

    iget-object v1, p2, LX/DXb;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    iget-boolean v2, p2, LX/DXb;->b:Z

    const/4 p2, 0x2

    .line 2010543
    if-nez v1, :cond_0

    .line 2010544
    const/4 v3, 0x0

    .line 2010545
    :goto_0
    move-object v0, v3

    .line 2010546
    return-object v0

    .line 2010547
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->n()Z

    move-result v4

    .line 2010548
    iget-object v3, v0, LX/DXe;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const p0, 0x7f0208fe

    invoke-virtual {v3, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const p0, -0x413d37

    invoke-virtual {v3, p0}, LX/2xv;->i(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 2010549
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p0

    const p2, 0x7f0207fb

    invoke-interface {p0, p2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object p0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {p0, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->o()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const p2, 0x7f0b0050

    invoke-virtual {v3, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p2

    if-eqz v4, :cond_4

    const v3, 0x7f0a00fc

    :goto_1
    invoke-virtual {p2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    sget-object p2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, p2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-interface {v3, p2}, LX/1Di;->b(F)LX/1Di;

    move-result-object v3

    invoke-interface {p0, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    .line 2010550
    if-nez v2, :cond_1

    if-eqz v4, :cond_2

    .line 2010551
    :cond_1
    if-eqz v4, :cond_5

    .line 2010552
    iget-object v3, v0, LX/DXe;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const p2, 0x7f0207c2

    invoke-virtual {v3, p2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const p2, -0x5c1c2

    invoke-virtual {v3, p2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 2010553
    :goto_2
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {p0, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2010554
    :cond_2
    if-nez v4, :cond_3

    .line 2010555
    const v3, -0x65574241

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object p1, v4, p2

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2010556
    invoke-interface {p0, v3}, LX/1Dh;->e(LX/1dQ;)LX/1Dh;

    .line 2010557
    :cond_3
    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 2010558
    :cond_4
    const v3, 0x7f0a00f9

    goto :goto_1

    .line 2010559
    :cond_5
    iget-object v3, v0, LX/DXe;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const p2, 0x7f0207da

    invoke-virtual {v3, p2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const p2, -0xbd48d6

    invoke-virtual {v3, p2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2010504
    invoke-static {}, LX/1dS;->b()V

    .line 2010505
    iget v0, p1, LX/1dQ;->b:I

    .line 2010506
    packed-switch v0, :pswitch_data_0

    .line 2010507
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2010508
    :pswitch_0
    check-cast p2, LX/48C;

    .line 2010509
    iget-object v1, p2, LX/48C;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/1De;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 2010510
    check-cast v2, LX/DXb;

    .line 2010511
    iget-object v3, p0, LX/DXd;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DXe;

    iget-object v4, v2, LX/DXb;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010512
    iget-object p1, v3, LX/DXe;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DXP;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 2010513
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    .line 2010514
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2010515
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v4}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2010516
    const-string v2, "text/plain"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2010517
    iget-object v2, p1, LX/DXP;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v1, 0x7f083099

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p0

    invoke-interface {v2, p0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2010518
    iget-object p1, v0, LX/1De;->g:LX/1X1;

    move-object p1, p1

    .line 2010519
    if-nez p1, :cond_0

    .line 2010520
    :goto_1
    const/4 p1, 0x1

    move v3, p1

    .line 2010521
    move v0, v3

    .line 2010522
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2010523
    :cond_0
    check-cast p1, LX/DXb;

    .line 2010524
    new-instance p2, LX/DXc;

    iget-object p0, p1, LX/DXb;->c:LX/DXd;

    invoke-direct {p2, p0}, LX/DXc;-><init>(LX/DXd;)V

    move-object p1, p2

    .line 2010525
    invoke-virtual {v0, p1}, LX/1De;->a(LX/48B;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x65574241
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 2010500
    check-cast p1, LX/DXb;

    .line 2010501
    check-cast p2, LX/DXb;

    .line 2010502
    iget-boolean v0, p1, LX/DXb;->b:Z

    iput-boolean v0, p2, LX/DXb;->b:Z

    .line 2010503
    return-void
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 2010490
    check-cast p2, LX/DXb;

    .line 2010491
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 2010492
    iget-object v0, p0, LX/DXd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2010493
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2010494
    iput-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2010495
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2010496
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/DXb;->b:Z

    .line 2010497
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 2010498
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2010499
    const/4 v0, 0x1

    return v0
.end method
