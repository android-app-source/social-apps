.class public LX/EPA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EJ5;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/CvY;

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Ot;LX/CvY;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EJ5;",
            ">;",
            "LX/CvY;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2116028
    iput-object p1, p0, LX/EPA;->a:LX/0Ot;

    .line 2116029
    iput-object p2, p0, LX/EPA;->b:LX/CvY;

    .line 2116030
    iput-object p3, p0, LX/EPA;->c:LX/0ad;

    .line 2116031
    return-void
.end method

.method public static a(LX/0QB;)LX/EPA;
    .locals 6

    .prologue
    .line 2116016
    const-class v1, LX/EPA;

    monitor-enter v1

    .line 2116017
    :try_start_0
    sget-object v0, LX/EPA;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116018
    sput-object v2, LX/EPA;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116019
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116020
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116021
    new-instance v5, LX/EPA;

    const/16 v3, 0x337d

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v3

    check-cast v3, LX/CvY;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, p0, v3, v4}, LX/EPA;-><init>(LX/0Ot;LX/CvY;LX/0ad;)V

    .line 2116022
    move-object v0, v5

    .line 2116023
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116024
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EPA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116025
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116026
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1Ps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Ps;",
            ">(TE;)Z"
        }
    .end annotation

    .prologue
    .line 2116004
    invoke-interface {p0}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    .line 2116005
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    if-nez v1, :cond_0

    instance-of v0, v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1Ps;LX/EK5;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Ps;",
            ">(TE;",
            "LX/EK5;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2116006
    iget-object v0, p0, LX/EPA;->c:LX/0ad;

    sget-short v1, LX/100;->X:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2116007
    invoke-static {p1}, LX/EPA;->a(LX/1Ps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2116008
    const v0, 0x7f0e095a

    invoke-virtual {p2, v0}, LX/EK5;->m(I)LX/EK5;

    move-result-object v0

    const v1, 0x7f0b14af

    invoke-virtual {v0, v1}, LX/EK5;->k(I)LX/EK5;

    move-result-object v0

    const v1, 0x7f0b14af

    invoke-virtual {v0, v1}, LX/EK5;->j(I)LX/EK5;

    .line 2116009
    :cond_0
    :goto_0
    return-void

    .line 2116010
    :cond_1
    const v0, 0x7f0e095b

    .line 2116011
    iget-object v1, p2, LX/EK5;->a:LX/EK6;

    iput v0, v1, LX/EK6;->k:I

    .line 2116012
    move-object v0, p2

    .line 2116013
    const v1, 0x7f0e095a

    invoke-virtual {v0, v1}, LX/EK5;->m(I)LX/EK5;

    .line 2116014
    :cond_2
    iget-object v0, p0, LX/EPA;->c:LX/0ad;

    sget-short v1, LX/100;->Y:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116015
    const v0, 0x7f0b1712

    invoke-virtual {p2, v0}, LX/EK5;->k(I)LX/EK5;

    move-result-object v0

    const v1, 0x7f0b1712

    invoke-virtual {v0, v1}, LX/EK5;->j(I)LX/EK5;

    goto :goto_0
.end method
