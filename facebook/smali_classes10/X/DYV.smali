.class public final LX/DYV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DKm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/DKm",
        "<",
        "Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;",
        "Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberrequests/MemberRequestsFragment;)V
    .locals 0

    .prologue
    .line 2011544
    iput-object p1, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/DKn;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;",
            ">;)",
            "LX/DKn",
            "<",
            "Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2011559
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2011560
    if-eqz v0, :cond_4

    .line 2011561
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2011562
    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2011563
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2011564
    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2011565
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2011566
    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2011567
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2011568
    iget-object v6, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2011569
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2011570
    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    if-eq v7, v0, :cond_0

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2011571
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2011572
    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    if-ne v7, v0, :cond_3

    :cond_0
    move v0, v2

    .line 2011573
    :goto_0
    iput-boolean v0, v6, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->h:Z

    .line 2011574
    if-eqz v5, :cond_1

    invoke-virtual {v4, v5, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2011575
    :cond_1
    if-eqz v5, :cond_2

    invoke-virtual {v4, v5, v2}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v2

    .line 2011576
    :cond_2
    iget-object v2, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    .line 2011577
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2011578
    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    .line 2011579
    iput-object v0, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->j:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    .line 2011580
    new-instance v2, LX/DKn;

    .line 2011581
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2011582
    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel;->a()LX/0Px;

    move-result-object v0

    invoke-direct {v2, v0, v1, v3}, LX/DKn;-><init>(LX/0Px;ZLjava/lang/String;)V

    move-object v0, v2

    .line 2011583
    :goto_1
    return-object v0

    .line 2011584
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    .line 2011585
    goto :goto_0

    :cond_4
    move-object v0, v3

    .line 2011586
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0zS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2011551
    iget-object v0, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2011552
    invoke-static {}, LX/0Vg;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2011553
    :goto_0
    return-object v0

    .line 2011554
    :cond_0
    new-instance v0, LX/DYb;

    invoke-direct {v0}, LX/DYb;-><init>()V

    move-object v0, v0

    .line 2011555
    const-string v1, "group_id"

    iget-object v2, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_size"

    iget-object v3, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b2074

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "group_member_requests_page_size"

    const-string v3, "10"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2011556
    if-eqz p1, :cond_1

    .line 2011557
    const-string v1, "group_member_requests_page_cursor"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2011558
    :cond_1
    iget-object v1, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->e:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0Px;Z)V
    .locals 6

    .prologue
    .line 2011545
    iget-object v0, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    .line 2011546
    iput-boolean p2, v0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->m:Z

    .line 2011547
    iget-object v0, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    .line 2011548
    iput-object p1, v0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->i:LX/0Px;

    .line 2011549
    iget-object v0, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->g:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v1, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->j:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    iget-object v2, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->i:LX/0Px;

    iget-object v3, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object v3, v3, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->c:LX/DYT;

    const/4 v4, 0x0

    iget-object v5, p0, LX/DYV;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-boolean v5, v5, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->h:Z

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->a(Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;LX/0Px;LX/DYT;ZZ)V

    .line 2011550
    return-void
.end method
