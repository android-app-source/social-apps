.class public final LX/Dzp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/widget/EditText;

.field public final b:Landroid/widget/EditText;

.field public final c:Landroid/widget/EditText;

.field public final d:Lcom/facebook/resources/ui/FbTextView;

.field public final e:Lcom/facebook/resources/ui/FbTextView;

.field public final f:Landroid/widget/CheckBox;

.field public final g:Lcom/facebook/maps/FbStaticMapView;

.field public final h:Landroid/widget/ImageView;

.field public final i:Landroid/widget/ImageView;

.field public final j:Landroid/view/View;

.field public final synthetic k:Lcom/facebook/places/create/NewPlaceCreationFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2067182
    iput-object p1, p0, LX/Dzp;->k:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067183
    const v0, 0x7f0d162b

    .line 2067184
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067185
    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LX/Dzp;->a:Landroid/widget/EditText;

    .line 2067186
    const v0, 0x7f0d1da9

    .line 2067187
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067188
    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LX/Dzp;->b:Landroid/widget/EditText;

    .line 2067189
    const v0, 0x7f0d1dab

    .line 2067190
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067191
    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LX/Dzp;->c:Landroid/widget/EditText;

    .line 2067192
    const v0, 0x7f0d162c

    .line 2067193
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067194
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Dzp;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2067195
    const v0, 0x7f0d1daa

    .line 2067196
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067197
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Dzp;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2067198
    const v0, 0x7f0d1dad

    .line 2067199
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067200
    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, LX/Dzp;->f:Landroid/widget/CheckBox;

    .line 2067201
    const v0, 0x7f0d1dac

    .line 2067202
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067203
    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, LX/Dzp;->g:Lcom/facebook/maps/FbStaticMapView;

    .line 2067204
    const v0, 0x7f0d1da6

    .line 2067205
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067206
    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Dzp;->h:Landroid/widget/ImageView;

    .line 2067207
    const v0, 0x7f0d1da8

    .line 2067208
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067209
    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Dzp;->i:Landroid/widget/ImageView;

    .line 2067210
    const v0, 0x7f0d1da7

    .line 2067211
    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2067212
    iput-object v0, p0, LX/Dzp;->j:Landroid/view/View;

    .line 2067213
    return-void
.end method
