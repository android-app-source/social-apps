.class public final LX/E91;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/1Zj;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/E92;


# direct methods
.method public constructor <init>(LX/E92;Ljava/lang/String;LX/1Zj;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2083606
    iput-object p1, p0, LX/E91;->d:LX/E92;

    iput-object p2, p0, LX/E91;->a:Ljava/lang/String;

    iput-object p3, p0, LX/E91;->b:LX/1Zj;

    iput-object p4, p0, LX/E91;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2083607
    const/4 v5, 0x0

    .line 2083608
    iget-object v0, p0, LX/E91;->d:LX/E92;

    iget-object v0, v0, LX/E92;->a:LX/E93;

    iget-object v0, v0, LX/E8m;->e:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    if-eqz v0, :cond_0

    .line 2083609
    iget-object v0, p0, LX/E91;->d:LX/E92;

    iget-object v0, v0, LX/E92;->a:LX/E93;

    iget-object v0, v0, LX/E93;->y:LX/CvY;

    iget-object v1, p0, LX/E91;->d:LX/E92;

    iget-object v1, v1, LX/E92;->a:LX/E93;

    iget-object v1, v1, LX/E8m;->e:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iget-object v2, p0, LX/E91;->d:LX/E92;

    iget-object v2, v2, LX/E92;->a:LX/E93;

    iget-object v2, v2, LX/E93;->x:LX/E1l;

    iget-object v3, p0, LX/E91;->a:Ljava/lang/String;

    .line 2083610
    const/4 v4, 0x0

    move v6, v4

    :goto_0
    iget-object v4, v2, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v6, v4, :cond_3

    .line 2083611
    iget-object v4, v2, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Cfo;

    invoke-interface {v4}, LX/Cfo;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2083612
    if-eqz v4, :cond_2

    .line 2083613
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2083614
    :goto_1
    move v2, v6

    .line 2083615
    iget-object v3, p0, LX/E91;->a:Ljava/lang/String;

    iget-object v4, p0, LX/E91;->b:LX/1Zj;

    iget-boolean v4, v4, LX/1Zj;->e:Z

    if-eqz v4, :cond_1

    sget-object v4, LX/8ch;->LIKED:LX/8ch;

    :goto_2
    move-object v6, v5

    .line 2083616
    sget-object p0, LX/CvJ;->ITEM_TAPPED:LX/CvJ;

    invoke-static {p0, v1}, LX/CvY;->a(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "action"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "tapped_result_position"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "tapped_result_entity_id"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "results_module_role"

    invoke-virtual {p0, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "results_module_extra_logging"

    invoke-virtual {p0, p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 2083617
    invoke-static {v0, v1, p0}, LX/CvY;->a(LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2083618
    :cond_0
    return-void

    .line 2083619
    :cond_1
    sget-object v4, LX/8ch;->UNLIKED:LX/8ch;

    goto :goto_2

    .line 2083620
    :cond_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 2083621
    :cond_3
    const/4 v6, -0x1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 2083622
    iget-object v0, p0, LX/E91;->d:LX/E92;

    iget-object v0, v0, LX/E92;->a:LX/E93;

    iget-object v0, v0, LX/E93;->z:LX/3mH;

    iget-object v1, p0, LX/E91;->c:Ljava/lang/String;

    iget-object v2, p0, LX/E91;->d:LX/E92;

    iget-object v2, v2, LX/E92;->a:LX/E93;

    iget-object v3, p0, LX/E91;->b:LX/1Zj;

    iget-boolean v3, v3, LX/1Zj;->e:Z

    invoke-virtual {p2}, Lcom/facebook/fbservice/service/ServiceException;->getMessage()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/AkV;->FAILURE:LX/AkV;

    invoke-virtual/range {v0 .. v5}, LX/3mH;->a(Ljava/lang/String;Ljava/lang/Object;ZLjava/lang/String;LX/AkV;)V

    .line 2083623
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2083624
    iget-object v0, p0, LX/E91;->d:LX/E92;

    iget-object v0, v0, LX/E92;->a:LX/E93;

    iget-object v0, v0, LX/E93;->z:LX/3mH;

    iget-object v1, p0, LX/E91;->c:Ljava/lang/String;

    iget-object v2, p0, LX/E91;->d:LX/E92;

    iget-object v2, v2, LX/E92;->a:LX/E93;

    iget-object v3, p0, LX/E91;->b:LX/1Zj;

    iget-boolean v3, v3, LX/1Zj;->e:Z

    const/4 v4, 0x0

    sget-object v5, LX/AkV;->SUCCESS:LX/AkV;

    invoke-virtual/range {v0 .. v5}, LX/3mH;->a(Ljava/lang/String;Ljava/lang/Object;ZLjava/lang/String;LX/AkV;)V

    .line 2083625
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2083626
    return-void
.end method
