.class public LX/DBC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/events/common/EventAnalyticsParams;

.field private b:LX/0Tn;

.field public final c:Landroid/content/Context;

.field public final d:LX/6RZ;

.field private final e:LX/Bla;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field public final g:LX/1Ck;

.field public final h:LX/Bl6;

.field public final i:LX/0tX;

.field public final j:LX/B9y;

.field private final k:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6RZ;LX/Bla;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/Bl6;LX/0tX;LX/B9y;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 1
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/6RZ;",
            "LX/Bla;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1Ck;",
            "LX/Bl6;",
            "LX/0tX;",
            "LX/B9y;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1972496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1972497
    iput-object p1, p0, LX/DBC;->c:Landroid/content/Context;

    .line 1972498
    iput-object p2, p0, LX/DBC;->d:LX/6RZ;

    .line 1972499
    iput-object p3, p0, LX/DBC;->e:LX/Bla;

    .line 1972500
    iput-object p4, p0, LX/DBC;->f:Lcom/facebook/content/SecureContextHelper;

    .line 1972501
    iput-object p5, p0, LX/DBC;->g:LX/1Ck;

    .line 1972502
    iput-object p6, p0, LX/DBC;->h:LX/Bl6;

    .line 1972503
    iput-object p7, p0, LX/DBC;->i:LX/0tX;

    .line 1972504
    iput-object p8, p0, LX/DBC;->j:LX/B9y;

    .line 1972505
    iput-object p9, p0, LX/DBC;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1972506
    invoke-interface {p10}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    iput-object v0, p0, LX/DBC;->b:LX/0Tn;

    .line 1972507
    return-void
.end method

.method private static a(Ljava/util/Date;)J
    .locals 2

    .prologue
    .line 1972495
    if-nez p0, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/DBC;
    .locals 14

    .prologue
    .line 1972484
    const-class v1, LX/DBC;

    monitor-enter v1

    .line 1972485
    :try_start_0
    sget-object v0, LX/DBC;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1972486
    sput-object v2, LX/DBC;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1972487
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1972488
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1972489
    new-instance v3, LX/DBC;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v5

    check-cast v5, LX/6RZ;

    invoke-static {v0}, LX/Bla;->b(LX/0QB;)LX/Bla;

    move-result-object v6

    check-cast v6, LX/Bla;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {v0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v9

    check-cast v9, LX/Bl6;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    invoke-static {v0}, LX/B9y;->a(LX/0QB;)LX/B9y;

    move-result-object v11

    check-cast v11, LX/B9y;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v12

    check-cast v12, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v13, 0x15e7

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/DBC;-><init>(Landroid/content/Context;LX/6RZ;LX/Bla;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/Bl6;LX/0tX;LX/B9y;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 1972490
    move-object v0, v3

    .line 1972491
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1972492
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DBC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1972493
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1972494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(J)Ljava/util/Date;
    .locals 2

    .prologue
    .line 1972483
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public static a(LX/0Rf;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1972479
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {p0}, LX/0Rf;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 1972480
    invoke-virtual {p0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1972481
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1972482
    :cond_0
    return-object v1
.end method

.method public static a(LX/DBC;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1972355
    iget-object v0, p0, LX/DBC;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v0, :cond_0

    .line 1972356
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mEventAnalyticsParams is not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1972357
    :cond_0
    if-nez p1, :cond_1

    .line 1972358
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "refActionMechanism is not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1972359
    :cond_1
    return-void
.end method

.method public static a(LX/DBC;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Rf;)V
    .locals 5
    .param p0    # LX/DBC;
        .annotation build Lcom/facebook/graphql/calls/EventInviteNotificationType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1972450
    invoke-static {p0, p3}, LX/DBC;->a(LX/DBC;Ljava/lang/String;)V

    .line 1972451
    invoke-static {p0, p2}, LX/DBC;->b(LX/DBC;Ljava/lang/String;)LX/0Ve;

    move-result-object v1

    .line 1972452
    iget-object v0, p0, LX/DBC;->h:LX/Bl6;

    new-instance v2, LX/BlV;

    invoke-direct {v2}, LX/BlV;-><init>()V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1972453
    invoke-virtual {p4}, LX/0Rf;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1972454
    invoke-virtual {p4}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1972455
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1972456
    :cond_0
    new-instance v3, LX/4EG;

    invoke-direct {v3}, LX/4EG;-><init>()V

    .line 1972457
    iget-object v0, p0, LX/DBC;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1972458
    new-instance v4, LX/4EG;

    invoke-direct {v4}, LX/4EG;-><init>()V

    .line 1972459
    iget-object v0, p0, LX/DBC;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v4, v0}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1972460
    invoke-virtual {v4, p3}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1972461
    invoke-static {p0}, LX/DBC;->a(LX/DBC;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "contacts_upload_on"

    :goto_1
    invoke-virtual {v4, v0}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    .line 1972462
    new-instance v0, LX/4EL;

    invoke-direct {v0}, LX/4EL;-><init>()V

    .line 1972463
    invoke-static {v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1972464
    new-instance v3, LX/4EO;

    invoke-direct {v3}, LX/4EO;-><init>()V

    .line 1972465
    const-string v4, "context"

    invoke-virtual {v3, v4, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1972466
    move-object v0, v3

    .line 1972467
    const-string v3, "event_id"

    invoke-virtual {v0, v3, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972468
    move-object v0, v0

    .line 1972469
    const-string v3, "invitee_ids"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1972470
    move-object v0, v0

    .line 1972471
    const-string v2, "invite_notification_type"

    invoke-virtual {v0, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972472
    move-object v0, v0

    .line 1972473
    new-instance v2, LX/7uE;

    invoke-direct {v2}, LX/7uE;-><init>()V

    move-object v2, v2

    .line 1972474
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7uE;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1972475
    iget-object v2, p0, LX/DBC;->i:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1972476
    iget-object v2, p0, LX/DBC;->g:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tasks-inviteToEvent:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1972477
    return-void

    .line 1972478
    :cond_1
    const-string v0, "contacts_upload_off"

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;LX/0Rf;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1972444
    const-string v0, "SUPPRESS_PUSH"

    invoke-static {p0, v0, p1, p2, p4}, LX/DBC;->a(LX/DBC;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Rf;)V

    .line 1972445
    if-eqz p3, :cond_0

    .line 1972446
    iget-object v1, p0, LX/DBC;->j:LX/B9y;

    iget-object v2, p0, LX/DBC;->c:Landroid/content/Context;

    const-class v3, Landroid/app/Activity;

    invoke-static {v2, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const-string v3, "extra_event_name"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v3, "extra_event_cover_photo_uri"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v3, "extra_event_location_name"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v3, "extra_event_start_time"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, LX/DBC;->a(J)Ljava/util/Date;

    move-result-object v3

    const-string v7, "extra_event_end_time"

    invoke-virtual {p3, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, LX/DBC;->a(J)Ljava/util/Date;

    move-result-object v7

    .line 1972447
    if-eqz v3, :cond_1

    iget-object v8, p0, LX/DBC;->d:LX/6RZ;

    const/4 v9, 0x0

    invoke-virtual {v8, v9, v3, v7}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    :goto_0
    move-object v7, v8

    .line 1972448
    invoke-static {p4}, LX/DBC;->a(LX/0Rf;)Ljava/util/Set;

    move-result-object v8

    const-string v9, "event"

    const/16 v10, 0x64

    move-object v3, p1

    invoke-virtual/range {v1 .. v10}, LX/B9y;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;I)V

    .line 1972449
    :cond_0
    return-void

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public static a(LX/DBC;)Z
    .locals 3

    .prologue
    .line 1972443
    iget-object v0, p0, LX/DBC;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/DBC;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public static b(LX/DBC;Ljava/lang/String;)LX/0Ve;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 1972439
    iget-object v0, p0, LX/DBC;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1972440
    iget-object v1, p0, LX/DBC;->c:Landroid/content/Context;

    const v2, 0x7f082f31

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1972441
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1972442
    new-instance v2, LX/DBB;

    invoke-direct {v2, p0, p1, v1, v0}, LX/DBB;-><init>(LX/DBC;Ljava/lang/String;Landroid/widget/Toast;Landroid/content/res/Resources;)V

    return-object v2
.end method

.method public static b(LX/DBC;Lcom/facebook/events/model/Event;Ljava/lang/String;LX/Blb;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 1972416
    iget-object v0, p0, LX/DBC;->e:LX/Bla;

    .line 1972417
    iget-object v1, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1972418
    iget-object v2, p1, Lcom/facebook/events/model/Event;->t:Ljava/lang/String;

    move-object v2, v2

    .line 1972419
    iget-object v3, p1, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v3, v3

    .line 1972420
    sget-object v4, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {p1, v4}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v4

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, LX/Bla;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1972421
    if-eqz v0, :cond_1

    .line 1972422
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1972423
    const-string v2, "extra_event_name"

    .line 1972424
    iget-object v3, p1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1972425
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972426
    const-string v2, "extra_invite_entry_point"

    invoke-virtual {p3}, LX/Blb;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972427
    const-string v2, "extra_event_location_name"

    .line 1972428
    iget-object v3, p1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v3, v3

    .line 1972429
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972430
    const-string v2, "extra_event_start_time"

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v3

    invoke-static {v3}, LX/DBC;->a(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1972431
    const-string v2, "extra_event_end_time"

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v3

    invoke-static {v3}, LX/DBC;->a(Ljava/util/Date;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1972432
    iget-object v2, p1, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    move-object v2, v2

    .line 1972433
    if-eqz v2, :cond_0

    .line 1972434
    const-string v2, "extra_event_cover_photo_uri"

    .line 1972435
    iget-object v3, p1, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    move-object v3, v3

    .line 1972436
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972437
    :cond_0
    const-string v2, "extra_invite_configuration_bundle"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1972438
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1972372
    const/16 v0, 0x1f5

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1972373
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 1972374
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1972375
    goto :goto_0

    .line 1972376
    :cond_2
    const-string v0, "profiles"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1972377
    const-string v0, "event_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1972378
    const-string v0, "extra_invite_action_mechanism"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1972379
    const-string v0, "extra_enable_extended_invite"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1972380
    const-string v0, "profiles"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1972381
    array-length v0, v1

    if-lez v0, :cond_0

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1972382
    const-string v0, "extra_events_note_text"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "extra_events_note_text"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 1972383
    invoke-static {p0, v3}, LX/DBC;->a(LX/DBC;Ljava/lang/String;)V

    .line 1972384
    invoke-static {p0, v2}, LX/DBC;->b(LX/DBC;Ljava/lang/String;)LX/0Ve;

    move-result-object v5

    .line 1972385
    iget-object v4, p0, LX/DBC;->h:LX/Bl6;

    new-instance p1, LX/BlV;

    invoke-direct {p1}, LX/BlV;-><init>()V

    invoke-virtual {v4, p1}, LX/0b4;->a(LX/0b7;)V

    .line 1972386
    new-instance p1, LX/4EG;

    invoke-direct {p1}, LX/4EG;-><init>()V

    .line 1972387
    iget-object v4, p0, LX/DBC;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v4}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1972388
    new-instance p2, LX/4EG;

    invoke-direct {p2}, LX/4EG;-><init>()V

    .line 1972389
    iget-object v4, p0, LX/DBC;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {p2, v4}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1972390
    invoke-virtual {p2, v3}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1972391
    invoke-static {p0}, LX/DBC;->a(LX/DBC;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "contacts_upload_on"

    :goto_3
    invoke-virtual {p2, v4}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    .line 1972392
    new-instance v4, LX/4EL;

    invoke-direct {v4}, LX/4EL;-><init>()V

    .line 1972393
    invoke-static {p1, p2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    invoke-virtual {v4, p1}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1972394
    new-instance p1, LX/4Ec;

    invoke-direct {p1}, LX/4Ec;-><init>()V

    .line 1972395
    const-string p2, "context"

    invoke-virtual {p1, p2, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1972396
    move-object v4, p1

    .line 1972397
    const-string p1, "event_id"

    invoke-virtual {v4, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972398
    move-object v4, v4

    .line 1972399
    const-string p1, "tokens"

    invoke-virtual {v4, p1, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1972400
    move-object v4, v4

    .line 1972401
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 1972402
    const-string p1, "message"

    invoke-virtual {v4, p1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972403
    :cond_3
    new-instance p1, LX/7uM;

    invoke-direct {p1}, LX/7uM;-><init>()V

    move-object p1, p1

    .line 1972404
    const-string p2, "input"

    invoke-virtual {p1, p2, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/7uM;

    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 1972405
    iget-object p1, p0, LX/DBC;->i:LX/0tX;

    invoke-virtual {p1, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1972406
    iget-object p1, p0, LX/DBC;->g:LX/1Ck;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "tasks-unifiedInviteToEvent:"

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1972407
    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1972408
    :cond_5
    const-string v0, "profiles"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    .line 1972409
    array-length v4, v0

    if-lez v4, :cond_0

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1972410
    const-string v4, "extra_redirect_to_messenger"

    invoke-virtual {p3, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1972411
    const-string v1, "extra_invite_configuration_bundle"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0}, LX/1YA;->a([J)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    invoke-direct {p0, v2, v3, v1, v0}, LX/DBC;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;LX/0Rf;)V

    goto/16 :goto_1

    .line 1972412
    :cond_6
    invoke-static {v0}, LX/1YA;->a([J)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    .line 1972413
    const-string v1, "NORMAL"

    invoke-static {p0, v1, v2, v3, v0}, LX/DBC;->a(LX/DBC;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Rf;)V

    .line 1972414
    goto/16 :goto_1

    .line 1972415
    :cond_7
    const-string v4, "contacts_upload_off"

    goto/16 :goto_3
.end method

.method public final a(Lcom/facebook/events/model/Event;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1972369
    if-nez p1, :cond_0

    .line 1972370
    :goto_0
    return-void

    .line 1972371
    :cond_0
    sget-object v0, LX/Blb;->FACEBOOK:LX/Blb;

    invoke-virtual {p0, p1, p2, v0}, LX/DBC;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;LX/Blb;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/model/Event;Ljava/lang/String;LX/Blb;)V
    .locals 5

    .prologue
    .line 1972364
    if-nez p1, :cond_1

    .line 1972365
    :cond_0
    :goto_0
    return-void

    .line 1972366
    :cond_1
    invoke-static {p0, p1, p2, p3}, LX/DBC;->b(LX/DBC;Lcom/facebook/events/model/Event;Ljava/lang/String;LX/Blb;)Landroid/content/Intent;

    move-result-object v1

    .line 1972367
    if-eqz v1, :cond_0

    .line 1972368
    iget-object v2, p0, LX/DBC;->f:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x1f5

    iget-object v0, p0, LX/DBC;->c:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;ZLjava/lang/String;ILandroid/app/Activity;)V
    .locals 6

    .prologue
    .line 1972360
    iget-object v0, p0, LX/DBC;->e:LX/Bla;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/Bla;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1972361
    if-eqz v0, :cond_0

    .line 1972362
    iget-object v1, p0, LX/DBC;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p6, p7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1972363
    :cond_0
    return-void
.end method
