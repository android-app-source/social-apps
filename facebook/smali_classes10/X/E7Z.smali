.class public final LX/E7Z;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;)V
    .locals 0

    .prologue
    .line 2081788
    iput-object p1, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2081789
    iget-object v0, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    const/4 v1, 0x0

    .line 2081790
    iput-object v1, v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->h:Ljava/lang/String;

    .line 2081791
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2081792
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2081793
    iget-object v0, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    const/4 v1, 0x0

    .line 2081794
    iput-boolean v1, v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->e:Z

    .line 2081795
    iget-object v0, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    invoke-virtual {v0, p1}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/util/List;

    move-result-object v0

    .line 2081796
    if-nez v0, :cond_0

    .line 2081797
    iget-object v0, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    const/4 v1, 0x0

    .line 2081798
    iput-object v1, v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->h:Ljava/lang/String;

    .line 2081799
    :goto_0
    return-void

    .line 2081800
    :cond_0
    iget-object v1, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v1, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->j:LX/E7X;

    invoke-virtual {v1, v0}, LX/E7X;->a(Ljava/util/List;)V

    .line 2081801
    iget-object v1, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v2, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v2, v2, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->l:Ljava/lang/String;

    iget-object v3, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v3, v3, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->m:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v4, p0, LX/E7Z;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v4, v4, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->j:LX/E7X;

    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v4

    .line 2081802
    invoke-virtual {v1, v2, v3, v0, v4}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2081803
    goto :goto_0
.end method
