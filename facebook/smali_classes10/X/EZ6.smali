.class public LX/EZ6;
.super LX/EWc;
.source ""


# static fields
.field private static final c:[I


# instance fields
.field private final d:I

.field public final e:LX/EWc;

.field public final f:LX/EWc;

.field private final g:I

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2138660
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 2138661
    :goto_0
    if-lez v0, :cond_0

    .line 2138662
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2138663
    add-int/2addr v1, v0

    move v4, v1

    move v1, v0

    move v0, v4

    .line 2138664
    goto :goto_0

    .line 2138665
    :cond_0
    const v0, 0x7fffffff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2138666
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    sput-object v0, LX/EZ6;->c:[I

    .line 2138667
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    sget-object v0, LX/EZ6;->c:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 2138668
    sget-object v3, LX/EZ6;->c:[I

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 2138669
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2138670
    :cond_1
    return-void
.end method

.method private a(LX/EWc;)Z
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 2138637
    new-instance v7, LX/EZ3;

    invoke-direct {v7, p0}, LX/EZ3;-><init>(LX/EWc;)V

    .line 2138638
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYy;

    .line 2138639
    new-instance v8, LX/EZ3;

    invoke-direct {v8, p1}, LX/EZ3;-><init>(LX/EWc;)V

    .line 2138640
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYy;

    move-object v3, v1

    move v4, v2

    move-object v5, v0

    move v6, v2

    move v0, v2

    .line 2138641
    :goto_0
    invoke-virtual {v5}, LX/EYy;->b()I

    move-result v1

    sub-int v9, v1, v6

    .line 2138642
    invoke-virtual {v3}, LX/EYy;->b()I

    move-result v1

    sub-int v10, v1, v4

    .line 2138643
    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 2138644
    if-nez v6, :cond_0

    invoke-virtual {v5, v3, v4, v11}, LX/EYy;->a(LX/EYy;II)Z

    move-result v1

    .line 2138645
    :goto_1
    if-nez v1, :cond_1

    .line 2138646
    :goto_2
    return v2

    .line 2138647
    :cond_0
    invoke-virtual {v3, v5, v6, v11}, LX/EYy;->a(LX/EYy;II)Z

    move-result v1

    goto :goto_1

    .line 2138648
    :cond_1
    add-int v1, v0, v11

    .line 2138649
    iget v0, p0, LX/EZ6;->d:I

    if-lt v1, v0, :cond_3

    .line 2138650
    iget v0, p0, LX/EZ6;->d:I

    if-ne v1, v0, :cond_2

    .line 2138651
    const/4 v2, 0x1

    goto :goto_2

    .line 2138652
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2138653
    :cond_3
    if-ne v11, v9, :cond_4

    .line 2138654
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYy;

    move-object v5, v0

    move v6, v2

    .line 2138655
    :goto_3
    if-ne v11, v10, :cond_5

    .line 2138656
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYy;

    move-object v3, v0

    move v4, v2

    move v0, v1

    goto :goto_0

    .line 2138657
    :cond_4
    add-int/2addr v6, v11

    goto :goto_3

    .line 2138658
    :cond_5
    add-int v0, v4, v11

    move v4, v0

    move v0, v1

    .line 2138659
    goto :goto_0
.end method


# virtual methods
.method public final a(I)B
    .locals 3

    .prologue
    .line 2138629
    if-gez p1, :cond_0

    .line 2138630
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Index < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2138631
    :cond_0
    iget v0, p0, LX/EZ6;->d:I

    if-le p1, v0, :cond_1

    .line 2138632
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Index > length: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/EZ6;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2138633
    :cond_1
    iget v0, p0, LX/EZ6;->g:I

    if-ge p1, v0, :cond_2

    .line 2138634
    iget-object v0, p0, LX/EZ6;->e:LX/EWc;

    invoke-virtual {v0, p1}, LX/EWc;->a(I)B

    move-result v0

    .line 2138635
    :goto_0
    return v0

    .line 2138636
    :cond_2
    iget-object v0, p0, LX/EZ6;->f:LX/EWc;

    iget v1, p0, LX/EZ6;->g:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/EWc;->a(I)B

    move-result v0

    goto :goto_0
.end method

.method public final a(III)I
    .locals 4

    .prologue
    .line 2138620
    add-int v0, p2, p3

    .line 2138621
    iget v1, p0, LX/EZ6;->g:I

    if-gt v0, v1, :cond_0

    .line 2138622
    iget-object v0, p0, LX/EZ6;->e:LX/EWc;

    invoke-virtual {v0, p1, p2, p3}, LX/EWc;->a(III)I

    move-result v0

    .line 2138623
    :goto_0
    return v0

    .line 2138624
    :cond_0
    iget v0, p0, LX/EZ6;->g:I

    if-lt p2, v0, :cond_1

    .line 2138625
    iget-object v0, p0, LX/EZ6;->f:LX/EWc;

    iget v1, p0, LX/EZ6;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1, p3}, LX/EWc;->a(III)I

    move-result v0

    goto :goto_0

    .line 2138626
    :cond_1
    iget v0, p0, LX/EZ6;->g:I

    sub-int/2addr v0, p2

    .line 2138627
    iget-object v1, p0, LX/EZ6;->e:LX/EWc;

    invoke-virtual {v1, p1, p2, v0}, LX/EWc;->a(III)I

    move-result v1

    .line 2138628
    iget-object v2, p0, LX/EZ6;->f:LX/EWc;

    const/4 v3, 0x0

    sub-int v0, p3, v0

    invoke-virtual {v2, v1, v3, v0}, LX/EWc;->a(III)I

    move-result v0

    goto :goto_0
.end method

.method public final a()LX/EWa;
    .locals 2

    .prologue
    .line 2138619
    new-instance v0, LX/EZ4;

    invoke-direct {v0, p0}, LX/EZ4;-><init>(LX/EZ6;)V

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2138618
    iget v0, p0, LX/EZ6;->d:I

    return v0
.end method

.method public final b(III)I
    .locals 4

    .prologue
    .line 2138609
    add-int v0, p2, p3

    .line 2138610
    iget v1, p0, LX/EZ6;->g:I

    if-gt v0, v1, :cond_0

    .line 2138611
    iget-object v0, p0, LX/EZ6;->e:LX/EWc;

    invoke-virtual {v0, p1, p2, p3}, LX/EWc;->b(III)I

    move-result v0

    .line 2138612
    :goto_0
    return v0

    .line 2138613
    :cond_0
    iget v0, p0, LX/EZ6;->g:I

    if-lt p2, v0, :cond_1

    .line 2138614
    iget-object v0, p0, LX/EZ6;->f:LX/EWc;

    iget v1, p0, LX/EZ6;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1, p3}, LX/EWc;->b(III)I

    move-result v0

    goto :goto_0

    .line 2138615
    :cond_1
    iget v0, p0, LX/EZ6;->g:I

    sub-int/2addr v0, p2

    .line 2138616
    iget-object v1, p0, LX/EZ6;->e:LX/EWc;

    invoke-virtual {v1, p1, p2, v0}, LX/EWc;->b(III)I

    move-result v1

    .line 2138617
    iget-object v2, p0, LX/EZ6;->f:LX/EWc;

    const/4 v3, 0x0

    sub-int v0, p3, v0

    invoke-virtual {v2, v1, v3, v0}, LX/EWc;->b(III)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2138671
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, LX/EWc;->d()[B

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method public final b([BIII)V
    .locals 4

    .prologue
    .line 2138601
    add-int v0, p2, p4

    iget v1, p0, LX/EZ6;->g:I

    if-gt v0, v1, :cond_0

    .line 2138602
    iget-object v0, p0, LX/EZ6;->e:LX/EWc;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/EWc;->b([BIII)V

    .line 2138603
    :goto_0
    return-void

    .line 2138604
    :cond_0
    iget v0, p0, LX/EZ6;->g:I

    if-lt p2, v0, :cond_1

    .line 2138605
    iget-object v0, p0, LX/EZ6;->f:LX/EWc;

    iget v1, p0, LX/EZ6;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1, p3, p4}, LX/EWc;->b([BIII)V

    goto :goto_0

    .line 2138606
    :cond_1
    iget v0, p0, LX/EZ6;->g:I

    sub-int/2addr v0, p2

    .line 2138607
    iget-object v1, p0, LX/EZ6;->e:LX/EWc;

    invoke-virtual {v1, p1, p2, p3, v0}, LX/EWc;->b([BIII)V

    .line 2138608
    iget-object v1, p0, LX/EZ6;->f:LX/EWc;

    const/4 v2, 0x0

    add-int v3, p3, v0

    sub-int v0, p4, v0

    invoke-virtual {v1, p1, v2, v3, v0}, LX/EWc;->b([BIII)V

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2138588
    if-ne p1, p0, :cond_1

    .line 2138589
    :cond_0
    :goto_0
    return v0

    .line 2138590
    :cond_1
    instance-of v2, p1, LX/EWc;

    if-nez v2, :cond_2

    move v0, v1

    .line 2138591
    goto :goto_0

    .line 2138592
    :cond_2
    check-cast p1, LX/EWc;

    .line 2138593
    iget v2, p0, LX/EZ6;->d:I

    invoke-virtual {p1}, LX/EWc;->b()I

    move-result v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2138594
    goto :goto_0

    .line 2138595
    :cond_3
    iget v2, p0, LX/EZ6;->d:I

    if-eqz v2, :cond_0

    .line 2138596
    iget v0, p0, LX/EZ6;->h:I

    if-eqz v0, :cond_4

    .line 2138597
    invoke-virtual {p1}, LX/EWc;->i()I

    move-result v0

    .line 2138598
    if-eqz v0, :cond_4

    iget v2, p0, LX/EZ6;->h:I

    if-eq v2, v0, :cond_4

    move v0, v1

    .line 2138599
    goto :goto_0

    .line 2138600
    :cond_4
    invoke-direct {p0, p1}, LX/EZ6;->a(LX/EWc;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2138585
    iget-object v1, p0, LX/EZ6;->e:LX/EWc;

    iget v2, p0, LX/EZ6;->g:I

    invoke-virtual {v1, v0, v0, v2}, LX/EWc;->a(III)I

    move-result v1

    .line 2138586
    iget-object v2, p0, LX/EZ6;->f:LX/EWc;

    iget-object v3, p0, LX/EZ6;->f:LX/EWc;

    invoke-virtual {v3}, LX/EWc;->b()I

    move-result v3

    invoke-virtual {v2, v1, v0, v3}, LX/EWc;->a(III)I

    move-result v1

    .line 2138587
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final g()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 2138584
    new-instance v0, LX/EZ5;

    invoke-direct {v0, p0}, LX/EZ5;-><init>(LX/EZ6;)V

    return-object v0
.end method

.method public final h()LX/EWd;
    .locals 1

    .prologue
    .line 2138581
    new-instance v0, LX/EZ5;

    invoke-direct {v0, p0}, LX/EZ5;-><init>(LX/EZ6;)V

    .line 2138582
    new-instance p0, LX/EWd;

    invoke-direct {p0, v0}, LX/EWd;-><init>(Ljava/io/InputStream;)V

    move-object v0, p0

    .line 2138583
    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2138573
    iget v0, p0, LX/EZ6;->h:I

    .line 2138574
    if-nez v0, :cond_1

    .line 2138575
    iget v0, p0, LX/EZ6;->d:I

    .line 2138576
    const/4 v1, 0x0

    iget v2, p0, LX/EZ6;->d:I

    invoke-virtual {p0, v0, v1, v2}, LX/EZ6;->b(III)I

    move-result v0

    .line 2138577
    if-nez v0, :cond_0

    .line 2138578
    const/4 v0, 0x1

    .line 2138579
    :cond_0
    iput v0, p0, LX/EZ6;->h:I

    .line 2138580
    :cond_1
    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 2138572
    iget v0, p0, LX/EZ6;->h:I

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 2138571
    invoke-virtual {p0}, LX/EZ6;->a()LX/EWa;

    move-result-object v0

    return-object v0
.end method
