.class public final LX/EfU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AFW;

.field public final synthetic b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V
    .locals 0

    .prologue
    .line 2154596
    iput-object p1, p0, LX/EfU;->b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iput-object p2, p0, LX/EfU;->a:LX/AFW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x65267e10

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2154597
    iget-object v1, p0, LX/EfU;->b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iget-object v2, p0, LX/EfU;->a:LX/AFW;

    invoke-static {v1, v2}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->f(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V

    .line 2154598
    iget-object v1, p0, LX/EfU;->b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iget-object v1, v1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->C:LX/AFQ;

    iget-object v2, p0, LX/EfU;->a:LX/AFW;

    iget-object v3, p0, LX/EfU;->b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iget-object v3, v3, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/AFQ;->a(LX/AFW;Landroid/content/Context;)V

    .line 2154599
    iget-object v1, p0, LX/EfU;->b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iget-object v2, p0, LX/EfU;->a:LX/AFW;

    .line 2154600
    iget-object v3, v2, LX/AFW;->a:LX/7h0;

    move-object v3, v3

    .line 2154601
    iget-boolean v5, v3, LX/7h0;->l:Z

    move v5, v5

    .line 2154602
    if-nez v5, :cond_0

    .line 2154603
    iget-boolean v5, v3, LX/7h0;->h:Z

    move v3, v5

    .line 2154604
    if-nez v3, :cond_0

    .line 2154605
    iget-object v3, v2, LX/AFW;->b:LX/0Px;

    move-object p0, v3

    .line 2154606
    const/4 v3, 0x1

    move v5, v3

    :goto_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_0

    .line 2154607
    invoke-virtual {p0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/audience/model/MinimalReplyThread;

    .line 2154608
    iget-object p1, v1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->F:LX/AFO;

    .line 2154609
    iget-object v2, v3, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    move-object v3, v2

    .line 2154610
    invoke-virtual {p1, v3}, LX/AFO;->a(Ljava/lang/String;)V

    .line 2154611
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 2154612
    :cond_0
    const v1, 0x139c523f

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
