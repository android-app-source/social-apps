.class public LX/Dph;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# instance fields
.field public final body:LX/Dpi;

.field public final date_micros:Ljava/lang/Long;

.field public final nonce:[B

.field public final result:Ljava/lang/Integer;

.field public final version:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x1

    .line 2046832
    new-instance v0, LX/1sv;

    const-string v1, "StoredProcedureResponse"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dph;->b:LX/1sv;

    .line 2046833
    new-instance v0, LX/1sw;

    const-string v1, "version"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dph;->c:LX/1sw;

    .line 2046834
    new-instance v0, LX/1sw;

    const-string v1, "result"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dph;->d:LX/1sw;

    .line 2046835
    new-instance v0, LX/1sw;

    const-string v1, "nonce"

    const/16 v2, 0xb

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dph;->e:LX/1sw;

    .line 2046836
    new-instance v0, LX/1sw;

    const-string v1, "body"

    const/16 v2, 0xc

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dph;->f:LX/1sw;

    .line 2046837
    new-instance v0, LX/1sw;

    const-string v1, "date_micros"

    const/16 v2, 0xa

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dph;->g:LX/1sw;

    .line 2046838
    sput-boolean v4, LX/Dph;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;[BLX/Dpi;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2046825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2046826
    iput-object p1, p0, LX/Dph;->version:Ljava/lang/Integer;

    .line 2046827
    iput-object p2, p0, LX/Dph;->result:Ljava/lang/Integer;

    .line 2046828
    iput-object p3, p0, LX/Dph;->nonce:[B

    .line 2046829
    iput-object p4, p0, LX/Dph;->body:LX/Dpi;

    .line 2046830
    iput-object p5, p0, LX/Dph;->date_micros:Ljava/lang/Long;

    .line 2046831
    return-void
.end method

.method public static a(LX/Dph;)V
    .locals 3

    .prologue
    .line 2046697
    iget-object v0, p0, LX/Dph;->result:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/Dpk;->a:LX/1sn;

    iget-object v1, p0, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2046698
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'result\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2046699
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2046767
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2046768
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v2, v0

    .line 2046769
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    move-object v1, v0

    .line 2046770
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "StoredProcedureResponse"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046771
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046772
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046773
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046774
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046775
    const-string v0, "version"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046776
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046777
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046778
    iget-object v0, p0, LX/Dph;->version:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 2046779
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046780
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046781
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046782
    const-string v0, "result"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046783
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046784
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046785
    iget-object v0, p0, LX/Dph;->result:Ljava/lang/Integer;

    if-nez v0, :cond_5

    .line 2046786
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046787
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046788
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046789
    const-string v0, "nonce"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046790
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046791
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046792
    iget-object v0, p0, LX/Dph;->nonce:[B

    if-nez v0, :cond_7

    .line 2046793
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046794
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046795
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046796
    const-string v0, "body"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046797
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046798
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046799
    iget-object v0, p0, LX/Dph;->body:LX/Dpi;

    if-nez v0, :cond_8

    .line 2046800
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046801
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046802
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046803
    const-string v0, "date_micros"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046804
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046805
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046806
    iget-object v0, p0, LX/Dph;->date_micros:Ljava/lang/Long;

    if-nez v0, :cond_9

    .line 2046807
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046808
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046809
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046810
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2046811
    :cond_1
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2046812
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2046813
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 2046814
    :cond_4
    iget-object v0, p0, LX/Dph;->version:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2046815
    :cond_5
    sget-object v0, LX/Dpk;->b:Ljava/util/Map;

    iget-object v5, p0, LX/Dph;->result:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2046816
    if-eqz v0, :cond_6

    .line 2046817
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046818
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046819
    :cond_6
    iget-object v5, p0, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2046820
    if-eqz v0, :cond_0

    .line 2046821
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2046822
    :cond_7
    iget-object v0, p0, LX/Dph;->nonce:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2046823
    :cond_8
    iget-object v0, p0, LX/Dph;->body:LX/Dpi;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2046824
    :cond_9
    iget-object v0, p0, LX/Dph;->date_micros:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2046747
    invoke-static {p0}, LX/Dph;->a(LX/Dph;)V

    .line 2046748
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2046749
    iget-object v0, p0, LX/Dph;->version:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2046750
    sget-object v0, LX/Dph;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046751
    iget-object v0, p0, LX/Dph;->version:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2046752
    :cond_0
    iget-object v0, p0, LX/Dph;->result:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2046753
    sget-object v0, LX/Dph;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046754
    iget-object v0, p0, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2046755
    :cond_1
    iget-object v0, p0, LX/Dph;->nonce:[B

    if-eqz v0, :cond_2

    .line 2046756
    sget-object v0, LX/Dph;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046757
    iget-object v0, p0, LX/Dph;->nonce:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2046758
    :cond_2
    iget-object v0, p0, LX/Dph;->body:LX/Dpi;

    if-eqz v0, :cond_3

    .line 2046759
    sget-object v0, LX/Dph;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046760
    iget-object v0, p0, LX/Dph;->body:LX/Dpi;

    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    .line 2046761
    :cond_3
    iget-object v0, p0, LX/Dph;->date_micros:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2046762
    sget-object v0, LX/Dph;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046763
    iget-object v0, p0, LX/Dph;->date_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2046764
    :cond_4
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2046765
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2046766
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2046704
    if-nez p1, :cond_1

    .line 2046705
    :cond_0
    :goto_0
    return v0

    .line 2046706
    :cond_1
    instance-of v1, p1, LX/Dph;

    if-eqz v1, :cond_0

    .line 2046707
    check-cast p1, LX/Dph;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2046708
    if-nez p1, :cond_3

    .line 2046709
    :cond_2
    :goto_1
    move v0, v2

    .line 2046710
    goto :goto_0

    .line 2046711
    :cond_3
    iget-object v0, p0, LX/Dph;->version:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2046712
    :goto_2
    iget-object v3, p1, LX/Dph;->version:Ljava/lang/Integer;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2046713
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2046714
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046715
    iget-object v0, p0, LX/Dph;->version:Ljava/lang/Integer;

    iget-object v3, p1, LX/Dph;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046716
    :cond_5
    iget-object v0, p0, LX/Dph;->result:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    move v0, v1

    .line 2046717
    :goto_4
    iget-object v3, p1, LX/Dph;->result:Ljava/lang/Integer;

    if-eqz v3, :cond_11

    move v3, v1

    .line 2046718
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2046719
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046720
    iget-object v0, p0, LX/Dph;->result:Ljava/lang/Integer;

    iget-object v3, p1, LX/Dph;->result:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046721
    :cond_7
    iget-object v0, p0, LX/Dph;->nonce:[B

    if-eqz v0, :cond_12

    move v0, v1

    .line 2046722
    :goto_6
    iget-object v3, p1, LX/Dph;->nonce:[B

    if-eqz v3, :cond_13

    move v3, v1

    .line 2046723
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2046724
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046725
    iget-object v0, p0, LX/Dph;->nonce:[B

    iget-object v3, p1, LX/Dph;->nonce:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046726
    :cond_9
    iget-object v0, p0, LX/Dph;->body:LX/Dpi;

    if-eqz v0, :cond_14

    move v0, v1

    .line 2046727
    :goto_8
    iget-object v3, p1, LX/Dph;->body:LX/Dpi;

    if-eqz v3, :cond_15

    move v3, v1

    .line 2046728
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2046729
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046730
    iget-object v0, p0, LX/Dph;->body:LX/Dpi;

    iget-object v3, p1, LX/Dph;->body:LX/Dpi;

    invoke-virtual {v0, v3}, LX/Dpi;->a(LX/Dpi;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046731
    :cond_b
    iget-object v0, p0, LX/Dph;->date_micros:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 2046732
    :goto_a
    iget-object v3, p1, LX/Dph;->date_micros:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 2046733
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2046734
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046735
    iget-object v0, p0, LX/Dph;->date_micros:Ljava/lang/Long;

    iget-object v3, p1, LX/Dph;->date_micros:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_d
    move v2, v1

    .line 2046736
    goto/16 :goto_1

    :cond_e
    move v0, v2

    .line 2046737
    goto/16 :goto_2

    :cond_f
    move v3, v2

    .line 2046738
    goto/16 :goto_3

    :cond_10
    move v0, v2

    .line 2046739
    goto :goto_4

    :cond_11
    move v3, v2

    .line 2046740
    goto :goto_5

    :cond_12
    move v0, v2

    .line 2046741
    goto :goto_6

    :cond_13
    move v3, v2

    .line 2046742
    goto :goto_7

    :cond_14
    move v0, v2

    .line 2046743
    goto :goto_8

    :cond_15
    move v3, v2

    .line 2046744
    goto :goto_9

    :cond_16
    move v0, v2

    .line 2046745
    goto :goto_a

    :cond_17
    move v3, v2

    .line 2046746
    goto :goto_b
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2046703
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2046700
    sget-boolean v0, LX/Dph;->a:Z

    .line 2046701
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Dph;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2046702
    return-object v0
.end method
