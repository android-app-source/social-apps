.class public final LX/DqQ;
.super LX/DqP;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/notifications/constants/NotificationType;

.field public final g:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 6
    .param p2    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048341
    sget-object v0, LX/3B0;->e:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/DqP;-><init>(J)V

    .line 2048342
    sget-object v0, LX/3B0;->a:LX/0U1;

    invoke-virtual {v0, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 2048343
    sget-object v1, LX/3B0;->b:LX/0U1;

    invoke-virtual {v1, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v1

    .line 2048344
    sget-object v2, LX/3B0;->d:LX/0U1;

    invoke-virtual {v2, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 2048345
    sget-object v3, LX/3B0;->f:LX/0U1;

    invoke-virtual {v3, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 2048346
    sget-object v4, LX/3B0;->g:LX/0U1;

    invoke-virtual {v4, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 2048347
    sget-object v5, LX/3B0;->h:LX/0U1;

    invoke-virtual {v5, p1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v5

    .line 2048348
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, LX/DqQ;->b:I

    .line 2048349
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DqQ;->c:Ljava/lang/String;

    .line 2048350
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DqQ;->d:Ljava/lang/String;

    .line 2048351
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/notifications/constants/NotificationType;->valueOf(Ljava/lang/String;)Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    iput-object v0, p0, LX/DqQ;->f:Lcom/facebook/notifications/constants/NotificationType;

    .line 2048352
    invoke-static {p1}, LX/DqQ;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DqQ;->e:Ljava/lang/String;

    .line 2048353
    if-eqz p2, :cond_0

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2048354
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DqQ;->h:Ljava/lang/String;

    .line 2048355
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DqQ;->i:Ljava/lang/String;

    .line 2048356
    return-void

    .line 2048357
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2048358
    sget-object v0, LX/3B0;->c:LX/0U1;

    invoke-virtual {v0, p0}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
