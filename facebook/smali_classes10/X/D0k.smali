.class public final LX/D0k;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/D0m;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/D0l;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 1955871
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1955872
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "facewebUrl"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "textRes"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "linkRes"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "alertSrcRes"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/D0k;->b:[Ljava/lang/String;

    .line 1955873
    iput v3, p0, LX/D0k;->c:I

    .line 1955874
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/D0k;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/D0k;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/D0k;LX/1De;IILX/D0l;)V
    .locals 1

    .prologue
    .line 1955901
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1955902
    iput-object p4, p0, LX/D0k;->a:LX/D0l;

    .line 1955903
    iget-object v0, p0, LX/D0k;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1955904
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1955897
    invoke-super {p0}, LX/1X5;->a()V

    .line 1955898
    const/4 v0, 0x0

    iput-object v0, p0, LX/D0k;->a:LX/D0l;

    .line 1955899
    sget-object v0, LX/D0m;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1955900
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/D0k;
    .locals 2

    .prologue
    .line 1955894
    iget-object v0, p0, LX/D0k;->a:LX/D0l;

    iput-object p1, v0, LX/D0l;->a:Ljava/lang/String;

    .line 1955895
    iget-object v0, p0, LX/D0k;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1955896
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/D0k;
    .locals 2

    .prologue
    .line 1955905
    iget-object v0, p0, LX/D0k;->a:LX/D0l;

    iput-object p1, v0, LX/D0l;->b:Ljava/lang/String;

    .line 1955906
    iget-object v0, p0, LX/D0k;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1955907
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/D0m;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1955884
    iget-object v1, p0, LX/D0k;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/D0k;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/D0k;->c:I

    if-ge v1, v2, :cond_2

    .line 1955885
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1955886
    :goto_0
    iget v2, p0, LX/D0k;->c:I

    if-ge v0, v2, :cond_1

    .line 1955887
    iget-object v2, p0, LX/D0k;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1955888
    iget-object v2, p0, LX/D0k;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1955889
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1955890
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1955891
    :cond_2
    iget-object v0, p0, LX/D0k;->a:LX/D0l;

    .line 1955892
    invoke-virtual {p0}, LX/D0k;->a()V

    .line 1955893
    return-object v0
.end method

.method public final h(I)LX/D0k;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1955881
    iget-object v0, p0, LX/D0k;->a:LX/D0l;

    iput p1, v0, LX/D0l;->c:I

    .line 1955882
    iget-object v0, p0, LX/D0k;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1955883
    return-object p0
.end method

.method public final i(I)LX/D0k;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1955878
    iget-object v0, p0, LX/D0k;->a:LX/D0l;

    iput p1, v0, LX/D0l;->d:I

    .line 1955879
    iget-object v0, p0, LX/D0k;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1955880
    return-object p0
.end method

.method public final j(I)LX/D0k;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 1955875
    iget-object v0, p0, LX/D0k;->a:LX/D0l;

    iput p1, v0, LX/D0l;->e:I

    .line 1955876
    iget-object v0, p0, LX/D0k;->d:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1955877
    return-object p0
.end method
