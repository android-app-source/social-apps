.class public final enum LX/CrQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CrQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CrQ;

.field public static final enum ANGLE:LX/CrQ;

.field public static final enum FADES_WITH_CONTROLS:LX/CrQ;

.field public static final enum OPACITY:LX/CrQ;

.field public static final enum RECT:LX/CrQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1940936
    new-instance v0, LX/CrQ;

    const-string v1, "RECT"

    invoke-direct {v0, v1, v2}, LX/CrQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrQ;->RECT:LX/CrQ;

    .line 1940937
    new-instance v0, LX/CrQ;

    const-string v1, "OPACITY"

    invoke-direct {v0, v1, v3}, LX/CrQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrQ;->OPACITY:LX/CrQ;

    .line 1940938
    new-instance v0, LX/CrQ;

    const-string v1, "ANGLE"

    invoke-direct {v0, v1, v4}, LX/CrQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrQ;->ANGLE:LX/CrQ;

    .line 1940939
    new-instance v0, LX/CrQ;

    const-string v1, "FADES_WITH_CONTROLS"

    invoke-direct {v0, v1, v5}, LX/CrQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CrQ;->FADES_WITH_CONTROLS:LX/CrQ;

    .line 1940940
    const/4 v0, 0x4

    new-array v0, v0, [LX/CrQ;

    sget-object v1, LX/CrQ;->RECT:LX/CrQ;

    aput-object v1, v0, v2

    sget-object v1, LX/CrQ;->OPACITY:LX/CrQ;

    aput-object v1, v0, v3

    sget-object v1, LX/CrQ;->ANGLE:LX/CrQ;

    aput-object v1, v0, v4

    sget-object v1, LX/CrQ;->FADES_WITH_CONTROLS:LX/CrQ;

    aput-object v1, v0, v5

    sput-object v0, LX/CrQ;->$VALUES:[LX/CrQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1940941
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CrQ;
    .locals 1

    .prologue
    .line 1940942
    const-class v0, LX/CrQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CrQ;

    return-object v0
.end method

.method public static values()[LX/CrQ;
    .locals 1

    .prologue
    .line 1940943
    sget-object v0, LX/CrQ;->$VALUES:[LX/CrQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CrQ;

    return-object v0
.end method
