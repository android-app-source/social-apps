.class public LX/Eni;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Enh;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:LX/End;

.field public final b:LX/Eoc;

.field public final c:Ljava/lang/String;

.field public final d:LX/03V;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/Enq;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Enq;",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/End;LX/Eoc;Ljava/lang/String;Landroid/os/Bundle;LX/03V;Ljava/util/concurrent/ExecutorService;)V
    .locals 3
    .param p1    # LX/End;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Eoc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2167397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167398
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Eni;->f:Ljava/util/HashSet;

    .line 2167399
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Eni;->g:Ljava/util/HashMap;

    .line 2167400
    iput-object p1, p0, LX/Eni;->a:LX/End;

    .line 2167401
    iput-object p2, p0, LX/Eni;->b:LX/Eoc;

    .line 2167402
    iput-object p3, p0, LX/Eni;->c:Ljava/lang/String;

    .line 2167403
    iput-object p5, p0, LX/Eni;->d:LX/03V;

    .line 2167404
    iput-object p6, p0, LX/Eni;->e:Ljava/util/concurrent/ExecutorService;

    .line 2167405
    if-eqz p4, :cond_1

    .line 2167406
    const-string v0, "page_loader_currently_loading_ids_left"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167407
    const-string v0, "page_loader_currently_loading_ids_left"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;

    .line 2167408
    iget-object v1, p0, LX/Eni;->g:Ljava/util/HashMap;

    sget-object v2, LX/Enq;->LEFT:LX/Enq;

    invoke-virtual {v0}, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->a()LX/En3;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167409
    :cond_0
    const-string v0, "page_loader_currently_loading_ids_right"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167410
    const-string v0, "page_loader_currently_loading_ids_right"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;

    .line 2167411
    iget-object v1, p0, LX/Eni;->g:Ljava/util/HashMap;

    sget-object v2, LX/Enq;->RIGHT:LX/Enq;

    invoke-virtual {v0}, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;->a()LX/En3;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167412
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167387
    iget-object v0, p0, LX/Eni;->a:LX/End;

    invoke-interface {v0}, LX/End;->a()LX/0am;

    move-result-object v0

    .line 2167388
    iget-object v1, p0, LX/Eni;->g:Ljava/util/HashMap;

    sget-object v2, LX/Enq;->LEFT:LX/Enq;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2167389
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2167390
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    move-object v2, v0

    .line 2167391
    :goto_0
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v3, "page_loader_currently_loading_ids_left"

    new-instance v4, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;

    iget-object v1, p0, LX/Eni;->g:Ljava/util/HashMap;

    sget-object v5, LX/Enq;->LEFT:LX/Enq;

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/En3;

    invoke-direct {v4, v1}, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;-><init>(LX/En3;)V

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2167392
    :goto_1
    iget-object v0, p0, LX/Eni;->g:Ljava/util/HashMap;

    sget-object v1, LX/Enq;->RIGHT:LX/Enq;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167393
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2167394
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 2167395
    :cond_0
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v3, "page_loader_currently_loading_ids_right"

    new-instance v4, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;

    iget-object v1, p0, LX/Eni;->g:Ljava/util/HashMap;

    sget-object v5, LX/Enq;->RIGHT:LX/Enq;

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/En3;

    invoke-direct {v4, v1}, Lcom/facebook/entitycards/collect/ParcelableOffsetArray;-><init>(LX/En3;)V

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2167396
    :cond_1
    return-object v2

    :cond_2
    move-object v2, v0

    goto :goto_0

    :cond_3
    move-object v2, v0

    goto :goto_1
.end method

.method public final a(LX/1My;LX/0TF;Ljava/lang/String;LX/Enq;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1My;",
            "LX/0TF",
            "<*>;",
            "Ljava/lang/String;",
            "LX/Enq;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167373
    if-lez p5, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2167374
    iget-object v0, p0, LX/Eni;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2167375
    iget-object v0, p0, LX/Eni;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167376
    iget-object v0, p0, LX/Eni;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2167377
    :goto_1
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 2167378
    new-instance v2, LX/Enf;

    invoke-direct {v2, p0, v1, p4}, LX/Enf;-><init>(LX/Eni;Lcom/google/common/util/concurrent/SettableFuture;LX/Enq;)V

    iget-object v3, p0, LX/Eni;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2167379
    return-object v1

    .line 2167380
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2167381
    :cond_1
    iget-object v0, p0, LX/Eni;->a:LX/End;

    invoke-interface {v0, p4, p5}, LX/End;->a(LX/Enq;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;LX/Eny;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;",
            "Lcom/facebook/entitycards/model/EntityCardMutationService;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2167386
    return-void
.end method

.method public final a(LX/Enq;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2167382
    iget-object v1, p0, LX/Eni;->f:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2167383
    :cond_0
    :goto_0
    return v0

    .line 2167384
    :cond_1
    iget-object v1, p0, LX/Eni;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2167385
    iget-object v0, p0, LX/Eni;->a:LX/End;

    invoke-interface {v0, p1}, LX/End;->a(LX/Enq;)Z

    move-result v0

    goto :goto_0
.end method
