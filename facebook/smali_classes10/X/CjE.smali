.class public LX/CjE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/CjE;


# instance fields
.field public final a:LX/0tX;

.field private final b:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929214
    iput-object p1, p0, LX/CjE;->a:LX/0tX;

    .line 1929215
    iput-object p2, p0, LX/CjE;->b:LX/1Ck;

    .line 1929216
    return-void
.end method

.method public static a(Ljava/lang/String;J)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1929217
    new-instance v0, LX/8Ym;

    invoke-direct {v0}, LX/8Ym;-><init>()V

    move-object v0, v0

    .line 1929218
    const-string v1, "feedback_node_id"

    invoke-virtual {v0, v1, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/8Ym;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1929219
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1929220
    move-object v0, v0

    .line 1929221
    return-object v0
.end method

.method public static a(LX/0QB;)LX/CjE;
    .locals 5

    .prologue
    .line 1929222
    sget-object v0, LX/CjE;->c:LX/CjE;

    if-nez v0, :cond_1

    .line 1929223
    const-class v1, LX/CjE;

    monitor-enter v1

    .line 1929224
    :try_start_0
    sget-object v0, LX/CjE;->c:LX/CjE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1929225
    if-eqz v2, :cond_0

    .line 1929226
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1929227
    new-instance p0, LX/CjE;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {p0, v3, v4}, LX/CjE;-><init>(LX/0tX;LX/1Ck;)V

    .line 1929228
    move-object v0, p0

    .line 1929229
    sput-object v0, LX/CjE;->c:LX/CjE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1929230
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1929231
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1929232
    :cond_1
    sget-object v0, LX/CjE;->c:LX/CjE;

    return-object v0

    .line 1929233
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1929234
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/2h1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/2h1",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1929235
    const-wide/32 v0, 0x93a80

    invoke-static {p1, v0, v1}, LX/CjE;->a(Ljava/lang/String;J)LX/0zO;

    move-result-object v0

    .line 1929236
    iget-object v1, p0, LX/CjE;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1929237
    iget-object v1, p0, LX/CjE;->b:LX/1Ck;

    invoke-virtual {v1, p1, v0, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1929238
    return-void
.end method
