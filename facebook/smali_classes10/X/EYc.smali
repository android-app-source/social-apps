.class public final LX/EYc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<FieldDescriptorType::",
        "Lcom/google/protobuf/FieldSet$FieldDescriptorLite",
        "<TFieldDescriptorType;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final d:LX/EYc;


# instance fields
.field public final a:LX/EZ8;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ8",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2137744
    new-instance v0, LX/EYc;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/EYc;-><init>(B)V

    sput-object v0, LX/EYc;->d:LX/EYc;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2137745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137746
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EYc;->c:Z

    .line 2137747
    const/16 v0, 0x10

    invoke-static {v0}, LX/EZ8;->a(I)LX/EZ8;

    move-result-object v0

    iput-object v0, p0, LX/EYc;->a:LX/EZ8;

    .line 2137748
    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2137749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137750
    iput-boolean v0, p0, LX/EYc;->c:Z

    .line 2137751
    invoke-static {v0}, LX/EZ8;->a(I)LX/EZ8;

    move-result-object v0

    iput-object v0, p0, LX/EYc;->a:LX/EZ8;

    .line 2137752
    invoke-virtual {p0}, LX/EYc;->c()V

    .line 2137753
    return-void
.end method

.method private static a(LX/EZV;ILjava/lang/Object;)I
    .locals 2

    .prologue
    .line 2137754
    invoke-static {p1}, LX/EWf;->k(I)I

    move-result v0

    .line 2137755
    sget-object v1, LX/EZV;->GROUP:LX/EZV;

    if-ne p0, v1, :cond_0

    .line 2137756
    mul-int/lit8 v0, v0, 0x2

    .line 2137757
    :cond_0
    invoke-static {p0, p2}, LX/EYc;->b(LX/EZV;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static a(LX/EZV;Z)I
    .locals 1

    .prologue
    .line 2137758
    if-eqz p1, :cond_0

    .line 2137759
    const/4 v0, 0x2

    .line 2137760
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/EZV;->getWireType()I

    move-result v0

    goto :goto_0
.end method

.method public static a()LX/EYc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/protobuf/FieldSet$FieldDescriptorLite",
            "<TT;>;>()",
            "LX/EYc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2137729
    new-instance v0, LX/EYc;

    invoke-direct {v0}, LX/EYc;-><init>()V

    return-object v0
.end method

.method public static a(LX/EWd;LX/EZV;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2137761
    sget-object v0, LX/EYb;->b:[I

    invoke-virtual {p1}, LX/EZV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2137762
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There is no way to get here, but the compiler thinks otherwise."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137763
    :pswitch_0
    invoke-virtual {p0}, LX/EWd;->b()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 2137764
    :goto_0
    return-object v0

    .line 2137765
    :pswitch_1
    invoke-static {p0}, LX/EWd;->v(LX/EWd;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    move v0, v0

    .line 2137766
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 2137767
    :pswitch_2
    invoke-virtual {p0}, LX/EWd;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 2137768
    :pswitch_3
    invoke-virtual {p0}, LX/EWd;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 2137769
    :pswitch_4
    invoke-virtual {p0}, LX/EWd;->f()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2137770
    :pswitch_5
    invoke-virtual {p0}, LX/EWd;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 2137771
    :pswitch_6
    invoke-virtual {p0}, LX/EWd;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2137772
    :pswitch_7
    invoke-virtual {p0}, LX/EWd;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2137773
    :pswitch_8
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v1

    .line 2137774
    iget v0, p0, LX/EWd;->b:I

    iget v2, p0, LX/EWd;->d:I

    sub-int/2addr v0, v2

    if-gt v1, v0, :cond_0

    if-lez v1, :cond_0

    .line 2137775
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, LX/EWd;->a:[B

    iget v3, p0, LX/EWd;->d:I

    const-string p1, "UTF-8"

    invoke-direct {v0, v2, v3, v1, p1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 2137776
    iget v2, p0, LX/EWd;->d:I

    add-int/2addr v1, v2

    iput v1, p0, LX/EWd;->d:I

    .line 2137777
    :goto_1
    move-object v0, v0

    .line 2137778
    goto :goto_0

    .line 2137779
    :pswitch_9
    invoke-virtual {p0}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    goto :goto_0

    .line 2137780
    :pswitch_a
    invoke-virtual {p0}, LX/EWd;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2137781
    :pswitch_b
    invoke-static {p0}, LX/EWd;->v(LX/EWd;)I

    move-result v0

    move v0, v0

    .line 2137782
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2137783
    :pswitch_c
    invoke-static {p0}, LX/EWd;->w(LX/EWd;)J

    move-result-wide v2

    move-wide v0, v2

    .line 2137784
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 2137785
    :pswitch_d
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v0

    .line 2137786
    ushr-int/lit8 v1, v0, 0x1

    and-int/lit8 p0, v0, 0x1

    neg-int p0, p0

    xor-int/2addr v1, p0

    move v0, v1

    .line 2137787
    move v0, v0

    .line 2137788
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 2137789
    :pswitch_e
    invoke-static {p0}, LX/EWd;->u(LX/EWd;)J

    move-result-wide v2

    .line 2137790
    const/4 v4, 0x1

    ushr-long v4, v2, v4

    const-wide/16 v6, 0x1

    and-long/2addr v6, v2

    neg-long v6, v6

    xor-long/2addr v4, v6

    move-wide v2, v4

    .line 2137791
    move-wide v0, v2

    .line 2137792
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 2137793
    :pswitch_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle nested groups."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137794
    :pswitch_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle embedded messages."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137795
    :pswitch_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readPrimitiveField() cannot handle enums."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-static {p0, v1}, LX/EWd;->f(LX/EWd;I)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private static a(LX/EWf;LX/EZV;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2137796
    sget-object v0, LX/EZV;->GROUP:LX/EZV;

    if-ne p1, v0, :cond_0

    .line 2137797
    check-cast p3, LX/EWW;

    invoke-virtual {p0, p2, p3}, LX/EWf;->a(ILX/EWW;)V

    .line 2137798
    :goto_0
    return-void

    .line 2137799
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/EYc;->a(LX/EZV;Z)I

    move-result v0

    invoke-virtual {p0, p2, v0}, LX/EWf;->i(II)V

    .line 2137800
    invoke-static {p0, p1, p3}, LX/EYc;->a(LX/EWf;LX/EZV;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(LX/EWf;LX/EZV;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2137801
    sget-object v0, LX/EYb;->b:[I

    invoke-virtual {p1}, LX/EZV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2137802
    :goto_0
    return-void

    .line 2137803
    :pswitch_0
    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/EWf;->a(D)V

    goto :goto_0

    .line 2137804
    :pswitch_1
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2137805
    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    invoke-static {p0, v1}, LX/EWf;->o(LX/EWf;I)V

    .line 2137806
    goto :goto_0

    .line 2137807
    :pswitch_2
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2137808
    invoke-static {p0, v0, v1}, LX/EWf;->i(LX/EWf;J)V

    .line 2137809
    goto :goto_0

    .line 2137810
    :pswitch_3
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2137811
    invoke-static {p0, v0, v1}, LX/EWf;->i(LX/EWf;J)V

    .line 2137812
    goto :goto_0

    .line 2137813
    :pswitch_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LX/EWf;->a(I)V

    goto :goto_0

    .line 2137814
    :pswitch_5
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2137815
    invoke-static {p0, v0, v1}, LX/EWf;->k(LX/EWf;J)V

    .line 2137816
    goto :goto_0

    .line 2137817
    :pswitch_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2137818
    invoke-static {p0, v0}, LX/EWf;->o(LX/EWf;I)V

    .line 2137819
    goto :goto_0

    .line 2137820
    :pswitch_7
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, LX/EWf;->a(Z)V

    goto :goto_0

    .line 2137821
    :pswitch_8
    check-cast p2, Ljava/lang/String;

    .line 2137822
    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 2137823
    array-length v1, v0

    invoke-virtual {p0, v1}, LX/EWf;->l(I)V

    .line 2137824
    const/4 v1, 0x0

    array-length v2, v0

    .line 2137825
    iget v3, p0, LX/EWf;->b:I

    iget v4, p0, LX/EWf;->c:I

    sub-int/2addr v3, v4

    if-lt v3, v2, :cond_0

    .line 2137826
    iget-object v3, p0, LX/EWf;->a:[B

    iget v4, p0, LX/EWf;->c:I

    invoke-static {v0, v1, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2137827
    iget v3, p0, LX/EWf;->c:I

    add-int/2addr v3, v2

    iput v3, p0, LX/EWf;->c:I

    .line 2137828
    :goto_1
    goto :goto_0

    .line 2137829
    :pswitch_9
    check-cast p2, LX/EWW;

    .line 2137830
    invoke-interface {p2, p0}, LX/EWW;->a(LX/EWf;)V

    .line 2137831
    goto :goto_0

    .line 2137832
    :pswitch_a
    check-cast p2, LX/EWW;

    invoke-virtual {p0, p2}, LX/EWf;->b(LX/EWW;)V

    goto/16 :goto_0

    .line 2137833
    :pswitch_b
    check-cast p2, LX/EWc;

    invoke-virtual {p0, p2}, LX/EWf;->a(LX/EWc;)V

    goto/16 :goto_0

    .line 2137834
    :pswitch_c
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2137835
    invoke-virtual {p0, v0}, LX/EWf;->l(I)V

    .line 2137836
    goto/16 :goto_0

    .line 2137837
    :pswitch_d
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2137838
    invoke-static {p0, v0}, LX/EWf;->o(LX/EWf;I)V

    .line 2137839
    goto/16 :goto_0

    .line 2137840
    :pswitch_e
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2137841
    invoke-static {p0, v0, v1}, LX/EWf;->k(LX/EWf;J)V

    .line 2137842
    goto/16 :goto_0

    .line 2137843
    :pswitch_f
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2137844
    invoke-static {v0}, LX/EWf;->p(I)I

    move-result v1

    invoke-virtual {p0, v1}, LX/EWf;->l(I)V

    .line 2137845
    goto/16 :goto_0

    .line 2137846
    :pswitch_10
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2137847
    invoke-static {v0, v1}, LX/EWf;->l(J)J

    move-result-wide v2

    invoke-static {p0, v2, v3}, LX/EWf;->i(LX/EWf;J)V

    .line 2137848
    goto/16 :goto_0

    .line 2137849
    :pswitch_11
    check-cast p2, LX/EXG;

    invoke-interface {p2}, LX/EXG;->getNumber()I

    move-result v0

    .line 2137850
    invoke-virtual {p0, v0}, LX/EWf;->a(I)V

    .line 2137851
    goto/16 :goto_0

    .line 2137852
    :cond_0
    iget v3, p0, LX/EWf;->b:I

    iget v4, p0, LX/EWf;->c:I

    sub-int/2addr v3, v4

    .line 2137853
    iget-object v4, p0, LX/EWf;->a:[B

    iget p1, p0, LX/EWf;->c:I

    invoke-static {v0, v1, v4, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2137854
    add-int v4, v1, v3

    .line 2137855
    sub-int v3, v2, v3

    .line 2137856
    iget p1, p0, LX/EWf;->b:I

    iput p1, p0, LX/EWf;->c:I

    .line 2137857
    invoke-static {p0}, LX/EWf;->i(LX/EWf;)V

    .line 2137858
    iget p1, p0, LX/EWf;->b:I

    if-gt v3, p1, :cond_1

    .line 2137859
    iget-object p1, p0, LX/EWf;->a:[B

    const/4 p2, 0x0

    invoke-static {v0, v4, p1, p2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2137860
    iput v3, p0, LX/EWf;->c:I

    goto/16 :goto_1

    .line 2137861
    :cond_1
    iget-object p1, p0, LX/EWf;->d:Ljava/io/OutputStream;

    invoke-virtual {p1, v0, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_9
        :pswitch_a
        :pswitch_11
    .end packed-switch
.end method

.method public static a(LX/EYP;Ljava/lang/Object;LX/EWf;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/FieldSet$FieldDescriptorLite",
            "<*>;",
            "Ljava/lang/Object;",
            "LX/EWf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2137862
    invoke-virtual {p0}, LX/EYP;->j()LX/EZV;

    move-result-object v1

    .line 2137863
    invoke-virtual {p0}, LX/EYP;->e()I

    move-result v0

    .line 2137864
    invoke-virtual {p0}, LX/EYP;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2137865
    check-cast p1, Ljava/util/List;

    .line 2137866
    invoke-virtual {p0}, LX/EYP;->n()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2137867
    const/4 v2, 0x2

    invoke-virtual {p2, v0, v2}, LX/EWf;->i(II)V

    .line 2137868
    const/4 v0, 0x0

    .line 2137869
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 2137870
    invoke-static {v1, v3}, LX/EYc;->b(LX/EZV;Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    .line 2137871
    goto :goto_0

    .line 2137872
    :cond_0
    invoke-virtual {p2, v0}, LX/EWf;->l(I)V

    .line 2137873
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2137874
    invoke-static {p2, v1, v2}, LX/EYc;->a(LX/EWf;LX/EZV;Ljava/lang/Object;)V

    goto :goto_1

    .line 2137875
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 2137876
    invoke-static {p2, v1, v0, v3}, LX/EYc;->a(LX/EWf;LX/EZV;ILjava/lang/Object;)V

    goto :goto_2

    .line 2137877
    :cond_2
    instance-of v2, p1, LX/EYu;

    if-eqz v2, :cond_4

    .line 2137878
    check-cast p1, LX/EYu;

    invoke-virtual {p1}, LX/EYu;->a()LX/EWW;

    move-result-object v2

    invoke-static {p2, v1, v0, v2}, LX/EYc;->a(LX/EWf;LX/EZV;ILjava/lang/Object;)V

    .line 2137879
    :cond_3
    :goto_3
    return-void

    .line 2137880
    :cond_4
    invoke-static {p2, v1, v0, p1}, LX/EYc;->a(LX/EWf;LX/EZV;ILjava/lang/Object;)V

    goto :goto_3
.end method

.method private static a(LX/EZV;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2137881
    if-nez p1, :cond_0

    .line 2137882
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2137883
    :cond_0
    sget-object v1, LX/EYb;->a:[I

    invoke-virtual {p0}, LX/EZV;->getJavaType()LX/EZa;

    move-result-object v2

    invoke-virtual {v2}, LX/EZa;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2137884
    :cond_1
    :goto_0
    if-nez v0, :cond_3

    .line 2137885
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137886
    :pswitch_0
    instance-of v0, p1, Ljava/lang/Integer;

    goto :goto_0

    .line 2137887
    :pswitch_1
    instance-of v0, p1, Ljava/lang/Long;

    goto :goto_0

    .line 2137888
    :pswitch_2
    instance-of v0, p1, Ljava/lang/Float;

    goto :goto_0

    .line 2137889
    :pswitch_3
    instance-of v0, p1, Ljava/lang/Double;

    goto :goto_0

    .line 2137890
    :pswitch_4
    instance-of v0, p1, Ljava/lang/Boolean;

    goto :goto_0

    .line 2137891
    :pswitch_5
    instance-of v0, p1, Ljava/lang/String;

    goto :goto_0

    .line 2137892
    :pswitch_6
    instance-of v0, p1, LX/EWc;

    goto :goto_0

    .line 2137893
    :pswitch_7
    instance-of v0, p1, LX/EXG;

    goto :goto_0

    .line 2137894
    :pswitch_8
    instance-of v1, p1, LX/EWW;

    if-nez v1, :cond_2

    instance-of v1, p1, LX/EYu;

    if-eqz v1, :cond_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 2137895
    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Ljava/util/Map$Entry;LX/EWf;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;",
            "LX/EWf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2137896
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2137897
    invoke-virtual {v0}, LX/EYP;->g()LX/EZa;

    move-result-object v1

    sget-object v2, LX/EZa;->MESSAGE:LX/EZa;

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/EYP;->n()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2137898
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    invoke-virtual {v0}, LX/EYP;->e()I

    move-result v1

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(ILX/EWW;)V

    .line 2137899
    :goto_0
    return-void

    .line 2137900
    :cond_0
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1, p1}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;LX/EWf;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;Ljava/util/Map$Entry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map$Entry",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2137901
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2137902
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 2137903
    instance-of v2, v1, LX/EYu;

    if-eqz v2, :cond_0

    .line 2137904
    check-cast v1, LX/EYu;

    invoke-virtual {v1}, LX/EYu;->a()LX/EWW;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2137905
    :goto_0
    return-void

    .line 2137906
    :cond_0
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(Ljava/util/Map$Entry;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2137730
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2137731
    invoke-virtual {v0}, LX/EYP;->g()LX/EZa;

    move-result-object v3

    sget-object v4, LX/EZa;->MESSAGE:LX/EZa;

    if-ne v3, v4, :cond_4

    .line 2137732
    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2137733
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    .line 2137734
    invoke-interface {v0}, LX/EWQ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2137735
    :goto_0
    return v0

    .line 2137736
    :cond_1
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 2137737
    instance-of v3, v0, LX/EWW;

    if-eqz v3, :cond_2

    .line 2137738
    check-cast v0, LX/EWW;

    invoke-interface {v0}, LX/EWQ;->a()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 2137739
    goto :goto_0

    .line 2137740
    :cond_2
    instance-of v0, v0, LX/EYu;

    if-eqz v0, :cond_3

    move v0, v2

    .line 2137741
    goto :goto_0

    .line 2137742
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    .line 2137743
    goto :goto_0
.end method

.method private static b(LX/EZV;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2137907
    sget-object v0, LX/EYb;->b:[I

    invoke-virtual {p0}, LX/EZV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2137908
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "There is no way to get here, but the compiler thinks otherwise."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137909
    :pswitch_0
    const/16 v0, 0x8

    move v0, v0

    .line 2137910
    :goto_0
    return v0

    .line 2137911
    :pswitch_1
    const/4 v0, 0x4

    move v0, v0

    .line 2137912
    goto :goto_0

    .line 2137913
    :pswitch_2
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/EWf;->j(J)I

    move-result v0

    goto :goto_0

    .line 2137914
    :pswitch_3
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/EWf;->j(J)I

    move-result v0

    goto :goto_0

    .line 2137915
    :pswitch_4
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/EWf;->g(I)I

    move-result v0

    goto :goto_0

    .line 2137916
    :pswitch_5
    const/16 v0, 0x8

    move v0, v0

    .line 2137917
    goto :goto_0

    .line 2137918
    :pswitch_6
    const/4 v0, 0x4

    move v0, v0

    .line 2137919
    goto :goto_0

    .line 2137920
    :pswitch_7
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 2137921
    const/4 v0, 0x1

    move v0, v0

    .line 2137922
    goto :goto_0

    .line 2137923
    :pswitch_8
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, LX/EWf;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 2137924
    :pswitch_9
    check-cast p1, LX/EWW;

    invoke-static {p1}, LX/EWf;->c(LX/EWW;)I

    move-result v0

    goto :goto_0

    .line 2137925
    :pswitch_a
    check-cast p1, LX/EWc;

    invoke-static {p1}, LX/EWf;->b(LX/EWc;)I

    move-result v0

    goto :goto_0

    .line 2137926
    :pswitch_b
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/EWf;->m(I)I

    move-result v0

    goto :goto_0

    .line 2137927
    :pswitch_c
    const/4 v0, 0x4

    move v0, v0

    .line 2137928
    goto :goto_0

    .line 2137929
    :pswitch_d
    const/16 v0, 0x8

    move v0, v0

    .line 2137930
    goto :goto_0

    .line 2137931
    :pswitch_e
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2137932
    invoke-static {v0}, LX/EWf;->p(I)I

    move-result v1

    invoke-static {v1}, LX/EWf;->m(I)I

    move-result v1

    move v0, v1

    .line 2137933
    goto :goto_0

    .line 2137934
    :pswitch_f
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2137935
    invoke-static {v0, v1}, LX/EWf;->l(J)J

    move-result-wide v2

    invoke-static {v2, v3}, LX/EWf;->j(J)I

    move-result v2

    move v0, v2

    .line 2137936
    goto :goto_0

    .line 2137937
    :pswitch_10
    instance-of v0, p1, LX/EYu;

    if-eqz v0, :cond_0

    .line 2137938
    check-cast p1, LX/EYu;

    invoke-static {p1}, LX/EWf;->a(LX/EYu;)I

    move-result v0

    goto/16 :goto_0

    .line 2137939
    :cond_0
    check-cast p1, LX/EWW;

    invoke-static {p1}, LX/EWf;->d(LX/EWW;)I

    move-result v0

    goto/16 :goto_0

    .line 2137940
    :pswitch_11
    check-cast p1, LX/EXG;

    invoke-interface {p1}, LX/EXG;->getNumber()I

    move-result v0

    invoke-static {v0}, LX/EWf;->g(I)I

    move-result v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_9
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private b(Ljava/util/Map$Entry;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2137610
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2137611
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 2137612
    instance-of v2, v1, LX/EYu;

    if-eqz v2, :cond_0

    .line 2137613
    check-cast v1, LX/EYu;

    invoke-virtual {v1}, LX/EYu;->a()LX/EWW;

    move-result-object v1

    .line 2137614
    :cond_0
    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2137615
    invoke-virtual {p0, v0}, LX/EYc;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v2

    .line 2137616
    if-nez v2, :cond_1

    .line 2137617
    iget-object v2, p0, LX/EYc;->a:LX/EZ8;

    new-instance v3, Ljava/util/ArrayList;

    check-cast v1, Ljava/util/List;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v0, v3}, LX/EZ8;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2137618
    :goto_0
    return-void

    :cond_1
    move-object v0, v2

    .line 2137619
    check-cast v0, Ljava/util/List;

    check-cast v1, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 2137620
    :cond_2
    invoke-virtual {v0}, LX/EYP;->g()LX/EZa;

    move-result-object v2

    sget-object v3, LX/EZa;->MESSAGE:LX/EZa;

    if-ne v2, v3, :cond_4

    .line 2137621
    invoke-virtual {p0, v0}, LX/EYc;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v2

    .line 2137622
    if-nez v2, :cond_3

    .line 2137623
    iget-object v2, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v2, v0, v1}, LX/EZ8;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2137624
    :cond_3
    iget-object v3, p0, LX/EYc;->a:LX/EZ8;

    check-cast v2, LX/EWW;

    invoke-interface {v2}, LX/EWW;->u()LX/EWR;

    move-result-object v2

    check-cast v1, LX/EWW;

    .line 2137625
    check-cast v2, LX/EWU;

    check-cast v1, LX/EWY;

    invoke-interface {v2, v1}, LX/EWU;->c(LX/EWY;)LX/EWU;

    move-result-object p0

    move-object v1, p0

    .line 2137626
    invoke-interface {v1}, LX/EWR;->k()LX/EWW;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/EZ8;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2137627
    :cond_4
    iget-object v2, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v2, v0, v1}, LX/EZ8;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static c(LX/EYP;Ljava/lang/Object;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/FieldSet$FieldDescriptorLite",
            "<*>;",
            "Ljava/lang/Object;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2137628
    invoke-virtual {p0}, LX/EYP;->j()LX/EZV;

    move-result-object v1

    .line 2137629
    invoke-virtual {p0}, LX/EYP;->e()I

    move-result v2

    .line 2137630
    invoke-virtual {p0}, LX/EYP;->m()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2137631
    invoke-virtual {p0}, LX/EYP;->n()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2137632
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 2137633
    invoke-static {v1, v4}, LX/EYc;->b(LX/EZV;Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2137634
    goto :goto_0

    .line 2137635
    :cond_0
    invoke-static {v2}, LX/EWf;->k(I)I

    move-result v1

    add-int/2addr v1, v0

    invoke-static {v0}, LX/EWf;->m(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 2137636
    :cond_1
    :goto_1
    return v0

    .line 2137637
    :cond_2
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 2137638
    invoke-static {v1, v2, v4}, LX/EYc;->a(LX/EZV;ILjava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2137639
    goto :goto_2

    .line 2137640
    :cond_3
    invoke-static {v1, v2, p1}, LX/EYc;->a(LX/EZV;ILjava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method

.method public static c(Ljava/util/Map$Entry;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2137641
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2137642
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 2137643
    invoke-virtual {v0}, LX/EYP;->g()LX/EZa;

    move-result-object v2

    sget-object v3, LX/EZa;->MESSAGE:LX/EZa;

    if-ne v2, v3, :cond_1

    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, LX/EYP;->n()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2137644
    instance-of v0, v1, LX/EYu;

    if-eqz v0, :cond_0

    .line 2137645
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    invoke-virtual {v0}, LX/EYP;->e()I

    move-result v2

    move-object v0, v1

    check-cast v0, LX/EYu;

    .line 2137646
    const/4 v1, 0x1

    invoke-static {v1}, LX/EWf;->k(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    invoke-static {v3, v2}, LX/EWf;->g(II)I

    move-result v3

    add-int/2addr v1, v3

    const/4 v3, 0x3

    .line 2137647
    invoke-static {v3}, LX/EWf;->k(I)I

    move-result p0

    invoke-static {v0}, LX/EWf;->a(LX/EYu;)I

    move-result v2

    add-int/2addr p0, v2

    move v3, p0

    .line 2137648
    add-int/2addr v1, v3

    move v0, v1

    .line 2137649
    :goto_0
    return v0

    .line 2137650
    :cond_0
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    invoke-virtual {v0}, LX/EYP;->e()I

    move-result v0

    check-cast v1, LX/EWW;

    invoke-static {v0, v1}, LX/EWf;->f(ILX/EWW;)I

    move-result v0

    goto :goto_0

    .line 2137651
    :cond_1
    invoke-static {v0, v1}, LX/EYc;->c(LX/EYP;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/EYP;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2137652
    invoke-virtual {p1}, LX/EYP;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2137653
    instance-of v0, p2, Ljava/util/List;

    if-nez v0, :cond_0

    .line 2137654
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong object type used with protocol message reflection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137655
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2137656
    check-cast p2, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2137657
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2137658
    invoke-virtual {p1}, LX/EYP;->j()LX/EZV;

    move-result-object v3

    invoke-static {v3, v2}, LX/EYc;->a(LX/EZV;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move-object p2, v0

    .line 2137659
    :goto_1
    instance-of v0, p2, LX/EYu;

    if-eqz v0, :cond_2

    .line 2137660
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EYc;->c:Z

    .line 2137661
    :cond_2
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0, p1, p2}, LX/EZ8;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2137662
    return-void

    .line 2137663
    :cond_3
    invoke-virtual {p1}, LX/EYP;->j()LX/EZV;

    move-result-object v0

    invoke-static {v0, p2}, LX/EYc;->a(LX/EZV;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(LX/EYc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYc",
            "<TFieldDescriptorType;>;)V"
        }
    .end annotation

    .prologue
    .line 2137664
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v1}, LX/EZ8;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2137665
    iget-object v1, p1, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v1, v0}, LX/EZ8;->b(I)Ljava/util/Map$Entry;

    move-result-object v1

    invoke-direct {p0, v1}, LX/EYc;->b(Ljava/util/Map$Entry;)V

    .line 2137666
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2137667
    :cond_0
    iget-object v0, p1, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2137668
    invoke-direct {p0, v0}, LX/EYc;->b(Ljava/util/Map$Entry;)V

    goto :goto_1

    .line 2137669
    :cond_1
    return-void
.end method

.method public final a(LX/EYP;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFieldDescriptorType;)Z"
        }
    .end annotation

    .prologue
    .line 2137670
    invoke-virtual {p1}, LX/EYP;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2137671
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "hasField() can only be called on non-repeated fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137672
    :cond_0
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0, p1}, LX/EZ8;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/EYP;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFieldDescriptorType;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 2137673
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0, p1}, LX/EZ8;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2137674
    instance-of v1, v0, LX/EYu;

    if-eqz v1, :cond_0

    .line 2137675
    check-cast v0, LX/EYu;

    invoke-virtual {v0}, LX/EYu;->a()LX/EWW;

    move-result-object v0

    .line 2137676
    :cond_0
    return-object v0
.end method

.method public final b(LX/EYP;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2137677
    invoke-virtual {p1}, LX/EYP;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2137678
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addRepeatedField() can only be called on repeated fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137679
    :cond_0
    invoke-virtual {p1}, LX/EYP;->j()LX/EZV;

    move-result-object v0

    invoke-static {v0, p2}, LX/EYc;->a(LX/EZV;Ljava/lang/Object;)V

    .line 2137680
    invoke-virtual {p0, p1}, LX/EYc;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    .line 2137681
    if-nez v0, :cond_1

    .line 2137682
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2137683
    iget-object v1, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v1, p1, v0}, LX/EZ8;->a(Ljava/lang/Comparable;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2137684
    :goto_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2137685
    return-void

    .line 2137686
    :cond_1
    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2137606
    iget-boolean v0, p0, LX/EYc;->b:Z

    if-eqz v0, :cond_0

    .line 2137607
    :goto_0
    return-void

    .line 2137608
    :cond_0
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->a()V

    .line 2137609
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EYc;->b:Z

    goto :goto_0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2137687
    invoke-virtual {p0}, LX/EYc;->e()LX/EYc;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EYc",
            "<TFieldDescriptorType;>;"
        }
    .end annotation

    .prologue
    .line 2137688
    invoke-static {}, LX/EYc;->a()LX/EYc;

    move-result-object v2

    .line 2137689
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->c()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2137690
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0, v1}, LX/EZ8;->b(I)Ljava/util/Map$Entry;

    move-result-object v3

    .line 2137691
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2137692
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;)V

    .line 2137693
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2137694
    :cond_0
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2137695
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    .line 2137696
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;)V

    goto :goto_1

    .line 2137697
    :cond_1
    iget-boolean v0, p0, LX/EYc;->c:Z

    iput-boolean v0, v2, LX/EYc;->c:Z

    .line 2137698
    return-object v2
.end method

.method public final f()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TFieldDescriptorType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2137699
    iget-boolean v0, p0, LX/EYc;->c:Z

    if-eqz v0, :cond_3

    .line 2137700
    const/16 v0, 0x10

    invoke-static {v0}, LX/EZ8;->a(I)LX/EZ8;

    move-result-object v1

    .line 2137701
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v2}, LX/EZ8;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2137702
    iget-object v2, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v2, v0}, LX/EZ8;->b(I)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-static {v1, v2}, LX/EYc;->a(Ljava/util/Map;Ljava/util/Map$Entry;)V

    .line 2137703
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2137704
    :cond_0
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2137705
    invoke-static {v1, v0}, LX/EYc;->a(Ljava/util/Map;Ljava/util/Map$Entry;)V

    goto :goto_1

    .line 2137706
    :cond_1
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    .line 2137707
    iget-boolean v2, v0, LX/EZ8;->d:Z

    move v0, v2

    .line 2137708
    if-eqz v0, :cond_2

    .line 2137709
    invoke-virtual {v1}, LX/EZ8;->a()V

    :cond_2
    move-object v0, v1

    .line 2137710
    :goto_2
    return-object v0

    :cond_3
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    .line 2137711
    iget-boolean v1, v0, LX/EZ8;->d:Z

    move v0, v1

    .line 2137712
    if-eqz v0, :cond_4

    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    goto :goto_2

    :cond_4
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_2
.end method

.method public final h()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2137713
    move v0, v1

    :goto_0
    iget-object v2, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v2}, LX/EZ8;->c()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2137714
    iget-object v2, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v2, v0}, LX/EZ8;->b(I)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-static {v2}, LX/EYc;->a(Ljava/util/Map$Entry;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2137715
    :goto_1
    return v1

    .line 2137716
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2137717
    :cond_1
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2137718
    invoke-static {v0}, LX/EYc;->a(Ljava/util/Map$Entry;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    .line 2137719
    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final i()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2137720
    move v1, v0

    move v2, v0

    .line 2137721
    :goto_0
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->c()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2137722
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0, v1}, LX/EZ8;->b(I)Ljava/util/Map$Entry;

    move-result-object v3

    .line 2137723
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v3}, LX/EYc;->c(LX/EYP;Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2137724
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2137725
    :cond_0
    iget-object v0, p0, LX/EYc;->a:LX/EZ8;

    invoke-virtual {v0}, LX/EZ8;->d()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2137726
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, LX/EYc;->c(LX/EYP;Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2137727
    goto :goto_1

    .line 2137728
    :cond_1
    return v2
.end method
