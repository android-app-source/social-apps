.class public final LX/DKe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1987324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;
    .locals 3

    .prologue
    .line 1987325
    iget-object v0, p0, LX/DKe;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DKe;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1987326
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing required parameter for setting as cover photo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1987327
    :cond_1
    new-instance v0, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;

    iget-object v1, p0, LX/DKe;->a:Ljava/lang/String;

    iget-object v2, p0, LX/DKe;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
