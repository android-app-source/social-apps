.class public LX/EbC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Eb9;


# instance fields
.field private final a:I

.field public final b:I

.field public final c:I

.field public final d:[B

.field private final e:[B


# direct methods
.method public constructor <init>(II[BLX/Eas;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 2143095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2143096
    new-array v0, v6, [B

    invoke-static {v4, v4}, LX/Eco;->a(II)B

    move-result v1

    aput-byte v1, v0, v5

    .line 2143097
    invoke-static {}, LX/EbP;->u()LX/EbP;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/EbP;->a(I)LX/EbP;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/EbP;->b(I)LX/EbP;

    move-result-object v1

    invoke-static {p3}, LX/EWc;->a([B)LX/EWc;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EbP;->a(LX/EWc;)LX/EbP;

    move-result-object v1

    invoke-virtual {v1}, LX/EbP;->l()LX/EbQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EWX;->jZ_()[B

    move-result-object v1

    .line 2143098
    new-array v2, v7, [[B

    aput-object v0, v2, v5

    aput-object v1, v2, v6

    invoke-static {v2}, LX/Eco;->a([[B)[B

    move-result-object v2

    invoke-static {p4, v2}, LX/EbC;->a(LX/Eas;[B)[B

    move-result-object v2

    .line 2143099
    new-array v3, v4, [[B

    aput-object v0, v3, v5

    aput-object v1, v3, v6

    aput-object v2, v3, v7

    invoke-static {v3}, LX/Eco;->a([[B)[B

    move-result-object v0

    iput-object v0, p0, LX/EbC;->e:[B

    .line 2143100
    iput v4, p0, LX/EbC;->a:I

    .line 2143101
    iput p1, p0, LX/EbC;->b:I

    .line 2143102
    iput p2, p0, LX/EbC;->c:I

    .line 2143103
    iput-object p3, p0, LX/EbC;->d:[B

    .line 2143104
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2143072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2143073
    const/4 v0, 0x1

    :try_start_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v1, v1, -0x40

    const/16 v2, 0x40

    invoke-static {p1, v0, v1, v2}, LX/Eco;->a([BIII)[[B

    move-result-object v0

    .line 2143074
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    .line 2143075
    const/4 v2, 0x1

    aget-object v0, v0, v2

    .line 2143076
    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v2

    if-ge v2, v3, :cond_0

    .line 2143077
    new-instance v0, LX/Eak;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Legacy message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eak;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2143078
    :catch_0
    move-exception v0

    .line 2143079
    :goto_0
    new-instance v1, LX/Eai;

    invoke-direct {v1, v0}, LX/Eai;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2143080
    :cond_0
    :try_start_1
    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v2

    if-le v2, v3, :cond_1

    .line 2143081
    new-instance v0, LX/Eai;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2143082
    :catch_1
    move-exception v0

    goto :goto_0

    .line 2143083
    :cond_1
    sget-object v2, LX/EbQ;->a:LX/EWZ;

    invoke-virtual {v2, v0}, LX/EWZ;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EbQ;

    move-object v0, v2

    .line 2143084
    invoke-virtual {v0}, LX/EbQ;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, LX/EbQ;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, LX/EbQ;->o()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2143085
    :cond_2
    new-instance v0, LX/Eai;

    const-string v1, "Incomplete message."

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2143086
    :cond_3
    iput-object p1, p0, LX/EbC;->e:[B

    .line 2143087
    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v1

    iput v1, p0, LX/EbC;->a:I

    .line 2143088
    iget v1, v0, LX/EbQ;->id_:I

    move v1, v1

    .line 2143089
    iput v1, p0, LX/EbC;->b:I

    .line 2143090
    iget v1, v0, LX/EbQ;->iteration_:I

    move v1, v1

    .line 2143091
    iput v1, p0, LX/EbC;->c:I

    .line 2143092
    iget-object v1, v0, LX/EbQ;->ciphertext_:LX/EWc;

    move-object v0, v1

    .line 2143093
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    iput-object v0, p0, LX/EbC;->d:[B
    :try_end_1
    .catch LX/EYr; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2143094
    return-void
.end method

.method private static a(LX/Eas;[B)[B
    .locals 2

    .prologue
    .line 2143062
    :try_start_0
    invoke-static {p0, p1}, LX/Ear;->a(LX/Eas;[B)[B
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2143063
    :catch_0
    move-exception v0

    .line 2143064
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method


# virtual methods
.method public final a(LX/Eat;)V
    .locals 3

    .prologue
    .line 2143066
    :try_start_0
    iget-object v0, p0, LX/EbC;->e:[B

    iget-object v1, p0, LX/EbC;->e:[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x40

    const/16 v2, 0x40

    invoke-static {v0, v1, v2}, LX/Eco;->a([BII)[[B

    move-result-object v0

    .line 2143067
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-static {p1, v1, v0}, LX/Ear;->a(LX/Eat;[B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2143068
    new-instance v0, LX/Eai;

    const-string v1, "Invalid signature!"

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    .line 2143069
    :catch_0
    move-exception v0

    .line 2143070
    new-instance v1, LX/Eai;

    invoke-direct {v1, v0}, LX/Eai;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2143071
    :cond_0
    return-void
.end method

.method public final a()[B
    .locals 1

    .prologue
    .line 2143065
    iget-object v0, p0, LX/EbC;->e:[B

    return-object v0
.end method
