.class public final LX/DJ3;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

.field public final synthetic b:Lcom/facebook/groupcommerce/composer/ComposerSellView;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/composer/ComposerSellView;Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)V
    .locals 0

    .prologue
    .line 1984669
    iput-object p1, p0, LX/DJ3;->b:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iput-object p2, p0, LX/DJ3;->a:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1984661
    new-instance v1, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;-><init>()V

    .line 1984662
    iget-object v0, p0, LX/DJ3;->a:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v2, p0, LX/DJ3;->b:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v2}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getShouldPostToMarketplace()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;Z)V

    .line 1984663
    new-instance v0, LX/DJ1;

    invoke-direct {v0, p0}, LX/DJ1;-><init>(LX/DJ3;)V

    .line 1984664
    iput-object v0, v1, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->m:Landroid/view/View$OnClickListener;

    .line 1984665
    new-instance v0, LX/DJ2;

    invoke-direct {v0, p0}, LX/DJ2;-><init>(LX/DJ3;)V

    .line 1984666
    iput-object v0, v1, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->n:Landroid/view/View$OnClickListener;

    .line 1984667
    iget-object v0, p0, LX/DJ3;->b:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "MARKETPLACE_INFO_DIALOG"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1984668
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1984657
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1984658
    iget-object v0, p0, LX/DJ3;->b:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1984659
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1984660
    return-void
.end method
