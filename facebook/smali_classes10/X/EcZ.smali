.class public final LX/EcZ;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EcY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EcZ;",
        ">;",
        "LX/EcY;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field private c:LX/EWc;

.field private d:LX/EWc;

.field private e:LX/EWc;

.field private f:LX/EWc;

.field private g:LX/EWc;

.field private h:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2147225
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2147226
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->c:LX/EWc;

    .line 2147227
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->d:LX/EWc;

    .line 2147228
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->e:LX/EWc;

    .line 2147229
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->f:LX/EWc;

    .line 2147230
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->g:LX/EWc;

    .line 2147231
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->h:LX/EWc;

    .line 2147232
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2147171
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2147172
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->c:LX/EWc;

    .line 2147173
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->d:LX/EWc;

    .line 2147174
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->e:LX/EWc;

    .line 2147175
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->f:LX/EWc;

    .line 2147176
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->g:LX/EWc;

    .line 2147177
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcZ;->h:LX/EWc;

    .line 2147178
    return-void
.end method

.method private a(LX/EWc;)LX/EcZ;
    .locals 1

    .prologue
    .line 2147179
    if-nez p1, :cond_0

    .line 2147180
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147181
    :cond_0
    iget v0, p0, LX/EcZ;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EcZ;->a:I

    .line 2147182
    iput-object p1, p0, LX/EcZ;->c:LX/EWc;

    .line 2147183
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147184
    return-object p0
.end method

.method private b(LX/EWc;)LX/EcZ;
    .locals 1

    .prologue
    .line 2147185
    if-nez p1, :cond_0

    .line 2147186
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147187
    :cond_0
    iget v0, p0, LX/EcZ;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EcZ;->a:I

    .line 2147188
    iput-object p1, p0, LX/EcZ;->d:LX/EWc;

    .line 2147189
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147190
    return-object p0
.end method

.method private c(LX/EWc;)LX/EcZ;
    .locals 1

    .prologue
    .line 2147191
    if-nez p1, :cond_0

    .line 2147192
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147193
    :cond_0
    iget v0, p0, LX/EcZ;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EcZ;->a:I

    .line 2147194
    iput-object p1, p0, LX/EcZ;->e:LX/EWc;

    .line 2147195
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147196
    return-object p0
.end method

.method private d(LX/EWY;)LX/EcZ;
    .locals 1

    .prologue
    .line 2147197
    instance-of v0, p1, LX/Eca;

    if-eqz v0, :cond_0

    .line 2147198
    check-cast p1, LX/Eca;

    invoke-virtual {p0, p1}, LX/EcZ;->a(LX/Eca;)LX/EcZ;

    move-result-object p0

    .line 2147199
    :goto_0
    return-object p0

    .line 2147200
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWc;)LX/EcZ;
    .locals 1

    .prologue
    .line 2147201
    if-nez p1, :cond_0

    .line 2147202
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147203
    :cond_0
    iget v0, p0, LX/EcZ;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/EcZ;->a:I

    .line 2147204
    iput-object p1, p0, LX/EcZ;->f:LX/EWc;

    .line 2147205
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147206
    return-object p0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EcZ;
    .locals 4

    .prologue
    .line 2147207
    const/4 v2, 0x0

    .line 2147208
    :try_start_0
    sget-object v0, LX/Eca;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eca;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2147209
    if-eqz v0, :cond_0

    .line 2147210
    invoke-virtual {p0, v0}, LX/EcZ;->a(LX/Eca;)LX/EcZ;

    .line 2147211
    :cond_0
    return-object p0

    .line 2147212
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2147213
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2147214
    check-cast v0, LX/Eca;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2147215
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2147216
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2147217
    invoke-virtual {p0, v1}, LX/EcZ;->a(LX/Eca;)LX/EcZ;

    :cond_1
    throw v0

    .line 2147218
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private e(LX/EWc;)LX/EcZ;
    .locals 1

    .prologue
    .line 2147219
    if-nez p1, :cond_0

    .line 2147220
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147221
    :cond_0
    iget v0, p0, LX/EcZ;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, LX/EcZ;->a:I

    .line 2147222
    iput-object p1, p0, LX/EcZ;->g:LX/EWc;

    .line 2147223
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147224
    return-object p0
.end method

.method private f(LX/EWc;)LX/EcZ;
    .locals 1

    .prologue
    .line 2147233
    if-nez p1, :cond_0

    .line 2147234
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147235
    :cond_0
    iget v0, p0, LX/EcZ;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, LX/EcZ;->a:I

    .line 2147236
    iput-object p1, p0, LX/EcZ;->h:LX/EWc;

    .line 2147237
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147238
    return-object p0
.end method

.method public static u()LX/EcZ;
    .locals 1

    .prologue
    .line 2147239
    new-instance v0, LX/EcZ;

    invoke-direct {v0}, LX/EcZ;-><init>()V

    return-object v0
.end method

.method private w()LX/EcZ;
    .locals 2

    .prologue
    .line 2147240
    invoke-static {}, LX/EcZ;->u()LX/EcZ;

    move-result-object v0

    invoke-virtual {p0}, LX/EcZ;->l()LX/Eca;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcZ;->a(LX/Eca;)LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/Eca;
    .locals 2

    .prologue
    .line 2147241
    invoke-virtual {p0}, LX/EcZ;->l()LX/Eca;

    move-result-object v0

    .line 2147242
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2147243
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2147244
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2147245
    invoke-direct {p0, p1}, LX/EcZ;->d(LX/EWY;)LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2147135
    invoke-direct {p0, p1, p2}, LX/EcZ;->d(LX/EWd;LX/EYZ;)LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Eca;)LX/EcZ;
    .locals 2

    .prologue
    .line 2147136
    sget-object v0, LX/Eca;->c:LX/Eca;

    move-object v0, v0

    .line 2147137
    if-ne p1, v0, :cond_0

    .line 2147138
    :goto_0
    return-object p0

    .line 2147139
    :cond_0
    const/4 v0, 0x1

    .line 2147140
    iget v1, p1, LX/Eca;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_1
    move v0, v0

    .line 2147141
    if-eqz v0, :cond_1

    .line 2147142
    iget v0, p1, LX/Eca;->sequence_:I

    move v0, v0

    .line 2147143
    iget v1, p0, LX/EcZ;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/EcZ;->a:I

    .line 2147144
    iput v0, p0, LX/EcZ;->b:I

    .line 2147145
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147146
    :cond_1
    iget v0, p1, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2147147
    if-eqz v0, :cond_2

    .line 2147148
    iget-object v0, p1, LX/Eca;->localBaseKey_:LX/EWc;

    move-object v0, v0

    .line 2147149
    invoke-direct {p0, v0}, LX/EcZ;->a(LX/EWc;)LX/EcZ;

    .line 2147150
    :cond_2
    iget v0, p1, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2147151
    if-eqz v0, :cond_3

    .line 2147152
    iget-object v0, p1, LX/Eca;->localBaseKeyPrivate_:LX/EWc;

    move-object v0, v0

    .line 2147153
    invoke-direct {p0, v0}, LX/EcZ;->b(LX/EWc;)LX/EcZ;

    .line 2147154
    :cond_3
    iget v0, p1, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2147155
    if-eqz v0, :cond_4

    .line 2147156
    iget-object v0, p1, LX/Eca;->localRatchetKey_:LX/EWc;

    move-object v0, v0

    .line 2147157
    invoke-direct {p0, v0}, LX/EcZ;->c(LX/EWc;)LX/EcZ;

    .line 2147158
    :cond_4
    iget v0, p1, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 2147159
    if-eqz v0, :cond_5

    .line 2147160
    iget-object v0, p1, LX/Eca;->localRatchetKeyPrivate_:LX/EWc;

    move-object v0, v0

    .line 2147161
    invoke-direct {p0, v0}, LX/EcZ;->d(LX/EWc;)LX/EcZ;

    .line 2147162
    :cond_5
    iget v0, p1, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_d

    const/4 v0, 0x1

    :goto_6
    move v0, v0

    .line 2147163
    if-eqz v0, :cond_6

    .line 2147164
    iget-object v0, p1, LX/Eca;->localIdentityKey_:LX/EWc;

    move-object v0, v0

    .line 2147165
    invoke-direct {p0, v0}, LX/EcZ;->e(LX/EWc;)LX/EcZ;

    .line 2147166
    :cond_6
    iget v0, p1, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_7
    move v0, v0

    .line 2147167
    if-eqz v0, :cond_7

    .line 2147168
    iget-object v0, p1, LX/Eca;->localIdentityKeyPrivate_:LX/EWc;

    move-object v0, v0

    .line 2147169
    invoke-direct {p0, v0}, LX/EcZ;->f(LX/EWc;)LX/EcZ;

    .line 2147170
    :cond_7
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_2

    :cond_a
    const/4 v0, 0x0

    goto :goto_3

    :cond_b
    const/4 v0, 0x0

    goto :goto_4

    :cond_c
    const/4 v0, 0x0

    goto :goto_5

    :cond_d
    const/4 v0, 0x0

    goto :goto_6

    :cond_e
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2147087
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2147088
    invoke-direct {p0, p1, p2}, LX/EcZ;->d(LX/EWd;LX/EYZ;)LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2147089
    invoke-direct {p0}, LX/EcZ;->w()LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2147090
    invoke-direct {p0, p1, p2}, LX/EcZ;->d(LX/EWd;LX/EYZ;)LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2147091
    invoke-direct {p0}, LX/EcZ;->w()LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2147092
    invoke-direct {p0, p1}, LX/EcZ;->d(LX/EWY;)LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2147093
    invoke-direct {p0}, LX/EcZ;->w()LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2147134
    sget-object v0, LX/Eck;->j:LX/EYn;

    const-class v1, LX/Eca;

    const-class v2, LX/EcZ;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2147094
    sget-object v0, LX/Eck;->i:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2147095
    invoke-direct {p0}, LX/EcZ;->w()LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2147096
    invoke-virtual {p0}, LX/EcZ;->l()LX/Eca;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2147097
    invoke-direct {p0}, LX/EcZ;->y()LX/Eca;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2147098
    invoke-virtual {p0}, LX/EcZ;->l()LX/Eca;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2147099
    invoke-direct {p0}, LX/EcZ;->y()LX/Eca;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Eca;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2147100
    new-instance v2, LX/Eca;

    invoke-direct {v2, p0}, LX/Eca;-><init>(LX/EWj;)V

    .line 2147101
    iget v3, p0, LX/EcZ;->a:I

    .line 2147102
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    .line 2147103
    :goto_0
    iget v1, p0, LX/EcZ;->b:I

    .line 2147104
    iput v1, v2, LX/Eca;->sequence_:I

    .line 2147105
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2147106
    or-int/lit8 v0, v0, 0x2

    .line 2147107
    :cond_0
    iget-object v1, p0, LX/EcZ;->c:LX/EWc;

    .line 2147108
    iput-object v1, v2, LX/Eca;->localBaseKey_:LX/EWc;

    .line 2147109
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2147110
    or-int/lit8 v0, v0, 0x4

    .line 2147111
    :cond_1
    iget-object v1, p0, LX/EcZ;->d:LX/EWc;

    .line 2147112
    iput-object v1, v2, LX/Eca;->localBaseKeyPrivate_:LX/EWc;

    .line 2147113
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2147114
    or-int/lit8 v0, v0, 0x8

    .line 2147115
    :cond_2
    iget-object v1, p0, LX/EcZ;->e:LX/EWc;

    .line 2147116
    iput-object v1, v2, LX/Eca;->localRatchetKey_:LX/EWc;

    .line 2147117
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2147118
    or-int/lit8 v0, v0, 0x10

    .line 2147119
    :cond_3
    iget-object v1, p0, LX/EcZ;->f:LX/EWc;

    .line 2147120
    iput-object v1, v2, LX/Eca;->localRatchetKeyPrivate_:LX/EWc;

    .line 2147121
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 2147122
    or-int/lit8 v0, v0, 0x20

    .line 2147123
    :cond_4
    iget-object v1, p0, LX/EcZ;->g:LX/EWc;

    .line 2147124
    iput-object v1, v2, LX/Eca;->localIdentityKey_:LX/EWc;

    .line 2147125
    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    .line 2147126
    or-int/lit8 v0, v0, 0x40

    .line 2147127
    :cond_5
    iget-object v1, p0, LX/EcZ;->h:LX/EWc;

    .line 2147128
    iput-object v1, v2, LX/Eca;->localIdentityKeyPrivate_:LX/EWc;

    .line 2147129
    iput v0, v2, LX/Eca;->bitField0_:I

    .line 2147130
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2147131
    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2147132
    sget-object v0, LX/Eca;->c:LX/Eca;

    move-object v0, v0

    .line 2147133
    return-object v0
.end method
