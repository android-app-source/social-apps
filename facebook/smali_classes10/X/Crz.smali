.class public LX/Crz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

.field private final b:LX/0eF;


# direct methods
.method public constructor <init>(LX/0eF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1941610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941611
    iput-object p1, p0, LX/Crz;->b:LX/0eF;

    .line 1941612
    return-void
.end method

.method public static a(LX/0QB;)LX/Crz;
    .locals 4

    .prologue
    .line 1941616
    const-class v1, LX/Crz;

    monitor-enter v1

    .line 1941617
    :try_start_0
    sget-object v0, LX/Crz;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1941618
    sput-object v2, LX/Crz;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1941619
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941620
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1941621
    new-instance p0, LX/Crz;

    invoke-static {v0}, LX/0eE;->a(LX/0QB;)LX/0eE;

    move-result-object v3

    check-cast v3, LX/0eF;

    invoke-direct {p0, v3}, LX/Crz;-><init>(LX/0eF;)V

    .line 1941622
    move-object v0, p0

    .line 1941623
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1941624
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Crz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1941625
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1941626
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c()Z
    .locals 2

    .prologue
    .line 1941627
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1941615
    iget-object v0, p0, LX/Crz;->a:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;->RIGHT_TO_LEFT:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1941613
    iget-object v1, p0, LX/Crz;->b:LX/0eF;

    invoke-interface {v1}, LX/0eF;->a()Ljava/util/Locale;

    move-result-object v1

    .line 1941614
    invoke-static {v1}, LX/3rK;->a(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
