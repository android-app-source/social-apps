.class public LX/Ey7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/Ey6;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2185792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2185793
    sget-object v0, LX/Ey6;->LOADING_STATE_IDLE:LX/Ey6;

    iput-object v0, p0, LX/Ey7;->a:LX/Ey6;

    .line 2185794
    return-void
.end method

.method public static a(LX/0QB;)LX/Ey7;
    .locals 3

    .prologue
    .line 2185781
    const-class v1, LX/Ey7;

    monitor-enter v1

    .line 2185782
    :try_start_0
    sget-object v0, LX/Ey7;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2185783
    sput-object v2, LX/Ey7;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2185784
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2185785
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2185786
    new-instance v0, LX/Ey7;

    invoke-direct {v0}, LX/Ey7;-><init>()V

    .line 2185787
    move-object v0, v0

    .line 2185788
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2185789
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ey7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2185790
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2185791
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
