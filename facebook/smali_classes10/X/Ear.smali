.class public LX/Ear;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2142594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([B)LX/Eas;
    .locals 1

    .prologue
    .line 2142595
    new-instance v0, LX/Eas;

    invoke-direct {v0, p0}, LX/Eas;-><init>([B)V

    return-object v0
.end method

.method public static a([BI)LX/Eat;
    .locals 4

    .prologue
    const/16 v3, 0x20

    .line 2142596
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    .line 2142597
    packed-switch v0, :pswitch_data_0

    .line 2142598
    new-instance v1, LX/Eag;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad key type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Eag;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2142599
    :pswitch_0
    new-array v0, v3, [B

    .line 2142600
    add-int/lit8 v1, p1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2142601
    new-instance v1, LX/Eat;

    invoke-direct {v1, v0}, LX/Eat;-><init>([B)V

    return-object v1

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public static a()LX/Eau;
    .locals 5

    .prologue
    .line 2142602
    const-string v0, "best"

    invoke-static {v0}, LX/EZe;->a(Ljava/lang/String;)LX/EZe;

    move-result-object v0

    .line 2142603
    iget-object v1, v0, LX/EZe;->a:LX/EZc;

    invoke-interface {v1}, LX/EZc;->a()[B

    move-result-object v1

    .line 2142604
    iget-object v2, v0, LX/EZe;->a:LX/EZc;

    invoke-interface {v2, v1}, LX/EZc;->generatePublicKey([B)[B

    move-result-object v2

    .line 2142605
    new-instance v3, LX/EZf;

    invoke-direct {v3, v2, v1}, LX/EZf;-><init>([B[B)V

    move-object v0, v3

    .line 2142606
    new-instance v1, LX/Eau;

    new-instance v2, LX/Eat;

    .line 2142607
    iget-object v3, v0, LX/EZf;->a:[B

    move-object v3, v3

    .line 2142608
    invoke-direct {v2, v3}, LX/Eat;-><init>([B)V

    new-instance v3, LX/Eas;

    .line 2142609
    iget-object v4, v0, LX/EZf;->b:[B

    move-object v0, v4

    .line 2142610
    invoke-direct {v3, v0}, LX/Eas;-><init>([B)V

    invoke-direct {v1, v2, v3}, LX/Eau;-><init>(LX/Eat;LX/Eas;)V

    return-object v1
.end method

.method public static a(LX/Eat;[B[B)Z
    .locals 3

    .prologue
    .line 2142611
    const/4 v0, 0x5

    move v0, v0

    .line 2142612
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 2142613
    const-string v0, "best"

    invoke-static {v0}, LX/EZe;->a(Ljava/lang/String;)LX/EZe;

    move-result-object v0

    check-cast p0, LX/Eat;

    .line 2142614
    iget-object v1, p0, LX/Eat;->a:[B

    move-object v1, v1

    .line 2142615
    invoke-virtual {v0, v1, p1, p2}, LX/EZe;->a([B[B[B)Z

    move-result v0

    return v0

    .line 2142616
    :cond_0
    new-instance v0, LX/Eag;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2142617
    const/4 v2, 0x5

    move v2, v2

    .line 2142618
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eag;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/Eas;[B)[B
    .locals 3

    .prologue
    .line 2142619
    const/4 v0, 0x5

    move v0, v0

    .line 2142620
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 2142621
    const-string v0, "best"

    invoke-static {v0}, LX/EZe;->a(Ljava/lang/String;)LX/EZe;

    move-result-object v0

    check-cast p0, LX/Eas;

    .line 2142622
    iget-object v1, p0, LX/Eas;->a:[B

    move-object v1, v1

    .line 2142623
    invoke-virtual {v0, v1, p1}, LX/EZe;->b([B[B)[B

    move-result-object v0

    return-object v0

    .line 2142624
    :cond_0
    new-instance v0, LX/Eag;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2142625
    const/4 v2, 0x5

    move v2, v2

    .line 2142626
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eag;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/Eat;LX/Eas;)[B
    .locals 3

    .prologue
    .line 2142627
    const/4 v0, 0x5

    move v0, v0

    .line 2142628
    const/4 v1, 0x5

    move v1, v1

    .line 2142629
    if-eq v0, v1, :cond_0

    .line 2142630
    new-instance v0, LX/Eag;

    const-string v1, "Public and private keys must be of the same type!"

    invoke-direct {v0, v1}, LX/Eag;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142631
    :cond_0
    const/4 v0, 0x5

    move v0, v0

    .line 2142632
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 2142633
    const-string v0, "best"

    invoke-static {v0}, LX/EZe;->a(Ljava/lang/String;)LX/EZe;

    move-result-object v0

    check-cast p0, LX/Eat;

    .line 2142634
    iget-object v1, p0, LX/Eat;->a:[B

    move-object v1, v1

    .line 2142635
    check-cast p1, LX/Eas;

    .line 2142636
    iget-object v2, p1, LX/Eas;->a:[B

    move-object v2, v2

    .line 2142637
    invoke-virtual {v0, v1, v2}, LX/EZe;->a([B[B)[B

    move-result-object v0

    return-object v0

    .line 2142638
    :cond_1
    new-instance v0, LX/Eag;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2142639
    const/4 v2, 0x5

    move v2, v2

    .line 2142640
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eag;-><init>(Ljava/lang/String;)V

    throw v0
.end method
