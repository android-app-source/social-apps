.class public LX/E4X;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/E4W;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/E4Y;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Cz;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2076854
    const-class v0, LX/E4X;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/E4X;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Ljava/util/List;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/E4Y;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/1Cz;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2076835
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2076836
    iput-object p1, p0, LX/E4X;->b:LX/0Ot;

    .line 2076837
    iput-object p2, p0, LX/E4X;->c:Ljava/util/List;

    .line 2076838
    iput-object p3, p0, LX/E4X;->d:Ljava/util/Map;

    .line 2076839
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2076840
    iget-object v0, p0, LX/E4X;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2076841
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2076842
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 2076843
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cz;

    .line 2076844
    new-instance v1, LX/E4W;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Cz;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, v0}, LX/E4W;-><init>(Landroid/view/View;)V

    move-object v0, v1

    .line 2076845
    :goto_0
    return-object v0

    .line 2076846
    :cond_1
    iget-object v0, p0, LX/E4X;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/E4X;->a:Ljava/lang/String;

    const-string v2, "Uknown ViewType found, returning empty view"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2076847
    new-instance v0, LX/E4W;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E4W;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2076848
    check-cast p1, LX/E4W;

    .line 2076849
    iget-object v0, p0, LX/E4X;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E4Y;

    iget-object v0, v0, LX/E4Y;->a:LX/5eI;

    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/5eI;->a(Landroid/view/View;)V

    .line 2076850
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2076851
    iget-object v0, p0, LX/E4X;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E4Y;

    iget-object v0, v0, LX/E4Y;->b:LX/1Cz;

    .line 2076852
    iget-object v1, p0, LX/E4X;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2076853
    iget-object v0, p0, LX/E4X;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
