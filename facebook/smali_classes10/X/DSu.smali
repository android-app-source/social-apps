.class public final LX/DSu;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;",
        ">;"
    }
.end annotation


# instance fields
.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Z

.field private final l:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;LX/0tX;LX/B1b;Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2000059
    invoke-direct {p0, p1, p7, p8}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;)V

    .line 2000060
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2000061
    iput-object v0, p0, LX/DSu;->e:LX/0Px;

    .line 2000062
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/DSu;->f:Z

    .line 2000063
    iput-object p10, p0, LX/DSu;->j:Ljava/lang/String;

    .line 2000064
    iput-object p2, p0, LX/DSu;->h:Ljava/lang/String;

    .line 2000065
    iput-object p6, p0, LX/DSu;->g:Ljava/lang/Integer;

    .line 2000066
    iput-object p3, p0, LX/DSu;->i:Ljava/lang/String;

    .line 2000067
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/DSu;->k:Z

    .line 2000068
    iput-object p9, p0, LX/DSu;->l:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 2000069
    return-void
.end method

.method public static a(LX/15i;I)LX/3rL;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "I)",
            "LX/3rL",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2000058
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz p1, :cond_0

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->h(II)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v0}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private a(LX/0Pz;Ljava/util/List;LX/DSe;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DSf;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;",
            ">;",
            "LX/DSe;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2000071
    const/4 v2, 0x0

    move v11, v2

    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v11, v2, :cond_7

    .line 2000072
    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;

    .line 2000073
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2000074
    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/DSu;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2000075
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/DSu;->f:Z

    if-eqz v3, :cond_0

    if-nez v2, :cond_2

    .line 2000076
    :cond_0
    sget-object v7, LX/DSd;->NONE:LX/DSd;

    .line 2000077
    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x7f489e48

    if-ne v3, v4, :cond_3

    .line 2000078
    sget-object v7, LX/DSd;->EMAIL_INVITE:LX/DSd;

    .line 2000079
    :cond_1
    :goto_1
    if-eqz v2, :cond_4

    invoke-direct/range {p0 .. p0}, LX/DSu;->k()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    move v9, v2

    .line 2000080
    :goto_2
    new-instance v2, LX/DSf;

    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v3

    sget-object v4, LX/DSb;->NOT_ADMIN:LX/DSb;

    sget-object v5, LX/DSc;->NOT_BLOCKED:LX/DSc;

    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    move-result-object v6

    if-nez v6, :cond_5

    const/4 v6, 0x0

    :goto_3
    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->l()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    move-result-object v8

    if-eqz v9, :cond_6

    sget-object v9, LX/DSe;->SELF:LX/DSe;

    :goto_4
    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->l()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;

    move-result-object v10

    invoke-direct/range {v2 .. v10}, LX/DSf;-><init>(LX/DUV;LX/DSb;LX/DSc;LX/DS2;LX/DSd;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;LX/DSe;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2000081
    :cond_2
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_0

    .line 2000082
    :cond_3
    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x58017a73

    if-ne v3, v4, :cond_1

    .line 2000083
    sget-object v7, LX/DSd;->ONSITE_INVITE:LX/DSd;

    goto :goto_1

    .line 2000084
    :cond_4
    const/4 v2, 0x0

    move v9, v2

    goto :goto_2

    .line 2000085
    :cond_5
    new-instance v6, LX/DS2;

    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->j()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->j()J

    move-result-wide v14

    invoke-direct {v6, v8, v12, v14, v15}, LX/DS2;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_3

    :cond_6
    move-object/from16 v9, p3

    goto :goto_4

    .line 2000086
    :cond_7
    return-void
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 2000070
    iget-object v0, p0, LX/DSu;->l:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    const-string v1, "gk_groups_member_bios"

    invoke-static {v0, v1}, LX/88m;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Enum;)LX/0gW;
    .locals 4

    .prologue
    .line 2000029
    invoke-static {}, LX/DUT;->a()LX/DUS;

    move-result-object v1

    .line 2000030
    const-string v0, "group_id"

    iget-object v2, p0, LX/DSu;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "search_term"

    iget-object v3, p0, LX/DSu;->i:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "profile_image_size"

    iget-object v3, p0, LX/DSu;->g:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "friend_end_cursor"

    iget-object v3, p0, LX/B1d;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2000031
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 2000032
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2000033
    const-string v2, "other_end_cursor"

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2000034
    :cond_0
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->INVITES:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2000035
    const-string v2, "invite_end_cursor"

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->INVITES:LX/DSe;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2000036
    :cond_1
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->FRIEND:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2000037
    const-string v2, "friend_end_cursor"

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->FRIEND:LX/DSe;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2000038
    :cond_2
    sget-object v0, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    if-ne p1, v0, :cond_3

    .line 2000039
    const-string v0, "other_count"

    const-string v2, "12"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "friend_count"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "invite_count"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-object v0, v1

    .line 2000040
    :goto_0
    return-object v0

    .line 2000041
    :cond_3
    sget-object v0, LX/DSe;->INVITES:LX/DSe;

    if-ne p1, v0, :cond_4

    .line 2000042
    const-string v0, "other_count"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "friend_count"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "invite_count"

    const-string v3, "12"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-object v0, v1

    .line 2000043
    goto :goto_0

    .line 2000044
    :cond_4
    sget-object v0, LX/DSe;->FRIEND:LX/DSe;

    if-ne p1, v0, :cond_5

    .line 2000045
    const-string v0, "other_count"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "invite_count"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "friend_count"

    const-string v3, "50"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-object v0, v1

    .line 2000046
    goto :goto_0

    .line 2000047
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0xc

    const/4 v2, 0x1

    .line 2000048
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    .line 2000049
    invoke-static {}, LX/DUT;->a()LX/DUS;

    move-result-object v4

    .line 2000050
    const-string v0, "group_id"

    iget-object v5, p0, LX/DSu;->h:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v5, "search_term"

    iget-object v6, p0, LX/DSu;->i:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v5, "profile_image_size"

    iget-object v6, p0, LX/DSu;->g:Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v5, "friend_count"

    const-string v6, "12"

    invoke-virtual {v0, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v5, "friend_end_cursor"

    invoke-virtual {v0, v5, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "other_count"

    if-eqz v3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v5, "invite_count"

    if-eqz v3, :cond_3

    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2000051
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 2000052
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v1, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2000053
    const-string v1, "other_end_cursor"

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2000054
    :cond_0
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v1, LX/DSe;->INVITES:LX/DSe;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2000055
    const-string v1, "invite_end_cursor"

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v2, LX/DSe;->INVITES:LX/DSe;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2000056
    :cond_1
    return-object v4

    :cond_2
    move v0, v2

    .line 2000057
    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1999968
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1999969
    iget-object v0, p0, LX/DSu;->e:LX/0Px;

    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1999970
    if-eqz p1, :cond_2

    .line 1999971
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999972
    if-eqz v0, :cond_2

    .line 1999973
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999974
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1999975
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999976
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->q()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p0, LX/DSu;->d:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1999977
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999978
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1999979
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999980
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1999981
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999982
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    sget-object v2, LX/DSe;->FRIEND:LX/DSe;

    invoke-direct {p0, v5, v0, v2}, LX/DSu;->a(LX/0Pz;Ljava/util/List;LX/DSe;)V

    .line 1999983
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999984
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1999985
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999986
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1999987
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999988
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    sget-object v2, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    invoke-direct {p0, v5, v0, v2}, LX/DSu;->a(LX/0Pz;Ljava/util/List;LX/DSe;)V

    .line 1999989
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999990
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1999991
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999992
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/DSu;->k:Z

    if-nez v0, :cond_2

    .line 1999993
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999994
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    sget-object v2, LX/DSe;->INVITES:LX/DSe;

    invoke-direct {p0, v5, v0, v2}, LX/DSu;->a(LX/0Pz;Ljava/util/List;LX/DSe;)V

    .line 1999995
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999996
    if-eqz v0, :cond_8

    .line 1999997
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999998
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1999999
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000000
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 2000001
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000002
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2000003
    :goto_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iput-object v5, p0, LX/DSu;->e:LX/0Px;

    .line 2000004
    if-eqz v0, :cond_3

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iput-object v3, p0, LX/DSu;->a:Ljava/lang/String;

    .line 2000005
    if-eqz v0, :cond_7

    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_1
    iput-boolean v1, p0, LX/DSu;->b:Z

    .line 2000006
    if-eqz p1, :cond_6

    .line 2000007
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000008
    if-eqz v0, :cond_6

    .line 2000009
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000010
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2000011
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000012
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    sget-object v0, LX/DSe;->FRIEND:LX/DSe;

    invoke-virtual {p0, v0}, LX/B1d;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2000013
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000014
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2000015
    iget-object v2, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->FRIEND:LX/DSe;

    invoke-static {v1, v0}, LX/DSu;->a(LX/15i;I)LX/3rL;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2000016
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000017
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    sget-object v0, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    invoke-virtual {p0, v0}, LX/B1d;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2000018
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000019
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2000020
    iget-object v2, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    invoke-static {v1, v0}, LX/DSu;->a(LX/15i;I)LX/3rL;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2000021
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000022
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    sget-object v0, LX/DSe;->INVITES:LX/DSe;

    invoke-virtual {p0, v0}, LX/B1d;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2000023
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000024
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2000025
    iget-object v2, p0, LX/B1d;->c:Ljava/util/HashMap;

    sget-object v3, LX/DSe;->INVITES:LX/DSe;

    invoke-static {v1, v0}, LX/DSu;->a(LX/15i;I)LX/3rL;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2000026
    :cond_6
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 2000027
    return-void

    :cond_7
    move v1, v4

    .line 2000028
    goto/16 :goto_1

    :cond_8
    move v0, v1

    move-object v2, v3

    goto/16 :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1999967
    const/4 v0, 0x1

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1999966
    const-string v0, "Group members fetch failed"

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1999962
    iget-object v0, p0, LX/DSu;->e:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1999963
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1999964
    iput-object v0, p0, LX/DSu;->e:LX/0Px;

    .line 1999965
    return-void
.end method
