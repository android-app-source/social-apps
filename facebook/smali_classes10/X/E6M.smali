.class public final LX/E6M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:LX/E6N;


# direct methods
.method public constructor <init>(LX/E6N;)V
    .locals 0

    .prologue
    .line 2080024
    iput-object p1, p0, LX/E6M;->a:LX/E6N;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    .line 2080025
    iget-object v0, p0, LX/E6M;->a:LX/E6N;

    .line 2080026
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 2080027
    invoke-static {v0}, LX/E6N;->c(LX/E6N;)Ljava/lang/String;

    move-result-object v4

    .line 2080028
    invoke-static {v0}, LX/E6N;->d(LX/E6N;)Ljava/lang/String;

    move-result-object v2

    .line 2080029
    iget-object v3, v0, LX/E6N;->i:LX/2jY;

    .line 2080030
    iget-object v5, v3, LX/2jY;->x:Landroid/os/Bundle;

    move-object v3, v5

    .line 2080031
    if-eqz v3, :cond_4

    .line 2080032
    iget-object v3, v0, LX/E6N;->i:LX/2jY;

    .line 2080033
    iget-object v5, v3, LX/2jY;->x:Landroid/os/Bundle;

    move-object v3, v5

    .line 2080034
    const-string v5, "gravity_location_data"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/placetips/settings/PlaceTipsLocationData;

    .line 2080035
    :goto_0
    move-object v3, v3

    .line 2080036
    const v5, 0x7f0d3209

    if-ne v1, v5, :cond_2

    .line 2080037
    iget-object v1, v0, LX/E6N;->d:LX/CeT;

    .line 2080038
    new-instance v5, LX/4KM;

    invoke-direct {v5}, LX/4KM;-><init>()V

    .line 2080039
    const-string v6, "page_id"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2080040
    move-object v5, v5

    .line 2080041
    new-instance v6, LX/CeY;

    invoke-direct {v6}, LX/CeY;-><init>()V

    move-object v6, v6

    .line 2080042
    const-string v7, "input"

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/CeY;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 2080043
    iget-object v5, v1, LX/CeT;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-virtual {v5, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2080044
    new-instance v1, Landroid/content/Intent;

    iget-object v5, v0, LX/E6N;->a:Landroid/content/Context;

    const-class v6, Lcom/facebook/placetips/settings/ui/PlaceTipsBlacklistFeedbackActivity;

    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2080045
    if-eqz v3, :cond_0

    .line 2080046
    const-string v5, "gravity_location_data"

    invoke-virtual {v1, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2080047
    :cond_0
    const-string v3, "place_id"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2080048
    const-string v3, "place_name"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2080049
    iget-object v2, v0, LX/E6N;->e:Lcom/facebook/content/SecureContextHelper;

    sget-object v3, LX/Cfc;->PLACE_TIPS_HIDE_TAP:LX/Cfc;

    invoke-virtual {v3}, LX/Cfc;->ordinal()I

    move-result v3

    iget-object v4, v0, LX/E6N;->h:Landroid/support/v4/app/Fragment;

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2080050
    :cond_1
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 2080051
    :cond_2
    const v5, 0x7f0d320a

    if-ne v1, v5, :cond_3

    .line 2080052
    iget-object v1, v0, LX/E6N;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f083135

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2080053
    iget-object v1, v0, LX/E6N;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0kL;

    new-instance v5, LX/27k;

    invoke-direct {v5, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2080054
    iget-object v1, v0, LX/E6N;->d:LX/CeT;

    const-string v2, "INCORRECT_LOCATION"

    iget-object v5, v0, LX/E6N;->g:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-virtual/range {v1 .. v6}, LX/CeT;->a(Ljava/lang/String;Lcom/facebook/placetips/settings/PlaceTipsLocationData;Ljava/lang/String;J)V

    goto :goto_1

    .line 2080055
    :cond_3
    const v2, 0x7f0d320b

    if-ne v1, v2, :cond_1

    .line 2080056
    iget-object v1, v0, LX/E6N;->c:LX/17W;

    iget-object v2, v0, LX/E6N;->a:Landroid/content/Context;

    invoke-static {v0}, LX/E6N;->e(LX/E6N;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0
.end method
