.class public LX/CzA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "LX/Cyv;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/80n;


# instance fields
.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Cyv;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field private d:LX/80n;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1954223
    new-instance v0, LX/Cz7;

    invoke-direct {v0}, LX/Cz7;-><init>()V

    sput-object v0, LX/CzA;->a:LX/80n;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1954219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954220
    const/4 v0, 0x0

    iput v0, p0, LX/CzA;->c:I

    .line 1954221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CzA;->b:Ljava/util/ArrayList;

    .line 1954222
    return-void
.end method


# virtual methods
.method public final a(LX/Cyv;)I
    .locals 1

    .prologue
    .line 1954218
    iget-object v0, p0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I
    .locals 2

    .prologue
    .line 1954196
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1954197
    iget-object v0, p0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyv;

    invoke-interface {v0}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1954198
    :goto_1
    return v1

    .line 1954199
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1954200
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final a(ILX/Cyv;)LX/Cyv;
    .locals 1

    .prologue
    .line 1954217
    iget-object v0, p0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyv;

    return-object v0
.end method

.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1954216
    invoke-virtual {p0, p1}, LX/CzA;->b(I)LX/Cyv;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;LX/80n;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/Cyv;",
            ">;",
            "LX/80n;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1954211
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyv;

    .line 1954212
    iget-object v3, p0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1954213
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1954214
    :cond_0
    iput-object p2, p0, LX/CzA;->d:LX/80n;

    .line 1954215
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1954210
    iget-object v0, p0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final b(I)LX/Cyv;
    .locals 1

    .prologue
    .line 1954209
    iget-object v0, p0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyv;

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1954207
    iget v0, p0, LX/CzA;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/CzA;->c:I

    .line 1954208
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1954203
    iget-object v0, p0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1954204
    sget-object v0, LX/CzA;->a:LX/80n;

    iput-object v0, p0, LX/CzA;->d:LX/80n;

    .line 1954205
    const/4 v0, 0x0

    iput v0, p0, LX/CzA;->c:I

    .line 1954206
    return-void
.end method

.method public final e()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "LX/Cz3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954202
    new-instance v0, LX/Cz8;

    invoke-direct {v0, p0}, LX/Cz8;-><init>(LX/CzA;)V

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1954201
    iget-object v0, p0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
