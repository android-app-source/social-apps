.class public final LX/EYs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Map$Entry",
        "<TK;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<TK;",
            "LX/EYu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map$Entry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;",
            "LX/EYu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2138114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138115
    iput-object p1, p0, LX/EYs;->a:Ljava/util/Map$Entry;

    .line 2138116
    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 2138117
    iget-object v0, p0, LX/EYs;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2138118
    iget-object v0, p0, LX/EYs;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYu;

    .line 2138119
    if-nez v0, :cond_0

    .line 2138120
    const/4 v0, 0x0

    .line 2138121
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/EYu;->a()LX/EWW;

    move-result-object v0

    goto :goto_0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2138122
    instance-of v0, p1, LX/EWW;

    if-nez v0, :cond_0

    .line 2138123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2138124
    :cond_0
    iget-object v0, p0, LX/EYs;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYu;

    check-cast p1, LX/EWW;

    .line 2138125
    iget-object v1, v0, LX/EYu;->d:LX/EWW;

    .line 2138126
    iput-object p1, v0, LX/EYu;->d:LX/EWW;

    .line 2138127
    const/4 p0, 0x0

    iput-object p0, v0, LX/EYu;->c:LX/EWc;

    .line 2138128
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/EYu;->e:Z

    .line 2138129
    move-object v0, v1

    .line 2138130
    return-object v0
.end method
