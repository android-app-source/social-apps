.class public final enum LX/DSe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DSe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DSe;

.field public static final enum ADMIN:LX/DSe;

.field public static final enum COMMUNITY_MEMBER:LX/DSe;

.field public static final enum FRIEND:LX/DSe;

.field public static final enum INVITES:LX/DSe;

.field public static final enum MODERATOR:LX/DSe;

.field public static final enum NON_FRIEND_MEMBER:LX/DSe;

.field public static final enum SELF:LX/DSe;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1999758
    new-instance v0, LX/DSe;

    const-string v1, "SELF"

    invoke-direct {v0, v1, v3}, LX/DSe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSe;->SELF:LX/DSe;

    .line 1999759
    new-instance v0, LX/DSe;

    const-string v1, "FRIEND"

    invoke-direct {v0, v1, v4}, LX/DSe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSe;->FRIEND:LX/DSe;

    .line 1999760
    new-instance v0, LX/DSe;

    const-string v1, "NON_FRIEND_MEMBER"

    invoke-direct {v0, v1, v5}, LX/DSe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    .line 1999761
    new-instance v0, LX/DSe;

    const-string v1, "INVITES"

    invoke-direct {v0, v1, v6}, LX/DSe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSe;->INVITES:LX/DSe;

    .line 1999762
    new-instance v0, LX/DSe;

    const-string v1, "ADMIN"

    invoke-direct {v0, v1, v7}, LX/DSe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSe;->ADMIN:LX/DSe;

    .line 1999763
    new-instance v0, LX/DSe;

    const-string v1, "MODERATOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/DSe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSe;->MODERATOR:LX/DSe;

    .line 1999764
    new-instance v0, LX/DSe;

    const-string v1, "COMMUNITY_MEMBER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/DSe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSe;->COMMUNITY_MEMBER:LX/DSe;

    .line 1999765
    const/4 v0, 0x7

    new-array v0, v0, [LX/DSe;

    sget-object v1, LX/DSe;->SELF:LX/DSe;

    aput-object v1, v0, v3

    sget-object v1, LX/DSe;->FRIEND:LX/DSe;

    aput-object v1, v0, v4

    sget-object v1, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    aput-object v1, v0, v5

    sget-object v1, LX/DSe;->INVITES:LX/DSe;

    aput-object v1, v0, v6

    sget-object v1, LX/DSe;->ADMIN:LX/DSe;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DSe;->MODERATOR:LX/DSe;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DSe;->COMMUNITY_MEMBER:LX/DSe;

    aput-object v2, v0, v1

    sput-object v0, LX/DSe;->$VALUES:[LX/DSe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1999766
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DSe;
    .locals 1

    .prologue
    .line 1999757
    const-class v0, LX/DSe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DSe;

    return-object v0
.end method

.method public static values()[LX/DSe;
    .locals 1

    .prologue
    .line 1999756
    sget-object v0, LX/DSe;->$VALUES:[LX/DSe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DSe;

    return-object v0
.end method
