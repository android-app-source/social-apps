.class public final LX/D0d;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/D0e;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/D0e;


# direct methods
.method public constructor <init>(LX/D0e;)V
    .locals 1

    .prologue
    .line 1955679
    iput-object p1, p0, LX/D0d;->d:LX/D0e;

    .line 1955680
    move-object v0, p1

    .line 1955681
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1955682
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1955683
    const-string v0, "DTIAppealHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1955684
    if-ne p0, p1, :cond_1

    .line 1955685
    :cond_0
    :goto_0
    return v0

    .line 1955686
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1955687
    goto :goto_0

    .line 1955688
    :cond_3
    check-cast p1, LX/D0d;

    .line 1955689
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1955690
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1955691
    if-eq v2, v3, :cond_0

    .line 1955692
    iget-boolean v2, p0, LX/D0d;->a:Z

    iget-boolean v3, p1, LX/D0d;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1955693
    goto :goto_0

    .line 1955694
    :cond_4
    iget-object v2, p0, LX/D0d;->b:LX/1Pn;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/D0d;->b:LX/1Pn;

    iget-object v3, p1, LX/D0d;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1955695
    goto :goto_0

    .line 1955696
    :cond_6
    iget-object v2, p1, LX/D0d;->b:LX/1Pn;

    if-nez v2, :cond_5

    .line 1955697
    :cond_7
    iget-object v2, p0, LX/D0d;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/D0d;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/D0d;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1955698
    goto :goto_0

    .line 1955699
    :cond_8
    iget-object v2, p1, LX/D0d;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
