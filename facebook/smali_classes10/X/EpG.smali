.class public LX/EpG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/EpG;


# instance fields
.field public a:LX/Epa;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2170091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170092
    return-void
.end method

.method public static a(LX/0QB;)LX/EpG;
    .locals 4

    .prologue
    .line 2170093
    sget-object v0, LX/EpG;->b:LX/EpG;

    if-nez v0, :cond_1

    .line 2170094
    const-class v1, LX/EpG;

    monitor-enter v1

    .line 2170095
    :try_start_0
    sget-object v0, LX/EpG;->b:LX/EpG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2170096
    if-eqz v2, :cond_0

    .line 2170097
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2170098
    new-instance p0, LX/EpG;

    invoke-direct {p0}, LX/EpG;-><init>()V

    .line 2170099
    const-class v3, LX/Epa;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Epa;

    .line 2170100
    iput-object v3, p0, LX/EpG;->a:LX/Epa;

    .line 2170101
    move-object v0, p0

    .line 2170102
    sput-object v0, LX/EpG;->b:LX/EpG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2170103
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2170104
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2170105
    :cond_1
    sget-object v0, LX/EpG;->b:LX/EpG;

    return-object v0

    .line 2170106
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2170107
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/5wN;ILandroid/content/Context;LX/2h7;LX/5P2;LX/EpL;)Z
    .locals 7
    .param p2    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 2170108
    sparse-switch p2, :sswitch_data_0

    .line 2170109
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2170110
    :sswitch_0
    iget-object v1, p0, LX/EpG;->a:LX/Epa;

    invoke-virtual {v1, p4, p5}, LX/Epa;->a(LX/2h7;LX/5P2;)LX/EpZ;

    move-result-object v1

    invoke-virtual {v1, p3, p1, p6}, LX/EpZ;->a(Landroid/content/Context;LX/5wN;LX/EpL;)V

    goto :goto_0

    .line 2170111
    :sswitch_1
    iget-object v1, p0, LX/EpG;->a:LX/Epa;

    invoke-virtual {v1, p4, p5}, LX/Epa;->a(LX/2h7;LX/5P2;)LX/EpZ;

    move-result-object v1

    invoke-interface {p1}, LX/5wN;->ce_()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/5wN;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-object v2, p3

    move-object v6, p6

    .line 2170112
    iget-object p0, v1, LX/EpZ;->d:LX/Epg;

    .line 2170113
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f080fa9

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2170114
    invoke-static {p0, v2, p1}, LX/Epg;->c(LX/Epg;Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    move-object p0, p1

    .line 2170115
    new-instance p1, LX/EpT;

    invoke-direct {p1, v1, v4, v5, v6}, LX/EpT;-><init>(LX/EpZ;JLX/EpL;)V

    iget-object p2, v1, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, p1, p2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    iput-object p0, v1, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2170116
    iget-object p0, v1, LX/EpZ;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p1, LX/EpU;

    invoke-direct {p1, v1}, LX/EpU;-><init>(LX/EpZ;)V

    iget-object p2, v1, LX/EpZ;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2170117
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch
.end method
