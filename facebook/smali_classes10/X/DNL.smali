.class public final LX/DNL;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/44w",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedHomeStories;",
        "LX/0ta;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DNR;


# direct methods
.method public constructor <init>(LX/DNR;)V
    .locals 0

    .prologue
    .line 1991365
    iput-object p1, p0, LX/DNL;->a:LX/DNR;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1991366
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 1991367
    iget-object v0, p0, LX/DNL;->a:LX/DNR;

    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    invoke-static {v0, p1}, LX/DNR;->a$redex0(LX/DNR;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1991368
    :cond_0
    iget-object v0, p0, LX/DNL;->a:LX/DNR;

    iget-object v1, p0, LX/DNL;->a:LX/DNR;

    iget-object v1, v1, LX/DNR;->l:LX/DKg;

    invoke-static {v0, v1}, LX/DNR;->a$redex0(LX/DNR;LX/DKg;)V

    .line 1991369
    iget-object v0, p0, LX/DNL;->a:LX/DNR;

    invoke-static {v0}, LX/DNR;->p(LX/DNR;)V

    .line 1991370
    iget-object v0, p0, LX/DNL;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->b:LX/2lS;

    if-eqz v0, :cond_1

    .line 1991371
    iget-object v0, p0, LX/DNL;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->b:LX/2lS;

    .line 1991372
    sget-object v1, LX/2lT;->CACHED_STORIES:LX/2lT;

    invoke-static {v0, v1}, LX/2lS;->c(LX/2lS;LX/2lT;)V

    .line 1991373
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1991374
    check-cast p1, LX/44w;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1991375
    iget-object v0, p0, LX/DNL;->a:LX/DNR;

    invoke-static {v0}, LX/DNR;->q(LX/DNR;)V

    .line 1991376
    iget-object v0, p1, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 1991377
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1991378
    iget-object v4, p0, LX/DNL;->a:LX/DNR;

    .line 1991379
    iput-boolean v2, v4, LX/DNR;->A:Z

    .line 1991380
    iget-object v4, p0, LX/DNL;->a:LX/DNR;

    iget-object v4, v4, LX/DNR;->b:LX/2lS;

    if-eqz v4, :cond_1

    .line 1991381
    iget-object v4, p0, LX/DNL;->a:LX/DNR;

    iget-object v4, v4, LX/DNR;->b:LX/2lS;

    const/4 v6, 0x1

    .line 1991382
    iget-boolean v5, v4, LX/2lS;->g:Z

    if-nez v5, :cond_0

    if-lez v1, :cond_5

    :cond_0
    move v5, v6

    :goto_0
    iput-boolean v5, v4, LX/2lS;->g:Z

    .line 1991383
    iput-boolean v6, v4, LX/2lS;->e:Z

    .line 1991384
    new-instance v5, LX/0P2;

    invoke-direct {v5}, LX/0P2;-><init>()V

    const-string v6, "stories_count"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v5

    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    .line 1991385
    sget-object v6, LX/2lT;->CACHED_STORIES:LX/2lT;

    invoke-static {v4, v6, v5}, LX/2lS;->a(LX/2lS;LX/2lT;LX/0P1;)V

    .line 1991386
    :cond_1
    iget-object v1, p1, LX/44w;->b:Ljava/lang/Object;

    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v1, v4, :cond_3

    .line 1991387
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1991388
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1991389
    iget-object v4, p0, LX/DNL;->a:LX/DNR;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p1, LX/44w;->b:Ljava/lang/Object;

    sget-object v6, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v5, v6, :cond_4

    .line 1991390
    :goto_2
    iput-boolean v2, v4, LX/DNR;->v:Z

    .line 1991391
    iget-object v2, p0, LX/DNL;->a:LX/DNR;

    invoke-virtual {v2}, LX/DNR;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1991392
    invoke-static {v0}, LX/DNR;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1991393
    :cond_2
    iget-object v2, p0, LX/DNL;->a:LX/DNR;

    invoke-static {v2, p1, v0}, LX/DNR;->a$redex0(LX/DNR;LX/44w;Ljava/util/List;)V

    .line 1991394
    iget-object v2, p0, LX/DNL;->a:LX/DNR;

    iget-object v2, v2, LX/DNR;->c:LX/0fz;

    invoke-virtual {v2, v0, v1}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    .line 1991395
    iget-object v0, p0, LX/DNL;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->u:LX/DNQ;

    invoke-interface {v0, v3}, LX/DNQ;->b(Z)V

    .line 1991396
    iget-object v0, p0, LX/DNL;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->u:LX/DNQ;

    invoke-interface {v0}, LX/DNQ;->a()V

    .line 1991397
    iget-object v0, p0, LX/DNL;->a:LX/DNR;

    invoke-static {v0}, LX/DNR;->p(LX/DNR;)V

    .line 1991398
    return-void

    .line 1991399
    :cond_3
    goto :goto_1

    :cond_4
    move v2, v3

    .line 1991400
    goto :goto_2

    .line 1991401
    :cond_5
    const/4 v5, 0x0

    goto :goto_0
.end method
