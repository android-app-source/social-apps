.class public final LX/E6p;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/E6r;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/5sc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)V
    .locals 5

    .prologue
    .line 2080692
    iput-object p1, p0, LX/E6p;->a:Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;

    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2080693
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E6p;->b:Ljava/util/List;

    .line 2080694
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 2080695
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2080696
    iget-object v4, p0, LX/E6p;->b:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2080697
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2080698
    :cond_1
    return-void
.end method

.method private d()I
    .locals 2

    .prologue
    .line 2080699
    iget-object v0, p0, LX/E6p;->a:Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;

    .line 2080700
    iget-object v1, v0, LX/Cfk;->d:Landroid/content/Context;

    move-object v1, v1

    .line 2080701
    move-object v0, v1

    .line 2080702
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1649

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2080703
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03114d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2080704
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {p0}, LX/E6p;->d()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2080705
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {p0}, LX/E6p;->d()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2080706
    const v1, 0x7f0d28e3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2080707
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2080708
    new-instance v1, LX/E6r;

    iget-object v2, p0, LX/E6p;->a:Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;

    invoke-direct {v1, v2, v0}, LX/E6r;-><init>(Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2080709
    check-cast p1, LX/E6r;

    .line 2080710
    iget-object v0, p0, LX/E6p;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5sc;

    .line 2080711
    invoke-interface {v0}, LX/5sc;->l()LX/5sY;

    move-result-object v1

    invoke-interface {v1}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2080712
    iget-object v2, p1, LX/E6r;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2080713
    iget-object v1, p1, LX/E6r;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p1, LX/E6r;->l:Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;

    iget-object p0, p1, LX/E6r;->l:Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;

    iget-object p0, p0, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->j:Ljava/lang/String;

    iget-object p2, p1, LX/E6r;->l:Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;

    iget-object p2, p2, Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;->k:Ljava/lang/String;

    .line 2080714
    new-instance p1, LX/E6o;

    invoke-direct {p1, v2, v0, p0, p2}, LX/E6o;-><init>(Lcom/facebook/reaction/ui/attachment/handler/ReactionFacepileHscrollHandler;LX/5sc;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, p1

    .line 2080715
    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080716
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2080717
    iget-object v0, p0, LX/E6p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
