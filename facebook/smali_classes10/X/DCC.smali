.class public LX/DCC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0bH;

.field private final b:LX/189;

.field public final c:LX/99v;

.field private final d:LX/1L1;

.field private e:LX/1L6;

.field public f:LX/0qq;

.field private g:LX/0g4;

.field private h:LX/DCA;


# direct methods
.method public constructor <init>(LX/0bH;LX/189;LX/99v;LX/1L1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1973720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1973721
    iput-object p1, p0, LX/DCC;->a:LX/0bH;

    .line 1973722
    iput-object p2, p0, LX/DCC;->b:LX/189;

    .line 1973723
    iput-object p3, p0, LX/DCC;->c:LX/99v;

    .line 1973724
    iput-object p4, p0, LX/DCC;->d:LX/1L1;

    .line 1973725
    return-void
.end method

.method public static a(LX/0QB;)LX/DCC;
    .locals 5

    .prologue
    .line 1973726
    new-instance v4, LX/DCC;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v0

    check-cast v0, LX/0bH;

    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v1

    check-cast v1, LX/189;

    invoke-static {p0}, LX/99v;->b(LX/0QB;)LX/99v;

    move-result-object v2

    check-cast v2, LX/99v;

    invoke-static {p0}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v3

    check-cast v3, LX/1L1;

    invoke-direct {v4, v0, v1, v2, v3}, LX/DCC;-><init>(LX/0bH;LX/189;LX/99v;LX/1L1;)V

    .line 1973727
    move-object v0, v4

    .line 1973728
    return-object v0
.end method

.method public static a$redex0(LX/DCC;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1973729
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1973730
    :goto_0
    return-object v0

    .line 1973731
    :cond_0
    iget-object v0, p0, LX/DCC;->f:LX/0qq;

    invoke-virtual {v0, p1}, LX/0qq;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1973732
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1973733
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1973734
    instance-of v3, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_1

    .line 1973735
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1973736
    goto :goto_0
.end method

.method public static a$redex0(LX/DCC;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1973737
    iget-object v0, p0, LX/DCC;->b:LX/189;

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0, p1, v1}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/StoryVisibility;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1973738
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1973739
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1973740
    iget-object v1, p0, LX/DCC;->f:LX/0qq;

    invoke-virtual {v1, v0}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1973741
    iget-object v0, p0, LX/DCC;->g:LX/0g4;

    invoke-interface {v0}, LX/0g4;->d()V

    .line 1973742
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1973743
    iget-object v0, p0, LX/DCC;->a:LX/0bH;

    iget-object v1, p0, LX/DCC;->e:LX/1L6;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1973744
    iget-object v0, p0, LX/DCC;->d:LX/1L1;

    iget-object v1, p0, LX/DCC;->h:LX/DCA;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1973745
    return-void
.end method

.method public final a(LX/0qq;LX/0g4;)V
    .locals 2

    .prologue
    .line 1973746
    new-instance v0, LX/DCB;

    invoke-direct {v0, p0}, LX/DCB;-><init>(LX/DCC;)V

    iput-object v0, p0, LX/DCC;->e:LX/1L6;

    .line 1973747
    new-instance v0, LX/DCA;

    invoke-direct {v0, p0}, LX/DCA;-><init>(LX/DCC;)V

    iput-object v0, p0, LX/DCC;->h:LX/DCA;

    .line 1973748
    iget-object v0, p0, LX/DCC;->a:LX/0bH;

    iget-object v1, p0, LX/DCC;->e:LX/1L6;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1973749
    iget-object v0, p0, LX/DCC;->d:LX/1L1;

    iget-object v1, p0, LX/DCC;->h:LX/DCA;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1973750
    iput-object p1, p0, LX/DCC;->f:LX/0qq;

    .line 1973751
    iput-object p2, p0, LX/DCC;->g:LX/0g4;

    .line 1973752
    return-void
.end method
