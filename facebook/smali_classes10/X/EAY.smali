.class public final LX/EAY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BNC;


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/reviews/handler/DeleteReviewHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/reviews/handler/DeleteReviewHandler;LX/4BY;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2085665
    iput-object p1, p0, LX/EAY;->d:Lcom/facebook/reviews/handler/DeleteReviewHandler;

    iput-object p2, p0, LX/EAY;->a:LX/4BY;

    iput-object p3, p0, LX/EAY;->b:Ljava/lang/String;

    iput-object p4, p0, LX/EAY;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2085666
    iget-object v0, p0, LX/EAY;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2085667
    iget-object v0, p0, LX/EAY;->d:Lcom/facebook/reviews/handler/DeleteReviewHandler;

    iget-object v1, p0, LX/EAY;->b:Ljava/lang/String;

    iget-object v2, p0, LX/EAY;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/reviews/handler/DeleteReviewHandler;->a$redex0(Lcom/facebook/reviews/handler/DeleteReviewHandler;Ljava/lang/String;Ljava/lang/String;)V

    .line 2085668
    return-void
.end method

.method public final a(Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;)V
    .locals 3

    .prologue
    .line 2085669
    iget-object v0, p0, LX/EAY;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2085670
    iget-object v0, p0, LX/EAY;->d:Lcom/facebook/reviews/handler/DeleteReviewHandler;

    iget-object v1, p0, LX/EAY;->b:Ljava/lang/String;

    iget-object v2, p0, LX/EAY;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/reviews/handler/DeleteReviewHandler;->a$redex0(Lcom/facebook/reviews/handler/DeleteReviewHandler;Ljava/lang/String;Ljava/lang/String;)V

    .line 2085671
    if-eqz p1, :cond_0

    .line 2085672
    iget-object v0, p0, LX/EAY;->d:Lcom/facebook/reviews/handler/DeleteReviewHandler;

    iget-object v0, v0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->c:LX/Ch5;

    new-instance v1, LX/Ch7;

    iget-object v2, p0, LX/EAY;->b:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, LX/Ch7;-><init>(Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2085673
    :cond_0
    return-void
.end method
