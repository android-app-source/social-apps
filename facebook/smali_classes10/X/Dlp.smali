.class public final LX/Dlp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;)V
    .locals 0

    .prologue
    .line 2038680
    iput-object p1, p0, LX/Dlp;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2038684
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2038685
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2038681
    iget-object v0, p0, LX/Dlp;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->b:LX/Dls;

    if-eqz v0, :cond_0

    .line 2038682
    iget-object v0, p0, LX/Dlp;->a:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->b:LX/Dls;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Dls;->a(Ljava/lang/String;)V

    .line 2038683
    :cond_0
    return-void
.end method
