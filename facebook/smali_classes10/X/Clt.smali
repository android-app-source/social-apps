.class public final enum LX/Clt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Clt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Clt;

.field public static final enum BASIC_LAYOUT:LX/Clt;

.field public static final enum GROWTH_EXPERIMENT:LX/Clt;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1933142
    new-instance v0, LX/Clt;

    const-string v1, "BASIC_LAYOUT"

    invoke-direct {v0, v1, v2}, LX/Clt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clt;->BASIC_LAYOUT:LX/Clt;

    .line 1933143
    new-instance v0, LX/Clt;

    const-string v1, "GROWTH_EXPERIMENT"

    invoke-direct {v0, v1, v3}, LX/Clt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Clt;->GROWTH_EXPERIMENT:LX/Clt;

    .line 1933144
    const/4 v0, 0x2

    new-array v0, v0, [LX/Clt;

    sget-object v1, LX/Clt;->BASIC_LAYOUT:LX/Clt;

    aput-object v1, v0, v2

    sget-object v1, LX/Clt;->GROWTH_EXPERIMENT:LX/Clt;

    aput-object v1, v0, v3

    sput-object v0, LX/Clt;->$VALUES:[LX/Clt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1933141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Clt;
    .locals 1

    .prologue
    .line 1933139
    const-class v0, LX/Clt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Clt;

    return-object v0
.end method

.method public static values()[LX/Clt;
    .locals 1

    .prologue
    .line 1933140
    sget-object v0, LX/Clt;->$VALUES:[LX/Clt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Clt;

    return-object v0
.end method
