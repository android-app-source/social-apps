.class public LX/E6n;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/events/widget/eventcard/EventsCardView;

.field public final b:LX/38v;

.field public final c:LX/7vZ;

.field public final d:LX/7vW;

.field public final e:LX/1Ad;

.field public final f:LX/DB4;

.field private final g:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/38v;LX/7vZ;LX/7vW;LX/1Ad;LX/DB4;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2080600
    iput-object p1, p0, LX/E6n;->b:LX/38v;

    .line 2080601
    iput-object p2, p0, LX/E6n;->c:LX/7vZ;

    .line 2080602
    iput-object p3, p0, LX/E6n;->d:LX/7vW;

    .line 2080603
    iput-object p4, p0, LX/E6n;->e:LX/1Ad;

    .line 2080604
    iput-object p5, p0, LX/E6n;->f:LX/DB4;

    .line 2080605
    iput-object p6, p0, LX/E6n;->g:Landroid/content/res/Resources;

    .line 2080606
    return-void
.end method

.method public static b(LX/0QB;)LX/E6n;
    .locals 7

    .prologue
    .line 2080607
    new-instance v0, LX/E6n;

    const-class v1, LX/38v;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/38v;

    invoke-static {p0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v2

    check-cast v2, LX/7vZ;

    invoke-static {p0}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object v3

    check-cast v3, LX/7vW;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-static {p0}, LX/DB4;->b(LX/0QB;)LX/DB4;

    move-result-object v5

    check-cast v5, LX/DB4;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct/range {v0 .. v6}, LX/E6n;-><init>(LX/38v;LX/7vZ;LX/7vW;LX/1Ad;LX/DB4;Landroid/content/res/Resources;)V

    .line 2080608
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Lcom/facebook/events/widget/eventcard/EventsCardView;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 9

    .prologue
    .line 2080609
    iput-object p2, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    .line 2080610
    iget-object v0, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->e()V

    .line 2080611
    iget-object v0, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    iget-object v1, p0, LX/E6n;->g:Landroid/content/res/Resources;

    const v2, 0x7f020640

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2080612
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    const/4 v5, 0x0

    .line 2080613
    iget-object v3, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-interface {v0}, LX/7oY;->eR_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2080614
    iget-object v3, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    .line 2080615
    invoke-interface {v0}, LX/7oX;->j()J

    move-result-wide v7

    invoke-static {v7, v8}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v7

    move-object v4, v7

    .line 2080616
    invoke-virtual {v3, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCalendarFormatStartDate(Ljava/util/Date;)V

    .line 2080617
    iget-object v3, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    .line 2080618
    invoke-interface {v0}, LX/7oX;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v4

    .line 2080619
    if-eqz v4, :cond_2

    .line 2080620
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 2080621
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 2080622
    :goto_0
    move-object v4, v4

    .line 2080623
    invoke-virtual {v3, v4, v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2080624
    invoke-interface {v0}, LX/7oY;->n()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2080625
    iget-object v3, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    iget-object v4, p0, LX/E6n;->f:LX/DB4;

    invoke-interface {v0}, LX/7oY;->o()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, LX/DB4;->a(J)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0a00d2

    invoke-virtual {v3, v4, v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Ljava/lang/CharSequence;I)V

    .line 2080626
    :goto_1
    invoke-static {p1}, LX/E6k;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Ljava/lang/String;

    move-result-object v0

    .line 2080627
    if-eqz v0, :cond_3

    .line 2080628
    iget-object v1, p0, LX/E6n;->e:LX/1Ad;

    invoke-virtual {v1, p3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2080629
    iget-object v2, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v2, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoController(LX/1aZ;)V

    .line 2080630
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    .line 2080631
    new-instance v1, LX/E6m;

    invoke-direct {v1, p0, v0}, LX/E6m;-><init>(LX/E6n;LX/7oY;)V

    .line 2080632
    iget-object v2, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v2}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getActionButton()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v2

    iget-object v3, p0, LX/E6n;->b:LX/38v;

    invoke-virtual {v3, v1}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v1

    invoke-interface {v0}, LX/7oY;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v3

    invoke-interface {v0}, LX/7oY;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v4

    invoke-interface {v0}, LX/7oY;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    .line 2080633
    return-void

    .line 2080634
    :cond_0
    iget-object v3, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v3, v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setSocialContextText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2080635
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v4

    .line 2080636
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2080637
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2080638
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 2080639
    :cond_3
    iget-object v1, p0, LX/E6n;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoController(LX/1aZ;)V

    goto :goto_2
.end method
