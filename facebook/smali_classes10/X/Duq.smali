.class public final LX/Duq;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dus;


# direct methods
.method public constructor <init>(LX/Dus;)V
    .locals 0

    .prologue
    .line 2057667
    iput-object p1, p0, LX/Duq;->a:LX/Dus;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2057697
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2057668
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2057669
    iget-object v0, p0, LX/Duq;->a:LX/Dus;

    .line 2057670
    sget-object v1, LX/Dur;->a:[I

    iget-object p0, v0, LX/Dus;->e:LX/Dup;

    invoke-virtual {p0}, LX/Dup;->ordinal()I

    move-result p0

    aget v1, v1, p0

    packed-switch v1, :pswitch_data_0

    .line 2057671
    :cond_0
    :goto_0
    const v1, 0x4e46e296    # 8.3418458E8f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2057672
    return-void

    .line 2057673
    :pswitch_0
    if-eqz p1, :cond_0

    .line 2057674
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057675
    if-eqz v1, :cond_0

    .line 2057676
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057677
    check-cast v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2057678
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057679
    check-cast v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2057680
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057681
    check-cast v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2057682
    iget-object p0, v0, LX/Dus;->f:Ljava/util/ArrayList;

    .line 2057683
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057684
    check-cast v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 2057685
    :pswitch_1
    if-eqz p1, :cond_0

    .line 2057686
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057687
    if-eqz v1, :cond_0

    .line 2057688
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057689
    check-cast v1, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;->j()Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2057690
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057691
    check-cast v1, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;->j()Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2057692
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057693
    check-cast v1, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;->j()Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2057694
    iget-object p0, v0, LX/Dus;->f:Ljava/util/ArrayList;

    .line 2057695
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2057696
    check-cast v1, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;->j()Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel$VideoListVideosModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
