.class public final LX/EJB;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/A2F;

.field public final synthetic b:LX/CzL;

.field public final synthetic c:LX/1Pn;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;LX/A2F;LX/CzL;LX/1Pn;)V
    .locals 1

    .prologue
    .line 2103455
    iput-object p1, p0, LX/EJB;->d:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    iput-object p2, p0, LX/EJB;->a:LX/A2F;

    iput-object p3, p0, LX/EJB;->b:LX/CzL;

    iput-object p4, p0, LX/EJB;->c:LX/1Pn;

    invoke-direct {p0}, LX/2eI;-><init>()V

    .line 2103456
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EJB;->e:Z

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2103457
    iget-object v0, p0, LX/EJB;->a:LX/A2F;

    invoke-interface {v0}, LX/A2F;->bm()LX/0Px;

    move-result-object v2

    .line 2103458
    iget-object v0, p0, LX/EJB;->a:LX/A2F;

    invoke-interface {v0}, LX/A2F;->bl()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v0

    const/4 v5, 0x0

    .line 2103459
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;->c()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object v6

    if-nez v6, :cond_4

    .line 2103460
    :cond_0
    :goto_0
    move-object v1, v5

    .line 2103461
    if-eqz v1, :cond_1

    .line 2103462
    iget-object v0, p0, LX/EJB;->d:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RC;

    iget-object v3, p0, LX/EJB;->b:LX/CzL;

    invoke-virtual {v3, v1}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2103463
    :cond_1
    iget-object v0, p0, LX/EJB;->d:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    iget-object v1, p0, LX/EJB;->a:LX/A2F;

    const/16 v13, 0xb

    const/4 v6, 0x0

    .line 2103464
    invoke-interface {v1}, LX/A2F;->cB()LX/A2E;

    move-result-object v5

    if-nez v5, :cond_6

    move v5, v6

    .line 2103465
    :goto_1
    move v0, v5

    .line 2103466
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    move v1, v0

    .line 2103467
    :goto_3
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2103468
    iget-object v0, p0, LX/EJB;->d:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1RC;

    iget-object v3, p0, LX/EJB;->b:LX/CzL;

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2103469
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2103470
    :cond_2
    const/4 v0, 0x1

    goto :goto_2

    .line 2103471
    :cond_3
    return-void

    .line 2103472
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;->b()LX/1Fb;

    move-result-object v6

    .line 2103473
    new-instance v7, LX/A2J;

    invoke-direct {v7}, LX/A2J;-><init>()V

    const-wide/16 v9, -0x1

    .line 2103474
    iput-wide v9, v7, LX/A2J;->d:J

    .line 2103475
    move-object v7, v7

    .line 2103476
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;->c()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object v8

    .line 2103477
    iput-object v8, v7, LX/A2J;->c:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    .line 2103478
    move-object v7, v7

    .line 2103479
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 2103480
    iput-object v8, v7, LX/A2J;->a:Ljava/lang/String;

    .line 2103481
    move-object v7, v7

    .line 2103482
    if-eqz v6, :cond_5

    invoke-static {v6}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    .line 2103483
    :cond_5
    iput-object v5, v7, LX/A2J;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2103484
    move-object v5, v7

    .line 2103485
    invoke-virtual {v5}, LX/A2J;->a()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    move-result-object v5

    goto :goto_0

    .line 2103486
    :cond_6
    invoke-interface {v1}, LX/A2F;->cB()LX/A2E;

    move-result-object v5

    invoke-interface {v5}, LX/A2E;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    .line 2103487
    invoke-static {v5}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v7

    .line 2103488
    iget-object v5, v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->b:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2103489
    invoke-interface {v1}, LX/A2F;->bm()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    .line 2103490
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 2103491
    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;->d()J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    invoke-virtual {v8, v9, v10}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2103492
    invoke-virtual {v7, v13}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v8, v13}, Ljava/util/Calendar;->get(I)I

    move-result v7

    if-eq v5, v7, :cond_7

    const/4 v5, 0x1

    goto/16 :goto_1

    :cond_7
    move v5, v6

    goto/16 :goto_1
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2103493
    if-ltz p1, :cond_0

    iget-boolean v0, p0, LX/EJB;->e:Z

    if-nez v0, :cond_0

    .line 2103494
    iget-object v0, p0, LX/EJB;->d:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v1, p0, LX/EJB;->c:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    .line 2103495
    sget-object p1, LX/CvJ;->WEATHER_FORECAST_INTERACTION:LX/CvJ;

    invoke-static {p1, v1}, LX/CvY;->a(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    .line 2103496
    invoke-static {v0, v1, p1}, LX/CvY;->a(LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2103497
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EJB;->e:Z

    .line 2103498
    :cond_0
    return-void
.end method
