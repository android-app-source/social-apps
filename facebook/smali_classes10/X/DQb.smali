.class public final LX/DQb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "pageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "pageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1994868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1994869
    return-void
.end method

.method public static a(Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;)LX/DQb;
    .locals 4

    .prologue
    .line 1994870
    new-instance v0, LX/DQb;

    invoke-direct {v0}, LX/DQb;-><init>()V

    .line 1994871
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;->a()I

    move-result v1

    iput v1, v0, LX/DQb;->a:I

    .line 1994872
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;->j()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/DQb;->b:LX/0Px;

    .line 1994873
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/DQb;->c:LX/15i;

    iput v1, v0, LX/DQb;->d:I

    monitor-exit v3

    .line 1994874
    return-object v0

    .line 1994875
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 1994876
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1994877
    iget-object v1, p0, LX/DQb;->b:LX/0Px;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1994878
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v5, p0, LX/DQb;->c:LX/15i;

    iget v6, p0, LX/DQb;->d:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v3, 0x6920893f

    invoke-static {v5, v6, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$DraculaImplementation;

    move-result-object v3

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1994879
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1994880
    iget v5, p0, LX/DQb;->a:I

    invoke-virtual {v0, v7, v5, v7}, LX/186;->a(III)V

    .line 1994881
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1994882
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1994883
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1994884
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1994885
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1994886
    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1994887
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1994888
    new-instance v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    invoke-direct {v1, v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;-><init>(LX/15i;)V

    .line 1994889
    return-object v1

    .line 1994890
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
