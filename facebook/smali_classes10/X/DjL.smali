.class public final LX/DjL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dj9;


# instance fields
.field public final synthetic a:Landroid/view/ViewStub;

.field public final synthetic b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;Landroid/view/ViewStub;)V
    .locals 0

    .prologue
    .line 2033125
    iput-object p1, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iput-object p2, p0, LX/DjL;->a:Landroid/view/ViewStub;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 5

    .prologue
    .line 2033080
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    .line 2033081
    iput-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->r:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    .line 2033082
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->k:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    if-nez v0, :cond_0

    .line 2033083
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    .line 2033084
    invoke-static {p1}, LX/DnS;->j(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    move-result-object v1

    .line 2033085
    if-nez v1, :cond_2

    .line 2033086
    new-instance v1, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    const-string v4, ""

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;-><init>(Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;Ljava/lang/String;)V

    .line 2033087
    :goto_0
    move-object v1, v1

    .line 2033088
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->k:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2033089
    :cond_0
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v1, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->k:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2033090
    iput-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    .line 2033091
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2033092
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2033093
    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->g:LX/0Uh;

    const/16 v4, 0x61e

    const/4 v1, 0x0

    invoke-virtual {v3, v4, v1}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2033094
    invoke-static {p1}, LX/DnS;->k(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2033095
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2033096
    :goto_1
    move-object v2, v2

    .line 2033097
    iput-object v2, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->b:LX/0Px;

    .line 2033098
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->m:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2033099
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    const v1, 0x7f082b9d

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;I)V

    .line 2033100
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->r:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->r()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 2033101
    iget-object v2, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2033102
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2033103
    iget-object v0, p0, LX/DjL;->a:Landroid/view/ViewStub;

    const v1, 0x7f030df4

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2033104
    iget-object v1, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v0, p0, LX/DjL;->a:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2033105
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->o:Lcom/facebook/resources/ui/FbButton;

    .line 2033106
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->o:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082ba1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2033107
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->o:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/DjK;

    invoke-direct {v1, p0}, LX/DjK;-><init>(LX/DjL;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2033108
    :cond_1
    return-void

    .line 2033109
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2033110
    invoke-virtual {v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2033111
    new-instance v1, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    new-instance v4, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    invoke-direct {v4, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->VIEW_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    invoke-direct {v1, v4, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;-><init>(Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2033112
    :cond_3
    sget-object v3, LX/DnJ;->APPOINTMENT_HEADER:LX/DnJ;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033113
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$ProductItemModel;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 2033114
    sget-object v3, LX/DnJ;->SERVICE_INFO_V2:LX/DnJ;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033115
    :cond_4
    sget-object v3, LX/DnJ;->SERVICE_DATE:LX/DnJ;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033116
    sget-object v3, LX/DnJ;->SERVICE_TIME:LX/DnJ;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033117
    invoke-static {p1}, LX/DnS;->c(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 2033118
    sget-object v3, LX/DnJ;->SERVICE_PRICE:LX/DnJ;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033119
    :cond_5
    sget-object v3, LX/DnJ;->APPOINTMENT_NOTE:LX/DnJ;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033120
    :goto_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_1

    .line 2033121
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$ProductItemModel;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 2033122
    sget-object v3, LX/DnJ;->SERVICE_INFO:LX/DnJ;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033123
    :cond_7
    sget-object v3, LX/DnJ;->APPOINTMENT_TIME:LX/DnJ;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2033124
    sget-object v3, LX/DnJ;->BOOKING_STATUS:LX/DnJ;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2033077
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    const v1, 0x7f08003a

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;I)V

    .line 2033078
    iget-object v0, p0, LX/DjL;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;

    const-string v1, "load_admin_appointment_detail"

    invoke-static {v0, v1, p1}, Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/PageAdminAppointmentDetailFragment;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2033079
    return-void
.end method
