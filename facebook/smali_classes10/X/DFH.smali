.class public LX/DFH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/intent/feed/IFeedIntentBuilder;


# direct methods
.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977974
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1977975
    iput-object p1, p0, LX/DFH;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1977976
    return-void
.end method

.method public static a(LX/0QB;)LX/DFH;
    .locals 4

    .prologue
    .line 1977977
    const-class v1, LX/DFH;

    monitor-enter v1

    .line 1977978
    :try_start_0
    sget-object v0, LX/DFH;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1977979
    sput-object v2, LX/DFH;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1977980
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977981
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1977982
    new-instance p0, LX/DFH;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v3

    check-cast v3, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-direct {p0, v3}, LX/DFH;-><init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;)V

    .line 1977983
    move-object v0, p0

    .line 1977984
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1977985
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977986
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1977987
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
