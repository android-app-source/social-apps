.class public LX/EA9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/03V;

.field private final b:LX/BNE;

.field private final c:Landroid/content/res/Resources;

.field public final d:LX/BNP;

.field public final e:LX/Ch5;

.field public final f:LX/EAH;

.field private final g:LX/0kL;


# direct methods
.method public constructor <init>(LX/03V;LX/BNE;Landroid/content/res/Resources;LX/BNP;LX/Ch5;LX/EAH;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2085311
    iput-object p1, p0, LX/EA9;->a:LX/03V;

    .line 2085312
    iput-object p2, p0, LX/EA9;->b:LX/BNE;

    .line 2085313
    iput-object p3, p0, LX/EA9;->c:Landroid/content/res/Resources;

    .line 2085314
    iput-object p4, p0, LX/EA9;->d:LX/BNP;

    .line 2085315
    iput-object p5, p0, LX/EA9;->e:LX/Ch5;

    .line 2085316
    iput-object p6, p0, LX/EA9;->f:LX/EAH;

    .line 2085317
    iput-object p7, p0, LX/EA9;->g:LX/0kL;

    .line 2085318
    return-void
.end method

.method public static a(LX/EA9;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/55r;)V
    .locals 16
    .param p1    # Landroid/app/Activity;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085319
    :try_start_0
    invoke-static/range {p3 .. p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    .line 2085320
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EA9;->d:LX/BNP;

    const/16 v3, 0x6df

    sget-object v5, LX/21D;->NEWSFEED:LX/21D;

    const-string v6, "review_feed"

    const-string v7, "page_see_all_reviews"

    invoke-static/range {p5 .. p5}, LX/BNJ;->a(LX/55r;)I

    move-result v9

    invoke-static/range {p5 .. p5}, LX/BNJ;->b(LX/55r;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p5 .. p5}, LX/BNJ;->c(LX/55r;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v4, p1

    move-object/from16 v8, p2

    move-object/from16 v12, p4

    invoke-virtual/range {v2 .. v15}, LX/BNP;->a(ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)V

    .line 2085321
    :goto_0
    return-void

    .line 2085322
    :catch_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EA9;->a:LX/03V;

    const-class v3, LX/EA9;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Trying to edit review with invalid page id. Page id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2085323
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EA9;->g:LX/0kL;

    new-instance v3, LX/27k;

    const v4, 0x7f081502

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v2, v3}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public static a$redex0(LX/EA9;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2085324
    iget-object v0, p0, LX/EA9;->e:LX/Ch5;

    .line 2085325
    new-instance v1, LX/ChF;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, v3}, LX/ChF;-><init>(ILjava/lang/String;LX/5tj;)V

    move-object v1, v1

    .line 2085326
    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2085327
    return-void
.end method

.method public static a$redex0(LX/EA9;Ljava/lang/String;Lcom/facebook/fbservice/service/OperationResult;LX/0am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/fbservice/service/OperationResult;",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2085328
    invoke-virtual {p2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5tj;

    .line 2085329
    invoke-static {p1, v0}, LX/ChH;->a(Ljava/lang/String;LX/5tj;)LX/ChF;

    move-result-object v0

    .line 2085330
    iput-object p3, v0, LX/ChF;->c:LX/0am;

    .line 2085331
    iget-object v1, p0, LX/EA9;->e:LX/Ch5;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    .line 2085332
    return-void
.end method

.method public static b(LX/0QB;)LX/EA9;
    .locals 8

    .prologue
    .line 2085333
    new-instance v0, LX/EA9;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/BNE;->a(LX/0QB;)LX/BNE;

    move-result-object v2

    check-cast v2, LX/BNE;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {p0}, LX/BNP;->a(LX/0QB;)LX/BNP;

    move-result-object v4

    check-cast v4, LX/BNP;

    invoke-static {p0}, LX/Ch5;->a(LX/0QB;)LX/Ch5;

    move-result-object v5

    check-cast v5, LX/Ch5;

    invoke-static {p0}, LX/EAH;->b(LX/0QB;)LX/EAH;

    move-result-object v6

    check-cast v6, LX/EAH;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-direct/range {v0 .. v7}, LX/EA9;-><init>(LX/03V;LX/BNE;Landroid/content/res/Resources;LX/BNP;LX/Ch5;LX/EAH;LX/0kL;)V

    .line 2085334
    return-object v0
.end method
