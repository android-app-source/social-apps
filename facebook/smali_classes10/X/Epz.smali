.class public final LX/Epz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V
    .locals 0

    .prologue
    .line 2170826
    iput-object p1, p0, LX/Epz;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2170827
    iget-object v0, p0, LX/Epz;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->x:LX/03V;

    sget-object v1, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->G:Ljava/lang/String;

    const-string v2, "Failed to fetch group invitees"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2170828
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2170829
    move-object v1, v1

    .line 2170830
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2170831
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170832
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;

    .line 2170833
    iget-object v0, p0, LX/Epz;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170834
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->n()Z

    move-result v1

    move v0, v1

    .line 2170835
    if-eqz v0, :cond_1

    .line 2170836
    :cond_0
    return-void

    .line 2170837
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2170838
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel$EdgesModel;

    .line 2170839
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel$EdgesModel$NodeModel;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2170840
    iget-object v4, p0, LX/Epz;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v4, v4, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$GroupEventFriendInviteCandidatesQueryModel$GroupMembersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2170841
    iget-object v0, p0, LX/Epz;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    const/4 v4, 0x1

    invoke-static {v0, v4}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->c(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;Z)V

    .line 2170842
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
