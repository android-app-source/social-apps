.class public final LX/DyX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Lcom/facebook/places/checkin/PlacePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V
    .locals 0

    .prologue
    .line 2064684
    iput-object p1, p0, LX/DyX;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 2064685
    iget-object v0, p0, LX/DyX;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2064686
    const/4 v2, 0x0

    .line 2064687
    if-nez p1, :cond_1

    .line 2064688
    :cond_0
    :goto_0
    move-object v2, v2

    .line 2064689
    move-object v1, v2

    .line 2064690
    invoke-static {v0, v1}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/Dyr;)V

    .line 2064691
    const/4 v0, 0x1

    return v0

    .line 2064692
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p0

    const v1, 0x7f0d325b

    if-ne p0, v1, :cond_2

    .line 2064693
    sget-object v2, LX/Dyr;->SUGGEST_EDITS:LX/Dyr;

    goto :goto_0

    .line 2064694
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p0

    const v1, 0x7f0d325c

    if-ne p0, v1, :cond_3

    .line 2064695
    sget-object v2, LX/Dyr;->REPORT_DUPLICATES:LX/Dyr;

    goto :goto_0

    .line 2064696
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p0

    const v1, 0x7f0d325d

    if-ne p0, v1, :cond_4

    .line 2064697
    sget-object v2, LX/Dyr;->INAPPROPRIATE_CONTENT:LX/Dyr;

    goto :goto_0

    .line 2064698
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p0

    const v1, 0x7f0d325e

    if-ne p0, v1, :cond_0

    .line 2064699
    sget-object v2, LX/Dyr;->NOT_A_PUBLIC_PLACE:LX/Dyr;

    goto :goto_0
.end method
