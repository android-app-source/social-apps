.class public final enum LX/EID;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EID;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EID;

.field public static final enum CENTER_CROP:LX/EID;

.field public static final enum CENTER_CROP_THRESHOLD:LX/EID;

.field public static final enum DYNAMIC_WIDTH:LX/EID;

.field public static final enum FIT:LX/EID;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2101219
    new-instance v0, LX/EID;

    const-string v1, "FIT"

    invoke-direct {v0, v1, v2}, LX/EID;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EID;->FIT:LX/EID;

    .line 2101220
    new-instance v0, LX/EID;

    const-string v1, "CENTER_CROP"

    invoke-direct {v0, v1, v3}, LX/EID;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EID;->CENTER_CROP:LX/EID;

    .line 2101221
    new-instance v0, LX/EID;

    const-string v1, "CENTER_CROP_THRESHOLD"

    invoke-direct {v0, v1, v4}, LX/EID;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EID;->CENTER_CROP_THRESHOLD:LX/EID;

    .line 2101222
    new-instance v0, LX/EID;

    const-string v1, "DYNAMIC_WIDTH"

    invoke-direct {v0, v1, v5}, LX/EID;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EID;->DYNAMIC_WIDTH:LX/EID;

    .line 2101223
    const/4 v0, 0x4

    new-array v0, v0, [LX/EID;

    sget-object v1, LX/EID;->FIT:LX/EID;

    aput-object v1, v0, v2

    sget-object v1, LX/EID;->CENTER_CROP:LX/EID;

    aput-object v1, v0, v3

    sget-object v1, LX/EID;->CENTER_CROP_THRESHOLD:LX/EID;

    aput-object v1, v0, v4

    sget-object v1, LX/EID;->DYNAMIC_WIDTH:LX/EID;

    aput-object v1, v0, v5

    sput-object v0, LX/EID;->$VALUES:[LX/EID;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2101218
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EID;
    .locals 1

    .prologue
    .line 2101217
    const-class v0, LX/EID;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EID;

    return-object v0
.end method

.method public static values()[LX/EID;
    .locals 1

    .prologue
    .line 2101216
    sget-object v0, LX/EID;->$VALUES:[LX/EID;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EID;

    return-object v0
.end method
