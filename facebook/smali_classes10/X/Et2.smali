.class public final LX/Et2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

.field public final synthetic c:LX/1Pm;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/Et3;


# direct methods
.method public constructor <init>(LX/Et3;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;LX/1Pm;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2176998
    iput-object p1, p0, LX/Et2;->f:LX/Et3;

    iput-object p2, p0, LX/Et2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Et2;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object p4, p0, LX/Et2;->c:LX/1Pm;

    iput-object p5, p0, LX/Et2;->d:Ljava/lang/String;

    iput-object p6, p0, LX/Et2;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x118711a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2176999
    iget-object v0, p0, LX/Et2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2177000
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2177001
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/Esx;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/CEz;->PERMALINK:LX/CEz;

    move-object v1, v0

    .line 2177002
    :goto_0
    iget-object v0, p0, LX/Et2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2177003
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2177004
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/Et3;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v3

    .line 2177005
    iget-object v0, p0, LX/Et2;->f:LX/Et3;

    iget-object v4, v0, LX/Et3;->j:LX/1Cn;

    iget-object v0, p0, LX/Et2;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, LX/Et2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2177006
    iget-object v6, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v6

    .line 2177007
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/Et3;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0, v1}, LX/1Cn;->b(Ljava/lang/String;Ljava/lang/String;LX/CEz;)V

    .line 2177008
    new-instance v0, LX/6WS;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2177009
    const v4, 0x7f0314b7

    invoke-virtual {v0, v4}, LX/5OM;->b(I)V

    .line 2177010
    new-instance v4, LX/Et1;

    invoke-direct {v4, p0, p1, v3, v1}, LX/Et1;-><init>(LX/Et2;Landroid/view/View;Ljava/lang/String;LX/CEz;)V

    .line 2177011
    iput-object v4, v0, LX/5OM;->p:LX/5OO;

    .line 2177012
    invoke-virtual {v0, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2177013
    const v0, -0x526f01a2

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 2177014
    :cond_0
    sget-object v0, LX/CEz;->PROMOTION:LX/CEz;

    move-object v1, v0

    goto :goto_0
.end method
