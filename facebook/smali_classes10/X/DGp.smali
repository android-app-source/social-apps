.class public LX/DGp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1qb;

.field private final b:LX/17R;


# direct methods
.method public constructor <init>(LX/17R;LX/1qb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1980731
    iput-object p1, p0, LX/DGp;->b:LX/17R;

    .line 1980732
    iput-object p2, p0, LX/DGp;->a:LX/1qb;

    .line 1980733
    return-void
.end method

.method public static a(LX/0QB;)LX/DGp;
    .locals 5

    .prologue
    .line 1980734
    const-class v1, LX/DGp;

    monitor-enter v1

    .line 1980735
    :try_start_0
    sget-object v0, LX/DGp;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980736
    sput-object v2, LX/DGp;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980737
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980738
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980739
    new-instance p0, LX/DGp;

    invoke-static {v0}, LX/17R;->a(LX/0QB;)LX/17R;

    move-result-object v3

    check-cast v3, LX/17R;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v4

    check-cast v4, LX/1qb;

    invoke-direct {p0, v3, v4}, LX/DGp;-><init>(LX/17R;LX/1qb;)V

    .line 1980740
    move-object v0, p0

    .line 1980741
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980742
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DGp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980743
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980744
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)LX/DGo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;",
            ")",
            "LX/DGo;"
        }
    .end annotation

    .prologue
    .line 1980745
    if-eqz p1, :cond_0

    .line 1980746
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980747
    if-nez v0, :cond_1

    .line 1980748
    :cond_0
    sget-object v0, LX/DGo;->d:LX/DGo;

    .line 1980749
    :goto_0
    return-object v0

    .line 1980750
    :cond_1
    new-instance v1, LX/DGo;

    invoke-direct {v1}, LX/DGo;-><init>()V

    .line 1980751
    const/4 v2, 0x0

    .line 1980752
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980753
    if-nez v0, :cond_8

    move-object v0, v2

    .line 1980754
    :cond_2
    :goto_1
    move-object v2, v0

    .line 1980755
    const-string v0, ""

    .line 1980756
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    if-eq p2, v3, :cond_3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    if-ne p2, v3, :cond_5

    .line 1980757
    :cond_3
    const/4 p0, 0x0

    .line 1980758
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980759
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1980760
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 1980761
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1980762
    :goto_2
    move-object v0, v0

    .line 1980763
    :cond_4
    :goto_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1980764
    iput-object v0, v1, LX/DGo;->a:Ljava/lang/CharSequence;

    .line 1980765
    :goto_4
    move-object v0, v1

    .line 1980766
    goto :goto_0

    .line 1980767
    :cond_5
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LINK_ONLY_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    if-ne p2, v3, :cond_4

    .line 1980768
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980769
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v0

    .line 1980770
    if-nez v0, :cond_6

    const-string v0, ""

    :cond_6
    move-object v0, v0

    .line 1980771
    goto :goto_3

    .line 1980772
    :cond_7
    iput-object v2, v1, LX/DGo;->a:Ljava/lang/CharSequence;

    .line 1980773
    iput-object v0, v1, LX/DGo;->b:Ljava/lang/CharSequence;

    .line 1980774
    goto :goto_4

    .line 1980775
    :cond_8
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980776
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/17R;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 1980777
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1980778
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    if-nez v3, :cond_9

    move-object v0, v2

    .line 1980779
    goto/16 :goto_1

    .line 1980780
    :cond_9
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-static {v2}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1980781
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1980782
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_a
    const-string v0, ""

    goto :goto_2
.end method
