.class public LX/DMZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Landroid/content/res/Resources;

.field private final d:LX/0tX;

.field public final e:LX/0kL;

.field public f:LX/DMR;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0kL;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1989936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1989937
    iput-object p1, p0, LX/DMZ;->c:Landroid/content/res/Resources;

    .line 1989938
    iput-object p2, p0, LX/DMZ;->a:Ljava/lang/String;

    .line 1989939
    iput-object p3, p0, LX/DMZ;->b:Ljava/util/concurrent/ExecutorService;

    .line 1989940
    iput-object p4, p0, LX/DMZ;->d:LX/0tX;

    .line 1989941
    iput-object p5, p0, LX/DMZ;->e:LX/0kL;

    .line 1989942
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/facebook/graphql/calls/EventGuestStatusMutationValue;
    .end annotation

    .prologue
    .line 1989943
    sget-object v0, LX/DMY;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1989944
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported guest status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1989945
    :pswitch_0
    const-string v0, "going"

    .line 1989946
    :goto_0
    return-object v0

    .line 1989947
    :pswitch_1
    const-string v0, "maybe"

    goto :goto_0

    .line 1989948
    :pswitch_2
    const-string v0, "not_going"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/facebook/graphql/calls/EventWatchStatus;
    .end annotation

    .prologue
    .line 1989949
    sget-object v0, LX/DMY;->b:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1989950
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported event watch status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1989951
    :pswitch_0
    const-string v0, "GOING"

    .line 1989952
    :goto_0
    return-object v0

    .line 1989953
    :pswitch_1
    const-string v0, "WATCHED"

    goto :goto_0

    .line 1989954
    :pswitch_2
    const-string v0, "DECLINED"

    goto :goto_0

    .line 1989955
    :pswitch_3
    const-string v0, "UNWATCHED"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/DMZ;
    .locals 6

    .prologue
    .line 1989956
    new-instance v0, LX/DMZ;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-direct/range {v0 .. v5}, LX/DMZ;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0kL;)V

    .line 1989957
    return-object v0
.end method


# virtual methods
.method public final a(LX/7vF;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1989958
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    const-string v1, "group_events"

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    .line 1989959
    new-instance v1, LX/4EL;

    invoke-direct {v1}, LX/4EL;-><init>()V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    move-result-object v0

    .line 1989960
    new-instance v1, LX/4ES;

    invoke-direct {v1}, LX/4ES;-><init>()V

    iget-object v2, p0, LX/DMZ;->a:Ljava/lang/String;

    .line 1989961
    const-string v3, "actor_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1989962
    move-object v1, v1

    .line 1989963
    invoke-virtual {v1, v0}, LX/4ES;->a(LX/4EL;)LX/4ES;

    move-result-object v0

    invoke-virtual {p1}, LX/7vF;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ES;->b(Ljava/lang/String;)LX/4ES;

    move-result-object v0

    invoke-static {p2}, LX/DMZ;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ES;->c(Ljava/lang/String;)LX/4ES;

    move-result-object v0

    .line 1989964
    new-instance v1, LX/7uI;

    invoke-direct {v1}, LX/7uI;-><init>()V

    .line 1989965
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1989966
    iget-object v0, p0, LX/DMZ;->d:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1989967
    new-instance v1, LX/DMW;

    invoke-direct {v1, p0, p1}, LX/DMW;-><init>(LX/DMZ;LX/7vF;)V

    iget-object v2, p0, LX/DMZ;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1989968
    return-void
.end method

.method public final a(LX/7vF;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1989969
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    const-string v1, "group_events"

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v0

    .line 1989970
    new-instance v1, LX/4EL;

    invoke-direct {v1}, LX/4EL;-><init>()V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    move-result-object v0

    .line 1989971
    new-instance v1, LX/4Ed;

    invoke-direct {v1}, LX/4Ed;-><init>()V

    iget-object v2, p0, LX/DMZ;->a:Ljava/lang/String;

    .line 1989972
    const-string v3, "actor_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1989973
    move-object v1, v1

    .line 1989974
    invoke-virtual {v1, v0}, LX/4Ed;->a(LX/4EL;)LX/4Ed;

    move-result-object v0

    invoke-virtual {p1}, LX/7vF;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Ed;->b(Ljava/lang/String;)LX/4Ed;

    move-result-object v0

    invoke-static {p2}, LX/DMZ;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Ed;->c(Ljava/lang/String;)LX/4Ed;

    move-result-object v0

    .line 1989975
    new-instance v1, LX/7uQ;

    invoke-direct {v1}, LX/7uQ;-><init>()V

    .line 1989976
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1989977
    iget-object v0, p0, LX/DMZ;->d:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1989978
    new-instance v1, LX/DMX;

    invoke-direct {v1, p0, p1}, LX/DMX;-><init>(LX/DMZ;LX/7vF;)V

    iget-object v2, p0, LX/DMZ;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1989979
    return-void
.end method

.method public final b(LX/DMR;)V
    .locals 1

    .prologue
    .line 1989980
    iget-object v0, p0, LX/DMZ;->f:LX/DMR;

    if-ne v0, p1, :cond_0

    .line 1989981
    const/4 v0, 0x0

    iput-object v0, p0, LX/DMZ;->f:LX/DMR;

    .line 1989982
    :cond_0
    return-void
.end method
