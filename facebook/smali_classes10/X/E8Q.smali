.class public LX/E8Q;
.super LX/E8N;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/E8N",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel;",
        "LX/E39;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/E39;

.field private final c:LX/E6V;

.field private final d:Lcom/facebook/reaction/ReactionUtil;

.field private final e:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/E6V;LX/2jY;Ljava/lang/String;Ljava/lang/String;LX/0Sh;LX/E39;LX/2j3;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/E6V;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2082445
    iget-object v0, p3, LX/2jY;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2082446
    iget-object v1, p3, LX/2jY;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2082447
    invoke-direct {p0, p6, p8, v0, v1}, LX/E8N;-><init>(LX/0Sh;LX/2j3;Ljava/lang/String;Ljava/lang/String;)V

    .line 2082448
    iput-object p1, p0, LX/E8Q;->a:Ljava/lang/String;

    .line 2082449
    iput-object p7, p0, LX/E8Q;->b:LX/E39;

    .line 2082450
    iput-object p2, p0, LX/E8Q;->c:LX/E6V;

    .line 2082451
    iput-object p9, p0, LX/E8Q;->d:Lcom/facebook/reaction/ReactionUtil;

    .line 2082452
    iget-object v0, p3, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2082453
    iput-object v0, p0, LX/E8Q;->e:Ljava/lang/String;

    .line 2082454
    iput-object p4, p0, LX/E8Q;->f:Ljava/lang/String;

    .line 2082455
    iput-object p5, p0, LX/E8Q;->g:Ljava/lang/String;

    .line 2082456
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0us;
    .locals 1

    .prologue
    .line 2082422
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel;

    .line 2082423
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel$ReactionPaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel$ReactionPaginatedComponentsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/E39;
    .locals 1

    .prologue
    .line 2082444
    iget-object v0, p0, LX/E8Q;->b:LX/E39;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0Ve;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2082434
    iget-object v0, p0, LX/E8Q;->d:Lcom/facebook/reaction/ReactionUtil;

    const/16 v3, 0xa

    iget-object v4, p0, LX/E8Q;->a:Ljava/lang/String;

    iget-object v5, p0, LX/E8Q;->e:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    .line 2082435
    new-instance p0, LX/9qO;

    invoke-direct {p0}, LX/9qO;-><init>()V

    move-object p0, p0

    .line 2082436
    const-string p1, "reaction_component_id"

    invoke-virtual {p0, p1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p0

    const-string p1, "reaction_after_cursor"

    invoke-virtual {p0, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string p2, "automatic_photo_captioning_enabled"

    iget-object p0, v0, Lcom/facebook/reaction/ReactionUtil;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0sX;

    invoke-virtual {p0}, LX/0sX;->a()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p2, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p0

    const-string p1, "reaction_result_count"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p0

    const-string p1, "enable_download"

    iget-object p2, v0, Lcom/facebook/reaction/ReactionUtil;->G:LX/0tQ;

    invoke-virtual {p2}, LX/0tQ;->c()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p0

    check-cast p0, LX/9qO;

    .line 2082437
    invoke-virtual {v0, p0, v5}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 2082438
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    .line 2082439
    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 2082440
    iput-object v5, p0, LX/0zO;->z:Ljava/lang/String;

    .line 2082441
    :cond_0
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    .line 2082442
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    invoke-virtual {p1, v4, p0, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2082443
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2082424
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel;

    .line 2082425
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel$ReactionPaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel$ReactionPaginatedComponentsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 2082426
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel$ReactionPaginatedComponentsModel$EdgesModel;

    .line 2082427
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreComponentsResultModel$ReactionPaginatedComponentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v0

    .line 2082428
    if-eqz v0, :cond_0

    .line 2082429
    iget-object v4, p0, LX/E8Q;->b:LX/E39;

    new-instance v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v6, p0, LX/E8Q;->f:Ljava/lang/String;

    iget-object v7, p0, LX/E8Q;->g:Ljava/lang/String;

    invoke-direct {v5, v0, v6, v7}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2082430
    iget-object v0, v4, LX/E39;->a:Ljava/util/List;

    iget-object v6, v4, LX/E39;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {v0, v6, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2082431
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2082432
    :cond_1
    iget-object v0, p0, LX/E8Q;->c:LX/E6V;

    invoke-interface {v0}, LX/E6V;->d()V

    .line 2082433
    return-void
.end method
