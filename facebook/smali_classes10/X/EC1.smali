.class public final LX/EC1;
.super LX/EC0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 0

    .prologue
    .line 2088059
    iput-object p1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-direct {p0}, LX/EC0;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2088060
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    new-instance v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$1;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$1;-><init>(LX/EC1;)V

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2088061
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2088062
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aI:Lcom/facebook/rtc/views/RtcLevelTileView;

    if-eqz v0, :cond_0

    .line 2088063
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aI:Lcom/facebook/rtc/views/RtcLevelTileView;

    invoke-virtual {v0, p1}, Lcom/facebook/rtc/views/RtcLevelTileView;->a(I)V

    .line 2088064
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 2088055
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    new-instance v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$6;-><init>(LX/EC1;II)V

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2088056
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2088065
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088066
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    .line 2088067
    invoke-static {v0}, LX/ED6;->h(LX/ED6;)V

    .line 2088068
    iget-object v1, v0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    .line 2088069
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->b:LX/EDx;

    if-nez v2, :cond_2

    .line 2088070
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_1

    .line 2088071
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2088072
    iget-object v1, v0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    if-nez v1, :cond_a

    .line 2088073
    :cond_1
    return-void

    .line 2088074
    :cond_2
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    .line 2088075
    if-nez p1, :cond_4

    .line 2088076
    :cond_3
    :goto_1
    goto :goto_0

    .line 2088077
    :cond_4
    const/4 v4, 0x1

    .line 2088078
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 2088079
    iget-object v0, v2, LX/EEp;->g:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v2, LX/EEp;->g:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-eq v0, v3, :cond_5

    :cond_6
    move v3, v4

    .line 2088080
    :goto_2
    move v3, v3

    .line 2088081
    if-eqz v3, :cond_3

    .line 2088082
    const v3, -0x42d9fc8

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2088083
    iput-object p1, v2, LX/EEp;->g:Ljava/util/Map;

    goto :goto_1

    .line 2088084
    :cond_7
    iget-object v3, v2, LX/EEp;->g:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2088085
    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v3, v4

    .line 2088086
    goto :goto_2

    .line 2088087
    :cond_9
    const/4 v3, 0x0

    goto :goto_2

    .line 2088088
    :cond_a
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2088089
    iget-object v4, v0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Lcom/facebook/rtc/views/RtcGridView;->b(Ljava/lang/String;)LX/EIC;

    move-result-object v2

    .line 2088090
    if-eqz v2, :cond_b

    .line 2088091
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2088092
    iget-object v4, v2, LX/EIC;->k:Lcom/facebook/rtc/views/RtcLevelTileView;

    invoke-virtual {v4, v1}, Lcom/facebook/rtc/views/RtcLevelTileView;->a(I)V

    .line 2088093
    goto :goto_3
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2088094
    if-eqz p1, :cond_1

    .line 2088095
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088096
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->P(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088097
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EDx;->k(Z)V

    .line 2088098
    :goto_0
    return-void

    .line 2088099
    :cond_0
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0

    .line 2088100
    :cond_1
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088101
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 6

    .prologue
    .line 2088102
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->setVolumeControlStream(I)V

    .line 2088103
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088104
    iget-boolean v1, v0, LX/EDx;->bx:Z

    move v0, v1

    .line 2088105
    if-eqz v0, :cond_0

    .line 2088106
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-virtual {v0, v1}, LX/EDx;->b(Landroid/content/Context;)V

    .line 2088107
    :cond_0
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    const v2, 0x7f080735

    invoke-virtual {v1, v2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088108
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    if-eqz v0, :cond_1

    .line 2088109
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    .line 2088110
    iget-object v1, v0, LX/EIY;->g:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2088111
    :cond_1
    if-eqz p1, :cond_5

    .line 2088112
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aX()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2088113
    if-eqz p2, :cond_2

    .line 2088114
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088115
    :cond_2
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088116
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_3

    .line 2088117
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->e()V

    .line 2088118
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->q:LX/00H;

    .line 2088119
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 2088120
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aV()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aa()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ap:Ljava/util/concurrent/Future;

    if-nez v0, :cond_4

    .line 2088121
    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$5;

    invoke-direct {v2, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$5;-><init>(LX/EC1;)V

    const-wide/16 v4, 0x1f40

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v4, v5, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    .line 2088122
    iput-object v0, v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ap:Ljava/util/concurrent/Future;

    .line 2088123
    :cond_3
    :goto_0
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ak(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088124
    return-void

    .line 2088125
    :cond_4
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->x()V

    goto :goto_0

    .line 2088126
    :cond_5
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->y$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0
.end method

.method public final a([B)V
    .locals 1

    .prologue
    .line 2088127
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->az:Lcom/facebook/rtc/views/RtcSnakeView;

    if-eqz v0, :cond_0

    .line 2088128
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->az:Lcom/facebook/rtc/views/RtcSnakeView;

    invoke-virtual {v0, p1}, Lcom/facebook/rtc/views/RtcSnakeView;->a([B)V

    .line 2088129
    :cond_0
    return-void
.end method

.method public final a(IJZZZ)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 2088130
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    .line 2088131
    iput-wide p2, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aZ:J

    .line 2088132
    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    .line 2088133
    iput-boolean v0, v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->S:Z

    .line 2088134
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0, v5}, Lcom/facebook/rtc/views/RtcActionBar;->a(Ljava/lang/String;)V

    .line 2088135
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->d()V

    .line 2088136
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EFj;

    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v1, v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 2088137
    iget-object v4, v1, LX/EDx;->bl:LX/7TQ;

    move-object v1, v4

    .line 2088138
    sget-object v4, LX/EFi;->Activity:LX/EFi;

    invoke-virtual {v0, v1, v4}, LX/EFj;->a(LX/7TQ;LX/EFi;)Ljava/lang/String;

    move-result-object v0

    .line 2088139
    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v1, v0, v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;Z)V

    .line 2088140
    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v1, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088141
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    sget-object v1, LX/EC8;->INCOMING_CALL:LX/EC8;

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-eqz v0, :cond_1

    .line 2088142
    :cond_0
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0, v2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088143
    :cond_1
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2088144
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->I$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088145
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->T:Z

    if-eqz v0, :cond_2

    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088146
    iget-object v4, v0, LX/EDx;->bl:LX/7TQ;

    move-object v0, v4

    .line 2088147
    invoke-static {v1, v0, p5}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/7TQ;Z)Z

    move-result v0

    if-nez v0, :cond_c

    :cond_2
    move v1, v3

    .line 2088148
    :goto_0
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ak()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2088149
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ac(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088150
    :cond_3
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aH(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088151
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    if-eqz v0, :cond_4

    .line 2088152
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    invoke-virtual {v0, v5}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 2088153
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    .line 2088154
    iput-object v5, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    .line 2088155
    :cond_4
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_5

    .line 2088156
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0, v5}, LX/EIU;->a(Ljava/lang/String;)V

    .line 2088157
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aR:Landroid/widget/FrameLayout;

    iget-object v4, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v4, v4, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 2088158
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->E()V

    .line 2088159
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    .line 2088160
    iput-object v5, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2088161
    :cond_5
    if-eqz v1, :cond_d

    .line 2088162
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    invoke-virtual {v0}, LX/2S7;->g()V

    .line 2088163
    :goto_1
    return v3

    .line 2088164
    :cond_6
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_b

    .line 2088165
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aG()Z

    move-result v0

    if-nez v0, :cond_b

    .line 2088166
    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088167
    iget-object v4, v0, LX/EDx;->bl:LX/7TQ;

    move-object v0, v4

    .line 2088168
    const/4 v6, 0x0

    .line 2088169
    sget-object v4, LX/EBu;->b:[I

    invoke-virtual {v0}, LX/7TQ;->ordinal()I

    move-result p1

    aget v4, v4, p1

    packed-switch v4, :pswitch_data_0

    move v4, v6

    .line 2088170
    :goto_2
    move v0, v4

    .line 2088171
    if-eqz v0, :cond_7

    .line 2088172
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088173
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0, p2, p3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;J)V

    move v1, v3

    .line 2088174
    goto/16 :goto_0

    .line 2088175
    :cond_7
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2088176
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    move v1, v2

    goto/16 :goto_0

    .line 2088177
    :cond_8
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ag()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2088178
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088179
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->I$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088180
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->T:Z

    if-eqz v0, :cond_9

    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088181
    iget-object v4, v0, LX/EDx;->bl:LX/7TQ;

    move-object v0, v4

    .line 2088182
    invoke-static {v1, v0, p5}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/7TQ;Z)Z

    move-result v0

    if-nez v0, :cond_c

    :cond_9
    move v1, v3

    .line 2088183
    goto/16 :goto_0

    .line 2088184
    :cond_a
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->I$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    move v1, v3

    goto/16 :goto_0

    .line 2088185
    :cond_b
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-virtual {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    :cond_c
    move v1, v2

    goto/16 :goto_0

    :cond_d
    move v3, v2

    .line 2088186
    goto/16 :goto_1

    .line 2088187
    :pswitch_0
    iget-object v4, v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->y:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ka;

    invoke-virtual {v4}, LX/0ka;->c()Landroid/net/NetworkInfo;

    move-result-object v4

    .line 2088188
    if-eqz v4, :cond_e

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v4

    if-eqz v4, :cond_e

    const/4 v4, 0x1

    goto/16 :goto_2

    :cond_e
    move v4, v6

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2088189
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088190
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2088191
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0, p1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Lcom/facebook/rtc/activities/WebrtcIncallActivity;I)V

    .line 2088192
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2088193
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    .line 2088194
    iput-boolean v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    .line 2088195
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->T:Z

    if-nez v0, :cond_0

    .line 2088196
    :goto_0
    return-void

    .line 2088197
    :cond_0
    if-eqz p1, :cond_1

    .line 2088198
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0

    .line 2088199
    :cond_1
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2088200
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088201
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2088202
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    new-instance v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$2;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$2;-><init>(LX/EC1;)V

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2088203
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2088057
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    new-instance v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$3;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$3;-><init>(LX/EC1;)V

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2088058
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2087944
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    new-instance v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$4;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$5$4;-><init>(LX/EC1;)V

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2087945
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2087946
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    .line 2087947
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2087948
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aM:Lcom/facebook/rtc/views/VoipConnectionBanner;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Lcom/facebook/rtc/views/VoipConnectionBanner;->setVisibility(I)V

    .line 2087949
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aM:Lcom/facebook/rtc/views/VoipConnectionBanner;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a()V

    .line 2087950
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v1, :cond_0

    .line 2087951
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2087952
    iget-object p0, v1, LX/EIU;->H:Lcom/facebook/rtc/views/VoipConnectionBanner;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a()V

    .line 2087953
    invoke-static {v1}, LX/EIU;->N(LX/EIU;)V

    .line 2087954
    :cond_0
    :goto_0
    return-void

    .line 2087955
    :cond_1
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aM:Lcom/facebook/rtc/views/VoipConnectionBanner;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Lcom/facebook/rtc/views/VoipConnectionBanner;->setVisibility(I)V

    .line 2087956
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/RtcActionBar;->h()V

    .line 2087957
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v1, :cond_0

    .line 2087958
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v1}, LX/EIU;->s()V

    goto :goto_0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 2087959
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    sget-object v1, LX/EC8;->VIDEO_REQUEST:LX/EC8;

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-nez v0, :cond_0

    .line 2087960
    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2087961
    :cond_0
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2087962
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/rtc/views/RtcActionBar;->a(Ljava/lang/String;)V

    .line 2087963
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_1

    .line 2087964
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/EIU;->a(Ljava/lang/String;)V

    .line 2087965
    :cond_1
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    .line 2087966
    iget v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aT:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aT:I

    move v0, v1

    .line 2087967
    rem-int/lit8 v0, v0, 0x3

    if-lez v0, :cond_3

    .line 2087968
    :cond_2
    :goto_0
    return-void

    .line 2087969
    :cond_3
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->C()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->D()Z

    move-result v0

    iget-object v1, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v1, v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->p:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v1

    if-eq v0, v1, :cond_4

    .line 2087970
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->J$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2087971
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aD()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2087972
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Speaker state inconsistent: turning "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->D()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "on"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2S7;->a(Ljava/lang/String;)V

    .line 2087973
    :cond_4
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2087974
    iget-object v1, v0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v1

    move v0, v1

    .line 2087975
    if-eqz v0, :cond_2

    .line 2087976
    invoke-virtual {p0}, LX/EC1;->q()V

    .line 2087977
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aD()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2087978
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Mute state inconsistent: turning "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->A()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "on"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2S7;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2087979
    :cond_5
    const-string v0, "off"

    goto :goto_1

    .line 2087980
    :cond_6
    const-string v0, "off"

    goto :goto_2
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 2087981
    invoke-virtual {p0}, LX/EC1;->q()V

    .line 2087982
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2087983
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2087984
    iget v1, v0, LX/EDx;->am:I

    move v0, v1

    .line 2087985
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aX()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2087986
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->T:Z

    if-eqz v0, :cond_1

    .line 2087987
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2087988
    :cond_0
    :goto_0
    return-void

    .line 2087989
    :cond_1
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2087990
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2087991
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EDx;->o(Z)Z

    goto :goto_0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2087992
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2087993
    iget v1, v0, LX/EDx;->am:I

    move v0, v1

    .line 2087994
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 2087995
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ax(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2087996
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2087997
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->T:Z

    if-eqz v0, :cond_0

    .line 2087998
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/03R;->YES:LX/03R;

    .line 2087999
    iput-object v1, v0, LX/EDx;->bv:LX/03R;

    .line 2088000
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088001
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2088002
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->T:Z

    if-nez v0, :cond_1

    .line 2088003
    :cond_0
    :goto_0
    return-void

    .line 2088004
    :cond_1
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    if-eqz v0, :cond_2

    .line 2088005
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    .line 2088006
    iput-boolean v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    .line 2088007
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    goto :goto_0

    .line 2088008
    :cond_2
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    sget-object v1, LX/EC8;->VIDEO_REQUEST:LX/EC8;

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088009
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 2088010
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    const/4 p0, 0x1

    .line 2088011
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 2088012
    iget v2, v1, LX/EDx;->am:I

    move v1, v2

    .line 2088013
    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 2088014
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2088015
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->f(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088016
    :cond_0
    :goto_0
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v1, :cond_1

    .line 2088017
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v1}, LX/EIU;->e()V

    .line 2088018
    :cond_1
    return-void

    .line 2088019
    :cond_2
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aS()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2088020
    iget-object v1, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aQ()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2088021
    const v1, 0x7f08072d

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088022
    const v1, 0x7f08072d

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2088023
    :cond_3
    const v1, 0x7f08072b

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088024
    const v1, 0x7f08072b

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2088025
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2088026
    :goto_0
    return-void

    .line 2088027
    :cond_0
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->N()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-nez v0, :cond_1

    .line 2088028
    invoke-virtual {p0}, LX/EC1;->k()V

    .line 2088029
    :cond_1
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-nez v0, :cond_2

    .line 2088030
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088031
    :goto_1
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->o$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088032
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->n(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0

    .line 2088033
    :cond_2
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->p(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_1
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 2088034
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088035
    return-void
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 2088036
    const/4 v0, 0x1

    return v0
.end method

.method public final s()V
    .locals 2

    .prologue
    .line 2088037
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-boolean v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-eqz v0, :cond_1

    .line 2088038
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2088039
    iget-object v1, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->j()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2088040
    :cond_0
    :goto_0
    return-void

    .line 2088041
    :cond_1
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->br()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088042
    invoke-virtual {p0}, LX/EC1;->k()V

    goto :goto_0

    .line 2088043
    :cond_2
    iget-object v1, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->bj()LX/0Px;

    move-result-object v1

    invoke-static {v0}, LX/EIU;->getConferenceCall(LX/EIU;)LX/EFy;

    move-result-object p0

    invoke-static {v0, v1, p0}, LX/EIU;->a(LX/EIU;LX/0Px;LX/EFy;)V

    goto :goto_0
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 2088044
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    if-eqz v0, :cond_0

    .line 2088045
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2088046
    :cond_0
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_1

    .line 2088047
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->s()V

    .line 2088048
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->C(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2088049
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->q()V

    .line 2088050
    :cond_1
    return-void
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 2088051
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->p(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088052
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->o$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088053
    iget-object v0, p0, LX/EC1;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->n(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088054
    return-void
.end method
