.class public final LX/DxB;
.super Landroid/database/DataSetObserver;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)V
    .locals 0

    .prologue
    .line 2062456
    iput-object p1, p0, LX/DxB;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 2062450
    iget-object v0, p0, LX/DxB;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v3, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->s:Landroid/view/View;

    iget-object v0, p0, LX/DxB;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    invoke-static {v0}, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2062451
    iget-object v0, p0, LX/DxB;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->r:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->setVisibility(I)V

    .line 2062452
    iget-object v0, p0, LX/DxB;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->q:LX/DxD;

    iget-object v3, p0, LX/DxB;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    invoke-static {v3}, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, LX/DxD;->setVisibility(I)V

    .line 2062453
    return-void

    :cond_0
    move v0, v2

    .line 2062454
    goto :goto_0

    :cond_1
    move v2, v1

    .line 2062455
    goto :goto_1
.end method
