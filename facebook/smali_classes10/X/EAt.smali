.class public final LX/EAt;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0am;

.field public final synthetic b:LX/EA4;

.field public final synthetic c:LX/EA4;

.field public final synthetic d:LX/EB0;


# direct methods
.method public constructor <init>(LX/EB0;LX/0am;LX/EA4;LX/EA4;)V
    .locals 0

    .prologue
    .line 2085992
    iput-object p1, p0, LX/EAt;->d:LX/EB0;

    iput-object p2, p0, LX/EAt;->a:LX/0am;

    iput-object p3, p0, LX/EAt;->b:LX/EA4;

    iput-object p4, p0, LX/EAt;->c:LX/EA4;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2085968
    iget-object v0, p0, LX/EAt;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    .line 2085969
    iget-object v0, p0, LX/EAt;->c:LX/EA4;

    invoke-virtual {v0}, LX/EA4;->d()V

    .line 2085970
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2085971
    check-cast p1, Ljava/util/List;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2085972
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2085973
    iget-object v4, p0, LX/EAt;->a:LX/0am;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    iget-object v2, p0, LX/EAt;->b:LX/EA4;

    .line 2085974
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2085975
    if-nez v0, :cond_2

    const/4 v5, 0x0

    .line 2085976
    :goto_1
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->m()Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 2085977
    if-eqz v3, :cond_0

    .line 2085978
    iget-object v3, v2, LX/EA4;->q:LX/E9P;

    .line 2085979
    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/E9P;->a:LX/0am;

    .line 2085980
    if-nez v5, :cond_4

    const/4 v4, 0x0

    :goto_3
    invoke-static {v4}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/E9P;->b:LX/0am;

    .line 2085981
    iget-object v4, v3, LX/E9P;->c:LX/9Ea;

    invoke-static {v5}, LX/BNJ;->d(LX/5tj;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/9Ea;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2085982
    iget-object v3, v2, LX/EA4;->r:LX/E9U;

    const v4, -0x530e7589

    invoke-static {v3, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2085983
    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    iget-object v1, p0, LX/EAt;->c:LX/EA4;

    .line 2085984
    if-nez v0, :cond_5

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v1, v2}, LX/EA4;->a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel;)V

    .line 2085985
    return-void

    :cond_1
    move v0, v2

    .line 2085986
    goto :goto_0

    .line 2085987
    :cond_2
    iget-object v5, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v5, v5

    .line 2085988
    check-cast v5, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 2085989
    :cond_4
    invoke-virtual {v5}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->m()Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    move-result-object v4

    goto :goto_3

    .line 2085990
    :cond_5
    iget-object v2, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2085991
    check-cast v2, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel;

    goto :goto_4
.end method
