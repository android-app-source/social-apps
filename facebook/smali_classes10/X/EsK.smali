.class public LX/EsK;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field private a:[Lcom/facebook/fbui/widget/text/GlyphWithTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2175291
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2175292
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0314ae

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2175293
    invoke-virtual {p0, v3}, LX/EsK;->setOrientation(I)V

    .line 2175294
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2175295
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2175296
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2175297
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/EsK;->a:[Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 2175298
    iget-object v1, p0, LX/EsK;->a:[Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const/4 v2, 0x0

    const v0, 0x7f0d10d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aput-object v0, v1, v2

    .line 2175299
    iget-object v1, p0, LX/EsK;->a:[Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const v0, 0x7f0d10d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aput-object v0, v1, v3

    .line 2175300
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2175301
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 2175302
    iget-object v1, p0, LX/EsK;->a:[Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2175303
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2175304
    :cond_0
    return-void
.end method

.method public final a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ILandroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2175305
    iget-object v0, p0, LX/EsK;->a:[Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aget-object v0, v0, p1

    .line 2175306
    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2175307
    invoke-virtual {v0, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2175308
    invoke-virtual {v0, p4}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 2175309
    invoke-virtual {v0, p5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2175310
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2175311
    return-void
.end method

.method public setNumButtons(I)V
    .locals 2

    .prologue
    .line 2175312
    :goto_0
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 2175313
    iget-object v0, p0, LX/EsK;->a:[Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aget-object v0, v0, p1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2175314
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2175315
    :cond_0
    return-void
.end method
