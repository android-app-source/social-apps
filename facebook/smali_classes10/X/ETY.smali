.class public LX/ETY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ETX;


# instance fields
.field private final a:LX/ETB;


# direct methods
.method public constructor <init>(LX/ETB;)V
    .locals 0

    .prologue
    .line 2125217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125218
    iput-object p1, p0, LX/ETY;->a:LX/ETB;

    .line 2125219
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETO;)V
    .locals 5

    .prologue
    .line 2125220
    const-string v0, "download-section-id"

    .line 2125221
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2125222
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2125223
    iget-object v0, p0, LX/ETY;->a:LX/ETB;

    .line 2125224
    iget-object v1, v0, LX/ETB;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ETN;

    const/4 p0, 0x0

    invoke-virtual {v1, p0, p2}, LX/ETN;->a(ZLX/ETO;)V

    .line 2125225
    :goto_0
    return-void

    .line 2125226
    :cond_0
    iget-object v0, p0, LX/ETY;->a:LX/ETB;

    .line 2125227
    instance-of v1, p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-nez v1, :cond_2

    .line 2125228
    :cond_1
    :goto_1
    goto :goto_0

    .line 2125229
    :cond_2
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125230
    invoke-static {p1}, LX/Cfu;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v1

    .line 2125231
    if-eqz v1, :cond_1

    .line 2125232
    iget-object v2, p1, Lcom/facebook/video/videohome/data/VideoHomeItem;->b:LX/0us;

    move-object v2, v2

    .line 2125233
    if-eqz v2, :cond_1

    .line 2125234
    iget-object v2, v0, LX/ETB;->s:LX/ETG;

    .line 2125235
    iget-object v3, p1, Lcom/facebook/video/videohome/data/VideoHomeItem;->b:LX/0us;

    move-object v3, v3

    .line 2125236
    invoke-interface {v3}, LX/0us;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->b()Ljava/lang/String;

    move-result-object v1

    new-instance p0, LX/ET6;

    invoke-direct {p0, v0, p1, p2}, LX/ET6;-><init>(LX/ETB;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETO;)V

    invoke-virtual {v2, v3, v4, v1, p0}, LX/ETG;->a(Ljava/lang/String;ILjava/lang/String;LX/0Ve;)V

    goto :goto_1
.end method
