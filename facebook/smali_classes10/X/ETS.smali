.class public final LX/ETS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/157;


# instance fields
.field public final synthetic a:LX/ETT;


# direct methods
.method public constructor <init>(LX/ETT;)V
    .locals 0

    .prologue
    .line 2125056
    iput-object p1, p0, LX/ETS;->a:LX/ETT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2125057
    iget-object v0, p0, LX/ETS;->a:LX/ETT;

    iget-object v0, v0, LX/ETT;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2125058
    if-nez v0, :cond_0

    .line 2125059
    :goto_0
    return-void

    .line 2125060
    :cond_0
    iget-object v1, p0, LX/ETS;->a:LX/ETT;

    .line 2125061
    iget-object p0, v1, LX/ETT;->d:LX/0Sh;

    new-instance p1, Lcom/facebook/video/videohome/data/VideoHomeLiveStatusListener$1;

    invoke-direct {p1, v1, v0, v2, v2}, Lcom/facebook/video/videohome/data/VideoHomeLiveStatusListener$1;-><init>(LX/ETT;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V

    invoke-virtual {p0, p1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2125062
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V
    .locals 2
    .param p3    # Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2125063
    iget-object v0, p0, LX/ETS;->a:LX/ETT;

    iget-object v0, v0, LX/ETT;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2125064
    if-nez v0, :cond_1

    .line 2125065
    :cond_0
    :goto_0
    return-void

    .line 2125066
    :cond_1
    invoke-static {p2}, LX/Ac6;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2125067
    invoke-static {v0}, LX/ETT;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    .line 2125068
    invoke-static {v1}, LX/Ac6;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2125069
    iget-object v1, p0, LX/ETS;->a:LX/ETT;

    .line 2125070
    iget-object p0, v1, LX/ETT;->d:LX/0Sh;

    new-instance p1, Lcom/facebook/video/videohome/data/VideoHomeLiveStatusListener$1;

    invoke-direct {p1, v1, v0, p2, p3}, Lcom/facebook/video/videohome/data/VideoHomeLiveStatusListener$1;-><init>(LX/ETT;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V

    invoke-virtual {p0, p1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2125071
    goto :goto_0

    .line 2125072
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p2, v1, :cond_3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne p2, v1, :cond_0

    .line 2125073
    :cond_3
    iget-object v1, p0, LX/ETS;->a:LX/ETT;

    .line 2125074
    iget-object p0, v1, LX/ETT;->d:LX/0Sh;

    new-instance p1, Lcom/facebook/video/videohome/data/VideoHomeLiveStatusListener$1;

    invoke-direct {p1, v1, v0, p2, p3}, Lcom/facebook/video/videohome/data/VideoHomeLiveStatusListener$1;-><init>(LX/ETT;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V

    invoke-virtual {p0, p1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2125075
    goto :goto_0
.end method
