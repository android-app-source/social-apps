.class public LX/EIg;
.super LX/EH2;
.source ""


# instance fields
.field public a:LX/00H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/Button;

.field public c:Landroid/widget/Button;

.field public d:LX/EBq;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2102223
    invoke-direct {p0, p1}, LX/EH2;-><init>(Landroid/content/Context;)V

    .line 2102224
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2102225
    iput-object v0, p0, LX/EIg;->f:LX/0Ot;

    .line 2102226
    const-class v0, LX/EIg;

    invoke-static {v0, p0}, LX/EIg;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2102227
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2102228
    const v1, 0x7f03161d

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2102229
    const v0, 0x7f0d31bc

    invoke-virtual {p0, v0}, LX/EH2;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/EIg;->b:Landroid/widget/Button;

    .line 2102230
    const v0, 0x7f0d31bb

    invoke-virtual {p0, v0}, LX/EH2;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/EIg;->c:Landroid/widget/Button;

    .line 2102231
    const v0, 0x7f0d31ba

    invoke-virtual {p0, v0}, LX/EH2;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EIg;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2102232
    iget-object v0, p0, LX/EIg;->b:Landroid/widget/Button;

    new-instance v1, LX/EIe;

    invoke-direct {v1, p0}, LX/EIe;-><init>(LX/EIg;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2102233
    iget-object v0, p0, LX/EIg;->c:Landroid/widget/Button;

    new-instance v1, LX/EIf;

    invoke-direct {v1, p0}, LX/EIf;-><init>(LX/EIg;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2102234
    iget-object v0, p0, LX/EIg;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2102235
    iget-object v1, v0, LX/EDx;->bl:LX/7TQ;

    move-object v2, v1

    .line 2102236
    iget-object v0, p0, LX/EIg;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2102237
    iget-boolean v1, v0, LX/EDx;->aB:Z

    move v3, v1

    .line 2102238
    const v1, 0x7f08076a

    .line 2102239
    const v0, 0x7f080763

    .line 2102240
    if-nez v3, :cond_1

    sget-object p1, LX/7TQ;->CallEndClientInterrupted:LX/7TQ;

    if-ne v2, p1, :cond_1

    .line 2102241
    const v1, 0x7f080770

    .line 2102242
    const v0, 0x7f080760

    .line 2102243
    :cond_0
    :goto_0
    iget-object v2, p0, LX/EIg;->b:Landroid/widget/Button;

    invoke-virtual {p0, v0}, LX/EH2;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2102244
    iget-object v2, p0, LX/EIg;->e:Lcom/facebook/resources/ui/FbTextView;

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-object v0, p0, LX/EIg;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->az()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, p1

    .line 2102245
    invoke-virtual {p0}, LX/EH2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2102246
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2102247
    return-void

    .line 2102248
    :cond_1
    sget-object p1, LX/7TQ;->CallEndClientInterrupted:LX/7TQ;

    if-ne v2, p1, :cond_2

    .line 2102249
    const v1, 0x7f08076b

    .line 2102250
    const v0, 0x7f080763

    goto :goto_0

    .line 2102251
    :cond_2
    if-nez v3, :cond_0

    .line 2102252
    const v1, 0x7f08076f

    .line 2102253
    const v0, 0x7f080760

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/EIg;

    const-class v1, LX/00H;

    invoke-interface {v2, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/00H;

    const/16 p0, 0x3257

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, LX/EIg;->a:LX/00H;

    iput-object v2, p1, LX/EIg;->f:LX/0Ot;

    return-void
.end method
