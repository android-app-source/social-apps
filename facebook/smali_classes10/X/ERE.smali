.class public final LX/ERE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;)V
    .locals 0

    .prologue
    .line 2120629
    iput-object p1, p0, LX/ERE;->a:Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x2fc77fb1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2120630
    iget-object v1, p0, LX/ERE;->a:Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;

    iget-object v1, v1, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->c:LX/EQh;

    .line 2120631
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "shoebox_moments_app_open"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2120632
    iget-object v3, v1, LX/EQh;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2120633
    iget-object v1, p0, LX/ERE;->a:Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;

    iget-object v1, v1, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->b:LX/1Vg;

    new-instance v2, LX/1Wx;

    invoke-direct {v2}, LX/1Wx;-><init>()V

    invoke-virtual {v1, v2}, LX/1Vg;->a(LX/1Vq;)LX/1Vr;

    move-result-object v1

    const-string v2, "moments://"

    iget-object v3, p0, LX/ERE;->a:Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;

    iget-object v3, v3, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, LX/1Vr;->b(Ljava/lang/String;Landroid/content/Context;)V

    .line 2120634
    const v1, 0x54394d14

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
