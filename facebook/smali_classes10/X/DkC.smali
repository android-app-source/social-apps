.class public final LX/DkC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Djl;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/DkN;


# direct methods
.method public constructor <init>(LX/DkN;Ljava/lang/String;Ljava/lang/String;LX/Djl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2034011
    iput-object p1, p0, LX/DkC;->e:LX/DkN;

    iput-object p2, p0, LX/DkC;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DkC;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DkC;->c:LX/Djl;

    iput-object p5, p0, LX/DkC;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2034012
    iget-object v0, p0, LX/DkC;->e:LX/DkN;

    iget-object v0, v0, LX/DkN;->f:LX/Dka;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DkC;->e:LX/DkN;

    iget-object v2, v2, LX/DkN;->e:Ljava/lang/String;

    iget-object v3, p0, LX/DkC;->a:Ljava/lang/String;

    iget-object v4, p0, LX/DkC;->b:Ljava/lang/String;

    iget-object v5, p0, LX/DkC;->c:LX/Djl;

    iget-object v6, p0, LX/DkC;->d:Ljava/lang/String;

    iget-object v7, p0, LX/DkC;->e:LX/DkN;

    iget-object v7, v7, LX/DkN;->e:Ljava/lang/String;

    .line 2034013
    invoke-static {}, LX/DlU;->a()LX/DlT;

    move-result-object v8

    .line 2034014
    new-instance v9, LX/4HK;

    invoke-direct {v9}, LX/4HK;-><init>()V

    .line 2034015
    invoke-virtual {v9, v1}, LX/4HK;->a(Ljava/lang/String;)LX/4HK;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/4HK;->b(Ljava/lang/String;)LX/4HK;

    move-result-object v10

    const-string p0, "CREATE"

    invoke-virtual {v10, p0}, LX/4HK;->d(Ljava/lang/String;)LX/4HK;

    move-result-object v10

    .line 2034016
    iget-object p0, v5, LX/Djl;->a:Ljava/lang/Integer;

    move-object p0, p0

    .line 2034017
    invoke-virtual {v10, p0}, LX/4HK;->a(Ljava/lang/Integer;)LX/4HK;

    .line 2034018
    if-eqz v4, :cond_2

    .line 2034019
    const-string v10, "request_owner_id"

    invoke-virtual {v9, v10, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034020
    const-string v10, "page_id"

    invoke-virtual {v9, v10, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034021
    :goto_0
    iget-object v10, v5, LX/Djl;->b:Ljava/lang/Integer;

    move-object v10, v10

    .line 2034022
    if-eqz v10, :cond_0

    .line 2034023
    iget-object v10, v5, LX/Djl;->a:Ljava/lang/Integer;

    move-object v10, v10

    .line 2034024
    iget-object p0, v5, LX/Djl;->b:Ljava/lang/Integer;

    move-object p0, p0

    .line 2034025
    invoke-virtual {v10, p0}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 2034026
    iget-object v10, v5, LX/Djl;->b:Ljava/lang/Integer;

    move-object v10, v10

    .line 2034027
    invoke-virtual {v9, v10}, LX/4HK;->b(Ljava/lang/Integer;)LX/4HK;

    .line 2034028
    :cond_0
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 2034029
    const-string v10, "service_id"

    invoke-virtual {v9, v10, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034030
    :cond_1
    const-string v10, "input"

    invoke-virtual {v8, v10, v9}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2034031
    iget-object v9, v0, LX/Dka;->a:LX/0tX;

    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v8

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v8, v8

    .line 2034032
    new-instance v9, LX/DkZ;

    invoke-direct {v9, v0, v7, v3}, LX/DkZ;-><init>(LX/Dka;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, v0, LX/Dka;->f:Ljava/util/concurrent/Executor;

    invoke-static {v8, v9, v10}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2034033
    move-object v0, v8

    .line 2034034
    return-object v0

    .line 2034035
    :cond_2
    invoke-virtual {v9, v3}, LX/4HK;->c(Ljava/lang/String;)LX/4HK;

    goto :goto_0
.end method
