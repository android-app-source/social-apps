.class public final LX/EZI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2138906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138907
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EZI;->a:Z

    .line 2138908
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EZI;->b:Z

    .line 2138909
    return-void
.end method

.method private a(IILjava/util/List;LX/EZJ;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<*>;",
            "LX/EZJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2138910
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 2138911
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138912
    const-string v2, ": "

    invoke-virtual {p4, v2}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138913
    invoke-static {p2, v0, p4}, LX/EZK;->b(ILjava/lang/Object;LX/EZJ;)V

    .line 2138914
    iget-boolean v0, p0, LX/EZI;->a:Z

    if-eqz v0, :cond_0

    const-string v0, " "

    :goto_1
    invoke-virtual {p4, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const-string v0, "\n"

    goto :goto_1

    .line 2138915
    :cond_1
    return-void
.end method

.method public static a$redex0(LX/EZI;LX/EWT;LX/EZJ;)V
    .locals 5

    .prologue
    .line 2138916
    invoke-interface {p1}, LX/EWT;->kb_()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2138917
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EYP;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 2138918
    invoke-virtual {v1}, LX/EYP;->m()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2138919
    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 2138920
    invoke-static {p0, v1, v4, p2}, LX/EZI;->b(LX/EZI;LX/EYP;Ljava/lang/Object;LX/EZJ;)V

    goto :goto_1

    .line 2138921
    :cond_0
    invoke-static {p0, v1, v0, p2}, LX/EZI;->b(LX/EZI;LX/EYP;Ljava/lang/Object;LX/EZJ;)V

    .line 2138922
    :cond_1
    goto :goto_0

    .line 2138923
    :cond_2
    invoke-interface {p1}, LX/EWT;->g()LX/EZQ;

    move-result-object v0

    invoke-static {p0, v0, p2}, LX/EZI;->a$redex0(LX/EZI;LX/EZQ;LX/EZJ;)V

    .line 2138924
    return-void
.end method

.method public static a$redex0(LX/EZI;LX/EZQ;LX/EZJ;)V
    .locals 6

    .prologue
    .line 2138925
    iget-object v0, p1, LX/EZQ;->b:Ljava/util/Map;

    move-object v0, v0

    .line 2138926
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2138927
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2138928
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EZO;

    .line 2138929
    const/4 v4, 0x0

    .line 2138930
    iget-object v5, v1, LX/EZO;->b:Ljava/util/List;

    move-object v5, v5

    .line 2138931
    invoke-direct {p0, v2, v4, v5, p2}, LX/EZI;->a(IILjava/util/List;LX/EZJ;)V

    .line 2138932
    const/4 v4, 0x5

    .line 2138933
    iget-object v5, v1, LX/EZO;->c:Ljava/util/List;

    move-object v5, v5

    .line 2138934
    invoke-direct {p0, v2, v4, v5, p2}, LX/EZI;->a(IILjava/util/List;LX/EZJ;)V

    .line 2138935
    const/4 v4, 0x1

    .line 2138936
    iget-object v5, v1, LX/EZO;->d:Ljava/util/List;

    move-object v5, v5

    .line 2138937
    invoke-direct {p0, v2, v4, v5, p2}, LX/EZI;->a(IILjava/util/List;LX/EZJ;)V

    .line 2138938
    const/4 v4, 0x2

    .line 2138939
    iget-object v5, v1, LX/EZO;->e:Ljava/util/List;

    move-object v5, v5

    .line 2138940
    invoke-direct {p0, v2, v4, v5, p2}, LX/EZI;->a(IILjava/util/List;LX/EZJ;)V

    .line 2138941
    iget-object v2, v1, LX/EZO;->f:Ljava/util/List;

    move-object v1, v2

    .line 2138942
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EZQ;

    .line 2138943
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138944
    iget-boolean v2, p0, LX/EZI;->a:Z

    if-eqz v2, :cond_1

    .line 2138945
    const-string v2, " { "

    invoke-virtual {p2, v2}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138946
    :goto_1
    invoke-static {p0, v1, p2}, LX/EZI;->a$redex0(LX/EZI;LX/EZQ;LX/EZJ;)V

    .line 2138947
    iget-boolean v1, p0, LX/EZI;->a:Z

    if-eqz v1, :cond_2

    .line 2138948
    const-string v1, "} "

    invoke-virtual {p2, v1}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2138949
    :cond_1
    const-string v2, " {\n"

    invoke-virtual {p2, v2}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138950
    invoke-virtual {p2}, LX/EZJ;->a()V

    goto :goto_1

    .line 2138951
    :cond_2
    invoke-virtual {p2}, LX/EZJ;->b()V

    .line 2138952
    const-string v1, "}\n"

    invoke-virtual {p2, v1}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2138953
    :cond_3
    return-void
.end method

.method public static b(LX/EZI;LX/EYP;Ljava/lang/Object;LX/EZJ;)V
    .locals 2

    .prologue
    .line 2138954
    invoke-virtual {p1}, LX/EYP;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2138955
    const-string v0, "["

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138956
    iget-object v0, p1, LX/EYP;->h:LX/EYF;

    move-object v0, v0

    .line 2138957
    invoke-virtual {v0}, LX/EYF;->d()LX/EXf;

    move-result-object v0

    .line 2138958
    iget-boolean v1, v0, LX/EXf;->messageSetWireFormat_:Z

    move v0, v1

    .line 2138959
    if-eqz v0, :cond_0

    .line 2138960
    iget-object v0, p1, LX/EYP;->g:LX/EYO;

    move-object v0, v0

    .line 2138961
    sget-object v1, LX/EYO;->MESSAGE:LX/EYO;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, LX/EYP;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/EYP;->s()LX/EYF;

    move-result-object v0

    invoke-virtual {p1}, LX/EYP;->t()LX/EYF;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 2138962
    invoke-virtual {p1}, LX/EYP;->t()LX/EYF;

    move-result-object v0

    invoke-virtual {v0}, LX/EYF;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138963
    :goto_0
    const-string v0, "]"

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138964
    :goto_1
    invoke-virtual {p1}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v0, v1, :cond_4

    .line 2138965
    iget-boolean v0, p0, LX/EZI;->a:Z

    if-eqz v0, :cond_3

    .line 2138966
    const-string v0, " { "

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138967
    :goto_2
    invoke-direct {p0, p1, p2, p3}, LX/EZI;->c(LX/EYP;Ljava/lang/Object;LX/EZJ;)V

    .line 2138968
    invoke-virtual {p1}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v0, v1, :cond_6

    .line 2138969
    iget-boolean v0, p0, LX/EZI;->a:Z

    if-eqz v0, :cond_5

    .line 2138970
    const-string v0, "} "

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138971
    :goto_3
    return-void

    .line 2138972
    :cond_0
    invoke-virtual {p1}, LX/EYP;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2138973
    :cond_1
    iget-object v0, p1, LX/EYP;->g:LX/EYO;

    move-object v0, v0

    .line 2138974
    sget-object v1, LX/EYO;->GROUP:LX/EYO;

    if-ne v0, v1, :cond_2

    .line 2138975
    invoke-virtual {p1}, LX/EYP;->t()LX/EYF;

    move-result-object v0

    invoke-virtual {v0}, LX/EYF;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2138976
    :cond_2
    invoke-virtual {p1}, LX/EYP;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2138977
    :cond_3
    const-string v0, " {\n"

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2138978
    invoke-virtual {p3}, LX/EZJ;->a()V

    goto :goto_2

    .line 2138979
    :cond_4
    const-string v0, ": "

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2138980
    :cond_5
    invoke-virtual {p3}, LX/EZJ;->b()V

    .line 2138981
    const-string v0, "}\n"

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2138982
    :cond_6
    iget-boolean v0, p0, LX/EZI;->a:Z

    if-eqz v0, :cond_7

    .line 2138983
    const-string v0, " "

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2138984
    :cond_7
    const-string v0, "\n"

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method private c(LX/EYP;Ljava/lang/Object;LX/EZJ;)V
    .locals 6

    .prologue
    .line 2138985
    sget-object v0, LX/EZG;->a:[I

    .line 2138986
    iget-object v1, p1, LX/EYP;->g:LX/EYO;

    move-object v1, v1

    .line 2138987
    invoke-virtual {v1}, LX/EYO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2138988
    :goto_0
    return-void

    .line 2138989
    :pswitch_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2138990
    :pswitch_1
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2138991
    :pswitch_2
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2138992
    :pswitch_3
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2138993
    :pswitch_4
    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2138994
    :pswitch_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2138995
    if-ltz v0, :cond_1

    .line 2138996
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 2138997
    :goto_1
    move-object v0, v2

    .line 2138998
    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2138999
    :pswitch_6
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/EZK;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2139000
    :pswitch_7
    const-string v0, "\""

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2139001
    iget-boolean v0, p0, LX/EZI;->b:Z

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/String;

    .line 2139002
    invoke-static {p2}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    invoke-static {v0}, LX/EZK;->a(LX/EWc;)Ljava/lang/String;

    move-result-object v0

    move-object p2, v0

    .line 2139003
    :goto_2
    invoke-virtual {p3, p2}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2139004
    const-string v0, "\""

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2139005
    :cond_0
    check-cast p2, Ljava/lang/String;

    goto :goto_2

    .line 2139006
    :pswitch_8
    const-string v0, "\""

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2139007
    check-cast p2, LX/EWc;

    invoke-static {p2}, LX/EZK;->a(LX/EWc;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    .line 2139008
    const-string v0, "\""

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2139009
    :pswitch_9
    check-cast p2, LX/EYM;

    invoke-virtual {p2}, LX/EYM;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/EZJ;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2139010
    :pswitch_a
    check-cast p2, LX/EWY;

    invoke-static {p0, p2, p3}, LX/EZI;->a$redex0(LX/EZI;LX/EWT;LX/EZJ;)V

    goto/16 :goto_0

    :cond_1
    int-to-long v2, v0

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
    .end packed-switch
.end method
