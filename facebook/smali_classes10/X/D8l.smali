.class public LX/D8l;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideoAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideoAnnotation;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 1969320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1969321
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1969322
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1969323
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1969324
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1969325
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1969326
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1969327
    iput-object p1, p0, LX/D8l;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969328
    iput-object p2, p0, LX/D8l;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969329
    iput p3, p0, LX/D8l;->c:I

    .line 1969330
    iput p4, p0, LX/D8l;->d:I

    .line 1969331
    return-void
.end method


# virtual methods
.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1969332
    iget-object v0, p0, LX/D8l;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1969333
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1969334
    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
