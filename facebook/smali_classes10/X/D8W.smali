.class public LX/D8W;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/0hE;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/view/ViewGroup;

.field private final d:LX/D8b;

.field private final e:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

.field private final f:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

.field private final g:LX/D8S;

.field public final h:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

.field private final i:LX/D8Y;

.field private j:LX/394;

.field private k:Z

.field public l:LX/2mz;

.field private m:LX/D8f;

.field public n:Landroid/view/ViewGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/2my;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/D8c;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/D8T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/D8Z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/D8o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1968785
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D8W;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1968786
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1968787
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D8W;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1968788
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1968789
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1968790
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D8W;->k:Z

    .line 1968791
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/D8W;

    invoke-static {v0}, LX/2my;->a(LX/0QB;)LX/2my;

    move-result-object v3

    check-cast v3, LX/2my;

    const-class p1, LX/D8c;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/D8c;

    const-class p2, LX/D8T;

    invoke-interface {v0, p2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p2

    check-cast p2, LX/D8T;

    const-class p3, LX/D8Z;

    invoke-interface {v0, p3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p3

    check-cast p3, LX/D8Z;

    invoke-static {v0}, LX/D8o;->a(LX/0QB;)LX/D8o;

    move-result-object v0

    check-cast v0, LX/D8o;

    iput-object v3, v2, LX/D8W;->q:LX/2my;

    iput-object p1, v2, LX/D8W;->r:LX/D8c;

    iput-object p2, v2, LX/D8W;->s:LX/D8T;

    iput-object p3, v2, LX/D8W;->t:LX/D8Z;

    iput-object v0, v2, LX/D8W;->u:LX/D8o;

    .line 1968792
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, LX/D8W;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0a0a

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f03160f

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1968793
    new-instance v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-virtual {p0}, LX/D8W;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/D8W;->h:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    .line 1968794
    const v0, 0x7f0d319b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    iput-object v0, p0, LX/D8W;->e:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    .line 1968795
    const v0, 0x7f0d3197

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iput-object v0, p0, LX/D8W;->f:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    .line 1968796
    const v0, 0x7f0d3199

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/D8W;->a:Landroid/view/ViewGroup;

    .line 1968797
    const v0, 0x7f0d319a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/D8W;->b:Landroid/view/ViewGroup;

    .line 1968798
    const v0, 0x7f0d3198

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/D8W;->c:Landroid/view/ViewGroup;

    .line 1968799
    invoke-virtual {p0}, LX/D8W;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1968800
    sget-object v1, LX/D8d;->a:LX/D8d;

    move-object v1, v1

    .line 1968801
    const/4 v2, 0x0

    .line 1968802
    new-instance v3, LX/D8b;

    invoke-direct {v3, v0, v1, v2}, LX/D8b;-><init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;)V

    .line 1968803
    move-object v0, v3

    .line 1968804
    iput-object v0, p0, LX/D8W;->d:LX/D8b;

    .line 1968805
    iget-object v0, p0, LX/D8W;->s:LX/D8T;

    invoke-virtual {p0}, LX/D8W;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/D8W;->h:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    iget-object v3, p0, LX/D8W;->e:Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    invoke-virtual {v0, v1, v2, v3}, LX/D8T;->a(Landroid/content/Context;Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;)LX/D8S;

    move-result-object v0

    iput-object v0, p0, LX/D8W;->g:LX/D8S;

    .line 1968806
    invoke-virtual {p0}, LX/D8W;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1968807
    new-instance v1, LX/D8Y;

    invoke-direct {v1, v0}, LX/D8Y;-><init>(Landroid/content/Context;)V

    .line 1968808
    move-object v0, v1

    .line 1968809
    iput-object v0, p0, LX/D8W;->i:LX/D8Y;

    .line 1968810
    return-void
.end method

.method private static c$redex0(LX/D8W;)V
    .locals 10

    .prologue
    .line 1968811
    iget-object v0, p0, LX/D8W;->l:LX/2mz;

    const v1, 0x7f0d3199

    iget-object v2, p0, LX/D8W;->a:Landroid/view/ViewGroup;

    iget-object v3, p0, LX/D8W;->b:Landroid/view/ViewGroup;

    invoke-virtual {p0}, LX/D8W;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, LX/D8W;->p:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v6, p0, LX/D8W;->d:LX/D8b;

    new-instance v7, LX/D8V;

    invoke-direct {v7, p0}, LX/D8V;-><init>(LX/D8W;)V

    iget-object v8, p0, LX/D8W;->h:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    .line 1968812
    iget-object v9, v8, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    move-object v8, v9

    .line 1968813
    iget v8, v8, LX/3FO;->b:I

    iget-object v9, p0, LX/D8W;->g:LX/D8S;

    invoke-interface/range {v0 .. v9}, LX/2mz;->a(ILandroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;LX/D8V;ILX/D8S;)V

    .line 1968814
    iget-object v0, p0, LX/D8W;->l:LX/2mz;

    invoke-interface {v0}, LX/2mz;->e()LX/D7g;

    move-result-object v0

    .line 1968815
    if-eqz v0, :cond_0

    .line 1968816
    new-instance v1, LX/D8f;

    iget-object v2, p0, LX/D8W;->h:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    .line 1968817
    iget-object v3, v2, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    move-object v2, v3

    .line 1968818
    iget v2, v2, LX/3FO;->b:I

    invoke-virtual {p0}, LX/D8W;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/D8W;->g:LX/D8S;

    invoke-direct {v1, v0, v2, v3, v4}, LX/D8f;-><init>(LX/D7g;ILandroid/content/Context;LX/D8S;)V

    iput-object v1, p0, LX/D8W;->m:LX/D8f;

    .line 1968819
    iget-object v0, p0, LX/D8W;->f:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v1, p0, LX/D8W;->m:LX/D8f;

    .line 1968820
    iput-object v1, v0, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a:LX/D8f;

    .line 1968821
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D8W;->o:Z

    .line 1968822
    return-void
.end method

.method public static d(LX/D8W;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1968823
    iput-boolean v0, p0, LX/D8W;->k:Z

    .line 1968824
    iput-boolean v0, p0, LX/D8W;->o:Z

    .line 1968825
    iget-object v0, p0, LX/D8W;->f:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    invoke-virtual {v0}, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->a()V

    .line 1968826
    iget-object v0, p0, LX/D8W;->m:LX/D8f;

    if-eqz v0, :cond_1

    .line 1968827
    iget-object v0, p0, LX/D8W;->m:LX/D8f;

    const/4 v3, 0x0

    .line 1968828
    iget-object v1, v0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    .line 1968829
    iget-object v1, v0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1968830
    iput-object v3, v0, LX/D8f;->d:Landroid/animation/ValueAnimator;

    .line 1968831
    :cond_0
    iput-object v3, v0, LX/D8f;->e:LX/D8S;

    .line 1968832
    iput-object v3, v0, LX/D8f;->a:LX/D7g;

    .line 1968833
    :cond_1
    iget-object v0, p0, LX/D8W;->l:LX/2mz;

    invoke-interface {v0}, LX/2mz;->a()V

    .line 1968834
    iget-object v0, p0, LX/D8W;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1968835
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1968836
    const/4 v3, 0x0

    .line 1968837
    iget-object v1, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v1

    .line 1968838
    if-gez v1, :cond_2

    move v1, v3

    .line 1968839
    :cond_2
    iget-object v4, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v4

    .line 1968840
    if-gez v4, :cond_3

    move v4, v3

    .line 1968841
    :cond_3
    if-le v4, v1, :cond_4

    move v4, v1

    .line 1968842
    :cond_4
    new-instance v9, LX/7Ju;

    invoke-direct {v9}, LX/7Ju;-><init>()V

    iget-object v5, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v5

    if-nez v5, :cond_7

    const/4 v5, 0x1

    .line 1968843
    :goto_0
    iput-boolean v5, v9, LX/7Ju;->a:Z

    .line 1968844
    move-object v5, v9

    .line 1968845
    iget-object v9, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v9}, Lcom/facebook/video/player/RichVideoPlayer;->s()Z

    move-result v9

    .line 1968846
    iput-boolean v9, v5, LX/7Ju;->b:Z

    .line 1968847
    move-object v5, v5

    .line 1968848
    iput v1, v5, LX/7Ju;->c:I

    .line 1968849
    move-object v1, v5

    .line 1968850
    iput v4, v1, LX/7Ju;->d:I

    .line 1968851
    move-object v1, v1

    .line 1968852
    iput-boolean v3, v1, LX/7Ju;->e:Z

    .line 1968853
    move-object v1, v1

    .line 1968854
    sget-object v3, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 1968855
    iput-object v3, v1, LX/7Ju;->h:LX/04g;

    .line 1968856
    move-object v1, v1

    .line 1968857
    invoke-virtual {v1}, LX/7Ju;->a()LX/7Jv;

    move-result-object v1

    move-object v1, v1

    .line 1968858
    invoke-static {v0, v1}, LX/D8S;->a(LX/D8S;LX/7Jv;)V

    .line 1968859
    iget-object v3, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v4, 0x1

    sget-object v5, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1968860
    iget-object v3, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v4, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    invoke-virtual {v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1968861
    iget-object v3, v0, LX/D8S;->d:LX/Bug;

    invoke-virtual {v3}, LX/Bug;->a()V

    .line 1968862
    iget-object v3, v0, LX/D8S;->h:LX/D8K;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1968863
    iput-object v4, v3, LX/D8K;->b:Ljava/lang/ref/WeakReference;

    .line 1968864
    iput-object v4, v3, LX/D8K;->c:Ljava/lang/ref/WeakReference;

    .line 1968865
    const/4 v4, 0x1

    iput v4, v3, LX/D8K;->f:I

    .line 1968866
    iput-boolean v5, v3, LX/D8K;->h:Z

    .line 1968867
    iput-boolean v5, v3, LX/D8K;->g:Z

    .line 1968868
    iget-object v3, v0, LX/D8S;->n:LX/394;

    if-eqz v3, :cond_5

    .line 1968869
    iget-object v3, v0, LX/D8S;->n:LX/394;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    invoke-interface {v3, v4, v1}, LX/394;->a(LX/04g;LX/7Jv;)V

    .line 1968870
    :cond_5
    iget-object v1, v0, LX/D8S;->e:LX/99w;

    .line 1968871
    iget-object v3, v1, LX/99w;->a:LX/1B1;

    iget-object v4, v1, LX/99w;->b:LX/0bH;

    invoke-virtual {v3, v4}, LX/1B1;->b(LX/0b4;)V

    .line 1968872
    iget-object v1, v0, LX/D8S;->v:Landroid/view/ViewGroup;

    iget-object v3, v0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1968873
    iput-object v6, v0, LX/D8S;->v:Landroid/view/ViewGroup;

    .line 1968874
    iput-object v6, v0, LX/D8S;->u:LX/04g;

    .line 1968875
    iput-object v6, v0, LX/D8S;->t:LX/04D;

    .line 1968876
    iput-object v6, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968877
    iput-object v6, v0, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968878
    iput v7, v0, LX/D8S;->o:F

    .line 1968879
    iput v7, v0, LX/D8S;->p:F

    .line 1968880
    iput v7, v0, LX/D8S;->q:F

    .line 1968881
    iput v8, v0, LX/D8S;->x:I

    .line 1968882
    iput-object v6, v0, LX/D8S;->s:LX/2pa;

    .line 1968883
    iput-object v6, v0, LX/D8S;->w:LX/D8g;

    .line 1968884
    iput-boolean v8, v0, LX/D8S;->l:Z

    .line 1968885
    iget-object v0, p0, LX/D8W;->i:LX/D8Y;

    .line 1968886
    iget-object v1, v0, LX/D8Y;->b:Landroid/view/Window;

    if-eqz v1, :cond_6

    .line 1968887
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v1, v3, :cond_8

    .line 1968888
    iget-object v1, v0, LX/D8Y;->b:Landroid/view/Window;

    const/16 v3, 0x400

    invoke-virtual {v1, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 1968889
    :cond_6
    :goto_1
    invoke-direct {p0}, LX/D8W;->k()V

    .line 1968890
    iput-object v2, p0, LX/D8W;->m:LX/D8f;

    .line 1968891
    iput-object v2, p0, LX/D8W;->l:LX/2mz;

    .line 1968892
    iput-object v2, p0, LX/D8W;->j:LX/394;

    .line 1968893
    iput-object v2, p0, LX/D8W;->p:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968894
    return-void

    :cond_7
    move v5, v3

    goto/16 :goto_0

    .line 1968895
    :cond_8
    iget-object v1, v0, LX/D8Y;->b:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_1
.end method

.method private static j(LX/D8W;)V
    .locals 1

    .prologue
    .line 1968896
    invoke-virtual {p0}, LX/D8W;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/D8W;->n:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1968897
    iget-object v0, p0, LX/D8W;->i:LX/D8Y;

    invoke-virtual {v0}, LX/D8Y;->c()V

    .line 1968898
    iget-object v0, p0, LX/D8W;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1968899
    :cond_0
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 1968901
    invoke-virtual {p0}, LX/D8W;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/D8W;->n:Landroid/view/ViewGroup;

    if-ne v0, v1, :cond_0

    .line 1968902
    iget-object v0, p0, LX/D8W;->i:LX/D8Y;

    .line 1968903
    iget-object v1, v0, LX/D8Y;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1968904
    iget-object v0, p0, LX/D8W;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1968905
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/394;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1968900
    invoke-virtual {p0, p1}, LX/D8W;->b(LX/394;)LX/D8W;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2mz;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3FT;IILX/04D;LX/04g;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2mz;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/3FT;",
            "II",
            "LX/04D;",
            "LX/04g;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1968912
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    .line 1968913
    :cond_0
    :goto_0
    return-void

    .line 1968914
    :cond_1
    iput-object p1, p0, LX/D8W;->l:LX/2mz;

    .line 1968915
    iput-object p2, p0, LX/D8W;->p:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968916
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D8W;->k:Z

    .line 1968917
    invoke-static {p0}, LX/D8W;->j(LX/D8W;)V

    .line 1968918
    iget-object v0, p0, LX/D8W;->i:LX/D8Y;

    invoke-virtual {v0}, LX/D8Y;->a()V

    .line 1968919
    invoke-interface {p1}, LX/2mz;->c()LX/D8g;

    move-result-object v0

    .line 1968920
    new-instance v1, LX/D8U;

    invoke-direct {v1, p0, v0}, LX/D8U;-><init>(LX/D8W;LX/D8g;)V

    .line 1968921
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    iget-object v2, p0, LX/D8W;->l:LX/2mz;

    invoke-virtual {p0}, LX/D8W;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, p2}, LX/2mz;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v2

    iget-object v6, p0, LX/D8W;->c:Landroid/view/ViewGroup;

    iget-object v3, p0, LX/D8W;->l:LX/2mz;

    invoke-interface {v3}, LX/2mz;->c()LX/D8g;

    move-result-object v10

    iget-object v3, p0, LX/D8W;->l:LX/2mz;

    invoke-interface {v3}, LX/2mz;->e()LX/D7g;

    move-result-object v11

    move/from16 v3, p5

    move/from16 v4, p4

    move-object v5, p2

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object v9, p3

    invoke-virtual/range {v0 .. v11}, LX/D8S;->a(LX/D8U;IIILcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/ViewGroup;LX/04D;LX/04g;LX/3FT;LX/D8g;LX/D7g;)V

    .line 1968922
    iget-object v0, p0, LX/D8W;->f:Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;

    iget-object v1, p0, LX/D8W;->g:LX/D8S;

    invoke-virtual {v0, v1}, Lcom/facebook/video/watchandmore/WatchAndMoreGestureDelegateView;->setPlayerGestureListener(LX/D8S;)V

    .line 1968923
    invoke-virtual {p0}, LX/D8W;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1968924
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1968925
    invoke-static {p0}, LX/D8W;->c$redex0(LX/D8W;)V

    goto :goto_0

    .line 1968926
    :cond_2
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1968927
    invoke-virtual {p0, v0}, LX/D8W;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method public final a(LX/395;)V
    .locals 0

    .prologue
    .line 1968911
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1968910
    iget-boolean v0, p0, LX/D8W;->k:Z

    return v0
.end method

.method public final b(LX/394;)LX/D8W;
    .locals 1

    .prologue
    .line 1968906
    iput-object p1, p0, LX/D8W;->j:LX/394;

    .line 1968907
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    .line 1968908
    iput-object p1, v0, LX/D8S;->n:LX/394;

    .line 1968909
    return-object p0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 1968769
    invoke-virtual {p0}, LX/D8W;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1968770
    const/4 v0, 0x0

    .line 1968771
    :goto_0
    return v0

    .line 1968772
    :cond_0
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    .line 1968773
    iget-object v1, v0, LX/D8S;->h:LX/D8K;

    if-nez v1, :cond_2

    .line 1968774
    const/4 v1, 0x0

    .line 1968775
    :goto_1
    move v0, v1

    .line 1968776
    if-nez v0, :cond_1

    .line 1968777
    iget-object v0, p0, LX/D8W;->l:LX/2mz;

    invoke-interface {v0}, LX/2mz;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1968778
    invoke-static {p0}, LX/D8W;->d(LX/D8W;)V

    .line 1968779
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, v0, LX/D8S;->h:LX/D8K;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1968780
    iget-boolean v0, v1, LX/D8K;->g:Z

    if-nez v0, :cond_4

    .line 1968781
    :cond_3
    :goto_2
    move v1, v2

    .line 1968782
    goto :goto_1

    .line 1968783
    :cond_4
    invoke-static {v1}, LX/D8K;->e(LX/D8K;)V

    .line 1968784
    iget v0, v1, LX/D8K;->f:I

    if-ne v0, v3, :cond_3

    move v2, v3

    goto :goto_2
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1968710
    invoke-static {p0}, LX/D8W;->j(LX/D8W;)V

    .line 1968711
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1968764
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    if-eqz v0, :cond_0

    .line 1968765
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    .line 1968766
    iget-object v1, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, v0, LX/D8S;->l:Z

    if-nez v1, :cond_0

    .line 1968767
    iget-object v1, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_FLYOUT:LX/04g;

    iget-object p0, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result p0

    invoke-virtual {v1, v2, p0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    .line 1968768
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1968712
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    if-eqz v0, :cond_0

    .line 1968713
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    .line 1968714
    iget-object v1, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1968715
    iget-object v1, v0, LX/D8S;->r:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object p0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, p0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1968716
    :cond_0
    return-void
.end method

.method public getFullScreenListener()LX/394;
    .locals 1

    .prologue
    .line 1968717
    iget-object v0, p0, LX/D8W;->j:LX/394;

    return-object v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1968718
    invoke-direct {p0}, LX/D8W;->k()V

    .line 1968719
    return-void
.end method

.method public final i()Z
    .locals 3

    .prologue
    .line 1968720
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    .line 1968721
    const/4 p0, 0x3

    .line 1968722
    iget-object v1, v0, LX/D8S;->a:Landroid/media/AudioManager;

    invoke-virtual {v1, p0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 1968723
    iget-object v2, v0, LX/D8S;->a:Landroid/media/AudioManager;

    invoke-virtual {v2, p0}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    .line 1968724
    mul-int/lit8 v1, v1, 0x64

    div-int/2addr v1, v2

    move v1, v1

    .line 1968725
    if-gtz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1968726
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 1968727
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1968728
    iget-object v0, p0, LX/D8W;->g:LX/D8S;

    .line 1968729
    iget-object v1, v0, LX/D8S;->h:LX/D8K;

    if-eqz v1, :cond_0

    .line 1968730
    iget-object v1, v0, LX/D8S;->h:LX/D8K;

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    .line 1968731
    iget v3, v1, LX/D8K;->f:I

    if-ne v3, v2, :cond_6

    .line 1968732
    :cond_0
    :goto_0
    iget-object v1, v0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    .line 1968733
    iget-object v2, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    move-object v1, v2

    .line 1968734
    iget v1, v1, LX/3FO;->b:I

    .line 1968735
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1968736
    iget v2, v0, LX/D8S;->x:I

    invoke-static {v0, v2, v1}, LX/D8S;->a(LX/D8S;II)V

    .line 1968737
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1968738
    iget-boolean v0, p0, LX/D8W;->o:Z

    if-eqz v0, :cond_5

    .line 1968739
    iget-object v0, p0, LX/D8W;->l:LX/2mz;

    invoke-interface {v0}, LX/2mz;->e()LX/D7g;

    move-result-object v0

    .line 1968740
    if-eqz v0, :cond_2

    .line 1968741
    invoke-interface {v0}, LX/D7g;->j()V

    .line 1968742
    :cond_2
    :goto_1
    iget-boolean v0, p0, LX/D8W;->o:Z

    if-eqz v0, :cond_3

    .line 1968743
    iget-object v0, p0, LX/D8W;->h:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    .line 1968744
    iget-object v1, v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->j:LX/3FO;

    move-object v0, v1

    .line 1968745
    iget v0, v0, LX/3FO;->b:I

    .line 1968746
    iget-object v1, p0, LX/D8W;->l:LX/2mz;

    invoke-interface {v1, p1, v0}, LX/2mz;->a(Landroid/content/res/Configuration;I)V

    .line 1968747
    :cond_3
    invoke-virtual {p0}, LX/D8W;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1968748
    if-eqz v0, :cond_4

    .line 1968749
    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1968750
    :cond_4
    return-void

    .line 1968751
    :cond_5
    invoke-static {p0}, LX/D8W;->c$redex0(LX/D8W;)V

    goto :goto_1

    .line 1968752
    :cond_6
    iput v2, v1, LX/D8K;->f:I

    .line 1968753
    iget-boolean v3, v1, LX/D8K;->h:Z

    if-nez v3, :cond_0

    .line 1968754
    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    iget-boolean v3, v1, LX/D8K;->g:Z

    if-nez v3, :cond_7

    .line 1968755
    invoke-static {v1}, LX/D8K;->d(LX/D8K;)V

    goto :goto_0

    .line 1968756
    :cond_7
    iget-boolean v3, v1, LX/D8K;->g:Z

    if-eqz v3, :cond_0

    .line 1968757
    invoke-static {v1}, LX/D8K;->e(LX/D8K;)V

    goto :goto_0
.end method

.method public setAllowLooping(Z)V
    .locals 0

    .prologue
    .line 1968758
    return-void
.end method

.method public setLogEnteringStartEvent(Z)V
    .locals 0

    .prologue
    .line 1968759
    return-void
.end method

.method public setLogExitingPauseEvent(Z)V
    .locals 0

    .prologue
    .line 1968760
    return-void
.end method

.method public setNextStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1968761
    return-void
.end method

.method public setPreviousStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1968762
    return-void
.end method

.method public final t_(I)V
    .locals 0

    .prologue
    .line 1968763
    return-void
.end method
