.class public final enum LX/D9Q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D9Q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D9Q;

.field public static final enum MESSENGER:LX/D9Q;

.field public static final enum SMS:LX/D9Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1970078
    new-instance v0, LX/D9Q;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v2}, LX/D9Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D9Q;->MESSENGER:LX/D9Q;

    .line 1970079
    new-instance v0, LX/D9Q;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v3}, LX/D9Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D9Q;->SMS:LX/D9Q;

    .line 1970080
    const/4 v0, 0x2

    new-array v0, v0, [LX/D9Q;

    sget-object v1, LX/D9Q;->MESSENGER:LX/D9Q;

    aput-object v1, v0, v2

    sget-object v1, LX/D9Q;->SMS:LX/D9Q;

    aput-object v1, v0, v3

    sput-object v0, LX/D9Q;->$VALUES:[LX/D9Q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1970081
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D9Q;
    .locals 1

    .prologue
    .line 1970082
    const-class v0, LX/D9Q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D9Q;

    return-object v0
.end method

.method public static values()[LX/D9Q;
    .locals 1

    .prologue
    .line 1970083
    sget-object v0, LX/D9Q;->$VALUES:[LX/D9Q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D9Q;

    return-object v0
.end method
