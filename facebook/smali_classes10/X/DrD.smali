.class public final LX/DrD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;)V
    .locals 0

    .prologue
    .line 2049089
    iput-object p1, p0, LX/DrD;->a:Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2049090
    iget-object v0, p0, LX/DrD;->a:Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;

    iget-object v0, v0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2049091
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2049092
    check-cast p1, Ljava/util/List;

    .line 2049093
    if-nez p1, :cond_0

    .line 2049094
    :goto_0
    return-void

    .line 2049095
    :cond_0
    iget-object v0, p0, LX/DrD;->a:Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;

    iget-object v0, v0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;->b:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
