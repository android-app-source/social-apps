.class public final LX/DQx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1996830
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1996831
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1996832
    :goto_0
    return v1

    .line 1996833
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1996834
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1996835
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1996836
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1996837
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1996838
    const-string v3, "photo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1996839
    const/4 v2, 0x0

    .line 1996840
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1996841
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1996842
    :goto_2
    move v0, v2

    .line 1996843
    goto :goto_1

    .line 1996844
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1996845
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1996846
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 1996847
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1996848
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 1996849
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1996850
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1996851
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 1996852
    const-string v4, "image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1996853
    const/4 v3, 0x0

    .line 1996854
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_b

    .line 1996855
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1996856
    :goto_4
    move v0, v3

    .line 1996857
    goto :goto_3

    .line 1996858
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1996859
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1996860
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    .line 1996861
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1996862
    :cond_9
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_a

    .line 1996863
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1996864
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1996865
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_9

    if-eqz v4, :cond_9

    .line 1996866
    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1996867
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 1996868
    :cond_a
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1996869
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1996870
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_4

    :cond_b
    move v0, v3

    goto :goto_5
.end method
