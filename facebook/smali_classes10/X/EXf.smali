.class public final LX/EXf;
.super LX/EX1;
.source ""

# interfaces
.implements LX/EXd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EX1",
        "<",
        "LX/EXf;",
        ">;",
        "LX/EXd;"
    }
.end annotation


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXf;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EXf;


# instance fields
.field public bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public messageSetWireFormat_:Z

.field public noStandardDescriptorAccessor_:Z

.field public uninterpretedOption_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2134840
    new-instance v0, LX/EXc;

    invoke-direct {v0}, LX/EXc;-><init>()V

    sput-object v0, LX/EXf;->a:LX/EWZ;

    .line 2134841
    new-instance v0, LX/EXf;

    invoke-direct {v0}, LX/EXf;-><init>()V

    .line 2134842
    sput-object v0, LX/EXf;->c:LX/EXf;

    invoke-direct {v0}, LX/EXf;->q()V

    .line 2134843
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2134844
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2134845
    iput-byte v0, p0, LX/EXf;->memoizedIsInitialized:B

    .line 2134846
    iput v0, p0, LX/EXf;->memoizedSerializedSize:I

    .line 2134847
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2134848
    iput-object v0, p0, LX/EXf;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v6, 0x4

    .line 2134849
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2134850
    iput-byte v1, p0, LX/EXf;->memoizedIsInitialized:B

    .line 2134851
    iput v1, p0, LX/EXf;->memoizedSerializedSize:I

    .line 2134852
    invoke-direct {p0}, LX/EXf;->q()V

    .line 2134853
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v3

    move v1, v0

    .line 2134854
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 2134855
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v4

    .line 2134856
    sparse-switch v4, :sswitch_data_0

    .line 2134857
    invoke-virtual {p0, p1, v3, p2, v4}, LX/EX1;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 2134858
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 2134859
    goto :goto_0

    .line 2134860
    :sswitch_1
    iget v4, p0, LX/EXf;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/EXf;->bitField0_:I

    .line 2134861
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXf;->messageSetWireFormat_:Z
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2134862
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 2134863
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2134864
    move-object v0, v0

    .line 2134865
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2134866
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_1

    .line 2134867
    iget-object v1, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    .line 2134868
    :cond_1
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EXf;->unknownFields:LX/EZQ;

    .line 2134869
    invoke-virtual {p0}, LX/EX1;->E()V

    throw v0

    .line 2134870
    :sswitch_2
    :try_start_2
    iget v4, p0, LX/EXf;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, LX/EXf;->bitField0_:I

    .line 2134871
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXf;->noStandardDescriptorAccessor_:Z
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2134872
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 2134873
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2134874
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2134875
    move-object v0, v2

    .line 2134876
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2134877
    :sswitch_3
    and-int/lit8 v4, v0, 0x4

    if-eq v4, v6, :cond_2

    .line 2134878
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    .line 2134879
    or-int/lit8 v0, v0, 0x4

    .line 2134880
    :cond_2
    iget-object v4, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    sget-object v5, LX/EYB;->a:LX/EWZ;

    invoke-virtual {p1, v5, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 2134881
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    :cond_3
    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_4

    .line 2134882
    iget-object v0, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    .line 2134883
    :cond_4
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXf;->unknownFields:LX/EZQ;

    .line 2134884
    invoke-virtual {p0}, LX/EX1;->E()V

    .line 2134885
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1f3a -> :sswitch_3
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWy",
            "<",
            "LX/EXf;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 2134886
    invoke-direct {p0, p1}, LX/EX1;-><init>(LX/EWy;)V

    .line 2134887
    iput-byte v0, p0, LX/EXf;->memoizedIsInitialized:B

    .line 2134888
    iput v0, p0, LX/EXf;->memoizedSerializedSize:I

    .line 2134889
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXf;->unknownFields:LX/EZQ;

    .line 2134890
    return-void
.end method

.method public static a(LX/EXf;)LX/EXe;
    .locals 1

    .prologue
    .line 2134891
    invoke-static {}, LX/EXe;->x()LX/EXe;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EXe;->a(LX/EXf;)LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2134892
    iput-boolean v0, p0, LX/EXf;->messageSetWireFormat_:Z

    .line 2134893
    iput-boolean v0, p0, LX/EXf;->noStandardDescriptorAccessor_:Z

    .line 2134894
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    .line 2134895
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2134896
    new-instance v0, LX/EXe;

    invoke-direct {v0, p1}, LX/EXe;-><init>(LX/EYd;)V

    .line 2134897
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 2134815
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2134816
    invoke-virtual {p0}, LX/EX1;->G()LX/EYf;

    move-result-object v2

    .line 2134817
    iget v0, p0, LX/EXf;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2134818
    iget-boolean v0, p0, LX/EXf;->messageSetWireFormat_:Z

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(IZ)V

    .line 2134819
    :cond_0
    iget v0, p0, LX/EXf;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 2134820
    iget-boolean v0, p0, LX/EXf;->noStandardDescriptorAccessor_:Z

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(IZ)V

    .line 2134821
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2134822
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2134823
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2134824
    :cond_2
    const/high16 v0, 0x20000000

    invoke-virtual {v2, v0, p1}, LX/EYf;->a(ILX/EWf;)V

    .line 2134825
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2134826
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2134827
    iget-byte v0, p0, LX/EXf;->memoizedIsInitialized:B

    .line 2134828
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2134829
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2134830
    :goto_1
    iget-object v3, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move v3, v3

    .line 2134831
    if-ge v0, v3, :cond_3

    .line 2134832
    iget-object v3, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EYB;

    move-object v3, v3

    .line 2134833
    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2134834
    iput-byte v1, p0, LX/EXf;->memoizedIsInitialized:B

    goto :goto_0

    .line 2134835
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2134836
    :cond_3
    invoke-virtual {p0}, LX/EX1;->F()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2134837
    iput-byte v1, p0, LX/EXf;->memoizedIsInitialized:B

    goto :goto_0

    .line 2134838
    :cond_4
    iput-byte v2, p0, LX/EXf;->memoizedIsInitialized:B

    move v1, v2

    .line 2134839
    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2134793
    iget v0, p0, LX/EXf;->memoizedSerializedSize:I

    .line 2134794
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2134795
    :goto_0
    return v0

    .line 2134796
    :cond_0
    iget v0, p0, LX/EXf;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 2134797
    iget-boolean v0, p0, LX/EXf;->messageSetWireFormat_:Z

    invoke-static {v3, v0}, LX/EWf;->b(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2134798
    :goto_1
    iget v2, p0, LX/EXf;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2134799
    iget-boolean v2, p0, LX/EXf;->noStandardDescriptorAccessor_:Z

    invoke-static {v4, v2}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v0

    .line 2134800
    :goto_2
    iget-object v0, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2134801
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2134802
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2134803
    :cond_2
    invoke-virtual {p0}, LX/EX1;->H()I

    move-result v0

    add-int/2addr v0, v2

    .line 2134804
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2134805
    iput v0, p0, LX/EXf;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2134814
    iget-object v0, p0, LX/EXf;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2134813
    sget-object v0, LX/EYC;->v:LX/EYn;

    const-class v1, LX/EXf;

    const-class v2, LX/EXe;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2134812
    sget-object v0, LX/EXf;->a:LX/EWZ;

    return-object v0
.end method

.method public final n()LX/EXe;
    .locals 1

    .prologue
    .line 2134811
    invoke-static {p0}, LX/EXf;->a(LX/EXf;)LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2134810
    invoke-virtual {p0}, LX/EXf;->n()LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2134809
    invoke-static {}, LX/EXe;->x()LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2134808
    invoke-virtual {p0}, LX/EXf;->n()LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2134807
    sget-object v0, LX/EXf;->c:LX/EXf;

    return-object v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2134806
    invoke-super {p0}, LX/EX1;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
