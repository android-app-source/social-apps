.class public LX/Cl7;
.super LX/Ci8;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Z

.field public b:Landroid/support/v7/widget/RecyclerView;

.field private c:LX/Chi;

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(LX/0Zb;LX/Chv;LX/Chi;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1932016
    invoke-direct {p0}, LX/Ci8;-><init>()V

    .line 1932017
    invoke-virtual {p2, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 1932018
    new-instance v0, LX/Cl6;

    invoke-direct {v0, p0}, LX/Cl6;-><init>(LX/Cl7;)V

    invoke-virtual {p2, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 1932019
    const-string v0, "android_native_article_perf"

    invoke-interface {p1, v0, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/Cl7;->a:Z

    .line 1932020
    iput-object p3, p0, LX/Cl7;->c:LX/Chi;

    .line 1932021
    return-void
.end method

.method public static a(LX/0QB;)LX/Cl7;
    .locals 6

    .prologue
    .line 1932005
    const-class v1, LX/Cl7;

    monitor-enter v1

    .line 1932006
    :try_start_0
    sget-object v0, LX/Cl7;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1932007
    sput-object v2, LX/Cl7;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1932008
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1932009
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1932010
    new-instance p0, LX/Cl7;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v4

    check-cast v4, LX/Chv;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v5

    check-cast v5, LX/Chi;

    invoke-direct {p0, v3, v4, v5}, LX/Cl7;-><init>(LX/0Zb;LX/Chv;LX/Chi;)V

    .line 1932011
    move-object v0, p0

    .line 1932012
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1932013
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Cl7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1932014
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1932015
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/Cl7;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1931984
    iget-boolean v0, p0, LX/Cl7;->a:Z

    if-nez v0, :cond_1

    .line 1931985
    :cond_0
    :goto_0
    return-void

    .line 1931986
    :cond_1
    iget-object v0, p0, LX/Cl7;->b:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cl7;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    instance-of v0, v0, LX/1P1;

    if-eqz v0, :cond_0

    .line 1931987
    iget-object v0, p0, LX/Cl7;->c:LX/Chi;

    .line 1931988
    iget v1, v0, LX/Chi;->l:I

    move v0, v1

    .line 1931989
    iput v0, p0, LX/Cl7;->d:I

    .line 1931990
    iget-object v0, p0, LX/Cl7;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 1931991
    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 1931992
    iget v2, p0, LX/Cl7;->e:I

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_0

    .line 1931993
    iget v1, p0, LX/Cl7;->e:I

    .line 1931994
    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v2

    iput v2, p0, LX/Cl7;->e:I

    .line 1931995
    iget v2, p0, LX/Cl7;->e:I

    if-eq v1, v2, :cond_2

    .line 1931996
    iput v3, p0, LX/Cl7;->g:I

    .line 1931997
    iput v3, p0, LX/Cl7;->f:I

    .line 1931998
    :cond_2
    iget v1, p0, LX/Cl7;->e:I

    iget v2, p0, LX/Cl7;->d:I

    if-ge v1, v2, :cond_0

    .line 1931999
    iget v1, p0, LX/Cl7;->e:I

    invoke-virtual {v0, v1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1932000
    if-eqz v0, :cond_0

    .line 1932001
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, p0, LX/Cl7;->f:I

    .line 1932002
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget-object v3, p0, LX/Cl7;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v3

    sub-int/2addr v0, v3

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1932003
    iget v1, p0, LX/Cl7;->g:I

    if-le v0, v1, :cond_0

    .line 1932004
    iput v0, p0, LX/Cl7;->g:I

    goto :goto_0
.end method


# virtual methods
.method public final b()F
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1931975
    iget v1, p0, LX/Cl7;->d:I

    if-nez v1, :cond_1

    .line 1931976
    const/4 v0, 0x0

    .line 1931977
    :cond_0
    :goto_0
    return v0

    .line 1931978
    :cond_1
    iget v1, p0, LX/Cl7;->e:I

    if-nez v1, :cond_2

    .line 1931979
    iget v1, p0, LX/Cl7;->g:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, LX/Cl7;->f:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, LX/Cl7;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0

    .line 1931980
    :cond_2
    iget v1, p0, LX/Cl7;->e:I

    iget v2, p0, LX/Cl7;->d:I

    if-ge v1, v2, :cond_0

    .line 1931981
    iget v1, p0, LX/Cl7;->e:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, LX/Cl7;->d:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, LX/Cl7;->g:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iget v2, p0, LX/Cl7;->f:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    iget v2, p0, LX/Cl7;->d:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public final b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1931982
    invoke-static {p0}, LX/Cl7;->d(LX/Cl7;)V

    .line 1931983
    return-void
.end method
