.class public final LX/CnX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;

.field public final synthetic b:LX/CnZ;


# direct methods
.method public constructor <init>(LX/CnZ;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;)V
    .locals 0

    .prologue
    .line 1933912
    iput-object p1, p0, LX/CnX;->b:LX/CnZ;

    iput-object p2, p0, LX/CnX;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x3caad053

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1933913
    iget-object v2, p0, LX/CnX;->b:LX/CnZ;

    iget-object v2, v2, LX/CnZ;->k:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v2, v3, :cond_0

    .line 1933914
    :goto_0
    iget-object v2, p0, LX/CnX;->b:LX/CnZ;

    invoke-static {v2}, LX/CnZ;->c(LX/CnZ;)V

    .line 1933915
    iget-object v2, p0, LX/CnX;->b:LX/CnZ;

    iget-object v2, v2, LX/CnZ;->d:LX/1Ck;

    const-string v3, "instant_article_follow_profile"

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CnX;->b:LX/CnZ;

    iget-object v0, v0, LX/CnZ;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K22;

    iget-object v4, p0, LX/CnX;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;

    invoke-virtual {v4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "UNDEFINED"

    .line 1933916
    iget-object p1, v0, LX/K22;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/2dj;

    invoke-virtual {p1, v4, v5}, LX/2dj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    move-object v0, p1

    .line 1933917
    :goto_1
    new-instance v4, LX/CnW;

    invoke-direct {v4, p0}, LX/CnW;-><init>(LX/CnX;)V

    invoke-virtual {v2, v3, v0, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1933918
    const v0, 0x6ef474a3

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1933919
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1933920
    :cond_1
    iget-object v0, p0, LX/CnX;->b:LX/CnZ;

    iget-object v0, v0, LX/CnZ;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K22;

    iget-object v4, p0, LX/CnX;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;

    invoke-virtual {v4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "UNDEFINED"

    .line 1933921
    iget-object p1, v0, LX/K22;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/2dj;

    invoke-virtual {p1, v4, v5}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    move-object v0, p1

    .line 1933922
    goto :goto_1
.end method
