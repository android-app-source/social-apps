.class public final LX/EKH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

.field public final synthetic b:LX/Cxh;

.field public final synthetic c:LX/EKJ;


# direct methods
.method public constructor <init>(LX/EKJ;Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;LX/Cxh;)V
    .locals 0

    .prologue
    .line 2106419
    iput-object p1, p0, LX/EKH;->c:LX/EKJ;

    iput-object p2, p0, LX/EKH;->a:Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    iput-object p3, p0, LX/EKH;->b:LX/Cxh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x46060761

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2106420
    iget-object v0, p0, LX/EKH;->a:Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2106421
    iget-object v0, p0, LX/EKH;->a:Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    .line 2106422
    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->g:LX/8dH;

    move-object v0, v3

    .line 2106423
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2106424
    :cond_0
    const v0, -0x5fd2afd5

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2106425
    :goto_0
    return-void

    .line 2106426
    :cond_1
    if-eqz v0, :cond_2

    .line 2106427
    iget-object v2, p0, LX/EKH;->c:LX/EKJ;

    iget-object v3, p0, LX/EKH;->a:Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    iget-object v0, p0, LX/EKH;->b:LX/Cxh;

    check-cast v0, LX/1Pn;

    .line 2106428
    iget-object v4, v2, LX/EKJ;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    new-instance v5, LX/EK9;

    sget-object p0, LX/EKJ;->e:LX/3ag;

    const p1, 0x7f0822b4

    invoke-direct {v5, v3, p0, p1}, LX/EK9;-><init>(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;LX/3ag;I)V

    invoke-virtual {v4, v0, v5}, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->a(LX/1Pn;LX/EK9;)V

    .line 2106429
    :goto_1
    const v0, 0x324c4a9a

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2106430
    :cond_2
    iget-object v0, p0, LX/EKH;->b:LX/Cxh;

    invoke-interface {v0, v2}, LX/Cxh;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2106431
    iget-object v3, p0, LX/EKH;->c:LX/EKJ;

    iget-object v0, p0, LX/EKH;->b:LX/Cxh;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, LX/EKJ;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    goto :goto_1
.end method
