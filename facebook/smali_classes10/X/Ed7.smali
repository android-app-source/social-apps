.class public LX/Ed7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ed0;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2148361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2148362
    iput-object p1, p0, LX/Ed7;->d:Landroid/content/Context;

    .line 2148363
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Ed7;->e:Landroid/util/SparseArray;

    .line 2148364
    return-void
.end method

.method private a(ILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2148365
    invoke-static {p0, p1, p2}, LX/Ed7;->c(LX/Ed7;ILandroid/os/Bundle;)V

    .line 2148366
    invoke-static {}, LX/EdL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2148367
    :try_start_0
    invoke-static {p1}, LX/EdL;->a(I)Landroid/telephony/SmsManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/SmsManager;->getCarrierConfigValues()Landroid/os/Bundle;

    move-result-object v0

    .line 2148368
    if-eqz v0, :cond_0

    .line 2148369
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2148370
    :cond_0
    :goto_0
    return-void

    .line 2148371
    :catch_0
    move-exception v0

    .line 2148372
    const-string v1, "MmsLib"

    const-string p0, "Calling system getCarrierConfigValues exception"

    invoke-static {v1, p0, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static c(LX/Ed7;ILandroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2148373
    iget-object v0, p0, LX/Ed7;->d:Landroid/content/Context;

    .line 2148374
    invoke-static {}, LX/EdL;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2148375
    :cond_0
    :goto_0
    move-object v1, v0

    .line 2148376
    const/4 v0, 0x0

    .line 2148377
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2148378
    :try_start_1
    new-instance v1, LX/Ed1;

    new-instance v2, LX/Ed6;

    invoke-direct {v2, p0, p2}, LX/Ed6;-><init>(LX/Ed7;Landroid/os/Bundle;)V

    invoke-direct {v1, v0, v2}, LX/Ed1;-><init>(Lorg/xmlpull/v1/XmlPullParser;LX/Ed6;)V

    invoke-virtual {v1}, LX/Ecy;->c()V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2148379
    if-eqz v0, :cond_1

    .line 2148380
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->close()V

    .line 2148381
    :cond_1
    :goto_1
    return-void

    .line 2148382
    :catch_0
    :try_start_2
    const-string v1, "MmsLib"

    const-string v2, "Can not get mms_config.xml"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2148383
    if-eqz v0, :cond_1

    .line 2148384
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_1

    .line 2148385
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_2
    if-eqz v1, :cond_2

    .line 2148386
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_2
    throw v0

    .line 2148387
    :catchall_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_2

    .line 2148388
    :cond_3
    invoke-static {v0, p1}, LX/EdL;->a(Landroid/content/Context;I)[I

    move-result-object v1

    .line 2148389
    const/4 v2, 0x0

    aget v2, v1, v2

    .line 2148390
    const/4 v3, 0x1

    aget v1, v1, v3

    .line 2148391
    if-nez v2, :cond_4

    if-eqz v1, :cond_0

    .line 2148392
    :cond_4
    new-instance v3, Landroid/content/res/Configuration;

    invoke-direct {v3}, Landroid/content/res/Configuration;-><init>()V

    .line 2148393
    iput v2, v3, Landroid/content/res/Configuration;->mcc:I

    .line 2148394
    iput v1, v3, Landroid/content/res/Configuration;->mnc:I

    .line 2148395
    invoke-virtual {v0, v3}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 2148396
    invoke-static {p1}, LX/EdL;->b(I)I

    move-result v2

    .line 2148397
    const/4 v1, 0x0

    .line 2148398
    monitor-enter p0

    .line 2148399
    :try_start_0
    iget-object v0, p0, LX/Ed7;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 2148400
    if-nez v0, :cond_0

    .line 2148401
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2148402
    iget-object v1, p0, LX/Ed7;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2148403
    invoke-direct {p0, v2, v0}, LX/Ed7;->a(ILandroid/os/Bundle;)V

    .line 2148404
    const/4 v1, 0x1

    .line 2148405
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2148406
    if-eqz v1, :cond_1

    .line 2148407
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Carrier configs loaded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2148408
    :cond_1
    return-object v0

    .line 2148409
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
