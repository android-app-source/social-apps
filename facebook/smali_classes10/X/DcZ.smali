.class public LX/DcZ;
.super LX/3Tf;
.source ""


# instance fields
.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Dca;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/DcW;


# direct methods
.method public constructor <init>(LX/DcW;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018554
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2018555
    iput-object p1, p0, LX/DcZ;->d:LX/DcW;

    .line 2018556
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/DcZ;->c:Ljava/util/List;

    .line 2018557
    return-void
.end method

.method private d(II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;
    .locals 1

    .prologue
    .line 2018579
    iget-object v0, p0, LX/DcZ;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dca;

    invoke-virtual {v0}, LX/Dca;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    return-object v0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2018578
    sget-object v0, LX/DcY;->HEADER:LX/DcY;

    invoke-virtual {v0}, LX/DcY;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2018558
    if-nez p4, :cond_1

    .line 2018559
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031404

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    move-object p4, v0

    .line 2018560
    :goto_0
    invoke-direct {p0, p1, p2}, LX/DcZ;->d(II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    move-result-object v1

    .line 2018561
    iget-object v2, p0, LX/DcZ;->d:LX/DcW;

    .line 2018562
    new-instance v0, LX/DcX;

    invoke-direct {v0, p0, p1, p2}, LX/DcX;-><init>(LX/DcZ;II)V

    move-object v3, v0

    .line 2018563
    if-nez p2, :cond_2

    const/4 v0, 0x1

    .line 2018564
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->e()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p4, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2018565
    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p4, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2018566
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->e()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p1, " "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p1, " "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-static {v1}, LX/DcW;->b(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    move-object p0, p0

    .line 2018567
    invoke-virtual {p4, p0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2018568
    invoke-static {v2, p4, v1, v3}, LX/DcW;->a(LX/DcW;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;LX/DcX;)V

    .line 2018569
    const/4 p1, 0x0

    .line 2018570
    if-eqz v0, :cond_3

    iget p0, v2, LX/DcW;->b:I

    .line 2018571
    :goto_2
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v3

    .line 2018572
    if-eqz v3, :cond_4

    move v1, p1

    :goto_3
    if-eqz v3, :cond_0

    iget p1, v2, LX/DcW;->d:I

    :cond_0
    invoke-virtual {p4}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p4, v1, p0, p1, v3}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setPadding(IIII)V

    .line 2018573
    return-object p4

    .line 2018574
    :cond_1
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    goto :goto_0

    .line 2018575
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2018576
    :cond_3
    iget p0, v2, LX/DcW;->c:I

    goto :goto_2

    .line 2018577
    :cond_4
    iget v1, v2, LX/DcW;->d:I

    goto :goto_3
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2018543
    check-cast p2, Landroid/widget/TextView;

    .line 2018544
    if-nez p2, :cond_0

    .line 2018545
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031406

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object p2, v0

    .line 2018546
    :cond_0
    iget-object v0, p0, LX/DcZ;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dca;

    invoke-virtual {v0}, LX/Dca;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2018547
    return-object p2
.end method

.method public final synthetic a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2018553
    invoke-direct {p0, p1, p2}, LX/DcZ;->d(II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2018580
    iget-object v0, p0, LX/DcZ;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dca;

    return-object v0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2018552
    const/4 v0, 0x0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2018551
    iget-object v0, p0, LX/DcZ;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 2018550
    iget-object v0, p0, LX/DcZ;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dca;

    invoke-virtual {v0}, LX/Dca;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final c(II)I
    .locals 1

    .prologue
    .line 2018549
    sget-object v0, LX/DcY;->MENU_ITEM:LX/DcY;

    invoke-virtual {v0}, LX/DcY;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2018548
    invoke-static {}, LX/DcY;->values()[LX/DcY;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
