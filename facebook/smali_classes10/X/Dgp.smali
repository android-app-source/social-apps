.class public final LX/Dgp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;)V
    .locals 0

    .prologue
    .line 2029914
    iput-object p1, p0, LX/Dgp;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2029911
    iget-object v0, p0, LX/Dgp;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->f:LX/Dgr;

    if-eqz v0, :cond_0

    .line 2029912
    iget-object v0, p0, LX/Dgp;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->f:LX/Dgr;

    invoke-interface {v0}, LX/Dgr;->a()V

    .line 2029913
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2029883
    check-cast p1, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel;

    .line 2029884
    iget-object v0, p0, LX/Dgp;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->f:LX/Dgr;

    if-eqz v0, :cond_1

    .line 2029885
    iget-object v0, p0, LX/Dgp;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->f:LX/Dgr;

    .line 2029886
    invoke-virtual {p1}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel;->a()Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel;->a()Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2029887
    :cond_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2029888
    :goto_0
    move-object v1, v1

    .line 2029889
    invoke-interface {v0, v1}, LX/Dgr;->a(LX/0Px;)V

    .line 2029890
    :cond_1
    return-void

    .line 2029891
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2029892
    invoke-virtual {p1}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel;->a()Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel;->a()LX/0Px;

    move-result-object v4

    .line 2029893
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel$EdgesModel;

    .line 2029894
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2029895
    if-nez v1, :cond_5

    .line 2029896
    :goto_2
    move v6, v8

    .line 2029897
    if-eqz v6, :cond_3

    .line 2029898
    invoke-static {v1}, LX/Dgn;->b(Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel$EdgesModel;)Lcom/facebook/messaging/location/sending/NearbyPlace;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2029899
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2029900
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_0

    .line 2029901
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel$EdgesModel;->j()Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel$EdgesModel$NodeModel;

    move-result-object p0

    .line 2029902
    if-eqz p0, :cond_7

    invoke-virtual {p0}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {p0}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 2029903
    invoke-virtual {p0}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel$EdgesModel$NodeModel;->n()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    .line 2029904
    if-eqz v6, :cond_6

    move v6, v7

    :goto_3
    if-eqz v6, :cond_9

    .line 2029905
    invoke-virtual {p0}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel$EdgesModel$NodeModel;->n()LX/1vs;

    move-result-object v6

    iget-object p1, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2029906
    invoke-virtual {p1, v6, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_8

    move v6, v7

    :goto_4
    if-eqz v6, :cond_b

    .line 2029907
    invoke-virtual {p0}, Lcom/facebook/messaging/location/sending/graphql/NearbyPlacesGraphQLModels$NearbyPlacesQueryModel$PlaceResultsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    .line 2029908
    if-eqz v6, :cond_a

    :goto_5
    move v8, v7

    .line 2029909
    goto :goto_2

    :cond_6
    move v6, v8

    .line 2029910
    goto :goto_3

    :cond_7
    move v6, v8

    goto :goto_3

    :cond_8
    move v6, v8

    goto :goto_4

    :cond_9
    move v6, v8

    goto :goto_4

    :cond_a
    move v7, v8

    goto :goto_5

    :cond_b
    move v7, v8

    goto :goto_5
.end method
