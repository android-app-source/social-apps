.class public final LX/Drq;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/BAa;

.field public final synthetic c:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic e:I

.field public final synthetic f:Landroid/content/Intent;

.field public final synthetic g:Lcom/facebook/notifications/model/SystemTrayNotification;

.field public final synthetic h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;ILX/BAa;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Lcom/facebook/graphql/model/GraphQLStory;ILandroid/content/Intent;Lcom/facebook/notifications/model/SystemTrayNotification;)V
    .locals 0

    .prologue
    .line 2049711
    iput-object p1, p0, LX/Drq;->h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iput p2, p0, LX/Drq;->a:I

    iput-object p3, p0, LX/Drq;->b:LX/BAa;

    iput-object p4, p0, LX/Drq;->c:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    iput-object p5, p0, LX/Drq;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iput p6, p0, LX/Drq;->e:I

    iput-object p7, p0, LX/Drq;->f:Landroid/content/Intent;

    iput-object p8, p0, LX/Drq;->g:Lcom/facebook/notifications/model/SystemTrayNotification;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/model/SystemTrayNotification;)LX/8D4;
    .locals 2

    .prologue
    .line 2049712
    invoke-virtual {p0}, Lcom/facebook/notifications/model/SystemTrayNotification;->a()Lcom/facebook/notifications/constants/NotificationType;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->NEKO_INSTALL_REMINDER:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v0, v1, :cond_0

    .line 2049713
    sget-object v0, LX/8D4;->THIRD_PARTY_ACTIVITY:LX/8D4;

    .line 2049714
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/8D4;->ACTIVITY:LX/8D4;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v6, 0x35000f

    .line 2049715
    iget-object v0, p0, LX/Drq;->h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/Drq;->a:I

    const/16 v2, 0xd6

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2049716
    if-eqz p1, :cond_0

    .line 2049717
    iget-object v0, p0, LX/Drq;->b:LX/BAa;

    invoke-virtual {v0, p1}, LX/BAa;->a(Landroid/graphics/Bitmap;)LX/BAa;

    .line 2049718
    iget-object v0, p0, LX/Drq;->c:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    const/4 v1, 0x1

    .line 2049719
    iput-boolean v1, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->m:Z

    .line 2049720
    iget-object v0, p0, LX/Drq;->d:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 2049721
    iget-object v0, p0, LX/Drq;->b:LX/BAa;

    iget-object v1, p0, LX/Drq;->h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v2, p0, LX/Drq;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1, v2, p1}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a$redex0(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/graphql/model/GraphQLStory;Landroid/graphics/Bitmap;)LX/3pw;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BAa;->a(LX/3pw;)LX/BAa;

    .line 2049722
    :cond_0
    iget-object v0, p0, LX/Drq;->h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->k:LX/2c4;

    iget v1, p0, LX/Drq;->e:I

    iget-object v2, p0, LX/Drq;->b:LX/BAa;

    iget-object v3, p0, LX/Drq;->f:Landroid/content/Intent;

    iget-object v4, p0, LX/Drq;->g:Lcom/facebook/notifications/model/SystemTrayNotification;

    invoke-static {v4}, LX/Drq;->a(Lcom/facebook/notifications/model/SystemTrayNotification;)LX/8D4;

    move-result-object v4

    iget-object v5, p0, LX/Drq;->c:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-virtual/range {v0 .. v5}, LX/2c4;->a(ILX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 2049723
    iget-object v0, p0, LX/Drq;->h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/Drq;->a:I

    const/16 v2, 0xd5

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2049724
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const v6, 0x35000f

    .line 2049725
    iget-object v0, p0, LX/Drq;->h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/Drq;->a:I

    const/16 v2, 0xd7

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2049726
    iget-object v0, p0, LX/Drq;->d:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 2049727
    iget-object v0, p0, LX/Drq;->b:LX/BAa;

    iget-object v1, p0, LX/Drq;->h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v2, p0, LX/Drq;->d:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a$redex0(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Lcom/facebook/graphql/model/GraphQLStory;Landroid/graphics/Bitmap;)LX/3pw;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BAa;->a(LX/3pw;)LX/BAa;

    .line 2049728
    :cond_0
    iget-object v0, p0, LX/Drq;->h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->k:LX/2c4;

    iget v1, p0, LX/Drq;->e:I

    iget-object v2, p0, LX/Drq;->b:LX/BAa;

    iget-object v3, p0, LX/Drq;->f:Landroid/content/Intent;

    iget-object v4, p0, LX/Drq;->g:Lcom/facebook/notifications/model/SystemTrayNotification;

    invoke-static {v4}, LX/Drq;->a(Lcom/facebook/notifications/model/SystemTrayNotification;)LX/8D4;

    move-result-object v4

    iget-object v5, p0, LX/Drq;->c:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-virtual/range {v0 .. v5}, LX/2c4;->a(ILX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 2049729
    iget-object v0, p0, LX/Drq;->h:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/Drq;->a:I

    const/16 v2, 0xd5

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2049730
    return-void
.end method
