.class public final LX/DHv;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:Landroid/widget/PopupWindow;

.field public final synthetic b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;Landroid/widget/PopupWindow;)V
    .locals 0

    .prologue
    .line 1982605
    iput-object p1, p0, LX/DHv;->b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iput-object p2, p0, LX/DHv;->a:Landroid/widget/PopupWindow;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 1982608
    iget-object v0, p0, LX/DHv;->b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iget-object v0, v0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->c:LX/3OL;

    if-eqz v0, :cond_0

    .line 1982609
    iget-object v0, p0, LX/DHv;->b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iget-object v1, p0, LX/DHv;->b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iget-object v1, v1, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->c:LX/3OL;

    .line 1982610
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->a$redex0(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;LX/3OL;)V

    .line 1982611
    iget-object v0, p0, LX/DHv;->b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    const/4 v1, 0x0

    .line 1982612
    iput-object v1, v0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->c:LX/3OL;

    .line 1982613
    :goto_0
    iget-object v0, p0, LX/DHv;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1982614
    iget-object v0, p0, LX/DHv;->b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setVisibility(I)V

    .line 1982615
    return-void

    .line 1982616
    :cond_0
    iget-object v1, p0, LX/DHv;->b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iget-object v0, p0, LX/DHv;->b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    iget-object v0, v0, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->b:LX/3OL;

    sget-object v2, LX/3OL;->NOT_SENT:LX/3OL;

    if-ne v0, v2, :cond_1

    sget-object v0, LX/3OL;->SENT_UNDOABLE:LX/3OL;

    .line 1982617
    :goto_1
    invoke-static {v1, v0}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->a$redex0(Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;LX/3OL;)V

    .line 1982618
    goto :goto_0

    :cond_1
    sget-object v0, LX/3OL;->INTERACTED:LX/3OL;

    goto :goto_1
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1982606
    iget-object v0, p0, LX/DHv;->b:Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/waves/FriendsNearbyWaveView;->setVisibility(I)V

    .line 1982607
    return-void
.end method
