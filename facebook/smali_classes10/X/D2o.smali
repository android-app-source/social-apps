.class public LX/D2o;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/view/View;

.field public final c:Landroid/view/View;

.field public final d:Landroid/widget/ImageView;

.field public final e:LX/D2l;

.field public f:Landroid/animation/Animator;

.field public final g:Landroid/graphics/Rect;

.field public final h:Landroid/graphics/Rect;

.field public final i:Landroid/graphics/Rect;

.field public final j:Landroid/graphics/Point;

.field public k:F

.field public final l:Landroid/animation/Animator$AnimatorListener;

.field public final m:Landroid/animation/Animator$AnimatorListener;

.field public n:Z

.field public final o:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/D2l;)V
    .locals 2

    .prologue
    .line 1959306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1959307
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/D2o;->g:Landroid/graphics/Rect;

    .line 1959308
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/D2o;->h:Landroid/graphics/Rect;

    .line 1959309
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/D2o;->i:Landroid/graphics/Rect;

    .line 1959310
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/D2o;->j:Landroid/graphics/Point;

    .line 1959311
    new-instance v0, LX/D2m;

    invoke-direct {v0, p0}, LX/D2m;-><init>(LX/D2o;)V

    iput-object v0, p0, LX/D2o;->l:Landroid/animation/Animator$AnimatorListener;

    .line 1959312
    new-instance v0, LX/D2n;

    invoke-direct {v0, p0}, LX/D2n;-><init>(LX/D2o;)V

    iput-object v0, p0, LX/D2o;->m:Landroid/animation/Animator$AnimatorListener;

    .line 1959313
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D2o;->n:Z

    .line 1959314
    new-instance v0, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedAnimator$3;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedAnimator$3;-><init>(LX/D2o;)V

    iput-object v0, p0, LX/D2o;->o:Ljava/lang/Runnable;

    .line 1959315
    iput-object p1, p0, LX/D2o;->a:Landroid/view/View;

    .line 1959316
    const v0, 0x7f0d2782

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D2o;->b:Landroid/view/View;

    .line 1959317
    iget-object v0, p0, LX/D2o;->b:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1959318
    const v0, 0x7f0d2785

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D2o;->c:Landroid/view/View;

    .line 1959319
    const v0, 0x7f0d2784

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/D2o;->d:Landroid/widget/ImageView;

    .line 1959320
    iget-object v0, p0, LX/D2o;->d:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1959321
    iput-object p2, p0, LX/D2o;->e:LX/D2l;

    .line 1959322
    return-void
.end method
