.class public LX/EtD;
.super LX/EtB;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EtB",
        "<",
        "Landroid/graphics/Bitmap;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:LX/1Er;

.field public final e:LX/9iU;

.field private final f:Landroid/net/Uri;

.field private final g:Landroid/net/Uri;

.field private final h:Lcom/facebook/common/callercontext/CallerContext;

.field private final i:LX/1HI;

.field private j:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/EtA;LX/1Er;LX/9iU;Ljava/util/concurrent/Executor;Landroid/net/Uri;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EtA",
            "<",
            "Landroid/net/Uri;",
            "*>;",
            "LX/1Er;",
            "LX/9iU;",
            "Ljava/util/concurrent/Executor;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2177389
    const/4 v0, 0x0

    invoke-direct {p0, p1, p4, v0}, LX/EtB;-><init>(LX/EtA;Ljava/util/concurrent/Executor;Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;)V

    .line 2177390
    iput-object p5, p0, LX/EtD;->f:Landroid/net/Uri;

    .line 2177391
    iput-object p6, p0, LX/EtD;->g:Landroid/net/Uri;

    .line 2177392
    iput-object p2, p0, LX/EtD;->d:LX/1Er;

    .line 2177393
    iput-object p3, p0, LX/EtD;->e:LX/9iU;

    .line 2177394
    iput-object p7, p0, LX/EtD;->h:Lcom/facebook/common/callercontext/CallerContext;

    .line 2177395
    invoke-static {}, LX/4AN;->b()LX/1HI;

    move-result-object v0

    iput-object v0, p0, LX/EtD;->i:LX/1HI;

    .line 2177396
    return-void
.end method

.method private static declared-synchronized a(LX/EtD;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 2177374
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EtB;->c:Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;

    .line 2177375
    iput-object p1, v0, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;->a:Ljava/lang/Object;

    .line 2177376
    iget-object v0, p0, LX/EtB;->c:Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;->run()V

    .line 2177377
    iget-object v0, p0, LX/EtB;->c:Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;

    .line 2177378
    iget-object v1, v0, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 2177379
    check-cast v0, Landroid/net/Uri;

    .line 2177380
    const/4 v1, 0x0

    iput-object v1, p0, LX/EtD;->j:LX/1ca;

    .line 2177381
    if-eqz v0, :cond_0

    .line 2177382
    iget-object v1, p0, LX/EtB;->a:LX/EtA;

    .line 2177383
    invoke-static {}, LX/EtA;->n()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2177384
    invoke-virtual {v1, v0}, LX/EtA;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2177385
    :goto_0
    monitor-exit p0

    return-void

    .line 2177386
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/EtB;->a:LX/EtA;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EtA;->d(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2177387
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2177388
    :cond_1
    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v2

    new-instance v3, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkController$3;

    invoke-direct {v3, v1, v0}, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkController$3;-><init>(LX/EtA;Ljava/lang/Object;)V

    const p1, 0xdadb875

    invoke-static {v2, v3, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method private static declared-synchronized a(LX/EtD;LX/1ca;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2177373
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/EtD;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EtD;->j:LX/1ca;

    if-ne v0, p1, :cond_0

    invoke-interface {p1}, LX/1ca;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/EtD;Landroid/graphics/Bitmap;LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "LX/1ca",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2177337
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p2}, LX/EtD;->a(LX/EtD;LX/1ca;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2177338
    :goto_0
    monitor-exit p0

    return-void

    .line 2177339
    :cond_0
    :try_start_1
    invoke-static {p0, p1}, LX/EtD;->a(LX/EtD;Landroid/graphics/Bitmap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2177340
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/EtD;ZLX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/1ca",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2177366
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p2}, LX/EtD;->a(LX/EtD;LX/1ca;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2177367
    :goto_0
    monitor-exit p0

    return-void

    .line 2177368
    :cond_0
    if-eqz p1, :cond_1

    .line 2177369
    :try_start_1
    invoke-static {p0}, LX/EtD;->f(LX/EtD;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2177370
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2177371
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, LX/EtD;->j:LX/1ca;

    .line 2177372
    iget-object v0, p0, LX/EtB;->a:LX/EtA;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EtA;->d(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 2177365
    iget-object v0, p0, LX/EtD;->j:LX/1ca;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized f(LX/EtD;)V
    .locals 4

    .prologue
    .line 2177356
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EtB;->c:Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;

    if-nez v0, :cond_0

    .line 2177357
    new-instance v0, Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;-><init>(LX/EtD;)V

    .line 2177358
    iput-object v0, p0, LX/EtB;->c:Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;

    .line 2177359
    :cond_0
    invoke-virtual {p0}, LX/EtB;->b()V

    .line 2177360
    iget-object v0, p0, LX/EtD;->g:Landroid/net/Uri;

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2177361
    iget-object v1, p0, LX/EtD;->i:LX/1HI;

    iget-object v2, p0, LX/EtD;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    iput-object v0, p0, LX/EtD;->j:LX/1ca;

    .line 2177362
    iget-object v0, p0, LX/EtD;->j:LX/1ca;

    new-instance v1, LX/EtC;

    iget-object v2, p0, LX/EtD;->j:LX/1ca;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, LX/EtC;-><init>(LX/EtD;LX/1ca;Z)V

    iget-object v2, p0, LX/EtB;->b:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2177363
    monitor-exit p0

    return-void

    .line 2177364
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 2177349
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;-><init>(LX/EtD;)V

    .line 2177350
    iput-object v0, p0, LX/EtB;->c:Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;

    .line 2177351
    iget-object v0, p0, LX/EtD;->f:Landroid/net/Uri;

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2177352
    iget-object v1, p0, LX/EtD;->i:LX/1HI;

    iget-object v2, p0, LX/EtD;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    iput-object v0, p0, LX/EtD;->j:LX/1ca;

    .line 2177353
    iget-object v0, p0, LX/EtD;->j:LX/1ca;

    new-instance v1, LX/EtC;

    iget-object v2, p0, LX/EtD;->j:LX/1ca;

    const/4 v3, 0x1

    invoke-direct {v1, p0, v2, v3}, LX/EtC;-><init>(LX/EtD;LX/1ca;Z)V

    iget-object v2, p0, LX/EtB;->b:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2177354
    monitor-exit p0

    return-void

    .line 2177355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 2177344
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/EtD;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2177345
    iget-object v0, p0, LX/EtD;->j:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 2177346
    const/4 v0, 0x0

    iput-object v0, p0, LX/EtD;->j:LX/1ca;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2177347
    :cond_0
    monitor-exit p0

    return-void

    .line 2177348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2177342
    invoke-static {p0}, LX/EtD;->f(LX/EtD;)V

    .line 2177343
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2177341
    invoke-direct {p0}, LX/EtD;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
