.class public LX/DSf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DSV;


# instance fields
.field public final a:LX/DSb;

.field private final b:LX/DSc;

.field public final c:LX/DSe;

.field public final d:LX/DUV;

.field public final e:LX/DS2;

.field public final f:LX/DSd;

.field public final g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

.field private final h:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;


# direct methods
.method public constructor <init>(LX/DUV;LX/DSb;LX/DSc;LX/DS2;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1999812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1999813
    iput-object p1, p0, LX/DSf;->d:LX/DUV;

    .line 1999814
    iput-object p2, p0, LX/DSf;->a:LX/DSb;

    .line 1999815
    iput-object p3, p0, LX/DSf;->b:LX/DSc;

    .line 1999816
    invoke-interface {p1}, LX/DUU;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/DSe;->FRIEND:LX/DSe;

    :goto_0
    iput-object v0, p0, LX/DSf;->c:LX/DSe;

    .line 1999817
    iput-object p4, p0, LX/DSf;->e:LX/DS2;

    .line 1999818
    sget-object v0, LX/DSd;->NONE:LX/DSd;

    iput-object v0, p0, LX/DSf;->f:LX/DSd;

    .line 1999819
    iput-object v1, p0, LX/DSf;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    .line 1999820
    iput-object v1, p0, LX/DSf;->h:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;

    .line 1999821
    return-void

    .line 1999822
    :cond_0
    sget-object v0, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    goto :goto_0
.end method

.method public constructor <init>(LX/DUV;LX/DSb;LX/DSc;LX/DS2;LX/DSd;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;LX/DSe;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;)V
    .locals 0

    .prologue
    .line 1999802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1999803
    iput-object p1, p0, LX/DSf;->d:LX/DUV;

    .line 1999804
    iput-object p2, p0, LX/DSf;->a:LX/DSb;

    .line 1999805
    iput-object p3, p0, LX/DSf;->b:LX/DSc;

    .line 1999806
    iput-object p4, p0, LX/DSf;->e:LX/DS2;

    .line 1999807
    iput-object p5, p0, LX/DSf;->f:LX/DSd;

    .line 1999808
    iput-object p6, p0, LX/DSf;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    .line 1999809
    iput-object p7, p0, LX/DSf;->c:LX/DSe;

    .line 1999810
    iput-object p8, p0, LX/DSf;->h:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;

    .line 1999811
    return-void
.end method

.method public constructor <init>(LX/DUV;LX/DSb;LX/DSc;LX/DSe;LX/DS2;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1999792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1999793
    iput-object p1, p0, LX/DSf;->d:LX/DUV;

    .line 1999794
    iput-object p2, p0, LX/DSf;->a:LX/DSb;

    .line 1999795
    iput-object p3, p0, LX/DSf;->b:LX/DSc;

    .line 1999796
    iput-object p4, p0, LX/DSf;->c:LX/DSe;

    .line 1999797
    iput-object p5, p0, LX/DSf;->e:LX/DS2;

    .line 1999798
    sget-object v0, LX/DSd;->NONE:LX/DSd;

    iput-object v0, p0, LX/DSf;->f:LX/DSd;

    .line 1999799
    iput-object v1, p0, LX/DSf;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    .line 1999800
    iput-object v1, p0, LX/DSf;->h:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;

    .line 1999801
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1999791
    iget-object v0, p0, LX/DSf;->d:LX/DUV;

    invoke-interface {v0}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/DSa;
    .locals 1

    .prologue
    .line 1999790
    sget-object v0, LX/DSa;->GroupMemberRow:LX/DSa;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1999785
    iget-object v0, p0, LX/DSf;->e:LX/DS2;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1999786
    iget-object v0, p0, LX/DSf;->e:LX/DS2;

    .line 1999787
    iget-object p0, v0, LX/DS2;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1999788
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1999789
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1999784
    iget-object v0, p0, LX/DSf;->a:LX/DSb;

    sget-object v1, LX/DSb;->ADMIN:LX/DSb;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1999783
    iget-object v0, p0, LX/DSf;->a:LX/DSb;

    sget-object v1, LX/DSb;->MODERATOR:LX/DSb;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1999776
    if-ne p0, p1, :cond_0

    .line 1999777
    const/4 v0, 0x1

    .line 1999778
    :goto_0
    return v0

    .line 1999779
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 1999780
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1999781
    :cond_2
    check-cast p1, LX/DSf;

    invoke-direct {p1}, LX/DSf;->m()Ljava/lang/String;

    move-result-object v0

    .line 1999782
    invoke-direct {p0}, LX/DSf;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 1999775
    iget-object v0, p0, LX/DSf;->b:LX/DSc;

    sget-object v1, LX/DSc;->BLOCKED:LX/DSc;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1999767
    iget-object v1, p0, LX/DSf;->f:LX/DSd;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/DSf;->f:LX/DSd;

    invoke-virtual {v1}, LX/DSd;->name()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1999768
    iget-object v1, p0, LX/DSf;->f:LX/DSd;

    invoke-virtual {v1}, LX/DSd;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/DSd;->NONE:LX/DSd;

    invoke-virtual {v2}, LX/DSd;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1999769
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1999772
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 1999773
    iget-object v2, p0, LX/DSf;->d:LX/DUV;

    move-object v2, v2

    .line 1999774
    invoke-interface {v2}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1999771
    iget-object v0, p0, LX/DSf;->h:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DSf;->h:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1999770
    iget-object v0, p0, LX/DSf;->d:LX/DUV;

    invoke-interface {v0}, LX/DUU;->kA_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
