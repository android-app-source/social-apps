.class public final LX/Cri;
.super LX/Crh;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/CnP;",
        ">",
        "LX/Crh",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final e:Landroid/view/LayoutInflater;

.field private final f:I


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;IILandroid/view/LayoutInflater;)V
    .locals 2

    .prologue
    .line 1941318
    invoke-direct {p0, p1, p2}, LX/Crh;-><init>(Landroid/view/ViewGroup;I)V

    .line 1941319
    if-nez p3, :cond_0

    .line 1941320
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Provide valid layout resource id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1941321
    :cond_0
    if-nez p4, :cond_1

    .line 1941322
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Layout inflater cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1941323
    :cond_1
    iput-object p4, p0, LX/Cri;->e:Landroid/view/LayoutInflater;

    .line 1941324
    iput p3, p0, LX/Cri;->f:I

    .line 1941325
    return-void
.end method


# virtual methods
.method public final b()LX/CnP;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 1941326
    iget-object v0, p0, LX/Cri;->e:Landroid/view/LayoutInflater;

    iget v1, p0, LX/Cri;->f:I

    iget-object v2, p0, LX/Crh;->d:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CnP;

    .line 1941327
    iget-object v1, p0, LX/Crh;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1941328
    return-object v0
.end method
