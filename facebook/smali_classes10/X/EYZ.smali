.class public LX/EYZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static volatile a:Z

.field public static final c:LX/EYZ;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object",
            "<**>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2137580
    sput-boolean v1, LX/EYZ;->a:Z

    .line 2137581
    new-instance v0, LX/EYZ;

    invoke-direct {v0, v1}, LX/EYZ;-><init>(B)V

    sput-object v0, LX/EYZ;->c:LX/EYZ;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2137582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137583
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EYZ;->b:Ljava/util/Map;

    .line 2137584
    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    .prologue
    .line 2137585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137586
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/EYZ;->b:Ljava/util/Map;

    .line 2137587
    return-void
.end method

.method public constructor <init>(LX/EYZ;)V
    .locals 1

    .prologue
    .line 2137588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137589
    sget-object v0, LX/EYZ;->c:LX/EYZ;

    if-ne p1, v0, :cond_0

    .line 2137590
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/EYZ;->b:Ljava/util/Map;

    .line 2137591
    :goto_0
    return-void

    .line 2137592
    :cond_0
    iget-object v0, p1, LX/EYZ;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/EYZ;->b:Ljava/util/Map;

    goto :goto_0
.end method
