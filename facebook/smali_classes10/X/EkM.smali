.class public final LX/EkM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:I

.field public static b:I

.field private static c:J

.field private static d:J

.field private static e:Ljava/lang/String;

.field private static f:Ljava/lang/String;

.field private static volatile g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 2163323
    sput v1, LX/EkM;->a:I

    .line 2163324
    sput v1, LX/EkM;->b:I

    .line 2163325
    sput-wide v2, LX/EkM;->c:J

    .line 2163326
    sput-wide v2, LX/EkM;->d:J

    .line 2163327
    const-string v0, "Unknown"

    sput-object v0, LX/EkM;->e:Ljava/lang/String;

    .line 2163328
    const-string v0, "Unknown"

    sput-object v0, LX/EkM;->f:Ljava/lang/String;

    .line 2163329
    sput-boolean v1, LX/EkM;->g:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2163330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163331
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2163332
    sget-boolean v0, LX/EkM;->g:Z

    if-eqz v0, :cond_0

    .line 2163333
    :goto_0
    return-void

    .line 2163334
    :cond_0
    const-class v1, LX/EkM;

    monitor-enter v1

    .line 2163335
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2163336
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2163337
    if-eqz v0, :cond_1

    .line 2163338
    :try_start_2
    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    sput v2, LX/EkM;->a:I

    .line 2163339
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v2, LX/EkM;->e:Ljava/lang/String;

    .line 2163340
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    sput v2, LX/EkM;->b:I

    .line 2163341
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    sput-object v2, LX/EkM;->f:Ljava/lang/String;

    .line 2163342
    iget-wide v2, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    sput-wide v2, LX/EkM;->c:J

    .line 2163343
    iget-wide v2, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    sput-wide v2, LX/EkM;->d:J

    .line 2163344
    const/4 v0, 0x1

    sput-boolean v0, LX/EkM;->g:Z

    .line 2163345
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2163346
    :catch_0
    move-exception v0

    .line 2163347
    :try_start_3
    const-string v2, "ApplicationManifestHelper"

    const-string v3, "Failed to get package info for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2163348
    monitor-exit v1

    goto :goto_0

    .line 2163349
    :cond_1
    const-string v0, "ApplicationManifestHelper"

    const-string v2, "Package info for %s is null"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
