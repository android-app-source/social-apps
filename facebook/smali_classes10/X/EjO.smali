.class public LX/EjO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ContactUploadModifier;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2161304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2161305
    const/4 v0, 0x0

    iput-object v0, p0, LX/EjO;->g:Ljava/lang/String;

    .line 2161306
    iput-object p1, p0, LX/EjO;->a:Ljava/lang/String;

    .line 2161307
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/EjO;->b:Ljava/util/Set;

    .line 2161308
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/EjO;->c:Ljava/util/Set;

    .line 2161309
    return-void
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/EjO;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/4Dj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2161310
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2161311
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjO;

    .line 2161312
    new-instance v4, Ljava/util/ArrayList;

    iget-object v3, v0, LX/EjO;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2161313
    iget-object v3, v0, LX/EjO;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2161314
    new-instance v6, LX/4Dl;

    invoke-direct {v6}, LX/4Dl;-><init>()V

    .line 2161315
    const-string v7, "value"

    invoke-virtual {v6, v7, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161316
    move-object v3, v6

    .line 2161317
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2161318
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    iget-object v3, v0, LX/EjO;->b:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2161319
    iget-object v3, v0, LX/EjO;->b:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2161320
    new-instance v7, LX/4Dk;

    invoke-direct {v7}, LX/4Dk;-><init>()V

    .line 2161321
    const-string p0, "value"

    invoke-virtual {v7, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161322
    move-object v3, v7

    .line 2161323
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2161324
    :cond_1
    new-instance v3, LX/4Dj;

    invoke-direct {v3}, LX/4Dj;-><init>()V

    iget-object v6, v0, LX/EjO;->a:Ljava/lang/String;

    .line 2161325
    const-string v7, "record_id"

    invoke-virtual {v3, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161326
    move-object v3, v3

    .line 2161327
    iget-object v6, v0, LX/EjO;->g:Ljava/lang/String;

    .line 2161328
    const-string v7, "modifier"

    invoke-virtual {v3, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161329
    move-object v3, v3

    .line 2161330
    iget-object v6, v0, LX/EjO;->d:Ljava/lang/String;

    .line 2161331
    const-string v7, "name"

    invoke-virtual {v3, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161332
    move-object v3, v3

    .line 2161333
    iget-object v6, v0, LX/EjO;->e:Ljava/lang/String;

    .line 2161334
    const-string v7, "first_name"

    invoke-virtual {v3, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161335
    move-object v3, v3

    .line 2161336
    iget-object v6, v0, LX/EjO;->f:Ljava/lang/String;

    .line 2161337
    const-string v7, "last_name"

    invoke-virtual {v3, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161338
    move-object v3, v3

    .line 2161339
    const-string v6, "phones"

    invoke-virtual {v3, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2161340
    move-object v3, v3

    .line 2161341
    const-string v4, "emails"

    invoke-virtual {v3, v4, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2161342
    move-object v3, v3

    .line 2161343
    invoke-virtual {v0}, LX/EjO;->c()Ljava/lang/String;

    move-result-object v4

    .line 2161344
    const-string v5, "minimal_hash"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161345
    move-object v3, v3

    .line 2161346
    invoke-virtual {v0}, LX/EjO;->c()Ljava/lang/String;

    move-result-object v4

    .line 2161347
    const-string v5, "extended_hash"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2161348
    move-object v3, v3

    .line 2161349
    move-object v0, v3

    .line 2161350
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2161351
    :cond_2
    return-object v1
.end method


# virtual methods
.method public final a()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 2161352
    iget-object v0, p0, LX/EjO;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2161353
    sget-object v0, LX/51s;->a:LX/51l;

    move-object v0, v0

    .line 2161354
    invoke-virtual {p0}, LX/EjO;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2aP;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1, v2}, LX/51l;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2161355
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EjO;->c:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2161356
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/EjO;->b:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2161357
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2161358
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2161359
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, LX/EjO;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/EjO;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/EjO;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, LX/EjO;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, ","

    invoke-static {v4, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x5

    const-string v3, ","

    invoke-static {v3, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 2161360
    const-string v0, ";"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
