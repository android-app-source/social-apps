.class public final LX/DRs;
.super LX/DRr;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/GroupAdminListFragment;)V
    .locals 0

    .prologue
    .line 1998569
    iput-object p1, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    invoke-direct {p0}, LX/DRr;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1998570
    check-cast p1, LX/DTi;

    .line 1998571
    iget-object v0, p1, LX/DTi;->a:Ljava/lang/String;

    iget-object v1, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    .line 1998572
    iget-object v2, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1998573
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1998574
    :goto_0
    return-void

    .line 1998575
    :cond_0
    iget-object v0, p1, LX/DTi;->b:Ljava/lang/String;

    iget-object v1, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1998576
    iget-object v0, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    iget-object v1, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1998577
    iput-object v1, v0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1998578
    iget-object v0, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    iget-object v1, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    .line 1998579
    iget-object v2, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object v1, v2

    .line 1998580
    iput-object v1, v0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1998581
    iget-object v1, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    iget-object v0, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    invoke-virtual {v0}, LX/DTZ;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c(Z)V

    .line 1998582
    iget-object v0, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    .line 1998583
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 1998584
    if-eqz v0, :cond_1

    .line 1998585
    const-string v1, "group_admin_type"

    iget-object v2, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1998586
    :cond_1
    iget-object v0, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    .line 1998587
    iget-object v1, v0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->z:LX/DSY;

    invoke-virtual {v1}, LX/DSY;->c()V

    .line 1998588
    iget-object v1, v0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->y:LX/DSp;

    const v2, -0x47e7efc1

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1998589
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->p()V

    .line 1998590
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->C()V

    .line 1998591
    iget-object v0, p0, LX/DRs;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o()V

    goto :goto_0

    .line 1998592
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
