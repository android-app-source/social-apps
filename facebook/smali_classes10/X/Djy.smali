.class public final LX/Djy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Djz;


# direct methods
.method public constructor <init>(LX/Djz;)V
    .locals 0

    .prologue
    .line 2033877
    iput-object p1, p0, LX/Djy;->a:LX/Djz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033878
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2033879
    if-eqz p1, :cond_0

    .line 2033880
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2033881
    if-eqz v0, :cond_0

    .line 2033882
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2033883
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel$NativeBookingRequestModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2033884
    :cond_0
    iget-object v0, p0, LX/Djy;->a:LX/Djz;

    iget-object v0, v0, LX/Djz;->e:LX/03V;

    const-string v1, "appointment calendar"

    const-string v2, "appointments are null"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033885
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2033886
    :goto_0
    return-object v0

    .line 2033887
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2033888
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel$NativeBookingRequestModel;

    move-result-object v0

    .line 2033889
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel$NativeBookingRequestModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2033890
    iget-object v3, p0, LX/Djy;->a:LX/Djz;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v4}, LX/15i;->h(II)Z

    move-result v4

    .line 2033891
    iput-boolean v4, v3, LX/Djz;->f:Z

    .line 2033892
    iget-object v3, p0, LX/Djy;->a:LX/Djz;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2033893
    iput-object v1, v3, LX/Djz;->g:Ljava/lang/String;

    .line 2033894
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAppointmentsCalendarQueryModel$NativeBookingRequestModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2033895
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
