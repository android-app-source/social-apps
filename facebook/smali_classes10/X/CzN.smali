.class public LX/CzN;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSavedState;",
            "Lcom/facebook/graphql/enums/GraphQLSavedState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1954849
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/CzN;->a:LX/0P1;

    .line 1954850
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/CzN;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1954848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/CzL;)LX/CzL;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;)",
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1954752
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v0

    .line 1954753
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1954754
    check-cast v0, LX/8d0;

    .line 1954755
    instance-of v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    if-eqz v4, :cond_1

    move-object v1, v0

    .line 1954756
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1954757
    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v1

    .line 1954758
    invoke-static {v1}, LX/8dX;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)LX/8dX;

    move-result-object v1

    invoke-interface {v0}, LX/8d0;->w()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 1954759
    :goto_0
    iput-boolean v0, v1, LX/8dX;->u:Z

    .line 1954760
    move-object v0, v1

    .line 1954761
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1954762
    :goto_1
    invoke-virtual {p0, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v3

    .line 1954763
    goto :goto_0

    .line 1954764
    :cond_1
    instance-of v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    if-eqz v4, :cond_3

    .line 1954765
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v1

    invoke-interface {v0}, LX/8d0;->w()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1954766
    :goto_2
    iput-boolean v2, v1, LX/8dQ;->r:Z

    .line 1954767
    move-object v0, v1

    .line 1954768
    invoke-virtual {v0}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    .line 1954769
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 4

    .prologue
    .line 1954851
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 1954852
    :goto_0
    sparse-switch v0, :sswitch_data_0

    .line 1954853
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported node type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1954854
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1954855
    :sswitch_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 1954856
    sget-object v1, LX/CzN;->a:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1954857
    if-nez v0, :cond_2

    .line 1954858
    :goto_1
    move-object v0, p0

    .line 1954859
    :goto_2
    return-object v0

    .line 1954860
    :sswitch_1
    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cc()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 1954861
    :goto_3
    iput-boolean v0, v1, LX/4XR;->ds:Z

    .line 1954862
    move-object v0, v1

    .line 1954863
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    move-object v0, v0

    .line 1954864
    goto :goto_2

    .line 1954865
    :sswitch_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kO()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v1, :cond_4

    :goto_4
    move-object v0, p0

    .line 1954866
    goto :goto_2

    .line 1954867
    :sswitch_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bx()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v0

    .line 1954868
    if-eqz v0, :cond_6

    .line 1954869
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kK()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v0, v1, :cond_5

    .line 1954870
    :cond_1
    :goto_5
    move-object v0, p0

    .line 1954871
    goto :goto_2

    :cond_2
    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    .line 1954872
    iput-object v0, v1, LX/4XR;->fb:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1954873
    move-object v0, v1

    .line 1954874
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1954875
    iput-object v1, v0, LX/4XR;->pC:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1954876
    move-object v0, v0

    .line 1954877
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_4

    .line 1954878
    :cond_5
    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1954879
    iput-object v1, v0, LX/4XR;->py:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1954880
    move-object v0, v0

    .line 1954881
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_5

    .line 1954882
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kW()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v0, v1, :cond_1

    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1954883
    iput-object v1, v0, LX/4XR;->pL:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1954884
    move-object v0, v0

    .line 1954885
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_5

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
    .end sparse-switch
.end method

.method public static b(LX/CzL;)LX/CzL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8dA;",
            ">;)",
            "LX/CzL",
            "<+",
            "LX/8dA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954829
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v0

    .line 1954830
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1954831
    check-cast v0, LX/8dA;

    .line 1954832
    invoke-interface {v0}, LX/8dA;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v2, v3, :cond_0

    .line 1954833
    :goto_0
    return-object p0

    .line 1954834
    :cond_0
    instance-of v2, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    if-eqz v2, :cond_1

    .line 1954835
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1954836
    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1954837
    invoke-static {v0}, LX/8dX;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)LX/8dX;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1954838
    iput-object v1, v0, LX/8dX;->ba:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1954839
    move-object v0, v0

    .line 1954840
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1954841
    :goto_1
    invoke-virtual {p0, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object p0

    goto :goto_0

    .line 1954842
    :cond_1
    instance-of v2, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    if-eqz v2, :cond_2

    .line 1954843
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-static {v0}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1954844
    iput-object v1, v0, LX/8dQ;->aO:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1954845
    move-object v0, v0

    .line 1954846
    invoke-virtual {v0}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    goto :goto_1

    .line 1954847
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static c(LX/CzL;)LX/CzL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d9;",
            ">;)",
            "LX/CzL",
            "<+",
            "LX/8d9;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954793
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v0

    .line 1954794
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1954795
    check-cast v0, LX/8d9;

    .line 1954796
    invoke-interface {v0}, LX/8d9;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v1

    .line 1954797
    if-eqz v1, :cond_2

    .line 1954798
    invoke-interface {v0}, LX/8d9;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v1, v3, :cond_1

    .line 1954799
    :cond_0
    :goto_0
    return-object p0

    .line 1954800
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1954801
    iput-object v3, v1, LX/8dQ;->aN:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1954802
    move-object v1, v1

    .line 1954803
    invoke-virtual {v1}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    .line 1954804
    :goto_1
    instance-of v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    if-eqz v3, :cond_3

    .line 1954805
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1954806
    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1954807
    invoke-static {v0}, LX/8dX;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)LX/8dX;

    move-result-object v0

    invoke-interface {v1}, LX/8d9;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 1954808
    iput-object v2, v0, LX/8dX;->aZ:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1954809
    move-object v0, v0

    .line 1954810
    invoke-interface {v1}, LX/8d9;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    .line 1954811
    iput-object v1, v0, LX/8dX;->bc:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1954812
    move-object v0, v0

    .line 1954813
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1954814
    :goto_2
    invoke-virtual {p0, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object p0

    goto :goto_0

    .line 1954815
    :cond_2
    invoke-interface {v0}, LX/8d9;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v1, v3, :cond_0

    .line 1954816
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1954817
    iput-object v3, v1, LX/8dQ;->aQ:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1954818
    move-object v1, v1

    .line 1954819
    invoke-virtual {v1}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    goto :goto_1

    .line 1954820
    :cond_3
    instance-of v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    if-eqz v3, :cond_4

    .line 1954821
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-static {v0}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v0

    invoke-interface {v1}, LX/8d9;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 1954822
    iput-object v2, v0, LX/8dQ;->aN:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1954823
    move-object v0, v0

    .line 1954824
    invoke-interface {v1}, LX/8d9;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    .line 1954825
    iput-object v1, v0, LX/8dQ;->aQ:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1954826
    move-object v0, v0

    .line 1954827
    invoke-virtual {v0}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    goto :goto_2

    .line 1954828
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static d(LX/CzL;)LX/CzL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8dB;",
            ">;)",
            "LX/CzL",
            "<+",
            "LX/8dB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954770
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1954771
    check-cast v0, LX/8dB;

    .line 1954772
    invoke-interface {v0}, LX/8dB;->K()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    .line 1954773
    sget-object v2, LX/CzN;->b:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1954774
    if-nez v1, :cond_0

    .line 1954775
    :goto_0
    return-object p0

    .line 1954776
    :cond_0
    instance-of v2, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    if-eqz v2, :cond_1

    .line 1954777
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1954778
    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1954779
    invoke-static {v0}, LX/8dX;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)LX/8dX;

    move-result-object v0

    .line 1954780
    iput-object v1, v0, LX/8dX;->bb:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1954781
    move-object v0, v0

    .line 1954782
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1954783
    invoke-virtual {p0, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object p0

    goto :goto_0

    .line 1954784
    :cond_1
    instance-of v2, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    if-eqz v2, :cond_2

    .line 1954785
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 1954786
    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 1954787
    invoke-static {v0}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v0

    .line 1954788
    iput-object v1, v0, LX/8dQ;->aP:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1954789
    move-object v0, v0

    .line 1954790
    invoke-virtual {v0}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 1954791
    invoke-virtual {p0, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object p0

    goto :goto_0

    .line 1954792
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
