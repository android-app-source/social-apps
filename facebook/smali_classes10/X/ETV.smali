.class public final LX/ETV;
.super LX/D7c;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;)V
    .locals 0

    .prologue
    .line 2125122
    iput-object p1, p0, LX/ETV;->a:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    invoke-direct {p0}, LX/D7c;-><init>()V

    return-void
.end method


# virtual methods
.method public final d()V
    .locals 5

    .prologue
    const/4 v1, 0x2

    .line 2125123
    iget-object v0, p0, LX/ETV;->a:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    const-wide/16 v2, 0x0

    .line 2125124
    iput-wide v2, v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->k:J

    .line 2125125
    iget-object v0, p0, LX/ETV;->a:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    iget-object v0, v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ETV;->a:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    iget-object v0, v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->i:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2125126
    iget-object v0, p0, LX/ETV;->a:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    iget-object v0, v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->i:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2125127
    iget-object v0, p0, LX/ETV;->a:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    iget-object v0, v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->j:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 2125128
    :cond_0
    return-void
.end method

.method public final kQ_()V
    .locals 5

    .prologue
    .line 2125119
    iget-object v0, p0, LX/ETV;->a:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    iget-object v1, p0, LX/ETV;->a:Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    iget-object v1, v1, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->f:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2125120
    iput-wide v2, v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->k:J

    .line 2125121
    return-void
.end method
