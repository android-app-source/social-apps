.class public final LX/Ed3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ecx;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2148241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2148242
    iput-object p1, p0, LX/Ed3;->a:Ljava/lang/String;

    .line 2148243
    iput-object p2, p0, LX/Ed3;->b:Ljava/lang/String;

    .line 2148244
    iput p3, p0, LX/Ed3;->c:I

    .line 2148245
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Ed3;
    .locals 10

    .prologue
    const/16 v0, 0x50

    const/4 v1, 0x0

    .line 2148218
    invoke-static {p0}, LX/Ed5;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mms"

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2148219
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2148220
    :cond_0
    :goto_0
    move v2, v4

    .line 2148221
    if-nez v2, :cond_1

    move-object v0, v1

    .line 2148222
    :goto_1
    return-object v0

    .line 2148223
    :cond_1
    invoke-static {p1}, LX/Ed5;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2148224
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v0, v1

    .line 2148225
    goto :goto_1

    .line 2148226
    :cond_2
    invoke-static {v2}, LX/Ed5;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2148227
    :try_start_0
    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, v3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2148228
    invoke-static {p2}, LX/Ed5;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2148229
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2148230
    invoke-static {v1}, LX/Ed5;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2148231
    invoke-static {p3}, LX/Ed5;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2148232
    if-eqz v1, :cond_4

    .line 2148233
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 2148234
    if-gtz v1, :cond_5

    :goto_2
    move-object v1, v2

    .line 2148235
    :cond_3
    :goto_3
    new-instance v2, LX/Ed3;

    invoke-direct {v2, v3, v1, v0}, LX/Ed3;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    move-object v0, v2

    goto :goto_1

    .line 2148236
    :catch_0
    move-object v0, v1

    goto :goto_1

    :catch_1
    :cond_4
    move-object v1, v2

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2

    .line 2148237
    :cond_6
    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    move v6, v5

    :goto_4
    if-ge v6, v8, :cond_7

    aget-object v9, v7, v6

    .line 2148238
    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string p0, "*"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 2148239
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_7
    move v4, v5

    .line 2148240
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148217
    iget-object v0, p0, LX/Ed3;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148216
    iget-object v0, p0, LX/Ed3;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2148215
    iget v0, p0, LX/Ed3;->c:I

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2148214
    return-void
.end method
