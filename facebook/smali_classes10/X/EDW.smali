.class public final LX/EDW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;)V
    .locals 0

    .prologue
    .line 2091414
    iput-object p1, p0, LX/EDW;->a:LX/EDx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x60722016

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2091415
    iget-object v1, p0, LX/EDW;->a:LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aa()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2091416
    const/16 v1, 0x27

    const v2, 0x4a10002

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2091417
    :goto_0
    return-void

    .line 2091418
    :cond_0
    iget-object v1, p0, LX/EDW;->a:LX/EDx;

    iget-object v1, v1, LX/EDx;->p:LX/ECx;

    invoke-virtual {v1}, LX/ECx;->f()V

    .line 2091419
    iget-object v1, p0, LX/EDW;->a:LX/EDx;

    iget-object v1, v1, LX/EDx;->p:LX/ECx;

    invoke-virtual {v1}, LX/ECx;->e()V

    .line 2091420
    iget-object v1, p0, LX/EDW;->a:LX/EDx;

    iget-object v1, v1, LX/EDx;->l:LX/2S7;

    .line 2091421
    iget-wide v4, v1, LX/2S7;->w:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 2091422
    :goto_1
    const v1, -0x6aa0d4f4

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    .line 2091423
    :cond_1
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "connectivity_status"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2091424
    invoke-static {v1}, LX/2S7;->m(LX/2S7;)Ljava/lang/String;

    move-result-object v5

    .line 2091425
    const-string v6, "call_id"

    iget-wide v8, v1, LX/2S7;->w:J

    invoke-virtual {v4, v6, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2091426
    const-string v6, "mqtt"

    iget-object v7, v1, LX/2S7;->e:LX/1MZ;

    invoke-virtual {v7}, LX/1MZ;->e()Z

    move-result v7

    invoke-virtual {v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2091427
    const-string v6, "wifi"

    iget-object v7, v1, LX/2S7;->f:LX/0ka;

    invoke-virtual {v7}, LX/0ka;->b()Z

    move-result v7

    invoke-virtual {v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2091428
    iget-object v6, v1, LX/2S7;->h:Landroid/telephony/TelephonyManager;

    if-eqz v6, :cond_2

    .line 2091429
    const-string v6, "network_type"

    iget-object v7, v1, LX/2S7;->h:Landroid/telephony/TelephonyManager;

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v7

    invoke-static {v7}, LX/0km;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2091430
    const-string v6, "phone_type"

    iget-object v7, v1, LX/2S7;->h:Landroid/telephony/TelephonyManager;

    invoke-static {v7}, LX/0km;->a(Landroid/telephony/TelephonyManager;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2091431
    :cond_2
    const-string v6, "connectivity"

    invoke-virtual {v4, v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2091432
    invoke-static {v1, v4}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_1
.end method
