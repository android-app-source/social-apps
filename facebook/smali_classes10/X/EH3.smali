.class public LX/EH3;
.super LX/EH2;
.source ""


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/view/View;

.field public e:Landroid/widget/ImageButton;

.field public f:Landroid/widget/ImageButton;

.field public g:Landroid/widget/ImageButton;

.field public h:Landroid/widget/ImageButton;

.field public i:Landroid/widget/ImageButton;

.field public j:Landroid/widget/ImageButton;

.field public k:Landroid/widget/ImageButton;

.field public l:Landroid/widget/ImageButton;

.field public m:Landroid/widget/ImageButton;

.field public n:Landroid/widget/ImageButton;

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:LX/EBz;

.field public v:Z

.field public w:LX/0hs;

.field public x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/EH1;)V
    .locals 5

    .prologue
    .line 2098677
    invoke-direct {p0, p1}, LX/EH2;-><init>(Landroid/content/Context;)V

    .line 2098678
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EH3;->v:Z

    .line 2098679
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2098680
    iput-object v0, p0, LX/EH3;->x:LX/0Ot;

    .line 2098681
    const/4 v4, 0x1

    .line 2098682
    const-class v0, LX/EH3;

    invoke-static {v0, p0}, LX/EH3;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2098683
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2098684
    sget-object v1, LX/EH1;->VOICE:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 2098685
    const v1, 0x7f030906

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EH3;->d:Landroid/view/View;

    .line 2098686
    :cond_0
    :goto_0
    sget-object v0, LX/EH1;->VOICE:LX/EH1;

    invoke-virtual {p2, v0}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, LX/EH1;->CONFERENCE:LX/EH1;

    invoke-virtual {p2, v0}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, LX/EH1;->VOICE_WITH_ADD_CALLEE:LX/EH1;

    invoke-virtual {p2, v0}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, LX/EH1;->CONFERENCE_WITH_ADD_CALLEE:LX/EH1;

    invoke-virtual {p2, v0}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2098687
    :cond_1
    const v0, 0x7f021ab7

    iput v0, p0, LX/EH3;->o:I

    .line 2098688
    const v0, 0x7f021a9b

    iput v0, p0, LX/EH3;->p:I

    .line 2098689
    const v0, 0x7f021a88

    iput v0, p0, LX/EH3;->q:I

    .line 2098690
    const v0, 0x7f021a98

    iput v0, p0, LX/EH3;->r:I

    .line 2098691
    const v0, 0x7f021ab5

    iput v0, p0, LX/EH3;->s:I

    .line 2098692
    const v0, 0x7f021ac7

    iput v0, p0, LX/EH3;->t:I

    .line 2098693
    :goto_1
    iget-object v0, p0, LX/EH3;->a:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget v3, LX/3Dx;->em:I

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    .line 2098694
    const v1, 0x7f0d172c

    invoke-static {p0, v1}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, LX/EH3;->e:Landroid/widget/ImageButton;

    .line 2098695
    const v1, 0x7f0d172b

    invoke-static {p0, v1}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    .line 2098696
    const v1, 0x7f0d1728

    invoke-static {p0, v1}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, LX/EH3;->g:Landroid/widget/ImageButton;

    .line 2098697
    const v1, 0x7f0d172d

    invoke-static {p0, v1}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    .line 2098698
    const v1, 0x7f0d1727

    invoke-static {p0, v1}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    .line 2098699
    sget-object v1, LX/EH1;->CONFERENCE_VIDEO:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, LX/EH1;->VIDEO:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 2098700
    :cond_2
    const v1, 0x7f0d1729

    invoke-static {p0, v1}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, LX/EH3;->k:Landroid/widget/ImageButton;

    .line 2098701
    :goto_2
    sget-object v1, LX/EH1;->VOICEMAIL:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2098702
    const v1, 0x7f0d172f

    invoke-static {p0, v1}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, LX/EH3;->l:Landroid/widget/ImageButton;

    .line 2098703
    const v1, 0x7f0d1730

    invoke-static {p0, v1}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, LX/EH3;->m:Landroid/widget/ImageButton;

    .line 2098704
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    iget-object v1, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    if-eqz v1, :cond_4

    .line 2098705
    iget-object v1, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    invoke-virtual {p0}, LX/EH3;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f021aaa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2098706
    iput-boolean v4, p0, LX/EH3;->v:Z

    .line 2098707
    :cond_4
    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    iget-object v1, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    if-eqz v1, :cond_5

    .line 2098708
    iget-object v1, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    invoke-virtual {p0}, LX/EH3;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f021aab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2098709
    iput-boolean v4, p0, LX/EH3;->v:Z

    .line 2098710
    :cond_5
    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    iget-object v0, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    if-eqz v0, :cond_6

    .line 2098711
    iget-object v0, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    invoke-virtual {p0}, LX/EH3;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021aac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2098712
    :cond_6
    const v0, 0x7f0d172a

    invoke-static {p0, v0}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v0

    iput-object v0, p0, LX/EH3;->i:Landroid/widget/ImageButton;

    .line 2098713
    iget-object v0, p0, LX/EH3;->e:Landroid/widget/ImageButton;

    if-eqz v0, :cond_7

    .line 2098714
    iget-object v0, p0, LX/EH3;->e:Landroid/widget/ImageButton;

    new-instance v1, LX/EGs;

    invoke-direct {v1, p0}, LX/EGs;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098715
    :cond_7
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    if-eqz v0, :cond_8

    .line 2098716
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    new-instance v1, LX/EGt;

    invoke-direct {v1, p0}, LX/EGt;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098717
    :cond_8
    iget-object v0, p0, LX/EH3;->g:Landroid/widget/ImageButton;

    if-eqz v0, :cond_9

    .line 2098718
    iget-object v0, p0, LX/EH3;->g:Landroid/widget/ImageButton;

    new-instance v1, LX/EGu;

    invoke-direct {v1, p0}, LX/EGu;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098719
    :cond_9
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    if-eqz v0, :cond_a

    .line 2098720
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    new-instance v1, LX/EGv;

    invoke-direct {v1, p0}, LX/EGv;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098721
    :cond_a
    iget-object v0, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    if-eqz v0, :cond_b

    .line 2098722
    iget-object v0, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    new-instance v1, LX/EGw;

    invoke-direct {v1, p0}, LX/EGw;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098723
    :cond_b
    iget-object v0, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    if-eqz v0, :cond_c

    .line 2098724
    iget-object v0, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    new-instance v1, LX/EGx;

    invoke-direct {v1, p0}, LX/EGx;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098725
    :cond_c
    iget-object v0, p0, LX/EH3;->i:Landroid/widget/ImageButton;

    if-eqz v0, :cond_d

    .line 2098726
    iget-object v0, p0, LX/EH3;->i:Landroid/widget/ImageButton;

    new-instance v1, LX/EGy;

    invoke-direct {v1, p0}, LX/EGy;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098727
    iget-object v0, p0, LX/EH3;->i:Landroid/widget/ImageButton;

    new-instance v1, LX/EGz;

    invoke-direct {v1, p0}, LX/EGz;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2098728
    :cond_d
    iget-object v0, p0, LX/EH3;->k:Landroid/widget/ImageButton;

    if-eqz v0, :cond_e

    .line 2098729
    iget-object v0, p0, LX/EH3;->k:Landroid/widget/ImageButton;

    new-instance v1, LX/EH0;

    invoke-direct {v1, p0}, LX/EH0;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098730
    :cond_e
    iget-object v0, p0, LX/EH3;->l:Landroid/widget/ImageButton;

    if-eqz v0, :cond_f

    .line 2098731
    iget-object v0, p0, LX/EH3;->l:Landroid/widget/ImageButton;

    new-instance v1, LX/EGq;

    invoke-direct {v1, p0}, LX/EGq;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098732
    :cond_f
    iget-object v0, p0, LX/EH3;->m:Landroid/widget/ImageButton;

    if-eqz v0, :cond_10

    .line 2098733
    iget-object v0, p0, LX/EH3;->m:Landroid/widget/ImageButton;

    new-instance v1, LX/EGr;

    invoke-direct {v1, p0}, LX/EGr;-><init>(LX/EH3;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098734
    :cond_10
    invoke-virtual {p0}, LX/EH3;->a()V

    .line 2098735
    return-void

    .line 2098736
    :cond_11
    sget-object v1, LX/EH1;->VIDEO:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2098737
    const v1, 0x7f030905

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EH3;->d:Landroid/view/View;

    goto/16 :goto_0

    .line 2098738
    :cond_12
    sget-object v1, LX/EH1;->VOICE_WITH_ADD_CALLEE:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2098739
    const v1, 0x7f030907

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EH3;->d:Landroid/view/View;

    goto/16 :goto_0

    .line 2098740
    :cond_13
    sget-object v1, LX/EH1;->CONFERENCE_WITH_ADD_CALLEE:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 2098741
    const v1, 0x7f030904

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EH3;->d:Landroid/view/View;

    goto/16 :goto_0

    .line 2098742
    :cond_14
    sget-object v1, LX/EH1;->CONFERENCE_VIDEO:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 2098743
    const v1, 0x7f030902

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EH3;->d:Landroid/view/View;

    goto/16 :goto_0

    .line 2098744
    :cond_15
    sget-object v1, LX/EH1;->CONFERENCE:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 2098745
    const v1, 0x7f030903

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EH3;->d:Landroid/view/View;

    goto/16 :goto_0

    .line 2098746
    :cond_16
    sget-object v1, LX/EH1;->VOICEMAIL:LX/EH1;

    invoke-virtual {p2, v1}, LX/EH1;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2098747
    const v1, 0x7f030908

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EH3;->d:Landroid/view/View;

    goto/16 :goto_0

    .line 2098748
    :cond_17
    const v1, 0x7f0d172e

    invoke-static {p0, v1}, LX/EH3;->c(LX/EH3;I)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    goto/16 :goto_2

    .line 2098749
    :cond_18
    const v0, 0x7f021ad1

    iput v0, p0, LX/EH3;->o:I

    .line 2098750
    const v0, 0x7f021acc

    iput v0, p0, LX/EH3;->p:I

    .line 2098751
    const v0, 0x7f021ac6

    iput v0, p0, LX/EH3;->q:I

    .line 2098752
    const v0, 0x7f021ac9

    iput v0, p0, LX/EH3;->r:I

    .line 2098753
    const v0, 0x7f021ad0

    iput v0, p0, LX/EH3;->s:I

    .line 2098754
    const v0, 0x7f021ad5

    iput v0, p0, LX/EH3;->t:I

    goto/16 :goto_1
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/EH3;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v3}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v4, 0x1430

    invoke-static {v3, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x3257

    invoke-static {v3, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    iput-object v1, p1, LX/EH3;->a:LX/0ad;

    iput-object v2, p1, LX/EH3;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, p1, LX/EH3;->c:LX/0Ot;

    iput-object v3, p1, LX/EH3;->x:LX/0Ot;

    return-void
.end method

.method public static c(LX/EH3;I)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 2098676
    iget-object v0, p0, LX/EH3;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2098604
    iget-object v0, p0, LX/EH3;->e:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 2098605
    iget-object v3, p0, LX/EH3;->e:Landroid/widget/ImageButton;

    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ae()Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2098606
    :cond_0
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 2098607
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aI()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2098608
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->D()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2098609
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    invoke-virtual {p0}, LX/EH3;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, LX/EH3;->o:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2098610
    :goto_0
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    const v3, 0x7f0806f9

    invoke-virtual {p0, v3}, LX/EH2;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2098611
    :cond_1
    :goto_1
    iget-object v0, p0, LX/EH3;->g:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 2098612
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->A()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2098613
    iget-object v0, p0, LX/EH3;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2098614
    iget-object v0, p0, LX/EH3;->g:Landroid/widget/ImageButton;

    const v3, 0x7f080707

    invoke-virtual {p0, v3}, LX/EH2;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2098615
    :cond_2
    :goto_2
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    .line 2098616
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2098617
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0}, LX/EH3;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, LX/EH3;->t:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2098618
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 v3, 0x0

    .line 2098619
    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 2098620
    iget-object v4, v0, LX/EDx;->M:LX/0Uh;

    const/16 v6, 0x5f5

    invoke-virtual {v4, v6, v3}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-nez v4, :cond_3

    iget-boolean v4, v0, LX/EDx;->bX:Z

    if-eqz v4, :cond_4

    :cond_3
    const/4 v3, 0x1

    .line 2098621
    :cond_4
    :goto_3
    move v0, v3

    .line 2098622
    if-nez v0, :cond_10

    .line 2098623
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2098624
    iget-object v0, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    .line 2098625
    iget-object v0, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2098626
    :cond_5
    :goto_4
    iget-object v0, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    if-eqz v0, :cond_6

    .line 2098627
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->Y()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098628
    iget-boolean v3, v0, LX/EDx;->aB:Z

    move v0, v3

    .line 2098629
    if-eqz v0, :cond_13

    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aR()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2098630
    iget-object v0, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2098631
    iget-object v0, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2098632
    iget-object v6, p0, LX/EH3;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/EDJ;->l:LX/0Tn;

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v7

    .line 2098633
    iget-object v6, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v6

    if-nez v6, :cond_6

    const/4 v6, 0x3

    if-ge v7, v6, :cond_6

    iget-object v6, p0, LX/EH3;->w:LX/0hs;

    if-nez v6, :cond_6

    .line 2098634
    new-instance v6, LX/0hs;

    invoke-virtual {p0}, LX/EH3;->getContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x2

    invoke-direct {v6, v8, v9}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v6, p0, LX/EH3;->w:LX/0hs;

    .line 2098635
    iget-object v6, p0, LX/EH3;->w:LX/0hs;

    const/16 v8, 0x1f40

    .line 2098636
    iput v8, v6, LX/0hs;->t:I

    .line 2098637
    iget-object v6, p0, LX/EH3;->w:LX/0hs;

    const v8, 0x7f080806

    invoke-virtual {v6, v8}, LX/0hs;->a(I)V

    .line 2098638
    iget-object v6, p0, LX/EH3;->w:LX/0hs;

    iget-object v8, p0, LX/EH3;->d:Landroid/view/View;

    invoke-virtual {v6, v8}, LX/0ht;->c(Landroid/view/View;)V

    .line 2098639
    iget-object v6, p0, LX/EH3;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v8, Lcom/facebook/rtc/views/IncallControlButtonsView$12;

    invoke-direct {v8, p0, v7}, Lcom/facebook/rtc/views/IncallControlButtonsView$12;-><init>(LX/EH3;I)V

    const-wide/16 v10, 0x7d0

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v8, v10, v11, v7}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2098640
    :cond_6
    :goto_5
    iget-object v0, p0, LX/EH3;->k:Landroid/widget/ImageButton;

    if-eqz v0, :cond_7

    .line 2098641
    iget-object v1, p0, LX/EH3;->k:Landroid/widget/ImageButton;

    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2098642
    :cond_7
    iget-object v0, p0, LX/EH3;->m:Landroid/widget/ImageButton;

    if-eqz v0, :cond_8

    .line 2098643
    iget-object v1, p0, LX/EH3;->m:Landroid/widget/ImageButton;

    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->au()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2098644
    :cond_8
    return-void

    .line 2098645
    :cond_9
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->C()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2098646
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    invoke-virtual {p0}, LX/EH3;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, LX/EH3;->q:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2098647
    :cond_a
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098648
    iget-boolean v3, v0, LX/EDx;->be:Z

    move v0, v3

    .line 2098649
    if-eqz v0, :cond_b

    .line 2098650
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    invoke-virtual {p0}, LX/EH3;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, LX/EH3;->p:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2098651
    :cond_b
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    invoke-virtual {p0}, LX/EH3;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, LX/EH3;->r:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 2098652
    :cond_c
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    invoke-virtual {p0}, LX/EH3;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, LX/EH3;->s:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2098653
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->D()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2098654
    iget-object v3, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    :goto_6
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2098655
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2098656
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    const v3, 0x7f0806fb

    invoke-virtual {p0, v3}, LX/EH2;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_d
    move v0, v2

    .line 2098657
    goto :goto_6

    .line 2098658
    :cond_e
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2098659
    iget-object v0, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    const v3, 0x7f0806fa

    invoke-virtual {p0, v3}, LX/EH2;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2098660
    :cond_f
    iget-object v0, p0, LX/EH3;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2098661
    iget-object v0, p0, LX/EH3;->g:Landroid/widget/ImageButton;

    const v3, 0x7f080706

    invoke-virtual {p0, v3}, LX/EH2;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2098662
    :cond_10
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aR()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2098663
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2098664
    iget-object v3, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_4

    .line 2098665
    :cond_11
    iget-object v0, p0, LX/EH3;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2098666
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2098667
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    const v3, 0x7f080709

    invoke-virtual {p0, v3}, LX/EH2;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2098668
    :goto_7
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_4

    .line 2098669
    :cond_12
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2098670
    iget-object v0, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    const v3, 0x7f080708

    invoke-virtual {p0, v3}, LX/EH2;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 2098671
    :cond_13
    iget-object v0, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2098672
    iget-object v0, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2098673
    iget-object v0, p0, LX/EH3;->w:LX/0hs;

    if-eqz v0, :cond_6

    .line 2098674
    iget-object v0, p0, LX/EH3;->w:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2098675
    const/4 v0, 0x0

    iput-object v0, p0, LX/EH3;->w:LX/0hs;

    goto/16 :goto_5

    :cond_14
    iget-boolean v3, v0, LX/EDx;->ao:Z

    goto/16 :goto_3
.end method

.method public final gatherTransparentRegion(Landroid/graphics/Region;)Z
    .locals 1

    .prologue
    .line 2098602
    invoke-virtual {p1}, Landroid/graphics/Region;->setEmpty()V

    .line 2098603
    invoke-super {p0, p1}, LX/EH2;->gatherTransparentRegion(Landroid/graphics/Region;)Z

    move-result v0

    return v0
.end method

.method public getListener()LX/EBz;
    .locals 1

    .prologue
    .line 2098594
    iget-object v0, p0, LX/EH3;->u:LX/EBz;

    return-object v0
.end method

.method public setButtonsEnabled(Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x0

    .line 2098595
    new-array v1, v4, [Landroid/view/View;

    iget-object v2, p0, LX/EH3;->e:Landroid/widget/ImageButton;

    aput-object v2, v1, v0

    const/4 v2, 0x1

    iget-object v3, p0, LX/EH3;->f:Landroid/widget/ImageButton;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LX/EH3;->g:Landroid/widget/ImageButton;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, LX/EH3;->h:Landroid/widget/ImageButton;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, LX/EH3;->j:Landroid/widget/ImageButton;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, LX/EH3;->i:Landroid/widget/ImageButton;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, LX/EH3;->m:Landroid/widget/ImageButton;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, LX/EH3;->n:Landroid/widget/ImageButton;

    aput-object v3, v1, v2

    .line 2098596
    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v2, v1, v0

    .line 2098597
    if-eqz v2, :cond_0

    .line 2098598
    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 2098599
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2098600
    :cond_1
    invoke-static {p0, p1}, LX/EH2;->a(Landroid/view/View;Z)V

    .line 2098601
    return-void
.end method
