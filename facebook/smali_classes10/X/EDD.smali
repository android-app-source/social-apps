.class public final LX/EDD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# instance fields
.field public final synthetic a:LX/3GP;


# direct methods
.method public constructor <init>(LX/3GP;)V
    .locals 0

    .prologue
    .line 2091093
    iput-object p1, p0, LX/EDD;->a:LX/3GP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 1

    .prologue
    .line 2091094
    iget-object v0, p0, LX/EDD;->a:LX/3GP;

    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    .line 2091095
    iput-object p2, v0, LX/3GP;->f:Landroid/bluetooth/BluetoothHeadset;

    .line 2091096
    iget-object v0, p0, LX/EDD;->a:LX/3GP;

    iget-object v0, v0, LX/3GP;->g:LX/EDp;

    if-eqz v0, :cond_1

    .line 2091097
    iget-object v0, p0, LX/EDD;->a:LX/3GP;

    iget-object v0, v0, LX/3GP;->g:LX/EDp;

    .line 2091098
    iget-object p0, v0, LX/EDp;->a:LX/EDx;

    iget-object p0, p0, LX/EDx;->m:LX/3GP;

    invoke-virtual {p0}, LX/3GP;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2091099
    iget-object p0, v0, LX/EDp;->a:LX/EDx;

    invoke-static {p0}, LX/EDx;->bC(LX/EDx;)V

    .line 2091100
    :cond_0
    iget-object p0, v0, LX/EDp;->a:LX/EDx;

    invoke-static {p0}, LX/EDx;->bS(LX/EDx;)V

    .line 2091101
    :cond_1
    return-void
.end method

.method public final onServiceDisconnected(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2091102
    iget-object v0, p0, LX/EDD;->a:LX/3GP;

    .line 2091103
    iput-object v1, v0, LX/3GP;->f:Landroid/bluetooth/BluetoothHeadset;

    .line 2091104
    iget-object v0, p0, LX/EDD;->a:LX/3GP;

    .line 2091105
    iput-object v1, v0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    .line 2091106
    iget-object v0, p0, LX/EDD;->a:LX/3GP;

    iget-object v0, v0, LX/3GP;->g:LX/EDp;

    if-eqz v0, :cond_1

    .line 2091107
    iget-object v0, p0, LX/EDD;->a:LX/3GP;

    iget-object v0, v0, LX/3GP;->g:LX/EDp;

    .line 2091108
    iget-object v1, v0, LX/EDp;->a:LX/EDx;

    iget-boolean v1, v1, LX/EDx;->bg:Z

    if-eqz v1, :cond_0

    .line 2091109
    iget-object v1, v0, LX/EDp;->a:LX/EDx;

    invoke-static {v1}, LX/EDx;->bD(LX/EDx;)V

    .line 2091110
    :cond_0
    iget-object v1, v0, LX/EDp;->a:LX/EDx;

    invoke-static {v1}, LX/EDx;->bS(LX/EDx;)V

    .line 2091111
    :cond_1
    return-void
.end method
