.class public final LX/E2A;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E2B;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:I

.field public c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

.field public final synthetic d:LX/E2B;


# direct methods
.method public constructor <init>(LX/E2B;)V
    .locals 1

    .prologue
    .line 2072343
    iput-object p1, p0, LX/E2A;->d:LX/E2B;

    .line 2072344
    move-object v0, p1

    .line 2072345
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2072346
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2072363
    const-string v0, "ReactionCoreTextComponentMessage"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2072347
    if-ne p0, p1, :cond_1

    .line 2072348
    :cond_0
    :goto_0
    return v0

    .line 2072349
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2072350
    goto :goto_0

    .line 2072351
    :cond_3
    check-cast p1, LX/E2A;

    .line 2072352
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2072353
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2072354
    if-eq v2, v3, :cond_0

    .line 2072355
    iget-object v2, p0, LX/E2A;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E2A;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/E2A;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2072356
    goto :goto_0

    .line 2072357
    :cond_5
    iget-object v2, p1, LX/E2A;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 2072358
    :cond_6
    iget v2, p0, LX/E2A;->b:I

    iget v3, p1, LX/E2A;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2072359
    goto :goto_0

    .line 2072360
    :cond_7
    iget-object v2, p0, LX/E2A;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E2A;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    iget-object v3, p1, LX/E2A;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2072361
    goto :goto_0

    .line 2072362
    :cond_8
    iget-object v2, p1, LX/E2A;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
