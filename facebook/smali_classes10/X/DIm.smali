.class public final LX/DIm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1984361
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1984362
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1984363
    :goto_0
    return v1

    .line 1984364
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1984365
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1984366
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1984367
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1984368
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1984369
    const-string v3, "weather_setting_saved_cities"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1984370
    invoke-static {p0, p1}, LX/DIl;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1984371
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1984372
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1984373
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1984374
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1984375
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1984376
    if-eqz v0, :cond_0

    .line 1984377
    const-string v1, "weather_setting_saved_cities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1984378
    invoke-static {p0, v0, p2, p3}, LX/DIl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1984379
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1984380
    return-void
.end method
