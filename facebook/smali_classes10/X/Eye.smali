.class public final LX/Eye;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 0

    .prologue
    .line 2186431
    iput-object p1, p0, LX/Eye;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x44037ec8

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186412
    iget-object v1, p0, LX/Eye;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->an:LX/2hl;

    .line 2186413
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2186414
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2186415
    sget-object v4, LX/2hl;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result p0

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, p0, :cond_2

    sget-object v4, LX/2hl;->a:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2hm;

    .line 2186416
    sget-object p1, LX/2hm;->SEARCH:LX/2hm;

    if-ne v4, p1, :cond_0

    iget-boolean p1, v1, LX/2hl;->g:Z

    if-nez p1, :cond_1

    .line 2186417
    :cond_0
    invoke-static {v1, v4}, LX/2hl;->a(LX/2hl;LX/2hm;)LX/Ez3;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2186418
    :cond_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2186419
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v4, v4

    .line 2186420
    const/4 v6, 0x1

    const/4 p0, 0x0

    .line 2186421
    new-instance p1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    invoke-direct {p1}, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;-><init>()V

    .line 2186422
    iput-object v3, p1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->p:Ljava/lang/String;

    .line 2186423
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2186424
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    if-lez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 2186425
    invoke-virtual {v4, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Ez3;

    iput-object v5, p1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->r:LX/Ez3;

    .line 2186426
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v4, v6, v5}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->reverse()LX/0Px;

    move-result-object v5

    iput-object v5, p1, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->q:LX/0Px;

    .line 2186427
    move-object v3, p1

    .line 2186428
    iget-object v4, v1, LX/2hl;->d:LX/0gc;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2186429
    const v1, 0x1af4e219

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_3
    move v5, p0

    .line 2186430
    goto :goto_1
.end method
