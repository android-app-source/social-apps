.class public final LX/EP7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pn;

.field public final synthetic b:LX/CzL;

.field public final synthetic c:LX/8dL;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;LX/1Pn;LX/CzL;LX/8dL;)V
    .locals 0

    .prologue
    .line 2115919
    iput-object p1, p0, LX/EP7;->d:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;

    iput-object p2, p0, LX/EP7;->a:LX/1Pn;

    iput-object p3, p0, LX/EP7;->b:LX/CzL;

    iput-object p4, p0, LX/EP7;->c:LX/8dL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v11, 0x2

    const/4 v0, 0x1

    const v1, -0x4edae8a3

    invoke-static {v11, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2115920
    iget-object v0, p0, LX/EP7;->d:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v1, p0, LX/EP7;->a:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->CLICK:LX/8ch;

    iget-object v3, p0, LX/EP7;->b:LX/CzL;

    .line 2115921
    iget v4, v3, LX/CzL;->b:I

    move v3, v4

    .line 2115922
    iget-object v4, p0, LX/EP7;->b:LX/CzL;

    .line 2115923
    iget-object v5, v4, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v4, v5

    .line 2115924
    invoke-static {v4}, LX/CvY;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CvV;

    move-result-object v4

    iget-object v5, p0, LX/EP7;->d:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;

    iget-object v5, v5, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v5, p0, LX/EP7;->a:LX/1Pn;

    check-cast v5, LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    iget-object v5, p0, LX/EP7;->a:LX/1Pn;

    check-cast v5, LX/CxP;

    iget-object v9, p0, LX/EP7;->b:LX/CzL;

    invoke-interface {v5, v9}, LX/CxP;->b(LX/CzL;)I

    move-result v5

    iget-object v9, p0, LX/EP7;->c:LX/8dL;

    invoke-interface {v9}, LX/8dL;->dW_()Ljava/lang/String;

    move-result-object v9

    .line 2115925
    sget-object v10, LX/0Rg;->a:LX/0Rg;

    move-object v10, v10

    .line 2115926
    invoke-static {v8, v5, v9, v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2115927
    iget-object v0, p0, LX/EP7;->c:LX/8dL;

    invoke-interface {v0}, LX/8dL;->an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2115928
    iget-object v0, p0, LX/EP7;->c:LX/8dL;

    invoke-interface {v0}, LX/8dL;->an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2115929
    iget-object v0, p0, LX/EP7;->c:LX/8dL;

    invoke-interface {v0}, LX/8dL;->an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;->c()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2115930
    iget-object v0, p0, LX/EP7;->c:LX/8dL;

    invoke-interface {v0}, LX/8dL;->an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;->c()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2115931
    :goto_0
    iget-object v0, p0, LX/EP7;->d:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EJ5;

    iget-object v1, p0, LX/EP7;->c:LX/8dL;

    invoke-interface {v1}, LX/8dL;->bQ()LX/8dJ;

    move-result-object v1

    invoke-interface {v1}, LX/8dJ;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/EP7;->c:LX/8dL;

    invoke-interface {v2}, LX/8dL;->O()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/EP7;->c:LX/8dL;

    invoke-interface {v3}, LX/8dL;->aV()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, LX/EP7;->a:LX/1Pn;

    check-cast v6, LX/CxV;

    invoke-virtual/range {v0 .. v6}, LX/EJ5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CxV;)V

    .line 2115932
    const v0, -0x390003a2

    invoke-static {v11, v11, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move-object v5, v6

    goto :goto_0

    :cond_1
    move-object v5, v6

    move-object v4, v6

    goto :goto_0
.end method
