.class public final LX/D5a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D5X;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 0

    .prologue
    .line 1963761
    iput-object p1, p0, LX/D5a;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/D6B;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;)V
    .locals 0

    .prologue
    .line 1963738
    return-void
.end method

.method public final a(LX/D6B;Ljava/lang/String;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;)V
    .locals 4

    .prologue
    .line 1963739
    iget-boolean v0, p1, LX/D6B;->h:Z

    move v0, v0

    .line 1963740
    if-nez v0, :cond_1

    .line 1963741
    iget-object v0, p0, LX/D5a;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    .line 1963742
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->b:Ljava/util/List;

    invoke-static {v1}, LX/3ha;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 1963743
    :goto_0
    if-nez v1, :cond_3

    .line 1963744
    :cond_0
    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/D6B;->a(Z)V

    .line 1963745
    :cond_1
    iget-object v0, p0, LX/D5a;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-static {v0, p2, p3}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Ljava/lang/String;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;)V

    .line 1963746
    return-void

    .line 1963747
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 1963748
    :cond_3
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v2}, LX/D6S;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, LX/D6S;->b(I)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1963749
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 1963750
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1963751
    instance-of v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedVideoSectionSeeMore;

    .line 1963752
    if-eqz v1, :cond_0

    .line 1963753
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v2}, LX/D6S;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 1963754
    if-ltz v2, :cond_4

    iget-object v3, v1, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_5

    .line 1963755
    :cond_4
    :goto_2
    goto :goto_1

    .line 1963756
    :cond_5
    iget-object v3, v1, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963757
    if-eqz v3, :cond_4

    .line 1963758
    invoke-static {v3}, LX/D6S;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v0

    .line 1963759
    iget-object v2, v1, LX/D6S;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1963760
    goto :goto_2
.end method
