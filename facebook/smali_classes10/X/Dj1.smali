.class public final LX/Dj1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dix;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V
    .locals 0

    .prologue
    .line 2032818
    iput-object p1, p0, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 2032819
    iget-object v0, p0, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->h:LX/Dih;

    iget-object v1, p0, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->c:Ljava/lang/String;

    iget-object v2, p0, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->o:Ljava/lang/String;

    iget-object v3, p0, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->b:Ljava/lang/String;

    .line 2032820
    iget-object v4, v0, LX/Dih;->a:LX/0Zb;

    const-string v5, "profservices_booking_admin_decline_request"

    invoke-static {v5, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "referrer"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "request_id"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032821
    iget-object v0, p0, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->n:LX/DkN;

    new-instance v1, LX/Dj0;

    invoke-direct {v1, p0}, LX/Dj0;-><init>(LX/Dj1;)V

    .line 2032822
    iget-object v2, v0, LX/DkN;->b:LX/1Ck;

    const-string v3, "admin_decline_appointment"

    new-instance v4, LX/Dk4;

    invoke-direct {v4, v0}, LX/Dk4;-><init>(LX/DkN;)V

    new-instance v5, LX/Dk5;

    invoke-direct {v5, v0, v1}, LX/Dk5;-><init>(LX/DkN;LX/Dj0;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2032823
    return-void
.end method
