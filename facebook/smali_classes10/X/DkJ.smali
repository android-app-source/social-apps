.class public final LX/DkJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2034077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034078
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2034079
    if-eqz p1, :cond_0

    .line 2034080
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034081
    if-eqz v0, :cond_0

    .line 2034082
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034083
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2034084
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034085
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel;->k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel$FuturePendingAppointmentsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2034086
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034087
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel;->j()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel$FutureConfirmedAppointmentsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2034088
    :cond_0
    const/4 v0, 0x0

    .line 2034089
    :goto_0
    return-object v0

    .line 2034090
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034091
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel;->j()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel$FutureConfirmedAppointmentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel$FutureConfirmedAppointmentsModel;->a()LX/0Px;

    move-result-object v1

    .line 2034092
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034093
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel;->k()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel$FuturePendingAppointmentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$PageAdminAppointmentsWithAUserQueryModel$ActorModel$FuturePendingAppointmentsModel;->a()LX/0Px;

    move-result-object v0

    .line 2034094
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2034095
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2034096
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2034097
    new-instance v0, LX/DkI;

    invoke-direct {v0, p0}, LX/DkI;-><init>(LX/DkJ;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2034098
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
