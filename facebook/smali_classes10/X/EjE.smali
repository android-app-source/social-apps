.class public final LX/EjE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Z

.field public final synthetic d:LX/30Z;


# direct methods
.method public constructor <init>(LX/30Z;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2161023
    iput-object p1, p0, LX/EjE;->d:LX/30Z;

    iput-object p2, p0, LX/EjE;->a:Ljava/lang/String;

    iput-object p3, p0, LX/EjE;->b:Ljava/lang/String;

    iput-boolean p4, p0, LX/EjE;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2161024
    const-string v0, "off"

    iget-object v1, p0, LX/EjE;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2161025
    const-string v0, "user_setting"

    iget-object v1, p0, LX/EjE;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2161026
    iget-object v0, p0, LX/EjE;->d:LX/30Z;

    iget-object v0, v0, LX/30Z;->l:LX/1wU;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1wU;->a(Z)V

    .line 2161027
    iget-object v0, p0, LX/EjE;->d:LX/30Z;

    iget-object v0, v0, LX/30Z;->e:LX/0Sh;

    new-instance v1, Lcom/facebook/contacts/ccu/ContactsUploadClient$4$2;

    invoke-direct {v1, p0}, Lcom/facebook/contacts/ccu/ContactsUploadClient$4$2;-><init>(LX/EjE;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2161028
    :cond_0
    const-string v0, "ccu_background_ping"

    iget-object v1, p0, LX/EjE;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/EjE;->c:Z

    if-eqz v0, :cond_1

    .line 2161029
    iget-object v0, p0, LX/EjE;->d:LX/30Z;

    iget-object v0, v0, LX/30Z;->l:LX/1wU;

    .line 2161030
    invoke-static {v0}, LX/1wU;->d(LX/1wU;)LX/0Tn;

    move-result-object v1

    .line 2161031
    if-nez v1, :cond_2

    .line 2161032
    :cond_1
    :goto_0
    iget-object v0, p0, LX/EjE;->d:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    iget-object v1, p0, LX/EjE;->a:Ljava/lang/String;

    iget-object v2, p0, LX/EjE;->b:Ljava/lang/String;

    iget-boolean v3, p0, LX/EjE;->c:Z

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 2161033
    iget-object v5, v0, LX/30d;->a:LX/0Zb;

    sget-object p0, LX/Ejs;->CCU_SETTING_FAILED:LX/Ejs;

    invoke-virtual {p0}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "client_ccu_enabled"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "source"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "restore_existing_setting"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "failure_message"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v5, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2161034
    return-void

    .line 2161035
    :cond_2
    iget-object v2, v0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-interface {v2, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2161003
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    .line 2161004
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161005
    if-eqz v0, :cond_0

    .line 2161006
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161007
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2161008
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161009
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->j()Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2161010
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161011
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->j()Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    move-result-object v2

    .line 2161012
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->ON:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    if-ne v2, v0, :cond_3

    move v0, v1

    .line 2161013
    :goto_0
    iget-object v3, p0, LX/EjE;->d:LX/30Z;

    iget-object v3, v3, LX/30Z;->l:LX/1wU;

    invoke-virtual {v3, v0}, LX/1wU;->a(Z)V

    .line 2161014
    iget-object v0, p0, LX/EjE;->d:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    iget-object v3, p0, LX/EjE;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/EjE;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, LX/EjE;->c:Z

    .line 2161015
    iget-object v7, v0, LX/30d;->a:LX/0Zb;

    sget-object v8, LX/Ejs;->CCU_SETTING:LX/Ejs;

    invoke-virtual {v8}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/30d;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string p1, "client_ccu_enabled"

    invoke-virtual {v8, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string p1, "server_ccu_enabled"

    invoke-virtual {v8, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string p1, "source"

    invoke-virtual {v8, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string p1, "restore_existing_setting"

    invoke-virtual {v8, p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-interface {v7, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2161016
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->name()Ljava/lang/String;

    .line 2161017
    :cond_0
    const-string v0, "off"

    iget-object v2, p0, LX/EjE;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "user_setting"

    iget-object v2, p0, LX/EjE;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2161018
    iget-object v0, p0, LX/EjE;->d:LX/30Z;

    iget-object v0, v0, LX/30Z;->e:LX/0Sh;

    new-instance v2, Lcom/facebook/contacts/ccu/ContactsUploadClient$4$1;

    invoke-direct {v2, p0}, Lcom/facebook/contacts/ccu/ContactsUploadClient$4$1;-><init>(LX/EjE;)V

    invoke-virtual {v0, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2161019
    :cond_1
    const-string v0, "ccu_background_ping"

    iget-object v2, p0, LX/EjE;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2161020
    iget-object v0, p0, LX/EjE;->d:LX/30Z;

    iget-object v0, v0, LX/30Z;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/2vf;->e:LX/0Tn;

    invoke-interface {v0, v2, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2161021
    :cond_2
    return-void

    .line 2161022
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
