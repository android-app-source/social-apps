.class public LX/EGG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/EGG;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2096984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096985
    iput-object p1, p0, LX/EGG;->a:Ljava/lang/String;

    .line 2096986
    iput-object p2, p0, LX/EGG;->b:Ljava/lang/String;

    .line 2096987
    return-void
.end method

.method private a(LX/EGG;)I
    .locals 2

    .prologue
    .line 2096967
    iget-object v0, p1, LX/EGG;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EGG;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2096968
    const/4 v0, 0x0

    .line 2096969
    :goto_0
    return v0

    .line 2096970
    :cond_0
    iget-object v0, p1, LX/EGG;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EGG;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2096971
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 2096972
    :cond_2
    iget-object v0, p0, LX/EGG;->b:Ljava/lang/String;

    iget-object v1, p1, LX/EGG;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2096983
    check-cast p1, LX/EGG;

    invoke-direct {p0, p1}, LX/EGG;->a(LX/EGG;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2096977
    instance-of v2, p1, LX/EGG;

    if-nez v2, :cond_1

    .line 2096978
    :cond_0
    :goto_0
    return v0

    .line 2096979
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 2096980
    goto :goto_0

    .line 2096981
    :cond_2
    check-cast p1, LX/EGG;

    .line 2096982
    invoke-direct {p0, p1}, LX/EGG;->a(LX/EGG;)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2096973
    iget-object v0, p0, LX/EGG;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EGG;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 2096974
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/EGG;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/EGG;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 2096975
    return v0

    :cond_1
    move v0, v1

    .line 2096976
    goto :goto_0
.end method
