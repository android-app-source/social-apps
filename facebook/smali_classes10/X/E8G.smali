.class public LX/E8G;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/Cfn;


# static fields
.field public static final a:[I


# instance fields
.field public b:LX/E1f;

.field public c:LX/1vj;

.field public d:LX/0o8;

.field public e:LX/E8M;

.field public f:LX/E1i;

.field public g:LX/3Tx;

.field private h:Lcom/facebook/reaction/common/ReactionCardNode;

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Landroid/view/ViewGroup;

.field private m:LX/Cfk;

.field public n:Landroid/view/View;

.field public o:Landroid/view/ViewGroup;

.field public p:Landroid/view/ViewGroup;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2082232
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f01072b

    aput v2, v0, v1

    sput-object v0, LX/E8G;->a:[I

    return-void
.end method

.method public constructor <init>(LX/0o8;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2082218
    invoke-direct {p0, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2082219
    iput-object p1, p0, LX/E8G;->d:LX/0o8;

    .line 2082220
    const/4 v3, 0x0

    .line 2082221
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/E8G;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v4

    check-cast v4, LX/E1f;

    invoke-static {v0}, LX/1vj;->a(LX/0QB;)LX/1vj;

    move-result-object v5

    check-cast v5, LX/1vj;

    invoke-static {v0}, LX/E8M;->b(LX/0QB;)LX/E8M;

    move-result-object p1

    check-cast p1, LX/E8M;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object p2

    check-cast p2, LX/E1i;

    invoke-static {v0}, LX/3Tx;->a(LX/0QB;)LX/3Tx;

    move-result-object v0

    check-cast v0, LX/3Tx;

    iput-object v4, v2, LX/E8G;->b:LX/E1f;

    iput-object v5, v2, LX/E8G;->c:LX/1vj;

    iput-object p1, v2, LX/E8G;->e:LX/E8M;

    iput-object p2, v2, LX/E8G;->f:LX/E1i;

    iput-object v0, v2, LX/E8G;->g:LX/3Tx;

    .line 2082222
    const v0, 0x7f03111f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2082223
    const v0, 0x7f0d2893

    invoke-virtual {p0, v0}, LX/E8G;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/E8G;->l:Landroid/view/ViewGroup;

    .line 2082224
    const v0, 0x7f0d289d

    invoke-virtual {p0, v0}, LX/E8G;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/E8G;->n:Landroid/view/View;

    .line 2082225
    const v0, 0x7f0d289c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/E8G;->o:Landroid/view/ViewGroup;

    .line 2082226
    const v0, 0x7f0d289b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/E8G;->p:Landroid/view/ViewGroup;

    .line 2082227
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, LX/E8G;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2082228
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/E8G;->setOrientation(I)V

    .line 2082229
    invoke-virtual {p0}, LX/E8G;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b163c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2082230
    invoke-virtual {p0, v3, v0, v3, v0}, LX/E8G;->setPadding(IIII)V

    .line 2082231
    return-void
.end method

.method private static a(LX/E8G;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;LX/9qX;)Ljava/util/LinkedHashMap;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionStoryFragment$ReactionAttachments;",
            "LX/9qX;",
            ")",
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "LX/Cfl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2082208
    new-instance v8, Ljava/util/LinkedHashMap;

    invoke-direct {v8}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2082209
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v10, :cond_3

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2082210
    invoke-interface {p2}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v3, 0x0

    .line 2082211
    :goto_1
    iget-object v0, p0, LX/E8G;->b:LX/E1f;

    invoke-virtual {p0}, LX/E8G;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, LX/E8G;->d:LX/0o8;

    invoke-interface {v4}, LX/0o8;->n()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/E8G;->d:LX/0o8;

    invoke-interface {v5}, LX/0o8;->o()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/E8G;->j:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    .line 2082212
    if-eqz v0, :cond_1

    .line 2082213
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2082214
    invoke-virtual {v8, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2082215
    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 2082216
    :cond_2
    invoke-interface {p2}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v0

    invoke-interface {v0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2082217
    :cond_3
    return-object v8
.end method

.method private static a(LX/E8G;LX/2jb;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;LX/Cfl;)V
    .locals 6

    .prologue
    .line 2082206
    new-instance v0, LX/E8C;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/E8C;-><init>(LX/E8G;LX/2jb;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2082207
    return-void
.end method

.method public static a(LX/E8G;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;LX/2jb;)V
    .locals 8

    .prologue
    .line 2082182
    const v0, 0x7f0d28a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2082183
    iget-object v2, p0, LX/E8G;->j:Ljava/lang/String;

    iget-object v3, p0, LX/E8G;->k:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v4

    move-object v0, p0

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/E8G;->a(LX/E8G;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Ljava/lang/String;Ljava/lang/String;LX/3Ab;LX/2jb;)V

    .line 2082184
    const v0, 0x7f0d28a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2082185
    iget-object v4, p0, LX/E8G;->j:Ljava/lang/String;

    iget-object v5, p0, LX/E8G;->k:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->j()LX/3Ab;

    move-result-object v6

    move-object v2, p0

    move-object v7, p3

    invoke-static/range {v2 .. v7}, LX/E8G;->a(LX/E8G;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Ljava/lang/String;Ljava/lang/String;LX/3Ab;LX/2jb;)V

    .line 2082186
    sget-object v0, LX/E8E;->a:[I

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2082187
    :goto_0
    return-void

    .line 2082188
    :pswitch_0
    const/16 v0, 0x11

    .line 2082189
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 2082190
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 2082191
    check-cast p1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2082192
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2082193
    goto :goto_0

    .line 2082194
    :pswitch_1
    const v2, 0x800003

    const/4 v0, 0x3

    .line 2082195
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 2082196
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 2082197
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 2082198
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 2082199
    check-cast p1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2082200
    const v0, 0x7f0a010a

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2082201
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->d()LX/5sY;

    move-result-object v0

    .line 2082202
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2082203
    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2082204
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2082205
    :cond_0
    :pswitch_2
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setGravity(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(LX/E8G;Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/Cfl;Landroid/widget/TextView;LX/2jb;Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    .line 2082090
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    .line 2082091
    if-eqz p4, :cond_0

    if-eqz v0, :cond_0

    .line 2082092
    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2082093
    :cond_0
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v4, p4

    .line 2082094
    :goto_0
    iget-object v2, p0, LX/E8G;->j:Ljava/lang/String;

    iget-object v3, p0, LX/E8G;->k:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p5

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/E8G;->a(LX/E8G;LX/2jb;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;LX/Cfl;)V

    .line 2082095
    invoke-virtual {p1, p4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 2082096
    return-void

    :cond_1
    move-object v4, p1

    .line 2082097
    goto :goto_0
.end method

.method private static a(LX/E8G;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;LX/9qX;LX/2jb;)V
    .locals 11
    .param p0    # LX/E8G;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v1, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2082163
    if-nez p1, :cond_0

    .line 2082164
    iget-object v0, p0, LX/E8G;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2082165
    :goto_0
    return-void

    .line 2082166
    :cond_0
    invoke-static {p0, p1, p2}, LX/E8G;->a(LX/E8G;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;LX/9qX;)Ljava/util/LinkedHashMap;

    move-result-object v7

    .line 2082167
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 2082168
    :cond_1
    iget-object v0, p0, LX/E8G;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2082169
    :cond_2
    iget-object v0, p0, LX/E8G;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2082170
    invoke-virtual {p0}, LX/E8G;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031120

    iget-object v2, p0, LX/E8G;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2082171
    invoke-virtual {v7}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-ne v0, v10, :cond_4

    .line 2082172
    invoke-virtual {p0}, LX/E8G;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03112b

    invoke-virtual {v0, v2, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2082173
    invoke-virtual {v7}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2082174
    invoke-virtual {v7, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Cfl;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v0, p0

    move-object v5, p3

    invoke-static/range {v0 .. v6}, LX/E8G;->a(LX/E8G;Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/Cfl;Landroid/widget/TextView;LX/2jb;Ljava/lang/Boolean;)V

    .line 2082175
    new-instance v0, LX/E8F;

    invoke-direct {v0}, LX/E8F;-><init>()V

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2082176
    :cond_3
    iget-object v0, p0, LX/E8G;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2082177
    :cond_4
    invoke-virtual {v7}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-le v0, v10, :cond_3

    .line 2082178
    invoke-virtual {v7}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2082179
    invoke-virtual {p0}, LX/E8G;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f031129

    invoke-virtual {v0, v3, v1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2082180
    invoke-virtual {v7, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Cfl;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v0, p0

    move-object v5, p3

    invoke-static/range {v0 .. v6}, LX/E8G;->a(LX/E8G;Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/Cfl;Landroid/widget/TextView;LX/2jb;Ljava/lang/Boolean;)V

    .line 2082181
    new-instance v0, LX/8sz;

    invoke-direct {v0}, LX/8sz;-><init>()V

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1
.end method

.method private static a(LX/E8G;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Ljava/lang/String;Ljava/lang/String;LX/3Ab;LX/2jb;)V
    .locals 1
    .param p0    # LX/E8G;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2082157
    if-nez p1, :cond_0

    .line 2082158
    :goto_0
    return-void

    .line 2082159
    :cond_0
    if-eqz p4, :cond_1

    invoke-interface {p4}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2082160
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_0

    .line 2082161
    :cond_2
    new-instance v0, LX/E8D;

    invoke-direct {v0, p0, p5, p2, p3}, LX/E8D;-><init>(LX/E8G;LX/2jb;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p4, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/3Ab;LX/7wC;)V

    .line 2082162
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b(LX/E8G;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;LX/2jb;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 2082143
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2082144
    invoke-virtual {p1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2082145
    :goto_0
    return-void

    .line 2082146
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2082147
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->e()LX/3Ab;

    move-result-object v0

    invoke-interface {v0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v3

    .line 2082148
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->EVENTS_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_2

    .line 2082149
    iget-object v0, p0, LX/E8G;->e:LX/E8M;

    iget-object v1, p0, LX/E8G;->h:Lcom/facebook/reaction/common/ReactionCardNode;

    check-cast p1, Landroid/widget/TextView;

    iget-object v2, p0, LX/E8G;->d:LX/0o8;

    invoke-interface {v2}, LX/0o8;->m()LX/2ja;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LX/E8M;->a(Lcom/facebook/reaction/common/ReactionCardNode;Landroid/widget/TextView;LX/2ja;)V

    goto :goto_0

    .line 2082150
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_INLINE_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 2082151
    check-cast v0, Landroid/widget/TextView;

    .line 2082152
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2082153
    invoke-virtual {p0}, LX/E8G;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->hh_()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f0a00d2

    :goto_1
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2082154
    :cond_3
    iget-object v0, p0, LX/E8G;->j:Ljava/lang/String;

    move-object v7, v0

    .line 2082155
    iget-object v8, p0, LX/E8G;->k:Ljava/lang/String;

    iget-object v0, p0, LX/E8G;->b:LX/E1f;

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {p0}, LX/E8G;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, LX/E8G;->d:LX/0o8;

    invoke-interface {v4}, LX/0o8;->n()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/E8G;->d:LX/0o8;

    invoke-interface {v5}, LX/0o8;->o()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/E8G;->j:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v5

    move-object v0, p0

    move-object v1, p3

    move-object v2, v7

    move-object v3, v8

    move-object v4, p1

    invoke-static/range {v0 .. v5}, LX/E8G;->a(LX/E8G;LX/2jb;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;LX/Cfl;)V

    goto/16 :goto_0

    .line 2082156
    :cond_4
    const v1, 0x7f0a010e

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/9qX;LX/Cfx;LX/Cgb;)V
    .locals 12
    .param p3    # LX/Cgb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2082103
    invoke-interface {p1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/E8G;->j:Ljava/lang/String;

    .line 2082104
    invoke-interface {p1}, LX/9qX;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/E8G;->k:Ljava/lang/String;

    .line 2082105
    invoke-interface {p1}, LX/9qX;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    .line 2082106
    iget-object v1, p2, LX/Cfx;->a:LX/Cfk;

    move-object v1, v1

    .line 2082107
    iput-object v1, p0, LX/E8G;->m:LX/Cfk;

    .line 2082108
    iget-object v1, p0, LX/E8G;->d:LX/0o8;

    invoke-interface {v1}, LX/0o8;->m()LX/2ja;

    move-result-object v1

    .line 2082109
    iget-object v2, p0, LX/E8G;->l:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2082110
    iget-object v2, p0, LX/E8G;->o:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2082111
    iget-object v2, p0, LX/E8G;->p:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2082112
    iget-object v2, p0, LX/E8G;->m:LX/Cfk;

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 2082113
    if-eqz v0, :cond_0

    if-nez v2, :cond_3

    .line 2082114
    :cond_0
    iget-object v3, p0, LX/E8G;->l:Landroid/view/ViewGroup;

    invoke-virtual {v3, v11}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2082115
    :goto_0
    const/4 v5, 0x0

    .line 2082116
    invoke-static {p1}, LX/Cfw;->b(LX/9qX;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2082117
    :goto_1
    iget-object v2, p0, LX/E8G;->m:LX/Cfk;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/E8G;->m:LX/Cfk;

    invoke-virtual {v2}, LX/Cfk;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2082118
    :cond_1
    invoke-static {p0, v0, p1, v1}, LX/E8G;->a(LX/E8G;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;LX/9qX;LX/2jb;)V

    .line 2082119
    :cond_2
    return-void

    .line 2082120
    :cond_3
    iget-object v3, p0, LX/E8G;->l:Landroid/view/ViewGroup;

    invoke-virtual {v3, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2082121
    iget-object v5, p0, LX/E8G;->l:Landroid/view/ViewGroup;

    iget-object v6, p0, LX/E8G;->d:LX/0o8;

    iget-object v3, p0, LX/E8G;->d:LX/0o8;

    invoke-interface {v3}, LX/0o8;->n()Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, LX/E8G;->d:LX/0o8;

    invoke-interface {v3}, LX/0o8;->o()Ljava/lang/String;

    move-result-object v8

    move-object v3, v2

    move-object v4, v1

    move-object v9, p3

    invoke-virtual/range {v3 .. v9}, LX/Cfk;->a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V

    .line 2082122
    iget-object v3, p0, LX/E8G;->j:Ljava/lang/String;

    iget-object v4, p0, LX/E8G;->k:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Z

    .line 2082123
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    iput v3, p0, LX/E8G;->i:I

    .line 2082124
    invoke-virtual {v2}, LX/Cfk;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2082125
    iget-object v3, p0, LX/E8G;->l:Landroid/view/ViewGroup;

    invoke-virtual {v3, v10}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 2082126
    iget-object v3, p0, LX/E8G;->n:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->setVisibility(I)V

    .line 2082127
    iget-object v3, p0, LX/E8G;->o:Landroid/view/ViewGroup;

    invoke-virtual {v3, v11}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2082128
    :cond_4
    invoke-virtual {p0}, LX/E8G;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/E8G;->a:[I

    invoke-virtual {v3, v4}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 2082129
    iget-object v4, p0, LX/E8G;->l:Landroid/view/ViewGroup;

    invoke-virtual {v3, v10}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2082130
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 2082131
    iget-object v3, p0, LX/E8G;->n:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2082132
    iget-object v3, p0, LX/E8G;->o:Landroid/view/ViewGroup;

    invoke-virtual {v3, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 2082133
    :cond_5
    invoke-interface {p1}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    move-result-object v2

    .line 2082134
    sget-object v3, LX/E8E;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_1

    .line 2082135
    :pswitch_0
    invoke-virtual {p0}, LX/E8G;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f031123

    iget-object v4, p0, LX/E8G;->p:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    move-object v3, v2

    .line 2082136
    :goto_2
    invoke-interface {p1}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v4

    invoke-static {p0, v3, v4, v1}, LX/E8G;->a(LX/E8G;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;LX/2jb;)V

    .line 2082137
    invoke-interface {p1}, LX/9qX;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v4

    invoke-static {p0, v2, v4, v1}, LX/E8G;->b(LX/E8G;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;LX/2jb;)V

    .line 2082138
    iget-object v2, p0, LX/E8G;->p:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2082139
    :pswitch_1
    invoke-virtual {p0}, LX/E8G;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f031127

    iget-object v4, p0, LX/E8G;->p:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2082140
    const v2, 0x7f0d28a8

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    goto :goto_2

    .line 2082141
    :pswitch_2
    invoke-virtual {p0}, LX/E8G;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f031122

    iget-object v4, p0, LX/E8G;->p:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2082142
    const v2, 0x7f0d28a1

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getActionHandler()LX/E1f;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2082102
    iget-object v0, p0, LX/E8G;->b:LX/E1f;

    return-object v0
.end method

.method public getAttachmentStyleMapper()LX/1vj;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2082101
    iget-object v0, p0, LX/E8G;->c:LX/1vj;

    return-object v0
.end method

.method public getNumAttachmentsLoaded()I
    .locals 1

    .prologue
    .line 2082100
    iget v0, p0, LX/E8G;->i:I

    return v0
.end method

.method public getUnitType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2082099
    iget-object v0, p0, LX/E8G;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2082098
    return-object p0
.end method
