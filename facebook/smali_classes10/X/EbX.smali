.class public LX/EbX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Eaf;

.field public final b:LX/Eau;

.field public final c:LX/Eae;

.field public final d:LX/Eat;

.field public final e:LX/Ecs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ecs",
            "<",
            "Lorg/whispersystems/libsignal/ecc/ECPublicKey;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/Eat;


# direct methods
.method private constructor <init>(LX/Eaf;LX/Eau;LX/Eae;LX/Eat;LX/Eat;LX/Ecs;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Eaf;",
            "LX/Eau;",
            "LX/Eae;",
            "Lorg/whispersystems/libsignal/ecc/ECPublicKey;",
            "Lorg/whispersystems/libsignal/ecc/ECPublicKey;",
            "LX/Ecs",
            "<",
            "Lorg/whispersystems/libsignal/ecc/ECPublicKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2144037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144038
    iput-object p1, p0, LX/EbX;->a:LX/Eaf;

    .line 2144039
    iput-object p2, p0, LX/EbX;->b:LX/Eau;

    .line 2144040
    iput-object p3, p0, LX/EbX;->c:LX/Eae;

    .line 2144041
    iput-object p4, p0, LX/EbX;->d:LX/Eat;

    .line 2144042
    iput-object p5, p0, LX/EbX;->f:LX/Eat;

    .line 2144043
    iput-object p6, p0, LX/EbX;->e:LX/Ecs;

    .line 2144044
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-nez p6, :cond_1

    .line 2144045
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null values!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2144046
    :cond_1
    return-void
.end method

.method public synthetic constructor <init>(LX/Eaf;LX/Eau;LX/Eae;LX/Eat;LX/Eat;LX/Ecs;B)V
    .locals 0

    .prologue
    .line 2144047
    invoke-direct/range {p0 .. p6}, LX/EbX;-><init>(LX/Eaf;LX/Eau;LX/Eae;LX/Eat;LX/Eat;LX/Ecs;)V

    return-void
.end method
