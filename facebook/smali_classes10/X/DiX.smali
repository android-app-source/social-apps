.class public LX/DiX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DiR;


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2032189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2032190
    iput-object p1, p0, LX/DiX;->a:Ljava/lang/String;

    .line 2032191
    iput-object p2, p0, LX/DiX;->b:Ljava/lang/String;

    .line 2032192
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2032193
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2032194
    iget-object v1, p0, LX/DiX;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2032195
    const-string v1, "provider"

    iget-object v2, p0, LX/DiX;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2032196
    :cond_0
    iget-object v1, p0, LX/DiX;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2032197
    const-string v1, "destination"

    iget-object v2, p0, LX/DiX;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2032198
    :cond_1
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/DiR;)Z
    .locals 1

    .prologue
    .line 2032199
    instance-of v0, p1, LX/DiX;

    if-nez v0, :cond_0

    .line 2032200
    const/4 v0, 0x0

    .line 2032201
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
