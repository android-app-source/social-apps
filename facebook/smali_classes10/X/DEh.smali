.class public LX/DEh;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field private final b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/175;LX/175;LX/DEb;)V
    .locals 2

    .prologue
    .line 1977250
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1977251
    const v0, 0x7f030f80

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1977252
    const v0, 0x7f0d2568

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/DEh;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1977253
    const v0, 0x7f0d2569

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/DEh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1977254
    const v0, 0x7f0d256a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/DEh;->c:Landroid/view/View;

    .line 1977255
    const v0, 0x7f0d256b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/DEh;->d:Landroid/view/View;

    .line 1977256
    const v0, 0x7f0d256c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/DEh;->e:Landroid/view/View;

    .line 1977257
    iget-object v0, p0, LX/DEh;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v0, p2}, LX/DEj;->a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;LX/175;)V

    .line 1977258
    iget-object v0, p0, LX/DEh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v0, p3}, LX/DEj;->a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;LX/175;)V

    .line 1977259
    iget-object v0, p0, LX/DEh;->c:Landroid/view/View;

    new-instance v1, LX/DEe;

    invoke-direct {v1, p0, p4}, LX/DEe;-><init>(LX/DEh;LX/DEb;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1977260
    iget-object v0, p0, LX/DEh;->d:Landroid/view/View;

    new-instance v1, LX/DEf;

    invoke-direct {v1, p0, p4}, LX/DEf;-><init>(LX/DEh;LX/DEb;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1977261
    iget-object v0, p0, LX/DEh;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1977262
    iget-object v0, p0, LX/DEh;->e:Landroid/view/View;

    new-instance v1, LX/DEg;

    invoke-direct {v1, p0, p4}, LX/DEg;-><init>(LX/DEh;LX/DEb;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1977263
    return-void
.end method
