.class public LX/DVb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2005439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2005440
    iput-object p1, p0, LX/DVb;->a:Landroid/content/res/Resources;

    .line 2005441
    return-void
.end method

.method public static a(LX/0QB;)LX/DVb;
    .locals 2

    .prologue
    .line 2005442
    new-instance v1, LX/DVb;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/DVb;-><init>(Landroid/content/res/Resources;)V

    .line 2005443
    move-object v0, v1

    .line 2005444
    return-object v0
.end method

.method private static a(LX/DVb;II)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2005445
    if-nez p1, :cond_0

    .line 2005446
    const-string v0, ""

    .line 2005447
    :goto_0
    return-object v0

    .line 2005448
    :cond_0
    if-eqz p2, :cond_1

    .line 2005449
    iget-object v0, p0, LX/DVb;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f0159

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2005450
    :cond_1
    iget-object v0, p0, LX/DVb;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f015a

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/DVb;III)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2005451
    if-nez p1, :cond_0

    .line 2005452
    const-string v0, ""

    .line 2005453
    :goto_0
    return-object v0

    .line 2005454
    :cond_0
    if-nez p2, :cond_1

    if-eqz p3, :cond_2

    .line 2005455
    :cond_1
    iget-object v0, p0, LX/DVb;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f0157

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2005456
    :cond_2
    iget-object v0, p0, LX/DVb;->a:Landroid/content/res/Resources;

    const v1, 0x7f0f0158

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/DVb;IIIIIII)Ljava/lang/String;
    .locals 12

    .prologue
    .line 2005457
    add-int v3, p2, p3

    .line 2005458
    add-int v2, v3, p4

    if-nez v2, :cond_0

    const/4 v2, 0x1

    .line 2005459
    :goto_0
    if-eqz v2, :cond_1

    .line 2005460
    iget-object v2, p0, LX/DVb;->a:Landroid/content/res/Resources;

    const v3, 0x7f0f0156

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, p1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2005461
    :goto_1
    return-object v2

    .line 2005462
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2005463
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2005464
    if-nez p2, :cond_6

    if-eqz p3, :cond_6

    const/4 v2, 0x1

    .line 2005465
    :goto_2
    move/from16 v0, p4

    move/from16 v1, p6

    invoke-static {p0, v0, v1}, LX/DVb;->a(LX/DVb;II)Ljava/lang/String;

    move-result-object v5

    .line 2005466
    move/from16 v0, p5

    move/from16 v1, p7

    invoke-static {p0, v3, v0, v1}, LX/DVb;->a(LX/DVb;III)Ljava/lang/String;

    move-result-object v6

    .line 2005467
    iget-object v7, p0, LX/DVb;->a:Landroid/content/res/Resources;

    const v8, 0x7f0f015b

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v3, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2005468
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2005469
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2005470
    :cond_2
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2005471
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-eqz v6, :cond_3

    .line 2005472
    const/16 v6, 0x20

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2005473
    :cond_3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2005474
    :cond_4
    if-eqz v2, :cond_5

    .line 2005475
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-eqz v2, :cond_5

    .line 2005476
    const/16 v2, 0x20

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2005477
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2005478
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 2005479
    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;I)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2005480
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005481
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2005482
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005483
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    .line 2005484
    :goto_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005485
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2005486
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005487
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    move v4, v0

    .line 2005488
    :goto_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005489
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2005490
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005491
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    .line 2005492
    :goto_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005493
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2005494
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005495
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    .line 2005496
    :goto_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005497
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2005498
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005499
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    .line 2005500
    :goto_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005501
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->m()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2005502
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005503
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    move v7, v0

    .line 2005504
    :goto_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005505
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->n()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2005506
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005507
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_6
    add-int/2addr v4, v0

    .line 2005508
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005509
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2005510
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005511
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    :cond_0
    add-int/2addr v7, v1

    move-object v0, p0

    move v1, p2

    .line 2005512
    invoke-static/range {v0 .. v7}, LX/DVb;->a(LX/DVb;IIIIIII)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    .line 2005513
    goto/16 :goto_0

    :cond_2
    move v4, v1

    .line 2005514
    goto/16 :goto_1

    :cond_3
    move v5, v1

    .line 2005515
    goto/16 :goto_2

    :cond_4
    move v6, v1

    .line 2005516
    goto :goto_3

    :cond_5
    move v3, v1

    .line 2005517
    goto :goto_4

    :cond_6
    move v7, v1

    .line 2005518
    goto :goto_5

    :cond_7
    move v0, v1

    .line 2005519
    goto :goto_6
.end method
