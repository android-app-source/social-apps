.class public LX/E1q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2071501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;
    .locals 2

    .prologue
    .line 2071502
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2071503
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 2071504
    :goto_0
    if-eqz v0, :cond_1

    .line 2071505
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2071506
    instance-of v1, v1, LX/2nq;

    if-eqz v1, :cond_0

    .line 2071507
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2071508
    check-cast v0, LX/2nq;

    return-object v0

    .line 2071509
    :cond_0
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 2071510
    goto :goto_0

    .line 2071511
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getNotificationEdgeFromReactionNode should only be called from a FeedProp that stems from a notification"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2071512
    invoke-static {p0}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 2071513
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2071514
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2071515
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getNotificationIdFromReactionNode should only be called from a FeedProp that stems from a notification"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
