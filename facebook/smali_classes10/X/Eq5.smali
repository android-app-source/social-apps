.class public final LX/Eq5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/events/invite/EventInviteeToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V
    .locals 0

    .prologue
    .line 2170980
    iput-object p1, p0, LX/Eq5;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2170981
    iget-object v0, p0, LX/Eq5;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->z:LX/ErU;

    invoke-virtual {v0}, LX/ErU;->b()V

    .line 2170982
    iget-object v0, p0, LX/Eq5;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->x:LX/03V;

    sget-object v1, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->G:Ljava/lang/String;

    const-string v2, "Failed to fetch facebook friends"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2170983
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2170984
    move-object v1, v1

    .line 2170985
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2170986
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2170987
    check-cast p1, LX/0P1;

    .line 2170988
    iget-object v0, p0, LX/Eq5;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170989
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->n()Z

    move-result v1

    move v0, v1

    .line 2170990
    if-eqz v0, :cond_0

    .line 2170991
    :goto_0
    return-void

    .line 2170992
    :cond_0
    iget-object v0, p0, LX/Eq5;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170993
    iput-object p1, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->I:LX/0P1;

    .line 2170994
    iget-object v0, p0, LX/Eq5;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 2170995
    iget-object v0, p0, LX/Eq5;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-static {v0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->P(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    goto :goto_0
.end method
