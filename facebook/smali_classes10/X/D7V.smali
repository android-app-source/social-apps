.class public final synthetic LX/D7V;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1967165
    invoke-static {}, LX/2qY;->values()[LX/2qY;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/D7V;->b:[I

    :try_start_0
    sget-object v0, LX/D7V;->b:[I

    sget-object v1, LX/2qY;->SAVE_OFFLINE:LX/2qY;

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_0
    :try_start_1
    sget-object v0, LX/D7V;->b:[I

    sget-object v1, LX/2qY;->DOWNLOAD:LX/2qY;

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_1
    :try_start_2
    sget-object v0, LX/D7V;->b:[I

    sget-object v1, LX/2qY;->DOWNLOAD_TO_FACEBOOK:LX/2qY;

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    .line 1967166
    :goto_2
    invoke-static {}, LX/1A0;->values()[LX/1A0;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/D7V;->a:[I

    :try_start_3
    sget-object v0, LX/D7V;->a:[I

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_3
    :try_start_4
    sget-object v0, LX/D7V;->a:[I

    sget-object v1, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_4
    :try_start_5
    sget-object v0, LX/D7V;->a:[I

    sget-object v1, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_5
    :try_start_6
    sget-object v0, LX/D7V;->a:[I

    sget-object v1, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_6
    :try_start_7
    sget-object v0, LX/D7V;->a:[I

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_7
    :try_start_8
    sget-object v0, LX/D7V;->a:[I

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_8
    :try_start_9
    sget-object v0, LX/D7V;->a:[I

    sget-object v1, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_9
    return-void

    :catch_0
    goto :goto_9

    :catch_1
    goto :goto_8

    :catch_2
    goto :goto_7

    :catch_3
    goto :goto_6

    :catch_4
    goto :goto_5

    :catch_5
    goto :goto_4

    :catch_6
    goto :goto_3

    :catch_7
    goto :goto_2

    :catch_8
    goto :goto_1

    :catch_9
    goto :goto_0
.end method
