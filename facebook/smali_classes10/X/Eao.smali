.class public LX/Eao;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Object;


# instance fields
.field private final b:LX/DoQ;

.field private final c:LX/Eam;

.field private final d:LX/DoP;

.field private final e:LX/Eap;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2142565
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Eao;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/DoQ;LX/DoP;LX/DoR;LX/DoO;LX/Eap;)V
    .locals 6

    .prologue
    .line 2142559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142560
    iput-object p1, p0, LX/Eao;->b:LX/DoQ;

    .line 2142561
    iput-object p2, p0, LX/Eao;->d:LX/DoP;

    .line 2142562
    iput-object p5, p0, LX/Eao;->e:LX/Eap;

    .line 2142563
    new-instance v0, LX/Eam;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/Eam;-><init>(LX/DoQ;LX/DoP;LX/DoR;LX/DoO;LX/Eap;)V

    iput-object v0, p0, LX/Eao;->c:LX/Eam;

    .line 2142564
    return-void
.end method

.method public constructor <init>(LX/DoS;LX/Eap;)V
    .locals 6

    .prologue
    .line 2142557
    move-object v0, p0

    move-object v1, p1

    move-object v2, p1

    move-object v3, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/Eao;-><init>(LX/DoQ;LX/DoP;LX/DoR;LX/DoO;LX/Eap;)V

    .line 2142558
    return-void
.end method

.method private static a(LX/Ebj;LX/Eat;)LX/Eba;
    .locals 5

    .prologue
    .line 2142333
    :try_start_0
    invoke-static {p0, p1}, LX/Ebj;->c(LX/Ebj;LX/Eat;)LX/Ecr;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2142334
    if-eqz v0, :cond_0

    .line 2142335
    invoke-static {p0, p1}, LX/Ebj;->c(LX/Ebj;LX/Eat;)LX/Ecr;

    move-result-object v0

    .line 2142336
    iget-object v1, v0, LX/Ecr;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2142337
    check-cast v0, LX/EcW;

    .line 2142338
    if-nez v0, :cond_2

    .line 2142339
    const/4 v0, 0x0

    .line 2142340
    :goto_1
    move-object v0, v0

    .line 2142341
    :goto_2
    return-object v0

    .line 2142342
    :cond_0
    invoke-virtual {p0}, LX/Ebj;->g()LX/Ebe;

    move-result-object v0

    .line 2142343
    invoke-virtual {p0}, LX/Ebj;->h()LX/Eat;

    move-result-object v1

    .line 2142344
    iget-object v2, p0, LX/Ebj;->a:LX/Ecf;

    .line 2142345
    iget-object v3, v2, LX/Ecf;->senderChain_:LX/EcW;

    move-object v2, v3

    .line 2142346
    iget-object v3, v2, LX/EcW;->senderRatchetKeyPrivate_:LX/EWc;

    move-object v2, v3

    .line 2142347
    invoke-virtual {v2}, LX/EWc;->d()[B

    move-result-object v2

    invoke-static {v2}, LX/Ear;->a([B)LX/Eas;

    move-result-object v2

    .line 2142348
    new-instance v3, LX/Eau;

    invoke-direct {v3, v1, v2}, LX/Eau;-><init>(LX/Eat;LX/Eas;)V

    move-object v1, v3

    .line 2142349
    invoke-virtual {v0, p1, v1}, LX/Ebe;->a(LX/Eat;LX/Eau;)LX/Ecr;

    move-result-object v1

    .line 2142350
    invoke-static {}, LX/Ear;->a()LX/Eau;

    move-result-object v2

    .line 2142351
    iget-object v0, v1, LX/Ecr;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2142352
    check-cast v0, LX/Ebe;

    invoke-virtual {v0, p1, v2}, LX/Ebe;->a(LX/Eat;LX/Eau;)LX/Ecr;

    move-result-object v3

    .line 2142353
    iget-object v0, v3, LX/Ecr;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2142354
    check-cast v0, LX/Ebe;

    invoke-virtual {p0, v0}, LX/Ebj;->a(LX/Ebe;)V

    .line 2142355
    iget-object v0, v1, LX/Ecr;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 2142356
    check-cast v0, LX/Eba;

    invoke-virtual {p0, p1, v0}, LX/Ebj;->a(LX/Eat;LX/Eba;)V

    .line 2142357
    invoke-virtual {p0}, LX/Ebj;->k()LX/Eba;

    move-result-object v0

    .line 2142358
    iget v4, v0, LX/Eba;->e:I

    move v0, v4

    .line 2142359
    add-int/lit8 v0, v0, -0x1

    const/4 v4, 0x0

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2142360
    iget-object v4, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v4}, LX/Ecf;->O()LX/EcK;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/EcK;->b(I)LX/EcK;

    move-result-object v4

    invoke-virtual {v4}, LX/EcK;->l()LX/Ecf;

    move-result-object v4

    iput-object v4, p0, LX/Ebj;->a:LX/Ecf;

    .line 2142361
    iget-object v0, v3, LX/Ecr;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 2142362
    check-cast v0, LX/Eba;

    invoke-virtual {p0, v2, v0}, LX/Ebj;->a(LX/Eau;LX/Eba;)V

    .line 2142363
    iget-object v0, v1, LX/Ecr;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 2142364
    check-cast v0, LX/Eba;
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2142365
    :catch_0
    move-exception v0

    .line 2142366
    new-instance v1, LX/Eai;

    invoke-direct {v1, v0}, LX/Eai;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    :try_start_1
    const/4 v0, 0x0

    goto/16 :goto_0
    :try_end_1
    .catch LX/Eag; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    new-instance v1, LX/Eba;

    invoke-virtual {p0}, LX/Ebj;->c()I

    move-result v2

    invoke-static {v2}, LX/Eb5;->a(I)LX/Eb5;

    move-result-object v2

    .line 2142367
    iget-object v3, v0, LX/EcW;->chainKey_:LX/EcR;

    move-object v3, v3

    .line 2142368
    iget-object p0, v3, LX/EcR;->key_:LX/EWc;

    move-object v3, p0

    .line 2142369
    invoke-virtual {v3}, LX/EWc;->d()[B

    move-result-object v3

    .line 2142370
    iget-object p0, v0, LX/EcW;->chainKey_:LX/EcR;

    move-object v0, p0

    .line 2142371
    iget p0, v0, LX/EcR;->index_:I

    move v0, p0

    .line 2142372
    invoke-direct {v1, v2, v3, v0}, LX/Eba;-><init>(LX/Eb5;[BI)V

    move-object v0, v1

    goto/16 :goto_1
.end method

.method private static a(LX/Ebj;LX/Eat;LX/Eba;I)LX/Ebb;
    .locals 9

    .prologue
    .line 2142461
    iget v0, p2, LX/Eba;->e:I

    move v0, v0

    .line 2142462
    if-le v0, p3, :cond_1

    .line 2142463
    invoke-virtual {p0, p1, p3}, LX/Ebj;->a(LX/Eat;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2142464
    const/4 v2, 0x0

    .line 2142465
    invoke-static {p0, p1}, LX/Ebj;->c(LX/Ebj;LX/Eat;)LX/Ecr;

    move-result-object v3

    .line 2142466
    iget-object v0, v3, LX/Ecr;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2142467
    check-cast v0, LX/EcW;

    .line 2142468
    if-nez v0, :cond_5

    .line 2142469
    :goto_0
    move-object v0, v2

    .line 2142470
    :goto_1
    return-object v0

    .line 2142471
    :cond_0
    new-instance v0, LX/Ead;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received message with old counter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2142472
    iget v2, p2, LX/Eba;->e:I

    move v2, v2

    .line 2142473
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Ead;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142474
    :cond_1
    iget v0, p2, LX/Eba;->e:I

    move v0, v0

    .line 2142475
    sub-int v0, p3, v0

    const/16 v1, 0x7d0

    if-le v0, v1, :cond_2

    .line 2142476
    new-instance v0, LX/Eai;

    const-string v1, "Over 2000 messages into the future!"

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142477
    :cond_2
    :goto_2
    iget v0, p2, LX/Eba;->e:I

    move v0, v0

    .line 2142478
    if-ge v0, p3, :cond_4

    .line 2142479
    invoke-virtual {p2}, LX/Eba;->d()LX/Ebb;

    move-result-object v0

    .line 2142480
    invoke-static {p0, p1}, LX/Ebj;->c(LX/Ebj;LX/Eat;)LX/Ecr;

    move-result-object v2

    .line 2142481
    iget-object v1, v2, LX/Ecr;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2142482
    check-cast v1, LX/EcW;

    .line 2142483
    invoke-static {}, LX/EcU;->u()LX/EcU;

    move-result-object v3

    .line 2142484
    iget-object v4, v0, LX/Ebb;->a:Ljavax/crypto/spec/SecretKeySpec;

    move-object v4, v4

    .line 2142485
    invoke-virtual {v4}, Ljavax/crypto/spec/SecretKeySpec;->getEncoded()[B

    move-result-object v4

    invoke-static {v4}, LX/EWc;->a([B)LX/EWc;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/EcU;->a(LX/EWc;)LX/EcU;

    move-result-object v3

    .line 2142486
    iget-object v4, v0, LX/Ebb;->b:Ljavax/crypto/spec/SecretKeySpec;

    move-object v4, v4

    .line 2142487
    invoke-virtual {v4}, Ljavax/crypto/spec/SecretKeySpec;->getEncoded()[B

    move-result-object v4

    invoke-static {v4}, LX/EWc;->a([B)LX/EWc;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/EcU;->b(LX/EWc;)LX/EcU;

    move-result-object v3

    .line 2142488
    iget v4, v0, LX/Ebb;->d:I

    move v4, v4

    .line 2142489
    invoke-virtual {v3, v4}, LX/EcU;->a(I)LX/EcU;

    move-result-object v3

    .line 2142490
    iget-object v4, v0, LX/Ebb;->c:Ljavax/crypto/spec/IvParameterSpec;

    move-object v4, v4

    .line 2142491
    invoke-virtual {v4}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v4

    invoke-static {v4}, LX/EWc;->a([B)LX/EWc;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/EcU;->c(LX/EWc;)LX/EcU;

    move-result-object v3

    invoke-virtual {v3}, LX/EcU;->l()LX/EcV;

    move-result-object v3

    .line 2142492
    invoke-virtual {v1}, LX/EcW;->r()LX/EcN;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/EcN;->a(LX/EcV;)LX/EcN;

    move-result-object v3

    .line 2142493
    iget-object v1, v3, LX/EcN;->g:LX/EZ2;

    if-nez v1, :cond_a

    .line 2142494
    iget-object v1, v3, LX/EcN;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 2142495
    :goto_3
    move v1, v1

    .line 2142496
    const/16 v4, 0x7d0

    if-le v1, v4, :cond_3

    .line 2142497
    const/4 v1, 0x0

    .line 2142498
    iget-object v4, v3, LX/EcN;->g:LX/EZ2;

    if-nez v4, :cond_b

    .line 2142499
    invoke-static {v3}, LX/EcN;->C(LX/EcN;)V

    .line 2142500
    iget-object v4, v3, LX/EcN;->f:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2142501
    invoke-virtual {v3}, LX/EWj;->t()V

    .line 2142502
    :cond_3
    :goto_4
    iget-object v1, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v1}, LX/Ecf;->O()LX/EcK;

    move-result-object v4

    .line 2142503
    iget-object v1, v2, LX/Ecr;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 2142504
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3}, LX/EcN;->l()LX/EcW;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, LX/EcK;->a(ILX/EcW;)LX/EcK;

    move-result-object v1

    invoke-virtual {v1}, LX/EcK;->l()LX/Ecf;

    move-result-object v1

    iput-object v1, p0, LX/Ebj;->a:LX/Ecf;

    .line 2142505
    invoke-virtual {p2}, LX/Eba;->c()LX/Eba;

    move-result-object p2

    goto/16 :goto_2

    .line 2142506
    :cond_4
    invoke-virtual {p2}, LX/Eba;->c()LX/Eba;

    move-result-object v0

    .line 2142507
    invoke-static {p0, p1}, LX/Ebj;->c(LX/Ebj;LX/Eat;)LX/Ecr;

    move-result-object v2

    .line 2142508
    iget-object v1, v2, LX/Ecr;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2142509
    check-cast v1, LX/EcW;

    .line 2142510
    invoke-static {}, LX/EcQ;->w()LX/EcQ;

    move-result-object v3

    .line 2142511
    iget-object v4, v0, LX/Eba;->d:[B

    move-object v4, v4

    .line 2142512
    invoke-static {v4}, LX/EWc;->a([B)LX/EWc;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/EcQ;->a(LX/EWc;)LX/EcQ;

    move-result-object v3

    .line 2142513
    iget v4, v0, LX/Eba;->e:I

    move v4, v4

    .line 2142514
    invoke-virtual {v3, v4}, LX/EcQ;->a(I)LX/EcQ;

    move-result-object v3

    invoke-virtual {v3}, LX/EcQ;->l()LX/EcR;

    move-result-object v3

    .line 2142515
    invoke-virtual {v1}, LX/EcW;->r()LX/EcN;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/EcN;->a(LX/EcR;)LX/EcN;

    move-result-object v1

    invoke-virtual {v1}, LX/EcN;->l()LX/EcW;

    move-result-object v3

    .line 2142516
    iget-object v1, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v1}, LX/Ecf;->O()LX/EcK;

    move-result-object v4

    .line 2142517
    iget-object v1, v2, LX/Ecr;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 2142518
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v4, v1, v3}, LX/EcK;->a(ILX/EcW;)LX/EcK;

    move-result-object v1

    invoke-virtual {v1}, LX/EcK;->l()LX/Ecf;

    move-result-object v1

    iput-object v1, p0, LX/Ebj;->a:LX/Ecf;

    .line 2142519
    invoke-virtual {p2}, LX/Eba;->d()LX/Ebb;

    move-result-object v0

    goto/16 :goto_1

    .line 2142520
    :cond_5
    new-instance v4, Ljava/util/LinkedList;

    .line 2142521
    iget-object v1, v0, LX/EcW;->messageKeys_:Ljava/util/List;

    move-object v1, v1

    .line 2142522
    invoke-direct {v4, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 2142523
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 2142524
    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2142525
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EcV;

    .line 2142526
    iget v6, v1, LX/EcV;->index_:I

    move v6, v6

    .line 2142527
    if-ne v6, p3, :cond_6

    .line 2142528
    new-instance v2, LX/Ebb;

    new-instance v6, Ljavax/crypto/spec/SecretKeySpec;

    .line 2142529
    iget-object v7, v1, LX/EcV;->cipherKey_:LX/EWc;

    move-object v7, v7

    .line 2142530
    invoke-virtual {v7}, LX/EWc;->d()[B

    move-result-object v7

    const-string v8, "AES"

    invoke-direct {v6, v7, v8}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    .line 2142531
    iget-object v8, v1, LX/EcV;->macKey_:LX/EWc;

    move-object v8, v8

    .line 2142532
    invoke-virtual {v8}, LX/EWc;->d()[B

    move-result-object v8

    const-string p2, "HmacSHA256"

    invoke-direct {v7, v8, p2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    new-instance v8, Ljavax/crypto/spec/IvParameterSpec;

    .line 2142533
    iget-object p2, v1, LX/EcV;->iv_:LX/EWc;

    move-object p2, p2

    .line 2142534
    invoke-virtual {p2}, LX/EWc;->d()[B

    move-result-object p2

    invoke-direct {v8, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 2142535
    iget p2, v1, LX/EcV;->index_:I

    move v1, p2

    .line 2142536
    invoke-direct {v2, v6, v7, v8, v1}, LX/Ebb;-><init>(Ljavax/crypto/spec/SecretKeySpec;Ljavax/crypto/spec/SecretKeySpec;Ljavax/crypto/spec/IvParameterSpec;I)V

    .line 2142537
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    move-object v1, v2

    .line 2142538
    :goto_5
    invoke-virtual {v0}, LX/EcW;->r()LX/EcN;

    move-result-object v0

    .line 2142539
    iget-object v2, v0, LX/EcN;->g:LX/EZ2;

    if-nez v2, :cond_8

    .line 2142540
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, LX/EcN;->f:Ljava/util/List;

    .line 2142541
    iget v2, v0, LX/EcN;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, v0, LX/EcN;->a:I

    .line 2142542
    invoke-virtual {v0}, LX/EWj;->t()V

    .line 2142543
    :goto_6
    move-object v0, v0

    .line 2142544
    iget-object v2, v0, LX/EcN;->g:LX/EZ2;

    if-nez v2, :cond_9

    .line 2142545
    invoke-static {v0}, LX/EcN;->C(LX/EcN;)V

    .line 2142546
    iget-object v2, v0, LX/EcN;->f:Ljava/util/List;

    invoke-static {v4, v2}, LX/EWS;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 2142547
    invoke-virtual {v0}, LX/EWj;->t()V

    .line 2142548
    :goto_7
    move-object v0, v0

    .line 2142549
    invoke-virtual {v0}, LX/EcN;->l()LX/EcW;

    move-result-object v2

    .line 2142550
    iget-object v0, p0, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v4

    .line 2142551
    iget-object v0, v3, LX/Ecr;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 2142552
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0, v2}, LX/EcK;->a(ILX/EcW;)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebj;->a:LX/Ecf;

    move-object v2, v1

    .line 2142553
    goto/16 :goto_0

    :cond_7
    move-object v1, v2

    goto :goto_5

    .line 2142554
    :cond_8
    iget-object v2, v0, LX/EcN;->g:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->e()V

    goto :goto_6

    .line 2142555
    :cond_9
    iget-object v2, v0, LX/EcN;->g:LX/EZ2;

    invoke-virtual {v2, v4}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_7

    :cond_a
    iget-object v1, v3, LX/EcN;->g:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->c()I

    move-result v1

    goto/16 :goto_3

    .line 2142556
    :cond_b
    iget-object v4, v3, LX/EcN;->g:LX/EZ2;

    invoke-virtual {v4, v1}, LX/EZ2;->d(I)V

    goto/16 :goto_4
.end method

.method private static a(ILjavax/crypto/spec/SecretKeySpec;I)Ljavax/crypto/Cipher;
    .locals 5

    .prologue
    .line 2142448
    :try_start_0
    const-string v0, "AES/CTR/NoPadding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 2142449
    const/16 v1, 0x10

    new-array v1, v1, [B

    .line 2142450
    const/4 v2, 0x0

    .line 2142451
    add-int/lit8 v3, v2, 0x3

    int-to-byte v4, p2

    aput-byte v4, v1, v3

    .line 2142452
    add-int/lit8 v3, v2, 0x2

    shr-int/lit8 v4, p2, 0x8

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 2142453
    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v4, p2, 0x10

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    .line 2142454
    shr-int/lit8 v3, p2, 0x18

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 2142455
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 2142456
    invoke-virtual {v0, p0, p1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2142457
    return-object v0

    .line 2142458
    :catch_0
    move-exception v0

    .line 2142459
    :goto_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2142460
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method private static a(ILjavax/crypto/spec/SecretKeySpec;Ljavax/crypto/spec/IvParameterSpec;)Ljavax/crypto/Cipher;
    .locals 2

    .prologue
    .line 2142442
    :try_start_0
    const-string v0, "AES/CBC/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 2142443
    invoke-virtual {v0, p0, p1, p2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2142444
    return-object v0

    .line 2142445
    :catch_0
    move-exception v0

    .line 2142446
    :goto_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2142447
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method private static a(ILX/Ebb;[B)[B
    .locals 3

    .prologue
    .line 2142566
    const/4 v0, 0x3

    if-lt p0, v0, :cond_0

    .line 2142567
    const/4 v0, 0x1

    .line 2142568
    :try_start_0
    iget-object v1, p1, LX/Ebb;->a:Ljavax/crypto/spec/SecretKeySpec;

    move-object v1, v1

    .line 2142569
    iget-object v2, p1, LX/Ebb;->c:Ljavax/crypto/spec/IvParameterSpec;

    move-object v2, v2

    .line 2142570
    invoke-static {v0, v1, v2}, LX/Eao;->a(ILjavax/crypto/spec/SecretKeySpec;Ljavax/crypto/spec/IvParameterSpec;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 2142571
    :goto_0
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    return-object v0

    .line 2142572
    :cond_0
    const/4 v0, 0x1

    .line 2142573
    iget-object v1, p1, LX/Ebb;->a:Ljavax/crypto/spec/SecretKeySpec;

    move-object v1, v1

    .line 2142574
    iget v2, p1, LX/Ebb;->d:I

    move v2, v2

    .line 2142575
    invoke-static {v0, v1, v2}, LX/Eao;->a(ILjavax/crypto/spec/SecretKeySpec;I)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2142576
    :catch_0
    move-exception v0

    .line 2142577
    :goto_1
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2142578
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(LX/Eao;LX/EbD;LX/Eac;)[B
    .locals 5

    .prologue
    .line 2142434
    sget-object v1, LX/Eao;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2142435
    :try_start_0
    iget-object v0, p0, LX/Eao;->b:LX/DoQ;

    iget-object v2, p0, LX/Eao;->e:LX/Eap;

    invoke-interface {v0, v2}, LX/DoQ;->b(LX/Eap;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2142436
    new-instance v0, LX/Eal;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No session for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Eao;->e:LX/Eap;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LX/Eal;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142437
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2142438
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Eao;->b:LX/DoQ;

    iget-object v2, p0, LX/Eao;->e:LX/Eap;

    invoke-interface {v0, v2}, LX/DoQ;->a(LX/Eap;)LX/Ebh;

    move-result-object v0

    .line 2142439
    invoke-static {p0, v0, p1}, LX/Eao;->a(LX/Eao;LX/Ebh;LX/EbD;)[B

    move-result-object v2

    .line 2142440
    iget-object v3, p0, LX/Eao;->b:LX/DoQ;

    iget-object v4, p0, LX/Eao;->e:LX/Eap;

    invoke-interface {v3, v4, v0}, LX/DoQ;->a(LX/Eap;LX/Ebh;)V

    .line 2142441
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method private static a(LX/Eao;LX/Ebh;LX/EbD;)[B
    .locals 5

    .prologue
    .line 2142411
    sget-object v1, LX/Eao;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2142412
    :try_start_0
    iget-object v0, p1, LX/Ebh;->b:Ljava/util/LinkedList;

    move-object v0, v0

    .line 2142413
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2142414
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2142415
    :try_start_1
    new-instance v4, LX/Ebj;

    .line 2142416
    iget-object v0, p1, LX/Ebh;->a:LX/Ebj;

    move-object v0, v0

    .line 2142417
    invoke-direct {v4, v0}, LX/Ebj;-><init>(LX/Ebj;)V

    .line 2142418
    invoke-direct {p0, v4, p2}, LX/Eao;->a(LX/Ebj;LX/EbD;)[B

    move-result-object v0

    .line 2142419
    iput-object v4, p1, LX/Ebh;->a:LX/Ebj;
    :try_end_1
    .catch LX/Eai; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2142420
    :try_start_2
    monitor-exit v1

    .line 2142421
    :goto_0
    return-object v0

    .line 2142422
    :catch_0
    move-exception v0

    .line 2142423
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2142424
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2142425
    :try_start_3
    new-instance v4, LX/Ebj;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebj;

    invoke-direct {v4, v0}, LX/Ebj;-><init>(LX/Ebj;)V

    .line 2142426
    invoke-direct {p0, v4, p2}, LX/Eao;->a(LX/Ebj;LX/EbD;)[B

    move-result-object v0

    .line 2142427
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 2142428
    invoke-virtual {p1, v4}, LX/Ebh;->a(LX/Ebj;)V
    :try_end_3
    .catch LX/Eai; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2142429
    :try_start_4
    monitor-exit v1

    goto :goto_0

    .line 2142430
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 2142431
    :catch_1
    move-exception v0

    .line 2142432
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2142433
    :cond_0
    new-instance v0, LX/Eai;

    const-string v2, "No valid sessions."

    invoke-direct {v0, v2, v3}, LX/Eai;-><init>(Ljava/lang/String;Ljava/util/List;)V

    throw v0
.end method

.method private a(LX/EbA;LX/Eac;)[B
    .locals 6

    .prologue
    .line 2142401
    sget-object v1, LX/Eao;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2142402
    :try_start_0
    iget-object v0, p0, LX/Eao;->b:LX/DoQ;

    iget-object v2, p0, LX/Eao;->e:LX/Eap;

    invoke-interface {v0, v2}, LX/DoQ;->a(LX/Eap;)LX/Ebh;

    move-result-object v0

    .line 2142403
    iget-object v2, p0, LX/Eao;->c:LX/Eam;

    invoke-virtual {v2, v0, p1}, LX/Eam;->a(LX/Ebh;LX/EbA;)LX/Ecs;

    move-result-object v2

    .line 2142404
    iget-object v3, p1, LX/EbA;->g:LX/EbD;

    move-object v3, v3

    .line 2142405
    invoke-static {p0, v0, v3}, LX/Eao;->a(LX/Eao;LX/Ebh;LX/EbD;)[B

    move-result-object v3

    .line 2142406
    iget-object v4, p0, LX/Eao;->b:LX/DoQ;

    iget-object v5, p0, LX/Eao;->e:LX/Eap;

    invoke-interface {v4, v5, v0}, LX/DoQ;->a(LX/Eap;LX/Ebh;)V

    .line 2142407
    invoke-virtual {v2}, LX/Ecs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2142408
    iget-object v4, p0, LX/Eao;->d:LX/DoP;

    invoke-virtual {v2}, LX/Ecs;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v4, v0}, LX/DoP;->b(I)V

    .line 2142409
    :cond_0
    monitor-exit v1

    return-object v3

    .line 2142410
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(LX/Ebj;LX/EbD;)[B
    .locals 5

    .prologue
    .line 2142373
    iget-object v0, p1, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->y()Z

    move-result v0

    move v0, v0

    .line 2142374
    if-nez v0, :cond_0

    .line 2142375
    new-instance v0, LX/Eai;

    const-string v1, "Uninitialized session!"

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142376
    :cond_0
    iget v0, p2, LX/EbD;->a:I

    move v0, v0

    .line 2142377
    invoke-virtual {p1}, LX/Ebj;->c()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 2142378
    new-instance v0, LX/Eai;

    const-string v1, "Message version %d, but session version %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2142379
    iget v4, p2, LX/EbD;->a:I

    move v4, v4

    .line 2142380
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, LX/Ebj;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142381
    :cond_1
    iget v0, p2, LX/EbD;->a:I

    move v0, v0

    .line 2142382
    iget-object v1, p2, LX/EbD;->b:LX/Eat;

    move-object v1, v1

    .line 2142383
    iget v2, p2, LX/EbD;->c:I

    move v2, v2

    .line 2142384
    invoke-static {p1, v1}, LX/Eao;->a(LX/Ebj;LX/Eat;)LX/Eba;

    move-result-object v3

    .line 2142385
    invoke-static {p1, v1, v3, v2}, LX/Eao;->a(LX/Ebj;LX/Eat;LX/Eba;I)LX/Ebb;

    move-result-object v1

    .line 2142386
    invoke-virtual {p1}, LX/Ebj;->d()LX/Eae;

    move-result-object v2

    invoke-virtual {p1}, LX/Ebj;->e()LX/Eae;

    move-result-object v3

    .line 2142387
    iget-object v4, v1, LX/Ebb;->b:Ljavax/crypto/spec/SecretKeySpec;

    move-object v4, v4

    .line 2142388
    invoke-virtual {p2, v0, v2, v3, v4}, LX/EbD;->a(ILX/Eae;LX/Eae;Ljavax/crypto/spec/SecretKeySpec;)V

    .line 2142389
    iget-object v2, p2, LX/EbD;->e:[B

    move-object v2, v2

    .line 2142390
    invoke-static {v0, v1, v2}, LX/Eao;->b(ILX/Ebb;[B)[B

    move-result-object v0

    .line 2142391
    iget-object v1, p1, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v1}, LX/Ecf;->O()LX/EcK;

    move-result-object v1

    .line 2142392
    iget-object v2, v1, LX/EcK;->n:LX/EZ7;

    if-nez v2, :cond_2

    .line 2142393
    sget-object v2, LX/Ece;->c:LX/Ece;

    move-object v2, v2

    .line 2142394
    iput-object v2, v1, LX/EcK;->m:LX/Ece;

    .line 2142395
    invoke-virtual {v1}, LX/EWj;->t()V

    .line 2142396
    :goto_0
    iget v2, v1, LX/EcK;->a:I

    and-int/lit16 v2, v2, -0x101

    iput v2, v1, LX/EcK;->a:I

    .line 2142397
    move-object v1, v1

    .line 2142398
    invoke-virtual {v1}, LX/EcK;->l()LX/Ecf;

    move-result-object v1

    iput-object v1, p1, LX/Ebj;->a:LX/Ecf;

    .line 2142399
    return-object v0

    .line 2142400
    :cond_2
    iget-object v2, v1, LX/EcK;->n:LX/EZ7;

    invoke-virtual {v2}, LX/EZ7;->g()LX/EZ7;

    goto :goto_0
.end method

.method private static b(ILX/Ebb;[B)[B
    .locals 3

    .prologue
    .line 2142320
    const/4 v0, 0x3

    if-lt p0, v0, :cond_0

    .line 2142321
    const/4 v0, 0x2

    .line 2142322
    :try_start_0
    iget-object v1, p1, LX/Ebb;->a:Ljavax/crypto/spec/SecretKeySpec;

    move-object v1, v1

    .line 2142323
    iget-object v2, p1, LX/Ebb;->c:Ljavax/crypto/spec/IvParameterSpec;

    move-object v2, v2

    .line 2142324
    invoke-static {v0, v1, v2}, LX/Eao;->a(ILjavax/crypto/spec/SecretKeySpec;Ljavax/crypto/spec/IvParameterSpec;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 2142325
    :goto_0
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    return-object v0

    .line 2142326
    :cond_0
    const/4 v0, 0x2

    .line 2142327
    iget-object v1, p1, LX/Ebb;->a:Ljavax/crypto/spec/SecretKeySpec;

    move-object v1, v1

    .line 2142328
    iget v2, p1, LX/Ebb;->d:I

    move v2, v2

    .line 2142329
    invoke-static {v0, v1, v2}, LX/Eao;->a(ILjavax/crypto/spec/SecretKeySpec;I)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2142330
    :catch_0
    move-exception v0

    .line 2142331
    :goto_1
    new-instance v1, LX/Eai;

    invoke-direct {v1, v0}, LX/Eai;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2142332
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a([B)LX/Eb9;
    .locals 14

    .prologue
    .line 2142285
    sget-object v10, LX/Eao;->a:Ljava/lang/Object;

    monitor-enter v10

    .line 2142286
    :try_start_0
    iget-object v1, p0, LX/Eao;->b:LX/DoQ;

    iget-object v2, p0, LX/Eao;->e:LX/Eap;

    invoke-interface {v1, v2}, LX/DoQ;->a(LX/Eap;)LX/Ebh;

    move-result-object v11

    .line 2142287
    iget-object v0, v11, LX/Ebh;->a:LX/Ebj;

    move-object v12, v0

    .line 2142288
    invoke-virtual {v12}, LX/Ebj;->k()LX/Eba;

    move-result-object v13

    .line 2142289
    invoke-virtual {v13}, LX/Eba;->d()LX/Ebb;

    move-result-object v3

    .line 2142290
    invoke-virtual {v12}, LX/Ebj;->h()LX/Eat;

    move-result-object v4

    .line 2142291
    invoke-virtual {v12}, LX/Ebj;->f()I

    move-result v6

    .line 2142292
    invoke-virtual {v12}, LX/Ebj;->c()I

    move-result v2

    .line 2142293
    invoke-static {v2, v3, p1}, LX/Eao;->a(ILX/Ebb;[B)[B

    move-result-object v7

    .line 2142294
    new-instance v1, LX/EbD;

    .line 2142295
    iget-object v0, v3, LX/Ebb;->b:Ljavax/crypto/spec/SecretKeySpec;

    move-object v3, v0

    .line 2142296
    iget v0, v13, LX/Eba;->e:I

    move v5, v0

    .line 2142297
    invoke-virtual {v12}, LX/Ebj;->e()LX/Eae;

    move-result-object v8

    invoke-virtual {v12}, LX/Ebj;->d()LX/Eae;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, LX/EbD;-><init>(ILjavax/crypto/spec/SecretKeySpec;LX/Eat;II[BLX/Eae;LX/Eae;)V

    .line 2142298
    iget-object v0, v12, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->D()Z

    move-result v0

    move v3, v0

    .line 2142299
    if-eqz v3, :cond_0

    .line 2142300
    invoke-virtual {v12}, LX/Ebj;->m()LX/Ebi;

    move-result-object v6

    .line 2142301
    invoke-virtual {v12}, LX/Ebj;->o()I

    move-result v3

    .line 2142302
    new-instance v9, LX/EbA;

    .line 2142303
    iget-object v0, v6, LX/Ebi;->a:LX/Ecs;

    move-object v4, v0

    .line 2142304
    iget v0, v6, LX/Ebi;->b:I

    move v5, v0

    .line 2142305
    iget-object v0, v6, LX/Ebi;->c:LX/Eat;

    move-object v6, v0

    .line 2142306
    invoke-virtual {v12}, LX/Ebj;->e()LX/Eae;

    move-result-object v7

    move-object v0, v1

    check-cast v0, LX/EbD;

    move-object v8, v0

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, LX/EbA;-><init>(IILX/Ecs;ILX/Eat;LX/Eae;LX/EbD;)V

    move-object v1, v9

    .line 2142307
    :cond_0
    invoke-virtual {v13}, LX/Eba;->c()LX/Eba;

    move-result-object v2

    .line 2142308
    invoke-static {}, LX/EcQ;->w()LX/EcQ;

    move-result-object v0

    .line 2142309
    iget-object v3, v2, LX/Eba;->d:[B

    move-object v3, v3

    .line 2142310
    invoke-static {v3}, LX/EWc;->a([B)LX/EWc;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/EcQ;->a(LX/EWc;)LX/EcQ;

    move-result-object v0

    .line 2142311
    iget v3, v2, LX/Eba;->e:I

    move v3, v3

    .line 2142312
    invoke-virtual {v0, v3}, LX/EcQ;->a(I)LX/EcQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EcQ;->l()LX/EcR;

    move-result-object v0

    .line 2142313
    iget-object v3, v12, LX/Ebj;->a:LX/Ecf;

    .line 2142314
    iget-object v2, v3, LX/Ecf;->senderChain_:LX/EcW;

    move-object v3, v2

    .line 2142315
    invoke-virtual {v3}, LX/EcW;->r()LX/EcN;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/EcN;->a(LX/EcR;)LX/EcN;

    move-result-object v0

    invoke-virtual {v0}, LX/EcN;->l()LX/EcW;

    move-result-object v0

    .line 2142316
    iget-object v3, v12, LX/Ebj;->a:LX/Ecf;

    invoke-virtual {v3}, LX/Ecf;->O()LX/EcK;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/EcK;->a(LX/EcW;)LX/EcK;

    move-result-object v0

    invoke-virtual {v0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    iput-object v0, v12, LX/Ebj;->a:LX/Ecf;

    .line 2142317
    iget-object v2, p0, LX/Eao;->b:LX/DoQ;

    iget-object v3, p0, LX/Eao;->e:LX/Eap;

    invoke-interface {v2, v3, v11}, LX/DoQ;->a(LX/Eap;LX/Ebh;)V

    .line 2142318
    monitor-exit v10

    return-object v1

    .line 2142319
    :catchall_0
    move-exception v1

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public final a(LX/EbA;)[B
    .locals 2

    .prologue
    .line 2142284
    new-instance v0, LX/Ean;

    invoke-direct {v0}, LX/Ean;-><init>()V

    invoke-direct {p0, p1, v0}, LX/Eao;->a(LX/EbA;LX/Eac;)[B

    move-result-object v0

    return-object v0
.end method
