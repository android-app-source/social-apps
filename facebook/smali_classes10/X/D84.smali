.class public final LX/D84;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D7g;


# instance fields
.field public final synthetic a:LX/D85;


# direct methods
.method public constructor <init>(LX/D85;)V
    .locals 0

    .prologue
    .line 1967880
    iput-object p1, p0, LX/D84;->a:LX/D85;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 2

    .prologue
    .line 1967858
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967859
    iget-object v1, v0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 1967860
    if-nez v0, :cond_0

    .line 1967861
    const/4 v0, 0x0

    .line 1967862
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967863
    iget-object v1, v0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 1967864
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getY()F

    move-result v0

    goto :goto_0
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 1967865
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967866
    iget-object v1, v0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 1967867
    if-nez v0, :cond_1

    .line 1967868
    :cond_0
    :goto_0
    return-void

    .line 1967869
    :cond_1
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->f:LX/D83;

    invoke-virtual {v0}, LX/D83;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1967870
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967871
    iget-object v1, v0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 1967872
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1967881
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1967882
    :goto_0
    return-void

    .line 1967883
    :cond_0
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ScrollView;->setScrollX(I)V

    goto :goto_0
.end method

.method public final b()F
    .locals 2

    .prologue
    .line 1967873
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967874
    iget-object v1, v0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 1967875
    if-nez v0, :cond_0

    .line 1967876
    const/4 v0, 0x0

    .line 1967877
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967878
    iget-object v1, v0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 1967879
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTranslationY()F

    move-result v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1967852
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1967853
    :goto_0
    return-void

    .line 1967854
    :cond_0
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ScrollView;->setScrollY(I)V

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1967855
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1967856
    const/4 v0, 0x0

    .line 1967857
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1967849
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1967850
    const/4 v0, 0x0

    .line 1967851
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollX()I

    move-result v0

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1967846
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1967847
    const/4 v0, 0x0

    .line 1967848
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1967825
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1967826
    const/4 v0, 0x0

    .line 1967827
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 1967839
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967840
    iget-object v1, v0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 1967841
    if-nez v0, :cond_0

    .line 1967842
    const/4 v0, 0x0

    .line 1967843
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967844
    iget-object v1, v0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 1967845
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1967838
    const/4 v0, 0x0

    return v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 1967835
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967836
    iget-object v1, v0, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 1967837
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v0}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d()Landroid/widget/ScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1967828
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    if-nez v0, :cond_1

    .line 1967829
    :cond_0
    :goto_0
    return-void

    .line 1967830
    :cond_1
    invoke-virtual {p0, v2}, LX/D84;->a(I)V

    .line 1967831
    invoke-virtual {p0, v2}, LX/D84;->b(I)V

    .line 1967832
    invoke-virtual {p0, v1}, LX/D84;->a(F)V

    .line 1967833
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->h:LX/D8S;

    if-eqz v0, :cond_0

    .line 1967834
    iget-object v0, p0, LX/D84;->a:LX/D85;

    iget-object v0, v0, LX/D85;->h:LX/D8S;

    invoke-virtual {v0, v1}, LX/D8S;->a(F)Z

    goto :goto_0
.end method
