.class public final LX/DCy;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3mb;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public final synthetic e:LX/3mb;


# direct methods
.method public constructor <init>(LX/3mb;)V
    .locals 1

    .prologue
    .line 1974773
    iput-object p1, p0, LX/DCy;->e:LX/3mb;

    .line 1974774
    move-object v0, p1

    .line 1974775
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1974776
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1974777
    const-string v0, "GroupsYouShouldJoinVisitCommunityComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1974778
    if-ne p0, p1, :cond_1

    .line 1974779
    :cond_0
    :goto_0
    return v0

    .line 1974780
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1974781
    goto :goto_0

    .line 1974782
    :cond_3
    check-cast p1, LX/DCy;

    .line 1974783
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1974784
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1974785
    if-eq v2, v3, :cond_0

    .line 1974786
    iget v2, p0, LX/DCy;->a:I

    iget v3, p1, LX/DCy;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1974787
    goto :goto_0

    .line 1974788
    :cond_4
    iget-object v2, p0, LX/DCy;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/DCy;->b:Ljava/lang/String;

    iget-object v3, p1, LX/DCy;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1974789
    goto :goto_0

    .line 1974790
    :cond_6
    iget-object v2, p1, LX/DCy;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1974791
    :cond_7
    iget-object v2, p0, LX/DCy;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/DCy;->c:Ljava/lang/String;

    iget-object v3, p1, LX/DCy;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1974792
    goto :goto_0

    .line 1974793
    :cond_9
    iget-object v2, p1, LX/DCy;->c:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 1974794
    :cond_a
    iget-object v2, p0, LX/DCy;->d:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/DCy;->d:Ljava/lang/String;

    iget-object v3, p1, LX/DCy;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1974795
    goto :goto_0

    .line 1974796
    :cond_b
    iget-object v2, p1, LX/DCy;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
