.class public final LX/DeV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2022584
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2022585
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2022586
    :goto_0
    return v1

    .line 2022587
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2022588
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2022589
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2022590
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2022591
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2022592
    const-string v5, "application"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2022593
    invoke-static {p0, p1}, LX/DeT;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2022594
    :cond_2
    const-string v5, "tags"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2022595
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2022596
    :cond_3
    const-string v5, "update_lines"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2022597
    invoke-static {p0, p1}, LX/Deb;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2022598
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2022599
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2022600
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2022601
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2022602
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2022603
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2022604
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2022605
    if-eqz v0, :cond_0

    .line 2022606
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022607
    invoke-static {p0, v0, p2, p3}, LX/DeT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2022608
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2022609
    if-eqz v0, :cond_1

    .line 2022610
    const-string v0, "tags"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022611
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2022612
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2022613
    if-eqz v0, :cond_2

    .line 2022614
    const-string v1, "update_lines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022615
    invoke-static {p0, v0, p2, p3}, LX/Deb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2022616
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2022617
    return-void
.end method
