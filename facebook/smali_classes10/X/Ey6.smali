.class public final enum LX/Ey6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ey6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ey6;

.field public static final enum LOADING_STATE_ERROR:LX/Ey6;

.field public static final enum LOADING_STATE_FINISHED:LX/Ey6;

.field public static final enum LOADING_STATE_IDLE:LX/Ey6;

.field public static final enum LOADING_STATE_LOADING:LX/Ey6;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2185773
    new-instance v0, LX/Ey6;

    const-string v1, "LOADING_STATE_LOADING"

    invoke-direct {v0, v1, v2}, LX/Ey6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ey6;->LOADING_STATE_LOADING:LX/Ey6;

    .line 2185774
    new-instance v0, LX/Ey6;

    const-string v1, "LOADING_STATE_IDLE"

    invoke-direct {v0, v1, v3}, LX/Ey6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ey6;->LOADING_STATE_IDLE:LX/Ey6;

    .line 2185775
    new-instance v0, LX/Ey6;

    const-string v1, "LOADING_STATE_FINISHED"

    invoke-direct {v0, v1, v4}, LX/Ey6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ey6;->LOADING_STATE_FINISHED:LX/Ey6;

    .line 2185776
    new-instance v0, LX/Ey6;

    const-string v1, "LOADING_STATE_ERROR"

    invoke-direct {v0, v1, v5}, LX/Ey6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ey6;->LOADING_STATE_ERROR:LX/Ey6;

    .line 2185777
    const/4 v0, 0x4

    new-array v0, v0, [LX/Ey6;

    sget-object v1, LX/Ey6;->LOADING_STATE_LOADING:LX/Ey6;

    aput-object v1, v0, v2

    sget-object v1, LX/Ey6;->LOADING_STATE_IDLE:LX/Ey6;

    aput-object v1, v0, v3

    sget-object v1, LX/Ey6;->LOADING_STATE_FINISHED:LX/Ey6;

    aput-object v1, v0, v4

    sget-object v1, LX/Ey6;->LOADING_STATE_ERROR:LX/Ey6;

    aput-object v1, v0, v5

    sput-object v0, LX/Ey6;->$VALUES:[LX/Ey6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2185778
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ey6;
    .locals 1

    .prologue
    .line 2185779
    const-class v0, LX/Ey6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ey6;

    return-object v0
.end method

.method public static values()[LX/Ey6;
    .locals 1

    .prologue
    .line 2185780
    sget-object v0, LX/Ey6;->$VALUES:[LX/Ey6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ey6;

    return-object v0
.end method
