.class public final LX/EY6;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EY5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EY6;",
        ">;",
        "LX/EY5;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYA;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EYA;",
            "LX/EY9;",
            "LX/EY8;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;

.field public e:J

.field public f:J

.field public g:D

.field private h:LX/EWc;

.field private i:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2136404
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2136405
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY6;->b:Ljava/util/List;

    .line 2136406
    const-string v0, ""

    iput-object v0, p0, LX/EY6;->d:Ljava/lang/Object;

    .line 2136407
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EY6;->h:LX/EWc;

    .line 2136408
    const-string v0, ""

    iput-object v0, p0, LX/EY6;->i:Ljava/lang/Object;

    .line 2136409
    invoke-direct {p0}, LX/EY6;->m()V

    .line 2136410
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2136397
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2136398
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY6;->b:Ljava/util/List;

    .line 2136399
    const-string v0, ""

    iput-object v0, p0, LX/EY6;->d:Ljava/lang/Object;

    .line 2136400
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EY6;->h:LX/EWc;

    .line 2136401
    const-string v0, ""

    iput-object v0, p0, LX/EY6;->i:Ljava/lang/Object;

    .line 2136402
    invoke-direct {p0}, LX/EY6;->m()V

    .line 2136403
    return-void
.end method

.method private B()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EYA;",
            "LX/EY9;",
            "LX/EY8;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2136390
    iget-object v1, p0, LX/EY6;->c:LX/EZ2;

    if-nez v1, :cond_0

    .line 2136391
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EY6;->b:Ljava/util/List;

    iget v3, p0, LX/EY6;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2136392
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2136393
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EY6;->c:LX/EZ2;

    .line 2136394
    const/4 v0, 0x0

    iput-object v0, p0, LX/EY6;->b:Ljava/util/List;

    .line 2136395
    :cond_0
    iget-object v0, p0, LX/EY6;->c:LX/EZ2;

    return-object v0

    .line 2136396
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/EWc;)LX/EY6;
    .locals 1

    .prologue
    .line 2136384
    if-nez p1, :cond_0

    .line 2136385
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2136386
    :cond_0
    iget v0, p0, LX/EY6;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, LX/EY6;->a:I

    .line 2136387
    iput-object p1, p0, LX/EY6;->h:LX/EWc;

    .line 2136388
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136389
    return-object p0
.end method

.method private d(LX/EWY;)LX/EY6;
    .locals 1

    .prologue
    .line 2136380
    instance-of v0, p1, LX/EYB;

    if-eqz v0, :cond_0

    .line 2136381
    check-cast p1, LX/EYB;

    invoke-virtual {p0, p1}, LX/EY6;->a(LX/EYB;)LX/EY6;

    move-result-object p0

    .line 2136382
    :goto_0
    return-object p0

    .line 2136383
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EY6;
    .locals 4

    .prologue
    .line 2136368
    const/4 v2, 0x0

    .line 2136369
    :try_start_0
    sget-object v0, LX/EYB;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYB;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2136370
    if-eqz v0, :cond_0

    .line 2136371
    invoke-virtual {p0, v0}, LX/EY6;->a(LX/EYB;)LX/EY6;

    .line 2136372
    :cond_0
    return-object p0

    .line 2136373
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2136374
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2136375
    check-cast v0, LX/EYB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2136376
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2136377
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2136378
    invoke-virtual {p0, v1}, LX/EY6;->a(LX/EYB;)LX/EY6;

    :cond_1
    throw v0

    .line 2136379
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 2136365
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2136366
    invoke-direct {p0}, LX/EY6;->B()LX/EZ2;

    .line 2136367
    :cond_0
    return-void
.end method

.method public static n()LX/EY6;
    .locals 1

    .prologue
    .line 2136364
    new-instance v0, LX/EY6;

    invoke-direct {v0}, LX/EY6;-><init>()V

    return-object v0
.end method

.method private u()LX/EY6;
    .locals 2

    .prologue
    .line 2136363
    invoke-static {}, LX/EY6;->n()LX/EY6;

    move-result-object v0

    invoke-direct {p0}, LX/EY6;->y()LX/EYB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EY6;->a(LX/EYB;)LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method private x()LX/EYB;
    .locals 2

    .prologue
    .line 2136359
    invoke-direct {p0}, LX/EY6;->y()LX/EYB;

    move-result-object v0

    .line 2136360
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2136361
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2136362
    :cond_0
    return-object v0
.end method

.method private y()LX/EYB;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2136322
    new-instance v2, LX/EYB;

    invoke-direct {v2, p0}, LX/EYB;-><init>(LX/EWj;)V

    .line 2136323
    iget v3, p0, LX/EY6;->a:I

    .line 2136324
    iget-object v4, p0, LX/EY6;->c:LX/EZ2;

    if-nez v4, :cond_6

    .line 2136325
    iget v4, p0, LX/EY6;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 2136326
    iget-object v4, p0, LX/EY6;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, LX/EY6;->b:Ljava/util/List;

    .line 2136327
    iget v4, p0, LX/EY6;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, LX/EY6;->a:I

    .line 2136328
    :cond_0
    iget-object v4, p0, LX/EY6;->b:Ljava/util/List;

    .line 2136329
    iput-object v4, v2, LX/EYB;->name_:Ljava/util/List;

    .line 2136330
    :goto_0
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_7

    .line 2136331
    :goto_1
    iget-object v1, p0, LX/EY6;->d:Ljava/lang/Object;

    .line 2136332
    iput-object v1, v2, LX/EYB;->identifierValue_:Ljava/lang/Object;

    .line 2136333
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2136334
    or-int/lit8 v0, v0, 0x2

    .line 2136335
    :cond_1
    iget-wide v4, p0, LX/EY6;->e:J

    .line 2136336
    iput-wide v4, v2, LX/EYB;->positiveIntValue_:J

    .line 2136337
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2136338
    or-int/lit8 v0, v0, 0x4

    .line 2136339
    :cond_2
    iget-wide v4, p0, LX/EY6;->f:J

    .line 2136340
    iput-wide v4, v2, LX/EYB;->negativeIntValue_:J

    .line 2136341
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2136342
    or-int/lit8 v0, v0, 0x8

    .line 2136343
    :cond_3
    iget-wide v4, p0, LX/EY6;->g:D

    .line 2136344
    iput-wide v4, v2, LX/EYB;->doubleValue_:D

    .line 2136345
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 2136346
    or-int/lit8 v0, v0, 0x10

    .line 2136347
    :cond_4
    iget-object v1, p0, LX/EY6;->h:LX/EWc;

    .line 2136348
    iput-object v1, v2, LX/EYB;->stringValue_:LX/EWc;

    .line 2136349
    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    .line 2136350
    or-int/lit8 v0, v0, 0x20

    .line 2136351
    :cond_5
    iget-object v1, p0, LX/EY6;->i:Ljava/lang/Object;

    .line 2136352
    iput-object v1, v2, LX/EYB;->aggregateValue_:Ljava/lang/Object;

    .line 2136353
    iput v0, v2, LX/EYB;->bitField0_:I

    .line 2136354
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2136355
    return-object v2

    .line 2136356
    :cond_6
    iget-object v4, p0, LX/EY6;->c:LX/EZ2;

    invoke-virtual {v4}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v4

    .line 2136357
    iput-object v4, v2, LX/EYB;->name_:Ljava/util/List;

    .line 2136358
    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2136321
    invoke-direct {p0, p1}, LX/EY6;->d(LX/EWY;)LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2136320
    invoke-direct {p0, p1, p2}, LX/EY6;->d(LX/EWd;LX/EYZ;)LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EYB;)LX/EY6;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2136411
    sget-object v1, LX/EYB;->c:LX/EYB;

    move-object v1, v1

    .line 2136412
    if-ne p1, v1, :cond_0

    .line 2136413
    :goto_0
    return-object p0

    .line 2136414
    :cond_0
    iget-object v1, p0, LX/EY6;->c:LX/EZ2;

    if-nez v1, :cond_a

    .line 2136415
    iget-object v0, p1, LX/EYB;->name_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2136416
    iget-object v0, p0, LX/EY6;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2136417
    iget-object v0, p1, LX/EYB;->name_:Ljava/util/List;

    iput-object v0, p0, LX/EY6;->b:Ljava/util/List;

    .line 2136418
    iget v0, p0, LX/EY6;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, LX/EY6;->a:I

    .line 2136419
    :goto_1
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136420
    :cond_1
    :goto_2
    const/4 v0, 0x1

    .line 2136421
    iget v1, p1, LX/EYB;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_d

    :goto_3
    move v0, v0

    .line 2136422
    if-eqz v0, :cond_2

    .line 2136423
    iget v0, p0, LX/EY6;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EY6;->a:I

    .line 2136424
    iget-object v0, p1, LX/EYB;->identifierValue_:Ljava/lang/Object;

    iput-object v0, p0, LX/EY6;->d:Ljava/lang/Object;

    .line 2136425
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136426
    :cond_2
    iget v0, p1, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2136427
    if-eqz v0, :cond_3

    .line 2136428
    iget-wide v2, p1, LX/EYB;->positiveIntValue_:J

    move-wide v0, v2

    .line 2136429
    iget v2, p0, LX/EY6;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, LX/EY6;->a:I

    .line 2136430
    iput-wide v0, p0, LX/EY6;->e:J

    .line 2136431
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136432
    :cond_3
    iget v0, p1, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_f

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 2136433
    if-eqz v0, :cond_4

    .line 2136434
    iget-wide v2, p1, LX/EYB;->negativeIntValue_:J

    move-wide v0, v2

    .line 2136435
    iget v2, p0, LX/EY6;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, LX/EY6;->a:I

    .line 2136436
    iput-wide v0, p0, LX/EY6;->f:J

    .line 2136437
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136438
    :cond_4
    iget v0, p1, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_6
    move v0, v0

    .line 2136439
    if-eqz v0, :cond_5

    .line 2136440
    iget-wide v2, p1, LX/EYB;->doubleValue_:D

    move-wide v0, v2

    .line 2136441
    iget v2, p0, LX/EY6;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, LX/EY6;->a:I

    .line 2136442
    iput-wide v0, p0, LX/EY6;->g:D

    .line 2136443
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136444
    :cond_5
    iget v0, p1, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_11

    const/4 v0, 0x1

    :goto_7
    move v0, v0

    .line 2136445
    if-eqz v0, :cond_6

    .line 2136446
    iget-object v0, p1, LX/EYB;->stringValue_:LX/EWc;

    move-object v0, v0

    .line 2136447
    invoke-direct {p0, v0}, LX/EY6;->a(LX/EWc;)LX/EY6;

    .line 2136448
    :cond_6
    iget v0, p1, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_12

    const/4 v0, 0x1

    :goto_8
    move v0, v0

    .line 2136449
    if-eqz v0, :cond_7

    .line 2136450
    iget v0, p0, LX/EY6;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, LX/EY6;->a:I

    .line 2136451
    iget-object v0, p1, LX/EYB;->aggregateValue_:Ljava/lang/Object;

    iput-object v0, p0, LX/EY6;->i:Ljava/lang/Object;

    .line 2136452
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136453
    :cond_7
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto/16 :goto_0

    .line 2136454
    :cond_8
    iget v0, p0, LX/EY6;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_9

    .line 2136455
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EY6;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EY6;->b:Ljava/util/List;

    .line 2136456
    iget v0, p0, LX/EY6;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EY6;->a:I

    .line 2136457
    :cond_9
    iget-object v0, p0, LX/EY6;->b:Ljava/util/List;

    iget-object v1, p1, LX/EYB;->name_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 2136458
    :cond_a
    iget-object v1, p1, LX/EYB;->name_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2136459
    iget-object v1, p0, LX/EY6;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2136460
    iget-object v1, p0, LX/EY6;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2136461
    iput-object v0, p0, LX/EY6;->c:LX/EZ2;

    .line 2136462
    iget-object v1, p1, LX/EYB;->name_:Ljava/util/List;

    iput-object v1, p0, LX/EY6;->b:Ljava/util/List;

    .line 2136463
    iget v1, p0, LX/EY6;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/EY6;->a:I

    .line 2136464
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_b

    invoke-direct {p0}, LX/EY6;->B()LX/EZ2;

    move-result-object v0

    :cond_b
    iput-object v0, p0, LX/EY6;->c:LX/EZ2;

    goto/16 :goto_2

    .line 2136465
    :cond_c
    iget-object v0, p0, LX/EY6;->c:LX/EZ2;

    iget-object v1, p1, LX/EYB;->name_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto/16 :goto_2

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_6

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_12
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2136293
    move v0, v1

    .line 2136294
    :goto_0
    iget-object v2, p0, LX/EY6;->c:LX/EZ2;

    if-nez v2, :cond_2

    .line 2136295
    iget-object v2, p0, LX/EY6;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2136296
    :goto_1
    move v2, v2

    .line 2136297
    if-ge v0, v2, :cond_1

    .line 2136298
    iget-object v2, p0, LX/EY6;->c:LX/EZ2;

    if-nez v2, :cond_3

    .line 2136299
    iget-object v2, p0, LX/EY6;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYA;

    .line 2136300
    :goto_2
    move-object v2, v2

    .line 2136301
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2136302
    :goto_3
    return v1

    .line 2136303
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2136304
    :cond_1
    const/4 v1, 0x1

    goto :goto_3

    :cond_2
    iget-object v2, p0, LX/EY6;->c:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_1

    :cond_3
    iget-object v2, p0, LX/EY6;->c:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EYA;

    goto :goto_2
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2136305
    invoke-direct {p0, p1, p2}, LX/EY6;->d(LX/EWd;LX/EYZ;)LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2136306
    invoke-direct {p0}, LX/EY6;->u()LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2136307
    invoke-direct {p0, p1, p2}, LX/EY6;->d(LX/EWd;LX/EYZ;)LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2136308
    invoke-direct {p0}, LX/EY6;->u()LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2136309
    invoke-direct {p0, p1}, LX/EY6;->d(LX/EWY;)LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2136310
    invoke-direct {p0}, LX/EY6;->u()LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2136311
    sget-object v0, LX/EYC;->H:LX/EYn;

    const-class v1, LX/EYB;

    const-class v2, LX/EY6;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2136312
    sget-object v0, LX/EYC;->G:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2136313
    invoke-direct {p0}, LX/EY6;->u()LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2136314
    invoke-direct {p0}, LX/EY6;->y()LX/EYB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2136315
    invoke-direct {p0}, LX/EY6;->x()LX/EYB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2136316
    invoke-direct {p0}, LX/EY6;->y()LX/EYB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2136317
    invoke-direct {p0}, LX/EY6;->x()LX/EYB;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2136318
    sget-object v0, LX/EYB;->c:LX/EYB;

    move-object v0, v0

    .line 2136319
    return-object v0
.end method
