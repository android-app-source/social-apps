.class public LX/DoE;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/4yu",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2041521
    new-instance v0, LX/6g7;

    invoke-direct {v0}, LX/6g7;-><init>()V

    sput-object v0, LX/DoE;->a:Ljava/util/Comparator;

    .line 2041522
    new-instance v0, LX/DoC;

    invoke-direct {v0}, LX/DoC;-><init>()V

    sput-object v0, LX/DoE;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2041578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Set;Ljava/util/Collection;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;>;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2041552
    new-instance v8, Ljava/util/PriorityQueue;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    sget-object v1, LX/DoE;->b:Ljava/util/Comparator;

    invoke-direct {v8, v0, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 2041553
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v3

    move-object v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2041554
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_7

    .line 2041555
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v9, :cond_0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-wide v6, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    const-wide/16 v10, -0x1

    cmp-long v1, v6, v10

    if-nez v1, :cond_0

    .line 2041556
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    :goto_1
    move-object v2, v1

    move-object v1, v0

    .line 2041557
    goto :goto_0

    .line 2041558
    :cond_0
    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    invoke-static {v1}, LX/0RZ;->i(Ljava/util/Iterator;)LX/4yu;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    move-object v1, v2

    goto :goto_1

    .line 2041559
    :cond_1
    invoke-virtual {v8}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-ne v0, v9, :cond_2

    if-eqz v1, :cond_2

    if-nez v2, :cond_2

    .line 2041560
    :goto_2
    return-object v1

    .line 2041561
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2041562
    const-wide/16 v0, 0x0

    move-object v5, v3

    .line 2041563
    :goto_3
    invoke-virtual {v8}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-interface {p0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2041564
    invoke-virtual {v8}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4yu;

    .line 2041565
    invoke-interface {v0}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2041566
    invoke-virtual {v9, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2041567
    add-int/lit8 v4, v4, 0x1

    .line 2041568
    iget-wide v6, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 2041569
    iget-object v5, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2041570
    if-eqz v2, :cond_6

    iget v1, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->U:I

    add-int/lit8 v1, v1, -0x1

    if-ne v4, v1, :cond_6

    .line 2041571
    invoke-static {v2, v6, v7}, LX/DoE;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    invoke-virtual {v9, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-object v1, v3

    .line 2041572
    :goto_4
    invoke-interface {v0}, LX/4yu;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2041573
    invoke-virtual {v8, v0}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    :cond_3
    move-object v2, v1

    move-wide v0, v6

    .line 2041574
    goto :goto_3

    .line 2041575
    :cond_4
    if-eqz v2, :cond_5

    .line 2041576
    invoke-static {v2, v0, v1}, LX/DoE;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2041577
    :cond_5
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_2

    :cond_6
    move-object v1, v2

    goto :goto_4

    :cond_7
    move-object v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method private static a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 2

    .prologue
    .line 2041543
    iget-object v0, p0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2041544
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v0

    .line 2041545
    iput-wide p1, v0, LX/6g6;->j:J

    .line 2041546
    move-object v0, v0

    .line 2041547
    iput-wide p1, v0, LX/6g6;->M:J

    .line 2041548
    move-object v0, v0

    .line 2041549
    iput-wide p1, v0, LX/6g6;->k:J

    .line 2041550
    move-object v0, v0

    .line 2041551
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Lcom/facebook/messaging/model/threads/ThreadsCollection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadsCollection;",
            ">;)",
            "Lcom/facebook/messaging/model/threads/ThreadsCollection;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2041526
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2041527
    new-instance v4, LX/0UE;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v4, v0}, LX/0UE;-><init>(I)V

    .line 2041528
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2041529
    iget-object v6, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v6, v6

    .line 2041530
    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 2041531
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    add-int/2addr v1, v7

    .line 2041532
    iget-boolean v7, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    move v0, v7

    .line 2041533
    if-nez v0, :cond_0

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2041534
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v0}, LX/0UE;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2041535
    :cond_1
    invoke-static {v4, v3}, LX/DoE;->a(Ljava/util/Set;Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2041536
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    if-ne v3, v1, :cond_3

    .line 2041537
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2041538
    iget-boolean p0, v1, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    move v1, p0

    .line 2041539
    if-nez v1, :cond_2

    .line 2041540
    const/4 v1, 0x0

    .line 2041541
    :goto_1
    move v2, v1

    .line 2041542
    :cond_3
    new-instance v1, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-direct {v1, v0, v2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    return-object v1

    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2041523
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 2041524
    new-instance v0, LX/DoD;

    invoke-direct {v0}, LX/DoD;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2041525
    :cond_0
    return-void
.end method
