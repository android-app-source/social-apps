.class public LX/ECU;
.super LX/ECT;
.source ""


# instance fields
.field public h:LX/3RT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/EG8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/util/Random;
    .annotation runtime Lcom/facebook/common/random/InsecureRandom;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:Z

.field private final o:J

.field private p:J

.field public q:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public constructor <init>(LX/ECY;JZJJJLjava/lang/String;)V
    .locals 11

    .prologue
    .line 2089905
    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p5

    move-wide/from16 v8, p9

    move-object/from16 v10, p11

    invoke-direct/range {v2 .. v10}, LX/ECT;-><init>(LX/ECY;JJJLjava/lang/String;)V

    .line 2089906
    iput-boolean p4, p0, LX/ECU;->n:Z

    .line 2089907
    move-wide/from16 v0, p7

    iput-wide v0, p0, LX/ECU;->o:J

    .line 2089908
    const-class v2, LX/ECU;

    invoke-virtual {p1}, LX/ECY;->a()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, p0, v3}, LX/ECU;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 2089909
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v0, p1

    check-cast v0, LX/ECU;

    invoke-static {p0}, LX/EBW;->a(LX/0QB;)LX/3RT;

    move-result-object v1

    check-cast v1, LX/3RT;

    invoke-static {p0}, LX/EBY;->a(LX/0QB;)LX/EG8;

    move-result-object v2

    check-cast v2, LX/EG8;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v5

    check-cast v5, Ljava/util/Random;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v1, v0, LX/ECU;->h:LX/3RT;

    iput-object v2, v0, LX/ECU;->i:LX/EG8;

    iput-object v3, v0, LX/ECU;->j:LX/0SG;

    iput-object v4, v0, LX/ECU;->k:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v5, v0, LX/ECU;->l:Ljava/util/Random;

    iput-object p0, v0, LX/ECU;->m:LX/0ad;

    return-void
.end method


# virtual methods
.method public final f()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2089910
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 2089911
    iget-object v2, p0, LX/ECU;->j:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2089912
    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 2089913
    const/16 v2, 0x17

    if-ge v1, v2, :cond_0

    const/4 v2, 0x7

    if-ge v1, v2, :cond_1

    .line 2089914
    :cond_0
    :goto_0
    return v0

    .line 2089915
    :cond_1
    iget-object v1, p0, LX/ECT;->a:LX/ECY;

    invoke-virtual {v1}, LX/ECY;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/ECT;->a:LX/ECY;

    invoke-virtual {v1}, LX/ECY;->d()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/ECT;->a:LX/ECY;

    .line 2089916
    iget-object v2, v1, LX/ECY;->c:LX/1MZ;

    invoke-virtual {v2}, LX/1MZ;->d()Z

    move-result v2

    move v1, v2

    .line 2089917
    if-eqz v1, :cond_0

    .line 2089918
    iget-wide v2, p0, LX/ECU;->p:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    iget-wide v2, p0, LX/ECU;->p:J

    iget-wide v4, p0, LX/ECT;->d:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, LX/ECU;->o:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2089890
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/ECU;->p:J

    .line 2089891
    return-void
.end method

.method public final i()V
    .locals 15

    .prologue
    .line 2089892
    const-string v0, "callee_inanother_call"

    .line 2089893
    iget-object v1, p0, LX/ECT;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2089894
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2089895
    iget-object v0, p0, LX/ECU;->m:LX/0ad;

    sget v1, LX/3Dx;->bh:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    if-eqz v0, :cond_1

    .line 2089896
    :cond_0
    iget-object v3, p0, LX/ECU;->l:Ljava/util/Random;

    const/16 v4, 0x4e20

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 2089897
    const/16 v4, 0x7d0

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2089898
    invoke-virtual {p0}, LX/ECT;->j()V

    .line 2089899
    iget-object v4, p0, LX/ECU;->k:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/facebook/rtc/campon/RtcCallerCamper$1;

    invoke-direct {v5, p0}, Lcom/facebook/rtc/campon/RtcCallerCamper$1;-><init>(LX/ECU;)V

    int-to-long v7, v3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v7, v8, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v3

    iput-object v3, p0, LX/ECU;->q:Ljava/util/concurrent/ScheduledFuture;

    .line 2089900
    iget-object v3, p0, LX/ECT;->a:LX/ECY;

    .line 2089901
    iget-object v9, v3, LX/ECY;->u:Ljava/util/Map;

    .line 2089902
    iget-wide v13, p0, LX/ECT;->b:J

    move-wide v11, v13

    .line 2089903
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, v10, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2089904
    :cond_1
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 2089886
    iget-object v0, p0, LX/ECU;->q:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2089887
    iget-object v0, p0, LX/ECU;->q:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2089888
    const/4 v0, 0x0

    iput-object v0, p0, LX/ECU;->q:Ljava/util/concurrent/ScheduledFuture;

    .line 2089889
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2089884
    iget-object v0, p0, LX/ECU;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/ECU;->p:J

    .line 2089885
    return-void
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 2089883
    const/4 v0, 0x1

    return v0
.end method
