.class public LX/DKO;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/DKP;",
        "LX/DKN;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DKO;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1986905
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1986906
    return-void
.end method

.method public static a(LX/0QB;)LX/DKO;
    .locals 3

    .prologue
    .line 1986907
    sget-object v0, LX/DKO;->a:LX/DKO;

    if-nez v0, :cond_1

    .line 1986908
    const-class v1, LX/DKO;

    monitor-enter v1

    .line 1986909
    :try_start_0
    sget-object v0, LX/DKO;->a:LX/DKO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1986910
    if-eqz v2, :cond_0

    .line 1986911
    :try_start_1
    new-instance v0, LX/DKO;

    invoke-direct {v0}, LX/DKO;-><init>()V

    .line 1986912
    move-object v0, v0

    .line 1986913
    sput-object v0, LX/DKO;->a:LX/DKO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1986914
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1986915
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1986916
    :cond_1
    sget-object v0, LX/DKO;->a:LX/DKO;

    return-object v0

    .line 1986917
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1986918
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
