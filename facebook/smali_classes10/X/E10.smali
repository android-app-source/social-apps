.class public LX/E10;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0lp;

.field private b:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lp;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2069107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2069108
    iput-object p1, p0, LX/E10;->a:LX/0lp;

    .line 2069109
    iput-object p2, p0, LX/E10;->b:LX/0lC;

    .line 2069110
    return-void
.end method

.method private b(Lcom/facebook/http/protocol/ApiErrorResult;)Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;
    .locals 2

    .prologue
    .line 2069102
    :try_start_0
    iget-object v0, p0, LX/E10;->a:LX/0lp;

    invoke-virtual {p1}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 2069103
    iget-object v1, p0, LX/E10;->b:LX/0lC;

    invoke-virtual {v0, v1}, LX/15w;->a(LX/0lD;)V

    .line 2069104
    const-class v1, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2069105
    :catch_0
    move-exception v0

    .line 2069106
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private c(Lcom/facebook/http/protocol/ApiErrorResult;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiErrorResult;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2069111
    :try_start_0
    iget-object v0, p0, LX/E10;->a:LX/0lp;

    invoke-virtual {p1}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 2069112
    iget-object v1, p0, LX/E10;->b:LX/0lC;

    invoke-virtual {v0, v1}, LX/15w;->a(LX/0lD;)V

    .line 2069113
    const-class v1, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceWrapper;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceWrapper;

    iget-object v0, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceWrapper;->similarPlaces:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2069114
    :catch_0
    move-exception v0

    .line 2069115
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Lcom/facebook/http/protocol/ApiErrorResult;)V
    .locals 2

    .prologue
    .line 2069092
    sget-object v0, LX/E0r;->a:[I

    invoke-virtual {p1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    invoke-static {v1}, LX/E0s;->fromErrorCode(I)LX/E0s;

    move-result-object v1

    invoke-virtual {v1}, LX/E0s;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2069093
    new-instance v0, LX/E0z;

    invoke-direct {v0}, LX/E0z;-><init>()V

    throw v0

    .line 2069094
    :pswitch_0
    new-instance v0, LX/E0t;

    invoke-direct {v0}, LX/E0t;-><init>()V

    throw v0

    .line 2069095
    :pswitch_1
    new-instance v0, LX/E0x;

    invoke-direct {v0}, LX/E0x;-><init>()V

    throw v0

    .line 2069096
    :pswitch_2
    new-instance v0, LX/E0y;

    invoke-direct {v0}, LX/E0y;-><init>()V

    throw v0

    .line 2069097
    :pswitch_3
    invoke-direct {p0, p1}, LX/E10;->c(Lcom/facebook/http/protocol/ApiErrorResult;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;

    throw v0

    .line 2069098
    :pswitch_4
    invoke-direct {p0, p1}, LX/E10;->b(Lcom/facebook/http/protocol/ApiErrorResult;)Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;

    move-result-object v0

    throw v0

    .line 2069099
    :pswitch_5
    invoke-direct {p0, p1}, LX/E10;->b(Lcom/facebook/http/protocol/ApiErrorResult;)Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;

    move-result-object v0

    throw v0

    .line 2069100
    :pswitch_6
    new-instance v0, LX/E0u;

    invoke-direct {v0}, LX/E0u;-><init>()V

    throw v0

    .line 2069101
    :pswitch_7
    new-instance v0, LX/E0v;

    invoke-direct {v0}, LX/E0v;-><init>()V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
