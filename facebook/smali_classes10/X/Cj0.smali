.class public final LX/Cj0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Ve",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CH6;

.field private final b:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/CH6;LX/0Ve;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1928956
    iput-object p1, p0, LX/Cj0;->a:LX/CH6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1928957
    iput-object p2, p0, LX/Cj0;->b:LX/0Ve;

    .line 1928958
    iput-object p3, p0, LX/Cj0;->c:Ljava/lang/String;

    .line 1928959
    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 1

    .prologue
    .line 1928954
    iget-object v0, p0, LX/Cj0;->b:LX/0Ve;

    invoke-interface {v0}, LX/0Ve;->dispose()V

    .line 1928955
    return-void
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 1928953
    iget-object v0, p0, LX/Cj0;->b:LX/0Ve;

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1928951
    iget-object v0, p0, LX/Cj0;->b:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1928952
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1928945
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1928946
    iget-object v0, p0, LX/Cj0;->a:LX/CH6;

    iget-object v0, v0, LX/CH6;->c:LX/0QI;

    iget-object v1, p0, LX/Cj0;->c:Ljava/lang/String;

    new-instance v2, LX/Ciz;

    iget-object v3, p0, LX/Cj0;->a:LX/CH6;

    iget-object v4, p0, LX/Cj0;->a:LX/CH6;

    iget-object v4, v4, LX/CH6;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    .line 1928947
    iget-object v6, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v6, v6

    .line 1928948
    invoke-direct {v2, v3, v4, v5, v6}, LX/Ciz;-><init>(LX/CH6;JLjava/lang/Object;)V

    invoke-interface {v0, v1, v2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1928949
    iget-object v0, p0, LX/Cj0;->b:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1928950
    return-void
.end method
