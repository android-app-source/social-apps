.class public final LX/DJK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/DJL;


# direct methods
.method public constructor <init>(LX/DJL;)V
    .locals 0

    .prologue
    .line 1985178
    iput-object p1, p0, LX/DJK;->a:LX/DJL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x21168d0a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1985179
    const-string v0, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DJK;->a:LX/DJL;

    iget-object v0, v0, LX/DJL;->e:Ljava/lang/String;

    const-string v2, "extra_request_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1985180
    :cond_0
    const/16 v0, 0x27

    const v2, -0x39bb55b3

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1985181
    :goto_0
    return-void

    .line 1985182
    :cond_1
    iget-object v0, p0, LX/DJK;->a:LX/DJL;

    .line 1985183
    iget-object v2, v0, LX/DJL;->h:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->c()V

    .line 1985184
    const-string v0, "extra_result"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7m7;->valueOf(Ljava/lang/String;)LX/7m7;

    move-result-object v0

    .line 1985185
    const-string v2, "extra_legacy_api_post_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1985186
    sget-object v3, LX/7m7;->SUCCESS:LX/7m7;

    if-ne v0, v3, :cond_2

    if-nez v2, :cond_3

    .line 1985187
    :cond_2
    const v0, 0x37b40927

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0

    .line 1985188
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1985189
    iget-object v0, p0, LX/DJK;->a:LX/DJL;

    iget-object v0, v0, LX/DJL;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJh;

    .line 1985190
    iget-object v0, v0, LX/DJh;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1985191
    :cond_4
    new-instance v0, LX/4Eu;

    invoke-direct {v0}, LX/4Eu;-><init>()V

    invoke-virtual {v0, v2}, LX/4Eu;->a(Ljava/lang/String;)LX/4Eu;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/4Eu;->a(Ljava/util/List;)LX/4Eu;

    move-result-object v0

    iget-object v2, p0, LX/DJK;->a:LX/DJL;

    .line 1985192
    sget-object v3, LX/DJI;->a:[I

    iget-object v4, v2, LX/DJL;->f:LX/21D;

    invoke-virtual {v4}, LX/21D;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1985193
    const-string v3, "UNKNOWN"

    :goto_2
    move-object v2, v3

    .line 1985194
    invoke-virtual {v0, v2}, LX/4Eu;->b(Ljava/lang/String;)LX/4Eu;

    move-result-object v0

    .line 1985195
    invoke-static {}, LX/9KI;->a()LX/9KH;

    move-result-object v2

    .line 1985196
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1985197
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1985198
    iget-object v2, p0, LX/DJK;->a:LX/DJL;

    iget-object v2, v2, LX/DJL;->b:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1985199
    new-instance v2, LX/DJJ;

    invoke-direct {v2, p0, p1}, LX/DJJ;-><init>(LX/DJK;Landroid/content/Context;)V

    iget-object v3, p0, LX/DJK;->a:LX/DJL;

    iget-object v3, v3, LX/DJL;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1985200
    const v0, -0x6b899f16

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto/16 :goto_0

    .line 1985201
    :pswitch_0
    const-string v3, "BOOKMARK"

    goto :goto_2

    .line 1985202
    :pswitch_1
    const-string v3, "INVENTORY_MANAGEMENT"

    goto :goto_2

    .line 1985203
    :pswitch_2
    const-string v3, "GROUP_COMPOSER"

    goto :goto_2

    .line 1985204
    :pswitch_3
    const-string v3, "NEWSFEED"

    goto :goto_2

    .line 1985205
    :pswitch_4
    const-string v3, "TIMELINE"

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
