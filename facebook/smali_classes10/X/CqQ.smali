.class public final LX/CqQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/1a1;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field public final synthetic d:LX/CqR;


# direct methods
.method public constructor <init>(LX/CqR;I)V
    .locals 1

    .prologue
    .line 1939690
    iput-object p1, p0, LX/CqQ;->d:LX/CqR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1939691
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CqQ;->a:Ljava/util/List;

    .line 1939692
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CqQ;->b:Ljava/util/Map;

    .line 1939693
    iput p2, p0, LX/CqQ;->c:I

    .line 1939694
    return-void
.end method

.method private static a(III)I
    .locals 1

    .prologue
    .line 1939695
    add-int v0, p0, p1

    div-int/lit8 v0, v0, 0x2

    .line 1939696
    sub-int/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public static d(LX/CqQ;I)I
    .locals 6

    .prologue
    .line 1939697
    iget-object v0, p0, LX/CqQ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/CqQ;->c:I

    if-gt v0, v1, :cond_1

    .line 1939698
    const/4 p1, -0x1

    .line 1939699
    :cond_0
    return p1

    .line 1939700
    :cond_1
    iget-object v0, p0, LX/CqQ;->d:LX/CqR;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v3

    .line 1939701
    iget-object v0, p0, LX/CqQ;->d:LX/CqR;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v4

    .line 1939702
    invoke-static {v3, v4, p1}, LX/CqQ;->a(III)I

    move-result v0

    .line 1939703
    iget-object v1, p0, LX/CqQ;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1939704
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v3, v4, v1}, LX/CqQ;->a(III)I

    move-result v1

    .line 1939705
    if-le v1, v2, :cond_2

    .line 1939706
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    move v0, v1

    :goto_1
    move v2, v0

    .line 1939707
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method
