.class public LX/EIU;
.super Landroid/widget/RelativeLayout;
.source ""

# interfaces
.implements LX/ECf;
.implements LX/EFH;


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field public A:Landroid/widget/FrameLayout;

.field public B:Landroid/view/View;

.field public C:Landroid/view/View;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/TextView;

.field public F:Landroid/widget/TextView;

.field public G:Z

.field public H:Lcom/facebook/rtc/views/VoipConnectionBanner;

.field public I:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

.field public J:Lcom/facebook/rtc/views/RtcActionBar;

.field public K:Lcom/facebook/rtc/views/ChildLockBanner;

.field private L:Lcom/facebook/widget/tiles/ThreadTileView;

.field public M:Landroid/support/v4/view/ViewPager;

.field public N:LX/ED6;

.field public O:LX/ED1;

.field public P:Landroid/support/v7/widget/RecyclerView;

.field public Q:LX/EH4;

.field public R:Landroid/view/View;

.field public S:Landroid/widget/FrameLayout;

.field public T:Landroid/widget/FrameLayout;

.field public U:LX/EIX;

.field public V:Lcom/facebook/resources/ui/FbFrameLayout;

.field public W:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;",
            ">;"
        }
    .end annotation
.end field

.field public a:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public aa:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Lcom/facebook/rtc/views/GroupRingNoticeView;",
            ">;"
        }
    .end annotation
.end field

.field public ab:Landroid/view/animation/Animation;

.field private ac:Landroid/view/animation/Animation;

.field public ad:Landroid/view/animation/Animation;

.field private ae:Landroid/view/animation/Animation;

.field private af:Landroid/view/animation/Animation;

.field public ag:Landroid/view/animation/Animation;

.field public ah:LX/EBz;

.field public ai:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public aj:Z

.field public ak:LX/EC2;

.field public al:J

.field public am:Z

.field public an:LX/EIT;

.field public ao:Z

.field public ap:Landroid/support/v7/widget/RecyclerView;

.field public b:Landroid/view/WindowManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/ECj;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0hs;

.field public j:Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

.field private l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EG4;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/ED2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private p:LX/ED7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/ECi;

.field public t:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

.field public v:Lcom/facebook/rtc/views/RtcGridView;

.field public w:Landroid/view/View;

.field public x:Landroid/widget/TextView;

.field public y:LX/EBk;

.field public z:LX/EH3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2101795
    const-class v0, LX/EIU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EIU;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2101796
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2101797
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101798
    iput-object v0, p0, LX/EIU;->l:LX/0Ot;

    .line 2101799
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101800
    iput-object v0, p0, LX/EIU;->n:LX/0Ot;

    .line 2101801
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101802
    iput-object v0, p0, LX/EIU;->q:LX/0Ot;

    .line 2101803
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101804
    iput-object v0, p0, LX/EIU;->r:LX/0Ot;

    .line 2101805
    iput-object v1, p0, LX/EIU;->i:LX/0hs;

    .line 2101806
    iput-object v1, p0, LX/EIU;->j:Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

    .line 2101807
    iput-boolean v2, p0, LX/EIU;->aj:Z

    .line 2101808
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EIU;->al:J

    .line 2101809
    iput-boolean v2, p0, LX/EIU;->am:Z

    .line 2101810
    sget-object v0, LX/EIT;->NONE:LX/EIT;

    iput-object v0, p0, LX/EIU;->an:LX/EIT;

    .line 2101811
    iput-boolean v2, p0, LX/EIU;->ao:Z

    .line 2101812
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2101813
    const v1, 0x7f0315f6

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2101814
    const v0, 0x7f0d315b

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    iput-object v0, p0, LX/EIU;->u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    .line 2101815
    const v0, 0x7f0d315d

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcGridView;

    iput-object v0, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    .line 2101816
    const v0, 0x7f0d315f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->w:Landroid/view/View;

    .line 2101817
    const v0, 0x7f0d3160

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EIU;->x:Landroid/widget/TextView;

    .line 2101818
    const v0, 0x7f0d316f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EIU;->D:Landroid/widget/TextView;

    .line 2101819
    const v0, 0x7f0d316b

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EIU;->E:Landroid/widget/TextView;

    .line 2101820
    const v0, 0x7f0d316e

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EIU;->F:Landroid/widget/TextView;

    .line 2101821
    const v0, 0x7f0d316a

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->B:Landroid/view/View;

    .line 2101822
    const v0, 0x7f0d316c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/EIU;->L:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2101823
    const v0, 0x7f0d3150

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/ChildLockBanner;

    iput-object v0, p0, LX/EIU;->K:Lcom/facebook/rtc/views/ChildLockBanner;

    .line 2101824
    const v0, 0x7f0d3161

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/VoipConnectionBanner;

    iput-object v0, p0, LX/EIU;->H:Lcom/facebook/rtc/views/VoipConnectionBanner;

    .line 2101825
    const v0, 0x7f0d3166

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    .line 2101826
    const v0, 0x7f0d315e

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->C:Landroid/view/View;

    .line 2101827
    const v0, 0x7f0d3168

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    .line 2101828
    new-instance v1, LX/1P1;

    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    .line 2101829
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/1P1;->b(Z)V

    .line 2101830
    const v0, 0x7f0d3164

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, LX/EIU;->P:Landroid/support/v7/widget/RecyclerView;

    .line 2101831
    iget-object v0, p0, LX/EIU;->P:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2101832
    const v0, 0x7f0d3167

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    .line 2101833
    iget-object v0, p0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2101834
    const v0, 0x7f0d3170

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/EIU;->T:Landroid/widget/FrameLayout;

    .line 2101835
    const v0, 0x7f0d3163

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->R:Landroid/view/View;

    .line 2101836
    const v0, 0x7f0d3165

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/EIU;->S:Landroid/widget/FrameLayout;

    .line 2101837
    const v0, 0x7f0d315c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, LX/EIU;->V:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 2101838
    const v0, 0x7f0d2ae0

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->W:LX/4ob;

    .line 2101839
    const v0, 0x7f0d2ae1

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->aa:LX/4ob;

    .line 2101840
    const v0, 0x7f04005a

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->ab:Landroid/view/animation/Animation;

    .line 2101841
    const v0, 0x7f04005c

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->ac:Landroid/view/animation/Animation;

    .line 2101842
    const v0, 0x7f040058

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->ad:Landroid/view/animation/Animation;

    .line 2101843
    const v0, 0x7f040059

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->ae:Landroid/view/animation/Animation;

    .line 2101844
    const v0, 0x7f0400f5

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->af:Landroid/view/animation/Animation;

    .line 2101845
    const v0, 0x7f0400f7

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->ag:Landroid/view/animation/Animation;

    .line 2101846
    return-void
.end method

.method public static H(LX/EIU;)V
    .locals 5

    .prologue
    .line 2101847
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2101848
    iget-object v2, p0, LX/EIU;->L:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v0, p0, LX/EIU;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EG4;

    iget-object v1, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 2101849
    invoke-interface {v0}, LX/EG4;->a()LX/8Vc;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2101850
    :goto_0
    return-void

    .line 2101851
    :cond_0
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101852
    iget-wide v3, v0, LX/EDx;->ak:J

    move-wide v0, v3

    .line 2101853
    invoke-static {p0, v0, v1}, LX/EIU;->setProfileImageForUser(LX/EIU;J)V

    goto :goto_0
.end method

.method public static I(LX/EIU;)V
    .locals 2

    .prologue
    .line 2101854
    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2101855
    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    new-instance v1, LX/EIO;

    invoke-direct {v1, p0}, LX/EIO;-><init>(LX/EIU;)V

    .line 2101856
    iput-object v1, v0, Lcom/facebook/rtc/views/GroupRingNoticeView;->a:LX/EGo;

    .line 2101857
    :cond_0
    return-void
.end method

.method private J()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2101858
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aQ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2101859
    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08072d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, LX/EIU;->a(Ljava/lang/String;Z)V

    .line 2101860
    :goto_0
    return-void

    .line 2101861
    :cond_0
    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08072b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, LX/EIU;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static K(LX/EIU;)V
    .locals 2

    .prologue
    .line 2101862
    iget-boolean v0, p0, LX/EIU;->am:Z

    if-nez v0, :cond_0

    .line 2101863
    iget-object v0, p0, LX/EIU;->B:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2101864
    :cond_0
    return-void
.end method

.method private static L(LX/EIU;)I
    .locals 12

    .prologue
    const/16 v8, 0x4e20

    const/4 v0, 0x0

    .line 2101865
    iget-object v1, p0, LX/EIU;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/EDJ;->e:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v9

    .line 2101866
    const/4 v1, 0x3

    if-ge v9, v1, :cond_1

    .line 2101867
    new-instance v0, LX/ECg;

    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e02a5

    const v3, 0x7f031244

    iget-object v4, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    .line 2101868
    iget-wide v10, v4, LX/EDx;->ak:J

    move-wide v4, v10

    .line 2101869
    iget-object v6, p0, LX/EIU;->h:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v6}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, LX/ECg;-><init>(Landroid/content/Context;IIJJ)V

    .line 2101870
    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0807d1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2101871
    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0807cf

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 2101872
    iput v8, v0, LX/0hs;->t:I

    .line 2101873
    iput-object p0, v0, LX/ECg;->a:LX/ECf;

    .line 2101874
    iget-object v1, p0, LX/EIU;->z:LX/EH3;

    .line 2101875
    iget-object v2, v1, LX/EH3;->j:Landroid/widget/ImageButton;

    move-object v1, v2

    .line 2101876
    if-eqz v1, :cond_0

    .line 2101877
    iget-object v1, p0, LX/EIU;->z:LX/EH3;

    .line 2101878
    iget-object v2, v1, LX/EH3;->j:Landroid/widget/ImageButton;

    move-object v1, v2

    .line 2101879
    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2101880
    :cond_0
    iput-object v0, p0, LX/EIU;->i:LX/0hs;

    .line 2101881
    iget-object v0, p0, LX/EIU;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/EDJ;->e:LX/0Tn;

    add-int/lit8 v2, v9, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    move v0, v8

    .line 2101882
    :cond_1
    return v0
.end method

.method public static N(LX/EIU;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2101883
    invoke-virtual {p0}, LX/EIU;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2101884
    :goto_0
    return-void

    .line 2101885
    :cond_0
    iget-object v0, p0, LX/EIU;->H:Lcom/facebook/rtc/views/VoipConnectionBanner;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/VoipConnectionBanner;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EIU;->H:Lcom/facebook/rtc/views/VoipConnectionBanner;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/VoipConnectionBanner;->getHeight()I

    move-result v0

    :goto_1
    add-int/lit8 v2, v0, 0x0

    .line 2101886
    iget-object v0, p0, LX/EIU;->K:Lcom/facebook/rtc/views/ChildLockBanner;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/ChildLockBanner;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EIU;->K:Lcom/facebook/rtc/views/ChildLockBanner;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/ChildLockBanner;->getHeight()I

    move-result v0

    :goto_2
    add-int/2addr v2, v0

    .line 2101887
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->isShown()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->getHeight()I

    move-result v0

    :goto_3
    add-int/2addr v2, v0

    .line 2101888
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    .line 2101889
    :goto_4
    iget-object v3, p0, LX/EIU;->y:LX/EBk;

    .line 2101890
    iget-object p0, v3, LX/EBk;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object p0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    if-eqz p0, :cond_1

    .line 2101891
    iget-object p0, v3, LX/EBk;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object p0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {p0, v1, v1, v2, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(IIII)V

    .line 2101892
    :cond_1
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2101893
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2101894
    goto :goto_2

    :cond_4
    move v0, v1

    .line 2101895
    goto :goto_3

    .line 2101896
    :cond_5
    iget-object v0, p0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_4
.end method

.method public static O(LX/EIU;)V
    .locals 2

    .prologue
    .line 2101897
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    if-eqz v0, :cond_0

    .line 2101898
    :goto_0
    return-void

    .line 2101899
    :cond_0
    const v0, 0x7f0d3169

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcActionBar;

    iput-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    .line 2101900
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101901
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    sget-object v1, LX/EHD;->VIDEO_CONFERENCE:LX/EHD;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcActionBar;->setType(LX/EHD;)V

    .line 2101902
    :cond_1
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    new-instance v1, LX/EII;

    invoke-direct {v1, p0}, LX/EII;-><init>(LX/EIU;)V

    .line 2101903
    iput-object v1, v0, Lcom/facebook/rtc/views/RtcActionBar;->B:LX/EBv;

    .line 2101904
    goto :goto_0
.end method

.method public static P(LX/EIU;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2101905
    iget-object v1, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EIU;->N:LX/ED6;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->isShown()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2101906
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 2101907
    :cond_1
    invoke-static {p0}, LX/EIU;->getPageType(LX/EIU;)LX/ED5;

    move-result-object v1

    .line 2101908
    if-eqz v1, :cond_0

    .line 2101909
    sget-object v2, LX/EIK;->a:[I

    invoke-virtual {v1}, LX/ED5;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2101910
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static V(LX/EIU;)V
    .locals 2

    .prologue
    .line 2101911
    iget-object v0, p0, LX/EIU;->ai:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2101912
    iget-object v0, p0, LX/EIU;->ai:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2101913
    const/4 v0, 0x0

    iput-object v0, p0, LX/EIU;->ai:Ljava/util/concurrent/ScheduledFuture;

    .line 2101914
    :cond_0
    return-void
.end method

.method public static X(LX/EIU;)V
    .locals 2

    .prologue
    .line 2101915
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2101916
    iget-object v0, p0, LX/EIU;->P:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101917
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/EIU;->ae:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2101918
    :goto_0
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2101919
    :cond_0
    return-void

    .line 2101920
    :cond_1
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/EIU;->ac:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static Y(LX/EIU;)V
    .locals 2

    .prologue
    .line 2101715
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2101716
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    iget-object v1, p0, LX/EIU;->af:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcActionBar;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2101717
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcActionBar;->setVisibility(I)V

    .line 2101718
    :cond_0
    return-void
.end method

.method public static a(LX/EIU;LX/0Px;LX/EFy;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/EGE;",
            ">;",
            "Lcom/facebook/rtc/interfaces/SourcedConferenceCall;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v11, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2101921
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101922
    iget-object v1, v0, LX/EDx;->af:LX/EGE;

    if-eqz v1, :cond_f

    .line 2101923
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101924
    iget-object v1, v0, LX/EDx;->af:LX/EGE;

    move-object v6, v1

    .line 2101925
    invoke-static {p0}, LX/EIU;->getMultiViewMode(LX/EIU;)LX/EIT;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->an:LX/EIT;

    .line 2101926
    iget-object v0, p0, LX/EIU;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x5f7

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2101927
    if-nez v0, :cond_e

    .line 2101928
    iget-object v0, p0, LX/EIU;->g:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v5, LX/3Dx;->aR:I

    invoke-interface {v0, v1, v4, v5, v11}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    .line 2101929
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bk()I

    move-result v0

    if-ge v0, v1, :cond_3

    move v0, v2

    :goto_1
    move v4, v0

    .line 2101930
    :goto_2
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 2101931
    if-eqz v6, :cond_1

    iget-object v0, p0, LX/EIU;->an:LX/EIT;

    sget-object v1, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    if-ne v0, v1, :cond_1

    .line 2101932
    invoke-direct {p0, v6, p2}, LX/EIU;->a(LX/EGE;LX/EFy;)Z

    .line 2101933
    iget-boolean v0, v6, LX/EGE;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, v6, LX/EGE;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2101934
    iget-object v0, v6, LX/EGE;->g:Ljava/lang/String;

    sget-object v1, LX/EFu;->HIGH:LX/EFu;

    invoke-virtual {v1}, LX/EFu;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2101935
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v8

    move v5, v3

    :goto_3
    if-ge v5, v8, :cond_9

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2101936
    iget-object v9, v0, LX/EGE;->b:Ljava/lang/String;

    iget-object v1, p0, LX/EIU;->m:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v9, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2101937
    invoke-virtual {v0}, LX/EGE;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2101938
    iget-boolean v1, v0, LX/EGE;->i:Z

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/EGE;->g:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    .line 2101939
    :goto_4
    iget-object v9, p0, LX/EIU;->an:LX/EIT;

    sget-object v10, LX/EIT;->GRID_VIEW:LX/EIT;

    if-ne v9, v10, :cond_5

    .line 2101940
    invoke-direct {p0, v0, p2}, LX/EIU;->a(LX/EGE;LX/EFy;)Z

    move-result v9

    if-eqz v9, :cond_2

    if-eqz v1, :cond_2

    .line 2101941
    iget-object v0, v0, LX/EGE;->g:Ljava/lang/String;

    sget-object v1, LX/EFu;->MEDIUM:LX/EFu;

    invoke-virtual {v1}, LX/EFu;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2101942
    :cond_2
    :goto_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_3

    :cond_3
    move v0, v3

    .line 2101943
    goto :goto_1

    :cond_4
    move v1, v3

    .line 2101944
    goto :goto_4

    .line 2101945
    :cond_5
    iget-object v9, p0, LX/EIU;->an:LX/EIT;

    sget-object v10, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    if-ne v9, v10, :cond_2

    .line 2101946
    if-eqz v6, :cond_6

    iget-object v9, v6, LX/EGE;->b:Ljava/lang/String;

    iget-object v10, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 2101947
    :cond_6
    if-eqz v4, :cond_7

    .line 2101948
    invoke-direct {p0, v0, p2}, LX/EIU;->b(LX/EGE;LX/EFy;)V

    .line 2101949
    if-eqz v1, :cond_2

    .line 2101950
    iget-object v0, v0, LX/EGE;->g:Ljava/lang/String;

    sget-object v1, LX/EFu;->LOW:LX/EFu;

    invoke-virtual {v1}, LX/EFu;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 2101951
    :cond_7
    if-eqz v0, :cond_8

    if-nez p2, :cond_11

    .line 2101952
    :cond_8
    :goto_6
    goto :goto_5

    .line 2101953
    :cond_9
    invoke-direct {p0}, LX/EIU;->ag()V

    .line 2101954
    iget-object v0, p0, LX/EIU;->an:LX/EIT;

    sget-object v1, LX/EIT;->GRID_VIEW:LX/EIT;

    if-ne v0, v1, :cond_c

    .line 2101955
    iget-object v0, p0, LX/EIU;->R:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 2101956
    invoke-static {p0}, LX/EIU;->ae(LX/EIU;)V

    .line 2101957
    :cond_a
    :goto_7
    iget-object v0, p0, LX/EIU;->an:LX/EIT;

    sget-object v1, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    if-ne v0, v1, :cond_d

    iget-object v0, p0, LX/EIU;->O:LX/ED1;

    .line 2101958
    iget-boolean v1, v0, LX/ED1;->f:Z

    move v0, v1

    .line 2101959
    if-nez v0, :cond_d

    .line 2101960
    :goto_8
    invoke-static {p0}, LX/EIU;->getConferenceCall(LX/EIU;)LX/EFy;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2101961
    invoke-static {p0}, LX/EIU;->getConferenceCall(LX/EIU;)LX/EFy;

    move-result-object v0

    .line 2101962
    iget-object v1, v0, LX/EFy;->a:LX/EFw;

    iget-object v3, v0, LX/EFy;->b:LX/EG5;

    .line 2101963
    iget-boolean v4, v1, LX/EFw;->d:Z

    if-eqz v4, :cond_13

    .line 2101964
    sget-object v4, LX/EFw;->a:Ljava/lang/String;

    const-string v5, "Unable to update subscribed streams because conference call has ended"

    invoke-static {v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2101965
    :cond_b
    :goto_9
    invoke-virtual {p0}, LX/EIU;->d()V

    .line 2101966
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    iget-object v1, p0, LX/EIU;->an:LX/EIT;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcActionBar;->setMultiViewModeSwitchMode(LX/EIT;)V

    .line 2101967
    invoke-virtual {p0}, LX/EIU;->p()V

    .line 2101968
    return-void

    .line 2101969
    :cond_c
    iget-object v0, p0, LX/EIU;->an:LX/EIT;

    sget-object v1, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    if-ne v0, v1, :cond_a

    .line 2101970
    iget-object v0, p0, LX/EIU;->R:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2101971
    invoke-static {p0}, LX/EIU;->af(LX/EIU;)V

    .line 2101972
    iget-object v0, p0, LX/EIU;->O:LX/ED1;

    invoke-virtual {v0, p1}, LX/ED1;->a(LX/0Px;)V

    goto :goto_7

    :cond_d
    move v2, v3

    .line 2101973
    goto :goto_8

    :cond_e
    move v4, v3

    goto/16 :goto_2

    .line 2101974
    :cond_f
    iget-object v1, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_10
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EGE;

    .line 2101975
    invoke-virtual {v1}, LX/EGE;->b()Z

    move-result v5

    if-eqz v5, :cond_10

    iget-object v5, v1, LX/EGE;->b:Ljava/lang/String;

    iget-object v6, v0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2101976
    iget-object v7, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2101977
    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 2101978
    iput-object v1, v0, LX/EDx;->af:LX/EGE;

    goto/16 :goto_0

    .line 2101979
    :cond_11
    iget-object v1, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    iget-object v9, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v1, v9}, Lcom/facebook/rtc/views/RtcGridView;->b(Ljava/lang/String;)LX/EIC;

    move-result-object v1

    .line 2101980
    if-nez v1, :cond_12

    .line 2101981
    iget-object v1, p0, LX/EIU;->O:LX/ED1;

    iget-object v9, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v1, v9}, LX/ED1;->a(Ljava/lang/String;)LX/EIC;

    move-result-object v1

    .line 2101982
    :cond_12
    if-eqz v1, :cond_8

    .line 2101983
    iget-object v9, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    iget-object v10, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/facebook/rtc/views/RtcGridView;->a(Ljava/lang/String;)V

    .line 2101984
    iget-object v9, p0, LX/EIU;->O:LX/ED1;

    iget-object v10, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v9, v10}, LX/ED1;->b(Ljava/lang/String;)Z

    .line 2101985
    invoke-virtual {v1, p2}, LX/EIC;->a(LX/EFy;)V

    goto/16 :goto_6

    .line 2101986
    :cond_13
    iget-object v4, v1, LX/EFw;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    invoke-virtual {v4}, LX/EDx;->j()Z

    move-result v4

    if-nez v4, :cond_14

    .line 2101987
    sget-object v4, LX/EFw;->a:Ljava/lang/String;

    const-string v5, "Unable to update subscribed streams because of invalid arguments"

    invoke-static {v4, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 2101988
    :cond_14
    const-string v4, "Subscribe to multiple streams"

    invoke-virtual {v1, v3, v4}, LX/EFw;->a(LX/EG5;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2101989
    iget-object v4, v1, LX/EFw;->i:Ljava/util/Map;

    invoke-interface {v4, v7}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    iget-boolean v4, v1, LX/EFw;->e:Z

    if-eq v4, v2, :cond_b

    .line 2101990
    :cond_15
    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v4

    new-array v6, v4, [I

    .line 2101991
    invoke-interface {v7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/Integer;

    invoke-interface {v4, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Integer;

    .line 2101992
    const/4 v5, 0x0

    :goto_a
    array-length v0, v6

    if-ge v5, v0, :cond_16

    .line 2101993
    aget-object v0, v4, v5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v6, v5

    .line 2101994
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    .line 2101995
    :cond_16
    iget-object v5, v1, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v5, v2, v4, v6}, Lcom/facebook/webrtc/ConferenceCall;->subscribeRemoteVideoStreams(Z[Ljava/lang/String;[I)Z

    .line 2101996
    invoke-static {v1}, LX/EFw;->i(LX/EFw;)V

    .line 2101997
    iput-object v7, v1, LX/EFw;->i:Ljava/util/Map;

    .line 2101998
    iput-boolean v2, v1, LX/EFw;->e:Z

    goto/16 :goto_9
.end method

.method private static a(LX/EIU;Ljava/util/concurrent/ScheduledExecutorService;Landroid/view/WindowManager;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;LX/ECj;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/0Or;LX/0Ot;LX/0Or;LX/0Ot;LX/ED2;LX/ED7;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EIU;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Landroid/view/WindowManager;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Ljava/util/concurrent/Executor;",
            "LX/ECj;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EG4;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;",
            "LX/ED2;",
            "LX/ED7;",
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2101999
    iput-object p1, p0, LX/EIU;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p2, p0, LX/EIU;->b:Landroid/view/WindowManager;

    iput-object p3, p0, LX/EIU;->c:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p4, p0, LX/EIU;->d:Ljava/util/concurrent/Executor;

    iput-object p5, p0, LX/EIU;->e:LX/ECj;

    iput-object p6, p0, LX/EIU;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p7, p0, LX/EIU;->g:LX/0ad;

    iput-object p8, p0, LX/EIU;->h:LX/0Or;

    iput-object p9, p0, LX/EIU;->l:LX/0Ot;

    iput-object p10, p0, LX/EIU;->m:LX/0Or;

    iput-object p11, p0, LX/EIU;->n:LX/0Ot;

    iput-object p12, p0, LX/EIU;->o:LX/ED2;

    iput-object p13, p0, LX/EIU;->p:LX/ED7;

    iput-object p14, p0, LX/EIU;->q:LX/0Ot;

    iput-object p15, p0, LX/EIU;->r:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 17

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, LX/EIU;

    invoke-static {v15}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v15}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-static {v15}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v15}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    const-class v5, LX/ECj;

    invoke-interface {v15, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/ECj;

    invoke-static {v15}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v15}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    const/16 v8, 0x12cd

    invoke-static {v15, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x3261

    invoke-static {v15, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x15e8

    invoke-static {v15, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x3257

    invoke-static {v15, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const-class v12, LX/ED2;

    invoke-interface {v15, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/ED2;

    const-class v13, LX/ED7;

    invoke-interface {v15, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/ED7;

    const/16 v14, 0x3266

    invoke-static {v15, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v16, 0xac0

    invoke-static/range {v15 .. v16}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v0 .. v15}, LX/EIU;->a(LX/EIU;Ljava/util/concurrent/ScheduledExecutorService;Landroid/view/WindowManager;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;LX/ECj;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/0Or;LX/0Ot;LX/0Or;LX/0Ot;LX/ED2;LX/ED7;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private a(LX/EGE;LX/EFy;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2102000
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v0, v1

    .line 2102001
    :goto_0
    return v0

    .line 2102002
    :cond_1
    iget-object v0, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    iget-object v2, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcGridView;->b(Ljava/lang/String;)LX/EIC;

    move-result-object v0

    .line 2102003
    if-eqz v0, :cond_2

    .line 2102004
    sget-object v1, LX/EIB;->Normal:LX/EIB;

    invoke-virtual {v0, p1, p2, v1}, LX/EIC;->a(LX/EGE;LX/EFy;LX/EIB;)V

    .line 2102005
    const/4 v0, 0x1

    goto :goto_0

    .line 2102006
    :cond_2
    iget-object v0, p0, LX/EIU;->O:LX/ED1;

    iget-object v2, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/ED1;->a(Ljava/lang/String;)LX/EIC;

    move-result-object v0

    .line 2102007
    if-nez v0, :cond_3

    .line 2102008
    invoke-static {p0}, LX/EIU;->ad(LX/EIU;)LX/EIC;

    move-result-object v0

    .line 2102009
    :goto_1
    invoke-virtual {v0, v1}, LX/EIC;->setZOrderOnTop(Z)V

    .line 2102010
    iget-object v1, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    iget-object v2, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/rtc/views/RtcGridView;->a(Ljava/lang/String;LX/EIC;)Z

    move-result v1

    .line 2102011
    if-eqz v1, :cond_4

    .line 2102012
    sget-object v2, LX/EIB;->Normal:LX/EIB;

    invoke-virtual {v0, p1, p2, v2}, LX/EIC;->a(LX/EGE;LX/EFy;LX/EIB;)V

    :goto_2
    move v0, v1

    .line 2102013
    goto :goto_0

    .line 2102014
    :cond_3
    iget-object v2, p0, LX/EIU;->O:LX/ED1;

    iget-object v3, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/ED1;->b(Ljava/lang/String;)Z

    goto :goto_1

    .line 2102015
    :cond_4
    invoke-virtual {v0, p2}, LX/EIC;->a(LX/EFy;)V

    goto :goto_2
.end method

.method public static a$redex0(LX/EIU;II)V
    .locals 3

    .prologue
    .line 2102100
    iget-object v0, p0, LX/EIU;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/views/VoipVideoView$12;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/rtc/views/VoipVideoView$12;-><init>(LX/EIU;II)V

    const v2, -0x1da8c0c0

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2102101
    return-void
.end method

.method public static a$redex0(LX/EIU;LX/ED5;)V
    .locals 3

    .prologue
    .line 2102085
    iget-object v0, p0, LX/EIU;->N:LX/ED6;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 2102086
    :cond_0
    :goto_0
    return-void

    .line 2102087
    :cond_1
    sget-object v0, LX/ED5;->ROSTER:LX/ED5;

    if-ne p1, v0, :cond_2

    .line 2102088
    iget-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 2102089
    :cond_2
    iget-object v0, p0, LX/EIU;->N:LX/ED6;

    .line 2102090
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    iget-object v1, v0, LX/ED6;->i:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_5

    .line 2102091
    iget-object v1, v0, LX/ED6;->i:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ED5;

    .line 2102092
    if-ne p1, v1, :cond_4

    .line 2102093
    :goto_2
    move v0, v2

    .line 2102094
    if-gez v0, :cond_3

    .line 2102095
    invoke-virtual {p1}, LX/ED5;->ordinal()I

    goto :goto_0

    .line 2102096
    :cond_3
    iget-object v1, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2102097
    invoke-direct {p0}, LX/EIU;->ag()V

    goto :goto_0

    .line 2102098
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2102099
    :cond_5
    const/4 v2, -0x1

    goto :goto_2
.end method

.method public static aa(LX/EIU;)V
    .locals 2

    .prologue
    .line 2102081
    iget-object v0, p0, LX/EIU;->t:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 2102082
    iget-object v0, p0, LX/EIU;->t:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2102083
    const/4 v0, 0x0

    iput-object v0, p0, LX/EIU;->t:Ljava/util/concurrent/Future;

    .line 2102084
    :cond_0
    return-void
.end method

.method public static ab(LX/EIU;)Z
    .locals 1

    .prologue
    .line 2102080
    invoke-virtual {p0}, LX/EIU;->getDeferredMillisecondsForVideoCall()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static ad(LX/EIU;)LX/EIC;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2102077
    new-instance v0, LX/EIC;

    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EIC;-><init>(Landroid/content/Context;)V

    .line 2102078
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, LX/EIC;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2102079
    return-object v0
.end method

.method public static ae(LX/EIU;)V
    .locals 2

    .prologue
    .line 2102068
    iget-object v0, p0, LX/EIU;->Q:LX/EH4;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcGridView;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2102069
    :cond_0
    :goto_0
    return-void

    .line 2102070
    :cond_1
    iget-object v0, p0, LX/EIU;->S:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/EIU;->Q:LX/EH4;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 2102071
    iget-object v0, p0, LX/EIU;->S:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2102072
    iget-object v0, p0, LX/EIU;->Q:LX/EH4;

    .line 2102073
    iget-object v1, v0, LX/EH4;->b:Landroid/view/View;

    move-object v0, v1

    .line 2102074
    check-cast v0, LX/EIE;

    .line 2102075
    sget-object v1, LX/EID;->CENTER_CROP_THRESHOLD:LX/EID;

    invoke-virtual {v0, v1}, LX/EIE;->setScaleType(LX/EID;)V

    .line 2102076
    iget-object v0, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    iget-object v1, p0, LX/EIU;->Q:LX/EH4;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/view/View;)Z

    goto :goto_0
.end method

.method public static af(LX/EIU;)V
    .locals 4

    .prologue
    .line 2102057
    iget-object v0, p0, LX/EIU;->Q:LX/EH4;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EIU;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 2102058
    :cond_0
    :goto_0
    return-void

    .line 2102059
    :cond_1
    iget-object v0, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcGridView;->a()V

    .line 2102060
    iget-object v0, p0, LX/EIU;->Q:LX/EH4;

    .line 2102061
    iget-object v1, v0, LX/EH4;->b:Landroid/view/View;

    move-object v0, v1

    .line 2102062
    check-cast v0, LX/EIE;

    .line 2102063
    sget-object v1, LX/EID;->DYNAMIC_WIDTH:LX/EID;

    invoke-virtual {v0, v1}, LX/EIE;->setScaleType(LX/EID;)V

    .line 2102064
    iget-object v0, p0, LX/EIU;->Q:LX/EH4;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, LX/EH4;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2102065
    iget-object v0, p0, LX/EIU;->S:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/EIU;->Q:LX/EH4;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2102066
    iget-object v0, p0, LX/EIU;->S:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2102067
    iget-object v0, p0, LX/EIU;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    goto :goto_0
.end method

.method private ag()V
    .locals 3

    .prologue
    .line 2102049
    invoke-static {p0}, LX/EIU;->getPageType(LX/EIU;)LX/ED5;

    move-result-object v1

    .line 2102050
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bo()LX/EGp;

    move-result-object v2

    .line 2102051
    if-eqz v1, :cond_0

    sget-object v0, LX/ED5;->EMPTY:LX/ED5;

    if-ne v1, v0, :cond_0

    if-nez v2, :cond_1

    .line 2102052
    :cond_0
    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    .line 2102053
    :goto_0
    return-void

    .line 2102054
    :cond_1
    invoke-static {p0}, LX/EIU;->I(LX/EIU;)V

    .line 2102055
    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/GroupRingNoticeView;->setModeAndShow(LX/EGp;)V

    .line 2102056
    const/16 v0, 0x1388

    invoke-static {p0, v0}, LX/EIU;->c(LX/EIU;I)V

    goto :goto_0
.end method

.method private b(LX/EGE;LX/EFy;)V
    .locals 3

    .prologue
    .line 2102033
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2102034
    :cond_0
    :goto_0
    return-void

    .line 2102035
    :cond_1
    iget-object v0, p0, LX/EIU;->O:LX/ED1;

    iget-object v1, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/ED1;->a(Ljava/lang/String;)LX/EIC;

    move-result-object v0

    .line 2102036
    if-eqz v0, :cond_2

    .line 2102037
    sget-object v1, LX/EIB;->Thumbnail:LX/EIB;

    invoke-virtual {v0, p1, p2, v1}, LX/EIC;->a(LX/EGE;LX/EFy;LX/EIB;)V

    goto :goto_0

    .line 2102038
    :cond_2
    iget-object v0, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    iget-object v1, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcGridView;->b(Ljava/lang/String;)LX/EIC;

    move-result-object v0

    .line 2102039
    if-nez v0, :cond_4

    .line 2102040
    invoke-static {p0}, LX/EIU;->ad(LX/EIU;)LX/EIC;

    move-result-object v0

    .line 2102041
    :goto_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EIC;->setZOrderOnTop(Z)V

    .line 2102042
    iget-object v1, p0, LX/EIU;->O:LX/ED1;

    iget-object v2, p1, LX/EGE;->b:Ljava/lang/String;

    .line 2102043
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_3

    if-nez v0, :cond_5

    .line 2102044
    :cond_3
    :goto_2
    sget-object v1, LX/EIB;->Thumbnail:LX/EIB;

    invoke-virtual {v0, p1, p2, v1}, LX/EIC;->a(LX/EGE;LX/EFy;LX/EIB;)V

    goto :goto_0

    .line 2102045
    :cond_4
    iget-object v1, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    iget-object v2, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/rtc/views/RtcGridView;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 2102046
    :cond_5
    iget-object p0, v1, LX/ED1;->b:Ljava/util/Map;

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102047
    invoke-static {v1, v2}, LX/ED1;->c(LX/ED1;Ljava/lang/String;)V

    .line 2102048
    goto :goto_2
.end method

.method public static c(LX/EIU;I)V
    .locals 2

    .prologue
    .line 2102027
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2102028
    iget-boolean v1, v0, LX/EDx;->cf:Z

    move v0, v1

    .line 2102029
    if-eqz v0, :cond_0

    .line 2102030
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EIU;->e(LX/EIU;I)V

    .line 2102031
    :goto_0
    return-void

    .line 2102032
    :cond_0
    invoke-static {p0, p1}, LX/EIU;->d(LX/EIU;I)V

    goto :goto_0
.end method

.method public static d(LX/EIU;I)V
    .locals 2

    .prologue
    .line 2102017
    iget-object v0, p0, LX/EIU;->t:Ljava/util/concurrent/Future;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/EIU;->W:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2102018
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2102019
    iget-object v0, p0, LX/EIU;->P:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2102020
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/EIU;->ad:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2102021
    :goto_0
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2102022
    :cond_0
    invoke-static {p0}, LX/EIU;->Y(LX/EIU;)V

    .line 2102023
    invoke-static {p0, p1}, LX/EIU;->e(LX/EIU;I)V

    .line 2102024
    invoke-static {p0}, LX/EIU;->N(LX/EIU;)V

    .line 2102025
    :cond_1
    return-void

    .line 2102026
    :cond_2
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/EIU;->ab:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static e(LX/EIU;I)V
    .locals 5

    .prologue
    .line 2101791
    invoke-static {p0}, LX/EIU;->aa(LX/EIU;)V

    .line 2101792
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101793
    :cond_0
    :goto_0
    return-void

    .line 2101794
    :cond_1
    iget-object v0, p0, LX/EIU;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/views/VoipVideoView$14;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/views/VoipVideoView$14;-><init>(LX/EIU;)V

    int-to-long v2, p1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->t:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public static getConferenceCall(LX/EIU;)LX/EFy;
    .locals 2

    .prologue
    .line 2102016
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EG5;->Activity:LX/EG5;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EG5;)LX/EFy;

    move-result-object v0

    return-object v0
.end method

.method public static getMultiViewMode(LX/EIU;)LX/EIT;
    .locals 2

    .prologue
    .line 2101560
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bk()I

    move-result v0

    .line 2101561
    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 2101562
    sget-object v0, LX/EIT;->GRID_VIEW:LX/EIT;

    .line 2101563
    :goto_0
    return-object v0

    .line 2101564
    :cond_0
    const/4 v1, 0x4

    if-gt v0, v1, :cond_1

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    .line 2101565
    :cond_1
    sget-object v0, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    goto :goto_0

    .line 2101566
    :cond_2
    iget-boolean v0, p0, LX/EIU;->ao:Z

    if-eqz v0, :cond_3

    .line 2101567
    iget-object v0, p0, LX/EIU;->an:LX/EIT;

    goto :goto_0

    .line 2101568
    :cond_3
    sget-object v0, LX/EIT;->GRID_VIEW:LX/EIT;

    goto :goto_0
.end method

.method public static getPageType(LX/EIU;)LX/ED5;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2101569
    iget-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EIU;->N:LX/ED6;

    if-nez v0, :cond_1

    .line 2101570
    :cond_0
    const/4 v0, 0x0

    .line 2101571
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/EIU;->N:LX/ED6;

    iget-object v1, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2101572
    if-ltz v1, :cond_2

    iget-object v2, v0, LX/ED6;->i:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 2101573
    :cond_2
    sget-object v2, LX/ED6;->a:Ljava/lang/String;

    const-string p0, "Invalid position for getType"

    invoke-static {v2, p0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2101574
    const/4 v2, 0x0

    .line 2101575
    :goto_1
    move-object v0, v2

    .line 2101576
    goto :goto_0

    :cond_3
    iget-object v2, v0, LX/ED6;->i:LX/0Px;

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ED5;

    goto :goto_1
.end method

.method public static setProfileImageForUser(LX/EIU;J)V
    .locals 5

    .prologue
    .line 2101577
    iget-object v0, p0, LX/EIU;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EG4;

    iget-object v1, p0, LX/EIU;->m:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {p1, p2, v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0}, LX/EG4;->a()LX/8Vc;

    move-result-object v0

    .line 2101578
    iget-object v1, p0, LX/EIU;->L:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2101579
    return-void
.end method


# virtual methods
.method public final C()V
    .locals 2

    .prologue
    .line 2101580
    iget-object v0, p0, LX/EIU;->K:Lcom/facebook/rtc/views/ChildLockBanner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/ChildLockBanner;->setVisibility(I)V

    .line 2101581
    invoke-virtual {p0}, LX/EIU;->t()Z

    .line 2101582
    invoke-static {p0}, LX/EIU;->N(LX/EIU;)V

    .line 2101583
    return-void
.end method

.method public final E()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2101584
    invoke-virtual {p0, v1}, LX/EIU;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2101585
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->a()V

    .line 2101586
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v2, p0, LX/EIU;->ak:LX/EC2;

    invoke-virtual {v0, v2}, LX/EDx;->b(LX/EC2;)V

    .line 2101587
    iget-object v0, p0, LX/EIU;->U:LX/EIX;

    if-eqz v0, :cond_0

    .line 2101588
    iget-object v0, p0, LX/EIU;->U:LX/EIX;

    .line 2101589
    iput-object v1, v0, LX/EIX;->c:LX/EIJ;

    .line 2101590
    iget-object v0, p0, LX/EIU;->U:LX/EIX;

    invoke-virtual {v0}, LX/EIX;->a()V

    .line 2101591
    :cond_0
    iget-object v0, p0, LX/EIU;->W:LX/4ob;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EIU;->W:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101592
    iget-object v0, p0, LX/EIU;->W:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->b()V

    .line 2101593
    :cond_1
    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2101594
    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/GroupRingNoticeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2101595
    :cond_2
    iget-object v0, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    if-nez v0, :cond_4

    .line 2101596
    :cond_3
    return-void

    .line 2101597
    :cond_4
    iget-object v0, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    .line 2101598
    iput-object v1, v0, Lcom/facebook/rtc/views/RtcGridView;->f:LX/EHI;

    .line 2101599
    iget-object v0, p0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcGridView;->getAllRemoteViews()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EIC;

    .line 2101600
    invoke-static {p0}, LX/EIU;->getConferenceCall(LX/EIU;)LX/EFy;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/EIC;->a(LX/EFy;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 2101601
    iget-object v0, p0, LX/EIU;->g:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget v3, LX/3Dx;->em:I

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    .line 2101602
    packed-switch v0, :pswitch_data_0

    .line 2101603
    :goto_0
    return-void

    .line 2101604
    :pswitch_0
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    .line 2101605
    iget-object v1, v0, Lcom/facebook/rtc/views/RtcActionBar;->f:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 2101606
    invoke-virtual {v0}, Landroid/view/View;->callOnClick()Z

    goto :goto_0

    .line 2101607
    :pswitch_1
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101608
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2101609
    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2101610
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2101611
    iget-object v2, v0, LX/EDx;->j:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v0, LX/EDx;->h:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2101612
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2101613
    iget-object v0, p0, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101614
    invoke-static {p0, p1}, LX/EIU;->e(LX/EIU;I)V

    .line 2101615
    :cond_0
    :goto_0
    return-void

    .line 2101616
    :cond_1
    invoke-static {p0}, LX/EIU;->aa(LX/EIU;)V

    .line 2101617
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2101618
    invoke-static {p0, p1}, LX/EIU;->d(LX/EIU;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2101619
    invoke-static {p0}, LX/EIU;->O(LX/EIU;)V

    .line 2101620
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0, p1}, Lcom/facebook/rtc/views/RtcActionBar;->a(Ljava/lang/String;)V

    .line 2101621
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2101622
    if-eqz p2, :cond_0

    .line 2101623
    iget-object v0, p0, LX/EIU;->D:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2101624
    iget-object v0, p0, LX/EIU;->E:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2101625
    :goto_0
    iget-object v1, p0, LX/EIU;->L:Lcom/facebook/widget/tiles/ThreadTileView;

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setVisibility(I)V

    .line 2101626
    invoke-virtual {p0}, LX/EIU;->j()V

    .line 2101627
    return-void

    .line 2101628
    :cond_0
    iget-object v0, p0, LX/EIU;->E:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2101629
    iget-object v0, p0, LX/EIU;->D:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2101630
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public final b(LX/0gc;)V
    .locals 3

    .prologue
    .line 2101631
    iget-object v0, p0, LX/EIU;->N:LX/ED6;

    if-eqz v0, :cond_0

    .line 2101632
    :goto_0
    return-void

    .line 2101633
    :cond_0
    iget-object v0, p0, LX/EIU;->p:LX/ED7;

    sget-object v1, LX/ED4;->VIDEO:LX/ED4;

    invoke-virtual {v0, p1, v1}, LX/ED7;->a(LX/0gc;LX/ED4;)LX/ED6;

    move-result-object v0

    iput-object v0, p0, LX/EIU;->N:LX/ED6;

    .line 2101634
    iget-object v0, p0, LX/EIU;->g:LX/0ad;

    sget-short v1, LX/3Dx;->aJ:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2101635
    if-eqz v0, :cond_1

    .line 2101636
    iget-object v0, p0, LX/EIU;->N:LX/ED6;

    new-instance v1, LX/EIR;

    invoke-direct {v1, p0}, LX/EIR;-><init>(LX/EIU;)V

    .line 2101637
    invoke-static {v0}, LX/ED6;->h(LX/ED6;)V

    .line 2101638
    iget-object v2, v0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    .line 2101639
    iput-object v1, v2, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->i:Landroid/view/View$OnClickListener;

    .line 2101640
    iget-object p1, v2, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    if-nez p1, :cond_2

    .line 2101641
    :cond_1
    :goto_1
    iget-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, LX/EIU;->N:LX/ED6;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2101642
    iget-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    new-instance v1, LX/EIS;

    invoke-direct {v1, p0}, LX/EIS;-><init>(LX/EIU;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2101643
    iget-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    goto :goto_0

    .line 2101644
    :cond_2
    iget-object p1, v2, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    iget-object v0, v2, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, LX/EEp;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public final c(LX/0gc;)I
    .locals 13

    .prologue
    .line 2101645
    iget-object v0, p0, LX/EIU;->g:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget v3, LX/3Dx;->em:I

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    .line 2101646
    packed-switch v0, :pswitch_data_0

    .line 2101647
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2101648
    :pswitch_0
    const/16 v0, 0x4e20

    const/4 v1, 0x0

    .line 2101649
    iget-object v2, p0, LX/EIU;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/EDJ;->d:LX/0Tn;

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 2101650
    const/4 v3, 0x3

    if-ge v2, v3, :cond_2

    .line 2101651
    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080011

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2101652
    new-instance v4, LX/0hs;

    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e02a3

    invoke-direct {v4, v5, v6}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v4, p0, LX/EIU;->i:LX/0hs;

    .line 2101653
    iget-object v4, p0, LX/EIU;->i:LX/0hs;

    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0807ce

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    aput-object v3, p1, v1

    invoke-virtual {v5, v6, p1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2101654
    iget-object v1, p0, LX/EIU;->i:LX/0hs;

    .line 2101655
    iput v0, v1, LX/0hs;->t:I

    .line 2101656
    iget-object v1, p0, LX/EIU;->i:LX/0hs;

    iget-object v3, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    .line 2101657
    iget-object v4, v3, Lcom/facebook/rtc/views/RtcActionBar;->f:Landroid/widget/LinearLayout;

    move-object v3, v4

    .line 2101658
    invoke-virtual {v1, v3}, LX/0ht;->a(Landroid/view/View;)V

    .line 2101659
    iget-object v1, p0, LX/EIU;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v3, LX/EDJ;->d:LX/0Tn;

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1, v3, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2101660
    :goto_1
    move v0, v0

    .line 2101661
    goto :goto_0

    .line 2101662
    :pswitch_1
    invoke-static {p0}, LX/EIU;->L(LX/EIU;)I

    move-result v0

    goto :goto_0

    .line 2101663
    :pswitch_2
    const/4 v5, 0x0

    .line 2101664
    iget-object v6, p0, LX/EIU;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/EDJ;->f:LX/0Tn;

    invoke-interface {v6, v7, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v6

    .line 2101665
    const/4 v7, 0x3

    if-ge v6, v7, :cond_1

    .line 2101666
    iget-object v5, p0, LX/EIU;->j:Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

    if-nez v5, :cond_0

    .line 2101667
    iget-object v5, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/EDx;

    .line 2101668
    iget-wide v11, v5, LX/EDx;->ak:J

    move-wide v7, v11

    .line 2101669
    iget-object v5, p0, LX/EIU;->h:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v5}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    .line 2101670
    new-instance v11, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

    invoke-direct {v11}, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;-><init>()V

    .line 2101671
    iput-wide v7, v11, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->p:J

    .line 2101672
    iput-wide v9, v11, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->q:J

    .line 2101673
    move-object v5, v11

    .line 2101674
    iput-object v5, p0, LX/EIU;->j:Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

    .line 2101675
    iget-object v5, p0, LX/EIU;->j:Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

    .line 2101676
    iput-object p0, v5, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->m:LX/EFH;

    .line 2101677
    invoke-virtual {p1}, LX/0gc;->a()LX/0hH;

    move-result-object v5

    iget-object v7, p0, LX/EIU;->j:Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

    const-string v8, "video_chat_head_nux_fragment_tag"

    invoke-virtual {v5, v7, v8}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v5

    invoke-virtual {v5}, LX/0hH;->c()I

    .line 2101678
    :cond_0
    iget-object v5, p0, LX/EIU;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v7, LX/EDJ;->f:LX/0Tn;

    add-int/lit8 v6, v6, 0x1

    invoke-interface {v5, v7, v6}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 2101679
    const/16 v5, 0x4e20

    .line 2101680
    :cond_1
    move v0, v5

    .line 2101681
    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c()V
    .locals 7

    .prologue
    .line 2101682
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aU()J

    move-result-wide v0

    iget-object v2, p0, LX/EIU;->g:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v5, LX/3Dx;->c:I

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/EIU;->al:J

    .line 2101683
    return-void
.end method

.method public final d()V
    .locals 9

    .prologue
    .line 2101684
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->br()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2101685
    :goto_0
    return-void

    .line 2101686
    :cond_0
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->N()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101687
    invoke-static {p0}, LX/EIU;->V(LX/EIU;)V

    .line 2101688
    iget-object v2, p0, LX/EIU;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/facebook/rtc/views/VoipVideoView$11;

    invoke-direct {v3, p0}, Lcom/facebook/rtc/views/VoipVideoView$11;-><init>(LX/EIU;)V

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x1f4

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v2 .. v8}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v2

    iput-object v2, p0, LX/EIU;->ai:Ljava/util/concurrent/ScheduledFuture;

    .line 2101689
    invoke-static {p0}, LX/EIU;->K(LX/EIU;)V

    goto :goto_0

    .line 2101690
    :cond_1
    invoke-static {p0}, LX/EIU;->V(LX/EIU;)V

    .line 2101691
    iget-object v0, p0, LX/EIU;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2101692
    iget-object v0, p0, LX/EIU;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2101693
    :cond_2
    invoke-virtual {p0}, LX/EIU;->e()V

    .line 2101694
    iget-object v0, p0, LX/EIU;->B:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final e()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2101695
    iget-boolean v0, p0, LX/EIU;->am:Z

    if-eqz v0, :cond_1

    .line 2101696
    :cond_0
    :goto_0
    return-void

    .line 2101697
    :cond_1
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aR()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2101698
    invoke-static {p0}, LX/EIU;->ab(LX/EIU;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2101699
    iget-object v5, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/EDx;

    invoke-virtual {v5}, LX/EDx;->aU()J

    move-result-wide v5

    iget-wide v7, p0, LX/EIU;->al:J

    cmp-long v5, v5, v7

    if-lez v5, :cond_5

    const/4 v5, 0x1

    :goto_1
    move v0, v5

    .line 2101700
    if-eqz v0, :cond_0

    .line 2101701
    invoke-direct {p0}, LX/EIU;->J()V

    goto :goto_0

    .line 2101702
    :cond_2
    invoke-direct {p0}, LX/EIU;->J()V

    goto :goto_0

    .line 2101703
    :cond_3
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2101704
    invoke-static {p0}, LX/EIU;->K(LX/EIU;)V

    goto :goto_0

    .line 2101705
    :cond_4
    invoke-virtual {p0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080700

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->az()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, LX/EIU;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public getDeferredMillisecondsForVideoCall()I
    .locals 5

    .prologue
    .line 2101706
    iget-object v0, p0, LX/EIU;->g:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget v3, LX/3Dx;->c:I

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    return v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 2101707
    iget-object v0, p0, LX/EIU;->z:LX/EH3;

    if-eqz v0, :cond_0

    .line 2101708
    iget-object v0, p0, LX/EIU;->z:LX/EH3;

    invoke-virtual {v0}, LX/EH3;->a()V

    .line 2101709
    :cond_0
    iget-object v0, p0, LX/EIU;->Q:LX/EH4;

    if-eqz v0, :cond_2

    .line 2101710
    iget-object v0, p0, LX/EIU;->Q:LX/EH4;

    .line 2101711
    iget-object v1, v0, LX/EH4;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->A()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/EH4;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aP()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, LX/EH4;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->K()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2101712
    :cond_1
    iget-object v1, v0, LX/EH4;->c:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2101713
    :cond_2
    :goto_0
    return-void

    .line 2101714
    :cond_3
    iget-object v1, v0, LX/EH4;->c:Landroid/view/View;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2101719
    iget-object v0, p0, LX/EIU;->i:LX/0hs;

    if-eqz v0, :cond_0

    .line 2101720
    iget-object v0, p0, LX/EIU;->i:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2101721
    iput-object v1, p0, LX/EIU;->i:LX/0hs;

    .line 2101722
    :cond_0
    iget-object v0, p0, LX/EIU;->j:Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

    if-eqz v0, :cond_1

    .line 2101723
    iget-object v0, p0, LX/EIU;->j:Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2101724
    iput-object v1, p0, LX/EIU;->j:Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;

    .line 2101725
    :cond_1
    return-void
.end method

.method public final l()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2101726
    iget-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2101727
    iget-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2101728
    iget-object v0, p0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 2101729
    iput-object v2, p0, LX/EIU;->N:LX/ED6;

    .line 2101730
    return-void
.end method

.method public onClick(I)V
    .locals 1

    .prologue
    .line 2101731
    const v0, 0x7f0d2af7

    if-ne p1, v0, :cond_1

    .line 2101732
    invoke-virtual {p0}, LX/EIU;->k()V

    .line 2101733
    :cond_0
    :goto_0
    return-void

    .line 2101734
    :cond_1
    const v0, 0x7f0d2af8

    if-ne p1, v0, :cond_0

    .line 2101735
    invoke-virtual {p0}, LX/EIU;->k()V

    .line 2101736
    invoke-virtual {p0}, LX/EIU;->a()V

    goto :goto_0
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3559ca7b    # -5446338.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2101737
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onWindowFocusChanged(Z)V

    .line 2101738
    if-eqz p1, :cond_0

    .line 2101739
    invoke-static {p0}, LX/EIU;->N(LX/EIU;)V

    .line 2101740
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x51fd8606

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2101741
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->e()V

    .line 2101742
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 2101743
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->f()V

    .line 2101744
    return-void
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 2101745
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->h()V

    .line 2101746
    return-void
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 2101747
    invoke-static {p0}, LX/EIU;->P(LX/EIU;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2101748
    sget-object v0, LX/ED5;->EMPTY:LX/ED5;

    invoke-static {p0, v0}, LX/EIU;->a$redex0(LX/EIU;LX/ED5;)V

    .line 2101749
    const/4 v0, 0x1

    .line 2101750
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()V
    .locals 2

    .prologue
    .line 2101751
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101752
    :cond_0
    :goto_0
    return-void

    .line 2101753
    :cond_1
    invoke-static {p0}, LX/EIU;->X(LX/EIU;)V

    .line 2101754
    invoke-static {p0}, LX/EIU;->P(LX/EIU;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2101755
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 2101756
    :cond_2
    :goto_1
    invoke-static {p0}, LX/EIU;->aa(LX/EIU;)V

    .line 2101757
    invoke-static {p0}, LX/EIU;->N(LX/EIU;)V

    goto :goto_0

    .line 2101758
    :cond_3
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    iget-object v1, p0, LX/EIU;->ag:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcActionBar;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2101759
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcActionBar;->setVisibility(I)V

    .line 2101760
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->g()V

    goto :goto_1
.end method

.method public final w()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 2101761
    iget-boolean v0, p0, LX/EIU;->am:Z

    if-nez v0, :cond_1

    .line 2101762
    :cond_0
    :goto_0
    return-void

    .line 2101763
    :cond_1
    iget-object v0, p0, LX/EIU;->T:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2101764
    iget-object v0, p0, LX/EIU;->T:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2101765
    iget-object v0, p0, LX/EIU;->U:LX/EIX;

    if-eqz v0, :cond_2

    .line 2101766
    iget-object v0, p0, LX/EIU;->U:LX/EIX;

    .line 2101767
    iput-object v2, v0, LX/EIX;->c:LX/EIJ;

    .line 2101768
    iget-object v0, p0, LX/EIU;->U:LX/EIX;

    invoke-virtual {v0}, LX/EIX;->a()V

    .line 2101769
    iput-object v2, p0, LX/EIU;->U:LX/EIX;

    .line 2101770
    :cond_2
    iget-object v0, p0, LX/EIU;->F:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2101771
    iget-object v0, p0, LX/EIU;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2101772
    iget-object v0, p0, LX/EIU;->B:Landroid/view/View;

    invoke-virtual {p0}, LX/EIU;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02177d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2101773
    invoke-virtual {p0, v3}, LX/EIU;->a(I)V

    .line 2101774
    invoke-static {p0}, LX/EIU;->H(LX/EIU;)V

    .line 2101775
    iput-boolean v3, p0, LX/EIU;->am:Z

    .line 2101776
    iget-object v0, p0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0, v3}, Lcom/facebook/rtc/views/RtcActionBar;->setRosterButtonVisibility(I)V

    .line 2101777
    iget-object v0, p0, LX/EIU;->an:LX/EIT;

    sget-object v1, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    if-ne v0, v1, :cond_0

    .line 2101778
    iget-object v0, p0, LX/EIU;->R:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 2101779
    const/16 v0, 0x1388

    invoke-static {p0, v0}, LX/EIU;->e(LX/EIU;I)V

    .line 2101780
    return-void
.end method

.method public final z()V
    .locals 3

    .prologue
    .line 2101781
    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bj()LX/0Px;

    move-result-object v1

    .line 2101782
    invoke-static {p0}, LX/EIU;->getConferenceCall(LX/EIU;)LX/EFy;

    move-result-object v2

    .line 2101783
    if-eqz v2, :cond_0

    iget-object v0, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2101784
    :cond_0
    sget-object v0, LX/EIU;->k:Ljava/lang/String;

    const-string v1, "Unable to updateVideoParticipants"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2101785
    :cond_1
    :goto_0
    return-void

    .line 2101786
    :cond_2
    invoke-static {p0, v1, v2}, LX/EIU;->a(LX/EIU;LX/0Px;LX/EFy;)V

    .line 2101787
    iget-object v0, p0, LX/EIU;->N:LX/ED6;

    if-eqz v0, :cond_3

    .line 2101788
    iget-object v0, p0, LX/EIU;->N:LX/ED6;

    invoke-virtual {v0}, LX/ED6;->d()V

    .line 2101789
    :cond_3
    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101790
    iget-object v0, p0, LX/EIU;->aa:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/GroupRingNoticeView;

    iget-object v1, p0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->bo()LX/EGp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/GroupRingNoticeView;->setModeAndShow(LX/EGp;)V

    goto :goto_0
.end method
