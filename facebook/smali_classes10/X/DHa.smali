.class public final LX/DHa;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DHb;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/DGm;

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/DHb;


# direct methods
.method public constructor <init>(LX/DHb;)V
    .locals 1

    .prologue
    .line 1982155
    iput-object p1, p0, LX/DHa;->c:LX/DHb;

    .line 1982156
    move-object v0, p1

    .line 1982157
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1982158
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1982140
    const-string v0, "StorySetPageHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1982141
    if-ne p0, p1, :cond_1

    .line 1982142
    :cond_0
    :goto_0
    return v0

    .line 1982143
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1982144
    goto :goto_0

    .line 1982145
    :cond_3
    check-cast p1, LX/DHa;

    .line 1982146
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1982147
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1982148
    if-eq v2, v3, :cond_0

    .line 1982149
    iget-object v2, p0, LX/DHa;->a:LX/DGm;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DHa;->a:LX/DGm;

    iget-object v3, p1, LX/DHa;->a:LX/DGm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1982150
    goto :goto_0

    .line 1982151
    :cond_5
    iget-object v2, p1, LX/DHa;->a:LX/DGm;

    if-nez v2, :cond_4

    .line 1982152
    :cond_6
    iget-object v2, p0, LX/DHa;->b:LX/1Pb;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/DHa;->b:LX/1Pb;

    iget-object v3, p1, LX/DHa;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1982153
    goto :goto_0

    .line 1982154
    :cond_7
    iget-object v2, p1, LX/DHa;->b:LX/1Pb;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
