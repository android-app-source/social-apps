.class public final LX/DCd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/wifiscan/WifiScanResult;",
        "LX/4Gr;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Long;

.field public final synthetic b:LX/34k;


# direct methods
.method public constructor <init>(LX/34k;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1974265
    iput-object p1, p0, LX/DCd;->b:LX/34k;

    iput-object p2, p0, LX/DCd;->a:Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1974266
    check-cast p1, Lcom/facebook/wifiscan/WifiScanResult;

    .line 1974267
    if-nez p1, :cond_0

    .line 1974268
    const/4 v0, 0x0

    .line 1974269
    :goto_0
    return-object v0

    .line 1974270
    :cond_0
    new-instance v0, LX/4Gr;

    invoke-direct {v0}, LX/4Gr;-><init>()V

    .line 1974271
    iget-object v1, p1, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1974272
    iget-object v1, p1, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4Gr;->a(Ljava/lang/String;)LX/4Gr;

    .line 1974273
    :cond_1
    iget-object v1, p1, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1974274
    iget-object v1, p1, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4Gr;->b(Ljava/lang/String;)LX/4Gr;

    .line 1974275
    :cond_2
    iget-object v1, p1, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1974276
    iget-object v1, p1, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/4Gr;->b(Ljava/lang/Integer;)LX/4Gr;

    .line 1974277
    :cond_3
    iget v1, p1, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Gr;->a(Ljava/lang/Integer;)LX/4Gr;

    .line 1974278
    iget-object v1, p0, LX/DCd;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {p1, v2, v3}, LX/2zX;->a(Lcom/facebook/wifiscan/WifiScanResult;J)J

    move-result-wide v2

    long-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Gr;->a(Ljava/lang/Double;)LX/4Gr;

    goto :goto_0
.end method
