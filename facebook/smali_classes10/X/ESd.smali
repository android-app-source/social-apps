.class public final LX/ESd;
.super Landroid/database/DataSetObserver;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V
    .locals 0

    .prologue
    .line 2123207
    iput-object p1, p0, LX/ESd;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 6

    .prologue
    .line 2123208
    iget-object v0, p0, LX/ESd;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-boolean v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->u:Z

    if-nez v0, :cond_0

    .line 2123209
    iget-object v0, p0, LX/ESd;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, p0, LX/ESd;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->x:Landroid/widget/GridView;

    const/4 p0, 0x1

    const/4 v5, 0x0

    .line 2123210
    const/4 v2, 0x2

    new-array v3, v2, [I

    .line 2123211
    invoke-virtual {v1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2123212
    iget-object v2, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->y:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2123213
    aget v4, v3, v5

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2123214
    aget v3, v3, p0

    add-int/lit8 v3, v3, 0x1e

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2123215
    iget-object v3, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->y:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2123216
    iget-object v2, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->y:Landroid/widget/RelativeLayout;

    new-instance v3, LX/ESW;

    invoke-direct {v3, v0}, LX/ESW;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2123217
    iget-object v2, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->y:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 2123218
    iget-object v2, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->y:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2123219
    iput-boolean p0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->u:Z

    .line 2123220
    :cond_0
    return-void
.end method
