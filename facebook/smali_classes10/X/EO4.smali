.class public final LX/EO4;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/EO4;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EO2;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/EO5;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2113880
    const/4 v0, 0x0

    sput-object v0, LX/EO4;->a:LX/EO4;

    .line 2113881
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EO4;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2113882
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2113883
    new-instance v0, LX/EO5;

    invoke-direct {v0}, LX/EO5;-><init>()V

    iput-object v0, p0, LX/EO4;->c:LX/EO5;

    .line 2113884
    return-void
.end method

.method public static declared-synchronized q()LX/EO4;
    .locals 2

    .prologue
    .line 2113885
    const-class v1, LX/EO4;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/EO4;->a:LX/EO4;

    if-nez v0, :cond_0

    .line 2113886
    new-instance v0, LX/EO4;

    invoke-direct {v0}, LX/EO4;-><init>()V

    sput-object v0, LX/EO4;->a:LX/EO4;

    .line 2113887
    :cond_0
    sget-object v0, LX/EO4;->a:LX/EO4;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2113888
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2113889
    check-cast p2, LX/EO3;

    .line 2113890
    iget v0, p2, LX/EO3;->a:I

    iget-object v1, p2, LX/EO3;->b:LX/0Px;

    iget-object v2, p2, LX/EO3;->c:LX/ENw;

    const/4 v5, 0x0

    .line 2113891
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x7

    const v6, 0x7f0b0060

    invoke-interface {v3, v4, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, v5, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    move v4, v5

    .line 2113892
    :goto_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_2

    .line 2113893
    const/4 v3, 0x0

    .line 2113894
    new-instance v7, LX/ENz;

    invoke-direct {v7}, LX/ENz;-><init>()V

    .line 2113895
    sget-object p0, LX/EO0;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/ENy;

    .line 2113896
    if-nez p0, :cond_0

    .line 2113897
    new-instance p0, LX/ENy;

    invoke-direct {p0}, LX/ENy;-><init>()V

    .line 2113898
    :cond_0
    invoke-static {p0, p1, v3, v3, v7}, LX/ENy;->a$redex0(LX/ENy;LX/1De;IILX/ENz;)V

    .line 2113899
    move-object v7, p0

    .line 2113900
    move-object v3, v7

    .line 2113901
    move-object v7, v3

    .line 2113902
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2113903
    iget-object p0, v7, LX/ENy;->a:LX/ENz;

    iput-object v3, p0, LX/ENz;->a:Ljava/lang/String;

    .line 2113904
    iget-object p0, v7, LX/ENy;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2113905
    move-object v7, v7

    .line 2113906
    if-ne v4, v0, :cond_1

    const/4 v3, 0x1

    .line 2113907
    :goto_1
    iget-object p0, v7, LX/ENy;->a:LX/ENz;

    iput-boolean v3, p0, LX/ENz;->b:Z

    .line 2113908
    iget-object p0, v7, LX/ENy;->d:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2113909
    move-object v3, v7

    .line 2113910
    new-instance v7, LX/ENv;

    invoke-direct {v7, v2, v4}, LX/ENv;-><init>(LX/ENw;I)V

    move-object v7, v7

    .line 2113911
    move-object v7, v7

    .line 2113912
    iget-object p0, v3, LX/ENy;->a:LX/ENz;

    iput-object v7, p0, LX/ENz;->c:LX/ENv;

    .line 2113913
    iget-object p0, v3, LX/ENy;->d:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2113914
    move-object v3, v3

    .line 2113915
    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2113916
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_1
    move v3, v5

    .line 2113917
    goto :goto_1

    .line 2113918
    :cond_2
    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2113919
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2113920
    invoke-static {}, LX/1dS;->b()V

    .line 2113921
    const/4 v0, 0x0

    return-object v0
.end method
