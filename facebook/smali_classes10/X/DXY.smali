.class public LX/DXY;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DXW;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DXZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2010375
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DXY;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DXZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2010415
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2010416
    iput-object p1, p0, LX/DXY;->b:LX/0Ot;

    .line 2010417
    return-void
.end method

.method public static a(LX/0QB;)LX/DXY;
    .locals 4

    .prologue
    .line 2010404
    const-class v1, LX/DXY;

    monitor-enter v1

    .line 2010405
    :try_start_0
    sget-object v0, LX/DXY;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2010406
    sput-object v2, LX/DXY;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2010407
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2010408
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2010409
    new-instance v3, LX/DXY;

    const/16 p0, 0x2485

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DXY;-><init>(LX/0Ot;)V

    .line 2010410
    move-object v0, v3

    .line 2010411
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2010412
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DXY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2010413
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2010414
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2010418
    const v0, -0x2c2d0500

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2010394
    check-cast p2, LX/DXX;

    .line 2010395
    iget-object v0, p0, LX/DXY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DXZ;

    iget-object v1, p2, LX/DXX;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010396
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->k()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2010397
    :cond_0
    const/4 v2, 0x0

    .line 2010398
    :goto_0
    move-object v0, v2

    .line 2010399
    return-object v0

    .line 2010400
    :cond_1
    iget-object v2, v0, LX/DXZ;->a:LX/1vg;

    invoke-virtual {v2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v2

    const p0, 0x7f0208fd

    invoke-virtual {v2, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v2

    const p0, -0xb4b0aa

    invoke-virtual {v2, p0}, LX/2xv;->i(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 2010401
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    const p2, 0x7f0207fb

    invoke-interface {p0, p2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object p0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2, v2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {p0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    const p2, 0x7f08309b

    invoke-virtual {p0, p2}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0051

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0a00f7

    invoke-virtual {p0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    sget-object p2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-interface {p0, p2}, LX/1Di;->b(F)LX/1Di;

    move-result-object p0

    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2010402
    const p0, -0x2c2d0500

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2010403
    invoke-interface {v2, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2010376
    invoke-static {}, LX/1dS;->b()V

    .line 2010377
    iget v0, p1, LX/1dQ;->b:I

    .line 2010378
    packed-switch v0, :pswitch_data_0

    .line 2010379
    :goto_0
    return-object v2

    .line 2010380
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2010381
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2010382
    check-cast v1, LX/DXX;

    .line 2010383
    iget-object p1, p0, LX/DXY;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DXZ;

    iget-object p2, v1, LX/DXX;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010384
    iget-object p0, p1, LX/DXZ;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DXP;

    .line 2010385
    new-instance v1, LX/4Fy;

    invoke-direct {v1}, LX/4Fy;-><init>()V

    .line 2010386
    iget-object p1, p0, LX/DXP;->e:Ljava/lang/String;

    .line 2010387
    const-string v0, "group_id"

    invoke-virtual {v1, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2010388
    new-instance p1, LX/DXs;

    invoke-direct {p1}, LX/DXs;-><init>()V

    move-object p1, p1

    .line 2010389
    const-string v0, "data"

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2010390
    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2010391
    iget-object p1, p0, LX/DXP;->f:LX/0TF;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2010392
    iget-object p1, p0, LX/DXP;->a:LX/0Sh;

    iget-object v0, p0, LX/DXP;->b:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v0, LX/DXO;

    invoke-direct {v0, p0, p2}, LX/DXO;-><init>(LX/DXP;Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;)V

    invoke-virtual {p1, v1, v0}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2010393
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2c2d0500
        :pswitch_0
    .end packed-switch
.end method
