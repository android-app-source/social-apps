.class public final LX/D2h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final r:Ljava/lang/Object;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/8LV;

.field public final c:LX/1EZ;

.field public final d:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final e:LX/03V;

.field public final f:LX/D2c;

.field public final g:LX/D2T;

.field public final h:LX/0SG;

.field public final i:LX/0TD;

.field public final j:LX/0TD;

.field public final k:LX/1Er;

.field public final l:LX/BQP;

.field private final m:LX/0ad;

.field public final n:LX/2MV;

.field public final o:LX/7TG;

.field public final p:LX/7St;

.field public final q:LX/7SE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1959070
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/D2h;->r:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/8LV;LX/1EZ;Lcom/facebook/auth/viewercontext/ViewerContext;LX/03V;LX/D2c;LX/D2T;LX/0SG;LX/0TD;LX/0TD;LX/1Er;LX/BQP;LX/2MV;LX/0ad;LX/2MM;LX/7TG;LX/7St;LX/7SE;)V
    .locals 1
    .param p9    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p10    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1959073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1959074
    iput-object p1, p0, LX/D2h;->a:Landroid/content/Context;

    .line 1959075
    iput-object p2, p0, LX/D2h;->b:LX/8LV;

    .line 1959076
    iput-object p3, p0, LX/D2h;->c:LX/1EZ;

    .line 1959077
    iput-object p4, p0, LX/D2h;->d:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1959078
    iput-object p5, p0, LX/D2h;->e:LX/03V;

    .line 1959079
    iput-object p6, p0, LX/D2h;->f:LX/D2c;

    .line 1959080
    iput-object p7, p0, LX/D2h;->g:LX/D2T;

    .line 1959081
    iput-object p8, p0, LX/D2h;->h:LX/0SG;

    .line 1959082
    iput-object p9, p0, LX/D2h;->i:LX/0TD;

    .line 1959083
    iput-object p10, p0, LX/D2h;->j:LX/0TD;

    .line 1959084
    iput-object p11, p0, LX/D2h;->k:LX/1Er;

    .line 1959085
    iput-object p12, p0, LX/D2h;->l:LX/BQP;

    .line 1959086
    iput-object p14, p0, LX/D2h;->m:LX/0ad;

    .line 1959087
    iput-object p13, p0, LX/D2h;->n:LX/2MV;

    .line 1959088
    move-object/from16 v0, p16

    iput-object v0, p0, LX/D2h;->o:LX/7TG;

    .line 1959089
    move-object/from16 v0, p17

    iput-object v0, p0, LX/D2h;->p:LX/7St;

    .line 1959090
    move-object/from16 v0, p18

    iput-object v0, p0, LX/D2h;->q:LX/7SE;

    .line 1959091
    return-void
.end method

.method public static a(LX/0QB;)LX/D2h;
    .locals 7

    .prologue
    .line 1959092
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1959093
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1959094
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1959095
    if-nez v1, :cond_0

    .line 1959096
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1959097
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1959098
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1959099
    sget-object v1, LX/D2h;->r:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1959100
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1959101
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1959102
    :cond_1
    if-nez v1, :cond_4

    .line 1959103
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1959104
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1959105
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/D2h;->b(LX/0QB;)LX/D2h;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 1959106
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1959107
    if-nez v1, :cond_2

    .line 1959108
    sget-object v0, LX/D2h;->r:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D2h;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1959109
    :goto_1
    if-eqz v0, :cond_3

    .line 1959110
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1959111
    :goto_3
    check-cast v0, LX/D2h;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1959112
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1959113
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1959114
    :catchall_1
    move-exception v0

    .line 1959115
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1959116
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1959117
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1959118
    :cond_2
    :try_start_8
    sget-object v0, LX/D2h;->r:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D2h;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/D2h;
    .locals 20

    .prologue
    .line 1959071
    new-instance v1, LX/D2h;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/8LV;->a(LX/0QB;)LX/8LV;

    move-result-object v3

    check-cast v3, LX/8LV;

    invoke-static/range {p0 .. p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v4

    check-cast v4, LX/1EZ;

    invoke-static/range {p0 .. p0}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    const-class v7, LX/D2c;

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/D2c;

    invoke-static/range {p0 .. p0}, LX/D2T;->a(LX/0QB;)LX/D2T;

    move-result-object v8

    check-cast v8, LX/D2T;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v12

    check-cast v12, LX/1Er;

    invoke-static/range {p0 .. p0}, LX/BQP;->a(LX/0QB;)LX/BQP;

    move-result-object v13

    check-cast v13, LX/BQP;

    invoke-static/range {p0 .. p0}, LX/2MU;->a(LX/0QB;)LX/2MU;

    move-result-object v14

    check-cast v14, LX/2MV;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v16

    check-cast v16, LX/2MM;

    invoke-static/range {p0 .. p0}, LX/7TG;->a(LX/0QB;)LX/7TG;

    move-result-object v17

    check-cast v17, LX/7TG;

    invoke-static/range {p0 .. p0}, LX/7St;->a(LX/0QB;)LX/7St;

    move-result-object v18

    check-cast v18, LX/7St;

    invoke-static/range {p0 .. p0}, LX/7SE;->a(LX/0QB;)LX/7SE;

    move-result-object v19

    check-cast v19, LX/7SE;

    invoke-direct/range {v1 .. v19}, LX/D2h;-><init>(Landroid/content/Context;LX/8LV;LX/1EZ;Lcom/facebook/auth/viewercontext/ViewerContext;LX/03V;LX/D2c;LX/D2T;LX/0SG;LX/0TD;LX/0TD;LX/1Er;LX/BQP;LX/2MV;LX/0ad;LX/2MM;LX/7TG;LX/7St;LX/7SE;)V

    .line 1959072
    return-object v1
.end method
