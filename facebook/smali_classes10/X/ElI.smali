.class public LX/ElI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:I

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2164173
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 2164174
    const/4 v1, 0x0

    .line 2164175
    iget-boolean v0, p0, LX/ElI;->d:Z

    if-eqz v0, :cond_1

    .line 2164176
    new-instance v0, LX/ElK;

    iget v1, p0, LX/ElI;->a:I

    iget-object v2, p0, LX/ElI;->b:Ljava/lang/String;

    iget-object v3, p0, LX/ElI;->c:Ljava/lang/String;

    sget-object v4, LX/ElJ;->API_EC_DOMAIN:LX/ElJ;

    invoke-direct {v0, v1, v2, v3, v4}, LX/ElK;-><init>(ILjava/lang/String;Ljava/lang/String;LX/ElJ;)V

    .line 2164177
    :goto_0
    move-object v0, v0

    .line 2164178
    if-eqz v0, :cond_0

    .line 2164179
    throw v0

    .line 2164180
    :cond_0
    return-void

    .line 2164181
    :cond_1
    iget-boolean v0, p0, LX/ElI;->g:Z

    if-eqz v0, :cond_2

    .line 2164182
    new-instance v0, LX/ElK;

    iget-object v2, p0, LX/ElI;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, LX/ElI;->f:Ljava/lang/String;

    sget-object v4, LX/ElJ;->API_EC_DOMAIN:LX/ElJ;

    invoke-direct {v0, v2, v3, v1, v4}, LX/ElK;-><init>(ILjava/lang/String;Ljava/lang/String;LX/ElJ;)V

    goto :goto_0

    .line 2164183
    :cond_2
    iget-boolean v0, p0, LX/ElI;->h:Z

    if-eqz v0, :cond_5

    .line 2164184
    iget-boolean v0, p0, LX/ElI;->m:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/ElI;->l:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2164185
    new-instance v0, LX/ElK;

    iget v2, p0, LX/ElI;->i:I

    iget-object v3, p0, LX/ElI;->l:Ljava/lang/String;

    sget-object v4, LX/ElJ;->GRAPHQL_KERROR_DOMAIN:LX/ElJ;

    invoke-direct {v0, v2, v3, v1, v4}, LX/ElK;-><init>(ILjava/lang/String;Ljava/lang/String;LX/ElJ;)V

    goto :goto_0

    .line 2164186
    :cond_3
    iget-object v0, p0, LX/ElI;->j:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2164187
    new-instance v0, LX/ElK;

    iget v1, p0, LX/ElI;->i:I

    iget-object v2, p0, LX/ElI;->j:Ljava/lang/String;

    iget-object v3, p0, LX/ElI;->k:Ljava/lang/String;

    sget-object v4, LX/ElJ;->API_EC_DOMAIN:LX/ElJ;

    invoke-direct {v0, v1, v2, v3, v4}, LX/ElK;-><init>(ILjava/lang/String;Ljava/lang/String;LX/ElJ;)V

    goto :goto_0

    .line 2164188
    :cond_4
    new-instance v0, LX/ElK;

    const/4 v2, 0x1

    sget-object v3, LX/ElJ;->API_EC_DOMAIN:LX/ElJ;

    invoke-direct {v0, v2, v1, v1, v3}, LX/ElK;-><init>(ILjava/lang/String;Ljava/lang/String;LX/ElJ;)V

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 2164189
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2164190
    new-instance v0, LX/ElI;

    invoke-direct {v0}, LX/ElI;-><init>()V

    .line 2164191
    :try_start_0
    new-instance v1, Landroid/util/JsonReader;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 2164192
    invoke-virtual {v1}, Landroid/util/JsonReader;->beginObject()V

    .line 2164193
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2164194
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 2164195
    iget-boolean v3, v0, LX/ElI;->h:Z

    if-nez v3, :cond_2

    .line 2164196
    const/4 v3, 0x1

    .line 2164197
    const-string v4, "error_code"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2164198
    invoke-static {v1, v3}, LX/Ekh;->a(Landroid/util/JsonReader;I)I

    move-result v4

    iput v4, v0, LX/ElI;->a:I

    .line 2164199
    iput-boolean v3, v0, LX/ElI;->d:Z

    .line 2164200
    :goto_1
    move v3, v3

    .line 2164201
    :goto_2
    move v2, v3

    .line 2164202
    if-nez v2, :cond_0

    .line 2164203
    invoke-virtual {v1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 2164204
    :cond_1
    invoke-virtual {v1}, Landroid/util/JsonReader;->endObject()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2164205
    invoke-direct {v0}, LX/ElI;->a()V

    .line 2164206
    return-void

    .line 2164207
    :catch_0
    move-exception v0

    .line 2164208
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    const/4 v3, 0x1

    .line 2164209
    const-string v4, "code"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2164210
    invoke-static {v1, v3}, LX/Ekh;->a(Landroid/util/JsonReader;I)I

    move-result v4

    iput v4, v0, LX/ElI;->i:I

    .line 2164211
    iput-boolean v3, v0, LX/ElI;->m:Z

    .line 2164212
    :goto_3
    move v3, v3

    .line 2164213
    goto :goto_2

    .line 2164214
    :cond_3
    const-string v4, "error_msg"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2164215
    invoke-static {v1}, LX/Ekh;->a(Landroid/util/JsonReader;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/ElI;->b:Ljava/lang/String;

    goto :goto_1

    .line 2164216
    :cond_4
    const-string v4, "error_data"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2164217
    invoke-static {v1}, LX/Ekh;->a(Landroid/util/JsonReader;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/ElI;->c:Ljava/lang/String;

    goto :goto_1

    .line 2164218
    :cond_5
    const-string v4, "error"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2164219
    invoke-virtual {v1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object p0, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    if-eq v4, p0, :cond_6

    .line 2164220
    invoke-virtual {v1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    .line 2164221
    sget-object p0, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, p0, :cond_9

    .line 2164222
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextNull()V

    .line 2164223
    const/4 v4, 0x0

    .line 2164224
    :goto_4
    move-object v4, v4

    .line 2164225
    iput-object v4, v0, LX/ElI;->e:Ljava/lang/Integer;

    .line 2164226
    iput-boolean v3, v0, LX/ElI;->g:Z

    goto :goto_1

    .line 2164227
    :cond_6
    iput-boolean v3, v0, LX/ElI;->h:Z

    .line 2164228
    invoke-virtual {v1}, Landroid/util/JsonReader;->beginObject()V

    goto :goto_1

    .line 2164229
    :cond_7
    const-string v4, "error_description"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2164230
    invoke-static {v1}, LX/Ekh;->a(Landroid/util/JsonReader;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/ElI;->f:Ljava/lang/String;

    goto :goto_1

    .line 2164231
    :cond_8
    const/4 v3, 0x0

    goto :goto_1

    :cond_9
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_4

    .line 2164232
    :cond_a
    const-string v4, "description"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2164233
    invoke-static {v1}, LX/Ekh;->a(Landroid/util/JsonReader;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/ElI;->l:Ljava/lang/String;

    goto :goto_3

    .line 2164234
    :cond_b
    const-string v4, "message"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2164235
    invoke-static {v1}, LX/Ekh;->a(Landroid/util/JsonReader;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/ElI;->j:Ljava/lang/String;

    goto :goto_3

    .line 2164236
    :cond_c
    const-string v4, "error_data"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2164237
    invoke-static {v1}, LX/Ekh;->a(Landroid/util/JsonReader;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/ElI;->k:Ljava/lang/String;

    goto/16 :goto_3

    .line 2164238
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_3
.end method
