.class public LX/D3y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/2HB;

.field public c:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field public f:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public g:LX/1FJ;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1961445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961446
    iput-object p1, p0, LX/D3y;->a:Landroid/content/Context;

    .line 1961447
    const/4 v3, 0x1

    .line 1961448
    new-instance v0, LX/2HB;

    invoke-direct {v0, p1}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 1961449
    const v1, 0x7f020343

    invoke-virtual {v0, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v1

    const-string v2, "video.playback.control.action.delete"

    invoke-static {p1, v2}, LX/D3x;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2HB;->b(Landroid/app/PendingIntent;)LX/2HB;

    move-result-object v1

    .line 1961450
    iput v3, v1, LX/2HB;->z:I

    .line 1961451
    move-object v1, v1

    .line 1961452
    const/4 v2, 0x2

    .line 1961453
    iput v2, v1, LX/2HB;->j:I

    .line 1961454
    move-object v0, v0

    .line 1961455
    iput-object v0, p0, LX/D3y;->b:LX/2HB;

    .line 1961456
    return-void
.end method

.method public static b(LX/0QB;)LX/D3y;
    .locals 2

    .prologue
    .line 1961441
    new-instance v1, LX/D3y;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/D3y;-><init>(Landroid/content/Context;)V

    .line 1961442
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    .line 1961443
    iput-object v0, v1, LX/D3y;->c:LX/0SG;

    .line 1961444
    return-object v1
.end method


# virtual methods
.method public final a(LX/1FJ;)LX/D3y;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "LX/D3y;"
        }
    .end annotation

    .prologue
    .line 1961437
    iget-object v0, p0, LX/D3y;->g:LX/1FJ;

    if-eqz v0, :cond_0

    .line 1961438
    iget-object v0, p0, LX/D3y;->g:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1961439
    :cond_0
    iput-object p1, p0, LX/D3y;->g:LX/1FJ;

    .line 1961440
    return-object p0
.end method
