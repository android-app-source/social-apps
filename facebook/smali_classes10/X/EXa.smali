.class public final enum LX/EXa;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/EXH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EXa;",
        ">;",
        "LX/EXH;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EXa;

.field public static final enum CODE_SIZE:LX/EXa;

.field public static final enum LITE_RUNTIME:LX/EXa;

.field public static final enum SPEED:LX/EXa;

.field private static final VALUES:[LX/EXa;

.field private static internalValueMap:LX/EXE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EXE",
            "<",
            "LX/EXa;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2134477
    new-instance v0, LX/EXa;

    const-string v1, "SPEED"

    invoke-direct {v0, v1, v4, v4, v2}, LX/EXa;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXa;->SPEED:LX/EXa;

    .line 2134478
    new-instance v0, LX/EXa;

    const-string v1, "CODE_SIZE"

    invoke-direct {v0, v1, v2, v2, v3}, LX/EXa;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXa;->CODE_SIZE:LX/EXa;

    .line 2134479
    new-instance v0, LX/EXa;

    const-string v1, "LITE_RUNTIME"

    invoke-direct {v0, v1, v3, v3, v5}, LX/EXa;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXa;->LITE_RUNTIME:LX/EXa;

    .line 2134480
    new-array v0, v5, [LX/EXa;

    sget-object v1, LX/EXa;->SPEED:LX/EXa;

    aput-object v1, v0, v4

    sget-object v1, LX/EXa;->CODE_SIZE:LX/EXa;

    aput-object v1, v0, v2

    sget-object v1, LX/EXa;->LITE_RUNTIME:LX/EXa;

    aput-object v1, v0, v3

    sput-object v0, LX/EXa;->$VALUES:[LX/EXa;

    .line 2134481
    new-instance v0, LX/EXZ;

    invoke-direct {v0}, LX/EXZ;-><init>()V

    sput-object v0, LX/EXa;->internalValueMap:LX/EXE;

    .line 2134482
    invoke-static {}, LX/EXa;->values()[LX/EXa;

    move-result-object v0

    sput-object v0, LX/EXa;->VALUES:[LX/EXa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2134473
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2134474
    iput p3, p0, LX/EXa;->index:I

    .line 2134475
    iput p4, p0, LX/EXa;->value:I

    .line 2134476
    return-void
.end method

.method public static final getDescriptor()LX/EYL;
    .locals 2

    .prologue
    .line 2134472
    sget-object v0, LX/EYC;->s:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->g()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYL;

    return-object v0
.end method

.method public static internalGetValueMap()LX/EXE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EXE",
            "<",
            "LX/EXa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2134471
    sget-object v0, LX/EXa;->internalValueMap:LX/EXE;

    return-object v0
.end method

.method public static valueOf(I)LX/EXa;
    .locals 1

    .prologue
    .line 2134466
    packed-switch p0, :pswitch_data_0

    .line 2134467
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2134468
    :pswitch_0
    sget-object v0, LX/EXa;->SPEED:LX/EXa;

    goto :goto_0

    .line 2134469
    :pswitch_1
    sget-object v0, LX/EXa;->CODE_SIZE:LX/EXa;

    goto :goto_0

    .line 2134470
    :pswitch_2
    sget-object v0, LX/EXa;->LITE_RUNTIME:LX/EXa;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(LX/EYM;)LX/EXa;
    .locals 2

    .prologue
    .line 2134483
    iget-object v0, p0, LX/EYM;->e:LX/EYL;

    move-object v0, v0

    .line 2134484
    invoke-static {}, LX/EXa;->getDescriptor()LX/EYL;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2134485
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EnumValueDescriptor is not for this type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2134486
    :cond_0
    sget-object v0, LX/EXa;->VALUES:[LX/EXa;

    .line 2134487
    iget v1, p0, LX/EYM;->a:I

    move v1, v1

    .line 2134488
    aget-object v0, v0, v1

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/EXa;
    .locals 1

    .prologue
    .line 2134465
    const-class v0, LX/EXa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EXa;

    return-object v0
.end method

.method public static values()[LX/EXa;
    .locals 1

    .prologue
    .line 2134464
    sget-object v0, LX/EXa;->$VALUES:[LX/EXa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EXa;

    return-object v0
.end method


# virtual methods
.method public final getDescriptorForType()LX/EYL;
    .locals 1

    .prologue
    .line 2134463
    invoke-static {}, LX/EXa;->getDescriptor()LX/EYL;

    move-result-object v0

    return-object v0
.end method

.method public final getNumber()I
    .locals 1

    .prologue
    .line 2134462
    iget v0, p0, LX/EXa;->value:I

    return v0
.end method

.method public final getValueDescriptor()LX/EYM;
    .locals 2

    .prologue
    .line 2134461
    invoke-static {}, LX/EXa;->getDescriptor()LX/EYL;

    move-result-object v0

    invoke-virtual {v0}, LX/EYL;->d()Ljava/util/List;

    move-result-object v0

    iget v1, p0, LX/EXa;->index:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYM;

    return-object v0
.end method
