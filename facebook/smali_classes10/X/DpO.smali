.class public LX/DpO;
.super LX/6kT;
.source ""


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;

.field private static final n:LX/1sw;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/16 v3, 0xc

    .line 2045272
    const/4 v0, 0x1

    sput-boolean v0, LX/DpO;->a:Z

    .line 2045273
    new-instance v0, LX/1sv;

    const-string v1, "PacketBody"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpO;->b:LX/1sv;

    .line 2045274
    new-instance v0, LX/1sw;

    const-string v1, "caller_id_payload"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->c:LX/1sw;

    .line 2045275
    new-instance v0, LX/1sw;

    const-string v1, "status_sender_nonce"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->d:LX/1sw;

    .line 2045276
    new-instance v0, LX/1sw;

    const-string v1, "status_payload"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->e:LX/1sw;

    .line 2045277
    new-instance v0, LX/1sw;

    const-string v1, "receipt_payload"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->f:LX/1sw;

    .line 2045278
    new-instance v0, LX/1sw;

    const-string v1, "ping_payload"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->g:LX/1sw;

    .line 2045279
    new-instance v0, LX/1sw;

    const-string v1, "register_payload"

    invoke-direct {v0, v1, v3, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->h:LX/1sw;

    .line 2045280
    new-instance v0, LX/1sw;

    const-string v1, "lookup_payload"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->i:LX/1sw;

    .line 2045281
    new-instance v0, LX/1sw;

    const-string v1, "primary_device_change_payload"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->j:LX/1sw;

    .line 2045282
    new-instance v0, LX/1sw;

    const-string v1, "prekey_upload_payload"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->k:LX/1sw;

    .line 2045283
    new-instance v0, LX/1sw;

    const-string v1, "salamander_payload"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->l:LX/1sw;

    .line 2045284
    new-instance v0, LX/1sw;

    const-string v1, "create_thread_payload"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->m:LX/1sw;

    .line 2045285
    new-instance v0, LX/1sw;

    const-string v1, "batch_lookup_payload"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpO;->n:LX/1sw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2045286
    invoke-direct {p0}, LX/6kT;-><init>()V

    .line 2045287
    return-void
.end method

.method public static a(LX/DpV;)LX/DpO;
    .locals 1

    .prologue
    .line 2045288
    new-instance v0, LX/DpO;

    invoke-direct {v0}, LX/DpO;-><init>()V

    .line 2045289
    invoke-direct {v0, p0}, LX/DpO;->b(LX/DpV;)V

    .line 2045290
    return-object v0
.end method

.method public static a(LX/DpW;)LX/DpO;
    .locals 1

    .prologue
    .line 2045291
    new-instance v0, LX/DpO;

    invoke-direct {v0}, LX/DpO;-><init>()V

    .line 2045292
    invoke-static {v0, p0}, LX/DpO;->b(LX/DpO;LX/DpW;)V

    .line 2045293
    return-object v0
.end method

.method public static a(LX/Dpa;)LX/DpO;
    .locals 1

    .prologue
    .line 2045294
    new-instance v0, LX/DpO;

    invoke-direct {v0}, LX/DpO;-><init>()V

    .line 2045295
    invoke-direct {v0, p0}, LX/DpO;->b(LX/Dpa;)V

    .line 2045296
    return-object v0
.end method

.method public static b(LX/DpO;LX/DpB;)V
    .locals 1

    .prologue
    .line 2045297
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2045298
    :cond_0
    const/16 v0, 0x15

    iput v0, p0, LX/DpO;->setField_:I

    .line 2045299
    iput-object p1, p0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2045300
    return-void
.end method

.method public static b(LX/DpO;LX/DpG;)V
    .locals 1

    .prologue
    .line 2045301
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2045302
    :cond_0
    const/16 v0, 0x14

    iput v0, p0, LX/DpO;->setField_:I

    .line 2045303
    iput-object p1, p0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2045304
    return-void
.end method

.method public static b(LX/DpO;LX/DpJ;)V
    .locals 1

    .prologue
    .line 2045305
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2045306
    :cond_0
    const/16 v0, 0xe

    iput v0, p0, LX/DpO;->setField_:I

    .line 2045307
    iput-object p1, p0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2045308
    return-void
.end method

.method public static b(LX/DpO;LX/DpS;)V
    .locals 1

    .prologue
    .line 2045309
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2045310
    :cond_0
    const/16 v0, 0x12

    iput v0, p0, LX/DpO;->setField_:I

    .line 2045311
    iput-object p1, p0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2045312
    return-void
.end method

.method private static b(LX/DpO;LX/DpW;)V
    .locals 1

    .prologue
    .line 2045313
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2045314
    :cond_0
    const/16 v0, 0xc

    iput v0, p0, LX/DpO;->setField_:I

    .line 2045315
    iput-object p1, p0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2045316
    return-void
.end method

.method public static b(LX/DpO;[B)V
    .locals 1

    .prologue
    .line 2045317
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2045318
    :cond_0
    const/16 v0, 0x8

    iput v0, p0, LX/DpO;->setField_:I

    .line 2045319
    iput-object p1, p0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2045320
    return-void
.end method

.method private b(LX/DpV;)V
    .locals 1

    .prologue
    .line 2045321
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2045322
    :cond_0
    const/16 v0, 0xa

    iput v0, p0, LX/DpO;->setField_:I

    .line 2045323
    iput-object p1, p0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2045324
    return-void
.end method

.method private b(LX/Dpa;)V
    .locals 1

    .prologue
    .line 2045325
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2045326
    :cond_0
    const/16 v0, 0x13

    iput v0, p0, LX/DpO;->setField_:I

    .line 2045327
    iput-object p1, p0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2045328
    return-void
.end method

.method private h()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2045329
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045330
    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 2045331
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045332
    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 2045333
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'status_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045334
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2045335
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private i()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2045336
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045337
    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 2045338
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045339
    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 2045340
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'ping_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045341
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2045342
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private j()LX/DpW;
    .locals 3

    .prologue
    .line 2045258
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045259
    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    .line 2045260
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045261
    check-cast v0, LX/DpW;

    return-object v0

    .line 2045262
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'register_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045263
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2045264
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private k()LX/DpJ;
    .locals 3

    .prologue
    .line 2045265
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045266
    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    .line 2045267
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045268
    check-cast v0, LX/DpJ;

    return-object v0

    .line 2045269
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'lookup_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045270
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2045271
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private l()LX/DpS;
    .locals 3

    .prologue
    .line 2044819
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2044820
    const/16 v1, 0x12

    if-ne v0, v1, :cond_0

    .line 2044821
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2044822
    check-cast v0, LX/DpS;

    return-object v0

    .line 2044823
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'prekey_upload_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2044824
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2044825
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private m()LX/DpG;
    .locals 3

    .prologue
    .line 2044826
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2044827
    const/16 v1, 0x14

    if-ne v0, v1, :cond_0

    .line 2044828
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2044829
    check-cast v0, LX/DpG;

    return-object v0

    .line 2044830
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'create_thread_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2044831
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2044832
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private n()LX/DpB;
    .locals 3

    .prologue
    .line 2044833
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2044834
    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 2044835
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2044836
    check-cast v0, LX/DpB;

    return-object v0

    .line 2044837
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'batch_lookup_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2044838
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2044839
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(LX/1su;LX/1sw;)Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 2044840
    iget-short v1, p2, LX/1sw;->c:S

    packed-switch v1, :pswitch_data_0

    .line 2044841
    :pswitch_0
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    .line 2044842
    :goto_0
    return-object v0

    .line 2044843
    :pswitch_1
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->c:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_4

    .line 2044844
    const/16 v9, 0xb

    const/4 v3, 0x0

    .line 2044845
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    move-object v4, v3

    move-object v5, v3

    .line 2044846
    :goto_1
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v6

    .line 2044847
    iget-byte v7, v6, LX/1sw;->b:B

    if-eqz v7, :cond_3

    .line 2044848
    iget-short v7, v6, LX/1sw;->c:S

    packed-switch v7, :pswitch_data_1

    .line 2044849
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p1, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2044850
    :pswitch_2
    iget-byte v7, v6, LX/1sw;->b:B

    const/16 v8, 0xa

    if-ne v7, v8, :cond_0

    .line 2044851
    invoke-virtual {p1}, LX/1su;->n()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_1

    .line 2044852
    :cond_0
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p1, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2044853
    :pswitch_3
    iget-byte v7, v6, LX/1sw;->b:B

    if-ne v7, v9, :cond_1

    .line 2044854
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 2044855
    :cond_1
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p1, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2044856
    :pswitch_4
    iget-byte v7, v6, LX/1sw;->b:B

    if-ne v7, v9, :cond_2

    .line 2044857
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2044858
    :cond_2
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {p1, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2044859
    :cond_3
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2044860
    new-instance v6, LX/DpE;

    invoke-direct {v6, v5, v4, v3}, LX/DpE;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 2044861
    move-object v0, v6

    .line 2044862
    goto :goto_0

    .line 2044863
    :cond_4
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044864
    :pswitch_5
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->d:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_5

    .line 2044865
    invoke-virtual {p1}, LX/1su;->q()[B

    move-result-object v0

    goto :goto_0

    .line 2044866
    :cond_5
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044867
    :pswitch_6
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->e:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_6

    .line 2044868
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2044869
    :cond_6
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044870
    :pswitch_7
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->f:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_9

    .line 2044871
    const/4 v3, 0x0

    .line 2044872
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    .line 2044873
    :goto_2
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v4

    .line 2044874
    iget-byte v5, v4, LX/1sw;->b:B

    if-eqz v5, :cond_8

    .line 2044875
    iget-short v5, v4, LX/1sw;->c:S

    packed-switch v5, :pswitch_data_2

    .line 2044876
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p1, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 2044877
    :pswitch_8
    iget-byte v5, v4, LX/1sw;->b:B

    const/16 v6, 0xa

    if-ne v5, v6, :cond_7

    .line 2044878
    invoke-virtual {p1}, LX/1su;->n()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_2

    .line 2044879
    :cond_7
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p1, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 2044880
    :cond_8
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2044881
    new-instance v4, LX/DpV;

    invoke-direct {v4, v3}, LX/DpV;-><init>(Ljava/lang/Long;)V

    .line 2044882
    move-object v0, v4

    .line 2044883
    goto/16 :goto_0

    .line 2044884
    :cond_9
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044885
    :pswitch_9
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->g:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_a

    .line 2044886
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2044887
    :cond_a
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044888
    :pswitch_a
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->h:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_10

    .line 2044889
    const/16 v7, 0xc

    const/16 v6, 0xb

    const/4 v0, 0x0

    .line 2044890
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    move-object v2, v0

    move-object v3, v0

    .line 2044891
    :goto_3
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v4

    .line 2044892
    iget-byte v5, v4, LX/1sw;->b:B

    if-eqz v5, :cond_f

    .line 2044893
    iget-short v5, v4, LX/1sw;->c:S

    packed-switch v5, :pswitch_data_3

    .line 2044894
    :pswitch_b
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p1, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_3

    .line 2044895
    :pswitch_c
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v6, :cond_b

    .line 2044896
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 2044897
    :cond_b
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p1, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_3

    .line 2044898
    :pswitch_d
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v6, :cond_c

    .line 2044899
    invoke-virtual {p1}, LX/1su;->q()[B

    move-result-object v2

    goto :goto_3

    .line 2044900
    :cond_c
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p1, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_3

    .line 2044901
    :pswitch_e
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v7, :cond_d

    .line 2044902
    invoke-static {p1}, LX/Dpf;->b(LX/1su;)LX/Dpf;

    move-result-object v1

    goto :goto_3

    .line 2044903
    :cond_d
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p1, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_3

    .line 2044904
    :pswitch_f
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v7, :cond_e

    .line 2044905
    invoke-static {p1}, LX/DpU;->b(LX/1su;)LX/DpU;

    move-result-object v0

    goto :goto_3

    .line 2044906
    :cond_e
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p1, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_3

    .line 2044907
    :cond_f
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2044908
    new-instance v4, LX/DpW;

    invoke-direct {v4, v3, v2, v1, v0}, LX/DpW;-><init>(Ljava/lang/String;[BLX/Dpf;LX/DpU;)V

    .line 2044909
    move-object v0, v4

    .line 2044910
    goto/16 :goto_0

    .line 2044911
    :cond_10
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044912
    :pswitch_10
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->i:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_13

    .line 2044913
    const/4 v3, 0x0

    .line 2044914
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    .line 2044915
    :goto_4
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v4

    .line 2044916
    iget-byte v5, v4, LX/1sw;->b:B

    if-eqz v5, :cond_12

    .line 2044917
    iget-short v5, v4, LX/1sw;->c:S

    packed-switch v5, :pswitch_data_4

    .line 2044918
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p1, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_4

    .line 2044919
    :pswitch_11
    iget-byte v5, v4, LX/1sw;->b:B

    const/16 v6, 0xa

    if-ne v5, v6, :cond_11

    .line 2044920
    invoke-virtual {p1}, LX/1su;->n()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_4

    .line 2044921
    :cond_11
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p1, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_4

    .line 2044922
    :cond_12
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2044923
    new-instance v4, LX/DpJ;

    invoke-direct {v4, v3}, LX/DpJ;-><init>(Ljava/lang/Long;)V

    .line 2044924
    move-object v0, v4

    .line 2044925
    goto/16 :goto_0

    .line 2044926
    :cond_13
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044927
    :pswitch_12
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->j:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_17

    .line 2044928
    const/4 v0, 0x0

    .line 2044929
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    .line 2044930
    :goto_5
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2044931
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_16

    .line 2044932
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_5

    .line 2044933
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p1, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_5

    .line 2044934
    :pswitch_13
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xc

    if-ne v3, v4, :cond_14

    .line 2044935
    invoke-static {p1}, LX/DpM;->b(LX/1su;)LX/DpM;

    move-result-object v1

    goto :goto_5

    .line 2044936
    :cond_14
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p1, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_5

    .line 2044937
    :pswitch_14
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xb

    if-ne v3, v4, :cond_15

    .line 2044938
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 2044939
    :cond_15
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p1, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_5

    .line 2044940
    :cond_16
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2044941
    new-instance v2, LX/DpT;

    invoke-direct {v2, v1, v0}, LX/DpT;-><init>(LX/DpM;Ljava/lang/String;)V

    .line 2044942
    move-object v0, v2

    .line 2044943
    goto/16 :goto_0

    .line 2044944
    :cond_17
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044945
    :pswitch_15
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->k:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_1d

    .line 2044946
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 2044947
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    .line 2044948
    :cond_18
    :goto_6
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2044949
    iget-byte v4, v2, LX/1sw;->b:B

    if-eqz v4, :cond_1c

    .line 2044950
    iget-short v4, v2, LX/1sw;->c:S

    packed-switch v4, :pswitch_data_6

    .line 2044951
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p1, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_6

    .line 2044952
    :pswitch_16
    iget-byte v4, v2, LX/1sw;->b:B

    const/16 v5, 0xf

    if-ne v4, v5, :cond_1a

    .line 2044953
    invoke-virtual {p1}, LX/1su;->h()LX/1u3;

    move-result-object v4

    .line 2044954
    new-instance v1, Ljava/util/ArrayList;

    iget v2, v4, LX/1u3;->b:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v3

    .line 2044955
    :goto_7
    iget v5, v4, LX/1u3;->b:I

    if-gez v5, :cond_19

    invoke-static {}, LX/1su;->t()Z

    move-result v5

    if-eqz v5, :cond_18

    .line 2044956
    :goto_8
    invoke-static {p1}, LX/DpU;->b(LX/1su;)LX/DpU;

    move-result-object v5

    .line 2044957
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2044958
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 2044959
    :cond_19
    iget v5, v4, LX/1u3;->b:I

    if-ge v2, v5, :cond_18

    goto :goto_8

    .line 2044960
    :cond_1a
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p1, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_6

    .line 2044961
    :pswitch_17
    iget-byte v4, v2, LX/1sw;->b:B

    const/16 v5, 0xc

    if-ne v4, v5, :cond_1b

    .line 2044962
    invoke-static {p1}, LX/Dpf;->b(LX/1su;)LX/Dpf;

    move-result-object v0

    goto :goto_6

    .line 2044963
    :cond_1b
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p1, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_6

    .line 2044964
    :cond_1c
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2044965
    new-instance v2, LX/DpS;

    invoke-direct {v2, v1, v0}, LX/DpS;-><init>(Ljava/util/List;LX/Dpf;)V

    .line 2044966
    move-object v0, v2

    .line 2044967
    goto/16 :goto_0

    .line 2044968
    :cond_1d
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044969
    :pswitch_18
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->l:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_1e

    .line 2044970
    invoke-static {p1}, LX/Dpa;->b(LX/1su;)LX/Dpa;

    move-result-object v0

    goto/16 :goto_0

    .line 2044971
    :cond_1e
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044972
    :pswitch_19
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->m:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_24

    .line 2044973
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 2044974
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    move-object v4, v3

    .line 2044975
    :cond_1f
    :goto_9
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v5

    .line 2044976
    iget-byte v7, v5, LX/1sw;->b:B

    if-eqz v7, :cond_23

    .line 2044977
    iget-short v7, v5, LX/1sw;->c:S

    packed-switch v7, :pswitch_data_7

    .line 2044978
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_9

    .line 2044979
    :pswitch_1a
    iget-byte v7, v5, LX/1sw;->b:B

    const/16 v8, 0xf

    if-ne v7, v8, :cond_21

    .line 2044980
    invoke-virtual {p1}, LX/1su;->h()LX/1u3;

    move-result-object v7

    .line 2044981
    new-instance v4, Ljava/util/ArrayList;

    iget v5, v7, LX/1u3;->b:I

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v5, v6

    .line 2044982
    :goto_a
    iget v8, v7, LX/1u3;->b:I

    if-gez v8, :cond_20

    invoke-static {}, LX/1su;->t()Z

    move-result v8

    if-eqz v8, :cond_1f

    .line 2044983
    :goto_b
    invoke-virtual {p1}, LX/1su;->n()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 2044984
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2044985
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    .line 2044986
    :cond_20
    iget v8, v7, LX/1u3;->b:I

    if-ge v5, v8, :cond_1f

    goto :goto_b

    .line 2044987
    :cond_21
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_9

    .line 2044988
    :pswitch_1b
    iget-byte v7, v5, LX/1sw;->b:B

    const/4 v8, 0x2

    if-ne v7, v8, :cond_22

    .line 2044989
    invoke-virtual {p1}, LX/1su;->j()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_9

    .line 2044990
    :cond_22
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_9

    .line 2044991
    :cond_23
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2044992
    new-instance v5, LX/DpG;

    invoke-direct {v5, v4, v3}, LX/DpG;-><init>(Ljava/util/List;Ljava/lang/Boolean;)V

    .line 2044993
    move-object v0, v5

    .line 2044994
    goto/16 :goto_0

    .line 2044995
    :cond_24
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044996
    :pswitch_1c
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/DpO;->n:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_2a

    .line 2044997
    const/4 v1, 0x0

    .line 2044998
    const/4 v0, 0x0

    .line 2044999
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    .line 2045000
    :goto_c
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2045001
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_29

    .line 2045002
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_8

    .line 2045003
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p1, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_c

    .line 2045004
    :pswitch_1d
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xf

    if-ne v3, v4, :cond_28

    .line 2045005
    invoke-virtual {p1}, LX/1su;->h()LX/1u3;

    move-result-object v3

    .line 2045006
    new-instance v2, Ljava/util/ArrayList;

    iget v0, v3, LX/1u3;->b:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 2045007
    :goto_d
    iget v4, v3, LX/1u3;->b:I

    if-gez v4, :cond_26

    invoke-static {}, LX/1su;->t()Z

    move-result v4

    if-eqz v4, :cond_27

    .line 2045008
    :cond_25
    invoke-static {p1}, LX/DpM;->b(LX/1su;)LX/DpM;

    move-result-object v4

    .line 2045009
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2045010
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 2045011
    :cond_26
    iget v4, v3, LX/1u3;->b:I

    if-lt v0, v4, :cond_25

    :cond_27
    move-object v0, v2

    .line 2045012
    goto :goto_c

    .line 2045013
    :cond_28
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p1, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_c

    .line 2045014
    :cond_29
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2045015
    new-instance v1, LX/DpB;

    invoke-direct {v1, v0}, LX/DpB;-><init>(Ljava/util/List;)V

    .line 2045016
    move-object v0, v1

    .line 2045017
    goto/16 :goto_0

    .line 2045018
    :cond_2a
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_10
        :pswitch_0
        :pswitch_12
        :pswitch_0
        :pswitch_15
        :pswitch_18
        :pswitch_19
        :pswitch_1c
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_8
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3
        :pswitch_c
        :pswitch_d
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_e
        :pswitch_b
        :pswitch_f
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x3
        :pswitch_11
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x2
        :pswitch_13
        :pswitch_14
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x3
        :pswitch_16
        :pswitch_17
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x2
        :pswitch_1a
        :pswitch_1b
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x2
        :pswitch_1d
    .end packed-switch
.end method

.method public final a(IZ)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2045019
    if-eqz p2, :cond_16

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2045020
    :goto_0
    if-eqz p2, :cond_17

    const-string v0, "\n"

    move-object v3, v0

    .line 2045021
    :goto_1
    if-eqz p2, :cond_18

    const-string v0, " "

    .line 2045022
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "PacketBody"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045023
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045024
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045025
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045026
    const/4 v1, 0x1

    .line 2045027
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045028
    const/4 v7, 0x6

    if-ne v6, v7, :cond_0

    .line 2045029
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045030
    const-string v1, "caller_id_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045031
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045032
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045033
    invoke-virtual {p0}, LX/DpO;->c()LX/DpE;

    move-result-object v1

    if-nez v1, :cond_19

    .line 2045034
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 2045035
    :cond_0
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045036
    const/16 v7, 0x8

    if-ne v6, v7, :cond_2

    .line 2045037
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045038
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045039
    const-string v1, "status_sender_nonce"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045040
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045041
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045042
    invoke-virtual {p0}, LX/DpO;->d()[B

    move-result-object v1

    if-nez v1, :cond_1a

    .line 2045043
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 2045044
    :cond_2
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045045
    const/16 v7, 0x9

    if-ne v6, v7, :cond_4

    .line 2045046
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045047
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045048
    const-string v1, "status_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045049
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045050
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045051
    invoke-direct {p0}, LX/DpO;->h()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1b

    .line 2045052
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 2045053
    :cond_4
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045054
    const/16 v7, 0xa

    if-ne v6, v7, :cond_6

    .line 2045055
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045056
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045057
    const-string v1, "receipt_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045058
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045059
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045060
    invoke-virtual {p0}, LX/DpO;->e()LX/DpV;

    move-result-object v1

    if-nez v1, :cond_1c

    .line 2045061
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v1, v2

    .line 2045062
    :cond_6
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045063
    const/16 v7, 0xb

    if-ne v6, v7, :cond_8

    .line 2045064
    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045065
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045066
    const-string v1, "ping_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045067
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045068
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045069
    invoke-direct {p0}, LX/DpO;->i()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1d

    .line 2045070
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    move v1, v2

    .line 2045071
    :cond_8
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045072
    const/16 v7, 0xc

    if-ne v6, v7, :cond_a

    .line 2045073
    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045074
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045075
    const-string v1, "register_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045076
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045077
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045078
    invoke-direct {p0}, LX/DpO;->j()LX/DpW;

    move-result-object v1

    if-nez v1, :cond_1e

    .line 2045079
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_8
    move v1, v2

    .line 2045080
    :cond_a
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045081
    const/16 v7, 0xe

    if-ne v6, v7, :cond_c

    .line 2045082
    if-nez v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045083
    :cond_b
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045084
    const-string v1, "lookup_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045085
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045086
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045087
    invoke-direct {p0}, LX/DpO;->k()LX/DpJ;

    move-result-object v1

    if-nez v1, :cond_1f

    .line 2045088
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_9
    move v1, v2

    .line 2045089
    :cond_c
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045090
    const/16 v7, 0x10

    if-ne v6, v7, :cond_e

    .line 2045091
    if-nez v1, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045092
    :cond_d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045093
    const-string v1, "primary_device_change_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045094
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045095
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045096
    invoke-virtual {p0}, LX/DpO;->f()LX/DpT;

    move-result-object v1

    if-nez v1, :cond_20

    .line 2045097
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_a
    move v1, v2

    .line 2045098
    :cond_e
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045099
    const/16 v7, 0x12

    if-ne v6, v7, :cond_10

    .line 2045100
    if-nez v1, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045101
    :cond_f
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045102
    const-string v1, "prekey_upload_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045103
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045104
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045105
    invoke-direct {p0}, LX/DpO;->l()LX/DpS;

    move-result-object v1

    if-nez v1, :cond_21

    .line 2045106
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_b
    move v1, v2

    .line 2045107
    :cond_10
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045108
    const/16 v7, 0x13

    if-ne v6, v7, :cond_12

    .line 2045109
    if-nez v1, :cond_11

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045110
    :cond_11
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045111
    const-string v1, "salamander_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045112
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045113
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045114
    invoke-virtual {p0}, LX/DpO;->g()LX/Dpa;

    move-result-object v1

    if-nez v1, :cond_22

    .line 2045115
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_c
    move v1, v2

    .line 2045116
    :cond_12
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2045117
    const/16 v7, 0x14

    if-ne v6, v7, :cond_25

    .line 2045118
    if-nez v1, :cond_13

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045119
    :cond_13
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045120
    const-string v1, "create_thread_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045121
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045122
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045123
    invoke-direct {p0}, LX/DpO;->m()LX/DpG;

    move-result-object v1

    if-nez v1, :cond_23

    .line 2045124
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045125
    :goto_d
    iget v1, p0, LX/6kT;->setField_:I

    move v1, v1

    .line 2045126
    const/16 v6, 0x15

    if-ne v1, v6, :cond_15

    .line 2045127
    if-nez v2, :cond_14

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045128
    :cond_14
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045129
    const-string v1, "batch_lookup_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045130
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045131
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045132
    invoke-direct {p0}, LX/DpO;->n()LX/DpB;

    move-result-object v0

    if-nez v0, :cond_24

    .line 2045133
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045134
    :cond_15
    :goto_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045135
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045136
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2045137
    :cond_16
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2045138
    :cond_17
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2045139
    :cond_18
    const-string v0, ""

    goto/16 :goto_2

    .line 2045140
    :cond_19
    invoke-virtual {p0}, LX/DpO;->c()LX/DpE;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2045141
    :cond_1a
    invoke-virtual {p0}, LX/DpO;->d()[B

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2045142
    :cond_1b
    invoke-direct {p0}, LX/DpO;->h()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2045143
    :cond_1c
    invoke-virtual {p0}, LX/DpO;->e()LX/DpV;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2045144
    :cond_1d
    invoke-direct {p0}, LX/DpO;->i()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2045145
    :cond_1e
    invoke-direct {p0}, LX/DpO;->j()LX/DpW;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 2045146
    :cond_1f
    invoke-direct {p0}, LX/DpO;->k()LX/DpJ;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 2045147
    :cond_20
    invoke-virtual {p0}, LX/DpO;->f()LX/DpT;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 2045148
    :cond_21
    invoke-direct {p0}, LX/DpO;->l()LX/DpS;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 2045149
    :cond_22
    invoke-virtual {p0}, LX/DpO;->g()LX/Dpa;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 2045150
    :cond_23
    invoke-direct {p0}, LX/DpO;->m()LX/DpG;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    .line 2045151
    :cond_24
    invoke-direct {p0}, LX/DpO;->n()LX/DpB;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e

    :cond_25
    move v2, v1

    goto/16 :goto_d
.end method

.method public final a(LX/1su;S)V
    .locals 3

    .prologue
    .line 2045152
    packed-switch p2, :pswitch_data_0

    .line 2045153
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot write union with unknown field "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2045154
    :pswitch_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045155
    check-cast v0, LX/DpE;

    .line 2045156
    invoke-virtual {v0, p1}, LX/DpE;->a(LX/1su;)V

    .line 2045157
    :goto_0
    return-void

    .line 2045158
    :pswitch_2
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045159
    check-cast v0, [B

    check-cast v0, [B

    .line 2045160
    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    goto :goto_0

    .line 2045161
    :pswitch_3
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045162
    check-cast v0, Ljava/lang/String;

    .line 2045163
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2045164
    :pswitch_4
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045165
    check-cast v0, LX/DpV;

    .line 2045166
    invoke-virtual {v0, p1}, LX/DpV;->a(LX/1su;)V

    goto :goto_0

    .line 2045167
    :pswitch_5
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045168
    check-cast v0, Ljava/lang/String;

    .line 2045169
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2045170
    :pswitch_6
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045171
    check-cast v0, LX/DpW;

    .line 2045172
    invoke-virtual {v0, p1}, LX/DpW;->a(LX/1su;)V

    goto :goto_0

    .line 2045173
    :pswitch_7
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045174
    check-cast v0, LX/DpJ;

    .line 2045175
    invoke-virtual {v0, p1}, LX/DpJ;->a(LX/1su;)V

    goto :goto_0

    .line 2045176
    :pswitch_8
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045177
    check-cast v0, LX/DpT;

    .line 2045178
    invoke-virtual {v0, p1}, LX/DpT;->a(LX/1su;)V

    goto :goto_0

    .line 2045179
    :pswitch_9
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045180
    check-cast v0, LX/DpS;

    .line 2045181
    invoke-virtual {v0, p1}, LX/DpS;->a(LX/1su;)V

    goto :goto_0

    .line 2045182
    :pswitch_a
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045183
    check-cast v0, LX/Dpa;

    .line 2045184
    invoke-virtual {v0, p1}, LX/Dpa;->a(LX/1su;)V

    goto :goto_0

    .line 2045185
    :pswitch_b
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045186
    check-cast v0, LX/DpG;

    .line 2045187
    invoke-virtual {v0, p1}, LX/DpG;->a(LX/1su;)V

    goto :goto_0

    .line 2045188
    :pswitch_c
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045189
    check-cast v0, LX/DpB;

    .line 2045190
    invoke-virtual {v0, p1}, LX/DpB;->a(LX/1su;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public final a(LX/DpO;)Z
    .locals 2

    .prologue
    .line 2045191
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045192
    iget v1, p1, LX/6kT;->setField_:I

    move v1, v1

    .line 2045193
    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_1

    .line 2045194
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045195
    check-cast v0, [B

    check-cast v0, [B

    .line 2045196
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 2045197
    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2045198
    :cond_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045199
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 2045200
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/1sw;
    .locals 3

    .prologue
    .line 2045201
    packed-switch p1, :pswitch_data_0

    .line 2045202
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown field id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2045203
    :pswitch_1
    sget-object v0, LX/DpO;->c:LX/1sw;

    .line 2045204
    :goto_0
    return-object v0

    .line 2045205
    :pswitch_2
    sget-object v0, LX/DpO;->d:LX/1sw;

    goto :goto_0

    .line 2045206
    :pswitch_3
    sget-object v0, LX/DpO;->e:LX/1sw;

    goto :goto_0

    .line 2045207
    :pswitch_4
    sget-object v0, LX/DpO;->f:LX/1sw;

    goto :goto_0

    .line 2045208
    :pswitch_5
    sget-object v0, LX/DpO;->g:LX/1sw;

    goto :goto_0

    .line 2045209
    :pswitch_6
    sget-object v0, LX/DpO;->h:LX/1sw;

    goto :goto_0

    .line 2045210
    :pswitch_7
    sget-object v0, LX/DpO;->i:LX/1sw;

    goto :goto_0

    .line 2045211
    :pswitch_8
    sget-object v0, LX/DpO;->j:LX/1sw;

    goto :goto_0

    .line 2045212
    :pswitch_9
    sget-object v0, LX/DpO;->k:LX/1sw;

    goto :goto_0

    .line 2045213
    :pswitch_a
    sget-object v0, LX/DpO;->l:LX/1sw;

    goto :goto_0

    .line 2045214
    :pswitch_b
    sget-object v0, LX/DpO;->m:LX/1sw;

    goto :goto_0

    .line 2045215
    :pswitch_c
    sget-object v0, LX/DpO;->n:LX/1sw;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public final c()LX/DpE;
    .locals 3

    .prologue
    .line 2045216
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045217
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 2045218
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045219
    check-cast v0, LX/DpE;

    return-object v0

    .line 2045220
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'caller_id_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045221
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2045222
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()[B
    .locals 3

    .prologue
    .line 2045223
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045224
    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 2045225
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045226
    check-cast v0, [B

    check-cast v0, [B

    return-object v0

    .line 2045227
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'status_sender_nonce\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045228
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2045229
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final e()LX/DpV;
    .locals 3

    .prologue
    .line 2045230
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045231
    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 2045232
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045233
    check-cast v0, LX/DpV;

    return-object v0

    .line 2045234
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'receipt_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045235
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2045236
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2045237
    instance-of v0, p1, LX/DpO;

    if-eqz v0, :cond_0

    .line 2045238
    check-cast p1, LX/DpO;

    invoke-virtual {p0, p1}, LX/DpO;->a(LX/DpO;)Z

    move-result v0

    .line 2045239
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()LX/DpT;
    .locals 3

    .prologue
    .line 2045240
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045241
    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 2045242
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045243
    check-cast v0, LX/DpT;

    return-object v0

    .line 2045244
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'primary_device_change_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045245
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2045246
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g()LX/Dpa;
    .locals 3

    .prologue
    .line 2045247
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2045248
    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    .line 2045249
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2045250
    check-cast v0, LX/Dpa;

    return-object v0

    .line 2045251
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'salamander_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045252
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2045253
    invoke-virtual {p0, v2}, LX/DpO;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2045254
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2045255
    sget-boolean v0, LX/DpO;->a:Z

    .line 2045256
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpO;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2045257
    return-object v0
.end method
