.class public LX/DFn;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1978764
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DFn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1978765
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1978766
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/DFn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V

    .line 1978767
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V
    .locals 3

    .prologue
    .line 1978768
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1978769
    const v0, 0x7f0310b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1978770
    const v0, 0x7f0d27ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1978771
    const v1, 0x7f0310b4

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1978772
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1978773
    const v0, 0x7f0d27af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1978774
    const v1, 0x7f081869

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1978775
    invoke-virtual {p0}, LX/DFn;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081869

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1978776
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1978777
    iget-boolean v0, p0, LX/DFn;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x36cbfc46

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978778
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1978779
    const/4 v1, 0x1

    .line 1978780
    iput-boolean v1, p0, LX/DFn;->a:Z

    .line 1978781
    const/16 v1, 0x2d

    const v2, 0x2a0d238a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6c68fbf2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978782
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1978783
    const/4 v1, 0x0

    .line 1978784
    iput-boolean v1, p0, LX/DFn;->a:Z

    .line 1978785
    const/16 v1, 0x2d

    const v2, 0x6f9385c2    # 9.1312E28f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
