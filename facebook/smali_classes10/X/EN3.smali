.class public LX/EN3;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EN1;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EN4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2111867
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EN3;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EN4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111864
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2111865
    iput-object p1, p0, LX/EN3;->b:LX/0Ot;

    .line 2111866
    return-void
.end method

.method public static a(LX/0QB;)LX/EN3;
    .locals 4

    .prologue
    .line 2111853
    const-class v1, LX/EN3;

    monitor-enter v1

    .line 2111854
    :try_start_0
    sget-object v0, LX/EN3;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111855
    sput-object v2, LX/EN3;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111856
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111857
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111858
    new-instance v3, LX/EN3;

    const/16 p0, 0x3430

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EN3;-><init>(LX/0Ot;)V

    .line 2111859
    move-object v0, v3

    .line 2111860
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111861
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EN3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111862
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111863
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2111845
    check-cast p2, LX/EN2;

    .line 2111846
    iget-object v0, p0, LX/EN3;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/EN2;->a:Ljava/lang/String;

    iget-object v1, p2, LX/EN2;->b:LX/1dc;

    iget v2, p2, LX/EN2;->c:I

    iget-object v3, p2, LX/EN2;->d:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 2111847
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a00d5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-interface {v4, v6}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v4

    .line 2111848
    const v6, -0x54525790

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 2111849
    invoke-interface {v4, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    if-nez v1, :cond_0

    move-object v4, v5

    :goto_0
    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1, p0, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v6, v7}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    if-nez v3, :cond_1

    :goto_1
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2111850
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v7, 0x5

    const v8, 0x7f0b16c1

    invoke-interface {v4, v7, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    goto :goto_0

    :cond_1
    const v5, 0x7f0e0125

    invoke-static {p1, p0, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    .line 2111851
    const v6, -0x5452525b

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 2111852
    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2111819
    invoke-static {}, LX/1dS;->b()V

    .line 2111820
    iget v0, p1, LX/1dQ;->b:I

    .line 2111821
    sparse-switch v0, :sswitch_data_0

    .line 2111822
    :goto_0
    return-object v2

    .line 2111823
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2111824
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2111825
    check-cast v1, LX/EN2;

    .line 2111826
    iget-object p1, p0, LX/EN3;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/EN2;->e:Landroid/view/View$OnClickListener;

    .line 2111827
    if-eqz p1, :cond_0

    .line 2111828
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2111829
    :cond_0
    goto :goto_0

    .line 2111830
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2111831
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2111832
    check-cast v1, LX/EN2;

    .line 2111833
    iget-object p1, p0, LX/EN3;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/EN2;->f:Landroid/view/View$OnClickListener;

    .line 2111834
    if-eqz p1, :cond_1

    .line 2111835
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2111836
    :cond_1
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x54525790 -> :sswitch_0
        -0x5452525b -> :sswitch_1
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/EN1;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2111837
    new-instance v1, LX/EN2;

    invoke-direct {v1, p0}, LX/EN2;-><init>(LX/EN3;)V

    .line 2111838
    sget-object v2, LX/EN3;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EN1;

    .line 2111839
    if-nez v2, :cond_0

    .line 2111840
    new-instance v2, LX/EN1;

    invoke-direct {v2}, LX/EN1;-><init>()V

    .line 2111841
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EN1;->a$redex0(LX/EN1;LX/1De;IILX/EN2;)V

    .line 2111842
    move-object v1, v2

    .line 2111843
    move-object v0, v1

    .line 2111844
    return-object v0
.end method
