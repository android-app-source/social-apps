.class public LX/D4T;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1962220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Collection;Landroid/animation/ValueAnimator;FFI)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/animation/ValueAnimator;",
            "FFI)V"
        }
    .end annotation

    .prologue
    .line 1962221
    int-to-long v0, p4

    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1962222
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1962223
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1962224
    new-instance v0, LX/D4S;

    invoke-direct {v0, p0}, LX/D4S;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1962225
    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p2, v0, v1

    const/4 v1, 0x1

    aput p3, v0, v1

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 1962226
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    .line 1962227
    return-void

    .line 1962228
    :cond_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p2

    .line 1962229
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;Landroid/animation/ValueAnimator;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/animation/ValueAnimator;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1962230
    const/high16 v0, 0x3f800000    # 1.0f

    const v1, 0x3da3d70a    # 0.08f

    invoke-static {p0, p1, v0, v1, p2}, LX/D4T;->a(Ljava/util/Collection;Landroid/animation/ValueAnimator;FFI)V

    .line 1962231
    return-void
.end method
