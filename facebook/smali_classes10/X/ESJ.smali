.class public final LX/ESJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/ERu;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSettingsActivity;)V
    .locals 0

    .prologue
    .line 2122806
    iput-object p1, p0, LX/ESJ;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/ERu;)V
    .locals 6

    .prologue
    .line 2122807
    iget-wide v0, p1, LX/ERu;->a:J

    iget-object v2, p0, LX/ESJ;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    invoke-static {v0, v1, v2}, Lcom/facebook/vault/ui/VaultSettingsActivity;->b(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2122808
    iget-wide v2, p1, LX/ERu;->b:J

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    iget-wide v4, p1, LX/ERu;->a:J

    div-long/2addr v2, v4

    long-to-float v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 2122809
    iget-object v2, p0, LX/ESJ;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    const v3, 0x7f0811ba

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/facebook/vault/ui/VaultSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2122810
    iget-object v2, p0, LX/ESJ;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultSettingsActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2122811
    iget-object v0, p0, LX/ESJ;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSettingsActivity;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2122812
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2122813
    sget-object v0, Lcom/facebook/vault/ui/VaultSettingsActivity;->x:Ljava/lang/Class;

    const-string v1, "Failed to retrieve quota information: "

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2122814
    iget-object v0, p0, LX/ESJ;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSettingsActivity;->B:Landroid/widget/TextView;

    iget-object v1, p0, LX/ESJ;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    const v2, 0x7f0811bb

    invoke-virtual {v1, v2}, Lcom/facebook/vault/ui/VaultSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2122815
    iget-object v0, p0, LX/ESJ;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSettingsActivity;->C:Landroid/widget/TextView;

    iget-object v1, p0, LX/ESJ;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    const v2, 0x7f0811bb

    invoke-virtual {v1, v2}, Lcom/facebook/vault/ui/VaultSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2122816
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2122817
    check-cast p1, LX/ERu;

    invoke-direct {p0, p1}, LX/ESJ;->a(LX/ERu;)V

    return-void
.end method
