.class public LX/EZ7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EYd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MType:",
        "LX/EWp;",
        "BType:",
        "LX/EWj;",
        "IType::",
        "LX/EWT;",
        ">",
        "Ljava/lang/Object;",
        "LX/EYd;"
    }
.end annotation


# instance fields
.field private a:LX/EYd;

.field public b:LX/EWj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TBType;"
        }
    .end annotation
.end field

.field public c:LX/EWp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMType;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>(LX/EWp;LX/EYd;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMType;",
            "LX/EYd;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2138712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138713
    if-nez p1, :cond_0

    .line 2138714
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2138715
    :cond_0
    iput-object p1, p0, LX/EZ7;->c:LX/EWp;

    .line 2138716
    iput-object p2, p0, LX/EZ7;->a:LX/EYd;

    .line 2138717
    iput-boolean p3, p0, LX/EZ7;->d:Z

    .line 2138718
    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 2138706
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    if-eqz v0, :cond_0

    .line 2138707
    const/4 v0, 0x0

    iput-object v0, p0, LX/EZ7;->c:LX/EWp;

    .line 2138708
    :cond_0
    iget-boolean v0, p0, LX/EZ7;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EZ7;->a:LX/EYd;

    if-eqz v0, :cond_1

    .line 2138709
    iget-object v0, p0, LX/EZ7;->a:LX/EYd;

    invoke-interface {v0}, LX/EYd;->a()V

    .line 2138710
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EZ7;->d:Z

    .line 2138711
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/EWp;)LX/EZ7;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMType;)",
            "LX/EZ7",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation

    .prologue
    .line 2138698
    if-nez p1, :cond_0

    .line 2138699
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2138700
    :cond_0
    iput-object p1, p0, LX/EZ7;->c:LX/EWp;

    .line 2138701
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    if-eqz v0, :cond_1

    .line 2138702
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    invoke-virtual {v0}, LX/EWj;->o()V

    .line 2138703
    const/4 v0, 0x0

    iput-object v0, p0, LX/EZ7;->b:LX/EWj;

    .line 2138704
    :cond_1
    invoke-direct {p0}, LX/EZ7;->h()V

    .line 2138705
    return-object p0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 2138696
    invoke-direct {p0}, LX/EZ7;->h()V

    .line 2138697
    return-void
.end method

.method public final b(LX/EWp;)LX/EZ7;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMType;)",
            "LX/EZ7",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation

    .prologue
    .line 2138691
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EZ7;->c:LX/EWp;

    iget-object v1, p0, LX/EZ7;->c:LX/EWp;

    invoke-virtual {v1}, LX/EWp;->v()LX/EWY;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 2138692
    iput-object p1, p0, LX/EZ7;->c:LX/EWp;

    .line 2138693
    :goto_0
    invoke-direct {p0}, LX/EZ7;->h()V

    .line 2138694
    return-object p0

    .line 2138695
    :cond_0
    invoke-virtual {p0}, LX/EZ7;->e()LX/EWj;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EWV;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2138672
    const/4 v0, 0x0

    iput-object v0, p0, LX/EZ7;->a:LX/EYd;

    .line 2138673
    return-void
.end method

.method public final c()LX/EWp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMType;"
        }
    .end annotation

    .prologue
    .line 2138688
    iget-object v0, p0, LX/EZ7;->c:LX/EWp;

    if-nez v0, :cond_0

    .line 2138689
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    invoke-virtual {v0}, LX/EWj;->h()LX/EWY;

    move-result-object v0

    check-cast v0, LX/EWp;

    iput-object v0, p0, LX/EZ7;->c:LX/EWp;

    .line 2138690
    :cond_0
    iget-object v0, p0, LX/EZ7;->c:LX/EWp;

    return-object v0
.end method

.method public final d()LX/EWp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMType;"
        }
    .end annotation

    .prologue
    .line 2138686
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EZ7;->d:Z

    .line 2138687
    invoke-virtual {p0}, LX/EZ7;->c()LX/EWp;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EWj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBType;"
        }
    .end annotation

    .prologue
    .line 2138681
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    if-nez v0, :cond_0

    .line 2138682
    iget-object v0, p0, LX/EZ7;->c:LX/EWp;

    invoke-virtual {v0, p0}, LX/EWp;->a(LX/EYd;)LX/EWU;

    move-result-object v0

    check-cast v0, LX/EWj;

    iput-object v0, p0, LX/EZ7;->b:LX/EWj;

    .line 2138683
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    iget-object v1, p0, LX/EZ7;->c:LX/EWp;

    invoke-virtual {v0, v1}, LX/EWV;->a(LX/EWY;)LX/EWV;

    .line 2138684
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    invoke-virtual {v0}, LX/EWj;->q()V

    .line 2138685
    :cond_0
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    return-object v0
.end method

.method public final g()LX/EZ7;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ7",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation

    .prologue
    .line 2138674
    iget-object v0, p0, LX/EZ7;->c:LX/EWp;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EZ7;->c:LX/EWp;

    invoke-virtual {v0}, LX/EWp;->v()LX/EWY;

    move-result-object v0

    :goto_0
    check-cast v0, LX/EWp;

    check-cast v0, LX/EWp;

    iput-object v0, p0, LX/EZ7;->c:LX/EWp;

    .line 2138675
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    if-eqz v0, :cond_0

    .line 2138676
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    invoke-virtual {v0}, LX/EWj;->o()V

    .line 2138677
    const/4 v0, 0x0

    iput-object v0, p0, LX/EZ7;->b:LX/EWj;

    .line 2138678
    :cond_0
    invoke-direct {p0}, LX/EZ7;->h()V

    .line 2138679
    return-object p0

    .line 2138680
    :cond_1
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    invoke-virtual {v0}, LX/EWj;->v()LX/EWY;

    move-result-object v0

    goto :goto_0
.end method
