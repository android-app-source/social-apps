.class public final LX/Ewu;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V
    .locals 0

    .prologue
    .line 2183819
    iput-object p1, p0, LX/Ewu;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2183820
    check-cast p1, LX/2f2;

    .line 2183821
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v0, :cond_0

    iget-boolean v0, p1, LX/2f2;->c:Z

    if-eqz v0, :cond_1

    .line 2183822
    :cond_0
    :goto_0
    return-void

    .line 2183823
    :cond_1
    iget-object v0, p0, LX/Ewu;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->n:Ljava/util/Map;

    iget-wide v2, p1, LX/2f2;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/83X;

    .line 2183824
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iget-object v2, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v1, v2, :cond_0

    .line 2183825
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-interface {v0, v1}, LX/2np;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2183826
    iget-object v0, p0, LX/Ewu;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    const v1, 0x7245f825

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method
