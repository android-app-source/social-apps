.class public LX/Ckz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0lB;

.field private final c:LX/0So;

.field private final d:LX/0W3;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;LX/0So;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1931820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931821
    iput-object p1, p0, LX/Ckz;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1931822
    iput-object p2, p0, LX/Ckz;->b:LX/0lB;

    .line 1931823
    iput-object p3, p0, LX/Ckz;->c:LX/0So;

    .line 1931824
    iput-object p4, p0, LX/Ckz;->d:LX/0W3;

    .line 1931825
    return-void
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1931826
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1931827
    iget-object v1, p0, LX/Ckz;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2fv;->c:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1931828
    iget-object v2, p0, LX/Ckz;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x1d4c0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1931829
    new-instance v1, Ljava/io/File;

    const-string v2, "instant_article_activity_json"

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1931830
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1931831
    new-instance v3, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;

    iget-object v4, p0, LX/Ckz;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2fv;->b:LX/0Tn;

    const-string v6, "no article debug info in shared preferences"

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;-><init>(Ljava/lang/String;)V

    .line 1931832
    iget-object v4, p0, LX/Ckz;->b:LX/0lB;

    invoke-virtual {v4, v1, v3}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Object;)V

    .line 1931833
    const-string v1, "instant_article_activity_json"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1931834
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1931835
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 1931836
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 1931837
    iget-object v0, p0, LX/Ckz;->d:LX/0W3;

    sget-wide v2, LX/0X5;->aS:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
