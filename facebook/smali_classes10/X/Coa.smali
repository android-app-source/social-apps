.class public LX/Coa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CoV;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bO;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/Chi;

.field private d:LX/Crz;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/Chi;LX/Crz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CoV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8bO;",
            ">;",
            "LX/Chi;",
            "LX/Crz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1935865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1935866
    iput-object p1, p0, LX/Coa;->a:LX/0Ot;

    .line 1935867
    iput-object p3, p0, LX/Coa;->c:LX/Chi;

    .line 1935868
    iput-object p2, p0, LX/Coa;->b:LX/0Ot;

    .line 1935869
    iput-object p4, p0, LX/Coa;->d:LX/Crz;

    .line 1935870
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)LX/CnN;
    .locals 6

    .prologue
    .line 1935864
    iget-object v0, p0, LX/Coa;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CoV;

    iget-object v2, p0, LX/Coa;->c:LX/Chi;

    const/16 v4, 0x843

    const/16 v5, 0x3ee

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/CoV;->a(Landroid/content/Context;LX/Chi;Ljava/lang/String;II)LX/CnN;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Coa;
    .locals 7

    .prologue
    .line 1935828
    const-class v1, LX/Coa;

    monitor-enter v1

    .line 1935829
    :try_start_0
    sget-object v0, LX/Coa;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1935830
    sput-object v2, LX/Coa;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1935831
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935832
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1935833
    new-instance v5, LX/Coa;

    const/16 v3, 0x3239

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x3233

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v3

    check-cast v3, LX/Chi;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v4

    check-cast v4, LX/Crz;

    invoke-direct {v5, v6, p0, v3, v4}, LX/Coa;-><init>(LX/0Ot;LX/0Ot;LX/Chi;LX/Crz;)V

    .line 1935834
    move-object v0, v5

    .line 1935835
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1935836
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Coa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1935837
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1935838
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1935859
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308c0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1935860
    packed-switch p2, :pswitch_data_0

    .line 1935861
    :goto_0
    return-void

    .line 1935862
    :pswitch_0
    invoke-virtual {p1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_0

    .line 1935863
    :pswitch_1
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/widget/CustomLinearLayout;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1935839
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    if-eqz p4, :cond_3

    if-eqz p5, :cond_3

    if-eqz p6, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1935840
    if-nez v0, :cond_1

    .line 1935841
    :cond_0
    :goto_1
    return-void

    .line 1935842
    :cond_1
    iget-object v0, p0, LX/Coa;->d:LX/Crz;

    invoke-static {p4, p3, v0}, LX/ClW;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;LX/Crz;)LX/ClW;

    move-result-object v1

    .line 1935843
    iget-object v0, p0, LX/Coa;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CoV;

    invoke-virtual {v0}, LX/CoV;->a()LX/CnR;

    move-result-object v2

    .line 1935844
    if-eqz v2, :cond_0

    .line 1935845
    invoke-virtual {v2, v1}, LX/CnR;->setAnnotation(LX/ClW;)V

    .line 1935846
    invoke-virtual {v2, v3}, LX/CnR;->setIsOverlay(Z)V

    .line 1935847
    if-nez p6, :cond_4

    .line 1935848
    const/4 v0, 0x0

    .line 1935849
    :goto_2
    move-object v0, v0

    .line 1935850
    invoke-virtual {v2, v0}, LX/CnR;->setFeedbackLoggingParams(LX/162;)V

    .line 1935851
    iget-object v0, p0, LX/Coa;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bO;

    .line 1935852
    iget-object v1, v0, LX/8bO;->a:LX/0ad;

    sget-short p3, LX/2yD;->h:S

    const/4 p4, 0x0

    invoke-interface {v1, p3, p4}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1935853
    if-eqz v0, :cond_2

    .line 1935854
    invoke-direct {p0, p1, p5}, LX/Coa;->a(Landroid/content/Context;Ljava/lang/String;)LX/CnN;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/CnR;->setComposerLaunchParams(LX/CnN;)V

    .line 1935855
    :goto_3
    invoke-virtual {p2, v2}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 1935856
    :cond_2
    invoke-virtual {v2, v3}, LX/CnR;->setShowShareButton(Z)V

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1935857
    :cond_4
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 1935858
    invoke-virtual {v0, p6}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_2
.end method
