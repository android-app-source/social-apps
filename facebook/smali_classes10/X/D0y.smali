.class public LX/D0y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1956170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1956171
    iput-object p1, p0, LX/D0y;->a:LX/0Ot;

    .line 1956172
    return-void
.end method

.method public static a(LX/D0y;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 1956164
    iget-object v0, p0, LX/D0y;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    move-object v0, v0

    .line 1956165
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1956166
    const-string v1, "store_locator"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1956167
    invoke-virtual {v0, p2}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 1956168
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1956169
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/D0y;
    .locals 2

    .prologue
    .line 1956162
    new-instance v0, LX/D0y;

    const/16 v1, 0xbc

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/D0y;-><init>(LX/0Ot;)V

    .line 1956163
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 1956160
    const-string v0, "canvas_store_locator_resize_up"

    invoke-static {p0, v0, p1}, LX/D0y;->a(LX/D0y;Ljava/lang/String;Ljava/util/Map;)V

    .line 1956161
    return-void
.end method

.method public final b(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 1956158
    const-string v0, "canvas_store_locator_resize_down"

    invoke-static {p0, v0, p1}, LX/D0y;->a(LX/D0y;Ljava/lang/String;Ljava/util/Map;)V

    .line 1956159
    return-void
.end method

.method public final c(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 1956156
    const-string v0, "canvas_store_locator_my_location"

    invoke-static {p0, v0, p1}, LX/D0y;->a(LX/D0y;Ljava/lang/String;Ljava/util/Map;)V

    .line 1956157
    return-void
.end method
