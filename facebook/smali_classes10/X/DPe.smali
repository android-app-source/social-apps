.class public final LX/DPe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

.field public final synthetic c:LX/DPo;


# direct methods
.method public constructor <init>(LX/DPo;ZLcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V
    .locals 0

    .prologue
    .line 1993648
    iput-object p1, p0, LX/DPe;->c:LX/DPo;

    iput-boolean p2, p0, LX/DPe;->a:Z

    iput-object p3, p0, LX/DPe;->b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1993649
    iget-boolean v0, p0, LX/DPe;->a:Z

    if-eqz v0, :cond_0

    .line 1993650
    iget-object v0, p0, LX/DPe;->c:LX/DPo;

    iget-object v0, v0, LX/DPo;->n:LX/3mF;

    iget-object v1, p0, LX/DPe;->b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GROUP"

    invoke-virtual {v0, v1, v2}, LX/3mF;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1993651
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DPe;->c:LX/DPo;

    iget-object v0, v0, LX/DPo;->n:LX/3mF;

    iget-object v1, p0, LX/DPe;->b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GROUP"

    invoke-virtual {v0, v1, v2}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
