.class public LX/Evy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2182670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2182671
    iput-object p1, p0, LX/Evy;->a:LX/0ad;

    .line 2182672
    return-void
.end method

.method public static a(LX/0QB;)LX/Evy;
    .locals 4

    .prologue
    .line 2182673
    const-class v1, LX/Evy;

    monitor-enter v1

    .line 2182674
    :try_start_0
    sget-object v0, LX/Evy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2182675
    sput-object v2, LX/Evy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2182676
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2182677
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2182678
    new-instance p0, LX/Evy;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/Evy;-><init>(LX/0ad;)V

    .line 2182679
    move-object v0, p0

    .line 2182680
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2182681
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Evy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2182682
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2182683
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
