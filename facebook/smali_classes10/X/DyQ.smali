.class public final LX/DyQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V
    .locals 0

    .prologue
    .line 2064450
    iput-object p1, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2064451
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->g:LX/0Ot;

    if-eqz v0, :cond_0

    .line 2064452
    new-instance v6, Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    sget-object v0, LX/3WA;->USER_INITIATED:LX/3WA;

    iget-object v1, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v6, v0, v1}, Lcom/facebook/photos/base/photos/PhotoFetchInfo;-><init>(LX/3WA;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2064453
    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2064454
    iget-boolean v1, v0, LX/Dxa;->a:Z

    move v0, v1

    .line 2064455
    if-eqz v0, :cond_1

    .line 2064456
    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v5, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    const/4 v7, 0x0

    move-wide v2, p4

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;Z)V

    .line 2064457
    :cond_0
    :goto_0
    return-void

    .line 2064458
    :cond_1
    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2064459
    iget-boolean v1, v0, LX/Dxa;->b:Z

    move v0, v1

    .line 2064460
    if-eqz v0, :cond_2

    .line 2064461
    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v5, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    const/4 v7, 0x1

    move-wide v2, p4

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;Z)V

    goto :goto_0

    .line 2064462
    :cond_2
    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxW;

    invoke-virtual {v0}, LX/DxW;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2064463
    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxW;

    .line 2064464
    invoke-virtual {v0}, LX/DxW;->b()Z

    move-result v2

    const-string v3, "Not in profile edit mode"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2064465
    iget-object v2, v0, LX/DxW;->g:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2064466
    iget-object v3, v2, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-object v2, v3

    .line 2064467
    move-object v4, v2

    .line 2064468
    iget-object v5, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    move-wide v2, p4

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;)V

    goto :goto_0

    .line 2064469
    :cond_3
    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxW;

    invoke-virtual {v0}, LX/DxW;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2064470
    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v3, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v1, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->f:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-wide v1, p4

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLcom/facebook/base/fragment/FbFragment;JLcom/facebook/photos/base/photos/PhotoFetchInfo;)V

    goto/16 :goto_0

    .line 2064471
    :cond_4
    iget-object v0, p0, LX/DyQ;->a:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    invoke-static {v0, p4, p5}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->a$redex0(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;J)V

    goto/16 :goto_0
.end method
