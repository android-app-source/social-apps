.class public LX/DSj;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/widget/text/BetterTextView;

.field public final b:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1999838
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DSj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1999839
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1999840
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1999841
    const v0, 0x7f030820

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1999842
    const v0, 0x7f0d1549

    invoke-virtual {p0, v0}, LX/DSj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DSj;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1999843
    const v0, 0x7f0d154a

    invoke-virtual {p0, v0}, LX/DSj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/DSj;->b:Landroid/widget/ProgressBar;

    .line 1999844
    return-void
.end method
