.class public LX/Efh;
.super LX/0gG;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/MinimalReplyThread;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field public final c:LX/EfA;

.field public final d:LX/AKu;

.field public final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/Efg;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/AL7;

.field private final g:Lcom/facebook/audience/model/ReplyThreadConfig;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/EfA;LX/AKu;LX/AL7;Lcom/facebook/audience/model/ReplyThreadConfig;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/MinimalReplyThread;",
            ">;",
            "Lcom/facebook/audience/direct/ui/BackstageReplyThreadView$ReplyThreadViewListener;",
            "LX/AKu;",
            "LX/AL7;",
            "Lcom/facebook/audience/model/ReplyThreadConfig;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2155129
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2155130
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Efh;->e:Landroid/util/SparseArray;

    .line 2155131
    const/4 v0, -0x1

    iput v0, p0, LX/Efh;->h:I

    .line 2155132
    iput-object p1, p0, LX/Efh;->b:Landroid/content/Context;

    .line 2155133
    iput-object p2, p0, LX/Efh;->a:LX/0Px;

    .line 2155134
    iput-object p3, p0, LX/Efh;->c:LX/EfA;

    .line 2155135
    iput-object p4, p0, LX/Efh;->d:LX/AKu;

    .line 2155136
    iput-object p5, p0, LX/Efh;->f:LX/AL7;

    .line 2155137
    iput-object p6, p0, LX/Efh;->g:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2155138
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 2155082
    new-instance v1, LX/EfL;

    iget-object v0, p0, LX/Efh;->b:Landroid/content/Context;

    invoke-direct {v1, v0}, LX/EfL;-><init>(Landroid/content/Context;)V

    .line 2155083
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, LX/EfL;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2155084
    iget-object v0, p0, LX/Efh;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/MinimalReplyThread;

    .line 2155085
    iget-object v2, v0, Lcom/facebook/audience/model/MinimalReplyThread;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2155086
    iget-object v2, p0, LX/Efh;->g:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2155087
    iput-object v2, v1, LX/EfL;->H:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2155088
    iget-object v2, v1, LX/EfL;->A:LX/EfK;

    if-eqz v2, :cond_0

    .line 2155089
    invoke-static {v1}, LX/EfL;->h(LX/EfL;)V

    .line 2155090
    :cond_0
    new-instance v2, LX/EfK;

    invoke-direct {v2, v1, v0}, LX/EfK;-><init>(LX/EfL;Ljava/lang/String;)V

    iput-object v2, v1, LX/EfL;->A:LX/EfK;

    .line 2155091
    iget-object v2, v1, LX/EfL;->d:LX/AFO;

    iget-object v4, v1, LX/EfL;->A:LX/EfK;

    .line 2155092
    iget-object v5, v2, LX/AFO;->e:LX/7gh;

    invoke-virtual {v5, v4}, LX/7gh;->a(Ljava/lang/Object;)V

    .line 2155093
    iget-object v5, v4, LX/EfK;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2155094
    iget-object v6, v2, LX/AFO;->i:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2155095
    iget-object v6, v2, LX/AFO;->c:LX/AHZ;

    iget-object v7, v2, LX/AFO;->b:LX/AFM;

    .line 2155096
    iget-object v8, v6, LX/AHZ;->f:LX/0gK;

    invoke-virtual {v8}, LX/0gK;->a()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2155097
    new-instance v8, LX/2r9;

    invoke-direct {v8}, LX/2r9;-><init>()V

    .line 2155098
    const-string v9, "thread_id"

    invoke-virtual {v8, v9, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2155099
    new-instance v9, LX/AHg;

    invoke-direct {v9}, LX/AHg;-><init>()V

    move-object v9, v9

    .line 2155100
    const-string v4, "data"

    invoke-virtual {v9, v4, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2155101
    :try_start_0
    iget-object v8, v6, LX/AHZ;->e:LX/0gX;

    new-instance v4, LX/AHW;

    invoke-direct {v4, v6, v5, v7}, LX/AHW;-><init>(LX/AHZ;Ljava/lang/String;LX/AFM;)V

    invoke-virtual {v8, v9, v4}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 2155102
    :goto_0
    move-object v8, v8

    .line 2155103
    :goto_1
    move-object v6, v8

    .line 2155104
    iget-object v7, v2, LX/AFO;->i:Ljava/util/Map;

    invoke-interface {v7, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2155105
    :cond_1
    iget-object v2, v1, LX/EfL;->H:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2155106
    iget-boolean v4, v2, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    move v2, v4

    .line 2155107
    if-eqz v2, :cond_3

    .line 2155108
    iget-object v2, v1, LX/EfL;->d:LX/AFO;

    .line 2155109
    iget-object v4, v2, LX/AFO;->j:LX/AFP;

    invoke-virtual {v4, v0}, LX/AFP;->a(Ljava/lang/String;)Lcom/facebook/audience/model/ReplyThread;

    move-result-object v4

    .line 2155110
    if-eqz v4, :cond_5

    .line 2155111
    iget-object v5, v2, LX/AFO;->b:LX/AFM;

    invoke-virtual {v5, v4}, LX/AFM;->a(Lcom/facebook/audience/model/ReplyThread;)V

    .line 2155112
    :goto_2
    iget-object v2, v1, LX/EfL;->a:LX/1Eo;

    sget-object v4, LX/7gQ;->OPEN_REPLY_THREAD:LX/7gQ;

    invoke-virtual {v2, v4}, LX/1Eo;->a(LX/7gQ;)V

    .line 2155113
    iget-object v0, p0, LX/Efh;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    iget-object v0, p0, LX/Efh;->a:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/MinimalReplyThread;

    .line 2155114
    iget-boolean v2, v0, Lcom/facebook/audience/model/MinimalReplyThread;->b:Z

    move v0, v2

    .line 2155115
    if-nez v0, :cond_2

    .line 2155116
    iget-object v0, v1, LX/EfL;->q:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    iget-object v2, v1, LX/EfL;->q:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    invoke-virtual {v2}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v1}, LX/EfL;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b1def

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v5, v1, LX/EfL;->q:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    invoke-virtual {v5}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->getPaddingRight()I

    move-result v5

    iget-object v6, v1, LX/EfL;->q:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    invoke-virtual {v6}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v0, v2, v4, v5, v6}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->setPadding(IIII)V

    .line 2155117
    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2155118
    iget-object v0, p0, LX/Efh;->e:Landroid/util/SparseArray;

    new-instance v2, LX/Efg;

    invoke-direct {v2, p0, v1, v3}, LX/Efg;-><init>(LX/Efh;LX/EfL;Z)V

    invoke-virtual {v0, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2155119
    return-object v1

    .line 2155120
    :cond_3
    iget-object v2, v1, LX/EfL;->d:LX/AFO;

    invoke-virtual {v2, v0}, LX/AFO;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 2155121
    :cond_4
    new-instance v8, LX/4D7;

    invoke-direct {v8}, LX/4D7;-><init>()V

    .line 2155122
    const-string v9, "thread_id"

    invoke-virtual {v8, v9, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2155123
    new-instance v9, LX/AHh;

    invoke-direct {v9}, LX/AHh;-><init>()V

    move-object v9, v9

    .line 2155124
    const-string v4, "input"

    invoke-virtual {v9, v4, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2155125
    :try_start_1
    iget-object v8, v6, LX/AHZ;->e:LX/0gX;

    new-instance v4, LX/AHV;

    invoke-direct {v4, v6, v5, v7}, LX/AHV;-><init>(LX/AHZ;Ljava/lang/String;LX/AFM;)V

    invoke-virtual {v8, v9, v4}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;
    :try_end_1
    .catch LX/31B; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    .line 2155126
    :goto_3
    move-object v8, v8

    .line 2155127
    goto/16 :goto_1

    :catch_0
    const/4 v8, 0x0

    goto/16 :goto_0

    :catch_1
    const/4 v8, 0x0

    goto :goto_3

    .line 2155128
    :cond_5
    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-static {v2, v4, v0}, LX/AFO;->a(LX/AFO;LX/0zS;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2155139
    check-cast p3, LX/EfL;

    .line 2155140
    const/4 v0, 0x0

    .line 2155141
    iput-object v0, p3, LX/EfL;->y:LX/EfA;

    .line 2155142
    invoke-virtual {p3}, LX/EfL;->d()V

    .line 2155143
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2155144
    iget-object v0, p0, LX/Efh;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 2155145
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2155081
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2155074
    iget-object v0, p0, LX/Efh;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2155075
    iget v0, p0, LX/Efh;->h:I

    if-ne v0, p2, :cond_1

    .line 2155076
    :cond_0
    :goto_0
    return-void

    .line 2155077
    :cond_1
    iget-object v0, p0, LX/Efh;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Efg;

    .line 2155078
    if-eqz v0, :cond_0

    .line 2155079
    invoke-virtual {v0}, LX/Efg;->a()V

    .line 2155080
    iput p2, p0, LX/Efh;->h:I

    goto :goto_0
.end method
