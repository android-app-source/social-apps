.class public final enum LX/CiH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CiH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CiH;

.field public static final enum REGISTER:LX/CiH;

.field public static final enum UNREGISTER:LX/CiH;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1928743
    new-instance v0, LX/CiH;

    const-string v1, "REGISTER"

    invoke-direct {v0, v1, v2}, LX/CiH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiH;->REGISTER:LX/CiH;

    .line 1928744
    new-instance v0, LX/CiH;

    const-string v1, "UNREGISTER"

    invoke-direct {v0, v1, v3}, LX/CiH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiH;->UNREGISTER:LX/CiH;

    .line 1928745
    const/4 v0, 0x2

    new-array v0, v0, [LX/CiH;

    sget-object v1, LX/CiH;->REGISTER:LX/CiH;

    aput-object v1, v0, v2

    sget-object v1, LX/CiH;->UNREGISTER:LX/CiH;

    aput-object v1, v0, v3

    sput-object v0, LX/CiH;->$VALUES:[LX/CiH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1928742
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CiH;
    .locals 1

    .prologue
    .line 1928746
    const-class v0, LX/CiH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CiH;

    return-object v0
.end method

.method public static values()[LX/CiH;
    .locals 1

    .prologue
    .line 1928741
    sget-object v0, LX/CiH;->$VALUES:[LX/CiH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CiH;

    return-object v0
.end method
