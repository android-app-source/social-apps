.class public final LX/E3L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E2S;

.field public final synthetic b:LX/9uc;

.field public final synthetic c:LX/1Pq;

.field public final synthetic d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;LX/E2S;LX/9uc;LX/1Pq;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2074116
    iput-object p1, p0, LX/E3L;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;

    iput-object p2, p0, LX/E3L;->a:LX/E2S;

    iput-object p3, p0, LX/E3L;->b:LX/9uc;

    iput-object p4, p0, LX/E3L;->c:LX/1Pq;

    iput-object p5, p0, LX/E3L;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x6368a847

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2074104
    iget-object v1, p0, LX/E3L;->a:LX/E2S;

    sget-object v2, LX/03R;->YES:LX/03R;

    invoke-virtual {v1, v2}, LX/E2S;->a(LX/03R;)V

    .line 2074105
    iget-object v1, p0, LX/E3L;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisActionUnitComponentPartDefinition;->b:LX/3U3;

    iget-object v2, p0, LX/E3L;->b:LX/9uc;

    invoke-interface {v2}, LX/9uc;->af()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2074106
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2074107
    new-instance v3, LX/4Gu;

    invoke-direct {v3}, LX/4Gu;-><init>()V

    invoke-virtual {v3, v2}, LX/4Gu;->a(Ljava/lang/String;)LX/4Gu;

    move-result-object v3

    .line 2074108
    new-instance v4, LX/9tt;

    invoke-direct {v4}, LX/9tt;-><init>()V

    move-object v4, v4

    .line 2074109
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/9tt;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2074110
    iget-object v4, v1, LX/3U3;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2074111
    :cond_0
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->POSITIVE:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    sget-object v3, LX/Cfc;->CRISIS_MARK_AS_SAFE:LX/Cfc;

    iget-object v4, p0, LX/E3L;->c:LX/1Pq;

    iget-object v5, p0, LX/E3L;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074112
    move-object p0, v4

    check-cast p0, LX/2kk;

    invoke-static {p0, v5, v2}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->a(LX/2kk;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/enums/GraphQLSelectedActionState;)V

    move-object p0, v4

    .line 2074113
    check-cast p0, LX/2kk;

    invoke-virtual {v3}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, v5, p1}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V

    .line 2074114
    const/4 p0, 0x1

    new-array p0, p0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p1, 0x0

    invoke-static {v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    aput-object v1, p0, p1

    invoke-interface {v4, p0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2074115
    const v1, 0x3a9733bc

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
