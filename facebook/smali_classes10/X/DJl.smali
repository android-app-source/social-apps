.class public LX/DJl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Kf;

.field private final b:LX/0y2;

.field public final c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final d:LX/0tX;

.field public final e:LX/B0k;

.field public final f:LX/0ad;

.field private final g:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/1Kf;LX/0y2;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0tX;LX/B0k;LX/0ad;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1985834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1985835
    iput-object p1, p0, LX/DJl;->a:LX/1Kf;

    .line 1985836
    iput-object p2, p0, LX/DJl;->b:LX/0y2;

    .line 1985837
    iput-object p3, p0, LX/DJl;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1985838
    iput-object p4, p0, LX/DJl;->d:LX/0tX;

    .line 1985839
    iput-object p5, p0, LX/DJl;->e:LX/B0k;

    .line 1985840
    iput-object p6, p0, LX/DJl;->f:LX/0ad;

    .line 1985841
    iput-object p7, p0, LX/DJl;->g:Ljava/util/concurrent/ExecutorService;

    .line 1985842
    return-void
.end method

.method private static a(LX/DJl;Lcom/facebook/location/ImmutableLocation;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/location/ImmutableLocation;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1985829
    invoke-static {}, LX/9Jh;->a()LX/9Jg;

    move-result-object v0

    .line 1985830
    if-eqz p1, :cond_0

    .line 1985831
    const-string v1, "latitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1985832
    const-string v1, "longitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1985833
    :cond_0
    iget-object v1, p0, LX/DJl;->d:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/21D;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;LX/DJr;Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 1985810
    invoke-static {p2}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1985811
    invoke-static {p2}, LX/16z;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    .line 1985812
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {p2}, LX/2vB;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_3

    if-nez v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1985813
    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    if-eqz p5, :cond_0

    .line 1985814
    iget-object v0, p0, LX/DJl;->f:LX/0ad;

    sget-short v1, LX/DIp;->h:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1985815
    if-nez v0, :cond_2

    .line 1985816
    :cond_0
    if-eqz p4, :cond_1

    .line 1985817
    invoke-interface {p4}, LX/DJr;->c()V

    .line 1985818
    :cond_1
    :goto_1
    return-void

    .line 1985819
    :cond_2
    invoke-static {p2}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1985820
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x0

    .line 1985821
    new-instance v3, LX/9LJ;

    invoke-direct {v3}, LX/9LJ;-><init>()V

    move-object v3, v3

    .line 1985822
    new-instance v4, LX/4JG;

    invoke-direct {v4}, LX/4JG;-><init>()V

    .line 1985823
    const-string v5, "text"

    invoke-virtual {v4, v5, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1985824
    invoke-virtual {p1}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v5

    .line 1985825
    const-string v0, "source"

    invoke-virtual {v4, v0, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1985826
    const-string v5, "query"

    invoke-virtual {v3, v5, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1985827
    iget-object v4, p0, LX/DJl;->d:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v0, v3

    .line 1985828
    aput-object v0, v1, v2

    const/4 v0, 0x1

    iget-object v2, p0, LX/DJl;->b:LX/0y2;

    invoke-virtual {v2}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v2

    invoke-static {p0, v2}, LX/DJl;->a(LX/DJl;Lcom/facebook/location/ImmutableLocation;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    new-instance v0, LX/DJk;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/DJk;-><init>(LX/DJl;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;LX/21D;Landroid/content/Context;LX/DJr;)V

    iget-object v1, p0, LX/DJl;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v7, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
