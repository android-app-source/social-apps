.class public LX/EiE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/EiE;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/EiF;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/EiF;)V
    .locals 0

    .prologue
    .line 2159200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2159201
    iput-object p1, p0, LX/EiE;->a:Ljava/lang/String;

    .line 2159202
    iput-object p2, p0, LX/EiE;->b:LX/EiF;

    .line 2159203
    return-void
.end method


# virtual methods
.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2159204
    check-cast p1, LX/EiE;

    .line 2159205
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/EiE;->b:LX/EiF;

    if-nez v0, :cond_1

    .line 2159206
    :cond_0
    const/4 v0, -0x1

    .line 2159207
    :goto_0
    return v0

    .line 2159208
    :cond_1
    iget-object v0, p0, LX/EiE;->b:LX/EiF;

    if-nez v0, :cond_2

    .line 2159209
    const/4 v0, 0x1

    goto :goto_0

    .line 2159210
    :cond_2
    iget-object v0, p0, LX/EiE;->b:LX/EiF;

    invoke-virtual {v0}, LX/EiF;->ordinal()I

    move-result v0

    iget-object v1, p1, LX/EiE;->b:LX/EiF;

    invoke-virtual {v1}, LX/EiF;->ordinal()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2159211
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/EiE;

    if-nez v1, :cond_1

    .line 2159212
    :cond_0
    :goto_0
    return v0

    .line 2159213
    :cond_1
    check-cast p1, LX/EiE;

    .line 2159214
    iget-object v1, p0, LX/EiE;->a:Ljava/lang/String;

    iget-object v2, p1, LX/EiE;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EiE;->b:LX/EiF;

    iget-object v2, p1, LX/EiE;->b:LX/EiF;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
