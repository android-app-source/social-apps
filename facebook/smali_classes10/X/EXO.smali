.class public final LX/EXO;
.super LX/EWy;
.source ""

# interfaces
.implements LX/EXN;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWy",
        "<",
        "LX/EXR;",
        "LX/EXO;",
        ">;",
        "LX/EXN;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:LX/EXQ;

.field public c:Z

.field public d:Z

.field public e:Z

.field private f:Ljava/lang/Object;

.field public g:Z

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2133286
    invoke-direct {p0}, LX/EWy;-><init>()V

    .line 2133287
    sget-object v0, LX/EXQ;->STRING:LX/EXQ;

    iput-object v0, p0, LX/EXO;->b:LX/EXQ;

    .line 2133288
    const-string v0, ""

    iput-object v0, p0, LX/EXO;->f:Ljava/lang/Object;

    .line 2133289
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXO;->h:Ljava/util/List;

    .line 2133290
    invoke-direct {p0}, LX/EXO;->w()V

    .line 2133291
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2133292
    invoke-direct {p0, p1}, LX/EWy;-><init>(LX/EYd;)V

    .line 2133293
    sget-object v0, LX/EXQ;->STRING:LX/EXQ;

    iput-object v0, p0, LX/EXO;->b:LX/EXQ;

    .line 2133294
    const-string v0, ""

    iput-object v0, p0, LX/EXO;->f:Ljava/lang/Object;

    .line 2133295
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXO;->h:Ljava/util/List;

    .line 2133296
    invoke-direct {p0}, LX/EXO;->w()V

    .line 2133297
    return-void
.end method

.method private A()LX/EXR;
    .locals 2

    .prologue
    .line 2133298
    invoke-virtual {p0}, LX/EXO;->l()LX/EXR;

    move-result-object v0

    .line 2133299
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2133300
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2133301
    :cond_0
    return-object v0
.end method

.method private D()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2133302
    iget-object v0, p0, LX/EXO;->i:LX/EZ2;

    if-nez v0, :cond_0

    .line 2133303
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EXO;->h:Ljava/util/List;

    iget v0, p0, LX/EXO;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2133304
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2133305
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EXO;->i:LX/EZ2;

    .line 2133306
    const/4 v0, 0x0

    iput-object v0, p0, LX/EXO;->h:Ljava/util/List;

    .line 2133307
    :cond_0
    iget-object v0, p0, LX/EXO;->i:LX/EZ2;

    return-object v0

    .line 2133308
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/EXQ;)LX/EXO;
    .locals 1

    .prologue
    .line 2133309
    if-nez p1, :cond_0

    .line 2133310
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2133311
    :cond_0
    iget v0, p0, LX/EXO;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EXO;->a:I

    .line 2133312
    iput-object p1, p0, LX/EXO;->b:LX/EXQ;

    .line 2133313
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2133314
    return-object p0
.end method

.method private d(LX/EWY;)LX/EXO;
    .locals 1

    .prologue
    .line 2133315
    instance-of v0, p1, LX/EXR;

    if-eqz v0, :cond_0

    .line 2133316
    check-cast p1, LX/EXR;

    invoke-virtual {p0, p1}, LX/EXO;->a(LX/EXR;)LX/EXO;

    move-result-object p0

    .line 2133317
    :goto_0
    return-object p0

    .line 2133318
    :cond_0
    invoke-super {p0, p1}, LX/EWy;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EXO;
    .locals 4

    .prologue
    .line 2133319
    const/4 v2, 0x0

    .line 2133320
    :try_start_0
    sget-object v0, LX/EXR;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXR;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2133321
    if-eqz v0, :cond_0

    .line 2133322
    invoke-virtual {p0, v0}, LX/EXO;->a(LX/EXR;)LX/EXO;

    .line 2133323
    :cond_0
    return-object p0

    .line 2133324
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2133325
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2133326
    check-cast v0, LX/EXR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2133327
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2133328
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2133329
    invoke-virtual {p0, v1}, LX/EXO;->a(LX/EXR;)LX/EXO;

    :cond_1
    throw v0

    .line 2133330
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 2133331
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2133332
    invoke-direct {p0}, LX/EXO;->D()LX/EZ2;

    .line 2133333
    :cond_0
    return-void
.end method

.method public static x()LX/EXO;
    .locals 1

    .prologue
    .line 2133334
    new-instance v0, LX/EXO;

    invoke-direct {v0}, LX/EXO;-><init>()V

    return-object v0
.end method

.method private y()LX/EXO;
    .locals 2

    .prologue
    .line 2133335
    invoke-static {}, LX/EXO;->x()LX/EXO;

    move-result-object v0

    invoke-virtual {p0}, LX/EXO;->l()LX/EXR;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EXO;->a(LX/EXR;)LX/EXO;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2133233
    invoke-direct {p0, p1}, LX/EXO;->d(LX/EWY;)LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2133336
    invoke-direct {p0, p1, p2}, LX/EXO;->d(LX/EWd;LX/EYZ;)LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EXR;)LX/EXO;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2133337
    sget-object v1, LX/EXR;->c:LX/EXR;

    move-object v1, v1

    .line 2133338
    if-ne p1, v1, :cond_0

    .line 2133339
    :goto_0
    return-object p0

    .line 2133340
    :cond_0
    const/4 v1, 0x1

    .line 2133341
    iget v2, p1, LX/EXR;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_d

    :goto_1
    move v1, v1

    .line 2133342
    if-eqz v1, :cond_1

    .line 2133343
    iget-object v1, p1, LX/EXR;->ctype_:LX/EXQ;

    move-object v1, v1

    .line 2133344
    invoke-direct {p0, v1}, LX/EXO;->a(LX/EXQ;)LX/EXO;

    .line 2133345
    :cond_1
    iget v1, p1, LX/EXR;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_e

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2133346
    if-eqz v1, :cond_2

    .line 2133347
    iget-boolean v1, p1, LX/EXR;->packed_:Z

    move v1, v1

    .line 2133348
    iget v2, p0, LX/EXO;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, LX/EXO;->a:I

    .line 2133349
    iput-boolean v1, p0, LX/EXO;->c:Z

    .line 2133350
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2133351
    :cond_2
    iget v1, p1, LX/EXR;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_f

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 2133352
    if-eqz v1, :cond_3

    .line 2133353
    iget-boolean v1, p1, LX/EXR;->lazy_:Z

    move v1, v1

    .line 2133354
    iget v2, p0, LX/EXO;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, LX/EXO;->a:I

    .line 2133355
    iput-boolean v1, p0, LX/EXO;->d:Z

    .line 2133356
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2133357
    :cond_3
    iget v1, p1, LX/EXR;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_10

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 2133358
    if-eqz v1, :cond_4

    .line 2133359
    iget-boolean v1, p1, LX/EXR;->deprecated_:Z

    move v1, v1

    .line 2133360
    iget v2, p0, LX/EXO;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, LX/EXO;->a:I

    .line 2133361
    iput-boolean v1, p0, LX/EXO;->e:Z

    .line 2133362
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2133363
    :cond_4
    iget v1, p1, LX/EXR;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_11

    const/4 v1, 0x1

    :goto_5
    move v1, v1

    .line 2133364
    if-eqz v1, :cond_5

    .line 2133365
    iget v1, p0, LX/EXO;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, LX/EXO;->a:I

    .line 2133366
    iget-object v1, p1, LX/EXR;->experimentalMapKey_:Ljava/lang/Object;

    iput-object v1, p0, LX/EXO;->f:Ljava/lang/Object;

    .line 2133367
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2133368
    :cond_5
    iget v1, p1, LX/EXR;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_12

    const/4 v1, 0x1

    :goto_6
    move v1, v1

    .line 2133369
    if-eqz v1, :cond_6

    .line 2133370
    iget-boolean v1, p1, LX/EXR;->weak_:Z

    move v1, v1

    .line 2133371
    iget v2, p0, LX/EXO;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, LX/EXO;->a:I

    .line 2133372
    iput-boolean v1, p0, LX/EXO;->g:Z

    .line 2133373
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2133374
    :cond_6
    iget-object v1, p0, LX/EXO;->i:LX/EZ2;

    if-nez v1, :cond_a

    .line 2133375
    iget-object v0, p1, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2133376
    iget-object v0, p0, LX/EXO;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2133377
    iget-object v0, p1, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    iput-object v0, p0, LX/EXO;->h:Ljava/util/List;

    .line 2133378
    iget v0, p0, LX/EXO;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, LX/EXO;->a:I

    .line 2133379
    :goto_7
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2133380
    :cond_7
    :goto_8
    invoke-virtual {p0, p1}, LX/EWy;->a(LX/EX1;)V

    .line 2133381
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto/16 :goto_0

    .line 2133382
    :cond_8
    iget v0, p0, LX/EXO;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_9

    .line 2133383
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EXO;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EXO;->h:Ljava/util/List;

    .line 2133384
    iget v0, p0, LX/EXO;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, LX/EXO;->a:I

    .line 2133385
    :cond_9
    iget-object v0, p0, LX/EXO;->h:Ljava/util/List;

    iget-object v1, p1, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_7

    .line 2133386
    :cond_a
    iget-object v1, p1, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 2133387
    iget-object v1, p0, LX/EXO;->i:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2133388
    iget-object v1, p0, LX/EXO;->i:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2133389
    iput-object v0, p0, LX/EXO;->i:LX/EZ2;

    .line 2133390
    iget-object v1, p1, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    iput-object v1, p0, LX/EXO;->h:Ljava/util/List;

    .line 2133391
    iget v1, p0, LX/EXO;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, LX/EXO;->a:I

    .line 2133392
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_b

    invoke-direct {p0}, LX/EXO;->D()LX/EZ2;

    move-result-object v0

    :cond_b
    iput-object v0, p0, LX/EXO;->i:LX/EZ2;

    goto :goto_8

    .line 2133393
    :cond_c
    iget-object v0, p0, LX/EXO;->i:LX/EZ2;

    iget-object v1, p1, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_8

    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_6
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2133394
    move v0, v1

    .line 2133395
    :goto_0
    iget-object v2, p0, LX/EXO;->i:LX/EZ2;

    if-nez v2, :cond_3

    .line 2133396
    iget-object v2, p0, LX/EXO;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2133397
    :goto_1
    move v2, v2

    .line 2133398
    if-ge v0, v2, :cond_2

    .line 2133399
    iget-object v2, p0, LX/EXO;->i:LX/EZ2;

    if-nez v2, :cond_4

    .line 2133400
    iget-object v2, p0, LX/EXO;->h:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYB;

    .line 2133401
    :goto_2
    move-object v2, v2

    .line 2133402
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2133403
    :cond_0
    :goto_3
    return v1

    .line 2133404
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2133405
    :cond_2
    invoke-virtual {p0}, LX/EWy;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2133406
    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    iget-object v2, p0, LX/EXO;->i:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/EXO;->i:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EYB;

    goto :goto_2
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2133285
    invoke-direct {p0, p1, p2}, LX/EXO;->d(LX/EWd;LX/EYZ;)LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2133407
    invoke-direct {p0}, LX/EXO;->y()LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2133284
    invoke-direct {p0, p1, p2}, LX/EXO;->d(LX/EWd;LX/EYZ;)LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2133283
    invoke-direct {p0}, LX/EXO;->y()LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2133282
    invoke-direct {p0, p1}, LX/EXO;->d(LX/EWY;)LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2133281
    invoke-direct {p0}, LX/EXO;->y()LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2133280
    sget-object v0, LX/EYC;->x:LX/EYn;

    const-class v1, LX/EXR;

    const-class v2, LX/EXO;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2133279
    sget-object v0, LX/EYC;->w:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2133278
    invoke-direct {p0}, LX/EXO;->y()LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2133277
    invoke-virtual {p0}, LX/EXO;->l()LX/EXR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2133276
    invoke-direct {p0}, LX/EXO;->A()LX/EXR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2133275
    invoke-virtual {p0}, LX/EXO;->l()LX/EXR;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2133274
    invoke-direct {p0}, LX/EXO;->A()LX/EXR;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EXR;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2133237
    new-instance v2, LX/EXR;

    invoke-direct {v2, p0}, LX/EXR;-><init>(LX/EWy;)V

    .line 2133238
    iget v3, p0, LX/EXO;->a:I

    .line 2133239
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 2133240
    :goto_0
    iget-object v1, p0, LX/EXO;->b:LX/EXQ;

    .line 2133241
    iput-object v1, v2, LX/EXR;->ctype_:LX/EXQ;

    .line 2133242
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2133243
    or-int/lit8 v0, v0, 0x2

    .line 2133244
    :cond_0
    iget-boolean v1, p0, LX/EXO;->c:Z

    .line 2133245
    iput-boolean v1, v2, LX/EXR;->packed_:Z

    .line 2133246
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2133247
    or-int/lit8 v0, v0, 0x4

    .line 2133248
    :cond_1
    iget-boolean v1, p0, LX/EXO;->d:Z

    .line 2133249
    iput-boolean v1, v2, LX/EXR;->lazy_:Z

    .line 2133250
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2133251
    or-int/lit8 v0, v0, 0x8

    .line 2133252
    :cond_2
    iget-boolean v1, p0, LX/EXO;->e:Z

    .line 2133253
    iput-boolean v1, v2, LX/EXR;->deprecated_:Z

    .line 2133254
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2133255
    or-int/lit8 v0, v0, 0x10

    .line 2133256
    :cond_3
    iget-object v1, p0, LX/EXO;->f:Ljava/lang/Object;

    .line 2133257
    iput-object v1, v2, LX/EXR;->experimentalMapKey_:Ljava/lang/Object;

    .line 2133258
    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 2133259
    or-int/lit8 v0, v0, 0x20

    .line 2133260
    :cond_4
    iget-boolean v1, p0, LX/EXO;->g:Z

    .line 2133261
    iput-boolean v1, v2, LX/EXR;->weak_:Z

    .line 2133262
    iget-object v1, p0, LX/EXO;->i:LX/EZ2;

    if-nez v1, :cond_6

    .line 2133263
    iget v1, p0, LX/EXO;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    .line 2133264
    iget-object v1, p0, LX/EXO;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXO;->h:Ljava/util/List;

    .line 2133265
    iget v1, p0, LX/EXO;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, LX/EXO;->a:I

    .line 2133266
    :cond_5
    iget-object v1, p0, LX/EXO;->h:Ljava/util/List;

    .line 2133267
    iput-object v1, v2, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    .line 2133268
    :goto_1
    iput v0, v2, LX/EXR;->bitField0_:I

    .line 2133269
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2133270
    return-object v2

    .line 2133271
    :cond_6
    iget-object v1, p0, LX/EXO;->i:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2133272
    iput-object v1, v2, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    .line 2133273
    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic m()LX/EWy;
    .locals 1

    .prologue
    .line 2133236
    invoke-direct {p0}, LX/EXO;->y()LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2133234
    sget-object v0, LX/EXR;->c:LX/EXR;

    move-object v0, v0

    .line 2133235
    return-object v0
.end method
