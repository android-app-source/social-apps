.class public LX/DPp;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:LX/03V;

.field private c:Landroid/content/Context;

.field private d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/facebook/content/SecureContextHelper;

.field public f:LX/0ad;

.field private g:LX/DK3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1993877
    const-class v0, LX/DPp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DPp;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;Landroid/content/Context;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0ad;LX/DK3;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/ReactFragmentActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0ad;",
            "LX/DK3;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1993878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1993879
    iput-object p1, p0, LX/DPp;->b:LX/03V;

    .line 1993880
    iput-object p2, p0, LX/DPp;->c:Landroid/content/Context;

    .line 1993881
    iput-object p3, p0, LX/DPp;->d:LX/0Or;

    .line 1993882
    iput-object p4, p0, LX/DPp;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1993883
    iput-object p5, p0, LX/DPp;->f:LX/0ad;

    .line 1993884
    iput-object p6, p0, LX/DPp;->g:LX/DK3;

    .line 1993885
    return-void
.end method

.method public static a(LX/0QB;)LX/DPp;
    .locals 1

    .prologue
    .line 1993886
    invoke-static {p0}, LX/DPp;->b(LX/0QB;)LX/DPp;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DPp;
    .locals 7

    .prologue
    .line 1993887
    new-instance v0, LX/DPp;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/16 v3, 0xd

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {p0}, LX/DK3;->a(LX/0QB;)LX/DK3;

    move-result-object v6

    check-cast v6, LX/DK3;

    invoke-direct/range {v0 .. v6}, LX/DPp;-><init>(LX/03V;Landroid/content/Context;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0ad;LX/DK3;)V

    .line 1993888
    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x11

    .line 1993889
    iget-object v0, p0, LX/DPp;->c:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1993890
    iget-object v0, p0, LX/DPp;->b:LX/03V;

    sget-object v1, LX/DPp;->a:Ljava/lang/String;

    const-string v2, "fragment is null while responding to leave action"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993891
    :cond_0
    :goto_0
    return-void

    .line 1993892
    :cond_1
    iget-object v0, p0, LX/DPp;->c:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_4

    .line 1993893
    iget-object v0, p0, LX/DPp;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 1993894
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_2

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v2, :cond_4

    .line 1993895
    :cond_3
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 1993896
    :cond_4
    if-nez p1, :cond_5

    .line 1993897
    iget-object v0, p0, LX/DPp;->f:LX/0ad;

    sget-short v1, LX/88j;->l:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1993898
    if-eqz v0, :cond_5

    .line 1993899
    iget-object v0, p0, LX/DPp;->c:Landroid/content/Context;

    iget-object v1, p0, LX/DPp;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08304f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1993900
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/DPp;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1993901
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUPS_GRID_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993902
    iget-object v1, p0, LX/DPp;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DPp;->c:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 1993903
    :cond_5
    if-eqz p1, :cond_0

    .line 1993904
    iget-object v0, p0, LX/DPp;->g:LX/DK3;

    new-instance v1, LX/DK7;

    invoke-direct {v1}, LX/DK7;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
