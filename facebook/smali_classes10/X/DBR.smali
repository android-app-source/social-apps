.class public final LX/DBR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/DBX;


# direct methods
.method public constructor <init>(LX/DBX;)V
    .locals 0

    .prologue
    .line 1972837
    iput-object p1, p0, LX/DBR;->a:LX/DBX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    .line 1972838
    iget-object v0, p0, LX/DBR;->a:LX/DBX;

    iget-object v0, v0, LX/DBX;->l:LX/DBI;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    const/4 p1, 0x0

    .line 1972839
    iget-object v2, v0, LX/DBI;->d:LX/1nQ;

    iget-object v3, v0, LX/DBI;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v1, p1}, LX/1nQ;->b(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)V

    .line 1972840
    iget-object v2, v0, LX/DBI;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Kf;

    sget-object v3, LX/21D;->NEWSFEED:LX/21D;

    const-string v4, "shareEvent"

    iget-object v5, v0, LX/DBI;->a:Lcom/facebook/events/model/Event;

    .line 1972841
    iget-object p0, v5, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v5, p0

    .line 1972842
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const p0, 0x403827a

    invoke-static {v5, p0}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    invoke-static {v5}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v5

    invoke-virtual {v5}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    iget-object v4, v0, LX/DBI;->c:Landroid/content/Context;

    invoke-interface {v2, p1, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1972843
    const/4 v0, 0x1

    return v0
.end method
