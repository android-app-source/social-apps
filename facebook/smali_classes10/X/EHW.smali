.class public final LX/EHW;
.super LX/6Il;
.source ""


# instance fields
.field public final synthetic a:LX/EHZ;


# direct methods
.method public constructor <init>(LX/EHZ;)V
    .locals 0

    .prologue
    .line 2099616
    iput-object p1, p0, LX/EHW;->a:LX/EHZ;

    invoke-direct {p0}, LX/6Il;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2099617
    sget-object v0, LX/EHZ;->c:Ljava/lang/Class;

    const-string v1, "msqrd_camera_open"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2099618
    iget-object v0, p0, LX/EHW;->a:LX/EHZ;

    invoke-static {v0}, LX/EHZ;->t(LX/EHZ;)V

    .line 2099619
    iget-object v0, p0, LX/EHW;->a:LX/EHZ;

    iget-object v0, v0, LX/EHZ;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 p0, 0x1

    .line 2099620
    instance-of v1, p1, LX/6JK;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "open"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2099621
    iget-object v1, v0, LX/EDx;->l:LX/2S7;

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2S7;->a(Ljava/lang/String;)V

    .line 2099622
    iget-object v1, v0, LX/EDx;->l:LX/2S7;

    const-string v2, "camera_error"

    invoke-virtual {v1, v2, p0}, LX/2S7;->a(Ljava/lang/String;Z)V

    .line 2099623
    iget-boolean v1, v0, LX/EDx;->aX:Z

    if-eqz v1, :cond_1

    .line 2099624
    :cond_0
    :goto_0
    return-void

    .line 2099625
    :cond_1
    iget-object v1, v0, LX/EDx;->bC:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/3Dx;->ep:I

    invoke-interface {v1, v2, v3, v4, p0}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    .line 2099626
    if-lez v1, :cond_0

    .line 2099627
    iget-object v1, v0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$1;

    invoke-direct {v2, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$1;-><init>(LX/EDx;)V

    const v3, 0x440707c5

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 14

    .prologue
    .line 2099628
    iget-object v0, p0, LX/EHW;->a:LX/EHZ;

    .line 2099629
    const/4 v13, 0x0

    .line 2099630
    invoke-static {v0}, LX/EHZ;->l(LX/EHZ;)I

    move-result v7

    .line 2099631
    invoke-static {v0}, LX/EHZ;->m(LX/EHZ;)I

    move-result v9

    .line 2099632
    :try_start_0
    iget-object v6, v0, LX/EHZ;->l:LX/6Ia;

    invoke-interface {v6}, LX/6Ia;->a()LX/6IP;

    move-result-object v10

    .line 2099633
    invoke-interface {v10}, LX/6IP;->c()Ljava/util/List;

    move-result-object v6

    invoke-static {v6, v7, v9}, LX/EHZ;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v6

    iput-object v6, v0, LX/EHZ;->m:LX/6JR;

    .line 2099634
    iget-object v6, v0, LX/EHZ;->m:LX/6JR;

    if-nez v6, :cond_0

    .line 2099635
    new-instance v6, LX/6JR;

    invoke-direct {v6, v7, v9}, LX/6JR;-><init>(II)V

    iput-object v6, v0, LX/EHZ;->m:LX/6JR;

    .line 2099636
    :cond_0
    iget-object v6, v0, LX/EHZ;->m:LX/6JR;

    iget v6, v6, LX/6JR;->a:I

    iput v6, v0, LX/EHZ;->x:I

    .line 2099637
    iget-object v6, v0, LX/EHZ;->m:LX/6JR;

    iget v6, v6, LX/6JR;->b:I

    iput v6, v0, LX/EHZ;->y:I

    .line 2099638
    invoke-interface {v10}, LX/6IP;->d()Ljava/util/List;

    move-result-object v6

    invoke-static {v6, v7, v9}, LX/EHZ;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v6

    .line 2099639
    if-nez v6, :cond_4

    .line 2099640
    iget-object v6, v0, LX/EHZ;->m:LX/6JR;

    move-object v8, v6

    .line 2099641
    :goto_0
    invoke-interface {v10}, LX/6IP;->e()Ljava/util/List;

    move-result-object v6

    invoke-static {v6, v7, v9}, LX/EHZ;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v6

    .line 2099642
    if-nez v6, :cond_3

    .line 2099643
    iget-object v6, v0, LX/EHZ;->m:LX/6JR;

    move-object v10, v6

    .line 2099644
    :goto_1
    new-instance v6, LX/6JB;

    iget v7, v8, LX/6JR;->a:I

    iget v8, v8, LX/6JR;->b:I

    iget v9, v10, LX/6JR;->a:I

    iget v10, v10, LX/6JR;->b:I

    iget-object v11, v0, LX/EHZ;->G:Landroid/view/WindowManager;

    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/Display;->getRotation()I

    move-result v11

    const/4 v12, 0x0

    invoke-direct/range {v6 .. v12}, LX/6JB;-><init>(IIIIILjava/util/List;)V
    :try_end_0
    .catch LX/6JJ; {:try_start_0 .. :try_end_0} :catch_0

    .line 2099645
    :goto_2
    move-object v1, v6

    .line 2099646
    iput-object v1, v0, LX/EHZ;->j:LX/6JB;

    .line 2099647
    iget-object v1, v0, LX/EHZ;->j:LX/6JB;

    if-nez v1, :cond_1

    .line 2099648
    sget-object v1, LX/EHZ;->c:Ljava/lang/Class;

    const-string v2, "Unable to create camera settings"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2099649
    invoke-static {v0}, LX/EHZ;->t(LX/EHZ;)V

    .line 2099650
    :goto_3
    return-void

    .line 2099651
    :cond_1
    const/4 v2, 0x0

    .line 2099652
    new-instance v1, LX/6JC;

    invoke-direct {v1, v2, v2}, LX/6JC;-><init>(LX/6JN;LX/6JO;)V

    move-object v1, v1

    .line 2099653
    iput-object v1, v0, LX/EHZ;->k:LX/6JC;

    .line 2099654
    iget-object v1, v0, LX/EHZ;->e:LX/6Jt;

    iget-object v2, v0, LX/EHZ;->l:LX/6Ia;

    iget-object v3, v0, LX/EHZ;->j:LX/6JB;

    iget-object v4, v0, LX/EHZ;->m:LX/6JR;

    invoke-virtual {v1, v2, v3, v4}, LX/6Jt;->a(LX/6Ia;LX/6JB;LX/6JR;)V

    .line 2099655
    iget-object v1, v0, LX/EHZ;->e:LX/6Jt;

    iget-object v2, v0, LX/EHZ;->i:LX/6Ik;

    iget-object v3, v0, LX/EHZ;->k:LX/6JC;

    invoke-virtual {v1, v2, v3}, LX/6Jt;->a(LX/6Ik;LX/6JC;)V

    .line 2099656
    iget-object v1, v0, LX/EHZ;->n:LX/6Kd;

    if-nez v1, :cond_2

    .line 2099657
    new-instance v2, LX/EFT;

    iget-object v3, v0, LX/EHZ;->H:LX/03V;

    iget-object v1, v0, LX/EHZ;->J:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->Z()Lcom/facebook/webrtc/MediaCaptureSink;

    move-result-object v1

    invoke-static {v0}, LX/EHZ;->l(LX/EHZ;)I

    move-result v4

    invoke-static {v0}, LX/EHZ;->m(LX/EHZ;)I

    move-result v5

    invoke-direct {v2, v3, v1, v4, v5}, LX/EFT;-><init>(LX/03V;Lcom/facebook/webrtc/MediaCaptureSink;II)V

    iput-object v2, v0, LX/EHZ;->n:LX/6Kd;

    .line 2099658
    iget-object v1, v0, LX/EHZ;->e:LX/6Jt;

    iget-object v2, v0, LX/EHZ;->n:LX/6Kd;

    invoke-virtual {v1, v2}, LX/6Jt;->a(LX/6Kd;)V

    .line 2099659
    :cond_2
    invoke-static {v0}, LX/EHZ;->s(LX/EHZ;)V

    goto :goto_3

    .line 2099660
    :catch_0
    move-exception v6

    .line 2099661
    sget-object v7, LX/EHZ;->c:Ljava/lang/Class;

    const-string v8, "msqrd_camera_get_characteristics"

    invoke-static {v7, v8, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v6, v13

    goto :goto_2

    :cond_3
    move-object v10, v6

    goto :goto_1

    :cond_4
    move-object v8, v6

    goto/16 :goto_0
.end method
