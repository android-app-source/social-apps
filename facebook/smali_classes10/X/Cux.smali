.class public final LX/Cux;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:Lcom/facebook/runtimepermissions/RequestPermissionsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/runtimepermissions/RequestPermissionsActivity;)V
    .locals 0

    .prologue
    .line 1947521
    iput-object p1, p0, LX/Cux;->a:Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1947522
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1947523
    iget-object v1, p0, LX/Cux;->a:Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    iget-object v1, v1, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->s:[Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->b(Ljava/util/Map;[Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1947524
    iget-object v1, p0, LX/Cux;->a:Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    invoke-static {v1, v0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->a$redex0(Lcom/facebook/runtimepermissions/RequestPermissionsActivity;Ljava/util/HashMap;)V

    .line 1947525
    return-void
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1947526
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1947527
    iget-object v1, p0, LX/Cux;->a:Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    iget-object v1, v1, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->s:[Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->b(Ljava/util/Map;[Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1947528
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->b(Ljava/util/Map;[Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1947529
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, p2, v1}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->b(Ljava/util/Map;[Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1947530
    iget-object v1, p0, LX/Cux;->a:Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    invoke-static {v1, v0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->a$redex0(Lcom/facebook/runtimepermissions/RequestPermissionsActivity;Ljava/util/HashMap;)V

    .line 1947531
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1947532
    iget-object v0, p0, LX/Cux;->a:Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->setResult(I)V

    .line 1947533
    iget-object v0, p0, LX/Cux;->a:Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    invoke-virtual {v0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->finish()V

    .line 1947534
    return-void
.end method
