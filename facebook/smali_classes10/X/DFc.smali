.class public LX/DFc;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFd;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DFc",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DFd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978531
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1978532
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DFc;->b:LX/0Zi;

    .line 1978533
    iput-object p1, p0, LX/DFc;->a:LX/0Ot;

    .line 1978534
    return-void
.end method

.method public static a(LX/0QB;)LX/DFc;
    .locals 4

    .prologue
    .line 1978535
    const-class v1, LX/DFc;

    monitor-enter v1

    .line 1978536
    :try_start_0
    sget-object v0, LX/DFc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978537
    sput-object v2, LX/DFc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978538
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978539
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978540
    new-instance v3, LX/DFc;

    const/16 p0, 0x20e9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DFc;-><init>(LX/0Ot;)V

    .line 1978541
    move-object v0, v3

    .line 1978542
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978543
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978544
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978545
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1978546
    check-cast p2, LX/DFb;

    .line 1978547
    iget-object v0, p0, LX/DFc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DFd;

    iget-object v1, p2, LX/DFb;->a:Ljava/lang/String;

    iget-object v2, p2, LX/DFb;->b:Ljava/lang/String;

    iget-object v3, p2, LX/DFb;->c:LX/1Pp;

    .line 1978548
    iget-object p0, v0, LX/DFd;->a:LX/1nu;

    invoke-virtual {p0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p0

    sget-object p2, Lcom/facebook/feedplugins/friendingcommon/ProfilePicturePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object p0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object p0

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, LX/1nw;->a(Z)LX/1nw;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const p2, 0x7f0b0f69

    invoke-interface {p0, p2}, LX/1Di;->q(I)LX/1Di;

    move-result-object p0

    invoke-interface {p0, v2}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1978549
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1978550
    invoke-static {}, LX/1dS;->b()V

    .line 1978551
    const/4 v0, 0x0

    return-object v0
.end method
