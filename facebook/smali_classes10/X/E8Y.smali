.class public final LX/E8Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;)V
    .locals 0

    .prologue
    .line 2082806
    iput-object p1, p0, LX/E8Y;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 11

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x2

    const/16 v0, 0x26

    const v1, -0x6c4eb33e

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2082807
    iget-object v1, p0, LX/E8Y;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-wide v2, v1, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->M:J

    cmp-long v1, v2, v8

    if-lez v1, :cond_0

    .line 2082808
    iget-object v1, p0, LX/E8Y;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-object v2, p0, LX/E8Y;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-object v2, v2, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-object v4, p0, LX/E8Y;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-wide v4, v4, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->M:J

    sub-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->b(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;J)J

    .line 2082809
    :cond_0
    iget-object v1, p0, LX/E8Y;->a:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    .line 2082810
    iput-wide v8, v1, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->M:J

    .line 2082811
    const/16 v1, 0x27

    const v2, 0x53a17e0b

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
