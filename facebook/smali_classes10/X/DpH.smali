.class public LX/DpH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final thread_id:Ljava/lang/Long;

.field public final user_devices:LX/DpR;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2043912
    new-instance v0, LX/1sv;

    const-string v1, "CreateThreadResponsePayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpH;->b:LX/1sv;

    .line 2043913
    new-instance v0, LX/1sw;

    const-string v1, "thread_id"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpH;->c:LX/1sw;

    .line 2043914
    new-instance v0, LX/1sw;

    const-string v1, "user_devices"

    const/16 v2, 0xc

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpH;->d:LX/1sw;

    .line 2043915
    const/4 v0, 0x1

    sput-boolean v0, LX/DpH;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;LX/DpR;)V
    .locals 0

    .prologue
    .line 2044021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2044022
    iput-object p1, p0, LX/DpH;->thread_id:Ljava/lang/Long;

    .line 2044023
    iput-object p2, p0, LX/DpH;->user_devices:LX/DpR;

    .line 2044024
    return-void
.end method

.method public static b(LX/1su;)LX/DpH;
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 2043980
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    .line 2043981
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2043982
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_a

    .line 2043983
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 2043984
    :pswitch_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2043985
    :pswitch_1
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xa

    if-ne v3, v4, :cond_0

    .line 2043986
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 2043987
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2043988
    :pswitch_2
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xc

    if-ne v3, v4, :cond_9

    .line 2043989
    const/4 v6, 0x0

    .line 2043990
    const/4 v5, 0x0

    .line 2043991
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 2043992
    :goto_1
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v7

    .line 2043993
    iget-byte v8, v7, LX/1sw;->b:B

    if-eqz v8, :cond_8

    .line 2043994
    iget-short v8, v7, LX/1sw;->c:S

    packed-switch v8, :pswitch_data_1

    .line 2043995
    iget-byte v7, v7, LX/1sw;->b:B

    invoke-static {p0, v7}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2043996
    :pswitch_3
    iget-byte v8, v7, LX/1sw;->b:B

    const/16 v9, 0xd

    if-ne v8, v9, :cond_7

    .line 2043997
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v9

    .line 2043998
    new-instance v8, Ljava/util/HashMap;

    iget v5, v9, LX/7H3;->c:I

    mul-int/lit8 v5, v5, 0x2

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-direct {v8, v5}, Ljava/util/HashMap;-><init>(I)V

    move v5, v6

    .line 2043999
    :goto_2
    iget v7, v9, LX/7H3;->c:I

    if-gez v7, :cond_3

    invoke-static {}, LX/1su;->s()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2044000
    :cond_1
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 2044001
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v11

    .line 2044002
    new-instance v12, Ljava/util/ArrayList;

    iget v7, v11, LX/1u3;->b:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-direct {v12, v7}, Ljava/util/ArrayList;-><init>(I)V

    move v7, v6

    .line 2044003
    :goto_3
    iget v13, v11, LX/1u3;->b:I

    if-gez v13, :cond_5

    invoke-static {}, LX/1su;->t()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 2044004
    :cond_2
    invoke-static {p0}, LX/DpM;->b(LX/1su;)LX/DpM;

    move-result-object v13

    .line 2044005
    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2044006
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 2044007
    :cond_3
    iget v7, v9, LX/7H3;->c:I

    if-lt v5, v7, :cond_1

    :cond_4
    move-object v5, v8

    .line 2044008
    goto :goto_1

    .line 2044009
    :cond_5
    iget v13, v11, LX/1u3;->b:I

    if-lt v7, v13, :cond_2

    .line 2044010
    :cond_6
    invoke-interface {v8, v10, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2044011
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2044012
    :cond_7
    iget-byte v7, v7, LX/1sw;->b:B

    invoke-static {p0, v7}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2044013
    :cond_8
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2044014
    new-instance v6, LX/DpR;

    invoke-direct {v6, v5}, LX/DpR;-><init>(Ljava/util/Map;)V

    .line 2044015
    move-object v0, v6

    .line 2044016
    goto/16 :goto_0

    .line 2044017
    :cond_9
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044018
    :cond_a
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2044019
    new-instance v2, LX/DpH;

    invoke-direct {v2, v1, v0}, LX/DpH;-><init>(Ljava/lang/Long;LX/DpR;)V

    .line 2044020
    return-object v2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2043952
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2043953
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2043954
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2043955
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CreateThreadResponsePayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2043956
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043957
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043958
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043959
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043960
    const-string v4, "thread_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043961
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043962
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043963
    iget-object v4, p0, LX/DpH;->thread_id:Ljava/lang/Long;

    if-nez v4, :cond_3

    .line 2043964
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043965
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043966
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043967
    const-string v4, "user_devices"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043968
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043969
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043970
    iget-object v0, p0, LX/DpH;->user_devices:LX/DpR;

    if-nez v0, :cond_4

    .line 2043971
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043972
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043973
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043974
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2043975
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2043976
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2043977
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2043978
    :cond_3
    iget-object v4, p0, LX/DpH;->thread_id:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2043979
    :cond_4
    iget-object v0, p0, LX/DpH;->user_devices:LX/DpR;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2043942
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2043943
    iget-object v0, p0, LX/DpH;->thread_id:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2043944
    sget-object v0, LX/DpH;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043945
    iget-object v0, p0, LX/DpH;->thread_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2043946
    :cond_0
    iget-object v0, p0, LX/DpH;->user_devices:LX/DpR;

    if-eqz v0, :cond_1

    .line 2043947
    sget-object v0, LX/DpH;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043948
    iget-object v0, p0, LX/DpH;->user_devices:LX/DpR;

    invoke-virtual {v0, p1}, LX/DpR;->a(LX/1su;)V

    .line 2043949
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2043950
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2043951
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2043920
    if-nez p1, :cond_1

    .line 2043921
    :cond_0
    :goto_0
    return v0

    .line 2043922
    :cond_1
    instance-of v1, p1, LX/DpH;

    if-eqz v1, :cond_0

    .line 2043923
    check-cast p1, LX/DpH;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2043924
    if-nez p1, :cond_3

    .line 2043925
    :cond_2
    :goto_1
    move v0, v2

    .line 2043926
    goto :goto_0

    .line 2043927
    :cond_3
    iget-object v0, p0, LX/DpH;->thread_id:Ljava/lang/Long;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2043928
    :goto_2
    iget-object v3, p1, LX/DpH;->thread_id:Ljava/lang/Long;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2043929
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2043930
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043931
    iget-object v0, p0, LX/DpH;->thread_id:Ljava/lang/Long;

    iget-object v3, p1, LX/DpH;->thread_id:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043932
    :cond_5
    iget-object v0, p0, LX/DpH;->user_devices:LX/DpR;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2043933
    :goto_4
    iget-object v3, p1, LX/DpH;->user_devices:LX/DpR;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2043934
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2043935
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043936
    iget-object v0, p0, LX/DpH;->user_devices:LX/DpR;

    iget-object v3, p1, LX/DpH;->user_devices:LX/DpR;

    invoke-virtual {v0, v3}, LX/DpR;->a(LX/DpR;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 2043937
    goto :goto_1

    :cond_8
    move v0, v2

    .line 2043938
    goto :goto_2

    :cond_9
    move v3, v2

    .line 2043939
    goto :goto_3

    :cond_a
    move v0, v2

    .line 2043940
    goto :goto_4

    :cond_b
    move v3, v2

    .line 2043941
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2043919
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2043916
    sget-boolean v0, LX/DpH;->a:Z

    .line 2043917
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpH;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2043918
    return-object v0
.end method
