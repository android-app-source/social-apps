.class public LX/ErA;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Er9;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172584
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2172585
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/Blb;Ljava/util/Set;IZLX/0Px;LX/0Px;ZLX/0Px;ZLX/Eqe;LX/EqG;LX/1OX;LX/1OX;LX/0gc;)LX/Er9;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "LX/Blb;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;IZ",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;Z",
            "Lcom/facebook/events/invite/EventsExtendedInviteFragment$AddContactsButtonClickListener;",
            "Lcom/facebook/events/invite/EventsExtendedInviteFriendSelectionChangedListener;",
            "LX/1OX;",
            "LX/1OX;",
            "LX/0gc;",
            ")",
            "LX/Er9;"
        }
    .end annotation

    .prologue
    .line 2172586
    new-instance v1, LX/Er9;

    const-class v2, LX/Eqy;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Eqy;

    invoke-static/range {p0 .. p0}, LX/ErU;->a(LX/0QB;)LX/ErU;

    move-result-object v3

    check-cast v3, LX/ErU;

    invoke-static/range {p0 .. p0}, LX/ErW;->a(LX/0QB;)LX/ErW;

    move-result-object v4

    check-cast v4, LX/ErW;

    invoke-static/range {p0 .. p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v5

    check-cast v5, LX/0iA;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v7

    check-cast v7, LX/0jo;

    const/16 v8, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/3kp;->a(LX/0QB;)LX/3kp;

    move-result-object v9

    check-cast v9, LX/3kp;

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move/from16 v13, p4

    move/from16 v14, p5

    move-object/from16 v15, p6

    move-object/from16 v16, p7

    move/from16 v17, p8

    move-object/from16 v18, p9

    move/from16 v19, p10

    move-object/from16 v20, p11

    move-object/from16 v21, p12

    move-object/from16 v22, p13

    move-object/from16 v23, p14

    move-object/from16 v24, p15

    invoke-direct/range {v1 .. v24}, LX/Er9;-><init>(LX/Eqy;LX/ErU;LX/ErW;LX/0iA;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0jo;LX/0Or;LX/3kp;Landroid/view/ViewGroup;LX/Blb;Ljava/util/Set;IZLX/0Px;LX/0Px;ZLX/0Px;ZLX/Eqe;LX/EqG;LX/1OX;LX/1OX;LX/0gc;)V

    .line 2172587
    return-object v1
.end method
