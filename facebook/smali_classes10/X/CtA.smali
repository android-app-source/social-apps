.class public LX/CtA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/CoJ;

.field public final c:LX/1P1;

.field private final d:LX/CtK;

.field public e:I

.field public f:I

.field public g:I

.field private h:Landroid/view/View;

.field public i:I

.field public j:[I

.field public k:F


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1P1;LX/CoJ;)V
    .locals 1

    .prologue
    .line 1944088
    new-instance v0, LX/CtK;

    invoke-direct {v0, p1}, LX/CtK;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, p3, v0}, LX/CtA;-><init>(Landroid/content/Context;LX/1P1;LX/CoJ;LX/CtK;)V

    .line 1944089
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/1P1;LX/CoJ;LX/CtK;)V
    .locals 2

    .prologue
    .line 1944072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1944073
    iput-object p1, p0, LX/CtA;->a:Landroid/content/Context;

    .line 1944074
    iput-object p2, p0, LX/CtA;->c:LX/1P1;

    .line 1944075
    iput-object p3, p0, LX/CtA;->b:LX/CoJ;

    .line 1944076
    const/4 v0, 0x0

    iput v0, p0, LX/CtA;->i:I

    .line 1944077
    iput-object p4, p0, LX/CtA;->d:LX/CtK;

    .line 1944078
    iget-object v0, p0, LX/CtA;->b:LX/CoJ;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/CtA;->j:[I

    .line 1944079
    iget-object v0, p0, LX/CtA;->j:[I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 1944080
    iget-object v0, p0, LX/CtA;->b:LX/CoJ;

    new-instance v1, LX/Ct9;

    invoke-direct {v1, p0}, LX/Ct9;-><init>(LX/CtA;)V

    invoke-virtual {v0, v1}, LX/1OM;->a(LX/1OD;)V

    .line 1944081
    return-void
.end method

.method public static a(LX/CtA;LX/Clr;)I
    .locals 2

    .prologue
    .line 1944082
    iget-object v0, p0, LX/CtA;->d:LX/CtK;

    .line 1944083
    invoke-interface {p1}, LX/Clr;->lx_()I

    move-result v1

    .line 1944084
    iget-object p0, v0, LX/CtK;->c:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1944085
    iget-object v1, v0, LX/CtK;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/Clr;->lx_()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1944086
    :goto_0
    move v0, v1

    .line 1944087
    return v0

    :cond_0
    iget v1, v0, LX/CtK;->d:I

    goto :goto_0
.end method

.method public static d(LX/CtA;)V
    .locals 8

    .prologue
    .line 1944042
    iget-object v0, p0, LX/CtA;->c:LX/1P1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    .line 1944043
    if-nez v0, :cond_1

    .line 1944044
    :cond_0
    :goto_0
    return-void

    .line 1944045
    :cond_1
    invoke-static {v0}, LX/1OR;->j(Landroid/view/View;)I

    move-result v1

    .line 1944046
    iget-object v2, p0, LX/CtA;->h:Landroid/view/View;

    if-ne v2, v0, :cond_2

    iget v2, p0, LX/CtA;->i:I

    if-eq v2, v1, :cond_0

    .line 1944047
    :cond_2
    iput-object v0, p0, LX/CtA;->h:Landroid/view/View;

    .line 1944048
    iput v1, p0, LX/CtA;->i:I

    .line 1944049
    const/4 v2, 0x0

    .line 1944050
    invoke-static {v0}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    .line 1944051
    iget-object v3, p0, LX/CtA;->c:LX/1P1;

    invoke-virtual {v3}, LX/1OR;->v()I

    move-result v3

    add-int/2addr v3, v1

    add-int/lit8 v4, v3, -0x1

    move v3, v1

    .line 1944052
    :goto_1
    if-gt v3, v4, :cond_3

    .line 1944053
    :try_start_0
    iget-object v5, p0, LX/CtA;->j:[I

    iget-object v6, p0, LX/CtA;->c:LX/1P1;

    sub-int v7, v3, v1

    invoke-virtual {v6, v7}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v6

    invoke-static {v6}, LX/1OR;->h(Landroid/view/View;)I

    move-result v6

    aput v6, v5, v3
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1944054
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v3, v2

    .line 1944055
    :goto_3
    if-ge v3, v1, :cond_4

    .line 1944056
    iget-object v4, p0, LX/CtA;->j:[I

    aget v4, v4, v3

    add-int/2addr v4, v2

    .line 1944057
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_3

    .line 1944058
    :cond_4
    iget v3, p0, LX/CtA;->i:I

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/2addr v3, v2

    .line 1944059
    :goto_4
    iget-object v4, p0, LX/CtA;->j:[I

    array-length v4, v4

    if-ge v1, v4, :cond_6

    .line 1944060
    iget-object v4, p0, LX/CtA;->j:[I

    aget v4, v4, v1

    const/high16 v5, -0x80000000

    if-ne v4, v5, :cond_5

    .line 1944061
    iget-object v4, p0, LX/CtA;->j:[I

    iget-object v5, p0, LX/CtA;->b:LX/CoJ;

    invoke-virtual {v5, v1}, LX/CoJ;->e(I)LX/Clr;

    move-result-object v5

    invoke-static {p0, v5}, LX/CtA;->a(LX/CtA;LX/Clr;)I

    move-result v5

    aput v5, v4, v1

    .line 1944062
    :cond_5
    iget-object v4, p0, LX/CtA;->j:[I

    aget v4, v4, v1

    add-int/2addr v2, v4

    .line 1944063
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1944064
    :cond_6
    iget-object v1, p0, LX/CtA;->c:LX/1P1;

    invoke-virtual {v1}, LX/1OR;->x()I

    move-result v1

    iput v1, p0, LX/CtA;->g:I

    .line 1944065
    int-to-float v1, v3

    int-to-float v4, v2

    div-float/2addr v1, v4

    .line 1944066
    iget v4, p0, LX/CtA;->f:I

    if-le v3, v4, :cond_7

    iget v4, p0, LX/CtA;->k:F

    cmpg-float v4, v1, v4

    if-ltz v4, :cond_8

    .line 1944067
    :cond_7
    iput v3, p0, LX/CtA;->f:I

    .line 1944068
    iput v2, p0, LX/CtA;->e:I

    .line 1944069
    iput v1, p0, LX/CtA;->k:F

    .line 1944070
    :cond_8
    goto/16 :goto_0

    :catch_0
    goto :goto_2
.end method


# virtual methods
.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1944071
    iget-object v0, p0, LX/CtA;->a:Landroid/content/Context;

    return-object v0
.end method
