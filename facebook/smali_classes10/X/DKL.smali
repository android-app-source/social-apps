.class public LX/DKL;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/DKM;",
        "LX/DKK;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DKL;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1986883
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1986884
    return-void
.end method

.method public static a(LX/0QB;)LX/DKL;
    .locals 3

    .prologue
    .line 1986885
    sget-object v0, LX/DKL;->a:LX/DKL;

    if-nez v0, :cond_1

    .line 1986886
    const-class v1, LX/DKL;

    monitor-enter v1

    .line 1986887
    :try_start_0
    sget-object v0, LX/DKL;->a:LX/DKL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1986888
    if-eqz v2, :cond_0

    .line 1986889
    :try_start_1
    new-instance v0, LX/DKL;

    invoke-direct {v0}, LX/DKL;-><init>()V

    .line 1986890
    move-object v0, v0

    .line 1986891
    sput-object v0, LX/DKL;->a:LX/DKL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1986892
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1986893
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1986894
    :cond_1
    sget-object v0, LX/DKL;->a:LX/DKL;

    return-object v0

    .line 1986895
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1986896
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
