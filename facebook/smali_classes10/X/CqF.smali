.class public LX/CqF;
.super LX/1UI;
.source ""

# interfaces
.implements LX/CqE;


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>(LX/1UG;Landroid/view/ViewGroup;Ljava/lang/Integer;LX/0Wd;)V
    .locals 1
    .param p1    # LX/1UG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1939478
    invoke-direct {p0, p1, p2, p3, p4}, LX/1UI;-><init>(LX/1UG;Landroid/view/ViewGroup;Ljava/lang/Integer;LX/0Wd;)V

    .line 1939479
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CqF;->c:Z

    .line 1939480
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 0

    .prologue
    .line 1939481
    return-void
.end method

.method public final f()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1939482
    iget-object v1, p0, LX/1UI;->b:LX/0YU;

    invoke-virtual {v1}, LX/0YU;->a()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1939483
    iget-object v3, p0, LX/1UI;->b:LX/0YU;

    invoke-virtual {v3, v1}, LX/0YU;->e(I)I

    move-result v3

    invoke-virtual {p0, v3}, LX/1UI;->c(I)LX/1df;

    move-result-object v3

    .line 1939484
    iput v0, v3, LX/1df;->c:I

    .line 1939485
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1939486
    :cond_0
    iget-object v1, p0, LX/1UI;->a:LX/1UG;

    invoke-interface {v1}, LX/1UG;->ij_()I

    move-result v2

    .line 1939487
    invoke-virtual {p0}, LX/1UI;->e()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_1

    .line 1939488
    invoke-virtual {p0, v0}, LX/1UI;->b(I)V

    .line 1939489
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1939490
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0}, LX/1UI;->e()I

    move-result v0

    :goto_2
    if-ge v0, v2, :cond_3

    .line 1939491
    iget-object v3, p0, LX/1UI;->a:LX/1UG;

    add-int/lit8 v4, v1, -0x1

    invoke-interface {v3, v4}, LX/1UG;->getItemViewType(I)I

    move-result v3

    .line 1939492
    invoke-virtual {p0, v3}, LX/1UI;->c(I)LX/1df;

    move-result-object v4

    .line 1939493
    sget-object v5, LX/CoL;->R:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1939494
    iget v3, v4, LX/1df;->c:I

    move v3, v3

    .line 1939495
    add-int/lit8 v3, v3, -0x1

    .line 1939496
    iput v3, v4, LX/1df;->c:I

    .line 1939497
    :cond_2
    invoke-virtual {p0, v0}, LX/1UI;->b(I)V

    .line 1939498
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1939499
    :cond_3
    return-void
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1939500
    iget-boolean v0, p0, LX/CqF;->c:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, LX/1UI;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1939501
    iget-boolean v0, p0, LX/CqF;->c:Z

    if-nez v0, :cond_0

    .line 1939502
    invoke-virtual {p0}, LX/1UI;->c()V

    .line 1939503
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CqF;->c:Z

    .line 1939504
    :goto_0
    return-void

    .line 1939505
    :cond_0
    invoke-virtual {p0}, LX/1UI;->g()V

    goto :goto_0
.end method
