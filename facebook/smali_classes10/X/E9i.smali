.class public LX/E9i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/E9i;


# instance fields
.field public final a:LX/17W;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/BNM;

.field public final d:LX/EAh;


# direct methods
.method public constructor <init>(LX/17W;Landroid/content/res/Resources;LX/BNM;LX/EAh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2084803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2084804
    iput-object p1, p0, LX/E9i;->a:LX/17W;

    .line 2084805
    iput-object p2, p0, LX/E9i;->b:Landroid/content/res/Resources;

    .line 2084806
    iput-object p3, p0, LX/E9i;->c:LX/BNM;

    .line 2084807
    iput-object p4, p0, LX/E9i;->d:LX/EAh;

    .line 2084808
    return-void
.end method

.method public static a(LX/0QB;)LX/E9i;
    .locals 7

    .prologue
    .line 2084762
    sget-object v0, LX/E9i;->e:LX/E9i;

    if-nez v0, :cond_1

    .line 2084763
    const-class v1, LX/E9i;

    monitor-enter v1

    .line 2084764
    :try_start_0
    sget-object v0, LX/E9i;->e:LX/E9i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2084765
    if-eqz v2, :cond_0

    .line 2084766
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2084767
    new-instance p0, LX/E9i;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/BNM;->a(LX/0QB;)LX/BNM;

    move-result-object v5

    check-cast v5, LX/BNM;

    invoke-static {v0}, LX/EAh;->a(LX/0QB;)LX/EAh;

    move-result-object v6

    check-cast v6, LX/EAh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/E9i;-><init>(LX/17W;Landroid/content/res/Resources;LX/BNM;LX/EAh;)V

    .line 2084768
    move-object v0, p0

    .line 2084769
    sput-object v0, LX/E9i;->e:LX/E9i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2084770
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2084771
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2084772
    :cond_1
    sget-object v0, LX/E9i;->e:LX/E9i;

    return-object v0

    .line 2084773
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2084774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/reviews/ui/UserPlacesToReviewView;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2084775
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->e()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->e()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2084776
    :cond_0
    const/4 v0, 0x0

    .line 2084777
    :goto_0
    move-object v0, v0

    .line 2084778
    if-nez v0, :cond_4

    move-object v0, v1

    .line 2084779
    :goto_1
    if-nez v0, :cond_6

    .line 2084780
    iget-object v2, p1, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02147e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2084781
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2084782
    iget-object v2, p1, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2084783
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->b()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->b()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2084784
    :cond_1
    iget-object v0, p1, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2084785
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->j()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;

    move-result-object v2

    if-nez v2, :cond_7

    const/4 v2, 0x0

    .line 2084786
    :goto_3
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2084787
    if-lez v2, :cond_2

    .line 2084788
    iget-object v4, p0, LX/E9i;->c:LX/BNM;

    iget-object v5, p0, LX/E9i;->b:Landroid/content/res/Resources;

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v4, v2, v5}, LX/BNM;->a(II)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2084789
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->bj_()LX/175;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->bj_()LX/175;

    move-result-object v2

    invoke-interface {v2}, LX/175;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2084790
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->bj_()LX/175;

    move-result-object v2

    invoke-interface {v2}, LX/175;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2084791
    :cond_3
    move-object v0, v3

    .line 2084792
    iget-object v1, p1, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2084793
    new-instance v0, LX/E9g;

    invoke-direct {v0, p0, p2}, LX/E9g;-><init>(LX/E9i;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;)V

    move-object v0, v0

    .line 2084794
    iget-object v1, p1, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->o:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084795
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    .line 2084796
    const/4 v0, 0x0

    .line 2084797
    :goto_4
    move-object v0, v0

    .line 2084798
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084799
    return-void

    .line 2084800
    :cond_4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->e()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ProfilePhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2084801
    :cond_6
    iget-object v2, p1, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/reviews/ui/UserPlacesToReviewView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_2

    .line 2084802
    :cond_7
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->j()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel$ViewerStarRatingModel;->a()D

    move-result-wide v2

    double-to-int v2, v2

    goto :goto_3

    :cond_8
    new-instance v0, LX/E9h;

    invoke-direct {v0, p0, p2}, LX/E9h;-><init>(LX/E9i;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;)V

    goto :goto_4
.end method
