.class public final LX/DWU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;)LX/15i;
    .locals 15

    .prologue
    .line 2007479
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2007480
    const/4 v10, 0x0

    .line 2007481
    const-wide/16 v8, 0x0

    .line 2007482
    const/4 v7, 0x0

    .line 2007483
    const/4 v6, 0x0

    .line 2007484
    const/4 v5, 0x0

    .line 2007485
    const/4 v4, 0x0

    .line 2007486
    const/4 v3, 0x0

    .line 2007487
    const/4 v2, 0x0

    .line 2007488
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 2007489
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2007490
    const/4 v2, 0x0

    .line 2007491
    :goto_0
    move v1, v2

    .line 2007492
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2007493
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 2007494
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v7, :cond_8

    .line 2007495
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2007496
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2007497
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v13, :cond_0

    if-eqz v3, :cond_0

    .line 2007498
    const-string v7, "bylines"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2007499
    invoke-static {p0, v0}, LX/DWT;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 2007500
    :cond_1
    const-string v7, "communicationRank"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2007501
    const/4 v2, 0x1

    .line 2007502
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    goto :goto_1

    .line 2007503
    :cond_2
    const-string v7, "id"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2007504
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 2007505
    :cond_3
    const-string v7, "name"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2007506
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 2007507
    :cond_4
    const-string v7, "name_search_tokens"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2007508
    invoke-static {p0, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 2007509
    :cond_5
    const-string v7, "profilePicture50"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2007510
    invoke-static {p0, v0}, LX/DWV;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto :goto_1

    .line 2007511
    :cond_6
    const-string v7, "structured_name"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2007512
    invoke-static {p0, v0}, LX/DWS;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 2007513
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2007514
    :cond_8
    const/4 v3, 0x7

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2007515
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2007516
    if-eqz v2, :cond_9

    .line 2007517
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object v2, v0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2007518
    :cond_9
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2007519
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2007520
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2007521
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2007522
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2007523
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v11, v6

    move v12, v7

    move v6, v10

    move v10, v5

    move v14, v4

    move-wide v4, v8

    move v9, v14

    move v8, v3

    goto/16 :goto_1
.end method
