.class public LX/DP3;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DP2;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DP4;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1992990
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DP3;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DP4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992991
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1992992
    iput-object p1, p0, LX/DP3;->b:LX/0Ot;

    .line 1992993
    return-void
.end method

.method public static a(LX/0QB;)LX/DP3;
    .locals 4

    .prologue
    .line 1992994
    const-class v1, LX/DP3;

    monitor-enter v1

    .line 1992995
    :try_start_0
    sget-object v0, LX/DP3;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1992996
    sput-object v2, LX/DP3;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1992997
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1992998
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1992999
    new-instance v3, LX/DP3;

    const/16 p0, 0x2417

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DP3;-><init>(LX/0Ot;)V

    .line 1993000
    move-object v0, v3

    .line 1993001
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1993002
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DP3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1993003
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1993004
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1993005
    iget-object v0, p0, LX/DP3;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DP4;

    .line 1993006
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x3

    const/16 p0, 0xc

    invoke-interface {v1, v2, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    const/16 p0, 0x8

    invoke-interface {v1, v2, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    iget-object v2, v0, LX/DP4;->b:LX/2g9;

    invoke-virtual {v2, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v2

    const/16 p0, 0x18

    invoke-virtual {v2, p0}, LX/2gA;->h(I)LX/2gA;

    move-result-object v2

    const p0, 0x7f081b49

    invoke-virtual {v2, p0}, LX/2gA;->i(I)LX/2gA;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x6

    const p2, 0x7f0b11df

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    .line 1993007
    const p0, 0x26d60b68

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1993008
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    iget-object v2, v0, LX/DP4;->b:LX/2g9;

    invoke-virtual {v2, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v2

    const/16 p0, 0x108

    invoke-virtual {v2, p0}, LX/2gA;->h(I)LX/2gA;

    move-result-object v2

    const p0, 0x7f081b4b

    invoke-virtual {v2, p0}, LX/2gA;->i(I)LX/2gA;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 1993009
    const p0, 0x26d610bc

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1993010
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1993011
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1993012
    invoke-static {}, LX/1dS;->b()V

    .line 1993013
    iget v0, p1, LX/1dQ;->b:I

    .line 1993014
    sparse-switch v0, :sswitch_data_0

    .line 1993015
    :goto_0
    return-object v2

    .line 1993016
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1993017
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1993018
    check-cast v1, LX/DP1;

    .line 1993019
    iget-object p1, p0, LX/DP3;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DP4;

    iget-object p2, v1, LX/DP1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1993020
    iget-object p0, p1, LX/DP4;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DOl;

    invoke-virtual {p0, p2}, LX/DOl;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1993021
    goto :goto_0

    .line 1993022
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1993023
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1993024
    check-cast v1, LX/DP1;

    .line 1993025
    iget-object p1, p0, LX/DP3;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DP4;

    iget-object p2, v1, LX/DP1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1993026
    iget-object p0, p1, LX/DP4;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DOl;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, LX/DOl;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 1993027
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x26d60b68 -> :sswitch_0
        0x26d610bc -> :sswitch_1
    .end sparse-switch
.end method
