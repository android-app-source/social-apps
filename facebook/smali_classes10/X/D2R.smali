.class public final LX/D2R;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/5w0;

.field public c:J

.field public d:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

.field public e:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1958668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958669
    new-instance v0, LX/5w0;

    invoke-direct {v0}, LX/5w0;-><init>()V

    iput-object v0, p0, LX/D2R;->b:LX/5w0;

    .line 1958670
    return-void
.end method

.method public constructor <init>(LX/D2S;)V
    .locals 3

    .prologue
    .line 1958675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958676
    iget-object v0, p1, LX/D2S;->a:Ljava/lang/String;

    iput-object v0, p0, LX/D2R;->a:Ljava/lang/String;

    .line 1958677
    iget-object v0, p1, LX/D2S;->b:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    .line 1958678
    new-instance v1, LX/5w0;

    invoke-direct {v1}, LX/5w0;-><init>()V

    .line 1958679
    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/5w0;->a:Ljava/lang/String;

    .line 1958680
    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/5w0;->b:Ljava/lang/String;

    .line 1958681
    move-object v0, v1

    .line 1958682
    iput-object v0, p0, LX/D2R;->b:LX/5w0;

    .line 1958683
    iget-wide v0, p1, LX/D2S;->c:J

    iput-wide v0, p0, LX/D2R;->c:J

    .line 1958684
    iget-object v0, p1, LX/D2S;->d:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, LX/D2R;->d:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1958685
    iget v0, p1, LX/D2S;->e:F

    iput v0, p0, LX/D2R;->e:F

    .line 1958686
    return-void
.end method


# virtual methods
.method public final a(J)LX/D2R;
    .locals 1

    .prologue
    .line 1958687
    iput-wide p1, p0, LX/D2R;->c:J

    .line 1958688
    return-object p0
.end method

.method public final a()LX/D2S;
    .locals 2

    .prologue
    .line 1958674
    new-instance v0, LX/D2S;

    invoke-direct {v0, p0}, LX/D2S;-><init>(LX/D2R;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/D2R;
    .locals 1

    .prologue
    .line 1958671
    iget-object v0, p0, LX/D2R;->b:LX/5w0;

    .line 1958672
    iput-object p1, v0, LX/5w0;->a:Ljava/lang/String;

    .line 1958673
    return-object p0
.end method
