.class public LX/CuM;
.super Lcom/facebook/video/player/plugins/Video360Plugin;
.source ""


# instance fields
.field public s:LX/Cqt;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1946598
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/CuM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1946599
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1946596
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/CuM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946597
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1946586
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946587
    return-void
.end method

.method public static setTextureOrientation(LX/CuM;LX/7Cm;)V
    .locals 2

    .prologue
    .line 1946591
    iget-object v0, p0, Lcom/facebook/video/player/plugins/VideoPlugin;->q:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->g()LX/2qW;

    move-result-object v0

    .line 1946592
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/7EB;

    if-eqz v1, :cond_0

    .line 1946593
    check-cast v0, LX/7EB;

    .line 1946594
    invoke-virtual {v0, p1}, LX/2qW;->setRenderAxisRotation(LX/7Cm;)V

    .line 1946595
    :cond_0
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 1

    .prologue
    .line 1946588
    sget-object v0, LX/7Cm;->RENDER_AXIS_DEFAULT:LX/7Cm;

    invoke-static {p0, v0}, LX/CuM;->setTextureOrientation(LX/CuM;LX/7Cm;)V

    .line 1946589
    invoke-super {p0}, Lcom/facebook/video/player/plugins/Video360Plugin;->d()V

    .line 1946590
    return-void
.end method
