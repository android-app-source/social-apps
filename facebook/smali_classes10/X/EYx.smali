.class public final LX/EYx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWa;


# instance fields
.field public final synthetic a:LX/EYy;

.field private b:I

.field private final c:I


# direct methods
.method public constructor <init>(LX/EYy;)V
    .locals 1

    .prologue
    .line 2138230
    iput-object p1, p0, LX/EYx;->a:LX/EYy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138231
    const/4 v0, 0x0

    iput v0, p0, LX/EYx;->b:I

    .line 2138232
    invoke-virtual {p1}, LX/EYy;->b()I

    move-result v0

    iput v0, p0, LX/EYx;->c:I

    .line 2138233
    return-void
.end method


# virtual methods
.method public final a()B
    .locals 3

    .prologue
    .line 2138227
    :try_start_0
    iget-object v0, p0, LX/EYx;->a:LX/EYy;

    iget-object v0, v0, LX/EYy;->c:[B

    iget v1, p0, LX/EYx;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/EYx;->b:I

    aget-byte v0, v0, v1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 2138228
    :catch_0
    move-exception v0

    .line 2138229
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 2138226
    iget v0, p0, LX/EYx;->b:I

    iget v1, p0, LX/EYx;->c:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2138224
    invoke-virtual {p0}, LX/EYx;->a()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 2138225
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
