.class public final enum LX/Ehr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ehr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ehr;

.field public static final enum CELLULAR:LX/Ehr;

.field public static final enum NOCONN:LX/Ehr;

.field public static final enum OTHER:LX/Ehr;

.field public static final enum WIFI:LX/Ehr;


# instance fields
.field public value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2158845
    new-instance v0, LX/Ehr;

    const-string v1, "NOCONN"

    invoke-direct {v0, v1, v2, v2}, LX/Ehr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Ehr;->NOCONN:LX/Ehr;

    new-instance v0, LX/Ehr;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v3, v3}, LX/Ehr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Ehr;->WIFI:LX/Ehr;

    new-instance v0, LX/Ehr;

    const-string v1, "CELLULAR"

    invoke-direct {v0, v1, v4, v4}, LX/Ehr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Ehr;->CELLULAR:LX/Ehr;

    new-instance v0, LX/Ehr;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v5, v5}, LX/Ehr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Ehr;->OTHER:LX/Ehr;

    .line 2158846
    const/4 v0, 0x4

    new-array v0, v0, [LX/Ehr;

    sget-object v1, LX/Ehr;->NOCONN:LX/Ehr;

    aput-object v1, v0, v2

    sget-object v1, LX/Ehr;->WIFI:LX/Ehr;

    aput-object v1, v0, v3

    sget-object v1, LX/Ehr;->CELLULAR:LX/Ehr;

    aput-object v1, v0, v4

    sget-object v1, LX/Ehr;->OTHER:LX/Ehr;

    aput-object v1, v0, v5

    sput-object v0, LX/Ehr;->$VALUES:[LX/Ehr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2158847
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2158848
    iput p3, p0, LX/Ehr;->value:I

    .line 2158849
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ehr;
    .locals 1

    .prologue
    .line 2158850
    const-class v0, LX/Ehr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ehr;

    return-object v0
.end method

.method public static values()[LX/Ehr;
    .locals 1

    .prologue
    .line 2158851
    sget-object v0, LX/Ehr;->$VALUES:[LX/Ehr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ehr;

    return-object v0
.end method
