.class public final LX/CkI;
.super LX/CkH;
.source ""


# instance fields
.field public final synthetic a:LX/CkJ;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cjt;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cjt;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "LX/Cjt;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/Cjt;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1930544
    iput-object p1, p0, LX/CkI;->a:LX/CkJ;

    .line 1930545
    invoke-direct {p0, p1, p2, p3}, LX/CkH;-><init>(LX/CkJ;II)V

    .line 1930546
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CkI;->c:Ljava/util/Map;

    .line 1930547
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CkI;->d:Ljava/util/Map;

    .line 1930548
    iget-object v0, p0, LX/CkI;->c:Ljava/util/Map;

    invoke-direct {p0, p4, v0}, LX/CkI;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 1930549
    iget-object v0, p0, LX/CkI;->d:Ljava/util/Map;

    invoke-direct {p0, p5, v0}, LX/CkI;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 1930550
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "LX/Cjt;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "LX/Cjt;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1930551
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1930552
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1930553
    const/4 v2, 0x0

    .line 1930554
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1930555
    iget-object v1, p0, LX/CkI;->a:LX/CkJ;

    iget-object v2, v1, LX/CkJ;->i:LX/Cju;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v2, v1}, LX/Cju;->c(I)I

    move-result v1

    .line 1930556
    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1930557
    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/Cjt;)I
    .locals 1

    .prologue
    .line 1930558
    iget-object v0, p0, LX/CkI;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1930559
    iget-object v0, p0, LX/CkI;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1930560
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/CkH;->a(LX/Cjt;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/Cjt;)I
    .locals 1

    .prologue
    .line 1930561
    iget-object v0, p0, LX/CkI;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1930562
    iget-object v0, p0, LX/CkI;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1930563
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/CkH;->b(LX/Cjt;)I

    move-result v0

    goto :goto_0
.end method
