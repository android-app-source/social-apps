.class public LX/E28;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E26;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E28;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E2D;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2072298
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E28;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E2D;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072295
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2072296
    iput-object p1, p0, LX/E28;->b:LX/0Ot;

    .line 2072297
    return-void
.end method

.method public static a(LX/0QB;)LX/E28;
    .locals 4

    .prologue
    .line 2072299
    sget-object v0, LX/E28;->c:LX/E28;

    if-nez v0, :cond_1

    .line 2072300
    const-class v1, LX/E28;

    monitor-enter v1

    .line 2072301
    :try_start_0
    sget-object v0, LX/E28;->c:LX/E28;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072302
    if-eqz v2, :cond_0

    .line 2072303
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072304
    new-instance v3, LX/E28;

    const/16 p0, 0x30b9

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E28;-><init>(LX/0Ot;)V

    .line 2072305
    move-object v0, v3

    .line 2072306
    sput-object v0, LX/E28;->c:LX/E28;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072307
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072308
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072309
    :cond_1
    sget-object v0, LX/E28;->c:LX/E28;

    return-object v0

    .line 2072310
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072311
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2072287
    check-cast p2, LX/E27;

    .line 2072288
    iget-object v0, p0, LX/E28;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2D;

    iget-object v1, p2, LX/E27;->a:LX/9qL;

    .line 2072289
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x0

    invoke-interface {v2, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    .line 2072290
    invoke-interface {v1}, LX/9qL;->b()LX/174;

    move-result-object p0

    invoke-interface {v1}, LX/9qL;->c()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object p2

    invoke-static {v0, p1, v2, p0, p2}, LX/E2D;->a(LX/E2D;LX/1De;LX/1Dh;LX/174;Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)V

    .line 2072291
    invoke-interface {v1}, LX/9qL;->d()LX/174;

    move-result-object p0

    invoke-interface {v1}, LX/9qL;->e()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object p2

    invoke-static {v0, p1, v2, p0, p2}, LX/E2D;->a(LX/E2D;LX/1De;LX/1Dh;LX/174;Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)V

    .line 2072292
    invoke-interface {v1}, LX/9qL;->hk_()LX/174;

    move-result-object p0

    invoke-interface {v1}, LX/9qL;->hj_()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object p2

    invoke-static {v0, p1, v2, p0, p2}, LX/E2D;->a(LX/E2D;LX/1De;LX/1Dh;LX/174;Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)V

    .line 2072293
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2072294
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2072285
    invoke-static {}, LX/1dS;->b()V

    .line 2072286
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/E26;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2072277
    new-instance v1, LX/E27;

    invoke-direct {v1, p0}, LX/E27;-><init>(LX/E28;)V

    .line 2072278
    sget-object v2, LX/E28;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E26;

    .line 2072279
    if-nez v2, :cond_0

    .line 2072280
    new-instance v2, LX/E26;

    invoke-direct {v2}, LX/E26;-><init>()V

    .line 2072281
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/E26;->a$redex0(LX/E26;LX/1De;IILX/E27;)V

    .line 2072282
    move-object v1, v2

    .line 2072283
    move-object v0, v1

    .line 2072284
    return-object v0
.end method
