.class public LX/Ee4;
.super LX/Ee3;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final c:Ljava/lang/String;

.field private static final d:I

.field private static final e:Landroid/util/SparseIntArray;


# instance fields
.field private final f:[J

.field private final g:[J

.field private final h:[J

.field private i:Z

.field private final j:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[J>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2151884
    const-class v0, LX/Ee4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ee4;->c:Ljava/lang/String;

    .line 2151885
    const-string v0, "wlan0"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, LX/Ee4;->d:I

    .line 2151886
    new-instance v0, Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, LX/Ee4;->e:Landroid/util/SparseIntArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2151887
    invoke-direct {p0}, LX/Ee3;-><init>()V

    .line 2151888
    new-array v0, v1, [J

    iput-object v0, p0, LX/Ee4;->f:[J

    .line 2151889
    new-array v0, v1, [J

    iput-object v0, p0, LX/Ee4;->g:[J

    .line 2151890
    const/4 v0, 0x4

    new-array v0, v0, [J

    iput-object v0, p0, LX/Ee4;->h:[J

    .line 2151891
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ee4;->i:Z

    .line 2151892
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Ee4;->j:Landroid/util/SparseArray;

    .line 2151893
    invoke-virtual {p0}, LX/Ee4;->b()Z

    .line 2151894
    return-void
.end method

.method private static a(Ljava/io/BufferedReader;C)V
    .locals 2

    .prologue
    .line 2151895
    :cond_0
    invoke-virtual {p0}, Ljava/io/BufferedReader;->read()I

    move-result v0

    .line 2151896
    if-eq v0, p1, :cond_1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2151897
    :cond_1
    return-void
.end method

.method private a(Ljava/io/BufferedReader;)Z
    .locals 13
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x4

    const/4 v3, 0x1

    const/4 v12, -0x1

    const/4 v2, 0x0

    .line 2151898
    iget-object v0, p0, LX/Ee4;->g:[J

    const-wide/16 v4, 0x0

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 2151899
    const/16 v0, 0xa

    :try_start_0
    invoke-static {p1, v0}, LX/Ee4;->a(Ljava/io/BufferedReader;C)V

    .line 2151900
    :goto_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->ready()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2151901
    const/16 v0, 0x20

    invoke-static {p1, v0}, LX/Ee4;->a(Ljava/io/BufferedReader;C)V

    .line 2151902
    const/4 v0, 0x0

    .line 2151903
    :goto_1
    invoke-virtual {p1}, Ljava/io/BufferedReader;->read()I

    move-result v4

    .line 2151904
    const/16 v5, 0x20

    if-eq v4, v5, :cond_0

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 2151905
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v4

    goto :goto_1

    .line 2151906
    :cond_0
    move v4, v0

    .line 2151907
    const/16 v0, 0x20

    invoke-static {p1, v0}, LX/Ee4;->a(Ljava/io/BufferedReader;C)V

    .line 2151908
    invoke-static {p1}, LX/Ee4;->b(Ljava/io/BufferedReader;)J

    move-result-wide v6

    .line 2151909
    sget v0, LX/Ee3;->a:I

    int-to-long v8, v0

    cmp-long v0, v6, v8

    if-nez v0, :cond_3

    iget-boolean v0, p0, LX/Ee4;->i:Z

    if-nez v0, :cond_1

    sget v0, LX/Ee4;->d:I

    if-eq v4, v0, :cond_1

    sget-object v0, LX/Ee4;->e:Landroid/util/SparseIntArray;

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    if-eq v0, v12, :cond_3

    :cond_1
    move v0, v3

    .line 2151910
    :goto_2
    if-eqz v0, :cond_6

    .line 2151911
    const/16 v0, 0x20

    invoke-static {p1, v0}, LX/Ee4;->a(Ljava/io/BufferedReader;C)V

    .line 2151912
    iget-object v0, p0, LX/Ee4;->h:[J

    const/4 v5, 0x0

    invoke-static {p1}, LX/Ee4;->b(Ljava/io/BufferedReader;)J

    move-result-wide v6

    aput-wide v6, v0, v5

    .line 2151913
    iget-object v0, p0, LX/Ee4;->h:[J

    const/4 v5, 0x2

    invoke-static {p1}, LX/Ee4;->b(Ljava/io/BufferedReader;)J

    move-result-wide v6

    aput-wide v6, v0, v5

    .line 2151914
    iget-object v0, p0, LX/Ee4;->h:[J

    const/4 v5, 0x1

    invoke-static {p1}, LX/Ee4;->b(Ljava/io/BufferedReader;)J

    move-result-wide v6

    aput-wide v6, v0, v5

    .line 2151915
    iget-object v0, p0, LX/Ee4;->h:[J

    const/4 v5, 0x3

    invoke-static {p1}, LX/Ee4;->b(Ljava/io/BufferedReader;)J

    move-result-wide v6

    aput-wide v6, v0, v5

    .line 2151916
    sget v0, LX/Ee4;->d:I

    if-eq v4, v0, :cond_2

    sget-object v0, LX/Ee4;->e:Landroid/util/SparseIntArray;

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    if-eq v0, v12, :cond_5

    .line 2151917
    :cond_2
    sget v0, LX/Ee4;->d:I

    if-ne v4, v0, :cond_4

    move v0, v1

    :goto_3
    move v4, v2

    .line 2151918
    :goto_4
    if-ge v4, v1, :cond_6

    .line 2151919
    iget-object v5, p0, LX/Ee4;->g:[J

    or-int v6, v0, v4

    aget-wide v8, v5, v6

    iget-object v7, p0, LX/Ee4;->h:[J

    aget-wide v10, v7, v4

    add-long/2addr v8, v10

    aput-wide v8, v5, v6

    .line 2151920
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_3
    move v0, v2

    .line 2151921
    goto :goto_2

    :cond_4
    move v0, v2

    .line 2151922
    goto :goto_3

    .line 2151923
    :cond_5
    iget-object v0, p0, LX/Ee4;->j:Landroid/util/SparseArray;

    iget-object v5, p0, LX/Ee4;->h:[J

    iget-object v6, p0, LX/Ee4;->h:[J

    array-length v6, v6

    invoke-static {v5, v6}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151924
    :cond_6
    const/16 v0, 0xa

    invoke-static {p1, v0}, LX/Ee4;->a(Ljava/io/BufferedReader;C)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2151925
    :catch_0
    move-exception v0

    .line 2151926
    sget-object v1, LX/Ee4;->c:Ljava/lang/String;

    const-string v3, "Unable to parse stats"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2151927
    :goto_5
    return v2

    .line 2151928
    :cond_7
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, LX/Ee4;->i:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v2

    .line 2151929
    :goto_6
    const/16 v1, 0x8

    if-ge v0, v1, :cond_8

    .line 2151930
    iget-object v1, p0, LX/Ee3;->b:[J

    iget-object v4, p0, LX/Ee4;->g:[J

    aget-wide v4, v4, v0

    iget-object v6, p0, LX/Ee4;->f:[J

    aget-wide v6, v6, v0

    sub-long/2addr v4, v6

    aput-wide v4, v1, v0

    .line 2151931
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 2151932
    :cond_8
    iget-object v0, p0, LX/Ee4;->g:[J

    iget-object v1, p0, LX/Ee4;->f:[J

    const/16 v4, 0x8

    invoke-static {v0, v2, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v3

    .line 2151933
    goto :goto_5
.end method

.method private static b(Ljava/io/BufferedReader;)J
    .locals 6

    .prologue
    .line 2151934
    const-wide/16 v0, 0x0

    .line 2151935
    :goto_0
    invoke-virtual {p0}, Ljava/io/BufferedReader;->read()I

    move-result v2

    .line 2151936
    const/16 v3, 0x30

    if-lt v2, v3, :cond_0

    const/16 v3, 0x39

    if-gt v2, v3, :cond_0

    .line 2151937
    const-wide/16 v4, 0xa

    mul-long/2addr v0, v4

    add-int/lit8 v2, v2, -0x30

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0

    .line 2151938
    :cond_0
    return-wide v0
.end method

.method private k()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v7, -0x1

    .line 2151939
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 2151940
    :try_start_0
    const-class v0, Landroid/net/TrafficStats;

    const-string v3, "getMobileIfaces"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2151941
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2151942
    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2151943
    :goto_0
    move-object v4, v0

    .line 2151944
    if-eqz v4, :cond_1

    move v1, v2

    .line 2151945
    :goto_1
    array-length v0, v4

    if-ge v1, v0, :cond_1

    .line 2151946
    aget-object v0, v4, v1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2151947
    sget-object v3, LX/Ee4;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0, v7}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    if-ne v3, v7, :cond_0

    .line 2151948
    sget-object v3, LX/Ee4;->e:Landroid/util/SparseIntArray;

    aget-object v5, v4, v1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, Landroid/util/SparseIntArray;->append(II)V

    .line 2151949
    iget-object v3, p0, LX/Ee4;->j:Landroid/util/SparseArray;

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    .line 2151950
    if-eqz v0, :cond_0

    move v3, v2

    .line 2151951
    :goto_2
    const/4 v5, 0x4

    if-ge v3, v5, :cond_0

    .line 2151952
    iget-object v5, p0, LX/Ee4;->f:[J

    or-int/lit8 v6, v3, 0x0

    aget-wide v8, v5, v6

    aget-wide v10, v0, v3

    add-long/2addr v8, v10

    aput-wide v8, v5, v6

    .line 2151953
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2151954
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2151955
    :cond_1
    return-void

    .line 2151956
    :catch_0
    move-exception v0

    .line 2151957
    sget-object v3, LX/Ee4;->c:Ljava/lang/String;

    const-string v4, "Unable to read interfaces"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v0, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 2151958
    goto :goto_0
.end method


# virtual methods
.method public final b()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2151959
    invoke-direct {p0}, LX/Ee4;->k()V

    .line 2151960
    const/4 v3, 0x0

    .line 2151961
    :try_start_0
    new-instance v1, Ljava/io/FileReader;

    new-instance v2, Ljava/io/File;

    const-string v4, "/proc/net/xt_qtaguid/stats"

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    .line 2151962
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2151963
    :try_start_1
    invoke-direct {p0, v2}, LX/Ee4;->a(Ljava/io/BufferedReader;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 2151964
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2151965
    :cond_0
    :goto_0
    return v0

    .line 2151966
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 2151967
    :goto_1
    :try_start_3
    sget-object v3, LX/Ee4;->c:Ljava/lang/String;

    const-string v4, "Unable to find %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "/proc/net/xt_qtaguid/stats"

    aput-object v7, v5, v6

    invoke-static {v3, v1, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2151968
    if-eqz v2, :cond_0

    .line 2151969
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    goto :goto_0

    .line 2151970
    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_2
    if-eqz v2, :cond_1

    .line 2151971
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 2151972
    :cond_1
    :goto_3
    throw v0

    :catch_2
    goto :goto_0

    :catch_3
    goto :goto_3

    .line 2151973
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 2151974
    :catch_4
    move-exception v1

    goto :goto_1
.end method
