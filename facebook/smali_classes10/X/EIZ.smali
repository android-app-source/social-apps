.class public LX/EIZ;
.super LX/EH2;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0yH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2S7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/rtc/views/RtcIncomingCallButtons;

.field public e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2102161
    invoke-direct {p0, p1}, LX/EH2;-><init>(Landroid/content/Context;)V

    .line 2102162
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2102163
    const-class v0, LX/EIZ;

    invoke-static {v0, p0}, LX/EIZ;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2102164
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2102165
    const v1, 0x7f03161b

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2102166
    const v0, 0x7f0d31b3

    invoke-virtual {p0, v0}, LX/EH2;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EIZ;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2102167
    const v0, 0x7f0d31b4

    invoke-virtual {p0, v0}, LX/EH2;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;

    iput-object v0, p0, LX/EIZ;->d:Lcom/facebook/rtc/views/RtcIncomingCallButtons;

    .line 2102168
    iget-object v0, p0, LX/EIZ;->b:LX/0yH;

    sget-object v1, LX/0yY;->VOIP_CALL_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102169
    iget-object v0, p0, LX/EIZ;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2102170
    iget-object v0, p0, LX/EIZ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/EDJ;->c:LX/0Tn;

    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2102171
    :goto_0
    return-void

    .line 2102172
    :cond_0
    iget-object v0, p0, LX/EIZ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/EDJ;->c:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2102173
    iget-object v0, p0, LX/EIZ;->c:LX/2S7;

    const-string v1, "data_warning"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2102174
    iget-object v0, p0, LX/EIZ;->e:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080789

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2102175
    iget-object v0, p0, LX/EIZ;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2102176
    iget-object v0, p0, LX/EIZ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/EDJ;->c:LX/0Tn;

    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    .line 2102177
    :cond_1
    iget-object v0, p0, LX/EIZ;->e:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/EIZ;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v2

    check-cast v2, LX/0yH;

    invoke-static {p0}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object p0

    check-cast p0, LX/2S7;

    iput-object v1, p1, LX/EIZ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v2, p1, LX/EIZ;->b:LX/0yH;

    iput-object p0, p1, LX/EIZ;->c:LX/2S7;

    return-void
.end method
