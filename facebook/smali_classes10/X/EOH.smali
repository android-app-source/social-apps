.class public LX/EOH;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EOJ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EOH",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EOJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2114250
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2114251
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/EOH;->b:LX/0Zi;

    .line 2114252
    iput-object p1, p0, LX/EOH;->a:LX/0Ot;

    .line 2114253
    return-void
.end method

.method public static a(LX/0QB;)LX/EOH;
    .locals 4

    .prologue
    .line 2114239
    const-class v1, LX/EOH;

    monitor-enter v1

    .line 2114240
    :try_start_0
    sget-object v0, LX/EOH;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114241
    sput-object v2, LX/EOH;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114242
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114243
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114244
    new-instance v3, LX/EOH;

    const/16 p0, 0x345b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EOH;-><init>(LX/0Ot;)V

    .line 2114245
    move-object v0, v3

    .line 2114246
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114247
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114248
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114249
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2114254
    check-cast p2, LX/EOG;

    .line 2114255
    iget-object v0, p0, LX/EOH;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EOJ;

    iget-object v1, p2, LX/EOG;->a:LX/CzL;

    iget-object v2, p2, LX/EOG;->b:LX/0Px;

    iget-object v3, p2, LX/EOG;->c:LX/1Ps;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/EOJ;->a(LX/1De;LX/CzL;LX/0Px;LX/1Ps;)LX/1Dg;

    move-result-object v0

    .line 2114256
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2114237
    invoke-static {}, LX/1dS;->b()V

    .line 2114238
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/EOF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/EOH",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2114229
    new-instance v1, LX/EOG;

    invoke-direct {v1, p0}, LX/EOG;-><init>(LX/EOH;)V

    .line 2114230
    iget-object v2, p0, LX/EOH;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EOF;

    .line 2114231
    if-nez v2, :cond_0

    .line 2114232
    new-instance v2, LX/EOF;

    invoke-direct {v2, p0}, LX/EOF;-><init>(LX/EOH;)V

    .line 2114233
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EOF;->a$redex0(LX/EOF;LX/1De;IILX/EOG;)V

    .line 2114234
    move-object v1, v2

    .line 2114235
    move-object v0, v1

    .line 2114236
    return-object v0
.end method
