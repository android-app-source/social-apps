.class public final LX/Eyl;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/util/List",
        "<+",
        "LX/2lr;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V
    .locals 0

    .prologue
    .line 2186510
    iput-object p1, p0, LX/Eyl;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iput-boolean p2, p0, LX/Eyl;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2186511
    iget-object v0, p0, LX/Eyl;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    .line 2186512
    invoke-static {v0, p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Ljava/lang/Throwable;)V

    .line 2186513
    iget-object v1, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2186514
    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->E(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 2186515
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2186516
    check-cast p1, Ljava/util/List;

    .line 2186517
    iget-object v0, p0, LX/Eyl;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-boolean v1, p0, LX/Eyl;->a:Z

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    .line 2186518
    iget-boolean v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aE:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aO:Z

    if-nez v3, :cond_0

    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 2186519
    iget-object v4, v3, LX/2iK;->m:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    const/4 v4, 0x1

    :goto_0
    move v3, v4

    .line 2186520
    if-nez v3, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2186521
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, LX/8DG;

    if-eqz v3, :cond_0

    .line 2186522
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, LX/8DG;

    const-string v4, "people_you_may_know"

    invoke-interface {v3, v4}, LX/8DG;->b(Ljava/lang/String;)V

    .line 2186523
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aO:Z

    .line 2186524
    if-eqz v1, :cond_1

    .line 2186525
    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v3}, LX/2iK;->e()V

    .line 2186526
    :cond_1
    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z:LX/2dj;

    invoke-virtual {v3}, LX/2dj;->d()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2186527
    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2186528
    :cond_2
    iget-object v3, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->ae:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2186529
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2186530
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2186531
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2186532
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lr;

    .line 2186533
    instance-of v5, v0, LX/2no;

    if-eqz v5, :cond_5

    .line 2186534
    check-cast v0, LX/2no;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2186535
    :cond_5
    instance-of v5, v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    if-eqz v5, :cond_4

    .line 2186536
    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2186537
    :cond_6
    iget-object v0, p0, LX/Eyl;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2iK;->a(Ljava/util/List;Ljava/util/List;)V

    .line 2186538
    iget-object v0, p0, LX/Eyl;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, p0, LX/Eyl;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v1}, LX/2iK;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    .line 2186539
    invoke-static {v0, v1, v2}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;ZZ)V

    .line 2186540
    return-void

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_0
.end method
