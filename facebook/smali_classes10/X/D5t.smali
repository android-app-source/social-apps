.class public LX/D5t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/D5t;


# instance fields
.field private final b:LX/D49;

.field private final c:LX/2mZ;

.field private final d:LX/14w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1964771
    const-class v0, LX/D5t;

    sput-object v0, LX/D5t;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/D49;LX/2mZ;LX/14w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1964753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1964754
    iput-object p1, p0, LX/D5t;->b:LX/D49;

    .line 1964755
    iput-object p2, p0, LX/D5t;->c:LX/2mZ;

    .line 1964756
    iput-object p3, p0, LX/D5t;->d:LX/14w;

    .line 1964757
    return-void
.end method

.method public static a(LX/0QB;)LX/D5t;
    .locals 6

    .prologue
    .line 1964758
    sget-object v0, LX/D5t;->e:LX/D5t;

    if-nez v0, :cond_1

    .line 1964759
    const-class v1, LX/D5t;

    monitor-enter v1

    .line 1964760
    :try_start_0
    sget-object v0, LX/D5t;->e:LX/D5t;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1964761
    if-eqz v2, :cond_0

    .line 1964762
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1964763
    new-instance p0, LX/D5t;

    invoke-static {v0}, LX/D49;->b(LX/0QB;)LX/D49;

    move-result-object v3

    check-cast v3, LX/D49;

    const-class v4, LX/2mZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2mZ;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v5

    check-cast v5, LX/14w;

    invoke-direct {p0, v3, v4, v5}, LX/D5t;-><init>(LX/D49;LX/2mZ;LX/14w;)V

    .line 1964764
    move-object v0, p0

    .line 1964765
    sput-object v0, LX/D5t;->e:LX/D5t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1964766
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1964767
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1964768
    :cond_1
    sget-object v0, LX/D5t;->e:LX/D5t;

    return-object v0

    .line 1964769
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1964770
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static e(LX/D5t;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1964722
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964723
    new-instance v0, LX/D5s;

    invoke-direct {v0, p0}, LX/D5s;-><init>(LX/D5t;)V

    .line 1964724
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964725
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, p1

    .line 1964726
    :goto_0
    if-eqz v1, :cond_1

    .line 1964727
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1964728
    :goto_1
    move-object v0, v1

    .line 1964729
    return-object v0

    .line 1964730
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    goto :goto_0

    .line 1964731
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2pa;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/2pa;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1964732
    if-nez p1, :cond_0

    move-object v0, v1

    .line 1964733
    :goto_0
    return-object v0

    .line 1964734
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1964735
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1964736
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1964737
    goto :goto_0

    .line 1964738
    :cond_1
    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1964739
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 1964740
    iget-object v3, p0, LX/D5t;->c:LX/2mZ;

    invoke-virtual {v3, v2, v0}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v0

    invoke-virtual {v0}, LX/3Im;->a()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1964741
    invoke-static {p1}, LX/D49;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0P2;

    move-result-object v2

    .line 1964742
    if-nez v2, :cond_2

    move-object v0, v1

    .line 1964743
    goto :goto_0

    .line 1964744
    :cond_2
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    .line 1964745
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1964746
    move-object v0, v1

    .line 1964747
    iget-object v1, p0, LX/D5t;->b:LX/D49;

    invoke-virtual {v1, p1}, LX/D49;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)D

    move-result-wide v4

    .line 1964748
    iput-wide v4, v0, LX/2pZ;->e:D

    .line 1964749
    move-object v0, v0

    .line 1964750
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1964751
    invoke-static {p0, p1}, LX/D5t;->e(LX/D5t;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1964752
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
