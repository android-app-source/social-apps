.class public LX/Dw7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pe;
.implements LX/1Po;
.implements LX/1Pq;
.implements LX/1Pr;
.implements LX/1Px;


# static fields
.field private static final a:LX/1PT;


# instance fields
.field private final b:LX/1QF;

.field private final c:LX/1Q2;

.field private final d:LX/1QR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2059997
    new-instance v0, LX/Dw6;

    invoke-direct {v0}, LX/Dw6;-><init>()V

    sput-object v0, LX/Dw7;->a:LX/1PT;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;LX/1QF;LX/1QC;LX/1Q2;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2059992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059993
    iput-object p2, p0, LX/Dw7;->b:LX/1QF;

    .line 2059994
    invoke-static {p1}, LX/1QC;->a(Ljava/lang/Runnable;)LX/1QR;

    move-result-object v0

    iput-object v0, p0, LX/Dw7;->d:LX/1QR;

    .line 2059995
    iput-object p4, p0, LX/Dw7;->c:LX/1Q2;

    .line 2059996
    return-void
.end method


# virtual methods
.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 2059991
    iget-object v0, p0, LX/Dw7;->b:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2059990
    iget-object v0, p0, LX/Dw7;->b:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1R6;)V
    .locals 1
    .param p1    # LX/1R6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2059988
    iget-object v0, p0, LX/Dw7;->d:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a(LX/1R6;)V

    .line 2059989
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 2059986
    iget-object v0, p0, LX/Dw7;->c:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2059987
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2059984
    iget-object v0, p0, LX/Dw7;->c:LX/1Q2;

    invoke-virtual {v0, p1, p2}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    .line 2059985
    return-void
.end method

.method public final varargs a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 2059982
    iget-object v0, p0, LX/Dw7;->d:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2059983
    return-void
.end method

.method public final varargs a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2059998
    iget-object v0, p0, LX/Dw7;->d:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Ljava/lang/Object;)V

    .line 2059999
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 2059981
    iget-object v0, p0, LX/Dw7;->b:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/1R6;)V
    .locals 1

    .prologue
    .line 2059979
    iget-object v0, p0, LX/Dw7;->d:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->b(LX/1R6;)V

    .line 2059980
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 2059977
    iget-object v0, p0, LX/Dw7;->c:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2059978
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2059975
    iget-object v0, p0, LX/Dw7;->b:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 2059976
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2059974
    sget-object v0, LX/Dw7;->a:LX/1PT;

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 2059973
    iget-object v0, p0, LX/Dw7;->c:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 2059971
    iget-object v0, p0, LX/Dw7;->d:LX/1QR;

    invoke-virtual {v0}, LX/1QR;->iN_()V

    .line 2059972
    return-void
.end method

.method public final m_(Z)V
    .locals 1

    .prologue
    .line 2059969
    iget-object v0, p0, LX/Dw7;->d:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->m_(Z)V

    .line 2059970
    return-void
.end method
