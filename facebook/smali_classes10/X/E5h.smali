.class public LX/E5h;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E5f;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E5h;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078778
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5h;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5i;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078785
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078786
    iput-object p1, p0, LX/E5h;->b:LX/0Ot;

    .line 2078787
    return-void
.end method

.method public static a(LX/0QB;)LX/E5h;
    .locals 4

    .prologue
    .line 2078788
    sget-object v0, LX/E5h;->c:LX/E5h;

    if-nez v0, :cond_1

    .line 2078789
    const-class v1, LX/E5h;

    monitor-enter v1

    .line 2078790
    :try_start_0
    sget-object v0, LX/E5h;->c:LX/E5h;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078791
    if-eqz v2, :cond_0

    .line 2078792
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078793
    new-instance v3, LX/E5h;

    const/16 p0, 0x3131

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E5h;-><init>(LX/0Ot;)V

    .line 2078794
    move-object v0, v3

    .line 2078795
    sput-object v0, LX/E5h;->c:LX/E5h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078796
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078797
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078798
    :cond_1
    sget-object v0, LX/E5h;->c:LX/E5h;

    return-object v0

    .line 2078799
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078800
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2078781
    check-cast p2, LX/E5g;

    .line 2078782
    iget-object v0, p0, LX/E5h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E5i;

    iget-object v1, p2, LX/E5g;->a:Ljava/lang/String;

    .line 2078783
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const v2, 0x7f01072b

    invoke-interface {v0, v2}, LX/1Dh;->U(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const p0, 0x7f0a00ab

    invoke-virtual {v2, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const p0, 0x7f0b0050

    invoke-virtual {v2, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-static {p1}, LX/E5i;->a(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x1

    invoke-interface {v2, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    const/4 p0, 0x6

    const p2, 0x7f010718

    invoke-interface {v2, p0, p2}, LX/1Di;->f(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x7

    const p2, 0x7f0b163a

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2078784
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2078779
    invoke-static {}, LX/1dS;->b()V

    .line 2078780
    const/4 v0, 0x0

    return-object v0
.end method
