.class public LX/Dp0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field public a:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final c:LX/Doz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042946
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Dp0;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Doz;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2042947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042948
    iput v0, p0, LX/Dp0;->a:I

    .line 2042949
    iput v0, p0, LX/Dp0;->b:I

    .line 2042950
    iput-object p1, p0, LX/Dp0;->c:LX/Doz;

    .line 2042951
    return-void
.end method

.method public static a(LX/0QB;)LX/Dp0;
    .locals 7

    .prologue
    .line 2042952
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2042953
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2042954
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2042955
    if-nez v1, :cond_0

    .line 2042956
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2042957
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2042958
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2042959
    sget-object v1, LX/Dp0;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2042960
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2042961
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2042962
    :cond_1
    if-nez v1, :cond_4

    .line 2042963
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2042964
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2042965
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2042966
    new-instance p0, LX/Dp0;

    invoke-static {v0}, LX/Doz;->a(LX/0QB;)LX/Doz;

    move-result-object v1

    check-cast v1, LX/Doz;

    invoke-direct {p0, v1}, LX/Dp0;-><init>(LX/Doz;)V

    .line 2042967
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2042968
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2042969
    if-nez v1, :cond_2

    .line 2042970
    sget-object v0, LX/Dp0;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dp0;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2042971
    :goto_1
    if-eqz v0, :cond_3

    .line 2042972
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042973
    :goto_3
    check-cast v0, LX/Dp0;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2042974
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2042975
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2042976
    :catchall_1
    move-exception v0

    .line 2042977
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042978
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2042979
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2042980
    :cond_2
    :try_start_8
    sget-object v0, LX/Dp0;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dp0;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()LX/Ebg;
    .locals 6

    .prologue
    .line 2042981
    monitor-enter p0

    :try_start_0
    iget v1, p0, LX/Dp0;->a:I

    add-int/lit8 v0, v1, 0x1

    iput v0, p0, LX/Dp0;->a:I

    .line 2042982
    const/4 v0, 0x1

    invoke-static {v1, v0}, LX/Ecq;->a(II)Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebg;

    .line 2042983
    iget-object v2, p0, LX/Dp0;->c:LX/Doz;

    .line 2042984
    iget-object v3, v2, LX/Doz;->b:LX/Ecl;

    .line 2042985
    iget-object v4, v3, LX/Ecl;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0}, LX/Ebg;->c()[B

    move-result-object v2

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2042986
    monitor-exit p0

    return-object v0

    .line 2042987
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/Ebk;
    .locals 6

    .prologue
    .line 2042988
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/Dp0;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/Dp0;->b:I

    .line 2042989
    iget-object v1, p0, LX/Dp0;->c:LX/Doz;

    invoke-virtual {v1}, LX/Doz;->a()LX/Eaf;

    move-result-object v1

    invoke-static {v1, v0}, LX/Ecq;->a(LX/Eaf;I)LX/Ebk;

    move-result-object v1

    .line 2042990
    iget-object v2, p0, LX/Dp0;->c:LX/Doz;

    .line 2042991
    iget-object v3, v2, LX/Doz;->d:LX/Ecn;

    .line 2042992
    iget-object v4, v3, LX/Ecn;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1}, LX/Ebk;->d()[B

    move-result-object v2

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2042993
    monitor-exit p0

    return-object v1

    .line 2042994
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
