.class public LX/EYw;
.super Ljava/util/AbstractList;
.source ""

# interfaces
.implements LX/EYv;
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Ljava/lang/String;",
        ">;",
        "LX/EYv;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# static fields
.field public static final a:LX/EYv;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2138212
    new-instance v0, LX/EZT;

    new-instance v1, LX/EYw;

    invoke-direct {v1}, LX/EYw;-><init>()V

    invoke-direct {v0, v1}, LX/EZT;-><init>(LX/EYv;)V

    sput-object v0, LX/EYw;->a:LX/EYv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2138217
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 2138218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EYw;->b:Ljava/util/List;

    .line 2138219
    return-void
.end method

.method public constructor <init>(LX/EYv;)V
    .locals 2

    .prologue
    .line 2138213
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 2138214
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, LX/EYv;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/EYw;->b:Ljava/util/List;

    .line 2138215
    invoke-virtual {p0, p1}, LX/EYw;->addAll(Ljava/util/Collection;)Z

    .line 2138216
    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2138176
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2138177
    check-cast p0, Ljava/lang/String;

    .line 2138178
    :goto_0
    return-object p0

    :cond_0
    check-cast p0, LX/EWc;

    invoke-virtual {p0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/EWc;
    .locals 2

    .prologue
    .line 2138207
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2138208
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2138209
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2138210
    iget-object v1, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2138211
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2138206
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWc;)V
    .locals 1

    .prologue
    .line 2138203
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2138204
    iget v0, p0, Ljava/util/AbstractList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EYw;->modCount:I

    .line 2138205
    return-void
.end method

.method public final add(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2138220
    check-cast p2, Ljava/lang/String;

    .line 2138221
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2138222
    iget v0, p0, Ljava/util/AbstractList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EYw;->modCount:I

    .line 2138223
    return-void
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2138199
    instance-of v0, p2, LX/EYv;

    if-eqz v0, :cond_0

    check-cast p2, LX/EYv;

    invoke-interface {p2}, LX/EYv;->a()Ljava/util/List;

    move-result-object p2

    .line 2138200
    :cond_0
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    .line 2138201
    iget v1, p0, Ljava/util/AbstractList;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/EYw;->modCount:I

    .line 2138202
    return v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2138198
    invoke-virtual {p0}, LX/EYw;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, LX/EYw;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 2138195
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2138196
    iget v0, p0, Ljava/util/AbstractList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EYw;->modCount:I

    .line 2138197
    return-void
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2138186
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2138187
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2138188
    check-cast v0, Ljava/lang/String;

    .line 2138189
    :goto_0
    return-object v0

    .line 2138190
    :cond_0
    check-cast v0, LX/EWc;

    .line 2138191
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2138192
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2138193
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2138194
    goto :goto_0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2138183
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 2138184
    iget v1, p0, Ljava/util/AbstractList;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/EYw;->modCount:I

    .line 2138185
    invoke-static {v0}, LX/EYw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2138180
    check-cast p2, Ljava/lang/String;

    .line 2138181
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2138182
    invoke-static {v0}, LX/EYw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2138179
    iget-object v0, p0, LX/EYw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
