.class public final enum LX/Cke;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cke;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cke;

.field public static final enum OVERLAY_NONE:LX/Cke;

.field public static final enum OVERLAY_PLAY_BUTTON:LX/Cke;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1931191
    new-instance v0, LX/Cke;

    const-string v1, "OVERLAY_NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v3, v2}, LX/Cke;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cke;->OVERLAY_NONE:LX/Cke;

    .line 1931192
    new-instance v0, LX/Cke;

    const-string v1, "OVERLAY_PLAY_BUTTON"

    const-string v2, "playButton"

    invoke-direct {v0, v1, v4, v2}, LX/Cke;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cke;->OVERLAY_PLAY_BUTTON:LX/Cke;

    .line 1931193
    const/4 v0, 0x2

    new-array v0, v0, [LX/Cke;

    sget-object v1, LX/Cke;->OVERLAY_NONE:LX/Cke;

    aput-object v1, v0, v3

    sget-object v1, LX/Cke;->OVERLAY_PLAY_BUTTON:LX/Cke;

    aput-object v1, v0, v4

    sput-object v0, LX/Cke;->$VALUES:[LX/Cke;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1931203
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1931204
    iput-object p3, p0, LX/Cke;->value:Ljava/lang/String;

    .line 1931205
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/Cke;
    .locals 5

    .prologue
    .line 1931196
    if-nez p0, :cond_1

    .line 1931197
    sget-object v0, LX/Cke;->OVERLAY_NONE:LX/Cke;

    .line 1931198
    :cond_0
    :goto_0
    return-object v0

    .line 1931199
    :cond_1
    invoke-static {}, LX/Cke;->values()[LX/Cke;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 1931200
    iget-object v4, v0, LX/Cke;->value:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1931201
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1931202
    :cond_2
    sget-object v0, LX/Cke;->OVERLAY_NONE:LX/Cke;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cke;
    .locals 1

    .prologue
    .line 1931195
    const-class v0, LX/Cke;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cke;

    return-object v0
.end method

.method public static values()[LX/Cke;
    .locals 1

    .prologue
    .line 1931194
    sget-object v0, LX/Cke;->$VALUES:[LX/Cke;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cke;

    return-object v0
.end method
