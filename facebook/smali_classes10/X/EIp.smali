.class public final enum LX/EIp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EIp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EIp;

.field public static final enum FINISHED:LX/EIp;

.field public static final enum NONE:LX/EIp;

.field public static final enum RECORDING:LX/EIp;

.field public static final enum STARTED:LX/EIp;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2102352
    new-instance v0, LX/EIp;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/EIp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIp;->NONE:LX/EIp;

    .line 2102353
    new-instance v0, LX/EIp;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v3}, LX/EIp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIp;->STARTED:LX/EIp;

    .line 2102354
    new-instance v0, LX/EIp;

    const-string v1, "RECORDING"

    invoke-direct {v0, v1, v4}, LX/EIp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIp;->RECORDING:LX/EIp;

    .line 2102355
    new-instance v0, LX/EIp;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v5}, LX/EIp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIp;->FINISHED:LX/EIp;

    .line 2102356
    const/4 v0, 0x4

    new-array v0, v0, [LX/EIp;

    sget-object v1, LX/EIp;->NONE:LX/EIp;

    aput-object v1, v0, v2

    sget-object v1, LX/EIp;->STARTED:LX/EIp;

    aput-object v1, v0, v3

    sget-object v1, LX/EIp;->RECORDING:LX/EIp;

    aput-object v1, v0, v4

    sget-object v1, LX/EIp;->FINISHED:LX/EIp;

    aput-object v1, v0, v5

    sput-object v0, LX/EIp;->$VALUES:[LX/EIp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2102357
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EIp;
    .locals 1

    .prologue
    .line 2102358
    const-class v0, LX/EIp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EIp;

    return-object v0
.end method

.method public static values()[LX/EIp;
    .locals 1

    .prologue
    .line 2102359
    sget-object v0, LX/EIp;->$VALUES:[LX/EIp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EIp;

    return-object v0
.end method
