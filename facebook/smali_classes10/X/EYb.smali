.class public final synthetic LX/EYb;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2137604
    invoke-static {}, LX/EZV;->values()[LX/EZV;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/EYb;->b:[I

    :try_start_0
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->DOUBLE:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1a

    :goto_0
    :try_start_1
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->FLOAT:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_19

    :goto_1
    :try_start_2
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->INT64:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_18

    :goto_2
    :try_start_3
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->UINT64:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_17

    :goto_3
    :try_start_4
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->INT32:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_16

    :goto_4
    :try_start_5
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->FIXED64:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_15

    :goto_5
    :try_start_6
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->FIXED32:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_14

    :goto_6
    :try_start_7
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->BOOL:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_13

    :goto_7
    :try_start_8
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->STRING:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_12

    :goto_8
    :try_start_9
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->BYTES:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_11

    :goto_9
    :try_start_a
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->UINT32:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_10

    :goto_a
    :try_start_b
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->SFIXED32:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_f

    :goto_b
    :try_start_c
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->SFIXED64:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_e

    :goto_c
    :try_start_d
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->SINT32:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :goto_d
    :try_start_e
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->SINT64:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_c

    :goto_e
    :try_start_f
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->GROUP:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_b

    :goto_f
    :try_start_10
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->MESSAGE:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_a

    :goto_10
    :try_start_11
    sget-object v0, LX/EYb;->b:[I

    sget-object v1, LX/EZV;->ENUM:LX/EZV;

    invoke-virtual {v1}, LX/EZV;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_9

    .line 2137605
    :goto_11
    invoke-static {}, LX/EZa;->values()[LX/EZa;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/EYb;->a:[I

    :try_start_12
    sget-object v0, LX/EYb;->a:[I

    sget-object v1, LX/EZa;->INT:LX/EZa;

    invoke-virtual {v1}, LX/EZa;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_8

    :goto_12
    :try_start_13
    sget-object v0, LX/EYb;->a:[I

    sget-object v1, LX/EZa;->LONG:LX/EZa;

    invoke-virtual {v1}, LX/EZa;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_7

    :goto_13
    :try_start_14
    sget-object v0, LX/EYb;->a:[I

    sget-object v1, LX/EZa;->FLOAT:LX/EZa;

    invoke-virtual {v1}, LX/EZa;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_6

    :goto_14
    :try_start_15
    sget-object v0, LX/EYb;->a:[I

    sget-object v1, LX/EZa;->DOUBLE:LX/EZa;

    invoke-virtual {v1}, LX/EZa;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_5

    :goto_15
    :try_start_16
    sget-object v0, LX/EYb;->a:[I

    sget-object v1, LX/EZa;->BOOLEAN:LX/EZa;

    invoke-virtual {v1}, LX/EZa;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_4

    :goto_16
    :try_start_17
    sget-object v0, LX/EYb;->a:[I

    sget-object v1, LX/EZa;->STRING:LX/EZa;

    invoke-virtual {v1}, LX/EZa;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_3

    :goto_17
    :try_start_18
    sget-object v0, LX/EYb;->a:[I

    sget-object v1, LX/EZa;->BYTE_STRING:LX/EZa;

    invoke-virtual {v1}, LX/EZa;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_2

    :goto_18
    :try_start_19
    sget-object v0, LX/EYb;->a:[I

    sget-object v1, LX/EZa;->ENUM:LX/EZa;

    invoke-virtual {v1}, LX/EZa;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_1

    :goto_19
    :try_start_1a
    sget-object v0, LX/EYb;->a:[I

    sget-object v1, LX/EZa;->MESSAGE:LX/EZa;

    invoke-virtual {v1}, LX/EZa;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_0

    :goto_1a
    return-void

    :catch_0
    goto :goto_1a

    :catch_1
    goto :goto_19

    :catch_2
    goto :goto_18

    :catch_3
    goto :goto_17

    :catch_4
    goto :goto_16

    :catch_5
    goto :goto_15

    :catch_6
    goto :goto_14

    :catch_7
    goto :goto_13

    :catch_8
    goto :goto_12

    :catch_9
    goto :goto_11

    :catch_a
    goto/16 :goto_10

    :catch_b
    goto/16 :goto_f

    :catch_c
    goto/16 :goto_e

    :catch_d
    goto/16 :goto_d

    :catch_e
    goto/16 :goto_c

    :catch_f
    goto/16 :goto_b

    :catch_10
    goto/16 :goto_a

    :catch_11
    goto/16 :goto_9

    :catch_12
    goto/16 :goto_8

    :catch_13
    goto/16 :goto_7

    :catch_14
    goto/16 :goto_6

    :catch_15
    goto/16 :goto_5

    :catch_16
    goto/16 :goto_4

    :catch_17
    goto/16 :goto_3

    :catch_18
    goto/16 :goto_2

    :catch_19
    goto/16 :goto_1

    :catch_1a
    goto/16 :goto_0
.end method
