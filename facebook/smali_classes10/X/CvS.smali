.class public final LX/CvS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gT;


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1948312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1948313
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CvS;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final serialize(LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1948304
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1948305
    iget-object v0, p0, LX/CvS;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1948306
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1948307
    const-string v3, "view"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v3, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1948308
    const-string v1, "height"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 1948309
    invoke-virtual {p1}, LX/0nX;->g()V

    goto :goto_0

    .line 1948310
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1948311
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 2

    .prologue
    .line 1948303
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Serialization infrastructure does not support type serialization."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
