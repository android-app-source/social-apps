.class public LX/DpT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final msg_to:LX/DpM;

.field public final suggested_codename:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2045570
    new-instance v0, LX/1sv;

    const-string v1, "PrimaryDeviceChangePayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpT;->b:LX/1sv;

    .line 2045571
    new-instance v0, LX/1sw;

    const-string v1, "msg_to"

    const/16 v2, 0xc

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpT;->c:LX/1sw;

    .line 2045572
    new-instance v0, LX/1sw;

    const-string v1, "suggested_codename"

    const/16 v2, 0xb

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpT;->d:LX/1sw;

    .line 2045573
    const/4 v0, 0x1

    sput-boolean v0, LX/DpT;->a:Z

    return-void
.end method

.method public constructor <init>(LX/DpM;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2045566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2045567
    iput-object p1, p0, LX/DpT;->msg_to:LX/DpM;

    .line 2045568
    iput-object p2, p0, LX/DpT;->suggested_codename:Ljava/lang/String;

    .line 2045569
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2045538
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2045539
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2045540
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2045541
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PrimaryDeviceChangePayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045542
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045543
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045544
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045545
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045546
    const-string v4, "msg_to"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045547
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045548
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045549
    iget-object v4, p0, LX/DpT;->msg_to:LX/DpM;

    if-nez v4, :cond_3

    .line 2045550
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045551
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045552
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045553
    const-string v4, "suggested_codename"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045554
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045555
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045556
    iget-object v0, p0, LX/DpT;->suggested_codename:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 2045557
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045558
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045559
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045560
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2045561
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2045562
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2045563
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2045564
    :cond_3
    iget-object v4, p0, LX/DpT;->msg_to:LX/DpM;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2045565
    :cond_4
    iget-object v0, p0, LX/DpT;->suggested_codename:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2045574
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2045575
    iget-object v0, p0, LX/DpT;->msg_to:LX/DpM;

    if-eqz v0, :cond_0

    .line 2045576
    sget-object v0, LX/DpT;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045577
    iget-object v0, p0, LX/DpT;->msg_to:LX/DpM;

    invoke-virtual {v0, p1}, LX/DpM;->a(LX/1su;)V

    .line 2045578
    :cond_0
    iget-object v0, p0, LX/DpT;->suggested_codename:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2045579
    sget-object v0, LX/DpT;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045580
    iget-object v0, p0, LX/DpT;->suggested_codename:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2045581
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2045582
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2045583
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2045512
    if-nez p1, :cond_1

    .line 2045513
    :cond_0
    :goto_0
    return v0

    .line 2045514
    :cond_1
    instance-of v1, p1, LX/DpT;

    if-eqz v1, :cond_0

    .line 2045515
    check-cast p1, LX/DpT;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2045516
    if-nez p1, :cond_3

    .line 2045517
    :cond_2
    :goto_1
    move v0, v2

    .line 2045518
    goto :goto_0

    .line 2045519
    :cond_3
    iget-object v0, p0, LX/DpT;->msg_to:LX/DpM;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2045520
    :goto_2
    iget-object v3, p1, LX/DpT;->msg_to:LX/DpM;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2045521
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2045522
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045523
    iget-object v0, p0, LX/DpT;->msg_to:LX/DpM;

    iget-object v3, p1, LX/DpT;->msg_to:LX/DpM;

    invoke-virtual {v0, v3}, LX/DpM;->a(LX/DpM;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2045524
    :cond_5
    iget-object v0, p0, LX/DpT;->suggested_codename:Ljava/lang/String;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2045525
    :goto_4
    iget-object v3, p1, LX/DpT;->suggested_codename:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2045526
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2045527
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045528
    iget-object v0, p0, LX/DpT;->suggested_codename:Ljava/lang/String;

    iget-object v3, p1, LX/DpT;->suggested_codename:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 2045529
    goto :goto_1

    :cond_8
    move v0, v2

    .line 2045530
    goto :goto_2

    :cond_9
    move v3, v2

    .line 2045531
    goto :goto_3

    :cond_a
    move v0, v2

    .line 2045532
    goto :goto_4

    :cond_b
    move v3, v2

    .line 2045533
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2045537
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2045534
    sget-boolean v0, LX/DpT;->a:Z

    .line 2045535
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpT;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2045536
    return-object v0
.end method
