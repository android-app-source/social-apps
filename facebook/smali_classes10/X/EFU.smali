.class public LX/EFU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[J

.field public final b:J

.field public final c:LX/0So;

.field public d:I


# direct methods
.method public constructor <init>(IJLX/0So;)V
    .locals 2
    .param p1    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2095928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2095929
    const/4 v0, 0x0

    iput v0, p0, LX/EFU;->d:I

    .line 2095930
    const/4 v0, 0x2

    if-lt p1, v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_1

    .line 2095931
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "need at least 2 events and > 0 time amount"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2095932
    :cond_1
    iput-object p4, p0, LX/EFU;->c:LX/0So;

    .line 2095933
    new-array v0, p1, [J

    iput-object v0, p0, LX/EFU;->a:[J

    .line 2095934
    iput-wide p2, p0, LX/EFU;->b:J

    .line 2095935
    invoke-static {p0}, LX/EFU;->b(LX/EFU;)V

    .line 2095936
    return-void
.end method

.method public static b(LX/EFU;)V
    .locals 6

    .prologue
    .line 2095937
    iget-object v0, p0, LX/EFU;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 2095938
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/EFU;->a:[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2095939
    iget-object v1, p0, LX/EFU;->a:[J

    iget-wide v4, p0, LX/EFU;->b:J

    sub-long v4, v2, v4

    aput-wide v4, v1, v0

    .line 2095940
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2095941
    :cond_0
    return-void
.end method
