.class public LX/Ebg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/Ebt;


# direct methods
.method public constructor <init>(ILX/Eau;)V
    .locals 2

    .prologue
    .line 2144206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144207
    invoke-static {}, LX/Ebs;->u()LX/Ebs;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Ebs;->a(I)LX/Ebs;

    move-result-object v0

    .line 2144208
    iget-object v1, p2, LX/Eau;->a:LX/Eat;

    move-object v1, v1

    .line 2144209
    invoke-virtual {v1}, LX/Eat;->a()[B

    move-result-object v1

    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ebs;->a(LX/EWc;)LX/Ebs;

    move-result-object v0

    .line 2144210
    iget-object v1, p2, LX/Eau;->b:LX/Eas;

    move-object v1, v1

    .line 2144211
    iget-object p1, v1, LX/Eas;->a:[B

    move-object v1, p1

    .line 2144212
    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ebs;->b(LX/EWc;)LX/Ebs;

    move-result-object v0

    invoke-virtual {v0}, LX/Ebs;->l()LX/Ebt;

    move-result-object v0

    iput-object v0, p0, LX/Ebg;->a:LX/Ebt;

    .line 2144213
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 2144214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144215
    sget-object v0, LX/Ebt;->a:LX/EWZ;

    invoke-virtual {v0, p1}, LX/EWZ;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebt;

    move-object v0, v0

    .line 2144216
    iput-object v0, p0, LX/Ebg;->a:LX/Ebt;

    .line 2144217
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2144218
    iget-object v0, p0, LX/Ebg;->a:LX/Ebt;

    .line 2144219
    iget p0, v0, LX/Ebt;->id_:I

    move v0, p0

    .line 2144220
    return v0
.end method

.method public final b()LX/Eau;
    .locals 3

    .prologue
    .line 2144221
    :try_start_0
    iget-object v0, p0, LX/Ebg;->a:LX/Ebt;

    .line 2144222
    iget-object v1, v0, LX/Ebt;->publicKey_:LX/EWc;

    move-object v0, v1

    .line 2144223
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v0

    .line 2144224
    iget-object v1, p0, LX/Ebg;->a:LX/Ebt;

    .line 2144225
    iget-object v2, v1, LX/Ebt;->privateKey_:LX/EWc;

    move-object v1, v2

    .line 2144226
    invoke-virtual {v1}, LX/EWc;->d()[B

    move-result-object v1

    invoke-static {v1}, LX/Ear;->a([B)LX/Eas;

    move-result-object v1

    .line 2144227
    new-instance v2, LX/Eau;

    invoke-direct {v2, v0, v1}, LX/Eau;-><init>(LX/Eat;LX/Eas;)V
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 2144228
    :catch_0
    move-exception v0

    .line 2144229
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final c()[B
    .locals 1

    .prologue
    .line 2144230
    iget-object v0, p0, LX/Ebg;->a:LX/Ebt;

    invoke-virtual {v0}, LX/EWX;->jZ_()[B

    move-result-object v0

    return-object v0
.end method
