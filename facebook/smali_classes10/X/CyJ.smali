.class public LX/CyJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cxj;


# instance fields
.field private final a:LX/1Pr;

.field private final b:LX/1VK;


# direct methods
.method public constructor <init>(LX/1Pr;LX/1VK;)V
    .locals 0
    .param p1    # LX/1Pr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952624
    iput-object p1, p0, LX/CyJ;->a:LX/1Pr;

    .line 1952625
    iput-object p2, p0, LX/CyJ;->b:LX/1VK;

    .line 1952626
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/CyM;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I)",
            "LX/CyM;"
        }
    .end annotation

    .prologue
    .line 1952633
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1952634
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1952635
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1952636
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1952637
    new-instance v2, LX/CyL;

    iget-object v3, p0, LX/CyJ;->b:LX/1VK;

    invoke-direct {v2, p1, v1, p2, v3}, LX/CyL;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;ILX/1VK;)V

    .line 1952638
    iget-object v1, p0, LX/CyJ;->a:LX/1Pr;

    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyM;

    return-object v0
.end method

.method public final d(LX/CzL;)LX/CyM;
    .locals 2

    .prologue
    .line 1952627
    iget v0, p1, LX/CzL;->b:I

    move v0, v0

    .line 1952628
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 1952629
    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1952630
    iget-object p1, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, p1

    .line 1952631
    move-object v1, v1

    .line 1952632
    invoke-virtual {p0, v1, v0}, LX/CyJ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/CyM;

    move-result-object v0

    return-object v0
.end method
