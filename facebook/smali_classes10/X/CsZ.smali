.class public LX/CsZ;
.super Lcom/facebook/resources/ui/FbImageButton;
.source ""

# interfaces
.implements LX/20T;


# static fields
.field public static final b:LX/0wT;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/215;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1942993
    sget-wide v0, LX/CoL;->J:D

    sget-wide v2, LX/CoL;->K:D

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/CsZ;->b:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1942976
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbImageButton;-><init>(Landroid/content/Context;)V

    .line 1942977
    const-class v0, LX/CsZ;

    invoke-static {v0, p0}, LX/CsZ;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1942978
    iget-object v0, p0, LX/CsZ;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    iput-object v0, p0, LX/CsZ;->c:LX/215;

    .line 1942979
    iget-object v0, p0, LX/CsZ;->c:LX/215;

    invoke-virtual {v0, p0}, LX/215;->a(LX/20T;)V

    .line 1942980
    iget-object v0, p0, LX/CsZ;->c:LX/215;

    const/4 p1, 0x1

    .line 1942981
    iput-boolean p1, v0, LX/215;->d:Z

    .line 1942982
    iget-object v0, p0, LX/CsZ;->c:LX/215;

    const p1, 0x3f4ccccd    # 0.8f

    .line 1942983
    iput p1, v0, LX/215;->b:F

    .line 1942984
    iget-object v0, p0, LX/CsZ;->c:LX/215;

    const/high16 p1, 0x3f800000    # 1.0f

    .line 1942985
    iput p1, v0, LX/215;->c:F

    .line 1942986
    iget-object v0, p0, LX/CsZ;->c:LX/215;

    sget-object p1, LX/CsZ;->b:LX/0wT;

    invoke-virtual {v0, p1}, LX/215;->a(LX/0wT;)V

    .line 1942987
    new-instance v0, LX/CsY;

    invoke-direct {v0, p0}, LX/CsY;-><init>(LX/CsZ;)V

    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1942988
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/CsZ;

    const/16 p0, 0x13a4

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    iput-object v1, p1, LX/CsZ;->a:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 0

    .prologue
    .line 1942990
    invoke-virtual {p0, p1}, LX/CsZ;->setScaleX(F)V

    .line 1942991
    invoke-virtual {p0, p1}, LX/CsZ;->setScaleY(F)V

    .line 1942992
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 2

    .prologue
    .line 1942989
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TouchSpring is OnTouchListener for this view"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSpring(LX/215;)V
    .locals 0

    .prologue
    .line 1942974
    iput-object p1, p0, LX/CsZ;->c:LX/215;

    .line 1942975
    return-void
.end method
