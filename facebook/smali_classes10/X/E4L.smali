.class public LX/E4L;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/resources/ui/FbButton;

.field public c:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2076484
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2076485
    const-class v0, LX/E4L;

    invoke-static {v0, p0}, LX/E4L;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2076486
    const v0, 0x7f031147

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2076487
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/E4L;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/E4L;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2076488
    invoke-virtual {p0}, LX/E4L;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 2076489
    invoke-virtual {p0, v0, v0, v0, v0}, LX/E4L;->setPadding(IIII)V

    .line 2076490
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/E4L;->setOrientation(I)V

    .line 2076491
    const v0, 0x7f0d28da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/E4L;->b:Lcom/facebook/resources/ui/FbButton;

    .line 2076492
    const v0, 0x7f0d28db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/E4L;->c:Lcom/facebook/resources/ui/FbButton;

    .line 2076493
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/E4L;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object p0

    check-cast p0, LX/23P;

    iput-object p0, p1, LX/E4L;->a:LX/23P;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2076494
    iget-object v0, p0, LX/E4L;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2076495
    iget-object v0, p0, LX/E4L;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2076496
    return-void
.end method
