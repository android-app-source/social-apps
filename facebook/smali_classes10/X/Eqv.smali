.class public LX/Eqv;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Equ;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172274
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2172275
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/Eqg;)LX/Equ;
    .locals 12

    .prologue
    .line 2172276
    new-instance v0, LX/Equ;

    invoke-static {p0}, LX/2RC;->a(LX/0QB;)LX/2RC;

    move-result-object v1

    check-cast v1, LX/2RC;

    invoke-static {p0}, LX/6NS;->b(LX/0QB;)LX/3Oq;

    move-result-object v2

    check-cast v2, LX/3Oq;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    const-class v6, LX/Era;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Era;

    invoke-static {p0}, LX/ErU;->a(LX/0QB;)LX/ErU;

    move-result-object v7

    check-cast v7, LX/ErU;

    invoke-static {p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    move-object v10, p1

    move-object v11, p2

    invoke-direct/range {v0 .. v11}, LX/Equ;-><init>(LX/2RC;LX/3Oq;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0tX;LX/Era;LX/ErU;LX/0TD;LX/1Ck;Ljava/lang/String;LX/Eqg;)V

    .line 2172277
    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v1

    check-cast v1, LX/2RQ;

    .line 2172278
    iput-object v1, v0, LX/Equ;->A:LX/2RQ;

    .line 2172279
    return-object v0
.end method
