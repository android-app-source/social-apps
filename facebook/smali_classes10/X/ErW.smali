.class public LX/ErW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/ErW;


# instance fields
.field private a:LX/ErY;

.field private b:LX/ErY;

.field private c:LX/ErY;

.field private d:LX/ErY;

.field private e:LX/ErY;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2173114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2173115
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->a:LX/ErY;

    .line 2173116
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->b:LX/ErY;

    .line 2173117
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->CONTACTS:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->c:LX/ErY;

    .line 2173118
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->d:LX/ErY;

    .line 2173119
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->REVIEW:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->e:LX/ErY;

    .line 2173120
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ErW;->f:Z

    .line 2173121
    return-void
.end method

.method public static a(LX/0QB;)LX/ErW;
    .locals 3

    .prologue
    .line 2173102
    sget-object v0, LX/ErW;->g:LX/ErW;

    if-nez v0, :cond_1

    .line 2173103
    const-class v1, LX/ErW;

    monitor-enter v1

    .line 2173104
    :try_start_0
    sget-object v0, LX/ErW;->g:LX/ErW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2173105
    if-eqz v2, :cond_0

    .line 2173106
    :try_start_1
    new-instance v0, LX/ErW;

    invoke-direct {v0}, LX/ErW;-><init>()V

    .line 2173107
    move-object v0, v0

    .line 2173108
    sput-object v0, LX/ErW;->g:LX/ErW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173109
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2173110
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2173111
    :cond_1
    sget-object v0, LX/ErW;->g:LX/ErW;

    return-object v0

    .line 2173112
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2173113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/ErY;)Ljava/util/Map;
    .locals 12
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ErY;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2173091
    iget-object v0, p0, LX/ErY;->a:LX/ErX;

    move-object v0, v0

    .line 2173092
    invoke-virtual {v0}, LX/ErX;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 2173093
    const-string v0, "_count"

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2173094
    iget v1, p0, LX/ErY;->c:I

    move v1, v1

    .line 2173095
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "_selections"

    invoke-virtual {v6, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2173096
    iget v3, p0, LX/ErY;->d:I

    move v3, v3

    .line 2173097
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "_deselections"

    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2173098
    iget v5, p0, LX/ErY;->e:I

    move v5, v5

    .line 2173099
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, "_time_spent"

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2173100
    iget-wide v10, p0, LX/ErY;->f:J

    move-wide v8, v10

    .line 2173101
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method private d(LX/ErX;)LX/ErY;
    .locals 2

    .prologue
    .line 2173084
    sget-object v0, LX/ErV;->a:[I

    invoke-virtual {p1}, LX/ErX;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2173085
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2173086
    :pswitch_0
    iget-object v0, p0, LX/ErW;->d:LX/ErY;

    goto :goto_0

    .line 2173087
    :pswitch_1
    iget-object v0, p0, LX/ErW;->b:LX/ErY;

    goto :goto_0

    .line 2173088
    :pswitch_2
    iget-object v0, p0, LX/ErW;->a:LX/ErY;

    goto :goto_0

    .line 2173089
    :pswitch_3
    iget-object v0, p0, LX/ErW;->c:LX/ErY;

    goto :goto_0

    .line 2173090
    :pswitch_4
    iget-object v0, p0, LX/ErW;->e:LX/ErY;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2173077
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 2173078
    iget-object v1, p0, LX/ErW;->b:LX/ErY;

    invoke-static {v1}, LX/ErW;->a(LX/ErY;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2173079
    iget-object v1, p0, LX/ErW;->a:LX/ErY;

    invoke-static {v1}, LX/ErW;->a(LX/ErY;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2173080
    iget-object v1, p0, LX/ErW;->c:LX/ErY;

    invoke-static {v1}, LX/ErW;->a(LX/ErY;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2173081
    iget-object v1, p0, LX/ErW;->d:LX/ErY;

    invoke-static {v1}, LX/ErW;->a(LX/ErY;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2173082
    iget-object v1, p0, LX/ErW;->e:LX/ErY;

    invoke-static {v1}, LX/ErW;->a(LX/ErY;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2173083
    return-object v0
.end method

.method public final a(LX/ErX;)V
    .locals 3

    .prologue
    .line 2173072
    invoke-direct {p0, p1}, LX/ErW;->d(LX/ErX;)LX/ErY;

    move-result-object v0

    .line 2173073
    sget-object v1, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v1, v1

    .line 2173074
    invoke-virtual {v1}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v1

    iput-wide v1, v0, LX/ErY;->g:J

    .line 2173075
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/ErY;->b:Z

    .line 2173076
    return-void
.end method

.method public final a(LX/ErX;I)V
    .locals 1

    .prologue
    .line 2173069
    invoke-direct {p0, p1}, LX/ErW;->d(LX/ErX;)LX/ErY;

    move-result-object v0

    .line 2173070
    iget p0, v0, LX/ErY;->d:I

    add-int/2addr p0, p2

    iput p0, v0, LX/ErY;->d:I

    .line 2173071
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2173063
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->a:LX/ErY;

    .line 2173064
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->b:LX/ErY;

    .line 2173065
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->CONTACTS:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->c:LX/ErY;

    .line 2173066
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->d:LX/ErY;

    .line 2173067
    new-instance v0, LX/ErY;

    sget-object v1, LX/ErX;->REVIEW:LX/ErX;

    invoke-direct {v0, v1}, LX/ErY;-><init>(LX/ErX;)V

    iput-object v0, p0, LX/ErW;->e:LX/ErY;

    .line 2173068
    return-void
.end method

.method public final b(LX/ErX;)V
    .locals 1

    .prologue
    .line 2173050
    invoke-direct {p0, p1}, LX/ErW;->d(LX/ErX;)LX/ErY;

    move-result-object v0

    .line 2173051
    invoke-virtual {v0}, LX/ErY;->a()V

    .line 2173052
    invoke-virtual {v0}, LX/ErY;->e()V

    .line 2173053
    return-void
.end method

.method public final b(LX/ErX;I)V
    .locals 1

    .prologue
    .line 2173060
    invoke-direct {p0, p1}, LX/ErW;->d(LX/ErX;)LX/ErY;

    move-result-object v0

    .line 2173061
    iget p0, v0, LX/ErY;->e:I

    add-int/2addr p0, p2

    iput p0, v0, LX/ErY;->e:I

    .line 2173062
    return-void
.end method

.method public final c(LX/ErX;)V
    .locals 1

    .prologue
    .line 2173054
    invoke-direct {p0, p1}, LX/ErW;->d(LX/ErX;)LX/ErY;

    move-result-object v0

    .line 2173055
    iget-boolean p0, v0, LX/ErY;->b:Z

    if-eqz p0, :cond_0

    .line 2173056
    invoke-virtual {v0}, LX/ErY;->a()V

    .line 2173057
    :cond_0
    iget-boolean p0, v0, LX/ErY;->b:Z

    if-eqz p0, :cond_1

    .line 2173058
    invoke-virtual {v0}, LX/ErY;->e()V

    .line 2173059
    :cond_1
    return-void
.end method
