.class public final enum LX/DtA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DtA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DtA;

.field public static final enum PAYMENT_REQUEST:LX/DtA;

.field public static final enum PAYMENT_TRANSACTION:LX/DtA;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2051343
    new-instance v0, LX/DtA;

    const-string v1, "PAYMENT_REQUEST"

    invoke-direct {v0, v1, v2}, LX/DtA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DtA;->PAYMENT_REQUEST:LX/DtA;

    .line 2051344
    new-instance v0, LX/DtA;

    const-string v1, "PAYMENT_TRANSACTION"

    invoke-direct {v0, v1, v3}, LX/DtA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DtA;->PAYMENT_TRANSACTION:LX/DtA;

    .line 2051345
    const/4 v0, 0x2

    new-array v0, v0, [LX/DtA;

    sget-object v1, LX/DtA;->PAYMENT_REQUEST:LX/DtA;

    aput-object v1, v0, v2

    sget-object v1, LX/DtA;->PAYMENT_TRANSACTION:LX/DtA;

    aput-object v1, v0, v3

    sput-object v0, LX/DtA;->$VALUES:[LX/DtA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2051346
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DtA;
    .locals 1

    .prologue
    .line 2051347
    const-class v0, LX/DtA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DtA;

    return-object v0
.end method

.method public static values()[LX/DtA;
    .locals 1

    .prologue
    .line 2051348
    sget-object v0, LX/DtA;->$VALUES:[LX/DtA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DtA;

    return-object v0
.end method
