.class public LX/E6z;
.super LX/E6y;
.source ""


# instance fields
.field private final a:LX/E1i;


# direct methods
.method public constructor <init>(LX/E1i;LX/3Tx;LX/0Ot;LX/0Ot;LX/0Ot;LX/961;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E1i;",
            "LX/3Tx;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/961;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080903
    invoke-direct/range {p0 .. p7}, LX/E6y;-><init>(LX/E1i;LX/3Tx;LX/0Ot;LX/0Ot;LX/0Ot;LX/961;Ljava/util/concurrent/Executor;)V

    .line 2080904
    iput-object p1, p0, LX/E6z;->a:LX/E1i;

    .line 2080905
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 2

    .prologue
    .line 2080906
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v0

    invoke-interface {v0}, LX/5sc;->m()Ljava/lang/String;

    move-result-object v0

    .line 2080907
    if-nez v0, :cond_0

    .line 2080908
    const/4 v0, 0x0

    .line 2080909
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, LX/Cfc;->FACEWEB_URL_TAP:LX/Cfc;

    invoke-static {v0, v1}, LX/E1i;->a(Landroid/net/Uri;LX/Cfc;)LX/Cfl;

    move-result-object v0

    goto :goto_0
.end method
