.class public final LX/Cvn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/util/Pair",
        "<",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/Cvp;


# direct methods
.method public constructor <init>(LX/Cvp;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1949296
    iput-object p1, p0, LX/Cvn;->b:LX/Cvp;

    iput-object p2, p0, LX/Cvn;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1949297
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1949298
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1949299
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Cvn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1949300
    iget-object v0, p0, LX/Cvn;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 1949301
    instance-of v4, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v4, :cond_1

    .line 1949302
    check-cast v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 1949303
    iget-boolean v4, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v4, v4

    .line 1949304
    if-nez v4, :cond_0

    .line 1949305
    iget-object v4, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, v4

    .line 1949306
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1949307
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1949308
    :cond_1
    instance-of v4, v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v4, :cond_0

    .line 1949309
    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 1949310
    iget-boolean v4, v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v4, v4

    .line 1949311
    if-nez v4, :cond_0

    .line 1949312
    invoke-virtual {v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1949313
    :cond_2
    iget-object v0, p0, LX/Cvn;->b:LX/Cvp;

    iget-object v0, v0, LX/Cvp;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Be;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7Be;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    iget-object v0, p0, LX/Cvn;->b:LX/Cvp;

    iget-object v0, v0, LX/Cvp;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Be;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/7Be;->b(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
