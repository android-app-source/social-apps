.class public LX/Erh;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Erh;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2173307
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2173308
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2173309
    const-string v1, "profiles"

    const/4 v2, 0x0

    new-array v2, v2, [J

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 2173310
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_INVITE_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2173311
    const-string v1, "event/{%s}/extendedinvite"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "event_id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2173312
    const-class v1, Lcom/facebook/events/invite/EventsInviteFriendsSelectorActivity;

    .line 2173313
    const-string v2, "event/{%s}/invite"

    invoke-static {v2}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "event_id"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2173314
    const-string v2, "event/{%s}/invitegroup/{%s}"

    invoke-static {v2}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "event_id"

    const-string v4, "group_id"

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2173315
    return-void
.end method

.method public static a(LX/0QB;)LX/Erh;
    .locals 3

    .prologue
    .line 2173316
    sget-object v0, LX/Erh;->a:LX/Erh;

    if-nez v0, :cond_1

    .line 2173317
    const-class v1, LX/Erh;

    monitor-enter v1

    .line 2173318
    :try_start_0
    sget-object v0, LX/Erh;->a:LX/Erh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2173319
    if-eqz v2, :cond_0

    .line 2173320
    :try_start_1
    new-instance v0, LX/Erh;

    invoke-direct {v0}, LX/Erh;-><init>()V

    .line 2173321
    move-object v0, v0

    .line 2173322
    sput-object v0, LX/Erh;->a:LX/Erh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173323
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2173324
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2173325
    :cond_1
    sget-object v0, LX/Erh;->a:LX/Erh;

    return-object v0

    .line 2173326
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2173327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
