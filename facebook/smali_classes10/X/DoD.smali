.class public final LX/DoD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/messaging/model/threads/ThreadSummary;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2041518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2041519
    check-cast p1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    check-cast p2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2041520
    iget-wide v0, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
