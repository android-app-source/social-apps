.class public LX/Cql;
.super LX/Cqk;
.source ""


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 8

    .prologue
    .line 1940407
    invoke-direct {p0, p1, p2}, LX/Cqk;-><init>(LX/Ctg;LX/CrK;)V

    .line 1940408
    new-instance v0, LX/Cqg;

    sget-object v2, LX/Cqw;->a:LX/Cqw;

    .line 1940409
    iget-object v1, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v3, v1

    .line 1940410
    check-cast v3, LX/Ctg;

    sget-object v4, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v5, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    sget-object v6, LX/Cqc;->ANNOTATION_OVERLAY:LX/Cqc;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/Cqg;-><init>(LX/Cql;LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940411
    new-instance v0, LX/Cqh;

    sget-object v2, LX/Cqw;->b:LX/Cqw;

    .line 1940412
    iget-object v1, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v3, v1

    .line 1940413
    check-cast v3, LX/Ctg;

    sget-object v4, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v5, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    sget-object v6, LX/Cqc;->ANNOTATION_OVERLAY:LX/Cqc;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/Cqh;-><init>(LX/Cql;LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940414
    return-void
.end method


# virtual methods
.method public final d()LX/Cqv;
    .locals 1

    .prologue
    .line 1940423
    :try_start_0
    invoke-virtual {p0}, LX/Cqk;->j()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0}, LX/CqX;->e()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    invoke-virtual {v0}, LX/Cqw;->e()LX/Cqw;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1940424
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1940425
    invoke-super {p0}, LX/Cqk;->h()V

    .line 1940426
    invoke-virtual {p0}, LX/Cqk;->j()LX/Cqj;

    move-result-object v0

    .line 1940427
    iget-object v1, v0, LX/CqX;->g:LX/CrK;

    move-object v0, v1

    .line 1940428
    invoke-virtual {p0, v0}, LX/CqX;->a(LX/CrK;)V

    .line 1940429
    return-void
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 1940415
    invoke-virtual {p0}, LX/Cqk;->j()LX/Cqj;

    move-result-object v0

    .line 1940416
    sget-object v1, LX/Cqw;->a:LX/Cqw;

    invoke-virtual {v0, v1}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v1

    .line 1940417
    invoke-virtual {p0}, LX/Cqk;->i()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v2

    invoke-static {v1, v2}, LX/Cqj;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 1940418
    invoke-virtual {v1}, LX/CrW;->e()I

    move-result v2

    invoke-virtual {v1}, LX/CrW;->f()I

    move-result v1

    invoke-virtual {p0, v2, v1}, LX/Cqi;->a(II)V

    .line 1940419
    sget-object v1, LX/Cqw;->b:LX/Cqw;

    invoke-virtual {v0, v1}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v0

    .line 1940420
    invoke-virtual {p0}, LX/Cqk;->i()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v1

    invoke-static {v0, v1}, LX/Cqj;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1940421
    invoke-virtual {v0}, LX/CrW;->e()I

    move-result v1

    invoke-virtual {v0}, LX/CrW;->f()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/Cqi;->b(II)V

    .line 1940422
    return-void
.end method
