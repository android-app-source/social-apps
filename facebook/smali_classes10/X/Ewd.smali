.class public LX/Ewd;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/Ewe;

.field private static final b:LX/Ewj;

.field private static final c:LX/Ewj;

.field private static final d:LX/Ewe;

.field private static final e:LX/Ewe;

.field private static final f:LX/Ewe;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2183750
    new-instance v0, LX/Ewl;

    invoke-direct {v0}, LX/Ewl;-><init>()V

    sput-object v0, LX/Ewd;->a:LX/Ewe;

    .line 2183751
    new-instance v0, LX/Ewk;

    invoke-direct {v0}, LX/Ewk;-><init>()V

    sput-object v0, LX/Ewd;->b:LX/Ewj;

    .line 2183752
    new-instance v0, LX/Ewm;

    invoke-direct {v0}, LX/Ewm;-><init>()V

    sput-object v0, LX/Ewd;->c:LX/Ewj;

    .line 2183753
    new-instance v0, LX/Ewg;

    invoke-direct {v0}, LX/Ewg;-><init>()V

    sput-object v0, LX/Ewd;->d:LX/Ewe;

    .line 2183754
    new-instance v0, LX/Ews;

    invoke-direct {v0}, LX/Ews;-><init>()V

    sput-object v0, LX/Ewd;->e:LX/Ewe;

    .line 2183755
    new-instance v0, LX/Ewq;

    invoke-direct {v0}, LX/Ewq;-><init>()V

    sput-object v0, LX/Ewd;->f:LX/Ewe;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2183727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;Ljava/util/List;LX/Ewf;ZZZI)Ljava/util/List;
    .locals 5
    .param p2    # LX/Ewf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Ewi;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/Ewo;",
            ">;",
            "LX/Ewf;",
            "ZZZI)",
            "Ljava/util/List",
            "<",
            "LX/Ewe;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2183728
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2183729
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    .line 2183730
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    .line 2183731
    if-eqz p2, :cond_0

    .line 2183732
    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2183733
    :cond_0
    if-nez v2, :cond_9

    .line 2183734
    sget-object v2, LX/Ewd;->b:LX/Ewj;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2183735
    if-eqz p4, :cond_1

    .line 2183736
    sget-object v2, LX/Ewd;->f:LX/Ewe;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2183737
    :cond_1
    if-eqz p5, :cond_2

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    move v2, v0

    :goto_0
    if-nez p4, :cond_7

    :goto_1
    and-int/2addr v0, v2

    if-eqz v0, :cond_2

    .line 2183738
    sget-object v0, LX/Ewd;->d:LX/Ewe;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2183739
    :cond_2
    if-ltz p6, :cond_3

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p6, v0, :cond_8

    .line 2183740
    :cond_3
    invoke-interface {v3, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2183741
    :cond_4
    :goto_2
    if-nez v4, :cond_5

    .line 2183742
    sget-object v0, LX/Ewd;->c:LX/Ewj;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2183743
    invoke-interface {v3, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2183744
    :cond_5
    return-object v3

    :cond_6
    move v2, v1

    .line 2183745
    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1

    .line 2183746
    :cond_8
    invoke-interface {p0, v1, p6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2183747
    sget-object v0, LX/Ewd;->e:LX/Ewe;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2183748
    :cond_9
    if-eqz v4, :cond_a

    if-eqz p3, :cond_4

    :cond_a
    if-nez p2, :cond_4

    .line 2183749
    sget-object v0, LX/Ewd;->a:LX/Ewe;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method
