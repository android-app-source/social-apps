.class public LX/CoB;
.super LX/CnT;
.source ""

# interfaces
.implements LX/Cny;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/WebViewBlockView;",
        "Lcom/facebook/richdocument/model/data/WebViewBlockData;",
        ">;",
        "LX/Cny;"
    }
.end annotation


# instance fields
.field public d:LX/Cs3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:Z

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field private l:Z


# direct methods
.method public constructor <init>(LX/CqA;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1935190
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1935191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CoB;->j:Ljava/util/List;

    .line 1935192
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v4, p0

    check-cast v4, LX/CoB;

    invoke-static {v0}, LX/Cs3;->a(LX/0QB;)LX/Cs3;

    move-result-object v5

    check-cast v5, LX/Cs3;

    const/16 v6, 0x1032

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x31dc

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v0

    check-cast v0, LX/0kb;

    iput-object v5, v4, LX/CoB;->d:LX/Cs3;

    iput-object v6, v4, LX/CoB;->e:LX/0Ot;

    iput-object v7, v4, LX/CoB;->f:LX/0Ot;

    iput-object v8, v4, LX/CoB;->g:LX/0Uh;

    iput-object v0, v4, LX/CoB;->h:LX/0kb;

    .line 1935193
    iget-object v0, p0, LX/CoB;->g:LX/0Uh;

    const/16 v3, 0x3d7

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935194
    iput-boolean v1, p0, LX/CoB;->i:Z

    .line 1935195
    :goto_0
    const/4 v0, 0x0

    .line 1935196
    instance-of v3, p1, LX/CqA;

    if-eqz v3, :cond_2

    .line 1935197
    check-cast p1, LX/CqA;

    .line 1935198
    :goto_1
    iget-boolean v0, p0, LX/CoB;->i:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1935199
    iget-boolean v0, p1, LX/CqA;->Z:Z

    move v0, v0

    .line 1935200
    if-nez v0, :cond_1

    move v0, v1

    :goto_2
    iput-boolean v0, p0, LX/CoB;->k:Z

    .line 1935201
    iget-object v0, p0, LX/CoB;->g:LX/0Uh;

    const/16 v1, 0xc6

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CoB;->l:Z

    .line 1935202
    return-void

    .line 1935203
    :cond_0
    iget-object v0, p0, LX/CoB;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/2yD;->H:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CoB;->i:Z

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1935204
    goto :goto_2

    :cond_2
    move-object p1, v0

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 1935189
    check-cast p1, LX/Cmi;

    invoke-virtual {p0, p1}, LX/CoB;->a(LX/Cmi;)V

    return-void
.end method

.method public final a(LX/Cmi;)V
    .locals 19

    .prologue
    .line 1935143
    move-object/from16 v0, p0

    iget-object v2, v0, LX/CoB;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/2yD;->m:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1935144
    move-object/from16 v0, p0

    iget-object v2, v0, LX/CoB;->h:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1935145
    :goto_0
    return-void

    .line 1935146
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/CoB;->k:Z

    if-eqz v2, :cond_1

    .line 1935147
    move-object/from16 v0, p0

    iget-object v2, v0, LX/CoB;->j:Ljava/util/List;

    new-instance v3, Lcom/facebook/richdocument/presenter/WebViewBlockPresenter$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/facebook/richdocument/presenter/WebViewBlockPresenter$1;-><init>(LX/CoB;LX/Cmi;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1935148
    :cond_1
    invoke-virtual/range {p1 .. p1}, LX/Cmi;->a()Ljava/lang/String;

    move-result-object v12

    .line 1935149
    invoke-virtual/range {p1 .. p1}, LX/Cmi;->r()Ljava/lang/String;

    move-result-object v5

    .line 1935150
    invoke-virtual/range {p1 .. p1}, LX/Cmi;->s()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object v8

    .line 1935151
    invoke-virtual/range {p1 .. p1}, LX/Cmi;->t()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v9

    .line 1935152
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v8, v2, :cond_4

    sget-object v7, LX/CtY;->BYPASS_LIMITS:LX/CtY;

    .line 1935153
    :goto_1
    invoke-interface/range {p1 .. p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v3

    .line 1935154
    invoke-static {v12}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    move-object v4, v2

    .line 1935155
    :goto_2
    invoke-virtual/range {p0 .. p0}, LX/CnT;->a()LX/CnG;

    move-result-object v2

    check-cast v2, LX/CqA;

    invoke-interface/range {p1 .. p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v6

    invoke-interface {v2, v6}, LX/CnG;->a(LX/Cml;)V

    .line 1935156
    invoke-virtual/range {p1 .. p1}, LX/Cmi;->l()I

    move-result v11

    .line 1935157
    invoke-virtual/range {p1 .. p1}, LX/Cmi;->b()I

    move-result v10

    .line 1935158
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1935159
    invoke-virtual/range {p0 .. p0}, LX/CnT;->a()LX/CnG;

    move-result-object v2

    check-cast v2, LX/CqA;

    invoke-virtual/range {p1 .. p1}, LX/Cmi;->v()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v9}, LX/CqA;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/CtY;Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V

    .line 1935160
    :cond_2
    :goto_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v8, v2, :cond_3

    .line 1935161
    invoke-virtual/range {p0 .. p0}, LX/CnT;->a()LX/CnG;

    move-result-object v2

    check-cast v2, LX/CqA;

    invoke-virtual {v2, v10, v11}, LX/CqA;->b(II)V

    .line 1935162
    invoke-virtual/range {p0 .. p0}, LX/CnT;->a()LX/CnG;

    move-result-object v2

    check-cast v2, LX/CqA;

    invoke-virtual/range {p1 .. p1}, LX/Cmi;->u()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/CqA;->a(Z)V

    .line 1935163
    :cond_3
    invoke-virtual/range {p0 .. p0}, LX/CnT;->a()LX/CnG;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, LX/CoB;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Chi;

    invoke-virtual {v2}, LX/Chi;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, LX/Clr;->o()Landroid/os/Bundle;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v3, v0, v2, v4}, LX/Co1;->a(LX/CnG;LX/Clq;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1935164
    :cond_4
    sget-object v7, LX/CtY;->NORMAL:LX/CtY;

    goto :goto_1

    .line 1935165
    :cond_5
    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v4, v2

    goto :goto_2

    .line 1935166
    :cond_6
    invoke-static {v12}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1935167
    invoke-virtual/range {p1 .. p1}, LX/Cmi;->t()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v5

    invoke-static {v8, v5}, LX/Cs3;->a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)Z

    move-result v2

    .line 1935168
    move-object/from16 v0, p0

    iget-object v5, v0, LX/CoB;->d:LX/Cs3;

    invoke-virtual/range {p0 .. p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6, v2, v8}, LX/Cs3;->a(Landroid/content/Context;ZLcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;)I

    move-result v5

    .line 1935169
    if-gt v11, v5, :cond_7

    if-eqz v2, :cond_f

    if-lez v11, :cond_f

    if-ge v11, v5, :cond_f

    .line 1935170
    :cond_7
    invoke-static {v5, v11}, Ljava/lang/Math;->min(II)I

    move-result v13

    .line 1935171
    if-lez v10, :cond_e

    .line 1935172
    int-to-float v2, v11

    int-to-float v6, v10

    div-float/2addr v2, v6

    .line 1935173
    int-to-float v6, v5

    div-float v2, v6, v2

    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v14

    double-to-int v2, v14

    .line 1935174
    invoke-static {v2, v10}, Ljava/lang/Math;->min(II)I

    move-result v6

    move/from16 v17, v6

    move/from16 v18, v13

    .line 1935175
    :goto_4
    move-object/from16 v0, p0

    iget-object v6, v0, LX/CoB;->d:LX/Cs3;

    invoke-virtual {v6, v4}, LX/Cs3;->a(Landroid/net/Uri;)Z

    move-result v6

    .line 1935176
    if-eqz v6, :cond_d

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v8, v6, :cond_8

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/CoB;->l:Z

    if-nez v6, :cond_d

    .line 1935177
    :cond_8
    if-lez v11, :cond_9

    if-lez v10, :cond_9

    .line 1935178
    const-string v6, "<html><body style=\"margin:0\"><iframe src=\"%s\" width=\"%d\" height=\"%d\" frameborder=\"0\"/></body></html>"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v10, v11

    const/4 v5, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v10, v5

    invoke-static {v6, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1935179
    :goto_5
    if-nez v4, :cond_c

    const-string v4, ""

    .line 1935180
    :goto_6
    invoke-virtual/range {p0 .. p0}, LX/CnT;->a()LX/CnG;

    move-result-object v2

    check-cast v2, LX/CqA;

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v9}, LX/CqA;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/CtY;Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V

    move/from16 v10, v17

    move/from16 v11, v18

    .line 1935181
    goto/16 :goto_3

    .line 1935182
    :cond_9
    if-lez v11, :cond_a

    .line 1935183
    const-string v2, "<html><body style=\"margin:0\"><iframe src=\"%s\" width=\"%d\" frameborder=\"0\"/></body></html>"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v12, v6, v10

    const/4 v10, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v6, v10

    invoke-static {v2, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 1935184
    :cond_a
    if-lez v10, :cond_b

    .line 1935185
    const-string v5, "<html><body style=\"margin:0\"><iframe src=\"%s\" height=\"%d\" width=\"100%%\" frameborder=\"0\"/></body></html>"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v12, v6, v10

    const/4 v10, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v10

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 1935186
    :cond_b
    const-string v2, "<html><body style=\"margin:0\"><iframe src=\"%s\" width=\"100%%\" frameborder=\"0\"/></body></html>"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v12, v5, v6

    invoke-static {v2, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 1935187
    :cond_c
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_6

    .line 1935188
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/CnT;->a()LX/CnG;

    move-result-object v10

    check-cast v10, LX/CqA;

    const/4 v13, 0x0

    move-object v11, v3

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v16, v9

    invoke-virtual/range {v10 .. v16}, LX/CqA;->a(Ljava/lang/String;Ljava/lang/String;ILX/CtY;Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V

    move/from16 v10, v17

    move/from16 v11, v18

    goto/16 :goto_3

    :cond_e
    move v2, v10

    move/from16 v17, v10

    move/from16 v18, v13

    goto/16 :goto_4

    :cond_f
    move v2, v10

    move v5, v11

    move/from16 v17, v10

    move/from16 v18, v11

    goto/16 :goto_4
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1935205
    iget-boolean v0, p0, LX/CoB;->k:Z

    if-eqz v0, :cond_1

    .line 1935206
    iget-object v0, p0, LX/CoB;->j:Ljava/util/List;

    new-instance v1, Lcom/facebook/richdocument/presenter/WebViewBlockPresenter$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/richdocument/presenter/WebViewBlockPresenter$3;-><init>(LX/CoB;Landroid/os/Bundle;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1935207
    :cond_0
    :goto_0
    return-void

    .line 1935208
    :cond_1
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 1935209
    const-string v0, "extra_webview_height"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1935210
    if-lez v1, :cond_0

    .line 1935211
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935212
    check-cast v0, LX/CqA;

    .line 1935213
    iget-object p0, v0, LX/CqA;->aa:LX/CsX;

    if-nez p0, :cond_2

    .line 1935214
    iput v1, v0, LX/CqA;->S:I

    .line 1935215
    :goto_1
    goto :goto_0

    .line 1935216
    :cond_2
    iget-object p0, v0, LX/CqA;->aa:LX/CsX;

    .line 1935217
    iget-object v0, p0, LX/CsX;->h:LX/CsU;

    invoke-virtual {v0}, LX/CsU;->a()V

    .line 1935218
    invoke-virtual {p0}, LX/CsX;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1935219
    invoke-virtual {p0}, LX/CsX;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/CsX;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1935220
    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1935123
    iget-boolean v0, p0, LX/CoB;->k:Z

    if-eqz v0, :cond_0

    .line 1935124
    iget-object v0, p0, LX/CoB;->j:Ljava/util/List;

    new-instance v1, Lcom/facebook/richdocument/presenter/WebViewBlockPresenter$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/richdocument/presenter/WebViewBlockPresenter$2;-><init>(LX/CoB;Landroid/os/Bundle;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1935125
    :goto_0
    return-void

    .line 1935126
    :cond_0
    invoke-super {p0, p1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 1935127
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935128
    check-cast v0, LX/CqA;

    .line 1935129
    iget-object v1, v0, LX/CqA;->aa:LX/CsX;

    .line 1935130
    invoke-virtual {v1}, LX/CsX;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    iget p0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1935131
    const/4 v0, -0x2

    if-eq p0, v0, :cond_2

    if-lez p0, :cond_2

    .line 1935132
    :goto_1
    move v1, p0

    .line 1935133
    move v0, v1

    .line 1935134
    if-lez v0, :cond_1

    .line 1935135
    const-string v1, "extra_webview_height"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 1935136
    :cond_1
    const-string v0, "extra_webview_height"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0

    .line 1935137
    :cond_2
    iget p0, v1, LX/CsX;->p:I

    if-lez p0, :cond_3

    .line 1935138
    iget p0, v1, LX/CsX;->p:I

    goto :goto_1

    .line 1935139
    :cond_3
    iget p0, v1, LX/CsX;->j:F

    const/4 v0, 0x0

    cmpl-float p0, p0, v0

    if-lez p0, :cond_4

    .line 1935140
    invoke-virtual {v1}, LX/CsX;->getMeasuredWidth()I

    move-result p0

    int-to-float p0, p0

    iget v0, v1, LX/CsX;->j:F

    div-float/2addr p0, v0

    float-to-int p0, p0

    .line 1935141
    goto :goto_1

    .line 1935142
    :cond_4
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1935114
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1935122
    const/4 v0, 0x1

    return v0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1935115
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935116
    check-cast v0, LX/CqA;

    .line 1935117
    iget-object v1, v0, LX/CqA;->h:LX/Cta;

    iget-object v2, v0, LX/CqA;->aa:LX/CsX;

    .line 1935118
    new-instance p0, LX/CtX;

    invoke-direct {p0, v2}, LX/CtX;-><init>(LX/CsX;)V

    .line 1935119
    invoke-static {v1, p0}, LX/Cta;->a$redex0(LX/Cta;LX/CtX;)V

    .line 1935120
    iget-object v1, v0, LX/CqA;->y:LX/CqH;

    iget-object v2, v0, LX/CqA;->aa:LX/CsX;

    invoke-virtual {v1, v2}, LX/CqH;->a(LX/CsX;)V

    .line 1935121
    return-void
.end method
