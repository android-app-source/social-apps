.class public LX/DDc;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1975872
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DDc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1975873
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1975866
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1975867
    const-class v0, LX/DDc;

    invoke-static {v0, p0}, LX/DDc;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1975868
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/DDc;->setOrientation(I)V

    .line 1975869
    const v0, 0x7f0307fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1975870
    const v0, 0x7f0d150e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DDc;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1975871
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/DDc;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object p0

    check-cast p0, LX/23P;

    iput-object p0, p1, LX/DDc;->a:LX/23P;

    return-void
.end method


# virtual methods
.method public setButtonContainerVisibility(I)V
    .locals 1

    .prologue
    .line 1975864
    iget-object v0, p0, LX/DDc;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1975865
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1975862
    iget-object v0, p0, LX/DDc;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1975863
    return-void
.end method
