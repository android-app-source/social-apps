.class public final LX/Eud;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/Eui;


# direct methods
.method public constructor <init>(LX/Eui;LX/0TF;)V
    .locals 0

    .prologue
    .line 2179776
    iput-object p1, p0, LX/Eud;->b:LX/Eui;

    iput-object p2, p0, LX/Eud;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2179777
    iget-object v0, p0, LX/Eud;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2179778
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2179779
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2179780
    if-eqz p1, :cond_0

    .line 2179781
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179782
    if-eqz v0, :cond_0

    .line 2179783
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179784
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2179785
    :cond_0
    iget-object v0, p0, LX/Eud;->a:LX/0TF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2179786
    :goto_0
    return-void

    .line 2179787
    :cond_1
    iget-object v1, p0, LX/Eud;->a:LX/0TF;

    .line 2179788
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179789
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;->a()LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
