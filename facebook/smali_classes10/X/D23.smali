.class public LX/D23;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B5y;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5SF;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/media/MediaMetadataRetriever;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/D2D;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1958076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958077
    return-void
.end method

.method public static a(LX/0QB;)LX/D23;
    .locals 1

    .prologue
    .line 1958075
    invoke-static {p0}, LX/D23;->b(LX/0QB;)LX/D23;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;I)V
    .locals 4

    .prologue
    .line 1958069
    iget-object v0, p0, LX/D23;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 1958070
    iget-object v1, p0, LX/D23;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1958071
    sget-object v2, LX/0ax;->bD:Ljava/lang/String;

    invoke-interface {v1, p1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1958072
    const-string v2, "video_model"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "session_id"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1958073
    invoke-interface {v0, v1, p4, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1958074
    return-void
.end method

.method private a(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;IJ)V
    .locals 9

    .prologue
    .line 1958022
    iget-object v0, p0, LX/D23;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 1958023
    iget-object v1, p0, LX/D23;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5SF;

    .line 1958024
    iget-object v2, p0, LX/D23;->f:LX/0ad;

    sget v3, LX/0wf;->aN:I

    const/4 v4, 0x7

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    .line 1958025
    iget-object v3, p0, LX/D23;->f:LX/0ad;

    sget-short v4, LX/0wf;->aL:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 1958026
    new-instance v4, LX/5SJ;

    invoke-direct {v4}, LX/5SJ;-><init>()V

    .line 1958027
    iget-object v5, p3, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    move-object v5, v5

    .line 1958028
    sget-object v6, LX/74j;->REMOTE_MEDIA:LX/74j;

    invoke-virtual {v6}, LX/74j;->getValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/5SJ;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;

    move-result-object v4

    .line 1958029
    iput-object p2, v4, LX/5SJ;->d:Ljava/lang/String;

    .line 1958030
    move-object v4, v4

    .line 1958031
    const/4 v5, 0x1

    .line 1958032
    iput-boolean v5, v4, LX/5SJ;->o:Z

    .line 1958033
    move-object v4, v4

    .line 1958034
    const v5, 0x7f082a32

    .line 1958035
    iput v5, v4, LX/5SJ;->f:I

    .line 1958036
    move-object v4, v4

    .line 1958037
    const v5, 0x7f082a30

    .line 1958038
    iput v5, v4, LX/5SJ;->g:I

    .line 1958039
    move-object v4, v4

    .line 1958040
    iget-object v5, p3, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v5, v5

    .line 1958041
    iput-object v5, v4, LX/5SJ;->n:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1958042
    move-object v4, v4

    .line 1958043
    invoke-virtual {v4, p5, p6}, LX/5SJ;->a(J)LX/5SJ;

    move-result-object v4

    invoke-virtual {v4}, LX/5SJ;->b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v4

    .line 1958044
    new-instance v5, LX/5SM;

    invoke-direct {v5}, LX/5SM;-><init>()V

    .line 1958045
    iput-object p2, v5, LX/5SM;->b:Ljava/lang/String;

    .line 1958046
    move-object v5, v5

    .line 1958047
    const/4 v6, 0x0

    .line 1958048
    iput-boolean v6, v5, LX/5SM;->e:Z

    .line 1958049
    move-object v5, v5

    .line 1958050
    iput-boolean v3, v5, LX/5SM;->f:Z

    .line 1958051
    move-object v3, v5

    .line 1958052
    const/4 v5, 0x1

    .line 1958053
    iput-boolean v5, v3, LX/5SM;->h:Z

    .line 1958054
    move-object v3, v3

    .line 1958055
    const/4 v5, 0x1

    .line 1958056
    iput-boolean v5, v3, LX/5SM;->d:Z

    .line 1958057
    move-object v3, v3

    .line 1958058
    mul-int/lit16 v2, v2, 0x3e8

    .line 1958059
    iput v2, v3, LX/5SM;->l:I

    .line 1958060
    move-object v2, v3

    .line 1958061
    const/4 v3, 0x1

    .line 1958062
    iput-boolean v3, v2, LX/5SM;->i:Z

    .line 1958063
    move-object v2, v2

    .line 1958064
    invoke-virtual {v2}, LX/5SM;->a()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-result-object v2

    .line 1958065
    invoke-interface {v1, p1, v4, v2}, LX/5SF;->a(Landroid/content/Context;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v1

    .line 1958066
    const-string v2, "video_model"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1958067
    invoke-interface {v0, v1, p4, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1958068
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0xb4

    const/4 v4, 0x1

    .line 1958078
    iget-object v0, p0, LX/D23;->e:LX/D2D;

    sget-object v1, LX/D2C;->VIDEO_TOO_SMALL:LX/D2C;

    invoke-virtual {v0, v1, p2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    .line 1958079
    const v0, 0x7f082a35

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1958080
    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1958081
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;I)Z
    .locals 4

    .prologue
    .line 1958012
    int-to-long v0, p3

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 1958013
    iget-object v0, p0, LX/D23;->e:LX/D2D;

    .line 1958014
    sget-object v1, LX/D2C;->VIDEO_TOO_LONG:LX/D2C;

    invoke-virtual {v1}, LX/D2C;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, LX/D2D;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1958015
    const-string v2, "video_duration"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1958016
    invoke-static {v0, v1}, LX/D2D;->a(LX/D2D;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1958017
    const/16 v0, 0x12c

    const/4 p2, 0x1

    .line 1958018
    const v1, 0x7f082a34

    new-array v2, p2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1958019
    invoke-static {p1, v1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1958020
    const/4 v0, 0x0

    .line 1958021
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1958007
    iget-object v1, p0, LX/D23;->f:LX/0ad;

    sget-short v2, LX/0wf;->aM:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p1, :cond_1

    .line 1958008
    iget-object v1, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v1, v1

    .line 1958009
    if-eqz v1, :cond_1

    .line 1958010
    iget-object v1, p1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v1, v1

    .line 1958011
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static b(LX/0QB;)LX/D23;
    .locals 7

    .prologue
    .line 1958003
    new-instance v0, LX/D23;

    invoke-direct {v0}, LX/D23;-><init>()V

    .line 1958004
    const/16 v1, 0x36f4

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0xbc6

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x455

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x20

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/D2D;->a(LX/0QB;)LX/D2D;

    move-result-object v5

    check-cast v5, LX/D2D;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    .line 1958005
    iput-object v1, v0, LX/D23;->a:LX/0Or;

    iput-object v2, v0, LX/D23;->b:LX/0Or;

    iput-object v3, v0, LX/D23;->c:LX/0Or;

    iput-object v4, v0, LX/D23;->d:LX/0Or;

    iput-object v5, v0, LX/D23;->e:LX/D2D;

    iput-object v6, v0, LX/D23;->f:LX/0ad;

    .line 1958006
    return-object v0
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1957976
    iget-object v0, p0, LX/D23;->e:LX/D2D;

    sget-object v1, LX/D2C;->INVALID_VIDEO:LX/D2C;

    invoke-virtual {v0, v1, p2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    .line 1957977
    const v0, 0x7f082a36

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1957978
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;IILX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V
    .locals 9
    .param p6    # LX/5QV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1957979
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1957980
    :cond_0
    invoke-direct {p0, p1, p2}, LX/D23;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 1957981
    :cond_1
    :goto_0
    return-void

    .line 1957982
    :cond_2
    const/4 v0, 0x4

    if-eq v0, p4, :cond_3

    const/4 v0, 0x3

    if-eq v0, p4, :cond_3

    const/4 v0, 0x5

    if-ne v0, p4, :cond_4

    .line 1957983
    :cond_3
    iget-object v0, p0, LX/D23;->e:LX/D2D;

    sget-object v1, LX/D2C;->EXISTING_VIDEO:LX/D2C;

    invoke-virtual {v0, v1, p2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    .line 1957984
    :goto_1
    iget-object v0, p0, LX/D23;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaMetadataRetriever;

    .line 1957985
    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1957986
    invoke-static {v0}, LX/BTk;->a(Landroid/media/MediaMetadataRetriever;)J

    move-result-wide v2

    long-to-int v1, v2

    .line 1957987
    invoke-static {v0}, LX/BTk;->c(Landroid/media/MediaMetadataRetriever;)I

    move-result v2

    .line 1957988
    invoke-static {v0}, LX/BTk;->d(Landroid/media/MediaMetadataRetriever;)I

    move-result v3

    .line 1957989
    invoke-static {v0}, LX/BTk;->e(Landroid/media/MediaMetadataRetriever;)I

    move-result v4

    .line 1957990
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1957991
    invoke-direct {p0, p1, p2, v1}, LX/D23;->a(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1957992
    if-nez v1, :cond_5

    .line 1957993
    invoke-direct {p0, p1, p2}, LX/D23;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 1957994
    :cond_4
    iget-object v0, p0, LX/D23;->e:LX/D2D;

    sget-object v1, LX/D2C;->TAKE_VIDEO:LX/D2C;

    invoke-virtual {v0, v1, p2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    goto :goto_1

    .line 1957995
    :cond_5
    const/16 v0, 0xb4

    if-lt v2, v0, :cond_6

    const/16 v0, 0xb4

    if-ge v3, v0, :cond_7

    .line 1957996
    :cond_6
    invoke-direct {p0, p1, p2}, LX/D23;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 1957997
    :cond_7
    invoke-static {v4, v2, v3}, LX/7K8;->a(III)Landroid/graphics/RectF;

    move-result-object v4

    .line 1957998
    iget-object v0, p0, LX/D23;->f:LX/0ad;

    sget v2, LX/0wf;->aN:I

    const/4 v3, 0x7

    invoke-interface {v0, v2, v3}, LX/0ad;->a(II)I

    move-result v0

    .line 1957999
    mul-int/lit16 v2, v0, 0x3e8

    move-object v0, p3

    move v3, p4

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-static/range {v0 .. v7}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a(Landroid/net/Uri;IIILandroid/graphics/RectF;LX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v4

    .line 1958000
    invoke-direct {p0, v4}, LX/D23;->a(Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;)Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p5

    move-wide/from16 v6, p9

    .line 1958001
    invoke-direct/range {v1 .. v7}, LX/D23;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;IJ)V

    goto :goto_0

    .line 1958002
    :cond_8
    invoke-direct {p0, p1, p2, v4, p5}, LX/D23;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;I)V

    goto/16 :goto_0
.end method
