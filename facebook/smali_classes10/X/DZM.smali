.class public final LX/DZM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DQQ;


# instance fields
.field public final synthetic a:LX/DZW;


# direct methods
.method public constructor <init>(LX/DZW;)V
    .locals 0

    .prologue
    .line 2013296
    iput-object p1, p0, LX/DZM;->a:LX/DZW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V
    .locals 2

    .prologue
    .line 2013292
    iget-object v0, p0, LX/DZM;->a:LX/DZW;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2013293
    iput-object v1, v0, LX/DZW;->i:Ljava/lang/String;

    .line 2013294
    iget-object v0, p0, LX/DZM;->a:LX/DZW;

    invoke-static {v0}, LX/DZW;->c(LX/DZW;)V

    .line 2013295
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;)V
    .locals 1

    .prologue
    .line 2013284
    iget-object v0, p0, LX/DZM;->a:LX/DZW;

    .line 2013285
    iput-object p1, v0, LX/DZW;->j:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 2013286
    iget-object v0, p0, LX/DZM;->a:LX/DZW;

    invoke-static {v0}, LX/DZW;->c(LX/DZW;)V

    .line 2013287
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V
    .locals 2

    .prologue
    .line 2013288
    iget-object v0, p0, LX/DZM;->a:LX/DZW;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2013289
    iput-object v1, v0, LX/DZW;->g:Ljava/lang/String;

    .line 2013290
    iget-object v0, p0, LX/DZM;->a:LX/DZW;

    invoke-static {v0}, LX/DZW;->c(LX/DZW;)V

    .line 2013291
    return-void
.end method
