.class public final LX/Dff;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/messaging/inbox2/morefooter/InboxMoreConversationsFooterItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2027636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2027638
    new-instance v0, Lcom/facebook/messaging/inbox2/morefooter/InboxMoreConversationsFooterItem;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/inbox2/morefooter/InboxMoreConversationsFooterItem;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2027637
    new-array v0, p1, [Lcom/facebook/messaging/inbox2/morefooter/InboxMoreConversationsFooterItem;

    return-object v0
.end method
