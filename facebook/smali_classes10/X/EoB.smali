.class public LX/EoB;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field private final b:LX/Eo0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2167959
    const-class v0, LX/EoB;

    sput-object v0, LX/EoB;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Eo0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2167960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167961
    iput-object p1, p0, LX/EoB;->b:LX/Eo0;

    .line 2167962
    return-void
.end method

.method public static b(LX/0QB;)LX/EoB;
    .locals 2

    .prologue
    .line 2167963
    new-instance v1, LX/EoB;

    const-class v0, LX/Eo0;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Eo0;

    invoke-direct {v1, v0}, LX/EoB;-><init>(LX/Eo0;)V

    .line 2167964
    return-object v1
.end method


# virtual methods
.method public final a(LX/1My;LX/Enh;LX/Eoc;LX/Emq;LX/Emy;Ljava/lang/String;LX/En3;LX/0P1;Ljava/lang/String;)LX/Enl;
    .locals 12
    .param p8    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1My;",
            "LX/Enh;",
            "Lcom/facebook/entitycards/service/EntityCardsEntityLoader;",
            "LX/Emq;",
            "LX/Emy;",
            "Ljava/lang/String;",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/entitycards/model/EntityCardsDataSource;"
        }
    .end annotation

    .prologue
    .line 2167965
    if-eqz p8, :cond_0

    .line 2167966
    invoke-virtual/range {p7 .. p7}, LX/En2;->c()I

    move-result v0

    .line 2167967
    invoke-virtual/range {p8 .. p8}, LX/0P1;->size()I

    move-result v1

    .line 2167968
    if-eq v0, v1, :cond_0

    .line 2167969
    sget-object v2, LX/EoB;->a:Ljava/lang/Class;

    const-string v3, "Discarding preliminary {%d} models that don\'t match the {%d} initial ids"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2167970
    const/16 p8, 0x0

    move-object/from16 v8, p8

    .line 2167971
    :goto_0
    iget-object v0, p0, LX/EoB;->b:LX/Eo0;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v11}, LX/Eo0;->a(LX/1My;LX/Enh;LX/Eoc;LX/Emq;LX/Emy;Ljava/lang/String;LX/En3;LX/0P1;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)LX/Enz;

    move-result-object v0

    return-object v0

    :cond_0
    move-object/from16 v8, p8

    goto :goto_0
.end method
