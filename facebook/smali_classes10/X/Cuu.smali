.class public LX/Cuu;
.super LX/7Mr;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public final o:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

.field public p:LX/Cut;

.field public q:LX/Cju;

.field public r:Ljava/util/Locale;

.field public s:LX/CqV;

.field private t:I

.field private u:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1947251
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Cuu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1947252
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1947249
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Cuu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947250
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1947242
    invoke-direct {p0, p1, p2, p3}, LX/7Mr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947243
    iput v0, p0, LX/Cuu;->t:I

    .line 1947244
    iput v0, p0, LX/Cuu;->u:I

    .line 1947245
    const-class v0, LX/Cuu;

    invoke-static {v0, p0}, LX/Cuu;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1947246
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Cuu;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    iput-object v0, p0, LX/Cuu;->o:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    .line 1947247
    iget-object v0, p0, LX/Cuu;->o:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    invoke-virtual {p0, v0}, LX/Cuu;->removeView(Landroid/view/View;)V

    .line 1947248
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/Cuu;

    invoke-static {v3}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    invoke-static {v3}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v2

    check-cast v2, LX/0W9;

    invoke-static {v3}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v3

    check-cast v3, LX/CqV;

    iput-object v1, p1, LX/Cuu;->q:LX/Cju;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v4

    iput-object v4, p1, LX/Cuu;->r:Ljava/util/Locale;

    iput-object v3, p1, LX/Cuu;->s:LX/CqV;

    const/4 v3, 0x0

    const v4, 0x7f0d132a

    invoke-virtual {p1, v4}, LX/Cuu;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    iget-object p0, p1, LX/Cuu;->s:LX/CqV;

    sget-object v0, LX/CqU;->VIDEO_CONTROLS:LX/CqU;

    if-nez v4, :cond_1

    :goto_0
    iget-object p0, p1, LX/Cuu;->q:LX/Cju;

    const v0, 0x7f0d011b

    invoke-interface {p0, v0}, LX/Cju;->c(I)I

    move-result v0

    iget-object p0, p1, LX/Cuu;->q:LX/Cju;

    const v1, 0x7f0d0133

    invoke-interface {p0, v1}, LX/Cju;->c(I)I

    move-result v1

    const p0, 0x7f0d1329

    invoke-virtual {p1, p0}, LX/Cuu;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/resources/ui/FbTextView;

    iput-object p0, p1, LX/Cuu;->a:Lcom/facebook/resources/ui/FbTextView;

    const p0, 0x7f0d132b

    invoke-virtual {p1, p0}, LX/Cuu;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/resources/ui/FbTextView;

    iput-object p0, p1, LX/Cuu;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object p0, p1, LX/Cuu;->a:Lcom/facebook/resources/ui/FbTextView;

    int-to-float v2, v1

    invoke-virtual {p0, v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    iget-object p0, p1, LX/Cuu;->b:Lcom/facebook/resources/ui/FbTextView;

    int-to-float v1, v1

    invoke-virtual {p0, v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v2, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {p0, v0, v1, v0, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    :cond_0
    invoke-virtual {v4, p0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    check-cast p0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    return-void

    :cond_1
    new-instance v1, LX/CqT;

    invoke-direct {v1, p0, v0}, LX/CqT;-><init>(LX/CqV;LX/CqU;)V

    invoke-virtual {v4, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method private d(I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1947228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1947229
    new-instance v1, Ljava/util/Formatter;

    iget-object v2, p0, LX/Cuu;->r:Ljava/util/Locale;

    invoke-direct {v1, v0, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 1947230
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1947231
    div-int/lit16 v0, p1, 0x3e8

    .line 1947232
    rem-int/lit8 v2, v0, 0x3c

    .line 1947233
    div-int/lit8 v3, v0, 0x3c

    rem-int/lit8 v3, v3, 0x3c

    .line 1947234
    div-int/lit16 v0, v0, 0xe10

    .line 1947235
    if-lez v0, :cond_0

    .line 1947236
    const-string v4, "%d:%2d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v1, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1947237
    :goto_0
    return-object v0

    .line 1947238
    :cond_0
    const/16 v0, 0x9

    if-le v3, v0, :cond_1

    .line 1947239
    const-string v0, "%2d:%02d"

    .line 1947240
    :goto_1
    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-virtual {v1, v0, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1947241
    :cond_1
    const-string v0, "%d:%02d"

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1947211
    return-void
.end method

.method public final b(II)V
    .locals 3

    .prologue
    .line 1947217
    div-int/lit16 v0, p2, 0x3e8

    .line 1947218
    div-int/lit16 v1, p1, 0x3e8

    .line 1947219
    sub-int/2addr v0, v1

    .line 1947220
    iget v2, p0, LX/Cuu;->t:I

    if-ne v1, v2, :cond_0

    iget v2, p0, LX/Cuu;->u:I

    if-eq v0, v2, :cond_1

    .line 1947221
    :cond_0
    iput v1, p0, LX/Cuu;->t:I

    .line 1947222
    iput v0, p0, LX/Cuu;->u:I

    .line 1947223
    mul-int/lit16 v1, v1, 0x3e8

    invoke-direct {p0, v1}, LX/Cuu;->d(I)Ljava/lang/String;

    move-result-object v1

    .line 1947224
    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {p0, v0}, LX/Cuu;->d(I)Ljava/lang/String;

    move-result-object v0

    .line 1947225
    iget-object v2, p0, LX/Cuu;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1947226
    iget-object v1, p0, LX/Cuu;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1947227
    :cond_1
    return-void
.end method

.method public getActiveThumbResource()I
    .locals 1

    .prologue
    .line 1947216
    const v0, 0x7f02167f

    return v0
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1947215
    const v0, 0x7f03121d

    return v0
.end method

.method public setEventBus(LX/2oj;)V
    .locals 2

    .prologue
    .line 1947212
    new-instance v0, LX/Cut;

    iget-object v1, p0, LX/2oy;->h:Ljava/util/List;

    invoke-direct {v0, v1}, LX/Cut;-><init>(Ljava/util/List;)V

    iput-object v0, p0, LX/Cuu;->p:LX/Cut;

    .line 1947213
    invoke-super {p0, p1}, LX/7Mr;->setEventBus(LX/2oj;)V

    .line 1947214
    return-void
.end method
