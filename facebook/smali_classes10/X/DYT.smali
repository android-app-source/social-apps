.class public LX/DYT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/DYS;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Ljava/lang/String;

.field public final d:LX/0tX;

.field public final e:LX/DYZ;

.field public f:LX/0kL;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/String;LX/0kL;LX/0tX;LX/DYZ;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2011533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2011534
    iput-object p1, p0, LX/DYT;->b:Ljava/util/concurrent/ExecutorService;

    .line 2011535
    iput-object p2, p0, LX/DYT;->c:Ljava/lang/String;

    .line 2011536
    iput-object p3, p0, LX/DYT;->f:LX/0kL;

    .line 2011537
    iput-object p4, p0, LX/DYT;->d:LX/0tX;

    .line 2011538
    iput-object p5, p0, LX/DYT;->e:LX/DYZ;

    .line 2011539
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/DYT;->a:Ljava/util/HashMap;

    .line 2011540
    return-void
.end method

.method public static b(LX/0QB;)LX/DYT;
    .locals 6

    .prologue
    .line 2011541
    new-instance v0, LX/DYT;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/DYZ;->a(LX/0QB;)LX/DYZ;

    move-result-object v5

    check-cast v5, LX/DYZ;

    invoke-direct/range {v0 .. v5}, LX/DYT;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/String;LX/0kL;LX/0tX;LX/DYZ;)V

    .line 2011542
    return-object v0
.end method
