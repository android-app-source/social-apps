.class public final LX/Crp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Crn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1941415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1941392
    instance-of v0, p1, LX/Clq;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 1941393
    check-cast v0, LX/Clq;

    .line 1941394
    invoke-interface {v0}, LX/Clq;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1941395
    const/4 v1, 0x1

    .line 1941396
    :goto_0
    invoke-interface {v0}, LX/Clq;->iX_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1941397
    add-int/lit8 v1, v1, 0x1

    .line 1941398
    :cond_0
    invoke-interface {v0}, LX/Clq;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1941399
    add-int/lit8 v1, v1, 0x1

    .line 1941400
    :cond_1
    instance-of v0, p1, LX/Cm3;

    if-eqz v0, :cond_6

    move-object v0, p1

    check-cast v0, LX/Cm3;

    invoke-interface {v0}, LX/Cm3;->a()LX/Clo;

    move-result-object v0

    if-eqz v0, :cond_6

    move-object v0, p1

    check-cast v0, LX/Cm3;

    invoke-interface {v0}, LX/Cm3;->a()LX/Clo;

    move-result-object v0

    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v0

    if-lez v0, :cond_6

    .line 1941401
    check-cast p1, LX/Cm3;

    .line 1941402
    invoke-interface {p1}, LX/Cm3;->a()LX/Clo;

    move-result-object v0

    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v3

    .line 1941403
    :goto_1
    if-ge v2, v3, :cond_6

    .line 1941404
    invoke-interface {p1}, LX/Cm3;->a()LX/Clo;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/Clo;->a(I)LX/Clr;

    move-result-object v0

    .line 1941405
    instance-of v4, v0, LX/Clq;

    if-eqz v4, :cond_4

    .line 1941406
    check-cast v0, LX/Clq;

    .line 1941407
    invoke-interface {v0}, LX/Clq;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1941408
    add-int/lit8 v1, v1, 0x1

    .line 1941409
    :cond_2
    invoke-interface {v0}, LX/Clq;->iX_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1941410
    add-int/lit8 v1, v1, 0x1

    .line 1941411
    :cond_3
    invoke-interface {v0}, LX/Clq;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1941412
    add-int/lit8 v1, v1, 0x1

    .line 1941413
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v1, v2

    .line 1941414
    :cond_6
    return v1

    :cond_7
    move v1, v2

    goto :goto_0
.end method
