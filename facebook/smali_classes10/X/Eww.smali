.class public final LX/Eww;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V
    .locals 0

    .prologue
    .line 2183841
    iput-object p1, p0, LX/Eww;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2183842
    const/16 v4, 0x14

    .line 2183843
    iget-object v0, p0, LX/Eww;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->g:Z

    if-eqz v0, :cond_0

    .line 2183844
    iget-object v0, p0, LX/Eww;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eub;

    sget-object v1, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v2, p0, LX/Eww;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v2, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->p:Ljava/lang/String;

    new-instance v3, LX/Ewv;

    invoke-direct {v3, p0}, LX/Ewv;-><init>(LX/Eww;)V

    .line 2183845
    invoke-static {v0, v1, v2, v4}, LX/Eub;->b(LX/Eub;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;I)LX/0zO;

    move-result-object v5

    sget-object v6, LX/0zS;->d:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    const/4 v6, 0x1

    .line 2183846
    iput-boolean v6, v5, LX/0zO;->p:Z

    .line 2183847
    move-object v6, v5

    .line 2183848
    iget-object v5, v0, LX/Eub;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1My;

    new-instance v7, LX/EuZ;

    invoke-direct {v7, v0, v3}, LX/EuZ;-><init>(LX/Eub;LX/0TF;)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 2183849
    iget-object v9, v6, LX/0zO;->m:LX/0gW;

    move-object v9, v9

    .line 2183850
    iget-object p0, v9, LX/0gW;->f:Ljava/lang/String;

    move-object v9, p0

    .line 2183851
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, LX/Eub;->e:I

    add-int/lit8 p0, v9, 0x1

    iput p0, v0, LX/Eub;->e:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2183852
    invoke-static {v0, v5}, LX/Eub;->a(LX/Eub;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 2183853
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Eww;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eub;

    sget-object v1, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v2, p0, LX/Eww;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v2, v2, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->p:Ljava/lang/String;

    .line 2183854
    invoke-static {v0, v1, v2, v4}, LX/Eub;->b(LX/Eub;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;I)LX/0zO;

    move-result-object v5

    .line 2183855
    iget-object v3, v0, LX/Eub;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-virtual {v3, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2183856
    invoke-static {v0, v3}, LX/Eub;->a(LX/Eub;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2183857
    goto :goto_0
.end method
