.class public final enum LX/EmV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EmV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EmV;

.field public static final enum ERROR_DOWNLOAD_MANAGER_COMPLETION_EXCEPTION:LX/EmV;

.field public static final enum ERROR_DOWNLOAD_MANAGER_FAILURE:LX/EmV;

.field public static final enum ERROR_EMPTY_DOWNLOAD_MANAGER_CURSOR:LX/EmV;

.field public static final enum ERROR_FAILED_TO_REMOVE_DOWNLOAD_ID:LX/EmV;

.field public static final enum ERROR_NO_FREE_SPACE_TO_DOWNLOAD:LX/EmV;

.field public static final enum ERROR_QUERYING_DOWNLOAD_SIZE:LX/EmV;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2165922
    new-instance v0, LX/EmV;

    const-string v1, "ERROR_NO_FREE_SPACE_TO_DOWNLOAD"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/EmV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EmV;->ERROR_NO_FREE_SPACE_TO_DOWNLOAD:LX/EmV;

    .line 2165923
    new-instance v0, LX/EmV;

    const-string v1, "ERROR_FAILED_TO_REMOVE_DOWNLOAD_ID"

    invoke-direct {v0, v1, v3, v4}, LX/EmV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EmV;->ERROR_FAILED_TO_REMOVE_DOWNLOAD_ID:LX/EmV;

    .line 2165924
    new-instance v0, LX/EmV;

    const-string v1, "ERROR_EMPTY_DOWNLOAD_MANAGER_CURSOR"

    invoke-direct {v0, v1, v4, v5}, LX/EmV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EmV;->ERROR_EMPTY_DOWNLOAD_MANAGER_CURSOR:LX/EmV;

    .line 2165925
    new-instance v0, LX/EmV;

    const-string v1, "ERROR_DOWNLOAD_MANAGER_FAILURE"

    invoke-direct {v0, v1, v5, v6}, LX/EmV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EmV;->ERROR_DOWNLOAD_MANAGER_FAILURE:LX/EmV;

    .line 2165926
    new-instance v0, LX/EmV;

    const-string v1, "ERROR_DOWNLOAD_MANAGER_COMPLETION_EXCEPTION"

    invoke-direct {v0, v1, v6, v7}, LX/EmV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EmV;->ERROR_DOWNLOAD_MANAGER_COMPLETION_EXCEPTION:LX/EmV;

    .line 2165927
    new-instance v0, LX/EmV;

    const-string v1, "ERROR_QUERYING_DOWNLOAD_SIZE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, LX/EmV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EmV;->ERROR_QUERYING_DOWNLOAD_SIZE:LX/EmV;

    .line 2165928
    const/4 v0, 0x6

    new-array v0, v0, [LX/EmV;

    const/4 v1, 0x0

    sget-object v2, LX/EmV;->ERROR_NO_FREE_SPACE_TO_DOWNLOAD:LX/EmV;

    aput-object v2, v0, v1

    sget-object v1, LX/EmV;->ERROR_FAILED_TO_REMOVE_DOWNLOAD_ID:LX/EmV;

    aput-object v1, v0, v3

    sget-object v1, LX/EmV;->ERROR_EMPTY_DOWNLOAD_MANAGER_CURSOR:LX/EmV;

    aput-object v1, v0, v4

    sget-object v1, LX/EmV;->ERROR_DOWNLOAD_MANAGER_FAILURE:LX/EmV;

    aput-object v1, v0, v5

    sget-object v1, LX/EmV;->ERROR_DOWNLOAD_MANAGER_COMPLETION_EXCEPTION:LX/EmV;

    aput-object v1, v0, v6

    sget-object v1, LX/EmV;->ERROR_QUERYING_DOWNLOAD_SIZE:LX/EmV;

    aput-object v1, v0, v7

    sput-object v0, LX/EmV;->$VALUES:[LX/EmV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2165930
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/EmV;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EmV;
    .locals 1

    .prologue
    .line 2165931
    const-class v0, LX/EmV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EmV;

    return-object v0
.end method

.method public static values()[LX/EmV;
    .locals 1

    .prologue
    .line 2165929
    sget-object v0, LX/EmV;->$VALUES:[LX/EmV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EmV;

    return-object v0
.end method
