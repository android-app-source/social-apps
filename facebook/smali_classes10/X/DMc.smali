.class public final LX/DMc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DMe;


# direct methods
.method public constructor <init>(LX/DMe;)V
    .locals 0

    .prologue
    .line 1990012
    iput-object p1, p0, LX/DMc;->a:LX/DMe;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1990044
    iget-object v0, p0, LX/DMc;->a:LX/DMe;

    iget-object v0, v0, LX/DMe;->i:LX/DMT;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/DMT;->a(Z)V

    .line 1990045
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1990013
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1990014
    if-eqz p1, :cond_0

    .line 1990015
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1990016
    if-nez v0, :cond_1

    .line 1990017
    :cond_0
    :goto_0
    return-void

    .line 1990018
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1990019
    check-cast v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    .line 1990020
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v3, :cond_2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v3, :cond_2

    .line 1990021
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1990022
    check-cast v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v0, v3, :cond_2

    .line 1990023
    iget-object v0, p0, LX/DMc;->a:LX/DMe;

    iget-object v0, v0, LX/DMe;->i:LX/DMT;

    .line 1990024
    iget-object v1, v0, LX/DMT;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    invoke-static {v1, v2}, Lcom/facebook/groups/events/GroupEventsBaseFragment;->a$redex0(Lcom/facebook/groups/events/GroupEventsBaseFragment;Z)V

    .line 1990025
    goto :goto_0

    .line 1990026
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1990027
    check-cast v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->a()Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    move-result-object v3

    .line 1990028
    invoke-virtual {v3}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v5, p0, LX/DMc;->a:LX/DMe;

    invoke-virtual {v4, v0, v1}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 1990029
    :goto_1
    iput-boolean v0, v5, LX/DMe;->l:Z

    .line 1990030
    invoke-virtual {v3}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v5, p0, LX/DMc;->a:LX/DMe;

    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1990031
    iput-object v0, v5, LX/DMe;->k:Ljava/lang/String;

    .line 1990032
    iget-object v0, p0, LX/DMc;->a:LX/DMe;

    iget-object v0, v0, LX/DMe;->i:LX/DMT;

    invoke-virtual {v0, v2}, LX/DMT;->a(Z)V

    .line 1990033
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1990034
    iget-object v0, p0, LX/DMc;->a:LX/DMe;

    iget-object v0, v0, LX/DMe;->h:LX/0Px;

    if-eqz v0, :cond_3

    .line 1990035
    iget-object v0, p0, LX/DMc;->a:LX/DMe;

    iget-object v0, v0, LX/DMe;->h:LX/0Px;

    invoke-virtual {v4, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1990036
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    :goto_2
    if-ge v2, v5, :cond_5

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel$EdgesModel;

    .line 1990037
    invoke-virtual {v0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v6

    invoke-static {v6}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1990038
    iget-object v6, p0, LX/DMc;->a:LX/DMe;

    invoke-virtual {v0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    invoke-static {v6, v0, p1}, LX/DMe;->a$redex0(LX/DMe;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1990039
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1990040
    goto :goto_1

    .line 1990041
    :cond_5
    iget-object v0, p0, LX/DMc;->a:LX/DMe;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1990042
    iput-object v2, v0, LX/DMe;->h:LX/0Px;

    .line 1990043
    iget-object v0, p0, LX/DMc;->a:LX/DMe;

    iget-object v0, v0, LX/DMe;->i:LX/DMT;

    iget-object v2, p0, LX/DMc;->a:LX/DMe;

    iget-object v2, v2, LX/DMe;->h:LX/0Px;

    invoke-virtual {v0, v2, v1}, LX/DMT;->a(LX/0Px;Z)V

    goto/16 :goto_0
.end method
