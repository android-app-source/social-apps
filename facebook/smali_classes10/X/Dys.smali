.class public final enum LX/Dys;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dys;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dys;

.field public static final enum NIEM_CLICKED:LX/Dys;

.field public static final enum NONE:LX/Dys;

.field public static final enum PTR:LX/Dys;

.field public static final enum QUERY:LX/Dys;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2064872
    new-instance v0, LX/Dys;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/Dys;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dys;->NONE:LX/Dys;

    .line 2064873
    new-instance v0, LX/Dys;

    const-string v1, "PTR"

    invoke-direct {v0, v1, v3}, LX/Dys;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dys;->PTR:LX/Dys;

    .line 2064874
    new-instance v0, LX/Dys;

    const-string v1, "QUERY"

    invoke-direct {v0, v1, v4}, LX/Dys;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dys;->QUERY:LX/Dys;

    .line 2064875
    new-instance v0, LX/Dys;

    const-string v1, "NIEM_CLICKED"

    invoke-direct {v0, v1, v5}, LX/Dys;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dys;->NIEM_CLICKED:LX/Dys;

    .line 2064876
    const/4 v0, 0x4

    new-array v0, v0, [LX/Dys;

    sget-object v1, LX/Dys;->NONE:LX/Dys;

    aput-object v1, v0, v2

    sget-object v1, LX/Dys;->PTR:LX/Dys;

    aput-object v1, v0, v3

    sget-object v1, LX/Dys;->QUERY:LX/Dys;

    aput-object v1, v0, v4

    sget-object v1, LX/Dys;->NIEM_CLICKED:LX/Dys;

    aput-object v1, v0, v5

    sput-object v0, LX/Dys;->$VALUES:[LX/Dys;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2064871
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dys;
    .locals 1

    .prologue
    .line 2064870
    const-class v0, LX/Dys;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dys;

    return-object v0
.end method

.method public static values()[LX/Dys;
    .locals 1

    .prologue
    .line 2064869
    sget-object v0, LX/Dys;->$VALUES:[LX/Dys;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dys;

    return-object v0
.end method
