.class public LX/Efa;
.super LX/1OM;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private final a:LX/EfW;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/AFW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/EfW;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2154885
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2154886
    const/4 v0, 0x0

    iput-object v0, p0, LX/Efa;->b:LX/0Px;

    .line 2154887
    iput-object p1, p0, LX/Efa;->a:LX/EfW;

    .line 2154888
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 13

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 2154783
    if-nez p2, :cond_0

    .line 2154784
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031369

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2154785
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2154786
    new-instance v0, LX/EfY;

    invoke-direct {v0, v1}, LX/EfY;-><init>(Landroid/view/View;)V

    .line 2154787
    :goto_0
    return-object v0

    .line 2154788
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 2154789
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031367

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2154790
    iget-object v1, p0, LX/Efa;->a:LX/EfW;

    .line 2154791
    new-instance v4, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    invoke-static {v1}, LX/1xf;->a(LX/0QB;)LX/1xf;

    move-result-object v6

    check-cast v6, LX/1xf;

    invoke-static {v1}, LX/AFQ;->b(LX/0QB;)LX/AFQ;

    move-result-object v7

    check-cast v7, LX/AFQ;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v8

    check-cast v8, LX/1Ad;

    invoke-static {v1}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v9

    check-cast v9, LX/0fO;

    invoke-static {v1}, LX/AFJ;->a(LX/0QB;)LX/AFJ;

    move-result-object v10

    check-cast v10, LX/AFJ;

    invoke-static {v1}, LX/0he;->a(LX/0QB;)LX/0he;

    move-result-object v11

    check-cast v11, LX/0he;

    invoke-static {v1}, LX/AFO;->a(LX/0QB;)LX/AFO;

    move-result-object v12

    check-cast v12, LX/AFO;

    move-object v5, v0

    invoke-direct/range {v4 .. v12}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;-><init>(Landroid/view/View;LX/1xf;LX/AFQ;LX/1Ad;LX/0fO;LX/AFJ;LX/0he;LX/AFO;)V

    .line 2154792
    move-object v0, v4

    .line 2154793
    goto :goto_0

    .line 2154794
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 2154795
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031368

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2154796
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2154797
    new-instance v0, LX/EfZ;

    invoke-direct {v0, p0, v1}, LX/EfZ;-><init>(LX/Efa;Landroid/view/View;)V

    goto :goto_0

    .line 2154798
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    .line 2154799
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2154800
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2154801
    iget-object v0, p0, LX/Efa;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AFW;

    .line 2154802
    check-cast p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    .line 2154803
    iget-object v1, v0, LX/AFW;->a:LX/7h0;

    move-object v2, v1

    .line 2154804
    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->A:LX/0he;

    .line 2154805
    iget-object v1, v2, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v1

    .line 2154806
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v4

    .line 2154807
    iget-boolean v1, v2, LX/7h0;->h:Z

    move v1, v1

    .line 2154808
    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 2154809
    :goto_0
    iget-object v5, v3, LX/0he;->h:LX/7gT;

    iget-object v6, v3, LX/0he;->o:Ljava/lang/String;

    .line 2154810
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2154811
    sget-object v8, LX/7gS;->TARGET_ID:LX/7gS;

    invoke-virtual {v8}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154812
    sget-object v8, LX/7gS;->ROW_INDEX:LX/7gS;

    invoke-virtual {v8}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v7, v8, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2154813
    sget-object v8, LX/7gS;->HAS_NEW_CONTENT:LX/7gS;

    invoke-virtual {v8}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2154814
    sget-object v8, LX/7gS;->DIRECT_SESSION_ID:LX/7gS;

    invoke-virtual {v8}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154815
    sget-object v8, LX/7gR;->ROW_IMPRESSION:LX/7gR;

    invoke-static {v5, v8, v7}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 2154816
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2154817
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->w:LX/AFW;

    if-eqz v1, :cond_2

    .line 2154818
    iget-object v1, v2, LX/7h0;->i:Ljava/lang/String;

    move-object v1, v1

    .line 2154819
    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->w:LX/AFW;

    .line 2154820
    iget-object v4, v3, LX/AFW;->a:LX/7h0;

    move-object v3, v4

    .line 2154821
    iget-object v4, v3, LX/7h0;->i:Ljava/lang/String;

    move-object v3, v4

    .line 2154822
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->w:LX/AFW;

    invoke-static {v1, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(LX/AFW;LX/AFW;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2154823
    iput-object v0, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->w:LX/AFW;

    .line 2154824
    invoke-static {p1, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->b$redex0(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V

    .line 2154825
    :cond_0
    :goto_1
    return-void

    .line 2154826
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2154827
    :cond_2
    iput-object v0, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->w:LX/AFW;

    .line 2154828
    invoke-static {p1, v2}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/7h0;)V

    .line 2154829
    const/4 v8, 0x0

    const/16 v7, 0x8

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2154830
    iget-boolean v1, v2, LX/7h0;->l:Z

    move v1, v1

    .line 2154831
    if-eqz v1, :cond_4

    .line 2154832
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2154833
    new-array v1, v4, [Landroid/view/View;

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->t:Lcom/facebook/resources/ui/FbTextView;

    aput-object v3, v1, v5

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->z:Landroid/view/View;

    aput-object v3, v1, v6

    invoke-static {v7, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154834
    new-array v1, v6, [Landroid/view/View;

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v3, v1, v5

    invoke-static {v5, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154835
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v3, 0x3e4ccccd    # 0.2f

    invoke-virtual {v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 2154836
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2154837
    iget-object v3, v2, LX/7h0;->e:Landroid/net/Uri;

    move-object v3, v3

    .line 2154838
    sget-object v4, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2154839
    iget-object v1, v2, LX/7h0;->n:LX/7gy;

    move-object v1, v1

    .line 2154840
    sget-object v3, LX/7gy;->INCOMING_STORY:LX/7gy;

    if-ne v1, v3, :cond_3

    .line 2154841
    new-array v1, v6, [Landroid/view/View;

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->u:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v3, v1, v5

    invoke-static {v7, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154842
    :goto_2
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v8, v5}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2154843
    :goto_3
    invoke-static {p1, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a$redex0(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V

    .line 2154844
    invoke-static {p1, v0}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->b$redex0(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/AFW;)V

    goto :goto_1

    .line 2154845
    :cond_3
    new-array v1, v6, [Landroid/view/View;

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->u:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v3, v1, v5

    invoke-static {v5, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    goto :goto_2

    .line 2154846
    :cond_4
    iget-boolean v1, v2, LX/7h0;->h:Z

    move v1, v1

    .line 2154847
    if-nez v1, :cond_6

    .line 2154848
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    const v3, 0x7f0a07ea

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2154849
    new-array v1, v4, [Landroid/view/View;

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->t:Lcom/facebook/resources/ui/FbTextView;

    aput-object v3, v1, v5

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v3, v1, v6

    invoke-static {v5, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154850
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 2154851
    iget v3, v2, LX/7h0;->j:I

    move v3, v3

    .line 2154852
    const/16 p0, 0x9

    if-le v3, p0, :cond_7

    .line 2154853
    iget-object p0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    .line 2154854
    const p2, 0x7f082aae

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2154855
    :goto_4
    move-object v3, p0

    .line 2154856
    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2154857
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 2154858
    iget-object v1, v2, LX/7h0;->n:LX/7gy;

    move-object v1, v1

    .line 2154859
    sget-object v3, LX/7gy;->INCOMING_STORY:LX/7gy;

    if-ne v1, v3, :cond_5

    .line 2154860
    new-array v1, v4, [Landroid/view/View;

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->u:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v3, v1, v5

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->z:Landroid/view/View;

    aput-object v3, v1, v6

    invoke-static {v7, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154861
    iget-object v1, v2, LX/7h0;->e:Landroid/net/Uri;

    move-object v1, v1

    .line 2154862
    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v3

    .line 2154863
    iget-boolean v1, v2, LX/7h0;->l:Z

    move v1, v1

    .line 2154864
    if-eqz v1, :cond_9

    const/4 v1, 0x0

    .line 2154865
    :goto_5
    iput-object v1, v3, LX/1bX;->j:LX/33B;

    .line 2154866
    move-object v1, v3

    .line 2154867
    invoke-static {}, LX/1bd;->a()LX/1bd;

    move-result-object v3

    .line 2154868
    iput-object v3, v1, LX/1bX;->d:LX/1bd;

    .line 2154869
    move-object v1, v1

    .line 2154870
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 2154871
    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->v:LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->o()LX/1Ad;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v3, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2154872
    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2154873
    :goto_6
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v8, v6}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/16 :goto_3

    .line 2154874
    :cond_5
    new-array v1, v4, [Landroid/view/View;

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->u:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v3, v1, v5

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->z:Landroid/view/View;

    aput-object v3, v1, v6

    invoke-static {v5, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154875
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2154876
    iget-object v3, v2, LX/7h0;->e:Landroid/net/Uri;

    move-object v3, v3

    .line 2154877
    sget-object v4, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_6

    .line 2154878
    :cond_6
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2154879
    const/4 v1, 0x4

    new-array v1, v1, [Landroid/view/View;

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->t:Lcom/facebook/resources/ui/FbTextView;

    aput-object v3, v1, v5

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->z:Landroid/view/View;

    aput-object v3, v1, v6

    iget-object v3, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->u:Lcom/facebook/fbui/glyph/GlyphView;

    aput-object v3, v1, v4

    const/4 v3, 0x3

    iget-object v4, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    aput-object v4, v1, v3

    invoke-static {v7, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->a(I[Landroid/view/View;)V

    .line 2154880
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v8, v5}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/16 :goto_3

    .line 2154881
    :cond_7
    if-gtz v3, :cond_8

    .line 2154882
    const-string p0, ""

    goto/16 :goto_4

    .line 2154883
    :cond_8
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_4

    .line 2154884
    :cond_9
    iget-object v1, p1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->G:LX/33B;

    goto :goto_5
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2154774
    iget-object v0, p0, LX/Efa;->b:LX/0Px;

    if-nez v0, :cond_0

    .line 2154775
    const/4 v0, 0x2

    .line 2154776
    :goto_0
    return v0

    .line 2154777
    :cond_0
    iget-object v0, p0, LX/Efa;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2154778
    const/4 v0, 0x0

    goto :goto_0

    .line 2154779
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2154780
    iget-object v0, p0, LX/Efa;->b:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Efa;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2154781
    :cond_0
    const/4 v0, 0x1

    .line 2154782
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/Efa;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
