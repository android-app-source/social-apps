.class public LX/EKk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1nD;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/17Y;


# direct methods
.method public constructor <init>(LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2107157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2107158
    iput-object p1, p0, LX/EKk;->a:LX/1nD;

    .line 2107159
    iput-object p2, p0, LX/EKk;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2107160
    iput-object p3, p0, LX/EKk;->c:LX/17Y;

    .line 2107161
    return-void
.end method

.method public static a(LX/0QB;)LX/EKk;
    .locals 6

    .prologue
    .line 2107162
    const-class v1, LX/EKk;

    monitor-enter v1

    .line 2107163
    :try_start_0
    sget-object v0, LX/EKk;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107164
    sput-object v2, LX/EKk;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107165
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107166
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107167
    new-instance p0, LX/EKk;

    invoke-static {v0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v3

    check-cast v3, LX/1nD;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-direct {p0, v3, v4, v5}, LX/EKk;-><init>(LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 2107168
    move-object v0, p0

    .line 2107169
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107170
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107171
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107172
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
