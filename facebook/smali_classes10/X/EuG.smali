.class public LX/EuG;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EuF;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EuH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2179303
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EuG;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EuH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2179304
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2179305
    iput-object p1, p0, LX/EuG;->b:LX/0Ot;

    .line 2179306
    return-void
.end method

.method public static a(LX/0QB;)LX/EuG;
    .locals 4

    .prologue
    .line 2179307
    const-class v1, LX/EuG;

    monitor-enter v1

    .line 2179308
    :try_start_0
    sget-object v0, LX/EuG;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2179309
    sput-object v2, LX/EuG;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2179310
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2179311
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2179312
    new-instance v3, LX/EuG;

    const/16 p0, 0x222f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EuG;-><init>(LX/0Ot;)V

    .line 2179313
    move-object v0, v3

    .line 2179314
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2179315
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EuG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2179316
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2179317
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2179318
    check-cast p2, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;

    .line 2179319
    iget-object v0, p0, LX/EuG;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EuH;

    iget-object v1, p2, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-object v2, p2, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    const/4 p0, 0x0

    .line 2179320
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    move-object v4, v3

    .line 2179321
    :goto_0
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p2

    if-nez v4, :cond_1

    const v3, 0x7f0203b2

    :goto_1
    invoke-virtual {p2, v3}, LX/1up;->h(I)LX/1up;

    move-result-object p2

    iget-object v3, v0, LX/EuH;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3, v4}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b08a6

    invoke-interface {v3, p0, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x2

    const p0, 0x7f0b08a6

    invoke-interface {v3, v4, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    const p0, 0x7f0b1536

    invoke-interface {v3, v4, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x3

    const p0, 0x7f0b1536

    invoke-interface {v3, v4, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b008e

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b008e

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2179322
    return-object v0

    .line 2179323
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    goto :goto_0

    :cond_1
    move v3, p0

    .line 2179324
    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2179325
    invoke-static {}, LX/1dS;->b()V

    .line 2179326
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/EuF;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2179327
    new-instance v1, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;

    invoke-direct {v1, p0}, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;-><init>(LX/EuG;)V

    .line 2179328
    sget-object v2, LX/EuG;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EuF;

    .line 2179329
    if-nez v2, :cond_0

    .line 2179330
    new-instance v2, LX/EuF;

    invoke-direct {v2}, LX/EuF;-><init>()V

    .line 2179331
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EuF;->a$redex0(LX/EuF;LX/1De;IILcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;)V

    .line 2179332
    move-object v1, v2

    .line 2179333
    move-object v0, v1

    .line 2179334
    return-object v0
.end method
