.class public final enum LX/Dqm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dqm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dqm;

.field public static final enum CLOSE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

.field public static final enum CONFIRM_AND_SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

.field public static final enum CONFIRM_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

.field public static final enum CONFIRM_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

.field public static final enum REJECT_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

.field public static final enum REJECT_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

.field public static final enum SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

.field public static final enum SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

.field public static final enum SEND_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2048508
    new-instance v0, LX/Dqm;

    const-string v1, "CONFIRM_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE"

    invoke-direct {v0, v1, v3}, LX/Dqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dqm;->CONFIRM_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    .line 2048509
    new-instance v0, LX/Dqm;

    const-string v1, "REJECT_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE"

    invoke-direct {v0, v1, v4}, LX/Dqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dqm;->REJECT_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    .line 2048510
    new-instance v0, LX/Dqm;

    const-string v1, "SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE"

    invoke-direct {v0, v1, v5}, LX/Dqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dqm;->SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    .line 2048511
    new-instance v0, LX/Dqm;

    const-string v1, "SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE"

    invoke-direct {v0, v1, v6}, LX/Dqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dqm;->SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    .line 2048512
    new-instance v0, LX/Dqm;

    const-string v1, "REJECT_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE"

    invoke-direct {v0, v1, v7}, LX/Dqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dqm;->REJECT_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    .line 2048513
    new-instance v0, LX/Dqm;

    const-string v1, "CONFIRM_AND_SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Dqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dqm;->CONFIRM_AND_SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    .line 2048514
    new-instance v0, LX/Dqm;

    const-string v1, "CONFIRM_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Dqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dqm;->CONFIRM_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    .line 2048515
    new-instance v0, LX/Dqm;

    const-string v1, "CLOSE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Dqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dqm;->CLOSE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    .line 2048516
    new-instance v0, LX/Dqm;

    const-string v1, "SEND_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Dqm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dqm;->SEND_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    .line 2048517
    const/16 v0, 0x9

    new-array v0, v0, [LX/Dqm;

    sget-object v1, LX/Dqm;->CONFIRM_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    aput-object v1, v0, v3

    sget-object v1, LX/Dqm;->REJECT_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    aput-object v1, v0, v4

    sget-object v1, LX/Dqm;->SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    aput-object v1, v0, v5

    sget-object v1, LX/Dqm;->SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    aput-object v1, v0, v6

    sget-object v1, LX/Dqm;->REJECT_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Dqm;->CONFIRM_AND_SEE_PROFILE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Dqm;->CONFIRM_AND_SEE_REQUESTS_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Dqm;->CLOSE_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Dqm;->SEND_FRIEND_REQUEST_PUSH_NOTIFICATION_REQUEST_TYPE:LX/Dqm;

    aput-object v2, v0, v1

    sput-object v0, LX/Dqm;->$VALUES:[LX/Dqm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2048518
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dqm;
    .locals 1

    .prologue
    .line 2048519
    const-class v0, LX/Dqm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dqm;

    return-object v0
.end method

.method public static values()[LX/Dqm;
    .locals 1

    .prologue
    .line 2048520
    sget-object v0, LX/Dqm;->$VALUES:[LX/Dqm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dqm;

    return-object v0
.end method
