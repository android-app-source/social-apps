.class public LX/CzG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/search/results/model/SearchResultsMutableContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1954477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954478
    return-void
.end method

.method public static a(LX/0QB;)LX/CzG;
    .locals 3

    .prologue
    .line 1954479
    const-class v1, LX/CzG;

    monitor-enter v1

    .line 1954480
    :try_start_0
    sget-object v0, LX/CzG;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1954481
    sput-object v2, LX/CzG;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1954482
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1954483
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1954484
    new-instance v0, LX/CzG;

    invoke-direct {v0}, LX/CzG;-><init>()V

    .line 1954485
    move-object v0, v0

    .line 1954486
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1954487
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CzG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1954488
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1954489
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
