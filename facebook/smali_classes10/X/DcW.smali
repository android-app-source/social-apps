.class public LX/DcW;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:LX/03V;

.field public final f:LX/0tX;

.field private final g:Landroid/content/res/Resources;

.field public final h:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0kL;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2018532
    const-class v0, LX/DcW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DcW;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0tX;Landroid/content/res/Resources;LX/1Ck;LX/0kL;LX/0Or;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tX;",
            "Landroid/content/res/Resources;",
            "LX/1Ck;",
            "LX/0kL;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018522
    iput-object p1, p0, LX/DcW;->e:LX/03V;

    .line 2018523
    iput-object p2, p0, LX/DcW;->f:LX/0tX;

    .line 2018524
    iput-object p3, p0, LX/DcW;->g:Landroid/content/res/Resources;

    .line 2018525
    iput-object p4, p0, LX/DcW;->h:LX/1Ck;

    .line 2018526
    iput-object p5, p0, LX/DcW;->i:LX/0kL;

    .line 2018527
    iget-object v0, p0, LX/DcW;->g:Landroid/content/res/Resources;

    const v1, 0x7f0b1c60

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/DcW;->b:I

    .line 2018528
    iget-object v0, p0, LX/DcW;->g:Landroid/content/res/Resources;

    const v1, 0x7f0b1c5e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/DcW;->c:I

    .line 2018529
    iget-object v0, p0, LX/DcW;->g:Landroid/content/res/Resources;

    const v1, 0x7f0b1c5d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/DcW;->d:I

    .line 2018530
    iput-object p6, p0, LX/DcW;->j:LX/0Or;

    .line 2018531
    return-void
.end method

.method public static a(LX/DcW;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;LX/DcX;)V
    .locals 3

    .prologue
    .line 2018504
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2018505
    invoke-static {p2}, LX/DcW;->b(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2018506
    const v0, 0x7f0d2e09

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/springbutton/SpringScaleButton;

    .line 2018507
    invoke-virtual {p2}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->c()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setSelected(Z)V

    .line 2018508
    invoke-virtual {p2}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f080fc9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2018509
    new-instance v1, LX/DcU;

    invoke-direct {v1, p0, p1, p2, p3}, LX/DcU;-><init>(LX/DcW;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;LX/DcX;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2018510
    iget-object v1, p0, LX/DcW;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/215;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->a(LX/215;)V

    .line 2018511
    return-void

    .line 2018512
    :cond_0
    const v2, 0x7f080fc8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 2018513
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2018514
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eI_()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_1

    .line 2018515
    :cond_0
    :goto_0
    return-object v0

    .line 2018516
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eI_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2018517
    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2018518
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eH_()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_0

    .line 2018519
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eH_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2018520
    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
