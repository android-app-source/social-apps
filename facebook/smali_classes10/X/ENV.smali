.class public LX/ENV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ENW;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ENV",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ENW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112723
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2112724
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ENV;->b:LX/0Zi;

    .line 2112725
    iput-object p1, p0, LX/ENV;->a:LX/0Ot;

    .line 2112726
    return-void
.end method

.method public static a(LX/0QB;)LX/ENV;
    .locals 4

    .prologue
    .line 2112727
    const-class v1, LX/ENV;

    monitor-enter v1

    .line 2112728
    :try_start_0
    sget-object v0, LX/ENV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112729
    sput-object v2, LX/ENV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112730
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112731
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112732
    new-instance v3, LX/ENV;

    const/16 p0, 0x3442

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ENV;-><init>(LX/0Ot;)V

    .line 2112733
    move-object v0, v3

    .line 2112734
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112735
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112736
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2112738
    check-cast p2, LX/ENU;

    .line 2112739
    iget-object v0, p0, LX/ENV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ENW;

    iget v1, p2, LX/ENU;->c:I

    const/4 p2, 0x4

    const/4 p0, 0x2

    .line 2112740
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v4, 0x7f0a011a

    invoke-virtual {v3, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    const v5, 0x7f0b0061

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0033

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x7

    const v5, 0x7f0b0069

    invoke-interface {v3, v4, v5}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    iget-object v5, v0, LX/ENW;->c:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/2xv;->i(I)LX/2xv;

    move-result-object v5

    const p0, 0x7f0219c9

    invoke-virtual {v5, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f080fd9

    invoke-virtual {v4, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1ne;->m(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    sget-object v5, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v4, v5}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0063

    invoke-interface {v4, p2, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b0061

    invoke-interface {v2, v3, v4}, LX/1Dh;->z(II)LX/1Dh;

    move-result-object v2

    .line 2112741
    const v3, -0x1ad1e53d

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2112742
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    .line 2112743
    const v3, 0x12def010

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2112744
    invoke-interface {v2, v3}, LX/1Dh;->f(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2112745
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2112746
    invoke-static {}, LX/1dS;->b()V

    .line 2112747
    iget v1, p1, LX/1dQ;->b:I

    .line 2112748
    sparse-switch v1, :sswitch_data_0

    .line 2112749
    :goto_0
    return-object v0

    .line 2112750
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2112751
    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 2112752
    check-cast v2, LX/ENU;

    .line 2112753
    iget-object v3, p0, LX/ENV;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ENW;

    iget-object p1, v2, LX/ENU;->a:LX/CzL;

    iget-object p2, v2, LX/ENU;->b:LX/1Pg;

    invoke-virtual {v3, p1, p2}, LX/ENW;->a(LX/CzL;LX/1Pg;)V

    .line 2112754
    goto :goto_0

    .line 2112755
    :sswitch_1
    check-cast p2, LX/48J;

    .line 2112756
    iget-object v0, p2, LX/48J;->a:Landroid/view/View;

    iget-object v1, p2, LX/48J;->b:Landroid/view/MotionEvent;

    .line 2112757
    iget-object v3, p0, LX/ENV;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2112758
    invoke-virtual {v1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 2112759
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const/4 p0, -0x1

    invoke-direct {v3, p0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2112760
    :goto_1
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt p0, v2, :cond_0

    .line 2112761
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2112762
    :goto_2
    const/4 v3, 0x0

    move v3, v3

    .line 2112763
    move v0, v3

    .line 2112764
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2112765
    :pswitch_0
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v2, 0x7f0a011e

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-direct {v3, p0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_1

    .line 2112766
    :cond_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x1ad1e53d -> :sswitch_0
        0x12def010 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
