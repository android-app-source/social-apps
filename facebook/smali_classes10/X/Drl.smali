.class public LX/Drl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Landroid/app/NotificationManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2049638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049639
    return-void
.end method

.method public static b(LX/0QB;)LX/Drl;
    .locals 3

    .prologue
    .line 2049640
    new-instance v2, LX/Drl;

    invoke-direct {v2}, LX/Drl;-><init>()V

    .line 2049641
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 2049642
    iput-object v0, v2, LX/Drl;->a:Landroid/content/Context;

    iput-object v1, v2, LX/Drl;->b:Landroid/app/NotificationManager;

    .line 2049643
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2049644
    new-instance v0, LX/2HB;

    iget-object v1, p0, LX/Drl;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 2049645
    const/4 v1, 0x2

    .line 2049646
    iput v1, v0, LX/2HB;->j:I

    .line 2049647
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, LX/Drl;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0310b0

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 2049648
    const v2, 0x7f0d119e

    invoke-virtual {v1, v2, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 2049649
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 2049650
    const v2, 0x7f0d119e

    iget-object v3, p0, LX/Drl;->a:Landroid/content/Context;

    const v4, 0x7f0a010c

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 2049651
    :cond_0
    invoke-virtual {v0, p1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 2049652
    invoke-virtual {v0, p1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    .line 2049653
    const v2, 0x7f0208eb

    invoke-virtual {v0, v2}, LX/2HB;->a(I)LX/2HB;

    .line 2049654
    iget-object v2, v0, LX/2HB;->B:Landroid/app/Notification;

    iput-object v1, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 2049655
    iget-object v1, p0, LX/Drl;->b:Landroid/app/NotificationManager;

    const/4 v2, 0x0

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, p2, v2, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2049656
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/notifications/tray/PushFeedbackNotificationHandler$1;

    invoke-direct {v1, p0, p2}, Lcom/facebook/notifications/tray/PushFeedbackNotificationHandler$1;-><init>(LX/Drl;Ljava/lang/String;)V

    const-wide/16 v2, 0x5dc

    const v4, 0x2dd28751

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2049657
    return-void
.end method
