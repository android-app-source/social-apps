.class public final LX/EMJ;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/CxA;

.field public final synthetic d:LX/CzL;

.field public final synthetic e:LX/CzL;

.field public final synthetic f:LX/EMK;


# direct methods
.method public constructor <init>(LX/EMK;ZLjava/lang/String;LX/CxA;LX/CzL;LX/CzL;)V
    .locals 0

    .prologue
    .line 2110521
    iput-object p1, p0, LX/EMJ;->f:LX/EMK;

    iput-boolean p2, p0, LX/EMJ;->a:Z

    iput-object p3, p0, LX/EMJ;->b:Ljava/lang/String;

    iput-object p4, p0, LX/EMJ;->c:LX/CxA;

    iput-object p5, p0, LX/EMJ;->d:LX/CzL;

    iput-object p6, p0, LX/EMJ;->e:LX/CzL;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2110522
    iget-object v0, p0, LX/EMJ;->f:LX/EMK;

    iget-object v0, v0, LX/EMK;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2110523
    iget-object v0, p0, LX/EMJ;->c:LX/CxA;

    iget-object v1, p0, LX/EMJ;->d:LX/CzL;

    iget-object v2, p0, LX/EMJ;->e:LX/CzL;

    invoke-interface {v0, v1, v2}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2110524
    iget-object v0, p0, LX/EMJ;->c:LX/CxA;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2110525
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2110526
    iget-object v0, p0, LX/EMJ;->f:LX/EMK;

    iget-object v0, v0, LX/EMK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-boolean v2, p0, LX/EMJ;->a:Z

    iget-object v3, p0, LX/EMJ;->b:Ljava/lang/String;

    iget-object v1, p0, LX/EMJ;->c:LX/CxA;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    .line 2110527
    iget-object v4, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v4, v4

    .line 2110528
    iget-object v1, p0, LX/EMJ;->c:LX/CxA;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v4, v1}, LX/CvY;->a(ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2110529
    return-void
.end method
