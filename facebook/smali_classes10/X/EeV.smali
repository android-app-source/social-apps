.class public abstract LX/EeV;
.super Landroid/app/Service;
.source ""


# instance fields
.field private a:Z

.field public b:Z

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/content/Intent;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public volatile f:Landroid/os/Handler;

.field private final g:LX/EeL;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2152769
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 2152770
    iput-boolean v0, p0, LX/EeV;->a:Z

    .line 2152771
    iput-boolean v0, p0, LX/EeV;->b:Z

    .line 2152772
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EeV;->c:Ljava/util/List;

    .line 2152773
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/EeV;->d:Ljava/util/Set;

    .line 2152774
    const/4 v0, -0x1

    iput v0, p0, LX/EeV;->e:I

    .line 2152775
    new-instance v0, LX/Eep;

    invoke-direct {v0, p0}, LX/Eep;-><init>(LX/EeV;)V

    iput-object v0, p0, LX/EeV;->g:LX/EeL;

    return-void
.end method

.method public static a$redex0(LX/EeV;Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 2152761
    iget v0, p0, LX/EeV;->e:I

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/EeV;->e:I

    .line 2152762
    iget-object v0, p0, LX/EeV;->d:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2152763
    invoke-virtual {p0, p1}, LX/EeV;->a(Landroid/content/Intent;)Z

    move-result v0

    .line 2152764
    if-nez v0, :cond_0

    .line 2152765
    iget-object v0, p0, LX/EeV;->d:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2152766
    iget-boolean v0, p0, LX/EeV;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EeV;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2152767
    iget v0, p0, LX/EeV;->e:I

    invoke-virtual {p0, v0}, LX/EeV;->stopSelf(I)V

    .line 2152768
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a(LX/1wl;)V
.end method

.method public abstract a(Landroid/content/Intent;)Z
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2152760
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x174a01d5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2152749
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 2152750
    iget-object v1, p0, LX/EeV;->g:LX/EeL;

    invoke-static {v1}, LX/1wl;->b(LX/EeL;)V

    .line 2152751
    const/16 v1, 0x25

    const v2, 0x5a49f5e1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x62fb6431

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2152752
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 2152753
    iget-boolean v1, p0, LX/EeV;->b:Z

    if-eqz v1, :cond_0

    .line 2152754
    invoke-static {p0, p1, p3}, LX/EeV;->a$redex0(LX/EeV;Landroid/content/Intent;I)V

    .line 2152755
    :goto_0
    const v1, -0x5eddf8bd

    invoke-static {v1, v0}, LX/02F;->d(II)V

    return v4

    .line 2152756
    :cond_0
    iget-boolean v1, p0, LX/EeV;->a:Z

    if-nez v1, :cond_1

    .line 2152757
    iget-object v1, p0, LX/EeV;->g:LX/EeL;

    invoke-static {v1}, LX/1wl;->a(LX/EeL;)V

    .line 2152758
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/EeV;->a:Z

    .line 2152759
    :cond_1
    iget-object v1, p0, LX/EeV;->c:Ljava/util/List;

    new-instance v2, Landroid/util/Pair;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
