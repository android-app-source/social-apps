.class public LX/DqE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zd;
.implements LX/0Ze;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1rn;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/1rn;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/1rn;",
            "LX/0Or",
            "<",
            "LX/B9n;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048077
    iput-object p1, p0, LX/DqE;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2048078
    iput-object p2, p0, LX/DqE;->b:LX/0Or;

    .line 2048079
    iput-object p3, p0, LX/DqE;->c:LX/1rn;

    .line 2048080
    iput-object p4, p0, LX/DqE;->d:LX/0Or;

    .line 2048081
    return-void
.end method

.method public static b(LX/0QB;)LX/DqE;
    .locals 5

    .prologue
    .line 2048082
    new-instance v2, LX/DqE;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v1, 0x19e

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v1

    check-cast v1, LX/1rn;

    const/16 v4, 0x2680

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v2, v0, v3, v1, v4}, LX/DqE;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/1rn;LX/0Or;)V

    .line 2048083
    return-object v2
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2048084
    invoke-virtual {p0}, LX/DqE;->d()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/0PM;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2048085
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 2048086
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 2048087
    iget-object v0, p0, LX/DqE;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2048088
    if-eqz v0, :cond_0

    .line 2048089
    const-string v1, "Count of Cache IDs: "

    iget-object v3, p0, LX/DqE;->c:LX/1rn;

    .line 2048090
    iget-object v4, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2048091
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2048092
    iget-object v7, v3, LX/1rn;->e:LX/0Sh;

    invoke-virtual {v7}, LX/0Sh;->b()V

    .line 2048093
    iget-object v7, v3, LX/1rn;->c:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v7, v4, v5}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->c(J)LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    move v3, v7

    .line 2048094
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2048095
    :cond_0
    sget-object v1, LX/0hM;->d:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/DqE;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0hM;->d:LX/0Tn;

    invoke-interface {v3, v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2048096
    sget-object v1, LX/0hM;->e:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/DqE;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0hM;->e:LX/0Tn;

    invoke-interface {v3, v4, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2048097
    sget-object v1, LX/0hM;->b:LX/0Tn;

    invoke-virtual {v1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/DqE;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0hM;->b:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v3, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2048098
    iget-object v1, p0, LX/DqE;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0hM;->c:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2048099
    if-eqz v1, :cond_1

    .line 2048100
    sget-object v3, LX/0hM;->c:LX/0Tn;

    invoke-virtual {v3}, LX/0To;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2048101
    :cond_1
    if-eqz v0, :cond_3

    .line 2048102
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2048103
    iget-object v1, p0, LX/DqE;->c:LX/1rn;

    .line 2048104
    iget-object v4, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2048105
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, LX/1rn;->a(J)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 2048106
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2048107
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2048108
    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2048109
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2048110
    :cond_2
    const-string v0, "Cache IDs"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2048111
    :cond_3
    iget-object v0, p0, LX/DqE;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9n;

    invoke-virtual {v0}, LX/B9n;->getDebugInfo()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2048112
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
