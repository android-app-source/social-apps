.class public LX/EIw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/EIv;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/5zm;


# direct methods
.method public constructor <init>(LX/0SG;LX/5zm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2102549
    iput-object p1, p0, LX/EIw;->a:LX/0SG;

    .line 2102550
    iput-object p2, p0, LX/EIw;->b:LX/5zm;

    .line 2102551
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2102552
    check-cast p1, LX/EIv;

    .line 2102553
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    .line 2102554
    iget-object v1, p1, LX/EIv;->a:Landroid/net/Uri;

    move-object v1, v1

    .line 2102555
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2102556
    move-object v0, v0

    .line 2102557
    sget-object v1, LX/2MK;->AUDIO:LX/2MK;

    .line 2102558
    iput-object v1, v0, LX/5zn;->c:LX/2MK;

    .line 2102559
    move-object v0, v0

    .line 2102560
    sget-object v1, LX/5zj;->AUDIO:LX/5zj;

    .line 2102561
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 2102562
    move-object v0, v0

    .line 2102563
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2102564
    iget-object v1, p0, LX/EIw;->b:LX/5zm;

    invoke-virtual {v1, v0}, LX/5zm;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zl;

    move-result-object v0

    .line 2102565
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2102566
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2102567
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "extension"

    const-string v5, "mp4"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2102568
    iget-object v3, p0, LX/EIw;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2102569
    new-instance v4, LX/4cQ;

    invoke-direct {v4, v3, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2102570
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v3, "custom_voicemail_update"

    .line 2102571
    iput-object v3, v0, LX/14O;->b:Ljava/lang/String;

    .line 2102572
    move-object v0, v0

    .line 2102573
    const-string v3, "POST"

    .line 2102574
    iput-object v3, v0, LX/14O;->c:Ljava/lang/String;

    .line 2102575
    move-object v0, v0

    .line 2102576
    iget-object v3, p1, LX/EIv;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2102577
    iput-object v3, v0, LX/14O;->d:Ljava/lang/String;

    .line 2102578
    move-object v0, v0

    .line 2102579
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2102580
    iput-object v2, v0, LX/14O;->g:Ljava/util/List;

    .line 2102581
    move-object v0, v0

    .line 2102582
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 2102583
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 2102584
    move-object v0, v0

    .line 2102585
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 2102586
    move-object v0, v0

    .line 2102587
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2102588
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
