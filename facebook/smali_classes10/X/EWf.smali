.class public final LX/EWf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[B

.field public final b:I

.field public c:I

.field public final d:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>([BII)V
    .locals 1

    .prologue
    .line 2130817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2130818
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWf;->d:Ljava/io/OutputStream;

    .line 2130819
    iput-object p1, p0, LX/EWf;->a:[B

    .line 2130820
    iput p2, p0, LX/EWf;->c:I

    .line 2130821
    add-int v0, p2, p3

    iput v0, p0, LX/EWf;->b:I

    .line 2130822
    return-void
.end method

.method public static a(LX/EYu;)I
    .locals 2

    .prologue
    .line 2130754
    iget-boolean v0, p0, LX/EYu;->e:Z

    if-eqz v0, :cond_0

    .line 2130755
    iget-object v0, p0, LX/EYu;->d:LX/EWW;

    invoke-interface {v0}, LX/EWW;->b()I

    move-result v0

    .line 2130756
    :goto_0
    move v0, v0

    .line 2130757
    invoke-static {v0}, LX/EWf;->m(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, LX/EYu;->c:LX/EWc;

    invoke-virtual {v0}, LX/EWc;->b()I

    move-result v0

    goto :goto_0
.end method

.method public static a([B)LX/EWf;
    .locals 3

    .prologue
    .line 2130758
    const/4 v0, 0x0

    array-length v1, p0

    .line 2130759
    new-instance v2, LX/EWf;

    invoke-direct {v2, p0, v0, v1}, LX/EWf;-><init>([BII)V

    move-object v0, v2

    .line 2130760
    return-object v0
.end method

.method public static a(LX/EWf;LX/EWc;II)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2130761
    iget v0, p0, LX/EWf;->b:I

    iget v1, p0, LX/EWf;->c:I

    sub-int/2addr v0, v1

    if-lt v0, p3, :cond_1

    .line 2130762
    iget-object v0, p0, LX/EWf;->a:[B

    iget v1, p0, LX/EWf;->c:I

    invoke-virtual {p1, v0, p2, v1, p3}, LX/EWc;->a([BIII)V

    .line 2130763
    iget v0, p0, LX/EWf;->c:I

    add-int/2addr v0, p3

    iput v0, p0, LX/EWf;->c:I

    .line 2130764
    :cond_0
    :goto_0
    return-void

    .line 2130765
    :cond_1
    iget v0, p0, LX/EWf;->b:I

    iget v1, p0, LX/EWf;->c:I

    sub-int/2addr v0, v1

    .line 2130766
    iget-object v1, p0, LX/EWf;->a:[B

    iget v2, p0, LX/EWf;->c:I

    invoke-virtual {p1, v1, p2, v2, v0}, LX/EWc;->a([BIII)V

    .line 2130767
    add-int v1, p2, v0

    .line 2130768
    sub-int v0, p3, v0

    .line 2130769
    iget v2, p0, LX/EWf;->b:I

    iput v2, p0, LX/EWf;->c:I

    .line 2130770
    invoke-static {p0}, LX/EWf;->i(LX/EWf;)V

    .line 2130771
    iget v2, p0, LX/EWf;->b:I

    if-gt v0, v2, :cond_2

    .line 2130772
    iget-object v2, p0, LX/EWf;->a:[B

    invoke-virtual {p1, v2, v1, v8, v0}, LX/EWc;->a([BIII)V

    .line 2130773
    iput v0, p0, LX/EWf;->c:I

    goto :goto_0

    .line 2130774
    :cond_2
    invoke-virtual {p1}, LX/EWc;->g()Ljava/io/InputStream;

    move-result-object v2

    .line 2130775
    int-to-long v4, v1

    int-to-long v6, v1

    invoke-virtual {v2, v6, v7}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_4

    .line 2130776
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Skip failed? Should never happen."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2130777
    :cond_3
    iget-object v1, p0, LX/EWf;->d:Ljava/io/OutputStream;

    iget-object v4, p0, LX/EWf;->a:[B

    invoke-virtual {v1, v4, v8, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 2130778
    sub-int/2addr v0, v3

    .line 2130779
    :cond_4
    if-lez v0, :cond_0

    .line 2130780
    iget v1, p0, LX/EWf;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2130781
    iget-object v3, p0, LX/EWf;->a:[B

    invoke-virtual {v2, v3, v8, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 2130782
    if-eq v3, v1, :cond_3

    .line 2130783
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Read failed? Should never happen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(IZ)I
    .locals 2

    .prologue
    .line 2130784
    invoke-static {p0}, LX/EWf;->k(I)I

    move-result v0

    .line 2130785
    const/4 v1, 0x1

    move v1, v1

    .line 2130786
    add-int/2addr v0, v1

    return v0
.end method

.method public static b(LX/EWc;)I
    .locals 2

    .prologue
    .line 2130787
    invoke-virtual {p0}, LX/EWc;->b()I

    move-result v0

    invoke-static {v0}, LX/EWf;->m(I)I

    move-result v0

    invoke-virtual {p0}, LX/EWc;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2130788
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 2130789
    array-length v1, v0

    invoke-static {v1}, LX/EWf;->m(I)I

    move-result v1

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v1

    return v0

    .line 2130790
    :catch_0
    move-exception v0

    .line 2130791
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "UTF-8 not supported."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static c(ILX/EWc;)I
    .locals 2

    .prologue
    .line 2130792
    invoke-static {p0}, LX/EWf;->k(I)I

    move-result v0

    invoke-static {p1}, LX/EWf;->b(LX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static c(LX/EWW;)I
    .locals 1

    .prologue
    .line 2130793
    invoke-interface {p0}, LX/EWW;->b()I

    move-result v0

    return v0
.end method

.method public static d(IJ)I
    .locals 3

    .prologue
    .line 2130794
    invoke-static {p0}, LX/EWf;->k(I)I

    move-result v0

    invoke-static {p1, p2}, LX/EWf;->j(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static d(LX/EWW;)I
    .locals 2

    .prologue
    .line 2130795
    invoke-interface {p0}, LX/EWW;->b()I

    move-result v0

    .line 2130796
    invoke-static {v0}, LX/EWf;->m(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static e(II)I
    .locals 2

    .prologue
    .line 2130797
    invoke-static {p0}, LX/EWf;->k(I)I

    move-result v0

    invoke-static {p1}, LX/EWf;->g(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static e(ILX/EWW;)I
    .locals 2

    .prologue
    .line 2130837
    invoke-static {p0}, LX/EWf;->k(I)I

    move-result v0

    invoke-static {p1}, LX/EWf;->d(LX/EWW;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static f(IJ)I
    .locals 2

    .prologue
    .line 2130798
    invoke-static {p0}, LX/EWf;->k(I)I

    move-result v0

    .line 2130799
    const/16 v1, 0x8

    move v1, v1

    .line 2130800
    add-int/2addr v0, v1

    return v0
.end method

.method public static f(ILX/EWW;)I
    .locals 2

    .prologue
    .line 2130801
    const/4 v0, 0x1

    invoke-static {v0}, LX/EWf;->k(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    invoke-static {v1, p0}, LX/EWf;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    invoke-static {v1, p1}, LX/EWf;->e(ILX/EWW;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static g(I)I
    .locals 1

    .prologue
    .line 2130802
    if-ltz p0, :cond_0

    .line 2130803
    invoke-static {p0}, LX/EWf;->m(I)I

    move-result v0

    .line 2130804
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static g(II)I
    .locals 2

    .prologue
    .line 2130805
    invoke-static {p0}, LX/EWf;->k(I)I

    move-result v0

    invoke-static {p1}, LX/EWf;->m(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static h(II)I
    .locals 2

    .prologue
    .line 2130806
    invoke-static {p0}, LX/EWf;->k(I)I

    move-result v0

    invoke-static {p1}, LX/EWf;->g(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static i(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2130807
    iget-object v0, p0, LX/EWf;->d:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    .line 2130808
    new-instance v0, LX/EWe;

    invoke-direct {v0}, LX/EWe;-><init>()V

    throw v0

    .line 2130809
    :cond_0
    iget-object v0, p0, LX/EWf;->d:Ljava/io/OutputStream;

    iget-object v1, p0, LX/EWf;->a:[B

    iget v2, p0, LX/EWf;->c:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 2130810
    iput v3, p0, LX/EWf;->c:I

    .line 2130811
    return-void
.end method

.method public static i(LX/EWf;J)V
    .locals 5

    .prologue
    .line 2130812
    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2130813
    long-to-int v0, p1

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130814
    return-void

    .line 2130815
    :cond_0
    long-to-int v0, p1

    and-int/lit8 v0, v0, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130816
    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0
.end method

.method private j()I
    .locals 2

    .prologue
    .line 2130823
    iget-object v0, p0, LX/EWf;->d:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    .line 2130824
    iget v0, p0, LX/EWf;->b:I

    iget v1, p0, LX/EWf;->c:I

    sub-int/2addr v0, v1

    return v0

    .line 2130825
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static j(J)I
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2130826
    const-wide/16 v0, -0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2130827
    :goto_0
    return v0

    .line 2130828
    :cond_0
    const-wide/16 v0, -0x4000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 2130829
    :cond_1
    const-wide/32 v0, -0x200000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    .line 2130830
    :cond_2
    const-wide/32 v0, -0x10000000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    .line 2130831
    :cond_3
    const-wide v0, -0x800000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const/4 v0, 0x5

    goto :goto_0

    .line 2130832
    :cond_4
    const-wide v0, -0x40000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    const/4 v0, 0x6

    goto :goto_0

    .line 2130833
    :cond_5
    const-wide/high16 v0, -0x2000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    const/4 v0, 0x7

    goto :goto_0

    .line 2130834
    :cond_6
    const-wide/high16 v0, -0x100000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    const/16 v0, 0x8

    goto :goto_0

    .line 2130835
    :cond_7
    const-wide/high16 v0, -0x8000000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    const/16 v0, 0x9

    goto :goto_0

    .line 2130836
    :cond_8
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static k(I)I
    .locals 1

    .prologue
    .line 2130744
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EZb;->a(II)I

    move-result v0

    invoke-static {v0}, LX/EWf;->m(I)I

    move-result v0

    return v0
.end method

.method public static k(LX/EWf;J)V
    .locals 3

    .prologue
    .line 2130745
    long-to-int v0, p1

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130746
    const/16 v0, 0x8

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130747
    const/16 v0, 0x10

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130748
    const/16 v0, 0x18

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130749
    const/16 v0, 0x20

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130750
    const/16 v0, 0x28

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130751
    const/16 v0, 0x30

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130752
    const/16 v0, 0x38

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130753
    return-void
.end method

.method public static l(J)J
    .locals 4

    .prologue
    .line 2130704
    const/4 v0, 0x1

    shl-long v0, p0, v0

    const/16 v2, 0x3f

    shr-long v2, p0, v2

    xor-long/2addr v0, v2

    return-wide v0
.end method

.method public static m(I)I
    .locals 1

    .prologue
    .line 2130698
    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2130699
    :goto_0
    return v0

    .line 2130700
    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 2130701
    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    .line 2130702
    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    .line 2130703
    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method

.method private n(I)V
    .locals 3

    .prologue
    .line 2130693
    int-to-byte v0, p1

    .line 2130694
    iget v1, p0, LX/EWf;->c:I

    iget v2, p0, LX/EWf;->b:I

    if-ne v1, v2, :cond_0

    .line 2130695
    invoke-static {p0}, LX/EWf;->i(LX/EWf;)V

    .line 2130696
    :cond_0
    iget-object v1, p0, LX/EWf;->a:[B

    iget v2, p0, LX/EWf;->c:I

    add-int/lit8 p1, v2, 0x1

    iput p1, p0, LX/EWf;->c:I

    aput-byte v0, v1, v2

    .line 2130697
    return-void
.end method

.method public static o(LX/EWf;I)V
    .locals 1

    .prologue
    .line 2130688
    and-int/lit16 v0, p1, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130689
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130690
    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130691
    shr-int/lit8 v0, p1, 0x18

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130692
    return-void
.end method

.method public static p(I)I
    .locals 2

    .prologue
    .line 2130743
    shl-int/lit8 v0, p0, 0x1

    shr-int/lit8 v1, p0, 0x1f

    xor-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(D)V
    .locals 3

    .prologue
    .line 2130686
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-static {p0, v0, v1}, LX/EWf;->k(LX/EWf;J)V

    .line 2130687
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2130682
    if-ltz p1, :cond_0

    .line 2130683
    invoke-virtual {p0, p1}, LX/EWf;->l(I)V

    .line 2130684
    :goto_0
    return-void

    .line 2130685
    :cond_0
    int-to-long v0, p1

    invoke-static {p0, v0, v1}, LX/EWf;->i(LX/EWf;J)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 2130679
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130680
    invoke-virtual {p0, p2}, LX/EWf;->a(I)V

    .line 2130681
    return-void
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 2130676
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130677
    invoke-static {p0, p2, p3}, LX/EWf;->i(LX/EWf;J)V

    .line 2130678
    return-void
.end method

.method public final a(ILX/EWW;)V
    .locals 1

    .prologue
    .line 2130672
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130673
    invoke-interface {p2, p0}, LX/EWW;->a(LX/EWf;)V

    .line 2130674
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130675
    return-void
.end method

.method public final a(ILX/EWc;)V
    .locals 1

    .prologue
    .line 2130669
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130670
    invoke-virtual {p0, p2}, LX/EWf;->a(LX/EWc;)V

    .line 2130671
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 2130666
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130667
    invoke-virtual {p0, p2}, LX/EWf;->a(Z)V

    .line 2130668
    return-void
.end method

.method public final a(LX/EWc;)V
    .locals 2

    .prologue
    .line 2130663
    invoke-virtual {p1}, LX/EWc;->b()I

    move-result v0

    invoke-virtual {p0, v0}, LX/EWf;->l(I)V

    .line 2130664
    const/4 v0, 0x0

    invoke-virtual {p1}, LX/EWc;->b()I

    move-result v1

    invoke-static {p0, p1, v0, v1}, LX/EWf;->a(LX/EWf;LX/EWc;II)V

    .line 2130665
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2130705
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130706
    return-void

    .line 2130707
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(ILX/EWW;)V
    .locals 1

    .prologue
    .line 2130708
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130709
    invoke-virtual {p0, p2}, LX/EWf;->b(LX/EWW;)V

    .line 2130710
    return-void
.end method

.method public final b(ILX/EWc;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 2130711
    invoke-virtual {p0, v1, v2}, LX/EWf;->i(II)V

    .line 2130712
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, LX/EWf;->c(II)V

    .line 2130713
    invoke-virtual {p0, v2, p2}, LX/EWf;->a(ILX/EWc;)V

    .line 2130714
    const/4 v0, 0x4

    invoke-virtual {p0, v1, v0}, LX/EWf;->i(II)V

    .line 2130715
    return-void
.end method

.method public final b(LX/EWW;)V
    .locals 1

    .prologue
    .line 2130716
    invoke-interface {p1}, LX/EWW;->b()I

    move-result v0

    invoke-virtual {p0, v0}, LX/EWf;->l(I)V

    .line 2130717
    invoke-interface {p1, p0}, LX/EWW;->a(LX/EWf;)V

    .line 2130718
    return-void
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 2130719
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130720
    invoke-virtual {p0, p2}, LX/EWf;->l(I)V

    .line 2130721
    return-void
.end method

.method public final c(IJ)V
    .locals 2

    .prologue
    .line 2130722
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130723
    invoke-static {p0, p2, p3}, LX/EWf;->k(LX/EWf;J)V

    .line 2130724
    return-void
.end method

.method public final c(ILX/EWW;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 2130725
    invoke-virtual {p0, v1, v2}, LX/EWf;->i(II)V

    .line 2130726
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, LX/EWf;->c(II)V

    .line 2130727
    invoke-virtual {p0, v2, p2}, LX/EWf;->b(ILX/EWW;)V

    .line 2130728
    const/4 v0, 0x4

    invoke-virtual {p0, v1, v0}, LX/EWf;->i(II)V

    .line 2130729
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 2130730
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/EWf;->i(II)V

    .line 2130731
    invoke-virtual {p0, p2}, LX/EWf;->a(I)V

    .line 2130732
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2130733
    invoke-direct {p0}, LX/EWf;->j()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2130734
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2130735
    :cond_0
    return-void
.end method

.method public final i(II)V
    .locals 1

    .prologue
    .line 2130736
    invoke-static {p1, p2}, LX/EZb;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/EWf;->l(I)V

    .line 2130737
    return-void
.end method

.method public final l(I)V
    .locals 1

    .prologue
    .line 2130738
    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    .line 2130739
    invoke-direct {p0, p1}, LX/EWf;->n(I)V

    .line 2130740
    return-void

    .line 2130741
    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, LX/EWf;->n(I)V

    .line 2130742
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method
