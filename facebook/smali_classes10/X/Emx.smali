.class public LX/Emx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Emj;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/Emu;

.field private static volatile h:LX/Emx;


# instance fields
.field private final b:LX/11i;

.field private final c:LX/0So;

.field public d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Z

.field public final g:LX/026;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "LX/Emw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2166442
    new-instance v0, LX/Emu;

    invoke-direct {v0}, LX/Emu;-><init>()V

    sput-object v0, LX/Emx;->a:LX/Emu;

    return-void
.end method

.method public constructor <init>(LX/11i;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2166443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166444
    iput-object p1, p0, LX/Emx;->b:LX/11i;

    .line 2166445
    iput-object p2, p0, LX/Emx;->c:LX/0So;

    .line 2166446
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/Emx;->g:LX/026;

    .line 2166447
    return-void
.end method

.method public static a(LX/0QB;)LX/Emx;
    .locals 5

    .prologue
    .line 2166448
    sget-object v0, LX/Emx;->h:LX/Emx;

    if-nez v0, :cond_1

    .line 2166449
    const-class v1, LX/Emx;

    monitor-enter v1

    .line 2166450
    :try_start_0
    sget-object v0, LX/Emx;->h:LX/Emx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2166451
    if-eqz v2, :cond_0

    .line 2166452
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2166453
    new-instance p0, LX/Emx;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/Emx;-><init>(LX/11i;LX/0So;)V

    .line 2166454
    move-object v0, p0

    .line 2166455
    sput-object v0, LX/Emx;->h:LX/Emx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2166456
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2166457
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2166458
    :cond_1
    sget-object v0, LX/Emx;->h:LX/Emx;

    return-object v0

    .line 2166459
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2166460
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2166461
    invoke-direct {p0}, LX/Emx;->g()Ljava/lang/String;

    move-result-object v0

    .line 2166462
    if-nez v0, :cond_0

    .line 2166463
    :goto_0
    return-void

    .line 2166464
    :cond_0
    iget-object v1, p0, LX/Emx;->b:LX/11i;

    sget-object v2, LX/Emx;->a:LX/Emu;

    invoke-interface {v1, v2, v0}, LX/11i;->a(LX/0Pq;Ljava/lang/String;)V

    .line 2166465
    iput-object v3, p0, LX/Emx;->d:LX/0P1;

    .line 2166466
    iput-object v3, p0, LX/Emx;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2166467
    const/4 v0, 0x0

    .line 2166468
    invoke-static {p0}, LX/Emx;->h(LX/Emx;)LX/11o;

    move-result-object v1

    .line 2166469
    if-nez v1, :cond_2

    .line 2166470
    :cond_0
    :goto_0
    move v0, v0

    .line 2166471
    if-nez v0, :cond_1

    .line 2166472
    :goto_1
    return-void

    .line 2166473
    :cond_1
    invoke-direct {p0}, LX/Emx;->g()Ljava/lang/String;

    move-result-object v2

    .line 2166474
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166475
    sget-object v0, LX/Emw;->LAUNCH_ENTITY_CARD:LX/Emw;

    invoke-virtual {p0, v0}, LX/Emx;->b(LX/Emw;)V

    .line 2166476
    iget-object v0, p0, LX/Emx;->b:LX/11i;

    sget-object v1, LX/Emx;->a:LX/Emu;

    iget-object v3, p0, LX/Emx;->d:LX/0P1;

    iget-object v4, p0, LX/Emx;->c:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    .line 2166477
    iput-object v6, p0, LX/Emx;->d:LX/0P1;

    .line 2166478
    iput-object v6, p0, LX/Emx;->e:Ljava/lang/String;

    goto :goto_1

    .line 2166479
    :cond_2
    iget-object v2, p0, LX/Emx;->g:LX/026;

    invoke-virtual {v2}, LX/01J;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2166480
    sget-object v2, LX/Emw;->INITIAL_ENTITIES_FETCHED:LX/Emw;

    iget-object v2, v2, LX/Emw;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2166481
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2166482
    iget-object v0, p0, LX/Emx;->d:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Emx;->d:LX/0P1;

    const-string v1, "instance_id"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/Emx;)LX/11o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<",
            "LX/Emu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2166483
    invoke-direct {p0}, LX/Emx;->g()Ljava/lang/String;

    move-result-object v0

    .line 2166484
    if-nez v0, :cond_0

    .line 2166485
    const/4 v0, 0x0

    .line 2166486
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/Emx;->b:LX/11i;

    sget-object v2, LX/Emx;->a:LX/Emu;

    invoke-interface {v1, v2, v0}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2166487
    return-void
.end method

.method public final a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Emo;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2166488
    return-void
.end method

.method public final a(LX/Emv;)V
    .locals 3

    .prologue
    .line 2166378
    invoke-static {p0}, LX/Emx;->h(LX/Emx;)LX/11o;

    move-result-object v0

    .line 2166379
    if-eqz v0, :cond_0

    .line 2166380
    iget-object v1, p1, LX/Emv;->name:Ljava/lang/String;

    const v2, 0x3f8bf89

    invoke-static {v0, v1, v2}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2166381
    sget-object v0, LX/Emv;->FINAL_DATA_AVAILABLE:LX/Emv;

    invoke-virtual {p1, v0}, LX/Emv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2166382
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Emx;->f:Z

    .line 2166383
    :cond_0
    return-void
.end method

.method public final a(LX/Emw;)V
    .locals 3

    .prologue
    .line 2166438
    invoke-static {p0}, LX/Emx;->h(LX/Emx;)LX/11o;

    move-result-object v0

    .line 2166439
    if-eqz v0, :cond_0

    .line 2166440
    iget-object v1, p1, LX/Emw;->name:Ljava/lang/String;

    const v2, -0x174763cf

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2166441
    :cond_0
    return-void
.end method

.method public final a(LX/EnB;)V
    .locals 1

    .prologue
    .line 2166433
    sget-object v0, LX/EnB;->PREVIEW:LX/EnB;

    if-ne p1, v0, :cond_1

    .line 2166434
    sget-object v0, LX/Emv;->PREVIEW_DATA_AVAILABLE:LX/Emv;

    invoke-virtual {p0, v0}, LX/Emx;->a(LX/Emv;)V

    .line 2166435
    :cond_0
    :goto_0
    return-void

    .line 2166436
    :cond_1
    sget-object v0, LX/EnB;->FINAL:LX/EnB;

    if-ne p1, v0, :cond_0

    .line 2166437
    sget-object v0, LX/Emv;->FINAL_DATA_AVAILABLE:LX/Emv;

    invoke-virtual {p0, v0}, LX/Emx;->a(LX/Emv;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2166432
    return-void
.end method

.method public final a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2166413
    invoke-static {p0}, LX/Emx;->h(LX/Emx;)LX/11o;

    move-result-object v0

    .line 2166414
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Emx;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Emx;->e:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2166415
    :cond_0
    :goto_0
    return-void

    .line 2166416
    :cond_1
    sget-object v0, LX/EnC;->FAILED:LX/EnC;

    if-ne p2, v0, :cond_2

    .line 2166417
    invoke-direct {p0}, LX/Emx;->d()V

    goto :goto_0

    .line 2166418
    :cond_2
    const-string v0, "ec_config_begin"

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2166419
    iget-object v0, p0, LX/Emx;->g:LX/026;

    invoke-virtual {v0}, LX/026;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emw;

    .line 2166420
    invoke-virtual {p0, v0}, LX/Emx;->a(LX/Emw;)V

    goto :goto_1

    .line 2166421
    :cond_3
    const-string v0, "ec_card_recycled"

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2166422
    iget-object v0, p0, LX/Emx;->g:LX/026;

    invoke-virtual {v0}, LX/026;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emw;

    .line 2166423
    invoke-virtual {p0, v0}, LX/Emx;->c(LX/Emw;)V

    goto :goto_2

    .line 2166424
    :cond_4
    iget-object v0, p0, LX/Emx;->g:LX/026;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 2166425
    invoke-direct {p0}, LX/Emx;->e()V

    goto :goto_0

    .line 2166426
    :cond_5
    iget-boolean v0, p0, LX/Emx;->f:Z

    if-eqz v0, :cond_0

    .line 2166427
    iget-object v0, p0, LX/Emx;->g:LX/026;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emw;

    .line 2166428
    if-eqz v0, :cond_0

    .line 2166429
    invoke-virtual {p0, v0}, LX/Emx;->b(LX/Emw;)V

    .line 2166430
    iget-object v0, p0, LX/Emx;->g:LX/026;

    invoke-virtual {v0, p1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166431
    invoke-direct {p0}, LX/Emx;->e()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;D)V
    .locals 0

    .prologue
    .line 2166412
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2166399
    invoke-direct {p0}, LX/Emx;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2166400
    invoke-direct {p0}, LX/Emx;->d()V

    .line 2166401
    :cond_0
    invoke-static {p1, p2, p3}, LX/Emq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/Emx;->d:LX/0P1;

    .line 2166402
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Emx;->f:Z

    .line 2166403
    iget-object v0, p0, LX/Emx;->g:LX/026;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 2166404
    iget-object v0, p0, LX/Emx;->g:LX/026;

    const-string v1, "ec_config_cover_photo"

    sget-object v2, LX/Emw;->INITIAL_CARD_COVER_PHOTO:LX/Emw;

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166405
    iget-object v0, p0, LX/Emx;->g:LX/026;

    const-string v1, "ec_config_profile_picture"

    sget-object v2, LX/Emw;->INITIAL_CARD_PROFILE_PICTURE:LX/Emw;

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166406
    iget-object v0, p0, LX/Emx;->g:LX/026;

    const-string v1, "ec_config_action_bar"

    sget-object v2, LX/Emw;->INITIAL_CARD_ACTION_BAR:LX/Emw;

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166407
    iget-object v0, p0, LX/Emx;->g:LX/026;

    const-string v1, "ec_config_context_rows"

    sget-object v2, LX/Emw;->INITIAL_CARD_CONTEXT_ROWS:LX/Emw;

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166408
    if-eqz p4, :cond_1

    .line 2166409
    invoke-virtual {p0, p4}, LX/Emx;->b(Ljava/lang/String;)V

    .line 2166410
    :cond_1
    iget-object v0, p0, LX/Emx;->b:LX/11i;

    sget-object v1, LX/Emx;->a:LX/Emu;

    iget-object v3, p0, LX/Emx;->d:LX/0P1;

    iget-object v2, p0, LX/Emx;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    .line 2166411
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2166398
    return-void
.end method

.method public final b(LX/Emw;)V
    .locals 3

    .prologue
    .line 2166392
    invoke-static {p0}, LX/Emx;->h(LX/Emx;)LX/11o;

    move-result-object v0

    .line 2166393
    if-eqz v0, :cond_0

    .line 2166394
    iget-object v1, p1, LX/Emw;->name:Ljava/lang/String;

    const v2, -0x5c7c9e9f

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2166395
    sget-object v0, LX/Emw;->INITIAL_ENTITIES_FETCHED:LX/Emw;

    if-ne p1, v0, :cond_0

    .line 2166396
    invoke-direct {p0}, LX/Emx;->e()V

    .line 2166397
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2166390
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Emx;->e:Ljava/lang/String;

    .line 2166391
    return-void
.end method

.method public final c(LX/Emw;)V
    .locals 3

    .prologue
    .line 2166384
    invoke-static {p0}, LX/Emx;->h(LX/Emx;)LX/11o;

    move-result-object v0

    .line 2166385
    if-eqz v0, :cond_0

    .line 2166386
    iget-object v1, p1, LX/Emw;->name:Ljava/lang/String;

    const v2, -0x4018ba9f

    invoke-static {v0, v1, v2}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2166387
    sget-object v0, LX/Emw;->INITIAL_ENTITIES_FETCHED:LX/Emw;

    if-ne p1, v0, :cond_0

    .line 2166388
    invoke-direct {p0}, LX/Emx;->d()V

    .line 2166389
    :cond_0
    return-void
.end method
