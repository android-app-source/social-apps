.class public LX/El3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/El3;


# instance fields
.field public final a:Lcom/facebook/http/common/FbHttpRequestProcessor;


# direct methods
.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2163987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163988
    iput-object p1, p0, LX/El3;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 2163989
    return-void
.end method

.method public static a(LX/0QB;)LX/El3;
    .locals 4

    .prologue
    .line 2163990
    sget-object v0, LX/El3;->b:LX/El3;

    if-nez v0, :cond_1

    .line 2163991
    const-class v1, LX/El3;

    monitor-enter v1

    .line 2163992
    :try_start_0
    sget-object v0, LX/El3;->b:LX/El3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2163993
    if-eqz v2, :cond_0

    .line 2163994
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2163995
    new-instance p0, LX/El3;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v3

    check-cast v3, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {p0, v3}, LX/El3;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    .line 2163996
    move-object v0, p0

    .line 2163997
    sput-object v0, LX/El3;->b:LX/El3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2163998
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2163999
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2164000
    :cond_1
    sget-object v0, LX/El3;->b:LX/El3;

    return-object v0

    .line 2164001
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2164002
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
