.class public LX/EDC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field public a:LX/2gU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2091092
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/EDC;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2091026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2091027
    return-void
.end method

.method public static a(LX/0QB;)LX/EDC;
    .locals 9

    .prologue
    .line 2091028
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2091029
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2091030
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2091031
    if-nez v1, :cond_0

    .line 2091032
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2091033
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2091034
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2091035
    sget-object v1, LX/EDC;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2091036
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2091037
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2091038
    :cond_1
    if-nez v1, :cond_4

    .line 2091039
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2091040
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2091041
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2091042
    new-instance p0, LX/EDC;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    invoke-direct {p0}, LX/EDC;-><init>()V

    .line 2091043
    invoke-static {v0}, LX/2gU;->a(LX/0QB;)LX/2gU;

    move-result-object v1

    check-cast v1, LX/2gU;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    .line 2091044
    iput-object v1, p0, LX/EDC;->a:LX/2gU;

    iput-object v7, p0, LX/EDC;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v8, p0, LX/EDC;->c:LX/0SG;

    .line 2091045
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2091046
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2091047
    if-nez v1, :cond_2

    .line 2091048
    sget-object v0, LX/EDC;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDC;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2091049
    :goto_1
    if-eqz v0, :cond_3

    .line 2091050
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2091051
    :goto_3
    check-cast v0, LX/EDC;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2091052
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2091053
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2091054
    :catchall_1
    move-exception v0

    .line 2091055
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2091056
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2091057
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2091058
    :cond_2
    :try_start_8
    sget-object v0, LX/EDC;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDC;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/0m9;)V
    .locals 3

    .prologue
    .line 2091059
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2091060
    const-string v1, "payload"

    invoke-virtual {p1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2091061
    iget-object v1, p0, LX/EDC;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/rtc/fbwebrtc/WebrtcAdminMessageSender$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcAdminMessageSender$1;-><init>(LX/EDC;LX/0m9;)V

    const v0, -0x4c0ab5da

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2091062
    return-void
.end method


# virtual methods
.method public final a(JZZJJJZ)V
    .locals 7

    .prologue
    .line 2091063
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 2091064
    const-string v3, "event_name"

    const-string v4, "call_record"

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2091065
    const-string v3, "%d:%d"

    iget-object v4, p0, LX/EDC;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2091066
    const-string v4, "msg_id"

    invoke-virtual {v2, v4, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2091067
    const-string v3, "call_id"

    invoke-virtual {v2, v3, p1, p2}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2091068
    const-string v3, "to"

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2091069
    const-string v3, "call_start_time"

    invoke-virtual {v2, v3, p7, p8}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2091070
    const-string v3, "call_duration"

    move-wide/from16 v0, p9

    invoke-virtual {v2, v3, v0, v1}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2091071
    if-eqz p3, :cond_0

    .line 2091072
    const-string v3, "call_type"

    sget-object v4, LX/ED9;->OUTGOING:LX/ED9;

    invoke-virtual {v4}, LX/ED9;->getValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2091073
    :goto_0
    if-eqz p4, :cond_1

    .line 2091074
    const-string v3, "event_type"

    sget-object v4, LX/EDA;->VIDEO:LX/EDA;

    invoke-virtual {v4}, LX/EDA;->getValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2091075
    :goto_1
    const-string v3, "call_acknowledged"

    move/from16 v0, p11

    invoke-virtual {v2, v3, v0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2091076
    invoke-direct {p0, v2}, LX/EDC;->a(LX/0m9;)V

    .line 2091077
    return-void

    .line 2091078
    :cond_0
    const-string v3, "call_type"

    sget-object v4, LX/ED9;->MISSED:LX/ED9;

    invoke-virtual {v4}, LX/ED9;->getValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    goto :goto_0

    .line 2091079
    :cond_1
    const-string v3, "event_type"

    sget-object v4, LX/EDA;->VOICE:LX/EDA;

    invoke-virtual {v4}, LX/EDA;->getValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    goto :goto_1
.end method

.method public final a(JZLX/EDB;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2091080
    invoke-virtual {p4}, LX/EDB;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2091081
    if-nez v2, :cond_0

    .line 2091082
    :goto_0
    return v0

    .line 2091083
    :cond_0
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2091084
    const-string v4, "event_name"

    const-string v5, "instant_video_lifecycle"

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2091085
    const-string v4, "to"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2091086
    const-string v4, "lifecycle_event"

    invoke-virtual {v3, v4, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2091087
    const-string v2, "is_caller"

    invoke-virtual {v3, v2, p3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2091088
    invoke-virtual {p4}, LX/EDB;->getValue()I

    move-result v2

    sget-object v4, LX/EDB;->INSTANT_VIDEO_ENDED:LX/EDB;

    invoke-virtual {v4}, LX/EDB;->getValue()I

    move-result v4

    if-eq v2, v4, :cond_1

    invoke-virtual {p4}, LX/EDB;->getValue()I

    move-result v2

    sget-object v4, LX/EDB;->INSTANT_VIDEO_ENDED_WITH_ERROR:LX/EDB;

    invoke-virtual {v4}, LX/EDB;->getValue()I

    move-result v4

    if-ne v2, v4, :cond_3

    .line 2091089
    :cond_1
    const-string v2, "call_failed"

    invoke-virtual {p4}, LX/EDB;->getValue()I

    move-result v4

    sget-object v5, LX/EDB;->INSTANT_VIDEO_ENDED_WITH_ERROR:LX/EDB;

    invoke-virtual {v5}, LX/EDB;->getValue()I

    move-result v5

    if-ne v4, v5, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {v3, v2, v0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2091090
    :cond_3
    invoke-direct {p0, v3}, LX/EDC;->a(LX/0m9;)V

    move v0, v1

    .line 2091091
    goto :goto_0
.end method
