.class public final LX/Enz;
.super LX/Enl;
.source ""


# instance fields
.field private final A:Ljava/util/concurrent/Executor;

.field public final B:LX/Emx;

.field public final C:LX/03V;

.field public final a:Lcom/facebook/entitycards/model/ScrollLoadTrigger;

.field public final b:Lcom/facebook/entitycards/model/ScrollLoadTrigger;

.field public final c:Lcom/facebook/entitycards/model/ScrollLoadError;

.field public final d:Lcom/facebook/entitycards/model/ScrollLoadError;

.field public final e:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final f:LX/En2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/En2",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/Enn;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/Enn;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/Enn;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z

.field public p:Z

.field public final q:LX/1My;

.field public final r:LX/Enh;

.field public final s:LX/Eoc;

.field public final t:LX/Emy;

.field public final u:LX/Emq;

.field public final v:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation
.end field

.field private final w:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final y:I

.field private final z:I


# direct methods
.method public constructor <init>(LX/1My;LX/Enh;LX/Eoc;LX/Emq;LX/Emy;Ljava/lang/String;LX/En3;LX/0P1;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/concurrent/Executor;LX/Emx;LX/03V;)V
    .locals 4
    .param p1    # LX/1My;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Enh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Eoc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Emq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/Emy;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/En3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/0P1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1My;",
            "LX/Enh;",
            "Lcom/facebook/entitycards/service/EntityCardsEntityLoader;",
            "LX/Emq;",
            "LX/Emy;",
            "Ljava/lang/String;",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/concurrent/Executor;",
            "LX/Emx;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2167869
    invoke-direct {p0, p6}, LX/Enl;-><init>(Ljava/lang/String;)V

    .line 2167870
    new-instance v1, Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    sget-object v2, LX/Enq;->LEFT:LX/Enq;

    invoke-direct {v1, v2}, Lcom/facebook/entitycards/model/ScrollLoadTrigger;-><init>(LX/Enq;)V

    iput-object v1, p0, LX/Enz;->a:Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    .line 2167871
    new-instance v1, Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    sget-object v2, LX/Enq;->RIGHT:LX/Enq;

    invoke-direct {v1, v2}, Lcom/facebook/entitycards/model/ScrollLoadTrigger;-><init>(LX/Enq;)V

    iput-object v1, p0, LX/Enz;->b:Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    .line 2167872
    new-instance v1, Lcom/facebook/entitycards/model/ScrollLoadError;

    const-string v2, "left_edge"

    invoke-direct {v1, v2}, Lcom/facebook/entitycards/model/ScrollLoadError;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, LX/Enz;->c:Lcom/facebook/entitycards/model/ScrollLoadError;

    .line 2167873
    new-instance v1, Lcom/facebook/entitycards/model/ScrollLoadError;

    const-string v2, "right_edge"

    invoke-direct {v1, v2}, Lcom/facebook/entitycards/model/ScrollLoadError;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, LX/Enz;->d:Lcom/facebook/entitycards/model/ScrollLoadError;

    .line 2167874
    new-instance v1, LX/Ent;

    invoke-direct {v1, p0}, LX/Ent;-><init>(LX/Enz;)V

    iput-object v1, p0, LX/Enz;->e:LX/0TF;

    .line 2167875
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, LX/Enz;->g:Ljava/util/HashMap;

    .line 2167876
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, LX/Enz;->h:Ljava/util/HashMap;

    .line 2167877
    const/4 v1, 0x0

    iput-object v1, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167878
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Enz;->j:Z

    .line 2167879
    const/4 v1, 0x0

    iput-object v1, p0, LX/Enz;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167880
    const/4 v1, 0x0

    iput-object v1, p0, LX/Enz;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167881
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/Enz;->m:LX/0am;

    .line 2167882
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/Enz;->n:LX/0am;

    .line 2167883
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Enz;->o:Z

    .line 2167884
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Enz;->p:Z

    .line 2167885
    iput-object p1, p0, LX/Enz;->q:LX/1My;

    .line 2167886
    iput-object p5, p0, LX/Enz;->t:LX/Emy;

    .line 2167887
    iput-object p4, p0, LX/Enz;->u:LX/Emq;

    .line 2167888
    iput-object p9, p0, LX/Enz;->x:Ljava/lang/String;

    .line 2167889
    invoke-virtual {p10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, LX/Enz;->y:I

    .line 2167890
    invoke-virtual {p11}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, LX/Enz;->z:I

    .line 2167891
    iput-object p2, p0, LX/Enz;->r:LX/Enh;

    .line 2167892
    iput-object p3, p0, LX/Enz;->s:LX/Eoc;

    .line 2167893
    move-object/from16 v0, p12

    iput-object v0, p0, LX/Enz;->A:Ljava/util/concurrent/Executor;

    .line 2167894
    move-object/from16 v0, p13

    iput-object v0, p0, LX/Enz;->B:LX/Emx;

    .line 2167895
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Enz;->C:LX/03V;

    .line 2167896
    if-eqz p9, :cond_0

    .line 2167897
    invoke-virtual {p7, p9}, LX/En2;->a(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "currentEntityId must be in initialEntityIds"

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2167898
    :cond_0
    if-eqz p8, :cond_1

    invoke-virtual {p8}, LX/0P1;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2167899
    new-instance v1, LX/Enp;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, p7, p8, v2, v3}, LX/Enp;-><init>(LX/En3;LX/0P1;Ljava/lang/String;Ljava/lang/String;)V

    .line 2167900
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, p0, LX/Enz;->v:LX/0am;

    .line 2167901
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    iput-object v2, p0, LX/Enz;->w:LX/0am;

    .line 2167902
    new-instance v2, LX/En2;

    invoke-virtual {v1}, LX/Enp;->b()LX/En3;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/En2;->a(I)I

    move-result v1

    invoke-direct {v2, v1}, LX/En2;-><init>(I)V

    iput-object v2, p0, LX/Enz;->f:LX/En2;

    .line 2167903
    :goto_0
    return-void

    .line 2167904
    :cond_1
    invoke-static {p7}, LX/En4;->a(LX/En2;)V

    .line 2167905
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/Enz;->v:LX/0am;

    .line 2167906
    invoke-static {p7}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/Enz;->w:LX/0am;

    .line 2167907
    new-instance v1, LX/En2;

    const/4 v2, 0x0

    invoke-virtual {p7, v2}, LX/En2;->a(I)I

    move-result v2

    invoke-direct {v1, v2}, LX/En2;-><init>(I)V

    iput-object v1, p0, LX/Enz;->f:LX/En2;

    goto :goto_0
.end method

.method public static a(LX/Enz;LX/En2;)LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/En2",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0P1",
            "<",
            "LX/Enn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167794
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    .line 2167795
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/En2;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2167796
    invoke-virtual {p1, v1}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2167797
    iget-object v3, p0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 2167798
    if-eqz v3, :cond_0

    .line 2167799
    iget-object v4, p0, LX/Enz;->g:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2167800
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2167801
    :cond_1
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/Enz;ILjava/lang/String;LX/Enn;)V
    .locals 5

    .prologue
    .line 2167802
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2167803
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v0}, LX/En2;->c()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2167804
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v0, v1}, LX/En2;->a(I)I

    move-result v3

    .line 2167805
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v0, v1}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2167806
    const-string v4, "("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2167807
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2167808
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2167809
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2167810
    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2167811
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2167812
    :cond_0
    iget-object v0, p0, LX/Enz;->C:LX/03V;

    const-string v1, "paged_data_source_discontinuity"

    const-string v3, "Received entity at position {%d} with key {%s} and id {%s} that can\'t be put into the ID list {%s}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, p3, p2, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2167813
    return-void
.end method

.method public static a(LX/Enz;LX/En3;LX/0P1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2167814
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v0}, LX/En2;->c()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2167815
    :goto_1
    invoke-virtual {p1}, LX/En2;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2167816
    invoke-virtual {p1, v1}, LX/En2;->a(I)I

    move-result v2

    .line 2167817
    invoke-virtual {p1, v1}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2167818
    iget-object v3, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v3, v2, v0}, LX/En2;->a(ILjava/lang/Object;)V

    .line 2167819
    iget-object v2, p0, LX/Enz;->g:Ljava/util/HashMap;

    new-instance v3, LX/Enn;

    invoke-direct {v3, v0}, LX/Enn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167820
    iget-object v2, p0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {p2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167821
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 2167822
    goto :goto_0

    .line 2167823
    :cond_1
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    invoke-static {v0}, LX/En4;->a(LX/En2;)V

    .line 2167824
    return-void
.end method

.method public static a$redex0(LX/Enz;LX/Enn;)V
    .locals 1

    .prologue
    .line 2167825
    invoke-direct {p0, p1}, LX/Enz;->b(LX/Enn;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Enl;->a(LX/0P1;)V

    .line 2167826
    return-void
.end method

.method private b(LX/Enn;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2167827
    const-string v0, "left_edge"

    .line 2167828
    iget-object v1, p1, LX/Enn;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2167829
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167830
    iget-boolean v0, p0, LX/Enz;->o:Z

    if-eqz v0, :cond_0

    .line 2167831
    iget-object v0, p0, LX/Enz;->c:Lcom/facebook/entitycards/model/ScrollLoadError;

    .line 2167832
    :goto_0
    return-object v0

    .line 2167833
    :cond_0
    iget-object v0, p0, LX/Enz;->a:Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    goto :goto_0

    .line 2167834
    :cond_1
    const-string v0, "right_edge"

    .line 2167835
    iget-object v1, p1, LX/Enn;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2167836
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2167837
    iget-boolean v0, p0, LX/Enz;->p:Z

    if-eqz v0, :cond_2

    .line 2167838
    iget-object v0, p0, LX/Enz;->d:Lcom/facebook/entitycards/model/ScrollLoadError;

    goto :goto_0

    .line 2167839
    :cond_2
    iget-object v0, p0, LX/Enz;->b:Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    goto :goto_0

    .line 2167840
    :cond_3
    iget-object v0, p0, LX/Enz;->h:Ljava/util/HashMap;

    .line 2167841
    iget-object v1, p1, LX/Enn;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2167842
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private b(LX/Enq;)Z
    .locals 1

    .prologue
    .line 2167843
    iget-object v0, p0, LX/Enz;->r:LX/Enh;

    invoke-interface {v0, p1}, LX/Enh;->a(LX/Enq;)Z

    move-result v0

    return v0
.end method

.method public static d(LX/Enz;LX/Enq;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2167844
    sget-object v0, LX/Enq;->LEFT:LX/Enq;

    if-ne p1, v0, :cond_0

    .line 2167845
    iput-object v1, p0, LX/Enz;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167846
    :goto_0
    return-void

    .line 2167847
    :cond_0
    sget-object v0, LX/Enq;->RIGHT:LX/Enq;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2167848
    iput-object v1, p0, LX/Enz;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2167849
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static d(LX/Enz;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2167850
    iget-object v0, p0, LX/Enz;->C:LX/03V;

    const-string v1, "paged_data_source_received_duplicate"

    const-string v2, "Received entity from {%s} loader with id {%s} already in list"

    .line 2167851
    iget-object v3, p0, LX/Enl;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2167852
    invoke-static {v2, v3, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2167853
    return-void
.end method

.method private m()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167854
    iget-object v0, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2167855
    iget-object v0, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167856
    :goto_0
    return-object v0

    .line 2167857
    :cond_0
    invoke-static {p0}, LX/Enz;->q(LX/Enz;)LX/0Px;

    move-result-object v0

    .line 2167858
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Enz;->j:Z

    .line 2167859
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2167860
    iget-object v3, p0, LX/Enz;->r:LX/Enh;

    iget-object v4, p0, LX/Enz;->q:LX/1My;

    iget-object v5, p0, LX/Enz;->e:LX/0TF;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "load_page_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/Enq;->RIGHT:LX/Enq;

    iget v8, p0, LX/Enz;->y:I

    invoke-interface/range {v3 .. v8}, LX/Enh;->a(LX/1My;LX/0TF;Ljava/lang/String;LX/Enq;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v1, v3

    .line 2167861
    iput-object v1, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167862
    :goto_1
    iget-object v1, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/Env;

    invoke-direct {v2, p0, v0}, LX/Env;-><init>(LX/Enz;LX/0Px;)V

    iget-object v0, p0, LX/Enz;->A:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2167863
    iget-object v0, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Enw;

    invoke-direct {v1, p0}, LX/Enw;-><init>(LX/Enz;)V

    iget-object v2, p0, LX/Enz;->A:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2167864
    :cond_1
    iget-object v1, p0, LX/Enz;->s:LX/Eoc;

    invoke-virtual {v1, v0}, LX/Eoc;->a(LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2167865
    new-instance v2, LX/Enx;

    invoke-direct {v2, p0, v0}, LX/Enx;-><init>(LX/Enz;LX/0Px;)V

    .line 2167866
    sget-object v3, LX/131;->INSTANCE:LX/131;

    move-object v3, v3

    .line 2167867
    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 2167868
    iput-object v1, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1
.end method

.method public static o(LX/Enz;)LX/Ens;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/Ens",
            "<",
            "LX/Enn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167769
    invoke-direct {p0}, LX/Enz;->r()V

    .line 2167770
    new-instance v2, LX/Enr;

    invoke-direct {v2}, LX/Enr;-><init>()V

    .line 2167771
    iget-object v0, p0, LX/Enz;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167772
    iget-object v0, p0, LX/Enz;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v0, p0, LX/Enz;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enn;

    invoke-direct {p0, v0}, LX/Enz;->b(LX/Enn;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/Enr;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2167773
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v0}, LX/En2;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2167774
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v0, v1}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2167775
    iget-object v3, p0, LX/Enz;->g:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/Enr;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2167776
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2167777
    :cond_1
    iget-object v0, p0, LX/Enz;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2167778
    iget-object v0, p0, LX/Enz;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v0, p0, LX/Enz;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enn;

    invoke-direct {p0, v0}, LX/Enz;->b(LX/Enn;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/Enr;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2167779
    :cond_2
    invoke-virtual {v2}, LX/Enr;->a()LX/Ens;

    move-result-object v0

    return-object v0
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 2167908
    iget-object v0, p0, LX/Enl;->e:LX/Eno;

    move-object v0, v0

    .line 2167909
    sget-object v1, LX/Eno;->UNINITIALIZED:LX/Eno;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(LX/Enz;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167780
    iget-object v0, p0, LX/Enz;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167781
    iget-object v0, p0, LX/Enz;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enp;

    .line 2167782
    iget-object p0, v0, LX/Enp;->a:LX/En3;

    move-object v0, p0

    .line 2167783
    :goto_0
    invoke-virtual {v0}, LX/En2;->e()LX/0Px;

    move-result-object v0

    return-object v0

    .line 2167784
    :cond_0
    iget-object v0, p0, LX/Enz;->w:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/En3;

    goto :goto_0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 2167785
    sget-object v0, LX/Enq;->LEFT:LX/Enq;

    invoke-direct {p0, v0}, LX/Enz;->b(LX/Enq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167786
    iget-object v0, p0, LX/Enz;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2167787
    new-instance v0, LX/Enn;

    const-string v1, "left_edge"

    invoke-direct {v0, v1}, LX/Enn;-><init>(Ljava/lang/String;)V

    .line 2167788
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Enz;->m:LX/0am;

    .line 2167789
    :cond_0
    sget-object v0, LX/Enq;->RIGHT:LX/Enq;

    invoke-direct {p0, v0}, LX/Enz;->b(LX/Enq;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167790
    iget-object v0, p0, LX/Enz;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2167791
    new-instance v0, LX/Enn;

    const-string v1, "right_edge"

    invoke-direct {v0, v1}, LX/Enn;-><init>(Ljava/lang/String;)V

    .line 2167792
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Enz;->n:LX/0am;

    .line 2167793
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2167659
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167660
    invoke-virtual {p0}, LX/Enz;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2167661
    const/4 v0, 0x0

    .line 2167662
    :goto_0
    return-object v0

    .line 2167663
    :cond_0
    check-cast p1, LX/Enn;

    .line 2167664
    const-string v0, "right_edge"

    .line 2167665
    iget-object v1, p1, LX/Enn;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2167666
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167667
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    iget-object v1, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v1}, LX/En2;->c()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 2167668
    :cond_1
    const-string v0, "left_edge"

    .line 2167669
    iget-object v1, p1, LX/Enn;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2167670
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2167671
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 2167672
    :cond_2
    iget-object v0, p1, LX/Enn;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2167673
    goto :goto_0
.end method

.method public final a(LX/Enq;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2167674
    invoke-direct {p0}, LX/Enz;->p()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2167675
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 2167676
    sget-object v3, LX/Enq;->LEFT:LX/Enq;

    if-ne p1, v3, :cond_5

    .line 2167677
    iget-object v3, p0, LX/Enz;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v3, :cond_4

    .line 2167678
    :cond_0
    :goto_0
    move v0, v0

    .line 2167679
    if-eqz v0, :cond_2

    .line 2167680
    :cond_1
    :goto_1
    return-void

    .line 2167681
    :cond_2
    invoke-direct {p0}, LX/Enz;->r()V

    .line 2167682
    sget-object v0, LX/Enq;->LEFT:LX/Enq;

    if-ne p1, v0, :cond_3

    .line 2167683
    iget-object v0, p0, LX/Enz;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167684
    iput-boolean v1, p0, LX/Enz;->o:Z

    .line 2167685
    iget-object v0, p0, LX/Enz;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enn;

    invoke-static {p0, v0}, LX/Enz;->a$redex0(LX/Enz;LX/Enn;)V

    .line 2167686
    :goto_2
    iget-object v0, p0, LX/Enz;->t:LX/Emy;

    .line 2167687
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EntityCardPageFetchTime_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/Enq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2167688
    const v1, 0x100004

    const-string v2, "ec_cards_page_download"

    invoke-static {v0, v1, v2}, LX/Emy;->a(LX/Emy;ILjava/lang/String;)V

    .line 2167689
    iget-object v0, p0, LX/Enz;->r:LX/Enh;

    iget-object v1, p0, LX/Enz;->q:LX/1My;

    iget-object v2, p0, LX/Enz;->e:LX/0TF;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "load_page_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget v5, p0, LX/Enz;->y:I

    move-object v4, p1

    invoke-interface/range {v0 .. v5}, LX/Enh;->a(LX/1My;LX/0TF;Ljava/lang/String;LX/Enq;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2167690
    sget-object v1, LX/Enq;->LEFT:LX/Enq;

    if-ne p1, v1, :cond_6

    .line 2167691
    iput-object v0, p0, LX/Enz;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167692
    :goto_3
    new-instance v1, LX/Enu;

    invoke-direct {v1, p0, p1}, LX/Enu;-><init>(LX/Enz;LX/Enq;)V

    iget-object v2, p0, LX/Enz;->A:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 2167693
    :cond_3
    iget-object v0, p0, LX/Enz;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167694
    iput-boolean v1, p0, LX/Enz;->p:Z

    .line 2167695
    iget-object v0, p0, LX/Enz;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enn;

    invoke-static {p0, v0}, LX/Enz;->a$redex0(LX/Enz;LX/Enn;)V

    goto :goto_2

    :cond_4
    move v0, v2

    .line 2167696
    goto/16 :goto_0

    .line 2167697
    :cond_5
    iget-object v3, p0, LX/Enz;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v3, :cond_0

    move v0, v2

    goto/16 :goto_0

    .line 2167698
    :cond_6
    sget-object v1, LX/Enq;->RIGHT:LX/Enq;

    if-ne p1, v1, :cond_7

    const/4 v1, 0x1

    :goto_4
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2167699
    iput-object v0, p0, LX/Enz;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_3

    .line 2167700
    :cond_7
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2167701
    iget-boolean v0, p0, LX/Enz;->j:Z

    if-nez v0, :cond_0

    .line 2167702
    const/4 v0, 0x0

    .line 2167703
    :goto_0
    return v0

    .line 2167704
    :cond_0
    iget-object v0, p0, LX/Enz;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167705
    iget-object v0, p0, LX/Enz;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enp;

    .line 2167706
    iget-object p0, v0, LX/Enp;->b:LX/0P1;

    move-object v0, p0

    .line 2167707
    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 2167708
    :cond_1
    iget-object v0, p0, LX/Enz;->w:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/En3;

    invoke-virtual {v0, p1}, LX/En2;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2167709
    const-string v0, "left_edge"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Enz;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167710
    sget-object v0, LX/Enq;->LEFT:LX/Enq;

    invoke-virtual {p0, v0}, LX/Enz;->a(LX/Enq;)V

    .line 2167711
    iget-object v0, p0, LX/Enz;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enn;

    invoke-static {p0, v0}, LX/Enz;->a$redex0(LX/Enz;LX/Enn;)V

    .line 2167712
    :goto_0
    return-void

    .line 2167713
    :cond_0
    const-string v0, "right_edge"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Enz;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2167714
    sget-object v0, LX/Enq;->RIGHT:LX/Enq;

    invoke-virtual {p0, v0}, LX/Enz;->a(LX/Enq;)V

    .line 2167715
    iget-object v0, p0, LX/Enz;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enn;

    invoke-static {p0, v0}, LX/Enz;->a$redex0(LX/Enz;LX/Enn;)V

    goto :goto_0

    .line 2167716
    :cond_1
    invoke-static {p0}, LX/Enz;->q(LX/Enz;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Asked to refetch an entity that is not in the initial list."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2167717
    invoke-direct {p0}, LX/Enz;->m()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167718
    iget-object v0, p0, LX/Enz;->f:LX/En2;

    invoke-static {p0, v0}, LX/Enz;->a(LX/Enz;LX/En2;)LX/0P1;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Enl;->a(LX/0P1;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2167719
    iget-object v0, p0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167720
    iget-object v0, p0, LX/Enl;->e:LX/Eno;

    move-object v0, v0

    .line 2167721
    sget-object v1, LX/Eno;->UNINITIALIZED:LX/Eno;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Only initialize once."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2167722
    iget-object v0, p0, LX/Enz;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167723
    iget-object v0, p0, LX/Enz;->u:LX/Emq;

    sget-object v1, LX/EnB;->PREVIEW:LX/EnB;

    invoke-virtual {v0, v1}, LX/Emq;->a(LX/EnB;)V

    .line 2167724
    iget-object v0, p0, LX/Enz;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enp;

    .line 2167725
    iget-object v1, v0, LX/Enp;->a:LX/En3;

    move-object v1, v1

    .line 2167726
    iget-object v0, p0, LX/Enz;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enp;

    .line 2167727
    iget-object v2, v0, LX/Enp;->b:LX/0P1;

    move-object v0, v2

    .line 2167728
    invoke-static {p0, v1, v0}, LX/Enz;->a(LX/Enz;LX/En3;LX/0P1;)V

    .line 2167729
    sget-object v0, LX/Eno;->PRELIMINARY_INITIALIZED:LX/Eno;

    .line 2167730
    iput-object v0, p0, LX/Enl;->e:LX/Eno;

    .line 2167731
    invoke-static {p0}, LX/Enz;->o(LX/Enz;)LX/Ens;

    move-result-object v0

    sget-object v1, LX/Eno;->UNINITIALIZED:LX/Eno;

    sget-object v2, LX/Eno;->PRELIMINARY_INITIALIZED:LX/Eno;

    invoke-virtual {p0, v0, v1, v2}, LX/Enl;->a(LX/Ens;LX/Eno;LX/Eno;)V

    .line 2167732
    iget-object v0, p0, LX/Enz;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enp;

    .line 2167733
    iget-object v1, v0, LX/Enp;->a:LX/En3;

    move-object v0, v1

    .line 2167734
    invoke-static {p0, v0}, LX/Enz;->a(LX/Enz;LX/En2;)LX/0P1;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Enl;->a(LX/0P1;)V

    .line 2167735
    :cond_0
    invoke-direct {p0}, LX/Enz;->m()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2167736
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2167737
    iget-object v0, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2167738
    iget-object v0, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2167739
    :cond_0
    iput-object v2, p0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167740
    iget-object v0, p0, LX/Enz;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Enz;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2167741
    iget-object v0, p0, LX/Enz;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2167742
    :cond_1
    iput-object v2, p0, LX/Enz;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167743
    iget-object v0, p0, LX/Enz;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Enz;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2167744
    iget-object v0, p0, LX/Enz;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2167745
    :cond_2
    iput-object v2, p0, LX/Enz;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167746
    return-void
.end method

.method public final g()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167747
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2167748
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v2}, LX/En2;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2167749
    iget-object v2, p0, LX/Enz;->f:LX/En2;

    invoke-virtual {v2, v0}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2167750
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2167751
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2167752
    return-object v0
.end method

.method public final h()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167753
    iget-object v0, p0, LX/Enz;->r:LX/Enh;

    invoke-interface {v0}, LX/Enh;->a()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final i()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2167754
    invoke-direct {p0}, LX/Enz;->p()Z

    move-result v1

    const-string v2, "An uninitialized datasource has no (meaningful) current index"

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2167755
    iget-object v1, p0, LX/Enz;->x:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2167756
    :goto_0
    return v0

    .line 2167757
    :cond_0
    invoke-static {p0}, LX/Enz;->o(LX/Enz;)LX/Ens;

    move-result-object v2

    move v1, v0

    .line 2167758
    :goto_1
    invoke-virtual {v2}, LX/Ens;->c()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2167759
    invoke-virtual {v2, v1}, LX/Ens;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enn;

    .line 2167760
    iget-object v3, p0, LX/Enz;->x:Ljava/lang/String;

    .line 2167761
    iget-object v4, v0, LX/Enn;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2167762
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 2167763
    goto :goto_0

    .line 2167764
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2167765
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to find initial entity ID in current list!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final j()LX/Eny;
    .locals 1

    .prologue
    .line 2167766
    new-instance v0, LX/Eny;

    invoke-direct {v0, p0}, LX/Eny;-><init>(LX/Enz;)V

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2167767
    iget-object v0, p0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 2167768
    iget v0, p0, LX/Enz;->z:I

    return v0
.end method
