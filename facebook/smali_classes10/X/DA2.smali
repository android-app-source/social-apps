.class public abstract LX/DA2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1970762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0m9;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 1970758
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1970759
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1970760
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1970761
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a()LX/DA1;
.end method

.method public a(Landroid/database/Cursor;)Lcom/facebook/contactlogs/data/ContactLogMetadata;
    .locals 3

    .prologue
    .line 1970755
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1970756
    invoke-static {v0, p1}, LX/DA2;->a(LX/0m9;Landroid/database/Cursor;)V

    .line 1970757
    new-instance v1, Lcom/facebook/contactlogs/data/ContactLogMetadata;

    invoke-virtual {p0}, LX/DA2;->a()LX/DA1;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/facebook/contactlogs/data/ContactLogMetadata;-><init>(LX/0m9;LX/DA1;)V

    return-object v1
.end method
