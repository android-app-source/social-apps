.class public LX/Ebk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/Ecj;


# direct methods
.method public constructor <init>(IJLX/Eau;[B)V
    .locals 2

    .prologue
    .line 2144437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144438
    invoke-static {}, LX/Eci;->u()LX/Eci;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Eci;->a(I)LX/Eci;

    move-result-object v0

    .line 2144439
    iget-object v1, p4, LX/Eau;->a:LX/Eat;

    move-object v1, v1

    .line 2144440
    invoke-virtual {v1}, LX/Eat;->a()[B

    move-result-object v1

    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Eci;->a(LX/EWc;)LX/Eci;

    move-result-object v0

    .line 2144441
    iget-object v1, p4, LX/Eau;->b:LX/Eas;

    move-object v1, v1

    .line 2144442
    iget-object p1, v1, LX/Eas;->a:[B

    move-object v1, p1

    .line 2144443
    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Eci;->b(LX/EWc;)LX/Eci;

    move-result-object v0

    invoke-static {p5}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Eci;->c(LX/EWc;)LX/Eci;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LX/Eci;->a(J)LX/Eci;

    move-result-object v0

    invoke-virtual {v0}, LX/Eci;->l()LX/Ecj;

    move-result-object v0

    iput-object v0, p0, LX/Ebk;->a:LX/Ecj;

    .line 2144444
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 2144445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144446
    sget-object v0, LX/Ecj;->a:LX/EWZ;

    invoke-virtual {v0, p1}, LX/EWZ;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ecj;

    move-object v0, v0

    .line 2144447
    iput-object v0, p0, LX/Ebk;->a:LX/Ecj;

    .line 2144448
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2144449
    iget-object v0, p0, LX/Ebk;->a:LX/Ecj;

    .line 2144450
    iget p0, v0, LX/Ecj;->id_:I

    move v0, p0

    .line 2144451
    return v0
.end method

.method public final b()LX/Eau;
    .locals 3

    .prologue
    .line 2144452
    :try_start_0
    iget-object v0, p0, LX/Ebk;->a:LX/Ecj;

    .line 2144453
    iget-object v1, v0, LX/Ecj;->publicKey_:LX/EWc;

    move-object v0, v1

    .line 2144454
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v0

    .line 2144455
    iget-object v1, p0, LX/Ebk;->a:LX/Ecj;

    .line 2144456
    iget-object v2, v1, LX/Ecj;->privateKey_:LX/EWc;

    move-object v1, v2

    .line 2144457
    invoke-virtual {v1}, LX/EWc;->d()[B

    move-result-object v1

    invoke-static {v1}, LX/Ear;->a([B)LX/Eas;

    move-result-object v1

    .line 2144458
    new-instance v2, LX/Eau;

    invoke-direct {v2, v0, v1}, LX/Eau;-><init>(LX/Eat;LX/Eas;)V
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 2144459
    :catch_0
    move-exception v0

    .line 2144460
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final c()[B
    .locals 1

    .prologue
    .line 2144461
    iget-object v0, p0, LX/Ebk;->a:LX/Ecj;

    .line 2144462
    iget-object p0, v0, LX/Ecj;->signature_:LX/EWc;

    move-object v0, p0

    .line 2144463
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 2144464
    iget-object v0, p0, LX/Ebk;->a:LX/Ecj;

    invoke-virtual {v0}, LX/EWX;->jZ_()[B

    move-result-object v0

    return-object v0
.end method
