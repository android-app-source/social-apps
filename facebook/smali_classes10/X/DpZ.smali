.class public LX/DpZ;
.super LX/6kT;
.source ""


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/16 v4, 0xb

    .line 2046007
    const/4 v0, 0x1

    sput-boolean v0, LX/DpZ;->a:Z

    .line 2046008
    new-instance v0, LX/1sv;

    const-string v1, "SalamanderBody"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpZ;->b:LX/1sv;

    .line 2046009
    new-instance v0, LX/1sw;

    const-string v1, "plain_text"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpZ;->c:LX/1sw;

    .line 2046010
    new-instance v0, LX/1sw;

    const-string v1, "attachment_info_list"

    const/16 v2, 0xf

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpZ;->d:LX/1sw;

    .line 2046011
    new-instance v0, LX/1sw;

    const-string v1, "device_local_text"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpZ;->e:LX/1sw;

    .line 2046012
    new-instance v0, LX/1sw;

    const-string v1, "sticker_info"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpZ;->f:LX/1sw;

    .line 2046013
    new-instance v0, LX/1sw;

    const-string v1, "group_key_info"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpZ;->g:LX/1sw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2046225
    invoke-direct {p0}, LX/6kT;-><init>()V

    .line 2046226
    return-void
.end method

.method public static a(LX/Dpl;)LX/DpZ;
    .locals 1

    .prologue
    .line 2046222
    new-instance v0, LX/DpZ;

    invoke-direct {v0}, LX/DpZ;-><init>()V

    .line 2046223
    invoke-static {v0, p0}, LX/DpZ;->b(LX/DpZ;LX/Dpl;)V

    .line 2046224
    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/DpZ;
    .locals 1

    .prologue
    .line 2046219
    new-instance v0, LX/DpZ;

    invoke-direct {v0}, LX/DpZ;-><init>()V

    .line 2046220
    invoke-direct {v0, p0}, LX/DpZ;->d(Ljava/lang/String;)V

    .line 2046221
    return-object v0
.end method

.method public static b(LX/DpZ;LX/Dpg;)V
    .locals 1

    .prologue
    .line 2046215
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2046216
    :cond_0
    const/4 v0, 0x6

    iput v0, p0, LX/DpZ;->setField_:I

    .line 2046217
    iput-object p1, p0, LX/DpZ;->value_:Ljava/lang/Object;

    .line 2046218
    return-void
.end method

.method private static b(LX/DpZ;LX/Dpl;)V
    .locals 1

    .prologue
    .line 2046211
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2046212
    :cond_0
    const/4 v0, 0x7

    iput v0, p0, LX/DpZ;->setField_:I

    .line 2046213
    iput-object p1, p0, LX/DpZ;->value_:Ljava/lang/Object;

    .line 2046214
    return-void
.end method

.method public static b(LX/DpZ;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/DpA;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2046207
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2046208
    :cond_0
    const/4 v0, 0x4

    iput v0, p0, LX/DpZ;->setField_:I

    .line 2046209
    iput-object p1, p0, LX/DpZ;->value_:Ljava/lang/Object;

    .line 2046210
    return-void
.end method

.method public static c(LX/DpZ;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2046203
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2046204
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, LX/DpZ;->setField_:I

    .line 2046205
    iput-object p1, p0, LX/DpZ;->value_:Ljava/lang/Object;

    .line 2046206
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2046199
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2046200
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, LX/DpZ;->setField_:I

    .line 2046201
    iput-object p1, p0, LX/DpZ;->value_:Ljava/lang/Object;

    .line 2046202
    return-void
.end method


# virtual methods
.method public final a(LX/1su;LX/1sw;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2046157
    iget-short v2, p2, LX/1sw;->c:S

    packed-switch v2, :pswitch_data_0

    .line 2046158
    iget-byte v0, p2, LX/1sw;->b:B

    invoke-static {p1, v0}, LX/3ae;->a(LX/1su;B)V

    move-object v0, v1

    .line 2046159
    :goto_0
    return-object v0

    .line 2046160
    :pswitch_0
    iget-byte v0, p2, LX/1sw;->b:B

    sget-object v2, LX/DpZ;->c:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v0, v2, :cond_0

    .line 2046161
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2046162
    :cond_0
    iget-byte v0, p2, LX/1sw;->b:B

    invoke-static {p1, v0}, LX/3ae;->a(LX/1su;B)V

    move-object v0, v1

    .line 2046163
    goto :goto_0

    .line 2046164
    :pswitch_1
    iget-byte v2, p2, LX/1sw;->b:B

    sget-object v3, LX/DpZ;->d:LX/1sw;

    iget-byte v3, v3, LX/1sw;->b:B

    if-ne v2, v3, :cond_4

    .line 2046165
    invoke-virtual {p1}, LX/1su;->h()LX/1u3;

    move-result-object v2

    .line 2046166
    new-instance v1, Ljava/util/ArrayList;

    iget v3, v2, LX/1u3;->b:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2046167
    :goto_1
    iget v3, v2, LX/1u3;->b:I

    if-gez v3, :cond_2

    invoke-static {}, LX/1su;->t()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2046168
    :cond_1
    invoke-static {p1}, LX/DpA;->b(LX/1su;)LX/DpA;

    move-result-object v3

    .line 2046169
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2046170
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2046171
    :cond_2
    iget v3, v2, LX/1u3;->b:I

    if-lt v0, v3, :cond_1

    :cond_3
    move-object v0, v1

    .line 2046172
    goto :goto_0

    .line 2046173
    :cond_4
    iget-byte v0, p2, LX/1sw;->b:B

    invoke-static {p1, v0}, LX/3ae;->a(LX/1su;B)V

    move-object v0, v1

    .line 2046174
    goto :goto_0

    .line 2046175
    :pswitch_2
    iget-byte v0, p2, LX/1sw;->b:B

    sget-object v2, LX/DpZ;->e:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v0, v2, :cond_5

    .line 2046176
    invoke-virtual {p1}, LX/1su;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2046177
    :cond_5
    iget-byte v0, p2, LX/1sw;->b:B

    invoke-static {p1, v0}, LX/3ae;->a(LX/1su;B)V

    move-object v0, v1

    .line 2046178
    goto :goto_0

    .line 2046179
    :pswitch_3
    iget-byte v0, p2, LX/1sw;->b:B

    sget-object v2, LX/DpZ;->f:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v0, v2, :cond_8

    .line 2046180
    const/4 v4, 0x0

    .line 2046181
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    .line 2046182
    :goto_2
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v5

    .line 2046183
    iget-byte v6, v5, LX/1sw;->b:B

    if-eqz v6, :cond_7

    .line 2046184
    iget-short v6, v5, LX/1sw;->c:S

    packed-switch v6, :pswitch_data_1

    .line 2046185
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 2046186
    :pswitch_4
    iget-byte v6, v5, LX/1sw;->b:B

    const/16 v7, 0xa

    if-ne v6, v7, :cond_6

    .line 2046187
    invoke-virtual {p1}, LX/1su;->n()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_2

    .line 2046188
    :cond_6
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 2046189
    :cond_7
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2046190
    new-instance v5, LX/Dpg;

    invoke-direct {v5, v4}, LX/Dpg;-><init>(Ljava/lang/Long;)V

    .line 2046191
    move-object v0, v5

    .line 2046192
    goto/16 :goto_0

    .line 2046193
    :cond_8
    iget-byte v0, p2, LX/1sw;->b:B

    invoke-static {p1, v0}, LX/3ae;->a(LX/1su;B)V

    move-object v0, v1

    .line 2046194
    goto/16 :goto_0

    .line 2046195
    :pswitch_5
    iget-byte v0, p2, LX/1sw;->b:B

    sget-object v2, LX/DpZ;->g:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v0, v2, :cond_9

    .line 2046196
    invoke-static {p1}, LX/Dpl;->b(LX/1su;)LX/Dpl;

    move-result-object v0

    goto/16 :goto_0

    .line 2046197
    :cond_9
    iget-byte v0, p2, LX/1sw;->b:B

    invoke-static {p1, v0}, LX/3ae;->a(LX/1su;B)V

    move-object v0, v1

    .line 2046198
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
    .end packed-switch
.end method

.method public final a(IZ)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2046094
    if-eqz p2, :cond_8

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2046095
    :goto_0
    if-eqz p2, :cond_9

    const-string v0, "\n"

    move-object v3, v0

    .line 2046096
    :goto_1
    if-eqz p2, :cond_a

    const-string v0, " "

    .line 2046097
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "SalamanderBody"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046098
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046099
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046100
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046101
    const/4 v1, 0x1

    .line 2046102
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2046103
    const/4 v7, 0x3

    if-ne v6, v7, :cond_0

    .line 2046104
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046105
    const-string v1, "plain_text"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046106
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046107
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046108
    invoke-virtual {p0}, LX/DpZ;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_b

    .line 2046109
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 2046110
    :cond_0
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2046111
    const/4 v7, 0x4

    if-ne v6, v7, :cond_2

    .line 2046112
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046113
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046114
    const-string v1, "attachment_info_list"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046115
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046116
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046117
    invoke-virtual {p0}, LX/DpZ;->d()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_c

    .line 2046118
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 2046119
    :cond_2
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2046120
    const/4 v7, 0x5

    if-ne v6, v7, :cond_4

    .line 2046121
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046122
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046123
    const-string v1, "device_local_text"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046124
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046125
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046126
    invoke-virtual {p0}, LX/DpZ;->e()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d

    .line 2046127
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 2046128
    :cond_4
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2046129
    const/4 v7, 0x6

    if-ne v6, v7, :cond_10

    .line 2046130
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046131
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046132
    const-string v1, "sticker_info"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046133
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046134
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046135
    invoke-virtual {p0}, LX/DpZ;->f()LX/Dpg;

    move-result-object v1

    if-nez v1, :cond_e

    .line 2046136
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046137
    :goto_6
    iget v1, p0, LX/6kT;->setField_:I

    move v1, v1

    .line 2046138
    const/4 v6, 0x7

    if-ne v1, v6, :cond_7

    .line 2046139
    if-nez v2, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046140
    :cond_6
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046141
    const-string v1, "group_key_info"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046142
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046143
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046144
    invoke-virtual {p0}, LX/DpZ;->g()LX/Dpl;

    move-result-object v0

    if-nez v0, :cond_f

    .line 2046145
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046146
    :cond_7
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046147
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046148
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2046149
    :cond_8
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2046150
    :cond_9
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2046151
    :cond_a
    const-string v0, ""

    goto/16 :goto_2

    .line 2046152
    :cond_b
    invoke-virtual {p0}, LX/DpZ;->c()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2046153
    :cond_c
    invoke-virtual {p0}, LX/DpZ;->d()Ljava/util/List;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2046154
    :cond_d
    invoke-virtual {p0}, LX/DpZ;->e()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2046155
    :cond_e
    invoke-virtual {p0}, LX/DpZ;->f()LX/Dpg;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2046156
    :cond_f
    invoke-virtual {p0}, LX/DpZ;->g()LX/Dpl;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_10
    move v2, v1

    goto/16 :goto_6
.end method

.method public final a(LX/1su;S)V
    .locals 4

    .prologue
    .line 2046074
    packed-switch p2, :pswitch_data_0

    .line 2046075
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot write union with unknown field "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2046076
    :pswitch_0
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046077
    check-cast v0, Ljava/lang/String;

    .line 2046078
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2046079
    :cond_0
    :goto_0
    return-void

    .line 2046080
    :pswitch_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046081
    check-cast v0, Ljava/util/List;

    .line 2046082
    new-instance v1, LX/1u3;

    const/16 v2, 0xc

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v2, v3}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v1}, LX/1su;->a(LX/1u3;)V

    .line 2046083
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DpA;

    .line 2046084
    invoke-virtual {v0, p1}, LX/DpA;->a(LX/1su;)V

    goto :goto_1

    .line 2046085
    :pswitch_2
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046086
    check-cast v0, Ljava/lang/String;

    .line 2046087
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2046088
    :pswitch_3
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046089
    check-cast v0, LX/Dpg;

    .line 2046090
    invoke-virtual {v0, p1}, LX/Dpg;->a(LX/1su;)V

    goto :goto_0

    .line 2046091
    :pswitch_4
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046092
    check-cast v0, LX/Dpl;

    .line 2046093
    invoke-virtual {v0, p1}, LX/Dpl;->a(LX/1su;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/DpZ;)Z
    .locals 2

    .prologue
    .line 2046064
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046065
    iget v1, p1, LX/6kT;->setField_:I

    move v1, v1

    .line 2046066
    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_1

    .line 2046067
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046068
    check-cast v0, [B

    check-cast v0, [B

    .line 2046069
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 2046070
    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2046071
    :cond_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046072
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 2046073
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/1sw;
    .locals 3

    .prologue
    .line 2046056
    packed-switch p1, :pswitch_data_0

    .line 2046057
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown field id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2046058
    :pswitch_0
    sget-object v0, LX/DpZ;->c:LX/1sw;

    .line 2046059
    :goto_0
    return-object v0

    .line 2046060
    :pswitch_1
    sget-object v0, LX/DpZ;->d:LX/1sw;

    goto :goto_0

    .line 2046061
    :pswitch_2
    sget-object v0, LX/DpZ;->e:LX/1sw;

    goto :goto_0

    .line 2046062
    :pswitch_3
    sget-object v0, LX/DpZ;->f:LX/1sw;

    goto :goto_0

    .line 2046063
    :pswitch_4
    sget-object v0, LX/DpZ;->g:LX/1sw;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2046049
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046050
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 2046051
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046052
    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 2046053
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'plain_text\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046054
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2046055
    invoke-virtual {p0, v2}, LX/DpZ;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/DpA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2046042
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046043
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 2046044
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046045
    check-cast v0, Ljava/util/List;

    return-object v0

    .line 2046046
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'attachment_info_list\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046047
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2046048
    invoke-virtual {p0, v2}, LX/DpZ;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2046035
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046036
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 2046037
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046038
    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 2046039
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'device_local_text\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046040
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2046041
    invoke-virtual {p0, v2}, LX/DpZ;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2046032
    instance-of v0, p1, LX/DpZ;

    if-eqz v0, :cond_0

    .line 2046033
    check-cast p1, LX/DpZ;

    invoke-virtual {p0, p1}, LX/DpZ;->a(LX/DpZ;)Z

    move-result v0

    .line 2046034
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()LX/Dpg;
    .locals 3

    .prologue
    .line 2046025
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046026
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 2046027
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046028
    check-cast v0, LX/Dpg;

    return-object v0

    .line 2046029
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'sticker_info\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046030
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2046031
    invoke-virtual {p0, v2}, LX/DpZ;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g()LX/Dpl;
    .locals 3

    .prologue
    .line 2046018
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046019
    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 2046020
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046021
    check-cast v0, LX/Dpl;

    return-object v0

    .line 2046022
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'group_key_info\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046023
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2046024
    invoke-virtual {p0, v2}, LX/DpZ;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2046017
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2046014
    sget-boolean v0, LX/DpZ;->a:Z

    .line 2046015
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpZ;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2046016
    return-object v0
.end method
