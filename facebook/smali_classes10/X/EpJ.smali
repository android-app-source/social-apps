.class public final LX/EpJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EpI;


# instance fields
.field public final synthetic a:LX/2h7;

.field public final synthetic b:LX/5P2;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/5wQ;

.field public final synthetic e:LX/EpL;

.field public final synthetic f:LX/EpK;


# direct methods
.method public constructor <init>(LX/EpK;LX/2h7;LX/5P2;Landroid/content/Context;LX/5wQ;LX/EpL;)V
    .locals 0

    .prologue
    .line 2170162
    iput-object p1, p0, LX/EpJ;->f:LX/EpK;

    iput-object p2, p0, LX/EpJ;->a:LX/2h7;

    iput-object p3, p0, LX/EpJ;->b:LX/5P2;

    iput-object p4, p0, LX/EpJ;->c:Landroid/content/Context;

    iput-object p5, p0, LX/EpJ;->d:LX/5wQ;

    iput-object p6, p0, LX/EpJ;->e:LX/EpL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 5

    .prologue
    .line 2170163
    if-eqz p2, :cond_0

    .line 2170164
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2170165
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2170166
    :goto_0
    iget-object v2, p0, LX/EpJ;->f:LX/EpK;

    iget-object v2, v2, LX/EpK;->c:LX/Epa;

    iget-object v3, p0, LX/EpJ;->a:LX/2h7;

    iget-object v4, p0, LX/EpJ;->b:LX/5P2;

    invoke-virtual {v2, v3, v4}, LX/Epa;->a(LX/2h7;LX/5P2;)LX/EpZ;

    move-result-object v2

    iget-object v3, p0, LX/EpJ;->d:LX/5wQ;

    iget-object v4, p0, LX/EpJ;->e:LX/EpL;

    invoke-virtual {v2, v3, v4, v1, v0}, LX/EpZ;->a(LX/5wN;LX/EpL;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2170167
    return-void

    .line 2170168
    :cond_0
    if-eqz p1, :cond_1

    .line 2170169
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2170170
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->REGULAR_FOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    goto :goto_0

    .line 2170171
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2170172
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->REGULAR_FOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    goto :goto_0
.end method
