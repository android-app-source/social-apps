.class public final LX/Dcg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/photos/PhotoCategoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/photos/PhotoCategoryFragment;)V
    .locals 0

    .prologue
    .line 2018668
    iput-object p1, p0, LX/Dcg;->a:Lcom/facebook/localcontent/photos/PhotoCategoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1c1b7ff2

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018669
    iget-object v1, p0, LX/Dcg;->a:Lcom/facebook/localcontent/photos/PhotoCategoryFragment;

    .line 2018670
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance p0, LX/8AA;

    sget-object p1, LX/8AB;->PHOTOS_BY_CATEGORY:LX/8AB;

    invoke-direct {p0, p1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {p0}, LX/8AA;->l()LX/8AA;

    move-result-object p0

    invoke-virtual {p0}, LX/8AA;->j()LX/8AA;

    move-result-object p0

    sget-object p1, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {p0, p1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object p0

    invoke-static {v3, p0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v3

    .line 2018671
    iget-object p0, v1, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;->p:Lcom/facebook/content/SecureContextHelper;

    const/4 p1, 0x1

    invoke-interface {p0, v3, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2018672
    const v1, -0x18b14ddd

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
