.class public LX/Cno;
.super LX/Cnn;
.source ""


# direct methods
.method public constructor <init>(LX/CpG;)V
    .locals 0

    .prologue
    .line 1934456
    invoke-direct {p0, p1}, LX/Cnn;-><init>(LX/CoY;)V

    .line 1934457
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 1934485
    check-cast p1, LX/Cly;

    invoke-virtual {p0, p1}, LX/Cnn;->a(LX/Cly;)V

    return-void
.end method

.method public final a(LX/Cly;)V
    .locals 6

    .prologue
    .line 1934458
    invoke-super {p0, p1}, LX/Cnn;->a(LX/Cly;)V

    .line 1934459
    check-cast p1, LX/CmO;

    .line 1934460
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934461
    check-cast v0, LX/CpG;

    .line 1934462
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1}, LX/Cly;->e()LX/8Z4;

    move-result-object v2

    .line 1934463
    iget-object v3, p1, LX/CmO;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1934464
    new-instance p0, LX/Cle;

    invoke-direct {p0, v1}, LX/Cle;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v2}, LX/Cle;->a(LX/8Z4;)LX/Cle;

    move-result-object p0

    .line 1934465
    iput-object v3, p0, LX/Cle;->d:Ljava/lang/CharSequence;

    .line 1934466
    move-object p0, p0

    .line 1934467
    invoke-virtual {p0}, LX/Cle;->a()LX/Clf;

    move-result-object p0

    move-object v1, p0

    .line 1934468
    iget-boolean v2, p1, LX/CmO;->b:Z

    move v2, v2

    .line 1934469
    const/16 p0, 0x8

    const/4 v5, 0x0

    .line 1934470
    if-eqz v2, :cond_0

    .line 1934471
    iget-object v3, v0, LX/CpG;->b:LX/CIg;

    iget-object v4, v0, LX/CpG;->h:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934472
    iget-object v2, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v2

    .line 1934473
    invoke-virtual {v3, v4, v1}, LX/CIg;->a(Landroid/widget/TextView;LX/Clf;)V

    .line 1934474
    iget-object v3, v0, LX/CpG;->h:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v3, v5}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1934475
    iget-object v3, v0, LX/CpG;->i:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1934476
    :goto_0
    iget v1, p1, LX/CmO;->a:I

    move v1, v1

    .line 1934477
    const/4 v4, 0x0

    .line 1934478
    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v2

    iget v3, v0, LX/CpG;->c:I

    mul-int/2addr v3, v1

    invoke-virtual {v2, v3, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 1934479
    return-void

    .line 1934480
    :cond_0
    iget-object v3, v0, LX/CpG;->i:Landroid/widget/TextView;

    .line 1934481
    iget-object v4, v1, LX/Clf;->a:Ljava/lang/CharSequence;

    move-object v4, v4

    .line 1934482
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1934483
    iget-object v3, v0, LX/CpG;->h:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v3, p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1934484
    iget-object v3, v0, LX/CpG;->i:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
