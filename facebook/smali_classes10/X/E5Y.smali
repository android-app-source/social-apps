.class public LX/E5Y;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/E5Y;


# instance fields
.field public final a:LX/1vg;


# direct methods
.method public constructor <init>(LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078569
    iput-object p1, p0, LX/E5Y;->a:LX/1vg;

    .line 2078570
    return-void
.end method

.method public static a(LX/0QB;)LX/E5Y;
    .locals 4

    .prologue
    .line 2078571
    sget-object v0, LX/E5Y;->b:LX/E5Y;

    if-nez v0, :cond_1

    .line 2078572
    const-class v1, LX/E5Y;

    monitor-enter v1

    .line 2078573
    :try_start_0
    sget-object v0, LX/E5Y;->b:LX/E5Y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078574
    if-eqz v2, :cond_0

    .line 2078575
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078576
    new-instance p0, LX/E5Y;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-direct {p0, v3}, LX/E5Y;-><init>(LX/1vg;)V

    .line 2078577
    move-object v0, p0

    .line 2078578
    sput-object v0, LX/E5Y;->b:LX/E5Y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078579
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078580
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078581
    :cond_1
    sget-object v0, LX/E5Y;->b:LX/E5Y;

    return-object v0

    .line 2078582
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078583
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
