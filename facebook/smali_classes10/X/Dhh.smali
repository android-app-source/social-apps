.class public final enum LX/Dhh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dhh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dhh;

.field public static final enum CLEAR:LX/Dhh;

.field public static final enum DOMINANT_COLOR_OF_STICKER:LX/Dhh;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2031265
    new-instance v0, LX/Dhh;

    const-string v1, "CLEAR"

    invoke-direct {v0, v1, v2}, LX/Dhh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhh;->CLEAR:LX/Dhh;

    .line 2031266
    new-instance v0, LX/Dhh;

    const-string v1, "DOMINANT_COLOR_OF_STICKER"

    invoke-direct {v0, v1, v3}, LX/Dhh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhh;->DOMINANT_COLOR_OF_STICKER:LX/Dhh;

    .line 2031267
    const/4 v0, 0x2

    new-array v0, v0, [LX/Dhh;

    sget-object v1, LX/Dhh;->CLEAR:LX/Dhh;

    aput-object v1, v0, v2

    sget-object v1, LX/Dhh;->DOMINANT_COLOR_OF_STICKER:LX/Dhh;

    aput-object v1, v0, v3

    sput-object v0, LX/Dhh;->$VALUES:[LX/Dhh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2031264
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static from(Ljava/lang/String;)LX/Dhh;
    .locals 1

    .prologue
    .line 2031261
    const-string v0, "dominant_color_of_sticker"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/Dhh;->DOMINANT_COLOR_OF_STICKER:LX/Dhh;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Dhh;->CLEAR:LX/Dhh;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dhh;
    .locals 1

    .prologue
    .line 2031263
    const-class v0, LX/Dhh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dhh;

    return-object v0
.end method

.method public static values()[LX/Dhh;
    .locals 1

    .prologue
    .line 2031262
    sget-object v0, LX/Dhh;->$VALUES:[LX/Dhh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dhh;

    return-object v0
.end method
