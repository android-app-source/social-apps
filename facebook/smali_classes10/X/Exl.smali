.class public final LX/Exl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/83X;

.field public final synthetic b:LX/Exs;


# direct methods
.method public constructor <init>(LX/Exs;LX/83X;)V
    .locals 0

    .prologue
    .line 2185303
    iput-object p1, p0, LX/Exl;->b:LX/Exs;

    iput-object p2, p0, LX/Exl;->a:LX/83X;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x1c982846

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2185304
    iget-object v1, p0, LX/Exl;->b:LX/Exs;

    iget-object v2, p0, LX/Exl;->a:LX/83X;

    .line 2185305
    instance-of v4, v2, LX/Eus;

    if-eqz v4, :cond_0

    .line 2185306
    iget-object v4, v1, LX/Exs;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2hd;

    iget-object v6, v1, LX/Exs;->s:Ljava/lang/String;

    move-object v4, v2

    check-cast v4, LX/Eus;

    .line 2185307
    iget-object v7, v4, LX/Eus;->h:Ljava/lang/String;

    move-object v7, v7

    .line 2185308
    invoke-interface {v2}, LX/2lr;->a()J

    move-result-wide v8

    invoke-interface {v2}, LX/83W;->g()LX/2h7;

    move-result-object v4

    iget-object v10, v4, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    invoke-virtual/range {v5 .. v10}, LX/2hd;->c(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2185309
    :cond_0
    invoke-interface {v2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    invoke-static {v1, v4}, LX/Exs;->a(LX/Exs;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185310
    iget-object v4, v1, LX/Exs;->q:Ljava/util/Map;

    invoke-interface {v2}, LX/2lr;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2185311
    iget-object v4, v1, LX/Exs;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2dj;

    invoke-interface {v2}, LX/2lr;->a()J

    move-result-wide v6

    sget-object v5, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    invoke-virtual {v4, v6, v7, v5}, LX/2dj;->a(JLX/2hC;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2185312
    iget-object v4, v1, LX/Exs;->g:LX/2do;

    new-instance v5, LX/2iB;

    invoke-interface {v2}, LX/2lr;->a()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, LX/2iB;-><init>(J)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2185313
    const v1, -0x6b53c993

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
