.class public final LX/Ep6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 44

    .prologue
    .line 2169826
    const/16 v40, 0x0

    .line 2169827
    const/16 v39, 0x0

    .line 2169828
    const/16 v38, 0x0

    .line 2169829
    const/16 v37, 0x0

    .line 2169830
    const/16 v36, 0x0

    .line 2169831
    const/16 v35, 0x0

    .line 2169832
    const/16 v34, 0x0

    .line 2169833
    const/16 v33, 0x0

    .line 2169834
    const/16 v32, 0x0

    .line 2169835
    const/16 v31, 0x0

    .line 2169836
    const/16 v30, 0x0

    .line 2169837
    const/16 v29, 0x0

    .line 2169838
    const/16 v28, 0x0

    .line 2169839
    const/16 v27, 0x0

    .line 2169840
    const/16 v26, 0x0

    .line 2169841
    const/16 v25, 0x0

    .line 2169842
    const/16 v24, 0x0

    .line 2169843
    const/16 v23, 0x0

    .line 2169844
    const/16 v22, 0x0

    .line 2169845
    const/16 v21, 0x0

    .line 2169846
    const/16 v20, 0x0

    .line 2169847
    const/16 v19, 0x0

    .line 2169848
    const/16 v18, 0x0

    .line 2169849
    const/16 v17, 0x0

    .line 2169850
    const/16 v16, 0x0

    .line 2169851
    const/4 v15, 0x0

    .line 2169852
    const/4 v14, 0x0

    .line 2169853
    const/4 v13, 0x0

    .line 2169854
    const/4 v12, 0x0

    .line 2169855
    const/4 v11, 0x0

    .line 2169856
    const/4 v10, 0x0

    .line 2169857
    const/4 v9, 0x0

    .line 2169858
    const/4 v8, 0x0

    .line 2169859
    const/4 v7, 0x0

    .line 2169860
    const/4 v6, 0x0

    .line 2169861
    const/4 v5, 0x0

    .line 2169862
    const/4 v4, 0x0

    .line 2169863
    const/4 v3, 0x0

    .line 2169864
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1

    .line 2169865
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2169866
    const/4 v3, 0x0

    .line 2169867
    :goto_0
    return v3

    .line 2169868
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2169869
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1b

    .line 2169870
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v41

    .line 2169871
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2169872
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v42

    sget-object v43, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-eq v0, v1, :cond_1

    if-eqz v41, :cond_1

    .line 2169873
    const-string v42, "alternate_name"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_2

    .line 2169874
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    goto :goto_1

    .line 2169875
    :cond_2
    const-string v42, "can_viewer_act_as_memorial_contact"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_3

    .line 2169876
    const/4 v14, 0x1

    .line 2169877
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 2169878
    :cond_3
    const-string v42, "can_viewer_block"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_4

    .line 2169879
    const/4 v13, 0x1

    .line 2169880
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 2169881
    :cond_4
    const-string v42, "can_viewer_message"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_5

    .line 2169882
    const/4 v12, 0x1

    .line 2169883
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 2169884
    :cond_5
    const-string v42, "can_viewer_poke"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_6

    .line 2169885
    const/4 v11, 0x1

    .line 2169886
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto :goto_1

    .line 2169887
    :cond_6
    const-string v42, "can_viewer_post"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_7

    .line 2169888
    const/4 v10, 0x1

    .line 2169889
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto :goto_1

    .line 2169890
    :cond_7
    const-string v42, "can_viewer_report"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_8

    .line 2169891
    const/4 v9, 0x1

    .line 2169892
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 2169893
    :cond_8
    const-string v42, "cover_photo"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_9

    .line 2169894
    invoke-static/range {p0 .. p1}, LX/Ep4;->a(LX/15w;LX/186;)I

    move-result v33

    goto/16 :goto_1

    .line 2169895
    :cond_9
    const-string v42, "friends"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_a

    .line 2169896
    invoke-static/range {p0 .. p1}, LX/5zW;->a(LX/15w;LX/186;)I

    move-result v32

    goto/16 :goto_1

    .line 2169897
    :cond_a
    const-string v42, "friendship_status"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_b

    .line 2169898
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v31

    goto/16 :goto_1

    .line 2169899
    :cond_b
    const-string v42, "id"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_c

    .line 2169900
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 2169901
    :cond_c
    const-string v42, "is_followed_by_everyone"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_d

    .line 2169902
    const/4 v8, 0x1

    .line 2169903
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 2169904
    :cond_d
    const-string v42, "is_verified"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_e

    .line 2169905
    const/4 v7, 0x1

    .line 2169906
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 2169907
    :cond_e
    const-string v42, "is_viewer_coworker"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_f

    .line 2169908
    const/4 v6, 0x1

    .line 2169909
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 2169910
    :cond_f
    const-string v42, "is_work_user"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_10

    .line 2169911
    const/4 v5, 0x1

    .line 2169912
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 2169913
    :cond_10
    const-string v42, "name"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_11

    .line 2169914
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 2169915
    :cond_11
    const-string v42, "posted_item_privacy_scope"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_12

    .line 2169916
    invoke-static/range {p0 .. p1}, LX/5R2;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 2169917
    :cond_12
    const-string v42, "preliminaryProfilePicture"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_13

    .line 2169918
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 2169919
    :cond_13
    const-string v42, "profile_picture"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_14

    .line 2169920
    invoke-static/range {p0 .. p1}, LX/Ep5;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 2169921
    :cond_14
    const-string v42, "profile_picture_is_silhouette"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_15

    .line 2169922
    const/4 v4, 0x1

    .line 2169923
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 2169924
    :cond_15
    const-string v42, "secondary_subscribe_status"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_16

    .line 2169925
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    goto/16 :goto_1

    .line 2169926
    :cond_16
    const-string v42, "structured_name"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_17

    .line 2169927
    invoke-static/range {p0 .. p1}, LX/3EF;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 2169928
    :cond_17
    const-string v42, "subscribe_status"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_18

    .line 2169929
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    goto/16 :goto_1

    .line 2169930
    :cond_18
    const-string v42, "timeline_context_items"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_19

    .line 2169931
    invoke-static/range {p0 .. p1}, LX/Ep2;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 2169932
    :cond_19
    const-string v42, "url"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1a

    .line 2169933
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 2169934
    :cond_1a
    const-string v42, "viewer_can_see_profile_insights"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_0

    .line 2169935
    const/4 v3, 0x1

    .line 2169936
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto/16 :goto_1

    .line 2169937
    :cond_1b
    const/16 v41, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2169938
    const/16 v41, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2169939
    if-eqz v14, :cond_1c

    .line 2169940
    const/4 v14, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 2169941
    :cond_1c
    if-eqz v13, :cond_1d

    .line 2169942
    const/4 v13, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 2169943
    :cond_1d
    if-eqz v12, :cond_1e

    .line 2169944
    const/4 v12, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 2169945
    :cond_1e
    if-eqz v11, :cond_1f

    .line 2169946
    const/4 v11, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 2169947
    :cond_1f
    if-eqz v10, :cond_20

    .line 2169948
    const/4 v10, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 2169949
    :cond_20
    if-eqz v9, :cond_21

    .line 2169950
    const/4 v9, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 2169951
    :cond_21
    const/4 v9, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 2169952
    const/16 v9, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 2169953
    const/16 v9, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 2169954
    const/16 v9, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 2169955
    if-eqz v8, :cond_22

    .line 2169956
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 2169957
    :cond_22
    if-eqz v7, :cond_23

    .line 2169958
    const/16 v7, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 2169959
    :cond_23
    if-eqz v6, :cond_24

    .line 2169960
    const/16 v6, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 2169961
    :cond_24
    if-eqz v5, :cond_25

    .line 2169962
    const/16 v5, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 2169963
    :cond_25
    const/16 v5, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 2169964
    const/16 v5, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 2169965
    const/16 v5, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 2169966
    const/16 v5, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 2169967
    if-eqz v4, :cond_26

    .line 2169968
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 2169969
    :cond_26
    const/16 v4, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2169970
    const/16 v4, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2169971
    const/16 v4, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2169972
    const/16 v4, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2169973
    const/16 v4, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2169974
    if-eqz v3, :cond_27

    .line 2169975
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->a(IZ)V

    .line 2169976
    :cond_27
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
