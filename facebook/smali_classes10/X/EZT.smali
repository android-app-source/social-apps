.class public LX/EZT;
.super Ljava/util/AbstractList;
.source ""

# interfaces
.implements LX/EYv;
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Ljava/lang/String;",
        ">;",
        "LX/EYv;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# instance fields
.field public final a:LX/EYv;


# direct methods
.method public constructor <init>(LX/EYv;)V
    .locals 0

    .prologue
    .line 2139501
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 2139502
    iput-object p1, p0, LX/EZT;->a:LX/EYv;

    .line 2139503
    return-void
.end method


# virtual methods
.method public final a(I)LX/EWc;
    .locals 1

    .prologue
    .line 2139500
    iget-object v0, p0, LX/EZT;->a:LX/EYv;

    invoke-interface {v0, p1}, LX/EYv;->a(I)LX/EWc;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2139499
    iget-object v0, p0, LX/EZT;->a:LX/EYv;

    invoke-interface {v0}, LX/EYv;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWc;)V
    .locals 1

    .prologue
    .line 2139498
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2139494
    iget-object v0, p0, LX/EZT;->a:LX/EYv;

    invoke-interface {v0, p1}, LX/EYv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2139497
    new-instance v0, LX/EZS;

    invoke-direct {v0, p0}, LX/EZS;-><init>(LX/EZT;)V

    return-object v0
.end method

.method public final listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2139496
    new-instance v0, LX/EZR;

    invoke-direct {v0, p0, p1}, LX/EZR;-><init>(LX/EZT;I)V

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2139495
    iget-object v0, p0, LX/EZT;->a:LX/EYv;

    invoke-interface {v0}, LX/EYv;->size()I

    move-result v0

    return v0
.end method
