.class public LX/Cjc;
.super LX/CjH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CjH",
        "<",
        "Lcom/facebook/richdocument/view/block/ImageBlockView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/Crk;


# direct methods
.method public constructor <init>(LX/Crk;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929429
    const v0, 0x7f03120c

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, LX/CjH;-><init>(II)V

    .line 1929430
    iput-object p1, p0, LX/Cjc;->a:LX/Crk;

    .line 1929431
    return-void
.end method

.method private static a(LX/Cow;)LX/CnT;
    .locals 1

    .prologue
    .line 1929432
    new-instance v0, LX/Cne;

    invoke-direct {v0, p0}, LX/Cne;-><init>(LX/Cow;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;)LX/CnG;
    .locals 1

    .prologue
    .line 1929428
    invoke-static {p1}, LX/Cow;->a(Landroid/view/View;)LX/Cow;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/CnG;)LX/CnT;
    .locals 1

    .prologue
    .line 1929427
    check-cast p1, LX/Cow;

    invoke-static {p1}, LX/Cjc;->a(LX/Cow;)LX/CnT;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)LX/Cs4;
    .locals 2

    .prologue
    .line 1929423
    iget-object v0, p0, LX/Cjc;->a:LX/Crk;

    const v1, 0x7f03120c

    invoke-virtual {v0, v1}, LX/Crk;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1929424
    invoke-static {v0}, LX/Cow;->a(Landroid/view/View;)LX/Cow;

    move-result-object v0

    .line 1929425
    invoke-static {v0}, LX/Cjc;->a(LX/Cow;)LX/CnT;

    .line 1929426
    new-instance v1, LX/Cs4;

    invoke-direct {v1, v0}, LX/Cs4;-><init>(LX/CnG;)V

    return-object v1
.end method
