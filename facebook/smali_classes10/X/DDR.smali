.class public LX/DDR;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DDT;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DDR",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DDT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975618
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1975619
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DDR;->b:LX/0Zi;

    .line 1975620
    iput-object p1, p0, LX/DDR;->a:LX/0Ot;

    .line 1975621
    return-void
.end method

.method public static a(LX/0QB;)LX/DDR;
    .locals 4

    .prologue
    .line 1975574
    const-class v1, LX/DDR;

    monitor-enter v1

    .line 1975575
    :try_start_0
    sget-object v0, LX/DDR;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975576
    sput-object v2, LX/DDR;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975577
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975578
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975579
    new-instance v3, LX/DDR;

    const/16 p0, 0x1f5c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DDR;-><init>(LX/0Ot;)V

    .line 1975580
    move-object v0, v3

    .line 1975581
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975582
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DDR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975583
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975584
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1975587
    check-cast p2, LX/DDQ;

    .line 1975588
    iget-object v0, p0, LX/DDR;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DDT;

    iget-object v1, p2, LX/DDQ;->a:LX/1Po;

    iget-object v2, p2, LX/DDQ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975589
    new-instance v4, LX/DDS;

    invoke-direct {v4, v0, v2}, LX/DDS;-><init>(LX/DDT;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1975590
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    .line 1975591
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1975592
    check-cast v3, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 1975593
    iput-object v3, v5, LX/3mP;->d:LX/25L;

    .line 1975594
    move-object v5, v5

    .line 1975595
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1975596
    check-cast v3, LX/0jW;

    .line 1975597
    iput-object v3, v5, LX/3mP;->e:LX/0jW;

    .line 1975598
    move-object v3, v5

    .line 1975599
    const/16 v5, 0x8

    .line 1975600
    iput v5, v3, LX/3mP;->b:I

    .line 1975601
    move-object v3, v3

    .line 1975602
    iput-object v4, v3, LX/3mP;->g:LX/25K;

    .line 1975603
    move-object v3, v3

    .line 1975604
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 1975605
    iget-object v3, v0, LX/DDT;->c:LX/DDO;

    const/4 v6, 0x0

    .line 1975606
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1975607
    check-cast v4, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-static {v4}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)LX/0Px;

    move-result-object v7

    .line 1975608
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1975609
    invoke-interface {v1}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    invoke-static {v4}, LX/DD1;->b(LX/1PT;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1975610
    new-instance v4, LX/DDZ;

    const/4 v5, 0x0

    const/4 v10, 0x1

    invoke-direct {v4, v2, v5, v10}, LX/DDZ;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;I)V

    invoke-virtual {v9, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1975611
    :cond_0
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v10

    move v5, v6

    :goto_0
    if-ge v5, v10, :cond_1

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 1975612
    new-instance v11, LX/DDZ;

    invoke-direct {v11, v2, v4, v6}, LX/DDZ;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;I)V

    invoke-virtual {v9, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1975613
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 1975614
    :cond_1
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v6, v4

    .line 1975615
    move-object v4, p1

    move-object v5, v2

    move-object v7, v1

    invoke-virtual/range {v3 .. v8}, LX/DDO;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;Ljava/lang/Object;LX/25M;)LX/DDN;

    move-result-object v3

    .line 1975616
    iget-object v4, v0, LX/DDT;->b:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const v5, 0x7f0b124e

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1975617
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1975585
    invoke-static {}, LX/1dS;->b()V

    .line 1975586
    const/4 v0, 0x0

    return-object v0
.end method
