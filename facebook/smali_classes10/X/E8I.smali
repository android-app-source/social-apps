.class public LX/E8I;
.super LX/BeU;
.source ""

# interfaces
.implements LX/Cfn;
.implements LX/Cgb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BeU",
        "<",
        "LX/E8H;",
        ">;",
        "LX/Cfn;",
        "LX/Cgb;"
    }
.end annotation


# instance fields
.field public a:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0o8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/9qX;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/Cfx;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2082236
    invoke-direct {p0, p1}, LX/BeU;-><init>(Landroid/content/Context;)V

    .line 2082237
    const-class v0, LX/E8I;

    invoke-static {v0, p0}, LX/E8I;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2082238
    invoke-virtual {p0}, LX/E8I;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b163c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2082239
    invoke-virtual {p0, v2, v0, v2, v0}, LX/E8I;->setPadding(IIII)V

    .line 2082240
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/E8I;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p0

    check-cast p0, LX/0So;

    iput-object p0, p1, LX/E8I;->a:LX/0So;

    return-void
.end method

.method private getInteractionTracker()LX/2ja;
    .locals 1

    .prologue
    .line 2082293
    iget-object v0, p0, LX/E8I;->b:LX/0o8;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/E8I;->b:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/E8H;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2082282
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2082283
    iget-object v0, p0, LX/E8I;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2082284
    const/4 v0, 0x0

    .line 2082285
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2082286
    goto :goto_0

    .line 2082287
    :cond_1
    new-instance v2, LX/E8H;

    iget-object v0, p0, LX/E8I;->b:LX/0o8;

    invoke-virtual {p0}, LX/E8I;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/E8H;-><init>(LX/0o8;Landroid/content/Context;)V

    .line 2082288
    iget-object v0, p0, LX/E8I;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qX;

    iget-object v3, p0, LX/E8I;->e:LX/Cfx;

    .line 2082289
    iget-object v4, v3, LX/Cfx;->c:LX/0Px;

    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Cfx;

    move-object v3, v4

    .line 2082290
    invoke-virtual {v2, v0, v3, p0}, LX/E8G;->a(LX/9qX;LX/Cfx;LX/Cgb;)V

    .line 2082291
    invoke-virtual {v2, v1, v1, v1, v1}, LX/E8H;->setPadding(IIII)V

    .line 2082292
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2082281
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 2082277
    invoke-direct {p0}, LX/E8I;->getInteractionTracker()LX/2ja;

    move-result-object v0

    .line 2082278
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/E8I;->getUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LX/E8I;->getUnitType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2082279
    invoke-virtual {p0}, LX/E8I;->getUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/E8I;->getUnitType()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Cfl;

    const/4 v4, 0x0

    sget-object v5, LX/Cfc;->STACK_CHILD_INTERACTION:LX/Cfc;

    invoke-direct {v3, v4, v5}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;)V

    invoke-virtual {v0, v1, v2, v3}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2082280
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 7

    .prologue
    .line 2082254
    invoke-direct {p0}, LX/E8I;->getInteractionTracker()LX/2ja;

    move-result-object v2

    .line 2082255
    if-eqz v2, :cond_1

    .line 2082256
    iget-object v0, p0, LX/E8I;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qX;

    .line 2082257
    if-nez p1, :cond_2

    const/4 v1, 0x0

    .line 2082258
    :goto_0
    invoke-virtual {v2, v0}, LX/2ja;->a(LX/9qX;)V

    .line 2082259
    invoke-interface {v0}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/9qX;->m()Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, LX/2ja;->b(Ljava/lang/String;I)V

    .line 2082260
    if-eqz v1, :cond_0

    .line 2082261
    iget-object v3, p0, LX/E8I;->a:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const/4 v6, -0x1

    .line 2082262
    invoke-interface {v1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2082263
    :cond_0
    :goto_1
    iget-object v1, p0, LX/E8I;->a:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    iget-object v1, p0, LX/E8I;->c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2082264
    invoke-interface {v1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/2ja;->e(LX/2ja;Ljava/lang/String;)LX/Cfq;

    move-result-object v3

    .line 2082265
    invoke-virtual {v3}, LX/Cfq;->g()Z

    move-result v6

    if-nez v6, :cond_4

    .line 2082266
    :cond_1
    :goto_2
    return-void

    .line 2082267
    :cond_2
    iget-object v1, p0, LX/E8I;->d:LX/0Px;

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9qX;

    goto :goto_0

    .line 2082268
    :cond_3
    invoke-interface {v1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/2ja;->e(LX/2ja;Ljava/lang/String;)LX/Cfq;

    move-result-object v3

    .line 2082269
    iput v6, v3, LX/Cfq;->e:I

    .line 2082270
    iput v6, v3, LX/Cfq;->d:I

    .line 2082271
    invoke-static {v2, v4, v5}, LX/2ja;->g(LX/2ja;J)V

    goto :goto_1

    .line 2082272
    :cond_4
    iget v6, v3, LX/Cfq;->c:I

    move v6, v6

    .line 2082273
    iget p0, v3, LX/Cfq;->e:I

    invoke-virtual {v2, v0, v6, p0}, LX/2ja;->a(LX/9qX;II)V

    .line 2082274
    iget v6, v3, LX/Cfq;->c:I

    move v6, v6

    .line 2082275
    iget v3, v3, LX/Cfq;->d:I

    invoke-virtual {v2, v0, v6, v3}, LX/2ja;->a(LX/9qX;II)V

    .line 2082276
    invoke-static {v2, v4, v5}, LX/2ja;->g(LX/2ja;J)V

    goto :goto_2
.end method

.method public getNumAttachmentsLoaded()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2082247
    move v1, v0

    move v2, v0

    .line 2082248
    :goto_0
    invoke-virtual {p0}, LX/E8I;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2082249
    invoke-virtual {p0, v1}, LX/E8I;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2082250
    instance-of v3, v0, LX/Cfn;

    if-eqz v3, :cond_0

    .line 2082251
    check-cast v0, LX/Cfn;

    invoke-interface {v0}, LX/Cfn;->getNumAttachmentsLoaded()I

    move-result v0

    add-int/2addr v2, v0

    .line 2082252
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2082253
    :cond_1
    return v2
.end method

.method public getNumQuestions()I
    .locals 1

    .prologue
    .line 2082246
    iget-object v0, p0, LX/E8I;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public getUnitId()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082245
    iget-object v0, p0, LX/E8I;->c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/E8I;->c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUnitType()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2082244
    iget-object v0, p0, LX/E8I;->c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/E8I;->c:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2082243
    return-object p0
.end method

.method public final kC_()V
    .locals 1

    .prologue
    .line 2082241
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BeU;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2082242
    return-void
.end method
