.class public LX/EKw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/EKQ;


# direct methods
.method public constructor <init>(LX/EKQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2107480
    iput-object p1, p0, LX/EKw;->a:LX/EKQ;

    .line 2107481
    return-void
.end method

.method public static a(LX/0QB;)LX/EKw;
    .locals 4

    .prologue
    .line 2107482
    const-class v1, LX/EKw;

    monitor-enter v1

    .line 2107483
    :try_start_0
    sget-object v0, LX/EKw;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107484
    sput-object v2, LX/EKw;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107485
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107486
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107487
    new-instance p0, LX/EKw;

    invoke-static {v0}, LX/EKQ;->a(LX/0QB;)LX/EKQ;

    move-result-object v3

    check-cast v3, LX/EKQ;

    invoke-direct {p0, v3}, LX/EKw;-><init>(LX/EKQ;)V

    .line 2107488
    move-object v0, p0

    .line 2107489
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107490
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107491
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107492
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
