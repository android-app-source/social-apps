.class public final LX/DOa;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/DOb;


# direct methods
.method public constructor <init>(LX/DOb;)V
    .locals 0

    .prologue
    .line 1992385
    iput-object p1, p0, LX/DOa;->b:LX/DOb;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1992391
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1992392
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1992393
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1992394
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    invoke-virtual {v1, v0}, LX/DOb;->m(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1992395
    const v1, 0x7f081b4e

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992396
    new-instance v2, LX/DOS;

    invoke-direct {v2, p0, v0, v3}, LX/DOS;-><init>(LX/DOa;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992397
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    invoke-static {}, LX/10A;->a()I

    move-result v4

    .line 1992398
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992399
    :cond_0
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    const/4 v2, 0x0

    .line 1992400
    invoke-static {v1, v0}, LX/DOb;->e(LX/DOb;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1, v0}, LX/DOb;->o(LX/DOb;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    iget-object v4, v1, LX/DOb;->j:LX/DOK;

    invoke-virtual {v4}, LX/DOK;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v1, LX/DOb;->p:LX/0ad;

    sget-short v5, LX/88j;->i:S

    invoke-interface {v4, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v1, LX/DOb;->p:LX/0ad;

    sget-short v5, LX/88j;->j:S

    invoke-interface {v4, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v2, 0x1

    :cond_2
    move v1, v2

    .line 1992401
    if-eqz v1, :cond_3

    .line 1992402
    const v1, 0x7f0824bd

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    move-object v1, v2

    .line 1992403
    check-cast v1, LX/3Ai;

    const v4, 0x7f0824be

    invoke-virtual {v1, v4}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 1992404
    new-instance v1, LX/DOT;

    invoke-direct {v1, p0, v0, v3}, LX/DOT;-><init>(LX/DOa;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992405
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    .line 1992406
    const v4, 0x7f020970

    move v4, v4

    .line 1992407
    invoke-virtual {v1, v2, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992408
    :cond_3
    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1992409
    invoke-virtual {p0, p1, p2}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1992410
    :cond_4
    invoke-virtual {p0, v0}, LX/1wH;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1992411
    sget-object v1, LX/04D;->GROUP_CHEVRON:LX/04D;

    invoke-virtual {v1}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3, v1}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;)V

    .line 1992412
    :cond_5
    invoke-virtual {p0, v0}, LX/DOa;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1992413
    invoke-virtual {p0, p1, p2, p3}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1992414
    :cond_6
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1992415
    const v1, 0x7f08106b

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992416
    new-instance v2, LX/DOU;

    invoke-direct {v2, p0, v0, v3}, LX/DOU;-><init>(LX/DOa;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992417
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992418
    const v4, 0x7f020952

    move v4, v4

    .line 1992419
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992420
    :cond_7
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 1992421
    if-eqz v1, :cond_8

    .line 1992422
    const v1, 0x7f08106d

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992423
    new-instance v2, LX/DOV;

    invoke-direct {v2, p0, v0, v3}, LX/DOV;-><init>(LX/DOa;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992424
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992425
    const v4, 0x7f0207fd

    move v4, v4

    .line 1992426
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992427
    :cond_8
    invoke-virtual {p0, p2}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1992428
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    .line 1992429
    invoke-virtual {v1, p1, p2, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1992430
    :cond_9
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    invoke-virtual {v1, v0}, LX/DOb;->j(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1992431
    const v1, 0x7f0824b8

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992432
    new-instance v2, LX/DOW;

    invoke-direct {v2, p0, p2}, LX/DOW;-><init>(LX/DOa;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992433
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992434
    const v4, 0x7f020995

    move v4, v4

    .line 1992435
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992436
    :cond_a
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    invoke-virtual {v1, v0}, LX/DOb;->l(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1992437
    const v1, 0x7f0824bc

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992438
    new-instance v2, LX/DOX;

    invoke-direct {v2, p0, p2}, LX/DOX;-><init>(LX/DOa;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992439
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992440
    const v4, 0x7f020995

    move v4, v4

    .line 1992441
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992442
    :cond_b
    invoke-virtual {p0, p1, p2}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1992443
    goto :goto_0

    .line 1992444
    :goto_0
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1992445
    const v1, 0x7f08104c

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992446
    new-instance v2, LX/DOY;

    invoke-direct {v2, p0, v0, p2, v3}, LX/DOY;-><init>(LX/DOa;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992447
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992448
    const v4, 0x7f020a0b

    move v4, v4

    .line 1992449
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992450
    :cond_c
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    .line 1992451
    invoke-static {v1, v0}, LX/DOb;->g(LX/DOb;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-static {v0}, LX/DOb;->d(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v2

    const v4, -0x7aa0fc74

    if-ne v2, v4, :cond_14

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 1992452
    if-eqz v1, :cond_13

    .line 1992453
    const v1, 0x7f081063

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992454
    new-instance v2, LX/DOZ;

    invoke-direct {v2, p0, v3, p2}, LX/DOZ;-><init>(LX/DOa;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992455
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992456
    const v4, 0x7f020a0b

    move v4, v4

    .line 1992457
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992458
    const v1, 0x7f0824ba

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992459
    new-instance v2, LX/DOO;

    invoke-direct {v2, p0, v3, p2}, LX/DOO;-><init>(LX/DOa;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992460
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992461
    const v4, 0x7f020a0b

    move v4, v4

    .line 1992462
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992463
    :cond_d
    :goto_2
    invoke-static {v0}, LX/DOb;->k(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1992464
    const v1, 0x7f0824b9

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992465
    new-instance v2, LX/DOQ;

    invoke-direct {v2, p0, v3, v0}, LX/DOQ;-><init>(LX/DOa;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992466
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992467
    const v4, 0x7f0209ae

    move v4, v4

    .line 1992468
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992469
    :cond_e
    invoke-virtual {p0, p2}, LX/1wH;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1992470
    invoke-virtual {p0, p1, p2, v3}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 1992471
    :cond_f
    invoke-static {v0}, LX/B0n;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    iget-object v1, v1, LX/DOb;->g:LX/B0n;

    invoke-virtual {v1, v0}, LX/B0n;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 1992472
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    iget-object v1, v1, LX/DOb;->g:LX/B0n;

    invoke-virtual {v1, v0}, LX/B0n;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992473
    new-instance v2, LX/DOR;

    invoke-direct {v2, p0, p2}, LX/DOR;-><init>(LX/DOa;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992474
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992475
    const v4, 0x7f020809

    move v4, v4

    .line 1992476
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992477
    :cond_10
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    iget-object v1, v1, LX/DOb;->h:LX/1e0;

    invoke-virtual {v1, v0}, LX/1e0;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1992478
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    iget-object v1, v1, LX/DOb;->h:LX/1e0;

    invoke-virtual {v1, p1, v0, v3}, LX/1e0;->a(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    .line 1992479
    :cond_11
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    iget-object v1, v1, LX/DOb;->h:LX/1e0;

    invoke-virtual {v1, v0}, LX/1e0;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1992480
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    iget-object v1, v1, LX/DOb;->h:LX/1e0;

    invoke-virtual {v1, p1, v0, v3}, LX/1e0;->b(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    .line 1992481
    :cond_12
    return-void

    .line 1992482
    :cond_13
    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    .line 1992483
    invoke-static {v1, v0}, LX/DOb;->g(LX/DOb;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-static {v0}, LX/DOb;->d(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v2

    const v4, -0x589999e3

    if-ne v2, v4, :cond_15

    const/4 v2, 0x1

    :goto_3
    move v1, v2

    .line 1992484
    if-eqz v1, :cond_d

    .line 1992485
    const v1, 0x7f0824ba

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1992486
    new-instance v2, LX/DOP;

    invoke-direct {v2, p0, v3, p2}, LX/DOP;-><init>(LX/DOa;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1992487
    iget-object v2, p0, LX/DOa;->b:LX/DOb;

    .line 1992488
    const v4, 0x7f020a0b

    move v4, v4

    .line 1992489
    invoke-virtual {v2, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1992490
    goto/16 :goto_2

    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_15
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1992491
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1992492
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1992493
    invoke-virtual {p0, p1}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1992494
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result p1

    move v1, p1

    .line 1992495
    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/DOa;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    invoke-virtual {v1, v0}, LX/DOb;->j(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/DOa;->b:LX/DOb;

    invoke-virtual {v1, v0}, LX/DOb;->l(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1992496
    const/4 v1, 0x0

    move v1, v1

    .line 1992497
    if-nez v1, :cond_0

    .line 1992498
    invoke-static {v0}, LX/B0n;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    .line 1992386
    iget-object v0, p0, LX/DOa;->b:LX/DOb;

    iget-object v0, v0, LX/DOb;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00H;

    .line 1992387
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 1992388
    sget-object v1, LX/01T;->GROUPS:LX/01T;

    if-ne v0, v1, :cond_0

    .line 1992389
    const/4 v0, 0x0

    .line 1992390
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    goto :goto_0
.end method
