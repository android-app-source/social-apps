.class public LX/DaX;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaW;


# static fields
.field public static final a:LX/0wT;

.field public static g:LX/0wW;


# instance fields
.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/4lY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/tablet/IsTablet;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:D

.field public i:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field public j:Landroid/widget/TextView;

.field public k:Landroid/widget/TextView;

.field private l:F

.field private m:I

.field public n:LX/0wd;

.field public o:LX/0xi;

.field public p:Ljava/lang/Object;

.field public q:Z

.field public r:Ljava/lang/String;

.field private s:Z

.field public t:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2015360
    const-wide v0, 0x4051800000000000L    # 70.0

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/DaX;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 2015361
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2015362
    const/4 v0, -0x1

    iput v0, p0, LX/DaX;->m:I

    .line 2015363
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2015364
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v3, p0

    check-cast v3, LX/DaX;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    const/16 v5, 0x11fa

    invoke-static {v1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v1}, LX/4lY;->a(LX/0QB;)LX/4lY;

    move-result-object v8

    check-cast v8, LX/4lY;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p1

    check-cast p1, LX/1Ad;

    invoke-static {v1}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    iput-object v4, v3, LX/DaX;->b:Landroid/content/res/Resources;

    iput-object v5, v3, LX/DaX;->c:LX/0Or;

    iput-object v8, v3, LX/DaX;->d:LX/4lY;

    iput-object p1, v3, LX/DaX;->e:LX/1Ad;

    iput-object v1, v3, LX/DaX;->f:Ljava/lang/Boolean;

    .line 2015365
    const v1, 0x7f030fe6

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2015366
    iget-object v1, p0, LX/DaX;->b:Landroid/content/res/Resources;

    .line 2015367
    const v2, 0x7f0b1fc0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0b1fbc

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    const v3, 0x7f0b1fcf

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    move v1, v2

    .line 2015368
    iget-object v2, p0, LX/DaX;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b1fba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2015369
    invoke-virtual {p0, v1}, LX/DaX;->setMinimumHeight(I)V

    .line 2015370
    invoke-virtual {p0, v2}, LX/DaX;->setMinimumWidth(I)V

    .line 2015371
    const v1, 0x7f0d2657

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, LX/DaX;->k:Landroid/widget/TextView;

    .line 2015372
    const v1, 0x7f0d1743

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, LX/DaX;->j:Landroid/widget/TextView;

    .line 2015373
    goto :goto_0

    .line 2015374
    :goto_0
    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, LX/DaX;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2015375
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v2

    invoke-virtual {p0}, LX/DaX;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a084d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p0}, LX/DaX;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v6, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v2

    .line 2015376
    iput-object v2, v1, LX/1Uo;->u:LX/4Ab;

    .line 2015377
    iget-object v2, p0, LX/DaX;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2015378
    sget-object v2, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v7, v2}, LX/1Uo;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)LX/1Uo;

    .line 2015379
    :goto_1
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {p0}, LX/DaX;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v1

    iput-object v1, p0, LX/DaX;->i:LX/1aX;

    .line 2015380
    iget-object v1, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2015381
    sget-object v1, LX/DaX;->g:LX/0wW;

    if-nez v1, :cond_0

    .line 2015382
    iget-object v1, p0, LX/DaX;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wW;

    sput-object v1, LX/DaX;->g:LX/0wW;

    .line 2015383
    iget-object v1, p0, LX/DaX;->d:LX/4lY;

    sget-object v2, LX/DaX;->a:LX/0wT;

    const-string v3, "POG Press/Release Scale"

    .line 2015384
    iget-object v4, v1, LX/4lY;->a:Ljava/util/Map;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2015385
    :cond_0
    :goto_2
    sget-object v1, LX/DaX;->g:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/DaX;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/DaX;->n:LX/0wd;

    .line 2015386
    new-instance v1, LX/EQW;

    invoke-direct {v1, p0}, LX/EQW;-><init>(LX/DaX;)V

    iput-object v1, p0, LX/DaX;->o:LX/0xi;

    .line 2015387
    invoke-virtual {p0, v6}, LX/DaX;->setClickable(Z)V

    .line 2015388
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/DaX;->setWillNotDraw(Z)V

    .line 2015389
    iput-boolean v6, p0, LX/DaX;->q:Z

    .line 2015390
    return-void

    .line 2015391
    :cond_1
    sget-object v2, LX/1Up;->e:LX/1Up;

    invoke-virtual {v1, v7, v2}, LX/1Uo;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)LX/1Uo;

    goto :goto_1

    .line 2015392
    :cond_2
    iget-object v4, v1, LX/4lY;->a:Ljava/util/Map;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2015393
    goto :goto_2
.end method

.method public static a(Landroid/content/res/Resources;)I
    .locals 1

    .prologue
    .line 2015394
    const v0, 0x7f0b1fbb

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    return v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2015395
    iget-wide v0, p0, LX/DaX;->h:D

    iget-object v2, p0, LX/DaX;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b1fbc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v1, v0

    .line 2015396
    iget-object v0, p0, LX/DaX;->j:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2015397
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v2, v1, :cond_0

    .line 2015398
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 2015399
    iget-object v1, p0, LX/DaX;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2015400
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2015417
    const/4 v0, 0x0

    return v0
.end method

.method public final a(D)V
    .locals 3

    .prologue
    .line 2015401
    iget-boolean v0, p0, LX/DaX;->s:Z

    if-eqz v0, :cond_0

    .line 2015402
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-double v0, p1, v0

    double-to-float v0, v0

    const/high16 v1, 0x40a00000    # 5.0f

    mul-float/2addr v0, v1

    invoke-virtual {p0, v0}, LX/DaX;->a(F)V

    .line 2015403
    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 4

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 2015404
    iput p1, p0, LX/DaX;->l:F

    .line 2015405
    const/high16 v1, 0x3e800000    # 0.25f

    sub-float v1, p1, v1

    .line 2015406
    cmpl-float v2, v1, v3

    if-lez v2, :cond_0

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2015407
    :cond_0
    iget-object v1, p0, LX/DaX;->j:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    .line 2015408
    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-static {v0, v2, v3, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 2015409
    iget-object v1, p0, LX/DaX;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2015410
    invoke-virtual {p0}, LX/DaX;->invalidate()V

    .line 2015411
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;DLcom/facebook/common/callercontext/CallerContext;)V
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/4 v1, 0x0

    .line 2015283
    iget-object v0, p0, LX/DaX;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2015284
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DaX;->a(F)V

    .line 2015285
    :cond_0
    iput-wide p5, p0, LX/DaX;->h:D

    .line 2015286
    invoke-direct {p0}, LX/DaX;->a()V

    .line 2015287
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 2015288
    iget-boolean v3, p0, LX/DaX;->q:Z

    if-eqz v3, :cond_a

    .line 2015289
    :cond_1
    :goto_0
    move v0, v0

    .line 2015290
    if-eqz v0, :cond_3

    .line 2015291
    iput-object p2, p0, LX/DaX;->p:Ljava/lang/Object;

    .line 2015292
    iput-object p1, p0, LX/DaX;->r:Ljava/lang/String;

    .line 2015293
    if-eqz p2, :cond_5

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2015294
    :goto_1
    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, LX/DaX;->a(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_c

    .line 2015295
    :cond_2
    iget-object v2, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v2}, LX/1aX;->h()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2015296
    :goto_2
    iget-object v2, p0, LX/DaX;->e:LX/1Ad;

    iget-object v3, p0, LX/DaX;->i:LX/1aX;

    .line 2015297
    iget-object p1, v3, LX/1aX;->f:LX/1aZ;

    move-object v3, p1

    .line 2015298
    invoke-virtual {v2, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2, v0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, p7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 2015299
    iget-object v2, p0, LX/DaX;->i:LX/1aX;

    iget-object v3, p0, LX/DaX;->e:LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1aX;->a(LX/1aZ;)V

    .line 2015300
    :cond_3
    iget-object v0, p0, LX/DaX;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2015301
    iget v0, p0, LX/DaX;->m:I

    if-eq v0, p3, :cond_4

    .line 2015302
    iget-object v0, p0, LX/DaX;->b:Landroid/content/res/Resources;

    invoke-static {v0}, LX/DaX;->a(Landroid/content/res/Resources;)I

    move-result v0

    int-to-double v2, v0

    iget-wide v4, p0, LX/DaX;->h:D

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 2015303
    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, LX/DaX;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b1fc6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    iget-object v2, p0, LX/DaX;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b1fc1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    add-int/2addr v2, v0

    .line 2015304
    iget-object v3, p0, LX/DaX;->k:Landroid/widget/TextView;

    if-nez p3, :cond_6

    const/16 v0, 0x8

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2015305
    iget-object v3, p0, LX/DaX;->k:Landroid/widget/TextView;

    if-le p3, v6, :cond_7

    const-string v0, "20+"

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2015306
    iget-object v0, p0, LX/DaX;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b1fc6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2015307
    iget-object v3, p0, LX/DaX;->b:Landroid/content/res/Resources;

    const v4, 0x7f0b1fc7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2015308
    if-le p3, v6, :cond_8

    .line 2015309
    iget-object v4, p0, LX/DaX;->k:Landroid/widget/TextView;

    mul-int/lit8 v5, v3, 0x2

    add-int/2addr v0, v5

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setMinWidth(I)V

    .line 2015310
    sub-int v0, v2, v3

    .line 2015311
    :goto_5
    iget-object v2, p0, LX/DaX;->k:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 2015312
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-eq v3, v0, :cond_4

    .line 2015313
    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2015314
    iget-object v3, p0, LX/DaX;->k:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2015315
    :cond_4
    iput p3, p0, LX/DaX;->m:I

    .line 2015316
    iput-boolean v1, p0, LX/DaX;->q:Z

    .line 2015317
    return-void

    .line 2015318
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 2015319
    goto :goto_3

    .line 2015320
    :cond_7
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 2015321
    :cond_8
    const/16 v4, 0x9

    if-le p3, v4, :cond_9

    .line 2015322
    iget-object v4, p0, LX/DaX;->k:Landroid/widget/TextView;

    add-int/2addr v0, v3

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setMinWidth(I)V

    .line 2015323
    div-int/lit8 v0, v3, 0x2

    sub-int v0, v2, v0

    goto :goto_5

    .line 2015324
    :cond_9
    iget-object v3, p0, LX/DaX;->k:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setMinWidth(I)V

    move v0, v2

    .line 2015325
    goto :goto_5

    .line 2015326
    :cond_a
    iget-object v3, p0, LX/DaX;->r:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2015327
    if-nez p2, :cond_b

    .line 2015328
    iget-object v3, p0, LX/DaX;->p:Ljava/lang/Object;

    if-nez v3, :cond_1

    move v0, v2

    goto/16 :goto_0

    .line 2015329
    :cond_b
    iget-object v3, p0, LX/DaX;->p:Ljava/lang/Object;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v2

    goto/16 :goto_0

    .line 2015330
    :cond_c
    iget-object v2, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v2}, LX/1aX;->h()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    invoke-virtual {p0, p1}, LX/DaX;->a(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, LX/1af;->b(I)V

    goto/16 :goto_2
.end method

.method public final dispatchSetPressed(Z)V
    .locals 5

    .prologue
    .line 2015412
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchSetPressed(Z)V

    .line 2015413
    if-eqz p1, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    .line 2015414
    :goto_0
    iget-object v1, p0, LX/DaX;->n:LX/0wd;

    float-to-double v3, v0

    invoke-virtual {v1, v3, v4}, LX/0wd;->b(D)LX/0wd;

    .line 2015415
    return-void

    .line 2015416
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 2015338
    iget-object v0, p0, LX/DaX;->b:Landroid/content/res/Resources;

    invoke-static {v0}, LX/DaX;->a(Landroid/content/res/Resources;)I

    move-result v0

    int-to-double v2, v0

    iget-wide v4, p0, LX/DaX;->h:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    .line 2015339
    invoke-virtual {p0}, LX/DaX;->getWidth()I

    move-result v0

    sub-int/2addr v0, v2

    div-int/lit8 v3, v0, 0x2

    iget-boolean v0, p0, LX/DaX;->s:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/DaX;->getLeft()I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    .line 2015340
    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, LX/DaX;->l:F

    const v5, 0x3dcccccd    # 0.1f

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    const v4, 0x3f933333    # 1.15f

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 2015341
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 2015342
    iget-object v5, p0, LX/DaX;->b:Landroid/content/res/Resources;

    .line 2015343
    const v6, 0x7f0b1fbf

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    move v5, v6

    .line 2015344
    iget-boolean v6, p0, LX/DaX;->s:Z

    if-eqz v6, :cond_0

    invoke-virtual {p0}, LX/DaX;->getTop()I

    move-result v1

    :cond_0
    add-int/2addr v1, v5

    .line 2015345
    invoke-virtual {p0}, LX/DaX;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    int-to-float v5, v5

    int-to-float v7, v2

    div-float/2addr v7, v8

    add-float/2addr v5, v7

    invoke-virtual {p1, v3, v3, v6, v5}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 2015346
    iget-object v3, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v3}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2015347
    add-int v5, v0, v2

    add-int/2addr v2, v1

    invoke-virtual {v3, v0, v1, v5, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2015348
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2015349
    iget-boolean v0, p0, LX/DaX;->s:Z

    if-eqz v0, :cond_2

    .line 2015350
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 2015351
    goto :goto_0

    .line 2015352
    :cond_2
    iget v0, p0, LX/DaX;->m:I

    if-eqz v0, :cond_3

    .line 2015353
    iget-object v0, p0, LX/DaX;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/DaX;->getDrawingTime()J

    move-result-wide v2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/DaX;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 2015354
    :cond_3
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2015355
    iget-object v0, p0, LX/DaX;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/DaX;->getDrawingTime()J

    move-result-wide v2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/DaX;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_1
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6caab250

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2015356
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2015357
    iget-object v1, p0, LX/DaX;->n:LX/0wd;

    iget-object v2, p0, LX/DaX;->o:LX/0xi;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2015358
    iget-object v1, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 2015359
    const/16 v1, 0x2d

    const v2, -0x4665b8f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x72c331f0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2015334
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2015335
    iget-object v1, p0, LX/DaX;->n:LX/0wd;

    iget-object v2, p0, LX/DaX;->o:LX/0xi;

    invoke-virtual {v1, v2}, LX/0wd;->b(LX/0xi;)LX/0wd;

    .line 2015336
    iget-object v1, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 2015337
    const/16 v1, 0x2d

    const v2, 0x68ddc2cd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2015331
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishTemporaryDetach()V

    .line 2015332
    iget-object v0, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2015333
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 2015280
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2015281
    invoke-virtual {p0}, LX/DaX;->getMeasuredWidth()I

    move-result v0

    iget-wide v2, p0, LX/DaX;->h:D

    iget-object v1, p0, LX/DaX;->b:Landroid/content/res/Resources;

    const v4, 0x7f0b1fbc

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-double v4, v1

    mul-double/2addr v2, v4

    double-to-int v1, v2

    iget-object v2, p0, LX/DaX;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b1fcf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, LX/DaX;->setMeasuredDimension(II)V

    .line 2015282
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2015277
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onStartTemporaryDetach()V

    .line 2015278
    iget-object v0, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2015279
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x6067bbc5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2015274
    iget-boolean v0, p0, LX/DaX;->t:Z

    if-eqz v0, :cond_0

    .line 2015275
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x65140814

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2015276
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    const v2, 0x5555238f

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setCurrentDragLevel(F)V
    .locals 4

    .prologue
    .line 2015272
    iget-object v0, p0, LX/DaX;->n:LX/0wd;

    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 2015273
    return-void
.end method

.method public setHovering(Z)V
    .locals 1

    .prologue
    .line 2015266
    iget-boolean v0, p0, LX/DaX;->s:Z

    if-ne p1, v0, :cond_0

    .line 2015267
    :goto_0
    return-void

    .line 2015268
    :cond_0
    if-eqz p1, :cond_1

    .line 2015269
    iget-object v0, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2015270
    :goto_1
    iput-boolean p1, p0, LX/DaX;->s:Z

    goto :goto_0

    .line 2015271
    :cond_1
    iget-object v0, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    goto :goto_1
.end method

.method public setPlaceholderImage(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 2015264
    iget-object v0, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->b(I)V

    .line 2015265
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 2015263
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DaX;->i:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
