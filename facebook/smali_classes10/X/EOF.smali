.class public final LX/EOF;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/EOH;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/EOG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EOH",
            "<TE;>.SearchResultsOpinionSearchQueryStorySelectorComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/EOH;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/EOH;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2114203
    iput-object p1, p0, LX/EOF;->b:LX/EOH;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2114204
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "moduleProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "stories"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/EOF;->c:[Ljava/lang/String;

    .line 2114205
    iput v3, p0, LX/EOF;->d:I

    .line 2114206
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/EOF;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/EOF;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/EOF;LX/1De;IILX/EOG;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/EOH",
            "<TE;>.SearchResultsOpinionSearchQueryStorySelectorComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2114199
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2114200
    iput-object p4, p0, LX/EOF;->a:LX/EOG;

    .line 2114201
    iget-object v0, p0, LX/EOF;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2114202
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/EOF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/C33;",
            ">;)",
            "LX/EOH",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2114176
    iget-object v0, p0, LX/EOF;->a:LX/EOG;

    iput-object p1, v0, LX/EOG;->b:LX/0Px;

    .line 2114177
    iget-object v0, p0, LX/EOF;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2114178
    return-object p0
.end method

.method public final a(LX/1Ps;)LX/EOF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/EOH",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2114196
    iget-object v0, p0, LX/EOF;->a:LX/EOG;

    iput-object p1, v0, LX/EOG;->c:LX/1Ps;

    .line 2114197
    iget-object v0, p0, LX/EOF;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2114198
    return-object p0
.end method

.method public final a(LX/CzL;)LX/EOF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleInterfaces$SearchResultsOpinionModule;",
            ">;)",
            "LX/EOH",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2114193
    iget-object v0, p0, LX/EOF;->a:LX/EOG;

    iput-object p1, v0, LX/EOG;->a:LX/CzL;

    .line 2114194
    iget-object v0, p0, LX/EOF;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2114195
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2114189
    invoke-super {p0}, LX/1X5;->a()V

    .line 2114190
    const/4 v0, 0x0

    iput-object v0, p0, LX/EOF;->a:LX/EOG;

    .line 2114191
    iget-object v0, p0, LX/EOF;->b:LX/EOH;

    iget-object v0, v0, LX/EOH;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2114192
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/EOH;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2114179
    iget-object v1, p0, LX/EOF;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EOF;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/EOF;->d:I

    if-ge v1, v2, :cond_2

    .line 2114180
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2114181
    :goto_0
    iget v2, p0, LX/EOF;->d:I

    if-ge v0, v2, :cond_1

    .line 2114182
    iget-object v2, p0, LX/EOF;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2114183
    iget-object v2, p0, LX/EOF;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2114184
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2114185
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2114186
    :cond_2
    iget-object v0, p0, LX/EOF;->a:LX/EOG;

    .line 2114187
    invoke-virtual {p0}, LX/EOF;->a()V

    .line 2114188
    return-object v0
.end method
