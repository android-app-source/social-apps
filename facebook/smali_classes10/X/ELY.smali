.class public final LX/ELY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EJZ;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;LX/EJZ;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2109246
    iput-object p1, p0, LX/ELY;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    iput-object p2, p0, LX/ELY;->a:LX/EJZ;

    iput-object p3, p0, LX/ELY;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 17

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const v3, -0x75464e19

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 2109247
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->a:LX/EJZ;

    iget-object v1, v1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->a:LX/EJZ;

    iget-object v1, v1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2109248
    :cond_0
    const/4 v1, 0x2

    const/4 v2, 0x2

    const v3, -0x3deffdbc

    invoke-static {v1, v2, v3, v10}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2109249
    :goto_0
    return-void

    .line 2109250
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->a:LX/EJZ;

    iget-object v1, v1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELY;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/0ax;->bY:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELY;->a:LX/EJZ;

    iget-object v2, v2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v9, v1

    .line 2109251
    :goto_1
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 2109252
    const-string v1, "profile_name"

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELY;->a:LX/EJZ;

    iget-object v2, v2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2109253
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, LX/CvY;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v12

    sget-object v13, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v14, LX/7CM;->INLINE_PHOTOS_LINK:LX/7CM;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->b:LX/1Pn;

    check-cast v1, LX/CxG;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELY;->a:LX/EJZ;

    iget-object v2, v2, LX/EJZ;->c:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-interface {v1, v2}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->a:LX/EJZ;

    iget-object v0, v1, LX/EJZ;->c:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v3, LX/7CM;->INLINE_PHOTOS_LINK:LX/7CM;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/ELY;->a:LX/EJZ;

    iget-object v4, v4, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, LX/ELY;->a:LX/EJZ;

    iget-object v6, v6, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    move-object v1, v8

    move-object v2, v12

    move-object v3, v13

    move-object v4, v14

    move v5, v15

    move-object/from16 v6, v16

    invoke-virtual/range {v1 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2109254
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ELY;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17W;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELY;->b:LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v9, v11, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2109255
    const v1, 0x2aafef62

    invoke-static {v1, v10}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2109256
    :cond_2
    sget-object v1, LX/0ax;->bZ:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ELY;->a:LX/EJZ;

    iget-object v2, v2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v9, v1

    goto/16 :goto_1
.end method
