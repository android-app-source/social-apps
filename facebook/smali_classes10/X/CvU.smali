.class public LX/CvU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:J

.field public final c:J

.field public final d:Ljava/lang/String;

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public final g:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public final h:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final i:Ljava/lang/String;

.field public final j:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/String;JJLX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0P1;)V
    .locals 1
    .param p8    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/graphql/enums/GraphQLObjectType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "JJ",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1948343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1948344
    iput p1, p0, LX/CvU;->a:I

    .line 1948345
    iput-wide p3, p0, LX/CvU;->b:J

    .line 1948346
    iput-wide p5, p0, LX/CvU;->c:J

    .line 1948347
    iput-object p7, p0, LX/CvU;->e:LX/0Px;

    .line 1948348
    iput-object p8, p0, LX/CvU;->f:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1948349
    iput-object p9, p0, LX/CvU;->g:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1948350
    iput-object p10, p0, LX/CvU;->i:Ljava/lang/String;

    .line 1948351
    iput-object p11, p0, LX/CvU;->h:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1948352
    iput-object p12, p0, LX/CvU;->j:LX/0P1;

    .line 1948353
    iput-object p2, p0, LX/CvU;->d:Ljava/lang/String;

    .line 1948354
    return-void
.end method

.method public static a(ILjava/lang/String;JJLcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;LX/0P1;)LX/CvU;
    .locals 16
    .param p8    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "JJ",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)",
            "LX/CvU;"
        }
    .end annotation

    .prologue
    .line 1948355
    move-object/from16 v0, p6

    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_1

    .line 1948356
    invoke-interface/range {p6 .. p6}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface/range {p6 .. p6}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    :goto_0
    move-object v10, v2

    .line 1948357
    :goto_1
    move-object/from16 v0, p6

    instance-of v2, v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v2, :cond_3

    move-object/from16 v2, p6

    .line 1948358
    check-cast v2, Lcom/facebook/search/results/model/SearchResultsBridge;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsBridge;->o()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    check-cast p6, Lcom/facebook/search/results/model/SearchResultsBridge;

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/search/results/model/SearchResultsBridge;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    invoke-static {v2, v3}, LX/8eM;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v14

    .line 1948359
    :goto_2
    new-instance v3, LX/CvU;

    move/from16 v4, p0

    move-object/from16 v5, p1

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move-object/from16 v15, p10

    invoke-direct/range {v3 .. v15}, LX/CvU;-><init>(ILjava/lang/String;JJLX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0P1;)V

    return-object v3

    .line 1948360
    :cond_0
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    goto :goto_0

    .line 1948361
    :cond_1
    move-object/from16 v0, p6

    instance-of v2, v0, LX/Cz5;

    if-eqz v2, :cond_2

    move-object/from16 v2, p6

    .line 1948362
    check-cast v2, LX/Cz5;

    invoke-interface {v2}, LX/Cz5;->p()LX/0Px;

    move-result-object v10

    goto :goto_1

    .line 1948363
    :cond_2
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v10

    goto :goto_1

    .line 1948364
    :cond_3
    invoke-interface/range {p6 .. p6}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v14

    goto :goto_2
.end method
