.class public final enum LX/E0V;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E0V;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E0V;

.field public static final enum CREATE:LX/E0V;

.field public static final enum EDIT:LX/E0V;

.field public static final enum INVALID:LX/E0V;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2068550
    new-instance v0, LX/E0V;

    const-string v1, "CREATE"

    invoke-direct {v0, v1, v2}, LX/E0V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0V;->CREATE:LX/E0V;

    .line 2068551
    new-instance v0, LX/E0V;

    const-string v1, "EDIT"

    invoke-direct {v0, v1, v3}, LX/E0V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0V;->EDIT:LX/E0V;

    .line 2068552
    new-instance v0, LX/E0V;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v4}, LX/E0V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0V;->INVALID:LX/E0V;

    .line 2068553
    const/4 v0, 0x3

    new-array v0, v0, [LX/E0V;

    sget-object v1, LX/E0V;->CREATE:LX/E0V;

    aput-object v1, v0, v2

    sget-object v1, LX/E0V;->EDIT:LX/E0V;

    aput-object v1, v0, v3

    sget-object v1, LX/E0V;->INVALID:LX/E0V;

    aput-object v1, v0, v4

    sput-object v0, LX/E0V;->$VALUES:[LX/E0V;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2068549
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/E0V;
    .locals 1

    .prologue
    .line 2068548
    const-class v0, LX/E0V;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E0V;

    return-object v0
.end method

.method public static values()[LX/E0V;
    .locals 1

    .prologue
    .line 2068547
    sget-object v0, LX/E0V;->$VALUES:[LX/E0V;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E0V;

    return-object v0
.end method
