.class public final LX/DY6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

.field public final synthetic b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;)V
    .locals 0

    .prologue
    .line 2011159
    iput-object p1, p0, LX/DY6;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iput-object p2, p0, LX/DY6;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x2

    const v0, 0x38827f19

    invoke-static {v7, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2011160
    new-instance v1, LX/0ju;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2011161
    iget-object v2, p0, LX/DY6;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v2, v2, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v3, 0x7f0830de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/DY4;

    invoke-direct {v3, p0}, LX/DY4;-><init>(LX/DY6;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2011162
    iget-object v2, p0, LX/DY6;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v2, v2, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v3, 0x7f0830da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/DY5;

    invoke-direct {v3, p0}, LX/DY5;-><init>(LX/DY6;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2011163
    iget-object v2, p0, LX/DY6;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v2, v2, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v3, 0x7f0830d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2011164
    iget-object v2, p0, LX/DY6;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v2, v2, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v3, 0x7f0830db

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/DY6;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

    invoke-virtual {v6}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2011165
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2011166
    const v1, 0x7b05e2d5

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
