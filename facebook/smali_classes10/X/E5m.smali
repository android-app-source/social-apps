.class public final LX/E5m;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5n;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public final synthetic b:LX/E5n;


# direct methods
.method public constructor <init>(LX/E5n;)V
    .locals 1

    .prologue
    .line 2078916
    iput-object p1, p0, LX/E5m;->b:LX/E5n;

    .line 2078917
    move-object v0, p1

    .line 2078918
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2078919
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078915
    const-string v0, "ReactionPlainHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2078904
    if-ne p0, p1, :cond_1

    .line 2078905
    :cond_0
    :goto_0
    return v0

    .line 2078906
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078907
    goto :goto_0

    .line 2078908
    :cond_3
    check-cast p1, LX/E5m;

    .line 2078909
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078910
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078911
    if-eq v2, v3, :cond_0

    .line 2078912
    iget-object v2, p0, LX/E5m;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/E5m;->a:Ljava/lang/String;

    iget-object v3, p1, LX/E5m;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2078913
    goto :goto_0

    .line 2078914
    :cond_4
    iget-object v2, p1, LX/E5m;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
