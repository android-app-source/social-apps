.class public final LX/ETL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/ETN;

.field public b:LX/ETK;

.field public c:LX/ETA;


# direct methods
.method public constructor <init>(LX/ETN;)V
    .locals 1

    .prologue
    .line 2124692
    iput-object p1, p0, LX/ETL;->a:LX/ETN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2124693
    sget-object v0, LX/ETK;->EMPTY:LX/ETK;

    iput-object v0, p0, LX/ETL;->b:LX/ETK;

    .line 2124694
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2124724
    iget-object v0, p0, LX/ETL;->c:LX/ETA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2124695
    iget-object v0, p0, LX/ETL;->c:LX/ETA;

    if-nez v0, :cond_0

    .line 2124696
    :goto_0
    return-void

    .line 2124697
    :cond_0
    iget-object v0, p0, LX/ETL;->b:LX/ETK;

    .line 2124698
    iget-object v1, p0, LX/ETL;->a:LX/ETN;

    iget-object v1, v1, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2124699
    sget-object v1, LX/ETK;->EMPTY:LX/ETK;

    .line 2124700
    :goto_1
    move-object v1, v1

    .line 2124701
    iput-object v1, p0, LX/ETL;->b:LX/ETK;

    .line 2124702
    iget-object v1, p0, LX/ETL;->b:LX/ETK;

    if-ne v1, v0, :cond_2

    .line 2124703
    iget-object v0, p0, LX/ETL;->b:LX/ETK;

    sget-object v1, LX/ETK;->EMPTY:LX/ETK;

    if-eq v0, v1, :cond_1

    .line 2124704
    iget-object v0, p0, LX/ETL;->c:LX/ETA;

    .line 2124705
    iget-object v1, v0, LX/ETA;->a:LX/ETB;

    invoke-static {v1}, LX/ETB;->u(LX/ETB;)V

    .line 2124706
    :cond_1
    iget-object v0, p0, LX/ETL;->a:LX/ETN;

    .line 2124707
    iput-boolean v2, v0, LX/ETN;->m:Z

    .line 2124708
    goto :goto_0

    .line 2124709
    :cond_2
    sget-object v1, LX/ETK;->EMPTY:LX/ETK;

    if-eq v0, v1, :cond_3

    .line 2124710
    iget-object v0, p0, LX/ETL;->c:LX/ETA;

    .line 2124711
    iget-object v1, v0, LX/ETA;->a:LX/ETB;

    .line 2124712
    iget-object v3, v1, LX/ETB;->t:LX/ETQ;

    move-object v1, v3

    .line 2124713
    const-string v3, "download-section-id"

    invoke-virtual {v1, v3}, LX/ETQ;->a(Ljava/lang/String;)I

    .line 2124714
    iget-object v1, v0, LX/ETA;->a:LX/ETB;

    invoke-static {v1}, LX/ETB;->u(LX/ETB;)V

    .line 2124715
    :cond_3
    iget-object v0, p0, LX/ETL;->b:LX/ETK;

    sget-object v1, LX/ETK;->EMPTY:LX/ETK;

    if-eq v0, v1, :cond_4

    .line 2124716
    iget-object v0, p0, LX/ETL;->c:LX/ETA;

    .line 2124717
    iget-object v1, v0, LX/ETA;->a:LX/ETB;

    invoke-virtual {v1}, LX/ETB;->k()V

    .line 2124718
    :cond_4
    iget-object v0, p0, LX/ETL;->a:LX/ETN;

    .line 2124719
    iput-boolean v2, v0, LX/ETN;->m:Z

    .line 2124720
    goto :goto_0

    .line 2124721
    :cond_5
    iget-object v1, p0, LX/ETL;->a:LX/ETN;

    iget-object v1, v1, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->s()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2124722
    sget-object v1, LX/ETK;->SINGLE:LX/ETK;

    goto :goto_1

    .line 2124723
    :cond_6
    sget-object v1, LX/ETK;->MULTIPLE:LX/ETK;

    goto :goto_1
.end method
