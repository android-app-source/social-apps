.class public LX/EGK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2097125
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "messages/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2097126
    sput-object v0, LX/EGK;->a:LX/0Tn;

    const-string v1, "notifications/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2097127
    sput-object v0, LX/EGK;->b:LX/0Tn;

    const-string v1, "voip_vibrate_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->c:LX/0Tn;

    .line 2097128
    sget-object v0, LX/EGK;->b:LX/0Tn;

    const-string v1, "voip_ringtone_uri"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->d:LX/0Tn;

    .line 2097129
    sget-object v0, LX/EGK;->a:LX/0Tn;

    const-string v1, "rtc_ringtone_level"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->e:LX/0Tn;

    .line 2097130
    sget-object v0, LX/EGK;->a:LX/0Tn;

    const-string v1, "rtc_instant_ringtone_level"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->f:LX/0Tn;

    .line 2097131
    sget-object v0, LX/EGK;->a:LX/0Tn;

    const-string v1, "rtc_one_on_one_over_multiway"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->g:LX/0Tn;

    .line 2097132
    sget-object v0, LX/EGK;->a:LX/0Tn;

    const-string v1, "rtc_instant_button_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->h:LX/0Tn;

    .line 2097133
    sget-object v0, LX/EGK;->b:LX/0Tn;

    const-string v1, "threads/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->i:LX/0Tn;

    .line 2097134
    sget-object v0, LX/EGK;->a:LX/0Tn;

    const-string v1, "rtc_pstn_call_log_matched_user_ids"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->j:LX/0Tn;

    .line 2097135
    sget-object v0, LX/EGK;->a:LX/0Tn;

    const-string v1, "rtc_call_log_last_fetch_user"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->k:LX/0Tn;

    .line 2097136
    sget-object v0, LX/EGK;->a:LX/0Tn;

    const-string v1, "rtc_call_log_last_fetch_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->l:LX/0Tn;

    .line 2097137
    sget-object v0, LX/EGK;->i:LX/0Tn;

    const-string v1, "custom/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/EGK;->m:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2097138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Long;)LX/0Tn;
    .locals 2

    .prologue
    .line 2097139
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2097140
    sget-object v1, LX/EGK;->i:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "/thread_voip_vibrate_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
