.class public final LX/Cli;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:I

.field public final h:I

.field public final i:Z

.field public final j:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field public final k:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

.field public final l:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

.field private final m:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/facebook/video/engine/VideoPlayerParams;IILjava/lang/String;Ljava/lang/String;IIZLcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V
    .locals 0

    .prologue
    .line 1932903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932904
    iput-object p1, p0, LX/Cli;->a:Ljava/lang/String;

    .line 1932905
    iput-object p2, p0, LX/Cli;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1932906
    iput p3, p0, LX/Cli;->c:I

    .line 1932907
    iput p4, p0, LX/Cli;->d:I

    .line 1932908
    iput-object p5, p0, LX/Cli;->e:Ljava/lang/String;

    .line 1932909
    iput-object p6, p0, LX/Cli;->f:Ljava/lang/String;

    .line 1932910
    iput p7, p0, LX/Cli;->g:I

    .line 1932911
    iput p8, p0, LX/Cli;->h:I

    .line 1932912
    iput-boolean p9, p0, LX/Cli;->i:Z

    .line 1932913
    iput-object p10, p0, LX/Cli;->j:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1932914
    iput-object p11, p0, LX/Cli;->k:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    .line 1932915
    iput-object p12, p0, LX/Cli;->l:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    .line 1932916
    iput-object p13, p0, LX/Cli;->m:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 1932917
    return-void
.end method

.method public static a(LX/8Ys;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;LX/1m0;LX/CoM;LX/03V;Z)LX/Cli;
    .locals 15

    .prologue
    .line 1932918
    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-virtual {v1}, LX/0mC;->b()LX/162;

    move-result-object v3

    .line 1932919
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/162;->g(Ljava/lang/String;)LX/162;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1932920
    :cond_0
    :goto_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-object/from16 v0, p4

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING_WITH_CROSS_FADE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-object/from16 v0, p4

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_1
    move-object v1, p0

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move/from16 v6, p8

    .line 1932921
    invoke-static/range {v1 .. v6}, LX/Cli;->a(LX/8Ys;ZLX/162;LX/1m0;LX/CoM;Z)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v3

    .line 1932922
    invoke-interface {p0}, LX/8Ys;->C()LX/1Fb;

    move-result-object v6

    .line 1932923
    new-instance v1, LX/Cli;

    invoke-interface {p0}, LX/8Ys;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, LX/8Ys;->D()I

    move-result v4

    invoke-interface {p0}, LX/8Ys;->m()I

    move-result v5

    if-nez v6, :cond_3

    const-string v6, ""

    :goto_2
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    invoke-direct/range {v1 .. v14}, LX/Cli;-><init>(Ljava/lang/String;Lcom/facebook/video/engine/VideoPlayerParams;IILjava/lang/String;Ljava/lang/String;IIZLcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V

    return-object v1

    .line 1932924
    :catch_0
    move-exception v1

    .line 1932925
    if-eqz p7, :cond_0

    .line 1932926
    const-string v2, "NativeVideoAd"

    const-string v4, "Error Parsing tracking codes"

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v0, v2, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1932927
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 1932928
    :cond_3
    invoke-interface {v6}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method

.method public static a(LX/Cm4;LX/1m0;LX/CoM;Z)LX/Cli;
    .locals 17

    .prologue
    .line 1932929
    if-eqz p0, :cond_8

    invoke-interface/range {p0 .. p0}, LX/Cm4;->r()LX/8Ys;

    move-result-object v2

    if-eqz v2, :cond_8

    if-eqz p1, :cond_8

    .line 1932930
    invoke-interface/range {p0 .. p0}, LX/Cm4;->r()LX/8Ys;

    move-result-object v7

    .line 1932931
    invoke-interface {v7}, LX/8Ys;->d()Ljava/lang/String;

    move-result-object v3

    .line 1932932
    invoke-interface {v7}, LX/8Ys;->q()Z

    move-result v11

    .line 1932933
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-static {v7, v0, v1}, LX/Cli;->a(LX/8Ys;LX/1m0;Z)Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v8

    .line 1932934
    invoke-interface/range {p0 .. p0}, LX/Cm4;->v()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    if-eq v2, v4, :cond_0

    invoke-interface/range {p0 .. p0}, LX/Cm4;->v()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING_WITH_CROSS_FADE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    if-ne v2, v4, :cond_2

    :cond_0
    const/4 v2, 0x1

    .line 1932935
    :goto_0
    invoke-interface {v7}, LX/8Ys;->v()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    const/4 v6, 0x1

    .line 1932936
    :goto_1
    const/4 v4, 0x0

    .line 1932937
    if-eqz v11, :cond_9

    if-eqz p3, :cond_9

    .line 1932938
    new-instance v4, LX/7DI;

    invoke-direct {v4}, LX/7DI;-><init>()V

    sget-object v5, LX/19o;->CUBEMAP:LX/19o;

    invoke-virtual {v4, v5}, LX/7DI;->a(LX/19o;)LX/7DI;

    move-result-object v4

    invoke-interface {v7}, LX/8Ys;->n()I

    move-result v5

    invoke-virtual {v4, v5}, LX/7DI;->a(I)LX/7DI;

    move-result-object v4

    invoke-interface {v7}, LX/8Ys;->o()I

    move-result v5

    invoke-virtual {v4, v5}, LX/7DI;->b(I)LX/7DI;

    move-result-object v4

    invoke-interface {v7}, LX/8Ys;->p()I

    move-result v5

    invoke-virtual {v4, v5}, LX/7DI;->c(I)LX/7DI;

    move-result-object v4

    invoke-interface {v7}, LX/8Ys;->B()I

    move-result v5

    invoke-virtual {v4, v5}, LX/7DI;->d(I)LX/7DI;

    move-result-object v4

    invoke-interface {v7}, LX/8Ys;->x()D

    move-result-wide v12

    invoke-virtual {v4, v12, v13}, LX/7DI;->a(D)LX/7DI;

    move-result-object v4

    invoke-interface {v7}, LX/8Ys;->w()D

    move-result-wide v12

    invoke-virtual {v4, v12, v13}, LX/7DI;->b(D)LX/7DI;

    move-result-object v4

    invoke-virtual {v4}, LX/7DI;->a()Lcom/facebook/spherical/model/SphericalVideoParams;

    move-result-object v5

    .line 1932939
    invoke-interface {v7}, LX/8Ys;->A()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    :goto_2
    or-int/2addr v4, v6

    move-object/from16 v16, v5

    move v5, v4

    move-object/from16 v4, v16

    .line 1932940
    :goto_3
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/2oH;->a(Ljava/lang/String;)LX/2oH;

    move-result-object v6

    invoke-interface {v7}, LX/8Ys;->r()I

    move-result v8

    invoke-virtual {v6, v8}, LX/2oH;->a(I)LX/2oH;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/2oH;->c(Z)LX/2oH;

    move-result-object v6

    if-nez v5, :cond_5

    if-eqz p2, :cond_5

    invoke-virtual/range {p2 .. p2}, LX/CoM;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_4
    invoke-virtual {v6, v2}, LX/2oH;->d(Z)LX/2oH;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/2oH;->a(Lcom/facebook/spherical/model/SphericalVideoParams;)LX/2oH;

    move-result-object v2

    invoke-virtual {v2}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v4

    .line 1932941
    invoke-interface/range {p0 .. p0}, LX/Cm4;->r()LX/8Ys;

    move-result-object v2

    invoke-interface {v2}, LX/8Ys;->D()I

    move-result v5

    .line 1932942
    invoke-interface/range {p0 .. p0}, LX/Cm4;->r()LX/8Ys;

    move-result-object v2

    invoke-interface {v2}, LX/8Ys;->m()I

    move-result v6

    .line 1932943
    invoke-interface/range {p0 .. p0}, LX/Cm4;->s()LX/8Yr;

    move-result-object v12

    .line 1932944
    invoke-interface {v7}, LX/8Ys;->C()LX/1Fb;

    move-result-object v13

    .line 1932945
    if-eqz v12, :cond_6

    invoke-interface {v12}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    .line 1932946
    :goto_5
    const-string v7, ""

    .line 1932947
    const/4 v8, 0x0

    .line 1932948
    const/4 v9, 0x0

    .line 1932949
    const/4 v10, 0x0

    .line 1932950
    if-eqz v2, :cond_7

    .line 1932951
    invoke-interface {v12}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v7

    .line 1932952
    invoke-interface {v12}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->c()I

    move-result v9

    .line 1932953
    invoke-interface {v12}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->a()I

    move-result v10

    .line 1932954
    invoke-interface {v12}, LX/8Yr;->l()Ljava/lang/String;

    move-result-object v8

    .line 1932955
    :cond_1
    :goto_6
    new-instance v2, LX/Cli;

    invoke-interface/range {p0 .. p0}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v12

    invoke-interface/range {p0 .. p0}, LX/Cm4;->t()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v13

    invoke-interface/range {p0 .. p0}, LX/Cm4;->u()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v14

    invoke-interface/range {p0 .. p0}, LX/Cm4;->v()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v15

    invoke-direct/range {v2 .. v15}, LX/Cli;-><init>(Ljava/lang/String;Lcom/facebook/video/engine/VideoPlayerParams;IILjava/lang/String;Ljava/lang/String;IIZLcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V

    .line 1932956
    :goto_7
    return-object v2

    .line 1932957
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1932958
    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 1932959
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1932960
    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    .line 1932961
    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    .line 1932962
    :cond_7
    if-eqz v13, :cond_1

    .line 1932963
    invoke-interface {v13}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v7

    .line 1932964
    invoke-interface {v13}, LX/1Fb;->c()I

    move-result v9

    .line 1932965
    invoke-interface {v13}, LX/1Fb;->a()I

    move-result v10

    goto :goto_6

    .line 1932966
    :cond_8
    const/4 v2, 0x0

    goto :goto_7

    :cond_9
    move v5, v6

    goto/16 :goto_3
.end method

.method public static a(Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;LX/1m0;LX/CoM;LX/03V;Z)LX/Cli;
    .locals 16

    .prologue
    .line 1932967
    if-eqz p0, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->y()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBVideoModel;

    move-result-object v1

    if-eqz v1, :cond_4

    if-eqz p1, :cond_4

    .line 1932968
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->y()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBVideoModel;

    move-result-object v1

    .line 1932969
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->b()LX/162;

    move-result-object v3

    .line 1932970
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/162;->g(Ljava/lang/String;)LX/162;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1932971
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->B()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    if-eq v2, v4, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->B()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->LOOPING_WITH_CROSS_FADE:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    if-ne v2, v4, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_1
    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p4

    .line 1932972
    invoke-static/range {v1 .. v6}, LX/Cli;->a(LX/8Ys;ZLX/162;LX/1m0;LX/CoM;Z)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v3

    .line 1932973
    invoke-interface {v1}, LX/8Ys;->C()LX/1Fb;

    move-result-object v6

    .line 1932974
    new-instance v15, LX/Cli;

    invoke-interface {v1}, LX/8Ys;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, LX/8Ys;->D()I

    move-result v4

    invoke-interface {v1}, LX/8Ys;->m()I

    move-result v5

    if-nez v6, :cond_3

    const-string v6, ""

    :goto_2
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->z()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->A()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->B()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v14

    move-object v1, v15

    invoke-direct/range {v1 .. v14}, LX/Cli;-><init>(Ljava/lang/String;Lcom/facebook/video/engine/VideoPlayerParams;IILjava/lang/String;Ljava/lang/String;IIZLcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V

    move-object v1, v15

    .line 1932975
    :goto_3
    return-object v1

    .line 1932976
    :catch_0
    move-exception v2

    .line 1932977
    if-eqz p3, :cond_0

    .line 1932978
    const-string v4, "NativeVideoAd"

    const-string v5, "Error Parsing tracking codes"

    invoke-virtual {v2}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1932979
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 1932980
    :cond_3
    invoke-interface {v6}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 1932981
    :cond_4
    const/4 v1, 0x0

    goto :goto_3
.end method

.method private static a(LX/8Ys;LX/1m0;Z)Lcom/facebook/video/engine/VideoDataSource;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1932982
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1932983
    :cond_0
    :goto_0
    return-object v2

    .line 1932984
    :cond_1
    invoke-interface {p0}, LX/8Ys;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_3

    move v0, v1

    .line 1932985
    :goto_1
    invoke-interface {p0}, LX/8Ys;->d()Ljava/lang/String;

    move-result-object v8

    .line 1932986
    if-eqz v0, :cond_4

    invoke-interface {p0}, LX/8Ys;->z()Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    .line 1932987
    :goto_2
    if-eqz v0, :cond_5

    invoke-interface {p0}, LX/8Ys;->y()Ljava/lang/String;

    move-result-object v3

    move-object v6, v3

    .line 1932988
    :goto_3
    if-eqz v0, :cond_6

    invoke-interface {p0}, LX/8Ys;->y()Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    .line 1932989
    :goto_4
    if-eqz v0, :cond_7

    invoke-interface {p0}, LX/8Ys;->A()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1932990
    :goto_5
    if-eqz v7, :cond_8

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1932991
    :goto_6
    invoke-virtual {p1, v0, v8, v1}, LX/1m0;->a(Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v3

    .line 1932992
    if-nez v3, :cond_2

    move-object v3, v0

    .line 1932993
    :cond_2
    if-nez v6, :cond_9

    move-object v0, v2

    .line 1932994
    :goto_7
    if-nez v5, :cond_a

    .line 1932995
    :goto_8
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v1

    .line 1932996
    iput-object v3, v1, LX/2oE;->a:Landroid/net/Uri;

    .line 1932997
    move-object v1, v1

    .line 1932998
    iput-object v0, v1, LX/2oE;->b:Landroid/net/Uri;

    .line 1932999
    move-object v0, v1

    .line 1933000
    iput-object v2, v0, LX/2oE;->c:Landroid/net/Uri;

    .line 1933001
    move-object v0, v0

    .line 1933002
    iput-object v4, v0, LX/2oE;->d:Ljava/lang/String;

    .line 1933003
    move-object v0, v0

    .line 1933004
    sget-object v1, LX/097;->FROM_STREAM:LX/097;

    .line 1933005
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1933006
    move-object v0, v0

    .line 1933007
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v2

    goto :goto_0

    .line 1933008
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1933009
    :cond_4
    invoke-interface {p0}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    goto :goto_2

    .line 1933010
    :cond_5
    invoke-interface {p0}, LX/8Ys;->t()Ljava/lang/String;

    move-result-object v3

    move-object v6, v3

    goto :goto_3

    .line 1933011
    :cond_6
    invoke-interface {p0}, LX/8Ys;->u()Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    goto :goto_4

    .line 1933012
    :cond_7
    invoke-interface {p0}, LX/8Ys;->v()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_5

    :cond_8
    move-object v0, v2

    .line 1933013
    goto :goto_6

    .line 1933014
    :cond_9
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v8, v1}, LX/1m0;->a(Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    goto :goto_7

    .line 1933015
    :cond_a
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1, v2, v8, v1}, LX/1m0;->a(Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v2

    goto :goto_8
.end method

.method private static a(LX/8Ys;ZLX/162;LX/1m0;LX/CoM;Z)Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1933016
    invoke-interface {p0}, LX/8Ys;->d()Ljava/lang/String;

    move-result-object v3

    .line 1933017
    invoke-static {p0, p3, p5}, LX/Cli;->a(LX/8Ys;LX/1m0;Z)Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v4

    .line 1933018
    invoke-interface {p0}, LX/8Ys;->v()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p5, :cond_2

    invoke-interface {p0}, LX/8Ys;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, LX/8Ys;->A()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 1933019
    :goto_0
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v4

    .line 1933020
    iput-object v3, v4, LX/2oH;->b:Ljava/lang/String;

    .line 1933021
    move-object v3, v4

    .line 1933022
    invoke-interface {p0}, LX/8Ys;->r()I

    move-result v4

    .line 1933023
    iput v4, v3, LX/2oH;->c:I

    .line 1933024
    move-object v3, v3

    .line 1933025
    iput-boolean p1, v3, LX/2oH;->g:Z

    .line 1933026
    move-object v3, v3

    .line 1933027
    if-nez v0, :cond_1

    if-eqz p4, :cond_1

    invoke-virtual {p4}, LX/CoM;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    .line 1933028
    :cond_1
    iput-boolean v1, v3, LX/2oH;->n:Z

    .line 1933029
    move-object v0, v3

    .line 1933030
    iput-object p2, v0, LX/2oH;->e:LX/162;

    .line 1933031
    move-object v0, v0

    .line 1933032
    iput-boolean v2, v0, LX/2oH;->f:Z

    .line 1933033
    move-object v0, v0

    .line 1933034
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1933035
    return-object v0

    :cond_2
    move v0, v1

    .line 1933036
    goto :goto_0
.end method
