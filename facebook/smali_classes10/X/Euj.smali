.class public final enum LX/Euj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Euj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Euj;

.field public static final enum DELETE_ALL_ACTION_CANCELLED:LX/Euj;

.field public static final enum DELETE_ALL_ACTION_TAKEN:LX/Euj;

.field public static final enum DELETE_ALL_CLICKED:LX/Euj;

.field public static final enum DELETE_ALL_SHOWN:LX/Euj;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2179932
    new-instance v0, LX/Euj;

    const-string v1, "DELETE_ALL_SHOWN"

    const-string v2, "delete_all_shown"

    invoke-direct {v0, v1, v3, v2}, LX/Euj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Euj;->DELETE_ALL_SHOWN:LX/Euj;

    .line 2179933
    new-instance v0, LX/Euj;

    const-string v1, "DELETE_ALL_CLICKED"

    const-string v2, "delete_all_clicked"

    invoke-direct {v0, v1, v4, v2}, LX/Euj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Euj;->DELETE_ALL_CLICKED:LX/Euj;

    .line 2179934
    new-instance v0, LX/Euj;

    const-string v1, "DELETE_ALL_ACTION_TAKEN"

    const-string v2, "delete_all_action_taken"

    invoke-direct {v0, v1, v5, v2}, LX/Euj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Euj;->DELETE_ALL_ACTION_TAKEN:LX/Euj;

    .line 2179935
    new-instance v0, LX/Euj;

    const-string v1, "DELETE_ALL_ACTION_CANCELLED"

    const-string v2, "delete_all_action_cancelled"

    invoke-direct {v0, v1, v6, v2}, LX/Euj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Euj;->DELETE_ALL_ACTION_CANCELLED:LX/Euj;

    .line 2179936
    const/4 v0, 0x4

    new-array v0, v0, [LX/Euj;

    sget-object v1, LX/Euj;->DELETE_ALL_SHOWN:LX/Euj;

    aput-object v1, v0, v3

    sget-object v1, LX/Euj;->DELETE_ALL_CLICKED:LX/Euj;

    aput-object v1, v0, v4

    sget-object v1, LX/Euj;->DELETE_ALL_ACTION_TAKEN:LX/Euj;

    aput-object v1, v0, v5

    sget-object v1, LX/Euj;->DELETE_ALL_ACTION_CANCELLED:LX/Euj;

    aput-object v1, v0, v6

    sput-object v0, LX/Euj;->$VALUES:[LX/Euj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2179937
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2179938
    iput-object p3, p0, LX/Euj;->eventName:Ljava/lang/String;

    .line 2179939
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Euj;
    .locals 1

    .prologue
    .line 2179940
    const-class v0, LX/Euj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Euj;

    return-object v0
.end method

.method public static values()[LX/Euj;
    .locals 1

    .prologue
    .line 2179941
    sget-object v0, LX/Euj;->$VALUES:[LX/Euj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Euj;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2179942
    iget-object v0, p0, LX/Euj;->eventName:Ljava/lang/String;

    return-object v0
.end method
