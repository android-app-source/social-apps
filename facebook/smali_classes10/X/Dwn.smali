.class public final LX/Dwn;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 0

    .prologue
    .line 2061342
    iput-object p1, p0, LX/Dwn;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2061343
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2061344
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    .line 2061345
    iget-object v0, p0, LX/Dwn;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2061346
    :cond_0
    :goto_0
    return-void

    .line 2061347
    :cond_1
    iget-object v0, p0, LX/Dwn;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    .line 2061348
    iget-object v1, v0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    move-object v0, v1

    .line 2061349
    invoke-static {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->e(Lcom/facebook/graphql/model/GraphQLAlbum;)I

    move-result v1

    const v2, 0x403827a

    if-ne v1, v2, :cond_3

    .line 2061350
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2061351
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    .line 2061352
    :goto_1
    move-object v0, v1

    .line 2061353
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2061354
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 2061355
    iget-wide v4, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v2, v4

    .line 2061356
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 2061357
    :cond_2
    iget-object v0, p0, LX/Dwn;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    invoke-virtual {v0}, LX/Dvb;->c()V

    goto :goto_0

    .line 2061358
    :cond_3
    iget-object v0, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v0, v0

    .line 2061359
    iget-wide v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->g:J

    move-wide v0, v4

    .line 2061360
    iget-object v2, p0, LX/Dwn;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v2, v2, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
