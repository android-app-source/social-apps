.class public LX/DoY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/DoY;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2Oy;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dom;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2Oz;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2041986
    const-class v0, LX/DoY;

    sput-object v0, LX/DoY;->b:Ljava/lang/Class;

    .line 2041987
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/2P5;->a:LX/0U1;

    .line 2041988
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2041989
    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/2P5;->g:LX/0U1;

    .line 2041990
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2041991
    aput-object v2, v0, v1

    sput-object v0, LX/DoY;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2Oy;LX/0Or;LX/2Oz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;",
            "LX/2Oy;",
            "LX/0Or",
            "<",
            "LX/Dom;",
            ">;",
            "LX/2Oz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2041980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2041981
    iput-object p1, p0, LX/DoY;->c:LX/0Or;

    .line 2041982
    iput-object p2, p0, LX/DoY;->d:LX/2Oy;

    .line 2041983
    iput-object p3, p0, LX/DoY;->e:LX/0Or;

    .line 2041984
    iput-object p4, p0, LX/DoY;->f:LX/2Oz;

    .line 2041985
    return-void
.end method

.method public static a(LX/0QB;)LX/DoY;
    .locals 7

    .prologue
    .line 2041967
    sget-object v0, LX/DoY;->g:LX/DoY;

    if-nez v0, :cond_1

    .line 2041968
    const-class v1, LX/DoY;

    monitor-enter v1

    .line 2041969
    :try_start_0
    sget-object v0, LX/DoY;->g:LX/DoY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2041970
    if-eqz v2, :cond_0

    .line 2041971
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2041972
    new-instance v5, LX/DoY;

    const/16 v3, 0xdc6

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/2Oy;->a(LX/0QB;)LX/2Oy;

    move-result-object v3

    check-cast v3, LX/2Oy;

    const/16 v4, 0x2a17

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2Oz;->b(LX/0QB;)LX/2Oz;

    move-result-object v4

    check-cast v4, LX/2Oz;

    invoke-direct {v5, v6, v3, p0, v4}, LX/DoY;-><init>(LX/0Or;LX/2Oy;LX/0Or;LX/2Oz;)V

    .line 2041973
    move-object v0, v5

    .line 2041974
    sput-object v0, LX/DoY;->g:LX/DoY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2041975
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2041976
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2041977
    :cond_1
    sget-object v0, LX/DoY;->g:LX/DoY;

    return-object v0

    .line 2041978
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2041979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/DoY;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2041908
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2041909
    iget-object v0, p0, LX/DoY;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 2041910
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2041911
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v3, LX/3GM;->b:LX/0U1;

    .line 2041912
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2041913
    aput-object v3, v2, v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 2041914
    :goto_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2041915
    sget-object v0, LX/3GM;->b:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->f(Landroid/database/Cursor;)[B

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    .line 2041916
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2041917
    :catchall_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v1, :cond_5

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_0
    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2041918
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2041919
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_1

    if-eqz v9, :cond_6

    :try_start_6
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    :goto_4
    throw v0

    .line 2041920
    :cond_2
    if-eqz v2, :cond_3

    :try_start_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2041921
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 2041922
    :cond_4
    new-instance v0, LX/DoW;

    invoke-direct {v0, p0}, LX/DoW;-><init>(LX/DoY;)V

    invoke-static {v10, v0}, LX/0PN;->a(Ljava/util/Collection;LX/0QK;)Ljava/util/Collection;

    move-result-object v0

    .line 2041923
    new-instance v1, LX/DoX;

    invoke-direct {v1, p0}, LX/DoX;-><init>(LX/DoY;)V

    invoke-static {v0, v1}, LX/0PN;->a(Ljava/util/Collection;LX/0Rl;)Ljava/util/Collection;

    move-result-object v0

    .line 2041924
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0

    .line 2041925
    :catch_2
    move-exception v2

    :try_start_8
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2041926
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 2041927
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 2041928
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 2041929
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method

.method public static a$redex0(LX/DoY;[B)[B
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2041992
    if-nez p1, :cond_0

    .line 2041993
    const/4 v0, 0x0

    .line 2041994
    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, LX/DoY;->d:LX/2Oy;

    invoke-virtual {v0, p1}, LX/2Oy;->b([B)[B
    :try_end_0
    .catch LX/48g; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 2041995
    :catch_0
    move-exception v0

    .line 2041996
    :goto_1
    sget-object v1, LX/DoY;->b:Ljava/lang/Class;

    const-string v2, "Failed to decrypt pre-key"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2041997
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2041998
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method private b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)[B
    .locals 13
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 2041930
    iget-object v1, p0, LX/DoY;->f:LX/2Oz;

    .line 2041931
    invoke-virtual {v1}, LX/2Oz;->b()V

    .line 2041932
    move-object v11, v1

    .line 2041933
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, LX/Dpq;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2041934
    sget-object v2, LX/2P5;->a:LX/0U1;

    .line 2041935
    iget-object v0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v0

    .line 2041936
    invoke-static {v2, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v5

    .line 2041937
    iget-object v1, p0, LX/DoY;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/2gJ;

    move-object v9, v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 2041938
    :try_start_1
    invoke-virtual {v9}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2041939
    const-string v0, "BEGIN DEFERRED TRANSACTION"

    const v2, 0x7b17dc9f

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x44540dff

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    .line 2041940
    :try_start_2
    const-string v2, "threads"

    sget-object v3, LX/DoY;->a:[Ljava/lang/String;

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    .line 2041941
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2041942
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2041943
    sget-object v2, LX/2P5;->g:LX/0U1;

    invoke-virtual {v2, v4}, LX/0U1;->f(Landroid/database/Cursor;)[B
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    move-result-object v2

    .line 2041944
    if-eqz v4, :cond_0

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2041945
    :cond_0
    const v3, -0x119d5cd5

    :try_start_5
    invoke-static {v1, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 2041946
    if-eqz v9, :cond_1

    :try_start_6
    invoke-virtual {v9}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 2041947
    :cond_1
    if-eqz v11, :cond_2

    invoke-virtual {v11}, LX/2Oz;->close()V

    :cond_2
    move-object v1, v2

    :goto_0
    return-object v1

    .line 2041948
    :cond_3
    if-eqz v4, :cond_4

    :try_start_7
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 2041949
    :cond_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2041950
    const v2, -0x51768723

    :try_start_8
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    .line 2041951
    if-eqz v9, :cond_5

    :try_start_9
    invoke-virtual {v9}, LX/2gJ;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 2041952
    :cond_5
    if-eqz v11, :cond_6

    invoke-virtual {v11}, LX/2Oz;->close()V

    :cond_6
    move-object v1, v10

    goto :goto_0

    .line 2041953
    :catch_0
    move-exception v2

    :try_start_a
    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 2041954
    :catchall_0
    move-exception v3

    move-object v12, v3

    move-object v3, v2

    move-object v2, v12

    :goto_1
    if-eqz v4, :cond_7

    if-eqz v3, :cond_a

    :try_start_b
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :cond_7
    :goto_2
    :try_start_c
    throw v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 2041955
    :catchall_1
    move-exception v2

    const v3, 0x1c01dd9f

    :try_start_d
    invoke-static {v1, v3}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v2
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 2041956
    :catch_1
    move-exception v1

    :try_start_e
    throw v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 2041957
    :catchall_2
    move-exception v2

    move-object v12, v2

    move-object v2, v1

    move-object v1, v12

    :goto_3
    if-eqz v9, :cond_8

    if-eqz v2, :cond_b

    :try_start_f
    invoke-virtual {v9}, LX/2gJ;->close()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    :cond_8
    :goto_4
    :try_start_10
    throw v1
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    .line 2041958
    :catch_2
    move-exception v1

    :try_start_11
    throw v1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    .line 2041959
    :catchall_3
    move-exception v2

    move-object v10, v1

    move-object v1, v2

    :goto_5
    if-eqz v11, :cond_9

    if-eqz v10, :cond_c

    :try_start_12
    invoke-virtual {v11}, LX/2Oz;->close()V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_5

    :cond_9
    :goto_6
    throw v1

    .line 2041960
    :catch_3
    move-exception v4

    :try_start_13
    invoke-static {v3, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_a
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto :goto_2

    .line 2041961
    :catch_4
    move-exception v3

    :try_start_14
    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 2041962
    :catchall_4
    move-exception v1

    goto :goto_5

    .line 2041963
    :cond_b
    invoke-virtual {v9}, LX/2gJ;->close()V
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_2
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    goto :goto_4

    .line 2041964
    :catch_5
    move-exception v2

    invoke-static {v10, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_c
    invoke-virtual {v11}, LX/2Oz;->close()V

    goto :goto_6

    .line 2041965
    :catchall_5
    move-exception v1

    move-object v2, v10

    goto :goto_3

    .line 2041966
    :catchall_6
    move-exception v2

    move-object v3, v10

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2041895
    iget-object v0, p0, LX/DoY;->f:LX/2Oz;

    .line 2041896
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 2041897
    move-object v2, v0

    .line 2041898
    const/4 v1, 0x0

    .line 2041899
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/DoY;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)[B

    move-result-object v0

    .line 2041900
    if-nez v0, :cond_3

    .line 2041901
    const/4 v3, 0x0

    .line 2041902
    :goto_0
    move-object v0, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2041903
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    :cond_0
    return-object v0

    .line 2041904
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2041905
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1

    .line 2041906
    :cond_3
    iget-object v3, p0, LX/DoY;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Dom;

    invoke-virtual {v3, p1}, LX/Dom;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B

    move-result-object v3

    .line 2041907
    iget-object p2, p0, LX/DoY;->d:LX/2Oy;

    invoke-virtual {p2, v3, v0}, LX/2Oy;->c([B[B)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2041888
    iget-object v0, p0, LX/DoY;->f:LX/2Oz;

    .line 2041889
    invoke-virtual {v0}, LX/2Oz;->b()V

    .line 2041890
    move-object v2, v0

    .line 2041891
    const/4 v0, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-static {p0, p1, v0, v3}, LX/DoY;->a(LX/DoY;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2041892
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oz;->close()V

    :cond_0
    return-object v0

    .line 2041893
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2041894
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v2}, LX/2Oz;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_0
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, LX/2Oz;->close()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)[B
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 2041855
    iget-object v1, p0, LX/DoY;->f:LX/2Oz;

    .line 2041856
    invoke-virtual {v1}, LX/2Oz;->b()V

    .line 2041857
    move-object v11, v1

    .line 2041858
    :try_start_0
    sget-object v1, LX/3GM;->a:LX/0U1;

    .line 2041859
    iget-object v0, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v0

    .line 2041860
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v2

    .line 2041861
    iget-object v1, p0, LX/DoY;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/2gJ;

    move-object v9, v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 2041862
    :try_start_1
    invoke-virtual {v9}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2041863
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, LX/3GM;->b:LX/0U1;

    .line 2041864
    iget-object v0, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v0

    .line 2041865
    aput-object v5, v3, v4

    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, p1

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v3

    .line 2041866
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2041867
    sget-object v1, LX/3GM;->b:LX/0U1;

    invoke-virtual {v1, v3}, LX/0U1;->f(Landroid/database/Cursor;)[B
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    move-result-object v1

    .line 2041868
    :goto_0
    if-eqz v3, :cond_0

    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2041869
    :cond_0
    if-eqz v9, :cond_1

    :try_start_4
    invoke-virtual {v9}, LX/2gJ;->close()V

    .line 2041870
    :cond_1
    if-nez v1, :cond_9

    .line 2041871
    sget-object v1, LX/DoY;->b:Ljava/lang/Class;

    const-string v2, "Could not retrieve pre key %d from %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2041872
    if-eqz v11, :cond_2

    invoke-virtual {v11}, LX/2Oz;->close()V

    :cond_2
    move-object v1, v10

    :cond_3
    :goto_1
    return-object v1

    .line 2041873
    :catch_0
    move-exception v1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2041874
    :catchall_0
    move-exception v2

    move-object v12, v2

    move-object v2, v1

    move-object v1, v12

    :goto_2
    if-eqz v3, :cond_4

    if-eqz v2, :cond_7

    :try_start_6
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :cond_4
    :goto_3
    :try_start_7
    throw v1
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 2041875
    :catch_1
    move-exception v1

    :try_start_8
    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2041876
    :catchall_1
    move-exception v2

    move-object v12, v2

    move-object v2, v1

    move-object v1, v12

    :goto_4
    if-eqz v9, :cond_5

    if-eqz v2, :cond_8

    :try_start_9
    invoke-virtual {v9}, LX/2gJ;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :cond_5
    :goto_5
    :try_start_a
    throw v1
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 2041877
    :catch_2
    move-exception v1

    :try_start_b
    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 2041878
    :catchall_2
    move-exception v2

    move-object v10, v1

    move-object v1, v2

    :goto_6
    if-eqz v11, :cond_6

    if-eqz v10, :cond_a

    :try_start_c
    invoke-virtual {v11}, LX/2Oz;->close()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_5

    :cond_6
    :goto_7
    throw v1

    .line 2041879
    :catch_3
    move-exception v3

    :try_start_d
    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 2041880
    :catchall_3
    move-exception v1

    move-object v2, v10

    goto :goto_4

    .line 2041881
    :cond_7
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_3

    .line 2041882
    :catch_4
    move-exception v3

    :try_start_e
    invoke-static {v2, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 2041883
    :catchall_4
    move-exception v1

    goto :goto_6

    .line 2041884
    :cond_8
    invoke-virtual {v9}, LX/2gJ;->close()V

    goto :goto_5

    .line 2041885
    :cond_9
    invoke-static {p0, v1}, LX/DoY;->a$redex0(LX/DoY;[B)[B
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    move-result-object v1

    .line 2041886
    if-eqz v11, :cond_3

    invoke-virtual {v11}, LX/2Oz;->close()V

    goto :goto_1

    :catch_5
    move-exception v2

    invoke-static {v10, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_7

    :cond_a
    invoke-virtual {v11}, LX/2Oz;->close()V

    goto :goto_7

    .line 2041887
    :catchall_5
    move-exception v1

    move-object v2, v10

    goto :goto_2

    :cond_b
    move-object v1, v10

    goto :goto_0
.end method
