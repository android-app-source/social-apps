.class public final LX/DEN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Qt;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

.field public final synthetic e:LX/0tX;

.field public final synthetic f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field public final synthetic g:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/1Qt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;LX/0tX;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 1976923
    iput-object p1, p0, LX/DEN;->a:LX/1Qt;

    iput-object p2, p0, LX/DEN;->b:Ljava/lang/String;

    iput-object p3, p0, LX/DEN;->c:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    iput-object p4, p0, LX/DEN;->d:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    iput-object p5, p0, LX/DEN;->e:LX/0tX;

    iput-object p6, p0, LX/DEN;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iput-object p7, p0, LX/DEN;->g:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x1511dfba

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1976924
    iget-object v0, p0, LX/DEN;->a:LX/1Qt;

    sget-object v1, LX/1Qt;->GROUPS:LX/1Qt;

    if-ne v0, v1, :cond_0

    const-string v0, "GROUPS_YOU_SHOULD_CREATE_GROUPS_MALL"

    :goto_0
    iget-object v1, p0, LX/DEN;->b:Ljava/lang/String;

    iget-object v2, p0, LX/DEN;->c:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    iget-object v3, p0, LX/DEN;->d:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    iget-object v4, p0, LX/DEN;->e:LX/0tX;

    iget-object v5, p0, LX/DEN;->f:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v6, p0, LX/DEN;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v0 .. v6}, LX/DES;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;LX/0tX;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/util/concurrent/ExecutorService;)V

    .line 1976925
    const v0, -0x4417e329

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    .line 1976926
    :cond_0
    const-string v0, "GROUPS_YOU_SHOULD_CREATE_NEWS_FEED"

    goto :goto_0
.end method
