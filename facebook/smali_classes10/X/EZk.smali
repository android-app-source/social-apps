.class public LX/EZk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EZc;


# instance fields
.field private a:LX/EZc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2139792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2139793
    :try_start_0
    new-instance v0, Lorg/whispersystems/curve25519/NativeCurve25519Provider;

    invoke-direct {v0}, Lorg/whispersystems/curve25519/NativeCurve25519Provider;-><init>()V

    iput-object v0, p0, LX/EZk;->a:LX/EZc;
    :try_end_0
    .catch LX/EZj; {:try_start_0 .. :try_end_0} :catch_0

    .line 2139794
    :goto_0
    return-void

    .line 2139795
    :catch_0
    new-instance v0, LX/EZi;

    invoke-direct {v0}, LX/EZi;-><init>()V

    iput-object v0, p0, LX/EZk;->a:LX/EZc;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/EZg;)V
    .locals 1

    .prologue
    .line 2139796
    iget-object v0, p0, LX/EZk;->a:LX/EZc;

    invoke-interface {v0, p1}, LX/EZc;->a(LX/EZg;)V

    .line 2139797
    return-void
.end method

.method public final a()[B
    .locals 1

    .prologue
    .line 2139798
    iget-object v0, p0, LX/EZk;->a:LX/EZc;

    invoke-interface {v0}, LX/EZc;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final a(I)[B
    .locals 1

    .prologue
    .line 2139799
    iget-object v0, p0, LX/EZk;->a:LX/EZc;

    invoke-interface {v0, p1}, LX/EZc;->a(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final calculateAgreement([B[B)[B
    .locals 1

    .prologue
    .line 2139800
    iget-object v0, p0, LX/EZk;->a:LX/EZc;

    invoke-interface {v0, p1, p2}, LX/EZc;->calculateAgreement([B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public final calculateSignature([B[B[B)[B
    .locals 1

    .prologue
    .line 2139801
    iget-object v0, p0, LX/EZk;->a:LX/EZc;

    invoke-interface {v0, p1, p2, p3}, LX/EZc;->calculateSignature([B[B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public final generatePublicKey([B)[B
    .locals 1

    .prologue
    .line 2139802
    iget-object v0, p0, LX/EZk;->a:LX/EZc;

    invoke-interface {v0, p1}, LX/EZc;->generatePublicKey([B)[B

    move-result-object v0

    return-object v0
.end method

.method public final verifySignature([B[B[B)Z
    .locals 1

    .prologue
    .line 2139803
    iget-object v0, p0, LX/EZk;->a:LX/EZc;

    invoke-interface {v0, p1, p2, p3}, LX/EZc;->verifySignature([B[B[B)Z

    move-result v0

    return v0
.end method
