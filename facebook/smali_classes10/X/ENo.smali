.class public final LX/ENo;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ENp;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleInterfaces$SearchResultsOpinionModule;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Ps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/ENp;


# direct methods
.method public constructor <init>(LX/ENp;)V
    .locals 1

    .prologue
    .line 2113471
    iput-object p1, p0, LX/ENo;->c:LX/ENp;

    .line 2113472
    move-object v0, p1

    .line 2113473
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2113474
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2113475
    const-string v0, "SearchResultsOpinionSearchModuleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2113476
    if-ne p0, p1, :cond_1

    .line 2113477
    :cond_0
    :goto_0
    return v0

    .line 2113478
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2113479
    goto :goto_0

    .line 2113480
    :cond_3
    check-cast p1, LX/ENo;

    .line 2113481
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2113482
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2113483
    if-eq v2, v3, :cond_0

    .line 2113484
    iget-object v2, p0, LX/ENo;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ENo;->a:LX/CzL;

    iget-object v3, p1, LX/ENo;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2113485
    goto :goto_0

    .line 2113486
    :cond_5
    iget-object v2, p1, LX/ENo;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2113487
    :cond_6
    iget-object v2, p0, LX/ENo;->b:LX/1Ps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/ENo;->b:LX/1Ps;

    iget-object v3, p1, LX/ENo;->b:LX/1Ps;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2113488
    goto :goto_0

    .line 2113489
    :cond_7
    iget-object v2, p1, LX/ENo;->b:LX/1Ps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
