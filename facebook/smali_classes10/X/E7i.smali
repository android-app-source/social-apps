.class public LX/E7i;
.super LX/Cfm;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/E6i;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/8Do;


# direct methods
.method public constructor <init>(LX/0Or;LX/8Do;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/E6i;",
            ">;",
            "LX/8Do;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081936
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->CREATE_OWNED_PAGE_UPSELL:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-direct {p0, v0}, LX/Cfm;-><init>(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V

    .line 2081937
    iput-object p1, p0, LX/E7i;->a:LX/0Or;

    .line 2081938
    iput-object p2, p0, LX/E7i;->b:LX/8Do;

    .line 2081939
    return-void
.end method


# virtual methods
.method public final c()LX/Cfk;
    .locals 5

    .prologue
    .line 2081933
    iget-object v0, p0, LX/E7i;->b:LX/8Do;

    .line 2081934
    iget-object v1, v0, LX/8Do;->a:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/8Dn;->c:S

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    move v0, v1

    .line 2081935
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E7i;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E6i;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
