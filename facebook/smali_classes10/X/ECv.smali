.class public final enum LX/ECv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ECv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ECv;

.field public static final enum COMPLETED:LX/ECv;

.field public static final enum IN_PROGRESS:LX/ECv;

.field public static final enum NOT_STARTED:LX/ECv;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2090608
    new-instance v0, LX/ECv;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, LX/ECv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECv;->NOT_STARTED:LX/ECv;

    .line 2090609
    new-instance v0, LX/ECv;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/ECv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECv;->IN_PROGRESS:LX/ECv;

    .line 2090610
    new-instance v0, LX/ECv;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v4}, LX/ECv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ECv;->COMPLETED:LX/ECv;

    .line 2090611
    const/4 v0, 0x3

    new-array v0, v0, [LX/ECv;

    sget-object v1, LX/ECv;->NOT_STARTED:LX/ECv;

    aput-object v1, v0, v2

    sget-object v1, LX/ECv;->IN_PROGRESS:LX/ECv;

    aput-object v1, v0, v3

    sget-object v1, LX/ECv;->COMPLETED:LX/ECv;

    aput-object v1, v0, v4

    sput-object v0, LX/ECv;->$VALUES:[LX/ECv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2090612
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ECv;
    .locals 1

    .prologue
    .line 2090613
    const-class v0, LX/ECv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ECv;

    return-object v0
.end method

.method public static values()[LX/ECv;
    .locals 1

    .prologue
    .line 2090614
    sget-object v0, LX/ECv;->$VALUES:[LX/ECv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ECv;

    return-object v0
.end method
