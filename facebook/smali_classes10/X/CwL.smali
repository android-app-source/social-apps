.class public LX/CwL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/search/results/protocol/filters/FilterValue;


# direct methods
.method public constructor <init>(LX/CwK;)V
    .locals 1

    .prologue
    .line 1950448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950449
    iget-object v0, p1, LX/CwK;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/CwL;->a:Ljava/lang/String;

    .line 1950450
    iget-object v0, p1, LX/CwK;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/CwL;->b:Ljava/lang/String;

    .line 1950451
    iget-object v0, p1, LX/CwK;->c:Ljava/lang/String;

    iput-object v0, p0, LX/CwL;->c:Ljava/lang/String;

    .line 1950452
    iget-object v0, p1, LX/CwK;->d:Ljava/lang/String;

    iput-object v0, p0, LX/CwL;->d:Ljava/lang/String;

    .line 1950453
    iget-object v0, p1, LX/CwK;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    iput-object v0, p0, LX/CwL;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 1950454
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1950455
    new-instance v0, LX/0lp;

    invoke-direct {v0}, LX/0lp;-><init>()V

    .line 1950456
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 1950457
    :try_start_0
    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v2

    .line 1950458
    invoke-virtual {v2}, LX/0nX;->f()V

    .line 1950459
    const-string v0, "id"

    iget-object v3, p0, LX/CwL;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1950460
    const-string v0, "name"

    iget-object v3, p0, LX/CwL;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1950461
    const-string v0, "text"

    iget-object v3, p0, LX/CwL;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1950462
    const-string v0, "echo_text"

    iget-object v3, p0, LX/CwL;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1950463
    const-string v0, "filter_value"

    invoke-virtual {v2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1950464
    const-string v0, ":"

    invoke-virtual {v2, v0}, LX/0nX;->c(Ljava/lang/String;)V

    .line 1950465
    iget-object v0, p0, LX/CwL;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CwL;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/FilterValue;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, LX/0nX;->c(Ljava/lang/String;)V

    .line 1950466
    invoke-virtual {v2}, LX/0nX;->g()V

    .line 1950467
    invoke-virtual {v2}, LX/0nX;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1950468
    invoke-virtual {v1}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 1950469
    :cond_0
    :try_start_1
    const-string v0, "null"
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1950470
    :catch_0
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
