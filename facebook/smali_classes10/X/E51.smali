.class public final LX/E51;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E52;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/9uh;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public final synthetic f:LX/E52;


# direct methods
.method public constructor <init>(LX/E52;)V
    .locals 1

    .prologue
    .line 2077744
    iput-object p1, p0, LX/E51;->f:LX/E52;

    .line 2077745
    move-object v0, p1

    .line 2077746
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2077747
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2077748
    const-string v0, "ReactionMessageAndBreadcrumbsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2077749
    if-ne p0, p1, :cond_1

    .line 2077750
    :cond_0
    :goto_0
    return v0

    .line 2077751
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2077752
    goto :goto_0

    .line 2077753
    :cond_3
    check-cast p1, LX/E51;

    .line 2077754
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2077755
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2077756
    if-eq v2, v3, :cond_0

    .line 2077757
    iget-object v2, p0, LX/E51;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E51;->a:Ljava/lang/String;

    iget-object v3, p1, LX/E51;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2077758
    goto :goto_0

    .line 2077759
    :cond_5
    iget-object v2, p1, LX/E51;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2077760
    :cond_6
    iget-object v2, p0, LX/E51;->b:LX/0Px;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E51;->b:LX/0Px;

    iget-object v3, p1, LX/E51;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2077761
    goto :goto_0

    .line 2077762
    :cond_8
    iget-object v2, p1, LX/E51;->b:LX/0Px;

    if-nez v2, :cond_7

    .line 2077763
    :cond_9
    iget-object v2, p0, LX/E51;->c:LX/2km;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E51;->c:LX/2km;

    iget-object v3, p1, LX/E51;->c:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2077764
    goto :goto_0

    .line 2077765
    :cond_b
    iget-object v2, p1, LX/E51;->c:LX/2km;

    if-nez v2, :cond_a

    .line 2077766
    :cond_c
    iget-object v2, p0, LX/E51;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E51;->d:Ljava/lang/String;

    iget-object v3, p1, LX/E51;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2077767
    goto :goto_0

    .line 2077768
    :cond_e
    iget-object v2, p1, LX/E51;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2077769
    :cond_f
    iget-object v2, p0, LX/E51;->e:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/E51;->e:Ljava/lang/String;

    iget-object v3, p1, LX/E51;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2077770
    goto :goto_0

    .line 2077771
    :cond_10
    iget-object v2, p1, LX/E51;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
