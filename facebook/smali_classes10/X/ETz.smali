.class public LX/ETz;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/ETy;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125657
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2125658
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/1PY;LX/Bwd;LX/ETX;LX/ETQ;)LX/ETy;
    .locals 29

    .prologue
    .line 2125659
    new-instance v1, LX/ETy;

    invoke-static/range {p0 .. p0}, LX/ETB;->a(LX/0QB;)LX/ETB;

    move-result-object v10

    check-cast v10, LX/ETB;

    invoke-static/range {p0 .. p0}, LX/ETc;->a(LX/0QB;)LX/ETc;

    move-result-object v11

    check-cast v11, LX/ETc;

    invoke-static/range {p0 .. p0}, LX/ETr;->b(LX/0QB;)LX/ETr;

    move-result-object v12

    check-cast v12, LX/ETr;

    invoke-static/range {p0 .. p0}, LX/ETs;->a(LX/0QB;)LX/ETs;

    move-result-object v13

    check-cast v13, LX/ETs;

    invoke-static/range {p0 .. p0}, LX/EU7;->b(LX/0QB;)LX/EU7;

    move-result-object v14

    check-cast v14, LX/EU7;

    invoke-static/range {p0 .. p0}, LX/EU8;->b(LX/0QB;)LX/EU8;

    move-result-object v15

    check-cast v15, LX/EU8;

    const-class v2, LX/2kq;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/2kq;

    const-class v2, LX/EUA;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/EUA;

    invoke-static/range {p0 .. p0}, LX/ESm;->a(LX/0QB;)LX/ESm;

    move-result-object v18

    check-cast v18, LX/ESm;

    invoke-static/range {p0 .. p0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v19

    check-cast v19, LX/2xj;

    const-class v2, LX/BwE;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/BwE;

    const-class v2, LX/EUg;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/EUg;

    const-class v2, LX/ETm;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v22

    check-cast v22, LX/ETm;

    const-class v2, LX/EU6;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/EU6;

    const-class v2, LX/ETq;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/ETq;

    const-class v2, LX/ETo;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/ETo;

    const-class v2, LX/ETu;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/ETu;

    const-class v2, LX/ETw;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/ETw;

    invoke-static/range {p0 .. p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v28

    check-cast v28, LX/0xX;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v28}, LX/ETy;-><init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/1PY;LX/Bwd;LX/ETX;LX/ETQ;LX/ETB;LX/ETc;LX/ETr;LX/ETs;LX/EU7;LX/EU8;LX/2kq;LX/EUA;LX/ESm;LX/2xj;LX/BwE;LX/EUg;LX/ETm;LX/EU6;LX/ETq;LX/ETo;LX/ETu;LX/ETw;LX/0xX;)V

    .line 2125660
    return-object v1
.end method
