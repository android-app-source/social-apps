.class public final LX/E0f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeEditActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/home/HomeEditActivity;)V
    .locals 0

    .prologue
    .line 2068771
    iput-object p1, p0, LX/E0f;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2068772
    iget-object v0, p0, LX/E0f;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, p0, LX/E0f;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeEditActivity;->F:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    .line 2068773
    iget-object v0, p0, LX/E0f;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, p0, LX/E0f;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeEditActivity;->F:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    .line 2068774
    iget-object v0, p0, LX/E0f;->a:Lcom/facebook/places/create/home/HomeEditActivity;

    invoke-virtual {v0}, Lcom/facebook/places/create/home/HomeEditActivity;->o()V

    .line 2068775
    return-void
.end method
