.class public LX/DBE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/events/model/Event;

.field public b:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final c:LX/1Ck;

.field private final d:LX/0tX;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1972512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1972513
    iput-object p1, p0, LX/DBE;->c:LX/1Ck;

    .line 1972514
    iput-object p2, p0, LX/DBE;->d:LX/0tX;

    .line 1972515
    return-void
.end method

.method public static a(LX/0QB;)LX/DBE;
    .locals 1

    .prologue
    .line 1972511
    invoke-static {p0}, LX/DBE;->b(LX/0QB;)LX/DBE;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DBE;
    .locals 3

    .prologue
    .line 1972519
    new-instance v2, LX/DBE;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-direct {v2, v0, v1}, LX/DBE;-><init>(LX/1Ck;LX/0tX;)V

    .line 1972520
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/events/common/ActionMechanism;)V
    .locals 11

    .prologue
    .line 1972521
    iget-object v0, p0, LX/DBE;->a:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_0

    .line 1972522
    :goto_0
    return-void

    .line 1972523
    :cond_0
    new-instance v0, LX/4EQ;

    invoke-direct {v0}, LX/4EQ;-><init>()V

    iget-object v1, p0, LX/DBE;->a:Lcom/facebook/events/model/Event;

    .line 1972524
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1972525
    const-string v2, "event_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972526
    move-object v0, v0

    .line 1972527
    new-instance v1, LX/4EG;

    invoke-direct {v1}, LX/4EG;-><init>()V

    iget-object v2, p0, LX/DBE;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v1

    .line 1972528
    iget-object v2, p0, LX/DBE;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 1972529
    iget-object v3, v2, Lcom/facebook/events/common/EventActionContext;->h:Lcom/facebook/events/common/ActionMechanism;

    move-object v2, v3

    .line 1972530
    if-eqz v2, :cond_1

    .line 1972531
    iget-object v2, p0, LX/DBE;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 1972532
    iget-object v3, v2, Lcom/facebook/events/common/EventActionContext;->h:Lcom/facebook/events/common/ActionMechanism;

    move-object v2, v3

    .line 1972533
    invoke-virtual {v2}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1972534
    :cond_1
    new-instance v2, LX/4EG;

    invoke-direct {v2}, LX/4EG;-><init>()V

    iget-object v3, p0, LX/DBE;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object v2

    .line 1972535
    invoke-virtual {p1}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1972536
    new-instance v3, LX/4EL;

    invoke-direct {v3}, LX/4EL;-><init>()V

    .line 1972537
    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1972538
    move-object v1, v3

    .line 1972539
    const-string v2, "context"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1972540
    move-object v0, v0

    .line 1972541
    new-instance v1, LX/7uG;

    invoke-direct {v1}, LX/7uG;-><init>()V

    move-object v1, v1

    .line 1972542
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7uG;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1972543
    new-instance v1, LX/7uZ;

    invoke-direct {v1}, LX/7uZ;-><init>()V

    iget-object v2, p0, LX/DBE;->a:Lcom/facebook/events/model/Event;

    .line 1972544
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1972545
    iput-object v2, v1, LX/7uZ;->b:Ljava/lang/String;

    .line 1972546
    move-object v1, v1

    .line 1972547
    const/4 v2, 0x0

    .line 1972548
    iput-boolean v2, v1, LX/7uZ;->c:Z

    .line 1972549
    move-object v1, v1

    .line 1972550
    const/4 v2, 0x1

    .line 1972551
    iput-boolean v2, v1, LX/7uZ;->a:Z

    .line 1972552
    move-object v1, v1

    .line 1972553
    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v7, 0x0

    .line 1972554
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 1972555
    iget-object v6, v1, LX/7uZ;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1972556
    const/4 v8, 0x3

    invoke-virtual {v5, v8}, LX/186;->c(I)V

    .line 1972557
    iget-boolean v8, v1, LX/7uZ;->a:Z

    invoke-virtual {v5, v10, v8}, LX/186;->a(IZ)V

    .line 1972558
    invoke-virtual {v5, v9, v6}, LX/186;->b(II)V

    .line 1972559
    const/4 v6, 0x2

    iget-boolean v8, v1, LX/7uZ;->c:Z

    invoke-virtual {v5, v6, v8}, LX/186;->a(IZ)V

    .line 1972560
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 1972561
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 1972562
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1972563
    invoke-virtual {v6, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1972564
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1972565
    new-instance v6, Lcom/facebook/events/graphql/EventsMutationsModels$EventPublishMutationModel$EventModel;

    invoke-direct {v6, v5}, Lcom/facebook/events/graphql/EventsMutationsModels$EventPublishMutationModel$EventModel;-><init>(LX/15i;)V

    .line 1972566
    move-object v1, v6

    .line 1972567
    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    .line 1972568
    iget-object v1, p0, LX/DBE;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1972569
    new-instance v1, LX/DBD;

    invoke-direct {v1, p0}, LX/DBD;-><init>(LX/DBE;)V

    .line 1972570
    iget-object v2, p0, LX/DBE;->c:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "publish_event"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/DBE;->a:Lcom/facebook/events/model/Event;

    .line 1972571
    iget-object v5, v4, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1972572
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 1972516
    iput-object p1, p0, LX/DBE;->a:Lcom/facebook/events/model/Event;

    .line 1972517
    iput-object p2, p0, LX/DBE;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1972518
    return-void
.end method
