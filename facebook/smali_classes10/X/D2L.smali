.class public final LX/D2L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/D2M;


# direct methods
.method public constructor <init>(LX/D2M;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1958583
    iput-object p1, p0, LX/D2L;->b:LX/D2M;

    iput-object p2, p0, LX/D2L;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1958584
    iget-object v0, p0, LX/D2L;->a:Landroid/content/Context;

    const v1, 0x7f080039

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1958585
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1958586
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1958587
    if-eqz p1, :cond_0

    .line 1958588
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1958589
    if-nez v0, :cond_1

    .line 1958590
    :cond_0
    :goto_0
    return-void

    .line 1958591
    :cond_1
    iget-object v1, p0, LX/D2L;->b:LX/D2M;

    iget-object v2, p0, LX/D2L;->a:Landroid/content/Context;

    .line 1958592
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1958593
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 1958594
    iget-object v3, v1, LX/D2M;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/D3w;

    .line 1958595
    sget-object p0, LX/04D;->PROFILE_VIDEO:LX/04D;

    .line 1958596
    invoke-static {v0}, LX/5hK;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object p1

    const/4 v1, 0x1

    invoke-static {v3, p1, v2, p0, v1}, LX/D3w;->a(LX/D3w;Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;Z)V

    .line 1958597
    goto :goto_0
.end method
