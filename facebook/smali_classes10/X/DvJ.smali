.class public LX/DvJ;
.super LX/DvF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DvF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/DvJ;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dux;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dux;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2058200
    invoke-direct {p0}, LX/DvF;-><init>()V

    .line 2058201
    iput-object p1, p0, LX/DvJ;->a:LX/0Ot;

    .line 2058202
    return-void
.end method

.method public static a(LX/0QB;)LX/DvJ;
    .locals 4

    .prologue
    .line 2058203
    sget-object v0, LX/DvJ;->b:LX/DvJ;

    if-nez v0, :cond_1

    .line 2058204
    const-class v1, LX/DvJ;

    monitor-enter v1

    .line 2058205
    :try_start_0
    sget-object v0, LX/DvJ;->b:LX/DvJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2058206
    if-eqz v2, :cond_0

    .line 2058207
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2058208
    new-instance v3, LX/DvJ;

    const/16 p0, 0x2e88

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DvJ;-><init>(LX/0Ot;)V

    .line 2058209
    move-object v0, v3

    .line 2058210
    sput-object v0, LX/DvJ;->b:LX/DvJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2058211
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2058212
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2058213
    :cond_1
    sget-object v0, LX/DvJ;->b:LX/DvJ;

    return-object v0

    .line 2058214
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2058215
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2058216
    check-cast p1, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    check-cast p2, Lcom/facebook/fbservice/service/OperationResult;

    .line 2058217
    iget-boolean v0, p2, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2058218
    if-nez v0, :cond_1

    .line 2058219
    :cond_0
    :goto_0
    return-object p2

    .line 2058220
    :cond_1
    iget-object v0, p0, LX/DvJ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dux;

    invoke-virtual {v0, p1}, LX/Dux;->a(Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;)LX/Dv0;

    move-result-object v1

    .line 2058221
    if-eqz v1, :cond_0

    .line 2058222
    invoke-virtual {p2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;

    .line 2058223
    const/4 p1, 0x0

    .line 2058224
    if-eqz v0, :cond_2

    iget-object v2, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    if-nez v2, :cond_3

    .line 2058225
    :cond_2
    :goto_1
    goto :goto_0

    .line 2058226
    :cond_3
    iget-object v2, v1, LX/Dv0;->d:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v1, LX/Dv0;->d:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2058227
    :cond_4
    iget-object v2, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/Dv0;->d:Ljava/lang/String;

    .line 2058228
    iget-object v2, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v2

    iput-boolean v2, v1, LX/Dv0;->c:Z

    .line 2058229
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p0

    .line 2058230
    iget-object v2, v1, LX/Dv0;->b:LX/0Px;

    if-nez v2, :cond_5

    .line 2058231
    iget-object v2, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    invoke-virtual {p0, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    .line 2058232
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/Dv0;->b:LX/0Px;

    goto :goto_1

    .line 2058233
    :cond_5
    iget-object v2, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-eqz v2, :cond_2

    .line 2058234
    iget-object v2, v1, LX/Dv0;->b:LX/0Px;

    iget-object v3, v1, LX/Dv0;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    .line 2058235
    iget-object v3, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    invoke-virtual {v3, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    .line 2058236
    invoke-virtual {v2}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;->a()LX/Dv3;

    move-result-object v4

    sget-object v5, LX/Dv3;->ALBUM_POST_SECTION:LX/Dv3;

    if-ne v4, v5, :cond_6

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;->a()LX/Dv3;

    move-result-object v4

    sget-object v5, LX/Dv3;->ALBUM_POST_SECTION:LX/Dv3;

    if-ne v4, v5, :cond_6

    move-object v4, v2

    check-cast v4, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;

    move-object v5, v3

    check-cast v5, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;

    invoke-static {v1, v4, v5}, LX/Dv0;->a(LX/Dv0;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2058237
    check-cast v2, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;

    check-cast v3, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;

    invoke-static {v1, v2, v3}, LX/Dv0;->b(LX/Dv0;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;)Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;

    move-result-object v2

    .line 2058238
    iget-object v3, v1, LX/Dv0;->b:LX/0Px;

    iget-object v4, v1, LX/Dv0;->b:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, p1, v4}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    .line 2058239
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    .line 2058240
    iget-object v3, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    const/4 v4, 0x1

    iget-object v5, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    .line 2058241
    :goto_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/Dv0;->b:LX/0Px;

    goto/16 :goto_1

    .line 2058242
    :cond_6
    iget-object v2, v1, LX/Dv0;->b:LX/0Px;

    invoke-virtual {p0, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    .line 2058243
    iget-object v3, v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    goto :goto_2
.end method
