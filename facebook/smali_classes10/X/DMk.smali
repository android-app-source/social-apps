.class public final enum LX/DMk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DMk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DMk;

.field public static final enum CREATE_GROUP_EVENT:LX/DMk;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1990143
    new-instance v0, LX/DMk;

    const-string v1, "CREATE_GROUP_EVENT"

    invoke-direct {v0, v1, v2}, LX/DMk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DMk;->CREATE_GROUP_EVENT:LX/DMk;

    .line 1990144
    const/4 v0, 0x1

    new-array v0, v0, [LX/DMk;

    sget-object v1, LX/DMk;->CREATE_GROUP_EVENT:LX/DMk;

    aput-object v1, v0, v2

    sput-object v0, LX/DMk;->$VALUES:[LX/DMk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1990145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DMk;
    .locals 1

    .prologue
    .line 1990146
    const-class v0, LX/DMk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DMk;

    return-object v0
.end method

.method public static values()[LX/DMk;
    .locals 1

    .prologue
    .line 1990147
    sget-object v0, LX/DMk;->$VALUES:[LX/DMk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DMk;

    return-object v0
.end method
