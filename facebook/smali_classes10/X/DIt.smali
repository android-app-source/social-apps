.class public final LX/DIt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:LX/DIv;


# direct methods
.method public constructor <init>(LX/DIv;)V
    .locals 0

    .prologue
    .line 1984506
    iput-object p1, p0, LX/DIt;->a:LX/DIv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1984507
    iget-object v0, p0, LX/DIt;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->i:LX/03V;

    const-string v1, "ComposerSellController"

    const-string v2, "Couldn\'t complete UserGroupCommercePostToMarketplaceStateQuery."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1984508
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1984509
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1984510
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1984511
    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;

    .line 1984512
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchUserGroupCommercePostToMarketplaceStateGraphQLModels$UserGroupCommercePostToMarketplaceStateQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2

    .line 1984513
    if-nez v0, :cond_1

    .line 1984514
    :cond_0
    :goto_0
    return-void

    .line 1984515
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1984516
    :cond_1
    const v2, -0x1257f5f

    invoke-static {v1, v0, v4, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1984517
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 1984518
    :goto_1
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1984519
    invoke-virtual {v2, v1, v4}, LX/15i;->g(II)I

    move-result v1

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1984520
    if-eqz v1, :cond_2

    .line 1984521
    iget-object v0, p0, LX/DIt;->a:LX/DIv;

    invoke-virtual {v2, v1, v4}, LX/15i;->h(II)Z

    move-result v1

    .line 1984522
    iput-boolean v1, v0, LX/DIv;->m:Z

    .line 1984523
    iget-object v0, p0, LX/DIt;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJT;

    iget-object v1, p0, LX/DIt;->a:LX/DIv;

    iget-boolean v1, v1, LX/DIv;->m:Z

    .line 1984524
    iget-object v2, v0, LX/DJT;->a:LX/DJc;

    .line 1984525
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1984526
    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getMarketplaceCrossPostSettingModel()Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    move-result-object v2

    .line 1984527
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->a()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1984528
    iget-object v2, v0, LX/DJT;->a:LX/DJc;

    iget-object v2, v2, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v2, v1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setShouldCrossPostToMarketPlace(Z)V

    .line 1984529
    :cond_3
    goto :goto_0

    .line 1984530
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 1984531
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
