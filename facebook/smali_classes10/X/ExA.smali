.class public final LX/ExA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2184170
    iput-object p1, p0, LX/ExA;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 2184171
    if-nez p2, :cond_0

    .line 2184172
    iget-object v0, p0, LX/ExA;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->x:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 2184173
    :goto_0
    return-void

    .line 2184174
    :cond_0
    iget-object v0, p0, LX/ExA;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->x:LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 2184175
    add-int v0, p2, p3

    add-int/lit8 v0, v0, 0x3

    if-lt v0, p4, :cond_1

    const/4 v0, 0x1

    .line 2184176
    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ExA;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    invoke-virtual {v0}, LX/Exj;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2184177
    iget-object v0, p0, LX/ExA;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->c(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    .line 2184178
    :cond_0
    return-void

    .line 2184179
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
