.class public final LX/Eqi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7HP;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7HP",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V
    .locals 0

    .prologue
    .line 2171784
    iput-object p1, p0, LX/Eqi;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/7Hi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2171785
    iget-object v0, p1, LX/7Hi;->a:LX/7B6;

    move-object v0, v0

    .line 2171786
    iget-object v0, v0, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2171787
    :goto_0
    return-void

    .line 2171788
    :cond_0
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v0, v0

    .line 2171789
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v0, v1

    .line 2171790
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2171791
    iget-object v1, p0, LX/Eqi;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    .line 2171792
    iget-object v2, v1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2171793
    :cond_1
    iget-object v1, p0, LX/Eqi;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v1, v1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->g:LX/Er0;

    .line 2171794
    iput-object v0, v1, LX/Er0;->e:LX/0Px;

    .line 2171795
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2171796
    goto :goto_0
.end method
