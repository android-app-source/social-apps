.class public final LX/Egp;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Egq;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FBn;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/Egq;


# direct methods
.method public constructor <init>(LX/Egq;)V
    .locals 1

    .prologue
    .line 2157299
    iput-object p1, p0, LX/Egp;->d:LX/Egq;

    .line 2157300
    move-object v0, p1

    .line 2157301
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2157302
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2157303
    if-ne p0, p1, :cond_1

    .line 2157304
    :cond_0
    :goto_0
    return v0

    .line 2157305
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2157306
    goto :goto_0

    .line 2157307
    :cond_3
    check-cast p1, LX/Egp;

    .line 2157308
    iget-object v2, p0, LX/Egp;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Egp;->b:Ljava/lang/String;

    iget-object v3, p1, LX/Egp;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2157309
    goto :goto_0

    .line 2157310
    :cond_5
    iget-object v2, p1, LX/Egp;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2157311
    :cond_6
    iget-object v2, p0, LX/Egp;->c:Ljava/util/List;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Egp;->c:Ljava/util/List;

    iget-object v3, p1, LX/Egp;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2157312
    goto :goto_0

    .line 2157313
    :cond_7
    iget-object v2, p1, LX/Egp;->c:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
