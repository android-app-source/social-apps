.class public LX/EtH;
.super LX/Et9;
.source ""


# instance fields
.field private final c:J

.field private final d:Ljava/util/Timer;

.field private e:Lcom/facebook/feedplugins/goodwill/asynctask/TimeoutAsyncUITask$TimeoutTriggeringTimerTask;


# direct methods
.method public constructor <init>(LX/EtA;J)V
    .locals 2

    .prologue
    .line 2177521
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Et9;-><init>(LX/EtA;Ljava/util/concurrent/Executor;)V

    .line 2177522
    iput-wide p2, p0, LX/EtH;->c:J

    .line 2177523
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LX/EtH;->d:Ljava/util/Timer;

    .line 2177524
    return-void
.end method

.method private declared-synchronized c()Z
    .locals 1

    .prologue
    .line 2177525
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EtH;->e:Lcom/facebook/feedplugins/goodwill/asynctask/TimeoutAsyncUITask$TimeoutTriggeringTimerTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 2177526
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/EtH;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2177527
    :goto_0
    monitor-exit p0

    return-void

    .line 2177528
    :cond_0
    :try_start_1
    new-instance v0, Lcom/facebook/feedplugins/goodwill/asynctask/TimeoutAsyncUITask$TimeoutTriggeringTimerTask;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/goodwill/asynctask/TimeoutAsyncUITask$TimeoutTriggeringTimerTask;-><init>(LX/EtH;)V

    iput-object v0, p0, LX/EtH;->e:Lcom/facebook/feedplugins/goodwill/asynctask/TimeoutAsyncUITask$TimeoutTriggeringTimerTask;

    .line 2177529
    iget-object v0, p0, LX/EtH;->d:Ljava/util/Timer;

    iget-object v1, p0, LX/EtH;->e:Lcom/facebook/feedplugins/goodwill/asynctask/TimeoutAsyncUITask$TimeoutTriggeringTimerTask;

    iget-wide v2, p0, LX/EtH;->c:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2177530
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 2177531
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/EtH;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2177532
    :goto_0
    monitor-exit p0

    return-void

    .line 2177533
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/EtH;->e:Lcom/facebook/feedplugins/goodwill/asynctask/TimeoutAsyncUITask$TimeoutTriggeringTimerTask;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/goodwill/asynctask/TimeoutAsyncUITask$TimeoutTriggeringTimerTask;->cancel()Z

    .line 2177534
    const/4 v0, 0x0

    iput-object v0, p0, LX/EtH;->e:Lcom/facebook/feedplugins/goodwill/asynctask/TimeoutAsyncUITask$TimeoutTriggeringTimerTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2177535
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
