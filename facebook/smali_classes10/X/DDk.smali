.class public final enum LX/DDk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DDk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DDk;

.field public static final enum CROSS_POST:LX/DDk;

.field public static final enum CROSS_POST_GRAY:LX/DDk;

.field public static final enum MARK_AS_AVAILABLE:LX/DDk;

.field public static final enum MARK_AS_SOLD:LX/DDk;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1975962
    new-instance v0, LX/DDk;

    const-string v1, "MARK_AS_SOLD"

    invoke-direct {v0, v1, v2}, LX/DDk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DDk;->MARK_AS_SOLD:LX/DDk;

    .line 1975963
    new-instance v0, LX/DDk;

    const-string v1, "MARK_AS_AVAILABLE"

    invoke-direct {v0, v1, v3}, LX/DDk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DDk;->MARK_AS_AVAILABLE:LX/DDk;

    .line 1975964
    new-instance v0, LX/DDk;

    const-string v1, "CROSS_POST"

    invoke-direct {v0, v1, v4}, LX/DDk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DDk;->CROSS_POST:LX/DDk;

    .line 1975965
    new-instance v0, LX/DDk;

    const-string v1, "CROSS_POST_GRAY"

    invoke-direct {v0, v1, v5}, LX/DDk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DDk;->CROSS_POST_GRAY:LX/DDk;

    .line 1975966
    const/4 v0, 0x4

    new-array v0, v0, [LX/DDk;

    sget-object v1, LX/DDk;->MARK_AS_SOLD:LX/DDk;

    aput-object v1, v0, v2

    sget-object v1, LX/DDk;->MARK_AS_AVAILABLE:LX/DDk;

    aput-object v1, v0, v3

    sget-object v1, LX/DDk;->CROSS_POST:LX/DDk;

    aput-object v1, v0, v4

    sget-object v1, LX/DDk;->CROSS_POST_GRAY:LX/DDk;

    aput-object v1, v0, v5

    sput-object v0, LX/DDk;->$VALUES:[LX/DDk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1975967
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DDk;
    .locals 1

    .prologue
    .line 1975968
    const-class v0, LX/DDk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DDk;

    return-object v0
.end method

.method public static values()[LX/DDk;
    .locals 1

    .prologue
    .line 1975969
    sget-object v0, LX/DDk;->$VALUES:[LX/DDk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DDk;

    return-object v0
.end method
