.class public final LX/DBi;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/13a;


# direct methods
.method public constructor <init>(LX/13a;)V
    .locals 0

    .prologue
    .line 1973145
    iput-object p1, p0, LX/DBi;->a:LX/13a;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 1973147
    sget-object v0, LX/13Z;->DSM_INDICATOR_ENABLED_OFF_STATE:LX/13Z;

    iget-object v1, p0, LX/DBi;->a:LX/13a;

    invoke-static {v1}, LX/13a;->f(LX/13a;)LX/13Z;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1973148
    iget-object v0, p0, LX/DBi;->a:LX/13a;

    iget-object v0, v0, LX/13a;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v0

    .line 1973149
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 1973150
    iget-object v1, p0, LX/DBi;->a:LX/13a;

    iget-object v1, v1, LX/13a;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1973151
    :cond_0
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 0

    .prologue
    .line 1973146
    return-void
.end method
