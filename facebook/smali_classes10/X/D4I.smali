.class public LX/D4I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/3Vx;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1qb;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1qb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1962023
    iput-object p1, p0, LX/D4I;->a:Landroid/content/Context;

    .line 1962024
    iput-object p2, p0, LX/D4I;->b:LX/1qb;

    .line 1962025
    return-void
.end method

.method public static a(LX/0QB;)LX/D4I;
    .locals 5

    .prologue
    .line 1962026
    const-class v1, LX/D4I;

    monitor-enter v1

    .line 1962027
    :try_start_0
    sget-object v0, LX/D4I;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962028
    sput-object v2, LX/D4I;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962029
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962030
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1962031
    new-instance p0, LX/D4I;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v4

    check-cast v4, LX/1qb;

    invoke-direct {p0, v3, v4}, LX/D4I;-><init>(Landroid/content/Context;LX/1qb;)V

    .line 1962032
    move-object v0, p0

    .line 1962033
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962034
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D4I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962035
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962036
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/D4K;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/D4K;"
        }
    .end annotation

    .prologue
    .line 1962009
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1962010
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1962011
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1962012
    invoke-static {v0}, LX/1qb;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v3

    .line 1962013
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1962014
    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1962015
    :cond_0
    invoke-static {v0, v2}, LX/1qb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/text/SpannableStringBuilder;)V

    .line 1962016
    move-object v2, v2

    .line 1962017
    invoke-static {v0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v3

    .line 1962018
    const/4 v1, 0x0

    .line 1962019
    invoke-static {v0}, LX/2v7;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1962020
    iget-object v0, p0, LX/D4I;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LX/2v7;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1962021
    :goto_0
    new-instance v1, LX/D4K;

    invoke-direct {v1, v2, v3, v0}, LX/D4K;-><init>(Landroid/text/Spannable;Landroid/text/Spannable;Landroid/text/Spannable;)V

    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
