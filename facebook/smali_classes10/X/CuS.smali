.class public LX/CuS;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/CsO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 1

    .prologue
    .line 1946655
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1946656
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/CuS;

    invoke-static {v0}, LX/CsO;->a(LX/0QB;)LX/CsO;

    move-result-object v0

    check-cast v0, LX/CsO;

    iput-object v0, p0, LX/CuS;->a:LX/CsO;

    .line 1946657
    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1946658
    invoke-static {p0}, LX/CuS;->k(LX/CuS;)LX/Ctg;

    move-result-object v1

    .line 1946659
    if-eqz v1, :cond_0

    .line 1946660
    invoke-interface {v1}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v1

    .line 1946661
    invoke-virtual {v1}, LX/CqX;->d()LX/Cqv;

    move-result-object v1

    sget-object v2, LX/Cqw;->b:LX/Cqw;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1946662
    :cond_0
    return v0
.end method

.method public static k(LX/CuS;)LX/Ctg;
    .locals 1

    .prologue
    .line 1946663
    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    .line 1946664
    if-eqz v0, :cond_0

    .line 1946665
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->getMediaFrame()LX/Ctg;

    move-result-object v0

    .line 1946666
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Cqw;)V
    .locals 1

    .prologue
    .line 1946667
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, LX/CuS;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1946668
    invoke-static {p0}, LX/CuS;->k(LX/CuS;)LX/Ctg;

    move-result-object v0

    .line 1946669
    if-eqz v0, :cond_0

    .line 1946670
    invoke-interface {v0}, LX/Ctg;->c()V

    .line 1946671
    :cond_0
    return-void
.end method

.method public final a(LX/Crd;)Z
    .locals 3

    .prologue
    .line 1946672
    sget-object v0, LX/Crd;->CLICK_MEDIA:LX/Crd;

    if-ne p1, v0, :cond_0

    .line 1946673
    invoke-direct {p0}, LX/CuS;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1946674
    iget-object v0, p0, LX/CuS;->a:LX/CsO;

    invoke-virtual {v0}, LX/CsO;->a()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1946675
    invoke-static {p0}, LX/CuS;->k(LX/CuS;)LX/Ctg;

    move-result-object v0

    .line 1946676
    if-eqz v0, :cond_2

    .line 1946677
    sget-object v1, LX/Cqw;->b:LX/Cqw;

    invoke-interface {v0, v1}, LX/Ctg;->a(LX/Cqw;)V

    .line 1946678
    iget-object v0, p0, LX/CuS;->a:LX/CsO;

    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CsO;->a(Landroid/view/View;)V

    .line 1946679
    const/4 v0, 0x1

    .line 1946680
    :goto_0
    return v0

    .line 1946681
    :cond_0
    sget-object v0, LX/Crd;->BACK:LX/Crd;

    if-ne p1, v0, :cond_2

    .line 1946682
    const/4 v0, 0x1

    .line 1946683
    invoke-static {p0}, LX/CuS;->k(LX/CuS;)LX/Ctg;

    move-result-object v1

    .line 1946684
    if-eqz v1, :cond_1

    .line 1946685
    invoke-interface {v1}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v1

    .line 1946686
    invoke-virtual {v1}, LX/CqX;->d()LX/Cqv;

    move-result-object v1

    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v2

    if-ne v1, v2, :cond_3

    .line 1946687
    :cond_1
    :goto_1
    move v0, v0

    .line 1946688
    if-nez v0, :cond_2

    .line 1946689
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946690
    sget-object v1, LX/Crd;->CLICK_MEDIA:LX/Crd;

    invoke-interface {v0, v1}, LX/Cre;->a(LX/Crd;)Z

    move-result v0

    goto :goto_0

    .line 1946691
    :cond_2
    invoke-super {p0, p1}, LX/Cts;->a(LX/Crd;)Z

    move-result v0

    goto :goto_0

    .line 1946692
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
