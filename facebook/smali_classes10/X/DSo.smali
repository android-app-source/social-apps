.class public LX/DSo;
.super LX/BWh;
.source ""

# interfaces
.implements Landroid/widget/SectionIndexer;
.implements LX/2ht;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ITEM_ACTION_TYPES:",
        "Ljava/lang/Enum;",
        ">",
        "LX/BWh",
        "<TITEM_ACTION_TYPES;>;",
        "Landroid/widget/SectionIndexer;",
        "LX/2ht;"
    }
.end annotation


# instance fields
.field private final a:LX/DSX;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BWf;LX/DSX;LX/DS5;LX/BWd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BWf;",
            "LX/DSX;",
            "LX/DS5;",
            "LX/BWd",
            "<TITEM_ACTION_TYPES;>;)V"
        }
    .end annotation

    .prologue
    .line 1999885
    invoke-direct {p0, p1, p2, p4, p5}, LX/BWh;-><init>(Landroid/content/Context;LX/BWf;LX/BWg;LX/BWd;)V

    .line 1999886
    iput-object p3, p0, LX/DSo;->a:LX/DSX;

    .line 1999887
    return-void
.end method

.method private b()LX/DS5;
    .locals 1

    .prologue
    .line 1999867
    iget-object v0, p0, LX/BWh;->d:LX/BWg;

    move-object v0, v0

    .line 1999868
    check-cast v0, LX/DS5;

    return-object v0
.end method


# virtual methods
.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 1999876
    iget-object v0, p0, LX/DSo;->a:LX/DSX;

    invoke-interface {v0, p1}, LX/DSX;->v_(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1999877
    const/4 v3, 0x0

    .line 1999878
    :goto_0
    return-object v3

    .line 1999879
    :cond_0
    invoke-virtual {p0, p1}, LX/DSo;->getSectionForPosition(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/DSo;->getPositionForSection(I)I

    move-result v0

    .line 1999880
    invoke-virtual {p0, v0}, LX/DSo;->o_(I)I

    move-result v4

    .line 1999881
    if-nez p2, :cond_1

    .line 1999882
    invoke-virtual {p0, v4, p3}, LX/1Cv;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1999883
    :goto_1
    invoke-virtual {p0, v0}, LX/BWh;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, p0

    move v1, p1

    move-object v5, p3

    .line 1999884
    invoke-virtual/range {v0 .. v5}, LX/1Cv;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    goto :goto_0

    :cond_1
    move-object v3, p2

    goto :goto_1
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1999875
    invoke-direct {p0}, LX/DSo;->b()LX/DS5;

    move-result-object v0

    invoke-interface {v0}, LX/DS5;->b()I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 1999874
    invoke-direct {p0}, LX/DSo;->b()LX/DS5;

    move-result-object v0

    invoke-interface {v0}, LX/DS5;->a()I

    move-result v0

    return v0
.end method

.method public final f(I)Z
    .locals 1

    .prologue
    .line 1999873
    iget-object v0, p0, LX/DSo;->a:LX/DSX;

    invoke-interface {v0, p1}, LX/DSX;->c(I)Z

    move-result v0

    return v0
.end method

.method public final getPositionForSection(I)I
    .locals 1

    .prologue
    .line 1999872
    iget-object v0, p0, LX/DSo;->a:LX/DSX;

    invoke-interface {v0, p1}, LX/DSX;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public final getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 1999871
    iget-object v0, p0, LX/DSo;->a:LX/DSX;

    invoke-interface {v0, p1}, LX/DSX;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1999870
    iget-object v0, p0, LX/DSo;->a:LX/DSX;

    invoke-interface {v0}, LX/DSX;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 1999869
    invoke-virtual {p0, p1}, LX/DSo;->getSectionForPosition(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/DSo;->getPositionForSection(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/DSo;->getItemViewType(I)I

    move-result v0

    return v0
.end method
