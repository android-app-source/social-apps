.class public final enum LX/DKF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DKF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DKF;

.field public static final enum COVER_PHOTO_UPDATED_SUCCESSFULLY:LX/DKF;

.field public static final enum INITIAL:LX/DKF;

.field public static final enum INVITE_MEMBERS:LX/DKF;

.field public static final enum SETTING_AS_COVER:LX/DKF;

.field public static final enum UPLOADING_COVER_PHOTO:LX/DKF;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1986784
    new-instance v0, LX/DKF;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v2}, LX/DKF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DKF;->INITIAL:LX/DKF;

    .line 1986785
    new-instance v0, LX/DKF;

    const-string v1, "INVITE_MEMBERS"

    invoke-direct {v0, v1, v3}, LX/DKF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DKF;->INVITE_MEMBERS:LX/DKF;

    .line 1986786
    new-instance v0, LX/DKF;

    const-string v1, "UPLOADING_COVER_PHOTO"

    invoke-direct {v0, v1, v4}, LX/DKF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DKF;->UPLOADING_COVER_PHOTO:LX/DKF;

    .line 1986787
    new-instance v0, LX/DKF;

    const-string v1, "SETTING_AS_COVER"

    invoke-direct {v0, v1, v5}, LX/DKF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DKF;->SETTING_AS_COVER:LX/DKF;

    .line 1986788
    new-instance v0, LX/DKF;

    const-string v1, "COVER_PHOTO_UPDATED_SUCCESSFULLY"

    invoke-direct {v0, v1, v6}, LX/DKF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DKF;->COVER_PHOTO_UPDATED_SUCCESSFULLY:LX/DKF;

    .line 1986789
    const/4 v0, 0x5

    new-array v0, v0, [LX/DKF;

    sget-object v1, LX/DKF;->INITIAL:LX/DKF;

    aput-object v1, v0, v2

    sget-object v1, LX/DKF;->INVITE_MEMBERS:LX/DKF;

    aput-object v1, v0, v3

    sget-object v1, LX/DKF;->UPLOADING_COVER_PHOTO:LX/DKF;

    aput-object v1, v0, v4

    sget-object v1, LX/DKF;->SETTING_AS_COVER:LX/DKF;

    aput-object v1, v0, v5

    sget-object v1, LX/DKF;->COVER_PHOTO_UPDATED_SUCCESSFULLY:LX/DKF;

    aput-object v1, v0, v6

    sput-object v0, LX/DKF;->$VALUES:[LX/DKF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1986790
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DKF;
    .locals 1

    .prologue
    .line 1986791
    const-class v0, LX/DKF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DKF;

    return-object v0
.end method

.method public static values()[LX/DKF;
    .locals 1

    .prologue
    .line 1986792
    sget-object v0, LX/DKF;->$VALUES:[LX/DKF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DKF;

    return-object v0
.end method
