.class public LX/D49;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2mn;


# direct methods
.method public constructor <init>(LX/2mn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1961871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961872
    iput-object p1, p0, LX/D49;->a:LX/2mn;

    .line 1961873
    return-void
.end method

.method public static a(LX/0QB;)LX/D49;
    .locals 1

    .prologue
    .line 1961874
    invoke-static {p0}, LX/D49;->b(LX/0QB;)LX/D49;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0P2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1961848
    if-nez p0, :cond_0

    move-object v0, v1

    .line 1961849
    :goto_0
    return-object v0

    .line 1961850
    :cond_0
    invoke-static {p0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1961851
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1961852
    goto :goto_0

    .line 1961853
    :cond_1
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1961854
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1961855
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 1961856
    if-nez v2, :cond_2

    move-object v0, v1

    .line 1961857
    goto :goto_0

    .line 1961858
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x4ed245b

    if-eq v3, v4, :cond_4

    :cond_3
    move-object v0, v1

    .line 1961859
    goto :goto_0

    .line 1961860
    :cond_4
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v3, "GraphQLStoryProps"

    invoke-virtual {v1, v3, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v3, "SubtitlesLocalesKey"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->bv()LX/0Px;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v3, "ShowDeleteOptionKey"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->u()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v3, "ShowReportOptionKey"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->z()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    .line 1961861
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1961862
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-static {v0}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-nez v2, :cond_8

    .line 1961863
    :cond_5
    const/4 v2, 0x0

    .line 1961864
    :goto_1
    move-object v2, v2

    .line 1961865
    if-eqz v2, :cond_6

    .line 1961866
    const-string v3, "CoverImageParamsKey"

    invoke-virtual {v1, v3, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1961867
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 1961868
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1961869
    const-string v2, "BlurredCoverImageParamsKey"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    :cond_7
    move-object v0, v1

    .line 1961870
    goto/16 :goto_0

    :cond_8
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/D49;
    .locals 2

    .prologue
    .line 1961840
    new-instance v1, LX/D49;

    invoke-static {p0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v0

    check-cast v0, LX/2mn;

    invoke-direct {v1, v0}, LX/D49;-><init>(LX/2mn;)V

    .line 1961841
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)D
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)D"
        }
    .end annotation

    .prologue
    .line 1961842
    const-wide/16 v0, 0x0

    .line 1961843
    if-eqz p1, :cond_0

    .line 1961844
    invoke-static {p1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1961845
    if-eqz v2, :cond_0

    .line 1961846
    iget-object v0, p0, LX/D49;->a:LX/2mn;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, LX/2mn;->d(Lcom/facebook/feed/rows/core/props/FeedProps;F)D

    move-result-wide v0

    .line 1961847
    :cond_0
    return-wide v0
.end method
