.class public LX/DZ8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DZ7;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DZ8;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2013146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/DZ8;
    .locals 3

    .prologue
    .line 2013152
    sget-object v0, LX/DZ8;->a:LX/DZ8;

    if-nez v0, :cond_1

    .line 2013153
    const-class v1, LX/DZ8;

    monitor-enter v1

    .line 2013154
    :try_start_0
    sget-object v0, LX/DZ8;->a:LX/DZ8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2013155
    if-eqz v2, :cond_0

    .line 2013156
    :try_start_1
    new-instance v0, LX/DZ8;

    invoke-direct {v0}, LX/DZ8;-><init>()V

    .line 2013157
    move-object v0, v0

    .line 2013158
    sput-object v0, LX/DZ8;->a:LX/DZ8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2013159
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2013160
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2013161
    :cond_1
    sget-object v0, LX/DZ8;->a:LX/DZ8;

    return-object v0

    .line 2013162
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2013163
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/FbFragment;LX/DLO;)V
    .locals 3

    .prologue
    .line 2013164
    const-class v0, LX/1ZF;

    invoke-virtual {p1, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2013165
    if-nez v0, :cond_1

    .line 2013166
    :cond_0
    :goto_0
    return-void

    .line 2013167
    :cond_1
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2013168
    if-eqz p2, :cond_0

    invoke-interface {p2}, LX/DLO;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2013169
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-interface {p2}, LX/DLO;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2013170
    iput-object v2, v1, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2013171
    move-object v1, v1

    .line 2013172
    invoke-interface {p2}, LX/DLO;->b()Ljava/lang/String;

    move-result-object v2

    .line 2013173
    iput-object v2, v1, LX/108;->j:Ljava/lang/String;

    .line 2013174
    move-object v1, v1

    .line 2013175
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2013176
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2013177
    new-instance v1, LX/DZ6;

    invoke-direct {v1, p0, p2}, LX/DZ6;-><init>(LX/DZ8;LX/DLO;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V
    .locals 1
    .param p3    # LX/DLO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2013147
    const-class v0, LX/1ZF;

    invoke-virtual {p1, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2013148
    if-eqz v0, :cond_0

    .line 2013149
    invoke-interface {v0, p2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2013150
    invoke-virtual {p0, p1, p3}, LX/DZ8;->a(Lcom/facebook/base/fragment/FbFragment;LX/DLO;)V

    .line 2013151
    :cond_0
    return-void
.end method
