.class public LX/Edc;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic c:Z

.field private static h:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/io/ByteArrayOutputStream;

.field public b:I

.field public d:LX/EdM;

.field public e:LX/EdZ;

.field private final f:Landroid/content/ContentResolver;

.field private g:LX/Ede;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2149934
    const-class v0, LX/Edc;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/Edc;->c:Z

    .line 2149935
    const/4 v0, 0x0

    sput-object v0, LX/Edc;->h:LX/01J;

    .line 2149936
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    sput-object v0, LX/Edc;->h:LX/01J;

    .line 2149937
    :goto_1
    sget-object v0, LX/Edd;->a:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 2149938
    sget-object v0, LX/Edc;->h:LX/01J;

    sget-object v2, LX/Edd;->a:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2149939
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 2149940
    goto :goto_0

    .line 2149941
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EdM;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2149920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2149921
    iput-object v0, p0, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2149922
    iput-object v0, p0, LX/Edc;->d:LX/EdM;

    .line 2149923
    iput v1, p0, LX/Edc;->b:I

    .line 2149924
    iput-object v0, p0, LX/Edc;->e:LX/EdZ;

    .line 2149925
    iput-object v0, p0, LX/Edc;->g:LX/Ede;

    .line 2149926
    iput-object p2, p0, LX/Edc;->d:LX/EdM;

    .line 2149927
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, LX/Edc;->f:Landroid/content/ContentResolver;

    .line 2149928
    iget-object v0, p2, LX/EdM;->a:LX/Ede;

    move-object v0, v0

    .line 2149929
    iput-object v0, p0, LX/Edc;->g:LX/Ede;

    .line 2149930
    new-instance v0, LX/EdZ;

    invoke-direct {v0, p0}, LX/EdZ;-><init>(LX/Edc;)V

    iput-object v0, p0, LX/Edc;->e:LX/EdZ;

    .line 2149931
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2149932
    iput v1, p0, LX/Edc;->b:I

    .line 2149933
    return-void
.end method

.method private a(J)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 2149909
    move v2, v0

    move-wide v4, p1

    .line 2149910
    :goto_0
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    if-ge v2, v3, :cond_0

    .line 2149911
    ushr-long/2addr v4, v3

    .line 2149912
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2149913
    :cond_0
    invoke-static {p0, v2}, LX/Edc;->a(LX/Edc;I)V

    .line 2149914
    add-int/lit8 v1, v2, -0x1

    mul-int/lit8 v1, v1, 0x8

    .line 2149915
    :goto_1
    if-ge v0, v2, :cond_1

    .line 2149916
    ushr-long v4, p1, v1

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v3, v4

    invoke-static {p0, v3}, LX/Edc;->a(LX/Edc;I)V

    .line 2149917
    add-int/lit8 v1, v1, -0x8

    .line 2149918
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2149919
    :cond_1
    return-void
.end method

.method private a(LX/EdS;)V
    .locals 3

    .prologue
    .line 2149896
    sget-boolean v0, LX/Edc;->c:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2149897
    :cond_0
    iget v0, p1, LX/EdS;->a:I

    move v0, v0

    .line 2149898
    invoke-virtual {p1}, LX/EdS;->b()[B

    move-result-object v1

    .line 2149899
    if-nez v1, :cond_1

    .line 2149900
    :goto_0
    return-void

    .line 2149901
    :cond_1
    iget-object v2, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v2}, LX/EdZ;->a()V

    .line 2149902
    iget-object v2, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v2}, LX/EdZ;->d()LX/Edb;

    move-result-object v2

    .line 2149903
    invoke-static {p0, v0}, LX/Edc;->b(LX/Edc;I)V

    .line 2149904
    invoke-static {p0, v1}, LX/Edc;->a(LX/Edc;[B)V

    .line 2149905
    invoke-virtual {v2}, LX/Edb;->a()I

    move-result v0

    .line 2149906
    iget-object v1, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v1}, LX/EdZ;->b()V

    .line 2149907
    int-to-long v0, v0

    invoke-static {p0, v0, v1}, LX/Edc;->d(LX/Edc;J)V

    .line 2149908
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->c()V

    goto :goto_0
.end method

.method public static a(LX/Edc;I)V
    .locals 1

    .prologue
    .line 2149893
    iget-object v0, p0, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 2149894
    iget v0, p0, LX/Edc;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Edc;->b:I

    .line 2149895
    return-void
.end method

.method public static a(LX/Edc;[B)V
    .locals 3

    .prologue
    const/16 v2, 0x7f

    const/4 v1, 0x0

    .line 2149888
    aget-byte v0, p1, v1

    and-int/lit16 v0, v0, 0xff

    if-le v0, v2, :cond_0

    .line 2149889
    invoke-static {p0, v2}, LX/Edc;->a(LX/Edc;I)V

    .line 2149890
    :cond_0
    array-length v0, p1

    invoke-virtual {p0, p1, v1, v0}, LX/Edc;->a([BII)V

    .line 2149891
    invoke-static {p0, v1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149892
    return-void
.end method

.method private static b(LX/EdS;)LX/EdS;
    .locals 4

    .prologue
    .line 2149866
    :try_start_0
    invoke-virtual {p0}, LX/EdS;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    .line 2149867
    if-nez v0, :cond_4

    .line 2149868
    :cond_0
    :goto_0
    move v1, v1

    .line 2149869
    if-nez p0, :cond_8

    .line 2149870
    const/4 v0, 0x0

    .line 2149871
    :goto_1
    move-object v0, v0

    .line 2149872
    const/4 v2, 0x1

    if-ne v2, v1, :cond_2

    .line 2149873
    const-string v1, "/TYPE=PLMN"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EdS;->b([B)V

    .line 2149874
    :cond_1
    :goto_2
    return-object v0

    .line 2149875
    :cond_2
    const/4 v2, 0x3

    if-ne v2, v1, :cond_3

    .line 2149876
    const-string v1, "/TYPE=IPV4"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EdS;->b([B)V

    goto :goto_2

    .line 2149877
    :catch_0
    const/4 v0, 0x0

    goto :goto_2

    .line 2149878
    :cond_3
    const/4 v2, 0x4

    if-ne v2, v1, :cond_1

    .line 2149879
    const-string v1, "/TYPE=IPV6"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EdS;->b([B)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2149880
    :cond_4
    :try_start_1
    const-string v2, "[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2149881
    const/4 v1, 0x3

    goto :goto_0

    .line 2149882
    :cond_5
    const-string v2, "\\+?[0-9|\\.|\\-]+"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2149883
    const/4 v1, 0x1

    goto :goto_0

    .line 2149884
    :cond_6
    const-string v2, "[a-zA-Z| ]*\\<{0,1}[a-zA-Z| ]+@{1}[a-zA-Z| ]+\\.{1}[a-zA-Z| ]+\\>{0,1}"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2149885
    const/4 v1, 0x2

    goto :goto_0

    .line 2149886
    :cond_7
    const-string v2, "[a-fA-F]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2149887
    const/4 v1, 0x4

    goto :goto_0
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_8
    new-instance v0, LX/EdS;

    iget v2, p0, LX/EdS;->a:I

    iget-object v3, p0, LX/EdS;->b:[B

    invoke-direct {v0, v2, v3}, LX/EdS;-><init>(I[B)V

    goto :goto_1
.end method

.method private static b(LX/Edc;I)V
    .locals 1

    .prologue
    .line 2149864
    or-int/lit16 v0, p1, 0x80

    and-int/lit16 v0, v0, 0xff

    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;I)V

    .line 2149865
    return-void
.end method

.method private static b(LX/Edc;J)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x7f

    .line 2149622
    const/4 v0, 0x0

    move-wide v2, v4

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 2149623
    cmp-long v1, p1, v2

    if-ltz v1, :cond_0

    .line 2149624
    const/4 v1, 0x7

    shl-long/2addr v2, v1

    or-long/2addr v2, v4

    .line 2149625
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2149626
    :cond_0
    :goto_1
    if-lez v0, :cond_1

    .line 2149627
    mul-int/lit8 v1, v0, 0x7

    ushr-long v2, p1, v1

    .line 2149628
    and-long/2addr v2, v4

    .line 2149629
    const-wide/16 v6, 0x80

    or-long/2addr v2, v6

    const-wide/16 v6, 0xff

    and-long/2addr v2, v6

    long-to-int v1, v2

    invoke-static {p0, v1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149630
    add-int/lit8 v0, v0, -0x1

    .line 2149631
    goto :goto_1

    .line 2149632
    :cond_1
    and-long v0, p1, v4

    long-to-int v0, v0

    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;I)V

    .line 2149633
    return-void
.end method

.method public static b(LX/Edc;[B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2149634
    const/16 v0, 0x22

    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;I)V

    .line 2149635
    array-length v0, p1

    invoke-virtual {p0, p1, v1, v0}, LX/Edc;->a([BII)V

    .line 2149636
    invoke-static {p0, v1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149637
    return-void
.end method

.method private static d(LX/Edc;J)V
    .locals 3

    .prologue
    .line 2149638
    const-wide/16 v0, 0x1f

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 2149639
    long-to-int v0, p1

    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;I)V

    .line 2149640
    :goto_0
    return-void

    .line 2149641
    :cond_0
    const/16 v0, 0x1f

    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;I)V

    .line 2149642
    invoke-static {p0, p1, p2}, LX/Edc;->b(LX/Edc;J)V

    goto :goto_0
.end method

.method public static e(LX/Edc;)I
    .locals 4

    .prologue
    const/16 v3, 0x98

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2149643
    iget-object v2, p0, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    if-nez v2, :cond_0

    .line 2149644
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v2, p0, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2149645
    iput v0, p0, LX/Edc;->b:I

    .line 2149646
    :cond_0
    const/16 v2, 0x8c

    invoke-static {p0, v2}, LX/Edc;->a(LX/Edc;I)V

    .line 2149647
    const/16 v2, 0x80

    invoke-static {p0, v2}, LX/Edc;->a(LX/Edc;I)V

    .line 2149648
    invoke-static {p0, v3}, LX/Edc;->a(LX/Edc;I)V

    .line 2149649
    iget-object v2, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v2, v3}, LX/Ede;->b(I)[B

    move-result-object v2

    .line 2149650
    if-nez v2, :cond_1

    .line 2149651
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transaction-ID is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149652
    :cond_1
    invoke-static {p0, v2}, LX/Edc;->a(LX/Edc;[B)V

    .line 2149653
    const/16 v2, 0x8d

    invoke-static {p0, v2}, LX/Edc;->e(LX/Edc;I)I

    move-result v2

    if-eqz v2, :cond_3

    .line 2149654
    :cond_2
    :goto_0
    return v1

    .line 2149655
    :cond_3
    const/16 v2, 0x85

    invoke-static {p0, v2}, LX/Edc;->e(LX/Edc;I)I

    .line 2149656
    const/16 v2, 0x89

    invoke-static {p0, v2}, LX/Edc;->e(LX/Edc;I)I

    move-result v2

    if-nez v2, :cond_2

    .line 2149657
    const/16 v2, 0x97

    invoke-static {p0, v2}, LX/Edc;->e(LX/Edc;I)I

    move-result v2

    if-eq v2, v1, :cond_4

    move v0, v1

    .line 2149658
    :cond_4
    const/16 v2, 0x82

    invoke-static {p0, v2}, LX/Edc;->e(LX/Edc;I)I

    move-result v2

    if-eq v2, v1, :cond_5

    move v0, v1

    .line 2149659
    :cond_5
    const/16 v2, 0x81

    invoke-static {p0, v2}, LX/Edc;->e(LX/Edc;I)I

    move-result v2

    if-eq v2, v1, :cond_6

    move v0, v1

    .line 2149660
    :cond_6
    if-eqz v0, :cond_2

    .line 2149661
    const/16 v0, 0x96

    invoke-static {p0, v0}, LX/Edc;->e(LX/Edc;I)I

    .line 2149662
    const/16 v0, 0x8a

    invoke-static {p0, v0}, LX/Edc;->e(LX/Edc;I)I

    .line 2149663
    const/16 v0, 0x88

    invoke-static {p0, v0}, LX/Edc;->e(LX/Edc;I)I

    .line 2149664
    const/16 v0, 0x8f

    invoke-static {p0, v0}, LX/Edc;->e(LX/Edc;I)I

    .line 2149665
    const/16 v0, 0x86

    invoke-static {p0, v0}, LX/Edc;->e(LX/Edc;I)I

    .line 2149666
    const/16 v0, 0x90

    invoke-static {p0, v0}, LX/Edc;->e(LX/Edc;I)I

    .line 2149667
    const/16 v0, 0x84

    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;I)V

    .line 2149668
    invoke-direct {p0}, LX/Edc;->f()I

    move-result v1

    goto :goto_0
.end method

.method public static e(LX/Edc;I)I
    .locals 7

    .prologue
    const/16 v5, 0x80

    const/4 v1, 0x0

    const/16 v6, 0x81

    const/4 v2, 0x1

    const/4 v0, 0x2

    .line 2149669
    packed-switch p1, :pswitch_data_0

    .line 2149670
    :pswitch_0
    const/4 v1, 0x3

    .line 2149671
    :cond_0
    :goto_0
    return v1

    .line 2149672
    :pswitch_1
    invoke-static {p0, p1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149673
    iget-object v0, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v0, p1}, LX/Ede;->a(I)I

    move-result v0

    .line 2149674
    if-nez v0, :cond_1

    .line 2149675
    const/16 v0, 0x12

    invoke-static {p0, v0}, LX/Edc;->b(LX/Edc;I)V

    goto :goto_0

    .line 2149676
    :cond_1
    invoke-static {p0, v0}, LX/Edc;->b(LX/Edc;I)V

    goto :goto_0

    .line 2149677
    :pswitch_2
    iget-object v2, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v2, p1}, LX/Ede;->b(I)[B

    move-result-object v2

    .line 2149678
    if-nez v2, :cond_2

    move v1, v0

    .line 2149679
    goto :goto_0

    .line 2149680
    :cond_2
    invoke-static {p0, p1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149681
    invoke-static {p0, v2}, LX/Edc;->a(LX/Edc;[B)V

    goto :goto_0

    .line 2149682
    :pswitch_3
    iget-object v3, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v3, p1}, LX/Ede;->d(I)[LX/EdS;

    move-result-object v3

    .line 2149683
    if-nez v3, :cond_3

    move v1, v0

    .line 2149684
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2149685
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 2149686
    aget-object v4, v3, v0

    invoke-static {v4}, LX/Edc;->b(LX/EdS;)LX/EdS;

    move-result-object v4

    .line 2149687
    if-nez v4, :cond_4

    move v1, v2

    .line 2149688
    goto :goto_0

    .line 2149689
    :cond_4
    invoke-static {p0, p1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149690
    invoke-direct {p0, v4}, LX/Edc;->a(LX/EdS;)V

    .line 2149691
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2149692
    :pswitch_4
    invoke-static {p0, p1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149693
    iget-object v0, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v0, p1}, LX/Ede;->c(I)LX/EdS;

    move-result-object v0

    .line 2149694
    if-eqz v0, :cond_5

    invoke-virtual {v0}, LX/EdS;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v3, Ljava/lang/String;

    invoke-virtual {v0}, LX/EdS;->b()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    const-string v4, "insert-address-token"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2149695
    :cond_5
    invoke-static {p0, v2}, LX/Edc;->a(LX/Edc;I)V

    .line 2149696
    invoke-static {p0, v6}, LX/Edc;->a(LX/Edc;I)V

    goto :goto_0

    .line 2149697
    :cond_6
    iget-object v3, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v3}, LX/EdZ;->a()V

    .line 2149698
    iget-object v3, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v3}, LX/EdZ;->d()LX/Edb;

    move-result-object v3

    .line 2149699
    invoke-static {p0, v5}, LX/Edc;->a(LX/Edc;I)V

    .line 2149700
    invoke-static {v0}, LX/Edc;->b(LX/EdS;)LX/EdS;

    move-result-object v0

    .line 2149701
    if-nez v0, :cond_7

    move v1, v2

    .line 2149702
    goto/16 :goto_0

    .line 2149703
    :cond_7
    invoke-direct {p0, v0}, LX/Edc;->a(LX/EdS;)V

    .line 2149704
    invoke-virtual {v3}, LX/Edb;->a()I

    move-result v0

    .line 2149705
    iget-object v2, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v2}, LX/EdZ;->b()V

    .line 2149706
    int-to-long v2, v0

    invoke-static {p0, v2, v3}, LX/Edc;->d(LX/Edc;J)V

    .line 2149707
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->c()V

    goto/16 :goto_0

    .line 2149708
    :pswitch_5
    iget-object v2, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v2, p1}, LX/Ede;->a(I)I

    move-result v2

    .line 2149709
    if-nez v2, :cond_8

    move v1, v0

    .line 2149710
    goto/16 :goto_0

    .line 2149711
    :cond_8
    invoke-static {p0, p1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149712
    invoke-static {p0, v2}, LX/Edc;->a(LX/Edc;I)V

    goto/16 :goto_0

    .line 2149713
    :pswitch_6
    iget-object v2, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v2, p1}, LX/Ede;->e(I)J

    move-result-wide v2

    .line 2149714
    const-wide/16 v4, -0x1

    cmp-long v4, v4, v2

    if-nez v4, :cond_9

    move v1, v0

    .line 2149715
    goto/16 :goto_0

    .line 2149716
    :cond_9
    invoke-static {p0, p1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149717
    invoke-direct {p0, v2, v3}, LX/Edc;->a(J)V

    goto/16 :goto_0

    .line 2149718
    :pswitch_7
    iget-object v2, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v2, p1}, LX/Ede;->c(I)LX/EdS;

    move-result-object v2

    .line 2149719
    if-nez v2, :cond_a

    move v1, v0

    .line 2149720
    goto/16 :goto_0

    .line 2149721
    :cond_a
    invoke-static {p0, p1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149722
    invoke-direct {p0, v2}, LX/Edc;->a(LX/EdS;)V

    goto/16 :goto_0

    .line 2149723
    :pswitch_8
    iget-object v2, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v2, p1}, LX/Ede;->b(I)[B

    move-result-object v2

    .line 2149724
    if-nez v2, :cond_b

    move v1, v0

    .line 2149725
    goto/16 :goto_0

    .line 2149726
    :cond_b
    invoke-static {p0, p1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149727
    const-string v0, "advertisement"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2149728
    invoke-static {p0, v6}, LX/Edc;->a(LX/Edc;I)V

    goto/16 :goto_0

    .line 2149729
    :cond_c
    const-string v0, "auto"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2149730
    const/16 v0, 0x83

    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;I)V

    goto/16 :goto_0

    .line 2149731
    :cond_d
    const-string v0, "personal"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2149732
    invoke-static {p0, v5}, LX/Edc;->a(LX/Edc;I)V

    goto/16 :goto_0

    .line 2149733
    :cond_e
    const-string v0, "informational"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2149734
    const/16 v0, 0x82

    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;I)V

    goto/16 :goto_0

    .line 2149735
    :cond_f
    invoke-static {p0, v2}, LX/Edc;->a(LX/Edc;[B)V

    goto/16 :goto_0

    .line 2149736
    :pswitch_9
    iget-object v2, p0, LX/Edc;->g:LX/Ede;

    invoke-virtual {v2, p1}, LX/Ede;->e(I)J

    move-result-wide v2

    .line 2149737
    const-wide/16 v4, -0x1

    cmp-long v4, v4, v2

    if-nez v4, :cond_10

    move v1, v0

    .line 2149738
    goto/16 :goto_0

    .line 2149739
    :cond_10
    invoke-static {p0, p1}, LX/Edc;->a(LX/Edc;I)V

    .line 2149740
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->a()V

    .line 2149741
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->d()LX/Edb;

    move-result-object v0

    .line 2149742
    invoke-static {p0, v6}, LX/Edc;->a(LX/Edc;I)V

    .line 2149743
    invoke-direct {p0, v2, v3}, LX/Edc;->a(J)V

    .line 2149744
    invoke-virtual {v0}, LX/Edb;->a()I

    move-result v0

    .line 2149745
    iget-object v2, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v2}, LX/EdZ;->b()V

    .line 2149746
    int-to-long v2, v0

    invoke-static {p0, v2, v3}, LX/Edc;->d(LX/Edc;J)V

    .line 2149747
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->c()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x81
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_9
        :pswitch_4
        :pswitch_8
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_9
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private f()I
    .locals 15

    .prologue
    const/16 v14, 0x3e

    const/16 v13, 0x3c

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2149748
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->a()V

    .line 2149749
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->d()LX/Edb;

    move-result-object v3

    .line 2149750
    new-instance v0, Ljava/lang/String;

    iget-object v4, p0, LX/Edc;->g:LX/Ede;

    const/16 v5, 0x84

    invoke-virtual {v4, v5}, LX/Ede;->b(I)[B

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>([B)V

    .line 2149751
    sget-object v4, LX/Edc;->h:LX/01J;

    invoke-virtual {v4, v0}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2149752
    if-nez v0, :cond_0

    move v0, v1

    .line 2149753
    :goto_0
    return v0

    .line 2149754
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, LX/Edc;->b(LX/Edc;I)V

    .line 2149755
    iget-object v0, p0, LX/Edc;->d:LX/EdM;

    check-cast v0, LX/Edn;

    .line 2149756
    iget-object v4, v0, LX/EdV;->b:LX/EdY;

    move-object v5, v4

    .line 2149757
    if-eqz v5, :cond_1

    invoke-virtual {v5}, LX/EdY;->b()I

    move-result v0

    if-nez v0, :cond_2

    .line 2149758
    :cond_1
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, LX/Edc;->b(LX/Edc;J)V

    .line 2149759
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->b()V

    .line 2149760
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->c()V

    move v0, v2

    .line 2149761
    goto :goto_0

    .line 2149762
    :cond_2
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v5, v0}, LX/EdY;->a(I)LX/Edg;

    move-result-object v0

    .line 2149763
    invoke-virtual {v0}, LX/Edg;->c()[B

    move-result-object v4

    .line 2149764
    if-eqz v4, :cond_3

    .line 2149765
    const/16 v6, 0x8a

    invoke-static {p0, v6}, LX/Edc;->a(LX/Edc;I)V

    .line 2149766
    const/4 v6, 0x0

    aget-byte v6, v4, v6

    if-ne v13, v6, :cond_4

    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    aget-byte v6, v4, v6

    if-ne v14, v6, :cond_4

    .line 2149767
    invoke-static {p0, v4}, LX/Edc;->a(LX/Edc;[B)V

    .line 2149768
    :cond_3
    :goto_1
    const/16 v4, 0x89

    invoke-static {p0, v4}, LX/Edc;->a(LX/Edc;I)V

    .line 2149769
    invoke-virtual {v0}, LX/Edg;->g()[B

    move-result-object v0

    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;[B)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2149770
    :goto_2
    invoke-virtual {v3}, LX/Edb;->a()I

    move-result v0

    .line 2149771
    iget-object v3, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v3}, LX/EdZ;->b()V

    .line 2149772
    int-to-long v6, v0

    invoke-static {p0, v6, v7}, LX/Edc;->d(LX/Edc;J)V

    .line 2149773
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->c()V

    .line 2149774
    invoke-virtual {v5}, LX/EdY;->b()I

    move-result v6

    .line 2149775
    int-to-long v8, v6

    invoke-static {p0, v8, v9}, LX/Edc;->b(LX/Edc;J)V

    move v4, v2

    .line 2149776
    :goto_3
    if-ge v4, v6, :cond_14

    .line 2149777
    invoke-virtual {v5, v4}, LX/EdY;->a(I)LX/Edg;

    move-result-object v3

    .line 2149778
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->a()V

    .line 2149779
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->d()LX/Edb;

    move-result-object v7

    .line 2149780
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->a()V

    .line 2149781
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->d()LX/Edb;

    move-result-object v8

    .line 2149782
    invoke-virtual {v3}, LX/Edg;->g()[B

    move-result-object v9

    .line 2149783
    if-nez v9, :cond_5

    move v0, v1

    .line 2149784
    goto/16 :goto_0

    .line 2149785
    :cond_4
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "<"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ">"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2149786
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-static {p0, v6}, LX/Edc;->a(LX/Edc;[B)V
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2149787
    goto :goto_1

    .line 2149788
    :catch_0
    move-exception v0

    .line 2149789
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_2

    .line 2149790
    :cond_5
    sget-object v0, LX/Edc;->h:LX/01J;

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v9}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v10}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2149791
    if-nez v0, :cond_b

    .line 2149792
    invoke-static {p0, v9}, LX/Edc;->a(LX/Edc;[B)V

    .line 2149793
    :goto_4
    invoke-virtual {v3}, LX/Edg;->i()[B

    move-result-object v0

    .line 2149794
    if-nez v0, :cond_6

    .line 2149795
    invoke-virtual {v3}, LX/Edg;->j()[B

    move-result-object v0

    .line 2149796
    if-nez v0, :cond_6

    .line 2149797
    invoke-virtual {v3}, LX/Edg;->e()[B

    move-result-object v0

    .line 2149798
    if-nez v0, :cond_6

    .line 2149799
    const-string v0, "smil.xml"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 2149800
    :cond_6
    const/16 v9, 0x85

    invoke-static {p0, v9}, LX/Edc;->a(LX/Edc;I)V

    .line 2149801
    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;[B)V

    .line 2149802
    invoke-virtual {v3}, LX/Edg;->d()I

    move-result v0

    .line 2149803
    if-eqz v0, :cond_7

    .line 2149804
    const/16 v9, 0x81

    invoke-static {p0, v9}, LX/Edc;->a(LX/Edc;I)V

    .line 2149805
    invoke-static {p0, v0}, LX/Edc;->b(LX/Edc;I)V

    .line 2149806
    :cond_7
    invoke-virtual {v8}, LX/Edb;->a()I

    move-result v0

    .line 2149807
    iget-object v8, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v8}, LX/EdZ;->b()V

    .line 2149808
    int-to-long v8, v0

    invoke-static {p0, v8, v9}, LX/Edc;->d(LX/Edc;J)V

    .line 2149809
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->c()V

    .line 2149810
    invoke-virtual {v3}, LX/Edg;->c()[B

    move-result-object v0

    .line 2149811
    if-eqz v0, :cond_8

    .line 2149812
    const/16 v8, 0xc0

    invoke-static {p0, v8}, LX/Edc;->a(LX/Edc;I)V

    .line 2149813
    aget-byte v8, v0, v2

    if-ne v13, v8, :cond_c

    array-length v8, v0

    add-int/lit8 v8, v8, -0x1

    aget-byte v8, v0, v8

    if-ne v14, v8, :cond_c

    .line 2149814
    invoke-static {p0, v0}, LX/Edc;->b(LX/Edc;[B)V

    .line 2149815
    :cond_8
    :goto_5
    invoke-virtual {v3}, LX/Edg;->e()[B

    move-result-object v0

    .line 2149816
    if-eqz v0, :cond_9

    .line 2149817
    const/16 v8, 0x8e

    invoke-static {p0, v8}, LX/Edc;->a(LX/Edc;I)V

    .line 2149818
    invoke-static {p0, v0}, LX/Edc;->a(LX/Edc;[B)V

    .line 2149819
    :cond_9
    invoke-virtual {v7}, LX/Edb;->a()I

    move-result v8

    .line 2149820
    iget-object v0, v3, LX/Edg;->f:[B

    move-object v0, v0

    .line 2149821
    if-eqz v0, :cond_d

    .line 2149822
    array-length v3, v0

    invoke-virtual {p0, v0, v2, v3}, LX/Edc;->a([BII)V

    .line 2149823
    array-length v0, v0

    .line 2149824
    :cond_a
    :goto_6
    invoke-virtual {v7}, LX/Edb;->a()I

    move-result v3

    sub-int/2addr v3, v8

    if-eq v0, v3, :cond_13

    .line 2149825
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "BUG: Length sanity check failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149826
    :cond_b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, LX/Edc;->b(LX/Edc;I)V

    goto/16 :goto_4

    .line 2149827
    :cond_c
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "<"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ">"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2149828
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-static {p0, v8}, LX/Edc;->b(LX/Edc;[B)V

    .line 2149829
    goto :goto_5

    .line 2149830
    :cond_d
    const/4 v0, 0x0

    .line 2149831
    const/16 v9, 0x400

    :try_start_2
    new-array v9, v9, [B

    .line 2149832
    iget-object v10, p0, LX/Edc;->f:Landroid/content/ContentResolver;

    .line 2149833
    iget-object v11, v3, LX/Edg;->e:Landroid/net/Uri;

    move-object v3, v11

    .line 2149834
    invoke-virtual {v10, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    move v0, v2

    .line 2149835
    :goto_7
    :try_start_3
    invoke-virtual {v3, v9}, Ljava/io/InputStream;->read([B)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_e

    .line 2149836
    iget-object v11, p0, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    const/4 v12, 0x0

    invoke-virtual {v11, v9, v12, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 2149837
    iget v11, p0, LX/Edc;->b:I

    add-int/2addr v11, v10

    iput v11, p0, LX/Edc;->b:I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2149838
    add-int/2addr v0, v10

    goto :goto_7

    .line 2149839
    :cond_e
    if-eqz v3, :cond_a

    .line 2149840
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_6

    .line 2149841
    :catch_1
    goto :goto_6

    .line 2149842
    :catch_2
    :goto_8
    if-eqz v0, :cond_f

    .line 2149843
    :try_start_5
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    :cond_f
    :goto_9
    move v0, v1

    .line 2149844
    goto/16 :goto_0

    .line 2149845
    :catch_3
    move-object v3, v0

    :goto_a
    if-eqz v3, :cond_10

    .line 2149846
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    :cond_10
    :goto_b
    move v0, v1

    .line 2149847
    goto/16 :goto_0

    .line 2149848
    :catch_4
    move-object v3, v0

    :goto_c
    if-eqz v3, :cond_11

    .line 2149849
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    :cond_11
    :goto_d
    move v0, v1

    .line 2149850
    goto/16 :goto_0

    .line 2149851
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_e
    if-eqz v3, :cond_12

    .line 2149852
    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8

    .line 2149853
    :cond_12
    :goto_f
    throw v0

    .line 2149854
    :cond_13
    iget-object v3, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v3}, LX/EdZ;->b()V

    .line 2149855
    int-to-long v8, v8

    invoke-static {p0, v8, v9}, LX/Edc;->b(LX/Edc;J)V

    .line 2149856
    int-to-long v8, v0

    invoke-static {p0, v8, v9}, LX/Edc;->b(LX/Edc;J)V

    .line 2149857
    iget-object v0, p0, LX/Edc;->e:LX/EdZ;

    invoke-virtual {v0}, LX/EdZ;->c()V

    .line 2149858
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 2149859
    goto/16 :goto_0

    :catch_5
    goto :goto_9

    :catch_6
    goto :goto_b

    :catch_7
    goto :goto_d

    :catch_8
    goto :goto_f

    .line 2149860
    :catchall_1
    move-exception v0

    goto :goto_e

    :catch_9
    goto :goto_c

    :catch_a
    goto :goto_a

    :catch_b
    move-object v0, v3

    goto :goto_8
.end method


# virtual methods
.method public final a([BII)V
    .locals 1

    .prologue
    .line 2149861
    iget-object v0, p0, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 2149862
    iget v0, p0, LX/Edc;->b:I

    add-int/2addr v0, p3

    iput v0, p0, LX/Edc;->b:I

    .line 2149863
    return-void
.end method
