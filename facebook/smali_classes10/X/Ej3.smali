.class public LX/Ej3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2160758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160759
    return-void
.end method

.method public static a(LX/0QB;)LX/Ej3;
    .locals 1

    .prologue
    .line 2160760
    new-instance v0, LX/Ej3;

    invoke-direct {v0}, LX/Ej3;-><init>()V

    .line 2160761
    move-object v0, v0

    .line 2160762
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2160763
    check-cast p1, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;

    .line 2160764
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/http/NameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "email"

    iget-object v4, p1, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->a:Lcom/facebook/growth/model/Contactpoint;

    iget-object v4, v4, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "id_token"

    iget-object v4, p1, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "flow"

    iget-object v4, p1, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->c:LX/4gx;

    iget-object v4, v4, LX/4gx;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "provider"

    iget-object v4, p1, Lcom/facebook/confirmation/protocol/OpenIDConnectEmailConfirmationMethod$Params;->d:LX/4gy;

    iget-object v4, v4, LX/4gy;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2160765
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "openidConnectEmailConfirmation"

    .line 2160766
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2160767
    move-object v1, v1

    .line 2160768
    const-string v2, "POST"

    .line 2160769
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2160770
    move-object v1, v1

    .line 2160771
    const-string v2, "auth/openidconnect_email_confirmation"

    .line 2160772
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2160773
    move-object v1, v1

    .line 2160774
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    .line 2160775
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2160776
    move-object v0, v1

    .line 2160777
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2160778
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2160779
    move-object v0, v0

    .line 2160780
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2160781
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2160782
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2160783
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
