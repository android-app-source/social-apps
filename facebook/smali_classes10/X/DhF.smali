.class public final LX/DhF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/io/File;",
        ">;",
        "LX/Dh8;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)V
    .locals 0

    .prologue
    .line 2030727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030728
    iput-object p1, p0, LX/DhF;->a:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    .line 2030729
    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2030730
    check-cast p1, Ljava/util/List;

    .line 2030731
    new-instance v2, LX/Dh8;

    iget-object v3, p0, LX/DhF;->a:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-direct {v2, v3, v0, v1}, LX/Dh8;-><init>(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;Ljava/io/File;Ljava/io/File;)V

    return-object v2
.end method
