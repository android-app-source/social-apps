.class public LX/EFw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EFv;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/webrtc/ConferenceCall;

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/EG5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2096679
    const-class v0, LX/EFw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EFw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/webrtc/ConferenceCall;)V
    .locals 1
    .param p1    # Lcom/facebook/webrtc/ConferenceCall;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2096668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096669
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2096670
    iput-object v0, p0, LX/EFw;->b:LX/0Ot;

    .line 2096671
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2096672
    iput-object v0, p0, LX/EFw;->c:LX/0Ot;

    .line 2096673
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EFw;->i:Ljava/util/Map;

    .line 2096674
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EFw;->j:Ljava/util/Map;

    .line 2096675
    sget-object v0, LX/EG5;->Activity:LX/EG5;

    iput-object v0, p0, LX/EFw;->k:LX/EG5;

    .line 2096676
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096677
    iput-object p1, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    .line 2096678
    return-void
.end method

.method public static i(LX/EFw;)V
    .locals 1

    .prologue
    .line 2096664
    iget-object v0, p0, LX/EFw;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2096665
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EFw;->e:Z

    .line 2096666
    const/4 v0, 0x0

    iput-object v0, p0, LX/EFw;->g:Ljava/lang/String;

    .line 2096667
    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 1

    .prologue
    .line 2096661
    iget-boolean v0, p0, LX/EFw;->d:Z

    if-eqz v0, :cond_0

    .line 2096662
    :goto_0
    return-void

    .line 2096663
    :cond_0
    iget-object v0, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/webrtc/ConferenceCall;->setVideoParameters(III)V

    goto :goto_0
.end method

.method public final a(LX/EG5;Z)V
    .locals 8

    .prologue
    .line 2096636
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096637
    iget-boolean v0, p0, LX/EFw;->d:Z

    if-eqz v0, :cond_1

    .line 2096638
    sget-object v0, LX/EFw;->a:Ljava/lang/String;

    const-string v1, "Unable to update dominant speaker because conference call has ended"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096639
    :cond_0
    :goto_0
    return-void

    .line 2096640
    :cond_1
    iget-object v0, p0, LX/EFw;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2096641
    iget-object v1, v0, LX/EDx;->af:LX/EGE;

    move-object v1, v1

    .line 2096642
    if-nez v1, :cond_2

    .line 2096643
    sget-object v0, LX/EFw;->a:Ljava/lang/String;

    const-string v1, "Subscribe to dominant speaker failed because there is no dominant speaker"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2096644
    :cond_2
    if-eqz p2, :cond_0

    .line 2096645
    iget-object v0, p0, LX/EFw;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2096646
    iget-boolean p2, v0, LX/EDx;->ag:Z

    move v0, p2

    .line 2096647
    iget-boolean v4, p0, LX/EFw;->d:Z

    if-eqz v4, :cond_4

    .line 2096648
    sget-object v3, LX/EFw;->a:Ljava/lang/String;

    const-string v4, "Unable to update subscribed to single stream because conference call has ended"

    invoke-static {v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096649
    :cond_3
    :goto_1
    goto :goto_0

    .line 2096650
    :cond_4
    if-eqz v1, :cond_5

    if-eqz v0, :cond_6

    iget-object v4, v1, LX/EGE;->g:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2096651
    :cond_5
    sget-object v3, LX/EFw;->a:Ljava/lang/String;

    const-string v4, "Unable to subscribe to single video stream"

    invoke-static {v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2096652
    :cond_6
    const-string v4, "Subscribe to single stream"

    invoke-virtual {p0, p1, v4}, LX/EFw;->a(LX/EG5;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2096653
    iget-object v2, p0, LX/EFw;->g:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, v1, LX/EGE;->b:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/EFw;->g:Ljava/lang/String;

    iget-object v4, v1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2096654
    goto :goto_1

    .line 2096655
    :cond_7
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2096656
    iget-object v4, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    iget-object v5, v1, LX/EGE;->b:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v2, v1, LX/EGE;->g:Ljava/lang/String;

    :goto_2
    iget-wide v6, v1, LX/EGE;->h:J

    invoke-virtual {v4, v5, v2, v6, v7}, Lcom/facebook/webrtc/ConferenceCall;->subscribeSingleRemoteVideoStream(Ljava/lang/String;Ljava/lang/String;J)Z

    .line 2096657
    invoke-static {p0}, LX/EFw;->i(LX/EFw;)V

    .line 2096658
    iget-object v2, v1, LX/EGE;->b:Ljava/lang/String;

    iput-object v2, p0, LX/EFw;->g:Ljava/lang/String;

    .line 2096659
    goto :goto_1

    .line 2096660
    :cond_8
    const-string v2, ""

    goto :goto_2
.end method

.method public final a(JLandroid/view/View;)Z
    .locals 3
    .param p3    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2096630
    iget-object v0, p0, LX/EFw;->j:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2096631
    if-nez v0, :cond_0

    if-nez p3, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    if-nez p3, :cond_2

    :cond_1
    move v0, v1

    .line 2096632
    :goto_0
    return v0

    .line 2096633
    :cond_2
    if-nez v0, :cond_3

    if-eqz p3, :cond_4

    :cond_3
    invoke-virtual {p3}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2096634
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 2096635
    goto :goto_0
.end method

.method public final a(LX/EG5;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2096625
    iget-object v2, p0, LX/EFw;->k:LX/EG5;

    if-eq p1, v2, :cond_0

    sget-object v2, LX/EG5;->Override:LX/EG5;

    if-eq p1, v2, :cond_0

    .line 2096626
    sget-object v2, LX/EFw;->a:Ljava/lang/String;

    const-string v3, "%s failed Allowed: %s, Requested %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v0

    iget-object v5, p0, LX/EFw;->k:LX/EG5;

    aput-object v5, v4, v1

    const/4 v1, 0x2

    aput-object p1, v4, v1

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2096627
    :goto_0
    return v0

    .line 2096628
    :cond_0
    move v0, v1

    .line 2096629
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2096601
    iget-boolean v0, p0, LX/EFw;->d:Z

    if-eqz v0, :cond_0

    .line 2096602
    :goto_0
    return-void

    .line 2096603
    :cond_0
    iget-object v0, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v0, p1}, Lcom/facebook/webrtc/ConferenceCall;->setCamera(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2096622
    iget-boolean v0, p0, LX/EFw;->d:Z

    if-eqz v0, :cond_0

    .line 2096623
    :goto_0
    return-void

    .line 2096624
    :cond_0
    iget-object v0, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v0, p1}, Lcom/facebook/webrtc/ConferenceCall;->configureVideo(Z)V

    goto :goto_0
.end method

.method public final b([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2096617
    iget-boolean v0, p0, LX/EFw;->d:Z

    if-eqz v0, :cond_0

    .line 2096618
    :goto_0
    return-void

    .line 2096619
    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/EFw;->f:Z

    .line 2096620
    iget-object v0, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v0, p1}, Lcom/facebook/webrtc/ConferenceCall;->inviteParticipants([Ljava/lang/String;)V

    goto :goto_0

    .line 2096621
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2096614
    iget-boolean v0, p0, LX/EFw;->d:Z

    if-eqz v0, :cond_0

    .line 2096615
    const/4 v0, 0x0

    .line 2096616
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v0}, Lcom/facebook/webrtc/ConferenceCall;->isVideoEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2096611
    iget-boolean v0, p0, LX/EFw;->d:Z

    if-eqz v0, :cond_0

    .line 2096612
    const/4 v0, 0x0

    .line 2096613
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v0}, Lcom/facebook/webrtc/ConferenceCall;->conferenceName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2096608
    iget-boolean v0, p0, LX/EFw;->d:Z

    if-eqz v0, :cond_0

    .line 2096609
    const/4 v0, -0x1

    .line 2096610
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v0}, Lcom/facebook/webrtc/ConferenceCall;->callType()I

    move-result v0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2096604
    iget-boolean v0, p0, LX/EFw;->d:Z

    if-eqz v0, :cond_0

    .line 2096605
    :goto_0
    return-void

    .line 2096606
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EFw;->d:Z

    .line 2096607
    iget-object v0, p0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v0}, Lcom/facebook/webrtc/ConferenceCall;->b()V

    goto :goto_0
.end method
