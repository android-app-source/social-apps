.class public final LX/Dap;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;)V
    .locals 0

    .prologue
    .line 2016323
    iput-object p1, p0, LX/Dap;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2016327
    iget-object v0, p0, LX/Dap;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    const/4 v1, 0x1

    .line 2016328
    iput-boolean v1, v0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->f:Z

    .line 2016329
    iget-object v0, p0, LX/Dap;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    .line 2016330
    invoke-static {v0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->c$redex0(Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;)V

    .line 2016331
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2016324
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2016325
    iget-object v0, p0, LX/Dap;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    iget-object v0, v0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a05f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2016326
    return-void
.end method
