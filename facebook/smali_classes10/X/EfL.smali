.class public LX/EfL;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field private static final n:Ljava/lang/String;


# instance fields
.field public A:LX/EfK;

.field public B:LX/AL7;

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:Lcom/facebook/audience/model/ReplyThreadConfig;

.field private final I:LX/1OX;

.field private final J:LX/AKt;

.field private final K:Landroid/view/View$OnClickListener;

.field private final L:LX/AKk;

.field private final M:LX/AKw;

.field private final N:LX/AL3;

.field public final O:LX/AL6;

.field public a:LX/1Eo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/AHu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AEv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/AFO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1EX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/AFJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/audience/util/PrefetchUtils;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0he;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0hg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0fO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final o:Lcom/facebook/audience/ui/ReplyRecyclerView;

.field public final p:Landroid/view/View;

.field public final q:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

.field public final r:Lcom/facebook/resources/ui/FbTextView;

.field public final s:LX/1P1;

.field public final t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

.field public final u:Landroid/view/View;

.field public final v:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

.field private final w:Landroid/widget/LinearLayout;

.field public final x:Lcom/facebook/fbui/glyph/GlyphView;

.field public y:LX/EfA;

.field public z:Lcom/facebook/audience/model/ReplyThread;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2154335
    const-class v0, LX/EfL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EfL;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2154336
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EfL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2154337
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2154338
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/EfL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2154339
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2154340
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2154341
    iput-boolean v2, p0, LX/EfL;->C:Z

    .line 2154342
    iput-boolean v2, p0, LX/EfL;->D:Z

    .line 2154343
    iput-boolean v2, p0, LX/EfL;->E:Z

    .line 2154344
    iput-boolean v2, p0, LX/EfL;->F:Z

    .line 2154345
    iput-boolean v2, p0, LX/EfL;->G:Z

    .line 2154346
    new-instance v0, LX/EfB;

    invoke-direct {v0, p0}, LX/EfB;-><init>(LX/EfL;)V

    iput-object v0, p0, LX/EfL;->I:LX/1OX;

    .line 2154347
    new-instance v0, LX/EfC;

    invoke-direct {v0, p0}, LX/EfC;-><init>(LX/EfL;)V

    iput-object v0, p0, LX/EfL;->J:LX/AKt;

    .line 2154348
    new-instance v0, LX/EfD;

    invoke-direct {v0, p0}, LX/EfD;-><init>(LX/EfL;)V

    iput-object v0, p0, LX/EfL;->K:Landroid/view/View$OnClickListener;

    .line 2154349
    new-instance v0, LX/EfE;

    invoke-direct {v0, p0}, LX/EfE;-><init>(LX/EfL;)V

    iput-object v0, p0, LX/EfL;->L:LX/AKk;

    .line 2154350
    new-instance v0, LX/EfF;

    invoke-direct {v0, p0}, LX/EfF;-><init>(LX/EfL;)V

    iput-object v0, p0, LX/EfL;->M:LX/AKw;

    .line 2154351
    new-instance v0, LX/EfG;

    invoke-direct {v0, p0}, LX/EfG;-><init>(LX/EfL;)V

    iput-object v0, p0, LX/EfL;->N:LX/AL3;

    .line 2154352
    new-instance v0, LX/EfH;

    invoke-direct {v0, p0}, LX/EfH;-><init>(LX/EfL;)V

    iput-object v0, p0, LX/EfL;->O:LX/AL6;

    .line 2154353
    const-class v0, LX/EfL;

    invoke-static {v0, p0}, LX/EfL;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2154354
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2154355
    const v1, 0x7f030180

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2154356
    const v0, 0x7f0d06b2

    invoke-virtual {p0, v0}, LX/EfL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/ReplyRecyclerView;

    iput-object v0, p0, LX/EfL;->o:Lcom/facebook/audience/ui/ReplyRecyclerView;

    .line 2154357
    const v0, 0x7f0d06ae

    invoke-virtual {p0, v0}, LX/EfL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/ReplyThreadStoryView;

    iput-object v0, p0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    .line 2154358
    const v0, 0x7f0d06af

    invoke-virtual {p0, v0}, LX/EfL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EfL;->u:Landroid/view/View;

    .line 2154359
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, LX/EfL;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/EfL;->s:LX/1P1;

    .line 2154360
    iget-object v0, p0, LX/EfL;->s:LX/1P1;

    invoke-virtual {v0, v3}, LX/1P1;->a(Z)V

    .line 2154361
    iget-object v0, p0, LX/EfL;->o:Lcom/facebook/audience/ui/ReplyRecyclerView;

    iget-object v1, p0, LX/EfL;->s:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2154362
    iget-object v0, p0, LX/EfL;->o:Lcom/facebook/audience/ui/ReplyRecyclerView;

    iget-object v1, p0, LX/EfL;->e:Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2154363
    iget-object v0, p0, LX/EfL;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2154364
    const v0, 0x7f0d06b4

    invoke-virtual {p0, v0}, LX/EfL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EfL;->p:Landroid/view/View;

    .line 2154365
    const v0, 0x7f0d06b6

    invoke-virtual {p0, v0}, LX/EfL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EfL;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 2154366
    const v0, 0x7f0d06b5

    invoke-virtual {p0, v0}, LX/EfL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/EfL;->x:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2154367
    const v0, 0x7f0d06b1

    invoke-virtual {p0, v0}, LX/EfL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    iput-object v0, p0, LX/EfL;->q:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    .line 2154368
    const v0, 0x7f0d06b3

    invoke-virtual {p0, v0}, LX/EfL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    iput-object v0, p0, LX/EfL;->v:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    .line 2154369
    const v0, 0x7f0d06b0

    invoke-virtual {p0, v0}, LX/EfL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/EfL;->w:Landroid/widget/LinearLayout;

    .line 2154370
    iget-object v0, p0, LX/EfL;->v:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    iget-object v1, p0, LX/EfL;->N:LX/AL3;

    .line 2154371
    iput-object v1, v0, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->g:LX/AL3;

    .line 2154372
    iget-object v0, p0, LX/EfL;->o:Lcom/facebook/audience/ui/ReplyRecyclerView;

    iget-object v1, p0, LX/EfL;->I:LX/1OX;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2154373
    iget-object v0, p0, LX/EfL;->o:Lcom/facebook/audience/ui/ReplyRecyclerView;

    iget-object v1, p0, LX/EfL;->J:LX/AKt;

    .line 2154374
    iput-object v1, v0, Lcom/facebook/audience/ui/ReplyRecyclerView;->i:LX/AKt;

    .line 2154375
    iget-object v0, p0, LX/EfL;->p:Landroid/view/View;

    iget-object v1, p0, LX/EfL;->K:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2154376
    iget-object v0, p0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    iget-object v1, p0, LX/EfL;->M:LX/AKw;

    .line 2154377
    iput-object v1, v0, Lcom/facebook/audience/ui/ReplyThreadStoryView;->j:LX/AKw;

    .line 2154378
    iget-object v0, p0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    invoke-virtual {v0, v3}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->setLoading(Z)V

    .line 2154379
    iget-object v0, p0, LX/EfL;->q:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    iget-object v1, p0, LX/EfL;->L:LX/AKk;

    .line 2154380
    iput-object v1, v0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->f:LX/AKk;

    .line 2154381
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, LX/EfL;

    invoke-static {p0}, LX/1Eo;->a(LX/0QB;)LX/1Eo;

    move-result-object v2

    check-cast v2, LX/1Eo;

    invoke-static {p0}, LX/AHu;->b(LX/0QB;)LX/AHu;

    move-result-object v3

    check-cast v3, LX/AHu;

    invoke-static {p0}, LX/AEv;->a(LX/0QB;)LX/AEv;

    move-result-object v4

    check-cast v4, LX/AEv;

    invoke-static {p0}, LX/AFO;->a(LX/0QB;)LX/AFO;

    move-result-object v5

    check-cast v5, LX/AFO;

    new-instance v8, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const-class v7, LX/EfQ;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/EfQ;

    invoke-direct {v8, v6, v7}, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;-><init>(Landroid/content/Context;LX/EfQ;)V

    move-object v6, v8

    check-cast v6, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/1EX;->b(LX/0QB;)LX/1EX;

    move-result-object v9

    check-cast v9, LX/1EX;

    invoke-static {p0}, LX/AFJ;->a(LX/0QB;)LX/AFJ;

    move-result-object v10

    check-cast v10, LX/AFJ;

    invoke-static {p0}, Lcom/facebook/audience/util/PrefetchUtils;->a(LX/0QB;)Lcom/facebook/audience/util/PrefetchUtils;

    move-result-object v11

    check-cast v11, Lcom/facebook/audience/util/PrefetchUtils;

    invoke-static {p0}, LX/0he;->a(LX/0QB;)LX/0he;

    move-result-object v12

    check-cast v12, LX/0he;

    invoke-static {p0}, LX/0hg;->a(LX/0QB;)LX/0hg;

    move-result-object v13

    check-cast v13, LX/0hg;

    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object p0

    check-cast p0, LX/0fO;

    iput-object v2, v1, LX/EfL;->a:LX/1Eo;

    iput-object v3, v1, LX/EfL;->b:LX/AHu;

    iput-object v4, v1, LX/EfL;->c:LX/AEv;

    iput-object v5, v1, LX/EfL;->d:LX/AFO;

    iput-object v6, v1, LX/EfL;->e:Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;

    iput-object v7, v1, LX/EfL;->f:LX/03V;

    iput-object v8, v1, LX/EfL;->g:Ljava/util/concurrent/Executor;

    iput-object v9, v1, LX/EfL;->h:LX/1EX;

    iput-object v10, v1, LX/EfL;->i:LX/AFJ;

    iput-object v11, v1, LX/EfL;->j:Lcom/facebook/audience/util/PrefetchUtils;

    iput-object v12, v1, LX/EfL;->k:LX/0he;

    iput-object v13, v1, LX/EfL;->l:LX/0hg;

    iput-object p0, v1, LX/EfL;->m:LX/0fO;

    return-void
.end method

.method public static a$redex0(LX/EfL;I)V
    .locals 3

    .prologue
    .line 2154382
    invoke-virtual {p0}, LX/EfL;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2154383
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2154384
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 2154385
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    move v0, v1

    .line 2154386
    sub-int/2addr v0, p1

    .line 2154387
    iget-object v1, p0, LX/EfL;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2154388
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2154389
    iget-object v0, p0, LX/EfL;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2154390
    return-void
.end method

.method public static a$redex0(LX/EfL;Lcom/facebook/audience/model/ReplyThread;)V
    .locals 8

    .prologue
    .line 2154391
    iget-object v0, p0, LX/EfL;->c:LX/AEv;

    iget-object v1, p0, LX/EfL;->H:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2154392
    iget-object v2, v1, Lcom/facebook/audience/model/ReplyThreadConfig;->d:LX/7gw;

    move-object v1, v2

    .line 2154393
    invoke-static {p1}, LX/7h2;->a(Lcom/facebook/audience/model/ReplyThread;)Lcom/facebook/audience/model/Reply;

    move-result-object v2

    .line 2154394
    new-instance v3, LX/AFU;

    invoke-direct {v3}, LX/AFU;-><init>()V

    move-object v3, v3

    .line 2154395
    iget-wide v6, v2, Lcom/facebook/audience/model/Reply;->i:J

    move-wide v4, v6

    .line 2154396
    iput-wide v4, v3, LX/AFU;->a:J

    .line 2154397
    move-object v2, v3

    .line 2154398
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2154399
    iput-object v3, v2, LX/AFU;->b:Ljava/lang/String;

    .line 2154400
    move-object v2, v2

    .line 2154401
    new-instance v3, Lcom/facebook/audience/direct/model/CachedSeenModel;

    invoke-direct {v3, v2}, Lcom/facebook/audience/direct/model/CachedSeenModel;-><init>(LX/AFU;)V

    move-object v2, v3

    .line 2154402
    iget-object v3, v0, LX/AEv;->c:LX/AFD;

    .line 2154403
    iget-object v4, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2154404
    invoke-virtual {v3, v4, v2}, LX/AFD;->a(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2154405
    iget-object v2, v0, LX/AEv;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2154406
    iget-object v2, v0, LX/AEv;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AF5;

    .line 2154407
    iget-object v3, v2, LX/AF5;->a:LX/0gI;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0gI;->a(Z)V

    .line 2154408
    :cond_0
    iget-object v0, p0, LX/EfL;->H:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2154409
    iget-boolean v1, v0, Lcom/facebook/audience/model/ReplyThreadConfig;->b:Z

    move v0, v1

    .line 2154410
    if-eqz v0, :cond_1

    .line 2154411
    iget-object v0, p0, LX/EfL;->i:LX/AFJ;

    .line 2154412
    new-instance v2, LX/AFS;

    invoke-direct {v2}, LX/AFS;-><init>()V

    move-object v2, v2

    .line 2154413
    iget-object v3, v0, LX/AFJ;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 2154414
    iput-wide v4, v2, LX/AFS;->a:J

    .line 2154415
    move-object v2, v2

    .line 2154416
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2154417
    iput-object v3, v2, LX/AFS;->b:Ljava/lang/String;

    .line 2154418
    move-object v2, v2

    .line 2154419
    new-instance v3, Lcom/facebook/audience/direct/model/CachedReplayedModel;

    invoke-direct {v3, v2}, Lcom/facebook/audience/direct/model/CachedReplayedModel;-><init>(LX/AFS;)V

    move-object v2, v3

    .line 2154420
    iget-object v3, v0, LX/AFJ;->a:LX/AFD;

    .line 2154421
    iget-object v4, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2154422
    invoke-virtual {v3, v4, v2}, LX/AFD;->a(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2154423
    iget-object v2, v0, LX/AFJ;->c:LX/AF4;

    if-eqz v2, :cond_1

    .line 2154424
    iget-object v2, v0, LX/AFJ;->c:LX/AF4;

    .line 2154425
    iget-object v3, v2, LX/AF4;->a:LX/0gI;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0gI;->a(Z)V

    .line 2154426
    :cond_1
    iget-object v0, p0, LX/EfL;->b:LX/AHu;

    .line 2154427
    iget-object v1, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2154428
    iget-object v2, v0, LX/AHu;->e:LX/0gK;

    invoke-virtual {v2}, LX/0gK;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2154429
    invoke-static {v0, v1}, LX/AHu;->d(LX/AHu;Ljava/lang/String;)V

    .line 2154430
    :goto_0
    iget-object v0, p0, LX/EfL;->l:LX/0hg;

    const/4 v1, 0x1

    const v4, 0xbc0005

    .line 2154431
    iget-object v2, v0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2154432
    iget-object v3, v0, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v1, :cond_4

    const/4 v2, 0x2

    :goto_1
    invoke-interface {v3, v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2154433
    :cond_2
    return-void

    .line 2154434
    :cond_3
    new-instance v2, LX/AIA;

    invoke-direct {v2}, LX/AIA;-><init>()V

    move-object v3, v2

    .line 2154435
    new-instance v4, LX/4D8;

    invoke-direct {v4}, LX/4D8;-><init>()V

    iget-object v2, v0, LX/AHu;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2154436
    const-string v5, "actor_id"

    invoke-virtual {v4, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154437
    move-object v2, v4

    .line 2154438
    const-string v4, "thread_id"

    invoke-virtual {v2, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154439
    move-object v2, v2

    .line 2154440
    iget-object v4, v3, LX/0gW;->h:Ljava/lang/String;

    move-object v4, v4

    .line 2154441
    const-string v5, "client_mutation_id"

    invoke-virtual {v2, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154442
    move-object v2, v2

    .line 2154443
    const-string v4, "0"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2154444
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 2154445
    iget-object v3, v0, LX/AHu;->c:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 2154446
    new-instance v3, LX/AHs;

    invoke-direct {v3, v0, v1}, LX/AHs;-><init>(LX/AHu;Ljava/lang/String;)V

    iget-object v4, v0, LX/AHu;->b:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2154447
    goto :goto_0

    .line 2154448
    :cond_4
    const/4 v2, 0x3

    goto :goto_1
.end method

.method private b(Lcom/facebook/audience/model/ReplyThread;)V
    .locals 2
    .param p1    # Lcom/facebook/audience/model/ReplyThread;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2154231
    if-nez p1, :cond_0

    .line 2154232
    :goto_0
    return-void

    .line 2154233
    :cond_0
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v0, v0

    .line 2154234
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2154235
    iget-object v0, p0, LX/EfL;->u:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 2154236
    :cond_1
    iget-object v0, p0, LX/EfL;->u:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/audience/model/ReplyThread;)Z
    .locals 2

    .prologue
    .line 2154333
    iget-object v0, p0, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, v0

    .line 2154334
    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "100013803123634"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static f(LX/EfL;)V
    .locals 4

    .prologue
    .line 2154298
    iget-boolean v0, p0, LX/EfL;->C:Z

    if-nez v0, :cond_4

    iget-object v0, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    if-eqz v0, :cond_4

    .line 2154299
    iget-object v0, p0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    invoke-virtual {v0}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->a()V

    .line 2154300
    iget-object v0, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    .line 2154301
    invoke-static {v0}, LX/EfL;->c(Lcom/facebook/audience/model/ReplyThread;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2154302
    :goto_0
    iget-object v0, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    invoke-direct {p0, v0}, LX/EfL;->b(Lcom/facebook/audience/model/ReplyThread;)V

    .line 2154303
    iget-object v0, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    .line 2154304
    iget-object v1, v0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v1, v1

    .line 2154305
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2154306
    invoke-static {p0, v0}, LX/EfL;->a$redex0(LX/EfL;Lcom/facebook/audience/model/ReplyThread;)V

    .line 2154307
    :cond_0
    iget-object v0, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    const/16 v2, 0x8

    .line 2154308
    invoke-static {v0}, LX/EfL;->c(Lcom/facebook/audience/model/ReplyThread;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/EfL;->H:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2154309
    iget-boolean v0, v1, Lcom/facebook/audience/model/ReplyThreadConfig;->c:Z

    move v1, v0

    .line 2154310
    if-nez v1, :cond_2

    .line 2154311
    :cond_1
    iget-object v1, p0, LX/EfL;->p:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2154312
    iget-object v1, p0, LX/EfL;->v:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-virtual {v1, v2}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->setVisibility(I)V

    .line 2154313
    :cond_2
    iget-boolean v0, p0, LX/EfL;->E:Z

    if-eqz v0, :cond_3

    .line 2154314
    iget-object v0, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    invoke-static {p0, v0}, LX/EfL;->a$redex0(LX/EfL;Lcom/facebook/audience/model/ReplyThread;)V

    .line 2154315
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EfL;->E:Z

    .line 2154316
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EfL;->C:Z

    .line 2154317
    iget-object v0, p0, LX/EfL;->k:LX/0he;

    iget-object v1, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    .line 2154318
    iget-object v2, v1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v2

    .line 2154319
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    .line 2154320
    iget-object v3, v2, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2154321
    iget-object v3, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    .line 2154322
    iget-object p0, v3, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v3, p0

    .line 2154323
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/0he;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2154324
    :cond_4
    return-void

    .line 2154325
    :cond_5
    iget-object v1, v0, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v1, v1

    .line 2154326
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2154327
    invoke-static {p0}, LX/EfL;->i(LX/EfL;)V

    .line 2154328
    iget-object v1, p0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    .line 2154329
    invoke-static {v1}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->f(Lcom/facebook/audience/ui/ReplyThreadStoryView;)V

    .line 2154330
    goto :goto_0

    .line 2154331
    :cond_6
    invoke-virtual {p0}, LX/EfL;->e()V

    .line 2154332
    iget-object v1, p0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    invoke-virtual {v1}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->a()V

    goto :goto_0
.end method

.method public static g(LX/EfL;)Z
    .locals 1

    .prologue
    .line 2154293
    iget-object v0, p0, LX/EfL;->v:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-virtual {v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2154294
    invoke-virtual {p0}, LX/EfL;->e()V

    .line 2154295
    iget-object v0, p0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    invoke-virtual {v0}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->a()V

    .line 2154296
    const/4 v0, 0x1

    .line 2154297
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/EfL;)V
    .locals 6

    .prologue
    .line 2154282
    iget-object v0, p0, LX/EfL;->A:LX/EfK;

    if-eqz v0, :cond_1

    .line 2154283
    iget-object v0, p0, LX/EfL;->d:LX/AFO;

    iget-object v1, p0, LX/EfL;->A:LX/EfK;

    .line 2154284
    iget-object v2, v0, LX/AFO;->e:LX/7gh;

    invoke-virtual {v2, v1}, LX/7gh;->b(Ljava/lang/Object;)V

    .line 2154285
    iget-object v2, v1, LX/EfK;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2154286
    iget-object v3, v0, LX/AFO;->i:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/AFO;->e:LX/7gh;

    invoke-virtual {v3}, LX/7gh;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2154287
    iget-object v3, v0, LX/AFO;->i:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gM;

    .line 2154288
    iget-object v4, v0, LX/AFO;->c:LX/AHZ;

    .line 2154289
    iget-object v5, v4, LX/AHZ;->e:LX/0gX;

    invoke-static {v3}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/0gX;->a(Ljava/util/Set;)V

    .line 2154290
    iget-object v3, v0, LX/AFO;->j:LX/AFP;

    invoke-virtual {v3, v2}, LX/AFP;->b(Ljava/lang/String;)V

    .line 2154291
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/EfL;->A:LX/EfK;

    .line 2154292
    :cond_1
    return-void
.end method

.method public static i(LX/EfL;)V
    .locals 3

    .prologue
    .line 2154264
    iget-object v0, p0, LX/EfL;->B:LX/AL7;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EfL;->B:LX/AL7;

    .line 2154265
    iget v1, v0, LX/AL7;->j:I

    move v0, v1

    .line 2154266
    if-lez v0, :cond_0

    .line 2154267
    iget-object v0, p0, LX/EfL;->B:LX/AL7;

    .line 2154268
    iget v1, v0, LX/AL7;->j:I

    move v0, v1

    .line 2154269
    invoke-static {p0, v0}, LX/EfL;->a$redex0(LX/EfL;I)V

    .line 2154270
    :cond_0
    iget-object v0, p0, LX/EfL;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2154271
    iget-object v0, p0, LX/EfL;->v:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-virtual {v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->a()V

    .line 2154272
    iget-object v0, p0, LX/EfL;->u:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2154273
    iget-object v0, p0, LX/EfL;->y:LX/EfA;

    if-eqz v0, :cond_1

    .line 2154274
    iget-object v0, p0, LX/EfL;->y:LX/EfA;

    .line 2154275
    iget-object v1, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v1, v1, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->f:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    iget-object v2, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    .line 2154276
    iget v0, v2, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->h:I

    move v2, v0

    .line 2154277
    invoke-virtual {v1, v2}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->setIndicatorSegmentProgressToMax(I)V

    .line 2154278
    :cond_1
    iget-object v0, p0, LX/EfL;->o:Lcom/facebook/audience/ui/ReplyRecyclerView;

    const/4 v1, 0x0

    .line 2154279
    iput-boolean v1, v0, Lcom/facebook/audience/ui/ReplyRecyclerView;->k:Z

    .line 2154280
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EfL;->F:Z

    .line 2154281
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2154254
    iput-boolean v1, p0, LX/EfL;->D:Z

    .line 2154255
    iget-boolean v0, p0, LX/EfL;->C:Z

    if-nez v0, :cond_1

    .line 2154256
    :cond_0
    :goto_0
    return-void

    .line 2154257
    :cond_1
    iput-boolean v1, p0, LX/EfL;->C:Z

    .line 2154258
    iget-object v0, p0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    .line 2154259
    invoke-static {v0}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->f(Lcom/facebook/audience/ui/ReplyThreadStoryView;)V

    .line 2154260
    iget-object v0, p0, LX/EfL;->B:LX/AL7;

    if-eqz v0, :cond_0

    .line 2154261
    iget-object v0, p0, LX/EfL;->B:LX/AL7;

    const/4 v1, 0x0

    .line 2154262
    iput-object v1, v0, LX/AL7;->g:LX/AL6;

    .line 2154263
    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2154251
    invoke-virtual {p0}, LX/EfL;->c()V

    .line 2154252
    invoke-static {p0}, LX/EfL;->h(LX/EfL;)V

    .line 2154253
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2154243
    iget-object v0, p0, LX/EfL;->v:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-virtual {v0}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->b()V

    .line 2154244
    iget-object v0, p0, LX/EfL;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2154245
    iget-object v0, p0, LX/EfL;->o:Lcom/facebook/audience/ui/ReplyRecyclerView;

    const/4 v1, 0x1

    .line 2154246
    iput-boolean v1, v0, Lcom/facebook/audience/ui/ReplyRecyclerView;->k:Z

    .line 2154247
    iput-boolean v2, p0, LX/EfL;->F:Z

    .line 2154248
    iget-object v0, p0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    invoke-direct {p0, v0}, LX/EfL;->b(Lcom/facebook/audience/model/ReplyThread;)V

    .line 2154249
    invoke-static {p0, v2}, LX/EfL;->a$redex0(LX/EfL;I)V

    .line 2154250
    return-void
.end method

.method public setReplyThreadGestureController(LX/AKu;)V
    .locals 1
    .param p1    # LX/AKu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2154237
    if-eqz p1, :cond_0

    .line 2154238
    iget-object v0, p0, LX/EfL;->J:LX/AKt;

    .line 2154239
    iput-object v0, p1, LX/AKu;->j:LX/AKt;

    .line 2154240
    :cond_0
    iget-object v0, p0, LX/EfL;->o:Lcom/facebook/audience/ui/ReplyRecyclerView;

    .line 2154241
    iput-object p1, v0, Lcom/facebook/audience/ui/ReplyRecyclerView;->h:LX/AKu;

    .line 2154242
    return-void
.end method
