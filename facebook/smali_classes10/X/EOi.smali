.class public final LX/EOi;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EOj;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "LX/8d2;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:LX/EOS;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public final synthetic f:LX/EOj;


# direct methods
.method public constructor <init>(LX/EOj;)V
    .locals 1

    .prologue
    .line 2115041
    iput-object p1, p0, LX/EOi;->f:LX/EOj;

    .line 2115042
    move-object v0, p1

    .line 2115043
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2115044
    const-string v0, ""

    iput-object v0, p0, LX/EOi;->d:Ljava/lang/String;

    .line 2115045
    sget-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object v0, p0, LX/EOi;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2115046
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2115047
    const-string v0, "SearchResultsPhotoWithCaptionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2115048
    if-ne p0, p1, :cond_1

    .line 2115049
    :cond_0
    :goto_0
    return v0

    .line 2115050
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2115051
    goto :goto_0

    .line 2115052
    :cond_3
    check-cast p1, LX/EOi;

    .line 2115053
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2115054
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2115055
    if-eq v2, v3, :cond_0

    .line 2115056
    iget-object v2, p0, LX/EOi;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EOi;->a:LX/CzL;

    iget-object v3, p1, LX/EOi;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2115057
    goto :goto_0

    .line 2115058
    :cond_5
    iget-object v2, p1, LX/EOi;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2115059
    :cond_6
    iget-object v2, p0, LX/EOi;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EOi;->b:Ljava/lang/String;

    iget-object v3, p1, LX/EOi;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2115060
    goto :goto_0

    .line 2115061
    :cond_8
    iget-object v2, p1, LX/EOi;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2115062
    :cond_9
    iget-object v2, p0, LX/EOi;->c:LX/EOS;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/EOi;->c:LX/EOS;

    iget-object v3, p1, LX/EOi;->c:LX/EOS;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2115063
    goto :goto_0

    .line 2115064
    :cond_b
    iget-object v2, p1, LX/EOi;->c:LX/EOS;

    if-nez v2, :cond_a

    .line 2115065
    :cond_c
    iget-object v2, p0, LX/EOi;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/EOi;->d:Ljava/lang/String;

    iget-object v3, p1, LX/EOi;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2115066
    goto :goto_0

    .line 2115067
    :cond_e
    iget-object v2, p1, LX/EOi;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2115068
    :cond_f
    iget-object v2, p0, LX/EOi;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/EOi;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v3, p1, LX/EOi;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2115069
    goto :goto_0

    .line 2115070
    :cond_10
    iget-object v2, p1, LX/EOi;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
