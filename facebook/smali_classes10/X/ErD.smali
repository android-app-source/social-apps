.class public final LX/ErD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventPotentialGuestEntriesTokenQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ErF;


# direct methods
.method public constructor <init>(LX/ErF;)V
    .locals 0

    .prologue
    .line 2172680
    iput-object p1, p0, LX/ErD;->a:LX/ErF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2172681
    iget-object v0, p0, LX/ErD;->a:LX/ErF;

    iget-object v0, v0, LX/ErF;->d:LX/0tX;

    iget-object v1, p0, LX/ErD;->a:LX/ErF;

    iget-object v2, p0, LX/ErD;->a:LX/ErF;

    iget-object v2, v2, LX/ErF;->h:Ljava/util/List;

    .line 2172682
    new-instance v3, LX/7o3;

    invoke-direct {v3}, LX/7o3;-><init>()V

    move-object v3, v3

    .line 2172683
    const-string v4, "event_id"

    iget-object p0, v1, LX/ErF;->g:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2172684
    const-string v4, "tokens"

    invoke-virtual {v3, v4, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2172685
    const-string v4, "first_count"

    const/16 p0, 0xa

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2172686
    const-string v4, "after_cursor"

    iget-object p0, v1, LX/ErF;->i:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2172687
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    move-object v1, v3

    .line 2172688
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
