.class public final LX/Dl0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2036747
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_a

    .line 2036748
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2036749
    :goto_0
    return v1

    .line 2036750
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2036751
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 2036752
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2036753
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2036754
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 2036755
    const-string v9, "address"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2036756
    const/4 v8, 0x0

    .line 2036757
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_e

    .line 2036758
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2036759
    :goto_2
    move v7, v8

    .line 2036760
    goto :goto_1

    .line 2036761
    :cond_2
    const-string v9, "all_phones"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2036762
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2036763
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_3

    .line 2036764
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_3

    .line 2036765
    const/4 v9, 0x0

    .line 2036766
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v10, :cond_12

    .line 2036767
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2036768
    :goto_4
    move v8, v9

    .line 2036769
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2036770
    :cond_3
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 2036771
    goto :goto_1

    .line 2036772
    :cond_4
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2036773
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2036774
    :cond_5
    const-string v9, "location"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2036775
    invoke-static {p0, p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2036776
    :cond_6
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2036777
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 2036778
    :cond_7
    const-string v9, "profile_picture"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 2036779
    invoke-static {p0, p1}, LX/Dkz;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 2036780
    :cond_8
    const-string v9, "username"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2036781
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 2036782
    :cond_9
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2036783
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2036784
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2036785
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2036786
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2036787
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2036788
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2036789
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2036790
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1

    .line 2036791
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2036792
    :cond_c
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_d

    .line 2036793
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2036794
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2036795
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_c

    if-eqz v9, :cond_c

    .line 2036796
    const-string v10, "single_line_full_address"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 2036797
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_5

    .line 2036798
    :cond_d
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2036799
    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 2036800
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_2

    :cond_e
    move v7, v8

    goto :goto_5

    .line 2036801
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2036802
    :cond_10
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_11

    .line 2036803
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2036804
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2036805
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_10

    if-eqz v10, :cond_10

    .line 2036806
    const-string v11, "phone_number"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 2036807
    invoke-static {p0, p1}, LX/Dky;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_6

    .line 2036808
    :cond_11
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2036809
    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 2036810
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto/16 :goto_4

    :cond_12
    move v8, v9

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2036811
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2036812
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2036813
    if-eqz v0, :cond_1

    .line 2036814
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036815
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2036816
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2036817
    if-eqz v1, :cond_0

    .line 2036818
    const-string v2, "single_line_full_address"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036819
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2036820
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2036821
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2036822
    if-eqz v0, :cond_4

    .line 2036823
    const-string v1, "all_phones"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036824
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2036825
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 2036826
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 2036827
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2036828
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2036829
    if-eqz v3, :cond_2

    .line 2036830
    const-string v4, "phone_number"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036831
    invoke-static {p0, v3, p2}, LX/Dky;->a(LX/15i;ILX/0nX;)V

    .line 2036832
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2036833
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2036834
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2036835
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2036836
    if-eqz v0, :cond_5

    .line 2036837
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036838
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2036839
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2036840
    if-eqz v0, :cond_6

    .line 2036841
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036842
    invoke-static {p0, v0, p2}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 2036843
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2036844
    if-eqz v0, :cond_7

    .line 2036845
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036846
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2036847
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2036848
    if-eqz v0, :cond_8

    .line 2036849
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036850
    invoke-static {p0, v0, p2}, LX/Dkz;->a(LX/15i;ILX/0nX;)V

    .line 2036851
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2036852
    if-eqz v0, :cond_9

    .line 2036853
    const-string v1, "username"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036854
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2036855
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2036856
    return-void
.end method
