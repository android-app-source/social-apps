.class public LX/Cvh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1949132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)LX/0P1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1949133
    const-string v0, "entity_id"

    const-string v1, "style"

    invoke-static {v0, p0, v1, p1}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            ")",
            "LX/0Px",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1949134
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1949135
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1949136
    :goto_0
    return-object v0

    .line 1949137
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    .line 1949138
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1949139
    invoke-static {p0}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 1949140
    if-nez v2, :cond_1

    .line 1949141
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1949142
    goto :goto_0

    .line 1949143
    :cond_1
    const v4, -0x5389cb28

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    if-ne v4, v5, :cond_4

    .line 1949144
    invoke-static {p0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_6

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 1949145
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1949146
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-static {v0}, LX/Cvh;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Ljava/lang/String;

    move-result-object v0

    .line 1949147
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    if-eqz v0, :cond_2

    .line 1949148
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/Cvh;->a(Ljava/lang/String;Ljava/lang/String;)LX/0P1;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1949149
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1949150
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 1949151
    :cond_4
    invoke-static {p0}, LX/8eM;->c(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_MIXED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    const v4, 0x4ed245b

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    if-eq v4, v5, :cond_5

    const v4, 0x1eaef984

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    if-eq v4, v5, :cond_5

    const v4, -0x4fc08d5

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    if-ne v4, v2, :cond_7

    .line 1949152
    :cond_5
    if-nez v1, :cond_9

    .line 1949153
    const/4 v0, 0x0

    .line 1949154
    :goto_3
    move-object v0, v0

    .line 1949155
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->dW_()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    if-eqz v0, :cond_6

    .line 1949156
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->dW_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/Cvh;->a(Ljava/lang/String;Ljava/lang/String;)LX/0P1;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1949157
    :cond_6
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 1949158
    :cond_7
    invoke-static {p0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_4
    if-ge v1, v4, :cond_6

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 1949159
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1949160
    invoke-static {v0}, LX/Cvh;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Ljava/lang/String;

    move-result-object v5

    .line 1949161
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_8

    if-eqz v5, :cond_8

    .line 1949162
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, LX/Cvh;->a(Ljava/lang/String;Ljava/lang/String;)LX/0P1;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1949163
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1949164
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ap()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1949165
    sget-object v0, LX/Cvg;->LIVE:LX/Cvg;

    invoke-virtual {v0}, LX/Cvg;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1949166
    :cond_a
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x1eaef984

    if-ne v0, v2, :cond_b

    .line 1949167
    sget-object v0, LX/Cvg;->WEB:LX/Cvg;

    invoke-virtual {v0}, LX/Cvg;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1949168
    :cond_b
    sget-object v0, LX/Cvg;->NATIVE:LX/Cvg;

    invoke-virtual {v0}, LX/Cvg;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1949169
    if-nez p0, :cond_0

    .line 1949170
    const/4 v0, 0x0

    .line 1949171
    :goto_0
    return-object v0

    .line 1949172
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ap()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1949173
    sget-object v0, LX/Cvg;->LIVE:LX/Cvg;

    invoke-virtual {v0}, LX/Cvg;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1949174
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x1eaef984

    if-ne v0, v1, :cond_2

    .line 1949175
    sget-object v0, LX/Cvg;->WEB:LX/Cvg;

    invoke-virtual {v0}, LX/Cvg;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1949176
    :cond_2
    sget-object v0, LX/Cvg;->NATIVE:LX/Cvg;

    invoke-virtual {v0}, LX/Cvg;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
