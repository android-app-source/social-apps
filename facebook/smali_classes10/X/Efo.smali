.class public abstract LX/Efo;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# instance fields
.field public final a:LX/AKt;

.field public final b:LX/Efl;

.field public final c:LX/AKk;

.field public final d:LX/AKw;

.field public e:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

.field public f:Lcom/facebook/audience/ui/ReplyThreadStoryView;

.field public g:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

.field public h:LX/Efj;

.field public i:Z

.field public j:LX/AHu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/AKN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;

.field public m:LX/Efs;

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155324
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155325
    new-instance v0, LX/Efk;

    invoke-direct {v0, p0}, LX/Efk;-><init>(LX/Efo;)V

    iput-object v0, p0, LX/Efo;->a:LX/AKt;

    .line 2155326
    new-instance v0, LX/Efl;

    invoke-direct {v0, p0}, LX/Efl;-><init>(LX/Efo;)V

    iput-object v0, p0, LX/Efo;->b:LX/Efl;

    .line 2155327
    new-instance v0, LX/Efm;

    invoke-direct {v0, p0}, LX/Efm;-><init>(LX/Efo;)V

    iput-object v0, p0, LX/Efo;->c:LX/AKk;

    .line 2155328
    new-instance v0, LX/Efn;

    invoke-direct {v0, p0}, LX/Efn;-><init>(LX/Efo;)V

    iput-object v0, p0, LX/Efo;->d:LX/AKw;

    .line 2155329
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Efo;

    invoke-static {v0}, LX/AHu;->b(LX/0QB;)LX/AHu;

    move-result-object p2

    check-cast p2, LX/AHu;

    invoke-static {v0}, LX/AKN;->a(LX/0QB;)LX/AKN;

    move-result-object v0

    check-cast v0, LX/AKN;

    iput-object p2, p0, LX/Efo;->j:LX/AHu;

    iput-object v0, p0, LX/Efo;->k:LX/AKN;

    .line 2155330
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2155369
    const v0, 0x7f0d06ae

    invoke-virtual {p0, v0}, LX/Efo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/ReplyThreadStoryView;

    iput-object v0, p0, LX/Efo;->f:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    .line 2155370
    iget-object v0, p0, LX/Efo;->f:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    iget-object v1, p0, LX/Efo;->d:LX/AKw;

    .line 2155371
    iput-object v1, v0, Lcom/facebook/audience/ui/ReplyThreadStoryView;->j:LX/AKw;

    .line 2155372
    iget-object v0, p0, LX/Efo;->f:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->setLoading(Z)V

    .line 2155373
    const v0, 0x7f0d2ccc

    invoke-virtual {p0, v0}, LX/Efo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    iput-object v0, p0, LX/Efo;->g:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    .line 2155374
    const v0, 0x7f0d2ccd

    invoke-virtual {p0, v0}, LX/Efo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    iput-object v0, p0, LX/Efo;->e:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    .line 2155375
    iget-object v0, p0, LX/Efo;->e:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    iget-object v1, p0, LX/Efo;->c:LX/AKk;

    .line 2155376
    iput-object v1, v0, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->f:LX/AKk;

    .line 2155377
    const v0, 0x7f0d2cce

    invoke-virtual {p0, v0}, LX/Efo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;

    iput-object v0, p0, LX/Efo;->l:Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;

    .line 2155378
    iget-object v0, p0, LX/Efo;->l:Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;

    iget-object v1, p0, LX/Efo;->a:LX/AKt;

    .line 2155379
    iput-object v1, v0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->b:LX/AKt;

    .line 2155380
    return-void
.end method

.method public abstract a(I)V
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 2155368
    return-void
.end method

.method public final a(ZI)V
    .locals 1

    .prologue
    .line 2155364
    iget-object v0, p0, LX/Efo;->g:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    invoke-virtual {v0, p2}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->setCurrentSegmentIndex(I)V

    .line 2155365
    if-nez p1, :cond_0

    .line 2155366
    iget-object v0, p0, LX/Efo;->g:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    invoke-virtual {v0, p2}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->setIndicatorSegmentProgressToMax(I)V

    .line 2155367
    :cond_0
    return-void
.end method

.method public abstract b()Z
.end method

.method public c()V
    .locals 0

    .prologue
    .line 2155363
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 2155360
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Efo;->i:Z

    .line 2155361
    invoke-virtual {p0}, LX/Efo;->g()V

    .line 2155362
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 2155357
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Efo;->i:Z

    .line 2155358
    invoke-virtual {p0}, LX/Efo;->h()V

    .line 2155359
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2155346
    iget-boolean v0, p0, LX/Efo;->n:Z

    if-nez v0, :cond_0

    .line 2155347
    iget-object v0, p0, LX/Efo;->f:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    invoke-virtual {v0}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->a()V

    .line 2155348
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Efo;->n:Z

    .line 2155349
    invoke-virtual {p0}, LX/Efo;->getCurrentThreadId()Ljava/lang/String;

    move-result-object v0

    .line 2155350
    iget-object v1, p0, LX/Efo;->j:LX/AHu;

    .line 2155351
    invoke-static {v1, v0}, LX/AHu;->d(LX/AHu;Ljava/lang/String;)V

    .line 2155352
    iget-object v1, p0, LX/Efo;->k:LX/AKN;

    .line 2155353
    iget-object v2, v1, LX/AKN;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2155354
    iget-object v2, v1, LX/AKN;->b:LX/AJx;

    .line 2155355
    iget-object p0, v2, LX/AJx;->a:LX/AK0;

    const/4 v1, 0x0

    sget-object v0, LX/AJz;->OPTIMISTIC_UPDATE:LX/AJz;

    invoke-virtual {p0, v1, v0}, LX/AK0;->a(ZLX/AJz;)V

    .line 2155356
    :cond_0
    return-void
.end method

.method public abstract getCurrentThreadId()Ljava/lang/String;
.end method

.method public abstract getCurrentThreadMediaType()LX/7gk;
.end method

.method public abstract getNumberOfStoriesInCurrentUserBucket()I
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2155341
    iget-boolean v0, p0, LX/Efo;->n:Z

    if-eqz v0, :cond_0

    .line 2155342
    iget-object v0, p0, LX/Efo;->f:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    .line 2155343
    invoke-static {v0}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->f(Lcom/facebook/audience/ui/ReplyThreadStoryView;)V

    .line 2155344
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Efo;->n:Z

    .line 2155345
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 2155337
    invoke-virtual {p0}, LX/Efo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2155338
    iget-object v0, p0, LX/Efo;->g:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    invoke-virtual {p0}, LX/Efo;->getNumberOfStoriesInCurrentUserBucket()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->a(I)V

    .line 2155339
    :goto_0
    return-void

    .line 2155340
    :cond_0
    iget-object v0, p0, LX/Efo;->g:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setReplyThreadGestureController(LX/AKu;)V
    .locals 1

    .prologue
    .line 2155331
    if-eqz p1, :cond_0

    .line 2155332
    iget-object v0, p0, LX/Efo;->a:LX/AKt;

    .line 2155333
    iput-object v0, p1, LX/AKu;->j:LX/AKt;

    .line 2155334
    :cond_0
    iget-object v0, p0, LX/Efo;->l:Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;

    .line 2155335
    iput-object p1, v0, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->a:LX/AKu;

    .line 2155336
    return-void
.end method
