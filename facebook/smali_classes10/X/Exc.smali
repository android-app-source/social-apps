.class public LX/Exc;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "LX/BcS;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Exf;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Exc",
            "<TTUserInfo;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Exf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2185027
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2185028
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Exc;->b:LX/0Zi;

    .line 2185029
    iput-object p1, p0, LX/Exc;->a:LX/0Ot;

    .line 2185030
    return-void
.end method

.method public static a(LX/0QB;)LX/Exc;
    .locals 4

    .prologue
    .line 2185031
    const-class v1, LX/Exc;

    monitor-enter v1

    .line 2185032
    :try_start_0
    sget-object v0, LX/Exc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2185033
    sput-object v2, LX/Exc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2185034
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2185035
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2185036
    new-instance v3, LX/Exc;

    const/16 p0, 0x2243

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Exc;-><init>(LX/0Ot;)V

    .line 2185037
    move-object v0, v3

    .line 2185038
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2185039
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Exc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2185040
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2185041
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/BcP;Z)V
    .locals 3

    .prologue
    .line 2185042
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 2185043
    if-nez v0, :cond_0

    .line 2185044
    :goto_0
    return-void

    .line 2185045
    :cond_0
    check-cast v0, LX/Exa;

    .line 2185046
    new-instance v1, LX/Exb;

    iget-object v2, v0, LX/Exa;->d:LX/Exc;

    invoke-direct {v1, v2, p1}, LX/Exb;-><init>(LX/Exc;Z)V

    move-object v0, v1

    .line 2185047
    invoke-virtual {p0, v0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 2185048
    invoke-static {}, LX/1dS;->b()V

    .line 2185049
    iget v0, p1, LX/BcQ;->b:I

    .line 2185050
    sparse-switch v0, :sswitch_data_0

    move-object v0, v6

    .line 2185051
    :goto_0
    return-object v0

    .line 2185052
    :sswitch_0
    check-cast p2, LX/BdG;

    .line 2185053
    iget-object v2, p2, LX/BdG;->b:Ljava/lang/Object;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v4

    check-cast v0, LX/BcP;

    .line 2185054
    iget-object v4, p0, LX/Exc;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Exf;

    .line 2185055
    iget-object p0, v4, LX/Exf;->b:LX/EuJ;

    const/4 v1, 0x0

    .line 2185056
    new-instance v3, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;

    invoke-direct {v3, p0}, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;-><init>(LX/EuJ;)V

    .line 2185057
    sget-object v4, LX/EuJ;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EuI;

    .line 2185058
    if-nez v4, :cond_0

    .line 2185059
    new-instance v4, LX/EuI;

    invoke-direct {v4}, LX/EuI;-><init>()V

    .line 2185060
    :cond_0
    invoke-static {v4, v0, v1, v1, v3}, LX/EuI;->a$redex0(LX/EuI;LX/1De;IILcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;)V

    .line 2185061
    move-object v3, v4

    .line 2185062
    move-object v1, v3

    .line 2185063
    move-object p0, v1

    .line 2185064
    check-cast v2, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2185065
    iget-object v1, p0, LX/EuI;->a:Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;

    iput-object v2, v1, Lcom/facebook/friending/center/components/FriendSuggestionItemComponent$FriendSuggestionItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2185066
    iget-object v1, p0, LX/EuI;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2185067
    move-object p0, p0

    .line 2185068
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    move-object v4, p0

    .line 2185069
    move-object v0, v4

    .line 2185070
    goto :goto_0

    .line 2185071
    :sswitch_1
    check-cast p2, LX/BcM;

    .line 2185072
    iget-boolean v1, p2, LX/BcM;->a:Z

    iget-object v2, p2, LX/BcM;->b:LX/BcL;

    iget-object v3, p2, LX/BcM;->c:Ljava/lang/Throwable;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v4, v0, v4

    check-cast v4, LX/BcP;

    move-object v0, p0

    .line 2185073
    iget-object p0, v0, LX/Exc;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2185074
    sget-object p0, LX/Exe;->a:[I

    invoke-virtual {v2}, LX/BcL;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_0

    .line 2185075
    :goto_1
    invoke-static {v4, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 2185076
    move-object v0, v6

    .line 2185077
    goto :goto_0

    .line 2185078
    :pswitch_0
    invoke-static {v4, v5}, LX/Exc;->a(LX/BcP;Z)V

    goto :goto_1

    .line 2185079
    :pswitch_1
    const/4 p0, 0x1

    invoke-static {v4, p0}, LX/Exc;->a(LX/BcP;Z)V

    goto :goto_1

    .line 2185080
    :pswitch_2
    invoke-static {v4, v5}, LX/Exc;->a(LX/BcP;Z)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x622d4a04 -> :sswitch_1
        -0x269497e6 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 2185081
    check-cast p1, LX/Exa;

    .line 2185082
    check-cast p2, LX/Exa;

    .line 2185083
    iget-boolean v0, p1, LX/Exa;->b:Z

    iput-boolean v0, p2, LX/Exa;->b:Z

    .line 2185084
    return-void
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 4

    .prologue
    .line 2185085
    check-cast p3, LX/Exa;

    .line 2185086
    iget-object v0, p0, LX/Exc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Exf;

    iget-boolean v1, p3, LX/Exa;->b:Z

    iget-object v2, p3, LX/Exa;->c:LX/95R;

    .line 2185087
    const/4 v3, 0x0

    .line 2185088
    :try_start_0
    invoke-virtual {v2}, LX/95R;->a()LX/2kW;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2185089
    :goto_0
    if-eqz v3, :cond_0

    .line 2185090
    invoke-static {}, LX/Bck;->d()LX/Bck;

    move-result-object v3

    invoke-virtual {v3, p1}, LX/Bck;->c(LX/BcP;)LX/Bcg;

    move-result-object v3

    invoke-virtual {v2}, LX/95R;->a()LX/2kW;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/Bcg;->a(LX/2kW;)LX/Bcg;

    move-result-object v3

    const/16 p0, 0x14

    invoke-virtual {v3, p0}, LX/Bcg;->a(I)LX/Bcg;

    move-result-object v3

    .line 2185091
    const p0, -0x269497e6

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, p3, v2

    invoke-static {p1, p0, p3}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p0

    move-object p0, p0

    .line 2185092
    invoke-virtual {v3, p0}, LX/Bcg;->b(LX/BcQ;)LX/Bcg;

    move-result-object v3

    iget-object p0, v0, LX/Exf;->a:LX/Exd;

    .line 2185093
    iget-object p3, v3, LX/Bcg;->a:LX/Bch;

    iput-object p0, p3, LX/Bch;->g:LX/Exd;

    .line 2185094
    move-object v3, v3

    .line 2185095
    const p0, -0x622d4a04

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p3, v0

    invoke-static {p1, p0, p3}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p0

    move-object p0, p0

    .line 2185096
    invoke-virtual {v3, p0}, LX/Bcg;->c(LX/BcQ;)LX/Bcg;

    move-result-object v3

    invoke-virtual {v3}, LX/Bcg;->b()LX/BcO;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2185097
    :cond_0
    if-eqz v1, :cond_2

    .line 2185098
    invoke-static {p1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v3

    const/4 p0, 0x0

    .line 2185099
    new-instance p3, LX/Etv;

    invoke-direct {p3}, LX/Etv;-><init>()V

    .line 2185100
    sget-object v0, LX/Etw;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Etu;

    .line 2185101
    if-nez v0, :cond_1

    .line 2185102
    new-instance v0, LX/Etu;

    invoke-direct {v0}, LX/Etu;-><init>()V

    .line 2185103
    :cond_1
    invoke-static {v0, p1, p0, p0, p3}, LX/Etu;->a$redex0(LX/Etu;LX/1De;IILX/Etv;)V

    .line 2185104
    move-object p3, v0

    .line 2185105
    move-object p0, p3

    .line 2185106
    move-object p0, p0

    .line 2185107
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object v3

    invoke-virtual {v3}, LX/Bcc;->b()LX/BcO;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2185108
    :cond_2
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 2

    .prologue
    .line 2185109
    check-cast p2, LX/Exa;

    .line 2185110
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v0

    .line 2185111
    iget-object v1, p0, LX/Exc;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2185112
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2185113
    iput-object v1, v0, LX/1np;->a:Ljava/lang/Object;

    .line 2185114
    iget-object v1, v0, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2185115
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/Exa;->b:Z

    .line 2185116
    return-void
.end method
