.class public LX/D3b;
.super LX/4ok;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1960145
    invoke-direct {p0, p1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1960146
    sget-object v0, LX/1C0;->g:LX/0Tn;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/D3b;->setKey(Ljava/lang/String;)V

    .line 1960147
    const-string v0, "Prefetch every link regardless of server side value"

    invoke-virtual {p0, v0}, LX/D3b;->setSummary(Ljava/lang/CharSequence;)V

    .line 1960148
    const-string v0, "Prefetch Every Link "

    invoke-virtual {p0, v0}, LX/D3b;->setTitle(Ljava/lang/CharSequence;)V

    .line 1960149
    return-void
.end method
