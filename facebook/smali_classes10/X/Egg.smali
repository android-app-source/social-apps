.class public final LX/Egg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2157102
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2157103
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2157104
    :goto_0
    return v1

    .line 2157105
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2157106
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2157107
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2157108
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2157109
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2157110
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2157111
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2157112
    :cond_2
    const-string v5, "profile_picture"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2157113
    invoke-static {p0, p1}, LX/Egf;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2157114
    :cond_3
    const-string v5, "username"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2157115
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2157116
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2157117
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2157118
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2157119
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2157120
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2157121
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2157122
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2157123
    if-eqz v0, :cond_0

    .line 2157124
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157125
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2157126
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2157127
    if-eqz v0, :cond_1

    .line 2157128
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157129
    invoke-static {p0, v0, p2}, LX/Egf;->a(LX/15i;ILX/0nX;)V

    .line 2157130
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2157131
    if-eqz v0, :cond_2

    .line 2157132
    const-string v1, "username"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157133
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2157134
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2157135
    return-void
.end method
