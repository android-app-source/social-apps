.class public LX/E11;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/places/create/network/PlaceCreationParams;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2069185
    const-class v0, LX/E11;

    sput-object v0, LX/E11;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2069186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2069187
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2069188
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2069189
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2069190
    invoke-virtual {v1, v4, v5}, LX/162;->b(J)LX/162;

    goto :goto_0

    .line 2069191
    :cond_0
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 11

    .prologue
    .line 2069192
    check-cast p1, Lcom/facebook/places/create/network/PlaceCreationParams;

    .line 2069193
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2069194
    iget-object v0, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2069195
    invoke-virtual {p1}, Lcom/facebook/places/create/network/PlaceCreationParams;->a()Landroid/location/Location;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2069196
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "name"

    iget-object v3, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069197
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "coords"

    invoke-virtual {p1}, Lcom/facebook/places/create/network/PlaceCreationParams;->a()Landroid/location/Location;

    move-result-object v3

    .line 2069198
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 2069199
    const-string v8, "latitude"

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v9

    invoke-virtual {v7, v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2069200
    const-string v8, "longitude"

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v9

    invoke-virtual {v7, v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2069201
    invoke-virtual {v3}, Landroid/location/Location;->hasAccuracy()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2069202
    const-string v8, "accuracy"

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v9

    float-to-double v9, v9

    invoke-virtual {v7, v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2069203
    :cond_0
    invoke-virtual {v3}, Landroid/location/Location;->hasAltitude()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2069204
    const-string v8, "altitude"

    invoke-virtual {v3}, Landroid/location/Location;->getAltitude()D

    move-result-wide v9

    invoke-virtual {v7, v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2069205
    :cond_1
    invoke-virtual {v3}, Landroid/location/Location;->hasBearing()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2069206
    const-string v8, "heading"

    invoke-virtual {v3}, Landroid/location/Location;->getBearing()F

    move-result v9

    float-to-double v9, v9

    invoke-virtual {v7, v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2069207
    :cond_2
    invoke-virtual {v3}, Landroid/location/Location;->hasSpeed()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2069208
    const-string v8, "speed"

    invoke-virtual {v3}, Landroid/location/Location;->getSpeed()F

    move-result v9

    float-to-double v9, v9

    invoke-virtual {v7, v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2069209
    :cond_3
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 2069210
    :goto_0
    move-object v3, v7

    .line 2069211
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069212
    iget-object v0, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2069213
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "pin_source"

    iget-object v0, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/network/PlacePinAppId;

    invoke-virtual {v0}, Lcom/facebook/places/create/network/PlacePinAppId;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069214
    :cond_4
    const/4 v0, 0x0

    .line 2069215
    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->n:Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v2, :cond_5

    .line 2069216
    new-instance v0, Ljava/io/File;

    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2069217
    new-instance v2, LX/4ct;

    iget-object v3, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 2069218
    new-instance v0, LX/4cQ;

    const-string v3, "file"

    invoke-direct {v0, v3, v2}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 2069219
    :cond_5
    iget-boolean v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->k:Z

    if-eqz v2, :cond_6

    .line 2069220
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "type"

    const-string v4, "RESIDENCE"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069221
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "privacy"

    iget-object v4, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->l:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069222
    :cond_6
    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->c:LX/0Px;

    if-eqz v2, :cond_7

    .line 2069223
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "topics"

    iget-object v4, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->c:LX/0Px;

    invoke-static {v4}, LX/E11;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069224
    :cond_7
    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->m:LX/0Px;

    if-eqz v2, :cond_8

    .line 2069225
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "override_ids"

    iget-object v4, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->m:LX/0Px;

    invoke-static {v4}, LX/E11;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069226
    :cond_8
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069227
    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->f:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->f:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_9
    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->d:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->d:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 2069228
    :cond_a
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "address"

    iget-object v4, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->f:Ljava/lang/String;

    iget-object v5, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->d:Ljava/lang/String;

    iget-object v6, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->g:Ljava/lang/String;

    .line 2069229
    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-virtual {v7}, LX/0mC;->c()LX/0m9;

    move-result-object v7

    .line 2069230
    const-string v8, "city"

    invoke-virtual {v7, v8, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2069231
    const-string v8, "street"

    invoke-virtual {v7, v8, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2069232
    const-string v8, "postal_code"

    invoke-virtual {v7, v8, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2069233
    invoke-virtual {v7}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v4, v7

    .line 2069234
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069235
    :cond_b
    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->h:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 2069236
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "neighborhood_name"

    iget-object v4, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->h:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069237
    :cond_c
    iget-wide v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->e:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 2069238
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "city_id"

    iget-wide v4, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->e:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069239
    :cond_d
    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->i:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 2069240
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "phone"

    iget-object v4, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->i:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069241
    :cond_e
    iget-object v2, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->j:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 2069242
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "website"

    iget-object v4, p1, Lcom/facebook/places/create/network/PlaceCreationParams;->j:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069243
    :cond_f
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "places-create"

    .line 2069244
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2069245
    move-object v2, v2

    .line 2069246
    const-string v3, "POST"

    .line 2069247
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2069248
    move-object v2, v2

    .line 2069249
    const-string v3, "method/places.create"

    .line 2069250
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 2069251
    move-object v2, v2

    .line 2069252
    sget-object v3, LX/14S;->JSON:LX/14S;

    .line 2069253
    iput-object v3, v2, LX/14O;->k:LX/14S;

    .line 2069254
    move-object v2, v2

    .line 2069255
    iput-object v1, v2, LX/14O;->g:Ljava/util/List;

    .line 2069256
    move-object v1, v2

    .line 2069257
    if-eqz v0, :cond_10

    .line 2069258
    const/4 v2, 0x1

    new-array v2, v2, [LX/4cQ;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2069259
    iput-object v0, v1, LX/14O;->l:Ljava/util/List;

    .line 2069260
    :cond_10
    invoke-virtual {v1}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :catch_0
    const-string v7, ""

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2069261
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2069262
    invoke-virtual {v0}, LX/0lF;->D()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
