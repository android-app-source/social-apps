.class public final enum LX/CwO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CwO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CwO;

.field public static final enum DISABLED:LX/CwO;

.field public static final enum NONE:LX/CwO;

.field public static final enum WITH_SUBTITLE:LX/CwO;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1950484
    new-instance v0, LX/CwO;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v2}, LX/CwO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwO;->DISABLED:LX/CwO;

    .line 1950485
    new-instance v0, LX/CwO;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/CwO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwO;->NONE:LX/CwO;

    .line 1950486
    new-instance v0, LX/CwO;

    const-string v1, "WITH_SUBTITLE"

    invoke-direct {v0, v1, v4}, LX/CwO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwO;->WITH_SUBTITLE:LX/CwO;

    .line 1950487
    const/4 v0, 0x3

    new-array v0, v0, [LX/CwO;

    sget-object v1, LX/CwO;->DISABLED:LX/CwO;

    aput-object v1, v0, v2

    sget-object v1, LX/CwO;->NONE:LX/CwO;

    aput-object v1, v0, v3

    sget-object v1, LX/CwO;->WITH_SUBTITLE:LX/CwO;

    aput-object v1, v0, v4

    sput-object v0, LX/CwO;->$VALUES:[LX/CwO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1950483
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CwO;
    .locals 1

    .prologue
    .line 1950482
    const-class v0, LX/CwO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CwO;

    return-object v0
.end method

.method public static values()[LX/CwO;
    .locals 1

    .prologue
    .line 1950481
    sget-object v0, LX/CwO;->$VALUES:[LX/CwO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CwO;

    return-object v0
.end method
