.class public LX/DZB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/DZB;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2013191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2013192
    iput-object p1, p0, LX/DZB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2013193
    return-void
.end method

.method public static a(LX/0QB;)LX/DZB;
    .locals 4

    .prologue
    .line 2013195
    sget-object v0, LX/DZB;->b:LX/DZB;

    if-nez v0, :cond_1

    .line 2013196
    const-class v1, LX/DZB;

    monitor-enter v1

    .line 2013197
    :try_start_0
    sget-object v0, LX/DZB;->b:LX/DZB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2013198
    if-eqz v2, :cond_0

    .line 2013199
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2013200
    new-instance p0, LX/DZB;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/DZB;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2013201
    move-object v0, p0

    .line 2013202
    sput-object v0, LX/DZB;->b:LX/DZB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2013203
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2013204
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2013205
    :cond_1
    sget-object v0, LX/DZB;->b:LX/DZB;

    return-object v0

    .line 2013206
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2013207
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2013194
    iget-object v0, p0, LX/DZB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
