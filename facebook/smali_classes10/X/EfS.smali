.class public final LX/EfS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7h0;

.field public final synthetic b:LX/AFW;

.field public final synthetic c:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/7h0;LX/AFW;)V
    .locals 0

    .prologue
    .line 2154560
    iput-object p1, p0, LX/EfS;->c:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iput-object p2, p0, LX/EfS;->a:LX/7h0;

    iput-object p3, p0, LX/EfS;->b:LX/AFW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0x45e6eea7

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2154561
    iget-object v1, p0, LX/EfS;->c:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iget-object v1, v1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->A:LX/0he;

    iget-object v2, p0, LX/EfS;->a:LX/7h0;

    .line 2154562
    iget-object v3, v2, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v2, v3

    .line 2154563
    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    iget-object v4, p0, LX/EfS;->a:LX/7h0;

    .line 2154564
    iget-boolean v5, v4, LX/7h0;->l:Z

    move v4, v5

    .line 2154565
    iget-object v5, p0, LX/EfS;->b:LX/AFW;

    .line 2154566
    iget-object v6, v5, LX/AFW;->b:LX/0Px;

    move-object v5, v6

    .line 2154567
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    iget-object v6, p0, LX/EfS;->a:LX/7h0;

    .line 2154568
    iget-wide v9, v6, LX/7h0;->m:J

    move-wide v6, v9

    .line 2154569
    invoke-virtual/range {v1 .. v7}, LX/0he;->a(Ljava/lang/String;IZIJ)V

    .line 2154570
    iget-object v1, p0, LX/EfS;->c:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iget-object v1, v1, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->C:LX/AFQ;

    iget-object v2, p0, LX/EfS;->b:LX/AFW;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/AFQ;->a(LX/AFW;Landroid/content/Context;)V

    .line 2154571
    const v1, -0x61dc6d0c

    invoke-static {v8, v8, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
