.class public final LX/D1S;
.super LX/25T;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/25U;LX/25V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1956794
    invoke-direct {p0, p1, p2, p3}, LX/25T;-><init>(Landroid/content/Context;LX/25U;LX/25V;)V

    .line 1956795
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1P1;->b(I)V

    .line 1956796
    return-void
.end method

.method public static c(LX/0QB;)LX/D1S;
    .locals 4

    .prologue
    .line 1956797
    new-instance v3, LX/D1S;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/25U;->a(LX/0QB;)LX/25U;

    move-result-object v1

    check-cast v1, LX/25U;

    invoke-static {p0}, LX/25V;->a(LX/0QB;)LX/25V;

    move-result-object v2

    check-cast v2, LX/25V;

    invoke-direct {v3, v0, v1, v2}, LX/D1S;-><init>(Landroid/content/Context;LX/25U;LX/25V;)V

    .line 1956798
    return-object v3
.end method


# virtual methods
.method public final a(LX/1Od;LX/1Ok;II)V
    .locals 11

    .prologue
    .line 1956799
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1956800
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 1956801
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 1956802
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 1956803
    iget-object v0, p0, LX/25T;->d:LX/25V;

    iget-object v1, p0, LX/25T;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/25V;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1956804
    iget-object v0, p0, LX/25T;->d:LX/25V;

    iget-object v1, p0, LX/25T;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/25V;->a(Ljava/lang/String;)[I

    move-result-object v0

    .line 1956805
    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 1956806
    :goto_1
    packed-switch v3, :pswitch_data_1

    .line 1956807
    :goto_2
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-virtual {p0, v1, v0}, LX/1OR;->e(II)V

    .line 1956808
    return-void

    .line 1956809
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 1956810
    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v1

    if-lez v1, :cond_0

    .line 1956811
    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v6

    .line 1956812
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v6, :cond_3

    .line 1956813
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {p1, v1, v8, v9}, LX/25U;->a(LX/1Od;III)[I

    move-result-object v7

    iput-object v7, p0, LX/D1S;->b:[I

    .line 1956814
    iget v7, p0, LX/1P1;->j:I

    move v7, v7

    .line 1956815
    if-nez v7, :cond_2

    .line 1956816
    const/4 v7, 0x0

    aget v8, v0, v7

    iget-object v9, p0, LX/25T;->b:[I

    const/4 v10, 0x0

    aget v9, v9, v10

    add-int/2addr v8, v9

    aput v8, v0, v7

    .line 1956817
    const/4 v7, 0x1

    iget-object v8, p0, LX/25T;->b:[I

    const/4 v9, 0x1

    aget v8, v8, v9

    invoke-virtual {p0}, LX/1OR;->z()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {p0}, LX/1OR;->B()I

    move-result v9

    add-int/2addr v8, v9

    const/4 v9, 0x1

    aget v9, v0, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    aput v8, v0, v7

    .line 1956818
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1956819
    :cond_2
    const/4 v7, 0x1

    aget v8, v0, v7

    iget-object v9, p0, LX/25T;->b:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    add-int/2addr v8, v9

    aput v8, v0, v7

    .line 1956820
    const/4 v7, 0x0

    iget-object v8, p0, LX/25T;->b:[I

    const/4 v9, 0x0

    aget v8, v8, v9

    invoke-virtual {p0}, LX/1OR;->y()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {p0}, LX/1OR;->A()I

    move-result v9

    add-int/2addr v8, v9

    const/4 v9, 0x0

    aget v9, v0, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    aput v8, v0, v7

    goto :goto_4

    .line 1956821
    :cond_3
    iget-object v1, p0, LX/25T;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1956822
    iget-object v1, p0, LX/25T;->d:LX/25V;

    iget-object v6, p0, LX/25T;->a:Ljava/lang/String;

    invoke-virtual {v1, v6, v0}, LX/25V;->a(Ljava/lang/String;[I)V

    goto/16 :goto_0

    .line 1956823
    :pswitch_0
    const/4 v1, 0x0

    aput v4, v0, v1

    goto/16 :goto_1

    .line 1956824
    :pswitch_1
    const/4 v1, 0x1

    aput v5, v0, v1

    goto/16 :goto_2

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :pswitch_data_0
    .packed-switch 0x40000000
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x40000000
        :pswitch_1
    .end packed-switch
.end method
