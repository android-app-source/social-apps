.class public LX/Dv0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DwH;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DwH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2057966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057967
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Dv0;->c:Z

    .line 2057968
    iput-object p1, p0, LX/Dv0;->a:LX/0Ot;

    .line 2057969
    return-void
.end method

.method public static a(LX/Dv0;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;)Z
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2057959
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v3

    .line 2057960
    :goto_0
    return v0

    .line 2057961
    :cond_1
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    iget-object v1, p1, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2057962
    iget-object v1, p2, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2057963
    iget-object v2, p0, LX/Dv0;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DwH;

    invoke-virtual {v2, v0}, LX/DwH;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    .line 2057964
    iget-object v0, p0, LX/Dv0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DwH;

    invoke-virtual {v0, v1}, LX/DwH;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    .line 2057965
    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_0
.end method

.method public static b(LX/Dv0;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;)Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2057953
    invoke-static {p0, p1, p2}, LX/Dv0;->a(LX/Dv0;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2057954
    const/4 v0, 0x0

    .line 2057955
    :goto_0
    return-object v0

    .line 2057956
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2057957
    iget-object v0, p2, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2057958
    new-instance v0, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;-><init>(LX/0Px;)V

    goto :goto_0
.end method
