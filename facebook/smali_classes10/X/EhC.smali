.class public final enum LX/EhC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EhC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EhC;

.field public static final enum AD:LX/EhC;

.field public static final enum APP:LX/EhC;

.field public static final enum DEVELOPER:LX/EhC;

.field public static final enum FRIEND_LIST:LX/EhC;

.field public static final enum GROUP:LX/EhC;

.field public static final enum INTEREST_LIST:LX/EhC;

.field public static final enum PAGE:LX/EhC;

.field public static final enum PINNED:LX/EhC;

.field public static final enum PROFILE:LX/EhC;

.field public static final enum SETTINGS:LX/EhC;

.field public static final enum UNKNOWN:LX/EhC;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2158186
    new-instance v0, LX/EhC;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v3}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->PROFILE:LX/EhC;

    .line 2158187
    new-instance v0, LX/EhC;

    const-string v1, "PINNED"

    invoke-direct {v0, v1, v4}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->PINNED:LX/EhC;

    .line 2158188
    new-instance v0, LX/EhC;

    const-string v1, "AD"

    invoke-direct {v0, v1, v5}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->AD:LX/EhC;

    .line 2158189
    new-instance v0, LX/EhC;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v6}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->PAGE:LX/EhC;

    .line 2158190
    new-instance v0, LX/EhC;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v7}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->GROUP:LX/EhC;

    .line 2158191
    new-instance v0, LX/EhC;

    const-string v1, "APP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->APP:LX/EhC;

    .line 2158192
    new-instance v0, LX/EhC;

    const-string v1, "DEVELOPER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->DEVELOPER:LX/EhC;

    .line 2158193
    new-instance v0, LX/EhC;

    const-string v1, "FRIEND_LIST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->FRIEND_LIST:LX/EhC;

    .line 2158194
    new-instance v0, LX/EhC;

    const-string v1, "INTEREST_LIST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->INTEREST_LIST:LX/EhC;

    .line 2158195
    new-instance v0, LX/EhC;

    const-string v1, "SETTINGS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->SETTINGS:LX/EhC;

    .line 2158196
    new-instance v0, LX/EhC;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/EhC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EhC;->UNKNOWN:LX/EhC;

    .line 2158197
    const/16 v0, 0xb

    new-array v0, v0, [LX/EhC;

    sget-object v1, LX/EhC;->PROFILE:LX/EhC;

    aput-object v1, v0, v3

    sget-object v1, LX/EhC;->PINNED:LX/EhC;

    aput-object v1, v0, v4

    sget-object v1, LX/EhC;->AD:LX/EhC;

    aput-object v1, v0, v5

    sget-object v1, LX/EhC;->PAGE:LX/EhC;

    aput-object v1, v0, v6

    sget-object v1, LX/EhC;->GROUP:LX/EhC;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EhC;->APP:LX/EhC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EhC;->DEVELOPER:LX/EhC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EhC;->FRIEND_LIST:LX/EhC;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EhC;->INTEREST_LIST:LX/EhC;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/EhC;->SETTINGS:LX/EhC;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/EhC;->UNKNOWN:LX/EhC;

    aput-object v2, v0, v1

    sput-object v0, LX/EhC;->$VALUES:[LX/EhC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2158198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static lookup(Ljava/lang/String;)LX/EhC;
    .locals 1

    .prologue
    .line 2158199
    :try_start_0
    invoke-static {p0}, LX/EhC;->valueOf(Ljava/lang/String;)LX/EhC;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2158200
    :goto_0
    return-object v0

    :catch_0
    sget-object v0, LX/EhC;->UNKNOWN:LX/EhC;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/EhC;
    .locals 1

    .prologue
    .line 2158201
    const-class v0, LX/EhC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EhC;

    return-object v0
.end method

.method public static values()[LX/EhC;
    .locals 1

    .prologue
    .line 2158202
    sget-object v0, LX/EhC;->$VALUES:[LX/EhC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EhC;

    return-object v0
.end method
