.class public LX/Dd8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/2Ow;

.field private final b:LX/0aG;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019236
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Dd8;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Ow;LX/0aG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019238
    iput-object p1, p0, LX/Dd8;->a:LX/2Ow;

    .line 2019239
    iput-object p2, p0, LX/Dd8;->b:LX/0aG;

    .line 2019240
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/Dd8;->c:Ljava/util/Set;

    .line 2019241
    return-void
.end method
