.class public LX/D0T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field public b:LX/0WJ;

.field public c:LX/0lC;


# direct methods
.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/0WJ;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1955472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1955473
    iput-object p1, p0, LX/D0T;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 1955474
    iput-object p2, p0, LX/D0T;->b:LX/0WJ;

    .line 1955475
    iput-object p3, p0, LX/D0T;->c:LX/0lC;

    .line 1955476
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 1955477
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1955478
    const-string v1, "linkshim_click"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1955479
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1955480
    const-string v1, "linkshim_link_extra"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1955481
    iget-object v0, p0, LX/D0T;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 1955482
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1955483
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1955484
    if-eqz v0, :cond_0

    .line 1955485
    iget-object v1, p0, LX/D0T;->c:LX/0lC;

    invoke-static {v1, v0}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v4

    .line 1955486
    if-eqz v4, :cond_0

    .line 1955487
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/SessionCookie;

    .line 1955488
    invoke-virtual {v0}, Lcom/facebook/auth/credentials/SessionCookie;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1955489
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ";"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1955490
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1955491
    :cond_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1955492
    const-string v1, "Cookie"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1955493
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    .line 1955494
    iput-object v0, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1955495
    move-object v0, v1

    .line 1955496
    const-string v1, "linkshim"

    .line 1955497
    iput-object v1, v0, LX/15E;->c:Ljava/lang/String;

    .line 1955498
    move-object v0, v0

    .line 1955499
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1955500
    iput-object v1, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1955501
    move-object v0, v0

    .line 1955502
    new-instance v1, LX/D0S;

    invoke-direct {v1, p0}, LX/D0S;-><init>(LX/D0T;)V

    .line 1955503
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1955504
    move-object v0, v0

    .line 1955505
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 1955506
    iget-object v1, p0, LX/D0T;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    .line 1955507
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1955508
    move-object v0, v0

    .line 1955509
    return-object v0

    .line 1955510
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
