.class public LX/Dos;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field private static volatile c:LX/Dos;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Doc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042581
    const-class v0, LX/Dos;

    sput-object v0, LX/Dos;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Doc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042583
    iput-object p1, p0, LX/Dos;->b:LX/0Ot;

    .line 2042584
    return-void
.end method

.method public static a(LX/0QB;)LX/Dos;
    .locals 4

    .prologue
    .line 2042585
    sget-object v0, LX/Dos;->c:LX/Dos;

    if-nez v0, :cond_1

    .line 2042586
    const-class v1, LX/Dos;

    monitor-enter v1

    .line 2042587
    :try_start_0
    sget-object v0, LX/Dos;->c:LX/Dos;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2042588
    if-eqz v2, :cond_0

    .line 2042589
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2042590
    new-instance v3, LX/Dos;

    const/16 p0, 0x2a12

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Dos;-><init>(LX/0Ot;)V

    .line 2042591
    move-object v0, v3

    .line 2042592
    sput-object v0, LX/Dos;->c:LX/Dos;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042593
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2042594
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2042595
    :cond_1
    sget-object v0, LX/Dos;->c:LX/Dos;

    return-object v0

    .line 2042596
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2042597
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/Dos;)V
    .locals 3

    .prologue
    .line 2042598
    iget-object v0, p0, LX/Dos;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doc;

    .line 2042599
    iget-object v1, v0, LX/Doc;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Doj;

    .line 2042600
    iget-object v2, v1, LX/Doj;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    .line 2042601
    invoke-static {v1}, LX/Doj;->f(LX/Doj;)Ljava/lang/String;

    move-result-object v2

    .line 2042602
    if-eqz v2, :cond_0

    .line 2042603
    sget-object v0, LX/Don;->a:LX/0Tn;

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    invoke-interface {p0, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 2042604
    :cond_0
    sget-object v2, LX/Don;->a:LX/0Tn;

    invoke-interface {p0, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 2042605
    invoke-interface {p0}, LX/0hN;->commit()V

    .line 2042606
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2042607
    const-string v0, "SELECT encrypted_value FROM properties WHERE key=\'local_identity_key\'"

    invoke-virtual {p1, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    move v1, v2

    .line 2042608
    :goto_0
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2042609
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2042610
    invoke-static {v5, v0}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 2042611
    const-string v6, "encrypted_value"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 2042612
    :try_start_1
    iget-object v0, p0, LX/Dos;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doc;

    .line 2042613
    iget-object v8, v0, LX/Doc;->b:LX/1Hn;

    iget-object v7, v0, LX/Doc;->c:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/crypto/keychain/KeyChain;

    invoke-virtual {v8, v7}, LX/1Ho;->a(Lcom/facebook/crypto/keychain/KeyChain;)LX/1Hz;

    move-result-object v7

    .line 2042614
    iget-object v8, v0, LX/Doc;->d:LX/1Hb;

    invoke-virtual {v7, v6, v8}, LX/1Hz;->b([BLX/1Hb;)[B
    :try_end_1
    .catch LX/48e; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/Doi; {:try_start_1 .. :try_end_1} :catch_7
    .catch LX/48g; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/48f; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2042615
    goto :goto_0

    .line 2042616
    :catch_0
    move-exception v0

    .line 2042617
    :goto_1
    :try_start_2
    sget-object v1, LX/Dos;->a:Ljava/lang/Class;

    const-string v6, "Inaccessible encrypted data found in db; clearing data."

    invoke-static {v1, v6, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v3

    .line 2042618
    goto :goto_0

    .line 2042619
    :catch_1
    move-exception v0

    .line 2042620
    :goto_2
    sget-object v1, LX/Dos;->a:Ljava/lang/Class;

    const-string v6, "Upgrade to v24 failed trying legacy crypto check."

    invoke-static {v1, v6, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v1, v3

    .line 2042621
    goto :goto_0

    .line 2042622
    :cond_0
    if-eqz v5, :cond_1

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 2042623
    :cond_1
    if-eqz v1, :cond_4

    .line 2042624
    invoke-static {p0}, LX/Dos;->a(LX/Dos;)V

    move v0, v2

    .line 2042625
    :goto_3
    return v0

    .line 2042626
    :catch_2
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2042627
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_4
    if-eqz v5, :cond_2

    if-eqz v1, :cond_3

    :try_start_4
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_5
    throw v0

    :catch_3
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto :goto_5

    .line 2042628
    :cond_4
    new-instance v5, LX/0U1;

    const-string v0, "key"

    const-string v1, "TEXT"

    invoke-direct {v5, v0, v1}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042629
    new-instance v6, LX/0U1;

    const-string v0, "value"

    const-string v1, "BLOB"

    invoke-direct {v6, v0, v1}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042630
    invoke-static {v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2042631
    const-string v1, "keychain"

    new-instance v7, LX/0su;

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    invoke-direct {v7, v8}, LX/0su;-><init>(LX/0Px;)V

    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    invoke-static {v1, v0, v7}, LX/0Tz;->a(Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x1e54a9a1

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x489cca8b

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2042632
    iget-object v0, p0, LX/Dos;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doc;

    .line 2042633
    iget-object v1, v0, LX/Doc;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Doj;

    invoke-virtual {v1}, LX/Doj;->d()[B

    move-result-object v1

    move-object v1, v1

    .line 2042634
    if-eqz v1, :cond_5

    .line 2042635
    sget-object v0, LX/Doo;->c:LX/2PC;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    .line 2042636
    :goto_6
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2042637
    iget-object v7, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v7

    .line 2042638
    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042639
    iget-object v0, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2042640
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2042641
    const-string v0, "keychain"

    const v1, -0x7a7a3bc9

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x284a0a99

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2042642
    invoke-static {p0}, LX/Dos;->a(LX/Dos;)V

    move v0, v3

    .line 2042643
    goto/16 :goto_3

    .line 2042644
    :cond_5
    :try_start_5
    iget-object v0, p0, LX/Dos;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doc;

    .line 2042645
    iget-object v1, v0, LX/Doc;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Doj;

    invoke-virtual {v1}, LX/Doj;->e()[B

    move-result-object v1

    move-object v1, v1

    .line 2042646
    sget-object v0, LX/Doo;->d:LX/2PC;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;
    :try_end_5
    .catch LX/48g; {:try_start_5 .. :try_end_5} :catch_4

    move-result-object v0

    goto :goto_6

    .line 2042647
    :catch_4
    move-exception v0

    .line 2042648
    sget-object v1, LX/Dos;->a:Ljava/lang/Class;

    const-string v3, "Upgrade to v26 failed trying to retrieve V2 master key."

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2042649
    invoke-static {p0}, LX/Dos;->a(LX/Dos;)V

    move v0, v2

    .line 2042650
    goto/16 :goto_3

    .line 2042651
    :catchall_1
    move-exception v0

    move-object v1, v4

    goto/16 :goto_4

    .line 2042652
    :catch_5
    move-exception v0

    goto/16 :goto_2

    :catch_6
    move-exception v0

    goto/16 :goto_2

    .line 2042653
    :catch_7
    move-exception v0

    goto/16 :goto_1
.end method
