.class public LX/DpJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final user_id:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2044102
    new-instance v0, LX/1sv;

    const-string v1, "LookupPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpJ;->b:LX/1sv;

    .line 2044103
    new-instance v0, LX/1sw;

    const-string v1, "user_id"

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpJ;->c:LX/1sw;

    .line 2044104
    const/4 v0, 0x1

    sput-boolean v0, LX/DpJ;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2044105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2044106
    iput-object p1, p0, LX/DpJ;->user_id:Ljava/lang/Long;

    .line 2044107
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2044108
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2044109
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2044110
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2044111
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "LookupPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2044112
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044113
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044114
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044115
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044116
    const-string v4, "user_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044117
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044118
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044119
    iget-object v0, p0, LX/DpJ;->user_id:Ljava/lang/Long;

    if-nez v0, :cond_3

    .line 2044120
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044121
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044122
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044123
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2044124
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 2044125
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 2044126
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 2044127
    :cond_3
    iget-object v0, p0, LX/DpJ;->user_id:Ljava/lang/Long;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2044128
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2044129
    iget-object v0, p0, LX/DpJ;->user_id:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2044130
    sget-object v0, LX/DpJ;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044131
    iget-object v0, p0, LX/DpJ;->user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2044132
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2044133
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2044134
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2044135
    if-nez p1, :cond_1

    .line 2044136
    :cond_0
    :goto_0
    return v0

    .line 2044137
    :cond_1
    instance-of v1, p1, LX/DpJ;

    if-eqz v1, :cond_0

    .line 2044138
    check-cast p1, LX/DpJ;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2044139
    if-nez p1, :cond_3

    .line 2044140
    :cond_2
    :goto_1
    move v0, v2

    .line 2044141
    goto :goto_0

    .line 2044142
    :cond_3
    iget-object v0, p0, LX/DpJ;->user_id:Ljava/lang/Long;

    if-eqz v0, :cond_6

    move v0, v1

    .line 2044143
    :goto_2
    iget-object v3, p1, LX/DpJ;->user_id:Ljava/lang/Long;

    if-eqz v3, :cond_7

    move v3, v1

    .line 2044144
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2044145
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044146
    iget-object v0, p0, LX/DpJ;->user_id:Ljava/lang/Long;

    iget-object v3, p1, LX/DpJ;->user_id:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 2044147
    goto :goto_1

    :cond_6
    move v0, v2

    .line 2044148
    goto :goto_2

    :cond_7
    move v3, v2

    .line 2044149
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2044150
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2044151
    sget-boolean v0, LX/DpJ;->a:Z

    .line 2044152
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpJ;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2044153
    return-object v0
.end method
