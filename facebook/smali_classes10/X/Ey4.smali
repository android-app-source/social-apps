.class public LX/Ey4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2185748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2185749
    iput-object p1, p0, LX/Ey4;->a:LX/0Zb;

    .line 2185750
    return-void
.end method

.method public static a(LX/0QB;)LX/Ey4;
    .locals 2

    .prologue
    .line 2185751
    new-instance v1, LX/Ey4;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/Ey4;-><init>(LX/0Zb;)V

    .line 2185752
    move-object v0, v1

    .line 2185753
    return-object v0
.end method

.method public static a(LX/Ey4;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2185754
    iget-object v0, p0, LX/Ey4;->a:LX/0Zb;

    const-string v1, "friendscenter_make_friend_reliability"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2185755
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2185756
    const-string v1, "event"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2185757
    const-string v1, "source"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2185758
    const-string v1, "cc_enabled"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2185759
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2185760
    :cond_0
    return-void
.end method
