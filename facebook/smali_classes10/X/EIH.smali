.class public final enum LX/EIH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EIH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EIH;

.field public static final enum CONFERENCE:LX/EIH;

.field public static final enum VIDEO:LX/EIH;

.field public static final enum VOICE:LX/EIH;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2101387
    new-instance v0, LX/EIH;

    const-string v1, "VOICE"

    invoke-direct {v0, v1, v2}, LX/EIH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIH;->VOICE:LX/EIH;

    .line 2101388
    new-instance v0, LX/EIH;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/EIH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIH;->VIDEO:LX/EIH;

    .line 2101389
    new-instance v0, LX/EIH;

    const-string v1, "CONFERENCE"

    invoke-direct {v0, v1, v4}, LX/EIH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EIH;->CONFERENCE:LX/EIH;

    .line 2101390
    const/4 v0, 0x3

    new-array v0, v0, [LX/EIH;

    sget-object v1, LX/EIH;->VOICE:LX/EIH;

    aput-object v1, v0, v2

    sget-object v1, LX/EIH;->VIDEO:LX/EIH;

    aput-object v1, v0, v3

    sget-object v1, LX/EIH;->CONFERENCE:LX/EIH;

    aput-object v1, v0, v4

    sput-object v0, LX/EIH;->$VALUES:[LX/EIH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2101384
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EIH;
    .locals 1

    .prologue
    .line 2101386
    const-class v0, LX/EIH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EIH;

    return-object v0
.end method

.method public static values()[LX/EIH;
    .locals 1

    .prologue
    .line 2101385
    sget-object v0, LX/EIH;->$VALUES:[LX/EIH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EIH;

    return-object v0
.end method
