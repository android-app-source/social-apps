.class public LX/DXP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/0tX;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/0kL;

.field public e:Ljava/lang/String;

.field public f:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;LX/0tX;Lcom/facebook/content/SecureContextHelper;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2010200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2010201
    iput-object p1, p0, LX/DXP;->a:LX/0Sh;

    .line 2010202
    iput-object p2, p0, LX/DXP;->b:LX/0tX;

    .line 2010203
    iput-object p3, p0, LX/DXP;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2010204
    iput-object p4, p0, LX/DXP;->d:LX/0kL;

    .line 2010205
    return-void
.end method

.method public static a(LX/0QB;)LX/DXP;
    .locals 7

    .prologue
    .line 2010150
    const-class v1, LX/DXP;

    monitor-enter v1

    .line 2010151
    :try_start_0
    sget-object v0, LX/DXP;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2010152
    sput-object v2, LX/DXP;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2010153
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2010154
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2010155
    new-instance p0, LX/DXP;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-direct {p0, v3, v4, v5, v6}, LX/DXP;-><init>(LX/0Sh;LX/0tX;Lcom/facebook/content/SecureContextHelper;LX/0kL;)V

    .line 2010156
    move-object v0, p0

    .line 2010157
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2010158
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DXP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2010159
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2010160
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/DXP;Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;)V
    .locals 12

    .prologue
    .line 2010161
    new-instance v0, LX/4Fo;

    invoke-direct {v0}, LX/4Fo;-><init>()V

    .line 2010162
    invoke-virtual {p1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2010163
    const-string v2, "group_invite_link_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2010164
    new-instance v1, LX/DXh;

    invoke-direct {v1}, LX/DXh;-><init>()V

    move-object v1, v1

    .line 2010165
    new-instance v2, LX/DXl;

    invoke-direct {v2}, LX/DXl;-><init>()V

    .line 2010166
    invoke-virtual {p1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->j()Z

    move-result v3

    iput-boolean v3, v2, LX/DXl;->a:Z

    .line 2010167
    invoke-virtual {p1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->k()Z

    move-result v3

    iput-boolean v3, v2, LX/DXl;->b:Z

    .line 2010168
    invoke-virtual {p1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->l()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/DXl;->c:Ljava/lang/String;

    .line 2010169
    invoke-virtual {p1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->m()Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel$InviterModel;

    move-result-object v3

    iput-object v3, v2, LX/DXl;->d:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel$InviterModel;

    .line 2010170
    invoke-virtual {p1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->n()Z

    move-result v3

    iput-boolean v3, v2, LX/DXl;->e:Z

    .line 2010171
    invoke-virtual {p1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->o()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/DXl;->f:Ljava/lang/String;

    .line 2010172
    move-object v2, v2

    .line 2010173
    const/4 v3, 0x1

    .line 2010174
    iput-boolean v3, v2, LX/DXl;->e:Z

    .line 2010175
    move-object v2, v2

    .line 2010176
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 2010177
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2010178
    iget-object v5, v2, LX/DXl;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2010179
    iget-object v7, v2, LX/DXl;->d:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel$InviterModel;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2010180
    iget-object v9, v2, LX/DXl;->f:Ljava/lang/String;

    invoke-virtual {v4, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2010181
    const/4 v10, 0x6

    invoke-virtual {v4, v10}, LX/186;->c(I)V

    .line 2010182
    iget-boolean v10, v2, LX/DXl;->a:Z

    invoke-virtual {v4, v11, v10}, LX/186;->a(IZ)V

    .line 2010183
    iget-boolean v10, v2, LX/DXl;->b:Z

    invoke-virtual {v4, v8, v10}, LX/186;->a(IZ)V

    .line 2010184
    const/4 v10, 0x2

    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 2010185
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v7}, LX/186;->b(II)V

    .line 2010186
    const/4 v5, 0x4

    iget-boolean v7, v2, LX/DXl;->e:Z

    invoke-virtual {v4, v5, v7}, LX/186;->a(IZ)V

    .line 2010187
    const/4 v5, 0x5

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 2010188
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2010189
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2010190
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2010191
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2010192
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2010193
    new-instance v5, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;-><init>(LX/15i;)V

    .line 2010194
    move-object v2, v5

    .line 2010195
    iget-object v3, p0, LX/DXP;->f:LX/0TF;

    invoke-interface {v3, v2}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2010196
    const-string v2, "data"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2010197
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2010198
    iget-object v1, p0, LX/DXP;->a:LX/0Sh;

    iget-object v2, p0, LX/DXP;->b:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, LX/DXN;

    invoke-direct {v2, p0, p1}, LX/DXN;-><init>(LX/DXP;Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2010199
    return-void
.end method
