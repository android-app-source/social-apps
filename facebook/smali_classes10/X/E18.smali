.class public final LX/E18;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;)V
    .locals 0

    .prologue
    .line 2069374
    iput-object p1, p0, LX/E18;->a:Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V
    .locals 1

    .prologue
    .line 2069375
    iget-object v0, p0, LX/E18;->a:Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;

    iget-object v0, v0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->s:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2069376
    iget-object v0, p0, LX/E18;->a:Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;

    .line 2069377
    invoke-static {v0, p1}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->a$redex0(Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    .line 2069378
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2069379
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2069380
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-direct {p0, p1}, LX/E18;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    return-void
.end method
