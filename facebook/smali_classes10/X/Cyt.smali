.class public final LX/Cyt;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cyu;


# direct methods
.method public constructor <init>(LX/Cyu;)V
    .locals 0

    .prologue
    .line 1953859
    iput-object p1, p0, LX/Cyt;->a:LX/Cyu;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 1953860
    iget-object v0, p0, LX/Cyt;->a:LX/Cyu;

    invoke-static {v0}, LX/Cyu;->b$redex0(LX/Cyu;)V

    .line 1953861
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1953862
    iget-object v0, p0, LX/Cyt;->a:LX/Cyu;

    invoke-static {v0}, LX/Cyu;->b$redex0(LX/Cyu;)V

    .line 1953863
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1953864
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1953865
    if-eqz p1, :cond_0

    .line 1953866
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1953867
    check-cast v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;

    invoke-static {v0}, LX/Cyu;->b(Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;)LX/0Px;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1953868
    :cond_0
    iget-object v0, p0, LX/Cyt;->a:LX/Cyu;

    iget-object v0, v0, LX/Cyu;->c:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_GRAPH_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    const-string v2, "Result photo album data invalid"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 1953869
    iget-object v0, p0, LX/Cyt;->a:LX/Cyu;

    invoke-static {v0}, LX/Cyu;->b$redex0(LX/Cyu;)V

    .line 1953870
    :cond_1
    :goto_0
    return-void

    .line 1953871
    :cond_2
    iget-object v0, p0, LX/Cyt;->a:LX/Cyu;

    iget-object v0, v0, LX/Cyu;->d:LX/Cwy;

    if-eqz v0, :cond_1

    .line 1953872
    iget-object v0, p0, LX/Cyt;->a:LX/Cyu;

    iget-object v1, v0, LX/Cyu;->d:LX/Cwy;

    .line 1953873
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1953874
    check-cast v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;

    invoke-static {v0}, LX/Cyu;->b(Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/Cwy;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
