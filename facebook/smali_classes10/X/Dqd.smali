.class public final LX/Dqd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:LX/Dqe;


# direct methods
.method public constructor <init>(LX/Dqe;)V
    .locals 0

    .prologue
    .line 2048457
    iput-object p1, p0, LX/Dqd;->a:LX/Dqe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2048450
    iget-object v0, p0, LX/Dqd;->a:LX/Dqe;

    iget-object v0, v0, LX/Dqe;->f:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2048451
    iget-object v0, p0, LX/Dqd;->a:LX/Dqe;

    iget-object v0, v0, LX/Dqe;->d:LX/1rp;

    invoke-virtual {v0}, LX/1rp;->a()LX/2kW;

    move-result-object v0

    .line 2048452
    invoke-virtual {v0}, LX/2kW;->d()V

    .line 2048453
    iget-object v0, p0, LX/Dqd;->a:LX/Dqe;

    iget-object v0, v0, LX/Dqe;->e:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Notifications Deleted"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2048454
    :goto_0
    return v3

    .line 2048455
    :cond_0
    iget-object v0, p0, LX/Dqd;->a:LX/Dqe;

    iget-object v0, v0, LX/Dqe;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/notifications/preferences/settings/NotificationsClearDBPreference$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/notifications/preferences/settings/NotificationsClearDBPreference$1$1;-><init>(LX/Dqd;)V

    const v2, -0x7906a7d6

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2048456
    iget-object v0, p0, LX/Dqd;->a:LX/Dqe;

    iget-object v0, v0, LX/Dqe;->e:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Notifications Deleted"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_0
.end method
