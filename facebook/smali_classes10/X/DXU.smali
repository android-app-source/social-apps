.class public LX/DXU;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DXS;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DXV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2010283
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DXU;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DXV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2010284
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2010285
    iput-object p1, p0, LX/DXU;->b:LX/0Ot;

    .line 2010286
    return-void
.end method

.method public static a(LX/0QB;)LX/DXU;
    .locals 4

    .prologue
    .line 2010287
    const-class v1, LX/DXU;

    monitor-enter v1

    .line 2010288
    :try_start_0
    sget-object v0, LX/DXU;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2010289
    sput-object v2, LX/DXU;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2010290
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2010291
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2010292
    new-instance v3, LX/DXU;

    const/16 p0, 0x2483

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DXU;-><init>(LX/0Ot;)V

    .line 2010293
    move-object v0, v3

    .line 2010294
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2010295
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DXU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2010296
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2010297
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2010298
    const v0, -0x6eed2f2d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2010299
    check-cast p2, LX/DXT;

    .line 2010300
    iget-object v0, p0, LX/DXU;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DXV;

    iget-object v1, p2, LX/DXT;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010301
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->j()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2010302
    :cond_0
    const/4 v2, 0x0

    .line 2010303
    :goto_0
    move-object v0, v2

    .line 2010304
    return-object v0

    .line 2010305
    :cond_1
    iget-object v2, v0, LX/DXV;->a:LX/1vg;

    invoke-virtual {v2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v2

    const p0, 0x7f020818

    invoke-virtual {v2, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v2

    const p0, -0xb4b0aa

    invoke-virtual {v2, p0}, LX/2xv;->i(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 2010306
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    const p2, 0x7f0207fb

    invoke-interface {p0, p2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object p0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2, v2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {p0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    const p2, 0x7f08309a

    invoke-virtual {p0, p2}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0051

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0a00f7

    invoke-virtual {p0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    sget-object p2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-interface {p0, p2}, LX/1Di;->b(F)LX/1Di;

    move-result-object p0

    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2010307
    const p0, -0x6eed2f2d

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2010308
    invoke-interface {v2, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2010309
    invoke-static {}, LX/1dS;->b()V

    .line 2010310
    iget v0, p1, LX/1dQ;->b:I

    .line 2010311
    packed-switch v0, :pswitch_data_0

    .line 2010312
    :goto_0
    return-object v2

    .line 2010313
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2010314
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2010315
    check-cast v1, LX/DXT;

    .line 2010316
    iget-object v3, p0, LX/DXU;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DXV;

    iget-object v4, v1, LX/DXT;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010317
    iget-object p1, v3, LX/DXV;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DXP;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 2010318
    new-instance p0, LX/0ju;

    invoke-direct {p0, p2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2010319
    const v1, 0x7f08309d

    invoke-virtual {p0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v3, 0x7f08309e

    invoke-virtual {v1, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    const v3, 0x7f08309f

    new-instance v0, LX/DXM;

    invoke-direct {v0, p1, v4}, LX/DXM;-><init>(LX/DXP;Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;)V

    invoke-virtual {v1, v3, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v3, 0x7f080017

    const/4 v0, 0x0

    invoke-virtual {v1, v3, v0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2010320
    invoke-virtual {p0}, LX/0ju;->a()LX/2EJ;

    move-result-object p0

    invoke-virtual {p0}, LX/2EJ;->show()V

    .line 2010321
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x6eed2f2d
        :pswitch_0
    .end packed-switch
.end method
