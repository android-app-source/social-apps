.class public LX/EmO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/EmO;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2165661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)LX/EmK;
    .locals 1

    .prologue
    .line 2165674
    new-instance v0, LX/EmK;

    invoke-direct {v0, p0}, LX/EmK;-><init>(I)V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/EmO;
    .locals 3

    .prologue
    .line 2165662
    sget-object v0, LX/EmO;->a:LX/EmO;

    if-nez v0, :cond_1

    .line 2165663
    const-class v1, LX/EmO;

    monitor-enter v1

    .line 2165664
    :try_start_0
    sget-object v0, LX/EmO;->a:LX/EmO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2165665
    if-eqz v2, :cond_0

    .line 2165666
    :try_start_1
    new-instance v0, LX/EmO;

    invoke-direct {v0}, LX/EmO;-><init>()V

    .line 2165667
    move-object v0, v0

    .line 2165668
    sput-object v0, LX/EmO;->a:LX/EmO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2165669
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2165670
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2165671
    :cond_1
    sget-object v0, LX/EmO;->a:LX/EmO;

    return-object v0

    .line 2165672
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2165673
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
