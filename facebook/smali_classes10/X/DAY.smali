.class public LX/DAY;
.super LX/3OP;
.source ""


# instance fields
.field public final a:Lcom/facebook/user/model/User;

.field private b:Z


# virtual methods
.method public final a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "ARG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3L9",
            "<TT;TARG;>;TARG;)TT;"
        }
    .end annotation

    .prologue
    .line 1971361
    invoke-interface {p1, p0, p2}, LX/3L9;->a(LX/DAY;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1971362
    iput-boolean p1, p0, LX/DAY;->b:Z

    .line 1971363
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1971364
    iget-boolean v0, p0, LX/DAY;->b:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1971365
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 1971366
    :cond_0
    const/4 v0, 0x0

    .line 1971367
    :goto_0
    return v0

    .line 1971368
    :cond_1
    check-cast p1, LX/DAY;

    .line 1971369
    iget-object v0, p1, LX/DAY;->a:Lcom/facebook/user/model/User;

    .line 1971370
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1971371
    iget-object v1, p0, LX/DAY;->a:Lcom/facebook/user/model/User;

    .line 1971372
    iget-object p0, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p0

    .line 1971373
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1971374
    iget-object v0, p0, LX/DAY;->a:Lcom/facebook/user/model/User;

    .line 1971375
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1971376
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
