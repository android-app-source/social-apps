.class public final LX/DmU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2039269
    iput-object p1, p0, LX/DmU;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iput-object p2, p0, LX/DmU;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x1aeb9f96

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2039270
    iget-object v1, p0, LX/DmU;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->f:LX/Dih;

    iget-object v2, p0, LX/DmU;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    iget-object v3, p0, LX/DmU;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    iget-object v4, p0, LX/DmU;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-virtual {v1, v2, v3, v4}, LX/Dih;->c(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)V

    .line 2039271
    iget-object v1, p0, LX/DmU;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    invoke-virtual {v1}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2039272
    iget-object v2, p0, LX/DmU;->a:Ljava/lang/String;

    invoke-static {v2}, LX/DkQ;->f(Ljava/lang/String;)LX/DkQ;

    move-result-object v2

    .line 2039273
    iget-object v3, p0, LX/DmU;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2039274
    iget-object v3, p0, LX/DmU;->b:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2039275
    const v1, -0x5e90c3a4

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
