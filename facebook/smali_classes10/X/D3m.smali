.class public LX/D3m;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/fig/listitem/FigListItem;

.field public d:Lcom/facebook/fig/listitem/FigListItem;

.field public e:Lcom/facebook/fig/listitem/FigListItem;

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1960980
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1960981
    const/4 p1, 0x1

    const/4 v5, 0x0

    .line 1960982
    const-class v0, LX/D3m;

    invoke-static {v0, p0}, LX/D3m;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1960983
    const v0, 0x7f030388

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1960984
    invoke-virtual {p0, p1}, LX/D3m;->setOrientation(I)V

    .line 1960985
    invoke-virtual {p0}, LX/D3m;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 1960986
    invoke-virtual {p0}, LX/D3m;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a009f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1960987
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1960988
    const v0, 0x7f0d0b60

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, LX/D3m;->c:Lcom/facebook/fig/listitem/FigListItem;

    .line 1960989
    const v0, 0x7f0d0b61

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, LX/D3m;->d:Lcom/facebook/fig/listitem/FigListItem;

    .line 1960990
    const v0, 0x7f0d0b62

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, LX/D3m;->e:Lcom/facebook/fig/listitem/FigListItem;

    .line 1960991
    iget-object v0, p0, LX/D3m;->c:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, LX/D3m;->b:LX/0wM;

    const v2, 0x7f020914

    invoke-virtual {p0}, LX/D3m;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1960992
    iget-object v0, p0, LX/D3m;->c:Lcom/facebook/fig/listitem/FigListItem;

    const v1, 0x7f02033f

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBackgroundResource(I)V

    .line 1960993
    iget-object v0, p0, LX/D3m;->d:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, LX/D3m;->b:LX/0wM;

    const v2, 0x7f02084d

    invoke-virtual {p0}, LX/D3m;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1960994
    iget-object v0, p0, LX/D3m;->d:Lcom/facebook/fig/listitem/FigListItem;

    const v1, 0x7f0a0115

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBackgroundResource(I)V

    .line 1960995
    iget-object v0, p0, LX/D3m;->e:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, LX/D3m;->b:LX/0wM;

    const v2, 0x7f0208ed

    invoke-virtual {p0}, LX/D3m;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a008b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1960996
    new-array v0, p1, [I

    const v1, 0x101045c

    aput v1, v0, v5

    .line 1960997
    invoke-virtual {p0}, LX/D3m;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1960998
    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1960999
    iget-object v2, p0, LX/D3m;->e:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v2, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBackgroundResource(I)V

    .line 1961000
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1961001
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/D3m;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object v1, p1, LX/D3m;->a:LX/0hB;

    iput-object p0, p1, LX/D3m;->b:LX/0wM;

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1961002
    iget-object v0, p0, LX/D3m;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1961003
    invoke-super {p0, v0, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onMeasure(II)V

    .line 1961004
    return-void
.end method
