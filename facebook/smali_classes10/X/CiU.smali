.class public final enum LX/CiU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CiU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CiU;

.field public static final enum ON_CREATE:LX/CiU;

.field public static final enum ON_DESTROY:LX/CiU;

.field public static final enum ON_LOW_MEMORY:LX/CiU;

.field public static final enum ON_PAUSE:LX/CiU;

.field public static final enum ON_RESUME:LX/CiU;

.field public static final enum ON_SAVE_INSTANCE_STATE:LX/CiU;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1928804
    new-instance v0, LX/CiU;

    const-string v1, "ON_CREATE"

    invoke-direct {v0, v1, v3}, LX/CiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiU;->ON_CREATE:LX/CiU;

    .line 1928805
    new-instance v0, LX/CiU;

    const-string v1, "ON_RESUME"

    invoke-direct {v0, v1, v4}, LX/CiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiU;->ON_RESUME:LX/CiU;

    .line 1928806
    new-instance v0, LX/CiU;

    const-string v1, "ON_PAUSE"

    invoke-direct {v0, v1, v5}, LX/CiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiU;->ON_PAUSE:LX/CiU;

    .line 1928807
    new-instance v0, LX/CiU;

    const-string v1, "ON_DESTROY"

    invoke-direct {v0, v1, v6}, LX/CiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiU;->ON_DESTROY:LX/CiU;

    .line 1928808
    new-instance v0, LX/CiU;

    const-string v1, "ON_SAVE_INSTANCE_STATE"

    invoke-direct {v0, v1, v7}, LX/CiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiU;->ON_SAVE_INSTANCE_STATE:LX/CiU;

    .line 1928809
    new-instance v0, LX/CiU;

    const-string v1, "ON_LOW_MEMORY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CiU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiU;->ON_LOW_MEMORY:LX/CiU;

    .line 1928810
    const/4 v0, 0x6

    new-array v0, v0, [LX/CiU;

    sget-object v1, LX/CiU;->ON_CREATE:LX/CiU;

    aput-object v1, v0, v3

    sget-object v1, LX/CiU;->ON_RESUME:LX/CiU;

    aput-object v1, v0, v4

    sget-object v1, LX/CiU;->ON_PAUSE:LX/CiU;

    aput-object v1, v0, v5

    sget-object v1, LX/CiU;->ON_DESTROY:LX/CiU;

    aput-object v1, v0, v6

    sget-object v1, LX/CiU;->ON_SAVE_INSTANCE_STATE:LX/CiU;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CiU;->ON_LOW_MEMORY:LX/CiU;

    aput-object v2, v0, v1

    sput-object v0, LX/CiU;->$VALUES:[LX/CiU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1928812
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CiU;
    .locals 1

    .prologue
    .line 1928813
    const-class v0, LX/CiU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CiU;

    return-object v0
.end method

.method public static values()[LX/CiU;
    .locals 1

    .prologue
    .line 1928811
    sget-object v0, LX/CiU;->$VALUES:[LX/CiU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CiU;

    return-object v0
.end method
