.class public LX/CnD;
.super LX/16T;
.source ""


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field private final b:LX/0SG;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1933820
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSTANT_ARTICLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/CnD;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1933821
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1933822
    iput-object p1, p0, LX/CnD;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1933823
    iput-object p2, p0, LX/CnD;->b:LX/0SG;

    .line 1933824
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1933825
    const-wide/32 v0, 0xf731400

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1933826
    const-string v0, "3838"

    invoke-static {v0}, LX/11b;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 1933827
    iget-object v1, p0, LX/CnD;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 1933828
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/CnD;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/32 v2, 0xf731400

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1933829
    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 1933830
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1933831
    const-string v0, "3838"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1933832
    sget-object v0, LX/CnD;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
