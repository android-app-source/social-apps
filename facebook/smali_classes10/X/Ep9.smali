.class public LX/Ep9;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/2eZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomLinearLayout;",
        "LX/2eZ;"
    }
.end annotation


# instance fields
.field public a:LX/Eoj;

.field public b:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

.field public d:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

.field public e:Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;

.field public f:Landroid/view/View;

.field public g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2170014
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2170015
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ep9;->g:Z

    .line 2170016
    const-class v0, LX/Ep9;

    invoke-static {v0, p0}, LX/Ep9;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2170017
    const v0, 0x7f030f34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2170018
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Ep9;->setOrientation(I)V

    .line 2170019
    const v0, 0x7f0d0cc9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    iput-object v0, p0, LX/Ep9;->c:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    .line 2170020
    const v0, 0x7f0d24d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    iput-object v0, p0, LX/Ep9;->d:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    .line 2170021
    const v0, 0x7f0d24d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;

    iput-object v0, p0, LX/Ep9;->e:Lcom/facebook/entitycardsplugins/person/widget/contextitemlist/PersonCardContextItemListView;

    .line 2170022
    const v0, 0x7f0d24d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2170023
    iget-object v1, p0, LX/Ep9;->b:LX/0ad;

    sget-short v2, LX/EoV;->a:S

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2170024
    const v1, 0x7f030f31

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2170025
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Ep9;->f:Landroid/view/View;

    .line 2170026
    return-void

    .line 2170027
    :cond_0
    const v1, 0x7f030f32

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ep9;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object p0, p1, LX/Ep9;->b:LX/0ad;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2170028
    iget-boolean v0, p0, LX/Ep9;->g:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4beffad8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2170029
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2170030
    const/4 v1, 0x1

    .line 2170031
    iput-boolean v1, p0, LX/Ep9;->g:Z

    .line 2170032
    const/16 v1, 0x2d

    const v2, 0xe8a85a5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xe09c85c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2170033
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2170034
    const/4 v1, 0x0

    .line 2170035
    iput-boolean v1, p0, LX/Ep9;->g:Z

    .line 2170036
    iget-object v1, p0, LX/Ep9;->a:LX/Eoj;

    if-eqz v1, :cond_0

    .line 2170037
    iget-object v1, p0, LX/Ep9;->a:LX/Eoj;

    invoke-virtual {v1, p0}, LX/Eoj;->a(LX/Ep9;)V

    .line 2170038
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x79f88680

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public bridge synthetic setPresenter(LX/Emf;)V
    .locals 0

    .prologue
    .line 2170039
    check-cast p1, LX/Eoj;

    .line 2170040
    iput-object p1, p0, LX/Ep9;->a:LX/Eoj;

    .line 2170041
    return-void
.end method
