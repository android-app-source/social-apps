.class public final LX/Djg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;)V
    .locals 0

    .prologue
    .line 2033414
    iput-object p1, p0, LX/Djg;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x69ca5a38

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2033415
    iget-object v1, p0, LX/Djg;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->g:LX/0Uh;

    const/16 v2, 0x61e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2033416
    iget-object v1, p0, LX/Djg;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    .line 2033417
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, LX/Djc;->a:I

    iget-object v4, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->o:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {v4}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;->n()Ljava/lang/String;

    move-result-object v4

    iget-object p0, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->o:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object p0

    iget-object p1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->o:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->k()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, v3, v4, p0, p1}, Lcom/facebook/messaging/professionalservices/booking/activities/RejectAppointmentActivity;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2033418
    iget-object v3, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->i:Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x14

    invoke-interface {v3, v2, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2033419
    :goto_0
    const v1, -0x7f31f8f3

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2033420
    :cond_0
    iget-object v1, p0, LX/Djg;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    .line 2033421
    sget-object v2, LX/Diw;->USER_CANCEL:LX/Diw;

    invoke-static {v2}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;->a(LX/Diw;)Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;

    move-result-object v2

    .line 2033422
    iget-object v3, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->p:LX/Dix;

    .line 2033423
    iput-object v3, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentConfirmationDialogFragment;->m:LX/Dix;

    .line 2033424
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    const-string v4, "appointment_confirmation_dialog_fragment_tag"

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2033425
    goto :goto_0
.end method
