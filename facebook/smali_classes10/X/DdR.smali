.class public final LX/DdR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:J

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLX/0Px;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2019513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019514
    iput-wide p1, p0, LX/DdR;->a:J

    .line 2019515
    iput-object p3, p0, LX/DdR;->b:LX/0Px;

    .line 2019516
    iput-object p4, p0, LX/DdR;->c:LX/0Px;

    .line 2019517
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2019518
    const-class v0, LX/DdR;

    invoke-static {v0}, LX/0Qh;->toStringHelper(Ljava/lang/Class;)LX/0zA;

    move-result-object v0

    const-string v1, "actionId"

    iget-wide v2, p0, LX/DdR;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "participants"

    iget-object v2, p0, LX/DdR;->b:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "names"

    iget-object v2, p0, LX/DdR;->c:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
