.class public final LX/EKq;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EKr;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Z

.field public final synthetic e:LX/EKr;


# direct methods
.method public constructor <init>(LX/EKr;)V
    .locals 1

    .prologue
    .line 2107330
    iput-object p1, p0, LX/EKq;->e:LX/EKr;

    .line 2107331
    move-object v0, p1

    .line 2107332
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2107333
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2107334
    const-string v0, "SearchResultsElectoralVoteBarComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2107335
    if-ne p0, p1, :cond_1

    .line 2107336
    :cond_0
    :goto_0
    return v0

    .line 2107337
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2107338
    goto :goto_0

    .line 2107339
    :cond_3
    check-cast p1, LX/EKq;

    .line 2107340
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2107341
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2107342
    if-eq v2, v3, :cond_0

    .line 2107343
    iget v2, p0, LX/EKq;->a:I

    iget v3, p1, LX/EKq;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2107344
    goto :goto_0

    .line 2107345
    :cond_4
    iget-object v2, p0, LX/EKq;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/EKq;->b:Ljava/lang/String;

    iget-object v3, p1, LX/EKq;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2107346
    goto :goto_0

    .line 2107347
    :cond_6
    iget-object v2, p1, LX/EKq;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 2107348
    :cond_7
    iget v2, p0, LX/EKq;->c:I

    iget v3, p1, LX/EKq;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2107349
    goto :goto_0

    .line 2107350
    :cond_8
    iget-boolean v2, p0, LX/EKq;->d:Z

    iget-boolean v3, p1, LX/EKq;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2107351
    goto :goto_0
.end method
