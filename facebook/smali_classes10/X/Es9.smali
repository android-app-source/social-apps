.class public final LX/Es9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/CEy;

.field public final synthetic b:LX/EsE;

.field public final synthetic c:LX/1Pr;

.field public final synthetic d:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic e:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;LX/CEy;LX/EsE;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 2174532
    iput-object p1, p0, LX/Es9;->e:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iput-object p2, p0, LX/Es9;->a:LX/CEy;

    iput-object p3, p0, LX/Es9;->b:LX/EsE;

    iput-object p4, p0, LX/Es9;->c:LX/1Pr;

    iput-object p5, p0, LX/Es9;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x26

    const v1, 0x506198e0

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2174524
    const-string v0, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/7m7;->SUCCESS:LX/7m7;

    invoke-virtual {v0}, LX/7m7;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "extra_result"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Es9;->a:LX/CEy;

    const-string v2, "extra_request_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/CEy;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174525
    iget-object v0, p0, LX/Es9;->e:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->m:LX/0if;

    sget-object v2, LX/0ig;->aD:LX/0ih;

    const-string v3, "composer_post"

    invoke-virtual {v0, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2174526
    iget-object v0, p0, LX/Es9;->e:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->m:LX/0if;

    sget-object v2, LX/0ig;->aD:LX/0ih;

    invoke-virtual {v0, v2}, LX/0if;->c(LX/0ih;)V

    .line 2174527
    iget-object v0, p0, LX/Es9;->b:LX/EsE;

    .line 2174528
    iput-boolean v4, v0, LX/EsE;->a:Z

    .line 2174529
    iget-object v0, p0, LX/Es9;->c:LX/1Pr;

    check-cast v0, LX/1Pq;

    new-array v2, v4, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Es9;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2174530
    const/16 v0, 0x27

    const v2, -0x4b4ffd76

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2174531
    :goto_0
    return-void

    :cond_0
    const v0, -0x551b9e9c

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0
.end method
