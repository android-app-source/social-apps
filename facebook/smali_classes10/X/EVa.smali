.class public LX/EVa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile B:LX/EVa;

.field public static final c:Ljava/lang/String;

.field private static final d:Landroid/os/Handler;

.field private static final e:Ljava/lang/Object;


# instance fields
.field public A:J

.field public a:Lcom/facebook/common/async/CancellableRunnable;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/reaction/ReactionQueryParams;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2xj;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/3AW;

.field public k:LX/2jY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final l:Ljava/lang/String;

.field public final m:LX/0kL;

.field private final n:LX/ESy;

.field private final o:LX/EVZ;

.field public final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0ad;

.field private final r:LX/0SG;

.field public final s:LX/0So;

.field private t:Z

.field private u:J

.field private final v:Lcom/facebook/reaction/ReactionQueryParams;

.field private w:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0ho;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/0JU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:LX/0xX;

.field private z:LX/CfK;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2128439
    const-class v0, LX/EVa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EVa;->c:Ljava/lang/String;

    .line 2128440
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, LX/EVa;->d:Landroid/os/Handler;

    .line 2128441
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/EVa;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0kL;LX/0Or;LX/3AW;LX/0Ot;LX/0ad;LX/0xX;LX/0SG;LX/0So;LX/CfK;LX/ESy;)V
    .locals 4
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/video/videohome/data/IsVideoHomeDataFetchToastEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2xj;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;",
            ">;",
            "Ljava/lang/String;",
            "LX/0kL;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3AW;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0ad;",
            "LX/0xX;",
            "LX/0SG;",
            "LX/0So;",
            "LX/CfK;",
            "LX/ESy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2128452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2128453
    new-instance v2, LX/EVZ;

    invoke-direct {v2, p0}, LX/EVZ;-><init>(LX/EVa;)V

    iput-object v2, p0, LX/EVa;->o:LX/EVZ;

    .line 2128454
    const-wide/16 v2, 0x708

    iput-wide v2, p0, LX/EVa;->u:J

    .line 2128455
    iput-object p1, p0, LX/EVa;->f:LX/0Ot;

    .line 2128456
    iput-object p2, p0, LX/EVa;->h:LX/0Ot;

    .line 2128457
    iput-object p3, p0, LX/EVa;->i:LX/0Ot;

    .line 2128458
    iput-object p4, p0, LX/EVa;->l:Ljava/lang/String;

    .line 2128459
    iput-object p5, p0, LX/EVa;->m:LX/0kL;

    .line 2128460
    iput-object p6, p0, LX/EVa;->p:LX/0Or;

    .line 2128461
    iput-object p7, p0, LX/EVa;->j:LX/3AW;

    .line 2128462
    iput-object p8, p0, LX/EVa;->g:LX/0Ot;

    .line 2128463
    iput-object p9, p0, LX/EVa;->q:LX/0ad;

    .line 2128464
    iput-object p10, p0, LX/EVa;->y:LX/0xX;

    .line 2128465
    const-string v2, "normal"

    invoke-direct {p0, v2}, LX/EVa;->a(Ljava/lang/String;)Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v2

    iput-object v2, p0, LX/EVa;->b:Lcom/facebook/reaction/ReactionQueryParams;

    .line 2128466
    const-string v2, "video_home_perceived_perf_data"

    invoke-direct {p0, v2}, LX/EVa;->a(Ljava/lang/String;)Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v2

    iput-object v2, p0, LX/EVa;->v:Lcom/facebook/reaction/ReactionQueryParams;

    .line 2128467
    iput-object p11, p0, LX/EVa;->r:LX/0SG;

    .line 2128468
    move-object/from16 v0, p12

    iput-object v0, p0, LX/EVa;->s:LX/0So;

    .line 2128469
    move-object/from16 v0, p13

    iput-object v0, p0, LX/EVa;->z:LX/CfK;

    .line 2128470
    move-object/from16 v0, p14

    iput-object v0, p0, LX/EVa;->n:LX/ESy;

    .line 2128471
    return-void
.end method

.method public static a(LX/0QB;)LX/EVa;
    .locals 3

    .prologue
    .line 2128442
    sget-object v0, LX/EVa;->B:LX/EVa;

    if-nez v0, :cond_1

    .line 2128443
    const-class v1, LX/EVa;

    monitor-enter v1

    .line 2128444
    :try_start_0
    sget-object v0, LX/EVa;->B:LX/EVa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2128445
    if-eqz v2, :cond_0

    .line 2128446
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/EVa;->b(LX/0QB;)LX/EVa;

    move-result-object v0

    sput-object v0, LX/EVa;->B:LX/EVa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2128447
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2128448
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2128449
    :cond_1
    sget-object v0, LX/EVa;->B:LX/EVa;

    return-object v0

    .line 2128450
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2128451
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;)Lcom/facebook/reaction/ReactionQueryParams;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionRequestTypeValue;
        .end annotation
    .end param

    .prologue
    .line 2128426
    iget-object v0, p0, LX/EVa;->y:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->e()I

    move-result v0

    .line 2128427
    new-instance v1, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v1}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    iget-object v2, p0, LX/EVa;->l:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2128428
    iput-object v2, v1, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2128429
    move-object v1, v1

    .line 2128430
    int-to-long v2, v0

    .line 2128431
    iput-wide v2, v1, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2128432
    move-object v0, v1

    .line 2128433
    sget-object v1, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v1

    .line 2128434
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    .line 2128435
    move-object v0, v0

    .line 2128436
    iput-object p1, v0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    .line 2128437
    move-object v0, v0

    .line 2128438
    return-object v0
.end method

.method public static a$redex0(LX/EVa;LX/2jY;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2128472
    iget-object v0, p0, LX/EVa;->y:LX/0xX;

    sget-object v2, LX/1vy;->VH_LIVE_NOTIFICATIONS:LX/1vy;

    invoke-virtual {v0, v2}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v0, v1

    .line 2128473
    :goto_0
    return v0

    .line 2128474
    :cond_1
    invoke-virtual {p1}, LX/2jY;->o()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qT;

    .line 2128475
    invoke-interface {v0}, LX/9qT;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v3, v1

    :goto_2
    if-ge v3, v8, :cond_4

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    .line 2128476
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2128477
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v9

    .line 2128478
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v2, v1

    .line 2128479
    :goto_3
    if-ge v2, v10, :cond_3

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2128480
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v11

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v11, v12, :cond_2

    .line 2128481
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->ac()I

    move-result v0

    goto :goto_0

    .line 2128482
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2128483
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 2128484
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_5
    move v0, v1

    .line 2128485
    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/EVa;
    .locals 15

    .prologue
    .line 2128424
    new-instance v0, LX/EVa;

    const/16 v1, 0x3096

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1387

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x3810

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    const/16 v6, 0x159a

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v7

    check-cast v7, LX/3AW;

    const/16 v8, 0x271

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v10

    check-cast v10, LX/0xX;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v12

    check-cast v12, LX/0So;

    const-class v13, LX/CfK;

    invoke-interface {p0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/CfK;

    invoke-static {p0}, LX/ESy;->b(LX/0QB;)LX/ESy;

    move-result-object v14

    check-cast v14, LX/ESy;

    invoke-direct/range {v0 .. v14}, LX/EVa;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0kL;LX/0Or;LX/3AW;LX/0Ot;LX/0ad;LX/0xX;LX/0SG;LX/0So;LX/CfK;LX/ESy;)V

    .line 2128425
    return-object v0
.end method

.method public static e(LX/EVa;)LX/0ho;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2128423
    iget-object v0, p0, LX/EVa;->w:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EVa;->w:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ho;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2128399
    sget-object v1, LX/EVa;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 2128400
    :try_start_0
    iget-object v0, p0, LX/EVa;->k:LX/2jY;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2128401
    if-eqz v0, :cond_0

    .line 2128402
    iget-object v0, p0, LX/EVa;->k:LX/2jY;

    .line 2128403
    iget-object p0, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2128404
    monitor-exit v1

    .line 2128405
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_1

    .line 2128406
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized a(Z)V
    .locals 6

    .prologue
    .line 2128412
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, LX/EVa;->t:Z

    .line 2128413
    iget-boolean v0, p0, LX/EVa;->t:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2128414
    if-nez p1, :cond_1

    .line 2128415
    iget-object v0, p0, LX/EVa;->a:Lcom/facebook/common/async/CancellableRunnable;

    if-eqz v0, :cond_0

    .line 2128416
    iget-object v0, p0, LX/EVa;->a:Lcom/facebook/common/async/CancellableRunnable;

    invoke-virtual {v0}, Lcom/facebook/common/async/CancellableRunnable;->a()V

    .line 2128417
    :cond_0
    iget-wide v0, p0, LX/EVa;->u:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2128418
    new-instance v0, Lcom/facebook/common/async/CancellableRunnable;

    new-instance v1, Lcom/facebook/video/videohome/prefetching/VideoHomeReactionSessionHelper$2;

    invoke-direct {v1, p0}, Lcom/facebook/video/videohome/prefetching/VideoHomeReactionSessionHelper$2;-><init>(LX/EVa;)V

    invoke-direct {v0, v1}, Lcom/facebook/common/async/CancellableRunnable;-><init>(Ljava/lang/Runnable;)V

    move-object v0, v0

    .line 2128419
    iput-object v0, p0, LX/EVa;->a:Lcom/facebook/common/async/CancellableRunnable;

    .line 2128420
    iget-object v0, p0, LX/EVa;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iget-object v1, p0, LX/EVa;->a:Lcom/facebook/common/async/CancellableRunnable;

    iget-wide v2, p0, LX/EVa;->u:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2128421
    :cond_1
    monitor-exit p0

    return-void

    .line 2128422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 2128407
    iget-object v0, p0, LX/EVa;->k:LX/2jY;

    if-eqz v0, :cond_0

    .line 2128408
    iget-object v0, p0, LX/EVa;->k:LX/2jY;

    invoke-virtual {v0}, LX/2jY;->a()V

    .line 2128409
    const/4 v0, 0x0

    iput-object v0, p0, LX/EVa;->k:LX/2jY;

    .line 2128410
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EVa;->A:J

    .line 2128411
    :cond_0
    return-void
.end method
