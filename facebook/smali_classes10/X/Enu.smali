.class public final LX/Enu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Enp;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Enq;

.field public final synthetic b:LX/Enz;


# direct methods
.method public constructor <init>(LX/Enz;LX/Enq;)V
    .locals 0

    .prologue
    .line 2167533
    iput-object p1, p0, LX/Enu;->b:LX/Enz;

    iput-object p2, p0, LX/Enu;->a:LX/Enq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2167534
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 2167535
    iget-object v0, p0, LX/Enu;->b:LX/Enz;

    iget-object v0, v0, LX/Enz;->C:LX/03V;

    const-string v1, "load_entity_page_failure"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2167536
    :cond_0
    iget-object v0, p0, LX/Enu;->b:LX/Enz;

    iget-object v1, p0, LX/Enu;->a:LX/Enq;

    invoke-static {v0, v1}, LX/Enz;->d(LX/Enz;LX/Enq;)V

    .line 2167537
    iget-object v0, p0, LX/Enu;->b:LX/Enz;

    iget-object v0, v0, LX/Enz;->t:LX/Emy;

    iget-object v1, p0, LX/Enu;->a:LX/Enq;

    .line 2167538
    new-instance v4, Ljava/lang/StringBuilder;

    const-string p1, "EntityCardPageFetchFailed_"

    invoke-direct {v4, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/Enq;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2167539
    const v4, 0x100004

    const-string p1, "ec_cards_page_download"

    invoke-static {v0, v4, p1}, LX/Emy;->c(LX/Emy;ILjava/lang/String;)V

    .line 2167540
    iget-object v0, p0, LX/Enu;->b:LX/Enz;

    iget-object v0, v0, LX/Enz;->u:LX/Emq;

    invoke-virtual {v0, v2, v2}, LX/Emq;->a(ZI)V

    .line 2167541
    iget-object v0, p0, LX/Enu;->a:LX/Enq;

    sget-object v1, LX/Enq;->LEFT:LX/Enq;

    if-ne v0, v1, :cond_1

    .line 2167542
    iget-object v0, p0, LX/Enu;->b:LX/Enz;

    .line 2167543
    iput-boolean v3, v0, LX/Enz;->o:Z

    .line 2167544
    iget-object v1, p0, LX/Enu;->b:LX/Enz;

    iget-object v0, p0, LX/Enu;->b:LX/Enz;

    iget-object v0, v0, LX/Enz;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enn;

    invoke-static {v1, v0}, LX/Enz;->a$redex0(LX/Enz;LX/Enn;)V

    .line 2167545
    :goto_0
    return-void

    .line 2167546
    :cond_1
    iget-object v0, p0, LX/Enu;->b:LX/Enz;

    .line 2167547
    iput-boolean v3, v0, LX/Enz;->p:Z

    .line 2167548
    iget-object v1, p0, LX/Enu;->b:LX/Enz;

    iget-object v0, p0, LX/Enu;->b:LX/Enz;

    iget-object v0, v0, LX/Enz;->n:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Enn;

    invoke-static {v1, v0}, LX/Enz;->a$redex0(LX/Enz;LX/Enn;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2167549
    check-cast p1, LX/Enp;

    .line 2167550
    iget-object v0, p0, LX/Enu;->b:LX/Enz;

    .line 2167551
    iget-object v1, p1, LX/Enp;->a:LX/En3;

    move-object v1, v1

    .line 2167552
    iget-object v2, p1, LX/Enp;->b:LX/0P1;

    move-object v2, v2

    .line 2167553
    iget-object v3, p0, LX/Enu;->a:LX/Enq;

    .line 2167554
    sget-object v4, LX/Enq;->LEFT:LX/Enq;

    if-ne v3, v4, :cond_2

    .line 2167555
    invoke-virtual {v1}, LX/En2;->c()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move v6, v4

    :goto_0
    if-ltz v6, :cond_1

    .line 2167556
    invoke-virtual {v1, v6}, LX/En2;->a(I)I

    move-result v7

    .line 2167557
    invoke-virtual {v1, v6}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2167558
    iget-object v5, v0, LX/Enz;->m:LX/0am;

    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2167559
    iget-object v5, v0, LX/Enz;->m:LX/0am;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Enn;

    .line 2167560
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v8

    iput-object v8, v0, LX/Enz;->m:LX/0am;

    .line 2167561
    const/4 v8, 0x0

    iput-boolean v8, v0, LX/Enz;->o:Z

    .line 2167562
    :goto_1
    iput-object v4, v5, LX/Enn;->a:Ljava/lang/String;

    .line 2167563
    invoke-virtual {v2, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .line 2167564
    iget-object p0, v0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {p0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2167565
    invoke-static {v0, v4}, LX/Enz;->d(LX/Enz;Ljava/lang/String;)V

    .line 2167566
    :goto_2
    add-int/lit8 v4, v6, -0x1

    move v6, v4

    goto :goto_0

    .line 2167567
    :cond_0
    new-instance v5, LX/Enn;

    invoke-direct {v5, v4}, LX/Enn;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 2167568
    :cond_1
    :goto_3
    invoke-static {v0}, LX/Enz;->o(LX/Enz;)LX/Ens;

    move-result-object v4

    .line 2167569
    iget-object v5, v0, LX/Enl;->e:LX/Eno;

    move-object v5, v5

    .line 2167570
    iget-object v6, v0, LX/Enl;->e:LX/Eno;

    move-object v6, v6

    .line 2167571
    invoke-virtual {v0, v4, v5, v6}, LX/Enl;->a(LX/Ens;LX/Eno;LX/Eno;)V

    .line 2167572
    invoke-static {v0, v1}, LX/Enz;->a(LX/Enz;LX/En2;)LX/0P1;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/Enl;->a(LX/0P1;)V

    .line 2167573
    invoke-static {v0, v3}, LX/Enz;->d(LX/Enz;LX/Enq;)V

    .line 2167574
    iget-object v4, v0, LX/Enz;->t:LX/Emy;

    .line 2167575
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "EntityCardPageFetchSucceeded_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/Enq;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2167576
    const v5, 0x100004

    const-string v6, "ec_cards_page_download"

    invoke-static {v4, v5, v6}, LX/Emy;->b(LX/Emy;ILjava/lang/String;)V

    .line 2167577
    iget-object v4, v0, LX/Enz;->u:LX/Emq;

    const/4 v5, 0x1

    invoke-virtual {v1}, LX/En2;->c()I

    move-result v6

    invoke-virtual {v4, v5, v6}, LX/Emq;->a(ZI)V

    .line 2167578
    return-void

    .line 2167579
    :cond_2
    const/4 v7, 0x0

    .line 2167580
    move v6, v7

    :goto_4
    invoke-virtual {v1}, LX/En2;->c()I

    move-result v4

    if-ge v6, v4, :cond_4

    .line 2167581
    invoke-virtual {v1, v6}, LX/En2;->a(I)I

    move-result v8

    .line 2167582
    invoke-virtual {v1, v6}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2167583
    iget-object v5, v0, LX/Enz;->n:LX/0am;

    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2167584
    iget-object v5, v0, LX/Enz;->n:LX/0am;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Enn;

    .line 2167585
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p0

    iput-object p0, v0, LX/Enz;->n:LX/0am;

    .line 2167586
    iput-boolean v7, v0, LX/Enz;->p:Z

    .line 2167587
    :goto_5
    iput-object v4, v5, LX/Enn;->a:Ljava/lang/String;

    .line 2167588
    invoke-virtual {v2, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    .line 2167589
    iget-object p1, v0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 2167590
    invoke-static {v0, v4}, LX/Enz;->d(LX/Enz;Ljava/lang/String;)V

    .line 2167591
    :goto_6
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_4

    .line 2167592
    :cond_3
    new-instance v5, LX/Enn;

    invoke-direct {v5, v4}, LX/Enn;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 2167593
    :cond_4
    goto :goto_3

    .line 2167594
    :cond_5
    iget-object p0, v0, LX/Enz;->f:LX/En2;

    invoke-virtual {p0, v7}, LX/En2;->c(I)Z

    move-result p0

    if-nez p0, :cond_6

    .line 2167595
    invoke-static {v0, v7, v4, v5}, LX/Enz;->a(LX/Enz;ILjava/lang/String;LX/Enn;)V

    goto/16 :goto_2

    .line 2167596
    :cond_6
    iget-object p0, v0, LX/Enz;->f:LX/En2;

    invoke-virtual {p0, v7, v4}, LX/En2;->a(ILjava/lang/Object;)V

    .line 2167597
    iget-object p0, v0, LX/Enz;->g:Ljava/util/HashMap;

    invoke-virtual {p0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167598
    iget-object p0, v0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {p0, v4, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 2167599
    :cond_7
    iget-object p1, v0, LX/Enz;->f:LX/En2;

    invoke-virtual {p1, v8}, LX/En2;->c(I)Z

    move-result p1

    if-nez p1, :cond_8

    .line 2167600
    invoke-static {v0, v8, v4, v5}, LX/Enz;->a(LX/Enz;ILjava/lang/String;LX/Enn;)V

    goto :goto_6

    .line 2167601
    :cond_8
    iget-object p1, v0, LX/Enz;->f:LX/En2;

    invoke-virtual {p1, v8, v4}, LX/En2;->a(ILjava/lang/Object;)V

    .line 2167602
    iget-object p1, v0, LX/Enz;->g:Ljava/util/HashMap;

    invoke-virtual {p1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167603
    iget-object p1, v0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {p1, v4, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6
.end method
