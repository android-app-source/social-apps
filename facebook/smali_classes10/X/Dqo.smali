.class public LX/Dqo;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field private b:LX/2Yg;

.field private c:LX/1rn;

.field private d:Lcom/facebook/content/SecureContextHelper;

.field private e:LX/DqG;

.field private f:LX/03V;

.field private g:LX/2c4;

.field private h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2048618
    const-string v0, "REDIRECT_INTENT"

    sput-object v0, LX/Dqo;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2Yg;LX/1rn;Lcom/facebook/content/SecureContextHelper;LX/DqG;LX/03V;LX/2c4;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Yg;",
            "LX/1rn;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/DqG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2c4;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048620
    iput-object p1, p0, LX/Dqo;->b:LX/2Yg;

    .line 2048621
    iput-object p2, p0, LX/Dqo;->c:LX/1rn;

    .line 2048622
    iput-object p3, p0, LX/Dqo;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2048623
    iput-object p4, p0, LX/Dqo;->e:LX/DqG;

    .line 2048624
    iput-object p5, p0, LX/Dqo;->f:LX/03V;

    .line 2048625
    iput-object p6, p0, LX/Dqo;->g:LX/2c4;

    .line 2048626
    iput-object p7, p0, LX/Dqo;->h:LX/0Ot;

    .line 2048627
    return-void
.end method

.method public static b(LX/0QB;)LX/Dqo;
    .locals 8

    .prologue
    .line 2048628
    new-instance v0, LX/Dqo;

    invoke-static {p0}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v1

    check-cast v1, LX/2Yg;

    invoke-static {p0}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v2

    check-cast v2, LX/1rn;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/DqG;->a(LX/0QB;)LX/DqG;

    move-result-object v4

    check-cast v4, LX/DqG;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v6

    check-cast v6, LX/2c4;

    const/16 v7, 0x97

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/Dqo;-><init>(LX/2Yg;LX/1rn;Lcom/facebook/content/SecureContextHelper;LX/DqG;LX/03V;LX/2c4;LX/0Ot;)V

    .line 2048629
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2048630
    :try_start_0
    const-string v0, "EVENT_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3B2;

    .line 2048631
    const-string v1, "COMPONENT_TYPE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8D4;

    .line 2048632
    const-string v2, "NOTIF_LOG"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 2048633
    const-string v3, "NOTIFICATION_ID"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 2048634
    if-eqz v3, :cond_1

    .line 2048635
    iget-object v4, p0, LX/Dqo;->g:LX/2c4;

    invoke-virtual {v4, v3}, LX/2c4;->a(I)V

    .line 2048636
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2048637
    :cond_0
    :goto_0
    iget-object v3, p0, LX/Dqo;->b:LX/2Yg;

    invoke-virtual {v3, v2, v0}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;LX/3B2;)V

    .line 2048638
    iget-object v3, p0, LX/Dqo;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gh;

    const-string v4, "tap_system_tray_notification"

    invoke-virtual {v3, v4}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2048639
    sget-object v3, LX/3B2;->CLEAR_FROM_TRAY:LX/3B2;

    if-eq v0, v3, :cond_2

    .line 2048640
    iget-object v0, p0, LX/Dqo;->c:LX/1rn;

    invoke-virtual {v0, v2}, LX/1rn;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 2048641
    sget-object v0, LX/Dqo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 2048642
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    .line 2048643
    sget-object v2, LX/Dqn;->a:[I

    invoke-virtual {v1}, LX/8D4;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2048644
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2048645
    iget-object v1, p0, LX/Dqo;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2048646
    :goto_1
    return-void

    .line 2048647
    :cond_1
    if-eqz v2, :cond_0

    .line 2048648
    iget-object v3, v2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2048649
    if-eqz v3, :cond_0

    .line 2048650
    iget-object v3, p0, LX/Dqo;->g:LX/2c4;

    .line 2048651
    iget-object v4, v2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->f:Ljava/lang/String;

    move-object v4, v4

    .line 2048652
    invoke-virtual {v3, v4}, LX/2c4;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2048653
    :catch_0
    move-exception v0

    .line 2048654
    iget-object v1, p0, LX/Dqo;->f:LX/03V;

    const-class v2, Lcom/facebook/notifications/service/SystemTrayLogService;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error reading notification"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2048655
    :pswitch_0
    :try_start_1
    invoke-virtual {p2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1

    .line 2048656
    :pswitch_1
    iget-object v1, p0, LX/Dqo;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 2048657
    :cond_2
    iget-object v0, p0, LX/Dqo;->e:LX/DqG;

    invoke-virtual {v0}, LX/DqG;->b()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
