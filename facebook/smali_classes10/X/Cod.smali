.class public abstract LX/Cod;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/richdocument/presenter/BlockPresenter;",
        ">",
        "Ljava/lang/Object;",
        "LX/02k;",
        "LX/CnG",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public a:LX/1a1;

.field private b:Landroid/view/View;

.field private c:LX/CnT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public d:LX/Cmz;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1935881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1935882
    iput-object p1, p0, LX/Cod;->b:Landroid/view/View;

    .line 1935883
    return-void
.end method


# virtual methods
.method public a(LX/1a1;)V
    .locals 0

    .prologue
    .line 1935884
    iput-object p1, p0, LX/Cod;->a:LX/1a1;

    .line 1935885
    return-void
.end method

.method public final a(LX/Cml;)V
    .locals 2

    .prologue
    .line 1935876
    iget-object v0, p0, LX/Cod;->d:LX/Cmz;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1935877
    iget-object v0, p0, LX/Cod;->d:LX/Cmz;

    iget-object v1, p0, LX/Cod;->b:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, LX/Cmz;->a(Landroid/view/View;LX/Cml;)V

    .line 1935878
    :cond_0
    return-void
.end method

.method public a(LX/CnT;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1935887
    iput-object p1, p0, LX/Cod;->c:LX/CnT;

    .line 1935888
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1935886
    return-void
.end method

.method public a(II)Z
    .locals 1

    .prologue
    .line 1935879
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1935880
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 1

    .prologue
    .line 1935873
    iget-object v0, p0, LX/Cod;->b:Landroid/view/View;

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1935871
    return-void
.end method

.method public final d(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 1935872
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1935874
    iget-object v0, p0, LX/Cod;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final jh_()LX/CnT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1935875
    iget-object v0, p0, LX/Cod;->c:LX/CnT;

    return-object v0
.end method
