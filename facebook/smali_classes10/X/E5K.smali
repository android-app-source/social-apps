.class public final LX/E5K;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5L;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/E5L;


# direct methods
.method public constructor <init>(LX/E5L;)V
    .locals 1

    .prologue
    .line 2078237
    iput-object p1, p0, LX/E5K;->d:LX/E5L;

    .line 2078238
    move-object v0, p1

    .line 2078239
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2078240
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078241
    const-string v0, "ReactionPlaceWithMetadataComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2078242
    if-ne p0, p1, :cond_1

    .line 2078243
    :cond_0
    :goto_0
    return v0

    .line 2078244
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078245
    goto :goto_0

    .line 2078246
    :cond_3
    check-cast p1, LX/E5K;

    .line 2078247
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078248
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078249
    if-eq v2, v3, :cond_0

    .line 2078250
    iget-object v2, p0, LX/E5K;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E5K;->a:Ljava/lang/String;

    iget-object v3, p1, LX/E5K;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2078251
    goto :goto_0

    .line 2078252
    :cond_5
    iget-object v2, p1, LX/E5K;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2078253
    :cond_6
    iget-object v2, p0, LX/E5K;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E5K;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    iget-object v3, p1, LX/E5K;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2078254
    goto :goto_0

    .line 2078255
    :cond_8
    iget-object v2, p1, LX/E5K;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    if-nez v2, :cond_7

    .line 2078256
    :cond_9
    iget-object v2, p0, LX/E5K;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/E5K;->c:Ljava/lang/String;

    iget-object v3, p1, LX/E5K;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2078257
    goto :goto_0

    .line 2078258
    :cond_a
    iget-object v2, p1, LX/E5K;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
