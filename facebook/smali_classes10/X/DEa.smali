.class public final LX/DEa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/2cr;


# direct methods
.method public constructor <init>(LX/2cr;)V
    .locals 0

    .prologue
    .line 1977168
    iput-object p1, p0, LX/DEa;->a:LX/2cr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x29de75

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1977169
    iget-object v1, p0, LX/DEa;->a:LX/2cr;

    .line 1977170
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1977171
    iget-object v4, v1, LX/2cr;->b:LX/2d0;

    invoke-virtual {v4}, LX/2d0;->a()Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-result-object v4

    .line 1977172
    if-nez v4, :cond_0

    .line 1977173
    iget-object v3, v1, LX/2cr;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    const-string v4, "place_tips"

    new-instance v5, Ljava/lang/NullPointerException;

    const-string v6, "place tips feed unit click w/ null presence description"

    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1977174
    :goto_0
    const v1, 0x14aa61b7

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1977175
    :cond_0
    const-class v5, LX/0f4;

    invoke-static {v3, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0f4;

    .line 1977176
    if-nez v5, :cond_2

    .line 1977177
    const/4 v5, 0x0

    .line 1977178
    :goto_1
    move-object v5, v5

    .line 1977179
    if-nez v5, :cond_1

    .line 1977180
    :goto_2
    goto :goto_0

    .line 1977181
    :cond_1
    iget-object v6, v1, LX/2cr;->a:LX/2cw;

    sget-object v7, LX/3FC;->FEED_UNIT_CLICK:LX/3FC;

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v8

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->d()Z

    move-result p1

    invoke-virtual {v6, v7, v8, p0, p1}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/String;Z)V

    .line 1977182
    iget-object v6, v1, LX/2cr;->b:LX/2d0;

    sget-object v7, LX/8Y9;->NEWSFEED:LX/8Y9;

    invoke-virtual {v6, v5, v7}, LX/2d0;->a(Lcom/facebook/base/fragment/FbFragment;LX/8Y9;)V

    goto :goto_2

    :cond_2
    invoke-interface {v5}, LX/0f4;->c()Lcom/facebook/base/fragment/FbFragment;

    move-result-object v5

    goto :goto_1
.end method
