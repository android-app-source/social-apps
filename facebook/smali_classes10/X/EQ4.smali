.class public final LX/EQ4;
.super LX/Cwh;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cwh",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EQ6;


# direct methods
.method public constructor <init>(LX/EQ6;)V
    .locals 0

    .prologue
    .line 2118174
    iput-object p1, p0, LX/EQ4;->a:LX/EQ6;

    invoke-direct {p0}, LX/Cwh;-><init>()V

    .line 2118175
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118172
    iget-object v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2118173
    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118171
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateModuleSuggestionUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118169
    iget-object v0, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->j:Ljava/lang/String;

    move-object v0, v0

    .line 2118170
    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2118168
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[See More] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118176
    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2118177
    iget-object v0, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->m:Ljava/lang/String;

    move-object v0, v0

    .line 2118178
    :goto_0
    return-object v0

    .line 2118179
    :cond_0
    iget-object v0, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2118180
    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118166
    iget-object v0, p1, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->a:Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-object v0, v0

    .line 2118167
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118165
    invoke-virtual {p1}, Lcom/facebook/search/model/SeeMoreResultPageUnit;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2118161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[See More] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118163
    iget-object v0, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2118164
    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/TrendingTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2118162
    invoke-virtual {p1}, Lcom/facebook/search/model/TrendingTypeaheadUnit;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
