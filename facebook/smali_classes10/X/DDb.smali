.class public final LX/DDb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1975855
    iput-object p1, p0, LX/DDb;->b:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

    iput-object p2, p0, LX/DDb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x6474589e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1975856
    const-string v3, "ssfy_click"

    iget-object v0, p0, LX/DDb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975857
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1975858
    check-cast v0, LX/16h;

    iget-object v1, p0, LX/DDb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, LX/16h;

    invoke-static {v0, v1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    invoke-static {v3, v0}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1975859
    iget-object v1, p0, LX/DDb;->b:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->g:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1975860
    iget-object v0, p0, LX/DDb;->b:Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/groupcommerce/ForSaleHScrollItemAttachmentTitlePartDefinition;->d:LX/3Do;

    iget-object v1, p0, LX/DDb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/3Do;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2tm;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2tm;->onClick(Landroid/view/View;)V

    .line 1975861
    const v0, 0x1bbb3e2c

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
