.class public final LX/EAK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/EAO;


# direct methods
.method public constructor <init>(LX/EAO;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2085461
    iput-object p1, p0, LX/EAK;->d:LX/EAO;

    iput-object p2, p0, LX/EAK;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/EAK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/EAK;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2085462
    iget-object v0, p0, LX/EAK;->d:LX/EAO;

    iget-object v0, v0, LX/EAO;->c:LX/79D;

    const-string v1, "delete_menu_option_tap"

    iget-object v2, p0, LX/EAK;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/79D;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 2085463
    iget-object v0, p0, LX/EAK;->d:LX/EAO;

    iget-object v1, p0, LX/EAK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/EAK;->c:Landroid/content/Context;

    .line 2085464
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2085465
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2085466
    new-instance v4, LX/0ju;

    invoke-direct {v4, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0814e9

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    const p0, 0x7f08105c

    new-instance p1, LX/EAL;

    invoke-direct {p1, v0, v3, v1, v2}, LX/EAL;-><init>(LX/EAO;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-virtual {v4, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const v4, 0x7f0810d9

    const/4 p0, 0x0

    invoke-virtual {v3, v4, p0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    .line 2085467
    const/4 v0, 0x1

    return v0
.end method
