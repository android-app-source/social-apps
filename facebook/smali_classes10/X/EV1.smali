.class public final LX/EV1;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Ljava/lang/ref/WeakReference;

.field public final synthetic c:LX/EU1;

.field public final synthetic d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic e:LX/EU2;

.field public final synthetic f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;LX/0Px;Ljava/lang/ref/WeakReference;LX/EU1;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EU2;)V
    .locals 0

    .prologue
    .line 2127108
    iput-object p1, p0, LX/EV1;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    iput-object p2, p0, LX/EV1;->a:LX/0Px;

    iput-object p3, p0, LX/EV1;->b:Ljava/lang/ref/WeakReference;

    iput-object p4, p0, LX/EV1;->c:LX/EU1;

    iput-object p5, p0, LX/EV1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p6, p0, LX/EV1;->e:LX/EU2;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 2127064
    iget-object v0, p0, LX/EV1;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->n:LX/EVd;

    invoke-virtual {v0}, LX/EVd;->d()Z

    move-result v4

    .line 2127065
    if-nez v4, :cond_1

    .line 2127066
    iget-object v0, p0, LX/EV1;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    iget-object v0, p0, LX/EV1;->a:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127067
    iget-object v1, p0, LX/EV1;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    iget-object v1, v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->f:LX/1vo;

    .line 2127068
    iget-object v6, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v6

    .line 2127069
    invoke-interface {v6}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v1

    .line 2127070
    invoke-interface {v1, v0}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2127071
    instance-of v6, v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v6, :cond_0

    .line 2127072
    const/4 v2, 0x1

    .line 2127073
    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {p1, v1, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    :cond_0
    move v0, v2

    .line 2127074
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 2127075
    :cond_1
    if-nez v2, :cond_3

    .line 2127076
    iget-object v1, p0, LX/EV1;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    iget-object v0, p0, LX/EV1;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETX;

    .line 2127077
    if-eqz v0, :cond_2

    const-string v3, "VIDEO_HOME"

    move-object v2, v0

    check-cast v2, LX/ETy;

    .line 2127078
    const-string v5, "VIDEO_HOME"

    move-object v2, v5

    .line 2127079
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2127080
    :cond_2
    :goto_1
    iget-object v0, p0, LX/EV1;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->i:Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;

    invoke-virtual {p1, v0, v7}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2127081
    iget-object v0, p0, LX/EV1;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->i:Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;

    invoke-virtual {p1, v0, v7}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2127082
    iget-object v0, p0, LX/EV1;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->i:Lcom/facebook/video/videohome/partdefinitions/VideoHomeLoadingPageHscrollLiveVideoPartDefinition;

    invoke-virtual {p1, v0, v7}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2127083
    :cond_3
    if-eqz v4, :cond_4

    .line 2127084
    iget-object v0, p0, LX/EV1;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->n:LX/EVd;

    new-instance v1, LX/EV0;

    invoke-direct {v1, p0}, LX/EV0;-><init>(LX/EV1;)V

    .line 2127085
    iput-object v1, v0, LX/EVd;->c:LX/EV0;

    .line 2127086
    :cond_4
    return-void

    .line 2127087
    :cond_5
    check-cast v0, LX/1Ps;

    invoke-interface {v0}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v2

    .line 2127088
    instance-of v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v3, :cond_2

    .line 2127089
    check-cast v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127090
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v3

    .line 2127091
    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    .line 2127092
    if-eqz v2, :cond_2

    .line 2127093
    iget-object v3, v1, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->j:LX/03V;

    const-string v5, "VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition.noPages"

    const-string v6, "No needed pages for section: %s"

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final c(I)V
    .locals 5

    .prologue
    .line 2127094
    iget-object v0, p0, LX/EV1;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETX;

    .line 2127095
    if-nez v0, :cond_0

    .line 2127096
    :goto_0
    return-void

    .line 2127097
    :cond_0
    iget-object v1, p0, LX/EV1;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    move-object v1, v0

    .line 2127098
    check-cast v1, LX/1Pr;

    iget-object v2, p0, LX/EV1;->c:LX/EU1;

    iget-object v3, p0, LX/EV1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v1, v2, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EU2;

    .line 2127099
    iput p1, v1, LX/EU2;->c:I

    .line 2127100
    move-object v1, v0

    .line 2127101
    check-cast v1, LX/ETi;

    iget-object v2, p0, LX/EV1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127102
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2127103
    invoke-interface {v1, v2}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v3

    move-object v1, v0

    .line 2127104
    check-cast v1, LX/ETe;

    iget-object v2, p0, LX/EV1;->a:LX/0Px;

    invoke-virtual {v2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127105
    iget-object v4, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v4

    .line 2127106
    iget-object v4, p0, LX/EV1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v1, p1, v3, v2, v4}, LX/ETe;->b(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2127107
    :cond_1
    iget-object v1, p0, LX/EV1;->f:Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;

    iget-object v2, p0, LX/EV1;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p0, LX/EV1;->e:LX/EU2;

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;->a$redex0(Lcom/facebook/video/videohome/partdefinitions/VideoHomePaginatedHscrollLiveVideoComponentListPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/ETX;LX/EU2;)V

    goto :goto_0
.end method
