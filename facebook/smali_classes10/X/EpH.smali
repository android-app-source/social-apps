.class public LX/EpH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/EpH;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G4X;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/timeline/services/ProfileActionClient;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2170118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170119
    return-void
.end method

.method public static a(LX/0QB;)LX/EpH;
    .locals 10

    .prologue
    .line 2170120
    sget-object v0, LX/EpH;->h:LX/EpH;

    if-nez v0, :cond_1

    .line 2170121
    const-class v1, LX/EpH;

    monitor-enter v1

    .line 2170122
    :try_start_0
    sget-object v0, LX/EpH;->h:LX/EpH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2170123
    if-eqz v2, :cond_0

    .line 2170124
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2170125
    new-instance v3, LX/EpH;

    invoke-direct {v3}, LX/EpH;-><init>()V

    .line 2170126
    const/16 v4, 0x36f0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1430

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x36ed

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x455

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x97

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x15e7

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 p0, 0x2eb

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2170127
    iput-object v4, v3, LX/EpH;->a:LX/0Or;

    iput-object v5, v3, LX/EpH;->b:LX/0Or;

    iput-object v6, v3, LX/EpH;->c:LX/0Or;

    iput-object v7, v3, LX/EpH;->d:LX/0Or;

    iput-object v8, v3, LX/EpH;->e:LX/0Or;

    iput-object v9, v3, LX/EpH;->f:LX/0Or;

    iput-object p0, v3, LX/EpH;->g:LX/0Or;

    .line 2170128
    move-object v0, v3

    .line 2170129
    sput-object v0, LX/EpH;->h:LX/EpH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2170130
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2170131
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2170132
    :cond_1
    sget-object v0, LX/EpH;->h:LX/EpH;

    return-object v0

    .line 2170133
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2170134
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/5wO;ILandroid/content/Context;)Z
    .locals 8
    .param p2    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 2170135
    packed-switch p2, :pswitch_data_0

    .line 2170136
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2170137
    :pswitch_1
    iget-object v0, p0, LX/EpH;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    .line 2170138
    const-string v1, "person_card_message_button"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2170139
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    invoke-interface {p1}, LX/5wO;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2170140
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2170141
    const-string v3, "trigger"

    const-string v4, "person_card"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2170142
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2170143
    iget-object v0, p0, LX/EpH;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    .line 2170144
    invoke-interface {v0, v1, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    move v0, v2

    .line 2170145
    goto :goto_0

    .line 2170146
    :pswitch_2
    iget-object v0, p0, LX/EpH;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/services/ProfileActionClient;

    .line 2170147
    iget-object v1, p0, LX/EpH;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-interface {p1}, LX/5wO;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/facebook/timeline/services/ProfileActionClient;->a(JJ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2170148
    iget-object v0, p0, LX/EpH;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    .line 2170149
    iget-object v1, p0, LX/EpH;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G4X;

    .line 2170150
    invoke-interface {p1}, LX/5wO;->ce_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p3, v4}, LX/G4X;->a(Landroid/content/Context;Ljava/lang/String;)LX/0TF;

    move-result-object v1

    invoke-static {v3, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    move v0, v2

    .line 2170151
    goto :goto_0

    .line 2170152
    :pswitch_3
    iget-object v0, p0, LX/EpH;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    sget-object v1, LX/0ax;->ep:Ljava/lang/String;

    iget-object v3, p0, LX/EpH;->f:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1}, LX/5wO;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move v0, v2

    .line 2170153
    goto/16 :goto_0

    .line 2170154
    :pswitch_4
    iget-object v0, p0, LX/EpH;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    sget-object v1, LX/0ax;->dv:Ljava/lang/String;

    invoke-interface {p1}, LX/5wO;->c()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v5, LX/0wD;->TIMELINE_SOMEONE_ELSE:LX/0wD;

    invoke-virtual {v5}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move v0, v2

    .line 2170155
    goto/16 :goto_0

    .line 2170156
    :pswitch_5
    iget-object v0, p0, LX/EpH;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    sget-object v1, LX/0ax;->I:Ljava/lang/String;

    invoke-interface {p1}, LX/5wO;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/5wO;->ce_()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move v0, v2

    .line 2170157
    goto/16 :goto_0

    .line 2170158
    :pswitch_6
    iget-object v0, p0, LX/EpH;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    sget-object v1, LX/0ax;->dz:Ljava/lang/String;

    invoke-virtual {v0, p3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move v0, v2

    .line 2170159
    goto/16 :goto_0

    .line 2170160
    :pswitch_7
    iget-object v0, p0, LX/EpH;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    sget-object v1, LX/0ax;->bL:Ljava/lang/String;

    invoke-interface {p1}, LX/5wO;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move v0, v2

    .line 2170161
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_5
    .end packed-switch
.end method
