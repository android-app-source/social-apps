.class public LX/Ept;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Ai;

.field public final b:LX/1Ai;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

.field public final e:LX/EoZ;

.field public final f:LX/EpA;

.field public final g:LX/Emj;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BA0;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Emj;LX/EpA;LX/EoZ;LX/0Or;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Emj;",
            "LX/EpA;",
            "LX/EoZ;",
            "LX/0Or",
            "<",
            "LX/BA0;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2170712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170713
    new-instance v0, LX/Epq;

    invoke-direct {v0, p0}, LX/Epq;-><init>(LX/Ept;)V

    iput-object v0, p0, LX/Ept;->a:LX/1Ai;

    .line 2170714
    new-instance v0, LX/Epr;

    invoke-direct {v0, p0}, LX/Epr;-><init>(LX/Ept;)V

    iput-object v0, p0, LX/Ept;->b:LX/1Ai;

    .line 2170715
    iput-object p1, p0, LX/Ept;->g:LX/Emj;

    .line 2170716
    iput-object p2, p0, LX/Ept;->f:LX/EpA;

    .line 2170717
    iput-object p3, p0, LX/Ept;->e:LX/EoZ;

    .line 2170718
    iput-object p4, p0, LX/Ept;->h:LX/0Or;

    .line 2170719
    iput-object p5, p0, LX/Ept;->i:Ljava/util/concurrent/Executor;

    .line 2170720
    return-void
.end method

.method public static a$redex0(LX/Ept;Ljava/lang/String;Ljava/lang/String;LX/EnC;)V
    .locals 1

    .prologue
    .line 2170721
    iget-object v0, p0, LX/Ept;->g:LX/Emj;

    invoke-interface {v0, p2, p3, p1}, LX/Emj;->a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V

    .line 2170722
    return-void
.end method


# virtual methods
.method public final b(Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;)V
    .locals 4

    .prologue
    .line 2170723
    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eon;

    .line 2170724
    iget-object v1, p0, LX/Ept;->g:LX/Emj;

    const-string v2, "ec_card_recycled"

    sget-object v3, LX/EnC;->SUCCEEDED:LX/EnC;

    invoke-interface {v0}, LX/Eon;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, LX/Emj;->a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V

    .line 2170725
    invoke-virtual {p1}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a()V

    .line 2170726
    return-void
.end method
