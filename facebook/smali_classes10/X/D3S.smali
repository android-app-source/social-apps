.class public LX/D3S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private a:LX/0Zb;

.field private final b:LX/ClD;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0Zb;LX/ClD;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1959808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1959809
    iput-object p1, p0, LX/D3S;->a:LX/0Zb;

    .line 1959810
    iput-object p2, p0, LX/D3S;->b:LX/ClD;

    .line 1959811
    iput-object p3, p0, LX/D3S;->c:Landroid/content/Context;

    .line 1959812
    return-void
.end method

.method public static a(LX/0QB;)LX/D3S;
    .locals 6

    .prologue
    .line 1959813
    const-class v1, LX/D3S;

    monitor-enter v1

    .line 1959814
    :try_start_0
    sget-object v0, LX/D3S;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1959815
    sput-object v2, LX/D3S;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1959816
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1959817
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1959818
    new-instance p0, LX/D3S;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v4

    check-cast v4, LX/ClD;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, LX/D3S;-><init>(LX/0Zb;LX/ClD;Landroid/content/Context;)V

    .line 1959819
    move-object v0, p0

    .line 1959820
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1959821
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D3S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1959822
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1959823
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
