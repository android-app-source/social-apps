.class public LX/ElX;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/Ekr;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/Ekr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164492
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Ekr;
    .locals 4

    .prologue
    .line 2164493
    sget-object v0, LX/ElX;->a:LX/Ekr;

    if-nez v0, :cond_1

    .line 2164494
    const-class v1, LX/ElX;

    monitor-enter v1

    .line 2164495
    :try_start_0
    sget-object v0, LX/ElX;->a:LX/Ekr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2164496
    if-eqz v2, :cond_0

    .line 2164497
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2164498
    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/El3;->a(LX/0QB;)LX/El3;

    move-result-object p0

    check-cast p0, LX/El3;

    invoke-static {v3, p0}, LX/Elb;->a(Ljava/util/concurrent/ExecutorService;LX/El3;)LX/Ekr;

    move-result-object v3

    move-object v0, v3

    .line 2164499
    sput-object v0, LX/ElX;->a:LX/Ekr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2164500
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2164501
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2164502
    :cond_1
    sget-object v0, LX/ElX;->a:LX/Ekr;

    return-object v0

    .line 2164503
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2164504
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2164505
    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/El3;->a(LX/0QB;)LX/El3;

    move-result-object v1

    check-cast v1, LX/El3;

    invoke-static {v0, v1}, LX/Elb;->a(Ljava/util/concurrent/ExecutorService;LX/El3;)LX/Ekr;

    move-result-object v0

    return-object v0
.end method
