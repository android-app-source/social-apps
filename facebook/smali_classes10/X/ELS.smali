.class public final LX/ELS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/CxA;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;LX/CzL;LX/CxA;)V
    .locals 0

    .prologue
    .line 2108987
    iput-object p1, p0, LX/ELS;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    iput-object p2, p0, LX/ELS;->a:LX/CzL;

    iput-object p3, p0, LX/ELS;->b:LX/CxA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x40e2a0ee

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2108970
    iget-object v0, p0, LX/ELS;->a:LX/CzL;

    .line 2108971
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2108972
    check-cast v0, LX/8dA;

    .line 2108973
    iget-object v1, p0, LX/ELS;->a:LX/CzL;

    invoke-static {v1}, LX/CzN;->b(LX/CzL;)LX/CzL;

    move-result-object v4

    .line 2108974
    iget-object v1, p0, LX/ELS;->a:LX/CzL;

    if-ne v1, v4, :cond_0

    .line 2108975
    const v0, -0x43d7ffef

    invoke-static {v2, v2, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2108976
    :goto_0
    return-void

    .line 2108977
    :cond_0
    new-instance v5, LX/EJJ;

    iget-object v1, p0, LX/ELS;->a:LX/CzL;

    .line 2108978
    iget-object v2, v1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v2

    .line 2108979
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-direct {v5, v1}, LX/EJJ;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)V

    .line 2108980
    iget-object v1, p0, LX/ELS;->b:LX/CxA;

    check-cast v1, LX/1Pr;

    iget-object v2, p0, LX/ELS;->a:LX/CzL;

    .line 2108981
    iget-object p1, v2, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, p1

    .line 2108982
    check-cast v2, LX/8dA;

    invoke-interface {v2}, LX/8dA;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    invoke-interface {v1, v5, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2108983
    iget-object v1, p0, LX/ELS;->b:LX/CxA;

    iget-object v2, p0, LX/ELS;->a:LX/CzL;

    invoke-interface {v1, v2, v4}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2108984
    iget-object v1, p0, LX/ELS;->b:LX/CxA;

    check-cast v1, LX/1Pq;

    invoke-interface {v1}, LX/1Pq;->iN_()V

    .line 2108985
    iget-object v1, p0, LX/ELS;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "apply_mutation_"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, LX/8dA;->dW_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v5, LX/ELQ;

    invoke-direct {v5, p0, v0}, LX/ELQ;-><init>(LX/ELS;LX/8dA;)V

    new-instance v0, LX/ELR;

    invoke-direct {v0, p0, v4}, LX/ELR;-><init>(LX/ELS;LX/CzL;)V

    invoke-virtual {v1, v2, v5, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2108986
    const v0, -0x5b1d2584

    invoke-static {v0, v3}, LX/02F;->a(II)V

    goto :goto_0
.end method
