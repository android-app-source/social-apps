.class public final LX/DmX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;)V
    .locals 0

    .prologue
    .line 2039294
    iput-object p1, p0, LX/DmX;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x1608db27

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2039295
    iget-object v0, p0, LX/DmX;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->f:LX/Dih;

    iget-object v1, p0, LX/DmX;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    iget-object v2, p0, LX/DmX;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    .line 2039296
    iget-object v3, v0, LX/Dih;->a:LX/0Zb;

    const-string v4, "profservices_booking_consumer_decline_appointment"

    invoke-static {v4, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v6, "request_id"

    invoke-virtual {v4, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2039297
    iget-object v0, p0, LX/DmX;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->b:LX/Dka;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DmX;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2039298
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2039299
    iget-object v3, p0, LX/DmX;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    iget-object v4, p0, LX/DmX;->a:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->g:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, LX/Dka;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/DjX;)V

    .line 2039300
    const v0, -0x27c3db5e

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
