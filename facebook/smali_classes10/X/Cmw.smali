.class public LX/Cmw;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/Cmw;

.field public static final b:LX/Cmw;


# instance fields
.field public final c:LX/Cmv;

.field public final d:LX/Cmv;

.field public final e:LX/Cmv;

.field public final f:LX/Cmv;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1933647
    new-instance v0, LX/Cmw;

    sget-object v1, LX/Cmv;->b:LX/Cmv;

    sget-object v2, LX/Cmv;->b:LX/Cmv;

    sget-object v3, LX/Cmv;->b:LX/Cmv;

    sget-object v4, LX/Cmv;->b:LX/Cmv;

    invoke-direct {v0, v1, v2, v3, v4}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    sput-object v0, LX/Cmw;->a:LX/Cmw;

    .line 1933648
    new-instance v0, LX/Cmw;

    sget-object v1, LX/Cmv;->c:LX/Cmv;

    sget-object v2, LX/Cmv;->a:LX/Cmv;

    sget-object v3, LX/Cmv;->c:LX/Cmv;

    sget-object v4, LX/Cmv;->a:LX/Cmv;

    invoke-direct {v0, v1, v2, v3, v4}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    sput-object v0, LX/Cmw;->b:LX/Cmw;

    return-void
.end method

.method public constructor <init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V
    .locals 0

    .prologue
    .line 1933649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933650
    iput-object p1, p0, LX/Cmw;->c:LX/Cmv;

    .line 1933651
    iput-object p2, p0, LX/Cmw;->d:LX/Cmv;

    .line 1933652
    iput-object p3, p0, LX/Cmw;->e:LX/Cmv;

    .line 1933653
    iput-object p4, p0, LX/Cmw;->f:LX/Cmv;

    .line 1933654
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1933655
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x7b

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "left="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Cmw;->c:LX/Cmv;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", top="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Cmw;->d:LX/Cmv;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", right="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Cmw;->e:LX/Cmv;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bottom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Cmw;->f:LX/Cmv;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
