.class public final LX/CjK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/richdocument/genesis/BlockCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11i;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Pq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1929298
    const-class v0, LX/CjK;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CjK;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;Ljava/util/Set;LX/0Pq;)V
    .locals 5
    .param p3    # LX/0Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/11i;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/richdocument/genesis/BlockCreator;",
            ">;",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929300
    iput-object p1, p0, LX/CjK;->c:LX/0Ot;

    .line 1929301
    iput-object p3, p0, LX/CjK;->d:LX/0Pq;

    .line 1929302
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CjK;->a:Ljava/util/Map;

    .line 1929303
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CjH;

    .line 1929304
    iget v2, v0, LX/CjH;->b:I

    move v2, v2

    .line 1929305
    iget-object v3, p0, LX/CjK;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1929306
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Found block type conflict. value:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", creator:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1929307
    :cond_0
    iget-object v3, p0, LX/CjK;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1929308
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)LX/Cs4;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1929309
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/CjK;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1929310
    iget-object v0, p0, LX/CjK;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CjH;

    .line 1929311
    iget-object v1, p0, LX/CjK;->d:LX/0Pq;

    if-eqz v1, :cond_3

    .line 1929312
    iget-object v1, p0, LX/CjK;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11i;

    iget-object v3, p0, LX/CjK;->d:LX/0Pq;

    invoke-interface {v1, v3}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 1929313
    :goto_0
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1929314
    const-string v3, "rich_document_block_type"

    .line 1929315
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 1929316
    invoke-static {v3, v4}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    .line 1929317
    const-string v4, "rich_document_block_creation"

    const v5, 0x5871fde4

    invoke-static {v1, v4, v2, v3, v5}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1929318
    :cond_0
    invoke-virtual {v0, p2}, LX/CjH;->a(Landroid/view/ViewGroup;)LX/Cs4;

    move-result-object v0

    .line 1929319
    if-eqz v1, :cond_1

    .line 1929320
    const-string v2, "rich_document_block_creation"

    const v3, -0x77ee415b

    invoke-static {v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1929321
    :cond_1
    :goto_1
    return-object v0

    :cond_2
    move-object v0, v2

    goto :goto_1

    :cond_3
    move-object v1, v2

    goto :goto_0
.end method
