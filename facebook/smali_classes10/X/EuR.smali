.class public LX/EuR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2179598
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "first_name"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, LX/EuR;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2179596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179597
    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2179599
    new-instance v0, LX/EvN;

    invoke-direct {v0}, LX/EvN;-><init>()V

    move-object v0, v0

    .line 2179600
    const-string v1, "after_param"

    .line 2179601
    iget-object v2, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2179602
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first_param"

    .line 2179603
    iget v3, p1, LX/3DR;->e:I

    move v3, v3

    .line 2179604
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "order_param"

    sget-object v3, LX/EuR;->a:Ljava/util/List;

    invoke-virtual {v1, v2, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2179605
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179591
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179592
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v5

    .line 2179593
    new-instance v0, LX/5Mb;

    .line 2179594
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2179595
    check-cast v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v4

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/5Mb;-><init>(LX/0Px;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method
