.class public final LX/DNc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/groups/feed/data/GraphSearchQueryGroupsModifier;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1991878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1991879
    new-instance v1, LX/DNd;

    invoke-direct {v1}, LX/DNd;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1991880
    iput-object v0, v1, LX/DNd;->a:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1991881
    move-object v0, v1

    .line 1991882
    invoke-virtual {v0}, LX/DNd;->a()Lcom/facebook/groups/feed/data/GraphSearchQueryGroupsModifier;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1991883
    new-array v0, p1, [Lcom/facebook/groups/feed/data/GraphSearchQueryGroupsModifier;

    return-object v0
.end method
