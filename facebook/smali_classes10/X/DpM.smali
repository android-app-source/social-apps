.class public LX/DpM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final instance_id:Ljava/lang/String;

.field public final user_id:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2044567
    new-instance v0, LX/1sv;

    const-string v1, "MessagingCollectionAddress"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpM;->b:LX/1sv;

    .line 2044568
    new-instance v0, LX/1sw;

    const-string v1, "user_id"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpM;->c:LX/1sw;

    .line 2044569
    new-instance v0, LX/1sw;

    const-string v1, "instance_id"

    const/16 v2, 0xb

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpM;->d:LX/1sw;

    .line 2044570
    const/4 v0, 0x1

    sput-boolean v0, LX/DpM;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2044563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2044564
    iput-object p1, p0, LX/DpM;->user_id:Ljava/lang/Long;

    .line 2044565
    iput-object p2, p0, LX/DpM;->instance_id:Ljava/lang/String;

    .line 2044566
    return-void
.end method

.method public static b(LX/1su;)LX/DpM;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2044549
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    .line 2044550
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2044551
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_2

    .line 2044552
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 2044553
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044554
    :pswitch_0
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xa

    if-ne v3, v4, :cond_0

    .line 2044555
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 2044556
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044557
    :pswitch_1
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v4, 0xb

    if-ne v3, v4, :cond_1

    .line 2044558
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2044559
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044560
    :cond_2
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2044561
    new-instance v2, LX/DpM;

    invoke-direct {v2, v1, v0}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    .line 2044562
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2044521
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2044522
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2044523
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2044524
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MessagingCollectionAddress"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2044525
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044526
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044527
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044528
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044529
    const-string v4, "user_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044530
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044531
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044532
    iget-object v4, p0, LX/DpM;->user_id:Ljava/lang/Long;

    if-nez v4, :cond_3

    .line 2044533
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044534
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044535
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044536
    const-string v4, "instance_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044537
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044538
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044539
    iget-object v0, p0, LX/DpM;->instance_id:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 2044540
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044541
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044542
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044543
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2044544
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2044545
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2044546
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2044547
    :cond_3
    iget-object v4, p0, LX/DpM;->user_id:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2044548
    :cond_4
    iget-object v0, p0, LX/DpM;->instance_id:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2044486
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2044487
    iget-object v0, p0, LX/DpM;->user_id:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2044488
    sget-object v0, LX/DpM;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044489
    iget-object v0, p0, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2044490
    :cond_0
    iget-object v0, p0, LX/DpM;->instance_id:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2044491
    sget-object v0, LX/DpM;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044492
    iget-object v0, p0, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2044493
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2044494
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2044495
    return-void
.end method

.method public final a(LX/DpM;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2044504
    if-nez p1, :cond_1

    .line 2044505
    :cond_0
    :goto_0
    return v2

    .line 2044506
    :cond_1
    iget-object v0, p0, LX/DpM;->user_id:Ljava/lang/Long;

    if-eqz v0, :cond_6

    move v0, v1

    .line 2044507
    :goto_1
    iget-object v3, p1, LX/DpM;->user_id:Ljava/lang/Long;

    if-eqz v3, :cond_7

    move v3, v1

    .line 2044508
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 2044509
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2044510
    iget-object v0, p0, LX/DpM;->user_id:Ljava/lang/Long;

    iget-object v3, p1, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2044511
    :cond_3
    iget-object v0, p0, LX/DpM;->instance_id:Ljava/lang/String;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2044512
    :goto_3
    iget-object v3, p1, LX/DpM;->instance_id:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2044513
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2044514
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2044515
    iget-object v0, p0, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v3, p1, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_5
    move v2, v1

    .line 2044516
    goto :goto_0

    :cond_6
    move v0, v2

    .line 2044517
    goto :goto_1

    :cond_7
    move v3, v2

    .line 2044518
    goto :goto_2

    :cond_8
    move v0, v2

    .line 2044519
    goto :goto_3

    :cond_9
    move v3, v2

    .line 2044520
    goto :goto_4
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2044500
    if-nez p1, :cond_1

    .line 2044501
    :cond_0
    :goto_0
    return v0

    .line 2044502
    :cond_1
    instance-of v1, p1, LX/DpM;

    if-eqz v1, :cond_0

    .line 2044503
    check-cast p1, LX/DpM;

    invoke-virtual {p0, p1}, LX/DpM;->a(LX/DpM;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2044499
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2044496
    sget-boolean v0, LX/DpM;->a:Z

    .line 2044497
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpM;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2044498
    return-object v0
.end method
