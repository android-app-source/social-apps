.class public LX/EVP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/04J;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2128215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2128216
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2128217
    iput-object v0, p0, LX/EVP;->a:LX/0Ot;

    .line 2128218
    return-void
.end method

.method public static b(LX/0QB;)LX/EVP;
    .locals 2

    .prologue
    .line 2128219
    new-instance v0, LX/EVP;

    invoke-direct {v0}, LX/EVP;-><init>()V

    .line 2128220
    const/16 v1, 0x12f3

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2128221
    iput-object v1, v0, LX/EVP;->a:LX/0Ot;

    .line 2128222
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/model/GraphQLStory;LX/ETi;)V
    .locals 7

    .prologue
    .line 2128223
    invoke-static {p2}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2128224
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2128225
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2128226
    invoke-interface {v2}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v3

    .line 2128227
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2128228
    invoke-interface {p3, v2}, LX/ETi;->b_(Ljava/lang/String;)I

    move-result v4

    .line 2128229
    invoke-interface {p3, v2, p2}, LX/ETi;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v5

    .line 2128230
    iget-object v2, p0, LX/EVP;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/04J;

    sget-object v6, LX/0JS;->REACTION_COMPONENT_TRACKING_FIELD:LX/0JS;

    iget-object v6, v6, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v2, v1, v6, v3}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2128231
    iget-object v2, p0, LX/EVP;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/04J;

    sget-object v3, LX/0JS;->REACTION_UNIT_TYPE:LX/0JS;

    iget-object v3, v3, LX/0JS;->value:Ljava/lang/String;

    .line 2128232
    iget-object v6, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 2128233
    invoke-virtual {v2, v1, v3, v6}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2128234
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2128235
    iget-object v2, p0, LX/EVP;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/04J;

    sget-object v3, LX/0JS;->BROADCAST_STATUS_FIELD:LX/0JS;

    iget-object v3, v3, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v3, v0}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2128236
    :cond_0
    iget-object v0, p0, LX/EVP;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/04J;

    sget-object v2, LX/0JS;->UNIT_POSITION:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2128237
    iget-object v0, p0, LX/EVP;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/04J;

    sget-object v2, LX/0JS;->POSITION_IN_UNIT:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2128238
    iget-object v0, p0, LX/EVP;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/04J;

    sget-object v2, LX/0JS;->TARGET_ID:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2128239
    iget-object v0, p0, LX/EVP;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/04J;

    sget-object v2, LX/0JS;->EVENT_TARGET:LX/0JS;

    iget-object v2, v2, LX/0JS;->value:Ljava/lang/String;

    const-string v3, "story"

    invoke-virtual {v0, v1, v2, v3}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2128240
    return-void
.end method
