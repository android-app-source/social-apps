.class public LX/D55;
.super LX/2oV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oV",
        "<",
        "LX/D5z;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

.field private final b:LX/1g1;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963337
    invoke-direct {p0, p1}, LX/2oV;-><init>(Ljava/lang/String;)V

    .line 1963338
    new-instance v0, LX/D54;

    sget-object v1, LX/1g2;->SPONSORED_IMPRESSION:LX/1g2;

    invoke-direct {v0, v1, p2}, LX/D54;-><init>(LX/1g2;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iput-object v0, p0, LX/D55;->b:LX/1g1;

    .line 1963339
    iput-object p3, p0, LX/D55;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    .line 1963340
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1963341
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1963342
    iget-object v0, p0, LX/D55;->a:Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;

    iget-object v1, p0, LX/D55;->b:LX/1g1;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/logging/impression/FeedUnitSponsoredImpressionLogger;->a(LX/1g1;)V

    .line 1963343
    return-void
.end method
