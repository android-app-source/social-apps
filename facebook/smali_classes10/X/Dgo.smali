.class public final LX/Dgo;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2029875
    iput-object p1, p0, LX/Dgo;->b:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iput-object p2, p0, LX/Dgo;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2029876
    iget-object v0, p0, LX/Dgo;->b:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->f:LX/Dgr;

    if-eqz v0, :cond_0

    .line 2029877
    iget-object v0, p0, LX/Dgo;->b:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->f:LX/Dgr;

    invoke-interface {v0}, LX/Dgr;->a()V

    .line 2029878
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2029879
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2029880
    iget-object v0, p0, LX/Dgo;->b:Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;

    iget-object v1, p0, LX/Dgo;->a:Ljava/lang/String;

    .line 2029881
    iget-object v2, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->e:LX/1Ck;

    sget-object v3, LX/Dgs;->GET_PLACES:LX/Dgs;

    iget-object v4, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;->c:Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object p0

    invoke-virtual {v4, p0, v1}, Lcom/facebook/messaging/location/sending/NearbyPlaceGraphQLFetcher;->a(Landroid/location/Location;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p0, LX/Dgp;

    invoke-direct {p0, v0}, LX/Dgp;-><init>(Lcom/facebook/messaging/location/sending/NearbyPlacesLoader;)V

    invoke-virtual {v2, v3, v4, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2029882
    return-void
.end method
