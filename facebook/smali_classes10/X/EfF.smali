.class public final LX/EfF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AKw;


# instance fields
.field public final synthetic a:LX/EfL;


# direct methods
.method public constructor <init>(LX/EfL;)V
    .locals 0

    .prologue
    .line 2154072
    iput-object p1, p0, LX/EfF;->a:LX/EfL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2154073
    iget-object v0, p0, LX/EfF;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    if-eqz v0, :cond_0

    .line 2154074
    iget-object v0, p0, LX/EfF;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    .line 2154075
    iget-object v1, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v1, v1, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->f:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    iget-object p0, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object p0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result p0

    invoke-virtual {v1, p0}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->setIndicatorSegmentProgressToMax(I)V

    .line 2154076
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2154077
    iget-object v0, p0, LX/EfF;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    if-eqz v0, :cond_0

    .line 2154078
    iget-object v0, p0, LX/EfF;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    .line 2154079
    iget-object v1, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v1, v1, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->f:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    iget-object p0, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object p0, p0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result p0

    invoke-virtual {v1, p0, p1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->a(II)V

    .line 2154080
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2154081
    iget-object v0, p0, LX/EfF;->a:LX/EfL;

    iget-boolean v0, v0, LX/EfL;->C:Z

    if-eqz v0, :cond_0

    .line 2154082
    iget-object v0, p0, LX/EfF;->a:LX/EfL;

    iget-object v1, p0, LX/EfF;->a:LX/EfL;

    iget-object v1, v1, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    invoke-static {v0, v1}, LX/EfL;->a$redex0(LX/EfL;Lcom/facebook/audience/model/ReplyThread;)V

    .line 2154083
    :goto_0
    return-void

    .line 2154084
    :cond_0
    iget-object v0, p0, LX/EfF;->a:LX/EfL;

    const/4 v1, 0x1

    .line 2154085
    iput-boolean v1, v0, LX/EfL;->E:Z

    .line 2154086
    goto :goto_0
.end method
