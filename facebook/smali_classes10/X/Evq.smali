.class public LX/Evq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/195;

.field private b:LX/Eun;

.field private c:LX/Eun;

.field private d:Z

.field public final e:LX/Eum;

.field public final f:LX/Eun;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/Euo;LX/193;LX/Eum;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 2182499
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2182500
    iput-object v0, p0, LX/Evq;->b:LX/Eun;

    .line 2182501
    iput-object v0, p0, LX/Evq;->c:LX/Eun;

    .line 2182502
    iput-boolean v2, p0, LX/Evq;->d:Z

    .line 2182503
    iput-object p4, p0, LX/Evq;->e:LX/Eum;

    .line 2182504
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "fc_friends_scroll_perf"

    invoke-virtual {p3, v0, v1}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, LX/Evq;->a:LX/195;

    .line 2182505
    const v0, 0x2f0008

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "FriendCenterFriendsTabTTI"

    invoke-virtual {p2, p1, v0, v1}, LX/Euo;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)LX/Eun;

    move-result-object v0

    iput-object v0, p0, LX/Evq;->f:LX/Eun;

    .line 2182506
    const v0, 0x2f000c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "FriendCenterFriendsTabDiskLOAD"

    invoke-virtual {p2, p1, v0, v1}, LX/Euo;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)LX/Eun;

    move-result-object v0

    iput-object v0, p0, LX/Evq;->b:LX/Eun;

    .line 2182507
    const v0, 0x2f000d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "FriendCenterFriendsTabNetworkLOAD"

    invoke-virtual {p2, p1, v0, v1}, LX/Euo;->a(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)LX/Eun;

    move-result-object v0

    iput-object v0, p0, LX/Evq;->c:LX/Eun;

    .line 2182508
    iget-object v0, p0, LX/Evq;->f:LX/Eun;

    invoke-virtual {v0, v2}, LX/Eun;->a(Z)V

    .line 2182509
    return-void
.end method
