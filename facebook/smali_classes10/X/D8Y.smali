.class public LX/D8Y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1969013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1969014
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/D8Y;->a:Landroid/os/Handler;

    .line 1969015
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/D8Y;->b:Landroid/view/Window;

    .line 1969016
    return-void

    .line 1969017
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1969008
    iget-object v0, p0, LX/D8Y;->b:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1969009
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1969010
    iget-object v0, p0, LX/D8Y;->b:Landroid/view/Window;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1969011
    :cond_0
    :goto_0
    return-void

    .line 1969012
    :cond_1
    iget-object v0, p0, LX/D8Y;->b:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x504

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 1968999
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1969000
    iget-object v0, p0, LX/D8Y;->b:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1969001
    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_0

    .line 1969002
    invoke-virtual {p0}, LX/D8Y;->a()V

    .line 1969003
    :cond_0
    :goto_0
    iget-object v0, p0, LX/D8Y;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/watchandmore/WatchAndMoreWindowController$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/watchandmore/WatchAndMoreWindowController$1;-><init>(LX/D8Y;)V

    const-wide/16 v2, 0x7d0

    const v4, 0x5a8be018

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1969004
    return-void

    .line 1969005
    :cond_1
    iget-object v0, p0, LX/D8Y;->b:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    .line 1969006
    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_0

    .line 1969007
    invoke-virtual {p0}, LX/D8Y;->a()V

    goto :goto_0
.end method
