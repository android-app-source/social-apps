.class public final LX/EjG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/EjI;

.field public final synthetic c:LX/30Z;


# direct methods
.method public constructor <init>(LX/30Z;ZLX/EjI;)V
    .locals 0

    .prologue
    .line 2161050
    iput-object p1, p0, LX/EjG;->c:LX/30Z;

    iput-boolean p2, p0, LX/EjG;->a:Z

    iput-object p3, p0, LX/EjG;->b:LX/EjI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 10

    .prologue
    .line 2161051
    iget-object v0, p0, LX/EjG;->c:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    const-string v1, "upload_batch_fail"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, LX/EjG;->a:Z

    iget-object v4, p0, LX/EjG;->c:LX/30Z;

    iget-wide v4, v4, LX/30Z;->M:J

    iget-object v6, p0, LX/EjG;->c:LX/30Z;

    iget-object v6, v6, LX/30Z;->h:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    iget-object v8, p0, LX/EjG;->c:LX/30Z;

    iget-wide v8, v8, LX/30Z;->O:J

    sub-long/2addr v6, v8

    iget-object v8, p0, LX/EjG;->c:LX/30Z;

    iget v8, v8, LX/30Z;->N:I

    iget-object v9, p0, LX/EjG;->c:LX/30Z;

    iget-object v9, v9, LX/30Z;->u:Ljava/lang/String;

    invoke-virtual/range {v0 .. v9}, LX/30d;->a(Ljava/lang/String;Ljava/lang/String;ZJJILjava/lang/String;)V

    .line 2161052
    iget-object v0, p0, LX/EjG;->b:LX/EjI;

    iget-boolean v0, v0, LX/EjI;->d:Z

    if-nez v0, :cond_0

    .line 2161053
    iget-object v0, p0, LX/EjG;->c:LX/30Z;

    iget-object v1, p0, LX/EjG;->b:LX/EjI;

    .line 2161054
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/EjI;->d:Z

    .line 2161055
    move-object v1, v1

    .line 2161056
    iget-boolean v2, p0, LX/EjG;->a:Z

    invoke-static {v0, v1, v2}, LX/30Z;->a$redex0(LX/30Z;LX/EjI;Z)V

    .line 2161057
    :goto_0
    return-void

    .line 2161058
    :cond_0
    iget-object v0, p0, LX/EjG;->c:LX/30Z;

    iget-object v1, p0, LX/EjG;->b:LX/EjI;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p0, LX/EjG;->a:Z

    invoke-static {v0, v1, v2, v3}, LX/30Z;->a$redex0(LX/30Z;LX/EjI;Ljava/lang/Boolean;Z)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2161059
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2161060
    iget-boolean v0, p0, LX/EjG;->a:Z

    if-eqz v0, :cond_0

    .line 2161061
    iget-object v0, p0, LX/EjG;->c:LX/30Z;

    iget-object v0, v0, LX/30Z;->f:LX/30e;

    iget-object v1, p0, LX/EjG;->b:LX/EjI;

    iget-object v1, v1, LX/EjI;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, LX/30e;->b(I)V

    .line 2161062
    iget-object v0, p0, LX/EjG;->c:LX/30Z;

    iget-object v1, v0, LX/30Z;->f:LX/30e;

    .line 2161063
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161064
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/30e;->b(Ljava/util/List;)V

    .line 2161065
    iget-object v0, p0, LX/EjG;->c:LX/30Z;

    iget-object v1, v0, LX/30Z;->f:LX/30e;

    .line 2161066
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2161067
    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->j()LX/2uF;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/30e;->a(LX/3Sb;)V

    .line 2161068
    :cond_0
    iget-object v0, p0, LX/EjG;->c:LX/30Z;

    iget-object v0, v0, LX/30Z;->j:LX/2J1;

    iget-object v1, p0, LX/EjG;->b:LX/EjI;

    iget-object v1, v1, LX/EjI;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/2J1;->a(Ljava/util/List;)V

    .line 2161069
    iget-object v0, p0, LX/EjG;->c:LX/30Z;

    iget-object v1, p0, LX/EjG;->b:LX/EjI;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p0, LX/EjG;->a:Z

    invoke-static {v0, v1, v2, v3}, LX/30Z;->a$redex0(LX/30Z;LX/EjI;Ljava/lang/Boolean;Z)V

    .line 2161070
    return-void
.end method
