.class public final enum LX/DSd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DSd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DSd;

.field public static final enum EMAIL_INVITE:LX/DSd;

.field public static final enum NONE:LX/DSd;

.field public static final enum ONSITE_INVITE:LX/DSd;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1999749
    new-instance v0, LX/DSd;

    const-string v1, "ONSITE_INVITE"

    invoke-direct {v0, v1, v2}, LX/DSd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSd;->ONSITE_INVITE:LX/DSd;

    .line 1999750
    new-instance v0, LX/DSd;

    const-string v1, "EMAIL_INVITE"

    invoke-direct {v0, v1, v3}, LX/DSd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSd;->EMAIL_INVITE:LX/DSd;

    .line 1999751
    new-instance v0, LX/DSd;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/DSd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSd;->NONE:LX/DSd;

    .line 1999752
    const/4 v0, 0x3

    new-array v0, v0, [LX/DSd;

    sget-object v1, LX/DSd;->ONSITE_INVITE:LX/DSd;

    aput-object v1, v0, v2

    sget-object v1, LX/DSd;->EMAIL_INVITE:LX/DSd;

    aput-object v1, v0, v3

    sget-object v1, LX/DSd;->NONE:LX/DSd;

    aput-object v1, v0, v4

    sput-object v0, LX/DSd;->$VALUES:[LX/DSd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1999753
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DSd;
    .locals 1

    .prologue
    .line 1999754
    const-class v0, LX/DSd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DSd;

    return-object v0
.end method

.method public static values()[LX/DSd;
    .locals 1

    .prologue
    .line 1999755
    sget-object v0, LX/DSd;->$VALUES:[LX/DSd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DSd;

    return-object v0
.end method
