.class public final LX/Cut;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Cus;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2oa",
            "<+",
            "LX/2ol;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1947200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1947201
    new-instance v0, LX/Cur;

    invoke-direct {v0, p0}, LX/Cur;-><init>(LX/Cut;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1947202
    new-instance v0, LX/Cuq;

    invoke-direct {v0, p0}, LX/Cuq;-><init>(LX/Cut;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1947203
    return-void
.end method

.method public static a$redex0(LX/Cut;LX/2ol;)V
    .locals 2

    .prologue
    .line 1947204
    invoke-static {p1}, LX/Cut;->b(LX/2ol;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1947205
    instance-of v0, p1, LX/2qc;

    if-eqz v0, :cond_2

    check-cast p1, LX/2qc;

    iget-object v0, p1, LX/2qc;->a:LX/04g;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1947206
    if-eqz v0, :cond_1

    .line 1947207
    :cond_0
    sget-object v0, LX/Cus;->CONSUMING_PAUSE:LX/Cus;

    iput-object v0, p0, LX/Cut;->a:LX/Cus;

    .line 1947208
    :goto_1
    return-void

    .line 1947209
    :cond_1
    sget-object v0, LX/Cus;->IDLE:LX/Cus;

    iput-object v0, p0, LX/Cut;->a:LX/Cus;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/2ol;)Z
    .locals 2

    .prologue
    .line 1947210
    instance-of v0, p0, LX/2qb;

    if-eqz v0, :cond_0

    check-cast p0, LX/2qb;

    iget-object v0, p0, LX/2qb;->a:LX/04g;

    sget-object v1, LX/04g;->BY_SEEKBAR_CONTROLLER:LX/04g;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
