.class public final LX/Ebo;
.super LX/EWj;
.source ""

# interfaces
.implements LX/Ebn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/Ebo;",
        ">;",
        "LX/Ebn;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:LX/EWc;

.field private c:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2144562
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2144563
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebo;->b:LX/EWc;

    .line 2144564
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebo;->c:LX/EWc;

    .line 2144565
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2144566
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2144567
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebo;->b:LX/EWc;

    .line 2144568
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebo;->c:LX/EWc;

    .line 2144569
    return-void
.end method

.method private d(LX/EWY;)LX/Ebo;
    .locals 1

    .prologue
    .line 2144570
    instance-of v0, p1, LX/Ebp;

    if-eqz v0, :cond_0

    .line 2144571
    check-cast p1, LX/Ebp;

    invoke-virtual {p0, p1}, LX/Ebo;->a(LX/Ebp;)LX/Ebo;

    move-result-object p0

    .line 2144572
    :goto_0
    return-object p0

    .line 2144573
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/Ebo;
    .locals 4

    .prologue
    .line 2144574
    const/4 v2, 0x0

    .line 2144575
    :try_start_0
    sget-object v0, LX/Ebp;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebp;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2144576
    if-eqz v0, :cond_0

    .line 2144577
    invoke-virtual {p0, v0}, LX/Ebo;->a(LX/Ebp;)LX/Ebo;

    .line 2144578
    :cond_0
    return-object p0

    .line 2144579
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2144580
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2144581
    check-cast v0, LX/Ebp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2144582
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2144583
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2144584
    invoke-virtual {p0, v1}, LX/Ebo;->a(LX/Ebp;)LX/Ebo;

    :cond_1
    throw v0

    .line 2144585
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static u()LX/Ebo;
    .locals 1

    .prologue
    .line 2144586
    new-instance v0, LX/Ebo;

    invoke-direct {v0}, LX/Ebo;-><init>()V

    return-object v0
.end method

.method private w()LX/Ebo;
    .locals 2

    .prologue
    .line 2144587
    invoke-static {}, LX/Ebo;->u()LX/Ebo;

    move-result-object v0

    invoke-direct {p0}, LX/Ebo;->y()LX/Ebp;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ebo;->a(LX/Ebp;)LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/Ebp;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2144588
    new-instance v2, LX/Ebp;

    invoke-direct {v2, p0}, LX/Ebp;-><init>(LX/EWj;)V

    .line 2144589
    iget v3, p0, LX/Ebo;->a:I

    .line 2144590
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2144591
    :goto_0
    iget-object v1, p0, LX/Ebo;->b:LX/EWc;

    .line 2144592
    iput-object v1, v2, LX/Ebp;->publicKey_:LX/EWc;

    .line 2144593
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2144594
    or-int/lit8 v0, v0, 0x2

    .line 2144595
    :cond_0
    iget-object v1, p0, LX/Ebo;->c:LX/EWc;

    .line 2144596
    iput-object v1, v2, LX/Ebp;->privateKey_:LX/EWc;

    .line 2144597
    iput v0, v2, LX/Ebp;->bitField0_:I

    .line 2144598
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2144599
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2144600
    invoke-direct {p0, p1}, LX/Ebo;->d(LX/EWY;)LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2144601
    invoke-direct {p0, p1, p2}, LX/Ebo;->d(LX/EWd;LX/EYZ;)LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWc;)LX/Ebo;
    .locals 1

    .prologue
    .line 2144602
    if-nez p1, :cond_0

    .line 2144603
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2144604
    :cond_0
    iget v0, p0, LX/Ebo;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ebo;->a:I

    .line 2144605
    iput-object p1, p0, LX/Ebo;->b:LX/EWc;

    .line 2144606
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2144607
    return-object p0
.end method

.method public final a(LX/Ebp;)LX/Ebo;
    .locals 2

    .prologue
    .line 2144608
    sget-object v0, LX/Ebp;->c:LX/Ebp;

    move-object v0, v0

    .line 2144609
    if-ne p1, v0, :cond_0

    .line 2144610
    :goto_0
    return-object p0

    .line 2144611
    :cond_0
    const/4 v0, 0x1

    .line 2144612
    iget v1, p1, LX/Ebp;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    move v0, v0

    .line 2144613
    if-eqz v0, :cond_1

    .line 2144614
    iget-object v0, p1, LX/Ebp;->publicKey_:LX/EWc;

    move-object v0, v0

    .line 2144615
    invoke-virtual {p0, v0}, LX/Ebo;->a(LX/EWc;)LX/Ebo;

    .line 2144616
    :cond_1
    iget v0, p1, LX/Ebp;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2144617
    if-eqz v0, :cond_2

    .line 2144618
    iget-object v0, p1, LX/Ebp;->privateKey_:LX/EWc;

    move-object v0, v0

    .line 2144619
    invoke-virtual {p0, v0}, LX/Ebo;->b(LX/EWc;)LX/Ebo;

    .line 2144620
    :cond_2
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2144621
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2144560
    invoke-direct {p0, p1, p2}, LX/Ebo;->d(LX/EWd;LX/EYZ;)LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2144561
    invoke-direct {p0}, LX/Ebo;->w()LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/EWc;)LX/Ebo;
    .locals 1

    .prologue
    .line 2144537
    if-nez p1, :cond_0

    .line 2144538
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2144539
    :cond_0
    iget v0, p0, LX/Ebo;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/Ebo;->a:I

    .line 2144540
    iput-object p1, p0, LX/Ebo;->c:LX/EWc;

    .line 2144541
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2144542
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2144543
    invoke-direct {p0, p1, p2}, LX/Ebo;->d(LX/EWd;LX/EYZ;)LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2144544
    invoke-direct {p0}, LX/Ebo;->w()LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2144545
    invoke-direct {p0, p1}, LX/Ebo;->d(LX/EWY;)LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2144546
    invoke-direct {p0}, LX/Ebo;->w()LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2144547
    sget-object v0, LX/Eck;->t:LX/EYn;

    const-class v1, LX/Ebp;

    const-class v2, LX/Ebo;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2144548
    sget-object v0, LX/Eck;->s:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2144549
    invoke-direct {p0}, LX/Ebo;->w()LX/Ebo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2144550
    invoke-direct {p0}, LX/Ebo;->y()LX/Ebp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2144551
    invoke-virtual {p0}, LX/Ebo;->l()LX/Ebp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2144552
    invoke-direct {p0}, LX/Ebo;->y()LX/Ebp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2144553
    invoke-virtual {p0}, LX/Ebo;->l()LX/Ebp;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Ebp;
    .locals 2

    .prologue
    .line 2144554
    invoke-direct {p0}, LX/Ebo;->y()LX/Ebp;

    move-result-object v0

    .line 2144555
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2144556
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2144557
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2144558
    sget-object v0, LX/Ebp;->c:LX/Ebp;

    move-object v0, v0

    .line 2144559
    return-object v0
.end method
