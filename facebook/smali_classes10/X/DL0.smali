.class public final LX/DL0;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/DL7;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DL8;


# direct methods
.method public constructor <init>(LX/DL8;)V
    .locals 0

    .prologue
    .line 1987706
    iput-object p1, p0, LX/DL0;->a:LX/DL8;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1987707
    instance-of v0, p1, LX/DL2;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1987708
    check-cast v0, LX/DL2;

    iget-object v0, v0, LX/DL2;->reason:LX/DL5;

    .line 1987709
    sget-object v1, LX/DL5;->SEGMENT_UPLOADING_CANCELLATION:LX/DL5;

    if-ne v0, v1, :cond_1

    .line 1987710
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 1987711
    check-cast v0, LX/DL2;

    iget-object v0, v0, LX/DL2;->file:Ljava/io/File;

    .line 1987712
    invoke-static {v0}, LX/DL8;->d(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 1987713
    iget-object v1, p0, LX/DL0;->a:LX/DL8;

    check-cast p1, LX/DL2;

    iget-object v2, p1, LX/DL2;->reason:LX/DL5;

    .line 1987714
    iget-object p1, v1, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 1987715
    iget-object p1, v1, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DL6;

    .line 1987716
    iput-object v2, p1, LX/DL6;->b:LX/DL5;

    .line 1987717
    :cond_2
    iget-object v1, p0, LX/DL0;->a:LX/DL8;

    iget-object v1, v1, LX/DL8;->k:LX/DLN;

    if-eqz v1, :cond_0

    .line 1987718
    iget-object v1, p0, LX/DL0;->a:LX/DL8;

    iget-object v1, v1, LX/DL8;->k:LX/DLN;

    .line 1987719
    iget-object v2, v1, LX/DLN;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object v2, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    sget-object p0, LX/DLI;->UPLOADING_FAILED_SHOW_RETRY:LX/DLI;

    invoke-virtual {v2, v0, p0}, LX/DKt;->a(Ljava/lang/String;LX/DLI;)V

    .line 1987720
    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1987721
    check-cast p1, LX/DL7;

    .line 1987722
    iget-object v0, p1, LX/DL7;->a:Ljava/lang/String;

    .line 1987723
    iget-object v1, p0, LX/DL0;->a:LX/DL8;

    iget-object v1, v1, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1987724
    iget-object v1, p0, LX/DL0;->a:LX/DL8;

    iget-object v1, v1, LX/DL8;->k:LX/DLN;

    if-eqz v1, :cond_1

    .line 1987725
    iget-object v1, p0, LX/DL0;->a:LX/DL8;

    iget-object v1, v1, LX/DL8;->k:LX/DLN;

    iget-object v2, p1, LX/DL7;->b:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    .line 1987726
    iget-object p0, v1, LX/DLN;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object p0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    const/4 p1, 0x0

    invoke-virtual {p0, v0, p1}, LX/DKt;->a(Ljava/lang/String;Z)V

    .line 1987727
    iget-object p0, v1, LX/DLN;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object p0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->p:LX/DLb;

    .line 1987728
    new-instance p1, LX/0Pz;

    invoke-direct {p1}, LX/0Pz;-><init>()V

    .line 1987729
    invoke-virtual {p1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1987730
    iget-object v1, p0, LX/DLb;->e:LX/0Px;

    if-eqz v1, :cond_0

    .line 1987731
    iget-object v1, p0, LX/DLb;->e:LX/0Px;

    invoke-virtual {p1, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1987732
    :cond_0
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    iput-object p1, p0, LX/DLb;->e:LX/0Px;

    .line 1987733
    iget-object p1, p0, LX/DLb;->f:LX/DLL;

    iget-object v1, p0, LX/DLb;->e:LX/0Px;

    invoke-virtual {p1, v1}, LX/DLL;->a(LX/0Px;)V

    .line 1987734
    iget-object p1, p0, LX/DLb;->f:LX/DLL;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, LX/DLL;->b(Z)V

    .line 1987735
    :cond_1
    return-void
.end method
