.class public final LX/DKv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;)V
    .locals 0

    .prologue
    .line 1987619
    iput-object p1, p0, LX/DKv;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1987620
    const/4 v1, 0x0

    .line 1987621
    new-instance v0, Ljava/io/File;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, LX/DKv;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;

    iget-object v3, v3, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1987622
    const/4 v2, 0x0

    :try_start_0
    new-array v2, v2, [LX/3AS;

    invoke-static {v0, v2}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v2

    invoke-virtual {v2}, LX/3AU;->a()Ljava/io/OutputStream;

    move-result-object v1

    .line 1987623
    invoke-static {p1, v1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1987624
    if-eqz v1, :cond_0

    .line 1987625
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 1987626
    :cond_0
    return-object v0

    .line 1987627
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 1987628
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_1
    throw v0
.end method
