.class public final LX/EAy;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UpdatedPageReviewModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EAf;

.field public final synthetic b:LX/EB0;


# direct methods
.method public constructor <init>(LX/EB0;LX/EAf;)V
    .locals 0

    .prologue
    .line 2086040
    iput-object p1, p0, LX/EAy;->b:LX/EB0;

    iput-object p2, p0, LX/EAy;->a:LX/EAf;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2086018
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2086019
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2086020
    iget-object v1, p0, LX/EAy;->a:LX/EAf;

    if-eqz p1, :cond_0

    .line 2086021
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2086022
    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UpdatedPageReviewModel;

    .line 2086023
    :goto_0
    iget-object v2, v1, LX/EAf;->c:LX/EAh;

    iget-object v3, v1, LX/EAf;->a:Ljava/lang/String;

    iget-object v4, v1, LX/EAf;->b:LX/5tj;

    .line 2086024
    new-instance p0, LX/5uK;

    invoke-direct {p0}, LX/5uK;-><init>()V

    invoke-static {v4}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->a(LX/5tj;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    move-result-object p1

    .line 2086025
    iput-object p1, p0, LX/5uK;->b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    .line 2086026
    move-object p1, p0

    .line 2086027
    new-instance v1, LX/5uL;

    invoke-direct {v1}, LX/5uL;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UpdatedPageReviewModel;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UpdatedPageReviewModel$ReviewStoryModel;

    move-result-object p0

    if-nez p0, :cond_1

    const/4 p0, 0x0

    .line 2086028
    :goto_1
    iput-object p0, v1, LX/5uL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2086029
    move-object p0, v1

    .line 2086030
    invoke-virtual {p0}, LX/5uL;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    move-result-object p0

    .line 2086031
    iput-object p0, p1, LX/5uK;->a:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    .line 2086032
    move-object p0, p1

    .line 2086033
    invoke-virtual {p0}, LX/5uK;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    move-result-object p0

    .line 2086034
    iget-object p1, v2, LX/EAh;->c:LX/Ch5;

    const/4 v2, 0x0

    .line 2086035
    new-instance v1, LX/ChD;

    invoke-direct {v1, v2, v3, p0}, LX/ChD;-><init>(ILjava/lang/String;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)V

    move-object p0, v1

    .line 2086036
    invoke-virtual {p1, p0}, LX/0b4;->a(LX/0b7;)V

    .line 2086037
    return-void

    .line 2086038
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2086039
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UpdatedPageReviewModel;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UpdatedPageReviewModel$ReviewStoryModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UpdatedPageReviewModel$ReviewStoryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    goto :goto_1
.end method
