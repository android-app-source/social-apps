.class public final LX/ECm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/3LH;

.field public final synthetic b:Lcom/facebook/messaging/model/threads/ThreadSummary;

.field public final synthetic c:Z

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Landroid/content/Context;

.field public final synthetic f:LX/ECp;


# direct methods
.method public constructor <init>(LX/ECp;LX/3LH;Lcom/facebook/messaging/model/threads/ThreadSummary;ZLjava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2090348
    iput-object p1, p0, LX/ECm;->f:LX/ECp;

    iput-object p2, p0, LX/ECm;->a:LX/3LH;

    iput-object p3, p0, LX/ECm;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iput-boolean p4, p0, LX/ECm;->c:Z

    iput-object p5, p0, LX/ECm;->d:Ljava/lang/String;

    iput-object p6, p0, LX/ECm;->e:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 2090349
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2090350
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/ECm;->a:LX/3LH;

    invoke-virtual {v0}, LX/3LH;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2090351
    iget-object v0, p0, LX/ECm;->a:LX/3LH;

    invoke-virtual {v0, v1}, LX/3LH;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3OO;

    .line 2090352
    invoke-virtual {v0}, LX/3OP;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2090353
    iget-object v3, v0, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v0, v3

    .line 2090354
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2090355
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2090356
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2090357
    :cond_1
    iget-object v0, p0, LX/ECm;->f:LX/ECp;

    iget-object v0, v0, LX/ECp;->a:LX/3A0;

    iget-object v1, p0, LX/ECm;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iget-boolean v3, p0, LX/ECm;->c:Z

    iget-object v4, p0, LX/ECm;->d:Ljava/lang/String;

    iget-object v5, p0, LX/ECm;->e:Landroid/content/Context;

    invoke-virtual/range {v0 .. v5}, LX/3A0;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;[Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V

    .line 2090358
    return-void
.end method
