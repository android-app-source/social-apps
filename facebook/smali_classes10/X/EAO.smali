.class public LX/EAO;
.super LX/1SX;
.source ""


# static fields
.field public static final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/EA5;

.field public final c:LX/79D;

.field public final d:LX/0bH;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Sh;

.field private final h:LX/1Sp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2085529
    new-instance v0, LX/EAI;

    invoke-direct {v0}, LX/EAI;-><init>()V

    sput-object v0, LX/EAO;->a:LX/0Or;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/79D;LX/EA5;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0wL;)V
    .locals 42
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsfeedSpamReportingEnabled;
        .end annotation
    .end param
    .param p17    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p21    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .param p27    # LX/EA5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/79D;",
            "LX/EA5;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085517
    sget-object v17, LX/EAO;->a:LX/0Or;

    sget-object v18, LX/EAO;->a:LX/0Or;

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move-object/from16 v19, p15

    move-object/from16 v20, p16

    move-object/from16 v21, p17

    move-object/from16 v22, p18

    move-object/from16 v23, p19

    move-object/from16 v24, p20

    move-object/from16 v25, p21

    move-object/from16 v26, p22

    move-object/from16 v27, p23

    move-object/from16 v28, p24

    move-object/from16 v29, p25

    move-object/from16 v30, p27

    move-object/from16 v31, p28

    move-object/from16 v32, p29

    move-object/from16 v33, p30

    move-object/from16 v34, p31

    move-object/from16 v35, p32

    move-object/from16 v36, p33

    move-object/from16 v37, p34

    move-object/from16 v41, p35

    invoke-direct/range {v2 .. v41}, LX/1SX;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V

    .line 2085518
    new-instance v2, LX/EAJ;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/EAJ;-><init>(LX/EAO;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/EAO;->h:LX/1Sp;

    .line 2085519
    move-object/from16 v0, p13

    move-object/from16 v1, p0

    iput-object v0, v1, LX/EAO;->d:LX/0bH;

    .line 2085520
    move-object/from16 v0, p11

    move-object/from16 v1, p0

    iput-object v0, v1, LX/EAO;->e:LX/0Or;

    .line 2085521
    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, LX/EAO;->f:LX/0Or;

    .line 2085522
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, LX/EAO;->g:LX/0Sh;

    .line 2085523
    move-object/from16 v0, p26

    move-object/from16 v1, p0

    iput-object v0, v1, LX/EAO;->c:LX/79D;

    .line 2085524
    move-object/from16 v0, p27

    move-object/from16 v1, p0

    iput-object v0, v1, LX/EAO;->b:LX/EA5;

    .line 2085525
    const-string v2, "reviews_feed"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->b(Ljava/lang/String;)V

    .line 2085526
    const-string v2, "page_see_all_reviews"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(Ljava/lang/String;)V

    .line 2085527
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EAO;->h:LX/1Sp;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(LX/1Sp;)V

    .line 2085528
    return-void
.end method


# virtual methods
.method public final synthetic b()LX/1Pf;
    .locals 1

    .prologue
    .line 2085515
    iget-object v0, p0, LX/EAO;->b:LX/EA5;

    move-object v0, v0

    .line 2085516
    return-object v0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2085509
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2085510
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2085511
    iget-object v1, p0, LX/EAO;->d:LX/0bH;

    new-instance v2, LX/1ZX;

    invoke-direct {v2, p1}, LX/1ZX;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2085512
    iget-object v1, p0, LX/EAO;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1dv;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/1dv;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2085513
    iget-object v2, p0, LX/EAO;->g:LX/0Sh;

    new-instance v3, LX/EAM;

    invoke-direct {v3, p0, v0}, LX/EAM;-><init>(LX/EAO;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v2, v1, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2085514
    return-void
.end method

.method public final d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 2

    .prologue
    .line 2085506
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 2085507
    new-instance v0, LX/EAN;

    invoke-direct {v0, p0}, LX/EAN;-><init>(LX/EAO;)V

    .line 2085508
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, LX/1SX;->d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2085505
    const/4 v0, 0x1

    return v0
.end method
