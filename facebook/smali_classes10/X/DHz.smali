.class public final LX/DHz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveCreateModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DI1;

.field public final synthetic b:LX/3LX;


# direct methods
.method public constructor <init>(LX/3LX;LX/DI1;)V
    .locals 0

    .prologue
    .line 1982750
    iput-object p1, p0, LX/DHz;->b:LX/3LX;

    iput-object p2, p0, LX/DHz;->a:LX/DI1;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1982747
    iget-object v0, p0, LX/DHz;->b:LX/3LX;

    iget-object v0, v0, LX/3LX;->c:LX/03V;

    const-string v1, "friends_nearby_send_wave_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1982748
    iget-object v0, p0, LX/DHz;->a:LX/DI1;

    invoke-interface {v0}, LX/DI1;->a()V

    .line 1982749
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1982739
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1982740
    if-eqz p1, :cond_0

    .line 1982741
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1982742
    if-eqz v0, :cond_0

    .line 1982743
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1982744
    check-cast v0, Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveCreateModel;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveCreateModel;->a()Lcom/facebook/friendsnearby/waves/graphql/FriendsNearbyWavesMutationsModels$LocationWaveCreateModel$UserModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1982745
    :cond_0
    iget-object v0, p0, LX/DHz;->a:LX/DI1;

    invoke-interface {v0}, LX/DI1;->a()V

    .line 1982746
    :cond_1
    return-void
.end method
