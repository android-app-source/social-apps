.class public final LX/EcG;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EcE;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcG;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EcG;


# instance fields
.field public bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public private_:LX/EWc;

.field public public_:LX/EWc;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2145926
    new-instance v0, LX/EcD;

    invoke-direct {v0}, LX/EcD;-><init>()V

    sput-object v0, LX/EcG;->a:LX/EWZ;

    .line 2145927
    new-instance v0, LX/EcG;

    invoke-direct {v0}, LX/EcG;-><init>()V

    .line 2145928
    sput-object v0, LX/EcG;->c:LX/EcG;

    invoke-direct {v0}, LX/EcG;->r()V

    .line 2145929
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2145930
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145931
    iput-byte v0, p0, LX/EcG;->memoizedIsInitialized:B

    .line 2145932
    iput v0, p0, LX/EcG;->memoizedSerializedSize:I

    .line 2145933
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2145934
    iput-object v0, p0, LX/EcG;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2145935
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145936
    iput-byte v0, p0, LX/EcG;->memoizedIsInitialized:B

    .line 2145937
    iput v0, p0, LX/EcG;->memoizedSerializedSize:I

    .line 2145938
    invoke-direct {p0}, LX/EcG;->r()V

    .line 2145939
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2145940
    const/4 v0, 0x0

    .line 2145941
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2145942
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2145943
    sparse-switch v3, :sswitch_data_0

    .line 2145944
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2145945
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2145946
    goto :goto_0

    .line 2145947
    :sswitch_1
    iget v3, p0, LX/EcG;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EcG;->bitField0_:I

    .line 2145948
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EcG;->public_:LX/EWc;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2145949
    :catch_0
    move-exception v0

    .line 2145950
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145951
    move-object v0, v0

    .line 2145952
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2145953
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EcG;->unknownFields:LX/EZQ;

    .line 2145954
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2145955
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EcG;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EcG;->bitField0_:I

    .line 2145956
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EcG;->private_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2145957
    :catch_1
    move-exception v0

    .line 2145958
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2145959
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145960
    move-object v0, v1

    .line 2145961
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2145962
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcG;->unknownFields:LX/EZQ;

    .line 2145963
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2145964
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2145965
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2145966
    iput-byte v1, p0, LX/EcG;->memoizedIsInitialized:B

    .line 2145967
    iput v1, p0, LX/EcG;->memoizedSerializedSize:I

    .line 2145968
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcG;->unknownFields:LX/EZQ;

    .line 2145969
    return-void
.end method

.method public static a(LX/EcG;)LX/EcF;
    .locals 1

    .prologue
    .line 2145970
    invoke-static {}, LX/EcF;->w()LX/EcF;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EcF;->a(LX/EcG;)LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 2145971
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcG;->public_:LX/EWc;

    .line 2145972
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcG;->private_:LX/EWc;

    .line 2145973
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2145974
    new-instance v0, LX/EcF;

    invoke-direct {v0, p1}, LX/EcF;-><init>(LX/EYd;)V

    .line 2145975
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2145914
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2145915
    iget v0, p0, LX/EcG;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2145916
    iget-object v0, p0, LX/EcG;->public_:LX/EWc;

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2145917
    :cond_0
    iget v0, p0, LX/EcG;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2145918
    iget-object v0, p0, LX/EcG;->private_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2145919
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2145920
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2145921
    iget-byte v1, p0, LX/EcG;->memoizedIsInitialized:B

    .line 2145922
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2145923
    :goto_0
    return v0

    .line 2145924
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2145925
    :cond_1
    iput-byte v0, p0, LX/EcG;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2145896
    iget v0, p0, LX/EcG;->memoizedSerializedSize:I

    .line 2145897
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2145898
    :goto_0
    return v0

    .line 2145899
    :cond_0
    const/4 v0, 0x0

    .line 2145900
    iget v1, p0, LX/EcG;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2145901
    iget-object v0, p0, LX/EcG;->public_:LX/EWc;

    invoke-static {v2, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2145902
    :cond_1
    iget v1, p0, LX/EcG;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2145903
    iget-object v1, p0, LX/EcG;->private_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2145904
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2145905
    iput v0, p0, LX/EcG;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2145913
    iget-object v0, p0, LX/EcG;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2145906
    sget-object v0, LX/Eck;->B:LX/EYn;

    const-class v1, LX/EcG;

    const-class v2, LX/EcF;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2145907
    sget-object v0, LX/EcG;->a:LX/EWZ;

    return-object v0
.end method

.method public final o()LX/EcF;
    .locals 1

    .prologue
    .line 2145908
    invoke-static {p0}, LX/EcG;->a(LX/EcG;)LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2145909
    invoke-virtual {p0}, LX/EcG;->o()LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2145910
    invoke-static {}, LX/EcF;->w()LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2145911
    invoke-virtual {p0}, LX/EcG;->o()LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145912
    sget-object v0, LX/EcG;->c:LX/EcG;

    return-object v0
.end method
