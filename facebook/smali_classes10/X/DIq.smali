.class public final LX/DIq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:LX/DIv;


# direct methods
.method public constructor <init>(LX/DIv;)V
    .locals 0

    .prologue
    .line 1984436
    iput-object p1, p0, LX/DIq;->a:LX/DIv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1984437
    iget-object v0, p0, LX/DIq;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJT;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/DJT;->a(Ljava/lang/String;)V

    .line 1984438
    iget-object v0, p0, LX/DIq;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->i:LX/03V;

    const-string v1, "ComposerSellController"

    const-string v2, "Couldn\'t complete ."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1984439
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1984440
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1984441
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1984442
    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;

    .line 1984443
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;

    move-result-object v3

    .line 1984444
    if-eqz v3, :cond_1

    .line 1984445
    invoke-virtual {v3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1984446
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1984447
    invoke-virtual {v3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1984448
    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    .line 1984449
    :goto_1
    if-eqz v1, :cond_4

    .line 1984450
    invoke-virtual {v3}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceZipcodeModels$StructuredLocationZipcodeQueryModel$GroupCommerceSuggestedLocationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1984451
    :goto_2
    move-object v1, v0

    .line 1984452
    iget-object v0, p0, LX/DIq;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJT;

    invoke-virtual {v0, v1}, LX/DJT;->a(Ljava/lang/String;)V

    .line 1984453
    return-void

    :cond_0
    move v0, v2

    .line 1984454
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1

    .line 1984455
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method
