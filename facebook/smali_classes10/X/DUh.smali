.class public final LX/DUh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 2003527
    const/4 v12, 0x0

    .line 2003528
    const/4 v11, 0x0

    .line 2003529
    const/4 v10, 0x0

    .line 2003530
    const/4 v9, 0x0

    .line 2003531
    const/4 v8, 0x0

    .line 2003532
    const/4 v7, 0x0

    .line 2003533
    const/4 v6, 0x0

    .line 2003534
    const/4 v5, 0x0

    .line 2003535
    const/4 v4, 0x0

    .line 2003536
    const/4 v3, 0x0

    .line 2003537
    const/4 v2, 0x0

    .line 2003538
    const/4 v1, 0x0

    .line 2003539
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 2003540
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2003541
    const/4 v1, 0x0

    .line 2003542
    :goto_0
    return v1

    .line 2003543
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2003544
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 2003545
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 2003546
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2003547
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 2003548
    const-string v14, "__type__"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2

    const-string v14, "__typename"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 2003549
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v12

    goto :goto_1

    .line 2003550
    :cond_3
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 2003551
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 2003552
    :cond_4
    const-string v14, "invitee"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 2003553
    invoke-static/range {p0 .. p1}, LX/DUe;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2003554
    :cond_5
    const-string v14, "is_viewer_coworker"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 2003555
    const/4 v3, 0x1

    .line 2003556
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 2003557
    :cond_6
    const-string v14, "is_viewer_friend"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 2003558
    const/4 v2, 0x1

    .line 2003559
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 2003560
    :cond_7
    const-string v14, "is_work_user"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 2003561
    const/4 v1, 0x1

    .line 2003562
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 2003563
    :cond_8
    const-string v14, "last_active_messages_status"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 2003564
    invoke-static/range {p0 .. p1}, LX/Dau;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 2003565
    :cond_9
    const-string v14, "name"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 2003566
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 2003567
    :cond_a
    const-string v14, "profile_picture"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 2003568
    invoke-static/range {p0 .. p1}, LX/Dav;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2003569
    :cond_b
    const/16 v13, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 2003570
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 2003571
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 2003572
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 2003573
    if-eqz v3, :cond_c

    .line 2003574
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 2003575
    :cond_c
    if-eqz v2, :cond_d

    .line 2003576
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 2003577
    :cond_d
    if-eqz v1, :cond_e

    .line 2003578
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 2003579
    :cond_e
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2003580
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2003581
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2003582
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2003583
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2003584
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2003585
    if-eqz v0, :cond_0

    .line 2003586
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003587
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2003588
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2003589
    if-eqz v0, :cond_1

    .line 2003590
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003591
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2003592
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2003593
    if-eqz v0, :cond_2

    .line 2003594
    const-string v1, "invitee"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003595
    invoke-static {p0, v0, p2}, LX/DUe;->a(LX/15i;ILX/0nX;)V

    .line 2003596
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2003597
    if-eqz v0, :cond_3

    .line 2003598
    const-string v1, "is_viewer_coworker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003599
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2003600
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2003601
    if-eqz v0, :cond_4

    .line 2003602
    const-string v1, "is_viewer_friend"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003603
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2003604
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2003605
    if-eqz v0, :cond_5

    .line 2003606
    const-string v1, "is_work_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003607
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2003608
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2003609
    if-eqz v0, :cond_6

    .line 2003610
    const-string v1, "last_active_messages_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003611
    invoke-static {p0, v0, p2}, LX/Dau;->a(LX/15i;ILX/0nX;)V

    .line 2003612
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2003613
    if-eqz v0, :cond_7

    .line 2003614
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003615
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2003616
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2003617
    if-eqz v0, :cond_8

    .line 2003618
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003619
    invoke-static {p0, v0, p2}, LX/Dav;->a(LX/15i;ILX/0nX;)V

    .line 2003620
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2003621
    return-void
.end method
