.class public final LX/EuM;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/EuM;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EuK;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/EuN;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2179493
    const/4 v0, 0x0

    sput-object v0, LX/EuM;->a:LX/EuM;

    .line 2179494
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EuM;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2179514
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2179515
    new-instance v0, LX/EuN;

    invoke-direct {v0}, LX/EuN;-><init>()V

    iput-object v0, p0, LX/EuM;->c:LX/EuN;

    .line 2179516
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179513
    const v0, 0x3ec0609e

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/EuM;
    .locals 2

    .prologue
    .line 2179517
    const-class v1, LX/EuM;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/EuM;->a:LX/EuM;

    if-nez v0, :cond_0

    .line 2179518
    new-instance v0, LX/EuM;

    invoke-direct {v0}, LX/EuM;-><init>()V

    sput-object v0, LX/EuM;->a:LX/EuM;

    .line 2179519
    :cond_0
    sget-object v0, LX/EuM;->a:LX/EuM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2179520
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2179506
    check-cast p2, LX/EuL;

    .line 2179507
    iget-object v0, p2, LX/EuL;->a:Ljava/lang/CharSequence;

    .line 2179508
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const p0, 0x7f0b0054

    invoke-virtual {v2, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const p0, 0x7f0a00ab

    invoke-virtual {v2, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x2

    invoke-interface {v2, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    .line 2179509
    const p0, 0x3ec0609e

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2179510
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const/16 p0, 0x8

    const p2, 0x7f0b007b

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 2179511
    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2179512
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2179495
    invoke-static {}, LX/1dS;->b()V

    .line 2179496
    iget v0, p1, LX/1dQ;->b:I

    .line 2179497
    packed-switch v0, :pswitch_data_0

    .line 2179498
    :goto_0
    return-object v2

    .line 2179499
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2179500
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2179501
    check-cast v1, LX/EuL;

    .line 2179502
    iget-object p0, v1, LX/EuL;->b:Ljava/lang/Runnable;

    .line 2179503
    if-eqz p0, :cond_0

    .line 2179504
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 2179505
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3ec0609e
        :pswitch_0
    .end packed-switch
.end method
