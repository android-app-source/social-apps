.class public LX/DBJ;
.super Landroid/text/style/MetricAffectingSpan;
.source ""


# instance fields
.field private final a:Landroid/graphics/Typeface;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/graphics/Typeface;II)V
    .locals 0

    .prologue
    .line 1972645
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    .line 1972646
    iput-object p1, p0, LX/DBJ;->a:Landroid/graphics/Typeface;

    .line 1972647
    iput p2, p0, LX/DBJ;->b:I

    .line 1972648
    iput p3, p0, LX/DBJ;->c:I

    .line 1972649
    return-void
.end method

.method private static a(Landroid/graphics/Paint;Landroid/graphics/Typeface;II)V
    .locals 2

    .prologue
    .line 1972650
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 1972651
    if-nez v0, :cond_4

    .line 1972652
    const/4 v0, 0x0

    .line 1972653
    :goto_0
    if-nez p1, :cond_0

    .line 1972654
    invoke-static {p1, p3}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    .line 1972655
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    .line 1972656
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_1

    .line 1972657
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 1972658
    :cond_1
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 1972659
    const/high16 v0, -0x41800000    # -0.25f

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 1972660
    :cond_2
    invoke-virtual {p0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1972661
    if-lez p2, :cond_3

    .line 1972662
    int-to-float v0, p2

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1972663
    :cond_3
    return-void

    .line 1972664
    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 3

    .prologue
    .line 1972665
    iget-object v0, p0, LX/DBJ;->a:Landroid/graphics/Typeface;

    iget v1, p0, LX/DBJ;->b:I

    iget v2, p0, LX/DBJ;->c:I

    invoke-static {p1, v0, v1, v2}, LX/DBJ;->a(Landroid/graphics/Paint;Landroid/graphics/Typeface;II)V

    .line 1972666
    return-void
.end method

.method public final updateMeasureState(Landroid/text/TextPaint;)V
    .locals 3

    .prologue
    .line 1972667
    iget-object v0, p0, LX/DBJ;->a:Landroid/graphics/Typeface;

    iget v1, p0, LX/DBJ;->b:I

    iget v2, p0, LX/DBJ;->c:I

    invoke-static {p1, v0, v1, v2}, LX/DBJ;->a(Landroid/graphics/Paint;Landroid/graphics/Typeface;II)V

    .line 1972668
    return-void
.end method
