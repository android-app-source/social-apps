.class public final LX/D9V;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;


# direct methods
.method public constructor <init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V
    .locals 0

    .prologue
    .line 1970150
    iput-object p1, p0, LX/D9V;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;B)V
    .locals 0

    .prologue
    .line 1970151
    invoke-direct {p0, p1}, LX/D9V;-><init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 1

    .prologue
    .line 1970152
    iget-object v0, p0, LX/D9V;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-static {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->h(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V

    .line 1970153
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1970154
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1970155
    iget-object v0, p0, LX/D9V;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setVisibility(I)V

    .line 1970156
    :cond_0
    iget-object v0, p0, LX/D9V;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    iget-object v0, v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->s:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_1

    .line 1970157
    iget-object v0, p0, LX/D9V;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    iget-object v0, v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->s:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x716c36e5

    invoke-static {v0, v4, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1970158
    iget-object v0, p0, LX/D9V;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    .line 1970159
    iput-object v4, v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->s:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1970160
    :cond_1
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 2

    .prologue
    .line 1970161
    iget-object v0, p0, LX/D9V;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setVisibility(I)V

    .line 1970162
    return-void
.end method
