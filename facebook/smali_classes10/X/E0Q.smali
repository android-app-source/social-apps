.class public final enum LX/E0Q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E0Q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E0Q;

.field public static final enum INVALID:LX/E0Q;

.field public static final enum PAGES:LX/E0Q;

.field public static final enum PLACE_CREATION:LX/E0Q;

.field public static final enum PLACE_PICKER:LX/E0Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2068449
    new-instance v0, LX/E0Q;

    const-string v1, "PLACE_PICKER"

    invoke-direct {v0, v1, v2}, LX/E0Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0Q;->PLACE_PICKER:LX/E0Q;

    .line 2068450
    new-instance v0, LX/E0Q;

    const-string v1, "PLACE_CREATION"

    invoke-direct {v0, v1, v3}, LX/E0Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0Q;->PLACE_CREATION:LX/E0Q;

    .line 2068451
    new-instance v0, LX/E0Q;

    const-string v1, "PAGES"

    invoke-direct {v0, v1, v4}, LX/E0Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0Q;->PAGES:LX/E0Q;

    .line 2068452
    new-instance v0, LX/E0Q;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v5}, LX/E0Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0Q;->INVALID:LX/E0Q;

    .line 2068453
    const/4 v0, 0x4

    new-array v0, v0, [LX/E0Q;

    sget-object v1, LX/E0Q;->PLACE_PICKER:LX/E0Q;

    aput-object v1, v0, v2

    sget-object v1, LX/E0Q;->PLACE_CREATION:LX/E0Q;

    aput-object v1, v0, v3

    sget-object v1, LX/E0Q;->PAGES:LX/E0Q;

    aput-object v1, v0, v4

    sget-object v1, LX/E0Q;->INVALID:LX/E0Q;

    aput-object v1, v0, v5

    sput-object v0, LX/E0Q;->$VALUES:[LX/E0Q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2068454
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/E0Q;
    .locals 1

    .prologue
    .line 2068455
    const-class v0, LX/E0Q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E0Q;

    return-object v0
.end method

.method public static values()[LX/E0Q;
    .locals 1

    .prologue
    .line 2068456
    sget-object v0, LX/E0Q;->$VALUES:[LX/E0Q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E0Q;

    return-object v0
.end method
