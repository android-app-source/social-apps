.class public LX/E9N;
.super LX/E9M;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/E9M",
        "<",
        "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsInterfaces$PlaceToReview;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/E9V;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsInterfaces$PlaceToReview;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsInterfaces$PlaceToReview;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/E9V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2084335
    invoke-direct {p0}, LX/E9M;-><init>()V

    .line 2084336
    iput-object p1, p0, LX/E9N;->a:LX/E9V;

    .line 2084337
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E9N;->b:Ljava/util/List;

    .line 2084338
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/E9N;->d:Z

    .line 2084339
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/E9N;->c:Ljava/util/Map;

    .line 2084340
    return-void
.end method


# virtual methods
.method public final a(I)LX/E9W;
    .locals 1

    .prologue
    .line 2084334
    sget-object v0, LX/E9W;->PLACES_TO_REVIEW:LX/E9W;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2084333
    iget-object v0, p0, LX/E9N;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;)V
    .locals 7
    .param p1    # Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2084320
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;->b()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2084321
    :cond_0
    :goto_0
    return-void

    .line 2084322
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;

    .line 2084323
    if-eqz v0, :cond_2

    iget-object v5, p0, LX/E9N;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->c()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2084324
    iget-object v5, p0, LX/E9N;->b:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2084325
    iget-object v5, p0, LX/E9N;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->c()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2084326
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2084327
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2084328
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, LX/15i;->h(II)Z

    move-result v0

    iput-boolean v0, p0, LX/E9N;->d:Z

    .line 2084329
    iget-boolean v0, p0, LX/E9N;->d:Z

    if-nez v0, :cond_4

    .line 2084330
    iget-object v0, p0, LX/E9N;->a:LX/E9V;

    .line 2084331
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/E9V;->a:Z

    .line 2084332
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlacesToReviewModel$PlaceReviewSuggestionsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/E9N;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2084319
    iget-object v0, p0, LX/E9N;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 2084318
    iget-object v0, p0, LX/E9N;->b:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2084308
    iget-object v0, p0, LX/E9N;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2084309
    iget-object v0, p0, LX/E9N;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;

    .line 2084310
    iget-object v1, p0, LX/E9N;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2084311
    const/4 v0, 0x1

    .line 2084312
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/E9W;
    .locals 1

    .prologue
    .line 2084317
    iget-object v0, p0, LX/E9N;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/E9W;->NO_HEADER:LX/E9W;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/E9W;->DEFAULT_HEADER:LX/E9W;

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2084316
    iget-object v0, p0, LX/E9N;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2084315
    iget-object v0, p0, LX/E9N;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic l()LX/E9Q;
    .locals 1

    .prologue
    .line 2084313
    iget-object v0, p0, LX/E9N;->a:LX/E9V;

    move-object v0, v0

    .line 2084314
    return-object v0
.end method
