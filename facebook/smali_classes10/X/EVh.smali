.class public final LX/EVh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ja;


# instance fields
.field public final synthetic a:Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;)V
    .locals 0

    .prologue
    .line 2128714
    iput-object p1, p0, LX/EVh;->a:Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Jb;)V
    .locals 8

    .prologue
    .line 2128715
    iget-object v0, p0, LX/EVh;->a:Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;

    .line 2128716
    iget v4, p1, LX/0Jb;->a:F

    iget v5, p1, LX/0Jb;->e:F

    div-float/2addr v4, v5

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    long-to-float v5, v6

    mul-float/2addr v4, v5

    move v1, v4

    .line 2128717
    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Scroll Perf"

    const-string v3, "1_drop_per_min_last"

    .line 2128718
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128719
    iget-object v0, p0, LX/EVh;->a:Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;

    .line 2128720
    iget v4, p1, LX/0Jb;->c:F

    iget v5, p1, LX/0Jb;->e:F

    div-float/2addr v4, v5

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    long-to-float v5, v6

    mul-float/2addr v4, v5

    move v1, v4

    .line 2128721
    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Scroll Perf"

    const-string v3, "4_drop_per_min_last"

    .line 2128722
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128723
    iget-object v0, p0, LX/EVh;->a:Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;

    .line 2128724
    iget v4, p1, LX/0Jb;->b:F

    iget v5, p1, LX/0Jb;->f:F

    div-float/2addr v4, v5

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    long-to-float v5, v6

    mul-float/2addr v4, v5

    move v1, v4

    .line 2128725
    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Scroll Perf"

    const-string v3, "1_drop_per_min_overall"

    .line 2128726
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128727
    iget-object v0, p0, LX/EVh;->a:Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;

    .line 2128728
    iget v4, p1, LX/0Jb;->d:F

    iget v5, p1, LX/0Jb;->f:F

    div-float/2addr v4, v5

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    long-to-float v5, v6

    mul-float/2addr v4, v5

    move v1, v4

    .line 2128729
    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Scroll Perf"

    const-string v3, "4_drop_per_min_overall"

    .line 2128730
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;->a$redex0(Lcom/facebook/video/videohome/views/VideoHomeDebugOverlayView;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128731
    return-void
.end method
