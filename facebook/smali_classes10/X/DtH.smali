.class public final enum LX/DtH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DtH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DtH;

.field public static final enum CREDIT_CARD:LX/DtH;

.field public static final enum DEBIT_CARD:LX/DtH;

.field public static final enum UNKNOWN:LX/DtH;


# instance fields
.field public final cardType:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2051674
    new-instance v0, LX/DtH;

    const-string v1, "DEBIT_CARD"

    const v2, 0x7f082ed1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, LX/DtH;-><init>(Ljava/lang/String;ILX/0am;)V

    sput-object v0, LX/DtH;->DEBIT_CARD:LX/DtH;

    .line 2051675
    new-instance v0, LX/DtH;

    const-string v1, "CREDIT_CARD"

    const v2, 0x7f082ed2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, LX/DtH;-><init>(Ljava/lang/String;ILX/0am;)V

    sput-object v0, LX/DtH;->CREDIT_CARD:LX/DtH;

    .line 2051676
    new-instance v0, LX/DtH;

    const-string v1, "UNKNOWN"

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, LX/DtH;-><init>(Ljava/lang/String;ILX/0am;)V

    sput-object v0, LX/DtH;->UNKNOWN:LX/DtH;

    .line 2051677
    const/4 v0, 0x3

    new-array v0, v0, [LX/DtH;

    sget-object v1, LX/DtH;->DEBIT_CARD:LX/DtH;

    aput-object v1, v0, v3

    sget-object v1, LX/DtH;->CREDIT_CARD:LX/DtH;

    aput-object v1, v0, v4

    sget-object v1, LX/DtH;->UNKNOWN:LX/DtH;

    aput-object v1, v0, v5

    sput-object v0, LX/DtH;->$VALUES:[LX/DtH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2051671
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2051672
    iput-object p3, p0, LX/DtH;->cardType:LX/0am;

    .line 2051673
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/DtH;
    .locals 5

    .prologue
    .line 2051678
    invoke-static {}, LX/DtH;->values()[LX/DtH;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2051679
    invoke-virtual {v0}, LX/DtH;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2051680
    :goto_1
    return-object v0

    .line 2051681
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2051682
    :cond_1
    sget-object v0, LX/DtH;->UNKNOWN:LX/DtH;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/DtH;
    .locals 1

    .prologue
    .line 2051669
    const-class v0, LX/DtH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DtH;

    return-object v0
.end method

.method public static values()[LX/DtH;
    .locals 1

    .prologue
    .line 2051670
    sget-object v0, LX/DtH;->$VALUES:[LX/DtH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DtH;

    return-object v0
.end method
