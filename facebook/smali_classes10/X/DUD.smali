.class public final LX/DUD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2001867
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2001868
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2001869
    :goto_0
    return v1

    .line 2001870
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2001871
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2001872
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2001873
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2001874
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2001875
    const-string v4, "edges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2001876
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2001877
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 2001878
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 2001879
    invoke-static {p0, p1}, LX/DUC;->b(LX/15w;LX/186;)I

    move-result v3

    .line 2001880
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2001881
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2001882
    goto :goto_1

    .line 2001883
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2001884
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2001885
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_f

    .line 2001886
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2001887
    :goto_3
    move v0, v3

    .line 2001888
    goto :goto_1

    .line 2001889
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2001890
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2001891
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2001892
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2001893
    :cond_6
    const-string v12, "has_next_page"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2001894
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v8, v5

    move v5, v4

    .line 2001895
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 2001896
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2001897
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2001898
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_7

    if-eqz v11, :cond_7

    .line 2001899
    const-string v12, "delta_cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2001900
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_4

    .line 2001901
    :cond_8
    const-string v12, "end_cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2001902
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 2001903
    :cond_9
    const-string v12, "has_previous_page"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 2001904
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v4

    goto :goto_4

    .line 2001905
    :cond_a
    const-string v12, "start_cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 2001906
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 2001907
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2001908
    :cond_c
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2001909
    invoke-virtual {p1, v3, v10}, LX/186;->b(II)V

    .line 2001910
    invoke-virtual {p1, v4, v9}, LX/186;->b(II)V

    .line 2001911
    if-eqz v5, :cond_d

    .line 2001912
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v8}, LX/186;->a(IZ)V

    .line 2001913
    :cond_d
    if-eqz v0, :cond_e

    .line 2001914
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 2001915
    :cond_e
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2001916
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_f
    move v0, v3

    move v5, v3

    move v6, v3

    move v7, v3

    move v8, v3

    move v9, v3

    move v10, v3

    goto/16 :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2001917
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2001918
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2001919
    if-eqz v0, :cond_1

    .line 2001920
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001921
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2001922
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2001923
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/DUC;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2001924
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2001925
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2001926
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2001927
    if-eqz v0, :cond_7

    .line 2001928
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001929
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2001930
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2001931
    if-eqz v1, :cond_2

    .line 2001932
    const-string v2, "delta_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001933
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2001934
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2001935
    if-eqz v1, :cond_3

    .line 2001936
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001937
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2001938
    :cond_3
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2001939
    if-eqz v1, :cond_4

    .line 2001940
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001941
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2001942
    :cond_4
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2001943
    if-eqz v1, :cond_5

    .line 2001944
    const-string v2, "has_previous_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001945
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2001946
    :cond_5
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2001947
    if-eqz v1, :cond_6

    .line 2001948
    const-string v2, "start_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001949
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2001950
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2001951
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2001952
    return-void
.end method
