.class public final LX/Dwy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2062047
    iput-object p1, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iput-object p2, p0, LX/Dwy;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 6

    .prologue
    .line 2062048
    iget-object v0, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    invoke-static {v0, p1}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->a$redex0(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;I)V

    .line 2062049
    new-instance v1, LX/74W;

    iget-object v0, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->u:LX/9lP;

    invoke-virtual {v0}, LX/9lP;->g()LX/9lQ;

    move-result-object v0

    invoke-virtual {v0}, LX/9lQ;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/Dwy;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    invoke-static {v3, p1}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->d(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget-object v5, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget v5, v5, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->A:I

    invoke-static {v4, v5}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->d(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v0, v2, v3, v4}, LX/74W;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2062050
    iget-object v0, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73w;

    iget-object v2, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget-object v2, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/73w;->a(Ljava/lang/String;)V

    .line 2062051
    iget-object v0, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73w;

    invoke-virtual {v0, v1}, LX/73w;->a(LX/74W;)V

    .line 2062052
    iget-object v0, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    .line 2062053
    const/4 v1, 0x2

    .line 2062054
    iget-boolean v2, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->w:Z

    if-eqz v2, :cond_0

    .line 2062055
    const/4 v1, 0x3

    .line 2062056
    :cond_0
    iget-boolean v2, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->x:Z

    if-eqz v2, :cond_1

    .line 2062057
    add-int/lit8 v1, v1, 0x1

    .line 2062058
    :cond_1
    iget-boolean v2, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->y:Z

    if-eqz v2, :cond_2

    .line 2062059
    add-int/lit8 v1, v1, 0x1

    .line 2062060
    :cond_2
    if-ne v1, p1, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2062061
    if-eqz v0, :cond_3

    .line 2062062
    iget-object v0, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->g:LX/EQh;

    iget-object v1, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget-object v1, v1, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->r:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2062063
    if-nez v1, :cond_5

    .line 2062064
    const-string v2, "loading"

    .line 2062065
    :goto_1
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "shoebox_moments_tab_view"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2062066
    const-string v4, "view_state"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2062067
    iget-object v2, v0, LX/EQh;->a:LX/0Zb;

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2062068
    iget-object v0, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->s:LX/ERA;

    if-eqz v0, :cond_3

    .line 2062069
    iget-object v0, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->s:LX/ERA;

    iget-object v1, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    .line 2062070
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v2

    .line 2062071
    iget-object v2, v0, LX/ERA;->b:LX/0ht;

    invoke-virtual {v2, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2062072
    :cond_3
    iget-object v0, p0, LX/Dwy;->b:Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    .line 2062073
    iput p1, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->A:I

    .line 2062074
    return-void

    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 2062075
    :cond_5
    iget-boolean v2, v1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->b:Z

    move v2, v2

    .line 2062076
    if-eqz v2, :cond_6

    .line 2062077
    const-string v2, "interstitial"

    goto :goto_1

    .line 2062078
    :cond_6
    iget-boolean v2, v1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    move v2, v2

    .line 2062079
    if-eqz v2, :cond_7

    .line 2062080
    const-string v2, "moments"

    goto :goto_1

    .line 2062081
    :cond_7
    const-string v2, "shoebox"

    goto :goto_1
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2062082
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2062083
    return-void
.end method
