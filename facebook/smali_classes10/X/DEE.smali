.class public final LX/DEE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DEF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/DEI;

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/DEF;


# direct methods
.method public constructor <init>(LX/DEF;)V
    .locals 1

    .prologue
    .line 1976512
    iput-object p1, p0, LX/DEE;->c:LX/DEF;

    .line 1976513
    move-object v0, p1

    .line 1976514
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1976515
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1976516
    const-string v0, "GroupsYouShouldCreatePageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1976517
    if-ne p0, p1, :cond_1

    .line 1976518
    :cond_0
    :goto_0
    return v0

    .line 1976519
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1976520
    goto :goto_0

    .line 1976521
    :cond_3
    check-cast p1, LX/DEE;

    .line 1976522
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1976523
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1976524
    if-eq v2, v3, :cond_0

    .line 1976525
    iget-object v2, p0, LX/DEE;->a:LX/DEI;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DEE;->a:LX/DEI;

    iget-object v3, p1, LX/DEE;->a:LX/DEI;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1976526
    goto :goto_0

    .line 1976527
    :cond_5
    iget-object v2, p1, LX/DEE;->a:LX/DEI;

    if-nez v2, :cond_4

    .line 1976528
    :cond_6
    iget-object v2, p0, LX/DEE;->b:LX/1Po;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/DEE;->b:LX/1Po;

    iget-object v3, p1, LX/DEE;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1976529
    goto :goto_0

    .line 1976530
    :cond_7
    iget-object v2, p1, LX/DEE;->b:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
