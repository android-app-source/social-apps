.class public final LX/Cgz;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public final b:LX/AR4;

.field public final c:LX/ARB;

.field public final d:LX/AR2;

.field public final e:LX/943;

.field public final f:Landroid/view/inputmethod/InputMethodManager;

.field public final g:LX/Ch2;

.field public final h:LX/79D;

.field public i:Lcom/facebook/reviews/composer/ComposerRatingView;

.field public j:Lcom/facebook/composer/ui/text/ComposerEditText;

.field public k:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1927619
    const-class v0, LX/Cgz;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Cgz;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/B5j;Landroid/content/Context;LX/AR4;LX/ARB;LX/AR2;LX/943;Landroid/view/inputmethod/InputMethodManager;LX/Ch3;LX/79D;)V
    .locals 2
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Landroid/content/Context;",
            "LX/AR4;",
            "LX/ARB;",
            "LX/AR2;",
            "LX/943;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "LX/Ch3;",
            "LX/79D;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1927610
    invoke-direct {p0, p2, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1927611
    iput-object p4, p0, LX/Cgz;->c:LX/ARB;

    .line 1927612
    iput-object p3, p0, LX/Cgz;->b:LX/AR4;

    .line 1927613
    iput-object p5, p0, LX/Cgz;->d:LX/AR2;

    .line 1927614
    iput-object p6, p0, LX/Cgz;->e:LX/943;

    .line 1927615
    iput-object p7, p0, LX/Cgz;->f:Landroid/view/inputmethod/InputMethodManager;

    .line 1927616
    invoke-virtual {p1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p8, p1, v0, v1}, LX/Ch3;->a(LX/0il;Ljava/lang/String;Ljava/lang/String;)LX/Ch2;

    move-result-object v0

    iput-object v0, p0, LX/Cgz;->g:LX/Ch2;

    .line 1927617
    iput-object p9, p0, LX/Cgz;->h:LX/79D;

    .line 1927618
    return-void
.end method

.method public static a$redex0(LX/Cgz;I)V
    .locals 3

    .prologue
    .line 1927664
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/Cgz;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 1927665
    iget-object v1, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1927666
    iget-object v1, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRating()I

    move-result v1

    if-eq v1, p1, :cond_1

    .line 1927667
    iget-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v1, :cond_0

    .line 1927668
    iget-object v1, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v1

    iput-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 1927669
    :cond_0
    iget-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v1, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setRating(I)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 1927670
    iget-object v1, v0, LX/0jL;->a:LX/0cA;

    sget-object v2, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1927671
    :cond_1
    move-object v0, v0

    .line 1927672
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1927673
    invoke-static {p0, p1}, LX/Cgz;->b(LX/Cgz;I)V

    .line 1927674
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1927675
    invoke-static {p0}, LX/Cgz;->aN(LX/Cgz;)V

    .line 1927676
    :goto_0
    return-void

    .line 1927677
    :cond_2
    iget-object v0, p0, LX/Cgz;->i:Lcom/facebook/reviews/composer/ComposerRatingView;

    invoke-virtual {v0}, Lcom/facebook/reviews/composer/ComposerRatingView;->requestFocus()Z

    goto :goto_0
.end method

.method public static aN(LX/Cgz;)V
    .locals 4

    .prologue
    .line 1927678
    iget-object v0, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->requestFocus()Z

    .line 1927679
    new-instance v0, Lcom/facebook/reviews/composer/ReviewComposerPlugin$15;

    invoke-direct {v0, p0}, Lcom/facebook/reviews/composer/ReviewComposerPlugin$15;-><init>(LX/Cgz;)V

    .line 1927680
    iget-object v1, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/composer/ui/text/ComposerEditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1927681
    return-void
.end method

.method public static b(LX/Cgz;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1927682
    iget-object v0, p0, LX/AQ9;->b:Landroid/content/Context;

    move-object v0, v0

    .line 1927683
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1927684
    iget-object v1, p0, LX/AQ9;->b:Landroid/content/Context;

    move-object v1, v1

    .line 1927685
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0ca9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1927686
    iget-object v3, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v3, v0, v1, v0, v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->setPadding(IIII)V

    .line 1927687
    if-nez p1, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 1927688
    :goto_0
    iget-object v0, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v1, :cond_0

    const/16 v2, 0x8

    :cond_0
    invoke-virtual {v0, v2}, Lcom/facebook/composer/ui/text/ComposerEditText;->setVisibility(I)V

    .line 1927689
    iget-object v0, p0, LX/Cgz;->i:Lcom/facebook/reviews/composer/ComposerRatingView;

    invoke-virtual {v0}, Lcom/facebook/reviews/composer/ComposerRatingView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1927690
    if-eqz v1, :cond_2

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_1
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1927691
    iget-object v1, p0, LX/Cgz;->i:Lcom/facebook/reviews/composer/ComposerRatingView;

    invoke-virtual {v1, v0}, Lcom/facebook/reviews/composer/ComposerRatingView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1927692
    return-void

    :cond_1
    move v1, v2

    .line 1927693
    goto :goto_0

    .line 1927694
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final U()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927695
    new-instance v0, LX/Cgp;

    invoke-direct {v0, p0}, LX/Cgp;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final Y()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927696
    new-instance v0, LX/Cgq;

    invoke-direct {v0, p0}, LX/Cgq;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final Z()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927697
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 1927699
    sget-object v0, LX/Cgo;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1927700
    :goto_0
    return-void

    .line 1927701
    :pswitch_0
    iget-object v0, p0, LX/Cgz;->g:LX/Ch2;

    .line 1927702
    invoke-static {v0}, LX/Ch2;->g(LX/Ch2;)I

    move-result v1

    iput v1, v0, LX/Ch2;->h:I

    .line 1927703
    invoke-virtual {v0}, LX/Ch2;->b()V

    .line 1927704
    goto :goto_0

    .line 1927705
    :pswitch_1
    iget-object v0, p0, LX/Cgz;->g:LX/Ch2;

    invoke-virtual {v0}, LX/Ch2;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final aB()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927698
    new-instance v0, LX/Cgk;

    invoke-direct {v0, p0}, LX/Cgk;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final aC()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927708
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aF()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927709
    new-instance v0, LX/Cgn;

    invoke-direct {v0, p0}, LX/Cgn;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final aG()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927707
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927706
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ab()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927662
    new-instance v0, LX/Cgr;

    invoke-direct {v0, p0}, LX/Cgr;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final ac()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927663
    new-instance v0, LX/Cgs;

    invoke-direct {v0, p0}, LX/Cgs;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final ad()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927661
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ae()LX/ARN;
    .locals 1

    .prologue
    .line 1927660
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aj()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927659
    new-instance v0, LX/Cgt;

    invoke-direct {v0, p0}, LX/Cgt;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final ak()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927658
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final an()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927657
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ao()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927656
    new-instance v0, LX/Cgu;

    invoke-direct {v0, p0}, LX/Cgu;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final ap()LX/Aje;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927655
    new-instance v0, LX/Cgv;

    invoke-direct {v0, p0}, LX/Cgv;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final aq()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927654
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aw()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927653
    new-instance v0, LX/Cgw;

    invoke-direct {v0, p0}, LX/Cgw;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final ax()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927652
    new-instance v0, LX/Cgx;

    invoke-direct {v0, p0}, LX/Cgx;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final ay()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927651
    new-instance v0, LX/Cgi;

    invoke-direct {v0, p0}, LX/Cgi;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final az()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1927650
    new-instance v0, LX/Cgj;

    invoke-direct {v0, p0}, LX/Cgj;-><init>(LX/Cgz;)V

    return-object v0
.end method

.method public final b(Landroid/view/ViewStub;)Z
    .locals 2

    .prologue
    .line 1927630
    const v0, 0x7f0311d9

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1927631
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 1927632
    const v0, 0x7f0d29eb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/composer/ComposerRatingView;

    iput-object v0, p0, LX/Cgz;->i:Lcom/facebook/reviews/composer/ComposerRatingView;

    .line 1927633
    const v0, 0x7f0d1913

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/text/ComposerEditText;

    iput-object v0, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    .line 1927634
    iget-object v1, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    .line 1927635
    iget-object v0, p0, LX/AQ9;->K:LX/AQ4;

    move-object v0, v0

    .line 1927636
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1927637
    iget-object v0, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setIncludeFriends(Z)V

    .line 1927638
    iget-object v0, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    new-instance v1, LX/Cgl;

    invoke-direct {v1, p0}, LX/Cgl;-><init>(LX/Cgz;)V

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->a(LX/Be0;)V

    .line 1927639
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1927640
    iget-object v0, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1927641
    iget-object v0, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 1927642
    iget-object v1, p0, LX/Cgz;->j:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 1927643
    :cond_0
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRating()I

    move-result v0

    if-lez v0, :cond_1

    .line 1927644
    invoke-static {p0}, LX/Cgz;->aN(LX/Cgz;)V

    .line 1927645
    :cond_1
    iget-object v0, p0, LX/Cgz;->i:Lcom/facebook/reviews/composer/ComposerRatingView;

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRating()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/composer/ComposerRatingView;->setRating(Ljava/lang/Integer;)V

    .line 1927646
    iget-object v0, p0, LX/Cgz;->i:Lcom/facebook/reviews/composer/ComposerRatingView;

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/composer/ComposerRatingView;->setPageName(Ljava/lang/String;)V

    .line 1927647
    iget-object v0, p0, LX/Cgz;->i:Lcom/facebook/reviews/composer/ComposerRatingView;

    new-instance v1, LX/Cgm;

    invoke-direct {v1, p0}, LX/Cgm;-><init>(LX/Cgz;)V

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/composer/ComposerRatingView;->setOnRatingChangedListener(LX/Cgh;)V

    .line 1927648
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRating()I

    move-result v0

    invoke-static {p0, v0}, LX/Cgz;->b(LX/Cgz;I)V

    .line 1927649
    const/4 v0, 0x1

    return v0
.end method

.method public final c(Landroid/view/ViewStub;)V
    .locals 3

    .prologue
    .line 1927620
    const v0, 0x7f0301cd

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1927621
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Cgz;->k:Landroid/widget/TextView;

    .line 1927622
    iget-object v0, p0, LX/Cgz;->g:LX/Ch2;

    iget-object v1, p0, LX/Cgz;->k:Landroid/widget/TextView;

    .line 1927623
    invoke-static {v0}, LX/Ch2;->f(LX/Ch2;)V

    .line 1927624
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, v0, LX/Ch2;->d:LX/0am;

    .line 1927625
    iget-object v2, v0, LX/Ch2;->d:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1927626
    iget-object v2, v0, LX/Ch2;->d:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1927627
    const p0, 0x7f0b004e

    .line 1927628
    iget-object v2, v0, LX/Ch2;->d:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object p1, v0, LX/Ch2;->b:Landroid/content/res/Resources;

    invoke-static {p1, p0}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result p0

    int-to-float p0, p0

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1927629
    :cond_0
    return-void
.end method
