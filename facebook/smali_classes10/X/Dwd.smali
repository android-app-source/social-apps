.class public final LX/Dwd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 0

    .prologue
    .line 2061186
    iput-object p1, p0, LX/Dwd;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2061175
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2061176
    iget-object v0, p0, LX/Dwd;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v1, p0, LX/Dwd;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v1, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v1}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/4Vp;

    move-result-object v1

    .line 2061177
    iput-object p1, v1, LX/4Vp;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2061178
    move-object v1, v1

    .line 2061179
    invoke-virtual {v1}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 2061180
    iput-object v1, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2061181
    iget-object v0, p0, LX/Dwd;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    .line 2061182
    iget-object v1, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->T:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/21l;

    .line 2061183
    iget-object p1, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    invoke-interface {v1, p1}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 2061184
    :cond_0
    invoke-static {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->q(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    .line 2061185
    const/4 v0, 0x0

    return-object v0
.end method
