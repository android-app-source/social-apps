.class public final LX/DBn;
.super LX/95p;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/95p",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1CU;


# direct methods
.method public constructor <init>(LX/1CU;)V
    .locals 0

    .prologue
    .line 1973469
    iput-object p1, p0, LX/DBn;->a:LX/1CU;

    invoke-direct {p0}, LX/95p;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1973470
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1973471
    iget-object v0, p0, LX/DBn;->a:LX/1CU;

    iget-object v0, v0, LX/1CU;->j:LX/0qq;

    invoke-virtual {v0, p1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1973472
    iget-object v0, p0, LX/DBn;->a:LX/1CU;

    iget-object v0, v0, LX/1CU;->g:LX/0bH;

    new-instance v1, LX/1YL;

    invoke-direct {v1}, LX/1YL;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1973473
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1973474
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    const/4 v2, 0x1

    .line 1973475
    iget-object v0, p0, LX/DBn;->a:LX/1CU;

    iget-object v0, v0, LX/1CU;->b:LX/03V;

    const-string v1, "set_notify_me_fail"

    invoke-virtual {v0, v1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1973476
    iget-object v0, p0, LX/DBn;->a:LX/1CU;

    iget-object v0, v0, LX/1CU;->j:LX/0qq;

    invoke-virtual {v0, p1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1973477
    iget-object v0, p0, LX/DBn;->a:LX/1CU;

    iget-object v0, v0, LX/1CU;->g:LX/0bH;

    new-instance v1, LX/1YL;

    invoke-direct {v1}, LX/1YL;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1973478
    iget-object v0, p0, LX/DBn;->a:LX/1CU;

    iget-boolean v0, v0, LX/1CU;->i:Z

    if-nez v0, :cond_0

    .line 1973479
    :goto_0
    return-void

    .line 1973480
    :cond_0
    iget-object v0, p0, LX/DBn;->a:LX/1CU;

    iget-object v0, v0, LX/1CU;->c:LX/1CW;

    invoke-virtual {v0, p2, v2, v2}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 1973481
    invoke-static {p2}, LX/1CW;->b(Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1973482
    iget-object v1, p0, LX/DBn;->a:LX/1CU;

    iget-object v1, v1, LX/1CU;->f:LX/1CX;

    iget-object v2, p0, LX/DBn;->a:LX/1CU;

    iget-object v2, v2, LX/1CU;->d:Landroid/content/res/Resources;

    invoke-static {v2}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v2

    sget-object v3, LX/8iJ;->SENTRY_BLOCK:LX/8iJ;

    invoke-virtual {v3}, LX/8iJ;->getTitleId()I

    move-result v3

    invoke-virtual {v2, v3}, LX/4mn;->a(I)LX/4mn;

    move-result-object v2

    .line 1973483
    iput-object v0, v2, LX/4mn;->c:Ljava/lang/String;

    .line 1973484
    move-object v0, v2

    .line 1973485
    const v2, 0x7f080054

    invoke-virtual {v0, v2}, LX/4mn;->c(I)LX/4mn;

    move-result-object v0

    sget-object v2, LX/8iK;->a:Landroid/net/Uri;

    .line 1973486
    iput-object v2, v0, LX/4mn;->e:Landroid/net/Uri;

    .line 1973487
    move-object v0, v0

    .line 1973488
    invoke-virtual {v0}, LX/4mn;->l()LX/4mm;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    goto :goto_0

    .line 1973489
    :cond_1
    iget-object v1, p0, LX/DBn;->a:LX/1CU;

    iget-object v1, v1, LX/1CU;->e:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method
