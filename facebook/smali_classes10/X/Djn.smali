.class public final LX/Djn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Ljava/util/Calendar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/util/Calendar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/Calendar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2033636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)LX/Djn;
    .locals 5

    .prologue
    .line 2033637
    new-instance v0, LX/Djn;

    invoke-direct {v0}, LX/Djn;-><init>()V

    .line 2033638
    iget-boolean v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    .line 2033639
    iput-boolean v1, v0, LX/Djn;->a:Z

    .line 2033640
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->b:Ljava/lang/String;

    .line 2033641
    iput-object v1, v0, LX/Djn;->b:Ljava/lang/String;

    .line 2033642
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->c:Ljava/lang/String;

    .line 2033643
    iput-object v1, v0, LX/Djn;->c:Ljava/lang/String;

    .line 2033644
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->d:Ljava/lang/String;

    .line 2033645
    iput-object v1, v0, LX/Djn;->d:Ljava/lang/String;

    .line 2033646
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->e:Ljava/lang/String;

    .line 2033647
    iput-object v1, v0, LX/Djn;->e:Ljava/lang/String;

    .line 2033648
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->f:Ljava/lang/String;

    .line 2033649
    iput-object v1, v0, LX/Djn;->f:Ljava/lang/String;

    .line 2033650
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->g:Ljava/lang/String;

    .line 2033651
    iput-object v1, v0, LX/Djn;->g:Ljava/lang/String;

    .line 2033652
    iget-wide v2, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->h:J

    .line 2033653
    iput-wide v2, v0, LX/Djn;->h:J

    .line 2033654
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->i:Ljava/lang/String;

    .line 2033655
    iput-object v1, v0, LX/Djn;->i:Ljava/lang/String;

    .line 2033656
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->j:Ljava/lang/String;

    .line 2033657
    iput-object v1, v0, LX/Djn;->j:Ljava/lang/String;

    .line 2033658
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->k:Ljava/lang/String;

    .line 2033659
    iput-object v1, v0, LX/Djn;->k:Ljava/lang/String;

    .line 2033660
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->l:Ljava/lang/String;

    .line 2033661
    iput-object v1, v0, LX/Djn;->l:Ljava/lang/String;

    .line 2033662
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->m:Ljava/lang/String;

    .line 2033663
    iput-object v1, v0, LX/Djn;->m:Ljava/lang/String;

    .line 2033664
    iget-boolean v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->n:Z

    .line 2033665
    iput-boolean v1, v0, LX/Djn;->n:Z

    .line 2033666
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    .line 2033667
    iput-object v1, v0, LX/Djn;->o:Ljava/util/Calendar;

    .line 2033668
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->p:Ljava/util/Calendar;

    .line 2033669
    iput-object v1, v0, LX/Djn;->p:Ljava/util/Calendar;

    .line 2033670
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->q:Ljava/util/Calendar;

    .line 2033671
    iput-object v1, v0, LX/Djn;->q:Ljava/util/Calendar;

    .line 2033672
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;
    .locals 1

    .prologue
    .line 2033673
    new-instance v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;-><init>(LX/Djn;)V

    return-object v0
.end method
