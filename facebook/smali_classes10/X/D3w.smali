.class public LX/D3w;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/121;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/121;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/121;",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1961409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961410
    iput-object p1, p0, LX/D3w;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1961411
    iput-object p2, p0, LX/D3w;->b:LX/121;

    .line 1961412
    iput-object p3, p0, LX/D3w;->c:LX/0Ot;

    .line 1961413
    return-void
.end method

.method public static a(LX/0QB;)LX/D3w;
    .locals 1

    .prologue
    .line 1961414
    invoke-static {p0}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/D3w;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1961415
    const-class v0, Landroid/support/v4/app/FragmentActivity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 1961416
    if-nez v0, :cond_0

    .line 1961417
    :goto_0
    return-void

    .line 1961418
    :cond_0
    iget-object v1, p0, LX/D3w;->b:LX/121;

    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    const v3, 0x7f080e4a

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/D3v;

    invoke-direct {v4, p0, p2, p1}, LX/D3v;-><init>(LX/D3w;Landroid/content/Intent;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 1961419
    iget-object v1, p0, LX/D3w;->b:LX/121;

    sget-object v2, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/121;->a(LX/0yY;LX/0gc;)V

    goto :goto_0
.end method

.method public static a(LX/D3w;Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;Z)V
    .locals 2

    .prologue
    .line 1961420
    invoke-static {p2, p3}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Landroid/content/Context;LX/04D;)Landroid/content/Intent;

    move-result-object v0

    .line 1961421
    const-string v1, "video_graphql_object"

    invoke-static {v0, v1, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1961422
    const-string v1, "video_player_allow_looping"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1961423
    invoke-static {p0, p2, v0}, LX/D3w;->a(LX/D3w;Landroid/content/Context;Landroid/content/Intent;)V

    .line 1961424
    return-void
.end method

.method public static b(LX/0QB;)LX/D3w;
    .locals 4

    .prologue
    .line 1961425
    new-instance v2, LX/D3w;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v1

    check-cast v1, LX/121;

    const/16 v3, 0x4e0

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/D3w;-><init>(Lcom/facebook/content/SecureContextHelper;LX/121;LX/0Ot;)V

    .line 1961426
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;)V
    .locals 1

    .prologue
    .line 1961427
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/D3w;->a(LX/D3w;Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;Z)V

    .line 1961428
    return-void
.end method

.method public final b(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;Landroid/content/Context;LX/04D;)V
    .locals 2

    .prologue
    .line 1961429
    invoke-static {p1}, LX/5hK;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, p2, p3, v1}, LX/D3w;->a(LX/D3w;Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;Z)V

    .line 1961430
    return-void
.end method
