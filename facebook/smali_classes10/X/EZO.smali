.class public final LX/EZO;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/EZO;


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EWc;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EZQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2139366
    invoke-static {}, LX/EZN;->c()LX/EZN;

    move-result-object v0

    invoke-virtual {v0}, LX/EZN;->a()LX/EZO;

    move-result-object v0

    sput-object v0, LX/EZO;->a:LX/EZO;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2139367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private f()[Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2139368
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/EZO;->b:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/EZO;->c:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/EZO;->d:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/EZO;->e:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/EZO;->f:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2139369
    if-ne p0, p1, :cond_0

    .line 2139370
    const/4 v0, 0x1

    .line 2139371
    :goto_0
    return v0

    .line 2139372
    :cond_0
    instance-of v0, p1, LX/EZO;

    if-nez v0, :cond_1

    .line 2139373
    const/4 v0, 0x0

    goto :goto_0

    .line 2139374
    :cond_1
    invoke-direct {p0}, LX/EZO;->f()[Ljava/lang/Object;

    move-result-object v0

    check-cast p1, LX/EZO;

    invoke-direct {p1}, LX/EZO;->f()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2139375
    invoke-direct {p0}, LX/EZO;->f()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
