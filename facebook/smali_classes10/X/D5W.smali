.class public final LX/D5W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final synthetic b:LX/0QK;

.field public final synthetic c:LX/0QK;

.field public final synthetic d:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/0QK;LX/0QK;)V
    .locals 0

    .prologue
    .line 1963692
    iput-object p1, p0, LX/D5W;->d:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iput-object p2, p0, LX/D5W;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object p3, p0, LX/D5W;->b:LX/0QK;

    iput-object p4, p0, LX/D5W;->c:LX/0QK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 6

    .prologue
    .line 1963693
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1963694
    iget-object v0, p0, LX/D5W;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1963695
    :goto_0
    iget-object v0, p0, LX/D5W;->d:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->E:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    iget-object v1, p0, LX/D5W;->d:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->F:LX/0hI;

    iget-object v2, p0, LX/D5W;->d:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v2, v2, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->an:Landroid/view/Window;

    invoke-virtual {v1, v2}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v1

    sub-int v3, v0, v1

    .line 1963696
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, LX/D5W;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1963697
    iget-object v0, p0, LX/D5W;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1963698
    instance-of v0, v1, LX/D5z;

    if-eqz v0, :cond_2

    .line 1963699
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v5, p0, LX/D5W;->b:LX/0QK;

    move-object v0, v1

    check-cast v0, LX/D5z;

    invoke-interface {v5, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1963700
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    div-int/lit8 v1, v3, 0x2

    sub-int/2addr v0, v1

    .line 1963701
    iget-object v1, p0, LX/D5W;->c:LX/0QK;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1963702
    :cond_0
    return-void

    .line 1963703
    :cond_1
    iget-object v0, p0, LX/D5W;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 1963704
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method
