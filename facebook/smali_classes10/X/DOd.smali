.class public final LX/DOd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/DOJ;

.field public final synthetic c:LX/DOl;


# direct methods
.method public constructor <init>(LX/DOl;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DOJ;)V
    .locals 0

    .prologue
    .line 1992608
    iput-object p1, p0, LX/DOd;->c:LX/DOl;

    iput-object p2, p0, LX/DOd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/DOd;->b:LX/DOJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1992590
    iget-object v0, p0, LX/DOd;->b:LX/DOJ;

    .line 1992591
    instance-of v1, p1, LX/2Oo;

    if-eqz v1, :cond_2

    .line 1992592
    check-cast p1, LX/2Oo;

    .line 1992593
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1992594
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    .line 1992595
    :goto_0
    move v1, v1

    .line 1992596
    const v2, 0x14ff47

    if-ne v1, v2, :cond_3

    .line 1992597
    const v2, 0x7f0824bf

    .line 1992598
    :goto_1
    move v0, v2

    .line 1992599
    iget-object v1, p0, LX/DOd;->c:LX/DOl;

    iget-object v1, v1, LX/DOl;->c:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1992600
    iget-object v0, p0, LX/DOd;->c:LX/DOl;

    iget-object v0, v0, LX/DOl;->k:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1992601
    iget-object v0, p0, LX/DOd;->b:LX/DOJ;

    sget-object v1, LX/DOJ;->Pin:LX/DOJ;

    if-ne v0, v1, :cond_1

    sget-object v0, LX/DOJ;->Unpin:LX/DOJ;

    .line 1992602
    :goto_2
    iget-object v1, p0, LX/DOd;->c:LX/DOl;

    iget-object v1, v1, LX/DOl;->b:LX/0bH;

    new-instance v2, LX/DNz;

    iget-object v3, p0, LX/DOd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v0, v4}, LX/DNz;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/DOJ;Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1992603
    :cond_0
    return-void

    .line 1992604
    :cond_1
    sget-object v0, LX/DOJ;->Pin:LX/DOJ;

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 1992605
    :cond_3
    sget-object v2, LX/DOJ;->Pin:LX/DOJ;

    if-ne v0, v2, :cond_4

    .line 1992606
    const v2, 0x7f0824c0

    goto :goto_1

    .line 1992607
    :cond_4
    const v2, 0x7f0824c1

    goto :goto_1
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1992588
    iget-object v0, p0, LX/DOd;->c:LX/DOl;

    iget-object v0, v0, LX/DOl;->b:LX/0bH;

    new-instance v1, LX/DNz;

    iget-object v2, p0, LX/DOd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/DOd;->b:LX/DOJ;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, LX/DNz;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/DOJ;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1992589
    return-void
.end method
