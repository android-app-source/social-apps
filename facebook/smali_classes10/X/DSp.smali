.class public LX/DSp;
.super LX/DSo;
.source ""

# interfaces
.implements LX/DSA;


# instance fields
.field private a:LX/DSZ;


# direct methods
.method public constructor <init>(LX/BWf;LX/DSX;LX/DS5;LX/DSZ;LX/BWd;Landroid/content/Context;)V
    .locals 6
    .param p1    # LX/BWf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/DSX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/DS5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/DSZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/BWd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1999888
    move-object v0, p0

    move-object v1, p6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/DSo;-><init>(Landroid/content/Context;LX/BWf;LX/DSX;LX/DS5;LX/BWd;)V

    .line 1999889
    iput-object p4, p0, LX/DSp;->a:LX/DSZ;

    .line 1999890
    return-void
.end method


# virtual methods
.method public final a()LX/DSZ;
    .locals 1

    .prologue
    .line 1999936
    iget-object v0, p0, LX/DSp;->a:LX/DSZ;

    return-object v0
.end method

.method public final a(Lcom/facebook/widget/listview/BetterListView;)V
    .locals 8

    .prologue
    .line 1999922
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v0

    if-gt v1, v0, :cond_0

    .line 1999923
    invoke-virtual {p1, v1}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1999924
    if-eqz v0, :cond_1

    instance-of v2, v0, LX/DSn;

    if-eqz v2, :cond_1

    .line 1999925
    check-cast v0, LX/DSn;

    .line 1999926
    iget-object v4, v0, LX/DSn;->b:LX/2qr;

    if-eqz v4, :cond_2

    .line 1999927
    iget-object v4, v0, LX/DSn;->b:LX/2qr;

    .line 1999928
    :goto_1
    move-object v3, v4

    .line 1999929
    invoke-virtual {v0, v3}, LX/DSn;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1999930
    :cond_0
    return-void

    .line 1999931
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1999932
    :cond_2
    new-instance v4, LX/2qr;

    const/4 v5, 0x0

    invoke-direct {v4, v0, v5}, LX/2qr;-><init>(Landroid/view/View;I)V

    iput-object v4, v0, LX/DSn;->b:LX/2qr;

    .line 1999933
    iget-object v4, v0, LX/DSn;->b:LX/2qr;

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v6, v7}, LX/2qr;->setDuration(J)V

    .line 1999934
    iget-object v4, v0, LX/DSn;->b:LX/2qr;

    new-instance v5, LX/DSm;

    invoke-direct {v5, v0}, LX/DSm;-><init>(LX/DSn;)V

    invoke-virtual {v4, v5}, LX/2qr;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1999935
    iget-object v4, v0, LX/DSn;->b:LX/2qr;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/widget/listview/BetterListView;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1999898
    move v1, v2

    :goto_0
    invoke-virtual {p0}, LX/BWh;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1999899
    invoke-virtual {p0, v1}, LX/BWh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1999900
    instance-of v3, v0, LX/DSf;

    if-eqz v3, :cond_3

    check-cast v0, LX/DSf;

    .line 1999901
    iget-object v3, v0, LX/DSf;->d:LX/DUV;

    move-object v0, v3

    .line 1999902
    invoke-interface {v0}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1999903
    iget-object v0, p0, LX/DSp;->a:LX/DSZ;

    invoke-virtual {v0, p1}, LX/DSZ;->a(Ljava/lang/String;)V

    .line 1999904
    :cond_0
    const/4 v0, 0x0

    move v3, v2

    move v1, v2

    move-object v4, v0

    .line 1999905
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/widget/listview/BetterListView;->getChildCount()I

    move-result v0

    if-gt v3, v0, :cond_5

    .line 1999906
    invoke-virtual {p2, v3}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1999907
    if-eqz v0, :cond_2

    instance-of v5, v0, LX/DSs;

    if-eqz v5, :cond_2

    .line 1999908
    if-nez v1, :cond_1

    if-eqz v4, :cond_1

    .line 1999909
    invoke-virtual {v4}, LX/DSs;->a()V

    :cond_1
    move-object v1, v0

    .line 1999910
    check-cast v1, LX/DSs;

    move-object v4, v1

    move v1, v2

    .line 1999911
    :cond_2
    if-eqz v0, :cond_7

    instance-of v5, v0, LX/DTt;

    if-eqz v5, :cond_7

    .line 1999912
    check-cast v0, LX/DTt;

    .line 1999913
    iget-object v5, v0, LX/DTt;->o:Ljava/lang/String;

    move-object v5, v5

    .line 1999914
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1999915
    invoke-virtual {v0}, LX/DTt;->a()V

    move v0, v1

    .line 1999916
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 1999917
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1999918
    :cond_4
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    .line 1999919
    :cond_5
    if-nez v1, :cond_6

    if-eqz v4, :cond_6

    .line 1999920
    invoke-virtual {v4}, LX/DSs;->a()V

    .line 1999921
    :cond_6
    return-void

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1999937
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1999896
    iget-object v0, p0, LX/DSp;->a:LX/DSZ;

    invoke-virtual {v0}, LX/DSZ;->a()V

    .line 1999897
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 1999895
    invoke-virtual {p0}, LX/BWh;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DSp;->a:LX/DSZ;

    invoke-virtual {v0}, LX/DSZ;->b()I

    move-result v0

    invoke-virtual {p0}, LX/BWh;->getCount()I

    move-result v1

    invoke-virtual {p0}, LX/DSo;->getSections()[Ljava/lang/Object;

    move-result-object v2

    array-length v2, v2

    sub-int/2addr v1, v2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 1999891
    invoke-virtual {p0, p1}, LX/BWh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1999892
    instance-of v0, v0, LX/DSf;

    if-eqz v0, :cond_0

    .line 1999893
    const/4 v0, 0x1

    .line 1999894
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
