.class public final LX/E4w;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/E4y;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/E4x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/E4y",
            "<TE;>.ReactionImageBlockComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/E4y;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/E4y;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2077559
    iput-object p1, p0, LX/E4w;->b:LX/E4y;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2077560
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "imageUri"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "unitComponentStyle"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/E4w;->c:[Ljava/lang/String;

    .line 2077561
    iput v3, p0, LX/E4w;->d:I

    .line 2077562
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/E4w;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/E4w;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/E4w;LX/1De;IILX/E4x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/E4y",
            "<TE;>.ReactionImageBlockComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2077555
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2077556
    iput-object p4, p0, LX/E4w;->a:LX/E4x;

    .line 2077557
    iget-object v0, p0, LX/E4w;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2077558
    return-void
.end method


# virtual methods
.method public final a(LX/2km;)LX/E4w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077553
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->j:LX/2km;

    .line 2077554
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/E4w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077550
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->a:Landroid/net/Uri;

    .line 2077551
    iget-object v0, p0, LX/E4w;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2077552
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/E4w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077547
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2077548
    iget-object v0, p0, LX/E4w;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2077549
    return-object p0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/E4w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077545
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2077546
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2077541
    invoke-super {p0}, LX/1X5;->a()V

    .line 2077542
    const/4 v0, 0x0

    iput-object v0, p0, LX/E4w;->a:LX/E4x;

    .line 2077543
    iget-object v0, p0, LX/E4w;->b:LX/E4y;

    iget-object v0, v0, LX/E4y;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2077544
    return-void
.end method

.method public final b(Landroid/net/Uri;)LX/E4w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077563
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->e:Landroid/net/Uri;

    .line 2077564
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/E4w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077538
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->b:Ljava/lang/String;

    .line 2077539
    iget-object v0, p0, LX/E4w;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2077540
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/E4w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077536
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->d:Ljava/lang/String;

    .line 2077537
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/E4y;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2077526
    iget-object v1, p0, LX/E4w;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/E4w;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/E4w;->d:I

    if-ge v1, v2, :cond_2

    .line 2077527
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2077528
    :goto_0
    iget v2, p0, LX/E4w;->d:I

    if-ge v0, v2, :cond_1

    .line 2077529
    iget-object v2, p0, LX/E4w;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2077530
    iget-object v2, p0, LX/E4w;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2077531
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2077532
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2077533
    :cond_2
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    .line 2077534
    invoke-virtual {p0}, LX/E4w;->a()V

    .line 2077535
    return-object v0
.end method

.method public final d(Ljava/lang/String;)LX/E4w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077524
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->i:Ljava/lang/String;

    .line 2077525
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/E4w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077522
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->k:Ljava/lang/String;

    .line 2077523
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/E4w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2077520
    iget-object v0, p0, LX/E4w;->a:LX/E4x;

    iput-object p1, v0, LX/E4x;->l:Ljava/lang/String;

    .line 2077521
    return-object p0
.end method
