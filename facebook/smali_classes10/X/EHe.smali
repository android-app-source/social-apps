.class public final enum LX/EHe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EHe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EHe;

.field public static final enum COUNT_DOWN:LX/EHe;

.field public static final enum GAME_OVER:LX/EHe;

.field public static final enum IN_GAME:LX/EHe;

.field public static final enum NOT_START:LX/EHe;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2099966
    new-instance v0, LX/EHe;

    const-string v1, "NOT_START"

    invoke-direct {v0, v1, v2}, LX/EHe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EHe;->NOT_START:LX/EHe;

    .line 2099967
    new-instance v0, LX/EHe;

    const-string v1, "COUNT_DOWN"

    invoke-direct {v0, v1, v3}, LX/EHe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EHe;->COUNT_DOWN:LX/EHe;

    .line 2099968
    new-instance v0, LX/EHe;

    const-string v1, "IN_GAME"

    invoke-direct {v0, v1, v4}, LX/EHe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EHe;->IN_GAME:LX/EHe;

    .line 2099969
    new-instance v0, LX/EHe;

    const-string v1, "GAME_OVER"

    invoke-direct {v0, v1, v5}, LX/EHe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EHe;->GAME_OVER:LX/EHe;

    .line 2099970
    const/4 v0, 0x4

    new-array v0, v0, [LX/EHe;

    sget-object v1, LX/EHe;->NOT_START:LX/EHe;

    aput-object v1, v0, v2

    sget-object v1, LX/EHe;->COUNT_DOWN:LX/EHe;

    aput-object v1, v0, v3

    sget-object v1, LX/EHe;->IN_GAME:LX/EHe;

    aput-object v1, v0, v4

    sget-object v1, LX/EHe;->GAME_OVER:LX/EHe;

    aput-object v1, v0, v5

    sput-object v0, LX/EHe;->$VALUES:[LX/EHe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2099973
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EHe;
    .locals 1

    .prologue
    .line 2099972
    const-class v0, LX/EHe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EHe;

    return-object v0
.end method

.method public static values()[LX/EHe;
    .locals 1

    .prologue
    .line 2099971
    sget-object v0, LX/EHe;->$VALUES:[LX/EHe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EHe;

    return-object v0
.end method
