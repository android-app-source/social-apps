.class public final LX/DgG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/DgH;

.field public final b:LX/6eE;

.field public c:Lcom/facebook/android/maps/model/LatLng;

.field public d:Lcom/facebook/messaging/location/sending/NearbyPlace;

.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6eE;)V
    .locals 1

    .prologue
    .line 2029300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2029301
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DgG;->e:Z

    .line 2029302
    iput-object p1, p0, LX/DgG;->b:LX/6eE;

    .line 2029303
    return-void
.end method


# virtual methods
.method public final b()LX/DgI;
    .locals 2

    .prologue
    .line 2029304
    iget-object v0, p0, LX/DgG;->c:Lcom/facebook/android/maps/model/LatLng;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DgG;->d:Lcom/facebook/messaging/location/sending/NearbyPlace;

    if-eqz v0, :cond_0

    .line 2029305
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cant set both initialLocation and initialNearbyPlace"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2029306
    :cond_0
    new-instance v0, LX/DgI;

    invoke-direct {v0, p0}, LX/DgI;-><init>(LX/DgG;)V

    return-object v0
.end method
