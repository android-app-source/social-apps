.class public LX/EiD;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final f:[Ljava/lang/String;


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2159185
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "fb.com"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "fb.me"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "facebook.com"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "#fb"

    aput-object v2, v0, v1

    sput-object v0, LX/EiD;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2159186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2159187
    iput-wide p1, p0, LX/EiD;->a:J

    .line 2159188
    iput-wide p3, p0, LX/EiD;->b:J

    .line 2159189
    iput-object p5, p0, LX/EiD;->c:Ljava/lang/String;

    .line 2159190
    iput-object p6, p0, LX/EiD;->d:Ljava/lang/String;

    .line 2159191
    if-eqz p6, :cond_0

    .line 2159192
    sget-object v2, LX/EiD;->f:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 2159193
    invoke-virtual {p6, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2159194
    const/4 v0, 0x1

    .line 2159195
    :cond_0
    iput-boolean v0, p0, LX/EiD;->e:Z

    .line 2159196
    return-void

    .line 2159197
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2159198
    const-wide/16 v2, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, LX/EiD;-><init>(JJLjava/lang/String;Ljava/lang/String;)V

    .line 2159199
    return-void
.end method
