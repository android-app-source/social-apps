.class public final LX/Csa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:LX/CsC;


# direct methods
.method public constructor <init>(LX/CsC;)V
    .locals 0

    .prologue
    .line 1943000
    iput-object p1, p0, LX/Csa;->a:LX/CsC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 1943001
    iget-object v0, p0, LX/Csa;->a:LX/CsC;

    iget-boolean v0, v0, LX/CsC;->v:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Csa;->a:LX/CsC;

    iget-object v0, v0, LX/CsC;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getCurrentPlayTime()J

    move-result-wide v0

    const-wide/16 v2, 0x320

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 1943002
    iget-object v0, p0, LX/Csa;->a:LX/CsC;

    iget-object v0, v0, LX/CsC;->k:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1943003
    iget-object v0, p0, LX/Csa;->a:LX/CsC;

    const/4 v1, 0x1

    .line 1943004
    iput-boolean v1, v0, LX/CsC;->v:Z

    .line 1943005
    :cond_0
    iget-object v1, p0, LX/Csa;->a:LX/CsC;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1943006
    iput v0, v1, LX/CsC;->q:F

    .line 1943007
    iget-object v0, p0, LX/Csa;->a:LX/CsC;

    invoke-virtual {v0}, LX/CsC;->invalidate()V

    .line 1943008
    return-void
.end method
