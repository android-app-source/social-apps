.class public final enum LX/Cmp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cmp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cmp;

.field public static final enum BLOCK:LX/Cmp;

.field public static final enum INLINE:LX/Cmp;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1933599
    new-instance v0, LX/Cmp;

    const-string v1, "BLOCK"

    invoke-direct {v0, v1, v2}, LX/Cmp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cmp;->BLOCK:LX/Cmp;

    .line 1933600
    new-instance v0, LX/Cmp;

    const-string v1, "INLINE"

    invoke-direct {v0, v1, v3}, LX/Cmp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cmp;->INLINE:LX/Cmp;

    .line 1933601
    const/4 v0, 0x2

    new-array v0, v0, [LX/Cmp;

    sget-object v1, LX/Cmp;->BLOCK:LX/Cmp;

    aput-object v1, v0, v2

    sget-object v1, LX/Cmp;->INLINE:LX/Cmp;

    aput-object v1, v0, v3

    sput-object v0, LX/Cmp;->$VALUES:[LX/Cmp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1933602
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cmp;
    .locals 1

    .prologue
    .line 1933603
    const-class v0, LX/Cmp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cmp;

    return-object v0
.end method

.method public static values()[LX/Cmp;
    .locals 1

    .prologue
    .line 1933604
    sget-object v0, LX/Cmp;->$VALUES:[LX/Cmp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cmp;

    return-object v0
.end method
