.class public final LX/Dee;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 2022914
    const/4 v5, 0x0

    .line 2022915
    const-wide/16 v6, 0x0

    .line 2022916
    const/4 v4, 0x0

    .line 2022917
    const/4 v3, 0x0

    .line 2022918
    const/4 v2, 0x0

    .line 2022919
    const/4 v1, 0x0

    .line 2022920
    const/4 v0, 0x0

    .line 2022921
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 2022922
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2022923
    const/4 v0, 0x0

    .line 2022924
    :goto_0
    return v0

    .line 2022925
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v5, :cond_5

    .line 2022926
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 2022927
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2022928
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v10, :cond_0

    if-eqz v0, :cond_0

    .line 2022929
    const-string v5, "height"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2022930
    const/4 v0, 0x1

    .line 2022931
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v9, v4

    move v4, v0

    goto :goto_1

    .line 2022932
    :cond_1
    const-string v5, "scale"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2022933
    const/4 v0, 0x1

    .line 2022934
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 2022935
    :cond_2
    const-string v5, "uri"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2022936
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v8, v0

    goto :goto_1

    .line 2022937
    :cond_3
    const-string v5, "width"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2022938
    const/4 v0, 0x1

    .line 2022939
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v6, v0

    move v7, v5

    goto :goto_1

    .line 2022940
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2022941
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2022942
    if-eqz v4, :cond_6

    .line 2022943
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v9, v4}, LX/186;->a(III)V

    .line 2022944
    :cond_6
    if-eqz v1, :cond_7

    .line 2022945
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2022946
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2022947
    if-eqz v6, :cond_8

    .line 2022948
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v7, v1}, LX/186;->a(III)V

    .line 2022949
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v8, v4

    move v9, v5

    move v4, v2

    move-wide v11, v6

    move v6, v0

    move v7, v3

    move-wide v2, v11

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2022950
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2022951
    invoke-virtual {p0, p1, v3, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2022952
    if-eqz v0, :cond_0

    .line 2022953
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022954
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2022955
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2022956
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 2022957
    const-string v2, "scale"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022958
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2022959
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2022960
    if-eqz v0, :cond_2

    .line 2022961
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022962
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2022963
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2022964
    if-eqz v0, :cond_3

    .line 2022965
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022966
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2022967
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2022968
    return-void
.end method
