.class public final LX/E6D;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/E6C;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/E6A;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2km;

.field private final c:Landroid/view/LayoutInflater;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/2km;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/E6A;",
            ">;",
            "LX/2km;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2079784
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2079785
    iput-object p4, p0, LX/E6D;->d:Ljava/lang/String;

    .line 2079786
    iput-object p5, p0, LX/E6D;->e:Ljava/lang/String;

    .line 2079787
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/E6D;->c:Landroid/view/LayoutInflater;

    .line 2079788
    iput-object p2, p0, LX/E6D;->a:LX/0Px;

    .line 2079789
    iput-object p3, p0, LX/E6D;->b:LX/2km;

    .line 2079790
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2079791
    new-instance v1, LX/E6C;

    iget-object v0, p0, LX/E6D;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f031100

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-direct {v1, v0}, LX/E6C;-><init>(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2079792
    check-cast p1, LX/E6C;

    const/4 v2, 0x0

    .line 2079793
    iget-object v0, p0, LX/E6D;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E6A;

    .line 2079794
    iget-object v3, v0, LX/E6A;->a:LX/9rk;

    .line 2079795
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/fbui/widget/contentview/ContentView;

    move-object v4, v1

    .line 2079796
    invoke-interface {v3}, LX/9rk;->hi_()LX/174;

    move-result-object v1

    if-nez v1, :cond_0

    move-object v1, v2

    .line 2079797
    :goto_0
    invoke-interface {v3}, LX/9rk;->d()LX/5sY;

    move-result-object v5

    if-nez v5, :cond_1

    .line 2079798
    :goto_1
    if-eqz v2, :cond_2

    .line 2079799
    invoke-virtual {v4, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2079800
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2079801
    :goto_2
    invoke-interface {v3}, LX/9rk;->e()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2079802
    invoke-virtual {v4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2079803
    new-instance v1, LX/E6B;

    invoke-direct {v1, p0, v0}, LX/E6B;-><init>(LX/E6D;LX/E6A;)V

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2079804
    return-void

    .line 2079805
    :cond_0
    invoke-interface {v3}, LX/9rk;->hi_()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2079806
    :cond_1
    invoke-interface {v3}, LX/9rk;->d()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    .line 2079807
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_2
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2079808
    iget-object v0, p0, LX/E6D;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
