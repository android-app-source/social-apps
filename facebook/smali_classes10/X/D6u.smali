.class public final LX/D6u;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/D6v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/D6v;)V
    .locals 1

    .prologue
    .line 1966234
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1966235
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D6u;->a:Ljava/lang/ref/WeakReference;

    .line 1966236
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    .line 1966237
    iget-object v0, p0, LX/D6u;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D6v;

    .line 1966238
    if-nez v0, :cond_0

    .line 1966239
    :goto_0
    return-void

    .line 1966240
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1966241
    :pswitch_0
    const-wide/16 v6, -0x1

    .line 1966242
    iget-object v2, v0, LX/D6v;->n:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 1966243
    iget-wide v4, v0, LX/D6v;->u:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 1966244
    iput-wide v2, v0, LX/D6v;->u:J

    .line 1966245
    :cond_1
    invoke-static {v0}, LX/D6v;->x(LX/D6v;)J

    move-result-wide v2

    .line 1966246
    cmp-long v4, v2, v6

    if-nez v4, :cond_4

    .line 1966247
    :goto_1
    goto :goto_0

    .line 1966248
    :pswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v1, :cond_2

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, LX/BSU;

    .line 1966249
    :goto_2
    invoke-static {v0, v1}, LX/D6v;->c(LX/D6v;LX/BSU;)V

    goto :goto_0

    .line 1966250
    :cond_2
    sget-object v1, LX/BSU;->NONE:LX/BSU;

    goto :goto_2

    .line 1966251
    :pswitch_2
    invoke-static {v0}, LX/D6v;->m(LX/D6v;)V

    goto :goto_0

    .line 1966252
    :pswitch_3
    iget-object v2, v0, LX/D6v;->w:LX/3H0;

    sget-object v3, LX/3H0;->LIVE:LX/3H0;

    if-ne v2, v3, :cond_6

    iget-wide v2, v0, LX/D6v;->v:J

    .line 1966253
    :goto_3
    sget-object v4, LX/BSR;->PLAYBACK_FINISHED:LX/BSR;

    iput-object v4, v0, LX/D6v;->F:LX/BSR;

    .line 1966254
    const-wide/16 v4, 0x1388

    cmp-long v4, v2, v4

    if-lez v4, :cond_7

    .line 1966255
    sget-object v2, LX/BST;->SHORT_AD:LX/BST;

    iput-object v2, v0, LX/D6v;->E:LX/BST;

    .line 1966256
    invoke-static {v0}, LX/D6v;->p(LX/D6v;)V

    .line 1966257
    :goto_4
    goto :goto_0

    .line 1966258
    :pswitch_4
    invoke-static {v0}, LX/D6v;->p(LX/D6v;)V

    goto :goto_0

    .line 1966259
    :pswitch_5
    sget-object v2, LX/2oN;->WAIT_FOR_ADS:LX/2oN;

    invoke-static {v0, v2}, LX/D6v;->a(LX/D6v;LX/2oN;)V

    .line 1966260
    iget-object v2, v0, LX/D6v;->m:LX/D6u;

    const/4 v3, 0x6

    iget-object v4, v0, LX/D6v;->j:LX/2ml;

    iget-wide v4, v4, LX/2ml;->e:J

    invoke-virtual {v2, v3, v4, v5}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    .line 1966261
    goto :goto_0

    .line 1966262
    :pswitch_6
    sget-object v2, LX/2oN;->TRANSITION:LX/2oN;

    invoke-static {v0, v2}, LX/D6v;->a(LX/D6v;LX/2oN;)V

    .line 1966263
    iget-object v2, v0, LX/D6v;->m:LX/D6u;

    const/16 v3, 0x9

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    .line 1966264
    goto :goto_0

    .line 1966265
    :pswitch_7
    const/16 v4, 0x9

    .line 1966266
    sget-object v2, LX/D6s;->a:[I

    iget-object v3, v0, LX/D6v;->a:LX/2oN;

    invoke-virtual {v3}, LX/2oN;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 1966267
    :goto_5
    :pswitch_8
    goto :goto_0

    .line 1966268
    :pswitch_9
    iget-boolean v1, v0, LX/D6v;->B:Z

    if-nez v1, :cond_a

    .line 1966269
    :goto_6
    invoke-static {v0}, LX/D6v;->u(LX/D6v;)V

    .line 1966270
    sget-object v1, LX/2oN;->NONE:LX/2oN;

    invoke-static {v0, v1}, LX/D6v;->a(LX/D6v;LX/2oN;)V

    .line 1966271
    goto/16 :goto_0

    .line 1966272
    :pswitch_a
    sget-object v2, LX/2oN;->VOD_NO_VIDEO_AD:LX/2oN;

    invoke-static {v0, v2}, LX/D6v;->a(LX/D6v;LX/2oN;)V

    .line 1966273
    sget-object v2, LX/BSR;->NONE:LX/BSR;

    iput-object v2, v0, LX/D6v;->F:LX/BSR;

    .line 1966274
    iget-object v2, v0, LX/D6v;->m:LX/D6u;

    const/16 v3, 0x9

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    .line 1966275
    goto/16 :goto_0

    .line 1966276
    :cond_3
    iget-wide v4, v0, LX/D6v;->u:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 1966277
    iput-wide v6, v0, LX/D6v;->u:J

    goto/16 :goto_1

    .line 1966278
    :cond_4
    iget-object v4, v0, LX/D6v;->j:LX/2ml;

    iget v4, v4, LX/2ml;->b:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_5

    .line 1966279
    iget-object v2, v0, LX/D6v;->m:LX/D6u;

    iget-object v3, v0, LX/D6v;->m:LX/D6u;

    const/4 v4, 0x2

    sget-object v5, LX/BSU;->NORMAL:LX/BSU;

    invoke-virtual {v3, v4, v5}, LX/D6u;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/D6u;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 1966280
    :cond_5
    iget-object v2, v0, LX/D6v;->m:LX/D6u;

    const/4 v3, 0x1

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 1966281
    :cond_6
    const-wide/16 v2, 0x7d0

    goto/16 :goto_3

    .line 1966282
    :cond_7
    sget-object v4, LX/2oN;->TRANSITION:LX/2oN;

    invoke-static {v0, v4}, LX/D6v;->a(LX/D6v;LX/2oN;)V

    .line 1966283
    iget-object v4, v0, LX/D6v;->m:LX/D6u;

    const/16 v5, 0x9

    invoke-virtual {v4, v5, v2, v3}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_4

    .line 1966284
    :pswitch_b
    iget-object v2, v0, LX/D6v;->F:LX/BSR;

    sget-object v3, LX/BSR;->NONE:LX/BSR;

    if-ne v2, v3, :cond_8

    .line 1966285
    sget-object v2, LX/BSR;->PLAYBACK_FINISHED:LX/BSR;

    iput-object v2, v0, LX/D6v;->F:LX/BSR;

    .line 1966286
    :cond_8
    iget-object v2, v0, LX/D6v;->m:LX/D6u;

    invoke-virtual {v2, v4}, LX/D6u;->sendEmptyMessage(I)Z

    goto :goto_5

    .line 1966287
    :pswitch_c
    iget-boolean v2, v0, LX/D6v;->t:Z

    if-nez v2, :cond_9

    .line 1966288
    iget-object v2, v0, LX/D6v;->m:LX/D6u;

    const/16 v3, 0x8

    iget-object v4, v0, LX/D6v;->j:LX/2ml;

    iget-wide v4, v4, LX/2ml;->d:J

    invoke-virtual {v2, v3, v4, v5}, LX/D6u;->sendEmptyMessageDelayed(IJ)Z

    .line 1966289
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/D6v;->t:Z

    goto/16 :goto_5

    .line 1966290
    :cond_9
    iget-object v2, v0, LX/D6v;->m:LX/D6u;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/D6u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1966291
    sget-object v2, LX/BSR;->CUT_OFF:LX/BSR;

    iput-object v2, v0, LX/D6v;->F:LX/BSR;

    .line 1966292
    iget-object v2, v0, LX/D6v;->m:LX/D6u;

    iget-object v3, v0, LX/D6v;->m:LX/D6u;

    invoke-virtual {v3, v4}, LX/D6u;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/D6u;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto/16 :goto_5

    .line 1966293
    :cond_a
    new-instance v1, LX/BSP;

    invoke-direct {v1}, LX/BSP;-><init>()V

    .line 1966294
    iget-object v2, v0, LX/D6v;->F:LX/BSR;

    iput-object v2, v1, LX/BSP;->c:LX/BSR;

    .line 1966295
    iget-boolean v2, v0, LX/D6v;->C:Z

    iput-boolean v2, v1, LX/BSP;->g:Z

    .line 1966296
    invoke-virtual {v0}, LX/D6v;->d()LX/04D;

    move-result-object v2

    iput-object v2, v1, LX/BSP;->j:LX/04D;

    .line 1966297
    iget v2, v0, LX/D6v;->g:I

    iput v2, v1, LX/BSP;->k:I

    .line 1966298
    iget-object v2, v0, LX/D6v;->o:LX/3H4;

    iget-object v3, v0, LX/D6v;->i:Ljava/lang/String;

    sget-object v4, LX/BSS;->END:LX/BSS;

    iget-object v5, v0, LX/D6v;->w:LX/3H0;

    invoke-virtual {v2, v3, v4, v1, v5}, LX/3H4;->a(Ljava/lang/String;LX/BSS;LX/BSP;LX/3H0;)V

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_a
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_b
        :pswitch_c
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method
