.class public final LX/EJn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/1Ps;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;LX/CzL;LX/1Ps;)V
    .locals 0

    .prologue
    .line 2105247
    iput-object p1, p0, LX/EJn;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    iput-object p2, p0, LX/EJn;->a:LX/CzL;

    iput-object p3, p0, LX/EJn;->b:LX/1Ps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x2188d75e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2105248
    iget-object v0, p0, LX/EJn;->a:LX/CzL;

    invoke-static {v0}, LX/EJf;->a(LX/CzL;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    .line 2105249
    iget-object v0, p0, LX/EJn;->a:LX/CzL;

    .line 2105250
    iget-object v3, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v3

    .line 2105251
    check-cast v0, LX/A2T;

    .line 2105252
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v2, v3, :cond_1

    .line 2105253
    iget-object v2, p0, LX/EJn;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->d:LX/7j6;

    invoke-interface {v0}, LX/A2T;->dW_()Ljava/lang/String;

    move-result-object v0

    sget-object v3, LX/7iP;->GLOBAL_SEARCH:LX/7iP;

    invoke-virtual {v2, v0, v3}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2105254
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EJn;->b:LX/1Ps;

    check-cast v0, LX/Cxh;

    iget-object v2, p0, LX/EJn;->a:LX/CzL;

    invoke-interface {v0, v2}, LX/Cxh;->c(LX/CzL;)V

    .line 2105255
    const v0, 0x1a74da36

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2105256
    :cond_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v2, v3, :cond_0

    .line 2105257
    invoke-interface {v0}, LX/A2T;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v0

    .line 2105258
    new-instance v2, LX/89k;

    invoke-direct {v2}, LX/89k;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 2105259
    iput-object v3, v2, LX/89k;->b:Ljava/lang/String;

    .line 2105260
    move-object v2, v2

    .line 2105261
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2105262
    iput-object v0, v2, LX/89k;->c:Ljava/lang/String;

    .line 2105263
    move-object v0, v2

    .line 2105264
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 2105265
    iget-object v2, p0, LX/EJn;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->e:LX/0hy;

    invoke-interface {v2, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v2

    .line 2105266
    if-eqz v2, :cond_0

    .line 2105267
    iget-object v0, p0, LX/EJn;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    iget-object v3, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/EJn;->b:LX/1Ps;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
