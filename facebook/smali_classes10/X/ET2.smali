.class public final LX/ET2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/video/videohome/data/VideoHomeDataFetcher$VideoHomeDataFetchingListener",
        "<",
        "LX/9qT;",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionPaginatedSubComponents;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ETB;


# direct methods
.method public constructor <init>(LX/ETB;)V
    .locals 0

    .prologue
    .line 2123901
    iput-object p1, p0, LX/ET2;->a:LX/ETB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2123902
    check-cast p1, LX/9qT;

    .line 2123903
    iget-object v0, p0, LX/ET2;->a:LX/ETB;

    invoke-virtual {v0, p1}, LX/ETB;->a(LX/9qT;)V

    .line 2123904
    iget-object v0, p0, LX/ET2;->a:LX/ETB;

    invoke-virtual {v0}, LX/ETB;->m()V

    .line 2123905
    iget-object v0, p0, LX/ET2;->a:LX/ETB;

    const/4 v2, 0x0

    const/4 p0, 0x0

    .line 2123906
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123907
    invoke-interface {p1}, LX/9qT;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    .line 2123908
    if-lez v4, :cond_1

    .line 2123909
    invoke-interface {p1}, LX/9qT;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    .line 2123910
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    .line 2123911
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2123912
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->S()Ljava/lang/String;

    move-result-object v1

    move-object v2, v3

    .line 2123913
    :goto_0
    invoke-static {v0, v4, v2, v1}, LX/ETB;->a(LX/ETB;ILjava/lang/String;Ljava/lang/String;)V

    .line 2123914
    return-void

    :cond_0
    move-object v1, v2

    move-object v2, v3

    goto :goto_0

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method
