.class public LX/ESx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2123757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;)Lcom/facebook/graphql/model/GraphQLActor;
    .locals 6

    .prologue
    .line 2123758
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v0

    .line 2123759
    new-instance v1, LX/173;

    invoke-direct {v1}, LX/173;-><init>()V

    .line 2123760
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->l()LX/174;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2123761
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->l()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    .line 2123762
    iput-object v2, v1, LX/173;->f:Ljava/lang/String;

    .line 2123763
    :cond_0
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    .line 2123764
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->k()LX/174;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2123765
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->k()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    .line 2123766
    iput-object v3, v2, LX/173;->f:Ljava/lang/String;

    .line 2123767
    :cond_1
    new-instance v3, LX/2dc;

    invoke-direct {v3}, LX/2dc;-><init>()V

    .line 2123768
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->dN_()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->dN_()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->b()LX/1Fb;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2123769
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->dN_()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;->b()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    .line 2123770
    iput-object v4, v3, LX/2dc;->h:Ljava/lang/String;

    .line 2123771
    :cond_2
    new-instance v4, LX/25F;

    invoke-direct {v4}, LX/25F;-><init>()V

    invoke-virtual {v3}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 2123772
    iput-object v3, v4, LX/25F;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2123773
    move-object v3, v4

    .line 2123774
    new-instance v4, LX/3dL;

    invoke-direct {v4}, LX/3dL;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2123775
    iput-object v5, v4, LX/3dL;->E:Ljava/lang/String;

    .line 2123776
    move-object v4, v4

    .line 2123777
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->j()Z

    move-result v5

    .line 2123778
    iput-boolean v5, v4, LX/3dL;->aQ:Z

    .line 2123779
    move-object v4, v4

    .line 2123780
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->d()Z

    move-result v5

    .line 2123781
    iput-boolean v5, v4, LX/3dL;->aM:Z

    .line 2123782
    move-object v4, v4

    .line 2123783
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->dM_()Z

    move-result v5

    .line 2123784
    iput-boolean v5, v4, LX/3dL;->aP:Z

    .line 2123785
    move-object v4, v4

    .line 2123786
    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->e()Z

    move-result v0

    .line 2123787
    iput-boolean v0, v4, LX/3dL;->aN:Z

    .line 2123788
    move-object v0, v4

    .line 2123789
    invoke-virtual {v1}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2123790
    iput-object v1, v0, LX/3dL;->aS:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2123791
    move-object v0, v0

    .line 2123792
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2123793
    iput-object v1, v0, LX/3dL;->aR:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2123794
    move-object v0, v0

    .line 2123795
    invoke-virtual {v3}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 2123796
    iput-object v1, v0, LX/3dL;->aO:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 2123797
    move-object v0, v0

    .line 2123798
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 3
    .param p0    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2123799
    if-nez p0, :cond_1

    .line 2123800
    :cond_0
    :goto_0
    return v0

    .line 2123801
    :cond_1
    iget-object v1, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2123802
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/ESx;LX/ETQ;Lcom/facebook/graphql/model/GraphQLActor;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2123803
    move v0, v1

    :goto_0
    invoke-virtual {p1}, LX/ETQ;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2123804
    invoke-virtual {p1, v0}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v2

    .line 2123805
    invoke-static {v2}, LX/ESx;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2123806
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    .line 2123807
    iget-object v4, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2123808
    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2123809
    iget-object v1, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2123810
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    .line 2123811
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v4

    .line 2123812
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v5

    .line 2123813
    invoke-static {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->a(Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;)Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v5

    .line 2123814
    new-instance v6, LX/7OP;

    invoke-direct {v6}, LX/7OP;-><init>()V

    .line 2123815
    invoke-virtual {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    iput-object p0, v6, LX/7OP;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2123816
    invoke-virtual {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->c()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v6, LX/7OP;->b:Ljava/lang/String;

    .line 2123817
    invoke-virtual {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->d()Z

    move-result p0

    iput-boolean p0, v6, LX/7OP;->c:Z

    .line 2123818
    invoke-virtual {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->e()Z

    move-result p0

    iput-boolean p0, v6, LX/7OP;->d:Z

    .line 2123819
    invoke-virtual {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    move-result-object p0

    iput-object p0, v6, LX/7OP;->e:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    .line 2123820
    invoke-virtual {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->dM_()Z

    move-result p0

    iput-boolean p0, v6, LX/7OP;->f:Z

    .line 2123821
    invoke-virtual {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->j()Z

    move-result p0

    iput-boolean p0, v6, LX/7OP;->g:Z

    .line 2123822
    invoke-virtual {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object p0

    iput-object p0, v6, LX/7OP;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2123823
    invoke-virtual {v5}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object p0

    iput-object p0, v6, LX/7OP;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2123824
    move-object v5, v6

    .line 2123825
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v6

    .line 2123826
    iput-boolean v6, v5, LX/7OP;->c:Z

    .line 2123827
    move-object v5, v5

    .line 2123828
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v6

    .line 2123829
    iput-boolean v6, v5, LX/7OP;->g:Z

    .line 2123830
    move-object v5, v5

    .line 2123831
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v6

    .line 2123832
    iput-boolean v6, v5, LX/7OP;->d:Z

    .line 2123833
    move-object v5, v5

    .line 2123834
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v6

    .line 2123835
    iput-boolean v6, v5, LX/7OP;->f:Z

    .line 2123836
    move-object v5, v5

    .line 2123837
    invoke-virtual {v5}, LX/7OP;->a()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v5

    .line 2123838
    invoke-static {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v4

    .line 2123839
    new-instance v6, LX/9sd;

    invoke-direct {v6}, LX/9sd;-><init>()V

    .line 2123840
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->l()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    iput-object p0, v6, LX/9sd;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2123841
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v6, LX/9sd;->b:Ljava/lang/String;

    .line 2123842
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->c()I

    move-result p0

    iput p0, v6, LX/9sd;->c:I

    .line 2123843
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object p0

    iput-object p0, v6, LX/9sd;->d:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    .line 2123844
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->e()Z

    move-result p0

    iput-boolean p0, v6, LX/9sd;->e:Z

    .line 2123845
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->o()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object p0

    iput-object p0, v6, LX/9sd;->f:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    .line 2123846
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->gN_()Z

    move-result p0

    iput-boolean p0, v6, LX/9sd;->g:Z

    .line 2123847
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->gO_()I

    move-result p0

    iput p0, v6, LX/9sd;->h:I

    .line 2123848
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->p()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object p0

    iput-object p0, v6, LX/9sd;->i:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    .line 2123849
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->q()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object p0

    iput-object p0, v6, LX/9sd;->j:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    .line 2123850
    move-object v4, v6

    .line 2123851
    iput-object v5, v4, LX/9sd;->f:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    .line 2123852
    move-object v4, v4

    .line 2123853
    invoke-virtual {v4}, LX/9sd;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v4

    .line 2123854
    invoke-static {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    invoke-static {v3}, LX/9sr;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/9sr;

    move-result-object v3

    .line 2123855
    iput-object v4, v3, LX/9sr;->ag:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    .line 2123856
    move-object v3, v3

    .line 2123857
    invoke-virtual {v3}, LX/9sr;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    .line 2123858
    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;->a(LX/9uc;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/9vT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;)LX/9vT;

    move-result-object v1

    .line 2123859
    iput-object v3, v1, LX/9vT;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2123860
    move-object v1, v1

    .line 2123861
    invoke-virtual {v1}, LX/9vT;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v1

    .line 2123862
    invoke-virtual {v2, v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a(LX/9uc;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v1

    move-object v1, v1

    .line 2123863
    invoke-virtual {p1, v0, v1}, LX/ETQ;->b(ILcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v1

    .line 2123864
    :cond_0
    :goto_1
    return v1

    .line 2123865
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2123866
    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    invoke-static {p0, v2, p2}, LX/ESx;->b(LX/ESx;LX/ETQ;Lcom/facebook/graphql/model/GraphQLActor;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2123867
    const/4 v1, 0x1

    goto :goto_1

    .line 2123868
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method
