.class public LX/ErM;
.super LX/7HQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HQ",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/ErI;

.field private final c:LX/Er3;


# direct methods
.method public constructor <init>(LX/ErI;LX/ErL;LX/Er3;LX/7Hg;LX/7Hl;LX/ErP;LX/7Hk;LX/7Ho;LX/2Sd;Ljava/lang/String;)V
    .locals 7
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172822
    move-object v1, p0

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move-object/from16 v6, p9

    invoke-direct/range {v1 .. v6}, LX/7HQ;-><init>(LX/7Hg;LX/7Hl;LX/7HW;LX/7Hk;LX/2Sd;)V

    .line 2172823
    iput-object p1, p0, LX/ErM;->b:LX/ErI;

    .line 2172824
    iput-object p3, p0, LX/ErM;->c:LX/Er3;

    .line 2172825
    move-object/from16 v0, p10

    invoke-virtual {p2, v0}, LX/ErL;->a(Ljava/lang/String;)LX/ErK;

    move-result-object v1

    .line 2172826
    iget-object v2, p0, LX/ErM;->c:LX/Er3;

    invoke-virtual {p4, v2}, LX/7Hg;->b(LX/7HS;)V

    .line 2172827
    invoke-virtual {p4, v1}, LX/7Hg;->a(LX/7HS;)V

    .line 2172828
    sget-object v2, LX/7HY;->REMOTE:LX/7HY;

    invoke-virtual {v1}, LX/7HT;->c()LX/7Hn;

    move-result-object v1

    invoke-virtual {p8, v2, v1}, LX/7Ho;->a(LX/7HY;LX/7Hn;)V

    .line 2172829
    return-void
.end method


# virtual methods
.method public final a(LX/7Hi;Ljava/lang/String;)LX/7HN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/7HN",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2172830
    iget-object v0, p0, LX/ErM;->b:LX/ErI;

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2172831
    iget-object v0, p0, LX/ErM;->c:LX/Er3;

    .line 2172832
    iput-object p1, v0, LX/Er3;->d:LX/0Px;

    .line 2172833
    return-void
.end method
