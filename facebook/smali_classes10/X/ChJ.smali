.class public LX/ChJ;
.super Landroid/view/ContextThemeWrapper;
.source ""

# interfaces
.implements LX/0Xn;


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/0jb;

.field private c:Landroid/content/pm/ApplicationInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/high16 v4, 0x400000

    const/4 v3, 0x0

    .line 1927870
    invoke-direct {p0, p1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 1927871
    new-instance v0, LX/0jb;

    invoke-direct {v0}, LX/0jb;-><init>()V

    iput-object v0, p0, LX/ChJ;->b:LX/0jb;

    .line 1927872
    const/4 v0, 0x0

    iput-object v0, p0, LX/ChJ;->c:Landroid/content/pm/ApplicationInfo;

    .line 1927873
    invoke-static {p0, p0}, LX/ChJ;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1927874
    invoke-super {p0}, Landroid/view/ContextThemeWrapper;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 1927875
    iget-object v1, p0, LX/ChJ;->a:LX/0Uh;

    const/16 v2, 0xb3

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, LX/Crz;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1927876
    new-instance v1, Landroid/content/pm/ApplicationInfo;

    invoke-direct {v1, v0}, Landroid/content/pm/ApplicationInfo;-><init>(Landroid/content/pm/ApplicationInfo;)V

    iput-object v1, p0, LX/ChJ;->c:Landroid/content/pm/ApplicationInfo;

    .line 1927877
    iget-object v0, p0, LX/ChJ;->c:Landroid/content/pm/ApplicationInfo;

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    or-int/2addr v1, v4

    iput v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    .line 1927878
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1927866
    invoke-static {p0}, LX/ChJ;->b(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v0

    .line 1927867
    if-eqz v0, :cond_0

    .line 1927868
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 1927869
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/ChJ;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, LX/ChJ;->a:LX/0Uh;

    return-void
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 1927863
    instance-of v0, p0, LX/ChJ;

    if-eqz v0, :cond_0

    .line 1927864
    check-cast p0, LX/ChJ;

    const-string v0, "loggingClass"

    invoke-virtual {p0, v0}, LX/ChJ;->getProperty(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 1927865
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .locals 1

    .prologue
    .line 1927857
    iget-object v0, p0, LX/ChJ;->c:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_0

    .line 1927858
    iget-object v0, p0, LX/ChJ;->c:Landroid/content/pm/ApplicationInfo;

    .line 1927859
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/view/ContextThemeWrapper;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public final getProperty(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1927862
    iget-object v0, p0, LX/ChJ;->b:LX/0jb;

    invoke-virtual {v0, p1}, LX/0jb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final setProperty(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1927860
    iget-object v0, p0, LX/ChJ;->b:LX/0jb;

    invoke-virtual {v0, p1, p2}, LX/0jb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1927861
    return-void
.end method
