.class public final LX/Eq3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V
    .locals 0

    .prologue
    .line 2170916
    iput-object p1, p0, LX/Eq3;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2170917
    iget-object v0, p0, LX/Eq3;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->x:LX/03V;

    sget-object v1, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->G:Ljava/lang/String;

    const-string v2, "Failed to fetch invitees restrictions"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2170918
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2170919
    move-object v1, v1

    .line 2170920
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2170921
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170922
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;

    .line 2170923
    iget-object v0, p0, LX/Eq3;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170924
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->n()Z

    move-result v1

    move v0, v1

    .line 2170925
    if-eqz v0, :cond_0

    .line 2170926
    :goto_0
    return-void

    .line 2170927
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2170928
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2170929
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2170930
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 2170931
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;

    .line 2170932
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->c(Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 2170933
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2170934
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2170935
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->d(Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 2170936
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel$EventUninvitableFriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2170937
    :cond_3
    iget-object v0, p0, LX/Eq3;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170938
    iput-object v2, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->K:Ljava/util/List;

    .line 2170939
    iget-object v0, p0, LX/Eq3;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170940
    iput-object v3, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->M:Ljava/util/List;

    .line 2170941
    if-eqz p1, :cond_4

    .line 2170942
    iget-object v0, p0, LX/Eq3;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsUninvitableFriendsAndInviteeLimitModel;->a()I

    move-result v1

    .line 2170943
    iput v1, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->Q:I

    .line 2170944
    :cond_4
    iget-object v0, p0, LX/Eq3;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->R:LX/8tE;

    invoke-virtual {v0, v2}, LX/8tE;->b(Ljava/util/Collection;)V

    .line 2170945
    iget-object v0, p0, LX/Eq3;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->R:LX/8tE;

    invoke-virtual {v0, v3}, LX/8tE;->a(Ljava/util/Collection;)V

    .line 2170946
    iget-object v0, p0, LX/Eq3;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-static {v0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->P(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    goto/16 :goto_0
.end method
