.class public LX/Enp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/En3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/En3;LX/0P1;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2167478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167479
    invoke-static {p1, p2}, LX/Enp;->a(LX/En3;LX/0P1;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2167480
    iput-object p1, p0, LX/Enp;->a:LX/En3;

    .line 2167481
    iput-object p2, p0, LX/Enp;->b:LX/0P1;

    .line 2167482
    iput-object p3, p0, LX/Enp;->c:Ljava/lang/String;

    .line 2167483
    iput-object p4, p0, LX/Enp;->d:Ljava/lang/String;

    .line 2167484
    return-void
.end method

.method public static a(LX/En3;LX/0P1;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2167485
    invoke-virtual {p0}, LX/En2;->c()I

    move-result v0

    invoke-virtual {p1}, LX/0P1;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 2167486
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 2167487
    :goto_1
    invoke-virtual {p0}, LX/En2;->c()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2167488
    invoke-virtual {p0, v1}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2167489
    invoke-virtual {p1, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167490
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2167491
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final b()LX/En3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/En3",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167492
    iget-object v0, p0, LX/Enp;->a:LX/En3;

    return-object v0
.end method
