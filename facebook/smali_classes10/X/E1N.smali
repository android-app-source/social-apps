.class public final LX/E1N;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Set;

.field public final synthetic b:Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 2069725
    iput-object p1, p0, LX/E1N;->b:Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;

    iput-object p2, p0, LX/E1N;->a:Ljava/util/Set;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2069726
    const/4 v0, 0x0

    .line 2069727
    iget-object v1, p0, LX/E1N;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0bV;

    .line 2069728
    if-nez v1, :cond_0

    .line 2069729
    invoke-interface {v0}, LX/0bV;->a()Ljava/lang/CharSequence;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2069730
    :cond_0
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    const-string v4, "\n\n"

    aput-object v4, v3, v1

    const/4 v1, 0x2

    invoke-interface {v0}, LX/0bV;->a()Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    move-object v1, v0

    .line 2069731
    goto :goto_0

    .line 2069732
    :cond_1
    return-object v1
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2069733
    check-cast p1, Ljava/lang/CharSequence;

    .line 2069734
    iget-object v0, p0, LX/E1N;->b:Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;

    iget-object v0, v0, Lcom/facebook/placetips/settings/ui/PlaceTipsEmployeeSettingsFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2069735
    return-void
.end method
