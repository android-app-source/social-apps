.class public LX/EQa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/EQa;


# instance fields
.field private final a:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2118804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2118805
    iput-object p1, p0, LX/EQa;->a:Landroid/content/ContentResolver;

    .line 2118806
    return-void
.end method

.method public static a(LX/0QB;)LX/EQa;
    .locals 4

    .prologue
    .line 2118812
    sget-object v0, LX/EQa;->b:LX/EQa;

    if-nez v0, :cond_1

    .line 2118813
    const-class v1, LX/EQa;

    monitor-enter v1

    .line 2118814
    :try_start_0
    sget-object v0, LX/EQa;->b:LX/EQa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2118815
    if-eqz v2, :cond_0

    .line 2118816
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2118817
    new-instance p0, LX/EQa;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-direct {p0, v3}, LX/EQa;-><init>(Landroid/content/ContentResolver;)V

    .line 2118818
    move-object v0, p0

    .line 2118819
    sput-object v0, LX/EQa;->b:LX/EQa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2118820
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2118821
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2118822
    :cond_1
    sget-object v0, LX/EQa;->b:LX/EQa;

    return-object v0

    .line 2118823
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2118824
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2118807
    iget-object v0, p0, LX/EQa;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/0PJ;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 2118808
    if-eqz v0, :cond_0

    .line 2118809
    iget-object v1, p0, LX/EQa;->a:Landroid/content/ContentResolver;

    sget-object v2, LX/0PJ;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2118810
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 2118811
    :cond_0
    return-void
.end method
