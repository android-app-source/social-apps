.class public final LX/EPm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CyM;

.field public final synthetic b:LX/395;

.field public final synthetic c:LX/CzL;

.field public final synthetic d:LX/Cxj;

.field public final synthetic e:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;LX/CyM;LX/395;LX/CzL;LX/Cxj;)V
    .locals 0

    .prologue
    .line 2117498
    iput-object p1, p0, LX/EPm;->e:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    iput-object p2, p0, LX/EPm;->a:LX/CyM;

    iput-object p3, p0, LX/EPm;->b:LX/395;

    iput-object p4, p0, LX/EPm;->c:LX/CzL;

    iput-object p5, p0, LX/EPm;->d:LX/Cxj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x63bc2ce8

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2117499
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0f8;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 2117500
    if-nez v0, :cond_0

    .line 2117501
    const v0, -0x22c4e158

    invoke-static {v2, v2, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2117502
    :goto_0
    return-void

    .line 2117503
    :cond_0
    invoke-interface {v0}, LX/0f8;->i()LX/0hE;

    move-result-object v1

    .line 2117504
    new-instance v0, LX/EPl;

    invoke-direct {v0, p0, p1}, LX/EPl;-><init>(LX/EPm;Landroid/view/View;)V

    invoke-interface {v1, v0}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 2117505
    instance-of v0, p1, LX/3FR;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 2117506
    check-cast v0, LX/3FR;

    .line 2117507
    invoke-interface {v0}, LX/3FR;->getSeekPosition()I

    move-result v0

    .line 2117508
    iget-object v2, p0, LX/EPm;->b:LX/395;

    invoke-virtual {v2, v0}, LX/395;->b(I)LX/395;

    .line 2117509
    iget-object v2, p0, LX/EPm;->b:LX/395;

    invoke-virtual {v2, v0}, LX/395;->a(I)LX/395;

    .line 2117510
    :cond_1
    instance-of v0, p1, LX/3FT;

    if-eqz v0, :cond_2

    .line 2117511
    iget-object v0, p0, LX/EPm;->b:LX/395;

    check-cast p1, LX/3FT;

    .line 2117512
    iput-object p1, v0, LX/395;->h:LX/3FT;

    .line 2117513
    :cond_2
    iget-object v0, p0, LX/EPm;->b:LX/395;

    iget-object v2, p0, LX/EPm;->e:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->e:LX/0ad;

    invoke-static {v2}, LX/7Ax;->b(LX/0ad;)Z

    move-result v2

    .line 2117514
    iput-boolean v2, v0, LX/395;->q:Z

    .line 2117515
    iget-object v0, p0, LX/EPm;->b:LX/395;

    const-string v2, "search_results_page"

    .line 2117516
    iput-object v2, v0, LX/395;->r:Ljava/lang/String;

    .line 2117517
    iget-object v0, p0, LX/EPm;->b:LX/395;

    invoke-interface {v1, v0}, LX/0hE;->a(LX/395;)V

    .line 2117518
    iget-object v0, p0, LX/EPm;->c:LX/CzL;

    .line 2117519
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2117520
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2117521
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2117522
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2117523
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    move-object v2, v0

    .line 2117524
    :goto_1
    iget-object v0, p0, LX/EPm;->e:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v4, p0, LX/EPm;->c:LX/CzL;

    iget-object v1, p0, LX/EPm;->d:LX/Cxj;

    check-cast v1, LX/CxP;

    invoke-static {v0, v4, v2, v1}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(LX/CvY;LX/CzL;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/CxP;)V

    .line 2117525
    const v0, -0x1f6003fe

    invoke-static {v0, v3}, LX/02F;->a(II)V

    goto :goto_0

    .line 2117526
    :cond_3
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_1
.end method
