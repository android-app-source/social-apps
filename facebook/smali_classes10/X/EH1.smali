.class public final enum LX/EH1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EH1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EH1;

.field public static final enum CONFERENCE:LX/EH1;

.field public static final enum CONFERENCE_VIDEO:LX/EH1;

.field public static final enum CONFERENCE_WITH_ADD_CALLEE:LX/EH1;

.field public static final enum VIDEO:LX/EH1;

.field public static final enum VOICE:LX/EH1;

.field public static final enum VOICEMAIL:LX/EH1;

.field public static final enum VOICE_WITH_ADD_CALLEE:LX/EH1;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2098574
    new-instance v0, LX/EH1;

    const-string v1, "VOICE"

    invoke-direct {v0, v1, v3}, LX/EH1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EH1;->VOICE:LX/EH1;

    .line 2098575
    new-instance v0, LX/EH1;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v4}, LX/EH1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EH1;->VIDEO:LX/EH1;

    .line 2098576
    new-instance v0, LX/EH1;

    const-string v1, "CONFERENCE"

    invoke-direct {v0, v1, v5}, LX/EH1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EH1;->CONFERENCE:LX/EH1;

    .line 2098577
    new-instance v0, LX/EH1;

    const-string v1, "VOICE_WITH_ADD_CALLEE"

    invoke-direct {v0, v1, v6}, LX/EH1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EH1;->VOICE_WITH_ADD_CALLEE:LX/EH1;

    .line 2098578
    new-instance v0, LX/EH1;

    const-string v1, "CONFERENCE_WITH_ADD_CALLEE"

    invoke-direct {v0, v1, v7}, LX/EH1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EH1;->CONFERENCE_WITH_ADD_CALLEE:LX/EH1;

    .line 2098579
    new-instance v0, LX/EH1;

    const-string v1, "CONFERENCE_VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EH1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EH1;->CONFERENCE_VIDEO:LX/EH1;

    .line 2098580
    new-instance v0, LX/EH1;

    const-string v1, "VOICEMAIL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EH1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EH1;->VOICEMAIL:LX/EH1;

    .line 2098581
    const/4 v0, 0x7

    new-array v0, v0, [LX/EH1;

    sget-object v1, LX/EH1;->VOICE:LX/EH1;

    aput-object v1, v0, v3

    sget-object v1, LX/EH1;->VIDEO:LX/EH1;

    aput-object v1, v0, v4

    sget-object v1, LX/EH1;->CONFERENCE:LX/EH1;

    aput-object v1, v0, v5

    sget-object v1, LX/EH1;->VOICE_WITH_ADD_CALLEE:LX/EH1;

    aput-object v1, v0, v6

    sget-object v1, LX/EH1;->CONFERENCE_WITH_ADD_CALLEE:LX/EH1;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EH1;->CONFERENCE_VIDEO:LX/EH1;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EH1;->VOICEMAIL:LX/EH1;

    aput-object v2, v0, v1

    sput-object v0, LX/EH1;->$VALUES:[LX/EH1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2098582
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EH1;
    .locals 1

    .prologue
    .line 2098573
    const-class v0, LX/EH1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EH1;

    return-object v0
.end method

.method public static values()[LX/EH1;
    .locals 1

    .prologue
    .line 2098572
    sget-object v0, LX/EH1;->$VALUES:[LX/EH1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EH1;

    return-object v0
.end method
