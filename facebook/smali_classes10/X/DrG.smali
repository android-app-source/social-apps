.class public LX/DrG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2049129
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    const-string v1, "UNSEEN_NOTIFICATIONS"

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    const-string v1, "FRIEND_REQUESTS"

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    const-string v1, "SEEN_NOTIFICATIONS"

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    const-string v1, "MIXED_STATE_NOTIFICATIONS"

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    const-string v1, "FALLBACK"

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    sput-object v0, LX/DrG;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2049130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
