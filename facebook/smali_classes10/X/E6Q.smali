.class public LX/E6Q;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:F

.field public final b:Lcom/facebook/fbui/glyph/GlyphView;

.field public final c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2080108
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2080109
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/E6Q;->a:F

    .line 2080110
    const v0, 0x7f031151

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2080111
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/E6Q;->setOrientation(I)V

    .line 2080112
    const v0, 0x7f0d28e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/E6Q;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2080113
    const v0, 0x7f0d28e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/E6Q;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2080114
    const v0, 0x7f0d28e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/E6Q;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2080115
    invoke-virtual {p0}, LX/E6Q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2080116
    const v1, 0x7f020dfd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2080117
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    const v2, 0x3f2aaaab

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 2080118
    const v2, 0x7f0b0060

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 2080119
    const v2, 0x7f0b1605

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    .line 2080120
    iget-object v1, p0, LX/E6Q;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2080121
    iget-object v1, p0, LX/E6Q;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2080122
    return-void
.end method
