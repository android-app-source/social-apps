.class public final LX/DhC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Ljava/io/File;",
        ">;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2030695
    iput-object p1, p0, LX/DhC;->a:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030696
    iput-object p2, p0, LX/DhC;->b:Ljava/lang/String;

    .line 2030697
    iput-object p3, p0, LX/DhC;->c:Landroid/net/Uri;

    .line 2030698
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2030691
    iget-object v0, p0, LX/DhC;->a:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DhA;

    iget-object v1, p0, LX/DhC;->b:Ljava/lang/String;

    .line 2030692
    invoke-static {v0}, LX/DhA;->a(LX/DhA;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object p0

    new-instance p2, LX/Dh9;

    invoke-direct {p2, p1}, LX/Dh9;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->storeToPath(Ljava/lang/String;Lcom/facebook/compactdisk/WriteCallback;)V

    .line 2030693
    invoke-virtual {v0, v1}, LX/DhA;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/io/File;

    move-object v0, p0

    .line 2030694
    return-object v0
.end method

.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2030690
    iget-object v0, p0, LX/DhC;->a:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;

    iget-object v0, v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;->b:LX/3AP;

    new-instance v1, LX/34X;

    iget-object v2, p0, LX/DhC;->c:Landroid/net/Uri;

    sget-object v3, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferDownloader;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, p0, v3}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    return-object v0
.end method
