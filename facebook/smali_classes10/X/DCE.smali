.class public LX/DCE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private final b:LX/189;

.field private final c:LX/0Xl;

.field public d:LX/0qq;

.field private e:LX/0g4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Yb;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/189;LX/0Xl;)V
    .locals 0
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1973845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1973846
    iput-object p1, p0, LX/DCE;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 1973847
    iput-object p2, p0, LX/DCE;->b:LX/189;

    .line 1973848
    iput-object p3, p0, LX/DCE;->c:LX/0Xl;

    .line 1973849
    return-void
.end method

.method public static a(LX/0QB;)LX/DCE;
    .locals 4

    .prologue
    .line 1973836
    new-instance v3, LX/DCE;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {p0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v1

    check-cast v1, LX/189;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v2

    check-cast v2, LX/0Xl;

    invoke-direct {v3, v0, v1, v2}, LX/DCE;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/189;LX/0Xl;)V

    .line 1973837
    move-object v0, v3

    .line 1973838
    return-object v0
.end method

.method public static b$redex0(LX/DCE;)V
    .locals 1

    .prologue
    .line 1973833
    iget-object v0, p0, LX/DCE;->e:LX/0g4;

    if-eqz v0, :cond_0

    .line 1973834
    iget-object v0, p0, LX/DCE;->e:LX/0g4;

    invoke-interface {v0}, LX/0g4;->d()V

    .line 1973835
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1973839
    iget-object v0, p0, LX/DCE;->f:LX/1Mv;

    if-eqz v0, :cond_0

    .line 1973840
    iget-object v0, p0, LX/DCE;->f:LX/1Mv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1973841
    const/4 v0, 0x0

    iput-object v0, p0, LX/DCE;->f:LX/1Mv;

    .line 1973842
    :cond_0
    iget-object v0, p0, LX/DCE;->g:LX/0Yb;

    if-eqz v0, :cond_1

    .line 1973843
    iget-object v0, p0, LX/DCE;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1973844
    :cond_1
    return-void
.end method

.method public final a(LX/0qq;LX/0g4;)V
    .locals 0
    .param p2    # LX/0g4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1973830
    iput-object p1, p0, LX/DCE;->d:LX/0qq;

    .line 1973831
    iput-object p2, p0, LX/DCE;->e:LX/0g4;

    .line 1973832
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 1973776
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1973777
    if-nez v0, :cond_1

    .line 1973778
    :cond_0
    :goto_0
    return-void

    .line 1973779
    :cond_1
    iget-object v1, p0, LX/DCE;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v1, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1973780
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1973781
    :cond_2
    iget-object v1, p0, LX/DCE;->d:LX/0qq;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0qq;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1973782
    if-nez v1, :cond_9

    .line 1973783
    const-class v1, LX/DCE;

    const-string v2, "Original story couldn\'t be retrieved. LegacyStoryApiId : %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    invoke-static {v1, v2, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1973784
    :cond_3
    const/4 v1, 0x0

    .line 1973785
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1973786
    iget-object v6, p0, LX/DCE;->d:LX/0qq;

    invoke-virtual {v6, v1}, LX/0qq;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 1973787
    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1973788
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1973789
    :cond_5
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1973790
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v2, v1

    .line 1973791
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1973792
    invoke-static {v2}, LX/2vB;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1973793
    iget-object v1, p0, LX/DCE;->b:LX/189;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v7}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ProductItemAttachment;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1973794
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 1973795
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1973796
    :goto_3
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1973797
    iget-object v3, p0, LX/DCE;->b:LX/189;

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v3, v1, v4}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/StoryVisibility;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1973798
    invoke-static {v1}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1973799
    iget-object v3, p0, LX/DCE;->d:LX/0qq;

    invoke-virtual {v3, v1}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1973800
    invoke-static {p0}, LX/DCE;->b$redex0(LX/DCE;)V

    .line 1973801
    new-instance v1, LX/DCD;

    invoke-direct {v1, p0, v2, v0}, LX/DCD;-><init>(LX/DCE;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/EditPostParams;)V

    .line 1973802
    iget-object v0, p0, LX/DCE;->g:LX/0Yb;

    if-eqz v0, :cond_6

    .line 1973803
    iget-object v0, p0, LX/DCE;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1973804
    :cond_6
    iget-object v0, p0, LX/DCE;->c:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-interface {v0, v2, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/DCE;->g:LX/0Yb;

    .line 1973805
    iget-object v0, p0, LX/DCE;->g:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    goto/16 :goto_0

    .line 1973806
    :cond_7
    iget-object v1, p0, LX/DCE;->b:LX/189;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v3

    .line 1973807
    invoke-static {v3}, LX/6XV;->a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 1973808
    invoke-static {v1, v2}, LX/189;->b(LX/189;Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v5

    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 1973809
    iput-object v4, v5, LX/23u;->k:LX/0Px;

    .line 1973810
    move-object v4, v5

    .line 1973811
    sget-object v5, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/StoryVisibility;->name()Ljava/lang/String;

    move-result-object v5

    .line 1973812
    iput-object v5, v4, LX/23u;->ab:Ljava/lang/String;

    .line 1973813
    move-object v4, v4

    .line 1973814
    iput-boolean v7, v4, LX/23u;->U:Z

    .line 1973815
    move-object v4, v4

    .line 1973816
    invoke-virtual {v4}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 1973817
    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    move-object v1, v4

    .line 1973818
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 1973819
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_3

    .line 1973820
    :cond_8
    iget-object v1, p0, LX/DCE;->b:LX/189;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getMessage()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 1973821
    invoke-static {v1, v2}, LX/189;->b(LX/189;Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v4

    .line 1973822
    iput-boolean v7, v4, LX/23u;->U:Z

    .line 1973823
    move-object v4, v4

    .line 1973824
    iput-object v3, v4, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1973825
    move-object v4, v4

    .line 1973826
    invoke-virtual {v4}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 1973827
    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    move-object v1, v4

    .line 1973828
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 1973829
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    goto/16 :goto_3

    :cond_9
    move-object v2, v1

    goto/16 :goto_2
.end method
