.class public final LX/DAu;
.super Landroid/bluetooth/BluetoothGattCallback;
.source ""


# instance fields
.field public final synthetic a:Landroid/bluetooth/BluetoothDevice;

.field public final synthetic b:LX/DAv;


# direct methods
.method public constructor <init>(LX/DAv;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 1971565
    iput-object p1, p0, LX/DAu;->b:LX/DAv;

    iput-object p2, p0, LX/DAu;->a:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 3

    .prologue
    .line 1971566
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    .line 1971567
    if-nez p3, :cond_0

    .line 1971568
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v0

    sget-object v1, LX/1Zm;->c:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1971569
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    .line 1971570
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 1971571
    iget-object v1, p0, LX/DAu;->b:LX/DAv;

    iget-object v1, v1, LX/DAv;->a:LX/1Zm;

    iget-object v1, v1, LX/1Zm;->k:Ljava/util/Set;

    iget-object v2, p0, LX/DAu;->a:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1971572
    iget-object v1, p0, LX/DAu;->b:LX/DAv;

    iget-object v1, v1, LX/DAv;->a:LX/1Zm;

    iget-object v1, v1, LX/1Zm;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1971573
    iget-object v1, p0, LX/DAu;->b:LX/DAv;

    iget-object v1, v1, LX/DAv;->a:LX/1Zm;

    invoke-static {v1, v0}, LX/1Zm;->a$redex0(LX/1Zm;Ljava/lang/String;)V

    .line 1971574
    :cond_0
    return-void
.end method

.method public final onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 1

    .prologue
    .line 1971575
    invoke-super {p0, p1, p2, p3}, Landroid/bluetooth/BluetoothGattCallback;->onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V

    .line 1971576
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 1971577
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    .line 1971578
    :cond_0
    return-void
.end method

.method public final onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 2

    .prologue
    .line 1971579
    invoke-super {p0, p1, p2}, Landroid/bluetooth/BluetoothGattCallback;->onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V

    .line 1971580
    if-nez p2, :cond_0

    .line 1971581
    sget-object v0, LX/1Zm;->b:Ljava/util/UUID;

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v0

    .line 1971582
    sget-object v1, LX/1Zm;->c:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    .line 1971583
    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGatt;->readCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    .line 1971584
    :cond_0
    return-void
.end method
