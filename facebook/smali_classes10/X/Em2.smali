.class public LX/Em2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Elu;


# instance fields
.field private final a:LX/Elm;


# direct methods
.method public constructor <init>(LX/Elm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2165024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2165025
    iput-object p1, p0, LX/Em2;->a:LX/Elm;

    .line 2165026
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2165027
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/photo.php"

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2165028
    const-string v0, "fbid"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2165029
    const-string v1, "set"

    invoke-virtual {p2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2165030
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2165031
    sget-object v2, LX/0ax;->bW:Ljava/lang/String;

    invoke-static {v2, v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2165032
    invoke-static {v0}, LX/Elm;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2165033
    :goto_0
    return-object v0

    .line 2165034
    :cond_0
    if-eqz v0, :cond_1

    .line 2165035
    sget-object v1, LX/0ax;->bV:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2165036
    invoke-static {v0}, LX/Elm;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 2165037
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
