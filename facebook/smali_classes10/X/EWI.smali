.class public LX/EWI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWH;


# static fields
.field public static final a:LX/EWI;


# instance fields
.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/EWM;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2129859
    new-instance v0, LX/EWI;

    .line 2129860
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2129861
    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/EWI;-><init>(LX/0Px;Z)V

    sput-object v0, LX/EWI;->a:LX/EWI;

    return-void
.end method

.method public constructor <init>(LX/0Px;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2129849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129850
    iput-object p1, p0, LX/EWI;->c:LX/0Px;

    .line 2129851
    iput-boolean p2, p0, LX/EWI;->d:Z

    .line 2129852
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 2129853
    iget-object v0, p0, LX/EWI;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/EWI;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2129854
    instance-of v4, v0, LX/EWM;

    if-eqz v4, :cond_0

    .line 2129855
    check-cast v0, LX/EWM;

    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2129856
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2129857
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/EWI;->b:LX/0Rf;

    .line 2129858
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2129862
    iget-object v0, p0, LX/EWI;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2129847
    iget-object v0, p0, LX/EWI;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2129848
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "items"

    iget-object v2, p0, LX/EWI;->c:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "hasHeaderRow"

    iget-boolean v2, p0, LX/EWI;->d:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
