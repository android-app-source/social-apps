.class public LX/Ea9;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:[I

.field public static b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 2140855
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/Ea9;->a:[I

    .line 2140856
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, LX/Ea9;->b:[I

    return-void

    nop

    :array_0
    .array-data 4
        -0xa6874a
        0xd37285
        -0xea9143
        0x6a0a0f
        0x1c029
        -0x861768
        -0x5fc344
        -0x318e67
        -0x11d4900
        -0xb7f24c
    .end array-data

    .line 2140857
    :array_1
    .array-data 4
        -0x1f15f50
        -0x79362d
        0x8f189e
        0x35697f
        0xbd0c60
        -0x42859
        -0x17fb361
        -0x1e9a96
        0x4fc1e
        0xae0c92
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2140858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/EaI;[B)I
    .locals 7

    .prologue
    const/16 v4, 0xa

    .line 2140859
    new-array v0, v4, [I

    .line 2140860
    new-array v1, v4, [I

    .line 2140861
    new-array v2, v4, [I

    .line 2140862
    new-array v3, v4, [I

    .line 2140863
    new-array v4, v4, [I

    .line 2140864
    iget-object v5, p0, LX/EaI;->b:[I

    invoke-static {v5, p1}, LX/EZu;->a([I[B)V

    .line 2140865
    iget-object v5, p0, LX/EaI;->c:[I

    invoke-static {v5}, LX/EZp;->a([I)V

    .line 2140866
    iget-object v5, p0, LX/EaI;->b:[I

    invoke-static {v0, v5}, LX/Ea3;->a([I[I)V

    .line 2140867
    sget-object v5, LX/Ea9;->a:[I

    invoke-static {v1, v0, v5}, LX/EZz;->a([I[I[I)V

    .line 2140868
    iget-object v5, p0, LX/EaI;->c:[I

    invoke-static {v0, v0, v5}, LX/Ea4;->a([I[I[I)V

    .line 2140869
    iget-object v5, p0, LX/EaI;->c:[I

    invoke-static {v1, v1, v5}, LX/EZq;->a([I[I[I)V

    .line 2140870
    invoke-static {v2, v1}, LX/Ea3;->a([I[I)V

    .line 2140871
    invoke-static {v2, v2, v1}, LX/EZz;->a([I[I[I)V

    .line 2140872
    iget-object v5, p0, LX/EaI;->a:[I

    invoke-static {v5, v2}, LX/Ea3;->a([I[I)V

    .line 2140873
    iget-object v5, p0, LX/EaI;->a:[I

    iget-object v6, p0, LX/EaI;->a:[I

    invoke-static {v5, v6, v1}, LX/EZz;->a([I[I[I)V

    .line 2140874
    iget-object v5, p0, LX/EaI;->a:[I

    iget-object v6, p0, LX/EaI;->a:[I

    invoke-static {v5, v6, v0}, LX/EZz;->a([I[I[I)V

    .line 2140875
    iget-object v5, p0, LX/EaI;->a:[I

    iget-object v6, p0, LX/EaI;->a:[I

    invoke-static {v5, v6}, LX/Ea1;->a([I[I)V

    .line 2140876
    iget-object v5, p0, LX/EaI;->a:[I

    iget-object v6, p0, LX/EaI;->a:[I

    invoke-static {v5, v6, v2}, LX/EZz;->a([I[I[I)V

    .line 2140877
    iget-object v2, p0, LX/EaI;->a:[I

    iget-object v5, p0, LX/EaI;->a:[I

    invoke-static {v2, v5, v0}, LX/EZz;->a([I[I[I)V

    .line 2140878
    iget-object v2, p0, LX/EaI;->a:[I

    invoke-static {v3, v2}, LX/Ea3;->a([I[I)V

    .line 2140879
    invoke-static {v3, v3, v1}, LX/EZz;->a([I[I[I)V

    .line 2140880
    invoke-static {v4, v3, v0}, LX/Ea4;->a([I[I[I)V

    .line 2140881
    invoke-static {v4}, LX/EZx;->a([I)I

    move-result v1

    if-eqz v1, :cond_1

    .line 2140882
    invoke-static {v4, v3, v0}, LX/EZq;->a([I[I[I)V

    .line 2140883
    invoke-static {v4}, LX/EZx;->a([I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    .line 2140884
    :goto_0
    return v0

    .line 2140885
    :cond_0
    iget-object v0, p0, LX/EaI;->a:[I

    iget-object v1, p0, LX/EaI;->a:[I

    sget-object v2, LX/Ea9;->b:[I

    invoke-static {v0, v1, v2}, LX/EZz;->a([I[I[I)V

    .line 2140886
    :cond_1
    iget-object v0, p0, LX/EaI;->a:[I

    invoke-static {v0}, LX/EZw;->a([I)I

    move-result v0

    const/16 v1, 0x1f

    aget-byte v1, p1, v1

    ushr-int/lit8 v1, v1, 0x7

    and-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2140887
    iget-object v0, p0, LX/EaI;->a:[I

    iget-object v1, p0, LX/EaI;->a:[I

    invoke-static {v0, v1}, LX/Ea0;->a([I[I)V

    .line 2140888
    :cond_2
    iget-object v0, p0, LX/EaI;->d:[I

    iget-object v1, p0, LX/EaI;->a:[I

    iget-object v2, p0, LX/EaI;->b:[I

    invoke-static {v0, v1, v2}, LX/EZz;->a([I[I[I)V

    .line 2140889
    const/4 v0, 0x0

    goto :goto_0
.end method
