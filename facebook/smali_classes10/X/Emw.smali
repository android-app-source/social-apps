.class public final enum LX/Emw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Emw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Emw;

.field public static final enum ACTIVITY_CREATE:LX/Emw;

.field public static final enum FRAGMENT_CREATE:LX/Emw;

.field public static final enum INITIAL_CARD_ACTION_BAR:LX/Emw;

.field public static final enum INITIAL_CARD_CONTEXT_ROWS:LX/Emw;

.field public static final enum INITIAL_CARD_COVER_PHOTO:LX/Emw;

.field public static final enum INITIAL_CARD_PROFILE_PICTURE:LX/Emw;

.field public static final enum INITIAL_ENTITIES_FETCHED:LX/Emw;

.field public static final enum INTRO_ANIMATION:LX/Emw;

.field public static final enum LAUNCH_ENTITY_CARD:LX/Emw;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2166368
    new-instance v0, LX/Emw;

    const-string v1, "LAUNCH_ENTITY_CARD"

    const-string v2, "ec_launch"

    invoke-direct {v0, v1, v4, v2}, LX/Emw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emw;->LAUNCH_ENTITY_CARD:LX/Emw;

    .line 2166369
    new-instance v0, LX/Emw;

    const-string v1, "ACTIVITY_CREATE"

    const-string v2, "ec_activity_create"

    invoke-direct {v0, v1, v5, v2}, LX/Emw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emw;->ACTIVITY_CREATE:LX/Emw;

    .line 2166370
    new-instance v0, LX/Emw;

    const-string v1, "FRAGMENT_CREATE"

    const-string v2, "ec_fragment_create"

    invoke-direct {v0, v1, v6, v2}, LX/Emw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emw;->FRAGMENT_CREATE:LX/Emw;

    .line 2166371
    new-instance v0, LX/Emw;

    const-string v1, "INTRO_ANIMATION"

    const-string v2, "ec_intro_animation"

    invoke-direct {v0, v1, v7, v2}, LX/Emw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emw;->INTRO_ANIMATION:LX/Emw;

    .line 2166372
    new-instance v0, LX/Emw;

    const-string v1, "INITIAL_ENTITIES_FETCHED"

    const-string v2, "ec_initial_entities_fetched"

    invoke-direct {v0, v1, v8, v2}, LX/Emw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emw;->INITIAL_ENTITIES_FETCHED:LX/Emw;

    .line 2166373
    new-instance v0, LX/Emw;

    const-string v1, "INITIAL_CARD_COVER_PHOTO"

    const/4 v2, 0x5

    const-string v3, "ec_initial_card_cover_photo_configured"

    invoke-direct {v0, v1, v2, v3}, LX/Emw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emw;->INITIAL_CARD_COVER_PHOTO:LX/Emw;

    .line 2166374
    new-instance v0, LX/Emw;

    const-string v1, "INITIAL_CARD_PROFILE_PICTURE"

    const/4 v2, 0x6

    const-string v3, "ec_initial_card_profile_picture_configured"

    invoke-direct {v0, v1, v2, v3}, LX/Emw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emw;->INITIAL_CARD_PROFILE_PICTURE:LX/Emw;

    .line 2166375
    new-instance v0, LX/Emw;

    const-string v1, "INITIAL_CARD_ACTION_BAR"

    const/4 v2, 0x7

    const-string v3, "ec_initial_card_action_bar_configured"

    invoke-direct {v0, v1, v2, v3}, LX/Emw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emw;->INITIAL_CARD_ACTION_BAR:LX/Emw;

    .line 2166376
    new-instance v0, LX/Emw;

    const-string v1, "INITIAL_CARD_CONTEXT_ROWS"

    const/16 v2, 0x8

    const-string v3, "ec_initial_card_context_rows_configured"

    invoke-direct {v0, v1, v2, v3}, LX/Emw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emw;->INITIAL_CARD_CONTEXT_ROWS:LX/Emw;

    .line 2166377
    const/16 v0, 0x9

    new-array v0, v0, [LX/Emw;

    sget-object v1, LX/Emw;->LAUNCH_ENTITY_CARD:LX/Emw;

    aput-object v1, v0, v4

    sget-object v1, LX/Emw;->ACTIVITY_CREATE:LX/Emw;

    aput-object v1, v0, v5

    sget-object v1, LX/Emw;->FRAGMENT_CREATE:LX/Emw;

    aput-object v1, v0, v6

    sget-object v1, LX/Emw;->INTRO_ANIMATION:LX/Emw;

    aput-object v1, v0, v7

    sget-object v1, LX/Emw;->INITIAL_ENTITIES_FETCHED:LX/Emw;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Emw;->INITIAL_CARD_COVER_PHOTO:LX/Emw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Emw;->INITIAL_CARD_PROFILE_PICTURE:LX/Emw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Emw;->INITIAL_CARD_ACTION_BAR:LX/Emw;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Emw;->INITIAL_CARD_CONTEXT_ROWS:LX/Emw;

    aput-object v2, v0, v1

    sput-object v0, LX/Emw;->$VALUES:[LX/Emw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2166365
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2166366
    iput-object p3, p0, LX/Emw;->name:Ljava/lang/String;

    .line 2166367
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Emw;
    .locals 1

    .prologue
    .line 2166363
    const-class v0, LX/Emw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Emw;

    return-object v0
.end method

.method public static values()[LX/Emw;
    .locals 1

    .prologue
    .line 2166364
    sget-object v0, LX/Emw;->$VALUES:[LX/Emw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Emw;

    return-object v0
.end method
