.class public LX/ETB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile K:LX/ETB;

.field public static final a:Ljava/lang/String;


# instance fields
.field private A:J

.field public B:LX/ET8;

.field private final C:LX/AjN;

.field private final D:LX/BV4;

.field private final E:LX/BV4;

.field private final F:LX/ET2;

.field private final G:LX/ET3;

.field public H:Z

.field public I:LX/ETU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cfw;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ESx;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ESy;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ETN;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2xj;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Jc;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeDataController$OnDataChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/BV5;

.field public final n:LX/BV5;

.field public final o:LX/AjP;

.field private final p:LX/ESu;

.field public final q:LX/0xX;

.field public final r:LX/19j;

.field public final s:LX/ETG;

.field public final t:LX/ETQ;

.field private final u:LX/ESs;

.field private final v:LX/3AW;

.field public final w:LX/ETH;

.field public final x:LX/0rf;

.field private final y:LX/2zC;

.field private final z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2124208
    const-class v0, LX/ETB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ETB;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/BV6;LX/AjP;LX/ESu;LX/ETG;LX/ETQ;LX/3AW;LX/0xX;LX/19j;LX/0rb;LX/ETH;LX/0V6;)V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2124209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2124210
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124211
    iput-object v0, p0, LX/ETB;->b:LX/0Ot;

    .line 2124212
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124213
    iput-object v0, p0, LX/ETB;->c:LX/0Ot;

    .line 2124214
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124215
    iput-object v0, p0, LX/ETB;->d:LX/0Ot;

    .line 2124216
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124217
    iput-object v0, p0, LX/ETB;->e:LX/0Ot;

    .line 2124218
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124219
    iput-object v0, p0, LX/ETB;->f:LX/0Ot;

    .line 2124220
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124221
    iput-object v0, p0, LX/ETB;->g:LX/0Ot;

    .line 2124222
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124223
    iput-object v0, p0, LX/ETB;->h:LX/0Ot;

    .line 2124224
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124225
    iput-object v0, p0, LX/ETB;->i:LX/0Ot;

    .line 2124226
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124227
    iput-object v0, p0, LX/ETB;->j:LX/0Ot;

    .line 2124228
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2124229
    iput-object v0, p0, LX/ETB;->k:LX/0Ot;

    .line 2124230
    new-instance v0, LX/ET9;

    invoke-direct {v0, p0}, LX/ET9;-><init>(LX/ETB;)V

    iput-object v0, p0, LX/ETB;->u:LX/ESs;

    .line 2124231
    new-instance v0, LX/ET5;

    invoke-direct {v0, p0}, LX/ET5;-><init>(LX/ETB;)V

    iput-object v0, p0, LX/ETB;->x:LX/0rf;

    .line 2124232
    new-instance v0, LX/2zC;

    invoke-direct {v0, p0}, LX/2zC;-><init>(LX/ETB;)V

    iput-object v0, p0, LX/ETB;->y:LX/2zC;

    .line 2124233
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/ETB;->A:J

    .line 2124234
    new-instance v0, LX/ET0;

    invoke-direct {v0, p0}, LX/ET0;-><init>(LX/ETB;)V

    iput-object v0, p0, LX/ETB;->C:LX/AjN;

    .line 2124235
    new-instance v0, LX/31t;

    invoke-direct {v0, p0}, LX/31t;-><init>(LX/ETB;)V

    iput-object v0, p0, LX/ETB;->D:LX/BV4;

    .line 2124236
    new-instance v0, LX/ET1;

    invoke-direct {v0, p0}, LX/ET1;-><init>(LX/ETB;)V

    iput-object v0, p0, LX/ETB;->E:LX/BV4;

    .line 2124237
    new-instance v0, LX/ET2;

    invoke-direct {v0, p0}, LX/ET2;-><init>(LX/ETB;)V

    iput-object v0, p0, LX/ETB;->F:LX/ET2;

    .line 2124238
    new-instance v0, LX/ET3;

    invoke-direct {v0, p0}, LX/ET3;-><init>(LX/ETB;)V

    iput-object v0, p0, LX/ETB;->G:LX/ET3;

    .line 2124239
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/BV6;->a(Z)LX/BV5;

    move-result-object v0

    iput-object v0, p0, LX/ETB;->m:LX/BV5;

    .line 2124240
    invoke-virtual {p1, v2}, LX/BV6;->a(Z)LX/BV5;

    move-result-object v0

    iput-object v0, p0, LX/ETB;->n:LX/BV5;

    .line 2124241
    iput-object p2, p0, LX/ETB;->o:LX/AjP;

    .line 2124242
    iput-object p3, p0, LX/ETB;->p:LX/ESu;

    .line 2124243
    iput-object p7, p0, LX/ETB;->q:LX/0xX;

    .line 2124244
    iput-object p8, p0, LX/ETB;->r:LX/19j;

    .line 2124245
    iput-object p4, p0, LX/ETB;->s:LX/ETG;

    .line 2124246
    iput-object p6, p0, LX/ETB;->v:LX/3AW;

    .line 2124247
    iput-object p5, p0, LX/ETB;->t:LX/ETQ;

    .line 2124248
    iput-object p10, p0, LX/ETB;->w:LX/ETH;

    .line 2124249
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/ETB;->J:Ljava/util/Queue;

    .line 2124250
    iget-object v0, p0, LX/ETB;->o:LX/AjP;

    iget-object v1, p0, LX/ETB;->C:LX/AjN;

    .line 2124251
    iput-object v1, v0, LX/AjP;->c:LX/AjN;

    .line 2124252
    iget-object v0, p0, LX/ETB;->m:LX/BV5;

    iget-object v1, p0, LX/ETB;->D:LX/BV4;

    .line 2124253
    iput-object v1, v0, LX/BV5;->e:LX/BV4;

    .line 2124254
    iget-object v0, p0, LX/ETB;->n:LX/BV5;

    iget-object v1, p0, LX/ETB;->E:LX/BV4;

    .line 2124255
    iput-object v1, v0, LX/BV5;->e:LX/BV4;

    .line 2124256
    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    iget-object v1, p0, LX/ETB;->u:LX/ESs;

    invoke-virtual {v0, v1}, LX/ETQ;->a(LX/ESs;)V

    .line 2124257
    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    iget-object v1, p0, LX/ETB;->p:LX/ESu;

    invoke-virtual {v0, v1}, LX/ETQ;->a(LX/ESs;)V

    .line 2124258
    iget-object v0, p0, LX/ETB;->s:LX/ETG;

    iget-object v1, p0, LX/ETB;->F:LX/ET2;

    .line 2124259
    iget-object v2, v0, LX/ETG;->q:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2124260
    iget-object v2, v0, LX/ETG;->q:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2124261
    :cond_0
    iget-object v0, p0, LX/ETB;->v:LX/3AW;

    iget-object v1, p0, LX/ETB;->G:LX/ET3;

    .line 2124262
    iget-object v2, v0, LX/3AW;->m:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2124263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ETB;->l:Ljava/util/List;

    .line 2124264
    iget-object v0, p0, LX/ETB;->q:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2124265
    iget-object v0, p0, LX/ETB;->x:LX/0rf;

    invoke-interface {p9, v0}, LX/0rb;->a(LX/0rf;)V

    .line 2124266
    :cond_1
    iget-object v0, p0, LX/ETB;->q:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2124267
    iget-object v0, p0, LX/ETB;->w:LX/ETH;

    iget-object v1, p0, LX/ETB;->y:LX/2zC;

    invoke-virtual {v0, v1}, LX/ETH;->a(LX/3AY;)V

    .line 2124268
    :cond_2
    invoke-direct {p0, p11}, LX/ETB;->a(LX/0V6;)I

    move-result v0

    iput v0, p0, LX/ETB;->z:I

    .line 2124269
    return-void
.end method

.method private a(LX/0V6;)I
    .locals 2

    .prologue
    .line 2124270
    invoke-virtual {p1}, LX/0V6;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ETB;->q:LX/0xX;

    .line 2124271
    iget-object v1, v0, LX/0xX;->c:LX/0ad;

    sget-short p0, LX/0xY;->a:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 2124272
    if-eqz v0, :cond_0

    .line 2124273
    const/16 v0, 0x1e

    .line 2124274
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/ETB;
    .locals 15

    .prologue
    .line 2124275
    sget-object v0, LX/ETB;->K:LX/ETB;

    if-nez v0, :cond_1

    .line 2124276
    const-class v1, LX/ETB;

    monitor-enter v1

    .line 2124277
    :try_start_0
    sget-object v0, LX/ETB;->K:LX/ETB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2124278
    if-eqz v2, :cond_0

    .line 2124279
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2124280
    new-instance v3, LX/ETB;

    const-class v4, LX/BV6;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/BV6;

    invoke-static {v0}, LX/AjP;->b(LX/0QB;)LX/AjP;

    move-result-object v5

    check-cast v5, LX/AjP;

    .line 2124281
    new-instance v7, LX/ESu;

    .line 2124282
    new-instance v10, LX/ETT;

    invoke-static {v0}, LX/14v;->a(LX/0QB;)LX/14v;

    move-result-object v6

    check-cast v6, LX/14v;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {v0}, LX/3Hf;->a(LX/0QB;)LX/3Hf;

    move-result-object v9

    check-cast v9, LX/3Hf;

    invoke-direct {v10, v6, v8, v9}, LX/ETT;-><init>(LX/14v;LX/0Sh;LX/3Hf;)V

    .line 2124283
    move-object v6, v10

    .line 2124284
    check-cast v6, LX/ETT;

    invoke-direct {v7, v6}, LX/ESu;-><init>(LX/ETT;)V

    .line 2124285
    move-object v6, v7

    .line 2124286
    check-cast v6, LX/ESu;

    invoke-static {v0}, LX/ETG;->b(LX/0QB;)LX/ETG;

    move-result-object v7

    check-cast v7, LX/ETG;

    .line 2124287
    new-instance v8, LX/ETQ;

    invoke-direct {v8}, LX/ETQ;-><init>()V

    .line 2124288
    move-object v8, v8

    .line 2124289
    move-object v8, v8

    .line 2124290
    check-cast v8, LX/ETQ;

    invoke-static {v0}, LX/3AW;->a(LX/0QB;)LX/3AW;

    move-result-object v9

    check-cast v9, LX/3AW;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v10

    check-cast v10, LX/0xX;

    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v11

    check-cast v11, LX/19j;

    invoke-static {v0}, LX/0ra;->a(LX/0QB;)LX/0ra;

    move-result-object v12

    check-cast v12, LX/0rb;

    .line 2124291
    new-instance v14, LX/ETH;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v13

    check-cast v13, Landroid/os/Handler;

    invoke-direct {v14, v13}, LX/ETH;-><init>(Landroid/os/Handler;)V

    .line 2124292
    move-object v13, v14

    .line 2124293
    check-cast v13, LX/ETH;

    invoke-static {v0}, LX/0V4;->a(LX/0QB;)LX/0V6;

    move-result-object v14

    check-cast v14, LX/0V6;

    invoke-direct/range {v3 .. v14}, LX/ETB;-><init>(LX/BV6;LX/AjP;LX/ESu;LX/ETG;LX/ETQ;LX/3AW;LX/0xX;LX/19j;LX/0rb;LX/ETH;LX/0V6;)V

    .line 2124294
    const/16 v4, 0xafc

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2db

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x309c

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x3808

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x3809

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x380d

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1387

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x37b4

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3810

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x271

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    .line 2124295
    iput-object v4, v3, LX/ETB;->b:LX/0Ot;

    iput-object v5, v3, LX/ETB;->c:LX/0Ot;

    iput-object v6, v3, LX/ETB;->d:LX/0Ot;

    iput-object v7, v3, LX/ETB;->e:LX/0Ot;

    iput-object v8, v3, LX/ETB;->f:LX/0Ot;

    iput-object v9, v3, LX/ETB;->g:LX/0Ot;

    iput-object v10, v3, LX/ETB;->h:LX/0Ot;

    iput-object v11, v3, LX/ETB;->i:LX/0Ot;

    iput-object v12, v3, LX/ETB;->j:LX/0Ot;

    iput-object v13, v3, LX/ETB;->k:LX/0Ot;

    .line 2124296
    move-object v0, v3

    .line 2124297
    sput-object v0, LX/ETB;->K:LX/ETB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2124298
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2124299
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2124300
    :cond_1
    sget-object v0, LX/ETB;->K:LX/ETB;

    return-object v0

    .line 2124301
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2124302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/2rJ;I)LX/ETD;
    .locals 1

    .prologue
    .line 2124303
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2124304
    new-instance v0, LX/ETC;

    invoke-direct {v0}, LX/ETC;-><init>()V

    .line 2124305
    iput-object p0, v0, LX/ETC;->a:LX/2rJ;

    .line 2124306
    move-object v0, v0

    .line 2124307
    iput p1, v0, LX/ETC;->b:I

    .line 2124308
    move-object v0, v0

    .line 2124309
    invoke-virtual {v0}, LX/ETC;->a()LX/ETD;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/ETB;ILjava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 2124310
    iget-object v9, p0, LX/ETB;->v:LX/3AW;

    new-instance v0, LX/7Qo;

    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    sget-object v2, LX/0JP;->NEXT_UNITS:LX/0JP;

    iget-object v3, p0, LX/ETB;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Jc;

    .line 2124311
    iget-object v4, v3, LX/0Jc;->c:LX/0J9;

    move-object v3, v4

    .line 2124312
    iget-object v4, p0, LX/ETB;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iget-wide v6, p0, LX/ETB;->A:J

    sub-long/2addr v4, v6

    move v6, p1

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, LX/7Qo;-><init>(LX/04D;LX/0JP;LX/0J9;JILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, LX/3AW;->a(LX/7Qf;)V

    .line 2124313
    return-void
.end method

.method public static a(Ljava/util/Map;Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2124314
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2124315
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2124316
    :goto_0
    return-void

    .line 2124317
    :cond_0
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(LX/ETB;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;)Z
    .locals 5

    .prologue
    .line 2124318
    iget-object v0, p0, LX/ETB;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfw;

    .line 2124319
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2124320
    const-string v1, "EMPTY_SUB_COMPONENTS"

    .line 2124321
    :goto_0
    move-object v0, v1

    .line 2124322
    const-string v1, "SUCCESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 2124323
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;

    .line 2124324
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v1

    .line 2124325
    if-nez v1, :cond_1

    .line 2124326
    const-string v1, "ERROR_INVALID_COMPONENT"

    goto :goto_0

    .line 2124327
    :cond_1
    iget-object p0, v0, LX/Cfw;->b:LX/1vo;

    invoke-virtual {p0, v1}, LX/1vo;->a(LX/9oK;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2124328
    const-string v1, "UNSUPPORTED_COMPONENT_STYLE"

    goto :goto_0

    .line 2124329
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2124330
    :cond_3
    const-string v1, "SUCCESS"

    goto :goto_0
.end method

.method private static b(LX/ETB;LX/2rJ;)I
    .locals 2

    .prologue
    .line 2124378
    sget-object v0, LX/2rJ;->PAGINATION:LX/2rJ;

    invoke-virtual {v0, p1}, LX/2rJ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ETB;->q:LX/0xX;

    .line 2124379
    iget-object v1, v0, LX/0xX;->c:LX/0ad;

    sget p0, LX/0xY;->y:I

    const/4 p1, 0x2

    invoke-interface {v1, p0, p1}, LX/0ad;->a(II)I

    move-result v1

    move v0, v1

    .line 2124380
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/ETB;->q:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->e()I

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;
    .locals 1
    .param p0    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;",
            ">;)",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionPaginatedSubComponents;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2124331
    if-eqz p0, :cond_0

    .line 2124332
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2124333
    if-nez v0, :cond_1

    .line 2124334
    :cond_0
    const/4 v0, 0x0

    .line 2124335
    :goto_0
    return-object v0

    .line 2124336
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2124337
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2124338
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2124339
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_V_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2124340
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object p0

    invoke-virtual {v4, p0}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->v()I

    move-result v4

    const/4 p0, -0x1

    if-ne v4, p0, :cond_2

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 2124341
    if-eqz v4, :cond_0

    .line 2124342
    :goto_2
    return-object v0

    .line 2124343
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2124344
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static b(LX/0us;)Z
    .locals 1
    .param p0    # LX/0us;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2124345
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2124346
    iget-object v0, p0, LX/ETB;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfw;

    invoke-virtual {v0, p1}, LX/Cfw;->a(LX/9qX;)LX/Cfx;

    move-result-object v0

    .line 2124347
    iget-object v1, v0, LX/Cfx;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2124348
    const-string v1, "SUCCESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2124349
    const/4 v0, 0x0

    .line 2124350
    :goto_0
    return-object v0

    .line 2124351
    :cond_0
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 2124352
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->r()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    .line 2124353
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v6

    .line 2124354
    sget-object v7, LX/ESv;->a:[I

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result p0

    aget v7, v7, p0

    packed-switch v7, :pswitch_data_0

    .line 2124355
    new-instance v7, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-direct {v7, v0, v5, v6}, Lcom/facebook/video/videohome/data/VideoHomeItem;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    :goto_2
    move-object v0, v7

    .line 2124356
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2124357
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2124358
    :cond_1
    move-object v0, v2

    .line 2124359
    goto :goto_0

    .line 2124360
    :pswitch_0
    invoke-static {v5, v6, v0}, LX/ESw;->d(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    goto :goto_2

    .line 2124361
    :pswitch_1
    invoke-static {v0}, LX/Cfu;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/0Px;

    move-result-object v7

    .line 2124362
    if-nez v7, :cond_2

    .line 2124363
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    .line 2124364
    :goto_3
    move-object v7, v7

    .line 2124365
    goto :goto_2

    .line 2124366
    :pswitch_2
    invoke-static {v5, v6, v0}, LX/ESw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Ljava/util/List;

    move-result-object v7

    goto :goto_2

    :cond_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const/4 p0, 0x1

    if-le v7, p0, :cond_3

    invoke-static {v5, v6, v0}, LX/ESw;->d(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    goto :goto_3

    :cond_3
    invoke-static {v5, v6, v0}, LX/ESw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Ljava/util/List;

    move-result-object v7

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static q(LX/ETB;)Z
    .locals 2

    .prologue
    .line 2124367
    iget v0, p0, LX/ETB;->z:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    invoke-virtual {v0}, LX/ETQ;->size()I

    move-result v0

    iget v1, p0, LX/ETB;->z:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(LX/ETB;)V
    .locals 1

    .prologue
    .line 2124368
    invoke-virtual {p0}, LX/ETB;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/ETB;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2124369
    :cond_0
    :goto_0
    return-void

    .line 2124370
    :cond_1
    iget-object v0, p0, LX/ETB;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ETB;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xj;

    invoke-virtual {v0}, LX/2xj;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2124371
    :cond_2
    invoke-static {p0}, LX/ETB;->y(LX/ETB;)V

    goto :goto_0
.end method

.method public static u(LX/ETB;)V
    .locals 2

    .prologue
    .line 2124372
    iget-object v0, p0, LX/ETB;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EUW;

    .line 2124373
    iget-object p0, v0, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object p0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    if-eqz p0, :cond_0

    .line 2124374
    iget-object p0, v0, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object p0, p0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    invoke-interface {p0}, LX/1OP;->notifyDataSetChanged()V

    .line 2124375
    iget-object p0, v0, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-virtual {p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k()V

    .line 2124376
    :cond_0
    goto :goto_0

    .line 2124377
    :cond_1
    return-void
.end method

.method private v()V
    .locals 3

    .prologue
    .line 2124200
    iget-object v0, p0, LX/ETB;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EUW;

    .line 2124201
    iget-object v2, v0, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v2, v2, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    if-eqz v2, :cond_0

    .line 2124202
    iget-object v2, v0, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v2, v2, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    invoke-interface {v2}, LX/1OP;->notifyDataSetChanged()V

    .line 2124203
    iget-object v2, v0, LX/EUW;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    const/4 p0, 0x0

    invoke-static {v2, p0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->c(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Z)V

    .line 2124204
    :cond_0
    goto :goto_0

    .line 2124205
    :cond_1
    return-void
.end method

.method public static y(LX/ETB;)V
    .locals 2

    .prologue
    .line 2124206
    iget-object v0, p0, LX/ETB;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    new-instance v1, Lcom/facebook/video/videohome/data/VideoHomeDataController$7;

    invoke-direct {v1, p0}, Lcom/facebook/video/videohome/data/VideoHomeDataController$7;-><init>(LX/ETB;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2124207
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 8

    .prologue
    .line 2124013
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/ETB;->q(LX/ETB;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 2124014
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2124015
    :cond_1
    :try_start_1
    const/4 v0, 0x1

    .line 2124016
    iget-boolean v1, p0, LX/ETB;->H:Z

    if-eqz v1, :cond_3

    .line 2124017
    :goto_1
    move v0, v0

    .line 2124018
    iput-boolean v0, p0, LX/ETB;->H:Z

    .line 2124019
    iget-boolean v0, p0, LX/ETB;->H:Z

    if-nez v0, :cond_0

    .line 2124020
    iget-object v0, p0, LX/ETB;->J:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2124021
    invoke-virtual {p0}, LX/ETB;->m()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2124022
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2124023
    :cond_2
    :try_start_2
    sget-object v0, LX/2rJ;->PAGINATION:LX/2rJ;

    .line 2124024
    iget-object v1, p0, LX/ETB;->s:LX/ETG;

    invoke-static {p0, v0}, LX/ETB;->b(LX/ETB;LX/2rJ;)I

    move-result v2

    invoke-static {v0, v2}, LX/ETB;->a(LX/2rJ;I)LX/ETD;

    move-result-object v0

    .line 2124025
    iget-object v2, v1, LX/ETG;->p:LX/2jY;

    if-nez v2, :cond_5

    .line 2124026
    const/4 v2, 0x0

    .line 2124027
    :goto_2
    move v0, v2

    .line 2124028
    if-eqz v0, :cond_0

    .line 2124029
    iget-object v0, p0, LX/ETB;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/ETB;->A:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2124030
    :cond_3
    :try_start_3
    invoke-virtual {p0}, LX/ETB;->i()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2124031
    const/4 v0, 0x0

    goto :goto_1

    .line 2124032
    :cond_4
    new-instance v1, LX/ET7;

    iget-object v2, p0, LX/ETB;->I:LX/ETU;

    .line 2124033
    iget-object v3, v2, LX/ETU;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2124034
    invoke-direct {v1, p0, v2}, LX/ET7;-><init>(LX/ETB;Ljava/lang/String;)V

    .line 2124035
    iget-object v2, p0, LX/ETB;->s:LX/ETG;

    iget-object v3, p0, LX/ETB;->I:LX/ETU;

    .line 2124036
    iget-object v4, v3, LX/ETU;->d:LX/0us;

    move-object v3, v4

    .line 2124037
    invoke-interface {v3}, LX/0us;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/ETB;->q:LX/0xX;

    .line 2124038
    iget-object v5, v4, LX/0xX;->c:LX/0ad;

    sget v6, LX/0xY;->x:I

    const/16 v7, 0xa

    invoke-interface {v5, v6, v7}, LX/0ad;->a(II)I

    move-result v5

    move v4, v5

    .line 2124039
    iget-object v5, p0, LX/ETB;->I:LX/ETU;

    .line 2124040
    iget-object v6, v5, LX/ETU;->c:Ljava/lang/String;

    move-object v5, v6

    .line 2124041
    invoke-virtual {v2, v3, v4, v5, v1}, LX/ETG;->a(Ljava/lang/String;ILjava/lang/String;LX/0Ve;)V

    goto :goto_1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2124042
    :cond_5
    invoke-static {v1, v0}, LX/ETG;->f(LX/ETG;LX/ETD;)V

    .line 2124043
    iget-object v2, v1, LX/ETG;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/reaction/ReactionUtil;

    iget-object v3, v1, LX/ETG;->p:LX/2jY;

    invoke-virtual {v2, v3}, Lcom/facebook/reaction/ReactionUtil;->a(LX/2jY;)Z

    move-result v2

    goto :goto_2
.end method

.method public final a(J)V
    .locals 2

    .prologue
    .line 2124044
    iget-object v0, p0, LX/ETB;->w:LX/ETH;

    .line 2124045
    iput-wide p1, v0, LX/ETH;->f:J

    .line 2124046
    return-void
.end method

.method public final a(LX/0ho;ILX/0JU;)V
    .locals 6
    .param p1    # LX/0ho;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2124047
    iget-object v0, p0, LX/ETB;->q:LX/0xX;

    sget-object v1, LX/1vy;->PREFETCHING:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2124048
    :goto_0
    return-void

    .line 2124049
    :cond_0
    iget-object v0, p0, LX/ETB;->v:LX/3AW;

    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v2, p0, LX/ETB;->w:LX/ETH;

    .line 2124050
    iget-wide v4, v2, LX/ETH;->f:J

    move-wide v2, v4

    .line 2124051
    invoke-virtual {v0, p3, v1, v2, v3}, LX/3AW;->a(LX/0JU;LX/04D;J)V

    .line 2124052
    iget-object v1, p0, LX/ETB;->v:LX/3AW;

    iget-object v0, p0, LX/ETB;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 2124053
    iput-wide v2, v1, LX/3AW;->k:J

    .line 2124054
    sget-object v0, LX/2rJ;->PREFETCH:LX/2rJ;

    invoke-virtual {p0, p1, v0, p2}, LX/ETB;->a(LX/0ho;LX/2rJ;I)Z

    goto :goto_0
.end method

.method public final a(LX/9qT;)V
    .locals 4

    .prologue
    .line 2124055
    invoke-interface {p1}, LX/9qT;->a()LX/0Px;

    move-result-object v2

    .line 2124056
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2124057
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    .line 2124058
    if-eqz v0, :cond_0

    .line 2124059
    iget-object v3, p0, LX/ETB;->J:Ljava/util/Queue;

    invoke-interface {v3, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2124060
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2124061
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;)V
    .locals 2

    .prologue
    .line 2124062
    invoke-static {p0, p2}, LX/ETB;->a(LX/ETB;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2124063
    :cond_0
    :goto_0
    return-void

    .line 2124064
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v0

    .line 2124065
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2124066
    invoke-static {v0, v1, p2}, LX/ESw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;)Ljava/util/List;

    move-result-object v0

    .line 2124067
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->b()LX/0us;

    move-result-object v1

    .line 2124068
    iput-object v1, p1, Lcom/facebook/video/videohome/data/VideoHomeItem;->b:LX/0us;

    .line 2124069
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/ETQ;->addAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2124070
    invoke-static {p0}, LX/ETB;->u(LX/ETB;)V

    goto :goto_0
.end method

.method public final a(LX/0ho;LX/2rJ;I)Z
    .locals 2
    .param p1    # LX/0ho;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2124071
    iget-object v0, p0, LX/ETB;->s:LX/ETG;

    invoke-static {p2, p3}, LX/ETB;->a(LX/2rJ;I)LX/ETD;

    move-result-object v1

    .line 2124072
    iget-boolean p0, v0, LX/ETG;->l:Z

    move p0, p0

    .line 2124073
    if-eqz p0, :cond_0

    .line 2124074
    const/4 p0, 0x0

    .line 2124075
    :goto_0
    move v0, p0

    .line 2124076
    return v0

    .line 2124077
    :cond_0
    iget-object p0, v0, LX/ETG;->a:LX/0Sh;

    invoke-virtual {p0}, LX/0Sh;->c()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2124078
    invoke-static {v0, v1, p1}, LX/ETG;->b(LX/ETG;LX/ETD;LX/0ho;)V

    .line 2124079
    :goto_1
    const/4 p0, 0x1

    goto :goto_0

    .line 2124080
    :cond_1
    iget-object p0, v0, LX/ETG;->a:LX/0Sh;

    new-instance p2, Lcom/facebook/video/videohome/data/VideoHomeDataFetcher$1;

    invoke-direct {p2, v0, v1, p1}, Lcom/facebook/video/videohome/data/VideoHomeDataFetcher$1;-><init>(LX/ETG;LX/ETD;LX/0ho;)V

    invoke-virtual {p0, p2}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final a(LX/2rJ;)Z
    .locals 2

    .prologue
    .line 2124081
    const/4 v0, 0x0

    invoke-static {p0, p1}, LX/ETB;->b(LX/ETB;LX/2rJ;)I

    move-result v1

    invoke-virtual {p0, v0, p1, v1}, LX/ETB;->a(LX/0ho;LX/2rJ;I)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 15

    .prologue
    .line 2124082
    iget-object v0, p0, LX/ETB;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESy;

    const/4 v3, 0x0

    .line 2124083
    iget-object v4, v0, LX/ESy;->c:LX/0xX;

    sget-object v5, LX/1vy;->CACHED_SECTIONS:LX/1vy;

    invoke-virtual {v4, v5}, LX/0xX;->a(LX/1vy;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2124084
    :cond_0
    :goto_0
    move v0, v3

    .line 2124085
    if-nez v0, :cond_1

    .line 2124086
    :goto_1
    return-void

    .line 2124087
    :cond_1
    iget-object v0, p0, LX/ETB;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESy;

    .line 2124088
    iget-object v3, v0, LX/ESy;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/ESy;->b:LX/0Tn;

    iget-object v5, v0, LX/ESy;->e:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2124089
    sget-object v0, LX/2rJ;->CACHED_SECTION:LX/2rJ;

    .line 2124090
    iget-object v1, p0, LX/ETB;->s:LX/ETG;

    invoke-static {p0, v0}, LX/ETB;->b(LX/ETB;LX/2rJ;)I

    move-result v2

    invoke-static {v0, v2}, LX/ETB;->a(LX/2rJ;I)LX/ETD;

    move-result-object v0

    .line 2124091
    iget-object v2, v1, LX/ETG;->e:LX/CfW;

    const-string v3, "VIDEO_HOME"

    invoke-static {v1, v0}, LX/ETG;->e(LX/ETG;LX/ETD;)Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/CfW;->b(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    .line 2124092
    goto :goto_1

    .line 2124093
    :cond_2
    sget v4, LX/0xX;->b:I

    .line 2124094
    iget-object v5, v0, LX/ESy;->e:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 2124095
    iget-object v11, v0, LX/ESy;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v12, LX/ESy;->b:LX/0Tn;

    const-wide/16 v13, 0x0

    invoke-interface {v11, v12, v13, v14}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v11

    move-wide v7, v11

    .line 2124096
    sub-long/2addr v5, v7

    .line 2124097
    int-to-long v7, v4

    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    cmp-long v4, v5, v7

    if-lez v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2124098
    iget-object v0, p0, LX/ETB;->s:LX/ETG;

    .line 2124099
    iget-boolean p0, v0, LX/ETG;->l:Z

    move v0, p0

    .line 2124100
    return v0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 2124101
    iget-object v0, p0, LX/ETB;->s:LX/ETG;

    if-eqz v0, :cond_0

    .line 2124102
    iget-object v0, p0, LX/ETB;->s:LX/ETG;

    .line 2124103
    iget-object v1, v0, LX/ETG;->p:LX/2jY;

    if-eqz v1, :cond_0

    .line 2124104
    iget-object v1, v0, LX/ETG;->p:LX/2jY;

    invoke-virtual {v1}, LX/2jY;->a()V

    .line 2124105
    const/4 v1, 0x0

    iput-object v1, v0, LX/ETG;->p:LX/2jY;

    .line 2124106
    :cond_0
    invoke-virtual {p0}, LX/ETB;->l()V

    .line 2124107
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2124108
    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    invoke-virtual {v0}, LX/ETQ;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2124109
    invoke-virtual {p0}, LX/ETB;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ETB;->w:LX/ETH;

    .line 2124110
    iget-boolean p0, v0, LX/ETH;->d:Z

    move v0, p0

    .line 2124111
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 2124112
    iget-object v0, p0, LX/ETB;->I:LX/ETU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ETB;->I:LX/ETU;

    .line 2124113
    iget-object v1, v0, LX/ETU;->d:LX/0us;

    move-object v0, v1

    .line 2124114
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ETB;->I:LX/ETU;

    .line 2124115
    iget-object v1, v0, LX/ETU;->d:LX/0us;

    move-object v0, v1

    .line 2124116
    invoke-interface {v0}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 5

    .prologue
    .line 2124117
    iget-object v0, p0, LX/ETB;->q:LX/0xX;

    sget-object v1, LX/1vy;->DOWNLOAD_SECTION:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2124118
    :goto_0
    return-void

    .line 2124119
    :cond_0
    iget-object v0, p0, LX/ETB;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETN;

    .line 2124120
    iget-object v1, v0, LX/ETN;->e:LX/ETL;

    invoke-virtual {v1}, LX/ETL;->a()Z

    move-result v1

    move v0, v1

    .line 2124121
    if-nez v0, :cond_1

    .line 2124122
    iget-object v0, p0, LX/ETB;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETN;

    new-instance v1, LX/ETA;

    invoke-direct {v1, p0}, LX/ETA;-><init>(LX/ETB;)V

    .line 2124123
    iget-object v2, v0, LX/ETN;->e:LX/ETL;

    .line 2124124
    iput-object v1, v2, LX/ETL;->c:LX/ETA;

    .line 2124125
    iget-object v2, v0, LX/ETN;->f:LX/16U;

    const-class v3, LX/1ub;

    iget-object v4, v0, LX/ETN;->g:LX/ETM;

    invoke-virtual {v2, v3, v4}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 2124126
    :cond_1
    iget-object v0, p0, LX/ETB;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETN;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/ETN;->a(ZLX/ETO;)V

    goto :goto_0
.end method

.method public final declared-synchronized k()V
    .locals 8

    .prologue
    .line 2124127
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ETB;->q:LX/0xX;

    sget-object v1, LX/1vy;->DOWNLOAD_SECTION:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2124128
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2124129
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/ETB;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETN;

    .line 2124130
    iget-object v1, v0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v1, :cond_5

    iget-object v1, v0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->s()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2124131
    iget-object v1, v0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v1

    .line 2124132
    :goto_1
    move-object v1, v1

    .line 2124133
    if-nez v1, :cond_2

    .line 2124134
    invoke-virtual {p0}, LX/ETB;->j()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2124135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2124136
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    move-object v0, v0

    .line 2124137
    const-string v2, "download-section-id"

    invoke-virtual {v0, v2}, LX/ETQ;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/ETB;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETN;

    invoke-virtual {v0}, LX/ETN;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2124138
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2124139
    iget-object v0, p0, LX/ETB;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETN;

    .line 2124140
    new-instance v3, Lcom/facebook/video/videohome/data/VideoHomeItem;

    new-instance v4, LX/9vz;

    invoke-direct {v4}, LX/9vz;-><init>()V

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2124141
    iput-object v5, v4, LX/9vz;->J:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2124142
    move-object v4, v4

    .line 2124143
    new-instance v5, LX/9vl;

    invoke-direct {v5}, LX/9vl;-><init>()V

    iget-object v6, v0, LX/ETN;->d:Landroid/content/Context;

    const v7, 0x7f080dac

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2124144
    iput-object v6, v5, LX/9vl;->c:Ljava/lang/String;

    .line 2124145
    move-object v5, v5

    .line 2124146
    invoke-virtual {v5}, LX/9vl;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v5

    .line 2124147
    iput-object v5, v4, LX/9vz;->bt:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    .line 2124148
    move-object v4, v4

    .line 2124149
    invoke-virtual {v4}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v4

    const-string v5, "download-section-id"

    const-string v6, "download-type-token"

    invoke-direct {v3, v4, v5, v6}, Lcom/facebook/video/videohome/data/VideoHomeItem;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 2124150
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2124151
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2124152
    invoke-virtual {p0}, LX/ETB;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2124153
    iget-object v0, p0, LX/ETB;->w:LX/ETH;

    invoke-virtual {v0}, LX/ETH;->c()V

    .line 2124154
    :cond_3
    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    move-object v0, v0

    .line 2124155
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, LX/ETQ;->a(ILjava/util/Collection;)Z

    .line 2124156
    invoke-direct {p0}, LX/ETB;->v()V

    goto/16 :goto_0

    .line 2124157
    :cond_4
    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    move-object v0, v0

    .line 2124158
    const-string v1, "download-section-id"

    invoke-virtual {v0, v1}, LX/ETQ;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/ETB;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ETN;

    invoke-virtual {v0}, LX/ETN;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2124159
    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    move-object v0, v0

    .line 2124160
    const-string v1, "download-section-id"

    invoke-virtual {v0, v1}, LX/ETQ;->a(Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_5
    iget-object v1, v0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    goto/16 :goto_1
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2124161
    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    invoke-virtual {v0}, LX/ETQ;->clear()V

    .line 2124162
    iget-object v0, p0, LX/ETB;->J:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2124163
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ETB;->H:Z

    .line 2124164
    const/4 v0, 0x0

    iput-object v0, p0, LX/ETB;->I:LX/ETU;

    .line 2124165
    iget-object v0, p0, LX/ETB;->w:LX/ETH;

    .line 2124166
    invoke-static {v0}, LX/ETH;->e(LX/ETH;)V

    .line 2124167
    return-void
.end method

.method public final m()V
    .locals 7

    .prologue
    .line 2124168
    iget-object v0, p0, LX/ETB;->J:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2124169
    :cond_0
    :goto_0
    return-void

    .line 2124170
    :cond_1
    iget-object v0, p0, LX/ETB;->J:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2124171
    invoke-direct {p0, v0}, LX/ETB;->c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Ljava/util/List;

    move-result-object v1

    .line 2124172
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2124173
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124174
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2124175
    invoke-interface {v3}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_SEE_ALL_FOOTER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v3, v4, :cond_4

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2124176
    :goto_1
    move-object v2, v2

    .line 2124177
    iget-object v3, p0, LX/ETB;->t:LX/ETQ;

    invoke-virtual {v3, v1}, LX/ETQ;->addAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2124178
    const/4 v1, 0x0

    .line 2124179
    invoke-static {v0}, LX/ETB;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v3

    .line 2124180
    if-nez v3, :cond_5

    .line 2124181
    :cond_2
    :goto_2
    move-object v0, v1

    .line 2124182
    iput-object v0, p0, LX/ETB;->I:LX/ETU;

    .line 2124183
    if-eqz v2, :cond_3

    .line 2124184
    iget-object v0, p0, LX/ETB;->I:LX/ETU;

    if-nez v0, :cond_7

    .line 2124185
    iget-object v0, p0, LX/ETB;->t:LX/ETQ;

    invoke-virtual {v0, v2}, LX/ETQ;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    .line 2124186
    :cond_3
    :goto_3
    invoke-static {p0}, LX/ETB;->u(LX/ETB;)V

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 2124187
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v4

    .line 2124188
    if-nez v4, :cond_6

    .line 2124189
    const/4 v4, 0x0

    .line 2124190
    :goto_4
    move-object v4, v4

    .line 2124191
    if-eqz v4, :cond_2

    .line 2124192
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->b()LX/0us;

    move-result-object v4

    .line 2124193
    invoke-static {v4}, LX/ETB;->b(LX/0us;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2124194
    new-instance v1, LX/ETU;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v5, v6, v3}, LX/ETU;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2124195
    iput-object v4, v1, LX/ETU;->d:LX/0us;

    .line 2124196
    goto :goto_2

    :cond_6
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v4

    goto :goto_4

    .line 2124197
    :cond_7
    iget-object v0, p0, LX/ETB;->I:LX/ETU;

    .line 2124198
    iput-object v2, v0, LX/ETU;->e:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124199
    goto :goto_3
.end method
