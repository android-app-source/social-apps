.class public final enum LX/Emv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Emv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Emv;

.field public static final enum ADAPTER_UDPATED:LX/Emv;

.field public static final enum FINAL_DATA_AVAILABLE:LX/Emv;

.field public static final enum INTRO_ANIMATION_END:LX/Emv;

.field public static final enum PREVIEW_DATA_AVAILABLE:LX/Emv;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2166353
    new-instance v0, LX/Emv;

    const-string v1, "ADAPTER_UDPATED"

    const-string v2, "ec_adapter_updated"

    invoke-direct {v0, v1, v3, v2}, LX/Emv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emv;->ADAPTER_UDPATED:LX/Emv;

    .line 2166354
    new-instance v0, LX/Emv;

    const-string v1, "PREVIEW_DATA_AVAILABLE"

    const-string v2, "ec_preview_data_available"

    invoke-direct {v0, v1, v4, v2}, LX/Emv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emv;->PREVIEW_DATA_AVAILABLE:LX/Emv;

    .line 2166355
    new-instance v0, LX/Emv;

    const-string v1, "FINAL_DATA_AVAILABLE"

    const-string v2, "ec_final_data_available"

    invoke-direct {v0, v1, v5, v2}, LX/Emv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emv;->FINAL_DATA_AVAILABLE:LX/Emv;

    .line 2166356
    new-instance v0, LX/Emv;

    const-string v1, "INTRO_ANIMATION_END"

    const-string v2, "ec_intro_animation_end"

    invoke-direct {v0, v1, v6, v2}, LX/Emv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emv;->INTRO_ANIMATION_END:LX/Emv;

    .line 2166357
    const/4 v0, 0x4

    new-array v0, v0, [LX/Emv;

    sget-object v1, LX/Emv;->ADAPTER_UDPATED:LX/Emv;

    aput-object v1, v0, v3

    sget-object v1, LX/Emv;->PREVIEW_DATA_AVAILABLE:LX/Emv;

    aput-object v1, v0, v4

    sget-object v1, LX/Emv;->FINAL_DATA_AVAILABLE:LX/Emv;

    aput-object v1, v0, v5

    sget-object v1, LX/Emv;->INTRO_ANIMATION_END:LX/Emv;

    aput-object v1, v0, v6

    sput-object v0, LX/Emv;->$VALUES:[LX/Emv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2166358
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2166359
    iput-object p3, p0, LX/Emv;->name:Ljava/lang/String;

    .line 2166360
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Emv;
    .locals 1

    .prologue
    .line 2166361
    const-class v0, LX/Emv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Emv;

    return-object v0
.end method

.method public static values()[LX/Emv;
    .locals 1

    .prologue
    .line 2166362
    sget-object v0, LX/Emv;->$VALUES:[LX/Emv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Emv;

    return-object v0
.end method
