.class public final LX/DLn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1988908
    const/4 v7, 0x0

    .line 1988909
    const/4 v6, 0x0

    .line 1988910
    const/4 v3, 0x0

    .line 1988911
    const-wide/16 v4, 0x0

    .line 1988912
    const/4 v2, 0x0

    .line 1988913
    const/4 v1, 0x0

    .line 1988914
    const/4 v0, 0x0

    .line 1988915
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 1988916
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1988917
    const/4 v0, 0x0

    .line 1988918
    :goto_0
    return v0

    .line 1988919
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v9, :cond_9

    .line 1988920
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 1988921
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1988922
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v1, :cond_0

    .line 1988923
    const-string v9, "actors"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1988924
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1988925
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_1

    .line 1988926
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_1

    .line 1988927
    invoke-static {p0, p1}, LX/DLj;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1988928
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1988929
    :cond_1
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1988930
    move v8, v1

    goto :goto_1

    .line 1988931
    :cond_2
    const-string v9, "attachments"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1988932
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1988933
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v9, :cond_3

    .line 1988934
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v9, :cond_3

    .line 1988935
    invoke-static {p0, p1}, LX/DLl;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1988936
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1988937
    :cond_3
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1988938
    move v5, v1

    goto :goto_1

    .line 1988939
    :cond_4
    const-string v9, "cache_id"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1988940
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto/16 :goto_1

    .line 1988941
    :cond_5
    const-string v9, "creation_time"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1988942
    const/4 v0, 0x1

    .line 1988943
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto/16 :goto_1

    .line 1988944
    :cond_6
    const-string v9, "id"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1988945
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 1988946
    :cond_7
    const-string v9, "title"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1988947
    invoke-static {p0, p1}, LX/DLm;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 1988948
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1988949
    :cond_9
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1988950
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1988951
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1988952
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1988953
    if-eqz v0, :cond_a

    .line 1988954
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1988955
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1988956
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1988957
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v8, v7

    move v7, v2

    move-wide v11, v4

    move v4, v3

    move v5, v6

    move-wide v2, v11

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1988958
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1988959
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1988960
    if-eqz v0, :cond_1

    .line 1988961
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1988962
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1988963
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 1988964
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2}, LX/DLj;->a(LX/15i;ILX/0nX;)V

    .line 1988965
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1988966
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1988967
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1988968
    if-eqz v0, :cond_3

    .line 1988969
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1988970
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1988971
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 1988972
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/DLl;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1988973
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1988974
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1988975
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1988976
    if-eqz v0, :cond_4

    .line 1988977
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1988978
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1988979
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1988980
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 1988981
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1988982
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1988983
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1988984
    if-eqz v0, :cond_6

    .line 1988985
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1988986
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1988987
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1988988
    if-eqz v0, :cond_7

    .line 1988989
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1988990
    invoke-static {p0, v0, p2}, LX/DLm;->a(LX/15i;ILX/0nX;)V

    .line 1988991
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1988992
    return-void
.end method
