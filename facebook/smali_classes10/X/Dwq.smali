.class public final enum LX/Dwq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dwq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dwq;

.field public static final enum LOADING:LX/Dwq;

.field public static final enum MOMENTS_PROMOTION:LX/Dwq;

.field public static final enum MOMENTS_SEGUE:LX/Dwq;

.field public static final enum NONE:LX/Dwq;

.field public static final enum SYNC:LX/Dwq;

.field public static final enum SYNC_UNSUPPORTED:LX/Dwq;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2061704
    new-instance v0, LX/Dwq;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/Dwq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dwq;->NONE:LX/Dwq;

    .line 2061705
    new-instance v0, LX/Dwq;

    const-string v1, "SYNC"

    invoke-direct {v0, v1, v4}, LX/Dwq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dwq;->SYNC:LX/Dwq;

    .line 2061706
    new-instance v0, LX/Dwq;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v5}, LX/Dwq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dwq;->LOADING:LX/Dwq;

    .line 2061707
    new-instance v0, LX/Dwq;

    const-string v1, "MOMENTS_SEGUE"

    invoke-direct {v0, v1, v6}, LX/Dwq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dwq;->MOMENTS_SEGUE:LX/Dwq;

    .line 2061708
    new-instance v0, LX/Dwq;

    const-string v1, "MOMENTS_PROMOTION"

    invoke-direct {v0, v1, v7}, LX/Dwq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dwq;->MOMENTS_PROMOTION:LX/Dwq;

    .line 2061709
    new-instance v0, LX/Dwq;

    const-string v1, "SYNC_UNSUPPORTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Dwq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dwq;->SYNC_UNSUPPORTED:LX/Dwq;

    .line 2061710
    const/4 v0, 0x6

    new-array v0, v0, [LX/Dwq;

    sget-object v1, LX/Dwq;->NONE:LX/Dwq;

    aput-object v1, v0, v3

    sget-object v1, LX/Dwq;->SYNC:LX/Dwq;

    aput-object v1, v0, v4

    sget-object v1, LX/Dwq;->LOADING:LX/Dwq;

    aput-object v1, v0, v5

    sget-object v1, LX/Dwq;->MOMENTS_SEGUE:LX/Dwq;

    aput-object v1, v0, v6

    sget-object v1, LX/Dwq;->MOMENTS_PROMOTION:LX/Dwq;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Dwq;->SYNC_UNSUPPORTED:LX/Dwq;

    aput-object v2, v0, v1

    sput-object v0, LX/Dwq;->$VALUES:[LX/Dwq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2061703
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dwq;
    .locals 1

    .prologue
    .line 2061702
    const-class v0, LX/Dwq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dwq;

    return-object v0
.end method

.method public static values()[LX/Dwq;
    .locals 1

    .prologue
    .line 2061701
    sget-object v0, LX/Dwq;->$VALUES:[LX/Dwq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dwq;

    return-object v0
.end method
