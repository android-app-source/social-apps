.class public LX/ErH;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/62U;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Z

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2172764
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2172765
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ErH;->b:Ljava/util/List;

    return-void
.end method

.method public static d(LX/ErH;)I
    .locals 1

    .prologue
    .line 2172755
    iget-object v0, p0, LX/ErH;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2172756
    packed-switch p2, :pswitch_data_0

    .line 2172757
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2172758
    :pswitch_0
    new-instance v1, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 2172759
    check-cast v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a()V

    .line 2172760
    new-instance v0, LX/62U;

    invoke-direct {v0, v1}, LX/62U;-><init>(Landroid/view/View;)V

    .line 2172761
    :goto_0
    return-object v0

    .line 2172762
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2172763
    new-instance v0, LX/62U;

    const v2, 0x7f03057a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62U;-><init>(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2172747
    check-cast p1, LX/62U;

    const/4 v2, 0x0

    .line 2172748
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2172749
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2172750
    :pswitch_0
    iget-object v0, p1, LX/62U;->l:Landroid/view/View;

    check-cast v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    .line 2172751
    iget-object v1, p0, LX/ErH;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2172752
    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a(LX/8QL;Z)V

    .line 2172753
    :goto_0
    return-void

    .line 2172754
    :pswitch_1
    iget-object v0, p1, LX/62U;->l:Landroid/view/View;

    const v1, 0x7f0d0f32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v0, p0, LX/ErH;->a:Z

    if-eqz v0, :cond_0

    move v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2172744
    invoke-static {p0}, LX/ErH;->d(LX/ErH;)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2172745
    const/4 v0, 0x1

    .line 2172746
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2172743
    iget-object v0, p0, LX/ErH;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
