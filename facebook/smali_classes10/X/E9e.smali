.class public LX/E9e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/E9e;


# instance fields
.field public final a:LX/11R;

.field private final b:LX/1nI;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/EAb;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/154;

.field public final g:LX/8Sa;

.field public final h:LX/0wM;

.field private final i:LX/Ch5;

.field private final j:LX/BNK;

.field public final k:LX/BNM;

.field public final l:Landroid/text/SpannableString;


# direct methods
.method public constructor <init>(LX/11R;LX/1nI;LX/0Or;LX/EAb;LX/0Or;LX/154;LX/8Sa;LX/0wM;LX/Ch5;LX/BNK;LX/BNM;)V
    .locals 6
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/reviews/gating/gk/IsUserReviewsPageEnabled;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11R;",
            "LX/1nI;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/EAb;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/154;",
            "LX/8Sa;",
            "LX/0wM;",
            "LX/Ch5;",
            "LX/BNK;",
            "LX/BNM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2084706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2084707
    iput-object p1, p0, LX/E9e;->a:LX/11R;

    .line 2084708
    iput-object p2, p0, LX/E9e;->b:LX/1nI;

    .line 2084709
    iput-object p3, p0, LX/E9e;->c:LX/0Or;

    .line 2084710
    iput-object p4, p0, LX/E9e;->d:LX/EAb;

    .line 2084711
    iput-object p5, p0, LX/E9e;->e:LX/0Or;

    .line 2084712
    iput-object p6, p0, LX/E9e;->f:LX/154;

    .line 2084713
    iput-object p7, p0, LX/E9e;->g:LX/8Sa;

    .line 2084714
    iput-object p8, p0, LX/E9e;->h:LX/0wM;

    .line 2084715
    iput-object p9, p0, LX/E9e;->i:LX/Ch5;

    .line 2084716
    move-object/from16 v0, p10

    iput-object v0, p0, LX/E9e;->j:LX/BNK;

    .line 2084717
    move-object/from16 v0, p11

    iput-object v0, p0, LX/E9e;->k:LX/BNM;

    .line 2084718
    new-instance v1, Landroid/text/SpannableString;

    const-string v2, " \u2022 "

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v1, p0, LX/E9e;->l:Landroid/text/SpannableString;

    .line 2084719
    iget-object v1, p0, LX/E9e;->l:Landroid/text/SpannableString;

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const v3, -0x958e80

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2084720
    return-void
.end method

.method public static a(LX/0QB;)LX/E9e;
    .locals 15

    .prologue
    .line 2084721
    sget-object v0, LX/E9e;->m:LX/E9e;

    if-nez v0, :cond_1

    .line 2084722
    const-class v1, LX/E9e;

    monitor-enter v1

    .line 2084723
    :try_start_0
    sget-object v0, LX/E9e;->m:LX/E9e;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2084724
    if-eqz v2, :cond_0

    .line 2084725
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2084726
    new-instance v3, LX/E9e;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v4

    check-cast v4, LX/11R;

    invoke-static {v0}, LX/1nH;->a(LX/0QB;)LX/1nH;

    move-result-object v5

    check-cast v5, LX/1nI;

    const/16 v6, 0x35a

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const-class v7, LX/EAb;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/EAb;

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v9

    check-cast v9, LX/154;

    invoke-static {v0}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v10

    check-cast v10, LX/8Sa;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v11

    check-cast v11, LX/0wM;

    invoke-static {v0}, LX/Ch5;->a(LX/0QB;)LX/Ch5;

    move-result-object v12

    check-cast v12, LX/Ch5;

    .line 2084727
    new-instance v14, LX/BNK;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v13

    check-cast v13, LX/17W;

    invoke-direct {v14, v13}, LX/BNK;-><init>(LX/17W;)V

    .line 2084728
    move-object v13, v14

    .line 2084729
    check-cast v13, LX/BNK;

    invoke-static {v0}, LX/BNM;->a(LX/0QB;)LX/BNM;

    move-result-object v14

    check-cast v14, LX/BNM;

    invoke-direct/range {v3 .. v14}, LX/E9e;-><init>(LX/11R;LX/1nI;LX/0Or;LX/EAb;LX/0Or;LX/154;LX/8Sa;LX/0wM;LX/Ch5;LX/BNK;LX/BNM;)V

    .line 2084730
    move-object v0, v3

    .line 2084731
    sput-object v0, LX/E9e;->m:LX/E9e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2084732
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2084733
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2084734
    :cond_1
    sget-object v0, LX/E9e;->m:LX/E9e;

    return-object v0

    .line 2084735
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2084736
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/E9e;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableStringBuilder;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2084676
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, LX/E9e;->k:LX/BNM;

    const v2, 0x7f0b0052

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, p4, v2}, LX/BNM;->a(II)Landroid/text/SpannableString;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2084677
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2084678
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2084679
    const/4 v0, 0x0

    .line 2084680
    :cond_0
    :goto_0
    return-object v0

    .line 2084681
    :cond_1
    iget-object v1, p0, LX/E9e;->l:Landroid/text/SpannableString;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2084682
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2084683
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const p0, -0x958e80

    invoke-direct {v2, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 p0, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result p1

    const/16 p3, 0x21

    invoke-virtual {v1, v2, p0, p1, p3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2084684
    move-object v1, v1

    .line 2084685
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/reviews/ui/ReviewFeedRowView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2084686
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setTitleOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084687
    const v0, 0x7f0e013f

    invoke-virtual {p0, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setTitleTextAppearance(I)V

    .line 2084688
    invoke-virtual {p0, p1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setTitle(Ljava/lang/CharSequence;)V

    .line 2084689
    return-void
.end method

.method public static a$redex0(LX/E9e;LX/5tj;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2084690
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2084691
    :cond_0
    :goto_0
    return-void

    .line 2084692
    :cond_1
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    invoke-interface {p1}, LX/5tj;->bh_()LX/1VU;

    move-result-object v1

    invoke-static {v1}, LX/9JZ;->a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 2084693
    iput-object v1, v0, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2084694
    move-object v0, v0

    .line 2084695
    invoke-interface {p1}, LX/5tj;->bh_()LX/1VU;

    move-result-object v1

    invoke-interface {v1}, LX/1VU;->j()Ljava/lang/String;

    move-result-object v1

    .line 2084696
    iput-object v1, v0, LX/8qL;->d:Ljava/lang/String;

    .line 2084697
    move-object v0, v0

    .line 2084698
    new-instance v1, LX/21A;

    invoke-direct {v1}, LX/21A;-><init>()V

    .line 2084699
    iput-object p3, v1, LX/21A;->c:Ljava/lang/String;

    .line 2084700
    move-object v1, v1

    .line 2084701
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    .line 2084702
    iput-object v1, v0, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2084703
    move-object v0, v0

    .line 2084704
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v0

    .line 2084705
    iget-object v1, p0, LX/E9e;->b:LX/1nI;

    invoke-interface {v1, p2, v0}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    goto :goto_0
.end method

.method public static b(LX/5tj;)Z
    .locals 1

    .prologue
    .line 2084600
    invoke-interface {p0}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    invoke-interface {v0}, LX/1VU;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    invoke-interface {v0}, LX/1VU;->r_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/E9e;LX/5tj;)V
    .locals 3

    .prologue
    .line 2084597
    invoke-interface {p1}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2084598
    iget-object v0, p0, LX/E9e;->i:LX/Ch5;

    new-instance v1, LX/Ch9;

    invoke-interface {p1}, LX/5tj;->bh_()LX/1VU;

    move-result-object v2

    invoke-static {v2}, LX/9JZ;->a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Ch9;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2084599
    :cond_0
    return-void
.end method

.method public static f(LX/5tj;)Z
    .locals 1

    .prologue
    .line 2084601
    invoke-interface {p0}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    invoke-interface {v0}, LX/1VU;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/reviews/ui/ReviewFeedRowView;LX/5tj;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 2084602
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2084603
    invoke-interface {p2}, LX/5ti;->e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/5ti;->e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;->k()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/5ti;->e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;->k()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2084604
    :cond_0
    const/4 v0, 0x0

    .line 2084605
    :goto_0
    move-object v0, v0

    .line 2084606
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setProfilePicture(Landroid/net/Uri;)V

    .line 2084607
    invoke-static {p2}, LX/BNJ;->a(LX/5ti;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setTitle(Ljava/lang/CharSequence;)V

    .line 2084608
    const v0, 0x7f0e0147

    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setTitleTextAppearance(I)V

    .line 2084609
    invoke-interface {p2}, LX/5tj;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, LX/5tj;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel$TextModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, LX/5tj;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel$TextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel$TextModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    :cond_1
    const/4 v0, 0x0

    :goto_1
    move-object v3, v0

    .line 2084610
    invoke-static {p2}, LX/BNJ;->b(LX/55r;)Ljava/lang/String;

    move-result-object v4

    .line 2084611
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2084612
    iget-object v0, p0, LX/E9e;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p2}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 2084613
    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setSecondaryActionVisibility(I)V

    .line 2084614
    invoke-virtual {p1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-interface {p2}, LX/5tj;->b()I

    move-result v5

    invoke-static {p0, v0, v3, v4, v5}, LX/E9e;->a(LX/E9e;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-interface {p2}, LX/5tj;->b()I

    move-result v6

    .line 2084615
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2084616
    const/4 v7, 0x0

    .line 2084617
    :goto_3
    move-object v3, v7

    .line 2084618
    invoke-virtual {p1, v0, v3}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->a(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;)V

    .line 2084619
    invoke-virtual {p1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2084620
    new-instance v3, LX/3DI;

    const-string v4, "   "

    invoke-direct {v3, v4}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 2084621
    invoke-interface {p2}, LX/5tj;->bh_()LX/1VU;

    move-result-object v4

    if-nez v4, :cond_c

    .line 2084622
    :goto_4
    move-object v0, v3

    .line 2084623
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setFeedbackSummary(Landroid/text/SpannableStringBuilder;)V

    .line 2084624
    invoke-static {p2}, LX/E9e;->b(LX/5tj;)Z

    move-result v3

    .line 2084625
    invoke-static {p2}, LX/E9e;->f(LX/5tj;)Z

    move-result v4

    .line 2084626
    if-nez v3, :cond_2

    if-eqz v4, :cond_5

    :cond_2
    move v0, v2

    :goto_5
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setFeedbackDividerVisibility(I)V

    .line 2084627
    invoke-interface {p2}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    if-nez v0, :cond_11

    .line 2084628
    const/4 v0, 0x0

    .line 2084629
    :goto_6
    move v0, v0

    .line 2084630
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setLikeButtonLikeState(Z)V

    .line 2084631
    iget-object v0, p0, LX/E9e;->d:LX/EAb;

    new-instance v5, LX/E9b;

    invoke-direct {v5, p0}, LX/E9b;-><init>(LX/E9e;)V

    .line 2084632
    new-instance v8, LX/EAa;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/3id;->b(LX/0QB;)LX/3id;

    move-result-object v7

    check-cast v7, LX/3id;

    invoke-direct {v8, v5, v6, v7}, LX/EAa;-><init>(LX/E9b;LX/03V;LX/3id;)V

    .line 2084633
    move-object v0, v8

    .line 2084634
    new-instance v5, LX/E9c;

    invoke-direct {v5, p0, v0, p2}, LX/E9c;-><init>(LX/E9e;LX/EAa;LX/5tj;)V

    .line 2084635
    move-object v0, v5

    .line 2084636
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setLikeButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084637
    invoke-virtual {p1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2084638
    new-instance v5, LX/E9d;

    invoke-direct {v5, p0, p2, v0, p3}, LX/E9d;-><init>(LX/E9e;LX/5tj;Landroid/content/Context;Ljava/lang/String;)V

    move-object v0, v5

    .line 2084639
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setCommentButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084640
    if-eqz v3, :cond_6

    move v0, v2

    :goto_7
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setLikeButtonVisibility(I)V

    .line 2084641
    if-eqz v4, :cond_7

    :goto_8
    invoke-virtual {p1, v2}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setCommentButtonVisibility(I)V

    .line 2084642
    new-instance v1, LX/3DI;

    invoke-direct {v1}, LX/3DI;-><init>()V

    .line 2084643
    iget-object v2, p0, LX/E9e;->a:LX/11R;

    sget-object v3, LX/1lB;->FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

    invoke-interface {p2}, LX/5tj;->bg_()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-virtual {v2, v3, v5, v6}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3DI;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2084644
    move-object v0, v1

    .line 2084645
    invoke-virtual {p1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getResources()Landroid/content/res/Resources;

    const/4 v5, 0x0

    .line 2084646
    if-eqz p2, :cond_3

    invoke-interface {p2}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->a()LX/1Fd;

    move-result-object v1

    if-nez v1, :cond_12

    .line 2084647
    :cond_3
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2084648
    :goto_9
    move-object v0, v1

    .line 2084649
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setSubtitle(Landroid/text/SpannableString;)V

    .line 2084650
    return-void

    :cond_4
    move v0, v2

    .line 2084651
    goto/16 :goto_2

    :cond_5
    move v0, v1

    .line 2084652
    goto/16 :goto_5

    :cond_6
    move v0, v1

    .line 2084653
    goto :goto_7

    :cond_7
    move v2, v1

    .line 2084654
    goto :goto_8

    :cond_8
    invoke-interface {p2}, LX/5ti;->e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;->k()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    invoke-interface {p2}, LX/5tj;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel$TextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel$TextModel;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2084655
    :cond_a
    new-instance v7, LX/3DI;

    iget-object v8, p0, LX/E9e;->l:Landroid/text/SpannableString;

    invoke-direct {v7, v8}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 2084656
    if-nez v3, :cond_b

    .line 2084657
    iget-object v8, p0, LX/E9e;->k:LX/BNM;

    const v9, 0x7f0b0052

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-virtual {v8, v6, v9}, LX/BNM;->a(II)Landroid/text/SpannableString;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3DI;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2084658
    :cond_b
    invoke-virtual {v7, v4}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    goto/16 :goto_3

    .line 2084659
    :cond_c
    invoke-interface {p2}, LX/5tj;->bh_()LX/1VU;

    move-result-object v4

    .line 2084660
    invoke-interface {v4}, LX/1VU;->l()LX/17A;

    move-result-object v5

    if-nez v5, :cond_f

    const/4 v5, 0x0

    :goto_a
    move v4, v5

    .line 2084661
    if-lez v4, :cond_d

    invoke-static {p2}, LX/E9e;->b(LX/5tj;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 2084662
    const v5, 0x7f0f0060

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v0, v5, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2084663
    :cond_d
    invoke-interface {p2}, LX/5tj;->bh_()LX/1VU;

    move-result-object v4

    .line 2084664
    invoke-interface {v4}, LX/1VU;->m()LX/172;

    move-result-object v5

    if-nez v5, :cond_10

    const/4 v5, 0x0

    :goto_b
    move v4, v5

    .line 2084665
    if-lez v4, :cond_e

    invoke-static {p2}, LX/E9e;->f(LX/5tj;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 2084666
    const v5, 0x7f0f0062

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v0, v5, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2084667
    :cond_e
    new-array v4, v9, [Ljava/lang/Object;

    new-instance v5, LX/E9a;

    invoke-direct {v5, p0, p2, p3}, LX/E9a;-><init>(LX/E9e;LX/5tj;Ljava/lang/String;)V

    aput-object v5, v4, v8

    invoke-static {v3, v4}, LX/47t;->a(Landroid/text/SpannableStringBuilder;[Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_f
    invoke-interface {v4}, LX/1VU;->l()LX/17A;

    move-result-object v5

    invoke-interface {v5}, LX/17A;->a()I

    move-result v5

    goto :goto_a

    :cond_10
    invoke-interface {v4}, LX/1VU;->m()LX/172;

    move-result-object v5

    invoke-interface {v5}, LX/172;->a()I

    move-result v5

    goto :goto_b

    :cond_11
    invoke-interface {p2}, LX/5tj;->bh_()LX/1VU;

    move-result-object v0

    invoke-interface {v0}, LX/1VU;->s_()Z

    move-result v0

    goto/16 :goto_6

    .line 2084668
    :cond_12
    const-string v1, ""

    invoke-virtual {v0, v1}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2084669
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2084670
    iget-object v1, p0, LX/E9e;->g:LX/8Sa;

    invoke-interface {p2}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->a()LX/1Fd;

    move-result-object v3

    sget-object v4, LX/8SZ;->PILL:LX/8SZ;

    invoke-virtual {v1, v3, v4}, LX/8Sa;->a(LX/1Fd;LX/8SZ;)I

    move-result v1

    .line 2084671
    iget-object v3, p0, LX/E9e;->h:LX/0wM;

    const v4, -0x958e80

    invoke-virtual {v3, v1, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    .line 2084672
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v1, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2084673
    new-instance v3, LX/34T;

    const/4 v4, 0x2

    invoke-direct {v3, v1, v4}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 2084674
    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object v1, v2

    .line 2084675
    goto/16 :goto_9
.end method
