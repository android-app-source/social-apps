.class public final LX/D1s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;)V
    .locals 0

    .prologue
    .line 1957531
    iput-object p1, p0, LX/D1s;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x76670364

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1957532
    iget-object v1, p0, LX/D1s;->a:Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    .line 1957533
    iget-object v3, v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BQj;

    .line 1957534
    const-string v4, "android_profile_video_existing"

    invoke-static {v3, v4}, LX/BQj;->a(LX/BQj;Ljava/lang/String;)V

    .line 1957535
    iget-object v3, v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->g:LX/D2D;

    sget-object v4, LX/D2C;->OPEN_GALLERY:LX/D2C;

    iget-object p0, v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, p0}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    .line 1957536
    new-instance v4, Landroid/content/Intent;

    iget-object v3, v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->e:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v4, v3, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1957537
    new-instance v3, LX/8AA;

    sget-object p0, LX/8AB;->PROFILEPIC:LX/8AB;

    invoke-direct {v3, p0}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v3}, LX/8AA;->k()LX/8AA;

    move-result-object v3

    invoke-virtual {v3}, LX/8AA;->i()LX/8AA;

    move-result-object v3

    sget-object p0, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v3, p0}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v3

    invoke-virtual {v3}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v3

    .line 1957538
    const-string p0, "extra_simple_picker_launcher_settings"

    invoke-virtual {v4, p0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1957539
    iget-object v3, v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const/4 p0, 0x2

    iget-object p1, v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->e:Landroid/app/Activity;

    invoke-interface {v3, v4, p0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1957540
    const v1, 0x48dd530a

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
