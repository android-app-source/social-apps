.class public LX/Dck;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Dck;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018860
    iput-object p1, p0, LX/Dck;->a:LX/0tX;

    .line 2018861
    iput-object p2, p0, LX/Dck;->b:LX/1Ck;

    .line 2018862
    return-void
.end method

.method public static a(LX/0QB;)LX/Dck;
    .locals 5

    .prologue
    .line 2018863
    sget-object v0, LX/Dck;->c:LX/Dck;

    if-nez v0, :cond_1

    .line 2018864
    const-class v1, LX/Dck;

    monitor-enter v1

    .line 2018865
    :try_start_0
    sget-object v0, LX/Dck;->c:LX/Dck;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2018866
    if-eqz v2, :cond_0

    .line 2018867
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2018868
    new-instance p0, LX/Dck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {p0, v3, v4}, LX/Dck;-><init>(LX/0tX;LX/1Ck;)V

    .line 2018869
    move-object v0, p0

    .line 2018870
    sput-object v0, LX/Dck;->c:LX/Dck;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2018871
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2018872
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2018873
    :cond_1
    sget-object v0, LX/Dck;->c:LX/Dck;

    return-object v0

    .line 2018874
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2018875
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
