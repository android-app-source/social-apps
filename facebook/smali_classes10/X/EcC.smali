.class public final LX/EcC;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EcA;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcC;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EcC;


# instance fields
.field public bitField0_:I

.field public iteration_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public seed_:LX/EWc;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2145800
    new-instance v0, LX/Ec9;

    invoke-direct {v0}, LX/Ec9;-><init>()V

    sput-object v0, LX/EcC;->a:LX/EWZ;

    .line 2145801
    new-instance v0, LX/EcC;

    invoke-direct {v0}, LX/EcC;-><init>()V

    .line 2145802
    sput-object v0, LX/EcC;->c:LX/EcC;

    invoke-direct {v0}, LX/EcC;->q()V

    .line 2145803
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2145795
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145796
    iput-byte v0, p0, LX/EcC;->memoizedIsInitialized:B

    .line 2145797
    iput v0, p0, LX/EcC;->memoizedSerializedSize:I

    .line 2145798
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2145799
    iput-object v0, p0, LX/EcC;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2145765
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145766
    iput-byte v0, p0, LX/EcC;->memoizedIsInitialized:B

    .line 2145767
    iput v0, p0, LX/EcC;->memoizedSerializedSize:I

    .line 2145768
    invoke-direct {p0}, LX/EcC;->q()V

    .line 2145769
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2145770
    const/4 v0, 0x0

    .line 2145771
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2145772
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2145773
    sparse-switch v3, :sswitch_data_0

    .line 2145774
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2145775
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2145776
    goto :goto_0

    .line 2145777
    :sswitch_1
    iget v3, p0, LX/EcC;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EcC;->bitField0_:I

    .line 2145778
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EcC;->iteration_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2145779
    :catch_0
    move-exception v0

    .line 2145780
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145781
    move-object v0, v0

    .line 2145782
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2145783
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EcC;->unknownFields:LX/EZQ;

    .line 2145784
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2145785
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EcC;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EcC;->bitField0_:I

    .line 2145786
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EcC;->seed_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2145787
    :catch_1
    move-exception v0

    .line 2145788
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2145789
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145790
    move-object v0, v1

    .line 2145791
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2145792
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcC;->unknownFields:LX/EZQ;

    .line 2145793
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2145794
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2145760
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2145761
    iput-byte v1, p0, LX/EcC;->memoizedIsInitialized:B

    .line 2145762
    iput v1, p0, LX/EcC;->memoizedSerializedSize:I

    .line 2145763
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcC;->unknownFields:LX/EZQ;

    .line 2145764
    return-void
.end method

.method private static a(LX/EcC;)LX/EcB;
    .locals 1

    .prologue
    .line 2145759
    invoke-static {}, LX/EcB;->u()LX/EcB;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EcB;->a(LX/EcC;)LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 2145735
    const/4 v0, 0x0

    iput v0, p0, LX/EcC;->iteration_:I

    .line 2145736
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcC;->seed_:LX/EWc;

    .line 2145737
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2145757
    new-instance v0, LX/EcB;

    invoke-direct {v0, p1}, LX/EcB;-><init>(LX/EYd;)V

    .line 2145758
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2145750
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2145751
    iget v0, p0, LX/EcC;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2145752
    iget v0, p0, LX/EcC;->iteration_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2145753
    :cond_0
    iget v0, p0, LX/EcC;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2145754
    iget-object v0, p0, LX/EcC;->seed_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2145755
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2145756
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2145804
    iget-byte v1, p0, LX/EcC;->memoizedIsInitialized:B

    .line 2145805
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2145806
    :goto_0
    return v0

    .line 2145807
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2145808
    :cond_1
    iput-byte v0, p0, LX/EcC;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2145740
    iget v0, p0, LX/EcC;->memoizedSerializedSize:I

    .line 2145741
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2145742
    :goto_0
    return v0

    .line 2145743
    :cond_0
    const/4 v0, 0x0

    .line 2145744
    iget v1, p0, LX/EcC;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2145745
    iget v0, p0, LX/EcC;->iteration_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2145746
    :cond_1
    iget v1, p0, LX/EcC;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2145747
    iget-object v1, p0, LX/EcC;->seed_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2145748
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2145749
    iput v0, p0, LX/EcC;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2145739
    iget-object v0, p0, LX/EcC;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2145738
    sget-object v0, LX/Eck;->z:LX/EYn;

    const-class v1, LX/EcC;

    const-class v2, LX/EcB;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2145734
    sget-object v0, LX/EcC;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2145733
    invoke-static {p0}, LX/EcC;->a(LX/EcC;)LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2145732
    invoke-static {}, LX/EcB;->u()LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2145731
    invoke-static {p0}, LX/EcC;->a(LX/EcC;)LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145730
    sget-object v0, LX/EcC;->c:LX/EcC;

    return-object v0
.end method
