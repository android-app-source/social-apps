.class public final LX/EPs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Ps;

.field public final synthetic b:LX/CzL;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;LX/1Ps;LX/CzL;)V
    .locals 0

    .prologue
    .line 2117915
    iput-object p1, p0, LX/EPs;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    iput-object p2, p0, LX/EPs;->a:LX/1Ps;

    iput-object p3, p0, LX/EPs;->b:LX/CzL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 17

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const v3, 0x7ade4444

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2117916
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPs;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    iget-object v12, v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->l:LX/CvY;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPs;->a:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v13

    sget-object v14, LX/8ch;->CLICK:LX/8ch;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPs;->b:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->d()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, LX/EPs;->b:LX/CzL;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPs;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->a(Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;)LX/CvY;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPs;->a:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPs;->b:LX/CzL;

    invoke-virtual {v2}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/EPs;->b:LX/CzL;

    invoke-virtual {v3}, LX/CzL;->l()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/EPs;->a:LX/1Ps;

    check-cast v4, LX/CxP;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/EPs;->b:LX/CzL;

    invoke-interface {v4, v5}, LX/CxP;->b(LX/CzL;)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/EPs;->b:LX/CzL;

    invoke-virtual {v5}, LX/CzL;->d()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/EPs;->b:LX/CzL;

    invoke-virtual {v6}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v6

    invoke-static {v6}, LX/8eM;->j(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/EPs;->b:LX/CzL;

    invoke-virtual {v7}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v7

    sget-object v8, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/EPs;->b:LX/CzL;

    invoke-virtual {v9}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/A52;

    invoke-interface {v9}, LX/A52;->dW_()Ljava/lang/String;

    move-result-object v9

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v10

    invoke-static/range {v1 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v1, v12

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    move-object/from16 v5, v16

    invoke-virtual/range {v1 .. v6}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2117917
    new-instance v2, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2117918
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPs;->b:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/A52;

    invoke-interface {v1}, LX/A52;->O()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2117919
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPs;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    iget-object v3, v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->h:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPs;->a:LX/1Ps;

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2117920
    const/4 v1, 0x2

    const/4 v2, 0x2

    const v3, 0x4b5b377f    # 1.4366591E7f

    invoke-static {v1, v2, v3, v11}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
