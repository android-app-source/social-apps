.class public LX/EMB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/EM9;

.field private final b:LX/3mL;


# direct methods
.method public constructor <init>(LX/EM9;LX/3mL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2110267
    iput-object p1, p0, LX/EMB;->a:LX/EM9;

    .line 2110268
    iput-object p2, p0, LX/EMB;->b:LX/3mL;

    .line 2110269
    return-void
.end method

.method public static a(LX/0QB;)LX/EMB;
    .locals 5

    .prologue
    .line 2110270
    const-class v1, LX/EMB;

    monitor-enter v1

    .line 2110271
    :try_start_0
    sget-object v0, LX/EMB;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110272
    sput-object v2, LX/EMB;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110273
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110274
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110275
    new-instance p0, LX/EMB;

    const-class v3, LX/EM9;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/EM9;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v4

    check-cast v4, LX/3mL;

    invoke-direct {p0, v3, v4}, LX/EMB;-><init>(LX/EM9;LX/3mL;)V

    .line 2110276
    move-object v0, p0

    .line 2110277
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110278
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EMB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110279
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110280
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/CxA;LX/CzL;)LX/1Dg;
    .locals 11
    .param p2    # LX/CxA;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/CzL;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/entities/SearchResultsPageModuleInterfaces$SearchResultsPageModule;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2110281
    iget-object v0, p3, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2110282
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bK()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    .line 2110283
    if-nez v0, :cond_1

    .line 2110284
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2110285
    :goto_0
    move-object v0, v0

    .line 2110286
    invoke-static {}, LX/1l6;->a()LX/1l6;

    move-result-object v3

    .line 2110287
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 2110288
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CzL;

    .line 2110289
    iget-object p3, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, p3

    .line 2110290
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    .line 2110291
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2110292
    :cond_0
    invoke-virtual {v3}, LX/1l6;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2110293
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v2

    .line 2110294
    iput-boolean v4, v2, LX/3mP;->a:Z

    .line 2110295
    move-object v2, v2

    .line 2110296
    const/16 v3, 0x8

    .line 2110297
    iput v3, v2, LX/3mP;->b:I

    .line 2110298
    move-object v2, v2

    .line 2110299
    invoke-static {v1}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 2110300
    iput-object v3, v2, LX/3mP;->d:LX/25L;

    .line 2110301
    move-object v2, v2

    .line 2110302
    new-instance v3, LX/EMA;

    invoke-direct {v3, p0, v1}, LX/EMA;-><init>(LX/EMB;Ljava/lang/String;)V

    .line 2110303
    iput-object v3, v2, LX/3mP;->e:LX/0jW;

    .line 2110304
    move-object v1, v2

    .line 2110305
    invoke-virtual {v1}, LX/3mP;->a()LX/25M;

    move-result-object v1

    .line 2110306
    iget-object v2, p0, LX/EMB;->a:LX/EM9;

    .line 2110307
    new-instance v5, LX/EM8;

    move-object v8, p2

    check-cast v8, LX/CxA;

    invoke-static {v2}, LX/ELq;->a(LX/0QB;)LX/ELq;

    move-result-object v10

    check-cast v10, LX/ELq;

    move-object v6, p1

    move-object v7, v0

    move-object v9, v1

    invoke-direct/range {v5 .. v10}, LX/EM8;-><init>(Landroid/content/Context;LX/0Px;LX/CxA;LX/25M;LX/ELq;)V

    .line 2110308
    move-object v0, v5

    .line 2110309
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0a0097

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    iget-object v2, p0, LX/EMB;->b:LX/3mL;

    invoke-virtual {v2, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v0

    .line 2110310
    iget-object v2, v0, LX/3ml;->a:LX/3mk;

    iput-boolean v4, v2, LX/3mk;->e:Z

    .line 2110311
    move-object v0, v0

    .line 2110312
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x4

    const v2, 0x7f0b1737

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    const v2, 0x7f0b1738

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x3

    const v2, 0x7f0b1739

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 2110313
    :cond_1
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2110314
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p3}, LX/CzL;->k()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 2110315
    invoke-static {p3, v0}, LX/CzM;->a(LX/CzL;I)LX/CzL;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2110316
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2110317
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0
.end method
