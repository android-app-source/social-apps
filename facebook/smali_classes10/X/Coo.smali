.class public LX/Coo;
.super LX/Coi;
.source ""

# interfaces
.implements LX/CoW;


# instance fields
.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:I


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1936107
    invoke-direct {p0, p1}, LX/Coi;-><init>(Landroid/view/View;)V

    .line 1936108
    const-class v0, LX/Coo;

    invoke-static {v0, p0}, LX/Coo;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936109
    iget-object v0, p0, LX/Coo;->a:LX/Ck0;

    iget-object v1, p0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v3, 0x7f0d011e

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1936110
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0622

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/Coo;->b:I

    .line 1936111
    return-void
.end method

.method public static a(Landroid/view/View;)LX/CoY;
    .locals 1

    .prologue
    .line 1936106
    new-instance v0, LX/Coo;

    invoke-direct {v0, p0}, LX/Coo;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Coo;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object p0

    check-cast p0, LX/Ck0;

    iput-object p0, p1, LX/Coo;->a:LX/Ck0;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1936105
    iget v0, p0, LX/Coo;->b:I

    return v0
.end method
