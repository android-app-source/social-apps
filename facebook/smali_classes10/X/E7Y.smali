.class public final LX/E7Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1U8;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;LX/1U8;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2081776
    iput-object p1, p0, LX/E7Y;->d:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iput-object p2, p0, LX/E7Y;->a:LX/1U8;

    iput-object p3, p0, LX/E7Y;->b:Ljava/lang/String;

    iput-object p4, p0, LX/E7Y;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x2f3502b8

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2081777
    iget-object v1, p0, LX/E7Y;->d:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v2, p0, LX/E7Y;->a:LX/1U8;

    .line 2081778
    :try_start_0
    invoke-interface {v2}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 2081779
    invoke-interface {v2}, LX/1U8;->j()LX/1Fb;

    move-result-object v6

    invoke-interface {v6}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v11

    .line 2081780
    iget-object v6, v1, LX/Cfk;->d:Landroid/content/Context;

    move-object v7, v6

    .line 2081781
    iget-object v10, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->i:[J

    invoke-static/range {v7 .. v11}, LX/E1i;->a(Landroid/content/Context;J[JLjava/lang/String;)LX/Cfl;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 2081782
    :goto_0
    move-object v1, v6

    .line 2081783
    if-eqz v1, :cond_0

    .line 2081784
    iget-object v2, p0, LX/E7Y;->d:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v3, p0, LX/E7Y;->b:Ljava/lang/String;

    iget-object v4, p0, LX/E7Y;->c:Ljava/lang/String;

    .line 2081785
    invoke-virtual {v2, v3, v4, v1}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2081786
    iget-object v2, p0, LX/E7Y;->d:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v3, p0, LX/E7Y;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v1, p1}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->a(Ljava/lang/String;LX/Cfl;Landroid/view/View;)V

    .line 2081787
    :cond_0
    const v1, 0x552deccd

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :catch_0
    const/4 v6, 0x0

    goto :goto_0
.end method
