.class public final LX/Ejj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2162496
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2162497
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2162498
    :goto_0
    return v1

    .line 2162499
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2162500
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2162501
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2162502
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2162503
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2162504
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2162505
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2162506
    :cond_2
    const-string v6, "mutual_friends"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2162507
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2162508
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v7, :cond_b

    .line 2162509
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2162510
    :goto_2
    move v3, v5

    .line 2162511
    goto :goto_1

    .line 2162512
    :cond_3
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2162513
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 2162514
    :cond_4
    const-string v6, "profile_picture"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2162515
    const/4 v5, 0x0

    .line 2162516
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_f

    .line 2162517
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2162518
    :goto_3
    move v0, v5

    .line 2162519
    goto :goto_1

    .line 2162520
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2162521
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2162522
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2162523
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2162524
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2162525
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 2162526
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 2162527
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2162528
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2162529
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_7

    if-eqz v8, :cond_7

    .line 2162530
    const-string v9, "count"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2162531
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v6

    goto :goto_4

    .line 2162532
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2162533
    :cond_9
    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2162534
    if-eqz v3, :cond_a

    .line 2162535
    invoke-virtual {p1, v5, v7, v5}, LX/186;->a(III)V

    .line 2162536
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_b
    move v3, v5

    move v7, v5

    goto :goto_4

    .line 2162537
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2162538
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_e

    .line 2162539
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2162540
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2162541
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_d

    if-eqz v6, :cond_d

    .line 2162542
    const-string v7, "uri"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2162543
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 2162544
    :cond_e
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2162545
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2162546
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_f
    move v0, v5

    goto :goto_5
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2162547
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2162548
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2162549
    if-eqz v0, :cond_0

    .line 2162550
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162551
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2162552
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2162553
    if-eqz v0, :cond_2

    .line 2162554
    const-string v1, "mutual_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162555
    const/4 v1, 0x0

    .line 2162556
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2162557
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2162558
    if-eqz v1, :cond_1

    .line 2162559
    const-string p3, "count"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162560
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2162561
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2162562
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2162563
    if-eqz v0, :cond_3

    .line 2162564
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162565
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2162566
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2162567
    if-eqz v0, :cond_5

    .line 2162568
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162569
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2162570
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2162571
    if-eqz v1, :cond_4

    .line 2162572
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162573
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2162574
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2162575
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2162576
    return-void
.end method
