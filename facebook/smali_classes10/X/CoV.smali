.class public final LX/CoV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/Crk;

.field private final c:LX/Cju;

.field public final d:LX/Crz;

.field public final e:LX/Chi;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalComposer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1935827
    const-class v0, LX/CoV;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CoV;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Crk;LX/Cju;LX/Crz;LX/Chi;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Crk;",
            "LX/Cju;",
            "LX/Crz;",
            "LX/Chi;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalComposer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1935820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1935821
    iput-object p1, p0, LX/CoV;->b:LX/Crk;

    .line 1935822
    iput-object p2, p0, LX/CoV;->c:LX/Cju;

    .line 1935823
    iput-object p3, p0, LX/CoV;->d:LX/Crz;

    .line 1935824
    iput-object p4, p0, LX/CoV;->e:LX/Chi;

    .line 1935825
    iput-object p5, p0, LX/CoV;->f:LX/0Ot;

    .line 1935826
    return-void
.end method

.method public static a(LX/0QB;)LX/CoV;
    .locals 9

    .prologue
    .line 1935809
    const-class v1, LX/CoV;

    monitor-enter v1

    .line 1935810
    :try_start_0
    sget-object v0, LX/CoV;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1935811
    sput-object v2, LX/CoV;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1935812
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935813
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1935814
    new-instance v3, LX/CoV;

    invoke-static {v0}, LX/Crk;->a(LX/0QB;)LX/Crk;

    move-result-object v4

    check-cast v4, LX/Crk;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v5

    check-cast v5, LX/Cju;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v6

    check-cast v6, LX/Crz;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v7

    check-cast v7, LX/Chi;

    const/16 v8, 0x3222

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/CoV;-><init>(LX/Crk;LX/Cju;LX/Crz;LX/Chi;LX/0Ot;)V

    .line 1935815
    move-object v0, v3

    .line 1935816
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1935817
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CoV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1935818
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1935819
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Cqw;
    .locals 2

    .prologue
    .line 1935805
    sget-object v0, LX/CoU;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1935806
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    :goto_0
    return-object v0

    .line 1935807
    :pswitch_0
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    goto :goto_0

    .line 1935808
    :pswitch_1
    sget-object v0, LX/Cqw;->b:LX/Cqw;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/1a1;)V
    .locals 3

    .prologue
    .line 1935796
    if-nez p0, :cond_1

    .line 1935797
    :cond_0
    return-void

    .line 1935798
    :cond_1
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    .line 1935799
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1935800
    :goto_0
    if-eqz v1, :cond_0

    .line 1935801
    instance-of v0, v1, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 1935802
    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 1935803
    new-instance v2, Lcom/facebook/richdocument/view/block/BlockViewUtil$1;

    invoke-direct {v2, v0}, Lcom/facebook/richdocument/view/block/BlockViewUtil$1;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 1935804
    :cond_2
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0
.end method

.method public static a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1935790
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1935791
    const-string v1, "block_media_type"

    const-string v2, "article"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1935792
    const-string v1, "ia_source"

    const-string v2, "share_button"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1935793
    const-string v1, "share_type"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1935794
    const-string v1, "feed_share_action"

    invoke-static {p0, v1, p2, v0}, LX/CoV;->a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1935795
    return-void
.end method

.method public static a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ckw;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1935785
    if-nez p3, :cond_0

    .line 1935786
    new-instance p3, Ljava/util/HashMap;

    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    .line 1935787
    :cond_0
    const-string v0, "session_id"

    invoke-interface {p3, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1935788
    invoke-virtual {p0, p1, p3}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 1935789
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;I)V
    .locals 2

    .prologue
    .line 1935719
    if-eqz p0, :cond_0

    .line 1935720
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1935721
    :cond_0
    return-void
.end method

.method public static a(Landroid/view/View;IIII)V
    .locals 1

    .prologue
    .line 1935780
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v0, :cond_0

    .line 1935781
    :goto_0
    return-void

    .line 1935782
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1935783
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1935784
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/richdocument/view/widget/PressStateButton;II)V
    .locals 1

    .prologue
    .line 1935776
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1935777
    invoke-static {v0, p1}, LX/CoV;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 1935778
    invoke-virtual {p0, p2}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setTextColor(I)V

    .line 1935779
    return-void
.end method

.method public static a(Lcom/facebook/richdocument/view/widget/PressStateButton;IIII)V
    .locals 0

    .prologue
    .line 1935772
    invoke-virtual {p0, p1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setText(I)V

    .line 1935773
    invoke-virtual {p0, p2}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageResource(I)V

    .line 1935774
    invoke-static {p0, p3, p4}, LX/CoV;->a(Lcom/facebook/richdocument/view/widget/PressStateButton;II)V

    .line 1935775
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/Chi;Ljava/lang/String;II)LX/CnN;
    .locals 3

    .prologue
    .line 1935761
    new-instance v1, LX/CnM;

    invoke-direct {v1, p3, p4}, LX/CnM;-><init>(Ljava/lang/String;I)V

    .line 1935762
    iput p5, v1, LX/CnM;->d:I

    .line 1935763
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1935764
    const-class v0, LX/0ew;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 1935765
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1935766
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    iget-object v2, p0, LX/CoV;->e:LX/Chi;

    .line 1935767
    iget-object p0, v2, LX/Chi;->i:Ljava/lang/String;

    move-object v2, p0

    .line 1935768
    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1935769
    if-eqz v0, :cond_0

    .line 1935770
    iput-object v0, v1, LX/CnM;->c:Landroid/support/v4/app/Fragment;

    .line 1935771
    :cond_0
    invoke-virtual {v1}, LX/CnM;->a()LX/CnN;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/CnR;
    .locals 2

    .prologue
    .line 1935757
    iget-object v0, p0, LX/CoV;->b:LX/Crk;

    const v1, 0x7f0d017b

    invoke-virtual {v0, v1}, LX/Crk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CnR;

    .line 1935758
    if-eqz v0, :cond_0

    .line 1935759
    invoke-virtual {p0, v0}, LX/CoV;->a(Landroid/view/View;)V

    .line 1935760
    :cond_0
    return-object v0
.end method

.method public final a(LX/Cq9;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)V
    .locals 8

    .prologue
    .line 1935736
    iget-object v0, p0, LX/CoV;->e:LX/Chi;

    .line 1935737
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v7, v1

    .line 1935738
    if-eqz p2, :cond_0

    .line 1935739
    sget-object v0, LX/ClT;->TITLE:LX/ClT;

    invoke-static {v0, p2, v7}, LX/ClU;->a(LX/ClT;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)LX/ClU;

    move-result-object v1

    .line 1935740
    iget-object v0, p0, LX/CoV;->b:LX/Crk;

    const v2, 0x7f0311fc

    invoke-virtual {v0, v2}, LX/Crk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    .line 1935741
    invoke-virtual {p0, v0}, LX/CoV;->a(Landroid/view/View;)V

    .line 1935742
    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setAnnotation(LX/ClU;)V

    .line 1935743
    invoke-interface {p1, v0}, LX/Cq9;->a(LX/CnQ;)V

    .line 1935744
    :cond_0
    if-eqz p3, :cond_1

    .line 1935745
    sget-object v0, LX/ClT;->SUBTITLE:LX/ClT;

    invoke-static {v0, p3, v7}, LX/ClU;->a(LX/ClT;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)LX/ClU;

    move-result-object v1

    .line 1935746
    iget-object v0, p0, LX/CoV;->b:LX/Crk;

    const v2, 0x7f0311fc

    invoke-virtual {v0, v2}, LX/Crk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    .line 1935747
    invoke-virtual {p0, v0}, LX/CoV;->a(Landroid/view/View;)V

    .line 1935748
    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setAnnotation(LX/ClU;)V

    .line 1935749
    invoke-interface {p1, v0}, LX/Cq9;->a(LX/CnQ;)V

    .line 1935750
    :cond_1
    if-eqz p4, :cond_2

    .line 1935751
    new-instance v0, LX/ClU;

    sget-object v1, LX/ClT;->COPYRIGHT:LX/ClT;

    invoke-virtual {p4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->a()LX/8Z4;

    move-result-object v3

    sget-object v4, LX/ClS;->MINI_LABEL:LX/ClS;

    invoke-virtual {p4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->c()Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;

    move-result-object v5

    invoke-static {v5}, LX/ClQ;->from(Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;)LX/ClQ;

    move-result-object v5

    invoke-virtual {p4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->e()Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    move-result-object v6

    invoke-static {v6}, LX/ClR;->from(Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)LX/ClR;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, LX/ClU;-><init>(LX/ClT;Ljava/lang/String;LX/8Z4;LX/ClS;LX/ClQ;LX/ClR;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    .line 1935752
    iget-object v1, p0, LX/CoV;->b:LX/Crk;

    const v2, 0x7f0311fc

    invoke-virtual {v1, v2}, LX/Crk;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    .line 1935753
    invoke-virtual {p0, v1}, LX/CoV;->a(Landroid/view/View;)V

    .line 1935754
    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setAnnotation(LX/ClU;)V

    .line 1935755
    invoke-interface {p1, v1}, LX/Cq9;->a(LX/CnQ;)V

    .line 1935756
    :cond_2
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 1935728
    iget-object v0, p0, LX/CoV;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1935729
    iget-object v0, p0, LX/CoV;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K20;

    .line 1935730
    sget-object v2, LX/21D;->INSTANT_ARTICLE:LX/21D;

    const-string v3, "shareAsNewPost"

    invoke-static {p3}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object p0

    iget-object v1, v0, LX/K20;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Chi;

    invoke-virtual {v1}, LX/Chi;->c()Ljava/lang/String;

    move-result-object v1

    .line 1935731
    iput-object v1, p0, LX/89G;->d:Ljava/lang/String;

    .line 1935732
    move-object v1, p0

    .line 1935733
    invoke-virtual {v1}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    invoke-static {v2, v3, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 1935734
    invoke-static {v0, p4, p2, v1, p1}, LX/K20;->a(LX/K20;ILjava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1935735
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1935722
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 1935723
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1935724
    iget-object v1, p0, LX/CoV;->c:LX/Cju;

    const v2, 0x7f0d0121

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    .line 1935725
    iget-object v2, p0, LX/CoV;->c:LX/Cju;

    const v3, 0x7f0d0122

    invoke-interface {v2, v3}, LX/Cju;->c(I)I

    move-result v2

    .line 1935726
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-static {p1, v1, v3, v2, v0}, LX/CoV;->a(Landroid/view/View;IIII)V

    .line 1935727
    :cond_0
    return-void
.end method
