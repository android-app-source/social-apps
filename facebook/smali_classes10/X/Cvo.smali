.class public final LX/Cvo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/util/Pair",
        "<",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:Z

.field public final synthetic d:LX/Cvp;


# direct methods
.method public constructor <init>(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;Z)V
    .locals 0

    .prologue
    .line 1949314
    iput-object p1, p0, LX/Cvo;->d:LX/Cvp;

    iput-object p2, p0, LX/Cvo;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    iput-object p3, p0, LX/Cvo;->b:Ljava/util/List;

    iput-boolean p4, p0, LX/Cvo;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1949315
    sget-object v0, LX/Cvp;->a:Ljava/lang/Class;

    const-string v1, "Couldn\'t add bootstrap info to logs: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1949316
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1949317
    check-cast p1, Landroid/util/Pair;

    .line 1949318
    iget-object v0, p0, LX/Cvo;->d:LX/Cvp;

    const-string v1, "suggestions_at_end_of_session"

    iget-object v2, p0, LX/Cvo;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v3, p0, LX/Cvo;->b:Ljava/util/List;

    iget-boolean v4, p0, LX/Cvo;->c:Z

    iget-object v5, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, LX/0Px;

    iget-object v6, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, LX/0Px;

    .line 1949319
    invoke-static/range {v0 .. v6}, LX/Cvp;->a$redex0(LX/Cvp;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;ZLX/0Px;LX/0Px;)V

    .line 1949320
    iget-object v0, p0, LX/Cvo;->d:LX/Cvp;

    iget-object v0, v0, LX/Cvp;->b:LX/0Zb;

    iget-object v1, p0, LX/Cvo;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1949321
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1949322
    iget-object v0, p0, LX/Cvo;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 1949323
    :cond_0
    return-void
.end method
