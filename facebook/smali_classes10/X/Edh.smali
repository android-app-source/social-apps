.class public LX/Edh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field private static c:LX/Edh;

.field private static final d:LX/Edt;

.field private static final e:[I

.field private static final f:[Ljava/lang/String;

.field private static final g:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Landroid/util/SparseIntArray;

.field private static final i:Landroid/util/SparseIntArray;

.field private static final j:Landroid/util/SparseIntArray;

.field private static final k:Landroid/util/SparseIntArray;

.field private static final l:Landroid/util/SparseIntArray;

.field private static final m:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final o:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final p:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final q:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final r:Landroid/content/Context;

.field private final s:Landroid/content/ContentResolver;

.field private final t:Landroid/telephony/TelephonyManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x9a

    const/16 v6, 0x96

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x4

    .line 2151217
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/Edh;->e:[I

    .line 2151218
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const-string v1, "msg_box"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "thread_id"

    aput-object v2, v0, v1

    const-string v1, "retr_txt"

    aput-object v1, v0, v5

    const-string v1, "sub"

    aput-object v1, v0, v3

    const/4 v1, 0x5

    const-string v2, "ct_l"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ct_t"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "m_cls"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "m_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "resp_txt"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "tr_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "ct_cls"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "d_rpt"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "m_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "pri"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "rr"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "read_status"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "rpt_a"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "retr_st"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "st"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "date"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "d_tm"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "exp"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "retr_txt_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "read"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "seen"

    aput-object v2, v0, v1

    sput-object v0, LX/Edh;->a:[Ljava/lang/String;

    .line 2151219
    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "normalized_date"

    aput-object v2, v0, v1

    sput-object v0, LX/Edh;->b:[Ljava/lang/String;

    .line 2151220
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const-string v1, "chset"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "cd"

    aput-object v2, v0, v1

    const-string v1, "cid"

    aput-object v1, v0, v5

    const-string v1, "cl"

    aput-object v1, v0, v3

    const/4 v1, 0x5

    const-string v2, "ct"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "fn"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "text"

    aput-object v2, v0, v1

    sput-object v0, LX/Edh;->f:[Ljava/lang/String;

    .line 2151221
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    .line 2151222
    sput-object v0, LX/Edh;->g:LX/01J;

    sget-object v1, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2151223
    sget-object v0, LX/Edh;->g:LX/01J;

    sget-object v1, Landroid/provider/Telephony$Mms$Sent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2151224
    sget-object v0, LX/Edh;->g:LX/01J;

    sget-object v1, Landroid/provider/Telephony$Mms$Draft;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2151225
    sget-object v0, LX/Edh;->g:LX/01J;

    sget-object v1, Landroid/provider/Telephony$Mms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2151226
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 2151227
    sput-object v0, LX/Edh;->h:Landroid/util/SparseIntArray;

    const/16 v1, 0x19

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151228
    sget-object v0, LX/Edh;->h:Landroid/util/SparseIntArray;

    const/16 v1, 0x1a

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151229
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 2151230
    sput-object v0, LX/Edh;->m:Landroid/util/SparseArray;

    const-string v1, "sub_cs"

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151231
    sget-object v0, LX/Edh;->m:Landroid/util/SparseArray;

    const-string v1, "retr_txt_cs"

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151232
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 2151233
    sput-object v0, LX/Edh;->i:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v7, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151234
    sget-object v0, LX/Edh;->i:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v6, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151235
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 2151236
    sput-object v0, LX/Edh;->n:Landroid/util/SparseArray;

    const-string v1, "retr_txt"

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151237
    sget-object v0, LX/Edh;->n:Landroid/util/SparseArray;

    const-string v1, "sub"

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151238
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 2151239
    sput-object v0, LX/Edh;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x83

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151240
    sget-object v0, LX/Edh;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x84

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151241
    sget-object v0, LX/Edh;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x8a

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151242
    sget-object v0, LX/Edh;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x8b

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151243
    sget-object v0, LX/Edh;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x93

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151244
    sget-object v0, LX/Edh;->j:Landroid/util/SparseIntArray;

    const/16 v1, 0x98

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151245
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 2151246
    sput-object v0, LX/Edh;->o:Landroid/util/SparseArray;

    const/16 v1, 0x83

    const-string v2, "ct_l"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151247
    sget-object v0, LX/Edh;->o:Landroid/util/SparseArray;

    const/16 v1, 0x84

    const-string v2, "ct_t"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151248
    sget-object v0, LX/Edh;->o:Landroid/util/SparseArray;

    const/16 v1, 0x8a

    const-string v2, "m_cls"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151249
    sget-object v0, LX/Edh;->o:Landroid/util/SparseArray;

    const/16 v1, 0x8b

    const-string v2, "m_id"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151250
    sget-object v0, LX/Edh;->o:Landroid/util/SparseArray;

    const/16 v1, 0x93

    const-string v2, "resp_txt"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151251
    sget-object v0, LX/Edh;->o:Landroid/util/SparseArray;

    const/16 v1, 0x98

    const-string v2, "tr_id"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151252
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 2151253
    sput-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0xba

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151254
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0x86

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151255
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0x8c

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151256
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0x8d

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151257
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0x8f

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151258
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0x90

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151259
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0x9b

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151260
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0x91

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151261
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0x99

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151262
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    const/16 v1, 0x95

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151263
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 2151264
    sput-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0xba

    const-string v2, "ct_cls"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151265
    sget-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0x86

    const-string v2, "d_rpt"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151266
    sget-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0x8c

    const-string v2, "m_type"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151267
    sget-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0x8d

    const-string v2, "v"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151268
    sget-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0x8f

    const-string v2, "pri"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151269
    sget-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0x90

    const-string v2, "rr"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151270
    sget-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0x9b

    const-string v2, "read_status"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151271
    sget-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0x91

    const-string v2, "rpt_a"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151272
    sget-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0x99

    const-string v2, "retr_st"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151273
    sget-object v0, LX/Edh;->p:Landroid/util/SparseArray;

    const/16 v1, 0x95

    const-string v2, "st"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151274
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 2151275
    sput-object v0, LX/Edh;->l:Landroid/util/SparseIntArray;

    const/16 v1, 0x85

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151276
    sget-object v0, LX/Edh;->l:Landroid/util/SparseIntArray;

    const/16 v1, 0x87

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151277
    sget-object v0, LX/Edh;->l:Landroid/util/SparseIntArray;

    const/16 v1, 0x88

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151278
    sget-object v0, LX/Edh;->l:Landroid/util/SparseIntArray;

    const/16 v1, 0x8e

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 2151279
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 2151280
    sput-object v0, LX/Edh;->q:Landroid/util/SparseArray;

    const/16 v1, 0x85

    const-string v2, "date"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151281
    sget-object v0, LX/Edh;->q:Landroid/util/SparseArray;

    const/16 v1, 0x87

    const-string v2, "d_tm"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151282
    sget-object v0, LX/Edh;->q:Landroid/util/SparseArray;

    const/16 v1, 0x88

    const-string v2, "exp"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151283
    sget-object v0, LX/Edh;->q:Landroid/util/SparseArray;

    const/16 v1, 0x8e

    const-string v2, "m_size"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151284
    invoke-static {}, LX/Edt;->b()LX/Edt;

    move-result-object v0

    sput-object v0, LX/Edh;->d:LX/Edt;

    .line 2151285
    return-void

    nop

    :array_0
    .array-data 4
        0x81
        0x82
        0x89
        0x97
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2151286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2151287
    iput-object p1, p0, LX/Edh;->r:Landroid/content/Context;

    .line 2151288
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    .line 2151289
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, LX/Edh;->t:Landroid/telephony/TelephonyManager;

    .line 2151290
    return-void
.end method

.method private static a(ILX/Ede;LX/EdY;)LX/EdM;
    .locals 3

    .prologue
    .line 2151291
    packed-switch p0, :pswitch_data_0

    .line 2151292
    new-instance v0, LX/EdT;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized PDU type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2151293
    :pswitch_0
    new-instance v0, LX/EdW;

    invoke-direct {v0, p1}, LX/EdW;-><init>(LX/Ede;)V

    .line 2151294
    :goto_0
    return-object v0

    .line 2151295
    :pswitch_1
    new-instance v0, LX/EdR;

    invoke-direct {v0, p1}, LX/EdR;-><init>(LX/Ede;)V

    goto :goto_0

    .line 2151296
    :pswitch_2
    new-instance v0, LX/Edj;

    invoke-direct {v0, p1}, LX/Edj;-><init>(LX/Ede;)V

    goto :goto_0

    .line 2151297
    :pswitch_3
    new-instance v0, LX/Edl;

    invoke-direct {v0, p1, p2}, LX/Edl;-><init>(LX/Ede;LX/EdY;)V

    goto :goto_0

    .line 2151298
    :pswitch_4
    new-instance v0, LX/Edn;

    invoke-direct {v0, p1, p2}, LX/Edn;-><init>(LX/Ede;LX/EdY;)V

    goto :goto_0

    .line 2151299
    :pswitch_5
    new-instance v0, LX/EdN;

    invoke-direct {v0, p1}, LX/EdN;-><init>(LX/Ede;)V

    goto :goto_0

    .line 2151300
    :pswitch_6
    new-instance v0, LX/EdX;

    invoke-direct {v0, p1}, LX/EdX;-><init>(LX/Ede;)V

    goto :goto_0

    .line 2151301
    :pswitch_7
    new-instance v0, LX/Edk;

    invoke-direct {v0, p1}, LX/Edk;-><init>(LX/Ede;)V

    goto :goto_0

    .line 2151302
    :pswitch_8
    new-instance v0, LX/EdT;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported PDU type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x80
        :pswitch_4
        :pswitch_8
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_7
        :pswitch_2
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;)LX/Edh;
    .locals 1

    .prologue
    .line 2151303
    sget-object v0, LX/Edh;->c:LX/Edh;

    if-eqz v0, :cond_0

    sget-object v0, LX/Edh;->c:LX/Edh;

    iget-object v0, v0, LX/Edh;->r:Landroid/content/Context;

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2151304
    :cond_0
    new-instance v0, LX/Edh;

    invoke-direct {v0, p0}, LX/Edh;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/Edh;->c:LX/Edh;

    .line 2151305
    :cond_1
    sget-object v0, LX/Edh;->c:LX/Edh;

    return-object v0
.end method

.method private static a(LX/Edg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2151306
    invoke-virtual {p0}, LX/Edg;->g()[B

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/Edg;->g()[B

    move-result-object v0

    invoke-static {v0}, LX/Edh;->a([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2151307
    if-eqz p1, :cond_8

    .line 2151308
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 2151309
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2151310
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 2151311
    :cond_1
    :goto_0
    return-object v0

    .line 2151312
    :cond_2
    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2151313
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2151314
    :cond_3
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2151315
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 2151316
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2151317
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2151318
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given Uri could not be found in media store"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2151319
    :catch_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given Uri is not formatted in a way so that it can be found in media store."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2151320
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 2151321
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 2151322
    :cond_6
    :try_start_2
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 2151323
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2151324
    if-eqz v6, :cond_1

    .line 2151325
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2151326
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given Uri scheme is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object v0, v6

    goto :goto_0
.end method

.method private static a([B)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2151327
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "iso-8859-1"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2151328
    :goto_0
    return-object v0

    .line 2151329
    :catch_0
    move-exception v0

    .line 2151330
    const-string v1, "PduPersister"

    const-string v2, "ISO_8859_1 must be supported!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2151331
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(ILjava/util/HashSet;Landroid/util/SparseArray;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/util/SparseArray",
            "<[",
            "LX/EdS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2151332
    invoke-virtual {p2, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EdS;

    .line 2151333
    if-nez v0, :cond_1

    .line 2151334
    :cond_0
    return-void

    .line 2151335
    :cond_1
    array-length v4, v0

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 2151336
    if-eqz v1, :cond_2

    .line 2151337
    invoke-virtual {v1}, LX/EdS;->c()Ljava/lang/String;

    move-result-object v2

    .line 2151338
    invoke-static {v2}, LX/2UG;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2151339
    if-eqz v1, :cond_3

    .line 2151340
    :goto_1
    invoke-virtual {p1, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2151341
    invoke-virtual {p1, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2151342
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_3
    move-object v1, v2

    goto :goto_1
.end method

.method private a(JI[LX/EdS;)V
    .locals 7

    .prologue
    .line 2151343
    new-instance v1, Landroid/content/ContentValues;

    const/4 v0, 0x3

    invoke-direct {v1, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 2151344
    array-length v2, p4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p4, v0

    .line 2151345
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 2151346
    const-string v4, "address"

    invoke-virtual {v3}, LX/EdS;->b()[B

    move-result-object v5

    invoke-static {v5}, LX/Edh;->a([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151347
    const-string v4, "charset"

    .line 2151348
    iget v5, v3, LX/EdS;->a:I

    move v3, v5

    .line 2151349
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2151350
    const-string v3, "type"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2151351
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "content://mms/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/addr"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2151352
    iget-object v4, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v5, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    invoke-static {v4, v5, v3, v1}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2151353
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2151354
    :cond_0
    return-void
.end method

.method private a(JLX/Ede;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 2151355
    iget-object v0, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v1, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "content://mms/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/addr"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string v5, "address"

    aput-object v5, v3, v7

    const-string v5, "charset"

    aput-object v5, v3, v6

    const-string v5, "type"

    aput-object v5, v3, v8

    move-object v5, v4

    move-object v6, v4

    invoke-static/range {v0 .. v6}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2151356
    if-eqz v1, :cond_2

    .line 2151357
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2151358
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2151359
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2151360
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 2151361
    sparse-switch v2, :sswitch_data_0

    .line 2151362
    const-string v0, "PduPersister"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown address type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2151363
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2151364
    :sswitch_0
    :try_start_1
    new-instance v3, LX/EdS;

    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v0}, LX/Edh;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v3, v4, v0}, LX/EdS;-><init>(I[B)V

    invoke-virtual {p3, v3, v2}, LX/Ede;->a(LX/EdS;I)V

    goto :goto_0

    .line 2151365
    :sswitch_1
    new-instance v3, LX/EdS;

    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v0}, LX/Edh;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v3, v4, v0}, LX/EdS;-><init>(I[B)V

    invoke-virtual {p3, v3, v2}, LX/Ede;->b(LX/EdS;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2151366
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2151367
    :cond_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x81 -> :sswitch_1
        0x82 -> :sswitch_1
        0x89 -> :sswitch_0
        0x97 -> :sswitch_1
    .end sparse-switch
.end method

.method private static a(LX/Edg;Landroid/content/ContentValues;)V
    .locals 3

    .prologue
    .line 2151368
    invoke-virtual {p0}, LX/Edg;->j()[B

    move-result-object v0

    .line 2151369
    if-eqz v0, :cond_0

    .line 2151370
    const-string v1, "fn"

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151371
    :cond_0
    invoke-virtual {p0}, LX/Edg;->i()[B

    move-result-object v0

    .line 2151372
    if-eqz v0, :cond_1

    .line 2151373
    const-string v1, "name"

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151374
    :cond_1
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0xc5

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    move-object v0, v0

    .line 2151375
    if-eqz v0, :cond_2

    .line 2151376
    const-string v1, "cd"

    invoke-static {v0}, LX/Edh;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151377
    :cond_2
    invoke-virtual {p0}, LX/Edg;->c()[B

    move-result-object v0

    .line 2151378
    if-eqz v0, :cond_3

    .line 2151379
    const-string v1, "cid"

    invoke-static {v0}, LX/Edh;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151380
    :cond_3
    invoke-virtual {p0}, LX/Edg;->e()[B

    move-result-object v0

    .line 2151381
    if-eqz v0, :cond_4

    .line 2151382
    const-string v1, "cl"

    invoke-static {v0}, LX/Edh;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151383
    :cond_4
    return-void
.end method

.method private static a(LX/Edh;LX/Edg;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Edg;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2151120
    const/4 v6, 0x0

    .line 2151121
    const/4 v5, 0x0

    .line 2151122
    const/4 v4, 0x0

    .line 2151123
    const/4 v3, 0x0

    .line 2151124
    :try_start_0
    invoke-virtual {p1}, LX/Edg;->a()[B

    move-result-object v7

    .line 2151125
    invoke-virtual {p1}, LX/Edg;->d()I

    move-result v2

    .line 2151126
    const-string v8, "text/plain"

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "application/smil"

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "text/html"

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2151127
    :cond_0
    if-eqz v7, :cond_12

    .line 2151128
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 2151129
    const-string v9, "text"

    new-instance v10, LX/EdS;

    invoke-direct {v10, v2, v7}, LX/EdS;-><init>(I[B)V

    invoke-virtual {v10}, LX/EdS;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v9, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151130
    iget-object v2, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v8, v7, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v7, 0x1

    if-eq v2, v7, :cond_12

    .line 2151131
    new-instance v2, LX/EdT;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unable to update "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2151132
    :catch_0
    move-exception v2

    .line 2151133
    :try_start_1
    const-string v7, "PduPersister"

    const-string v8, "Failed with SQLiteException."

    invoke-static {v7, v8, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2151134
    new-instance v7, LX/EdT;

    invoke-direct {v7, v2}, LX/EdT;-><init>(Ljava/lang/Throwable;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2151135
    :catchall_0
    move-exception v2

    move-object v8, v2

    if-eqz v6, :cond_1

    .line 2151136
    :try_start_2
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    .line 2151137
    :cond_1
    :goto_0
    if-eqz v5, :cond_2

    .line 2151138
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8

    .line 2151139
    :cond_2
    :goto_1
    if-eqz v4, :cond_3

    .line 2151140
    invoke-virtual {v4, v3}, LX/Edr;->a(Ljava/lang/String;)I

    .line 2151141
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2151142
    new-instance v5, Landroid/content/ContentValues;

    const/4 v2, 0x0

    invoke-direct {v5, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 2151143
    iget-object v2, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v3, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "content://mms/resetFilePerm/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2151144
    :cond_3
    throw v8

    .line 2151145
    :cond_4
    :try_start_4
    invoke-static/range {p3 .. p3}, LX/Edq;->a(Ljava/lang/String;)Z
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v8

    .line 2151146
    if-eqz v8, :cond_7

    .line 2151147
    if-eqz p2, :cond_6

    .line 2151148
    :try_start_5
    iget-object v2, p0, LX/Edh;->r:Landroid/content/Context;

    move-object/from16 v0, p2

    invoke-static {v2, v0}, LX/Edh;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 2151149
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2151150
    invoke-virtual {v2}, Ljava/io/File;->length()J
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-wide v10

    .line 2151151
    const-wide/16 v12, 0x0

    cmp-long v2, v10, v12

    if-lez v2, :cond_6

    .line 2151152
    :cond_5
    :goto_2
    return-void

    .line 2151153
    :catch_1
    move-exception v2

    .line 2151154
    :try_start_6
    const-string v9, "PduPersister"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Can\'t get file info for: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/Edg;->b()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2151155
    :cond_6
    iget-object v2, p0, LX/Edh;->r:Landroid/content/Context;

    move-object/from16 v0, p3

    invoke-static {v2, v0}, LX/Edr;->a(Landroid/content/Context;Ljava/lang/String;)LX/Edr;

    move-result-object v4

    .line 2151156
    if-nez v4, :cond_7

    .line 2151157
    new-instance v2, LX/EdT;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Mimetype "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " can not be converted."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2151158
    :catch_2
    move-exception v2

    .line 2151159
    :try_start_7
    const-string v7, "PduPersister"

    const-string v8, "Failed to open Input/Output stream."

    invoke-static {v7, v8, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2151160
    new-instance v7, LX/EdT;

    invoke-direct {v7, v2}, LX/EdT;-><init>(Ljava/lang/Throwable;)V

    throw v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2151161
    :cond_7
    :try_start_8
    iget-object v2, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v6

    .line 2151162
    if-nez v6, :cond_8

    .line 2151163
    new-instance v2, LX/EdT;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Failed to create output stream on "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 2151164
    :catch_3
    move-exception v2

    .line 2151165
    :try_start_9
    const-string v7, "PduPersister"

    const-string v8, "Failed to read/write data."

    invoke-static {v7, v8, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2151166
    new-instance v7, LX/EdT;

    invoke-direct {v7, v2}, LX/EdT;-><init>(Ljava/lang/Throwable;)V

    throw v7
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2151167
    :cond_8
    if-nez v7, :cond_11

    .line 2151168
    :try_start_a
    invoke-virtual {p1}, LX/Edg;->b()Landroid/net/Uri;

    move-result-object v7

    .line 2151169
    if-eqz v7, :cond_9

    move-object/from16 v0, p2

    if-ne v7, v0, :cond_b

    .line 2151170
    :cond_9
    const-string v2, "PduPersister"

    const-string v7, "Can\'t find data for this part."

    invoke-static {v2, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 2151171
    if-eqz v6, :cond_a

    .line 2151172
    :try_start_b
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    .line 2151173
    :cond_a
    :goto_3
    if-eqz v4, :cond_5

    .line 2151174
    invoke-virtual {v4, v3}, LX/Edr;->a(Ljava/lang/String;)I

    .line 2151175
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2151176
    new-instance v5, Landroid/content/ContentValues;

    const/4 v2, 0x0

    invoke-direct {v5, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 2151177
    iget-object v2, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v3, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "content://mms/resetFilePerm/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2151178
    :catch_4
    move-exception v2

    .line 2151179
    const-string v5, "PduPersister"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "IOException while closing: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 2151180
    :cond_b
    if-eqz p4, :cond_c

    :try_start_c
    move-object/from16 v0, p4

    invoke-interface {v0, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2151181
    move-object/from16 v0, p4

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/InputStream;

    move-object v5, v2

    .line 2151182
    :cond_c
    if-nez v5, :cond_d

    .line 2151183
    iget-object v2, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    invoke-virtual {v2, v7}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    .line 2151184
    :cond_d
    if-nez v5, :cond_e

    .line 2151185
    new-instance v2, LX/EdT;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failed to create input stream on "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2151186
    :cond_e
    const/16 v2, 0x2000

    new-array v2, v2, [B

    .line 2151187
    :goto_4
    invoke-virtual {v5, v2}, Ljava/io/InputStream;->read([B)I

    move-result v7

    const/4 v9, -0x1

    if-eq v7, v9, :cond_12

    .line 2151188
    if-nez v8, :cond_f

    .line 2151189
    const/4 v9, 0x0

    invoke-virtual {v6, v2, v9, v7}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_4

    .line 2151190
    :cond_f
    invoke-virtual {v4, v2, v7}, LX/Edr;->a([BI)[B

    move-result-object v7

    .line 2151191
    if-eqz v7, :cond_10

    .line 2151192
    const/4 v9, 0x0

    array-length v10, v7

    invoke-virtual {v6, v7, v9, v10}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_4

    .line 2151193
    :cond_10
    new-instance v2, LX/EdT;

    const-string v7, "Error converting drm data."

    invoke-direct {v2, v7}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2151194
    :cond_11
    if-nez v8, :cond_15

    .line 2151195
    invoke-virtual {v6, v7}, Ljava/io/OutputStream;->write([B)V
    :try_end_c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 2151196
    :cond_12
    :goto_5
    if-eqz v6, :cond_13

    .line 2151197
    :try_start_d
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5

    .line 2151198
    :cond_13
    :goto_6
    if-eqz v5, :cond_14

    .line 2151199
    :try_start_e
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6

    .line 2151200
    :cond_14
    :goto_7
    if-eqz v4, :cond_5

    .line 2151201
    invoke-virtual {v4, v3}, LX/Edr;->a(Ljava/lang/String;)I

    .line 2151202
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2151203
    new-instance v5, Landroid/content/ContentValues;

    const/4 v2, 0x0

    invoke-direct {v5, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 2151204
    iget-object v2, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v3, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "content://mms/resetFilePerm/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2151205
    :cond_15
    :try_start_f
    array-length v2, v7

    invoke-virtual {v4, v7, v2}, LX/Edr;->a([BI)[B

    move-result-object v2

    .line 2151206
    if-eqz v2, :cond_16

    .line 2151207
    const/4 v7, 0x0

    array-length v8, v2

    invoke-virtual {v6, v2, v7, v8}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_5

    .line 2151208
    :cond_16
    new-instance v2, LX/EdT;

    const-string v7, "Error converting drm data."

    invoke-direct {v2, v7}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 2151209
    :catch_5
    move-exception v2

    .line 2151210
    const-string v7, "PduPersister"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "IOException while closing: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 2151211
    :catch_6
    move-exception v2

    .line 2151212
    const-string v6, "PduPersister"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "IOException while closing: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 2151213
    :catch_7
    move-exception v2

    .line 2151214
    const-string v7, "PduPersister"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "IOException while closing: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2151215
    :catch_8
    move-exception v2

    .line 2151216
    const-string v6, "PduPersister"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "IOException while closing: "

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method private static a(Landroid/database/Cursor;LX/Ede;)V
    .locals 6

    .prologue
    .line 2151384
    sget-object v0, LX/Edh;->i:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_1

    .line 2151385
    sget-object v1, LX/Edh;->i:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    sget-object v2, LX/Edh;->i:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    .line 2151386
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2151387
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 2151388
    sget-object v4, LX/Edh;->h:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    .line 2151389
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 2151390
    new-instance v5, LX/EdS;

    invoke-static {v3}, LX/Edh;->a(Ljava/lang/String;)[B

    move-result-object v3

    invoke-direct {v5, v4, v3}, LX/EdS;-><init>(I[B)V

    .line 2151391
    invoke-virtual {p1, v5, v2}, LX/Ede;->a(LX/EdS;I)V

    .line 2151392
    :cond_0
    goto :goto_0

    .line 2151393
    :cond_1
    sget-object v0, LX/Edh;->j:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    :goto_1
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_3

    .line 2151394
    sget-object v1, LX/Edh;->j:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    sget-object v2, LX/Edh;->j:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    .line 2151395
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2151396
    if-eqz v3, :cond_2

    .line 2151397
    invoke-static {v3}, LX/Edh;->a(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {p1, v3, v2}, LX/Ede;->a([BI)V

    .line 2151398
    :cond_2
    goto :goto_1

    .line 2151399
    :cond_3
    sget-object v0, LX/Edh;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    :goto_2
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_5

    .line 2151400
    sget-object v1, LX/Edh;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    sget-object v2, LX/Edh;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    .line 2151401
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2151402
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 2151403
    invoke-virtual {p1, v3, v2}, LX/Ede;->a(II)V

    .line 2151404
    :cond_4
    goto :goto_2

    .line 2151405
    :cond_5
    sget-object v0, LX/Edh;->l:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    :goto_3
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_7

    .line 2151406
    sget-object v1, LX/Edh;->l:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    sget-object v2, LX/Edh;->l:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    .line 2151407
    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2151408
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 2151409
    invoke-virtual {p1, v3, v4, v2}, LX/Ede;->a(JI)V

    .line 2151410
    :cond_6
    goto :goto_3

    .line 2151411
    :cond_7
    return-void
.end method

.method private a(Landroid/net/Uri;LX/Edg;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/Edg;",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2151104
    new-instance v3, Landroid/content/ContentValues;

    const/4 v0, 0x7

    invoke-direct {v3, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 2151105
    invoke-virtual {p2}, LX/Edg;->d()I

    move-result v0

    .line 2151106
    if-eqz v0, :cond_0

    .line 2151107
    const-string v1, "chset"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2151108
    :cond_0
    invoke-virtual {p2}, LX/Edg;->g()[B

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2151109
    invoke-virtual {p2}, LX/Edg;->g()[B

    move-result-object v0

    invoke-static {v0}, LX/Edh;->a([B)Ljava/lang/String;

    move-result-object v6

    .line 2151110
    const-string v0, "ct"

    invoke-virtual {v3, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151111
    invoke-static {p2, v3}, LX/Edh;->a(LX/Edg;Landroid/content/ContentValues;)V

    .line 2151112
    iget-object v0, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v1, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    move-object v2, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2151113
    iget-object v0, p2, LX/Edg;->f:[B

    move-object v0, v0

    .line 2151114
    if-nez v0, :cond_1

    .line 2151115
    iget-object v0, p2, LX/Edg;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 2151116
    if-eq p1, v0, :cond_2

    .line 2151117
    :cond_1
    invoke-static {p0, p2, p1, v6, p3}, LX/Edh;->a(LX/Edh;LX/Edg;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)V

    .line 2151118
    :cond_2
    return-void

    .line 2151119
    :cond_3
    new-instance v0, LX/EdT;

    const-string v1, "MIME type of the part must be set."

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/util/HashSet;Landroid/util/SparseArray;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/util/SparseArray",
            "<[",
            "LX/EdS;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2151084
    const/16 v0, 0x97

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EdS;

    .line 2151085
    const/16 v1, 0x82

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/EdS;

    .line 2151086
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2151087
    if-eqz v0, :cond_1

    .line 2151088
    array-length v5, v0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v6, v0, v3

    .line 2151089
    if-eqz v6, :cond_0

    .line 2151090
    invoke-virtual {v6}, LX/EdS;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2151091
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2151092
    :cond_1
    if-eqz v1, :cond_3

    .line 2151093
    array-length v3, v1

    move v0, v2

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v2, v1, v0

    .line 2151094
    if-eqz v2, :cond_2

    .line 2151095
    invoke-virtual {v2}, LX/EdS;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2151096
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2151097
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2151098
    invoke-static {v0}, LX/2UG;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2151099
    if-eqz v1, :cond_6

    move-object v0, v1

    .line 2151100
    :cond_5
    :goto_3
    invoke-virtual {p0, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2151101
    invoke-virtual {p0, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2151102
    :cond_6
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {v0, p2}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_3

    .line 2151103
    :cond_7
    return-void
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 2150721
    :try_start_0
    const-string v0, "iso-8859-1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2150722
    :goto_0
    return-object v0

    .line 2150723
    :catch_0
    move-exception v0

    .line 2150724
    const-string v1, "PduPersister"

    const-string v2, "ISO_8859_1 must be supported!"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2150725
    const/4 v0, 0x0

    new-array v0, v0, [B

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;I)[B
    .locals 1

    .prologue
    .line 2151081
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2151082
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Edh;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 2151083
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/Edh;J)[LX/Edg;
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 2151006
    iget-object v0, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v1, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "content://mms/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/part"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, LX/Edh;->f:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-static/range {v0 .. v6}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 2151007
    if-eqz v5, :cond_0

    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 2151008
    :cond_0
    if-eqz v5, :cond_1

    .line 2151009
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 2151010
    :cond_1
    :goto_0
    return-object v4

    .line 2151011
    :cond_2
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 2151012
    new-array v0, v0, [LX/Edg;

    move v3, v7

    .line 2151013
    :goto_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2151014
    new-instance v6, LX/Edg;

    invoke-direct {v6}, LX/Edg;-><init>()V

    .line 2151015
    const/4 v1, 0x1

    .line 2151016
    invoke-interface {v5, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_15

    .line 2151017
    invoke-interface {v5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2151018
    :goto_2
    move-object v2, v2

    .line 2151019
    if-eqz v2, :cond_3

    .line 2151020
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v6, v1}, LX/Edg;->a(I)V

    .line 2151021
    :cond_3
    const/4 v1, 0x2

    invoke-static {v5, v1}, LX/Edh;->b(Landroid/database/Cursor;I)[B

    move-result-object v1

    .line 2151022
    if-eqz v1, :cond_4

    .line 2151023
    invoke-virtual {v6, v1}, LX/Edg;->d([B)V

    .line 2151024
    :cond_4
    const/4 v1, 0x3

    invoke-static {v5, v1}, LX/Edh;->b(Landroid/database/Cursor;I)[B

    move-result-object v1

    .line 2151025
    if-eqz v1, :cond_5

    .line 2151026
    invoke-virtual {v6, v1}, LX/Edg;->b([B)V

    .line 2151027
    :cond_5
    const/4 v1, 0x4

    invoke-static {v5, v1}, LX/Edh;->b(Landroid/database/Cursor;I)[B

    move-result-object v1

    .line 2151028
    if-eqz v1, :cond_6

    .line 2151029
    invoke-virtual {v6, v1}, LX/Edg;->c([B)V

    .line 2151030
    :cond_6
    const/4 v1, 0x5

    invoke-static {v5, v1}, LX/Edh;->b(Landroid/database/Cursor;I)[B

    move-result-object v1

    .line 2151031
    if-eqz v1, :cond_c

    .line 2151032
    invoke-virtual {v6, v1}, LX/Edg;->e([B)V

    .line 2151033
    const/4 v7, 0x6

    invoke-static {v5, v7}, LX/Edh;->b(Landroid/database/Cursor;I)[B

    move-result-object v7

    .line 2151034
    if-eqz v7, :cond_7

    .line 2151035
    invoke-virtual {v6, v7}, LX/Edg;->h([B)V

    .line 2151036
    :cond_7
    const/4 v7, 0x7

    invoke-static {v5, v7}, LX/Edh;->b(Landroid/database/Cursor;I)[B

    move-result-object v7

    .line 2151037
    if-eqz v7, :cond_8

    .line 2151038
    invoke-virtual {v6, v7}, LX/Edg;->g([B)V

    .line 2151039
    :cond_8
    const/4 v7, 0x0

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 2151040
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v10, "content://mms/part/"

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 2151041
    iput-object v7, v6, LX/Edg;->e:Landroid/net/Uri;

    .line 2151042
    invoke-static {v1}, LX/Edh;->a([B)Ljava/lang/String;

    move-result-object v1

    .line 2151043
    invoke-static {v1}, LX/EdQ;->b(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    invoke-static {v1}, LX/EdQ;->c(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    invoke-static {v1}, LX/EdQ;->d(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 2151044
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2151045
    const-string v9, "text/plain"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_9

    const-string v9, "application/smil"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_9

    const-string v9, "text/html"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2151046
    :cond_9
    const/16 v1, 0x8

    invoke-interface {v5, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2151047
    new-instance v7, LX/EdS;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_3
    if-eqz v1, :cond_f

    :goto_4
    invoke-direct {v7, v2, v1}, LX/EdS;-><init>(ILjava/lang/String;)V

    invoke-virtual {v7}, LX/EdS;->b()[B

    move-result-object v1

    .line 2151048
    const/4 v2, 0x0

    array-length v7, v1

    invoke-virtual {v8, v1, v2, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 2151049
    :cond_a
    :goto_5
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 2151050
    iput-object v1, v6, LX/Edg;->f:[B

    .line 2151051
    :cond_b
    add-int/lit8 v1, v3, 0x1

    aput-object v6, v0, v3

    move v3, v1

    .line 2151052
    goto/16 :goto_1

    .line 2151053
    :cond_c
    new-instance v0, LX/EdT;

    const-string v1, "Content-Type must be set."

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2151054
    :catchall_0
    move-exception v0

    if-eqz v5, :cond_d

    .line 2151055
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v0

    .line 2151056
    :cond_e
    const/16 v2, 0x6a

    goto :goto_3

    :cond_f
    :try_start_2
    const-string v1, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 2151057
    :cond_10
    :try_start_3
    iget-object v1, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    invoke-virtual {v1, v7}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v1

    .line 2151058
    const/16 v2, 0x100

    :try_start_4
    new-array v7, v2, [B

    .line 2151059
    invoke-virtual {v1, v7}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 2151060
    :goto_6
    if-ltz v2, :cond_11

    .line 2151061
    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 2151062
    invoke-virtual {v1, v7}, Ljava/io/InputStream;->read([B)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v2

    goto :goto_6

    .line 2151063
    :cond_11
    if-eqz v1, :cond_a

    .line 2151064
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_5

    .line 2151065
    :catch_0
    move-exception v1

    .line 2151066
    :try_start_6
    const-string v2, "PduPersister"

    const-string v7, "Failed to close stream"

    invoke-static {v2, v7, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    .line 2151067
    :catch_1
    move-exception v0

    .line 2151068
    :goto_7
    :try_start_7
    const-string v1, "PduPersister"

    const-string v2, "Failed to load part data"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2151069
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 2151070
    new-instance v1, LX/EdT;

    invoke-direct {v1, v0}, LX/EdT;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2151071
    :catchall_1
    move-exception v0

    :goto_8
    if-eqz v4, :cond_12

    .line 2151072
    :try_start_8
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 2151073
    :cond_12
    :goto_9
    :try_start_9
    throw v0

    .line 2151074
    :catch_2
    move-exception v1

    .line 2151075
    const-string v2, "PduPersister"

    const-string v3, "Failed to close stream"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_9

    .line 2151076
    :cond_13
    if-eqz v5, :cond_14

    .line 2151077
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_14
    move-object v4, v0

    .line 2151078
    goto/16 :goto_0

    .line 2151079
    :catchall_2
    move-exception v0

    move-object v4, v1

    goto :goto_8

    .line 2151080
    :catch_3
    move-exception v0

    move-object v4, v1

    goto :goto_7

    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/EdM;
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2150948
    :try_start_0
    sget-object v1, LX/Edh;->d:LX/Edt;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2150949
    :try_start_1
    sget-object v0, LX/Edh;->d:LX/Edt;

    invoke-virtual {v0, p1}, LX/Edt;->a(Landroid/net/Uri;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2150950
    :try_start_2
    sget-object v0, LX/Edh;->d:LX/Edt;

    const v2, -0x23900fe

    invoke-static {v0, v2}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2150951
    :cond_0
    :goto_0
    :try_start_3
    sget-object v0, LX/Edh;->d:LX/Edt;

    invoke-virtual {v0, p1}, LX/Edp;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Edu;

    .line 2150952
    if-eqz v0, :cond_1

    .line 2150953
    iget-object v2, v0, LX/Edu;->a:LX/EdM;

    move-object v0, v2

    .line 2150954
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2150955
    sget-object v1, LX/Edh;->d:LX/Edt;

    monitor-enter v1

    .line 2150956
    :try_start_4
    sget-object v2, LX/Edh;->d:LX/Edt;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, LX/Edt;->a(Landroid/net/Uri;Z)V

    .line 2150957
    sget-object v2, LX/Edh;->d:LX/Edt;

    const v3, -0x552ba670

    invoke-static {v2, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 2150958
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2150959
    :goto_1
    return-object v0

    .line 2150960
    :catch_0
    move-exception v0

    .line 2150961
    :try_start_5
    const-string v2, "PduPersister"

    const-string v3, "load: "

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2150962
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2150963
    :catchall_1
    move-exception v0

    sget-object v1, LX/Edh;->d:LX/Edt;

    monitor-enter v1

    .line 2150964
    :try_start_7
    sget-object v2, LX/Edh;->d:LX/Edt;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, LX/Edt;->a(Landroid/net/Uri;Z)V

    .line 2150965
    sget-object v2, LX/Edh;->d:LX/Edt;

    const v3, 0x585be446

    invoke-static {v2, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 2150966
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    throw v0

    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    .line 2150967
    :cond_1
    :try_start_9
    sget-object v0, LX/Edh;->d:LX/Edt;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2}, LX/Edt;->a(Landroid/net/Uri;Z)V

    .line 2150968
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2150969
    :try_start_a
    iget-object v0, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v1, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    sget-object v3, LX/Edh;->a:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v6}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2150970
    new-instance v0, LX/Ede;

    invoke-direct {v0}, LX/Ede;-><init>()V

    .line 2150971
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-wide v2

    .line 2150972
    if-eqz v1, :cond_2

    :try_start_b
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-ne v4, v8, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    move-result v4

    if-nez v4, :cond_4

    .line 2150973
    :cond_2
    if-eqz v1, :cond_3

    .line 2150974
    :try_start_c
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 2150975
    :cond_3
    sget-object v1, LX/Edh;->d:LX/Edt;

    monitor-enter v1

    .line 2150976
    :try_start_d
    sget-object v0, LX/Edh;->d:LX/Edt;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, LX/Edt;->a(Landroid/net/Uri;Z)V

    .line 2150977
    sget-object v0, LX/Edh;->d:LX/Edt;

    const v2, 0x177ab36

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 2150978
    monitor-exit v1

    move-object v0, v7

    goto :goto_1

    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    throw v0

    .line 2150979
    :cond_4
    const/4 v4, 0x1

    :try_start_e
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 2150980
    invoke-static {v1, v0}, LX/Edh;->a(Landroid/database/Cursor;LX/Ede;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 2150981
    if-eqz v1, :cond_5

    .line 2150982
    :try_start_f
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2150983
    :cond_5
    cmp-long v1, v2, v10

    if-nez v1, :cond_7

    .line 2150984
    new-instance v0, LX/EdT;

    const-string v1, "Error! ID of the message: -1."

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150985
    :catchall_4
    move-exception v0

    if-eqz v1, :cond_6

    .line 2150986
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 2150987
    :cond_7
    invoke-direct {p0, v2, v3, v0}, LX/Edh;->a(JLX/Ede;)V

    .line 2150988
    const/16 v1, 0x8c

    invoke-virtual {v0, v1}, LX/Ede;->a(I)I

    move-result v1

    .line 2150989
    new-instance v6, LX/EdY;

    invoke-direct {v6}, LX/EdY;-><init>()V

    .line 2150990
    const/16 v5, 0x84

    if-eq v1, v5, :cond_8

    const/16 v5, 0x80

    if-ne v1, v5, :cond_9

    .line 2150991
    :cond_8
    invoke-static {p0, v2, v3}, LX/Edh;->b(LX/Edh;J)[LX/Edg;

    move-result-object v7

    .line 2150992
    if-eqz v7, :cond_9

    .line 2150993
    array-length v8, v7

    .line 2150994
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v8, :cond_9

    .line 2150995
    aget-object v9, v7, v5

    invoke-virtual {v6, v9}, LX/EdY;->a(LX/Edg;)Z

    .line 2150996
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2150997
    :cond_9
    move-object v2, v6

    .line 2150998
    invoke-static {v1, v0, v2}, LX/Edh;->a(ILX/Ede;LX/EdY;)LX/EdM;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    move-result-object v0

    .line 2150999
    sget-object v1, LX/Edh;->d:LX/Edt;

    monitor-enter v1

    .line 2151000
    if-eqz v0, :cond_a

    .line 2151001
    :try_start_10
    new-instance v2, LX/Edu;

    const-wide/16 v6, -0x1

    invoke-direct {v2, v0, v4, v6, v7}, LX/Edu;-><init>(LX/EdM;IJ)V

    .line 2151002
    sget-object v3, LX/Edh;->d:LX/Edt;

    invoke-virtual {v3, p1, v2}, LX/Edt;->a(Landroid/net/Uri;LX/Edu;)Z

    .line 2151003
    :cond_a
    sget-object v2, LX/Edh;->d:LX/Edt;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, LX/Edt;->a(Landroid/net/Uri;Z)V

    .line 2151004
    sget-object v2, LX/Edh;->d:LX/Edt;

    const v3, 0x616e3979

    invoke-static {v2, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 2151005
    monitor-exit v1

    goto/16 :goto_1

    :catchall_5
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    throw v0

    :catchall_6
    move-exception v0

    :try_start_11
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    throw v0
.end method

.method public final a(J)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 2150944
    sget-object v0, Landroid/provider/Telephony$MmsSms$PendingMessages;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 2150945
    const-string v0, "protocol"

    const-string v1, "mms"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2150946
    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "10"

    aput-object v1, v5, v0

    const/4 v0, 0x1

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    .line 2150947
    iget-object v0, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v1, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "err_type < ? AND due_time <= ?"

    const-string v6, "due_time"

    invoke-static/range {v0 .. v6}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EdM;Landroid/net/Uri;ILjava/lang/String;ZLjava/util/Map;)Landroid/net/Uri;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EdM;",
            "Landroid/net/Uri;",
            "I",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/io/InputStream;",
            ">;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .line 2150817
    if-nez p2, :cond_0

    .line 2150818
    new-instance v4, LX/EdT;

    const-string v5, "Uri may not be null."

    invoke-direct {v4, v5}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2150819
    :cond_0
    const-wide/16 v14, -0x1

    .line 2150820
    :try_start_0
    invoke-static/range {p2 .. p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v14

    .line 2150821
    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v14, v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    move/from16 v16, v4

    .line 2150822
    :goto_1
    if-nez v16, :cond_2

    sget-object v4, LX/Edh;->g:LX/01J;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    .line 2150823
    new-instance v4, LX/EdT;

    const-string v5, "Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp."

    invoke-direct {v4, v5}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2150824
    :cond_1
    const/4 v4, 0x0

    move/from16 v16, v4

    goto :goto_1

    .line 2150825
    :cond_2
    sget-object v5, LX/Edh;->d:LX/Edt;

    monitor-enter v5

    .line 2150826
    :try_start_1
    sget-object v4, LX/Edh;->d:LX/Edt;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, LX/Edt;->a(Landroid/net/Uri;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_3

    .line 2150827
    :try_start_2
    sget-object v4, LX/Edh;->d:LX/Edt;

    const v6, 0x1701957

    invoke-static {v4, v6}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2150828
    :cond_3
    :goto_2
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2150829
    sget-object v4, LX/Edh;->d:LX/Edt;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, LX/Edt;->b(Landroid/net/Uri;)LX/Edu;

    .line 2150830
    invoke-virtual/range {p1 .. p1}, LX/EdM;->a()LX/Ede;

    move-result-object v7

    .line 2150831
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 2150832
    const-string v4, "seen"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2150833
    sget-object v4, LX/Edh;->n:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    :goto_3
    add-int/lit8 v6, v4, -0x1

    if-ltz v6, :cond_5

    .line 2150834
    sget-object v4, LX/Edh;->n:Landroid/util/SparseArray;

    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 2150835
    invoke-virtual {v7, v4}, LX/Ede;->c(I)LX/EdS;

    move-result-object v8

    .line 2150836
    if-eqz v8, :cond_4

    .line 2150837
    sget-object v5, LX/Edh;->m:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2150838
    sget-object v5, LX/Edh;->n:Landroid/util/SparseArray;

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v8}, LX/EdS;->b()[B

    move-result-object v9

    invoke-static {v9}, LX/Edh;->a([B)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2150839
    invoke-virtual {v8}, LX/EdS;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_4
    move v4, v6

    .line 2150840
    goto :goto_3

    .line 2150841
    :catch_0
    move-exception v4

    .line 2150842
    :try_start_4
    const-string v6, "PduPersister"

    const-string v7, "persist1: "

    invoke-static {v6, v7, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2150843
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 2150844
    :cond_5
    sget-object v4, LX/Edh;->o:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    :goto_4
    add-int/lit8 v5, v4, -0x1

    if-ltz v5, :cond_7

    .line 2150845
    sget-object v4, LX/Edh;->o:Landroid/util/SparseArray;

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v7, v4}, LX/Ede;->b(I)[B

    move-result-object v6

    .line 2150846
    if-eqz v6, :cond_6

    .line 2150847
    sget-object v4, LX/Edh;->o:Landroid/util/SparseArray;

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v6}, LX/Edh;->a([B)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move v4, v5

    .line 2150848
    goto :goto_4

    .line 2150849
    :cond_7
    sget-object v4, LX/Edh;->p:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    :goto_5
    add-int/lit8 v5, v4, -0x1

    if-ltz v5, :cond_9

    .line 2150850
    sget-object v4, LX/Edh;->p:Landroid/util/SparseArray;

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v7, v4}, LX/Ede;->a(I)I

    move-result v6

    .line 2150851
    if-eqz v6, :cond_8

    .line 2150852
    sget-object v4, LX/Edh;->p:Landroid/util/SparseArray;

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_8
    move v4, v5

    .line 2150853
    goto :goto_5

    .line 2150854
    :cond_9
    sget-object v4, LX/Edh;->q:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    :goto_6
    add-int/lit8 v5, v4, -0x1

    if-ltz v5, :cond_b

    .line 2150855
    sget-object v4, LX/Edh;->q:Landroid/util/SparseArray;

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v7, v4}, LX/Ede;->e(I)J

    move-result-wide v8

    .line 2150856
    const-wide/16 v10, -0x1

    cmp-long v4, v8, v10

    if-eqz v4, :cond_a

    .line 2150857
    sget-object v4, LX/Edh;->q:Landroid/util/SparseArray;

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_a
    move v4, v5

    .line 2150858
    goto :goto_6

    .line 2150859
    :cond_b
    new-instance v18, Landroid/util/SparseArray;

    sget-object v4, LX/Edh;->e:[I

    array-length v4, v4

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Landroid/util/SparseArray;-><init>(I)V

    .line 2150860
    sget-object v6, LX/Edh;->e:[I

    array-length v8, v6

    const/4 v4, 0x0

    move v5, v4

    :goto_7
    if-ge v5, v8, :cond_e

    aget v9, v6, v5

    .line 2150861
    const/4 v4, 0x0

    .line 2150862
    const/16 v10, 0x89

    if-ne v9, v10, :cond_d

    .line 2150863
    invoke-virtual {v7, v9}, LX/Ede;->c(I)LX/EdS;

    move-result-object v10

    .line 2150864
    if-eqz v10, :cond_c

    .line 2150865
    const/4 v4, 0x1

    new-array v4, v4, [LX/EdS;

    .line 2150866
    const/4 v11, 0x0

    aput-object v10, v4, v11

    .line 2150867
    :cond_c
    :goto_8
    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150868
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_7

    .line 2150869
    :cond_d
    invoke-virtual {v7, v9}, LX/Ede;->d(I)[LX/EdS;

    move-result-object v4

    goto :goto_8

    .line 2150870
    :cond_e
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2150871
    invoke-virtual/range {p1 .. p1}, LX/EdM;->b()I

    move-result v4

    .line 2150872
    const/16 v5, 0x82

    if-eq v4, v5, :cond_f

    const/16 v5, 0x84

    if-eq v4, v5, :cond_f

    const/16 v5, 0x80

    if-ne v4, v5, :cond_12

    .line 2150873
    :cond_f
    packed-switch v4, :pswitch_data_0

    .line 2150874
    :goto_9
    :pswitch_0
    const-wide/16 v4, -0x1

    .line 2150875
    if-eqz p5, :cond_11

    .line 2150876
    invoke-virtual {v6}, Ljava/util/HashSet;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_18

    .line 2150877
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Edh;->r:Landroid/content/Context;

    invoke-static {v4, v6}, LX/2Uo;->a(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v12

    .line 2150878
    const-wide/16 v4, 0x0

    cmp-long v4, v12, v4

    if-lez v4, :cond_26

    .line 2150879
    const/4 v11, 0x0

    .line 2150880
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Edh;->r:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Edh;->s:Landroid/content/ContentResolver;

    sget-object v6, Landroid/provider/Telephony$MmsSms;->CONTENT_CONVERSATIONS_URI:Landroid/net/Uri;

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    sget-object v7, LX/Edh;->b:[Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "normalized_date DESC LIMIT 1"

    invoke-static/range {v4 .. v10}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v5

    .line 2150881
    if-eqz v5, :cond_10

    .line 2150882
    :try_start_6
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 2150883
    const-string v4, "date"

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    add-long/2addr v6, v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 2150884
    :cond_10
    if-eqz v5, :cond_17

    .line 2150885
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    move-wide v4, v12

    .line 2150886
    :cond_11
    :goto_a
    const-string v6, "thread_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2150887
    :cond_12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 2150888
    const/4 v4, 0x1

    .line 2150889
    const/4 v5, 0x0

    .line 2150890
    move-object/from16 v0, p1

    instance-of v6, v0, LX/EdV;

    if-eqz v6, :cond_1a

    .line 2150891
    check-cast p1, LX/EdV;

    invoke-virtual/range {p1 .. p1}, LX/EdV;->d()LX/EdY;

    move-result-object v7

    .line 2150892
    if-eqz v7, :cond_1a

    .line 2150893
    invoke-virtual {v7}, LX/EdY;->b()I

    move-result v8

    .line 2150894
    const/4 v6, 0x2

    if-le v8, v6, :cond_13

    .line 2150895
    const/4 v4, 0x0

    .line 2150896
    :cond_13
    const/4 v6, 0x0

    move/from16 v22, v5

    move v5, v4

    move/from16 v4, v22

    :goto_b
    if-ge v6, v8, :cond_1b

    .line 2150897
    invoke-virtual {v7, v6}, LX/EdY;->a(I)LX/Edg;

    move-result-object v9

    .line 2150898
    invoke-virtual {v9}, LX/Edg;->b()Landroid/net/Uri;

    move-result-object v10

    if-eqz v10, :cond_19

    const-string v10, "content"

    invoke-virtual {v9}, LX/Edg;->b()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_19

    .line 2150899
    :try_start_7
    move-object/from16 v0, p0

    iget-object v10, v0, LX/Edh;->s:Landroid/content/ContentResolver;

    invoke-virtual {v9}, LX/Edg;->b()Landroid/net/Uri;

    move-result-object v11

    const-string v12, "r"

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/AssetFileDescriptor;->getLength()J
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    move-result-wide v10

    long-to-int v10, v10

    .line 2150900
    const/4 v11, -0x1

    if-eq v10, v11, :cond_14

    .line 2150901
    add-int/2addr v4, v10

    .line 2150902
    :cond_14
    :goto_c
    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    move-object/from16 v3, p6

    invoke-virtual {v0, v9, v1, v2, v3}, LX/Edh;->a(LX/Edg;JLjava/util/Map;)Landroid/net/Uri;

    .line 2150903
    invoke-static {v9}, LX/Edh;->a(LX/Edg;)Ljava/lang/String;

    move-result-object v9

    .line 2150904
    if-eqz v9, :cond_15

    const-string v10, "application/smil"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_15

    const-string v10, "text/plain"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_15

    .line 2150905
    const/4 v5, 0x0

    .line 2150906
    :cond_15
    add-int/lit8 v6, v6, 0x1

    goto :goto_b

    .line 2150907
    :pswitch_1
    const/16 v4, 0x89

    move-object/from16 v0, v18

    invoke-static {v4, v6, v0}, LX/Edh;->a(ILjava/util/HashSet;Landroid/util/SparseArray;)V

    .line 2150908
    move-object/from16 v0, v18

    move-object/from16 v1, p4

    invoke-static {v6, v0, v1}, LX/Edh;->a(Ljava/util/HashSet;Landroid/util/SparseArray;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 2150909
    :pswitch_2
    const/16 v4, 0x97

    move-object/from16 v0, v18

    invoke-static {v4, v6, v0}, LX/Edh;->a(ILjava/util/HashSet;Landroid/util/SparseArray;)V

    goto/16 :goto_9

    .line 2150910
    :catchall_1
    move-exception v4

    move-object v5, v11

    :goto_d
    if-eqz v5, :cond_16

    .line 2150911
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_16
    throw v4

    :cond_17
    move-wide v4, v12

    .line 2150912
    goto/16 :goto_a

    .line 2150913
    :cond_18
    const-string v4, "PduPersister"

    const-string v5, "Empty recipients for Uri %s, skip this download"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2150914
    new-instance v4, LX/EdT;

    const-string v5, "Asked to create thread id but got empty recipients"

    invoke-direct {v4, v5}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2150915
    :cond_19
    invoke-virtual {v9}, LX/Edg;->k()I

    move-result v10

    add-int/2addr v4, v10

    goto :goto_c

    :cond_1a
    move/from16 v22, v5

    move v5, v4

    move/from16 v4, v22

    .line 2150916
    :cond_1b
    invoke-static {}, LX/EdL;->d()Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 2150917
    const-string v6, "text_only"

    if-eqz v5, :cond_21

    const/4 v5, 0x1

    :goto_e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2150918
    :cond_1c
    const-string v5, "m_size"

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    if-nez v5, :cond_1d

    .line 2150919
    const-string v5, "m_size"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2150920
    :cond_1d
    invoke-static {}, LX/EdL;->e()Z

    move-result v4

    if-eqz v4, :cond_22

    .line 2150921
    const-string v4, "sub_id"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2150922
    :cond_1e
    if-eqz v16, :cond_23

    .line 2150923
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Edh;->r:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Edh;->s:Landroid/content/ContentResolver;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v6, p2

    move-object/from16 v7, v17

    invoke-static/range {v4 .. v9}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v10, p2

    move-wide v12, v14

    .line 2150924
    :goto_f
    new-instance v7, Landroid/content/ContentValues;

    const/4 v4, 0x1

    invoke-direct {v7, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 2150925
    const-string v4, "mid"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2150926
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Edh;->r:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Edh;->s:Landroid/content/ContentResolver;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "content://mms/"

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "/part"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2150927
    if-nez v16, :cond_1f

    .line 2150928
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 2150929
    :cond_1f
    sget-object v6, LX/Edh;->e:[I

    array-length v7, v6

    const/4 v4, 0x0

    move v5, v4

    :goto_10
    if-ge v5, v7, :cond_25

    aget v8, v6, v5

    .line 2150930
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [LX/EdS;

    .line 2150931
    if-eqz v4, :cond_20

    .line 2150932
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13, v8, v4}, LX/Edh;->a(JI[LX/EdS;)V

    .line 2150933
    :cond_20
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_10

    .line 2150934
    :cond_21
    const/4 v5, 0x0

    goto/16 :goto_e

    .line 2150935
    :cond_22
    const/4 v4, -0x1

    move/from16 v0, p3

    if-eq v4, v0, :cond_1e

    .line 2150936
    new-instance v4, LX/EdT;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Incorrect subscription Id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2150937
    :cond_23
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Edh;->r:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Edh;->s:Landroid/content/ContentResolver;

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-static {v4, v5, v0, v1}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 2150938
    if-nez v4, :cond_24

    .line 2150939
    new-instance v4, LX/EdT;

    const-string v5, "persist() failed: return null."

    invoke-direct {v4, v5}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2150940
    :cond_24
    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    move-object v10, v4

    move-wide v12, v6

    goto/16 :goto_f

    :catch_1
    goto/16 :goto_0

    .line 2150941
    :cond_25
    return-object v10

    .line 2150942
    :catch_2
    goto/16 :goto_c

    .line 2150943
    :catchall_2
    move-exception v4

    goto/16 :goto_d

    :cond_26
    move-wide v4, v12

    goto/16 :goto_a

    :pswitch_data_0
    .packed-switch 0x80
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/Edg;JLjava/util/Map;)Landroid/net/Uri;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Edg;",
            "J",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/io/InputStream;",
            ">;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .line 2150786
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://mms/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/part"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2150787
    new-instance v3, Landroid/content/ContentValues;

    const/16 v0, 0x8

    invoke-direct {v3, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 2150788
    invoke-virtual {p1}, LX/Edg;->d()I

    move-result v2

    .line 2150789
    if-eqz v2, :cond_0

    .line 2150790
    const-string v0, "chset"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2150791
    :cond_0
    invoke-static {p1}, LX/Edh;->a(LX/Edg;)Ljava/lang/String;

    move-result-object v0

    .line 2150792
    iget-object v4, p1, LX/Edg;->f:[B

    move-object v4, v4

    .line 2150793
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "PduPersister.persistPart part: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " contentType: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2150794
    if-eqz v0, :cond_4

    .line 2150795
    const-string v5, "image/jpg"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2150796
    const-string v0, "image/jpeg"

    .line 2150797
    :cond_1
    const-string v5, "text/plain"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v4, :cond_2

    .line 2150798
    new-instance v5, LX/EdS;

    invoke-direct {v5, v2, v4}, LX/EdS;-><init>(I[B)V

    invoke-virtual {v5}, LX/EdS;->c()Ljava/lang/String;

    move-result-object v2

    .line 2150799
    if-eqz v2, :cond_2

    const-string v4, "BEGIN:VCARD"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2150800
    const-string v0, "text/x-vCard"

    .line 2150801
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {p1, v2}, LX/Edg;->e([B)V

    .line 2150802
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "PduPersister.persistPart part: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " contentType: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " changing to vcard"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2150803
    :cond_2
    const-string v2, "ct"

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2150804
    const-string v2, "application/smil"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2150805
    const-string v2, "seq"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2150806
    :cond_3
    invoke-static {p1, v3}, LX/Edh;->a(LX/Edg;Landroid/content/ContentValues;)V

    .line 2150807
    const/4 v2, 0x0

    .line 2150808
    :try_start_0
    iget-object v4, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v5, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    invoke-static {v4, v5, v1, v3}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2150809
    :goto_0
    if-nez v1, :cond_5

    .line 2150810
    new-instance v0, LX/EdT;

    const-string v1, "Failed to persist part, return null."

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150811
    :cond_4
    new-instance v0, LX/EdT;

    const-string v1, "MIME type of the part must be set."

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150812
    :catch_0
    move-exception v1

    .line 2150813
    const-string v3, "PduPersister"

    const-string v4, "SqliteWrapper.insert threw: "

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_0

    .line 2150814
    :cond_5
    invoke-static {p0, p1, v1, v0, p4}, LX/Edh;->a(LX/Edh;LX/Edg;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)V

    .line 2150815
    iput-object v1, p1, LX/Edg;->e:Landroid/net/Uri;

    .line 2150816
    return-object v1
.end method

.method public final a(Landroid/net/Uri;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2150776
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    .line 2150777
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-nez v0, :cond_0

    .line 2150778
    new-instance v0, LX/EdT;

    const-string v1, "Error! ID of the message: -1."

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150779
    :cond_0
    sget-object v0, LX/Edh;->g:LX/01J;

    invoke-virtual {v0, p2}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2150780
    if-nez v0, :cond_1

    .line 2150781
    new-instance v0, LX/EdT;

    const-string v1, "Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp."

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150782
    :cond_1
    new-instance v3, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v3, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 2150783
    const-string v1, "msg_box"

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2150784
    iget-object v0, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v1, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    move-object v2, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2150785
    invoke-static {p2, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2150773
    const-string v0, "content://mms/9223372036854775807/part"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2150774
    iget-object v1, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v2, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    invoke-static {v1, v2, v0, v3, v3}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2150775
    return-void
.end method

.method public final a(Landroid/net/Uri;LX/EdY;Ljava/util/Map;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/EdY;",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 2150726
    :try_start_0
    sget-object v3, LX/Edh;->d:LX/Edt;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2150727
    :try_start_1
    sget-object v0, LX/Edh;->d:LX/Edt;

    invoke-virtual {v0, p1}, LX/Edt;->a(Landroid/net/Uri;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2150728
    :try_start_2
    sget-object v0, LX/Edh;->d:LX/Edt;

    const v4, 0x4a9796e7    # 4967283.5f

    invoke-static {v0, v4}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2150729
    :goto_0
    :try_start_3
    sget-object v0, LX/Edh;->d:LX/Edt;

    invoke-virtual {v0, p1}, LX/Edp;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Edu;

    .line 2150730
    if-eqz v0, :cond_0

    .line 2150731
    iget-object v4, v0, LX/Edu;->a:LX/EdM;

    move-object v0, v4

    .line 2150732
    check-cast v0, LX/EdV;

    .line 2150733
    iput-object p2, v0, LX/EdV;->b:LX/EdY;

    .line 2150734
    :cond_0
    sget-object v0, LX/Edh;->d:LX/Edt;

    const/4 v4, 0x1

    invoke-virtual {v0, p1, v4}, LX/Edt;->a(Landroid/net/Uri;Z)V

    .line 2150735
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2150736
    :try_start_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2150737
    new-instance v4, LX/026;

    invoke-direct {v4}, LX/026;-><init>()V

    .line 2150738
    invoke-virtual {p2}, LX/EdY;->b()I

    move-result v5

    .line 2150739
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v0, "("

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v0, v2

    .line 2150740
    :goto_1
    if-ge v0, v5, :cond_4

    .line 2150741
    invoke-virtual {p2, v0}, LX/EdY;->a(I)LX/Edg;

    move-result-object v2

    .line 2150742
    iget-object v7, v2, LX/Edg;->e:Landroid/net/Uri;

    move-object v7, v7

    .line 2150743
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v8

    const-string v9, "mms"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 2150744
    :cond_1
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2150745
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2150746
    :catch_0
    move-exception v0

    .line 2150747
    :try_start_5
    const-string v4, "PduPersister"

    const-string v5, "updateParts: "

    invoke-static {v4, v5, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2150748
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2150749
    :catchall_1
    move-exception v0

    sget-object v1, LX/Edh;->d:LX/Edt;

    monitor-enter v1

    .line 2150750
    :try_start_7
    sget-object v2, LX/Edh;->d:LX/Edt;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, LX/Edt;->a(Landroid/net/Uri;Z)V

    .line 2150751
    sget-object v2, LX/Edh;->d:LX/Edt;

    const v3, -0x3d414ca5

    invoke-static {v2, v3}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 2150752
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    .line 2150753
    :cond_2
    :try_start_8
    invoke-virtual {v4, v7, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2150754
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-le v2, v10, :cond_3

    .line 2150755
    const-string v2, " AND "

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2150756
    :cond_3
    const-string v2, "_id"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2150757
    const-string v2, "!="

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2150758
    invoke-virtual {v7}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_2

    .line 2150759
    :cond_4
    const/16 v0, 0x29

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2150760
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 2150761
    iget-object v2, p0, LX/Edh;->r:Landroid/content/Context;

    iget-object v5, p0, LX/Edh;->s:Landroid/content/ContentResolver;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "/part"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v10, 0x2

    if-le v0, v10, :cond_5

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    const/4 v1, 0x0

    invoke-static {v2, v5, v7, v0, v1}, LX/550;->a(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2150762
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Edg;

    .line 2150763
    invoke-virtual {p0, v0, v8, v9, p3}, LX/Edh;->a(LX/Edg;JLjava/util/Map;)Landroid/net/Uri;

    goto :goto_4

    :cond_5
    move-object v0, v1

    .line 2150764
    goto :goto_3

    .line 2150765
    :cond_6
    invoke-virtual {v4}, LX/026;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2150766
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Edg;

    invoke-direct {p0, v1, v0, p3}, LX/Edh;->a(Landroid/net/Uri;LX/Edg;Ljava/util/Map;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_5

    .line 2150767
    :cond_7
    sget-object v1, LX/Edh;->d:LX/Edt;

    monitor-enter v1

    .line 2150768
    :try_start_9
    sget-object v0, LX/Edh;->d:LX/Edt;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, LX/Edt;->a(Landroid/net/Uri;Z)V

    .line 2150769
    sget-object v0, LX/Edh;->d:LX/Edt;

    const v2, 0x282d3b7d

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 2150770
    monitor-exit v1

    .line 2150771
    return-void

    .line 2150772
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v0

    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v0
.end method
