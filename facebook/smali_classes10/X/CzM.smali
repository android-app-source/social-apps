.class public LX/CzM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1954751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/CzL;I)LX/CzL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
            ">;I)",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954750
    invoke-static {p0, p1}, LX/CzM;->b(LX/CzL;I)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;I)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;
    .locals 4

    .prologue
    .line 1954742
    if-nez p0, :cond_0

    .line 1954743
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parent node is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1954744
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    .line 1954745
    if-nez v0, :cond_1

    .line 1954746
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Module results are not defined."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1954747
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lt p1, v1, :cond_2

    .line 1954748
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Item index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " out of bounds for module with size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1954749
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    return-object v0
.end method

.method public static b(LX/CzL;I)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
            ">;I)",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;"
        }
    .end annotation

    .prologue
    .line 1954729
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1954730
    instance-of v0, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    if-nez v0, :cond_0

    .line 1954731
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected module type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1954732
    iget-object v2, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1954733
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1954734
    :cond_0
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1954735
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-static {v0, p1}, LX/CzM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;I)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/CzL;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1954736
    iget-object v1, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 1954737
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    .line 1954738
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    .line 1954739
    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    move-object v1, v0

    .line 1954740
    goto :goto_0

    .line 1954741
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
